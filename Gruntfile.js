module.exports = function(grunt) {
  grunt.initConfig({
    sprite:{
      all: {
        src: 'img/sprites/*.png',
        dest: 'img/spritesheet.png',
        destCss: 'styles/sprites.css',
        cssTemplate: function(params) {
          var fs = require('fs'),
            mustache = require('mustache'),
            tmpl = fs.readFileSync(__dirname + '/views/css.template.mustache', 'utf8'),
            items = params.items,
            options = params.options;

          // Fallback class naming function
          var classFn = options.cssClass || function defaultCssClass (item) {
                return '.sprites-' + item.name;
              };

          // Add class to each of the options
          items.forEach(function saveClass (item) {
            item['class'] = classFn(item);
          });
          // Set the default class
          options.cssClass = options.cssClass || '.sprites';

          // Render and return CSS
          var css = mustache.render(tmpl, params);
          return css;
        }
      }
    },
    watch: {
      scripts: {
        files: ['img/sprites/*.png'],
        tasks: ['sprite', 'cssmin', 'uglify'],
        options: {
          spawn: false,
        },
      },
    },
    cssmin: {
        all: {
            options: {
                keepSpecialComments: 0
            },
            files: [{
                'styles/main.min.css': [
                    'styles/jquery-ui.css',
                    'styles/style.css',
                    'styles/jquery.autocomplete.css',
                    'styles/calendar.css',
                    'styles/tabs.css',
                    'styles/messagebar.css',
                    'styles/validationEngine.jquery.css',
                    'styles/jkmegamenu.css',
                    'styles/bootstrap.css',
                    'styles/bootstrap-rtl.css',
                    'styles/overrides.css',
                    'styles/BYekan.css',
                    'styles/ui.css',
                    'styles/utility.css',
                    'styles/padding.css',
                    'styles/margin.css',
                    'styles/gap.css',
                    'styles/switch.css',
                    'styles/table.css',
                    'styles/chosen.min.css',
                    'styles/fileuploader.css',
                    'styles/font-awesome/css/font-awesome.min.css',
                    'styles/sprites.css',
                    'styles/flexigrid.pack.css',
                    'styles/query-builder.default.css'
                ]
            }]
        }
    },
    uglify: {
        all: {
            files: {
                'js/main.min.js': [
                    'js/jquery-1.4.1.min.js',
                    'js/jquery-ui.min.js',
                    'js/plugins.js',
                    'js/JsFarsiCalendar.js',
                    'js/jscolor.js',
                    'js/enums.js',
                    'js/site.js',
                    'js/quicklaunch.js',
                    'js/directives.js',
                    'js/app.js',
                    'js/handlebars.min.js',
                    'js/helpers.js',
                    'js/jquery.regex.js',
                    'js/grid-keyboard-shortcuts.js',
                    'js/chosen.jquery.min.js',
                    'js/growl.js',
                    'js/smarttable.js',
                    'js/router.js',
                    'js/jquery.scrollintoview.min.js',
                    'js/TableKeyboardNavigation.js'
                ],
                'js/super.min.js': [
                    'js/jquery-1.9.1.min.js',
                    'js/bootstrap.min.js',
                    'js/shortcut.js',
                    'js/jquery-ui-1.9.2.min.js',
                    'js/underscore-min.js',
                    'js/backbone-min.js',
                    'js/jQuery.extendext.js',
                    'js/moment.js',
                    'js/query-builder.js',
                    'js/query-builder.fa.js',
                    'js/jquery.mobile.custom.js'
                ],
                'js/lang/fa.min.js': [
                    'js/lang/fa.js'
                ]
            }
        }
    },
    sass: {
        dist: {
            options: {
                style: 'compressed'
            },
            files: {
                'styles/gap.css': 'styles/gap.scss',
                'styles/padding.css': 'styles/padding.scss',
                'styles/margin.css': 'styles/margin.scss',
                'styles/utility.css': 'styles/utility.scss'
            }
        }
    }
  });

  // Load in `grunt-spritesmith`
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-spritesmith');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');

  grunt.registerTask('default', [
    'sprite',
    'cssmin',
    'uglify'
  ]);

  grunt.registerTask('release', [
    'sass',
    'sprite',
    'cssmin',
    'uglify'
  ]);
};