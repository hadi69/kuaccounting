﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class welcome2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            #region Labs
            //DataTable labs = DbProxy.Instance.FillData("SELECT ID, Name FROM Ward WHERE SectionType=0 ORDER BY [Name]", "Lab");
            //if (labs != null && labs.Rows.Count > 0)
            //    mLabs.Controls.Add(new LiteralControl(string.Format("<a href='list.aspx?table=FileService&view=tests&wardid={0}&menu=paraclinica'>پاراکلینیک</a>", labs.Rows[0][0], labs.Rows[0][1])));
            #endregion

            #region Clinic
            //DataTable clinics = DbProxy.Instance.FillData("SELECT TOP 1 ID, Name, LocationName FROM WardView WHERE SectionType=3 ORDER BY LocationName", "Clinic");
            //if (clinics != null && clinics.Rows.Count > 0)
            //    mClinics.Controls.Add(new LiteralControl(string.Format("<a href='list.aspx?table=Files&view=fileclinics&wardid={0}&menu=clinica'>کلینیک</a>", clinics.Rows[0][0], clinics.Rows[0][1])));
            #endregion

            #region Pharmacy
            //DataTable pharmacies = DbProxy.Instance.FillData("SELECT ID, Name FROM Ward WHERE SectionType=2 ORDER BY [Name]", "Lab");
            //if (pharmacies != null && pharmacies.Rows.Count > 0)
            //    mPharmacy.Controls.Add(new LiteralControl(string.Format("<a href='list.aspx?table=FileService&view=Prescriptions&wardid={0}&menu=druga'>داروخانه</a>", pharmacies.Rows[0][0], pharmacies.Rows[0][1])));
            #endregion
        }
    }
    protected int LabID
    {
        get
        {
            object labID = DbProxy.Instance.FillScalar("SELECT TOP 1 ID FROM Ward WHERE SectionType=0 ORDER BY [Name]");
            return Helper.GetInt(labID, -1);
        }
    }
    protected int ClinicID
    {
        get
        {
            object clinicID = DbProxy.Instance.FillScalar("SELECT TOP 1 ID FROM Ward WHERE SectionType=3 ORDER BY [Name]");
            return Helper.GetInt(clinicID, -1);
        }
    }
    protected int PharmacyID
    {
        get
        {
            object pharmacyID = DbProxy.Instance.FillScalar("SELECT TOP 1 ID FROM Ward WHERE SectionType=2 ORDER BY [Name]");
            return Helper.GetInt(pharmacyID, -1);
        }
    }
}