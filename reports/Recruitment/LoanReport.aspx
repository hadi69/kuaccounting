﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="LoanReport.aspx.cs" Inherits="reports_Recruitment_LoanReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" Text="Lang.Year" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"></asp:Label>
                    <div class="col-xs-12 col-sm-8">
                        <asp:DropDownList ID="ddlYear" runat="server" PlaceHolder="YYYY" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label1" runat="server" Text="Lang.Month" PlaceHolder="MM" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"></asp:Label>
                    <div class="col-xs-12 col-sm-8">
                        <asp:DropDownList ID="ddlMonth" runat="server" CssClass="chosen-select chosen-rtl">
                            <asp:ListItem Value="01">Lang.Farvardin</asp:ListItem>
                            <asp:ListItem Value="02">Lang.Ordibehesht</asp:ListItem>
                            <asp:ListItem Value="03">Lang.Khordad</asp:ListItem>
                            <asp:ListItem Value="04">Lang.Tir</asp:ListItem>
                            <asp:ListItem Value="05">Lang.Mordad</asp:ListItem>
                            <asp:ListItem Value="06">Lang.Shahrivar</asp:ListItem>
                            <asp:ListItem Value="07">Lang.Mehr</asp:ListItem>
                            <asp:ListItem Value="08">Lang.Aban</asp:ListItem>
                            <asp:ListItem Value="09">Lang.Azar</asp:ListItem>
                            <asp:ListItem Value="10">Lang.Dey</asp:ListItem>
                            <asp:ListItem Value="11">Lang.Bahman</asp:ListItem>
                            <asp:ListItem Value="12">Lang.Esfand</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">
                <div class="form-group">
                    <asp:Label Text="Lang.Personnel" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4" />
                    <div class="col-xs-12 col-sm-8">
                        <asp:DropDownList runat="server" ID="ddlPersonnel" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4">
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <input type="button" id="showReport" class="btn btn-sm btn-primary col-xs-12" value="Print" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#bar").prepend(breadcrumb({
                title: Lang.FieldReport,
                grandParent: {
                    title: Lang.MSalary,
                    url: 'menu/salary.aspx'
                },
                parent: {
                    title: Lang.SalaryReports,
                    url: 'menu/salaryreports.aspx'
                }
            }));
            setClassSalary();
        });
        $("#showReport").attr('value', Lang[$('#showReport').val()]);
        $("#showReport").click(function () {
            var year = $("#<%=ddlYear.ClientID%>").val();
            var month = $("#<%=ddlMonth.ClientID%>").val();
            var personnel = $("#<%=ddlPersonnel.ClientID%>").val();

            window.open(rootPath + Router.Generate("print-loanreport", { params: { year: year, month: month }, query: { personnel: personnel } }));
        });
    </script>
</asp:Content>

