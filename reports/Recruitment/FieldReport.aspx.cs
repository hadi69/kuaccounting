﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reports_Recruitment_FieldReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindPersonnel();
            BindYear();
            ShowFields();
            Page.SetLang();
        }
        

    }
    void BindPersonnel()
    {
        DataTable personnels = DbProxy.Instance.FillData("SELECT ID, [dbo].[DisplayPerson](ID, 0) AS PersonnelName  FROM Personnel WHERE PersonnelGroupID=" + DbProxy.Instance.GetConfig("PersonnelGroup"), "personnels");
        
        ddlPersonnel.DataTextField = "PersonnelName";
        ddlPersonnel.DataValueField = "ID";
        ddlPersonnel.DataSource = personnels;
        ddlPersonnel.DataBind();
        ddlPersonnel.Items.Insert(0, new ListItem("همه", ""));
    }

    void BindYear()
    {
        PersianCalendar calendar = new PersianCalendar();
        int year = calendar.GetYear(DateTime.Now);
        ddlYear.Items.Add((year - 1).ToString());
        ddlYear.Items.Add(year.ToString());
        ddlYear.Items.Add((year + 1).ToString());
        ddlYear.SelectedIndex = 1;
    }

    void ShowFields()
    {
        string FieldName="", FieldTitle = "";
        bool printable = false;
        DataTable functions = DbProxy.Instance.FillData(@"SELECT Fields.* FROM Fields INNER JOIN FishDefFields ON Fields.ID=FishDefFields.FieldID WHERE FieldShowType=1 ORDER BY Priority", "functions");
        if (functions != null)
        {
            MyPlaceHolder.Controls.Add(new LiteralControl(@"<div class='col-xs-12 col-sm-6 col-lg-3 padding-5-top padding-5-bottom'>"));
            MyPlaceHolder.Controls.Add(new LiteralControl(@"<div class='col-xs-12 form-group-container'>
                            <div class='form-group'>
                                <div class='col-sm-2'>&nbsp;</div>
                                <div class='col-xs-12 col-sm-10 form-col-without-padding'>
                                    <label class='pull-left-sm pull-left-md pull-left-lg'>
                                        <span class='control-label left-aligned-label padding-0-left padding-0-right selectall'>SelectAll</span>
                                        <input type='checkbox' id='AllFunction' name='AllFunction' v='' class='ios-switch'/>
                                        <div class='switch'></div>
                                    </label>
                                </div>
                            </div>
                        </div>"));
            for (int i = 0; i < functions.Rows.Count; i++)
            {
                FieldName = Helper.GetString(functions.Rows[i], "Name", "");
                FieldTitle = Helper.GetString(functions.Rows[i], "Title", "");
                printable = Helper.GetBool(functions.Rows[i], "Printable", false);
                MyPlaceHolder.Controls.Add(new LiteralControl(string.Format(@"
                        <div class='col-xs-12 form-group-container'>
                            <div class='form-group'>
                                <div class='col-sm-2'>&nbsp;</div>
                                <div class='col-xs-12 col-sm-10 form-col-without-padding'>
                                    <label class='pull-left-sm pull-left-md pull-left-lg'>
                                        <span class='control-label left-aligned-label padding-0-left padding-0-right'>{0}</span>
                                        <input type='checkbox' id='{1}' name='{1}' v='' class='ios-switch function' {2}/>
                                        <div class='switch'></div>
                                    </label>
                                </div>
                            </div>
                        </div>", FieldTitle,FieldName,printable?"checked":"")));
            }
            MyPlaceHolder.Controls.Add(new LiteralControl(@"</div>"));
        }
        DataTable benefits = DbProxy.Instance.FillData(@"SELECT Fields.* FROM Fields INNER JOIN FishDefFields ON Fields.ID=FishDefFields.FieldID WHERE FieldShowType=2 ORDER BY Priority", "benefits");
        if (benefits != null)
        {
            MyPlaceHolder.Controls.Add(new LiteralControl(@"<div class='col-xs-12 col-sm-6 col-lg-3 padding-5-top padding-5-bottom' style='border-left: 3px double #999; border-right: 3px double #999;'>"));
            MyPlaceHolder.Controls.Add(new LiteralControl(@"<div class='col-xs-12 form-group-container'>
                            <div class='form-group'>
                                <div class='col-sm-2'>&nbsp;</div>
                                <div class='col-xs-12 col-sm-10 form-col-without-padding'>
                                    <label class='pull-left-sm pull-left-md pull-left-lg'>
                                        <span class='control-label left-aligned-label padding-0-left padding-0-right selectall'>Lang.SelectAll</span>
                                        <input type='checkbox' id='AllBenefit' name='AllBenefit' v='' class='ios-switch'/>
                                        <div class='switch'></div>
                                    </label>
                                </div>
                            </div>
                        </div>"));
            for (int i = 0; i < benefits.Rows.Count; i++)
            {
                FieldName = Helper.GetString(benefits.Rows[i], "Name", "");
                FieldTitle = Helper.GetString(benefits.Rows[i], "Title", "");
                printable = Helper.GetBool(benefits.Rows[i], "Printable", false);
                MyPlaceHolder.Controls.Add(new LiteralControl(string.Format(@"
                        <div class='col-xs-12 form-group-container'>
                            <div class='form-group'>
                                <div class='col-sm-2'>&nbsp;</div>
                                <div class='col-xs-12 col-sm-10 form-col-without-padding'>
                                    <label class='pull-left-sm pull-left-md pull-left-lg'>
                                        <span class='control-label left-aligned-label padding-0-left padding-0-right'>{0}</span>
                                        <input type='checkbox' id='{1}' name='{1}' v='' class='ios-switch benefit' {2}/>
                                        <div class='switch'></div>
                                    </label>
                                </div>
                            </div>
                        </div>", FieldTitle, FieldName, printable ? "checked" : "")));
            }
            MyPlaceHolder.Controls.Add(new LiteralControl(@"</div>"));
        }
        DataTable deductions = DbProxy.Instance.FillData(@"SELECT Fields.* FROM Fields INNER JOIN FishDefFields ON Fields.ID=FishDefFields.FieldID WHERE FieldShowType=3 ORDER BY Priority", "deductions");
        if (deductions != null)
        {
            MyPlaceHolder.Controls.Add(new LiteralControl(@"<div class='col-xs-12 col-sm-6 col-lg-3 padding-5-top padding-5-bottom' style='border-left: 3px double #999;'>"));
            MyPlaceHolder.Controls.Add(new LiteralControl(@"<div class='col-xs-12 form-group-container'>
                            <div class='form-group'>
                                <div class='col-sm-2'>&nbsp;</div>
                                <div class='col-xs-12 col-sm-10 form-col-without-padding'>
                                    <label class='pull-left-sm pull-left-md pull-left-lg'>
                                        <span class='control-label left-aligned-label padding-0-left padding-0-right selectall'>Lang.SelectAll</span>
                                        <input type='checkbox' id='AllDeduction' name='AllDeduction' v='' class='ios-switch'/>
                                        <div class='switch'></div>
                                    </label>
                                </div>
                            </div>
                        </div>"));
            for (int i = 0; i < deductions.Rows.Count; i++)
            {
                FieldName = Helper.GetString(deductions.Rows[i], "Name", "");
                FieldTitle = Helper.GetString(deductions.Rows[i], "Title", "");
                printable = Helper.GetBool(deductions.Rows[i], "Printable", false);
                MyPlaceHolder.Controls.Add(new LiteralControl(string.Format(@"
                        <div class='col-xs-12 form-group-container'>
                            <div class='form-group'>
                                <div class='col-sm-2'>&nbsp;</div>
                                <div class='col-xs-12 col-sm-10 form-col-without-padding'>
                                    <label class='pull-left-sm pull-left-md pull-left-lg'>
                                        <span class='control-label left-aligned-label padding-0-left padding-0-right'>{0}</span>
                                        <input type='checkbox' id='{1}' name='{1}' v='' class='ios-switch deduction' {2}/>
                                        <div class='switch'></div>
                                    </label>
                                </div>
                            </div>
                        </div>", FieldTitle, FieldName, printable ? "checked" : "")));
            }
            MyPlaceHolder.Controls.Add(new LiteralControl(@"</div>"));
        }
        DataTable totals = DbProxy.Instance.FillData(@"SELECT Fields.* FROM Fields INNER JOIN FishDefFields ON Fields.ID=FishDefFields.FieldID WHERE FieldShowType=4 ORDER BY Priority", "totals");
        if (totals != null)
        {
            MyPlaceHolder.Controls.Add(new LiteralControl(@"<div class='col-xs-12 col-sm-6 col-lg-3 padding-5-top padding-5-bottom'>"));
            MyPlaceHolder.Controls.Add(new LiteralControl(@"<div class='col-xs-12 form-group-container'>
                            <div class='form-group'>
                                <div class='col-sm-2'>&nbsp;</div>
                                <div class='col-xs-12 col-sm-10 form-col-without-padding'>
                                    <label class='pull-left-sm pull-left-md pull-left-lg'>
                                        <span class='control-label left-aligned-label padding-0-left padding-0-right selectall'>Lang.SelectAll</span>
                                        <input type='checkbox' id='AllTotal' name='AllTotal' v='' class='ios-switch'/>
                                        <div class='switch'></div>
                                    </label>
                                </div>
                            </div>
                        </div>"));
            for (int i = 0; i < totals.Rows.Count; i++)
            {
                FieldName = Helper.GetString(totals.Rows[i], "Name", "");
                FieldTitle = Helper.GetString(totals.Rows[i], "Title", "");
                printable = Helper.GetBool(totals.Rows[i], "Printable", false);
                MyPlaceHolder.Controls.Add(new LiteralControl(string.Format(@"
                        <div class='col-xs-12 form-group-container'>
                            <div class='form-group'>
                                <div class='col-sm-2'>&nbsp;</div>
                                <div class='col-xs-12 col-sm-10 form-col-without-padding'>
                                    <label class='pull-left-sm pull-left-md pull-left-lg'>
                                        <span class='control-label left-aligned-label padding-0-left padding-0-right'>{0}</span>
                                        <input type='checkbox' id='{1}' name='{1}' v='' class='ios-switch total' {2}/>
                                        <div class='switch'></div>
                                    </label>
                                </div>
                            </div>
                        </div>", FieldTitle, FieldName, printable ? "checked" : "")));
            }
            MyPlaceHolder.Controls.Add(new LiteralControl(@"</div>"));
        }
    }
}