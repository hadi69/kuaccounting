﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reports_Recruitment_FishSet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindYear();
            Page.SetLang();
        }
    }

    void BindYear()
    {
        PersianCalendar calendar = new PersianCalendar();
        int year = calendar.GetYear(DateTime.Now);
        ddlYear.Items.Add((year - 1).ToString());
        ddlYear.Items.Add(year.ToString());
        ddlYear.Items.Add((year + 1).ToString());
        ddlYear.SelectedIndex = 1;
    }
}