﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reports_Recruitment_LoanReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindPersonnel();
            BindYear();
            Page.SetLang();
        }
    }

    void BindPersonnel()
    {
        DataTable personnels = DbProxy.Instance.FillData("SELECT ID, [dbo].[DisplayPerson](ID, 0) AS PersonnelName  FROM Personnel WHERE PersonnelGroupID=" + DbProxy.Instance.GetConfig("PersonnelGroup"), "personnels");

        ddlPersonnel.DataTextField = "PersonnelName";
        ddlPersonnel.DataValueField = "ID";
        ddlPersonnel.DataSource = personnels;
        ddlPersonnel.DataBind();
        ddlPersonnel.Items.Insert(0, new ListItem("همه", ""));
    }

    void BindYear()
    {
        PersianCalendar calendar = new PersianCalendar();
        int year = calendar.GetYear(DateTime.Now);
        ddlYear.Items.Add((year - 1).ToString());
        ddlYear.Items.Add(year.ToString());
        ddlYear.Items.Add((year + 1).ToString());
        ddlYear.SelectedIndex = 1;
    }
}