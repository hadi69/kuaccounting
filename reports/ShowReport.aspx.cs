﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using fyiReporting.RdlAsp;

public partial class reports_ShowReport : System.Web.UI.Page
{
    private RdlReport mReport = new RdlReport();
    bool error;
    string mName;
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Culture = "fa-IR";
        Page.UICulture = "fa-IR";
        mName = Request.QueryString["rs:Name"];
        string FirstRun = Request.QueryString["rs:FirstRun"];

        mReport.NoShow = FirstRun == "true";
        mReport.AutoRender = false;

        string arg = Request.QueryString["rs:Format"];
        if (arg != null)
            mReport.RenderType = arg;
        else
            mReport.RenderType = "html";
        mReport.PassPhrase = "northwind";       // user should provide in some fashion (from web.config??)
        // ReportFile must be the last item set since it triggers the building of the report
        arg = Request.QueryString["rs:url"];
        if (arg != null)
        {
            mReport.ReportFile = arg;
            
            mReport.Render(SiteSettings.ConnectionString);
            //mReport.Page.Culture = "fa-IR";
        }

        switch (mReport.RenderType)
        {
            case "xml":

                if (mReport.Xml == null)
                {
                    error = true;
                }
                else
                {
                    Response.ContentType = "application/xml";
                    Response.Write(mReport.Xml);
                }
                break;
            case "pdf":
                if (mReport.Object == null)
                {
                    error = true;
                }
                else
                {
                    Response.ContentType = "application/pdf";
                    Response.BinaryWrite(mReport.Object);
                }
                break;
            case "csv":

                if (mReport.CSV == null)
                {
                    error = true;
                }
                else
                {
                    Response.ContentType = "text/plain";
                    Response.Write(mReport.CSV);
                }
                break;
            case "html":	// Rest of the page takes care of the general html
            default:
                break;
        }
        if (mReport.RenderType != "html" && error == false)
            Response.End();
    }

    protected string Meta
    {
        get
        {
            if (mReport.ReportFile == "statistics")
                return "<meta http-equiv=\"Refresh\" contents=\"10\"/>";
            else
                return "";
        }
    }
    protected RdlReport Report
    {
        get
        {
            return mReport;
        }
    }
    protected string Name
    {
        get
        {
            return string.IsNullOrEmpty(mName) ? System.IO.Path.GetFileNameWithoutExtension(mReport.ReportFile) : mName;
        }
    }
    protected string ReportCSS
    {
        get
        {
            if (string.IsNullOrEmpty(mReport.CSS))
                return "";
            return "<style type='text/css'>\r\n" + mReport.CSS + "\r\n</style>";
        }
    }
}