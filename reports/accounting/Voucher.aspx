﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Voucher.aspx.cs" Inherits="reports_accounting_Voucher" %>

<%@ Register Src="../../controls/VoucherNavigator.ascx" TagName="VoucherNavigator"
    TagPrefix="uc7" %>
<%@ Register Src="../../controls/WardChooser.ascx" TagName="WardChooser" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div class="Title" style="text-align: center">
        <%= "DocumentProperties".Translate() %>
        <br />
        <asp:Label runat="server" ID="mPrograms"></asp:Label></div>
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="NameTD" valign="top">
                <%= "DocumentDate".Translate() %>:
                &nbsp;
            </td>
            <td valign="top">
                <asp:Label ID="mDate" runat="server" CssClass="LabelLtr"></asp:Label>
            </td>
            <td class="NameTD" valign="top">
                <%= "MoneyReceiver".Translate() %>:&nbsp;
            </td>
            <td valign="top">
                <asp:Label ID='mReciever' runat="server" CssClass="Label"></asp:Label>
            </td>
            <td class="NameTD" valign="top">
                <asp:Label runat="server" ID="sectionLbl" Text="Lang.Units"></asp:Label>
            </td>
            <td valign="top">
                <asp:Label ID='mSection' runat="server" CssClass="Label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="NameTD" valign="top">
                <%= "DocumentDescription".Translate() %>:&nbsp;
            </td>
            <td colspan="5">
                <asp:Label ID="mTitle" runat="server" CssClass="Label" />
            </td>
        </tr>
    </table>
    <asp:Panel ID="voucherDetailPanel" runat="server" Width="100%">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td colspan="8">
                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td style="text-align: left">
                                <asp:Button ID="doPrintVoucher" runat="server" Text="Lang.PrintDocument" CssClass="Button" Width="70px" />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:GridView ID="mGrid" AutoGenerateColumns="False" GridWidth="95%" Width="100%"
                                    Scrolling="Vertical" runat="server" CssClass="Grid" BorderStyle="None" OnRowDataBound="mGrid_RowDataBound"
                                    GridLines="Both" EmptyDataText="Lang.DocumentIsEmpty" SelectedRowStyle-CssClass="GridRowSelected"
                                    PagerStyle-CssClass="GridPager" HeaderStyle-CssClass="GridHeader" FooterStyle-CssClass="GridFooter"
                                    EditRowStyle-CssClass="GridEditRow" CellPadding="4" ShowFooter="true">
                                    <Columns>
                                        <asp:BoundField DataField="No" HeaderText="Lang.ColRowNr" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="TopicNames" HeaderText="Lang.Comments" />
                                        <asp:BoundField DataField="TopicCodes" HeaderText="Lang.TopicCode" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderText="Lang.Units">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%#Eval("Ward") %>' />
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label runat="server" Text="Lang.Sum" />
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lang.RetialAmount">
                                            <ItemTemplate>
                                                <asp:Label ID="unit" runat="server" Text='<%#Eval("Unit") %>' />
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="unitTotal" runat="server" Text="Lang.Sum" />
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lang.Owe">
                                            <ItemTemplate>
                                                <asp:Label ID="owe" runat="server" Text='<%#Eval("Owe") %>' />
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="oweTotal" runat="server" Text="Lang.Sum" />
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lang.Due">
                                            <ItemTemplate>
                                                <asp:Label ID="due" runat="server" Text='<%#Eval("Due") %>' />
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="dueTotal" runat="server" Text="Lang.Sum" />
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EditRowStyle CssClass="GridEditRow" />
                                    <SelectedRowStyle CssClass="GridRowSelected" />
                                    <PagerStyle CssClass="GridPager" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <FooterStyle CssClass="GridHeader" HorizontalAlign="Center" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <script>
        function OpenPrint(voucherID) {
            window.open('../../print/voucher.aspx?id=' + voucherID, null, 'left=100, top=100, height=550, width= 1000, status=no, resizable= yes, scrollbars= yes, toolbar= no,location= no, menubar= no');
            return false;
        }
    </script>
</asp:Content>
