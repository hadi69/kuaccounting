﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reports_accounting_ExpBalanceSimple : System.Web.UI.Page
{
    System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
    protected void Page_Load(object sender, EventArgs e)
    {
        msg.Visible = false;
        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["ExpBalanceSimple"];
            Page.SetLang();
            mReportSetting.ReportName = "ExpBalanceSimple";
            mTitle.Text = LangProvider.Instance["ExpBalanceSimple"];
            BindTree();
            BindWards();

            int year = cal.GetYear(DateTime.Now);
            //mFromDate.Text = string.Format("{0}/01/01", year - 1300);
            //mToDate.Text = Helper.FormatDate(cal.ToDateTime(year + 1, 1, 1, 1, 1, 1, 1).AddDays(-1), "yy/MM/dd");
            IDataReader financeYearStartDate = DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");//0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            //mFromDate.Text = 
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);
        }
        mReportSetting.SaveClick += new EventHandler(mReportSetting_SaveClick);
        mReportSetting.ReportSettingSelect += new EventHandler(mReportSetting_ReportSettingSelect);
    }

    #region Report Settings
    void mReportSetting_ReportSettingSelect(object sender, EventArgs e)
    {
        ReportSettingInfo info = mReportSetting.SelectedReportSettings;
        if (info == null)
            return;
        string[] lines = info.Settings.Split('\n');
        mFromDate.Text = lines[0].Trim('\r');
        mToDate.Text = lines[1].Trim('\r');
       // mFromVoucherID.Text = lines[2].Trim('\r');
        //mToVoucherID.Text = lines[3].Trim('\r');
        mTotals.SelectedValue = lines[4].Trim('\r');
        mWards.SelectedValue = lines[5].Trim('\r');
        mEquipment.SelectedValue = lines[6].Trim('\r');
        mPersons.SelectedValue = lines[7].Trim('\r');
        mFromVoucherNo.Text = lines[8].Trim('\r');
        mToVoucherNo.Text = lines[9].Trim('\r');
    }

    void mReportSetting_SaveClick(object sender, EventArgs e)
    {
        StringBuilder b = new StringBuilder();
        b.AppendLine(mFromDate.Text); //0
        b.AppendLine(mToDate.Text); //1
        //b.AppendLine(mFromVoucherID.Text);  //2
        //b.AppendLine(mToVoucherID.Text);    //3
        b.AppendLine(mTotals.SelectedValue); //4
        b.Append(mWards.SelectedValue);  //5
        b.Append(mEquipment.SelectedValue);  //6
        b.Append(mFromVoucherNo.Text);  //7
        b.Append(mToVoucherNo.Text);  //8
        mReportSetting.Save(b.ToString());
    }
    #endregion

    #region Bindings
    void BindTree()
    {
        mTotals.Items.Clear();
        mTotals.Items.Add(new ListItem(LangProvider.Instance["AllTopics"], ""));
        IDataReader arr = AccController.GetTreeNames(SiteSettings.ProgramID);
        if (arr != null)
        {
            while (arr.Read())
            {
                if (!arr.IsDBNull(0) && !arr.IsDBNull(1))
                    mTotals.Items.Add(new ListItem(arr.GetString(1), arr.GetString(0)));
            }
            arr.Close();
        }
        mTotals.SelectedIndex = 0;
    }
    void BindWards()
    {
        IDataReader projects = DbProxy.Instance.ExecuteReader("SELECT ID, Name + '(' + ISNULL(Code, '') + ')' FROM Ward WHERE SectionType=8");
        mWards.Items.Clear();
        mWards.Items.Add(new ListItem(LangProvider.Instance["AllProject"], ""));
        if (projects != null)
        {
            while (projects.Read())
            {
                if (!projects.IsDBNull(0) && !projects.IsDBNull(1))
                    mWards.Items.Add(new ListItem(projects.GetString(1), projects.GetInt32(0).ToString()));
            }
            projects.Close();
        }
        mWards.SelectedIndex = 0;

        IDataReader equipments = DbProxy.Instance.ExecuteReader("SELECT ID, FullName, Code, Title FROM EquipmentView");
        mEquipment.Items.Clear();
        mEquipment.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
        if (projects != null)
        {
            while (equipments.Read())
            {
                if (!equipments.IsDBNull(0) && !equipments.IsDBNull(1) && !equipments.IsDBNull(2))
                    mEquipment.Items.Add(new ListItem(equipments.GetString(1) + " - " + equipments.GetString(2),
                        equipments.GetInt32(0).ToString()));
            }
            equipments.Close();
        }
        mEquipment.SelectedIndex = 0;

        IDataReader persons = DbProxy.Instance.ExecuteReader("SELECT ID, [dbo].[DisplayPerson](ID, 1), ISNULL(Code, '') FROM Personnel WHERE Enabled = 1 AND Type=101");
        mPersons.Items.Clear();
        mPersons.Items.Add(new ListItem(LangProvider.Instance["AllPersons"], ""));
        if (persons != null)
        {
            while (persons.Read())
            {
                if (!persons.IsDBNull(0) && !persons.IsDBNull(1) && !persons.IsDBNull(2))
                    mPersons.Items.Add(new ListItem(persons.GetString(1) + " - کد: " + persons.GetString(2),
                        persons.GetInt32(0).ToString()));
            }
            persons.Close();
        }
        mPersons.SelectedIndex = 0;
    }


    #endregion
    string rowStyle = "GridAltRow";
    protected void doShow_Click(object sender, EventArgs e)
    {
        DateTime start = Helper.ParseDate(mFromDate.Text, Null.NullDate);
        DateTime end = Helper.ParseDate(mToDate.Text, Null.NullDate);
        if (Null.IsNull(start))
        {
            msg.Text = LangProvider.Instance["InvalidStartDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(end))
        {
            msg.Text = LangProvider.Instance["InvalidEndDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(start) || Null.IsNull(end))
            return;
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);

        IDataReader items = AccController.GetExpBalance(
            Helper.GetString(mTotals.SelectedValue, "")
            , start
            , end
            , Helper.GetInt(mWards.SelectedValue, Null.NullInteger), SiteSettings.ProgramID, Null.NullInteger, Helper.GetInt(mEquipment.SelectedValue, Null.NullInteger), Helper.GetInt(mPersons.SelectedValue, Null.NullInteger), mFromVoucherNo.Text.Trim(), mToVoucherNo.Text.Trim(), true, true);

        mPC.Controls.Clear();
        mPC.Controls.Add(new LiteralControl("<div>"));
        mPC.Controls.Add(new LiteralControl("<table class='ScrollPanelCartable table table-bordered table-condensed'>"));
        // --------------------------------------------------------
        // First Row
        mPC.Controls.Add(new LiteralControl("<tr class='GridHeader'>"));
        mPC.Controls.Add(new LiteralControl(string.Format("<td rowspan='2'>{0}</td>", LangProvider.Instance["ColRowNr"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<td rowspan='2'>{0}</td>", LangProvider.Instance["TopicCode"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<td rowspan='2'>{0}</td>", LangProvider.Instance["Topic"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<td colspan=2>{0}</td>", LangProvider.Instance["TopicFlow"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<td colspan=2>{0}</td>", LangProvider.Instance["TotalBalance"])));
        mPC.Controls.Add(new LiteralControl("</tr>"));
        // --------------------------------------------------------
        // Second Row
        mPC.Controls.Add(new LiteralControl("<tr class='GridHeader'>"));
        mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td><td>{1}</td>", LangProvider.Instance["OweFlow"], LangProvider.Instance["DueFlow"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td><td>{1}</td>", LangProvider.Instance["OweRemain"], LangProvider.Instance["DueRemain"])));
        mPC.Controls.Add(new LiteralControl("</tr>"));
        // --------------------------------------------------------

        int k = 1;
        decimal[] sums = { 0, 0, 0, 0 };
        //List<string> treePaths = new List<string>();
        while (items.Read())
        {
            decimal owe = SiteSettings.ToMonetary(items.IsDBNull(2) ? 0 : items.GetDecimal(2), DateTime.Now)
                , due = SiteSettings.ToMonetary(items.IsDBNull(3) ? 0 : items.GetDecimal(3), DateTime.Now);
            //if (mAllLevels.Checked)
            //{
                sums[0] += owe;
                sums[1] += due;
                sums[2] += Math.Max(0, owe - due);
                sums[3] += Math.Max(0, due - owe);
            if (Math.Max(0, owe - due) == 0)
            {
                continue;
            }
            mPC.Controls.Add(new LiteralControl(string.Format("<tr class='{0}'>", (k % 2) == 0 ? "GridRow" : "GridAltRow")));
           // }
            //else
            //{
            //    string treePath = items.GetString(0);
            //    int lastIndex = treePath.LastIndexOf('-');
            //    if (lastIndex > 0)
            //    {
            //        mPC.Controls.Add(new LiteralControl(string.Format("<tr class='{0} treegrid-{1} treegrid-parent-{2}'>", (k % 2) == 0 ? "GridRow" : "GridAltRow", treePath, treePath.Substring(0, lastIndex))));
            //    }
            //    else
            //    {
            //        sums[0] += owe;
            //        sums[1] += due;
            //        sums[2] += Math.Max(0, owe - due);
            //        sums[3] += Math.Max(0, due - owe);
            //        mPC.Controls.Add(new LiteralControl(string.Format("<tr class='{0} treegrid-{1}'>", (k % 2) == 0 ? "GridRow" : "GridAltRow", treePath)));
            //    }
            //}
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", k++)));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", items.IsDBNull(4) ? "" : items.GetString(4))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", items.IsDBNull(1) ? "" : items.GetString(1))));


            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", owe.ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", due.ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", Math.Max(0, owe - due).ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", Math.Max(0, due - owe).ToString("N0"))));

            mPC.Controls.Add(new LiteralControl("</tr>"));


            //treePaths.Add(items.GetString(0));
        }
        items.Close();
        items.Dispose();


        // ----------------------------------
        // Sums
        mPC.Controls.Add(new LiteralControl("<tr class='GridFooter'>"));
        mPC.Controls.Add(new LiteralControl("<td></td>"));
        mPC.Controls.Add(new LiteralControl("<td></td>"));
        mPC.Controls.Add(new LiteralControl("<td></td>"));
        for (int i = 0; i < sums.Length; i++)
        {
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", sums[i].ToString("N0"))));
        }
        mPC.Controls.Add(new LiteralControl("</tr>"));
        mPC.Controls.Add(new LiteralControl("</table></div>"));

    }
    protected void doExcel_Click(object sender, EventArgs e)
    {
        doShow_Click(null, null);
        string text = "";
        for (int i = 0; i < mPC.Controls.Count; i++)
            if (mPC.Controls[i] is LiteralControl)
                text += (mPC.Controls[i] as LiteralControl).Text;
        Exporter.ExportExcelSimple(Response, mTitle.Text, text);
    }
}