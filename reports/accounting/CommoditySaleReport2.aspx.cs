﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;

public partial class reports_accounting_CommoditySaleReport2 : System.Web.UI.Page
{
    private System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();

    protected void Page_Load(object sender, EventArgs e)
    {
        msg.Visible = false;
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");

        mGrid.EmptyDataText = LangProvider.Instance["ListIsEmpty"];

        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["CommoditySaleReport2"];
            Page.SetLang();
            IDataReader financeYearStartDate =
                DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");
            //0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            //mFromDate.Text = 
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);
            Initial();

            //  mReportSetting.ReportName = "CommoditySaleReport";
        }

    }
    private void Initial()
    {
        {
            IDataReader persons =
                DbProxy.Instance.ExecuteReader(
                    "SELECT ID, [dbo].[DisplayPerson](ID, 1), ISNULL(Code, '') FROM Personnel WHERE Type=101");
            IDataReader commodities =
                DbProxy.Instance.ExecuteReader(
                    "SELECT ID, ServiceName+'_'+ModelName ServiceName FROM ServicePriceView ORDER BY ServiceName");
            IDataReader visitors =
                DbProxy.Instance.ExecuteReader(
                    "SELECT Personnel.ID, [dbo].[DisplayPerson](ID, 0) FROM Personnel WHERE Enabled = 1 AND Type=101 AND Personnel.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID = CONVERT(int, dbo.GetSetting('VisitorGrouping'), 0))");
            IDataReader labels =
            DbProxy.Instance.ExecuteReader(
            "SELECT ID, [dbo].[DisplayPerson](ID, 1), ISNULL(Code, '') FROM Personnel WHERE Type=101");
            mPersons.Items.Clear();
            mCommodities.Items.Clear();
            mVisitores.Items.Clear();
            mLabels.Items.Clear();

            mPersons.Items.Add(new ListItem(LangProvider.Instance["AllPersons"], ""));
            mCommodities.Items.Add(new ListItem(LangProvider.Instance["AllCommodities"], ""));
            mVisitores.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
            mLabels.Items.Add(new ListItem(LangProvider.Instance["All"], ""));

            if (persons != null)
            {
                while (persons.Read())
                {
                    if (!persons.IsDBNull(0) && !persons.IsDBNull(1) && !persons.IsDBNull(2))
                        mPersons.Items.Add(new ListItem(persons.GetString(1) + " - کد: " + persons.GetString(2),
                        persons.GetInt32(0).ToString()));
                }
                persons.Close();
            }
            mPersons.SelectedIndex = 0;

            if (commodities != null)
            {
                while (commodities.Read())
                {
                    if (!commodities.IsDBNull(0) && !commodities.IsDBNull(1))
                        mCommodities.Items.Add(new ListItem(commodities.IsDBNull(1) ? "" : commodities.GetString(1),
                        commodities.IsDBNull(0) ? "" : commodities.GetInt32(0).ToString()));
                }
                commodities.Close();
            }
            mCommodities.SelectedIndex = 0;
            if (visitors != null)
            {
                while (visitors.Read())
                {
                    if (!visitors.IsDBNull(0) && !visitors.IsDBNull(1))
                        mVisitores.Items.Add(new ListItem(visitors.IsDBNull(1) ? "" : visitors.GetString(1),
                        visitors.GetInt32(0).ToString()));
                }
                visitors.Close();
            }
            mVisitores.SelectedIndex = 0;
            if (labels != null)
            {
                while (labels.Read())
                {
                    if (!labels.IsDBNull(0) && !labels.IsDBNull(1))
                        mLabels.Items.Add(new ListItem(labels.IsDBNull(1) ? "" : labels.GetString(1),
                        labels.GetInt32(0).ToString()));
                }
                labels.Close();
            }
            mLabels.SelectedIndex = 0;
        }
        IDataReader commodityGroupings =
            DbProxy.Instance.ExecuteReader(
                "SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping  WHERE GroupingType = 1");
        IDataReader personGroupings =
            DbProxy.Instance.ExecuteReader(
                "SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping  WHERE GroupingType = 0");
        IDataReader commodityGroup = DbProxy.Instance.ExecuteReader("SELECT ID, FullName FROM ServiceTypeView");
        IDataReader personGroup = DbProxy.Instance.ExecuteReader("SELECT ID, Title FROM PersonnelGroup");
        mCommodityGroupings.Items.Clear();
        if (commodityGroupings != null)
        {
            while (commodityGroupings.Read())
            {
                if (!commodityGroupings.IsDBNull(0) && !commodityGroupings.IsDBNull(1))
                    mCommodityGroupings.Items.Add(new ListItem(commodityGroupings.GetString(1), commodityGroupings.GetInt32(0).ToString()));
            }
            commodityGroupings.Close();
        }
        mPersonGroupings.Items.Clear();
        if (personGroupings != null)
        {
            while (personGroupings.Read())
            {
                if (!personGroupings.IsDBNull(0) && !personGroupings.IsDBNull(1))
                    mPersonGroupings.Items.Add(new ListItem(personGroupings.GetString(1),
                    personGroupings.GetInt32(0).ToString()));
            }
            personGroupings.Close();
        }
        mCommodityGroup.Items.Clear();
        if (commodityGroup != null)
        {
            while (commodityGroup.Read())
            {
                if (!commodityGroup.IsDBNull(0) && !commodityGroup.IsDBNull(1))
                    mCommodityGroup.Items.Add(new ListItem(commodityGroup.GetString(1),
                        commodityGroup.GetInt32(0).ToString()));
            }
            commodityGroup.Close();
        }
        mPersonGroup.Items.Clear();
        if (personGroup != null)
        {
            while (personGroup.Read())
            {
                if (!personGroup.IsDBNull(0) && !personGroup.IsDBNull(1))
                    mPersonGroup.Items.Add(new ListItem(personGroup.GetString(1), personGroup.GetInt32(0).ToString()));
            }
            personGroup.Close();
        }
    }

    protected void ShowGroupSearchClick(object sender, EventArgs e)
    {
        if (panelDropDowns.Visible)
        {
            panelDropDowns.Visible = false;
        }
        else
        {
            panelDropDowns.Visible = true;
        }
    }

    //protected void DlBaseChange(object sender, EventArgs e)
    //{
    //    DropDownList comboBox = (DropDownList)sender;
    //    int selected = Int32.Parse(comboBox.SelectedValue);
    //    if (selected == 1)
    //    {
    //        personLable.Text = LangProvider.Instance["ActorName"];
    //        panelDropDowns.Visible = panelInternal.Visible = true;
    //        //  mActorName.Visible = ActorNameLabel.Visible = mTransferee.Visible = TransfereeLabel.Visible = true;
    //    }
    //    else
    //    {
    //        personLable.Text = LangProvider.Instance["Person"];
    //        panelDropDowns.Visible = panelInternal.Visible = false;
    //        //mActorName.Visible = ActorNameLabel.Visible = mTransferee.Visible = TransfereeLabel.Visible = false;
    //    }
    //}

    protected void doShow_Click(object sender, EventArgs e)
    {
        doExcel.Enabled = true;
        DateTime start = Helper.ParseDate(mFromDate.Text, Null.NullDate);
        DateTime end = Helper.ParseDate(mToDate.Text, Null.NullDate);
        if (Null.IsNull(start))
        {
            msg.Text = LangProvider.Instance["InvalidStartDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(end))
        {
            msg.Text = LangProvider.Instance["InvalidEndDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(start) || Null.IsNull(end))
            return;
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);
        NumFactorTotal.Text = "0";
        AmountFactorTotal.Text = "0";
        DiscountFactorTotal.Text = "0";
        BindGrid(start, end);
    }

    #region Grid

    private decimal sumTotalTotalPrice, sumTotalDiscount;
    private float totalNum;

    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.Common.DbDataRecord record = e.Row.DataItem as System.Data.Common.DbDataRecord;
            DateTime dt = DateTime.Now;
            dt = Helper.GetDateTime(record["FactorDate"], Helper.NullDate);

            e.Row.Cells[1].Text = string.Format("<a href='#' ID='{1}' class='factornolink'>{0}</a>", record["NoSerial"], record["ID"]);
            decimal totalDiscount = SiteSettings.ToMonetary(Helper.GetDecimal(record["TotalDiscount"], 0), dt);
            decimal totalTotalPrice = SiteSettings.ToMonetary(Helper.GetDecimal(record["TotalPrice"], 0), dt);

            totalNum += 1;
            sumTotalTotalPrice += totalTotalPrice;
            sumTotalDiscount += totalDiscount;
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            NumFactorTotal.Text = totalNum.ToString();
            AmountFactorTotal.Text = sumTotalTotalPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            DiscountFactorTotal.Text = sumTotalDiscount.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }
    }

    protected void mGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        DateTime start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        DateTime end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);

        BindGrid(start, end);
    }

    private string totalQuery = "SELECT * FROM ( ";

    private void BindGrid(DateTime start, DateTime end)
    {
        gridPanel.Visible = true;

        string cogIDs, pegIDs, coIDs, peIDs, gWhere;
        GetGroupWhere(out cogIDs, out pegIDs, out coIDs, out peIDs, out gWhere);
        int personId = Helper.GetInt(mPersons.SelectedValue, Null.NullInteger);
        int commodityId = Helper.GetInt(mCommodities.SelectedValue, Null.NullInteger);
        int visitorId = Helper.GetInt(mVisitores.SelectedValue, Null.NullInteger);
        int labelId = Helper.GetInt(mLabels.SelectedValue, Null.NullInteger);
        string query = "";

        #region storedraft
        query = string.Format(@"
                    SELECT StoreDraftView.ID , StoreDraftView.NOSerial, StoreDraftView.FactorDate, StoreDraftView.BuyerID, StoreDraftView.BuyerName, StoreDraftView.Comments,
                    NULL StoreServiceID, NULL StoreServiceName, NULL UnitName, NULL Code, StoreDraftView.TotalPrice AS Total
                    ,StoreDraftView.TotalDiscount, (StoreDraftView.TotalTax + StoreDraftView.VAT) AS Tax
                    ,NULL UnitCurrencyConverter, NULL DeliveredAmount, NULL TotalAmount, NULL Price
                    , StoreDraftView.TotalPrice FROM StoreDraftView
                    LEFT OUTER JOIN Personnel ON StoreDraftView.BuyerID = Personnel.ID");

        string where =
            string.Format(
                " WHERE (StoreDraftView.StoreDraftType = 0 OR StoreDraftView.StoreDraftType IS NULL) AND FactorDate>={0} AND FactorDate<={1}",
                Helper.QoutedDateTime(start),
                Helper.QoutedDateTime(end));
        if (commodityId != Null.NullInteger)
        {
            where += @"AND StoreDraftView.ID IN (
                        SELECT DISTINCT StoreDraftID FROM StoreDraftItems 
                        LEFT OUTER JOIN StoreServices ON StoreDraftItems.StoreServiceID = StoreServices.ID
                        LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                        WHERE StoreServiceDef.ServicePriceID =" + commodityId + ")";

        }
        if (visitorId != Null.NullInteger)
        {
            where += @"AND StoreDraftView.VisitorID=" + visitorId;
        }
        if (personId != Null.NullInteger)
        {
            where += "AND BuyerID = " + personId;
        }
        if (labelId != Null.NullInteger)
        {
            where += @"AND StoreDraftView.ID IN (
                    SELECT DISTINCT StoreDraftID FROM StoreDraftItemsView 
                    WHERE StoreDraftItemsView.LabelID =" + labelId + ")";
        }

        if ((!string.IsNullOrEmpty(cogIDs) || !string.IsNullOrEmpty(pegIDs) || !string.IsNullOrEmpty(peIDs) ||
             !string.IsNullOrEmpty(coIDs)) && !string.IsNullOrEmpty(gWhere))
        {
            // ------------------------
            // ids: 1,2,5
            // where: '&1%' AND ('&2%' OR '&5%')
            // it could be better, but i dont know regex well :(
            // ------------------------
            where += "\r\nAND (\r\n";
            if (!string.IsNullOrEmpty(cogIDs))
            {
                //  groupings: '*1%', '!*1%'
                string[] cGroupingIDs = cogIDs.Split(',');
                for (int i = 0; i < cGroupingIDs.Length; i++)
                {
                    StringBuilder allIds = new StringBuilder();
                    int j = 0;
                    IDataReader childIds =
                        DbProxy.Instance.ExecuteReader("SELECT ID FROM [GetAllGroupingChilds](" + cGroupingIDs[i] + ")");
                    while (childIds.Read())
                    {
                        if (j != 0)
                        {
                            allIds.Append(",");
                        }
                        allIds.Append(childIds.GetValue(0));
                        j++;
                    }
                    gWhere = gWhere.Replace("'*" + cGroupingIDs[i] + "%'",
                        @" StoreDraftView.ID IN (
                        SELECT DISTINCT StoreDraftID FROM StoreDraftItems 
                        LEFT OUTER JOIN StoreServices ON StoreDraftItems.StoreServiceID = StoreServices.ID
                        LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                        LEFT OUTER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID
                        WHERE ServicePrice.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds +
                        "))) \r\n");
                    gWhere = gWhere.Replace("'!*" + cGroupingIDs[i] + "%'",
                        @" StoreDraftView.ID IN (
                        SELECT DISTINCT StoreDraftID FROM StoreDraftItems 
                        LEFT OUTER JOIN StoreServices ON StoreDraftItems.StoreServiceID = StoreServices.ID
                        LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                        LEFT OUTER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID             
                        WHERE NOT ServicePrice.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds +
                        "))) \r\n");
                }
            }
            if (!string.IsNullOrEmpty(pegIDs))
            {
                // personnel groups: '&1%', '!&1%'
                string[] pGroupingIDs = pegIDs.Split(',');
                for (int i = 0; i < pGroupingIDs.Length; i++)
                {
                    StringBuilder allIds = new StringBuilder();
                    int j = 0;
                    IDataReader childIds =
                        DbProxy.Instance.ExecuteReader("SELECT ID FROM [GetAllGroupingChilds](" + pGroupingIDs[i] + ")");
                    while (childIds.Read())
                    {
                        if (j != 0)
                        {
                            allIds.Append(",");
                        }
                        allIds.Append(childIds.GetValue(0));
                        j++;
                    }
                    gWhere = gWhere.Replace("'$" + pGroupingIDs[i] + "%'",
                        "Personnel.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + ")) \r\n");
                    gWhere = gWhere.Replace("'!$" + pGroupingIDs[i] + "%'",
                        " NOT Personnel.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds +
                        ")) \r\n");
                }
            }
            if (!string.IsNullOrEmpty(peIDs))
            {
                // personnel groups: '&1%', '!&1%'
                string[] pgids = peIDs.Split(',');
                for (int i = 0; i < pgids.Length; i++)
                {
                    gWhere = gWhere.Replace("'&" + pgids[i] + "%'",
                        "Personnel.PersonnelGroupID IN (" + pgids[i] + ") \r\n");
                    gWhere = gWhere.Replace("'!&" + pgids[i] + "%'",
                        " NOT Personnel.PersonnelGroupID IN (" + pgids[i] + ") \r\n");
                }
            }

            if (!string.IsNullOrEmpty(coIDs))
            {
                // ServiceType: '^1%', '!^1%'
                string[] cgids = coIDs.Split(',');
                for (int i = 0; i < cgids.Length; i++)
                {
                    StringBuilder allIds = new StringBuilder();
                    int j = 0;
                    IDataReader childIds =
                        DbProxy.Instance.ExecuteReader("SELECT ID FROM [GetAllServiceTypeChilds](" + cgids[i] + ")");
                    while (childIds.Read())
                    {
                        if (j != 0)
                        {
                            allIds.Append(",");
                        }
                        allIds.Append(childIds.GetValue(0));
                        j++;
                    }
                    gWhere = gWhere.Replace("'^" + cgids[i] + "%'",
                        @" StoreDraftView.ID IN (
                        SELECT DISTINCT StoreDraftID FROM StoreDraftItems 
                        LEFT OUTER JOIN StoreServices ON StoreDraftItems.StoreServiceID = StoreServices.ID
                        LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                        LEFT OUTER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID
                        LEFT OUTER JOIN Service ON ServicePrice.ServiceID = Service.ID
                        WHERE Service.ServiceTypeID IN (" + allIds + ")) \r\n");
                    gWhere = gWhere.Replace("'!^" + cgids[i] + "%'",
                        @" StoreDraftView.ID IN (
                        SELECT DISTINCT StoreDraftID FROM StoreDraftItems 
                        LEFT OUTER JOIN StoreServices ON StoreDraftItems.StoreServiceID = StoreServices.ID
                        LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                        LEFT OUTER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID
                        LEFT OUTER JOIN Service ON ServicePrice.ServiceID = Service.ID
                        WHERE NOT Service.ServiceTypeID IN (" + allIds + ")) \r\n");
                }

            }

            where += gWhere;
            where += "\r\n)\r\n";
        }
        query += where;
        #endregion
        totalQuery = query;
        if (!string.IsNullOrEmpty(SortCol))
            totalQuery += " ORDER BY " + SortCol + " " + SortOrder;

        IDataReader items = DbProxy.Instance.ExecuteReader(totalQuery);
        try
        {
            mGrid.DataSource = items;
            mGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Close();
                items.Dispose();
            }
        }
    }

    protected string SortCol
    {
        get { return Helper.GetString(ViewState["SortCol"], null); }
        set { ViewState["SortCol"] = value; }
    }

    protected string SortOrder
    {
        get { return Helper.GetString(ViewState["SortOrder"], "asc"); }
        set { ViewState["SortOrder"] = value; }
    }

    #endregion

    // ------------------------
    // ids: 1,2,5
    // where: '&1%' AND ('&2%' OR '&5%')
    // ------------------------
    private void GetGroupWhere(out string cgIDs, out string pgIDs, out string cIDs, out string pIDs, out string where)
    {
        cIDs = "";
        pIDs = "";
        cgIDs = "";
        pgIDs = "";
        where = "";

        // grouping members : '&1%', '!&1%'
        // category = 1375 AND ( category != 1076 OR category = 1075 ) 
        // --> '&1375%' AND ('!&1076%' OR '&1075%')
        where = mQueryBuilderSql.Value.Trim();
        where += " "; // we need this space at end to find IDs
        while (where.Contains("SGCategory = "))
        {
            int index = where.IndexOf("SGCategory = ") + "SGCategory = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            cgIDs += id + ",";
            where = where.Replace("SGCategory = " + id + " ", "'*" + id + "%' ");
        }
        while (where.Contains("SGCategory != "))
        {
            int index = where.IndexOf("SGCategory != ") + "SGCategory != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            cgIDs += id + ",";
            where = where.Replace("SGCategory != " + id + " ", "'!*" + id + "%' ");
        }
        if (cgIDs.EndsWith(","))
            cgIDs = cgIDs.Substring(0, cgIDs.Length - ",".Length);
        while (where.Contains("PGCategory = "))
        {
            int index = where.IndexOf("PGCategory = ") + "PGCategory = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pgIDs += id + ",";
            where = where.Replace("PGCategory = " + id + " ", "'$" + id + "%' ");
        }
        while (where.Contains("PGCategory != "))
        {
            int index = where.IndexOf("PGCategory != ") + "PGCategory != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pgIDs += id + ",";
            where = where.Replace("PGCategory != " + id + " ", "'!$" + id + "%' ");
        }
        if (pgIDs.EndsWith(","))
            pgIDs = pgIDs.Substring(0, pgIDs.Length - ",".Length);
        while (where.Contains("SCategory = "))
        {
            int index = where.IndexOf("SCategory = ") + "SCategory = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            cIDs += id + ",";
            where = where.Replace("SCategory = " + id + " ", "'^" + id + "%' ");
        }
        while (where.Contains("SCategory != "))
        {
            int index = where.IndexOf("SCategory != ") + "SCategory != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            cIDs += id + ",";
            where = where.Replace("SCategory != " + id + " ", "'!^" + id + "%' ");
        }
        if (cIDs.EndsWith(","))
            cIDs = cIDs.Substring(0, cIDs.Length - ",".Length);

        while (where.Contains("PCategory = "))
        {
            int index = where.IndexOf("PCategory = ") + "PCategory = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pIDs += id + ",";
            where = where.Replace("PCategory = " + id + " ", "'&" + id + "%' ");
        }
        while (where.Contains("PCategory != "))
        {
            int index = where.IndexOf("PCategory != ") + "PCategory != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pIDs += id + ",";
            where = where.Replace("PCategory != " + id + " ", "'!&" + id + "%' ");
        }
        if (pIDs.EndsWith(","))
            pIDs = pIDs.Substring(0, pIDs.Length - ",".Length);
    }

    protected void doExcel_Click(object sender, EventArgs e)
    {
        Exporter.ExportExcel(Response
            , new List<string>(new string[] { mGridTitle.Text })
            , new List<GridView>(new GridView[] { mGrid }));
    }
}