﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ExpBalanceBaseOn.aspx.cs" Inherits="reports_accounting_ExpBalanceBaseOn" %>


<%@ Register Src="../../controls/ReportSetting.ascx" TagName="ReportSetting" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>

    <uc2:ReportSetting ID="mReportSetting" runat="server" />
    <div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromDate" runat="server" CssClass="form-control input-sm dateselect"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="mFromDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label3" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToDate" runat="server" CssClass="form-control input-sm dateselect"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="mToDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label ID="Label6" runat="server">Lang.From</asp:Label>
                        <asp:Label ID="Label7" runat="server">Lang.DocumentNumber</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromVoucherNo" runat="server" CssClass="T2 form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label ID="Label8" runat="server">Lang.To</asp:Label>
                        <asp:Label ID="Label9" runat="server">Lang.DocumentNumber</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToVoucherNo" runat="server" CssClass="T2 form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label5" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.TopicName</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mTotals" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label26" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Person</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mPersons" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label4" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Project</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mWards" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label1" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.EquipmentShort</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mEquipment" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-xs-4 col-sm-6 form-group-container" align="center">
                <div class="form-group">
                    <asp:RadioButton ID="GBPerson" runat="server" GroupName="radio" Text="Lang.GBPerson" Checked="True" />
                    <asp:RadioButton ID="GBProject" runat="server" GroupName="radio" Text="Lang.GBProject" />
                    <asp:RadioButton ID="GBEquipment" runat="server" GroupName="radio" Text="Lang.GBEquipment" />
                </div>
            </div>
        </div>
        <div class="row">
        </div>
        <div class="row margin-5-top">
            <div class="col-md-8 col-lg-6 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container form-col-without-padding">
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcel_Click" Enabled="true" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <input type="button" id="print" class="btn btn-xs btn-dark-blue btn-block" value="" />
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <asp:Label runat="server" ID="msg" CssClass="label-30 alert alert-danger block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
    </div>
    <div class="row">
        <div class="col-xs-12">
            <asp:Panel ID="voucherDetailPanel" runat="server" Width="100%">
                <JP:MiniHeader ID="mTitle" runat="server"/>
                <asp:PlaceHolder runat="server" ID="mPC"></asp:PlaceHolder>
            </asp:Panel>
        </div>
    </div>
    <script>
        $(function () {
            $('#print').attr('value', Lang.Print);
            $('#print').click(function () {
                var title = Lang.ExpbalanceBaseOn;
                if ($('#<%=mTotals.ClientID %>').val() != '') {
                    title += '/' + Lang.Topic + ': ' + $('#<%=mTotals.ClientID %> option:selected').text();
                }
                if ($("#<%=mFromVoucherNo.ClientID %>").val() != '') {
                    title += '/' + Lang.FromDocNo + ': ' + $("#<%=mFromVoucherNo.ClientID %>").val();
                }
                if ($("#<%=mToVoucherNo.ClientID %>").val() != '') {
                    title += '/' + Lang.ToDocNo + ': ' + $("#<%=mToVoucherNo.ClientID %>").val();
                }
                if ($('#<%=mPersons.ClientID %>').val() != '') {
                    title += '/' + Lang.Person + ': ' + $('#<%=mPersons.ClientID %> option:selected').text();
                }
                if ($('#<%=mWards.ClientID %>').val() != '') {
                    title += '/' + Lang.Project + ': ' + $('#<%=mWards.ClientID %> option:selected').text();
                }
                if ($('#<%=mEquipment.ClientID %>').val() != '') {
                    title += '/' + Lang.Equipment + ': ' + $('#<%=mEquipment.ClientID %> option:selected').text();
                }
                if ($('#<%=GBPerson.ClientID %>').attr('checked')) {
                    title += '(' + Lang.GBPerson + ')';
                }
                if ($('#<%=GBProject.ClientID %>').attr('checked')) {
                    title += '(' + Lang.GBProject + ')';
                }
                if ($('#<%=GBEquipment.ClientID %>').attr('checked')) {
                    title += '(' + Lang.GBEquipment + ')';
                }
                printTable('.ScrollPanelCartable', '../../', title, null, null, 'GridHeader', true);
            });
            $('.GridRow,.GridAltRow').click(function () {
                $(this).toggleClass('selected');
            })
            dateSelect('.dateselect');
            $jQuery1_9('.ScrollPanelCartable').treegrid({ initialState: 'collapsed' });
            $("#bar").prepend(breadcrumb({
                title: Lang.ExpbalanceBaseOn,
                parent: {
                    title: Lang.Reports,
                    url: 'menu/Reports.aspx'
                }
            }));
            setClassReports();
        });
    </script>
</asp:Content>
