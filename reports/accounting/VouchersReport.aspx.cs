﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

public partial class reports_accounting_VouchersReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            mReportSetting.ReportName = "VouchersReport";

            BindWards();

            System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
            int year = cal.GetYear(DateTime.Now);
            mFromDate.Text = mToDate.Text = Helper.FormatDate(DateTime.Now, null);
        }
        mReportSetting.SaveClick += new EventHandler(mReportSetting_SaveClick);
        mReportSetting.ReportSettingSelect += new EventHandler(mReportSetting_ReportSettingSelect);
    }

    void mReportSetting_ReportSettingSelect(object sender, EventArgs e)
    {
        ReportSettingInfo info = mReportSetting.SelectedReportSettings;
        if (info == null)
            return;
        try
        {
            string[] lines = info.Settings.Split('\n');
            mFromDate.Text = lines[0].Trim('\r');
            mToDate.Text = lines[1].Trim('\r');
            mFromVoucherID.Text = lines[2].Trim('\r');
            mToVoucherID.Text = lines[3].Trim('\r');
            mWards.SelectedValue = lines[4].Trim('\r');
            mVocherTitle.Text = lines[5].Trim('\r');
            mReciever.Text = lines[6].Trim('\r');
            mDocNo.Text = lines[7].Trim('\r');
            mOweStart.Text = lines[8].Trim('\r');
            mOweEnd.Text = lines[9].Trim('\r');
            mDueStart.Text = lines[10].Trim('\r');
            mDueEnd.Text = lines[11].Trim('\r');
            for (int i = 0; i < mAccountsFilter.MaxDepth; i++)
                mAccountsFilter.SetIDs(i, lines[i + 11].Trim('\r'));
        }
        catch (Exception ex)
        {
            string fck = ex.Message;
        }
    }

    void mReportSetting_SaveClick(object sender, EventArgs e)
    {
        StringBuilder b = new StringBuilder();
        b.AppendLine(mFromDate.Text); //0
        b.AppendLine(mToDate.Text); //1
        b.AppendLine(mFromVoucherID.Text);  //2
        b.AppendLine(mToVoucherID.Text);    //3
        b.AppendLine(mWards.SelectedValue);  //4
        b.AppendLine(mVocherTitle.Text);  //5
        b.AppendLine(mReciever.Text);  //6
        b.AppendLine(mDocNo.Text);  //7
        b.AppendLine(mOweStart.Text);  //8
        b.AppendLine(mOweEnd.Text);  //9
        b.AppendLine(mDueStart.Text);  //10
        b.AppendLine(mDueEnd.Text);  //11

        for (int i = 0; i < mAccountsFilter.MaxDepth; i++)
            b.AppendLine(mAccountsFilter.GetIDs(i));
        mReportSetting.Save(b.ToString());
    }
    #region Bindings

    void BindWards()
    {
        mWards.DataTextField = "Name";
        mWards.DataValueField = "ID";

        mWards.DataSource = DbProxy.Instance.FillData("SELECT -1 AS ID, '' AS Name UNION SELECT ID, Name FROM Ward", "ward"); ;
        mWards.DataBind();

        mWards.SelectedIndex = 0;
    }
    #endregion
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[0].Text = (e.Row.RowIndex + 1).ToString();
            int sortIndex = Helper.GetInt((e.Row.DataItem as System.Data.Common.DbDataRecord)["SortIndex"], 0) + 1;
            e.Row.Cells[3].Text = sortIndex.ToString();
        }
    }

    protected void doShow_Click(object sender, EventArgs e)
    {
        string programIDs = mAccountsFilter.GetIDs(0);
        string totalIDs = mAccountsFilter.GetIDs(1);
        string moeinIDs = mAccountsFilter.GetIDs(2);
        string elab1IDs = mAccountsFilter.GetIDs(3);
        string elab2IDs = mAccountsFilter.GetIDs(4);
        string elab3IDs = mAccountsFilter.GetIDs(5);
        string elab4IDs = mAccountsFilter.GetIDs(6);
        IDataReader items = AccController.GetVouchersReport(
            mVocherTitle.Text.Trim(), mReciever.Text.Trim(), mDocNo.Text.Trim()
            , programIDs, totalIDs, moeinIDs, elab1IDs, elab2IDs, elab3IDs, elab4IDs
            , Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50))
            , Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50))
            , Helper.GetInt(mFromVoucherID.Text, 0), Helper.GetInt(mToVoucherID.Text, int.MaxValue - 1)
            , Helper.GetInt(mWards.SelectedValue, Null.NullInteger)
            , Helper.GetDecimal(mOweStart.Text, Null.NullInteger), Helper.GetDecimal(mOweEnd.Text, Null.NullInteger)
            , Helper.GetDecimal(mDueStart.Text, Null.NullInteger), Helper.GetDecimal(mDueEnd.Text, Null.NullInteger));
        try
        {
            mGrid.DataSource = items;
            mGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Close();
                items.Dispose();
            }
        }
    }
}