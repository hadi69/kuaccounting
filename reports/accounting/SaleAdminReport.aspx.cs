﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.Globalization;

public partial class reports_accounting_SaleAdminReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        msg.Visible = false;
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");

        mGrid.EmptyDataText = LangProvider.Instance["ListIsEmpty"];
        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["SaleAdminReport"];
            Page.SetLang();
            IDataReader financeYearStartDate =
                DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");
            //0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            //mFromDate.Text = 
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);
            Initial();
            init1(sender, e);
            BindDBs();
            mReportSetting.ReportName = "CommoditySaleReport";
        }
    }
    void BindDBs()
    {
        mDBList.Items.Clear();
        ListItem item = new ListItem(LangProvider.Instance["ActiveYear"] + " ", "");
        if (string.IsNullOrEmpty(SiteSettings.OtherCatalogName))
            item.Selected = true;
        mDBList.Items.Add(item);
        try
        {
            string path = Server.MapPath("~/files/fdb");
            if (System.IO.Directory.Exists(path))
            {
                string[] files = System.IO.Directory.GetFiles(path, "DB_*.mdf");
                for (int i = 0; i < files.Length; i++)
                {
                    string fileName = System.IO.Path.GetFileNameWithoutExtension(files[i]);
                    if (!fileName.ToLower().EndsWith("_log"))
                    {
                        item = new ListItem(fileName + " ", fileName);
                        if (fileName == SiteSettings.OtherCatalogName)
                            item.Selected = true;
                        mDBList.Items.Add(item);
                    }
                }
            }
        }
        catch { }
        panelDBs.Visible = mDBList.Items.Count > 1;
    }

    private void mReportSetting_ReportSettingSelect(object sender, EventArgs e)
    {
        ReportSettingInfo info = mReportSetting.SelectedReportSettings;
        if (info == null)
            return;
        string[] lines = info.Settings.Split('\n');
        mPersons.SelectedValue = lines[0].Trim('\r');
        mFromDate.Text = lines[2].Trim('\r');
        mToDate.Text = lines[3].Trim('\r');
    }

    private void mReportSetting_SaveClick(object sender, EventArgs e)
    {
        StringBuilder b = new StringBuilder();
        b.AppendLine(mPersons.SelectedValue); //0
        b.AppendLine(mFromDate.Text); //1
        b.AppendLine(mToDate.Text); //2
        mReportSetting.Save(b.ToString());
    }

    private void Initial()
    {

            IDataReader serviceGroupParents = DbProxy.Instance.ExecuteReader("SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping  WHERE (ParentID IS NULL OR ParentID = 0 OR ParentID = '') AND GroupingType = 1");
            IDataReader personGroupParents = DbProxy.Instance.ExecuteReader("SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping  WHERE (ParentID IS NULL OR ParentID = 0 OR ParentID = '') AND GroupingType = 0");
            IDataReader persons = DbProxy.Instance.ExecuteReader("SELECT ID, ISNULL([dbo].[DisplayPerson](ID, 1), ''), ISNULL(Code, '') FROM Personnel WHERE Enabled = 1 AND Type=101");
            IDataReader commodities = DbProxy.Instance.ExecuteReader("SELECT ID, ServiceFullName FROM ServicePriceView ORDER BY ServiceName");
            IDataReader visitors = DbProxy.Instance.ExecuteReader("SELECT Personnel.ID, [dbo].[DisplayPerson](ID, 0) FROM Personnel WHERE Enabled = 1 AND Type=101 AND Personnel.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID = CONVERT(int, dbo.GetSetting('VisitorGrouping'), 0))");

            mVisitor.Items.Clear();
            mServiceGroupParents.Items.Clear();
            mPersonGroupParents.Items.Clear();


            mVisitor.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
            mVisitor.Items.Add(new ListItem(LangProvider.Instance["NoVisitFactor"], "-2"));
            mServiceGroupParents.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
            mPersonGroupParents.Items.Add(new ListItem(LangProvider.Instance["All"], ""));

            mServiceGroupParents.Items.Clear();
            if (serviceGroupParents != null)
            {
                while (serviceGroupParents.Read())
                {
                    if (!serviceGroupParents.IsDBNull(0) && !serviceGroupParents.IsDBNull(1))
                        mServiceGroupParents.Items.Add(new ListItem(serviceGroupParents.GetString(1),
                            serviceGroupParents.GetInt32(0).ToString()));
                }
                serviceGroupParents.Close();
            }
            mPersonGroupParents.Items.Clear();
            if (personGroupParents != null)
            {
                while (personGroupParents.Read())
                {
                    if (!personGroupParents.IsDBNull(0) && !personGroupParents.IsDBNull(1))
                        mPersonGroupParents.Items.Add(new ListItem(personGroupParents.GetString(1),
                            personGroupParents.GetInt32(0).ToString()));
                }
                personGroupParents.Close();
            }

            if (persons != null)
            {
                while (persons.Read())
            {
                    if (!persons.IsDBNull(0) && !persons.IsDBNull(1) && !persons.IsDBNull(2))
                        mPersons.Items.Add(new ListItem(persons.GetString(1) + " - کد: " + persons.GetString(2), persons.GetInt32(0).ToString()));
                }
                persons.Close();
            }
            mPersons.SelectedIndex = 0;

        mCommodities.Items.Clear();
       // mPersons.Items.Add(new ListItem(LangProvider.Instance["AllPersons"], ""));
        mCommodities.Items.Add(new ListItem(LangProvider.Instance["AllCommodities"], ""));
        if (commodities != null)
            {
                while (commodities.Read())
            {
                    if (!commodities.IsDBNull(0) && !commodities.IsDBNull(1))
                        mCommodities.Items.Add(new ListItem(commodities.IsDBNull(1) ? "" : commodities.GetString(1), commodities.GetInt32(0).ToString()));
                }
                commodities.Close();
            }
            mCommodities.SelectedIndex = 0;
            if (visitors != null)
            {
                while (visitors.Read())
                {
                    if (!visitors.IsDBNull(0) && !visitors.IsDBNull(1))
                        mVisitor.Items.Add(new ListItem(visitors.IsDBNull(1) ? "" : visitors.GetString(1), visitors.GetInt32(0).ToString()));
                }
                visitors.Close();
            }
            mVisitor.SelectedIndex = 0;

        IDataReader commodityGroup = DbProxy.Instance.ExecuteReader("SELECT ID, FullName FROM ServiceTypeView");
        IDataReader personGroup = DbProxy.Instance.ExecuteReader("SELECT ID, Title FROM PersonnelGroup");
        mCommodityGroup.Items.Clear();
        if (commodityGroup != null)
        {
            while (commodityGroup.Read())
            {
                if (!commodityGroup.IsDBNull(0) && !commodityGroup.IsDBNull(1))
                    mCommodityGroup.Items.Add(new ListItem(commodityGroup.GetString(1),
                        commodityGroup.GetInt32(0).ToString()));
            }
            commodityGroup.Close();
        }
        mPersonGroup.Items.Clear();
        if (personGroup != null)
        {
            while (personGroup.Read())
            {
                if (!personGroup.IsDBNull(0) && !personGroup.IsDBNull(1))
                    mPersonGroup.Items.Add(new ListItem(personGroup.GetString(1), personGroup.GetInt32(0).ToString()));
            }
            personGroup.Close();
        }
    }

    public void init1(object sender, EventArgs e)
    {
        string service = "SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping  WHERE GroupingType = 1";
        string person = "SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping  WHERE GroupingType = 0";
        if (BasedServiceGroupLable.Checked)
        {
            service = "SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping  WHERE GroupingType = 1" +
                      " AND ID IN (SELECT ID FROM [dbo].[GetAllGroupingChilds](" + mServiceGroupParents.SelectedValue + "))";
        }
        if (BasedPersonsGroupLable.Checked)
        {
            person = "SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping  WHERE GroupingType = 0" +
                      " AND ID IN (SELECT ID FROM [dbo].[GetAllGroupingChilds](" + mPersonGroupParents.SelectedValue + "))";
        }
        IDataReader commodityGroupings = DbProxy.Instance.ExecuteReader(service);
        IDataReader personGroupings = DbProxy.Instance.ExecuteReader(person);
        mPersons.Items.Clear();
       // mCommodities.Items.Clear();
        mPersons.Items.Add(new ListItem(LangProvider.Instance["AllPersons"], ""));
       // mCommodities.Items.Add(new ListItem(LangProvider.Instance["AllCommodities"], ""));
        mCommodityGroupings.Items.Clear();
        if (commodityGroupings != null)
        {
            while (commodityGroupings.Read())
            {
                if (!commodityGroupings.IsDBNull(0) && !commodityGroupings.IsDBNull(1))
                    mCommodityGroupings.Items.Add(new ListItem(commodityGroupings.GetString(1),
                        commodityGroupings.GetInt32(0).ToString()));
            }
            commodityGroupings.Close();
        }
        mPersonGroupings.Items.Clear();
        if (personGroupings != null)
        {
            while (personGroupings.Read())
            {
                if (!personGroupings.IsDBNull(0) && !personGroupings.IsDBNull(1))
                    mPersonGroupings.Items.Add(new ListItem(personGroupings.GetString(1),
                        personGroupings.GetInt32(0).ToString()));
            }
            personGroupings.Close();
        }
    }

    protected void addGridColumns()
    {
        DbProxy mDbProxy = new DbProxy();
        int temp = 0;
        if(mBaseOnSaleTypeGrid.Rows.Count > 0)
          temp = mBaseOnSaleTypeGrid.Rows[0].Cells.Count - 1;

        while(temp > 9)
        {
            mBaseOnSaleTypeGrid.Columns.RemoveAt(temp);
            temp = temp - 1;
        }
        int count = 0;
        if(mBasedOnSaleType.Checked)
            count = Helper.GetInt(mDbProxy.FillScalar("SELECT COUNT(ID) FROM SaleType"), -1);
        if (mBasedOnPayMethod.Checked)
            count = 5;
        for (int i = 1; i <= count; i++)
        {
            BoundField field = new BoundField();
            field.DataField = "Num" + i;
            field.HeaderText = LangProvider.Instance["Num"];
            field.SortExpression = "Num" + i;
            mBaseOnSaleTypeGrid.Columns.Add(field);

            BoundField field1 = new BoundField();
            field1.DataField = "Price" + i;
            field1.HeaderText = LangProvider.Instance["TotalPrice"];
            field1.SortExpression = "Price" + i;
            mBaseOnSaleTypeGrid.Columns.Add(field1);

            BoundField field3 = new BoundField();
            field3.DataField = "ProfitInUnit" + i;
            field3.HeaderText = LangProvider.Instance["ProfitInUnit"];
            field3.SortExpression = "ProfitInUnit" + i;
            mBaseOnSaleTypeGrid.Columns.Add(field3);

            BoundField field2 = new BoundField();
            field2.DataField = "TotalProfit" + i;
            field2.HeaderText = LangProvider.Instance["TotalProfit"];
            field2.SortExpression = "TotalProfit" + i;
            mBaseOnSaleTypeGrid.Columns.Add(field2);
        }
    }

    protected void doShow_Click(object sender, EventArgs e)
    {
        DateTime start = Helper.ParseDate(mFromDate.Text, Null.NullDate);
        DateTime end = Helper.ParseDate(mToDate.Text, Null.NullDate);
        if (Null.IsNull(start))
        {
            msg.Text = LangProvider.Instance["InvalidStartDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(end))
        {
            msg.Text = LangProvider.Instance["InvalidEndDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(start) || Null.IsNull(end))
            return;

        doExcel.Enabled = true;
        if (!mBasedOnPayMethod.Checked && !mBasedOnSaleType.Checked)
        {
            BindGrid();
            BindMinMaxGrid();
        }
        else
        {
            addGridColumns();
            BindGrid();
            if (mBasedOnPayMethod.Checked)
            {
                BindBaseOnSaleTypeGrid(1);
            }
            else
            {
                BindBaseOnSaleTypeGrid(0);
            }
        }
    }

    private List<string> GetDBs()
    {
        List<string> dbs = new List<string>();
        for (int i = 0; i < mDBList.Items.Count; i++)
            if (mDBList.Items[i].Selected)
                dbs.Add(mDBList.Items[i].Value);
        return dbs;
    }
    #region Grid

    string rowStyle = "GridAltRow";
    decimal sumPrice = 0, sumProfit = 0;
    float sumNum = 0;
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView record = e.Row.DataItem as DataRowView;
            DateTime dt = DateTime.Now;
            decimal price = SiteSettings.ToMonetary(Helper.GetDecimal(record["Price"], 0), dt);
            float num = Helper.GetFloat(record["Num"], 0);
            decimal profit = SiteSettings.ToMonetary(Helper.GetDecimal(record["totalProfit"], 0), dt);

            sumPrice += price;
            sumProfit += profit;
            sumNum += num;

            e.Row.CssClass = rowStyle;

            e.Row.Cells[12].Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["ProfitInUnit"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("Price")).Text = SiteSettings.ToMonetary(price, dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("TotalProfit")).Text = SiteSettings.ToMonetary(profit, dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblTotal = (Label)e.Row.FindControl("totalNum");
            lblTotal.Text = sumNum.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            lblTotal = (Label)e.Row.FindControl("totalPrice");
            lblTotal.Text = SiteSettings.ToMonetary(sumPrice, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            lblTotal = (Label)e.Row.FindControl("totalTotalProfit");
            lblTotal.Text = SiteSettings.ToMonetary(sumProfit, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }
    }

    private int oldType = 0;
    private string minMaxRowStyle = "GridAltRow2";
    protected void minMaxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView record = e.Row.DataItem as DataRowView;
            DateTime dt = DateTime.Now;
            decimal price = SiteSettings.ToMonetary(Helper.GetDecimal(record["Price"], 0), dt);
            //float num = Helper.GetFloat(record["Num"], 0);
            decimal profit = SiteSettings.ToMonetary(Helper.GetDecimal(record["totalProfit"], 0), dt);

            int type = Helper.GetInt(record["Type"], 0);
            if (oldType != type)
            {
                oldType = type;
               minMaxRowStyle = minMaxRowStyle == "GridAltRow2" ? "GridRow" : "GridAltRow2";
            }
            switch (type)
            {
                case 1:
                {
                    e.Row.Cells[0].Text = LangProvider.Instance["MaxNum"];
                    e.Row.Cells[10].BackColor = Color.LightSeaGreen;
                    break;
                }
                case 2:
                {
                    e.Row.Cells[0].Text = LangProvider.Instance["MinNum"];
                    e.Row.Cells[10].BackColor = Color.LightSeaGreen;
                    break;
                }
                case 3:
                {
                    e.Row.Cells[0].Text = LangProvider.Instance["MaxPrice"];
                    e.Row.Cells[11].BackColor = Color.LightSeaGreen;
                    break;
                }
                case 4:
                {
                    e.Row.Cells[0].Text = LangProvider.Instance["MinPrice"];
                    e.Row.Cells[11].BackColor = Color.LightSeaGreen;
                    break;
                }
                case 5:
                {
                    e.Row.Cells[0].Text = LangProvider.Instance["MaxProfitInUnit"];
                    e.Row.Cells[12].BackColor = Color.LightSeaGreen;
                    break;
                }
                case 6:
                {
                    e.Row.Cells[0].Text = LangProvider.Instance["MinProfitInUnit"];
                    e.Row.Cells[12].BackColor = Color.LightSeaGreen;
                    break;
                }
                case 7:
                {
                    e.Row.Cells[0].Text = LangProvider.Instance["MaxTotalProfit"];
                    e.Row.Cells[13].BackColor = Color.LightSeaGreen;
                    break;
                }
                case 8:
                {
                    e.Row.Cells[0].Text = LangProvider.Instance["MinTotalProfit"];
                    e.Row.Cells[13].BackColor = Color.LightSeaGreen;
                    break;
                }
            }

            e.Row.Cells[12].Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["ProfitInUnit"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.Cells[11].Text = SiteSettings.ToMonetary(price, dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.Cells[13].Text = SiteSettings.ToMonetary(profit, dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            e.Row.CssClass = minMaxRowStyle;
        }
    }

    protected int count = -1;
    protected void mBaseOnSaleTypeGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView record = e.Row.DataItem as DataRowView;
            
            decimal maxPrice = 0, maxTotalProfit = 0, maxProfitInUnit = 0, price = 0, totalProfit = 0, profitInUnit = 0;
            float maxNum = 0,  num = 0;
            int maxPriceCell = -1, maxTotalProfitCell = -1, maxProfitInUnitCell = -1, maxNumCell = -1;
            DateTime dt = DateTime.Now;
            if (count == -1)
            {
                if (mBasedOnSaleType.Checked)
                {
                    DbProxy mDbProxy = new DbProxy();
                    count = Helper.GetInt(mDbProxy.FillScalar("SELECT COUNT(ID) FROM SaleType"), -1);
                }
                if (mBasedOnPayMethod.Checked)
                    count = 5;
            }
            int j = 0 ;
            for (int i = 1; i <= count; i++)
            {
                j = 9;// static column
                if (i > 1)
                {
                    j += (i - 1)*4;
                }

                price = SiteSettings.ToMonetary(Helper.GetDecimal(record["Price" + i], 0), dt);
                totalProfit = SiteSettings.ToMonetary(Helper.GetDecimal(record["TotalProfit" + i], 0), dt);
                profitInUnit = SiteSettings.ToMonetary(Helper.GetDecimal(record["ProfitInUnit" + i], 0), dt);
                num = Helper.GetFloat(record["Num" + i], 0);

                if (num > maxNum)
                {
                    maxNum = num;
                    maxNumCell = j + 1;
                }
                if (price > maxPrice)
                {
                    maxPrice = price;
                    maxPriceCell = j + 2;
                }
                if (profitInUnit > maxProfitInUnit)
                {
                    maxProfitInUnit = profitInUnit;
                    maxProfitInUnitCell = j + 3;
                }
                if (totalProfit > maxTotalProfit)
                {
                    maxTotalProfit = totalProfit;
                    maxTotalProfitCell = j + 4;
                }
                int nu = e.Row.Cells.Count;
                e.Row.Cells[j + 2].Text = price.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                e.Row.Cells[j + 3].Text = profitInUnit.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                e.Row.Cells[j + 4].Text = totalProfit.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            }
            if (maxPriceCell > 0)
            {
                e.Row.Cells[maxPriceCell].BackColor = Color.LightSeaGreen;
                e.Row.Cells[maxPriceCell].Text = maxPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "") + " *";
            }
            if (maxTotalProfitCell > 0)
            {
                e.Row.Cells[maxTotalProfitCell].BackColor = Color.LightSeaGreen;
                e.Row.Cells[maxTotalProfitCell].Text = maxTotalProfit.ToString("N4", Helper.NumberCulture).Replace(".0000", "") + " *";
            }
            if (maxProfitInUnitCell > 0)
            {
                e.Row.Cells[maxProfitInUnitCell].BackColor = Color.LightSeaGreen;
                e.Row.Cells[maxProfitInUnitCell].Text = maxProfitInUnit.ToString("N4", Helper.NumberCulture).Replace(".0000", "") + " *";
            }
            if (maxNumCell > 0)
            {
                e.Row.Cells[maxNumCell].Text = maxNum.ToString("N4", Helper.NumberCulture).Replace(".0000", "") + " *";
                e.Row.Cells[maxNumCell].BackColor = Color.LightSeaGreen;
            }
        } 
        if (e.Row.RowType == DataControlRowType.Header)
        {
            GridView HeaderGrid = (GridView)sender;
            GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
            TableCell HeaderCell = new TableCell();

            int firstHeaderColSpan = 4;
            if (Helper.GetInt(DisplayType.SelectedValue, 0) == 1)
            {
                firstHeaderColSpan = 1;
            }
            if (BasedBuyer.Checked || BasedServiceType.Checked || BasedVisitor.Checked
                || BasedPersonnelGroup.Checked || BasedServiceGroupLable.Checked || BasedPersonsGroupLable.Checked)
            {
                firstHeaderColSpan += 1;
            }

            HeaderCell.Text = LangProvider.Instance["Commodity"];
            HeaderCell.ColumnSpan = firstHeaderColSpan;
            HeaderGridRow.Cells.Add(HeaderCell);

            if (mBasedOnSaleType.Checked)
            {
                IDataReader saleTypes = DbProxy.Instance.ExecuteReader("SELECT Title FROM SaleType ORDER BY ID");
                if (saleTypes != null)
                {
                    while (saleTypes.Read())
                    {
                        HeaderCell = new TableCell();
                        HeaderCell.Text = saleTypes.GetString(0);
                        HeaderCell.ColumnSpan = 4;
                        HeaderGridRow.Cells.Add(HeaderCell);

                    }
                    saleTypes.Close();
                }
            }
            if (mBasedOnPayMethod.Checked)
            {
                HeaderCell = new TableCell();
                HeaderCell.Text = LangProvider.Instance["PayMethodCash"];
                HeaderCell.ColumnSpan = 4;
                HeaderGridRow.Cells.Add(HeaderCell);
                HeaderCell = new TableCell();
                HeaderCell.Text = LangProvider.Instance["PayMethodCheck"];
                HeaderCell.ColumnSpan = 4;
                HeaderGridRow.Cells.Add(HeaderCell);
                HeaderCell = new TableCell();
                HeaderCell.Text = LangProvider.Instance["PayMethodOwe"];
                HeaderCell.ColumnSpan = 4;
                HeaderGridRow.Cells.Add(HeaderCell);
                HeaderCell = new TableCell();
                HeaderCell.Text = LangProvider.Instance["PayMethodComplex"];
                HeaderCell.ColumnSpan = 4;
                HeaderGridRow.Cells.Add(HeaderCell);
                HeaderCell = new TableCell();
                HeaderCell.Text = LangProvider.Instance["PayMethodBank"];
                HeaderCell.ColumnSpan = 4;
                HeaderGridRow.Cells.Add(HeaderCell);
            }

            mBaseOnSaleTypeGrid.Controls[0].Controls.AddAt(0, HeaderGridRow);
        }
    }

    protected void mGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        BindGrid();
    }

    //protected void minMaxGrid_Sorting(object sender, GridViewSortEventArgs e)
    //{
    //    if (e.SortExpression == SortCol)
    //    {
    //        SortOrder = SortOrder == "asc" ? "desc" : "asc";
    //    }
    //    else
    //    {
    //        SortCol = e.SortExpression;
    //        SortOrder = "asc";
    //    }
    //    BindMinMaxGrid();
    //}

    protected void mBaseOnSaleTypeGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        if (mBasedOnPayMethod.Checked)
        {
            BindBaseOnSaleTypeGrid(1);
        }
        else
        {
            BindBaseOnSaleTypeGrid(0);
        }
    }

    private void BindGrid()
    {
        DateTime start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        DateTime end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);
        string cogIDs, pegIDs, coIDs, peIDs, gWhere;
        GetGroupWhere(out cogIDs, out pegIDs, out coIDs, out peIDs, out gWhere);

        int personId = Helper.GetInt(mPersons.SelectedValue, Null.NullInteger);
        int commodityId = Helper.GetInt(mCommodities.SelectedValue, Null.NullInteger);
        int visitorId = Helper.GetInt(mVisitor.SelectedValue, Null.NullInteger);
        string aditionalColumnValue = "";
        string aditionalColumnName = "";
        string aditionalGroupBy = "";
        string query = "";
        string join = "";
        string mainWhere = "";
        if (!mBasedOnPayMethod.Checked && !mBasedOnSaleType.Checked)
        {
            gridPanel.Visible = true;
        }
        else
        {
            gridPanel.Visible = false;
            MinmaxGridPanel.Visible = false;
            aditionalColumnValue += ", StoreDraftView.PayMethod, StoreDraftItemsView.SaleTypeID, StoreDraftItemsView.SaleTypeName";
            aditionalColumnName += ", PayMethod, SaleTypeID, SaleTypeName";
            aditionalGroupBy += ", StoreDraftView.PayMethod, StoreDraftItemsView.SaleTypeID, StoreDraftItemsView.SaleTypeName";
        }
        if (BasedBuyer.Checked)
        {
            mGrid.Columns[1].Visible = false;
            mGrid.Columns[2].Visible = true;
            mGrid.Columns[3].Visible = false;
            mGrid.Columns[4].Visible = false;
            mGrid.Columns[5].Visible = false;
            mGrid.Columns[6].Visible = false;
            aditionalColumnValue += ", StoreDraftView.BuyerName";
            aditionalColumnName += ", BuyerName";
            aditionalGroupBy += ", StoreDraftView.BuyerID, StoreDraftView.BuyerName";

        }
        else if (BasedVisitor.Checked)
        {
            mGrid.Columns[1].Visible = true;
            mGrid.Columns[2].Visible = false;
            mGrid.Columns[3].Visible = false;
            mGrid.Columns[4].Visible = false;
            mGrid.Columns[5].Visible = false;
            mGrid.Columns[6].Visible = false;
            aditionalColumnValue += ", StoreDraftView.VisitorName";
            aditionalColumnName += ", VisitorName";
            aditionalGroupBy += ", StoreDraftView.VisitorID, StoreDraftView.VisitorName";
        }
        else if (BasedPersonnelGroup.Checked)
        {
            mGrid.Columns[1].Visible = false;
            mGrid.Columns[2].Visible = false;
            mGrid.Columns[3].Visible = false;
            mGrid.Columns[4].Visible = true;
            mGrid.Columns[5].Visible = false;
            mGrid.Columns[6].Visible = false;
            aditionalColumnValue += ", StoreDraftView.BuyerPGName";
            aditionalColumnName += ", PersonGroup";
            aditionalGroupBy += ", StoreDraftView.BuyerPGID, StoreDraftView.BuyerPGName";
        }
        else if (BasedServiceType.Checked)
        {
            mGrid.Columns[1].Visible = false;
            mGrid.Columns[2].Visible = false;
            mGrid.Columns[3].Visible = true;
            mGrid.Columns[4].Visible = false;
            mGrid.Columns[5].Visible = false;
            mGrid.Columns[6].Visible = false;
            aditionalColumnValue += ", StoreDraftItemsView.ServiceTypeName";
            aditionalColumnName += ", ServiceGroup";
            aditionalGroupBy += ", StoreDraftItemsView.ServiceTypeID, StoreDraftItemsView.ServiceTypeName";
        }
        else if (BasedServiceGroupLable.Checked)
        {
            mGrid.Columns[1].Visible = false;
            mGrid.Columns[2].Visible = false;
            mGrid.Columns[3].Visible = false;
            mGrid.Columns[4].Visible = false;
            mGrid.Columns[5].Visible = true;
            mGrid.Columns[6].Visible = false;
            aditionalColumnValue += ", GroupingView.Name";
            aditionalColumnName += ", ServiceGrouping";
            aditionalGroupBy += ", GroupingView.Name";
            join = @"LEFT OUTER JOIN GroupingMember ON StoreDraftItemsView.ServicePriceID = GroupingMember.OtherID
                     LEFT OUTER JOIN GroupingView ON GroupingMember.GroupingID = GroupingView.ID";
            mainWhere = @" AND GroupingView.ID IN ( SELECT ID FROM [dbo].[GetAllGroupingChilds](" + mServiceGroupParents.SelectedValue +")) ";
        }
        else if (BasedPersonsGroupLable.Checked)
        {
            mGrid.Columns[1].Visible = false;
            mGrid.Columns[2].Visible = false;
            mGrid.Columns[3].Visible = false;
            mGrid.Columns[4].Visible = false;
            mGrid.Columns[5].Visible = false;
            mGrid.Columns[6].Visible = true;
            aditionalColumnValue += ", GroupingView.Name";
            aditionalColumnName += ", PersonGrouping";
            aditionalGroupBy += ", GroupingView.Name";
            join = @"LEFT OUTER JOIN GroupingMember ON StoreDraftView.BuyerID = GroupingMember.OtherID
                     LEFT OUTER JOIN GroupingView ON GroupingMember.GroupingID = GroupingView.ID";
            mainWhere = @" AND GroupingView.ID IN ( SELECT ID FROM [dbo].[GetAllGroupingChilds](" + mPersonGroupParents.SelectedValue + ")) ";
        }
        else
        {
            mGrid.Columns[1].Visible = mGrid.Columns[2].Visible = mGrid.Columns[3].Visible = mGrid.Columns[4].Visible = mGrid.Columns[5].Visible = mGrid.Columns[6].Visible = false;
        }

        #region SaleAdminReport
        mGrid.Columns[7].Visible = true;
        mGrid.Columns[8].Visible = true;
        mGrid.Columns[9].Visible = true;
        query = string.Format(@"
                            IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SaleAdminReport]') AND type in (N'U'))
	                            DROP TABLE SaleAdminReport
                            CREATE TABLE SaleAdminReport(
                                ID INT Identity(1, 1),
                                PayMethod INT,
                                SaleTypeID INT,
                                SaleTypeName NVARCHAR(1000),
                                BuyerName NVARCHAR(1000),
                                VisitorName NVARCHAR(1000),
                                PersonGroup NVARCHAR(1000),
                                ServiceGroup NVARCHAR(1000),
                                PersonGrouping NVARCHAR(1000),
                                ServiceGrouping NVARCHAR(1000),
                                Code NVARCHAR(200),
                                ServiceName NVARCHAR(1000),
                                UnitName NVARCHAR(500),
                                Num FLOAT,
                                Price NUMERIC(18,2),
                                TotalProfit NUMERIC(18,2),
                                ProfitInUnit NUMERIC(18,2)    
                                 );
                                INSERT INTO SaleAdminReport (
                                Code, ServiceName, UnitName{0}, Num, Price, TotalProfit, ProfitInUnit)
                                 SELECT StoreDraftItemsView.Code
                                ,StoreDraftItemsView.StoreServiceName
                                , StoreDraftItemsView.MainUnitName
                                 {1}
                                , SUM( UnitCurrencyConverter * ISNULL(DeliveredAmount, 0))
                                , SUM((UnitCurrencyConverter * ISNULL(DeliveredAmount, 0)) * ISNULL(Price, 0))
                                , SUM(((UnitCurrencyConverter * ISNULL(DeliveredAmount, 0)) * ISNULL(Price, 0))
                                   - ((UnitCurrencyConverter * ISNULL(DeliveredAmount, 0)) * ISNULL(BasePrice, 0)))
                                , SUM(((UnitCurrencyConverter * ISNULL(DeliveredAmount, 0)) * ISNULL(Price, 0))
                                   - ((UnitCurrencyConverter * ISNULL(DeliveredAmount, 0)) * ISNULL(BasePrice, 0)))
                                   / SUM(UnitCurrencyConverter * ISNULL(DeliveredAmount, 0))
                                FROM StoreDraftItemsView
                                LEFT OUTER JOIN StoreDraftView ON StoreDraftItemsView.StoreDraftID = StoreDraftView.ID
                                {2} ", aditionalColumnName, aditionalColumnValue, join);
        if (Helper.GetInt(DisplayType.SelectedValue, 0) == 1)
        {
            mGrid.Columns[7].Visible = false;
            mGrid.Columns[8].Visible = false;
            mGrid.Columns[9].Visible = false;
            query = string.Format(@"
                            IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SaleAdminReport]') AND type in (N'U'))
	                            DROP TABLE SaleAdminReport
                            CREATE TABLE SaleAdminReport(
                                ID INT Identity(1, 1),
                                PayMethod INT,
                                SaleTypeID INT,
                                SaleTypeName NVARCHAR(1000),
                                BuyerName NVARCHAR(1000),
                                VisitorName NVARCHAR(1000),
                                PersonGroup NVARCHAR(1000),
                                ServiceGroup NVARCHAR(1000),
                                PersonGrouping NVARCHAR(1000),
                                ServiceGrouping NVARCHAR(1000),
                                Code NVARCHAR(200),
                                ServiceName NVARCHAR(1000),
                                UnitName NVARCHAR(500),
                                Num FLOAT,
                                Price NUMERIC(18,2),
                                TotalProfit NUMERIC(18,2),
                                ProfitInUnit NUMERIC(18,2)    
                                 );
                                INSERT INTO SaleAdminReport (Num, Price, TotalProfit, ProfitInUnit {0})
                                 SELECT SUM( UnitCurrencyConverter * ISNULL(DeliveredAmount, 0))
                                , SUM((UnitCurrencyConverter * ISNULL(DeliveredAmount, 0)) * ISNULL(Price, 0))
                                , SUM(((UnitCurrencyConverter * ISNULL(DeliveredAmount, 0)) * ISNULL(Price, 0))
                                   - ((UnitCurrencyConverter * ISNULL(DeliveredAmount, 0)) * ISNULL(BasePrice, 0)))
                                , SUM(((UnitCurrencyConverter * ISNULL(DeliveredAmount, 0)) * ISNULL(Price, 0))
                                   - ((UnitCurrencyConverter * ISNULL(DeliveredAmount, 0)) * ISNULL(BasePrice, 0)))
                                   / SUM(UnitCurrencyConverter * ISNULL(DeliveredAmount, 0))
                                 {1}
                                FROM StoreDraftItemsView
                                LEFT OUTER JOIN StoreDraftView ON StoreDraftItemsView.StoreDraftID = StoreDraftView.ID
                                {2} ", aditionalColumnName, aditionalColumnValue, join);
        }
        string where = string.Format(" WHERE StoreDraftView.WorkflowStatus = 1 AND StoreDraftView.FactorDate BETWEEN {0} AND {1} {2} "
                , Helper.QoutedDateTime(start), Helper.QoutedDateTime(end), mainWhere);
        if (commodityId != Null.NullInteger)
        {
                where += @" AND StoreDraftItemsView.StoreServiceID IN (
                        SELECT StoreServices.ID FROM StoreServices                        
                        LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                        WHERE StoreServiceDef.ServicePriceID =" + commodityId + ")";
        }

        if (visitorId == -2)
            where += " AND (StoreDraftView.VisitorID IS NULL OR StoreDraftView.VisitorID = 0)";
        else if (visitorId != Null.NullInteger)
        {
            where += " AND StoreDraftView.VisitorID =" + visitorId;
        }

        if (personId != Null.NullInteger)
        {
            where += " AND StoreDraftView.BuyerID = " + personId;
        }

        if ((!string.IsNullOrEmpty(cogIDs) || !string.IsNullOrEmpty(pegIDs) || !string.IsNullOrEmpty(peIDs) || !string.IsNullOrEmpty(coIDs)) && !string.IsNullOrEmpty(gWhere))
        {
            // ------------------------
            // ids: 1,2,5
            // where: '&1%' AND ('&2%' OR '&5%')
            // it could be better, but i dont know regex well :(
            // ------------------------
            where += "\r\nAND (\r\n";
            if (!string.IsNullOrEmpty(cogIDs))
            {
                //  groupings: '*1%', '!*1%'
                string[] cGroupingIDs = cogIDs.Split(',');
                for (int i = 0; i < cGroupingIDs.Length; i++)
                {
                    StringBuilder allIds = new StringBuilder();
                    int j = 0;
                    IDataReader childIds = DbProxy.Instance.ExecuteReader("SELECT ID FROM [GetAllGroupingChilds](" + cGroupingIDs[i] + ")");
                    while (childIds.Read())
                    {
                        if (j != 0)
                        {
                            allIds.Append(",");
                        }
                        allIds.Append(childIds.GetValue(0));
                        j++;
                    }
                     gWhere = gWhere.Replace("'*" + cGroupingIDs[i] + "%'",
                        @" StoreDraftItemsView.StoreServiceID IN (
                        SELECT StoreServices.ID FROM StoreServices
                        LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                        LEFT OUTER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID
                        WHERE ServicePrice.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + "))) \r\n");
                        gWhere = gWhere.Replace("'!*" + cGroupingIDs[i] + "%'",
                        @" StoreDraftItemsView.StoreServiceID IN (
                        SELECT StoreServices.ID FROM StoreServices
                        LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                        LEFT OUTER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID             
                        WHERE NOT ServicePrice.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + "))) \r\n");
                }
            }
            if (!string.IsNullOrEmpty(pegIDs))
            {
                // personnel groups: '&1%', '!&1%'
                string[] pGroupingIDs = pegIDs.Split(',');
                for (int i = 0; i < pGroupingIDs.Length; i++)
                {
                    StringBuilder allIds = new StringBuilder();
                    int j = 0;
                    IDataReader childIds = DbProxy.Instance.ExecuteReader("SELECT ID FROM [GetAllGroupingChilds](" + pGroupingIDs[i] + ")");
                    while (childIds.Read())
                    {
                        if (j != 0)
                        {
                            allIds.Append(",");
                        }
                        allIds.Append(childIds.GetValue(0));
                        j++;
                    }
                    gWhere = gWhere.Replace("'$" + pGroupingIDs[i] + "%'",
                        " StoreDraftView.BuyerID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + ")) \r\n");
                    gWhere = gWhere.Replace("'!$" + pGroupingIDs[i] + "%'",
                        " NOT StoreDraftView.BuyerID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + ")) \r\n");
                }
            }

            if (!string.IsNullOrEmpty(peIDs))
            {
                // personnel groups: '&1%', '!&1%'
                string[] pgids = peIDs.Split(',');
                for (int i = 0; i < pgids.Length; i++)
                {
                    gWhere = gWhere.Replace("'&" + pgids[i] + "%'",
                        "StoreDraftView.BuyerID IN (SELECT ID FROM Personnel WHERE " +
                        "Personnel.PersonnelGroupID IN (" + pgids[i] + ")) \r\n");
                    gWhere = gWhere.Replace("'!&" + pgids[i] + "%'",
                        "StoreDraftView.BuyerID IN (SELECT ID FROM Personnel WHERE " +
                        " NOT Personnel.PersonnelGroupID IN (" + pgids[i] + ")) \r\n");
                }
            }

            if (!string.IsNullOrEmpty(coIDs))
            {
                // ServiceType: '^1%', '!^1%'
                string[] ids = coIDs.Split(',');
                for (int i = 0; i < ids.Length; i++)
                {
                    StringBuilder allIds = new StringBuilder();
                    int j = 0;
                    IDataReader childIds = DbProxy.Instance.ExecuteReader("SELECT ID FROM [GetAllServiceTypeChilds](" + ids[i] + ")");
                    while (childIds.Read())
                    {
                        if (j != 0)
                        {
                            allIds.Append(",");
                        }
                        allIds.Append(childIds.GetValue(0));
                        j++;
                    }
                        gWhere = gWhere.Replace("'^" + ids[i] + "%'",
                        @" StoreDraftItemsView.StoreServiceID IN (
                        SELECT StoreServices.ID FROM StoreServices
                        LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                        LEFT OUTER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID
                        LEFT OUTER JOIN Service ON ServicePrice.ServiceID = Service.ID
                        WHERE Service.ServiceTypeID IN (" + allIds + ")) \r\n");
                        gWhere = gWhere.Replace("'!^" + ids[i] + "%'",
                        @" StoreDraftItemsView.StoreServiceID IN (
                        SELECT StoreServices.ID FROM StoreServices
                        LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                        LEFT OUTER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID
                        LEFT OUTER JOIN Service ON ServicePrice.ServiceID = Service.ID
                        WHERE NOT Service.ServiceTypeID IN (" + allIds + ")) \r\n");
                }

            }

            where += gWhere;
            where += "\r\n)\r\n";
        }
        if (Helper.GetInt(DisplayType.SelectedValue, 0) == 0)
        {
            where += string.Format(@"GROUP BY StoreDraftItemsView.StoreServiceID, StoreDraftItemsView.Code
		                                , StoreDraftItemsView.StoreServiceName, StoreDraftItemsView.MainUnitName {0};
                                        SELECT * FROM SaleAdminReport ", aditionalGroupBy);
        }
        else
        {
            if (string.IsNullOrEmpty(aditionalGroupBy) || string.IsNullOrWhiteSpace(aditionalGroupBy))
            {
                where += " SELECT * FROM SaleAdminReport ";
            }
            else
            {
                where += string.Format(@"GROUP BY  {0};
                                        SELECT * FROM SaleAdminReport ", aditionalGroupBy.Substring(1));
            }
        }
        query += where;

        if (!string.IsNullOrEmpty(SortCol))
            query += " ORDER BY " + SortCol + " " + SortOrder;

        #endregion

        DataTable items = DbProxy.Instance.FillData(query, "all", GetDBs(), string.IsNullOrEmpty(SortCol) ? "" : (SortCol + " " + SortOrder));
        if (!mBasedOnPayMethod.Checked && !mBasedOnSaleType.Checked)
        {
            try
            {
                mGrid.DataSource = items;
                mGrid.DataBind();
            }
            finally
            {
                if (null != items)
                    items.Dispose();
            }
        }
    }

    private void BindMinMaxGrid()
    {
        MinmaxGridPanel.Visible = true;
        if (BasedBuyer.Checked)
        {
            mMinmaxGrid.Columns[1].Visible = false;
            mMinmaxGrid.Columns[2].Visible = true;
            mMinmaxGrid.Columns[3].Visible = false;
            mMinmaxGrid.Columns[4].Visible = false;
            mMinmaxGrid.Columns[5].Visible = false;
            mMinmaxGrid.Columns[6].Visible = false;
        }
        else if (BasedVisitor.Checked)
        {
            mMinmaxGrid.Columns[1].Visible = true;
            mMinmaxGrid.Columns[2].Visible = false;
            mMinmaxGrid.Columns[3].Visible = false;
            mMinmaxGrid.Columns[4].Visible = false;
            mMinmaxGrid.Columns[5].Visible = false;
            mMinmaxGrid.Columns[6].Visible = false;
        }
        else if (BasedPersonnelGroup.Checked)
        {
            mMinmaxGrid.Columns[1].Visible = false;
            mMinmaxGrid.Columns[2].Visible = false;
            mMinmaxGrid.Columns[3].Visible = false;
            mMinmaxGrid.Columns[4].Visible = true;
            mMinmaxGrid.Columns[5].Visible = false;
            mMinmaxGrid.Columns[6].Visible = false;
        }
        else if (BasedServiceType.Checked)
        {
            mMinmaxGrid.Columns[1].Visible = false;
            mMinmaxGrid.Columns[2].Visible = false;
            mMinmaxGrid.Columns[3].Visible = true;
            mMinmaxGrid.Columns[4].Visible = false;
            mMinmaxGrid.Columns[5].Visible = false;
            mMinmaxGrid.Columns[6].Visible = false;
        }
        else if (BasedServiceGroupLable.Checked)
        {
            mMinmaxGrid.Columns[1].Visible = false;
            mMinmaxGrid.Columns[2].Visible = false;
            mMinmaxGrid.Columns[3].Visible = false;
            mMinmaxGrid.Columns[4].Visible = false;
            mMinmaxGrid.Columns[5].Visible = true;
            mMinmaxGrid.Columns[6].Visible = false;
        }
        else if (BasedPersonsGroupLable.Checked)
        {
            mMinmaxGrid.Columns[1].Visible = false;
            mMinmaxGrid.Columns[2].Visible = false;
            mMinmaxGrid.Columns[3].Visible = false;
            mMinmaxGrid.Columns[4].Visible = false;
            mMinmaxGrid.Columns[5].Visible = false;
            mMinmaxGrid.Columns[6].Visible = true;
        }
        else
        {
            mMinmaxGrid.Columns[1].Visible = mMinmaxGrid.Columns[2].Visible = mMinmaxGrid.Columns[3].Visible = mMinmaxGrid.Columns[4].Visible = mMinmaxGrid.Columns[5].Visible = mMinmaxGrid.Columns[6].Visible = false;
        }
        string query = "";

        #region MinMax

        mMinmaxGrid.Columns[7].Visible = true;
        mMinmaxGrid.Columns[8].Visible = true;
        mMinmaxGrid.Columns[9].Visible = true;
        if (Helper.GetInt(DisplayType.SelectedValue, 0) == 1)
        {
            mMinmaxGrid.Columns[7].Visible = false;
            mMinmaxGrid.Columns[8].Visible = false;
            mMinmaxGrid.Columns[9].Visible = false;
        }

        query =
            string.Format(@"SELECT *, 1 [Type]  FROM SaleAdminReport WHERE Num = (SELECT MAX(Num) FROM SaleAdminReport)
                            UNION
                            SELECT *, 2 [Type]  FROM SaleAdminReport WHERE Num = (SELECT MIN(Num) FROM SaleAdminReport)
                            UNION
                            SELECT *, 3 [Type]  FROM SaleAdminReport WHERE Price = (SELECT MAX(Price) FROM SaleAdminReport)
                            UNION
                            SELECT *, 4 [Type]  FROM SaleAdminReport WHERE Price = (SELECT MIN(Price) FROM SaleAdminReport)
                            UNION
                            SELECT *, 5 [Type]  FROM SaleAdminReport WHERE ProfitInUnit = (SELECT MAX(ProfitInUnit) FROM SaleAdminReport)
                            UNION
                            SELECT *, 6 [Type]  FROM SaleAdminReport WHERE ProfitInUnit = (SELECT MIN(ProfitInUnit) FROM SaleAdminReport)
                            UNION
                            SELECT *, 7 [Type]  FROM SaleAdminReport WHERE TotalProfit = (SELECT MAX(TotalProfit) FROM SaleAdminReport)
                            UNION
                            SELECT *, 8 [Type]  FROM SaleAdminReport WHERE TotalProfit = (SELECT MIN(TotalProfit) FROM SaleAdminReport)
                            ORDER BY [Type]");
        #endregion
        //IDataReader items = DbProxy.Instance.ExecuteReader(query);
        DataTable items = DbProxy.Instance.FillData(query, "all", GetDBs(), "[Type] ASC");
        try
        {
            mMinmaxGrid.DataSource = items;
            mMinmaxGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Dispose();
            }
        }
    }

    private void BindBaseOnSaleTypeGrid(int basedOn)
    {
        BaseOnSaleTypeGridPanel.Visible = true;
        if (BasedBuyer.Checked)
        {
            mBaseOnSaleTypeGrid.Columns[1].Visible = false;
            mBaseOnSaleTypeGrid.Columns[2].Visible = true;
            mBaseOnSaleTypeGrid.Columns[3].Visible = false;
            mBaseOnSaleTypeGrid.Columns[4].Visible = false;
            mBaseOnSaleTypeGrid.Columns[5].Visible = false;
            mBaseOnSaleTypeGrid.Columns[6].Visible = false;
        }
        else if (BasedVisitor.Checked)
        {
            mBaseOnSaleTypeGrid.Columns[1].Visible = true;
            mBaseOnSaleTypeGrid.Columns[2].Visible = false;
            mBaseOnSaleTypeGrid.Columns[3].Visible = false;
            mBaseOnSaleTypeGrid.Columns[4].Visible = false;
            mBaseOnSaleTypeGrid.Columns[5].Visible = false;
            mBaseOnSaleTypeGrid.Columns[6].Visible = false;
        }
        else if (BasedPersonnelGroup.Checked)
        {
            mBaseOnSaleTypeGrid.Columns[1].Visible = false;
            mBaseOnSaleTypeGrid.Columns[2].Visible = false;
            mBaseOnSaleTypeGrid.Columns[3].Visible = false;
            mBaseOnSaleTypeGrid.Columns[4].Visible = true;
            mBaseOnSaleTypeGrid.Columns[5].Visible = false;
            mBaseOnSaleTypeGrid.Columns[6].Visible = false;
        }
        else if (BasedServiceType.Checked)
        {
            mBaseOnSaleTypeGrid.Columns[1].Visible = false;
            mBaseOnSaleTypeGrid.Columns[2].Visible = false;
            mBaseOnSaleTypeGrid.Columns[3].Visible = true;
            mBaseOnSaleTypeGrid.Columns[4].Visible = false;
            mBaseOnSaleTypeGrid.Columns[5].Visible = false;
            mBaseOnSaleTypeGrid.Columns[6].Visible = false;
        }
        else if (BasedServiceGroupLable.Checked)
        {
            mBaseOnSaleTypeGrid.Columns[1].Visible = false;
            mBaseOnSaleTypeGrid.Columns[2].Visible = false;
            mBaseOnSaleTypeGrid.Columns[4].Visible = false;
            mBaseOnSaleTypeGrid.Columns[3].Visible = false;
            mBaseOnSaleTypeGrid.Columns[5].Visible = true;
            mBaseOnSaleTypeGrid.Columns[6].Visible = false;
        }
        else if (BasedPersonsGroupLable.Checked)
        {
            mBaseOnSaleTypeGrid.Columns[1].Visible = false;
            mBaseOnSaleTypeGrid.Columns[2].Visible = false;
            mBaseOnSaleTypeGrid.Columns[3].Visible = false;
            mBaseOnSaleTypeGrid.Columns[4].Visible = false;
            mBaseOnSaleTypeGrid.Columns[5].Visible = false;
            mBaseOnSaleTypeGrid.Columns[6].Visible = true;
        }
        else
        {
            mBaseOnSaleTypeGrid.Columns[1].Visible = mBaseOnSaleTypeGrid.Columns[2].Visible = mBaseOnSaleTypeGrid.Columns[3].Visible = mBaseOnSaleTypeGrid.Columns[4].Visible = mBaseOnSaleTypeGrid.Columns[5].Visible = mBaseOnSaleTypeGrid.Columns[6].Visible = false;
        }
        string query = "";

        #region BaseOnSaleType
        mBaseOnSaleTypeGrid.Columns[7].Visible = true;
        mBaseOnSaleTypeGrid.Columns[8].Visible = true;
        mBaseOnSaleTypeGrid.Columns[9].Visible = true;
        if (Helper.GetInt(DisplayType.SelectedValue, 0) == 1)
        {
            mBaseOnSaleTypeGrid.Columns[7].Visible = false;
            mBaseOnSaleTypeGrid.Columns[8].Visible = false;
            mBaseOnSaleTypeGrid.Columns[9].Visible = false;
        }
        query =
            string.Format(@"EXEC [dbo].[CalcSaleAdminReportBaseSaleType] {0};
                          SELECT * from SaleAdminReportBaseSaleType ", basedOn);
        if (!string.IsNullOrEmpty(SortCol))
            query += " ORDER BY " + SortCol + " " + SortOrder;
        #endregion
        //IDataReader items = DbProxy.Instance.ExecuteReader(query);
        DataTable items = DbProxy.Instance.FillData(query, "all", GetDBs(), string.IsNullOrEmpty(SortCol) ? "" : (SortCol + " " + SortOrder));
        try
        {
            mBaseOnSaleTypeGrid.DataSource = items;
            mBaseOnSaleTypeGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Dispose();
            }
        }
    }

    protected string SortCol
    {
        get { return Helper.GetString(ViewState["SortCol"], null); }
        set { ViewState["SortCol"] = value; }
    }

    protected string SortOrder
    {
        get { return Helper.GetString(ViewState["SortOrder"], "asc"); }
        set { ViewState["SortOrder"] = value; }
    }

    #endregion

    // ------------------------
    // ids: 1,2,5
    // where: '&1%' AND ('&2%' OR '&5%')
    // ------------------------
    private void GetGroupWhere(out string cgIDs, out string pgIDs, out string cIDs, out string pIDs, out string where)
    {
        cIDs = "";
        pIDs = "";
        cgIDs = "";
        pgIDs = "";
        where = "";

        // grouping members : '&1%', '!&1%'
        // category = 1375 AND ( category != 1076 OR category = 1075 ) 
        // --> '&1375%' AND ('!&1076%' OR '&1075%')
        where = mQueryBuilderSql.Value.Trim();
        where += " "; // we need this space at end to find IDs
        while (where.Contains("SGCategory = "))
        {
            int index = where.IndexOf("SGCategory = ") + "SGCategory = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            cgIDs += id + ",";
            where = where.Replace("SGCategory = " + id + " ", "'*" + id + "%' ");
        }
        while (where.Contains("SGCategory != "))
        {
            int index = where.IndexOf("SGCategory != ") + "SGCategory != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            cgIDs += id + ",";
            where = where.Replace("SGCategory != " + id + " ", "'!*" + id + "%' ");
        }
        if (cgIDs.EndsWith(","))
            cgIDs = cgIDs.Substring(0, cgIDs.Length - ",".Length);
        while (where.Contains("PGCategory = "))
        {
            int index = where.IndexOf("PGCategory = ") + "PGCategory = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pgIDs += id + ",";
            where = where.Replace("PGCategory = " + id + " ", "'$" + id + "%' ");
        }
        while (where.Contains("PGCategory != "))
        {
            int index = where.IndexOf("PGCategory != ") + "PGCategory != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pgIDs += id + ",";
            where = where.Replace("PGCategory != " + id + " ", "'!$" + id + "%' ");
        }
        if (pgIDs.EndsWith(","))
            pgIDs = pgIDs.Substring(0, pgIDs.Length - ",".Length);

        while (where.Contains("SCategory = "))
        {
            int index = where.IndexOf("SCategory = ") + "SCategory = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            cIDs += id + ",";
            where = where.Replace("SCategory = " + id + " ", "'^" + id + "%' ");
        }
        while (where.Contains("SCategory != "))
        {
            int index = where.IndexOf("SCategory != ") + "SCategory != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            cIDs += id + ",";
            where = where.Replace("SCategory != " + id + " ", "'!^" + id + "%' ");
        }
        if (cIDs.EndsWith(","))
            cIDs = cIDs.Substring(0, cIDs.Length - ",".Length);

        while (where.Contains("PCategory = "))
        {
            int index = where.IndexOf("PCategory = ") + "PCategory = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pIDs += id + ",";
            where = where.Replace("PCategory = " + id + " ", "'&" + id + "%' ");
        }
        while (where.Contains("PCategory != "))
        {
            int index = where.IndexOf("PCategory != ") + "PCategory != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pIDs += id + ",";
            where = where.Replace("PCategory != " + id + " ", "'!&" + id + "%' ");
        }
        if (pIDs.EndsWith(","))
            pIDs = pIDs.Substring(0, pIDs.Length - ",".Length);
    }

    protected void doExcel_Click(object sender, EventArgs e)
    {
        Exporter.ExportExcel(Response
            , new List<string>(new string[] { mGridTitle.Text, MinmaxGridTitle.Text, BaseOnSaleTypeGridTitle.Text })
            , new List<GridView>(new GridView[] { mGrid, mMinmaxGrid, mBaseOnSaleTypeGrid }));
    }
}