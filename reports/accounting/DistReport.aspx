﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DistReport.aspx.cs" Inherits="reports_accounting_DistReport" %>

<%@ Register Src="~/controls/ReportSetting.ascx" TagName="ReportSetting" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>
    <uc1:ReportSetting ID="mReportSetting" runat="server" />
    <asp:Panel ID="panelDBs" runat="server" CssClass="well callout padding-5">
        <div class="row">
            <div class="col-xs-12">
                <h5 class="dashed-bottom padding-5-bottom yekan-family">
                    <asp:Label runat="server">Lang.DataBase</asp:Label>
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <asp:CheckBoxList runat="Server" ID="mDBList" RepeatLayout="Flow" RepeatDirection="Horizontal" />
            </div>
        </div>
    </asp:Panel>
    <div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" AssociatedControlID="mFromDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="mFromDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label3" runat="server" AssociatedControlID="mToDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="mToDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container display-none">
                <div class="form-group">
                    <asp:Label ID="Label4" runat="server" AssociatedControlID="mSFromDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mSFromDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="ma" runat="server" TargetControlID="mSFromDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container display-none">
                <div class="form-group">
                    <asp:Label ID="Label5" runat="server" AssociatedControlID="mSToDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mSToDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="me" runat="server" TargetControlID="mSToDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label26" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Person</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mPersons" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label1" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Visitor</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mVisitors" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4">&nbsp;</div>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox runat="server" ID="mAggregate" Text="Lang.Aggregate" CssClass="mAggregate" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4">&nbsp;</div>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox runat="server" ID="mShowServices" Text="Lang.ShowServicesTotal" CssClass="mShowServices" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container display-none">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4">&nbsp;</div>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox runat="server" ID="mShowChecks" Text="Lang.ShowChecks" CssClass="mShowChecks" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4">&nbsp;</div>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox runat="server" ID="mVisitorsPerformance" Text="Lang.VisitorsPerformance" CssClass="mVisitorsPerformance" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4">&nbsp;</div>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox runat="server" ID="mNoBuyCustomers" Text="Lang.NoBuyCustomers" CssClass="mNoBuyCustomers" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4">&nbsp;</div>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox runat="server" ID="mJustNotPaid" Text="Lang.JustNotPaid" CssClass="mJustNotPaid" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin-5-top">
            <div class="col-md-8 col-lg-9 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container padding-0-left">
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcel_Click" Enabled="false" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <input type="button" id="print" class="btn btn-xs btn-dark-blue btn-block" value="" />
                </div>
            </div>
        </div>
    </div>

    <div class="well callout padding-5">
        <input type="hidden" runat="server" id="mQueryBuilder" class="mQueryBuilder" />
        <input type="hidden" runat="server" id="mQueryBuilderSql" class="mQueryBuilderSql" />
        <div class="hidden">
            <asp:DropDownList runat="server" ID="mPersonsDummy" CssClass="mPersonsDummy"></asp:DropDownList>
            <asp:DropDownList runat="server" ID="mCatsDummy" CssClass="mCatsDummy"></asp:DropDownList>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <h5 class="dashed-bottom padding-5-bottom yekan-family">
                    <asp:Label runat="server">Lang.PersonsGroupLable</asp:Label>
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="ltr builder">
                    <div id="builder"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <asp:Label runat="server" ID="msg" CssClass="label-30 alert alert-danger block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
    </div>
    <asp:Panel ID="gridPanel" runat="server" Visible="false">
        <div>
            <a class="btn btn-xs btn-primary" id="factors-select-all">
                <i class="fa fa-check"></i>
                <asp:Label runat="server" Text="Lang.SelectAll"></asp:Label>
            </a>
            <a class="btn btn-xs btn-primary" id="factors-select-none">
                <i class="fa fa-remove"></i>
                <asp:Label runat="server" Text="Lang.SelectNone"></asp:Label>
            </a>
            <a class="btn btn-xs btn-primary" id="factors-print">
                <i class="fa fa-print"></i>
                <asp:Label runat="server" Text="Lang.PrintFactors"></asp:Label>
            </a>
            <a class="btn btn-xs btn-primary" id="factors-delete-sel">
                <i class="glyphicon glyphicon-remove-circle"></i>
                <asp:Label runat="server" Text="Lang.RemoveSelected"></asp:Label>
            </a>
            <a class="btn btn-xs btn-primary" id="factors-delete-undo">
                <i class="glyphicon glyphicon-ok-circle"></i>
                <asp:Label runat="server" Text="Lang.Reshow"></asp:Label>
            </a>
        </div>
        <JP:MiniHeader runat="server" Text="Lang.DistReport" ID="mGridTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="mGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable MainGrid Grid table table-bordered table-condensed"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true" OnSorting="mGrid_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.Print">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkPrintID" runat="server" />
                            </ItemTemplate>
                            <ItemStyle CssClass="align-center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:HyperLinkField DataNavigateUrlFields="ID" DataNavigateUrlFormatString="../../list.aspx?table=StoreDraft&id={0}&menu=accountinga" DataTextField="NOSerial" HeaderText="Lang.No" ItemStyle-HorizontalAlign="Center" SortExpression="NOSerial" />
                        <asp:BoundField DataField="BuyerName" HeaderText="Lang.BuyerName" ItemStyle-CssClass="tl" SortExpression="BuyerName" />
                        <asp:BoundField DataField="VisitorName" HeaderText="Lang.Visitor" SortExpression="VisitorName" Visible="false" />
                        <asp:TemplateField HeaderText="Lang.FactorDate" SortExpression="FactorDate">
                            <ItemTemplate>
                                <asp:Label ID="lblGridDate" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.FinalPrice" SortExpression="FinalPrice">
                            <ItemTemplate>
                                <asp:Label ID="totalPrice" runat="server" Text='<%#Eval("FinalPrice") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalPriceTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Pursant" SortExpression="Pursant" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="pursant" runat="server" Text='<%#Eval("Pursant") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="pursantTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.PayMethodCash" SortExpression="CashPrice">
                            <ItemTemplate>
                                <asp:Label ID="cashPrice" runat="server" Text='<%#Eval("CashPrice") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="cashPriceTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.PayMethodCheck" SortExpression="CheckPrice">
                            <ItemTemplate>
                                <asp:Label ID="checkPrice" runat="server" Text='<%#Eval("CheckPrice") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="checkPriceTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.PayMethodOwe" SortExpression="OwedPrice">
                            <ItemTemplate>
                                <asp:Label ID="owedPrice" runat="server" Text='<%#Eval("OwedPrice") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="owedPriceTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Remained" SortExpression="Remain">
                            <ItemTemplate>
                                <asp:Label ID="remain" runat="server" Text='<%#Eval("Remain") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="remainTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.TotalRemained" SortExpression="TotalRemain">
                            <ItemTemplate>
                                <asp:Label ID="totalRemain" runat="server" Text='<%#Eval("TotalRemain") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalRemainTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.NumFactors" SortExpression="NumFactors">
                            <ItemTemplate>
                                <asp:Label ID="NumFactors" runat="server" />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalNumFactors" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.NumOfPaidFactors" SortExpression="NumOfPaidFactors">
                            <ItemTemplate>
                                <asp:Label ID="NumOfPaidFactors" runat="server" />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalNumOfPaidFactors" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.NumOfNotPaidFactors" SortExpression="NumOfNotPaidFactors">
                            <ItemTemplate>
                                <asp:Label ID="NumOfNotPaidFactors" runat="server" />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalNumOfNotPaidFactors" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Paid" HeaderText="Lang.Status" ItemStyle-HorizontalAlign="Center" SortExpression="Paid" />
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>


    <asp:Panel ID="sGridPanel" runat="server" Visible="false">
        <JP:MiniHeader runat="server" Text="Lang.SDistReport" ID="mSGridTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="mSGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mSGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true" OnSorting="mGrid_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.Print">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkPrintID" runat="server" />
                            </ItemTemplate>
                            <ItemStyle CssClass="align-center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:HyperLinkField DataNavigateUrlFields="ID" DataNavigateUrlFormatString="../../list.aspx?table=StoreDraft&id={0}&menu=accountinga" DataTextField="NOSerial" HeaderText="Lang.No" ItemStyle-HorizontalAlign="Center" SortExpression="NOSerial" />
                        <asp:BoundField DataField="BuyerName" HeaderText="Lang.BuyerName" ItemStyle-CssClass="tl" SortExpression="BuyerName" />
                        <asp:BoundField DataField="VisitorName" HeaderText="Lang.Visitor" SortExpression="VisitorName" Visible="false" />
                        <asp:TemplateField HeaderText="Lang.FactorDate">
                            <ItemTemplate>
                                <asp:Label ID="lblGridDate" runat="server" SortExpression="FactorDate"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.FinalPrice" SortExpression="FinalPrice">
                            <ItemTemplate>
                                <asp:Label ID="totalPrice" runat="server" Text='<%#Eval("FinalPrice") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalPriceTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Pursant" SortExpression="Pursant" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="pursant" runat="server" Text='<%#Eval("Pursant") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="pursantTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.PayMethodCash" SortExpression="CashPrice">
                            <ItemTemplate>
                                <asp:Label ID="cashPrice" runat="server" Text='<%#Eval("CashPrice") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="cashPriceTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.PayMethodCheck" SortExpression="CheckPrice">
                            <ItemTemplate>
                                <asp:Label ID="checkPrice" runat="server" Text='<%#Eval("CheckPrice") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="checkPriceTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.PayMethodOwe" SortExpression="OwedPrice">
                            <ItemTemplate>
                                <asp:Label ID="owedPrice" runat="server" Text='<%#Eval("OwedPrice") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="owedPriceTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Remained" SortExpression="Remain">
                            <ItemTemplate>
                                <asp:Label ID="remain" runat="server" Text='<%#Eval("Remain") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="remainTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.TotalRemained" SortExpression="TotalRemain">
                            <ItemTemplate>
                                <asp:Label ID="totalRemain" runat="server" Text='<%#Eval("TotalRemain") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalRemainTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.NumFactors" SortExpression="NumFactors">
                            <ItemTemplate>
                                <asp:Label ID="NumFactors" runat="server" />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalNumFactors" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.NumOfPaidFactors" SortExpression="NumOfPaidFactors">
                            <ItemTemplate>
                                <asp:Label ID="NumOfPaidFactors" runat="server" />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalNumOfPaidFactors" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.NumOfNotPaidFactors" SortExpression="NumOfNotPaidFactors">
                            <ItemTemplate>
                                <asp:Label ID="NumOfNotPaidFactors" runat="server" />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalNumOfNotPaidFactors" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Paid" HeaderText="Lang.Status" ItemStyle-HorizontalAlign="Center" SortExpression="Paid" />
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>


    <asp:Panel ID="ReturnPanel" runat="server" Visible="false">
        <JP:MiniHeader runat="server" Text="Lang.ReturnInvoice" ID="ReturnGridTitle" />
        <div class="well callout row margin-5-top">
            <div class="col-md-8 col-lg-6 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container form-col-without-padding">
                <div class="col-xs-12 col-sm-3">
                    <asp:Button ID="doExcelReturns" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcelReturns_Click" />
                </div>
                <div class="col-xs-12 col-sm-3">
                    <input type="button" id="printReturns" class="btn btn-xs btn-dark-blue btn-block" value="" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="mGridReturn" AutoGenerateColumns="False" CssClass="ScrollPanelCartable returnGrid Grid table table-bordered table-condensed"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mGridReturn_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true" OnSorting="mGridReturn_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:HyperLinkField DataNavigateUrlFields="ID" DataNavigateUrlFormatString="../../list.aspx?table=Returns&id={0}&menu=accountinga" DataTextField="NOSerial" HeaderText="Lang.No" ItemStyle-HorizontalAlign="Center" SortExpression="NOSerial" />
                        <asp:BoundField DataField="BuyerName" HeaderText="Lang.BuyerName" ItemStyle-CssClass="tl" SortExpression="BuyerName" />
                        <asp:BoundField DataField="VisitorName" HeaderText="Lang.Visitor" SortExpression="VisitorName" />
                        <asp:TemplateField HeaderText="Lang.Date" SortExpression="ReturnDate">
                            <ItemTemplate>
                                <asp:Label ID="lblGridDate" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.FinalPrice" SortExpression="TotalPrice">
                            <ItemTemplate>
                                <asp:Label ID="TotalPrice" runat="server" Text='<%#Eval("TotalPrice") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalPriceTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Pursant" SortExpression="Pursant">
                            <ItemTemplate>
                                <asp:Label ID="pursant" runat="server" Text='<%#Eval("Pursant") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="pursantTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>


    <asp:Panel ID="checkGridPanel" runat="server" Visible="false">
        <JP:MiniHeader runat="server" Text="Lang.Checks" ID="mChecksTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="mChecksGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mChecksGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true" OnSorting="mChecksGrid_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:HyperLinkField DataNavigateUrlFields="ID" DataNavigateUrlFormatString="../../list.aspx?table=checks&id={0}" DataTextField="No" HeaderText="Lang.No" ItemStyle-HorizontalAlign="Center" SortExpression="No" />
                        <asp:BoundField DataField="PersonnelName" HeaderText="Lang.OwnerCheck" ItemStyle-CssClass="tl" SortExpression="PersonnelName" />
                        <asp:BoundField DataField="VisitorName" HeaderText="Lang.Visitor" SortExpression="VisitorName" />
                        <asp:TemplateField HeaderText="Lang.IssueDate" SortExpression="IssueDate">
                            <ItemTemplate>
                                <asp:Label ID="lblGridIssueDate" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.DueDate" SortExpression="DueDate">
                            <ItemTemplate>
                                <asp:Label ID="lblGridDueDate" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Status" SortExpression="Status">
                            <ItemTemplate>
                                <asp:Label ID="lblGridStatus" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Amount" SortExpression="Amount">
                            <ItemTemplate>
                                <asp:Label ID="amount" runat="server" Text='<%#Eval("Amount") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="amountTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Remained" SortExpression="Remained">
                            <ItemTemplate>
                                <asp:Label ID="remain" runat="server" Text='<%#Eval("Remain") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="remainTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="serviceGridPanel" runat="server" Visible="false">
        <JP:MiniHeader runat="server" Text="Lang.ServicesTotal" ID="mServiceTitle" />
        <div class="well callout row margin-5-top">
            <div class="col-md-8 col-lg-6 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container form-col-without-padding">
                <div class="col-xs-12 col-sm-3">
                    <asp:Button ID="doExcelServices" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcelServices_Click" />
                </div>
                <div class="col-xs-12 col-sm-3">
                    <input type="button" id="printServices" class="btn btn-xs btn-dark-blue btn-block" value="" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="serviceGrid" AutoGenerateColumns="False" CssClass="ServicesGrid Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="serviceGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true" OnSorting="serviceGrid_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:HyperLinkField DataNavigateUrlFields="StoreID,StoreServiceDefID" DataNavigateUrlFormatString="../../list.aspx?table=StoreServiceDef&id={0}&ssid={1}" DataTextField="ServiceTitle" HeaderText="Lang.Service" ItemStyle-HorizontalAlign="Center" SortExpression="ServiceTitle" />
                        <asp:BoundField DataField="Code" HeaderText="Lang.Code" SortExpression="Code" />
                        <asp:BoundField DataField="UnitTitle" HeaderText="Lang.Units" SortExpression="UnitTitle" />
                        <asp:TemplateField HeaderText="Lang.Num" SortExpression="SumNum" ItemStyle-CssClass="tl" FooterStyle-CssClass="tl">
                            <ItemTemplate>
                                <asp:Label ID="SumNum" runat="server" Text='<%#Eval("SumNum") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="SumNumTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Price" SortExpression="SumPrice" ItemStyle-CssClass="tl" FooterStyle-CssClass="tl">
                            <ItemTemplate>
                                <asp:Label ID="SumPrice" runat="server" Text='<%#Eval("SumPrice") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="SumPriceTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Weight" SortExpression="SumWeight" ItemStyle-CssClass="tl" FooterStyle-CssClass="tl">
                            <ItemTemplate>
                                <asp:Label ID="SumWeight" runat="server" Text='<%#Eval("SumWeight") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="SumWeightTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Volume" SortExpression="SumVolume" ItemStyle-CssClass="tl" FooterStyle-CssClass="tl">
                            <ItemTemplate>
                                <asp:Label ID="SumVolume" runat="server" Text='<%#Eval("SumVolume") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="SumVolumeTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Pursant" SortExpression="SumPursant" ItemStyle-CssClass="tl" FooterStyle-CssClass="tl">
                            <ItemTemplate>
                                <asp:Label ID="SumPursant" runat="server" Text='<%#Eval("SumPursant") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="SumPursantTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="StoreName" HeaderText="Lang.Store" SortExpression="StoreName" />
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
    <input type="hidden" runat="server" id="removeIDs" class="removeIDs" />
    <script>
        $(function () {
            $('#print').attr('value', Lang.Print);
            $('#print').click(function () {
                var head = Lang.DistReport;
                <%--if ($("#<%=ddlBy.ClientID %>").val() == <%= (int)AccController.PersonBookGroupBy.ByTotal %>) {
                    head = '<%=jpMiniHeaderTotalBills.Text %>';
            }--%>
                printTable('.ScrollPanelCartable', '../../', head);
            });
            $('#printServices').attr('value', Lang.Print);
            $('#printServices').click(function () {
                var head = Lang.ServicesTotal;
                printTable('.ServicesGrid', '../../', head);
            });
            $('#printReturns').attr('value', Lang.Print);
            $('#printReturns').click(function () {
                var head = Lang.ReturnInvoice;
                printTable('.returnGrid', '../../', head);
            });
            $('.GridRow,.GridAltRow').click(function () {
                $(this).toggleClass('selected');
            });
            dateSelect('.dateselect');
            $('.monetary').keyup(function () {
                $(this).val(numberWithCommas(stripCommas($(this).val())));
            });
            $("#bar").prepend(breadcrumb({
                title: Lang.DistReport,
                parent: {
                    title: Lang.Reports,
                    url: 'menu/reports.aspx'
                }
            }));
            setClassReports();
            var cats = {};
            $('.mPersonsDummy option').each(function () {
                cats[$(this).attr('value')] = $(this).text();
            });
            var pcats = {};
            $('.mCatsDummy option').each(function () {
                pcats[$(this).attr('value')] = $(this).text();
            });
            $jQuery1_9('#builder').queryBuilder({
                filters: [
                    {
                        id: 'category',
                        label: Lang.PersonsGroupLable,
                        type: 'integer',
                        input: 'select',
                        values: cats,
                        operators: ['equal', 'not_equal']
                    },
                    {
                        id: 'pcat',
                        label: Lang.PersonnelGroup,
                        type: 'integer',
                        input: 'select',
                        values: pcats,
                        operators: ['equal', 'not_equal']
                    }
                ]
            });

            // save rules before post back
            $('.doShow').click(function () {
                var rules = $jQuery1_9('#builder').queryBuilder('getRules');
                var rulesStr = JSON.stringify(rules);
                // alert(rulesStr);
                $('.mQueryBuilder').val(rulesStr);
                $('.mQueryBuilderSql').val($jQuery1_9('#builder').queryBuilder('getSQL', false, false).sql);
            });
            //reload saved rules
            $jQuery1_9('#builder').queryBuilder('reset');
            var ruleStr = $('.mQueryBuilder').val();
            if (ruleStr != '') {
                var rule = eval("(" + ruleStr + ")");
                try {
                    $jQuery1_9('#builder').queryBuilder('setRules', rule);
                } catch (e) {
                }
            }
            $jQuery1_9('#builder').on('afterCreateRuleInput.queryBuilder', function (rule) {
                makeChosen();
            });
            $(document).ready(function () {
                $("#factors-select-all").click(function () {
                    $(".data-print input").attr("checked", "checked");
                });

                $("#factors-select-none").click(function () {
                    $(".data-print input").attr("checked", "");
                });

                // Handle printing
                $("#factors-print").click(function () {
                    var checkedFactors = $(".data-print input:checked"),
                        ids = [],
                        printUrl = '',
                        baseURlLength = 100,
                        totalIds,
                        indexOfPipe = 0;
                    if (checkedFactors.length == 0) {
                        alert(Lang.PleaseCheckAtLeastOneFactor);
                        return;
                    }

                    $.each(checkedFactors, function () {
                        ids.push($(this).attr("data-id"));
                    });
                    printUrl = rootPath + Router.Generate("print-storedraft", { params: { ids: ids.join('|'), title: 'SalesInvoice' }, query: { report: 'simple', print: "true" } });

                    totalIds = ids.join('|');

                    while (indexOfPipe >= 0) {
                        indexOfPipe = totalIds.indexOf('|', 190);
                        if (indexOfPipe >= 0) {
                            window.open(rootPath + Router.Generate("print-storedraft", { params: { ids: totalIds.substr(0, indexOfPipe), title: 'SalesInvoice' }, query: { report: 'simple', print: "true" } }));
                        }

                        totalIds = totalIds.substr(indexOfPipe + 1);
                    }
                    window.open(rootPath + Router.Generate("print-storedraft", { params: { ids: totalIds, title: 'SalesInvoice' }, query: { report: 'simple', print: "true" } }));
                });

                $('#factors-delete-undo').click(function () {
                    $('.removeIDs').val('');
                    $('.doShow').click();
                });
                $('#factors-delete-sel').click(function () {
                    var ids = [];
                    $(".data-print input:checked").each(function () {
                        ids.push($(this).attr("data-id"));
                    });
                    if (ids.length == 0)
                        return;
                    var newIDs = ids.join('|');
                    var oldIDs = $('.removeIDs').val();
                    if (oldIDs)
                        $('.removeIDs').val(newIDs + '|' + oldIDs);
                    else
                        $('.removeIDs').val(newIDs);
                    $('.doShow').click();
                    /*
                    function handleCol(i) {
                        var s = 0;
                        $(".data-print input:checked").closest('tr').each(function () {
                            var me = $(this);
                            var htm = $("td:nth-child(" + i + ") span", me).html();
                            var _s = parseFloat(htm.replace(/\,/g, ''));
                            if (htm.indexOf(Lang.AbbrvOwe) > 0)
                                _s *= -1;
                            s += _s;
                        });

                        var lastHtm = $(".MainGrid tr:last td:nth-child(" + i + ") span").html();
                        var last = parseFloat(lastHtm.replace(/\,/g, ''));
                        if (lastHtm.indexOf(Lang.AbbrvOwe) > 0)
                            last *= -1;
                        last -= s;
                        lastHtm = numberWithCommas(last);
                        if (i > 7)
                            if (last < 0) {
                                last *= -1;
                                lastHtm = numberWithCommas(last) + ' ' + Lang.AbbrvOwe;
                            }
                            else
                                lastHtm += ' ' + Lang.AbbrvDue;
                        $('.MainGrid tr:last td:nth-child(' + i + ')').html('<span>' + lastHtm + '</span>');
                    }
                    handleCol(4);
                    handleCol(5);
                    handleCol(6);
                    handleCol(7);
                    handleCol(8);
                    handleCol(9);


                    $(".data-print input:checked").closest('tr').remove();
                    // fill Index = RADIF
                    var index = 0;
                    $('.MainGrid tr').each(function () {
                        if (index == 0) {
                            index++;
                            return;
                        }
                        var me = $(this);
                        $("td:nth-child(2)", me).html(index);
                        index++;
                    });
                    */
                    return false;
                });
            });

        });
    </script>
</asp:Content>

