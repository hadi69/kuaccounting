﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

public partial class reports_accounting_TotalBook : System.Web.UI.Page
{
    System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
    protected void Page_Load(object sender, EventArgs e)
    {
        msg.Visible = false;
        mGrid.EmptyDataText=LangProvider.Instance["DocumentIsEmpty"];
        mGrid.EnableViewState = false;
        if (!IsPostBack)
        {
           Page.Title = LangProvider.Instance["TotalBook"];
            Page.SetLang();
            this.Level = Helper.GetInt(Request.QueryString["level"], 0);
            if (this.Level == 0)
                mTitle.Text = LangProvider.Instance["TotalBook"];
            else if (this.Level == 1)
                mTitle.Text = LangProvider.Instance["Book1"];
            else if (this.Level > 1)
                mTitle.Text = LangProvider.Instance["Book2"] +(this.Level - 2).ToString();
            //mReportSetting.ReportName = "TotalBook" + this.Level;

            BindTree();
            BindWards();
            BindDBs();

            int year = cal.GetYear(DateTime.Now);
            IDataReader financeYearStartDate = DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");//0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            //mFromDate.Text = 
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);
        }
        //mReportSetting.SaveClick += new EventHandler(mReportSetting_SaveClick);
        //mReportSetting.ReportSettingSelect += new EventHandler(mReportSetting_ReportSettingSelect);
    }
    void BindDBs()
    {
        mDBList.Items.Clear();
        ListItem item = new ListItem(LangProvider.Instance["ActiveYear"] + " ", "");
        if (string.IsNullOrEmpty(SiteSettings.OtherCatalogName))
            item.Selected = true;
        mDBList.Items.Add(item);
        try
        {
            string path = Server.MapPath("~/files/fdb");
            if (System.IO.Directory.Exists(path))
            {
                string[] files = System.IO.Directory.GetFiles(path, "DB_*.mdf");
                for (int i = 0; i < files.Length; i++)
                {
                    string fileName = System.IO.Path.GetFileNameWithoutExtension(files[i]);
                    if (!fileName.ToLower().EndsWith("_log"))
                    {
                        item = new ListItem(fileName + " ", fileName);
                        if (fileName == SiteSettings.OtherCatalogName)
                            item.Selected = true;
                        mDBList.Items.Add(item);
                    }
                }
            }
        }
        catch { }
        panelDBs.Visible = mDBList.Items.Count > 1;
    }
    //void mReportSetting_ReportSettingSelect(object sender, EventArgs e)
    //{
    //    ReportSettingInfo info = mReportSetting.SelectedReportSettings;
    //    if (info == null)
    //        return;
    //    string[] lines = info.Settings.Split('\n');
    //    mFromDate.Text = lines[0].Trim('\r');
    //    mToDate.Text = lines[1].Trim('\r');
    //    mFromVoucherNo.Text = lines[2].Trim('\r');
    //    mToVoucherNo.Text = lines[3].Trim('\r');
    //    mTotals.SelectedValue = lines[4].Trim('\r');
    //    mWards.SelectedValue = lines[5].Trim('\r');
    //}

    //void mReportSetting_SaveClick(object sender, EventArgs e)
    //{
    //    StringBuilder b = new StringBuilder();
    //    b.AppendLine(mFromDate.Text); //0
    //    b.AppendLine(mToDate.Text); //1
    //    b.AppendLine(mFromVoucherNo.Text);  //2
    //    b.AppendLine(mToVoucherNo.Text);    //3
    //    b.AppendLine(mTotals.SelectedValue); //4
    //    b.Append(mWards.SelectedValue);  //5
    //    mReportSetting.Save(b.ToString());
    //}
    #region Bindings
    void BindTree()
    {
        mTotals.Items.Clear();
        mTotals.Items.Add(new ListItem(LangProvider.Instance["AllTopics"], ""));
        IDataReader arr = AccController.GetTreeNames(SiteSettings.ProgramID);
        if (arr != null)
        {
            while (arr.Read())
            {
                if (!arr.IsDBNull(0) && !arr.IsDBNull(1))
                    mTotals.Items.Add(new ListItem(arr.GetString(1), arr.GetString(0)));
            }
            arr.Close();
        }
        mTotals.SelectedIndex = 0;
    }
    void BindWards()
    {
        mWards.DataTextField = "Name";
        mWards.DataValueField = "ID";

        mWards.DataSource = DbProxy.Instance.FillData("SELECT -1 AS ID, '' AS Name UNION SELECT ID, Name + '(' + ISNULL(Code, '') + ')' FROM Ward", "ward"); ;
        mWards.DataBind();

        mWards.SelectedIndex = 0;
    }


    #endregion
    string rowStyle = "GridAltRow";
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView record = e.Row.DataItem as System.Data.DataRowView;
            string treePath = Helper.GetString(record["TreePath"], "");
            if (lastTreepath != treePath)
            {
                rowStyle = rowStyle == "GridAltRow" ? "GridRow" : "GridAltRow";
                if (!mShowInitRemain.Checked)
                _remain = _owe = _due = 0;
                lastTreepath = treePath;
                lastTopic = AccController.GetTopic(TreeInfo.GetTopicID(lastTreepath));
            }
            else
            {
                e.Row.Cells[4].Text = "";
                e.Row.Cells[5].Text = "";
            }
            DateTime dt = Helper.GetDateTime(record["Date"], Helper.NullDate);
            if (dt != Helper.NullDate)
            {
                if (SiteSettings.DateType == DateType.en || SiteSettings.DateType == DateType.ar)
                {
                    string date = Helper.FormatDate(Helper.GetDateTime(record["Date"], Helper.NullDate), "dd/MM/yyyy");
                    int index = date.IndexOf("/");
                    string day = date.Substring(0, index);
                    date = date.Substring(index + 1, date.Length - (index + 1));
                    index = date.IndexOf("/");
                    string month = date.Substring(0, index);

                    e.Row.Cells[2].Text = month;
                    e.Row.Cells[1].Text = day;
                }
                else
                {
                    e.Row.Cells[2].Text = cal.GetMonth(dt).ToString();
                    e.Row.Cells[1].Text = cal.GetDayOfMonth(dt).ToString();
                }
            }
            //if (string.IsNullOrEmpty(Helper.GetString((e.Row.DataItem as System.Data.Common.DbDataRecord)["DayVal"])))
            e.Row.CssClass = rowStyle;

            // url
            int otherID = Helper.GetInt(record["OtherID"], Null.NullInteger), mode= Helper.GetInt(record["Mode"], Null.NullInteger);
            if (otherID == Null.NullInteger)
                e.Row.Cells[3].Text = string.Format("<a href='../../list.aspx?table=DocumentAccounting&id={0}&itemid={1}'>{2}</a>", record["ID"], record["ItemID"], record["No"]);
            else
                e.Row.Cells[3].Text = string.Format("<a href='../../list.aspx?table=DocumentAccounting&id={0}&itemid={1}' class='no' mode='{3}' otherid='{4}'>{2}</a>", record["ID"], record["ItemID"], record["No"], mode, otherID);

            _due = SiteSettings.ToMonetary(Helper.GetDecimal(record["Due"], 0), dt);
            _owe = SiteSettings.ToMonetary(Helper.GetDecimal(record["Owe"], 0), dt);
            _remain += _due - _owe;

            sumDue += _due;
            sumOwe += _owe;
            sumRemaining += 0;// Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["Remaining"], 0);

            e.Row.Cells[12].Text = (_remain < 0) ? LangProvider.Instance["Li"] : LangProvider.Instance["Cr"] ;
            if (lastTopic != null && lastTopic.Nature != TopicNature.Both)
                if ((lastTopic.Nature == TopicNature.Owe && _remain > 0) || (lastTopic.Nature == TopicNature.Due && _remain < 0))
                    Helper.SignifyNegative(e.Row.Cells[11]);

            Label owe = (Label)e.Row.FindControl("owe");
            owe.Text = _owe.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            Label due = (Label)e.Row.FindControl("due");
            due.Text = _due.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            Label remaining = (Label)e.Row.FindControl("remaining");
            decimal _r = _remain;
            remaining.Text = (_r < 0) ? (-1 * _r).ToString("N4", Helper.NumberCulture).Replace(".0000", "") : _r.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            string no = Helper.GetString(record["NO"], "");
            if (string.IsNullOrEmpty(treePath) && no == "0" && mShowInitRemain.Checked)
            {
                e.Row.Cells[3].Text = "";
                e.Row.Cells[4].Text = LangProvider.Instance["InitRemain"];
            }
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblTotal = (Label)e.Row.FindControl("oweTotal");
            lblTotal.Text = sumOwe.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            lblTotal = (Label)e.Row.FindControl("dueTotal");
            lblTotal.Text = sumDue.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            lblTotal = (Label)e.Row.FindControl("remainingTotal");
            decimal _r = sumDue - sumOwe;
            lblTotal.Text = ((_r) < 0) ? (-1 * (_r)).ToString("N4", Helper.NumberCulture).Replace(".0000", "") : (_r).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            if (lastTopic != null && lastTopic.Nature != TopicNature.Both)
                if ((lastTopic.Nature == TopicNature.Owe && (_r) > 0) || (lastTopic.Nature == TopicNature.Due && (_r) < 0))
                    Helper.SignifyNegative(e.Row.Cells[11]);

            lblTotal = (Label)e.Row.FindControl("finalDiagnosis");
            lblTotal.Text = ((_r) < 0) ? LangProvider.Instance["Li"] : LangProvider.Instance["Cr"];
        }
    }

    private List<string> GetDBs()
    {
        List<string> dbs = new List<string>();
        for (int i = 0; i < mDBList.Items.Count; i++)
            if (mDBList.Items[i].Selected)
                dbs.Add(mDBList.Items[i].Value);
        return dbs;
    }
    decimal sumOwe = 0, sumDue = 0, sumRemaining = 0, _due = 0, _owe = 0, _remain = 0;
    string lastTreepath = null;
    TopicInfo lastTopic = null;
    protected void doShow_Click(object sender, EventArgs e)
    {
        sumOwe = sumDue = sumRemaining = 0;

        DateTime start = Helper.ParseDate(mFromDate.Text, Null.NullDate);
        DateTime end = Helper.ParseDate(mToDate.Text, Null.NullDate);
        if (Null.IsNull(start))
        {
            msg.Text = LangProvider.Instance["InvalidStartDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(end))
        {
            msg.Text = LangProvider.Instance["InvalidEndDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(start) || Null.IsNull(end))
            return;
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);
        int userID = Null.NullInteger;
        if (!SiteSettings.User.HasAccess("totalbook_all"))
            userID = SiteSettings.UserID;
        DataTable items = AccController.GetBook(
            Helper.GetString(mTotals.SelectedValue, "")
            , start
            , end, userID
            , Helper.GetInt(mWards.SelectedValue, Null.NullInteger), Null.NullInteger, SiteSettings.ProgramID, Null.NullInteger, Null.NullInteger, null
            , mFromVoucherNo.Text.Trim(), mToVoucherNo.Text.Trim(), SortCol, SortOrder, mShowInitRemain.Checked, false, false, false, false, null
            , Helper.GetDecimal(mAmount.Text, -1), -1, GetDBs(), -1, false);
        mGrid.DataSource = items;
        mGrid.DataBind();
        doExcel.Enabled = true;
        try
        {
            //mGrid.DataSource = items;
            //mGrid.DataBind();
        }
        finally
        {
            if (null != items)
                items.Dispose();
        }

    }
    protected void doExcel_Click(object sender, EventArgs e)
    {
        Exporter.ExportExcel(Response, mTitle.Text, mGrid);
    }
    private int Level
    {
        get
        {
            if (null == ViewState["Level"])
                return 1;
            return (int)ViewState["Level"];
        }
        set
        {
            ViewState["Level"] = value;
        }
    }

    protected void mGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        doShow_Click(null, null);
    }

    protected string SortCol
    {
        get
        {
            return Helper.GetString(ViewState["SortCol"], null);
        }
        set
        {
            ViewState["SortCol"] = value;
        }
    }
    protected string SortOrder
    {
        get
        {
            return Helper.GetString(ViewState["SortOrder"], "asc");
        }
        set
        {
            ViewState["SortOrder"] = value;
        }
    }
}