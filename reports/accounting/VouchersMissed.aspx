﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="VouchersMissed.aspx.cs" Inherits="reports_accounting_VouchersMissed" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cnt" Runat="Server">
    <div id="bar">
    </div>

    <div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label Text="Lang.FromDate" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4" />
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromDate" runat="server" CssClass="form-control input-sm dateselect"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="mFromDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label Text="Lang.ToDate" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4" />
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToDate" runat="server" CssClass="form-control input-sm dateselect"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="mToDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label Text="Lang.FromDocNo" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"/>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromVoucherID" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="fte1" runat="server" TargetControlID="mFromVoucherID"
                            FilterType="Numbers" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label Text="Lang.ToDocNo" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"/>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToVoucherID" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="fte2" runat="server" TargetControlID="mToVoucherID"
                            FilterType="Numbers" />
                    </div>
                </div>
            </div>
        </div>
         <div class="row margin-5-top">
            <div class="col-md-9 col-lg-10 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2 form-group-container">
                <asp:Button ID="doShow" runat="server" Text='Lang.Show' CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <asp:Label runat="server" ID="msg" CssClass="label-30 alert alert-danger block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
    </div>
    <asp:Label ID='mIDs' runat="server"></asp:Label>
    <script>
        $(document).ready(function() {
            setClassReports();
            dateSelect('.dateselect');
            $("#bar").prepend(breadcrumb({
                title: Lang.MissedVouchers,
                parent: {
                    title: Lang.MReports,
                    url: 'menu/reports.aspx'
                }
            }));
        });
    </script>
</asp:Content>

