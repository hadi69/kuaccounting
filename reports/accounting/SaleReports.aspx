﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SaleReports.aspx.cs" Inherits="reports_accounting_SalesReports" %>

<%@ Register Src="../../controls/ReportSetting.ascx" TagName="ReportSetting" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content runat="server" ContentPlaceHolderID="head">
    <style>
        input[type="text"]:disabled {
            opacity: 0.6;
        }
    </style>
</asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>
    <uc1:ReportSetting ID="mReportSetting" runat="server" />
    <div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="CooperationsLabel" runat="server" Text="Lang.CooperationType" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"></asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:DropDownList ID="Cooperations" runat="server" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label1" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromDate" runat="server" CssClass="form-control input-sm dateselect"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="mFromDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToDate" runat="server" CssClass="form-control input-sm dateselect"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="mToDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin-15-top">
            <div class="col-xs-12 form-group-container">
                <div class="form-group">
                    <asp:Label runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one" Style="line-height: 4em;">
                        Lang.DisplayBy
                    </asp:Label>
                    <div class="col-xs-12 col-eleven form-col-without-padding">
                        <div class="col-xs-12 form-col-without-padding">
                            <div class="col-xs-2">
                                <asp:RadioButton ID="byPerson" runat="server" Text="Lang.Person" Checked="true" GroupName="byP" CssClass="byperson" />
                            </div>
                            <div class="col-xs-10 form-col-without-padding">
                                <asp:DropDownList ID="Person" runat="server" CssClass="person chosen-select chosen-rtl"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-xs-12 form-col-without-padding">
                            <div class="col-xs-2">
                                <asp:RadioButton ID="byPersons" runat="server" Text="Lang.PersonsGroupLable" GroupName="byP" CssClass="bypersons" />
                            </div>
                            <div class="col-xs-10 form-col-without-padding">
                                <asp:TextBox ID="PersonsGroupTextBox" runat="server" CssClass="personsgrouptextbox form-control input-sm">0</asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 form-group-container">
                <div class="form-group">
                    <asp:Label runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one" Style="line-height: 4em;">
                        Lang.DisplayBy
                    </asp:Label>
                    <div class="col-xs-12 col-eleven form-col-without-padding">
                        <div class="col-xs-12 form-col-without-padding">
                            <div class="col-xs-2">
                                <asp:RadioButton ID="byCommodity" runat="server" Text="Lang.Commodity" Checked="true" GroupName="byC" CssClass="bycommodity" />
                            </div>
                            <div class="col-xs-10 form-col-without-padding">
                                <asp:DropDownList ID="Commodity" runat="server" CssClass="commodity chosen-select chosen-rtl"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-xs-12 form-col-without-padding">
                            <div class="col-xs-2">
                                <asp:RadioButton ID="byCommodities" runat="server" Text="Lang.CommoditisGroupLable" GroupName="byC" CssClass="bycommodities" />
                            </div>
                            <div class="col-xs-10 form-col-without-padding">
                                <asp:TextBox ID="CommoditisGroupTextBox" runat="server" CssClass="commoditisgrouptextbox form-control input-sm">0</asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin-5-top">
            <div class="col-md-8 col-lg-6 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container form-col-without-padding">
                <div class="col-xs-12 col-sm-3">
                    <asp:Button ID="CreateGroups" runat="server" Text="Lang.CreateGroups" ClientIDMode="AutoID" OnClick="Button1_Click" CssClass="creategroups btn btn-xs btn-primary btn-block" />
                </div>
                <div class="col-xs-12 col-sm-3">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
                </div>
                <div class="col-xs-12 col-sm-3">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcel_Click" Enabled="false" />
                </div>
                <div class="col-xs-12 col-sm-3">
                    <input type="button" id="print" class="btn btn-xs btn-dark-blue btn-block" value="" />
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="panelDropDowns" runat="server" CssClass="well callout padding-5" Visible="False">
        <div class="row">
            <div class="col-xs-12">
                <h5 class="dashed-bottom padding-5-bottom yekan-family">
                    <asp:Label runat="server">Lang.PersonsGroupLable</asp:Label>
                </h5>
            </div>
        </div>
        <div id="PersonsGroupDiv" runat="server" class="personsgroupdiv row"></div>

        <div class="row">
            <div class="col-xs-12">
                <h5 class="dashed-bottom padding-5-bottom yekan-family">
                    <asp:Label runat="server">Lang.CommoditisGroupLable</asp:Label>
                </h5>
            </div>
        </div>
        <div id="CommodityGroupDiv" runat="server" class="commoditygroupdiv row"></div>
    </asp:Panel>
    <div class="col-xs-12">
        <asp:Label runat="server" ID="msg" CssClass="label-30 alert alert-danger block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
    </div>
    <asp:Panel ID="voucherDetailPanel" runat="server" Width="100%" CssClass="voucherdetailpanel row">
        <div class="col-xs-12">
            <JP:MiniHeader ID="mTitle" runat="server" Text="Lang.SaleReports" />
            <asp:GridView ID="mGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                AllowSorting="true" OnSorting="mGrid_Sorting">
                <Columns>
                    <asp:TemplateField HeaderText="Lang.ColRowNr">
                        <ItemTemplate>
                            <%# Container.DataItemIndex + 1 %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="PersonName" HeaderText="Lang.Person" ItemStyle-HorizontalAlign="Center" SortExpression="PersonName" />
                    <asp:BoundField DataField="FactorDate" HeaderText="Lang.Date" ItemStyle-HorizontalAlign="Center" SortExpression="FactorDate" />
                    <asp:BoundField DataField="ServiceName" HeaderText="Lang.Commodity" ItemStyle-HorizontalAlign="Center" SortExpression="ServiceName" />
                    <asp:TemplateField HeaderText="Lang.NumOrValue" SortExpression="DeliveredAmount">
                        <ItemTemplate>
                            <asp:Label ID="DeliveredAmount" runat="server" Text='<%#Eval("DeliveredAmount") %>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="SumNum" runat="server" Text="Lang.Sum" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Price" HeaderText="Lang.Price" ItemStyle-HorizontalAlign="Center" SortExpression="Price" />

                    <asp:TemplateField HeaderText="Lang.TotalPrice" SortExpression="TotalPrice">
                        <ItemTemplate>
                            <asp:Label ID="TotalPrice" runat="server" Text='<%#Eval("TotalPrice") %>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="SumPrice" runat="server" Text="Lang.Sum" />
                        </FooterTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle CssClass="GridHeader" />
                <RowStyle CssClass="GridRow" />
                <SelectedRowStyle CssClass="GridRowSelected" />
            </asp:GridView>
        </div>
    </asp:Panel>

    <script>
        $(function () {
            $('#print').attr('value', Lang.Print);
            dateSelect('.dateselect');

            $('#print').click(function () {
                printTable('.ScrollPanelCartable', '../../', Lang.SaleReports);
            });

            $('.byperson').click(function () {
                $('.personsgrouptextbox').attr('value', 0);
                $('.personsgroupdiv').hide();
                $('.voucherdetailpanel').hide();
            });

            $('.bycommodity').click(function () {
                $('.commoditisgrouptextbox').attr('value', 0);
                $('.commoditygroupdiv').hide();
                $('.voucherdetailpanel').hide();
            });

            $('.bypersons').click(function () {
                $('.person').attr('value', '');
                $('.voucherdetailpanel').hide();
            });

            $('.bycommodities').click(function () {
                $('.commodity').attr('value', '');
                $('.voucherdetailpanel').hide();
            });

            $('.creategroups').click(function () {
                $('.voucherdetailpanel').hide();
            });

            $("#bar").prepend(breadcrumb({
                title: Lang.SaleReports,
                parent: {
                    title: Lang.Reports,
                    url: 'menu/Reports.aspx'
                }
            }));
            setClassReports();

            function TogglePerson() {
                if ($('#<%= byPerson.ClientID %>').attr('checked')) {
                    $('#<%= Person.ClientID %>').removeAttr('disabled').trigger('chosen:updated');
                    $('#<%= PersonsGroupTextBox.ClientID %>').attr('disabled', 'disabled');
                } else {
                    $('#<%= Person.ClientID %>').attr('disabled', 'disabled').trigger('chosen:updated');
                    $('#<%= PersonsGroupTextBox.ClientID %>').removeAttr('disabled');
                }
            }

            function ToggleCommodity() {
                if ($('#<%= byCommodity.ClientID %>').attr('checked')) {
                    $('#<%= Commodity.ClientID %>').removeAttr('disabled').trigger('chosen:updated');
                    $('#<%= CommoditisGroupTextBox.ClientID %>').attr('disabled', 'disabled');
                } else {
                    $('#<%= Commodity.ClientID %>').attr('disabled', 'disabled').trigger('chosen:updated');
                    $('#<%= CommoditisGroupTextBox.ClientID %>').removeAttr('disabled');
                }
            }

            TogglePerson();
            ToggleCommodity();

            $('#<%= byPerson.ClientID %>').change(function() {
                TogglePerson();
            });

            $('#<%= byPersons.ClientID %>').change(function() {
                TogglePerson();
            });

            $('#<%= byCommodity.ClientID %>').change(function() {
                ToggleCommodity();
            });

            $('#<%= byCommodities.ClientID %>').change(function() {
                ToggleCommodity();
            });
        });

    </script>
</asp:Content>















