﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reports_accounting_Lawashak : System.Web.UI.Page
{
    System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
    protected void Page_Load(object sender, EventArgs e)
    {
        msg.Visible = false;
        mGrid.EmptyDataText = LangProvider.Instance["DocumentIsEmpty"];
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");
        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["Lawashak"];
            Page.SetLang();
            int year = cal.GetYear(DateTime.Now);
            //mFromDate.Text = string.Format("{0}/01/01", year - 1300);
            //mToDate.Text = Helper.FormatDate(cal.ToDateTime(year + 1, 1, 1, 1, 1, 1, 1).AddDays(-1), "yy/MM/dd");
            IDataReader financeYearStartDate = DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");//0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            //mFromDate.Text = 
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);

            mReportSetting.ReportName = "Lawashak";
        }
        mReportSetting.SaveClick += new EventHandler(mReportSetting_SaveClick);
        mReportSetting.ReportSettingSelect += new EventHandler(mReportSetting_ReportSettingSelect);
    }

    void mReportSetting_ReportSettingSelect(object sender, EventArgs e)
    {
        ReportSettingInfo info = mReportSetting.SelectedReportSettings;
        if (info == null)
            return;
        string[] lines = info.Settings.Split('\n');
        mFromDate.Text = lines[0].Trim('\r');
        mToDate.Text = lines[1].Trim('\r');
        mProductType.Text = lines[2].Trim('\r');
        mNumKgFrom.Text = lines[3].Trim('\r');
        mNumKgTo.Text = lines[4].Trim('\r');
        mNumBoxFrom.Text = lines[5].Trim('\r');
        mNumBoxTo.Text = lines[6].Trim('\r');
        mPersonHourFrom.Text = lines[7].Trim('\r');
        mPersonHourTo.Text = lines[8].Trim('\r');
    }

    void mReportSetting_SaveClick(object sender, EventArgs e)
    {
        StringBuilder b = new StringBuilder();
        b.AppendLine(mFromDate.Text); //1
        b.AppendLine(mToDate.Text); //2
        b.AppendLine(mProductType.Text);
        b.AppendLine(mNumKgFrom.Text);
        b.AppendLine(mNumKgTo.Text);
        b.AppendLine(mNumBoxFrom.Text);
        b.AppendLine(mNumBoxTo.Text);
        b.AppendLine(mPersonHourFrom.Text);
        b.AppendLine(mPersonHourTo.Text);
        mReportSetting.Save(b.ToString());
    }

    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.Common.DbDataRecord record = e.Row.DataItem as System.Data.Common.DbDataRecord;
            DateTime dt = Helper.GetDateTime(record["ProductDate"], Helper.NullDate);
            if (dt != Helper.NullDate)
                e.Row.Cells[1].Text = Helper.FormatDate(dt, null);

            ((Label)e.Row.FindControl("NumKg")).Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["NumKg"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("NumBox")).Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["NumBox"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("PersonHour")).Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["PersonHour"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            sumNumKg += Helper.GetDecimal(record["NumKg"], 0);
            sumNumBox += Helper.GetDecimal(record["NumBox"], 0);
            sumPersonHour += Helper.GetDecimal(record["PersonHour"], 0);
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            ((Label)e.Row.FindControl("NumKgTotal")).Text = SiteSettings.ToMonetary(sumNumKg, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("NumBoxTotal")).Text = SiteSettings.ToMonetary(sumNumBox, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("PersonHourTotal")).Text = SiteSettings.ToMonetary(sumPersonHour, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }
    }
    decimal sumNumKg = 0, sumNumBox = 0, sumPersonHour = 0;
    protected void mGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        BindGrid();
    }
    protected void BindGrid()
    {
        sumNumKg = sumNumBox = sumPersonHour = 0;
        DateTime start = Helper.ParseDate(mFromDate.Text, Null.NullDate);
        DateTime end = Helper.ParseDate(mToDate.Text, Null.NullDate);
        if (Null.IsNull(start))
        {
            msg.Text = LangProvider.Instance["InvalidStartDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(end))
        {
            msg.Text = LangProvider.Instance["InvalidEndDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(start) || Null.IsNull(end))
            return;
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);

        //mProductType.Text

        string qry = string.Format(@"SELECT * FROM Lawashak WHERE {0} <= ProductDate AND ProductDate <= {1} "
            , Helper.QoutedDateTime(start), Helper.QoutedDateTime(end));

        if (mProductType.Text.Trim().Length > 0)
            qry += " AND ProductType LIKE '%" + mProductType.Text.Replace("'", "''") + "%'";

        decimal d = Helper.GetDecimal(mNumKgFrom.Text, -1);
        if (d != Null.NullInteger)
            qry += " AND NumKg >= " + d;
        d = Helper.GetDecimal(mNumKgTo.Text, -1);
        if (d != Null.NullInteger)
            qry += " AND NumKg <= " + d;

        d = Helper.GetDecimal(mNumBoxFrom.Text, -1);
        if (d != Null.NullInteger)
            qry += " AND NumBox >= " + d;
        d = Helper.GetDecimal(mNumBoxTo.Text, -1);
        if (d != Null.NullInteger)
            qry += " AND NumBox <= " + d;

        d = Helper.GetDecimal(mPersonHourFrom.Text, -1);
        if (d != Null.NullInteger)
            qry += " AND PersonHour >= " + d;
        d = Helper.GetDecimal(mPersonHourTo.Text, -1);
        if (d != Null.NullInteger)
            qry += " AND PersonHour <= " + d;

        if (!string.IsNullOrEmpty(SortCol))
            qry += " ORDER BY " + SortCol + " " + SortOrder;

        IDataReader reader = DbProxy.Instance.ExecuteReader(qry);
        try
        {
            mGrid.DataSource = reader;
            mGrid.DataBind();
        }
        finally
        {
            if (null != reader)
            {
                reader.Close();
                reader.Dispose();
            }
        }
    }
    protected string SortCol
    {
        get
        {
            return Helper.GetString(ViewState["SortCol"], null);
        }
        set
        {
            ViewState["SortCol"] = value;
        }
    }
    protected string SortOrder
    {
        get
        {
            return Helper.GetString(ViewState["SortOrder"], "asc");
        }
        set
        {
            ViewState["SortOrder"] = value;
        }
    }
    protected void doShow_Click(object sender, EventArgs e)
    {
        BindGrid();

        mDetailPanel.Visible = true;

        doExcel.Enabled = true;
    }
    protected void doExcel_Click(object sender, EventArgs e)
    {
        Exporter.ExportExcel(Response, mTitle.Text, mGrid);
    }
}