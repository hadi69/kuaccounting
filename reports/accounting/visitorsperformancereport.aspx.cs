﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;

public partial class reports_accounting_visitorsperformancereport : System.Web.UI.Page
{
    private System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");

        ConfirmedStoredraftGrid.EmptyDataText = LangProvider.Instance["ListIsEmpty"];
        NotConfirmedStoredraftGrid.EmptyDataText = LangProvider.Instance["ListIsEmpty"];
        VisitedCustomersGrid.EmptyDataText = LangProvider.Instance["ListIsEmpty"];
        NotVisitedCustomersGrid.EmptyDataText = LangProvider.Instance["ListIsEmpty"];

        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["VisitorsPerformanceReport"];
            Page.SetLang();
           // IDataReader financeYearStartDate =
               // DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");
            //0:Temp;1:Closed;2:Active;3:Closing
           // if (financeYearStartDate.Read())
             //   mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), "yy/MM/dd");
            //mFromDate.Text = 
            mFromDate.Text = Helper.FormatDate(DateTime.Now, null);
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);
            Initial();

            mReportSetting.ReportName = "VisitorsPerformanceReport";
        }

    }

    private void mReportSetting_ReportSettingSelect(object sender, EventArgs e)
    {
        ReportSettingInfo info = mReportSetting.SelectedReportSettings;
        if (info == null)
            return;
        string[] lines = info.Settings.Split('\n');
        mVisitors.SelectedValue = lines[0].Trim('\r');
        mFromDate.Text = lines[2].Trim('\r');
        mToDate.Text = lines[3].Trim('\r');
    }

    private void mReportSetting_SaveClick(object sender, EventArgs e)
    {
        StringBuilder b = new StringBuilder();
        b.AppendLine(mVisitors.SelectedValue); //0
        b.AppendLine(mFromDate.Text); //1
        b.AppendLine(mToDate.Text); //2
        mReportSetting.Save(b.ToString());
    }

    private void Initial()
    {
        {
            IDataReader visitors = DbProxy.Instance.ExecuteReader("SELECT ID, [dbo].[DisplayPerson](ID, 1), ISNULL(Code, '') FROM Personnel WHERE Type=101 AND Personnel.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID = CONVERT(int, dbo.GetSetting('VisitorGrouping'), 0))");
            mVisitors.Items.Clear();

           // mVisitors.Items.Add(new ListItem(LangProvider.Instance["All"], ""));

            if (visitors != null)
            {
                while (visitors.Read())
                {
                    if(!visitors.IsDBNull(0) && !visitors.IsDBNull(1) && !visitors.IsDBNull(2))
                    mVisitors.Items.Add(new ListItem(visitors.GetString(1) + " - کد: " + visitors.GetString(2), visitors.GetInt32(0).ToString()));
                }
                visitors.Close();
            }
            mVisitors.SelectedIndex = 0;
        }
    }

    //protected void DlBaseChange(object sender, EventArgs e)
    //{
    //    DropDownList comboBox = (DropDownList)sender;
    //    int selected = Int32.Parse(comboBox.SelectedValue);
    //    if (selected == 1)
    //    {
    //        personLable.Text = LangProvider.Instance["ActorName"];
    //        panelDropDowns.Visible = panelInternal.Visible = true;
    //        //  mActorName.Visible = ActorNameLabel.Visible = mTransferee.Visible = TransfereeLabel.Visible = true;
    //    }
    //    else
    //    {
    //        personLable.Text = LangProvider.Instance["Person"];
    //        panelDropDowns.Visible = panelInternal.Visible = false;
    //        //mActorName.Visible = ActorNameLabel.Visible = mTransferee.Visible = TransfereeLabel.Visible = false;
    //    }
    //}

    protected void doShow_Click(object sender, EventArgs e)
    {
        doExcel.Enabled = true;
        if (mShowVisitedCustomers.Checked)
        {
            VisitedCustomersGridPanel.Visible = true;
            BindVisitedCustomersGrid();
        }

        if (mShowNotVisitedCustomers.Checked)
        {
            NotVisitedCustomersGridPanel.Visible = true;
            BindNotVisitedCustomersGrid();
        }

        if (mShowConfirmedStoredraft.Checked)
        {
            ConfirmedStoredraftGridPanel.Visible = true;
            BindConfirmedStoredraftGrid();
        }

        if (mShowNotConfirmedStoredraft.Checked)
        {
            NotConfirmedStoredraftGridPanel.Visible = true;
            BindNotConfirmedStoredraftGrid();
        }

    }


    private decimal _sumTotalPrice, _sumPursant;
    private float _sumTotalAmount;
    protected void ConfirmedStoredraftGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.Common.DbDataRecord record = e.Row.DataItem as System.Data.Common.DbDataRecord;
            DateTime dt = DateTime.Now;
            dt = Helper.GetDateTime(record["FactorDate"], Helper.NullDate);
            Label lblGridDate = (Label)e.Row.FindControl("lblGridDate");
            if (dt != Helper.NullDate && lblGridDate != null)
            {
                lblGridDate.Text = Helper.ToFaFormat(dt); ;
            }
            e.Row.Cells[1].Text = string.Format("<a href='../../list.aspx?table=StoreDraft&id={0}&menu=accountinga'>{1}</a>", record["ID"], record["NOSerial"]);
            decimal totalAmount = Helper.GetDecimal(record["totalAmount"], 0);
            e.Row.Cells[5].Text = totalAmount.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            decimal totalPursant = SiteSettings.ToMonetary(Helper.GetDecimal(record["Pursant"], 0), dt);
            e.Row.Cells[6].Text = totalPursant.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            decimal TotalPrice = SiteSettings.ToMonetary(Helper.GetDecimal(record["TotalPrice"], 0), dt);
            e.Row.Cells[7].Text = TotalPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            _sumTotalAmount += Helper.GetFloat(record["TotalAmount"], 0);
            _sumTotalPrice += TotalPrice;
            _sumPursant += totalPursant;
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            (e.Row.FindControl("totalTotalAmount") as Label).Text = _sumTotalAmount.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("totalTotalPrice") as Label).Text = _sumTotalPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("totalPursant") as Label).Text = _sumPursant.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }
    }

    private decimal _sumTotalPrice2, _sumPursant2;
    private float _sumTotalAmount2;
    protected void NotConfirmedStoredraftGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.Common.DbDataRecord record = e.Row.DataItem as System.Data.Common.DbDataRecord;
            DateTime dt = DateTime.Now;
            dt = Helper.GetDateTime(record["FactorDate"], Helper.NullDate);
            Label lblGridDate = (Label)e.Row.FindControl("lblGridDate");
            if (dt != Helper.NullDate && lblGridDate != null)
            {
                lblGridDate.Text = Helper.ToFaFormat(dt); ;
            }
            e.Row.Cells[1].Text = string.Format("<a href='../../list.aspx?table=StoreDraft&id={0}&menu=accountinga'>{1}</a>", record["ID"], record["NOSerial"]);
            decimal totalAmount = Helper.GetDecimal(record["totalAmount"], 0);
            e.Row.Cells[5].Text = totalAmount.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            decimal totalPursant = SiteSettings.ToMonetary(Helper.GetDecimal(record["Pursant"], 0), dt);
            e.Row.Cells[6].Text = totalPursant.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            decimal TotalPrice = SiteSettings.ToMonetary(Helper.GetDecimal(record["TotalPrice"], 0), dt);
            e.Row.Cells[7].Text = TotalPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            _sumTotalAmount2 += Helper.GetFloat(record["TotalAmount"], 0);
            _sumTotalPrice2 += TotalPrice;
            _sumPursant2 += totalPursant;
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            (e.Row.FindControl("totalTotalAmount") as Label).Text = _sumTotalAmount2.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("totalTotalPrice") as Label).Text = _sumTotalPrice2.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("totalPursant") as Label).Text = _sumPursant2.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }
    }

    private decimal _sumTotalPrice3;
    protected void VisitedCustomersGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.Common.DbDataRecord record = e.Row.DataItem as System.Data.Common.DbDataRecord;
            DateTime dt = DateTime.Now;
            dt = Helper.GetDateTime(record["VisitorPlanDayDate"], Helper.NullDate);
            Label lblGridDate = (Label) e.Row.FindControl("lblGridDate");
            if (dt != Helper.NullDate && lblGridDate != null)
            {
                lblGridDate.Text = Helper.ToFaFormat(dt);
            }
            decimal price = SiteSettings.ToMonetary(Helper.GetDecimal(record["Price"], 0), dt);
            e.Row.Cells[4].Text = price.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            _sumTotalPrice3 += price;
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            (e.Row.FindControl("TotalPrice") as Label).Text = _sumTotalPrice3.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }
    }
    protected void NotVisitedCustomersGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.Common.DbDataRecord record = e.Row.DataItem as System.Data.Common.DbDataRecord;
            DateTime dt = DateTime.Now;
            dt = Helper.GetDateTime(record["VisitorPlanDayDate"], Helper.NullDate);
            string comments = Helper.GetString(record["Comments"], null);
            if (string.IsNullOrEmpty(comments))
                e.Row.Cells[6].Text = Helper.GetString(record["NoVisitReasonName"], "");
            Label lblGridDate = (Label)e.Row.FindControl("lblGridDate");
            if (dt != Helper.NullDate && lblGridDate != null)
            {
                lblGridDate.Text = Helper.ToFaFormat(dt);
            }
        }

    }

    protected void ConfirmedStoredraftGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        BindConfirmedStoredraftGrid();
    }

    protected void NotConfirmedStoredraftGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        BindNotConfirmedStoredraftGrid();
    }

    protected void VisitedCustomersGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        BindVisitedCustomersGrid();
    }
    protected void NotVisitedCustomersGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        BindNotVisitedCustomersGrid();
    }

    private void BindConfirmedStoredraftGrid()
    {
        DateTime start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        DateTime end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);

        int visitorId = Helper.GetInt(mVisitors.SelectedValue, Null.NullInteger);
        string query = "";

        #region storedraft

        query = string.Format(@"SELECT StoreDraftView.ID , StoreDraftView.NOSerial, StoreDraftView.FactorDate
                            , StoreDraftView.BuyerID, StoreDraftView.BuyerName, StoreDraftView.Comments
                            ,StoreDraftView.TotalPrice , StoreDraftView.Pursant
                            ,(SELECT SUM(DeliveredAmount * UnitCurrencyConverter) FROM StoreDraftItems WHERE StoreDraftID = StoreDraftView.ID) TotalAmount
                            FROM StoreDraftView WHERE StoreDraftView.WorkflowStatus = 1");
       
        string where = string.Format(" AND (StoreDraftView.StoreDraftType = 0 OR StoreDraftView.StoreDraftType IS NULL) AND FactorDate>={0} AND FactorDate<={1}", Helper.QoutedDateTime(start),
                Helper.QoutedDateTime(end));

        if (visitorId != Null.NullInteger)
        {
            where += "AND StoreDraftView.VisitorID = " + visitorId;
        }
        
        query += where;

        if (!string.IsNullOrEmpty(SortCol))
            query += " ORDER BY " + SortCol + " " + SortOrder;
        else
            query += " ORDER BY FactorDate, BuyerID, DATEADD(dd, 0, DATEDIFF(dd, 0, FactorDate)), NoSerial, ID";

        #endregion
        IDataReader items = DbProxy.Instance.ExecuteReader(query);
        try
        {
            ConfirmedStoredraftGrid.DataSource = items;
            ConfirmedStoredraftGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Close();
                items.Dispose();
            }
        }
    }

    private void BindNotConfirmedStoredraftGrid()
    {
        DateTime start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        DateTime end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);

        int visitorId = Helper.GetInt(mVisitors.SelectedValue, Null.NullInteger);
        string query = "";

        #region storedraft

        query = string.Format(@"SELECT StoreDraftView.ID , StoreDraftView.NOSerial, StoreDraftView.FactorDate
                            , StoreDraftView.BuyerID, StoreDraftView.BuyerName, StoreDraftView.Comments
                            ,StoreDraftView.TotalPrice , StoreDraftView.Pursant
                            ,(SELECT SUM(DeliveredAmount * UnitCurrencyConverter) FROM StoreDraftItems WHERE StoreDraftID = StoreDraftView.ID) TotalAmount
                            FROM StoreDraftView WHERE (StoreDraftView.WorkflowStatus <> 1 OR StoreDraftView.WorkflowStatus IS NULL)");

        string where = string.Format(" AND (StoreDraftView.StoreDraftType = 0 OR StoreDraftView.StoreDraftType IS NULL) AND FactorDate>={0} AND FactorDate<={1}", Helper.QoutedDateTime(start),
                Helper.QoutedDateTime(end));

        if (visitorId != Null.NullInteger)
        {
            where += "AND StoreDraftView.VisitorID = " + visitorId;
        }

        query += where;

        if (!string.IsNullOrEmpty(SortCol))
            query += " ORDER BY " + SortCol + " " + SortOrder;
        else
            query += " ORDER BY FactorDate, BuyerID, DATEADD(dd, 0, DATEDIFF(dd, 0, FactorDate)), NoSerial, ID";

        #endregion
        IDataReader items = DbProxy.Instance.ExecuteReader(query);
        try
        {
            NotConfirmedStoredraftGrid.DataSource = items;
            NotConfirmedStoredraftGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Close();
                items.Dispose();
            }
        }
    }

    private void BindVisitedCustomersGrid()
    {
        DateTime start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        DateTime end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);

        int visitorId = Helper.GetInt(mVisitors.SelectedValue, Null.NullInteger);
        string query = "";

        #region customers

        query = string.Format(@"SELECT VisitorPlanDays.VisitDate AS VisitorPlanDayDate, VisitorCustomersStatusView.CustomerName
                             ,VisitorPlanView.VisitorName
                             , (SELECT TotalPrice FROM StoreDraft WHERE CONVERT(DATE, FactorDate) = VisitorCustomersStatusView.VisitDate AND BuyerID = VisitorCustomersStatusView.CustomerID AND VisitorID = VisitorPlanView.VisitorID )  AS Price
                            ,VisitorCustomersStatusView.FGroupName, VisitorCustomersStatusView.SGroupName
                            ,VisitorCustomersStatusView.Address   
                            FROM VisitorCustomersStatusView
                            LEFT OUTER JOIN DailyCustomers ON VisitorCustomersStatusView.DailyCustomersID = DailyCustomers.ID
                            LEFT OUTER JOIN VisitorPlanDays ON DailyCustomers.VisitorPlanDaysID = VisitorPlanDays.ID
                            LEFT OUTER JOIN VisitorPlanView ON VisitorPlanDays.VisitorPlanID = VisitorPlanView.ID
                            WHERE VisitorCustomersStatusView.Status = 1");

        string where = string.Format(" AND VisitorCustomersStatusView.VisitDate >={0} AND VisitorCustomersStatusView.VisitDate <={1}", Helper.QoutedDateTime(start),
                Helper.QoutedDateTime(end));

        if (visitorId != Null.NullInteger)
        {
            where += "AND VisitorPlanView.VisitorID = " + visitorId;
        }

        query += where;

        if (!string.IsNullOrEmpty(SortCol))
            query += " ORDER BY " + SortCol + " " + SortOrder;
        else
            query += " ORDER BY VisitorCustomersStatusView.VisitDate";

        #endregion
        IDataReader items = DbProxy.Instance.ExecuteReader(query);
        try
        {
            VisitedCustomersGrid.DataSource = items;
            VisitedCustomersGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Close();
                items.Dispose();
            }
        }
    }

    private void BindNotVisitedCustomersGrid()
    {
        DateTime start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        DateTime end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);

        int visitorId = Helper.GetInt(mVisitors.SelectedValue, Null.NullInteger);
        string query = "";

        #region customers

        query = string.Format(@"SELECT VisitorPlanDays.VisitDate AS VisitorPlanDayDate, VisitorCustomersStatusView.CustomerName
                            ,VisitorCustomersStatusView.FGroupName, VisitorCustomersStatusView.SGroupName
                            ,VisitorCustomersStatusView.Address, VisitorCustomersStatusView.Comments, VisitorCustomersStatusView.NoVisitReasonName
                            FROM VisitorCustomersStatusView
                            LEFT OUTER JOIN DailyCustomers ON VisitorCustomersStatusView.DailyCustomersID = DailyCustomers.ID
                            LEFT OUTER JOIN VisitorPlanDays ON DailyCustomers.VisitorPlanDaysID = VisitorPlanDays.ID
                            LEFT OUTER JOIN VisitorPlan ON VisitorPlanDays.VisitorPlanID = VisitorPlan.ID
                            WHERE VisitorCustomersStatusView.Status IN (0 , 2)");

        string where = string.Format(" AND VisitorCustomersStatusView.VisitDate >={0} AND VisitorCustomersStatusView.VisitDate <={1}", Helper.QoutedDateTime(start),
                Helper.QoutedDateTime(end));

        if (visitorId != Null.NullInteger)
        {
            where += "AND VisitorPlan.VisitorID = " + visitorId;
        }

        query += where;

        if (!string.IsNullOrEmpty(SortCol))
            query += " ORDER BY " + SortCol + " " + SortOrder;
        else
            query += " ORDER BY VisitorCustomersStatusView.VisitDate";

        #endregion
        IDataReader items = DbProxy.Instance.ExecuteReader(query);
        try
        {
            NotVisitedCustomersGrid.DataSource = items;
            NotVisitedCustomersGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Close();
                items.Dispose();
            }
        }
    }

    protected string SortCol
    {
        get { return Helper.GetString(ViewState["SortCol"], null); }
        set { ViewState["SortCol"] = value; }
    }

    protected string SortOrder
    {
        get { return Helper.GetString(ViewState["SortOrder"], "asc"); }
        set { ViewState["SortOrder"] = value; }
    }

    protected void doExcel_Click(object sender, EventArgs e)
    {
        Exporter.ExportExcel(Response
            , new List<string>(new string[] { ConfirmedStoredraftGridTitle.Text })
            , new List<GridView>(new GridView[] { ConfirmedStoredraftGrid }));
    }

    protected void OnCheckedChanged(object sender, EventArgs e)
    {
        if (!mShowVisitedCustomers.Checked)
        {
            VisitedCustomersGridPanel.Visible = false;
        }

        if (!mShowNotVisitedCustomers.Checked)
        {
            NotVisitedCustomersGridPanel.Visible = false;
        }

        if (!mShowConfirmedStoredraft.Checked)
        {
            ConfirmedStoredraftGridPanel.Visible = false;
        }

        if (!mShowNotConfirmedStoredraft.Checked)
        {
            NotConfirmedStoredraftGridPanel.Visible = false;
        }
    }
}