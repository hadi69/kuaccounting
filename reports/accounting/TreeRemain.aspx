﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TreeRemain.aspx.cs" Inherits="reports_accounting_TreeRemain" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>

    <div class="row">
        <div class="col-xs-12">
            <asp:UpdatePanel runat="server" ID="mUP">
                <ContentTemplate>
                    <div class="well callout padding-5">
                        <div class="row">
                            <div class="col-xs-12 col-sm-10 form-group-container">
                                <asp:PlaceHolder runat="server" ID="mBC"></asp:PlaceHolder>
                            </div>
                            <div class="col-xs-12 col-sm-2 form-group-container form-col-without-padding">
                                <div class="col-xs-12 col-sm-3">
                                    <asp:LinkButton ID="doForward" runat="server" CssClass="btn btn-primary" OnClick="doForward_Click" Visible="false">
                                        <span aria-hidden="true" class="glyphicon glyphicon-arrow-left"></span>
                                    </asp:LinkButton>
                                </div>
                                <div class="col-xs-12 col-sm-3">
                                    <asp:LinkButton ID="doBack" runat="server" CssClass="btn btn-primary" OnClick="doBack_Click">
                                        <span aria-hidden="true" class="glyphicon glyphicon-arrow-right"></span>
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:GridView ID="mGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                        Scrolling="Vertical" runat="server" ShowFooter="True" BorderStyle="None" OnRowDataBound="mGrid_RowDataBound"
                        AllowSorting="True" OnSorting="mGrid_Sorting" OnSelectedIndexChanged="mGrid_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField DataField="TreePath" Visible="false" />
                            <asp:BoundField DataField="Code" HeaderText="Lang.Code" ItemStyle-HorizontalAlign="Center" SortExpression="Code">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Lang.Topic" ItemStyle-CssClass="tr" SortExpression="Title">
                                <ItemTemplate>
                                    <asp:Button ID="btnSelect" runat="server" Text='<%# Eval("Title") %>' CommandName="Select" CssClass="btn-link" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Lang.Liability" SortExpression="Owe" ItemStyle-CssClass="align-center">
                                <ItemTemplate>
                                    <asp:Label ID="Owe" runat="server" Text='<%#Eval("Owe") %>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="totalOwe" runat="server" Text="Lang.Sum" />
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Lang.Credit" SortExpression="Due">
                                <ItemTemplate>
                                    <asp:Label ID="Due" runat="server" Text='<%#Eval("Due") %>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="totalDue" runat="server" Text="Lang.Sum" />
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Lang.Remaining" SortExpression="Remain">
                                <ItemTemplate>
                                    <asp:Label ID="Remain" runat="server" Text='<%#Eval("Remain") %>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="totalRemain" runat="server" Text="Lang.Sum" />
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Owe" HeaderText="Lang.Diagnosis" ItemStyle-CssClass="tl align-center" SortExpression="Remain">
                                <ItemStyle CssClass="tl align-center" />
                            </asp:BoundField>
                        </Columns>
                        <HeaderStyle CssClass="GridHeader" />
                        <RowStyle CssClass="GridRow" />
                        <SelectedRowStyle CssClass="GridRowSelected" />
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <script>
        $(function () {
            $('.GridRow,.GridAltRow').click(function () {
                $(this).toggleClass('selected');
            });
            dateSelect('.dateselect');
            $("#bar").prepend(breadcrumb({
                title: Lang.TopicAccountReport,
                parent: {
                    title: Lang.Reports,
                    url: 'menu/reports.aspx'
                }
            }));
            setClassReports();
        });
    </script>
</asp:Content>

