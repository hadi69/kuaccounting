﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reports_accounting_persons : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");

        mGrid.EmptyDataText = LangProvider.Instance["ListIsEmpty"];
        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["SaleAdminReport"];
            Page.SetLang();
            Initial();
            BindDBs();
        }

    }
    void BindDBs()
    {
        mDBList.Items.Clear();
        ListItem item = new ListItem(LangProvider.Instance["ActiveYear"] + " ", "");
        if (string.IsNullOrEmpty(SiteSettings.OtherCatalogName))
            item.Selected = true;
        mDBList.Items.Add(item);
        try
        {
            string path = Server.MapPath("~/files/fdb");
            if (System.IO.Directory.Exists(path))
            {
                string[] files = System.IO.Directory.GetFiles(path, "DB_*.mdf");
                for (int i = 0; i < files.Length; i++)
                {
                    string fileName = System.IO.Path.GetFileNameWithoutExtension(files[i]);
                    if (!fileName.ToLower().EndsWith("_log"))
                    {
                        item = new ListItem(fileName + " ", fileName);
                        if (fileName == SiteSettings.OtherCatalogName)
                            item.Selected = true;
                        mDBList.Items.Add(item);
                    }
                }
            }
        }
        catch { }
        panelDBs.Visible = mDBList.Items.Count > 1;
    }

    private void Initial()
    {
        {
            IDataReader personGroupings = DbProxy.Instance.ExecuteReader("SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping  WHERE GroupingType = 0");
            IDataReader persons = DbProxy.Instance.ExecuteReader("SELECT ID, ISNULL([dbo].[DisplayPerson](ID, 1), ''), ISNULL(Code, '') FROM Personnel WHERE Type=101");

            mPersonGroupings.Items.Clear();
            if (personGroupings != null)
            {
                while (personGroupings.Read())
                {
                    if (!personGroupings.IsDBNull(0) && !personGroupings.IsDBNull(1))
                        mPersonGroupings.Items.Add(new ListItem(personGroupings.GetString(1),
                            personGroupings.GetInt32(0).ToString()));
                }
                personGroupings.Close();
            }
        }

        IDataReader personGroup = DbProxy.Instance.ExecuteReader("SELECT ID, Title FROM PersonnelGroup");
        mPersonGroup.Items.Clear();
        if (personGroup != null)
        {
            while (personGroup.Read())
            {
                if (!personGroup.IsDBNull(0) && !personGroup.IsDBNull(1))
                    mPersonGroup.Items.Add(new ListItem(personGroup.GetString(1), personGroup.GetInt32(0).ToString()));
            }
            personGroup.Close();
        }
    }


    protected void doShow_Click(object sender, EventArgs e)
    {
        doExcel.Enabled = true;
        BindGrid();
    }

    private List<string> GetDBs()
    {
        List<string> dbs = new List<string>();
        for (int i = 0; i < mDBList.Items.Count; i++)
            if (mDBList.Items[i].Selected)
                dbs.Add(mDBList.Items[i].Value);
        return dbs;
    }
    #region Grid

    string rowStyle = "GridAltRow";
    decimal sumPrice = 0, sumProfit = 0;
    float sumNum = 0;
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView record = e.Row.DataItem as DataRowView;
            string gps = Helper.GetString(record["Latittude"]);
            if (!string.IsNullOrEmpty(gps) && !string.IsNullOrWhiteSpace(gps))
            {
                e.Row.Cells[10].Text = "<span style='color: red;'><i class='fa fa-map-marker'></i></span>";
            }
        }

    }

    protected void mGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        BindGrid();
    }

    private void BindGrid()
    {
        string pegIDs, peIDs, gWhere;
        GetGroupWhere(out pegIDs, out peIDs, out gWhere);

        string query = @"SELECT ID, Personnel.Code, Personnel.Name, Personnel.Surname, Personnel.HomeAddress, HomeTelNum
, CellNumber1, Email
, CASE WHEN CHARINDEX('-',Pidentify,2) > 0 THEN SUBSTRING(PIdentify,2,CHARINDEX('-',PIdentify,2)-2) ELSE NULL END FIdentify
, CASE WHEN CHARINDEX('-',Pidentify,2) > 0 THEN reverse(SUBSTRING(reverse(PIdentify),1,CHARINDEX('-',reverse(PIdentify),2)-1)) ELSE NULL END SIdentify
, Latittude
  FROM Personnel";


        string where = " WHERE 1 = 1";

        if ((!string.IsNullOrEmpty(pegIDs) || !string.IsNullOrEmpty(peIDs)) && !string.IsNullOrEmpty(gWhere))
        {
            // ------------------------
            // ids: 1,2,5
            // where: '&1%' AND ('&2%' OR '&5%')
            // it could be better, but i dont know regex well :(
            // ------------------------
            where += "\r\nAND (\r\n";
            if (!string.IsNullOrEmpty(pegIDs))
            {
                // personnel groups: '&1%', '!&1%'
                string[] pGroupingIDs = pegIDs.Split(',');
                for (int i = 0; i < pGroupingIDs.Length; i++)
                {
                    gWhere = gWhere.Replace("'$" + pGroupingIDs[i] + "%'",
                        " ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (SELECT ID FROM [GetAllGroupingChilds](" + pGroupingIDs[i] + "))) \r\n");
                    gWhere = gWhere.Replace("'!$" + pGroupingIDs[i] + "%'",
                        " NOT ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (SELECT ID FROM [GetAllGroupingChilds](" + pGroupingIDs[i] + "))) \r\n");
                }
            }

            if (!string.IsNullOrEmpty(peIDs))
            {
                // personnel groups: '&1%', '!&1%'
                string[] pgids = peIDs.Split(',');
                for (int i = 0; i < pgids.Length; i++)
                {
                    gWhere = gWhere.Replace("'&" + pgids[i] + "%'",
                        "Personnel.PersonnelGroupID = " + pgids[i] + "\r\n");
                    gWhere = gWhere.Replace("'!&" + pgids[i] + "%'",
                        "NOT Personnel.PersonnelGroupID = " + pgids[i] + "\r\n");
                }
            }

            where += gWhere;
            where += "\r\n)\r\n";
        }

        query += where;

        if (!string.IsNullOrEmpty(SortCol))
            query += " ORDER BY " + SortCol + " " + SortOrder;


        DataTable items = DbProxy.Instance.FillData(query, "all", GetDBs(), string.IsNullOrEmpty(SortCol) ? "" : (SortCol + " " + SortOrder));
        try
        {
            mGrid.DataSource = items;
            mGrid.DataBind();
        }
        finally
        {
            if (null != items)
                items.Dispose();
        }
    }

    protected string SortCol
    {
        get { return Helper.GetString(ViewState["SortCol"], null); }
        set { ViewState["SortCol"] = value; }
    }

    protected string SortOrder
    {
        get { return Helper.GetString(ViewState["SortOrder"], "asc"); }
        set { ViewState["SortOrder"] = value; }
    }

    #endregion

    // ------------------------
    // ids: 1,2,5
    // where: '&1%' AND ('&2%' OR '&5%')
    // ------------------------
    private void GetGroupWhere(out string pgIDs, out string pIDs, out string where)
    {
        pIDs = "";
        pgIDs = "";
        where = "";

        // grouping members : '&1%', '!&1%'
        // category = 1375 AND ( category != 1076 OR category = 1075 ) 
        // --> '&1375%' AND ('!&1076%' OR '&1075%')
        where = mQueryBuilderSql.Value.Trim();
        where += " "; // we need this space at end to find IDs
       

        while (where.Contains("PGCategory = "))
        {
            int index = where.IndexOf("PGCategory = ") + "PGCategory = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pgIDs += id + ",";
            where = where.Replace("PGCategory = " + id + " ", "'$" + id + "%' ");
        }
        while (where.Contains("PGCategory != "))
        {
            int index = where.IndexOf("PGCategory != ") + "PGCategory != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pgIDs += id + ",";
            where = where.Replace("PGCategory != " + id + " ", "'!$" + id + "%' ");
        }
        if (pgIDs.EndsWith(","))
            pgIDs = pgIDs.Substring(0, pgIDs.Length - ",".Length);

        
        while (where.Contains("PCategory = "))
        {
            int index = where.IndexOf("PCategory = ") + "PCategory = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pIDs += id + ",";
            where = where.Replace("PCategory = " + id + " ", "'&" + id + "%' ");
        }
        while (where.Contains("PCategory != "))
        {
            int index = where.IndexOf("PCategory != ") + "PCategory != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pIDs += id + ",";
            where = where.Replace("PCategory != " + id + " ", "'!&" + id + "%' ");
        }
        if (pIDs.EndsWith(","))
            pIDs = pIDs.Substring(0, pIDs.Length - ",".Length);
    }

    protected void doExcel_Click(object sender, EventArgs e)
    {
        Exporter.ExportExcel(Response
            , new List<string>(new string[] { mGridTitle.Text})
            , new List<GridView>(new GridView[] { mGrid}));
    }
}