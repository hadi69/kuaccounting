﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;

public partial class reports_accounting_CommodityBuyReport : System.Web.UI.Page
{
    private System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();

    protected void Page_Load(object sender, EventArgs e)
    {
        msg.Visible = false;
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");

        mGrid.EmptyDataText = LangProvider.Instance["ListIsEmpty"];

        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["CommodityBuyReport"];
            Page.SetLang();
            IDataReader financeYearStartDate =
                DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");
            //0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            //mFromDate.Text = 
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);
            Initial();

           // mReportSetting.ReportName = "CommoditySaleReport";
        }

    }

    //private void mReportSetting_ReportSettingSelect(object sender, EventArgs e)
    //{
    //    ReportSettingInfo info = mReportSetting.SelectedReportSettings;
    //    if (info == null)
    //        return;
    //    string[] lines = info.Settings.Split('\n');
    //    mPersons.SelectedValue = lines[0].Trim('\r');
    //    mFromDate.Text = lines[2].Trim('\r');
    //    mToDate.Text = lines[3].Trim('\r');
    //}

    //private void mReportSetting_SaveClick(object sender, EventArgs e)
    //{
    //    StringBuilder b = new StringBuilder();
    //    b.AppendLine(mPersons.SelectedValue); //0
    //    b.AppendLine(mFromDate.Text); //1
    //    b.AppendLine(mToDate.Text); //2
    //    mReportSetting.Save(b.ToString());
    //}

    private void Initial()
    {
            mPersons.Items.Clear();
            mCommodities.Items.Clear();
            mProjectLabels.Items.Clear();
            mEquipmentLabels.Items.Clear();
            mPersonLabels.Items.Clear();

            mPersons.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
            mCommodities.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
            mProjectLabels.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
            mEquipmentLabels.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
            mPersonLabels.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
            mCommodityGroupings.Items.Clear();
            //IDataReader commodityGroupings = DbProxy.Instance.ExecuteReader("SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping  WHERE GroupingType = 1");
            //if (commodityGroupings != null)
            //{
            //    while (commodityGroupings.Read())
            //    {
            //        if (!commodityGroupings.IsDBNull(0) && !commodityGroupings.IsDBNull(1))
            //            mCommodityGroupings.Items.Add(new ListItem(commodityGroupings.GetString(1),
            //                commodityGroupings.GetInt32(0).ToString()));
            //    }
            //    commodityGroupings.Close();
            //}
            //mPersonGroupings.Items.Clear();
            //IDataReader personGroupings = DbProxy.Instance.ExecuteReader("SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping  WHERE GroupingType = 0");
            //if (personGroupings != null)
            //{
            //    while (personGroupings.Read())
            //    {
            //        if (!personGroupings.IsDBNull(0) && !personGroupings.IsDBNull(1))
            //            mPersonGroupings.Items.Add(new ListItem(personGroupings.GetString(1),
            //                personGroupings.GetInt32(0).ToString()));
            //    }
            //    personGroupings.Close();
            //}
            IDataReader persons = DbProxy.Instance.ExecuteReader("SELECT ID, ISNULL([dbo].[DisplayPerson](ID, 1), ''), ISNULL(Code, '') FROM Personnel WHERE Enabled = 1 AND Type=101");
            if (persons != null)
            {
                while (persons.Read())
                {
                    if (!persons.IsDBNull(0) && !persons.IsDBNull(1) && !persons.IsDBNull(2))
                        mPersons.Items.Add(new ListItem(persons.GetString(1) + " - کد: " + persons.GetString(2), persons.GetInt32(0).ToString()));
                }
                persons.Close();
            }
            mPersons.SelectedIndex = 0;
        
        IDataReader PersonGroups = DbProxy.Instance.ExecuteReader("SELECT ID, Title FROM PersonnelGroup");
        mPersonGroups.Items.Clear();
        mPersonGroups.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
        if (PersonGroups != null)
        {
            while (PersonGroups.Read())
                while (PersonGroups.Read())
                {
                    if (!PersonGroups.IsDBNull(0) && !PersonGroups.IsDBNull(1))
                        mPersonGroups.Items.Add(new ListItem(PersonGroups.GetString(1), PersonGroups.GetInt32(0).ToString()));
                }
            PersonGroups.Close();
        }
        mPersonGroups.SelectedIndex = 0;

        IDataReader commodities = DbProxy.Instance.ExecuteReader("SELECT ID, ServiceFullName FROM ServicePriceView ORDER BY ServiceFullName");
            if (commodities != null)
            {
                while (commodities.Read())
                {
                    if (!commodities.IsDBNull(0) && !commodities.IsDBNull(1))
                        mCommodities.Items.Add(new ListItem(commodities.IsDBNull(1) ? "" : commodities.GetString(1), commodities.GetInt32(0).ToString()));
                }
                commodities.Close();
            }
            mCommodities.SelectedIndex = 0;

            IDataReader serviceTypes = DbProxy.Instance.ExecuteReader("SELECT ID, FullName FROM ServiceTypeView");
            mServiceTypes.Items.Clear();
            mServiceTypes.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
            if (serviceTypes != null)
            {
                while (serviceTypes.Read())
                {
                    if (!serviceTypes.IsDBNull(0) && !serviceTypes.IsDBNull(1))
                        mServiceTypes.Items.Add(new ListItem(serviceTypes.GetString(1), serviceTypes.GetInt32(0).ToString()));
                }
                serviceTypes.Close();
            }
            mServiceTypes.SelectedIndex = 0;

            //    IDataReader equipmentLabels = DbProxy.Instance.ExecuteReader("SELECT ID, (ISNULL(Code, '') +' - '+ FullName) FROM EquipmentView WHERE NOT ID IN (SELECT ISNULL(ParentID, 0) FROM Equipment)");
            //    if (equipmentLabels != null)
            //    {
            //        while (equipmentLabels.Read())
            //        {
            //            if (!equipmentLabels.IsDBNull(0) && !equipmentLabels.IsDBNull(1))
            //                mEquipmentLabels.Items.Add(new ListItem(equipmentLabels.IsDBNull(1) ? "" : equipmentLabels.GetString(1), equipmentLabels.GetInt32(0).ToString()));
            //        }
            //        equipmentLabels.Close();
            //    }
            //    mEquipmentLabels.SelectedIndex = 0;
            //    IDataReader projectLabels = DbProxy.Instance.ExecuteReader("SELECT ID, (ISNULL(Code, '') +' - '+ FullName) FROM WardView WHERE NOT ID IN (SELECT ISNULL(ParentID, 0) FROM Ward)");
            //    if (projectLabels != null)
            //    {
            //        while (projectLabels.Read())
            //        {
            //            if (!projectLabels.IsDBNull(0) && !projectLabels.IsDBNull(1))
            //                mProjectLabels.Items.Add(new ListItem(projectLabels.IsDBNull(1) ? "" : projectLabels.GetString(1), projectLabels.GetInt32(0).ToString()));
            //        }
            //        projectLabels.Close();
            //    }
            //    mProjectLabels.SelectedIndex = 0;
            //    IDataReader personLabels = DbProxy.Instance.ExecuteReader("SELECT ID, [dbo].[DisplayPerson](ID, 1), ISNULL(Code, '') FROM Personnel WHERE Type=101");
            //    if (personLabels != null)
            //    {
            //        while (personLabels.Read())
            //        {
            //            if (!personLabels.IsDBNull(0) && !personLabels.IsDBNull(1))
            //                mPersonLabels.Items.Add(new ListItem(personLabels.IsDBNull(1) ? "" : personLabels.GetString(1), personLabels.GetInt32(0).ToString()));
            //        }
            //        personLabels.Close();
            //    }
            //    mPersonLabels.SelectedIndex = 0;
            //}

            //IDataReader commodityGroup = DbProxy.Instance.ExecuteReader("SELECT ID, FullName FROM ServiceTypeView");
            //IDataReader personGroup = DbProxy.Instance.ExecuteReader("SELECT ID, Title FROM PersonnelGroup");
            //mCommodityGroup.Items.Clear();
            //if (commodityGroup != null)
            //{
            //    while (commodityGroup.Read())
            //    {
            //        if (!commodityGroup.IsDBNull(0) && !commodityGroup.IsDBNull(1))
            //            mCommodityGroup.Items.Add(new ListItem(commodityGroup.GetString(1),
            //                commodityGroup.GetInt32(0).ToString()));
            //    }
            //    commodityGroup.Close();
            //}
            //mPersonGroup.Items.Clear();
            //if (personGroup != null)
            //{
            //    while (personGroup.Read())
            //    {
            //        if (!personGroup.IsDBNull(0) && !personGroup.IsDBNull(1))
            //            mPersonGroup.Items.Add(new ListItem(personGroup.GetString(1), personGroup.GetInt32(0).ToString()));
            //    }
            //    personGroup.Close();
            //}
        }

    //protected void DlBaseChange(object sender, EventArgs e)
    //{
    //    DropDownList comboBox = (DropDownList)sender;
    //    int selected = Int32.Parse(comboBox.SelectedValue);
    //    if (selected == 1)
    //    {
    //        personLable.Text = LangProvider.Instance["ActorName"];
    //        panelDropDowns.Visible = panelInternal.Visible = true;
    //        //  mActorName.Visible = ActorNameLabel.Visible = mTransferee.Visible = TransfereeLabel.Visible = true;
    //    }
    //    else
    //    {
    //        personLable.Text = LangProvider.Instance["Person"];
    //        panelDropDowns.Visible = panelInternal.Visible = false;
    //        //mActorName.Visible = ActorNameLabel.Visible = mTransferee.Visible = TransfereeLabel.Visible = false;
    //    }
    //}

    protected void doShow_Click(object sender, EventArgs e)
    {
        doExcel.Enabled = true;
        BindGrid();
        //if (Helper.GetInt(mEquipmentLabels.SelectedValue, Null.NullInteger) != Null.NullInteger && !factors.Checked)
        //{
        //    LabelVoucherDetail.Visible = true;
        //    BindLabelVoucherDetailGrid();
        //}
        //else
        //{
        //    LabelVoucherDetail.Visible = false;
        //}
    }

    #region Grid

    private decimal _sumTotalTotalPrice, _sumTotalPrice, _sumTotalTotal, _sumTotalDiscount, _sumTotalTax;
    private float _sumTotalAmount;
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.Common.DbDataRecord record = e.Row.DataItem as System.Data.Common.DbDataRecord;
            DateTime dt = DateTime.Now;
            dt = Helper.GetDateTime(record["ReceiptDate"], Helper.NullDate);
            Label lblGridDate = (Label)e.Row.FindControl("lblGridDate");
            DateTime dtR = Helper.GetDateTime(record["Date"], Helper.NullDate);
            Label lblGridRecieptDate = (Label)e.Row.FindControl("lblGridRecieptDate");
            if (dt != Helper.NullDate && lblGridDate != null)
            {
                lblGridDate.Text = Helper.FormatDate(Helper.GetDateTime(dt, Helper.NullDate), null);
            }
            if (dtR != Helper.NullDate && lblGridRecieptDate != null)
            {
                lblGridRecieptDate.Text = Helper.FormatDate(Helper.GetDateTime(dtR, Helper.NullDate), null);
            }
            e.Row.Cells[1].Text = string.Format("<a href='../../list.aspx?table=Purchase&id={0}&menu=accountinga'>{1}</a>", record["ID"], record["NOSerial"]);
            decimal totalPrice = SiteSettings.ToMonetary(Helper.GetDecimal(record["Price"], 0), dt);
            e.Row.Cells[16].Text = totalPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            decimal totalTotal = SiteSettings.ToMonetary(Helper.GetDecimal(record["Total"], 0), dt);
            e.Row.Cells[13].Text = totalTotal.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            decimal totalDiscount = SiteSettings.ToMonetary(Helper.GetDecimal(record["TotalDiscount"], 0), dt);
            e.Row.Cells[14].Text = totalDiscount.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            decimal totalTax = SiteSettings.ToMonetary(Helper.GetDecimal(record["Tax"], 0), dt);
            e.Row.Cells[15].Text = totalTax.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            decimal totalTotalPrice = SiteSettings.ToMonetary(Helper.GetDecimal(record["TotalPrice"], 0), dt);
            e.Row.Cells[17].Text = totalTotalPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            _sumTotalAmount += Helper.GetFloat(record["TotalAmount"], 0);
            _sumTotalPrice += totalPrice;
            _sumTotalTotalPrice += totalTotalPrice;
            _sumTotalTotal += totalTotal;
            _sumTotalDiscount += totalDiscount;
            _sumTotalTax += totalTax;

        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            (e.Row.FindControl("totalPrice") as Label).Text = _sumTotalPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("totalTotalPrice") as Label).Text = _sumTotalTotalPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("totalTotal") as Label).Text = _sumTotalTotal.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("totalTotalDiscount") as Label).Text = _sumTotalDiscount.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("totalTax") as Label).Text = _sumTotalTax.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("totalTotalAmount") as Label).Text = _sumTotalAmount.ToString();
        }
    }

    string rowStyle = "GridAltRow";
    decimal sumOwe = 0, sumDue = 0, _due = 0, _owe = 0, _remain = 0;
    protected void labelVoucherDetailGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.Common.DbDataRecord record = e.Row.DataItem as System.Data.Common.DbDataRecord;
            DateTime dt = Helper.GetDateTime(record["Date"], Helper.NullDate);
            Label lblGridDate = (Label)e.Row.FindControl("lblGridDate");
            if (dt != Helper.NullDate && lblGridDate != null)
            {
                lblGridDate.Text = Helper.ToFaFormat(dt);
            }
            e.Row.Cells[1].Text = string.Format("<a href='../../list.aspx?table=DocumentAccounting&id={0}&itemid={1}'>{2}</a>", record["ID"], record["ItemID"], record["No"]);

            _due = SiteSettings.ToMonetary(Helper.GetDecimal(record["Due"], 0), dt);
            _owe = SiteSettings.ToMonetary(Helper.GetDecimal(record["Owe"], 0), dt);

            _remain += _due - _owe;

            sumDue += _due;
            sumOwe += _owe;

            e.Row.CssClass = rowStyle;

            ((Label)e.Row.FindControl("owe")).Text = SiteSettings.ToMonetary(_owe, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("due")).Text = SiteSettings.ToMonetary(_due, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            decimal _r = SiteSettings.ToMonetary(_remain, DateTime.Now);
            ((Label)e.Row.FindControl("remaining")).Text = (_r < 0) ? (-1 * _r).ToString("N4", Helper.NumberCulture).Replace(".0000", "") : _r.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.Cells[10].Text = (_r < 0) ? LangProvider.Instance["Li"] : LangProvider.Instance["Cr"];

            string no = Helper.GetString(record["No"], "");
            if (no == "0")
            {
                e.Row.Cells[1].Text = e.Row.Cells[2].Text = e.Row.Cells[3].Text 
                    = e.Row.Cells[4].Text = e.Row.Cells[5].Text = e.Row.Cells[6].Text = LangProvider.Instance["Buy"];
            }
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblTotal = (Label)e.Row.FindControl("oweTotal");
            lblTotal.Text = sumOwe.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            lblTotal = (Label)e.Row.FindControl("dueTotal");
            lblTotal.Text = SiteSettings.ToMonetary(sumDue, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            lblTotal = (Label)e.Row.FindControl("remainingTotal");
            decimal s = SiteSettings.ToMonetary(sumDue - sumOwe, DateTime.Now);
            lblTotal.Text = (s < 0) ? (-1 * s).ToString("N4", Helper.NumberCulture).Replace(".0000", "") : s.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            lblTotal = (Label)e.Row.FindControl("finalDiagnosis");
            lblTotal.Text = ((sumDue - sumOwe) < 0) ? LangProvider.Instance["Li"] : LangProvider.Instance["Cr"];
        }
    }

    protected void mGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        BindGrid();
    }

    protected void mlabelVoucherDetailGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        BindLabelVoucherDetailGrid();
    }

    private void BindGrid()
    {
        DateTime start = Helper.ParseDate(mFromDate.Text, Null.NullDate);
        DateTime end = Helper.ParseDate(mToDate.Text, Null.NullDate);
        if (Null.IsNull(start))
        {
            msg.Text = LangProvider.Instance["InvalidStartDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(end))
        {
            msg.Text = LangProvider.Instance["InvalidEndDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(start) || Null.IsNull(end))
            return;
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);
        gridPanel.Visible = true;
        string cogIDs, pegIDs, coIDs, peIDs, gWhere;
        GetGroupWhere(out cogIDs, out pegIDs, out coIDs, out peIDs, out gWhere);

        int personId = Helper.GetInt(mPersons.SelectedValue, Null.NullInteger);
        int commodityId = Helper.GetInt(mCommodities.SelectedValue, Null.NullInteger);
        int personGroupId = Helper.GetInt(mPersonGroups.SelectedValue, Null.NullInteger);
        int serviceTypeId = Helper.GetInt(mServiceTypes.SelectedValue, Null.NullInteger);
        //int personIdL = Helper.GetInt(mPersonLabels.SelectedValue, Null.NullInteger);
        string comments = mComments.Text.Trim();
        string query = "";

        #region purchase

        if (Detail.Checked)
        {
            mGrid.Columns[6].Visible = mGrid.Columns[7].Visible = mGrid.Columns[13].Visible = mGrid.Columns[14].Visible
            = mGrid.Columns[15].Visible = false;
            mGrid.Columns[1].Visible = mGrid.Columns[2].Visible = mGrid.Columns[3].Visible = mGrid.Columns[4].Visible = mGrid.Columns[8].Visible = mGrid.Columns[9].Visible
             = mGrid.Columns[10].Visible = mGrid.Columns[11].Visible = mGrid.Columns[12].Visible = mGrid.Columns[16].Visible = mGrid.Columns[5].Visible = mGrid.Columns[18].Visible = true;

            query = string.Format(@"SELECT PurchaseView.ID , NOSerial, ReceiptDate, PurchaseView.Date, SellerID, SellerName, PurchaseView.Comments,
                    StoreServiceID, StoreServiceName, PurchaseItemsView.UnitName
                    , ServiceView.Code, NULL Total, NULL, NULL TotalDiscount, NULL Tax, 
                    UnitCurrencyConverter
                    , Amount,(ISNULL(UnitCurrencyConverter, 1) * ISNULL(Amount, 0)) TotalAmount
                    , Price, (ISNULL(UnitCurrencyConverter, 1) * ISNULL(Amount, 0) * ISNULL(Price, 0)) AS TotalPrice
                    , PurchaseItemsView.EquipmentLabel, PurchaseItemsView.Comment
                    FROM PurchaseView
                    LEFT OUTER JOIN PurchaseItemsView ON PurchaseView.ID = PurchaseItemsView.PurchaseID
                    LEFT OUTER JOIN StoreServices ON PurchaseItemsView.StoreServiceID = StoreServices.ID
                    LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                    LEFT OUTER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID
                    LEFT OUTER JOIN ServiceView ON ServicePrice.ServiceID = ServiceView.ID
                    LEFT OUTER JOIN Personnel ON PurchaseView.SellerID = Personnel.ID");
        }
        else if (CommoditySum.Checked)
        {
            mGrid.Columns[1].Visible = mGrid.Columns[2].Visible = mGrid.Columns[3].Visible = mGrid.Columns[4].Visible = mGrid.Columns[6].Visible
            = mGrid.Columns[10].Visible = mGrid.Columns[11].Visible = mGrid.Columns[13].Visible = mGrid.Columns[14].Visible
            = mGrid.Columns[15].Visible = mGrid.Columns[16].Visible = mGrid.Columns[18].Visible = false;
            mGrid.Columns[7].Visible = mGrid.Columns[8].Visible = mGrid.Columns[9].Visible = mGrid.Columns[12].Visible = mGrid.Columns[5].Visible = true;

            query = string.Format(@"
                    SELECT NULL ID , NULL NOSerial, NULL ReceiptDate, NULL Date, NULL SellerID, NULL SellerName, NULL Comments,
                    StoreServiceID, StoreServiceName, ServiceView.UnitName, ServiceView.Code, NULL Total, NULL, NULL TotalDiscount, NULL Tax,
                    NULL UnitCurrencyConverter, NULL Amount
                    , SUM(ISNULL(UnitCurrencyConverter, 1) * ISNULL(Amount, 0)) AS TotalAmount
                    , NULL Price, SUM(ISNULL(UnitCurrencyConverter, 1) * ISNULL(Amount, 0) * ISNULL(Price, 0)) AS TotalPrice
                    , PurchaseItemsView.EquipmentLabel
                    FROM PurchaseView
                    LEFT OUTER JOIN PurchaseItemsView ON PurchaseView.ID = PurchaseItemsView.PurchaseID
                    LEFT OUTER JOIN StoreServices ON PurchaseItemsView.StoreServiceID = StoreServices.ID
                    LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                    LEFT OUTER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID
                    LEFT OUTER JOIN ServiceView ON ServicePrice.ServiceID = ServiceView.ID
                    LEFT OUTER JOIN Personnel ON PurchaseView.SellerID = Personnel.ID");
        }
        else if (factors.Checked)
        {
            mGrid.Columns[7].Visible = mGrid.Columns[8].Visible = mGrid.Columns[9].Visible = mGrid.Columns[10].Visible
           = mGrid.Columns[11].Visible = mGrid.Columns[12].Visible = mGrid.Columns[13].Visible = mGrid.Columns[5].Visible = mGrid.Columns[18].Visible = mGrid.Columns[16].Visible = false;
            mGrid.Columns[1].Visible = mGrid.Columns[2].Visible = mGrid.Columns[3].Visible = mGrid.Columns[4].Visible = mGrid.Columns[6].Visible = mGrid.Columns[13].Visible
            = mGrid.Columns[14].Visible = mGrid.Columns[15].Visible = true;

            query = string.Format(@"
                    SELECT PurchaseView.ID , PurchaseView.NOSerial, PurchaseView.ReceiptDate , PurchaseView.Date, PurchaseView.SellerID, PurchaseView.SellerName
                    , PurchaseView.Comments,
                    NULL StoreServiceID, NULL StoreServiceName, NULL UnitName, NULL Code
                    , PurchaseView.TotalPrice AS Total
                    ,PurchaseView.TotalDiscount, (ISNULL(PurchaseView.TotalTax, 0) + ISNULL(PurchaseView.VAT, 0)) AS Tax
                    ,NULL UnitCurrencyConverter, NULL DeliveredAmount, NULL TotalAmount, NULL Price
                    ,((ISNULL(PurchaseView.TotalPrice, 0)) - (ISNULL(PurchaseView.TotalDiscount, 0) + ISNULL(PurchaseView.TotalTax, 0) + ISNULL(PurchaseView.VAT, 0))) AS TotalPrice FROM PurchaseView
                    LEFT OUTER JOIN Personnel ON PurchaseView.SellerID = Personnel.ID");
        }
        string where = "";
        if (mUseBillDate.Checked)
        {
            where =string.Format(
                    " WHERE (PurchaseView.PurchaseType = 0 OR PurchaseView.PurchaseType IS NULL) AND PurchaseView.Date>={0} AND PurchaseView.Date<={1}",
                    Helper.QoutedDateTime(start),
                    Helper.QoutedDateTime(end));
        }
        else
        {
            where =string.Format(
                    " WHERE (PurchaseView.PurchaseType = 0 OR PurchaseView.PurchaseType IS NULL) AND ReceiptDate>={0} AND ReceiptDate<={1}",
                    Helper.QoutedDateTime(start),
                    Helper.QoutedDateTime(end));
        }

        if (commodityId != Null.NullInteger)
        {
            if (factors.Checked)
            {
                where += @" AND PurchaseView.ID IN (
                        SELECT DISTINCT PurchaseID FROM PurchaseItems 
                        LEFT OUTER JOIN StoreServices ON PurchaseItems.StoreServiceID = StoreServices.ID
                        LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                        WHERE StoreServiceDef.ServicePriceID =" + commodityId + ")";
            }
            else
            {
                where += "AND ServicePrice.ID = " + commodityId;
            }
        }

        if (personGroupId != Null.NullInteger)
        {
                where += " AND Personnel.PersonnelGroupID = " + personGroupId;
        }

        if (serviceTypeId != Null.NullInteger)
        {
            if (factors.Checked)
            {
                where += @" AND PurchaseView.ID IN (
                        SELECT DISTINCT PurchaseID FROM PurchaseItems 
                        LEFT OUTER JOIN StoreServices ON PurchaseItems.StoreServiceID = StoreServices.ID
                        LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                        LEFT OUTER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID
                        LEFT OUTER JOIN Service ON ServicePrice.ServiceID = Service.ID
                        WHERE Service.ServiceTypeID IN (SELECT ID FROM [GetAllServiceTypeChilds]( " + serviceTypeId + ")))";
            }
            else
            {
                where += " AND ServiceView.ServiceTypeID IN (SELECT ID FROM [GetAllServiceTypeChilds]( " + serviceTypeId + "))";
            }
        }

        //if (personIdL != Null.NullInteger)
        //{
        //    if (factors.Checked)
        //    {
        //        where += @" AND PurchaseView.ID IN (
        //                SELECT DISTINCT PurchaseID FROM PurchaseItemsView 
        //                WHERE PurchaseItemsView.PersonLabelID =" + personIdL + ")";
        //    }
        //    else
        //    {
        //        where += " AND PurchaseItemsView.PersonLabelID = " + personIdL;
        //    }
        //}

        if (personId != Null.NullInteger)
        {
            where += "AND SellerID = " + personId;
        }
        if (comments.Length > 0)
            where += " AND PurchaseView.Comments LIKE " + Helper.Likenize(comments);

        if ((!string.IsNullOrEmpty(cogIDs) || !string.IsNullOrEmpty(pegIDs) || !string.IsNullOrEmpty(peIDs) || !string.IsNullOrEmpty(coIDs)) && !string.IsNullOrEmpty(gWhere))
        {
            // ------------------------
            // ids: 1,2,5
            // where: '&1%' AND ('&2%' OR '&5%')
            // it could be better, but i dont know regex well :(
            // ------------------------
            where += "\r\nAND (\r\n";
            if (!string.IsNullOrEmpty(cogIDs))
            {
                //  groupings: '*1%', '!*1%'
                string[] cGroupingIDs = cogIDs.Split(',');
                for (int i = 0; i < cGroupingIDs.Length; i++)
                {
                    StringBuilder allIds = new StringBuilder();
                    int j = 0;
                    IDataReader childIds = DbProxy.Instance.ExecuteReader("SELECT ID FROM [GetAllGroupingChilds](" + cGroupingIDs[i] + ")");
                    while (childIds.Read())
                    {
                        if (j != 0)
                        {
                            allIds.Append(",");
                        }
                        allIds.Append(childIds.GetValue(0));
                        j++;
                    }
                    if (factors.Checked)
                    {
                        gWhere = gWhere.Replace("'*" + cGroupingIDs[i] + "%'",
                        @" PurchaseView.ID IN (
                        SELECT DISTINCT PurchaseID FROM PurchaseItems 
                        LEFT OUTER JOIN StoreServices ON PurchaseItems.StoreServiceID = StoreServices.ID
                        LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                        LEFT OUTER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID
                        WHERE ServicePrice.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + "))) \r\n");
                        gWhere = gWhere.Replace("'!*" + cGroupingIDs[i] + "%'",
                        @" PurchaseView.ID IN (
                        SELECT DISTINCT PurchaseID FROM PurchaseItems 
                        LEFT OUTER JOIN StoreServices ON PurchaseItems.StoreServiceID = StoreServices.ID
                        LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                        LEFT OUTER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID             
                        WHERE NOT ServicePrice.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + "))) \r\n");
                    }
                    else
                    {
                        gWhere = gWhere.Replace("'*" + cGroupingIDs[i] + "%'",
                        " ServicePrice.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + ")) \r\n");
                        gWhere = gWhere.Replace("'!*" + cGroupingIDs[i] + "%'",
                            " NOT ServicePrice.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + ")) \r\n");
                    }
                }
            }
            if (!string.IsNullOrEmpty(pegIDs))
            {
                // personnel groups: '&1%', '!&1%'
                string[] pGroupingIDs = pegIDs.Split(',');
                for (int i = 0; i < pGroupingIDs.Length; i++)
                {
                    StringBuilder allIds = new StringBuilder();
                    int j = 0;
                    IDataReader childIds = DbProxy.Instance.ExecuteReader("SELECT ID FROM [GetAllGroupingChilds](" + pGroupingIDs[i] + ")");
                    while (childIds.Read())
                    {
                        if (j != 0)
                        {
                            allIds.Append(",");
                        }
                        allIds.Append(childIds.GetValue(0));
                        j++;
                    }
                    gWhere = gWhere.Replace("'$" + pGroupingIDs[i] + "%'",
                        "Personnel.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + ")) \r\n");
                    gWhere = gWhere.Replace("'!$" + pGroupingIDs[i] + "%'",
                        " NOT Personnel.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + ")) \r\n");
                }
            }

            if (!string.IsNullOrEmpty(peIDs))
            {
                // personnel groups: '&1%', '!&1%'
                string[] pgids = peIDs.Split(',');
                for (int i = 0; i < pgids.Length; i++)
                {
                    gWhere = gWhere.Replace("'&" + pgids[i] + "%'",
                        "Personnel.PersonnelGroupID IN (" + pgids[i] + ") \r\n");
                    gWhere = gWhere.Replace("'!&" + pgids[i] + "%'",
                        " NOT Personnel.PersonnelGroupID IN (" + pgids[i] + ") \r\n");
                }
            }

            if (!string.IsNullOrEmpty(coIDs))
            {
                // ServiceType: '^1%', '!^1%'
                string[] ids = coIDs.Split(',');
                for (int i = 0; i < ids.Length; i++)
                {
                    StringBuilder allIds = new StringBuilder();
                    int j = 0;
                    IDataReader childIds = DbProxy.Instance.ExecuteReader("SELECT ID FROM [GetAllServiceTypeChilds](" + ids[i] + ")");
                    while (childIds.Read())
                    {
                        if (j != 0)
                        {
                            allIds.Append(",");
                        }
                        allIds.Append(childIds.GetValue(0));
                        j++;
                    }
                    if (factors.Checked)
                    {
                        gWhere = gWhere.Replace("'^" + ids[i] + "%'",
                        @" PurchaseView.ID IN (
                        SELECT DISTINCT PurchaseID FROM PurchaseItems 
                        LEFT OUTER JOIN StoreServices ON PurchaseItems.StoreServiceID = StoreServices.ID
                        LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                        LEFT OUTER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID
                        LEFT OUTER JOIN Service ON ServicePrice.ServiceID = Service.ID
                        WHERE Service.ServiceTypeID IN (" + allIds + ")) \r\n");
                        gWhere = gWhere.Replace("'!^" + ids[i] + "%'",
                        @" PurchaseView.ID IN (
                        SELECT DISTINCT PurchaseID FROM PurchaseItems 
                        LEFT OUTER JOIN StoreServices ON PurchaseItems.StoreServiceID = StoreServices.ID
                        LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                        LEFT OUTER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID
                        LEFT OUTER JOIN Service ON ServicePrice.ServiceID = Service.ID
                        WHERE NOT Service.ServiceTypeID IN (" + allIds + ")) \r\n");
                    }
                    else
                    {
                        gWhere = gWhere.Replace("'^" + ids[i] + "%'",
                        " ServiceView.ServiceTypeID IN (" + allIds + ") \r\n");
                        gWhere = gWhere.Replace("'!^" + ids[i] + "%'",
                            " NOT ServiceView.ServiceTypeID IN (" + allIds + ") \r\n");
                    }
                }

            }

            where += gWhere;
            where += "\r\n)\r\n";
        }
        query += where;
        if (CommoditySum.Checked)
        {
            query += " GROUP BY StoreServiceID, StoreServiceName, ServiceView.Code, ServiceView.UnitName, PurchaseItemsView.EquipmentLabel ";
        }
        if (!string.IsNullOrEmpty(SortCol))
            query += " ORDER BY " + SortCol + " " + SortOrder;
        if (!CommoditySum.Checked && string.IsNullOrEmpty(SortCol))
        {
            query += " ORDER BY ReceiptDate, SellerID, DATEADD(dd, 0, DATEDIFF(dd, 0, ReceiptDate)), NoSerial, ID";
        }

        #endregion
        IDataReader items = DbProxy.Instance.ExecuteReader(query);
        try
        {
            mGrid.DataSource = items;
            mGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Close();
                items.Dispose();
            }
        }
    }

    private void BindLabelVoucherDetailGrid()
    {
        DateTime start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        DateTime end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);

        int equipmentId = Helper.GetInt(mEquipmentLabels.SelectedValue, Null.NullInteger);
        string query = "";

        #region labelVouchers

        query = string.Format(@"
                    SELECT NULL ID, NULL ItemID
                    , 0 [No], NULL [Date]
                    , NULL TreePathName
                    , NULL TopicCode
                    , NULL EquipmentLabel, NULL Comments
                    ,  {0} Owe, 0 Due
                    FROM DocumentAccountingItems
                    UNION
                    SELECT DocumentAccountingItems.ID, DocumentAccountingItems.ID AS ItemID
                    , DocumentAccounting.[No], DocumentAccounting.[Date]
                    , dbo.TreePathToTopicTitle(DocumentAccountingItems.TreePath) AS TreePathName
                    , dbo.TreePathToTopicCode(DocumentAccountingItems.TreePath) AS TopicCode
                    , EquipmentView.FullName AS EquipmentLabel, DocumentAccountingItems.Comments
                    , ISNULL(DocumentAccountingItems.Owe, 0) AS Owe, ISNULL(DocumentAccountingItems.Due, 0) AS Due
                    FROM DocumentAccountingItems
                    LEFT OUTER JOIN DocumentAccounting ON DocumentAccountingItems.DocumentAccountingID = DocumentAccounting.ID
                    LEFT OUTER JOIN EquipmentView ON DocumentAccountingItems.EquipmentID = EquipmentView.ID", Helper.GetDecimal(_sumTotalTotalPrice, 0));
        string where = string.Format(" WHERE DocumentAccounting.[Date] >={0} AND DocumentAccounting.[Date] <= {1} AND DocumentAccountingItems.EquipmentID = {2}", Helper.QoutedDateTime(start),
                Helper.QoutedDateTime(end), equipmentId);

        query += where;

        #endregion
        IDataReader items = DbProxy.Instance.ExecuteReader(query);
        try
        {
            mlabelVoucherDetailGrid.DataSource = items;
            mlabelVoucherDetailGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Close();
                items.Dispose();
            }
        }
    }

    protected string SortCol
    {
        get { return Helper.GetString(ViewState["SortCol"], null); }
        set { ViewState["SortCol"] = value; }
    }

    protected string SortOrder
    {
        get { return Helper.GetString(ViewState["SortOrder"], "asc"); }
        set { ViewState["SortOrder"] = value; }
    }

    #endregion

    // ------------------------
    // ids: 1,2,5
    // where: '&1%' AND ('&2%' OR '&5%')
    // ------------------------
    private void GetGroupWhere(out string cgIDs, out string pgIDs, out string cIDs, out string pIDs, out string where)
    {
        cIDs = "";
        pIDs = "";
        cgIDs = "";
        pgIDs = "";
        where = "";

        // grouping members : '&1%', '!&1%'
        // category = 1375 AND ( category != 1076 OR category = 1075 ) 
        // --> '&1375%' AND ('!&1076%' OR '&1075%')
        where = mQueryBuilderSql.Value.Trim();
        where += " "; // we need this space at end to find IDs
        while (where.Contains("SGCategory = "))
        {
            int index = where.IndexOf("SGCategory = ") + "SGCategory = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            cgIDs += id + ",";
            where = where.Replace("SGCategory = " + id + " ", "'*" + id + "%' ");
        }
        while (where.Contains("SGCategory != "))
        {
            int index = where.IndexOf("SGCategory != ") + "SGCategory != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            cgIDs += id + ",";
            where = where.Replace("SGCategory != " + id + " ", "'!*" + id + "%' ");
        }
        if (cgIDs.EndsWith(","))
            cgIDs = cgIDs.Substring(0, cgIDs.Length - ",".Length);
        while (where.Contains("PGCategory = "))
        {
            int index = where.IndexOf("PGCategory = ") + "PGCategory = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pgIDs += id + ",";
            where = where.Replace("PGCategory = " + id + " ", "'$" + id + "%' ");
        }
        while (where.Contains("PGCategory != "))
        {
            int index = where.IndexOf("PGCategory != ") + "PGCategory != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pgIDs += id + ",";
            where = where.Replace("PGCategory != " + id + " ", "'!$" + id + "%' ");
        }
        if (pgIDs.EndsWith(","))
            pgIDs = pgIDs.Substring(0, pgIDs.Length - ",".Length);

        while (where.Contains("SCategory = "))
        {
            int index = where.IndexOf("SCategory = ") + "SCategory = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            cIDs += id + ",";
            where = where.Replace("SCategory = " + id + " ", "'^" + id + "%' ");
        }
        while (where.Contains("SCategory != "))
        {
            int index = where.IndexOf("SCategory != ") + "SCategory != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            cIDs += id + ",";
            where = where.Replace("SCategory != " + id + " ", "'!^" + id + "%' ");
        }
        if (cIDs.EndsWith(","))
            cIDs = cIDs.Substring(0, cIDs.Length - ",".Length);

        while (where.Contains("PCategory = "))
        {
            int index = where.IndexOf("PCategory = ") + "PCategory = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pIDs += id + ",";
            where = where.Replace("PCategory = " + id + " ", "'&" + id + "%' ");
        }
        while (where.Contains("PCategory != "))
        {
            int index = where.IndexOf("PCategory != ") + "PCategory != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pIDs += id + ",";
            where = where.Replace("PCategory != " + id + " ", "'!&" + id + "%' ");
        }
        if (pIDs.EndsWith(","))
            pIDs = pIDs.Substring(0, pIDs.Length - ",".Length);
    }

    protected void doExcel_Click(object sender, EventArgs e)
    {
        Exporter.ExportExcel(Response
            , new List<string>(new string[] { mGridTitle.Text, LabelVoucherDetailTitle.Text })
            , new List<GridView>(new GridView[] { mGrid, mlabelVoucherDetailGrid }));
    }
}