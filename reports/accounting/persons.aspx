﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="persons.aspx.cs" Inherits="reports_accounting_persons" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register src="~/controls/ReportSetting.ascx" tagName="ReportSetting" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" Runat="Server">
    <div id="bar">
    </div>
    <asp:Panel ID="panelDBs" runat="server" CssClass="well callout padding-5">
        <div class="row">
            <div class="col-xs-12">
                <h5 class="dashed-bottom padding-5-bottom yekan-family">
                    <asp:Label runat="server">Lang.DataBase</asp:Label>
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <asp:CheckBoxList runat="Server" ID="mDBList" RepeatLayout="Flow" RepeatDirection="Horizontal" />
            </div>
        </div>
     </asp:Panel>
   
   <asp:Panel ID="panelDropDowns" runat="server" CssClass="well callout padding-5">
        <input type="hidden" runat="server" id="mQueryBuilder" class="mQueryBuilder" />
        <input type="hidden" runat="server" id="mQueryBuilderSql" class="mQueryBuilderSql" />
        <div class="hidden">
            <asp:DropDownList runat="server" ID="mPersonGroupings" CssClass="mPersonGroupings"></asp:DropDownList>
            <asp:DropDownList runat="server" ID="mPersonGroup" CssClass="mPersonGroup"></asp:DropDownList>          
        </div>
        <div class="row">
            <div class="col-xs-12">
                <h5 class="dashed-bottom padding-5-bottom yekan-family">
                    <asp:Label runat="server">Lang.Groups</asp:Label>
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="ltr builder">
                    <div id="builder"></div>
                </div>
            </div>
        </div>
       <div class="row margin-5-top">
            <div class="col-md-8 col-lg-6 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container form-col-without-padding">
                <div class="col-xs-12 col-sm-3">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
                </div>
                <div class="col-xs-12 col-sm-3">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcel_Click" Enabled="false" />
                </div>
                <div class="col-xs-12 col-sm-3">
                    <input type="button" id="print" class="btn btn-xs btn-dark-blue btn-block" value="" />
                </div>
            </div>
        </div>
     </asp:Panel>
    <asp:Panel ID="gridPanel" runat="server">
        <JP:MiniHeader runat="server" Text="Lang.Items" ID="mGridTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="mGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true" OnSorting="mGrid_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Code" HeaderText="Lang.Code" ItemStyle-CssClass="tl align-center" SortExpression="Code" />
                        <asp:BoundField DataField="Name" HeaderText="Lang.Name" ItemStyle-CssClass="tl align-center" SortExpression="Name" />
                        <asp:BoundField DataField="Surname" HeaderText="Lang.Surname" ItemStyle-CssClass="tl align-center" SortExpression="Surname" />
                        <asp:BoundField DataField="FIdentify" HeaderText="Lang.FirstGroup" ItemStyle-CssClass="tl align-center" SortExpression="FIdentify" />
                        <asp:BoundField DataField="SIdentify" HeaderText="Lang.SecondGroup" ItemStyle-CssClass="tl align-center" SortExpression="SIdentify" />
                        <asp:BoundField DataField="HomeAddress" HeaderText="Lang.HomeAddress" ItemStyle-CssClass="tl align-center" SortExpression="HomeAddress" />
                        <asp:BoundField DataField="HomeTelNum" HeaderText="Lang.HomeTelNum" ItemStyle-CssClass="tl align-center" SortExpression="HomeTelNum" />
                        <asp:BoundField DataField="CellNumber1" HeaderText="Lang.CellNumber1" ItemStyle-CssClass="tl align-center" SortExpression="CellNumber1" />
                        <asp:BoundField DataField="Email" HeaderText="Lang.Email" ItemStyle-CssClass="tl align-center" SortExpression="Email" />
                        <asp:BoundField DataField="Latittude" HeaderText="Lang.GPS" ItemStyle-CssClass="tl align-center" SortExpression="Latittude" />
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
   
    <script>
        $(function () {

            $('#print').attr('value', Lang.Print);
            $('#print').click(function () {
                var head = Lang.Persons;
                <%--if ($("#<%=ddlBy.ClientID %>").val() == <%= (int)AccController.PersonBookGroupBy.ByTotal %>) {
                    head = '<%=jpMiniHeaderTotalBills.Text %>';
            }--%>
                printTable('.ScrollPanelCartable', '../../', head);
            });
            $('.GridRow,.GridAltRow').click(function () {
                $(this).toggleClass('selected');
            });
            $("#bar").prepend(breadcrumb({
                title: Lang.ReportPersons,
                parent: {
                    title: Lang.Reports,
                    url: 'menu/reports.aspx'
                }
            }));
            setClassReports();
            var pgg = {};
            $('.mPersonGroupings option').each(function () {
                pgg[$(this).attr('value')] = $(this).text();
            });
            var peg = {};
            $('.mPersonGroup option').each(function () {
                peg[$(this).attr('value')] = $(this).text();
            });
            //if ($jQuery1_9('#builder').length > 0)
            $jQuery1_9('#builder').queryBuilder({
                filters: [
                    {
                        id: 'PGCategory',
                        label: Lang.PersonsGroupLable,
                        type: 'integer',
                        input: 'select',
                        values: pgg,
                        operators: ['equal', 'not_equal']
                    },
                    {
                        id: 'PCategory',
                        label: Lang.PersonnelGroup,
                        type: 'integer',
                        input: 'select',
                        values: peg,
                        operators: ['equal', 'not_equal']
                    }
                ]
            });
            // save rules before post back
            $('.doShow').click(function () {
                var rules = $jQuery1_9('#builder').queryBuilder('getRules');
                var rulesStr = JSON.stringify(rules);
                // alert(rulesStr);
                $('.mQueryBuilder').val(rulesStr);
                $('.mQueryBuilderSql').val($jQuery1_9('#builder').queryBuilder('getSQL', false, false).sql);
            });
            //reload saved rules
            $jQuery1_9('#builder').queryBuilder('reset');
            var ruleStr = $('.mQueryBuilder').val();
            if (ruleStr != '') {
                var rule = eval("(" + ruleStr + ")");
                try {
                    $jQuery1_9('#builder').queryBuilder('setRules', rule);
                } catch (e) {
                }
            }

        });
    </script>
</asp:Content>
