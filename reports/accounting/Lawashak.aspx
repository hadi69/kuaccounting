﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Lawashak.aspx.cs" Inherits="reports_accounting_Lawashak" %>

<%@ Register Src="../../controls/ReportSetting.ascx" TagName="ReportSetting" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>
    <uc1:ReportSetting ID="mReportSetting" runat="server" />
    <div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label1" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="mFromDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="mToDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label runat="server">Lang.ProductType</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mProductType" runat="server" CssClass="T form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label runat="server">Lang.From</asp:Label>
                        <asp:Label runat="server">Lang.NumKg</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mNumKgFrom" runat="server" CssClass="T form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label runat="server">Lang.To</asp:Label>
                        <asp:Label runat="server">Lang.NumKg</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mNumKgTo" runat="server" CssClass="T form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label runat="server">Lang.From</asp:Label>
                        <asp:Label runat="server">Lang.NumBox</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mNumBoxFrom" runat="server" CssClass="T form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label runat="server">Lang.To</asp:Label>
                        <asp:Label runat="server">Lang.NumBox</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mNumBoxTo" runat="server" CssClass="T form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label runat="server">Lang.From</asp:Label>
                        <asp:Label runat="server">Lang.PersonHour</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mPersonHourFrom" runat="server" CssClass="T form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label runat="server">Lang.To</asp:Label>
                        <asp:Label runat="server">Lang.PersonHour</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mPersonHourTo" runat="server" CssClass="T form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-lg-3 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcel_Click" Enabled="false" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <input type="button" id="print" class="btn btn-xs btn-dark-blue btn-block" value="" />
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <asp:Label runat="server" ID="msg" CssClass="label-30 alert alert-danger block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
    </div>
    <asp:Panel ID="mDetailPanel" runat="server" Visible="False">
        <JP:MiniHeader runat="server" Text="Lang.Lawashak" ID="mTitle" />
        <asp:GridView ID="mGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
            Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mGrid_RowDataBound" GridLines="Both" EmptyDataText="Lang.DocumentIsEmpty"
            AllowSorting="true" OnSorting="mGrid_Sorting">
            <Columns>
                <asp:TemplateField HeaderText="Lang.ColRowNr">
                    <ItemTemplate>
                        <%# Container.DataItemIndex + 1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="ProductDate" HeaderText="Lang.ProductDate" ItemStyle-HorizontalAlign="Center" SortExpression="ProductDate" />
                <asp:TemplateField HeaderText="Lang.ProductType" ItemStyle-HorizontalAlign="Center" SortExpression="ProductType">
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%#Eval("ProductType") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="sum" runat="server" Text=""></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lang.NumKg" ItemStyle-HorizontalAlign="Center" SortExpression="NumKg">
                    <ItemTemplate>
                        <asp:Label ID="NumKg" runat="server" Text='<%#Eval("NumKg") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="NumKgTotal" runat="server" Text="Lang.Sum" />
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lang.NumBox" ItemStyle-HorizontalAlign="Center" SortExpression="NumBox">
                    <ItemTemplate>
                        <asp:Label ID="NumBox" runat="server" Text='<%#Eval("NumBox") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="NumBoxTotal" runat="server" Text="Lang.Sum" />
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lang.PersonHour" ItemStyle-HorizontalAlign="Center" SortExpression="PersonHour">
                    <ItemTemplate>
                        <asp:Label ID="PersonHour" runat="server" Text='<%#Eval("NumBox") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="PersonHourTotal" runat="server" Text="Lang.Sum" />
                    </FooterTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle CssClass="GridHeader" />
            <RowStyle CssClass="GridRow" />
            <SelectedRowStyle CssClass="GridRowSelected" />
        </asp:GridView>
    </asp:Panel>
    <script>
        $(function () {
            $('#sum').attr('text', Lang.Print);
            $('#print').attr('value', Lang.Print);
            $('#print').click(function () {
                var title = Lang.Lawashak;
                if ($("#<%=mFromDate.ClientID %>").val() != '') {
                    title += '/' + Lang.FromDate + ': ' + $("#<%=mFromDate.ClientID %>").val();
                }
                if ($("#<%=mToDate.ClientID %>").val() != '') {
                    title += '/' + Lang.ToDate + ': ' + $("#<%=mToDate.ClientID %>").val();
                }
                printTable('.ScrollPanelCartable', '../../', title);
            });

            $('.GridRow,.GridAltRow').click(function () {
                $(this).toggleClass('selected');
            });
            dateSelect('.dateselect');
            $("#bar").prepend(breadcrumb({
                title: Lang.Lawashak,
                parent: {
                    title: Lang.Reports,
                    url: 'menu/reports.aspx'
                }
            }));
            setClassReports();
        });
    </script>
</asp:Content>
