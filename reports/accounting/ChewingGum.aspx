﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ChewingGum.aspx.cs" Inherits="reports_accounting_ChewingGum" %>

<%@ Register Src="../../controls/ReportSetting.ascx" TagName="ReportSetting" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>
    <uc1:ReportSetting ID="mReportSetting" runat="server" />
    <div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label1" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="mFromDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="mToDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label runat="server">Lang.From</asp:Label>
                        <asp:Label runat="server">Lang.NumMainBake</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mNumMainBakeFrom" runat="server" CssClass="T form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label runat="server">Lang.To</asp:Label>
                        <asp:Label runat="server">Lang.NumMainBake</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mNumMainBakeTo" runat="server" CssClass="T form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label runat="server">Lang.From</asp:Label>
                        <asp:Label runat="server">Lang.AvgWeight1</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mAvgWeight1From" runat="server" CssClass="T form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label runat="server">Lang.To</asp:Label>
                        <asp:Label runat="server">Lang.AvgWeight1</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mAvgWeight1To" runat="server" CssClass="T form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label runat="server">Lang.From</asp:Label>
                        <asp:Label runat="server">Lang.AvgWeight2</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mAvgWeight2From" runat="server" CssClass="T form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label runat="server">Lang.To</asp:Label>
                        <asp:Label runat="server">Lang.AvgWeight2</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mAvgWeight2To" runat="server" CssClass="T form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label runat="server">Lang.From</asp:Label>
                        <asp:Label runat="server">Lang.NumBoxed</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mNumBoxedFrom" runat="server" CssClass="T form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label runat="server">Lang.To</asp:Label>
                        <asp:Label runat="server">Lang.NumBoxed</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mNumBoxedTo" runat="server" CssClass="T form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-lg-3 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcel_Click" Enabled="false" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <input type="button" id="print" class="btn btn-xs btn-dark-blue btn-block" value="" />
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <asp:Label runat="server" ID="msg" CssClass="label-30 alert alert-danger block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
    </div>
    <asp:Panel ID="mDetailPanel" runat="server" Visible="False">
        <JP:MiniHeader runat="server" Text="Lang.Lawashak" ID="mTitle" />
        <asp:GridView ID="mGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
            Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mGrid_RowDataBound" GridLines="Both" EmptyDataText="Lang.DocumentIsEmpty"
            AllowSorting="true" OnSorting="mGrid_Sorting">
            <Columns>
                <asp:TemplateField HeaderText="Lang.ColRowNr">
                    <ItemTemplate>
                        <%# Container.DataItemIndex + 1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lang.ProductDate" ItemStyle-HorizontalAlign="Center" SortExpression="ProductDate">
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%#Eval("ProductDate") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="sum" runat="server" Text=""></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lang.NumMainBake" ItemStyle-HorizontalAlign="Center" SortExpression="NumMainBake">
                    <ItemTemplate>
                        <asp:Label ID="NumMainBake" runat="server" Text='<%#Eval("NumMainBake") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="NumMainBakeTotal" runat="server" Text="Lang.Sum" />
                    </FooterTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Lang.NumRecycleBake" ItemStyle-HorizontalAlign="Center" SortExpression="NumRecycleBake">
                    <ItemTemplate>
                        <asp:Label ID="NumRecycleBake" runat="server" Text='<%#Eval("NumRecycleBake") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="NumRecycleBakeTotal" runat="server" Text="Lang.Sum" />
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lang.NumRawBake" ItemStyle-HorizontalAlign="Center" SortExpression="NumRawBake">
                    <ItemTemplate>
                        <asp:Label ID="NumRawBake" runat="server" Text='<%#Eval("NumRawBake") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="NumRawBakeTotal" runat="server" Text="Lang.Sum" />
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lang.NumSugarFreeBake" ItemStyle-HorizontalAlign="Center" SortExpression="NumSugarFreeBake">
                    <ItemTemplate>
                        <asp:Label ID="NumSugarFreeBake" runat="server" Text='<%#Eval("NumSugarFreeBake") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="NumSugarFreeBakeTotal" runat="server" Text="Lang.Sum" />
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lang.NumSugar" ItemStyle-HorizontalAlign="Center" SortExpression="NumSugar">
                    <ItemTemplate>
                        <asp:Label ID="NumSugar" runat="server" Text='<%#Eval("NumSugar") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="NumSugarTotal" runat="server" Text="Lang.Sum" />
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lang.NumZyl" ItemStyle-HorizontalAlign="Center" SortExpression="NumZyl">
                    <ItemTemplate>
                        <asp:Label ID="NumZyl" runat="server" Text='<%#Eval("NumZyl") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="NumZylTotal" runat="server" Text="Lang.Sum" />
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lang.NumReturn" ItemStyle-HorizontalAlign="Center" SortExpression="NumReturn">
                    <ItemTemplate>
                        <asp:Label ID="NumReturn" runat="server" Text='<%#Eval("NumReturn") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="NumReturnTotal" runat="server" Text="Lang.Sum" />
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lang.NoBox" ItemStyle-HorizontalAlign="Center" SortExpression="NoBox">
                    <ItemTemplate>
                        <asp:Label ID="NoBox" runat="server" Text='<%#Eval("NoBox") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="NoBoxTotal" runat="server" Text="Lang.Sum" />
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lang.AvgWeight1" ItemStyle-HorizontalAlign="Center" SortExpression="AvgWeight1">
                    <ItemTemplate>
                        <asp:Label ID="AvgWeight1" runat="server" Text='<%#Eval("AvgWeight1") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="AvgWeight1Total" runat="server" Text="Lang.Sum" />
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lang.AvgWeight2" ItemStyle-HorizontalAlign="Center" SortExpression="AvgWeight2">
                    <ItemTemplate>
                        <asp:Label ID="AvgWeight2" runat="server" Text='<%#Eval("AvgWeight1") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="AvgWeight2Total" runat="server" Text="Lang.Sum" />
                    </FooterTemplate>
                </asp:TemplateField>

                
                <asp:TemplateField HeaderText="Lang.NumDump" ItemStyle-HorizontalAlign="Center" SortExpression="NumDump">
                    <ItemTemplate>
                        <asp:Label ID="NumDump" runat="server" Text='<%#Eval("NumDump") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="NumDumpTotal" runat="server" Text="Lang.Sum" />
                    </FooterTemplate>
                </asp:TemplateField>                
                <asp:TemplateField HeaderText="Lang.NumBoxed" ItemStyle-HorizontalAlign="Center" SortExpression="NumBoxed">
                    <ItemTemplate>
                        <asp:Label ID="NumBoxed" runat="server" Text='<%#Eval("NumBoxed") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="NumBoxedTotal" runat="server" Text="Lang.Sum" />
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lang.NumBoxShrink" ItemStyle-HorizontalAlign="Center" SortExpression="NumBoxShrink">
                    <ItemTemplate>
                        <asp:Label ID="NumBoxShrink" runat="server" Text='<%#Eval("NumBoxShrink") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="NumBoxShrinkTotal" runat="server" Text="Lang.Sum" />
                    </FooterTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Lang.PersonHour" ItemStyle-HorizontalAlign="Center" SortExpression="PersonHour">
                    <ItemTemplate>
                        <asp:Label ID="PersonHour" runat="server" Text='<%#Eval("PersonHour") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="PersonHourTotal" runat="server" Text="Lang.Sum" />
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lang.ReturnToBake" ItemStyle-HorizontalAlign="Center" SortExpression="ReturnToBake">
                    <ItemTemplate>
                        <asp:Label ID="ReturnToBake" runat="server" Text='<%#Eval("ReturnToBake") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="ReturnToBakeTotal" runat="server" Text="Lang.Sum" />
                    </FooterTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle CssClass="GridHeader" />
            <RowStyle CssClass="GridRow" />
            <SelectedRowStyle CssClass="GridRowSelected" />
        </asp:GridView>
    </asp:Panel>
    <script>
        $(function () {
            $('#sum').attr('text', Lang.Print);
            $('#print').attr('value', Lang.Print);
            $('#print').click(function () {
                var title = Lang.ChewingGum;
                if ($("#<%=mFromDate.ClientID %>").val() != '') {
                    title += '/' + Lang.FromDate + ': ' + $("#<%=mFromDate.ClientID %>").val();
                }
                if ($("#<%=mToDate.ClientID %>").val() != '') {
                    title += '/' + Lang.ToDate + ': ' + $("#<%=mToDate.ClientID %>").val();
                }
                printTable('.ScrollPanelCartable', '../../', title);
            });

            $('.GridRow,.GridAltRow').click(function () {
                $(this).toggleClass('selected');
            });
            dateSelect('.dateselect');
            $("#bar").prepend(breadcrumb({
                title: Lang.ChewingGum,
                parent: {
                    title: Lang.Reports,
                    url: 'menu/reports.aspx'
                }
            }));
            setClassReports();
        });
    </script>
</asp:Content>
