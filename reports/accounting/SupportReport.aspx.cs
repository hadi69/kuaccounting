﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.Globalization;

public partial class reports_accounting_SupportReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["SupportReport"];
            Page.SetLang();

            mReportSetting.ReportName = LangProvider.Instance["SupportReport"];

            BindAutoCompelet();

            System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
            mYear.Text = cal.GetYear(DateTime.Now).ToString();
            //mMonth.se = cal.GetMonth(DateTime.Now).ToString();
        }
        mReportSetting.SaveClick += new EventHandler(mReportSetting_SaveClick);
        mReportSetting.ReportSettingSelect += new EventHandler(mReportSetting_ReportSettingSelect);
    }

    void mReportSetting_ReportSettingSelect(object sender, EventArgs e)
    {
        ReportSettingInfo info = mReportSetting.SelectedReportSettings;
        if (info == null)
            return;
        string[] lines = info.Settings.Split('\n');
        mYear.Text = lines[0].Trim('\r');
        mPersonnel.SelectedValue = lines[1].Trim('\r');
        mPersonnelGroup.SelectedValue = lines[2].Trim('\r');
        mTopic.SelectedValue = lines[3].Trim('\r');
    }

    void mReportSetting_SaveClick(object sender, EventArgs e)
    {
        StringBuilder b = new StringBuilder();
        b.AppendLine(mYear.Text); //0
        b.AppendLine(mPersonnel.SelectedValue);  //1
        b.AppendLine(mPersonnelGroup.SelectedValue); //2
        b.AppendLine(mTopic.SelectedValue); //3
        mReportSetting.Save(b.ToString());
    }
    #region Bindings
    void BindAutoCompelet()
    {
        IDataReader persons = DbProxy.Instance.ExecuteReader("SELECT ID, [dbo].[DisplayPerson](ID, 1), ISNULL(Code, '') FROM Personnel WHERE Enabled = 1 AND Type=101");
        mPersonnel.Items.Clear();
        mPersonnel.Items.Add(new ListItem(LangProvider.Instance["AllPersons"], ""));
        if (persons != null)
        {
            while (persons.Read())
            {
                if (!persons.IsDBNull(0) && !persons.IsDBNull(1) && !persons.IsDBNull(2))
                    mPersonnel.Items.Add(new ListItem(persons.GetString(1) + " - کد: " + persons.GetString(2),
                        persons.GetInt32(0).ToString()));
            }
            persons.Close();
        }
        mPersonnel.SelectedIndex = 0;

        IDataReader projects = DbProxy.Instance.ExecuteReader("SELECT ID, Title FROM PersonnelGroup");
        mPersonnelGroup.Items.Clear();
        mPersonnelGroup.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
        if (projects != null)
        {
            while (projects.Read())
            {
                if (!projects.IsDBNull(0) && !projects.IsDBNull(1))
                    mPersonnelGroup.Items.Add(new ListItem(projects.GetString(1), projects.GetInt32(0).ToString()));
            }
            projects.Close();
        }
        mPersonnelGroup.SelectedIndex = 0;

        mTopic.Items.Clear();
        mTopic.Items.Add(new ListItem(LangProvider.Instance["AllTopics"], ""));
        IDataReader arr = AccController.GetTreeNames(SiteSettings.ProgramID);
        if (arr != null)
        {
            while (arr.Read())
            {
                if (!arr.IsDBNull(0) && !arr.IsDBNull(1))
                    mTopic.Items.Add(new ListItem(arr.GetString(1), arr.GetString(0)));
            }
            arr.Close();
        }
        mTopic.SelectedIndex = 0;
    }
    #endregion
   // string rowStyle = "BigGridRow";

    private decimal month1Pay = 0,
        month2Pay = 0,
        month3Pay = 0,
        month4Pay = 0,
        month5Pay = 0,
        month6Pay = 0,
        month7Pay = 0,
        month8Pay = 0,
        month9Pay = 0,
        month10Pay = 0,
        month11Pay = 0,
        month12Pay = 0,
        totalPay = 0,
        monthlyFee = 0,
        pastRemain = 0,
        totalRemain = 0;

    private decimal totalMonth1Pay = 0,
        totalMonth2Pay = 0,
        totalMonth3Pay = 0,
        totalMonth4Pay = 0,
        totalMonth5Pay = 0,
        totalMonth6Pay = 0,
        totalMonth7Pay = 0,
        totalMonth8Pay = 0,
        totalMonth9Pay = 0,
        totalMonth10Pay = 0,
        totalMonth11Pay = 0,
        totalMonth12Pay = 0,
        totalTotalRemain = 0;

    private string currentMonth = "";
    private string[] date;
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //rowStyle = rowStyle == "GridAltRow" ? "BigGridRow" : "GridAltRow";
            //e.Row.CssClass = rowStyle;

            totalPay = Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["TotalPay"], 0);
            monthlyFee = Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["MonthlyFee"], 0);

            for (int i = 1; i < Helper.GetInt(currentMonth, -1); i++)
            {
                if (i*monthlyFee <= totalPay)
                {
                    e.Row.Cells[i + 3].BackColor = Color.LightSeaGreen;
                }
                else
                {
                    for (int j = i; j < Helper.GetInt(currentMonth, -1); j++)
                    {
                        e.Row.Cells[j + 3].BackColor = Color.IndianRed;
                    }
                    break;
                }
            }

            month1Pay = Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["Month1Pay"], 0);
            month2Pay = Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["Month2Pay"], 0);
            month3Pay = Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["Month3Pay"], 0);
            month4Pay = Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["Month4Pay"], 0);
            month5Pay = Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["Month5Pay"], 0);
            month6Pay = Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["Month6Pay"], 0);
            month7Pay = Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["Month7Pay"], 0);
            month8Pay = Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["Month8Pay"], 0);
            month9Pay = Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["Month9Pay"], 0);
            month10Pay = Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["Month10Pay"], 0);
            month11Pay = Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["Month11Pay"], 0);
            month12Pay = Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["Month12Pay"], 0);
            pastRemain = Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["PastRemain"], 0);
            totalRemain = totalPay - ( Helper.GetInt(currentMonth, 0) * monthlyFee );

            if (pastRemain < 0)
            {
                e.Row.Cells[3].Text = SiteSettings.ToMonetary((-1 * pastRemain), DateTime.Now).ToString("N0") + " " + LangProvider.Instance["Li"];
            }
            else
            {
                e.Row.Cells[3].Text = SiteSettings.ToMonetary(pastRemain, DateTime.Now).ToString("N0") + " " + LangProvider.Instance["Cr"]; ;
            }

            if (totalRemain < 0)
            {
                e.Row.Cells[16].Text = SiteSettings.ToMonetary((-1 * totalRemain), DateTime.Now).ToString("N0") + " " + LangProvider.Instance["Li"];
            }
            else
            {
                e.Row.Cells[16].Text = SiteSettings.ToMonetary(totalRemain , DateTime.Now).ToString("N0") + " " + LangProvider.Instance["Cr"]; ;
            }

            e.Row.Cells[2].Text = SiteSettings.ToMonetary(monthlyFee, DateTime.Now).ToString("N0");
            e.Row.Cells[4].Text = SiteSettings.ToMonetary(month1Pay, DateTime.Now).ToString("N0");
            e.Row.Cells[5].Text = SiteSettings.ToMonetary(month2Pay, DateTime.Now).ToString("N0");
            e.Row.Cells[6].Text = SiteSettings.ToMonetary(month3Pay, DateTime.Now).ToString("N0");
            e.Row.Cells[7].Text = SiteSettings.ToMonetary(month4Pay, DateTime.Now).ToString("N0");
            e.Row.Cells[8].Text = SiteSettings.ToMonetary(month5Pay, DateTime.Now).ToString("N0");
            e.Row.Cells[9].Text = SiteSettings.ToMonetary(month6Pay, DateTime.Now).ToString("N0");
            e.Row.Cells[10].Text = SiteSettings.ToMonetary(month7Pay, DateTime.Now).ToString("N0");
            e.Row.Cells[11].Text = SiteSettings.ToMonetary(month8Pay, DateTime.Now).ToString("N0");
            e.Row.Cells[12].Text = SiteSettings.ToMonetary(month9Pay, DateTime.Now).ToString("N0");
            e.Row.Cells[13].Text = SiteSettings.ToMonetary(month10Pay, DateTime.Now).ToString("N0");
            e.Row.Cells[14].Text = SiteSettings.ToMonetary(month11Pay, DateTime.Now).ToString("N0");
            e.Row.Cells[15].Text = SiteSettings.ToMonetary(month12Pay, DateTime.Now).ToString("N0");



            totalMonth1Pay += month1Pay;
            totalMonth2Pay += month2Pay;
            totalMonth3Pay += month3Pay;
            totalMonth4Pay += month4Pay;
            totalMonth5Pay += month5Pay;
            totalMonth6Pay += month6Pay;
            totalMonth7Pay += month7Pay;
            totalMonth8Pay += month8Pay;
            totalMonth9Pay += month9Pay;
            totalMonth10Pay += month10Pay;
            totalMonth11Pay += month11Pay;
            totalMonth12Pay += month12Pay;
            totalTotalRemain += totalRemain;
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblTotalMonth1Pay = (Label)e.Row.FindControl("TotalMonth1Pay");
            Label lblTotalMonth2Pay = (Label)e.Row.FindControl("TotalMonth2Pay");
            Label lblTotalMonth3Pay = (Label)e.Row.FindControl("TotalMonth3Pay");
            Label lblTotalMonth4Pay = (Label)e.Row.FindControl("TotalMonth4Pay");
            Label lblTotalMonth5Pay = (Label)e.Row.FindControl("TotalMonth5Pay");
            Label lblTotalMonth6Pay = (Label)e.Row.FindControl("TotalMonth6Pay");
            Label lblTotalMonth7Pay = (Label)e.Row.FindControl("TotalMonth7Pay");
            Label lblTotalMonth8Pay = (Label)e.Row.FindControl("TotalMonth8Pay");
            Label lblTotalMonth9Pay = (Label)e.Row.FindControl("TotalMonth9Pay");
            Label lblTotalMonth10Pay = (Label)e.Row.FindControl("TotalMonth10Pay");
            Label lblTotalMonth11Pay = (Label)e.Row.FindControl("TotalMonth11Pay");
            Label lblTotalMonth12Pay = (Label)e.Row.FindControl("TotalMonth12Pay");
            Label lblTotalTotalRemained = (Label)e.Row.FindControl("TotalTotalRemained");

            lblTotalMonth1Pay.Text = SiteSettings.ToMonetary(totalMonth1Pay, DateTime.Now).ToString("N0");
            lblTotalMonth2Pay.Text = SiteSettings.ToMonetary(totalMonth2Pay, DateTime.Now).ToString("N0");
            lblTotalMonth3Pay.Text = SiteSettings.ToMonetary(totalMonth3Pay, DateTime.Now).ToString("N0");
            lblTotalMonth4Pay.Text = SiteSettings.ToMonetary(totalMonth4Pay, DateTime.Now).ToString("N0");
            lblTotalMonth5Pay.Text = SiteSettings.ToMonetary(totalMonth5Pay, DateTime.Now).ToString("N0");
            lblTotalMonth6Pay.Text = SiteSettings.ToMonetary(totalMonth6Pay, DateTime.Now).ToString("N0");
            lblTotalMonth7Pay.Text = SiteSettings.ToMonetary(totalMonth7Pay, DateTime.Now).ToString("N0");
            lblTotalMonth8Pay.Text = SiteSettings.ToMonetary(totalMonth8Pay, DateTime.Now).ToString("N0");
            lblTotalMonth9Pay.Text = SiteSettings.ToMonetary(totalMonth9Pay, DateTime.Now).ToString("N0");
            lblTotalMonth10Pay.Text = SiteSettings.ToMonetary(totalMonth10Pay, DateTime.Now).ToString("N0");
            lblTotalMonth11Pay.Text = SiteSettings.ToMonetary(totalMonth11Pay, DateTime.Now).ToString("N0");
            lblTotalMonth12Pay.Text = SiteSettings.ToMonetary(totalMonth12Pay, DateTime.Now).ToString("N0");

            if (totalTotalRemain < 0)
            {
                lblTotalTotalRemained.Text = SiteSettings.ToMonetary((-1 * totalTotalRemain), DateTime.Now).ToString("N0") + " " + LangProvider.Instance["Li"];
            }
            else
            {
                lblTotalTotalRemained.Text = SiteSettings.ToMonetary(totalTotalRemain, DateTime.Now).ToString("N0") + " " + LangProvider.Instance["Cr"]; ;
            }
            
        }

    }

    protected void doShow_Click(object sender, EventArgs e)
    {
        int year = Helper.GetInt(mYear.Text, -1);
        currentMonth = Helper.FormatDate(DateTime.Now, "yyyy/MM/dd");
        date = currentMonth.Split('/');
        currentMonth = date[1];
        if (year >= 0)
            BindData(year);
        
    }

    private void BindData(int year)
    {
        PersianCalendar cal = new PersianCalendar();
        int personId = Helper.GetInt(mPersonnel.SelectedValue, Null.NullInteger);
        int personGroupId = Helper.GetInt(mPersonnelGroup.SelectedValue, Null.NullInteger);
        string treePath = mTopic.SelectedValue;
        DateTime startMonth1 = cal.ToDateTime(year, 1, 1, 0, 0, 0, 0);
        DateTime startMonth2 = cal.ToDateTime(year, 2, 1, 0, 0, 0, 0);
        DateTime startMonth3 = cal.ToDateTime(year, 3, 1, 0, 0, 0, 0);
        DateTime startMonth4 = cal.ToDateTime(year, 4, 1, 0, 0, 0, 0);
        DateTime startMonth5 = cal.ToDateTime(year, 5, 1, 0, 0, 0, 0);
        DateTime startMonth6 = cal.ToDateTime(year, 6, 1, 0, 0, 0, 0);
        DateTime startMonth7 = cal.ToDateTime(year, 7, 1, 0, 0, 0, 0);
        DateTime startMonth8 = cal.ToDateTime(year, 8, 1, 0, 0, 0, 0);
        DateTime startMonth9 = cal.ToDateTime(year, 9, 1, 0, 0, 0, 0);
        DateTime startMonth10 = cal.ToDateTime(year, 10, 1, 0, 0, 0, 0);
        DateTime startMonth11 = cal.ToDateTime(year, 11, 1, 0, 0, 0, 0);
        DateTime startMonth12 = cal.ToDateTime(year, 12, 1, 0, 0, 0, 0);
        DateTime endMonth12 = cal.ToDateTime(year, 12, cal.GetDaysInMonth(year, 12), 23, 59, 59, 999);

        string qry = string.Format("SELECT * FROM [dbo].[GetSupportReport]({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15})"
            , personId, Helper.QoutedDateTime(startMonth1), Helper.QoutedDateTime(startMonth2), Helper.QoutedDateTime(startMonth3)
            , Helper.QoutedDateTime(startMonth4), Helper.QoutedDateTime(startMonth5), Helper.QoutedDateTime(startMonth6)
            , Helper.QoutedDateTime(startMonth7), Helper.QoutedDateTime(startMonth8), Helper.QoutedDateTime(startMonth9)
            , Helper.QoutedDateTime(startMonth10), Helper.QoutedDateTime(startMonth11), Helper.QoutedDateTime(startMonth12)
            , Helper.QoutedDateTime(endMonth12), personGroupId, Helper.QoutedString(treePath));
        IDataReader items = DbProxy.Instance.ExecuteReader(qry);

        try
        {
            mGrid.DataSource = items;
            mGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Close();
                items.Dispose();
            }
        }
    }
    protected void doExcel_Click(object sender, EventArgs e)
    {
        Exporter.ExportExcel(Response
            , new List<string>(new string[] { mGridTitle.Text})
            , new List<GridView>(new GridView[] { mGrid}));
    }
}