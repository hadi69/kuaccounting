﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="VouchersReport.aspx.cs" Inherits="reports_accounting_VouchersReport" %>

<%@ Register Src="../../controls/AccountsFilter.ascx" TagName="AccountsFilter" TagPrefix="uc3" %>
<%@ Register Src="../../controls/ReportSetting.ascx" TagName="ReportSetting" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cnt" runat="Server">
    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="CommandRow">
        <tr class='SettingRow'>
            <td colspan="9">
                <uc2:ReportSetting ID="mReportSetting" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="Name" style="text-align: left">&nbsp;
            </td>
            <td class="Name" style="text-align: left"></td>
            <td></td>
            <td class="Name" style="text-align: left"></td>
            <td></td>
            <td class="Name" style="text-align: left"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="8">
                <uc3:AccountsFilter ID="mAccountsFilter" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="Name" style="text-align: left"><%= "Ward".Translate() %>:</td>
            <td class="Name">
                <asp:DropDownList ID="mWards" runat="server" CssClass="Combo" Width="150px" /></td>
            <td></td>
            <td class="Name" style="text-align: left"><%= "FromDate".Translate() %>:
            </td>
            <td>
                <asp:TextBox ID="mFromDate" runat="server" CssClass="T2" Width="65"></asp:TextBox>
                <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="mFromDate" Mask="9999/99/99"
                    MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                    MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
            </td>
            <td class="Name" style="text-align: left"><%= "ToDate".Translate() %>:
            </td>
            <td>
                <asp:TextBox ID="mToDate" runat="server" CssClass="T2" Width="65"></asp:TextBox>
                <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="mToDate" Mask="9999/99/99"
                    MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                    MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
            </td>
            <td></td>
        </tr>
        <tr>
            <td class="Name"><%= "DocumentDescription".Translate() %>:</td>
            <td colspan="2">
                <asp:TextBox ID="mVocherTitle" runat="server" CssClass="T" Width="280px"></asp:TextBox></td>
            <td class="Name" style="text-align: left"><%= "FromDocNo".Translate() %>:
            </td>
            <td>
                <asp:TextBox ID="mFromVoucherID" runat="server" CssClass="T" Width="65px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="fte1" runat="server" TargetControlID="mFromVoucherID"
                    FilterType="Numbers" />
            </td>
            <td class="Name" style="text-align: left"><%= "ToDocNo".Translate() %>:
            </td>
            <td>
                <asp:TextBox ID="mToVoucherID" runat="server" CssClass="T" Width="65px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="fte2" runat="server" TargetControlID="mToVoucherID"
                    FilterType="Numbers" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="Name"><%= "Attainer".Translate() %>:</td>
            <td class="Name" colspan="2">
                <asp:TextBox ID="mReciever" runat="server" CssClass="T" Width="280px"></asp:TextBox></td>
            <td class="Name" style="text-align: left">&nbsp;<%= "InDebtAmountFrom".Translate() %>:</td>
            <td>
                <asp:TextBox ID="mOweStart" runat="server" CssClass="T" Width="150px"></asp:TextBox><cc1:FilteredTextBoxExtender ID="f1" runat="server" TargetControlID="mOweStart"
                    FilterType="Numbers" />
            </td>
            <td class="Name" style="text-align: left"><%= "InDebtAmountTo".Translate() %>:</td>
            <td>
                <asp:TextBox ID="mOweEnd" runat="server" CssClass="T" Width="150px"></asp:TextBox><cc1:FilteredTextBoxExtender ID="f2" runat="server" TargetControlID="mOweEnd"
                    FilterType="Numbers" />
            </td>
            <td></td>
        </tr>
        <tr>
            <td class="Name"><%= "DocNo".Translate() %>:</td>
            <td class="Name" colspan="2">
                <asp:TextBox ID="mDocNo" runat="server" CssClass="T" Width="150px"></asp:TextBox></td>
            <td class="Name" style="text-align: left"><%= "DueAmountFrom".Translate() %>:</td>
            <td>
                <asp:TextBox ID="mDueStart" runat="server" CssClass="T" Width="150px"></asp:TextBox><cc1:FilteredTextBoxExtender ID="f3" runat="server" TargetControlID="mDueStart"
                    FilterType="Numbers" />
            </td>
            <td class="Name" style="text-align: left"><%= "DueAmountTo".Translate() %>:</td>
            <td>
                <asp:TextBox ID="mDueEnd" runat="server" CssClass="T" Width="150px"></asp:TextBox><cc1:FilteredTextBoxExtender ID="f4" runat="server" TargetControlID="mDueEnd"
                    FilterType="Numbers" />
            </td>
            <td>
                <asp:Button ID="doShow" runat="server" Text='Lang.Display' CssClass="Button doShow" OnClick="doShow_Click" Width="50px" /></td>
        </tr>
    </table>
    <div class="Title" style="text-align: center">
        <asp:Label runat="server" ID="mTitle"><%= "DocumentReports".Translate() %></asp:Label>
    </div>
    <asp:GridView ID="mGrid" AutoGenerateColumns="False" GridWidth="95%"
        Width="100%" CssClass="ScrollPanelCartable" Scrolling="Vertical" runat="server"
        BorderStyle="None" OnRowDataBound="mGrid_RowDataBound" GridLines="Both" EmptyDataText="Lang.DocumentIsEmpty">
        <Columns>
            <asp:BoundField DataField="ID" ItemStyle-HorizontalAlign="Center" HeaderText="Lang.ColRowNr" />
            <asp:BoundField DataField="DoComments" HeaderText="Lang.DocumentDescription" />
            <asp:BoundField DataField="No" HeaderText="Lang.DocumentNumber" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="SortIndex" HeaderText="Lang.DocumentRow" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="DocNo" HeaderText="Lang.DocumentNo" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="Owe" HeaderText="Lang.OweAmount" ItemStyle-Width="100px"
                ItemStyle-HorizontalAlign="Center" DataFormatString="{0:N0}" />
            <asp:BoundField DataField="Due" HeaderText="Lang.DueAmount" ItemStyle-Width="100px"
                ItemStyle-HorizontalAlign="Center" DataFormatString="{0:N0}" />

        </Columns>
        <HeaderStyle CssClass="GridHeader" />
        <RowStyle CssClass="GridRow" />
        <SelectedRowStyle CssClass="GridRowSelected" />
    </asp:GridView>
</asp:Content>

