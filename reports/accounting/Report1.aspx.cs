﻿using System;
using System.Activities.Expressions;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reports_accounting_Report1 : System.Web.UI.Page
{
    System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
    protected void Page_Load(object sender, EventArgs e)
    {
        msg.Visible = false;
        mGrid.EmptyDataText = LangProvider.Instance["ListIsEmpty"];
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");
        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["Report1"];
            Page.SetLang();
            int year = cal.GetYear(DateTime.Now);
            //mFromDate.Text = string.Format("{0}/01/01", year - 1300);
            //mToDate.Text = Helper.FormatDate(cal.ToDateTime(year + 1, 1, 1, 1, 1, 1, 1).AddDays(-1), "yy/MM/dd");
            IDataReader financeYearStartDate = DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");//0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            //mFromDate.Text = 
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);
            Init();
            BindTopics();
            BindDBs();
        }
        mGrid.EmptyDataText = LangProvider.Instance["ListIsEmpty"];
    }
    void Init()
    {
        IDataReader persons = DbProxy.Instance.ExecuteReader("SELECT ID, [dbo].[DisplayPerson](ID, 1), ISNULL(Code, '') FROM Personnel WHERE Enabled = 1 AND Type=101");
        mPersons.Items.Clear();
        mPersons.Items.Add(new ListItem(LangProvider.Instance["AllPersons"], ""));
        if (persons != null)
        {
            while (persons.Read())
            {
                if (!persons.IsDBNull(0) && !persons.IsDBNull(1) && !persons.IsDBNull(2))
                    mPersons.Items.Add(
                        new ListItem(
                            (persons.IsDBNull(1) ? "" : persons.GetString(1)) + " - کد: " + persons.GetString(2),
                            persons.GetInt32(0).ToString()));
            }
            persons.Close();
        }
        mPersons.SelectedIndex = 0;

        IDataReader projects = DbProxy.Instance.ExecuteReader("SELECT ID, Name + '(' + ISNULL(Code, '') + ')' FROM Ward WHERE SectionType=8");
        mProjects.Items.Clear();
        mProjects.Items.Add(new ListItem(LangProvider.Instance["AllProject"], ""));
        if (projects != null)
        {
            while (projects.Read())
            {
                if (!projects.IsDBNull(0) && !projects.IsDBNull(1))
                    mProjects.Items.Add(new ListItem(projects.GetString(1), projects.GetInt32(0).ToString()));
            }
            projects.Close();
        }
        mProjects.SelectedIndex = 0;

        IDataReader equipments = DbProxy.Instance.ExecuteReader("SELECT ID, FullName, Code, Title FROM EquipmentView");
        mEquipments.Items.Clear();
        mEquipments.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
        if (projects != null)
        {
            while (equipments.Read())
            {
                if (!equipments.IsDBNull(0) && !equipments.IsDBNull(1) && !equipments.IsDBNull(2))
                    mEquipments.Items.Add(new ListItem(equipments.GetString(1) + " - " + equipments.GetString(2),
                        equipments.GetInt32(0).ToString()));
            }
            equipments.Close();
        }
        mEquipments.SelectedIndex = 0;

        IDataReader costcenters = DbProxy.Instance.ExecuteReader("SELECT ID, FullName, Code, Name FROM CostCenterView");
        mCostCenters.Items.Clear();
        mCostCenters.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
        if (costcenters != null)
        {
            while (costcenters.Read())
            {
                if (!costcenters.IsDBNull(0) && !costcenters.IsDBNull(1) && !costcenters.IsDBNull(2))
                    mCostCenters.Items.Add(new ListItem(costcenters.GetString(1) + " - " + costcenters.GetString(2),
                        costcenters.GetInt32(0).ToString()));
            }
            costcenters.Close();
        }
        mCostCenters.SelectedIndex = 0;

        panelShowInProgressVouchers.Visible = DbProxy.Instance.GetConfig("VoucherWorkflow", -1) == "1";
    }
    void BindDBs()
    {
        mDBList.Items.Clear();
        ListItem item = new ListItem(LangProvider.Instance["ActiveYear"] + " ", "");
        if (string.IsNullOrEmpty(SiteSettings.OtherCatalogName))
            item.Selected = true;
        mDBList.Items.Add(item);
        try
        {
            string path = Server.MapPath("~/files/fdb");
            if (System.IO.Directory.Exists(path))
            {
                string[] files = System.IO.Directory.GetFiles(path, "DB_*.mdf");
                for (int i = 0; i < files.Length; i++)
                {
                    string fileName = System.IO.Path.GetFileNameWithoutExtension(files[i]);
                    if (!fileName.ToLower().EndsWith("_log"))
                    {
                        item = new ListItem(fileName + " ", fileName);
                        if (fileName == SiteSettings.OtherCatalogName)
                            item.Selected = true;
                        mDBList.Items.Add(item);
                    }
                }
            }
        }
        catch { }
        panelDBs.Visible = mDBList.Items.Count > 1;
    }
    private void BindTopics()
    {
        mTree.Nodes.Clear();
        TreeNode rootNode = new TreeNode("MasterAccounts".Translate(), "");
        List<TreeInfo> roots = AccController.GetTrees(0, SiteSettings.ProgramID);
        roots.Sort();
        for (int i = 0; i < roots.Count; i++)
        {
            //rootNode.ChildNodes.Add(GetTreeNode(roots[i]));
            TreeNode node = GetTreeNode(roots[i]);
            if (node != null)
                rootNode.ChildNodes.Add(node);
            else
                rootNode.ChildNodes.Add(new TreeNode("INVALID TOPIC : " + roots[i].TreePath));
        }

        mTree.Nodes.Add(rootNode);
        mTree.CollapseAll();
        rootNode.Expand();
    }
    private TreeNode GetTreeNode(TreeInfo info)
    {
        TopicInfo topic = AccController.GetTopic(info.TopicID);
        TreeNode node = GetSingleTreeNode(info, topic);
        if (node == null)
            return null;
        List<TreeInfo> childs = AccController.GetTrees(info);
        childs.Sort();
        if (childs.Count > 0)
            node.ImageUrl = "~/img/programIcon.gif";
        for (int i = 0; i < childs.Count; i++)
        {
            //node.ChildNodes.Add(GetTreeNode(childs[i]));
            TreeNode ch = GetTreeNode(childs[i]);
            if (ch != null)
                node.ChildNodes.Add(ch);
            else
                node.ChildNodes.Add(new TreeNode("INVALID TOPIC : " + childs[i].TreePath));
        }
        return node;
    }
    private TreeNode GetSingleTreeNode(TreeInfo info, TopicInfo topic)
    {
        if (topic == null || info == null)
            return null;
        TreeNode node = new TreeNode(topic.NameCode, info.TreePath);
        node.ImageUrl = "~/img/leaf.gif";
        return node;
    }
    protected void doShow_Click(object sender, EventArgs e)
    {
        BindGrid();

        doExcel.Enabled = true;
    }

    private List<string> GetDBs()
    {
        List<string> dbs = new List<string>();
        for (int i = 0; i < mDBList.Items.Count; i++)
            if (mDBList.Items[i].Selected)
                dbs.Add(mDBList.Items[i].Value);
        return dbs;
    }

    string rowStyle = "GridRow";
    decimal sumOwe = 0, sumDue = 0;
    string lastTreePath = "";
    // int lastPersonnelID = 0, lastProjectID = 0, counter = 0;
    protected void BindGrid()
    {
        DateTime start = Helper.ParseDate(mFromDate.Text, Null.NullDate);
        DateTime end = Helper.ParseDate(mToDate.Text, Null.NullDate);
        if (Null.IsNull(start))
        {
            msg.Text = LangProvider.Instance["InvalidStartDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(end))
        {
            msg.Text = LangProvider.Instance["InvalidEndDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(start) || Null.IsNull(end))
            return;
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);
        List<string> treePathes = new List<string>();
        int count = -1, minLevel = -1;
        if (mTree.CheckedNodes != null && mTree.CheckedNodes.Count > 0)
            for (int i = 0; i < mTree.CheckedNodes.Count; i++)
            {
                if (!string.IsNullOrEmpty(mTree.CheckedNodes[i].Value))
                {
                    count = mTree.CheckedNodes[i].Value.Count(f => f == '-');
                    if (minLevel == -1)
                    {
                        minLevel = count;
                    }
                    else
                    {
                        if (count < minLevel)
                        {
                            minLevel = count;
                        }
                    }
                    treePathes.Add(mTree.CheckedNodes[i].Value);
                }
            }
        if (PWECommentMerge.Checked)
        {
            mGrid.Columns[5].Visible = mGrid.Columns[6].Visible = mGrid.Columns[7].Visible = mGrid.Columns[8].Visible = false;
        }
        else
        {
            mGrid.Columns[5].Visible = mGrid.Columns[6].Visible = mGrid.Columns[7].Visible = mGrid.Columns[8].Visible = true;
        }
        DataTable dt = AccController.GetBook(null, start, end, Null.NullInteger, Helper.GetInt(mProjects.SelectedValue, Null.NullInteger)
            , Helper.GetInt(mPersons.SelectedValue, Null.NullInteger), SiteSettings.ProgramID
            , Helper.GetInt(mEquipments.SelectedValue, Null.NullInteger)
            , Helper.GetInt(mCostCenters.SelectedValue, Null.NullInteger)
            , treePathes.ToArray()
            , mFromVoucherNo.Text.Trim(), mToVoucherNo.Text.Trim(), SortCol, SortOrder, mShowInitRemain.Checked
            , mnotShowDetail.Checked, mnotShowDetailByPerson.Checked, mjustOwe.Checked, mjustDue.Checked, mComments.Text.Trim()
            , Helper.GetDecimal(mAmount.Text, -1), Helper.GetDecimal(mPOrM.Text, -1), GetDBs(), minLevel, ShowAllVouchers());
        mGrid.DataSource = dt;
        mGrid.DataBind();
        if (null != dt)
            dt.Dispose();
    }
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblDocumentDate = (Label)e.Row.FindControl("lblDocumentDate");
            System.Data.DataRowView record = e.Row.DataItem as System.Data.DataRowView;
            lblDocumentDate.Text = Helper.ToFaFormat(Helper.GetDateTime(record["Date"], Helper.NullDate), "yyyy/MM/dd");
            decimal _owe = Helper.GetDecimal(record["Owe"], 0);
            decimal _due = Helper.GetDecimal(record["Due"], 0);
           // string treePath = record["TreePath"].ToString();
            Label owe = (Label)e.Row.FindControl("owe");
            owe.Text = SiteSettings.ToMonetary(_owe, DateTime.Now).ToString("N0");

            Label due = (Label)e.Row.FindControl("due");
            due.Text = SiteSettings.ToMonetary(_due, DateTime.Now).ToString("N0");
           // if (!string.IsNullOrEmpty(treePath))
          //  {
                sumDue += _due;
                sumOwe += _owe;
          //  }
            decimal _r = SiteSettings.ToMonetary(_due - _owe, DateTime.Now);
            if (mnotShowDetail.Checked || mnotShowDetailByPerson.Checked)
            {
                ((Label)e.Row.FindControl("remaining")).Text = ((_r) < 0)
                    ? (-1 * (_r)).ToString("N4", Helper.NumberCulture).Replace(".0000", "")
                    : (_r).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                e.Row.Cells[13].Text = ((_r) < 0) ? LangProvider.Instance["Li"] : LangProvider.Instance["Cr"];
            }
            else
            {
                _r = SiteSettings.ToMonetary(sumDue - sumOwe, DateTime.Now);
                ((Label)e.Row.FindControl("remaining")).Text = ((_r) < 0)
                    ? (-1 * (_r)).ToString("N4", Helper.NumberCulture).Replace(".0000", "")
                    : (_r).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                e.Row.Cells[13].Text = ((_r) < 0) ? LangProvider.Instance["Li"] : LangProvider.Instance["Cr"];
            }
            if (PWECommentMerge.Checked)
            {
                string cmd = record["Comments"].ToString();
                if (!string.IsNullOrEmpty(record["PersonnelName"].ToString()))
                {
                    cmd += "/ " + LangProvider.Instance["Person"] + " : " + record["PersonnelName"];
                }
                if (!string.IsNullOrEmpty(record["WardName"].ToString()))
                {
                    cmd += "/  " + LangProvider.Instance["Project"] + " : " + record["WardName"];
                }
                if (!string.IsNullOrEmpty(record["EquipmentName"].ToString()))
                {
                    cmd += "/  " + LangProvider.Instance["ReportEquipment"] + " : " + record["EquipmentName"];
                }
                if (!string.IsNullOrEmpty(record["CostCenterName"].ToString()))
                {
                    cmd += "/  " + LangProvider.Instance["CostCenter"] + " : " + record["CostCenterName"];
                }
                e.Row.Cells[9].Text = cmd;
            }
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblTotal = (Label)e.Row.FindControl("oweTotal");
            lblTotal.Text = SiteSettings.ToMonetary(sumOwe, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            lblTotal = (Label)e.Row.FindControl("dueTotal");
            lblTotal.Text = SiteSettings.ToMonetary(sumDue, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            lblTotal = (Label)e.Row.FindControl("remainingTotal");
            decimal _r = SiteSettings.ToMonetary(sumDue - sumOwe, DateTime.Now);
            lblTotal.Text = ((_r) < 0) ? (-1 * (_r)).ToString("N4", Helper.NumberCulture).Replace(".0000", "") : (_r).ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            //  if ((lastNature == TopicNature.Owe && (sumDue - sumOwe) > 0) || (lastNature == TopicNature.Due && (sumDue - sumOwe) < 0))
            //    Helper.SignifyNegative(e.Row.Cells[13]);

            lblTotal = (Label)e.Row.FindControl("finalDiagnosis");
            lblTotal.Text = ((_r) < 0) ? LangProvider.Instance["Li"] : LangProvider.Instance["Cr"];
        }
    }

    protected void mGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        BindGrid();
    }
    private bool ShowAllVouchers()
    {
        return mShowInProgressVouchers.Checked;
    }
    protected string SortCol
    {
        get
        {
            return Helper.GetString(ViewState["SortCol"], null);
        }
        set
        {
            ViewState["SortCol"] = value;
        }
    }
    protected string SortOrder
    {
        get
        {
            return Helper.GetString(ViewState["SortOrder"], "asc");
        }
        set
        {
            ViewState["SortOrder"] = value;
        }
    }

    protected void doExcel_Click(object sender, EventArgs e)
    {
        Exporter.ExportExcel(Response, mTree.SelectedNode == null ? mTree.Nodes[0].Text : mTree.SelectedNode.Text, mGrid);
    }
}