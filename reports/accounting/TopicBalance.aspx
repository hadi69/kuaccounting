﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TopicBalance.aspx.cs" Inherits="reports_accounting_TopicBalance" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>

    <div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label26" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Person</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mPersons" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" AssociatedControlID="mFromDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label3" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label ID="Label4" runat="server">Lang.From</asp:Label>
                        <asp:Label ID="Label7" runat="server">Lang.DocumentNumber</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromVoucherNo" runat="server" CssClass="T2 form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label ID="Label5" runat="server">Lang.To</asp:Label>
                        <asp:Label ID="Label6" runat="server">Lang.DocumentNumber</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToVoucherNo" runat="server" CssClass="T2 form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label9" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Comments</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:TextBox ID="mComments" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4">&nbsp;</div>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox runat="server" ID="mjustOwe" Text="Lang.justOwe" CssClass="mjustOwe" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4">&nbsp;</div>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox runat="server" ID="mjustDue" Text="Lang.justDue" CssClass="mjustDue" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin-5-top">
            <div class="col-md-8 col-lg-9 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container padding-0-left">
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcel_Click" Enabled="false" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <input type="button" id="print" class="btn btn-xs btn-dark-blue btn-block" value="" />
                </div>
            </div>
        </div>
    </div>

    <table class="full-width">
        <tr>
            <td valign="top">
                <div class="well callout">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tree">
                            <asp:Label runat="server">Lang.TopicGroup</asp:Label></a></li>
                        <li><a data-toggle="tab" href="#project">
                            <asp:Label ID="Label10" runat="server">Lang.Project</asp:Label></a></li>
                        <li><a data-toggle="tab" href="#equipment">
                            <asp:Label ID="Label8" runat="server">Lang.ReportEquipment</asp:Label></a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="tree" class="tab-pane fade in active">
                            <asp:TreeView ID="mTree" runat="server" CssClass="Tree" ShowLines="True" ShowCheckBoxes="All">
                                <SelectedNodeStyle CssClass="TreeSelected" />
                            </asp:TreeView>
                        </div>
                        <div id="project" class="tab-pane fade">
                            <asp:CheckBoxList ID="mProjects" runat="server" CssClass="chosen-rtl" />
                        </div>
                        <div id="equipment" class="tab-pane fade">
                            <asp:CheckBoxList ID="mEquipments" runat="server" CssClass="chosen-rtl" />
                        </div>
                    </div>
                </div>
            </td>
            <td valign="top">
                <div class="col-xs-12">
                    <asp:Label runat="server" ID="msg" CssClass="label-30 alert alert-danger block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
                </div>
                <asp:PlaceHolder ID="holder" runat="server" />
            </td>
        </tr>
    </table>
    <script>
        $(function () {
            $('#print').attr('value', Lang.Print);
            $('#print').click(function () {
                var head = Lang.Report1;
                printTable('.ScrollPanelCartable', '../../', head);
            });

            $("#bar").prepend(breadcrumb({
                title: Lang.TopicBalance,
                parent: {
                    title: Lang.Reports,
                    url: 'menu/reports.aspx'
                }
            }));
            setClassReports();
            dateSelect('.dateselect');
        });
    </script>
</asp:Content>
