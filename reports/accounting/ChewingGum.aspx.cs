﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reports_accounting_ChewingGum : System.Web.UI.Page
{
    System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
    protected void Page_Load(object sender, EventArgs e)
    {
        msg.Visible = false;
        mGrid.EmptyDataText = LangProvider.Instance["DocumentIsEmpty"];
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");
        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["ChewingGum"];
            Page.SetLang();
            int year = cal.GetYear(DateTime.Now);
            //mFromDate.Text = string.Format("{0}/01/01", year - 1300);
            //mToDate.Text = Helper.FormatDate(cal.ToDateTime(year + 1, 1, 1, 1, 1, 1, 1).AddDays(-1), "yy/MM/dd");
            IDataReader financeYearStartDate = DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");//0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            //mFromDate.Text = 
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);

            mReportSetting.ReportName = "ChewingGum";
        }
        mReportSetting.SaveClick += new EventHandler(mReportSetting_SaveClick);
        mReportSetting.ReportSettingSelect += new EventHandler(mReportSetting_ReportSettingSelect);
    }

    void mReportSetting_ReportSettingSelect(object sender, EventArgs e)
    {
        ReportSettingInfo info = mReportSetting.SelectedReportSettings;
        if (info == null)
            return;
        string[] lines = info.Settings.Split('\n');
        mFromDate.Text = lines[0].Trim('\r');
        mToDate.Text = lines[1].Trim('\r');
        mNumMainBakeFrom.Text = lines[2].Trim('\r');
        mNumMainBakeTo.Text = lines[3].Trim('\r');
        mAvgWeight1From.Text = lines[4].Trim('\r');
        mAvgWeight1To.Text = lines[5].Trim('\r');
        mAvgWeight2From.Text = lines[6].Trim('\r');
        mAvgWeight2To.Text = lines[7].Trim('\r');
        mNumBoxedFrom.Text = lines[8].Trim('\r');
        mNumBoxedTo.Text = lines[9].Trim('\r');
    }

    void mReportSetting_SaveClick(object sender, EventArgs e)
    {
        StringBuilder b = new StringBuilder();
        b.AppendLine(mFromDate.Text); //0
        b.AppendLine(mToDate.Text); //1
        b.AppendLine(mNumMainBakeFrom.Text);
        b.AppendLine(mNumMainBakeTo.Text);
        b.AppendLine(mAvgWeight1From.Text);
        b.AppendLine(mAvgWeight1To.Text);
        b.AppendLine(mAvgWeight2From.Text);
        b.AppendLine(mAvgWeight2To.Text);
        b.AppendLine(mNumBoxedFrom.Text);
        b.AppendLine(mNumBoxedTo.Text);
        mReportSetting.Save(b.ToString());
    }

    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.Common.DbDataRecord record = e.Row.DataItem as System.Data.Common.DbDataRecord;
            DateTime dt = Helper.GetDateTime(record["ProductDate"], Helper.NullDate);
            if (dt != Helper.NullDate)
                e.Row.Cells[1].Text = Helper.FormatDate(dt, null);

            ((Label)e.Row.FindControl("NumMainBake")).Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["NumMainBake"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("NumRecycleBake")).Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["NumRecycleBake"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("NumRawBake")).Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["NumRawBake"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("NumSugarFreeBake")).Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["NumSugarFreeBake"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("NumSugar")).Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["NumSugar"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("NumZyl")).Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["NumZyl"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("NumReturn")).Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["NumReturn"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("NoBox")).Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["NoBox"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("AvgWeight1")).Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["AvgWeight1"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("AvgWeight2")).Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["AvgWeight2"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("NumDump")).Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["NumDump"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("NumBoxed")).Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["NumBoxed"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("NumBoxShrink")).Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["NumBoxShrink"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("PersonHour")).Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["PersonHour"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("ReturnToBake")).Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["ReturnToBake"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            sumNumMainBake += Helper.GetDecimal(record["NumMainBake"], 0);
            sumNumRecycleBake += Helper.GetDecimal(record["NumRecycleBake"], 0);
            sumNumRawBake += Helper.GetDecimal(record["NumRawBake"], 0);
            sumNumSugarFreeBake += Helper.GetDecimal(record["NumSugarFreeBake"], 0);
            sumNumSugar += Helper.GetDecimal(record["NumSugar"], 0);
            sumNumZyl += Helper.GetDecimal(record["NumZyl"], 0);
            sumNumReturn += Helper.GetDecimal(record["NumReturn"], 0);
            sumNoBox += Helper.GetDecimal(record["NoBox"], 0);
            sumAvgWeight1 += Helper.GetDecimal(record["AvgWeight1"], 0);
            sumAvgWeight1 += Helper.GetDecimal(record["AvgWeight2"], 0);
            sumNumDump += Helper.GetDecimal(record["NumDump"], 0);
            sumNumBoxed += Helper.GetDecimal(record["NumBoxed"], 0);
            sumNumBoxShrink += Helper.GetDecimal(record["NumBoxShrink"], 0);
            sumPersonHour += Helper.GetDecimal(record["PersonHour"], 0);
            sumReturnToBake += Helper.GetDecimal(record["ReturnToBake"], 0);
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            ((Label)e.Row.FindControl("NumMainBakeTotal")).Text = SiteSettings.ToMonetary(sumNumMainBake, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("NumRecycleBakeTotal")).Text = SiteSettings.ToMonetary(sumNumRecycleBake, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("NumRawBakeTotal")).Text = SiteSettings.ToMonetary(sumNumRawBake, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("NumSugarFreeBakeTotal")).Text = SiteSettings.ToMonetary(sumNumSugarFreeBake, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("NumSugarTotal")).Text = SiteSettings.ToMonetary(sumNumSugar, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("NumZylTotal")).Text = SiteSettings.ToMonetary(sumNumZyl, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("NumReturnTotal")).Text = SiteSettings.ToMonetary(sumNumReturn, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("NoBoxTotal")).Text = SiteSettings.ToMonetary(sumNoBox, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("AvgWeight1Total")).Text = SiteSettings.ToMonetary(sumAvgWeight1, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("AvgWeight2Total")).Text = SiteSettings.ToMonetary(sumAvgWeight2, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("NumDumpTotal")).Text = SiteSettings.ToMonetary(sumNumDump, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("NumBoxedTotal")).Text = SiteSettings.ToMonetary(sumNumBoxed, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("NumBoxShrinkTotal")).Text = SiteSettings.ToMonetary(sumNumBoxShrink, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("PersonHourTotal")).Text = SiteSettings.ToMonetary(sumPersonHour, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("ReturnToBakeTotal")).Text = SiteSettings.ToMonetary(sumReturnToBake, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }
    }
    decimal sumNumMainBake = 0, sumNumRecycleBake = 0, sumNumRawBake = 0, sumNumSugarFreeBake = 0, sumNumSugar = 0, sumNumZyl = 0
          , sumNumReturn = 0, sumNoBox = 0, sumAvgWeight1 = 0, sumAvgWeight2 = 0, sumNumDump = 0, sumNumBoxed = 0, sumNumBoxShrink = 0, sumPersonHour = 0, sumReturnToBake = 0;
    protected void mGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        BindGrid();
    }
    protected void BindGrid()
    {
        sumNumMainBake = sumNumRecycleBake = sumNumRawBake = sumNumSugarFreeBake = sumNumSugar = sumNumZyl
            = sumNumReturn = sumNoBox = sumAvgWeight1 = sumAvgWeight2 = sumNumDump = sumNumBoxed = sumNumBoxShrink
            = sumPersonHour = sumReturnToBake = 0;
        DateTime start = Helper.ParseDate(mFromDate.Text, Null.NullDate);
        DateTime end = Helper.ParseDate(mToDate.Text, Null.NullDate);
        if (Null.IsNull(start))
        {
            msg.Text = LangProvider.Instance["InvalidStartDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(end))
        {
            msg.Text = LangProvider.Instance["InvalidEndDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(start) || Null.IsNull(end))
            return;
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);

        //mProductType.Text

        string qry = string.Format(@"SELECT * FROM ChewingGum WHERE {0} <= ProductDate AND ProductDate <= {1} "
            , Helper.QoutedDateTime(start), Helper.QoutedDateTime(end));

        decimal d = Helper.GetDecimal(mNumMainBakeFrom.Text, -1);
        if (d != Null.NullInteger)
            qry += " AND NumMainBake >= " + d;
        d = Helper.GetDecimal(mNumMainBakeTo.Text, -1);
        if (d != Null.NullInteger)
            qry += " AND NumMainBake <= " + d;

        d = Helper.GetDecimal(mAvgWeight1From.Text, -1);
        if (d != Null.NullInteger)
            qry += " AND AvgWeight1 >= " + d;
        d = Helper.GetDecimal(mAvgWeight1To.Text, -1);
        if (d != Null.NullInteger)
            qry += " AND AvgWeight1 <= " + d;

        d = Helper.GetDecimal(mAvgWeight2From.Text, -1);
        if (d != Null.NullInteger)
            qry += " AND AvgWeight2 >= " + d;
        d = Helper.GetDecimal(mAvgWeight2To.Text, -1);
        if (d != Null.NullInteger)
            qry += " AND AvgWeight2 <= " + d;

        d = Helper.GetDecimal(mNumBoxedFrom.Text, -1);
        if (d != Null.NullInteger)
            qry += " AND NumBoxed >= " + d;
        d = Helper.GetDecimal(mNumBoxedTo.Text, -1);
        if (d != Null.NullInteger)
            qry += " AND NumBoxed <= " + d;

        if (!string.IsNullOrEmpty(SortCol))
            qry += " ORDER BY " + SortCol + " " + SortOrder;
        IDataReader reader = DbProxy.Instance.ExecuteReader(qry);
        try
        {
            mGrid.DataSource = reader;
            mGrid.DataBind();
        }
        finally
        {
            if (null != reader)
            {
                reader.Close();
                reader.Dispose();
            }
        }
    }
    protected string SortCol
    {
        get
        {
            return Helper.GetString(ViewState["SortCol"], null);
        }
        set
        {
            ViewState["SortCol"] = value;
        }
    }
    protected string SortOrder
    {
        get
        {
            return Helper.GetString(ViewState["SortOrder"], "asc");
        }
        set
        {
            ViewState["SortOrder"] = value;
        }
    }
    protected void doShow_Click(object sender, EventArgs e)
    {
        BindGrid();

        mDetailPanel.Visible = true;

        doExcel.Enabled = true;
    }
    protected void doExcel_Click(object sender, EventArgs e)
    {
        Exporter.ExportExcel(Response, mTitle.Text, mGrid);
    }
}