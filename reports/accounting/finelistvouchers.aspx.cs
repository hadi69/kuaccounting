﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class reports_accounting_finelistvouchers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //={0}&={1}&={2}&={3}&={4}&={5}&={6}&={7}"
        if (!IsPostBack)
        {
            Page.SetLang();
            mGrid.EmptyDataText=LangProvider.Instance["DocumentIsEmpty"];
            int accountID = Helper.GetInt(Request.QueryString["aid"], Null.NullInteger);
            int programID = Helper.GetInt(Request.QueryString["pid"], Null.NullInteger);
            int depth = Helper.GetInt(Request.QueryString["dp"], Null.NullInteger);
            if (accountID == Null.NullInteger || programID == Null.NullInteger)
                return;
            IDataReader dr = AccController.GetFineVouchers(depth
                 , programID, accountID
                 , Helper.ParseDate(Request.QueryString["df"], DateTime.Now.AddYears(-50))
                 , Helper.ParseDate(Request.QueryString["dt"], DateTime.Now.AddYears(50))
                 , Helper.GetInt(Request.QueryString["vfid"], 0), Helper.GetInt(Request.QueryString["vtid"], int.MaxValue - 1)
                 , Helper.GetInt(Request.QueryString["sid"], Null.NullInteger));
            
            try
            {
                mGrid.DataSource = dr;
                mGrid.DataBind();
            }
            finally
            {
                if (null != dr)
                {
                    dr.Close();
                    dr.Dispose();
                }
            }
        }
    }
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[0].Text = (e.Row.RowIndex + 1).ToString();
            e.Row.Cells[2].Text = AccController.GetVoucherTitle(Helper.GetInt((e.Row.DataItem as System.Data.Common.DbDataRecord)["DocumentAccountingID"], -1));
            e.Row.Cells[3].Text = Helper.FormatDate((DateTime)(e.Row.DataItem as System.Data.Common.DbDataRecord)["Date"], "yy-MM-dd");
            // e.Row.Cells[3].Text = Controller.GetTreeFullPathCode((e.Row.DataItem as System.Data.Common.DbDataRecord)["TreePath"].ToString());
            //e.Row.Cells[3].Text = Controller.GetTreeFullPathCode((e.Row.DataItem as System.Data.Common.DbDataRecord)["TreePath"]);
        }
    }
}