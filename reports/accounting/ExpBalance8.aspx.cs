﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reports_accounting_ExpBalance8 : System.Web.UI.Page
{
    System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
    protected void Page_Load(object sender, EventArgs e)
    {
        msg.Visible = false;
        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["ExpBalance8"];
            Page.SetLang();
            mReportSetting.ReportName = "ExpBalance8";
            mTitle.Text = LangProvider.Instance["ExpBalance8"];
            BindTree();
            BindWards();

            int year = cal.GetYear(DateTime.Now);
            //mFromDate.Text = string.Format("{0}/01/01", year - 1300);
            //mToDate.Text = Helper.FormatDate(cal.ToDateTime(year + 1, 1, 1, 1, 1, 1, 1).AddDays(-1), "yy/MM/dd");
            IDataReader financeYearStartDate = DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");//0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            //mFromDate.Text = 
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);
        }
        mReportSetting.SaveClick += new EventHandler(mReportSetting_SaveClick);
        mReportSetting.ReportSettingSelect += new EventHandler(mReportSetting_ReportSettingSelect);
    }

    void mReportSetting_ReportSettingSelect(object sender, EventArgs e)
    {
        ReportSettingInfo info = mReportSetting.SelectedReportSettings;
        if (info == null)
            return;
        string[] lines = info.Settings.Split('\n');
        mFromDate.Text = lines[0].Trim('\r');
        mToDate.Text = lines[1].Trim('\r');
        mFromVoucherID.Text = lines[2].Trim('\r');
        mToVoucherID.Text = lines[3].Trim('\r');
        mTotals.SelectedValue = lines[4].Trim('\r');
        mWards.SelectedValue = lines[5].Trim('\r');
    }

    void mReportSetting_SaveClick(object sender, EventArgs e)
    {
        StringBuilder b = new StringBuilder();
        b.AppendLine(mFromDate.Text); //0
        b.AppendLine(mToDate.Text); //1
        b.AppendLine(mFromVoucherID.Text);  //2
        b.AppendLine(mToVoucherID.Text);    //3
        b.AppendLine(mTotals.SelectedValue); //4
        b.Append(mWards.SelectedValue);  //5
        mReportSetting.Save(b.ToString());
    }
    #region Bindings
    void BindTree()
    {
        mTotals.Items.Clear();
        mTotals.Items.Add(new ListItem(LangProvider.Instance["AllTopics"], ""));
        IDataReader arr = AccController.GetTreeNames(SiteSettings.ProgramID);
        if (arr != null)
        {
            while (arr.Read())
            {
                if (!arr.IsDBNull(0) && !arr.IsDBNull(1))
                    mTotals.Items.Add(new ListItem(arr.GetString(1), arr.GetString(0)));
            }
            arr.Close();
        }
        mTotals.SelectedIndex = 0;
    }
    void BindWards()
    {
        mWards.DataTextField = "Name";
        mWards.DataValueField = "ID";

        mWards.DataSource = DbProxy.Instance.FillData("SELECT -1 AS ID, '' AS Name UNION SELECT ID, Name FROM Ward", "ward"); ;
        mWards.DataBind();

        mWards.SelectedIndex = 0;
    }


    #endregion
    class MyRow
    {
        public string ActualTreePath { get; set; }
        public string TreePath { get; set; }
        public string TreePathName { get; set; }
        public decimal Owe { get; set; }
        public decimal Due { get; set; }
        public decimal OweStart { get; set; }
        public decimal DueStart { get; set; }
    }
    string rowStyle = "GridAltRow";
    protected void doShow_Click(object sender, EventArgs e)
    {
        DateTime start = Helper.ParseDate(mFromDate.Text, Null.NullDate);
        DateTime end = Helper.ParseDate(mToDate.Text, Null.NullDate);
        if (Null.IsNull(start))
        {
            msg.Text = LangProvider.Instance["InvalidStartDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(end))
        {
            msg.Text = LangProvider.Instance["InvalidEndDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(start) || Null.IsNull(end))
            return;
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);

        // Normal Flow
        IDataReader items = AccController.GetExpBalance(
            Helper.GetString(mTotals.SelectedValue, "")
            , start
            , end
            , Helper.GetInt(mWards.SelectedValue, Null.NullInteger), SiteSettings.ProgramID, Null.NullInteger, Null.NullInteger, Null.NullInteger, null, null, !mAllLevels.Checked);

        Dictionary<string, MyRow> rows = new Dictionary<string, MyRow>();
        while (items.Read())
        {
            MyRow row = new MyRow()
            {
                ActualTreePath = items.GetString(0),
                TreePath = items.IsDBNull(4) ? "" : items.GetString(4)
                ,
                TreePathName = items.IsDBNull(1) ? "" : items.GetString(1),
                Owe = SiteSettings.ToMonetary(items.IsDBNull(2) ? 0 : items.GetDecimal(2), DateTime.Now),
                Due = SiteSettings.ToMonetary(items.IsDBNull(3) ? 0 : items.GetDecimal(3), DateTime.Now)
            };
            if (!rows.ContainsKey(row.TreePath))
                rows.Add(row.TreePath, row);
        }
        items.Close();
        items.Dispose();

        // Opening Flow
        // Mode:    0:Normal;1:Bedridden;2:Clinic;3:Check;4:Purchase;5:Refuse;6:Return;7:StoreDraft;8:PayCheck;11:Closing;12:Final;13:Opening;14:CheckOwe;15:PayCheckOwe
        items = AccController.GetExpBalance(
            Helper.GetString(mTotals.SelectedValue, "")
            , start
            , end
            , Helper.GetInt(mWards.SelectedValue, Null.NullInteger), SiteSettings.ProgramID, 13, Null.NullInteger, Null.NullInteger, null, null, !mAllLevels.Checked);

        while (items.Read())
        {
            MyRow row = new MyRow()
            {
                ActualTreePath = items.GetString(0),
                TreePath = items.IsDBNull(4) ? "" : items.GetString(4)
                ,
                TreePathName = items.IsDBNull(1) ? "" : items.GetString(1)
                ,
                OweStart = SiteSettings.ToMonetary(items.IsDBNull(2) ? 0 : items.GetDecimal(2), DateTime.Now)
                ,
                DueStart = SiteSettings.ToMonetary(items.IsDBNull(3) ? 0 : items.GetDecimal(3), DateTime.Now)
            };
            if (!rows.ContainsKey(row.TreePath))
                rows.Add(row.TreePath, row);
            else
            {
                rows[row.TreePath].OweStart = row.OweStart;
                rows[row.TreePath].DueStart = row.DueStart;
            }
        }
        items.Close();
        items.Dispose();

        mPC.Controls.Clear();
        mPC.Controls.Add(new LiteralControl("<div>"));
        mPC.Controls.Add(new LiteralControl("<table class='ScrollPanelCartable table table-bordered table-condensed'>"));
        // --------------------------------------------------------
        // First Row
        mPC.Controls.Add(new LiteralControl("<tr class='GridHeader'>"));
        mPC.Controls.Add(new LiteralControl(string.Format("<td rowspan='2'>{0}</td>", LangProvider.Instance["ColRowNr"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<td rowspan='2'>{0}</td>", LangProvider.Instance["Topic"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<td rowspan='2'>{0}</td>", LangProvider.Instance["TopicCode"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<td colspan=2>{0}</td>", LangProvider.Instance["TopicFlowOpening"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<td colspan=2>{0}</td>", LangProvider.Instance["TotalBalanceOpening"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<td colspan=2>{0}</td>", LangProvider.Instance["TopicFlow"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<td colspan=2>{0}</td>", LangProvider.Instance["TotalBalance"])));
        mPC.Controls.Add(new LiteralControl("</tr>"));
        // --------------------------------------------------------
        // Second Row
        mPC.Controls.Add(new LiteralControl("<tr class='GridHeader'>"));
        mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td><td>{1}</td>", LangProvider.Instance["OweFlow"], LangProvider.Instance["DueFlow"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td><td>{1}</td>", LangProvider.Instance["OweRemain"], LangProvider.Instance["DueRemain"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td><td>{1}</td>", LangProvider.Instance["OweFlow"], LangProvider.Instance["DueFlow"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td><td>{1}</td>", LangProvider.Instance["OweRemain"], LangProvider.Instance["DueRemain"])));
        mPC.Controls.Add(new LiteralControl("</tr>"));
        // --------------------------------------------------------

        int k = 1;
        decimal[] sums = { 0, 0, 0, 0, 0, 0, 0, 0 };
        Dictionary<string, MyRow>.Enumerator enu = rows.GetEnumerator();
        while (enu.MoveNext())
        {
            MyRow row = enu.Current.Value;
            if (mAllLevels.Checked)
            {
                sums[0] += row.OweStart;
                sums[1] += row.DueStart;
                sums[2] += Math.Max(0, row.OweStart - row.DueStart);
                sums[3] += Math.Max(0, row.DueStart - row.OweStart);
                sums[4] += row.Owe;
                sums[5] += row.Due;
                sums[6] += Math.Max(0, row.Owe - row.Due);
                sums[7] += Math.Max(0, row.Due - row.Owe);
                mPC.Controls.Add(new LiteralControl(string.Format("<tr class='{0}'>", (k % 2) == 0 ? "GridRow" : "GridAltRow")));
            }
            else
            {
                string treePath = row.ActualTreePath;
                int lastIndex = treePath.LastIndexOf('-');
                if (lastIndex > 0)
                {
                    mPC.Controls.Add(new LiteralControl(string.Format("<tr class='{0} treegrid-{1} treegrid-parent-{2}'>", (k % 2) == 0 ? "GridRow" : "GridAltRow", treePath, treePath.Substring(0, lastIndex))));
                }
                else
                {
                    sums[0] += row.OweStart;
                    sums[1] += row.DueStart;
                    sums[2] += Math.Max(0, row.OweStart - row.DueStart);
                    sums[3] += Math.Max(0, row.DueStart - row.OweStart);
                    sums[4] += row.Owe;
                    sums[5] += row.Due;
                    sums[6] += Math.Max(0, row.Owe - row.Due);
                    sums[7] += Math.Max(0, row.Due - row.Owe);
                    mPC.Controls.Add(new LiteralControl(string.Format("<tr class='{0} treegrid-{1}'>", (k % 2) == 0 ? "GridRow" : "GridAltRow", treePath)));
                }
            }

            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", k++)));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", row.TreePath)));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", row.TreePathName)));

            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", row.OweStart.ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", row.DueStart.ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", Math.Max(0, row.OweStart - row.DueStart).ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", Math.Max(0, row.DueStart - row.OweStart).ToString("N0"))));

            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", row.Owe.ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", row.Due.ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", Math.Max(0, row.Owe - row.Due).ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", Math.Max(0, row.Due - row.Owe).ToString("N0"))));

            mPC.Controls.Add(new LiteralControl("</tr>"));
        }

        //// the above list was only for used Trees. for unused Trees, show 0
        //List<TreeInfo> infos = null;
        //if (Helper.GetString(mTotals.SelectedValue, "") == "")
        //    infos = AccController.GetTrees(mAllLevels.Checked ? Null.NullInteger : 0, SiteSettings.ProgramID);
        //else
        //{
        //    if (mAllLevels.Checked)
        //        infos = AccController.GetTrees(Null.NullInteger, SiteSettings.ProgramID, mTotals.SelectedValue);
        //    else
        //        infos = new List<TreeInfo>();
        //    infos.Insert(0, AccController.GetTree(mTotals.SelectedValue));
        //}
        //for (int i = 0; i < infos.Count; i++)
        //{
        //    if (rows.ContainsKey(infos[i].TreePath))
        //        continue;
        //    mPC.Controls.Add(new LiteralControl(string.Format("<tr class='{0}'>", (k % 2) == 0 ? "GridRow" : "GridAltRow")));
        //    mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", k++)));
        //    mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", infos[i].TopicCode)));
        //    mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", AccController.GetTreeFullPath(infos[i].TreePath))));

        //    mPC.Controls.Add(new LiteralControl("<td>0</td><td>0</td><td>0</td><td>0</td>"));
        //    mPC.Controls.Add(new LiteralControl("<td>0</td><td>0</td><td>0</td><td>0</td>"));

        //    mPC.Controls.Add(new LiteralControl("</tr>"));
        //}

        // ----------------------------------
        // Sums
        mPC.Controls.Add(new LiteralControl("<tr class='GridFooter'>"));
        mPC.Controls.Add(new LiteralControl("<td></td>"));
        mPC.Controls.Add(new LiteralControl("<td></td>"));
        mPC.Controls.Add(new LiteralControl("<td></td>"));
        for (int i = 0; i < sums.Length; i++)
        {
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", sums[i].ToString("N0"))));
        }
        mPC.Controls.Add(new LiteralControl("</tr>"));
        mPC.Controls.Add(new LiteralControl("</table></div>"));

    }
    protected void doExcel_Click(object sender, EventArgs e)
    {
        doShow_Click(null, null);
        string text = "";
        for (int i = 0; i < mPC.Controls.Count; i++)
            if (mPC.Controls[i] is LiteralControl)
                text += (mPC.Controls[i] as LiteralControl).Text;
        Exporter.ExportExcelSimple(Response, mTitle.Text, text);
    }
}