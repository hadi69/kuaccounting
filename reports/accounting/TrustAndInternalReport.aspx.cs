﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;

public partial class reports_accounting_TrustAndInternalReport : System.Web.UI.Page
{
    private System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();

    protected void Page_Load(object sender, EventArgs e)
    {
        msg.Visible = false;
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");

        mGrid.EmptyDataText = LangProvider.Instance["ListIsEmpty"];

        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["TrustAndInternalReport"];
            Page.SetLang();
            IDataReader financeYearStartDate =
                DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");
            //0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            //mFromDate.Text = 
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);
            Initial();

            mReportSetting.ReportName = "TrustAndInternalReport";
        }

    }

    private void mReportSetting_ReportSettingSelect(object sender, EventArgs e)
    {
        ReportSettingInfo info = mReportSetting.SelectedReportSettings;
        if (info == null)
            return;
        string[] lines = info.Settings.Split('\n');
        mPersons.SelectedValue = lines[0].Trim('\r');
        mFromDate.Text = lines[2].Trim('\r');
        mToDate.Text = lines[3].Trim('\r');
    }

    private void mReportSetting_SaveClick(object sender, EventArgs e)
    {
        StringBuilder b = new StringBuilder();
        b.AppendLine(mPersons.SelectedValue); //0
        b.AppendLine(mFromDate.Text); //1
        b.AppendLine(mToDate.Text); //2
        mReportSetting.Save(b.ToString());
    }

    private void Initial()
    {
        {
            IDataReader persons =
                DbProxy.Instance.ExecuteReader(
                    "SELECT ID, [dbo].[DisplayPerson](ID, 1), ISNULL(Code, '') FROM Personnel WHERE Enabled = 1 AND Type=101");

            mPersons.Items.Clear();
            mTransferee.Items.Clear();

            mPersons.Items.Add(new ListItem(LangProvider.Instance["AllPersons"], ""));
            mTransferee.Items.Add(new ListItem(LangProvider.Instance["AllPersons"], ""));

            if (persons != null)
            {
                while (persons.Read())
                {
                    if (!persons.IsDBNull(0) && !persons.IsDBNull(1) && !persons.IsDBNull(2))
                    {
                        mPersons.Items.Add(new ListItem(persons.GetString(1) + " - کد: " + persons.GetString(2), persons.GetInt32(0).ToString()));
                        mTransferee.Items.Add(new ListItem(persons.GetString(1) + " - کد: " + persons.GetString(2), persons.GetInt32(0).ToString()));
                    }
                }
                persons.Close();
            }
            mPersons.SelectedIndex = 0;
        }

        IDataReader companyUnit = DbProxy.Instance.ExecuteReader("SELECT ID, FullName FROM WardView WHERE NOT ID IN (SELECT ISNULL(ParentID, 0) FROM Ward) AND (CompanyUnit = 1 OR NOT CompanyUnit IS NULL)");
        mCompanyUnits.Items.Clear();
        if (companyUnit != null)
        {
            while (companyUnit.Read())
                if (!companyUnit.IsDBNull(0) && !companyUnit.IsDBNull(1))
                {
                    mCompanyUnits.Items.Add(new ListItem(companyUnit.GetString(1), companyUnit.GetInt32(0).ToString()));
                }
            companyUnit.Close();
        }
    }

    protected void DlBaseChange(object sender, EventArgs e)
    {
        DropDownList comboBox = (DropDownList)sender;
        int selected = Int32.Parse(comboBox.SelectedValue);
        if (selected == 1)
        {
            personLable.Text = LangProvider.Instance["ActorName"];
            panelDropDowns.Visible = panelInternal.Visible = true;
            //  mActorName.Visible = ActorNameLabel.Visible = mTransferee.Visible = TransfereeLabel.Visible = true;
        }
        else
        {
            personLable.Text = LangProvider.Instance["Person"];
            panelDropDowns.Visible = panelInternal.Visible = false;
            //mActorName.Visible = ActorNameLabel.Visible = mTransferee.Visible = TransfereeLabel.Visible = false;
        }
    }

    protected void doShow_Click(object sender, EventArgs e)
    {
        if (mAggregate.Checked)
        {
            //mGrid.Columns[10].Visible = false;
            //mGrid.Columns[6].HeaderText = LangProvider.Instance["PeriodBuy"];
            //mGrid.Columns[7].HeaderText = LangProvider.Instance["PeriodPursant"];
            //mGrid.Columns[8].HeaderText = LangProvider.Instance["PeriodCash"];
            //mGrid.Columns[9].HeaderText = LangProvider.Instance["PeriodCheck"];
            //mGrid.Columns[11].HeaderText = LangProvider.Instance["PriodRemain"];
        }
        else
        {
            //mGrid.Columns[10].Visible = true;
            //mGrid.Columns[6].HeaderText = LangProvider.Instance["FinalPrice"];
            //mGrid.Columns[7].HeaderText = LangProvider.Instance["Pursant"];
            //mGrid.Columns[8].HeaderText = LangProvider.Instance["PayMethodCash"];
            //mGrid.Columns[9].HeaderText = LangProvider.Instance["PayMethodCheck"];
            //mGrid.Columns[11].HeaderText = LangProvider.Instance["PriodRemain"];
        }
        doExcel.Enabled = true;
        BindGrid();

    }

    #region Grid

    private decimal _sumTotalTotal = 0, _sumTotalPrice = 0;
    private int _lastId = -1, _id, _identify;
    string _rowStyle = "GridAltRow";
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.Common.DbDataRecord record = e.Row.DataItem as System.Data.Common.DbDataRecord;
            DateTime dt = DateTime.Now;
            _id = Helper.GetInt(record["ID"], 0);
            _identify = Helper.GetInt(record["Identify"], 0);
            if (_id != _lastId)
            {
                _lastId = _id;
                _rowStyle = _rowStyle == "GridAltRow" ? "GridRow" : "GridAltRow";
                dt = Helper.GetDateTime(record["FactorDate"], Helper.NullDate);
                Label lblGridDate = (Label)e.Row.FindControl("lblGridDate");
                if (dt != Helper.NullDate && lblGridDate != null)
                {
                    lblGridDate.Text = Helper.FormatDateFull(Helper.GetDateTime(dt, Helper.NullDate));
                }
                CheckBox chkPrintID = e.Row.FindControl("chkPrintID") as CheckBox;
                if (chkPrintID != null && _identify == 1)
                {
                    chkPrintID.InputAttributes.Add("data-id", _id.ToString());
                    chkPrintID.CssClass += "data-print";
                    e.Row.Cells[2].Text = string.Format("<a href='../../list.aspx?table=StoreDraft&id={0}&menu=accountinga'>{1}</a>", _id, record["NOSerial"]);
                }
                if (_identify == 2)
                {
                    e.Row.Cells[2].Text = string.Format("<a href='../../list.aspx?table=Purchase&id={0}&menu=accountinga'>{1}</a>", _id, record["NOSerial"]);
                }
            }
            else
            {
                e.Row.Cells[2].Text = "";
                e.Row.Cells[3].Text = "";
                e.Row.Cells[4].Text = "";
                e.Row.Cells[5].Text = "";
                e.Row.Cells[6].Text = "";
                e.Row.Cells[7].Text = "";
                e.Row.Cells[0].Text = "";
            }
            decimal totalPrice = SiteSettings.ToMonetary(Helper.GetDecimal(record["Price"], 0), dt);
            e.Row.Cells[12].Text = totalPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            decimal totalTotal = SiteSettings.ToMonetary(Helper.GetDecimal(record["TotalPrice"], 0), dt);
            e.Row.Cells[13].Text = totalTotal.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.CssClass = _rowStyle;


            _sumTotalPrice += totalPrice;
            _sumTotalTotal += totalTotal;

        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            (e.Row.FindControl("totalPrice") as Label).Text = _sumTotalPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("totalTotalPrice") as Label).Text = _sumTotalTotal.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }
    }

    protected void mGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        BindGrid();
    }

    private void BindGrid()
    {
        DateTime start = Helper.ParseDate(mFromDate.Text, Null.NullDate);
        DateTime end = Helper.ParseDate(mToDate.Text, Null.NullDate);
        if (Null.IsNull(start))
        {
            msg.Text = LangProvider.Instance["InvalidStartDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(end))
        {
            msg.Text = LangProvider.Instance["InvalidEndDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(start) || Null.IsNull(end))
            return;
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);
        gridPanel.Visible = true;

        string cuIDs, pgWhere;
        GetPersonsGroupWhere(out cuIDs, out pgWhere);
        int type = Helper.GetInt(DLBase.SelectedValue, Null.NullInteger);
        int personId = Helper.GetInt(mPersons.SelectedValue, Null.NullInteger);
        int transfereeId = Helper.GetInt(mTransferee.SelectedValue, Null.NullInteger);
        string query = "";
        if (type == 5 || type == 6)
        {
            #region purchase
            query =
                   string.Format(@"SELECT 2 Identify, PurchaseView.ID , NOSerial, ReceiptDate AS FactorDate,
                    SellerID AS PersonID, SellerName AS PersonName
                    , StoreServiceID, StoreServiceName, UnitName,
                    UnitCurrencyConverter, Amount AS DeliveredAmount, Price, (UnitCurrencyConverter * Amount * Price) AS TotalPrice FROM PurchaseView
                    LEFT OUTER JOIN PurchaseItemsView ON PurchaseView.ID = PurchaseItemsView.PurchaseID
                  ");


            string where = string.Format(" WHERE ReceiptDate>={0} AND ReceiptDate<={1}", Helper.QoutedDateTime(start),
                Helper.QoutedDateTime(end));
            if (type != Null.NullInteger)
            {
                //purchaseType// 0:Common,1:IsStartingAmount;2:GuildTrustFrom;3:ConsignmentBuy
                if (type == 5)
                {
                    type = 2;
                }
                if (type == 6)
                {
                    type = 3;
                }
                where += " AND PurchaseType =" + type;
            }

            if (personId != Null.NullInteger)
            {
                where += " AND SellerID =" + personId;
            }
            mGrid.Columns[3].Visible = mGrid.Columns[6].Visible = mGrid.Columns[5].Visible = mGrid.Columns[0].Visible = printButton.Visible = false;
            mGrid.Columns[4].Visible = true;

            query += where;
            if (!string.IsNullOrEmpty(SortCol))
                query += " ORDER BY " + SortCol + " " + SortOrder;
            query += " ORDER BY ReceiptDate, SellerID, DATEADD(dd, 0, DATEDIFF(dd, 0, ReceiptDate)), NoSerial, ID";
            #endregion
        }
        else
        {
            #region storedraft

            query =
                string.Format(@"SELECT  1 Identify, StoreDraftView.ID , NOSerial, FactorDate, AccepterID, AccepterName,
                    PersonnelID, PersonnelName, BuyerID AS PersonID, BuyerName AS PersonName, WardID
                    , WardName AS CompanyUnit, StoreServiceID, StoreServiceName, UnitName,
                    UnitCurrencyConverter, DeliveredAmount, Price, (UnitCurrencyConverter * DeliveredAmount * Price) AS TotalPrice FROM StoreDraftView
                    LEFT OUTER JOIN StoreDraftItemsView ON StoreDraftView.ID =
                    StoreDraftItemsView.StoreDraftID");


            string where = string.Format(" WHERE FactorDate>={0} AND FactorDate<={1}", Helper.QoutedDateTime(start),
                Helper.QoutedDateTime(end));
            if (type != Null.NullInteger)
            {
                where += " AND StoreDraftType =" + type;
            }
            if (type == 1)
            {
                if (personId != Null.NullInteger)
                {
                    where += " AND PersonnelID =" + personId;
                }
                if (transfereeId != Null.NullInteger)
                {
                    where += " AND AccepterID=" + transfereeId;
                }
                mGrid.Columns[3].Visible = mGrid.Columns[6].Visible = mGrid.Columns[5].Visible = mGrid.Columns[0].Visible = printButton.Visible = true;
                mGrid.Columns[4].Visible = false;
            }
            else
            {
                if (personId != Null.NullInteger)
                {
                    where += " AND BuyerID =" + personId;
                }
                mGrid.Columns[3].Visible = mGrid.Columns[6].Visible = mGrid.Columns[5].Visible = false;
                mGrid.Columns[4].Visible = mGrid.Columns[0].Visible = printButton.Visible = true;
            }

            if (!string.IsNullOrEmpty(cuIDs) && !string.IsNullOrEmpty(pgWhere))
            {
                // ------------------------
                // ids: 1,2,5
                // where: '&1%' AND ('&2%' OR '&5%')
                // it could be better, but i dont know regex well :(
                // ------------------------
                where += "\r\nAND (\r\n";

                if (!string.IsNullOrEmpty(cuIDs))
                {
                    // personnel groups: '^1%', '!^1%'
                    string[] ids = cuIDs.Split(',');
                    for (int i = 0; i < ids.Length; i++)
                    {
                        pgWhere = pgWhere.Replace("'^" + ids[i] + "%'",
                            " WardID IN (SELECT ID FROM Ward WHERE ID=" + ids[i] + ") \r\n");
                        pgWhere = pgWhere.Replace("'!^" + ids[i] + "%'",
                            " NOT WardID IN (SELECT ID FROM Ward WHERE ID=" + ids[i] + ") \r\n");
                    }
                }

                where += pgWhere;
                where += "\r\n)\r\n";
            }

            query += where;
            if (!string.IsNullOrEmpty(SortCol))
                query += " ORDER BY " + SortCol + " " + SortOrder;
            query += " ORDER BY FactorDate, BuyerID, DATEADD(dd, 0, DATEDIFF(dd, 0, FactorDate)), NoSerial, ID";
            #endregion
        }
        IDataReader items = DbProxy.Instance.ExecuteReader(query);
        try
        {
            mGrid.DataSource = items;
            mGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Close();
                items.Dispose();
            }
        }
    }

    protected string SortCol
    {
        get { return Helper.GetString(ViewState["SortCol"], null); }
        set { ViewState["SortCol"] = value; }
    }

    protected string SortOrder
    {
        get { return Helper.GetString(ViewState["SortOrder"], "asc"); }
        set { ViewState["SortOrder"] = value; }
    }

    #endregion

    // ------------------------
    // ids: 1,2,5
    // where: '&1%' AND ('&2%' OR '&5%')
    // ------------------------
    private void GetPersonsGroupWhere(out string cuIDs, out string where)
    {
        cuIDs = "";
        where = "";

        // grouping members : '&1%', '!&1%'
        // category = 1375 AND ( category != 1076 OR category = 1075 ) 
        // --> '&1375%' AND ('!&1076%' OR '&1075%')
        where = mQueryBuilderSql.Value.Trim();
        where += " "; // we need this space at end to find IDs
        while (where.Contains("category = "))
        {
            int index = where.IndexOf("category = ") + "category = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            cuIDs += id + ",";
            where = where.Replace("category = " + id + " ", "'^" + id + "%' ");
        }
        while (where.Contains("category != "))
        {
            int index = where.IndexOf("category != ") + "category != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            cuIDs += id + ",";
            where = where.Replace("category != " + id + " ", "'!^" + id + "%' ");
        }
        if (cuIDs.EndsWith(","))
            cuIDs = cuIDs.Substring(0, cuIDs.Length - ",".Length);


        //// personnel groups: '^1%', '!^1%'
        //while (where.Contains("pcat = "))
        //{
        //    int index = where.IndexOf("pcat = ") + "pcat = ".Length;
        //    int nextIndex = where.IndexOf(' ', index);
        //    int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
        //    pgIDs += id + ",";
        //    where = where.Replace("pcat = " + id + " ", "'^" + id + "%' ");
        //}
        //while (where.Contains("pcat != "))
        //{
        //    int index = where.IndexOf("pcat != ") + "pcat != ".Length;
        //    int nextIndex = where.IndexOf(' ', index);
        //    int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
        //    pgIDs += id + ",";
        //    where = where.Replace("pcat != " + id + " ", "'!^" + id + "%' ");
        //}
        //if (pgIDs.EndsWith(","))
        //    pgIDs = pgIDs.Substring(0, pgIDs.Length - ",".Length);
    }

    protected void doExcel_Click(object sender, EventArgs e)
    {
        Exporter.ExportExcel(Response
            , new List<string>(new string[] { mGridTitle.Text })
            , new List<GridView>(new GridView[] { mGrid }));
    }
}