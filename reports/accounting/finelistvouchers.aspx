﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="finelistvouchers.aspx.cs" Inherits="reports_accounting_finelistvouchers" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../styles/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="mGrid" AutoGenerateColumns="False" Width="100%" CssClass="ScrollPanelCartable Grid"
                runat="server" BorderStyle="None" GridLines="Both" EmptyDataText="" OnRowDataBound="mGrid_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="DocumentAccountingID" ItemStyle-HorizontalAlign="Center" HeaderText="Lang.ColRowNr" />
                    <asp:BoundField DataField="DocumentAccountingID" ItemStyle-HorizontalAlign="Center" HeaderText="Lang.DocumentNumber" />
                    <asp:BoundField DataField="DocumentAccountingID" HeaderText="Lang.Comments"/>
                    <asp:BoundField DataField="Date" ItemStyle-HorizontalAlign="Center" HeaderText="Lang.Date"/>
                </Columns>
                <HeaderStyle CssClass="GridHeader" />
                <RowStyle CssClass="GridRow" />
                <SelectedRowStyle CssClass="GridRowSelected" />
            </asp:GridView>
        </div>
    </form>
</body>
</html>
