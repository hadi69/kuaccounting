﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reports_accounting_TreeRemain : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Page.SetLang();
            BindGrid();
        }
        BindBreadcrumb();
    }
    decimal sumOwe = 0, sumDue = 0, sumRemain = 0;
    DateTime dt = DateTime.Now;
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.Common.DbDataRecord record = e.Row.DataItem as System.Data.Common.DbDataRecord;
            e.Row.Cells[0].Text = Helper.GetString(record["TreePath"], "");
            decimal owe = SiteSettings.ToMonetary(Helper.GetDecimal(record["Owe"], 0), dt);
            e.Row.Cells[3].Text = owe.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            decimal due = SiteSettings.ToMonetary(Helper.GetDecimal(record["Due"], 0), dt);
            e.Row.Cells[4].Text = due.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            decimal remain = SiteSettings.ToMonetary(Helper.GetDecimal(record["Remain"], 0), dt);
            e.Row.Cells[5].Text = Math.Abs(remain).ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            e.Row.Cells[6].Text = remain > 0 ? LangProvider.Instance["Cr"] : remain < 0 ? LangProvider.Instance["Li"] : "";

            sumOwe += owe;
            sumDue += due;
            sumRemain += remain;

        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            (e.Row.FindControl("totalOwe") as Label).Text = sumOwe.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("totalDue") as Label).Text = sumDue.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("totalRemain") as Label).Text = sumRemain.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }
    }

    protected void mGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        BindGrid();
    }

    void BindBreadcrumb()
    {
        mBC.Controls.Clear();
        LinkButton b = new LinkButton();
        b.Text = LangProvider.Instance["MasterAccounts"];
        b.CommandArgument = "";
        b.Command += b_Command;
        b.ID = "NOTREE";
        mBC.Controls.Add(b);

        if (string.IsNullOrEmpty(ParentTreePath))
            return;

        List<TreeInfo> parents = new List<TreeInfo>();
        string treePath = ParentTreePath;
        TreeInfo info = AccController.GetTree(treePath);
        while ((info = AccController.GetTree(treePath)) != null)
        {
            parents.Add(info);

            int index = treePath.LastIndexOf('-');
            if (index <= 0)
                treePath = "";
            else
                treePath = treePath.Substring(0, index);
        }
        parents.Reverse();
        for (int i = 0; i < parents.Count; i++)
        {
            info = parents[i];
            mBC.Controls.Add(new LiteralControl("<span class='breadcrumb-seperator'><img src='../../img/dummy.png'></span>"));
            b = new LinkButton();
            b.Text = info.TopicName;
            b.CommandArgument = info.TreePath;
            b.Command += b_Command;
            b.ID = "a" + info.TreePath.Replace('-', '-');
            mBC.Controls.Add(b);
        }
    }

    void b_Command(object sender, CommandEventArgs e)
    {
        ParentTreePath = Helper.GetString(e.CommandArgument, "");
        BindGrid();
        BindBreadcrumb();
    }

    private void BindGrid()
    {
        TreeInfo parent = string.IsNullOrEmpty(ParentTreePath) ? null : AccController.GetTree(ParentTreePath);
        doBack.Enabled = parent != null;
        doForward.Enabled = NextTreePaths != null && NextTreePaths.Count > 0;

        string query = @"SELECT TreePath, dbo.[TreePathToTopicTitle](TreePath) Title, dbo.TreePathToTopicCode(TreePath)  Code
          , Trees.Owe, Trees.Due, (ISNULL(Trees.Owe, 0) - ISNULL(Trees.Due, 0)) Remain FROM Trees 
         INNER JOIN Topic ON [dbo].[TreePathToProgramID](TreePath) = Topic.ID
         WHERE  __PrOgrAmconD__ 
         ";
        if (parent != null)
            query += string.Format(" AND Depth={0} AND TreePath LIKE '{1}-%'", parent.Depth + 1, parent.TreePath);
        else
            query += string.Format(" AND Depth = 0");
        query = SiteSettings.ReplaceOptions(query);
        query += " ORDER BY dbo.TreePathToTopicCode(TreePath) ";

        IDataReader items = DbProxy.Instance.ExecuteReader(query);
        try
        {
            mGrid.DataSource = items;
            mGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Close();
                items.Dispose();
            }
        }
    }

    protected string ParentTreePath
    {
        get { return Helper.GetString(ViewState["ParentTreePath"], null); }
        set { ViewState["ParentTreePath"] = value; }
    }
    protected List<string> NextTreePaths
    {
        get
        {
            return ViewState["NextTreePaths"] as List<string>;
        }
        set
        {
            ViewState["NextTreePaths"] = value;
        }
    }
    protected string SortCol
    {
        get { return Helper.GetString(ViewState["SortCol"], null); }
        set { ViewState["SortCol"] = value; }
    }

    protected string SortOrder
    {
        get { return Helper.GetString(ViewState["SortOrder"], "asc"); }
        set { ViewState["SortOrder"] = value; }
    }
    protected void mGrid_SelectedIndexChanged(object sender, EventArgs e)
    {
        var a = mGrid.SelectedRow;
        ParentTreePath = a.Cells[0].Text;
        BindGrid();
        BindBreadcrumb();
    }
    protected void doBack_Click(object sender, EventArgs e)
    {
        string treePath = ParentTreePath;
        int index = treePath.LastIndexOf('-');
        if (index <= 0)
            treePath = "";
        else
            treePath = treePath.Substring(0, index);
        ParentTreePath = treePath;
        BindGrid();
        BindBreadcrumb();
    }
    protected void doForward_Click(object sender, EventArgs e)
    {

    }
}