﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reports_accounting_isosequence : System.Web.UI.Page
{
    System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
    protected void Page_Load(object sender, EventArgs e)
    {
        msg.Visible = false;
        mGrid.EmptyDataText = LangProvider.Instance["DocumentIsEmpty"];
        mGrid.EnableViewState = false;
        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["IsoSequence"];
            Page.SetLang();
            mReportSetting.ReportName = "IsoSequence";
            Init();

            int year = cal.GetYear(DateTime.Now);
            //mFromDate.Text = string.Format("{0}/01/01", year - 1300);
            //mToDate.Text = Helper.FormatDate(cal.ToDateTime(year + 1, 1, 1, 1, 1, 1, 1).AddDays(-1), "yy/MM/dd");
            IDataReader financeYearStartDate = DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");//0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            //mFromDate.Text = 
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);
        }
        mReportSetting.SaveClick += new EventHandler(mReportSetting_SaveClick);
        mReportSetting.ReportSettingSelect += new EventHandler(mReportSetting_ReportSettingSelect);
    }

    void Init()
    {
        IDataReader persons = DbProxy.Instance.ExecuteReader("SELECT ID, [dbo].[DisplayPerson](ID, 1), ISNULL(Code, '') FROM Personnel WHERE Enabled = 1 AND Type=101");
        mPersons.Items.Clear();
        mPersons.Items.Add(new ListItem(LangProvider.Instance["AllPersons"], ""));
        if (persons != null)
        {
            while (persons.Read())
            {
                if (!persons.IsDBNull(0) && !persons.IsDBNull(1) && !persons.IsDBNull(2))
                    mPersons.Items.Add(new ListItem(persons.GetString(1) + " - کد: " + persons.GetString(2),
                        persons.GetInt32(0).ToString()));
            }
            persons.Close();
        }
        mPersons.SelectedIndex = 0;

        IDataReader commodities = DbProxy.Instance.ExecuteReader("SELECT ServicePriceView.ID, ServiceFullName FROM ServicePriceView");
        mServiceName.Items.Clear();
        mServiceName.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
        if (commodities != null)
        {
            while (commodities.Read())
            {
                if (!commodities.IsDBNull(0) && !commodities.IsDBNull(1))
                    mServiceName.Items.Add(new ListItem(commodities.GetString(1), commodities.GetInt32(0).ToString()));
            }
            commodities.Close();
        }
        mServiceName.SelectedIndex = 0;
    }

    void mReportSetting_ReportSettingSelect(object sender, EventArgs e)
    {
        ReportSettingInfo info = mReportSetting.SelectedReportSettings;
        if (info == null)
            return;
        string[] lines = info.Settings.Split('\n');
        mFromDate.Text = lines[0].Trim('\r');
        mToDate.Text = lines[1].Trim('\r');
    }

    void mReportSetting_SaveClick(object sender, EventArgs e)
    {
        StringBuilder b = new StringBuilder();
        b.AppendLine(mFromDate.Text); //0
        b.AppendLine(mToDate.Text); //1
        mReportSetting.Save(b.ToString());
    }


    string rowStyle = "GridAltRow";
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.Common.DbDataRecord record = e.Row.DataItem as System.Data.Common.DbDataRecord;
            rowStyle = rowStyle == "GridAltRow" ? "GridRow" : "GridAltRow";
            //DateTime dt = Helper.GetDateTime(record["Date"], Helper.NullDate);
            //if (dt != Helper.NullDate)
            //    e.Row.Cells[2].Text = cal.GetMonth(dt).ToString();
            e.Row.CssClass = rowStyle;
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
        }
    }

    protected void doShow_Click(object sender, EventArgs e)
    {
        DateTime start = Helper.ParseDate(mFromDate.Text, Null.NullDate);
        DateTime end = Helper.ParseDate(mToDate.Text, Null.NullDate);
        if (Null.IsNull(start))
        {
            msg.Text = LangProvider.Instance["InvalidStartDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(end))
        {
            msg.Text = LangProvider.Instance["InvalidEndDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(start) || Null.IsNull(end))
            return;
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);
        int startNo = Helper.GetInt(mFromNo.Text, Null.NullInteger), endNo = Helper.GetInt(mToNo.Text, Null.NullInteger);
        int type = Helper.GetInt(mTypes.SelectedValue, Null.NullInteger);
        int servicePriceID = Helper.GetInt(mServiceName.SelectedValue, Null.NullInteger);
        int personID = Helper.GetInt(mPersons.SelectedValue, Null.NullInteger);
        string personWhere = string.Empty;
        string serviceWhere = string.Empty;

        /*
Request	PurchaseReq	BuildOrder	Purchase	StoreDraft	Returns	Refuse
+	    +	        -	        +	        -	        -	    +
+	    -	        -	        -	        +	        -	    -
-	    -	        +	        +	        +	        +	    -
-	    -	        -	        -	        +	        +	    -
-	    -	        -	        +	        -	        -	    +
         */
        string query = @"DECLARE @Res TABLE(
                --	ID INT IDENTITY(1,1) NOT NULL,  -- it prevents DISTINCT
	                RequestID INT NULL,
	                RequestNo NVARCHAR(200),
	                PurchaseRequestID INT NULL,
	                PurchaseRequestNo NVARCHAR(200),
	                BuildOrderID INT NULL,
	                BuildOrderNo NVARCHAR(200),
	                PurchaseID INT NULL,
	                PurchaseNo NVARCHAR(200),
	                StoreDraftID INT NULL,
	                StoreDraftNo NVARCHAR(200),
	                ReturnID INT NULL,
	                ReturnNo NVARCHAR(200),
	                RefuseID INT NULL,
	                RefuseNo NVARCHAR(200)
                );
                DECLARE @FromDate DATETIME, @ToDate DATETIME;
                SET @FromDate = _FromDate_
                SET @ToDate = _ToDate_
                ".Replace("_FromDate_", Helper.QoutedDateTime(start))
                                   .Replace("_ToDate_", Helper.QoutedDateTime(end));


        if (type <= 0) // all
        {
            #region all

            query += @"
-- First Row:
INSERT INTO @Res (RequestID, RequestNo, PurchaseRequestID, PurchaseRequestNo, PurchaseID, PurchaseNo, RefuseID, RefuseNo) 
	SELECT Request.ID, Request.NOSerial, PurchaseRequest.ID, PurchaseRequest.NOSerial, Purchase.ID, Purchase.NOSerial
    , Refuse.ID, Refuse.NOSerial FROM Request
		INNER JOIN PurchaseRequest ON Request.PurchaseRequestID = PurchaseRequest.ID
		LEFT JOIN Purchase ON Purchase.PurchaseRequestID = PurchaseRequest.ID
		LEFT JOIN Refuse ON Refuse.PurchaseID = Purchase.ID
        LEFT JOIN RequestItems ON Request.ID = RequestItems.RequestID
        LEFT JOIN StoreServices ON RequestItems.StoreServiceID = StoreServices.ID
        LEFT JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
	    WHERE @FromDate <= Request.RequestDate AND Request.RequestDate <= @ToDate";
            if (servicePriceID > 0)
            {
                query += " AND StoreServiceDef.ServicePriceID = " + servicePriceID;
            }
            if (personID > 0)
            {
                query += " AND Purchase.SellerID = " + personID;
            }

 query += @"
-- Second Row:
INSERT INTO @Res (PurchaseRequestID, PurchaseRequestNo, PurchaseID, PurchaseNo, RefuseID, RefuseNo) 
	SELECT PurchaseRequest.ID, PurchaseRequest.NOSerial, Purchase.ID, Purchase.NOSerial
    , Refuse.ID, Refuse.NOSerial FROM PurchaseRequest
		LEFT JOIN Purchase ON Purchase.PurchaseRequestID = PurchaseRequest.ID
		LEFT JOIN Refuse ON Refuse.PurchaseID = Purchase.ID
        LEFT JOIN PurchaseRequestItems ON PurchaseRequest.ID = PurchaseRequestItems.PurchaseRequestID
	    WHERE @FromDate <= PurchaseRequest.RequestDate AND PurchaseRequest.RequestDate <= @ToDate";
            if (servicePriceID > 0)
            {
                query += " AND PurchaseRequestItems.ServicePriceID = " + servicePriceID;
            }
            if (personID > 0)
            {
                query += " AND Purchase.SellerID = " + personID;
            }

query += @"
-- Third Row:
INSERT INTO @Res (PurchaseID, PurchaseNo, RefuseID, RefuseNo) 
	SELECT Purchase.ID, Purchase.NOSerial, Refuse.ID, Refuse.NOSerial FROM Purchase
        LEFT JOIN Refuse ON Purchase.ID = Refuse.PurchaseID
		LEFT JOIN PurchaseItems ON Purchase.ID = PurchaseItems.PurchaseID
		LEFT JOIN StoreServices ON PurchaseItems.StoreServiceID = StoreServices.ID
        LEFT JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
	    WHERE @FromDate <= Purchase.Date AND Purchase.Date <= @ToDate";
            if (servicePriceID > 0)
            {
                query += " AND StoreServiceDef.ServicePriceID = " + servicePriceID;
            }
            if (personID > 0)
            {
                query += " AND Purchase.SellerID = " + personID;
            }

query += @"
-- 4th Row:
INSERT INTO @Res (RequestID, RequestNo, StoreDraftID, StoreDraftNo) 
	SELECT Request.ID, Request.NOSerial, StoreDraft.ID, StoreDraft.NOSerial FROM Request 
		INNER JOIN StoreDraft ON Request.StoreDraftID = StoreDraft.ID
        LEFT JOIN RequestItems ON Request.ID = RequestItems.RequestID
        LEFT JOIN StoreServices ON RequestItems.StoreServiceID = StoreServices.ID
        LEFT JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
	WHERE @FromDate <= Request.RequestDate AND Request.RequestDate <= @ToDate";
        if (servicePriceID > 0)
        {
            query += " AND StoreServiceDef.ServicePriceID = " + servicePriceID;
        }
        if (personID > 0)
        {
            query += " AND StoreDraft.BuyerID = " + personID;
        }

query += @"
-- 4th and Third Row : uncontinued
INSERT INTO @Res (RequestID, RequestNo) 
	SELECT Request.ID, Request.NOSerial FROM Request
        LEFT JOIN RequestItems ON Request.ID = RequestItems.RequestID
        LEFT JOIN StoreServices ON RequestItems.StoreServiceID = StoreServices.ID
        LEFT JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID 
	WHERE Request.PurchaseRequestID IS NULL AND Request.StoreDraftID IS NULL
		AND @FromDate <= Request.RequestDate AND Request.RequestDate <= @ToDate";
        if (servicePriceID > 0)
        {
            query += " AND StoreServiceDef.ServicePriceID = " + servicePriceID;
        }
query += @"
-- 5th row
INSERT INTO @Res (BuildOrderID, BuildOrderNo, PurchaseID, PurchaseNo, StoreDraftID, StoreDraftNo, ReturnID, ReturnNo) 
	SELECT BuildOrder.ID, BuildOrder.NOSerial, Purchase.ID, Purchase.NOSerial, StoreDraft.ID, StoreDraft.NOSerial, [Returns].ID, [Returns].NOSerial 
	FROM BuildOrder
		LEFT JOIN Purchase ON BuildOrder.ID = Purchase.BuildOrderID
		LEFT JOIN StoreDraft ON StoreDraft.PurchaseID = Purchase.ID
		LEFT JOIN [Returns] ON [Returns].StoreDraftID = StoreDraft.ID
        LEFT JOIN BuildOrderItem ON BuildOrder.ID = BuildOrderItem.BuildOrderID
        LEFT JOIN StoreServices ON BuildOrderItem.StoreServiceID = StoreServices.ID
        LEFT JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
	WHERE @FromDate <= BuildOrder.OrderDate AND BuildOrder.OrderDate <= @ToDate";
        if (servicePriceID > 0)
        {
            query += " AND StoreServiceDef.ServicePriceID = " + servicePriceID;
        }
        if (personID > 0)
        {
            query += " AND (Purchase.SellerID = " + personID;
            query += " OR StoreDraft.BuyerID = " + personID;
            query += " OR [Returns].BuyerID = " + personID + " )";
        }
query += @"
-- 6th row
INSERT INTO @Res (StoreDraftID, StoreDraftNo, ReturnID, ReturnNo) 
	SELECT StoreDraft.ID, StoreDraft.NOSerial, [Returns].ID, [Returns].NOSerial FROM StoreDraft
		LEFT JOIN [Returns] ON [Returns].StoreDraftID = StoreDraft.ID
        LEFT JOIN StoreDraftItems ON StoreDraft.ID = StoreDraftItems.StoreDraftID
        LEFT JOIN StoreServices ON StoreDraftItems.StoreServiceID = StoreServices.ID
        LEFT JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
	WHERE @FromDate <= StoreDraft.FactorDate AND StoreDraft.FactorDate <= @ToDate";
        if (servicePriceID > 0)
        {
            query += " AND StoreServiceDef.ServicePriceID = " + servicePriceID;
        }
        if (personID > 0)
        {
            query += " AND ( StoreDraft.BuyerID = " + personID;
            query += " OR [Returns].BuyerID = " + personID + " )";
        }
query += @"
-- 7th row
INSERT INTO @Res (PurchaseID, PurchaseNo, RefuseID, RefuseNo) 
	SELECT Purchase.ID, Purchase.NOSerial, Refuse.ID, Refuse.NOSerial FROM Purchase
		LEFT JOIN Refuse ON Refuse.PurchaseID = Purchase.ID
        LEFT JOIN PurchaseItems ON Purchase.ID = PurchaseItems.PurchaseID
        LEFT JOIN StoreServices ON PurchaseItems.StoreServiceID = StoreServices.ID
        LEFT JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
	WHERE @FromDate <= Purchase.ReceiptDate AND Purchase.ReceiptDate <= @ToDate";
            if (servicePriceID > 0)
            {
                query += " AND StoreServiceDef.ServicePriceID = " + servicePriceID;
            }
            if (personID > 0)
            {
                query += " AND ( Purchase.SellerID = " + personID;
                query += " OR Refuse.SellerID = " + personID + " )";
            }

            #endregion
        }
        else if (type == 1) // request
        {
            #region Request
            string noCheck = "";
            if (startNo != Null.NullInteger)
                noCheck += " AND dbo.TryConvertInt(Request.NOSerial) >=" + startNo;
            if (endNo != Null.NullInteger)
                noCheck += " AND dbo.TryConvertInt(Request.NOSerial) <=" + endNo;

            query += @"
-- First Row:
INSERT INTO @Res (RequestID, RequestNo, PurchaseRequestID, PurchaseRequestNo, PurchaseID, PurchaseNo, RefuseID, RefuseNo) 
	SELECT Request.ID, Request.NOSerial, PurchaseRequest.ID, PurchaseRequest.NOSerial, Purchase.ID, Purchase.NOSerial
   , Refuse.ID, Refuse.NOSerial FROM Request 
		INNER JOIN PurchaseRequest ON Request.PurchaseRequestID = PurchaseRequest.ID
		LEFT JOIN Purchase ON Purchase.PurchaseRequestID = PurchaseRequest.ID
		LEFT JOIN Refuse ON Refuse.PurchaseID = Purchase.ID
        LEFT JOIN RequestItems ON Request.ID = RequestItems.RequestID
        LEFT JOIN StoreServices ON RequestItems.StoreServiceID = StoreServices.ID
        LEFT JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
	WHERE @FromDate <= Request.RequestDate AND Request.RequestDate <= @ToDate _noCheck_";
            if (servicePriceID > 0)
            {
                query += " AND StoreServiceDef.ServicePriceID = " + servicePriceID;
            }
            if (personID > 0)
            {
                query += " AND ( Purchase.SellerID = " + personID;
                query += " OR Refuse.SellerID = " + personID + " )";
            }
  query += @"
-- Second Row:
INSERT INTO @Res (RequestID, RequestNo, StoreDraftID, StoreDraftNo) 
	SELECT Request.ID, Request.NOSerial, StoreDraft.ID, StoreDraft.NOSerial FROM Request 
		INNER JOIN StoreDraft ON Request.StoreDraftID = StoreDraft.ID
        LEFT JOIN RequestItems ON Request.ID = RequestItems.RequestID
        LEFT JOIN StoreServices ON RequestItems.StoreServiceID = StoreServices.ID
        LEFT JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
	WHERE @FromDate <= Request.RequestDate AND Request.RequestDate <= @ToDate _noCheck_";
        if (servicePriceID > 0)
        {
            query += " AND StoreServiceDef.ServicePriceID = " + servicePriceID;
        }
        if (personID > 0)
        {
            query += " AND StoreDraft.BuyerID = " + personID;
        }
            query += @"
-- First and Second Row : uncontinued
INSERT INTO @Res (RequestID, RequestNo) 
	SELECT Request.ID, Request.NOSerial FROM Request 
        LEFT JOIN RequestItems ON Request.ID = RequestItems.RequestID
        LEFT JOIN StoreServices ON RequestItems.StoreServiceID = StoreServices.ID
        LEFT JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
	WHERE Request.PurchaseRequestID IS NULL AND Request.StoreDraftID IS NULL
		AND @FromDate <= Request.RequestDate AND Request.RequestDate <= @ToDate
	";
if (servicePriceID > 0)
{
    query += " AND StoreServiceDef.ServicePriceID = " + servicePriceID;
}
            query = query.Replace("_noCheck_", noCheck);
            #endregion
        }
        else if (type == 2) // PurchaseRequest
        {
            #region PurchaseRequest
            string noCheck = "";
            if (startNo != Null.NullInteger)
                noCheck += " AND dbo.TryConvertInt(PurchaseRequest.NOSerial) >=" + startNo;
            if (endNo != Null.NullInteger)
                noCheck += " AND dbo.TryConvertInt(PurchaseRequest.NOSerial) <=" + endNo;

            query += @"
-- First Row:
INSERT INTO @Res (RequestID, RequestNo, PurchaseRequestID, PurchaseRequestNo, PurchaseID, PurchaseNo, RefuseID, RefuseNo) 
	SELECT Request.ID, Request.NOSerial, PurchaseRequest.ID, PurchaseRequest.NOSerial, Purchase.ID, Purchase.NOSerial, Refuse.ID, Refuse.NOSerial 
    FROM PurchaseRequest
		LEFT JOIN Request ON Request.PurchaseRequestID = PurchaseRequest.ID
		LEFT JOIN Purchase ON Purchase.PurchaseRequestID = PurchaseRequest.ID
		LEFT JOIN Refuse ON Refuse.PurchaseID = Purchase.ID
        LEFT JOIN PurchaseRequestItems ON PurchaseRequest.ID = PurchaseRequestItems.PurchaseRequestID
	WHERE @FromDate <= PurchaseRequest.RequestDate AND PurchaseRequest.RequestDate <= @ToDate _noCheck_";
            if (servicePriceID > 0)
            {
                query += " AND PurchaseRequestItems.ServicePriceID = " + servicePriceID;
            }
            if (personID > 0)
            {
                query += " AND ( Purchase.SellerID = " + personID;
                query += " OR Refuse.SellerID = " + personID + " )";
            }
	query = query.Replace("_noCheck_", noCheck);
            #endregion
        }
        else if (type == 3) // Purchase
        {
            #region Purchase
            string noCheck = "";
            if (startNo != Null.NullInteger)
                noCheck += " AND dbo.TryConvertInt(Purchase.NOSerial) >=" + startNo;
            if (endNo != Null.NullInteger)
                noCheck += " AND dbo.TryConvertInt(Purchase.NOSerial) <=" + endNo;
            if (servicePriceID > 0)
            {
                serviceWhere = " AND StoreServiceDef.ServicePriceID = " + servicePriceID;
            }
            if (personID > 0)
            {
                personWhere = " AND Purchase.SellerID = " + personID;
            }

            query += @"
-- First Row:
INSERT INTO @Res (RequestID, RequestNo, PurchaseRequestID, PurchaseRequestNo, PurchaseID, PurchaseNo, RefuseID, RefuseNo) 
	SELECT Request.ID, Request.NOSerial, PurchaseRequest.ID, PurchaseRequest.NOSerial, Purchase.ID, Purchase.NOSerial, Refuse.ID, Refuse.NOSerial 
    FROM Purchase
		LEFT JOIN PurchaseRequest ON Purchase.PurchaseRequestID = PurchaseRequest.ID
		LEFT JOIN Request ON Request.PurchaseRequestID = PurchaseRequest.ID
		LEFT JOIN Refuse ON Refuse.PurchaseID = Purchase.ID
        LEFT JOIN PurchaseItems ON Purchase.ID = PurchaseItems.PurchaseID
        LEFT JOIN StoreServices ON PurchaseItems.StoreServiceID = StoreServices.ID
        LEFT JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
	WHERE @FromDate <= Purchase.ReceiptDate AND Purchase.ReceiptDate <= @ToDate _noCheck_ _serviceWhere_ _personWhere_

-- Third row
INSERT INTO @Res (BuildOrderID, BuildOrderNo, PurchaseID, PurchaseNo, StoreDraftID, StoreDraftNo, ReturnID, ReturnNo) 
	SELECT BuildOrder.ID, BuildOrder.NOSerial, Purchase.ID, Purchase.NOSerial, StoreDraft.ID, StoreDraft.NOSerial, [Returns].ID, [Returns].NOSerial 
	FROM Purchase
		LEFT JOIN BuildOrder ON BuildOrder.ID = Purchase.BuildOrderID
		LEFT JOIN StoreDraft ON StoreDraft.PurchaseID = Purchase.ID
		LEFT JOIN [Returns] ON [Returns].StoreDraftID = StoreDraft.ID
        LEFT JOIN PurchaseItems ON Purchase.ID = PurchaseItems.PurchaseID
        LEFT JOIN StoreServices ON PurchaseItems.StoreServiceID = StoreServices.ID
        LEFT JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
	WHERE @FromDate <= Purchase.ReceiptDate AND Purchase.ReceiptDate <= @ToDate _noCheck_ _serviceWhere_ _personWhere_

-- 5th row
INSERT INTO @Res (PurchaseID, PurchaseNo, RefuseID, RefuseNo) 
	SELECT Purchase.ID, Purchase.NOSerial, Refuse.ID, Refuse.NOSerial FROM Purchase
        LEFT JOIN PurchaseItems ON Purchase.ID = PurchaseItems.PurchaseID
        LEFT JOIN StoreServices ON PurchaseItems.StoreServiceID = StoreServices.ID
        LEFT JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
		LEFT JOIN Refuse ON Refuse.PurchaseID = Purchase.ID
	WHERE @FromDate <= Purchase.ReceiptDate AND Purchase.ReceiptDate <= @ToDate _noCheck_ _serviceWhere_ _personWhere_
".Replace("_noCheck_", noCheck).Replace("_serviceWhere_", serviceWhere).Replace("_personWhere_", personWhere);
            #endregion
        }
        else if (type == 4) // StoreDraft
        {
            #region StoreDraft
            string noCheck = "";
            if (startNo != Null.NullInteger)
                noCheck += " AND dbo.TryConvertInt(StoreDraft.NOSerial) >=" + startNo;
            if (endNo != Null.NullInteger)
                noCheck += " AND dbo.TryConvertInt(StoreDraft.NOSerial) <=" + endNo;
            if (servicePriceID > 0)
            {
                serviceWhere = " AND StoreServiceDef.ServicePriceID = " + servicePriceID;
            }
            if (personID > 0)
            {
                personWhere = " AND StoreDraft.BuyerID = " + personID;
            }

            query += @"
-- Second Row:
INSERT INTO @Res (RequestID, RequestNo, StoreDraftID, StoreDraftNo) 
	SELECT Request.ID, Request.NOSerial, StoreDraft.ID, StoreDraft.NOSerial 
    FROM StoreDraft
		LEFT JOIN Request ON Request.StoreDraftID = StoreDraft.ID
        LEFT JOIN StoreDraftItems ON StoreDraft.ID = StoreDraftItems.StoreDraftID
        LEFT JOIN StoreServices ON StoreDraftItems.StoreServiceID = StoreServices.ID
        LEFT JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
	WHERE @FromDate <= StoreDraft.FactorDate AND StoreDraft.FactorDate <= @ToDate _noCheck_ _serviceWhere_ _personWhere_

-- Third row
INSERT INTO @Res (BuildOrderID, BuildOrderNo, PurchaseID, PurchaseNo, StoreDraftID, StoreDraftNo, ReturnID, ReturnNo) 
	SELECT BuildOrder.ID, BuildOrder.NOSerial, Purchase.ID, Purchase.NOSerial, StoreDraft.ID, StoreDraft.NOSerial, [Returns].ID, [Returns].NOSerial 
	FROM StoreDraft
		LEFT JOIN Purchase ON StoreDraft.PurchaseID = Purchase.ID
		LEFT JOIN BuildOrder ON BuildOrder.ID = Purchase.BuildOrderID
		LEFT JOIN [Returns] ON [Returns].StoreDraftID = StoreDraft.ID
        LEFT JOIN StoreDraftItems ON StoreDraft.ID = StoreDraftItems.StoreDraftID
        LEFT JOIN StoreServices ON StoreDraftItems.StoreServiceID = StoreServices.ID
        LEFT JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
	WHERE @FromDate <= StoreDraft.FactorDate AND StoreDraft.FactorDate <= @ToDate _noCheck_ _serviceWhere_ _personWhere_

-- 4th row
INSERT INTO @Res (StoreDraftID, StoreDraftNo, ReturnID, ReturnNo) 
	SELECT StoreDraft.ID, StoreDraft.NOSerial, [Returns].ID, [Returns].NOSerial FROM StoreDraft
		LEFT JOIN [Returns] ON [Returns].StoreDraftID = StoreDraft.ID
        LEFT JOIN StoreDraftItems ON StoreDraft.ID = StoreDraftItems.StoreDraftID
        LEFT JOIN StoreServices ON StoreDraftItems.StoreServiceID = StoreServices.ID
        LEFT JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
	WHERE @FromDate <= StoreDraft.FactorDate AND StoreDraft.FactorDate <= @ToDate _noCheck_ _serviceWhere_ _personWhere_
".Replace("_noCheck_", noCheck).Replace("_serviceWhere_", serviceWhere).Replace("_personWhere_", personWhere);
            #endregion
        }
        else if (type == 5) // BuildOrder
        {
            #region BuildOrder
            string noCheck = "";
            if (startNo != Null.NullInteger)
                noCheck += " AND dbo.TryConvertInt(BuildOrder.NOSerial) >=" + startNo;
            if (endNo != Null.NullInteger)
                noCheck += " AND dbo.TryConvertInt(BuildOrder.NOSerial) <=" + endNo;

            query += @"
-- Third row
INSERT INTO @Res (BuildOrderID, BuildOrderNo, PurchaseID, PurchaseNo, StoreDraftID, StoreDraftNo, ReturnID, ReturnNo) 
	SELECT BuildOrder.ID, BuildOrder.NOSerial, Purchase.ID, Purchase.NOSerial, StoreDraft.ID, StoreDraft.NOSerial, [Returns].ID, [Returns].NOSerial 
	FROM BuildOrder
		LEFT JOIN Purchase ON BuildOrder.ID = Purchase.BuildOrderID
		LEFT JOIN StoreDraft ON StoreDraft.PurchaseID = Purchase.ID
		LEFT JOIN [Returns] ON [Returns].StoreDraftID = StoreDraft.ID
        LEFT JOIN BuildOrderItem ON BuildOrder.ID = BuildOrderItem.BuildOrderID
        LEFT JOIN StoreServices ON BuildOrderItem.StoreServiceID = StoreServices.ID
        LEFT JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
	WHERE @FromDate <= BuildOrder.OrderDate AND BuildOrder.OrderDate <= @ToDate _noCheck_
".Replace("_noCheck_", noCheck);
            if (servicePriceID > 0)
            {
                query += " AND StoreServiceDef.ServicePriceID = " + servicePriceID;
            }
            if (personID > 0)
            {
                query += " AND (Purchase.SellerID = " + personID;
                query += " OR StoreDraft.BuyerID = " + personID;
                query += " OR [Returns].BuyerID = " + personID + " )";
            }
            #endregion
        }
        else if (type == 6) // Returns
        {
            #region Returns
            string noCheck = "";
            if (startNo != Null.NullInteger)
                noCheck += " AND dbo.TryConvertInt([Returns].NOSerial) >=" + startNo;
            if (endNo != Null.NullInteger)
                noCheck += " AND dbo.TryConvertInt([Returns].NOSerial) <=" + endNo;
            if (servicePriceID > 0)
            {
                serviceWhere = " AND StoreServiceDef.ServicePriceID = " + servicePriceID;
            }
            if (personID > 0)
            {
                personWhere = " AND [Returns].BuyerID = " + personID;
            }

            query += @"
-- Third row
INSERT INTO @Res (BuildOrderID, BuildOrderNo, PurchaseID, PurchaseNo, StoreDraftID, StoreDraftNo, ReturnID, ReturnNo) 
	SELECT BuildOrder.ID, BuildOrder.NOSerial, Purchase.ID, Purchase.NOSerial, StoreDraft.ID, StoreDraft.NOSerial, [Returns].ID, [Returns].NOSerial 
	FROM [Returns]
		LEFT JOIN StoreDraft ON [Returns].StoreDraftID = StoreDraft.ID
		LEFT JOIN Purchase ON StoreDraft.PurchaseID = Purchase.ID
		LEFT JOIN BuildOrder ON BuildOrder.ID = Purchase.BuildOrderID
        LEFT JOIN ReturnItem ON [Returns].ID = ReturnItem.ReturnID
        LEFT JOIN StoreServices ON ReturnItem.StoreServiceID = StoreServices.ID
        LEFT JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
	WHERE @FromDate <= [Returns].ReturnDate AND [Returns].ReturnDate <= @ToDate _noCheck_ _serviceWhere_ _personWhere_

-- 4th row
INSERT INTO @Res (StoreDraftID, StoreDraftNo, ReturnID, ReturnNo) 
	SELECT StoreDraft.ID, StoreDraft.NOSerial, [Returns].ID, [Returns].NOSerial 
    FROM [Returns]
		LEFT JOIN StoreDraft ON [Returns].StoreDraftID = StoreDraft.ID
        LEFT JOIN ReturnItem ON [Returns].ID = ReturnItem.ReturnID
        LEFT JOIN StoreServices ON ReturnItem.StoreServiceID = StoreServices.ID
        LEFT JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
	WHERE @FromDate <= [Returns].ReturnDate AND [Returns].ReturnDate <= @ToDate _noCheck_ _serviceWhere_ _personWhere_
".Replace("_noCheck_", noCheck).Replace("_serviceWhere_", serviceWhere).Replace("_personWhere_", personWhere);
            #endregion
        }
        else if (type == 7) // Refuse
        {
            #region Refuse
            string noCheck = "";
            if (startNo != Null.NullInteger)
                noCheck += " AND dbo.TryConvertInt(Refuse.NOSerial) >=" + startNo;
            if (endNo != Null.NullInteger)
                noCheck += " AND dbo.TryConvertInt(Refuse.NOSerial) <=" + endNo;
            if (servicePriceID > 0)
            {
                serviceWhere = " AND StoreServiceDef.ServicePriceID = " + servicePriceID;
            }
            if (personID > 0)
            {
                personWhere = " AND Refuse.SellerID = " + personID;
            }

            query += @"
-- First Row:
INSERT INTO @Res (RequestID, RequestNo, PurchaseRequestID, PurchaseRequestNo, PurchaseID, PurchaseNo, RefuseID, RefuseNo) 
	SELECT Request.ID, Request.NOSerial, PurchaseRequest.ID, PurchaseRequest.NOSerial, Purchase.ID, Purchase.NOSerial, Refuse.ID, Refuse.NOSerial 
    FROM Refuse
		LEFT JOIN Purchase ON Refuse.PurchaseID = Purchase.ID
		LEFT JOIN PurchaseRequest ON Purchase.PurchaseRequestID = PurchaseRequest.ID
		LEFT JOIN Request ON Request.PurchaseRequestID = PurchaseRequest.ID
        LEFT JOIN RefuseItem ON Refuse.ID = RefuseItem.RefuseID
        LEFT JOIN StoreServices ON RefuseItem.StoreServiceID = StoreServices.ID
        LEFT JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
	WHERE @FromDate <= Refuse.RefuseDate AND Refuse.RefuseDate <= @ToDate _noCheck_ _serviceWhere_ _personWhere_

-- 5th row
INSERT INTO @Res (PurchaseID, PurchaseNo, RefuseID, RefuseNo) 
	SELECT Purchase.ID, Purchase.NOSerial, Refuse.ID, Refuse.NOSerial 
    FROM Refuse
		LEFT JOIN Purchase ON Refuse.PurchaseID = Purchase.ID
	WHERE @FromDate <= Refuse.RefuseDate AND Refuse.RefuseDate <= @ToDate _noCheck_ _serviceWhere_ _personWhere_
".Replace("_noCheck_", noCheck).Replace("_serviceWhere_", serviceWhere).Replace("_personWhere_", personWhere);
            #endregion
        }

        query += @"
SELECT DISTINCT * FROM @Res";

        using (DbProxy dbProxy = new DbProxy())
        {
            DataTable dt = null;
            try
            {
                dt = dbProxy.FillData(query, "ISO");
                if (dt == null && dbProxy.LastException != null)
                    mGrid.EmptyDataText = dbProxy.LastException.Message + "<br />" + query;
                mGrid.DataSource = dt;
                mGrid.DataBind();
                doExcel.Enabled = true;
            }
            catch (Exception ex)
            {
                mGrid.EmptyDataText = ex.Message;
            }
            finally
            {
                if (dt != null)
                    dt.Dispose();
            }
        }
    }
    protected void doExcel_Click(object sender, EventArgs e)
    {
        Exporter.ExportExcel(Response, mTitle.Text, mGrid);
    }

    protected void mGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        doShow_Click(null, null);
    }

    protected string SortCol
    {
        get
        {
            return Helper.GetString(ViewState["SortCol"], null);
        }
        set
        {
            ViewState["SortCol"] = value;
        }
    }
    protected string SortOrder
    {
        get
        {
            return Helper.GetString(ViewState["SortOrder"], "asc");
        }
        set
        {
            ViewState["SortOrder"] = value;
        }
    }
}