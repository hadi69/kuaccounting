﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SupportReport.aspx.cs" Inherits="reports_accounting_SupportReport" %>

<%@ Register Src="../../controls/ReportSetting.ascx" TagName="ReportSetting" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>
        <uc1:ReportSetting ID="mReportSetting" runat="server" />

        <div class="well callout padding-5">
            <div class="row">
                <div class="col-xs-12 col-sm-6 form-group-container">
                    <div class="form-group">
                        <asp:Label ID="Label7" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Person</asp:Label>
                        <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                            <asp:DropDownList ID="mPersonnel" runat="server" CssClass="chosen-select chosen-rtl" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 form-group-container">
                    <div class="form-group">
                        <asp:Label ID="Label4" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.PersonnelGroup</asp:Label>
                        <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                            <asp:DropDownList ID="mPersonnelGroup" runat="server" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 form-group-container">
                    <asp:Label ID="Label1" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.TopicName</asp:Label>
                    <div class="form-group">
                        <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                            <asp:DropDownList ID="mTopic" runat="server" CssClass="chosen-select chosen-rtl" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 form-group-container">
                    <div class="form-group">
                        <asp:Label ID="Label8" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Year</asp:Label>
                        <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                            <div class="col-xs-12 col-sm-3 form-col-without-padding">
                                <asp:TextBox ID="mYear" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row margin-5-top">
                <div class="col-md-8 col-lg-6 visible-md visible-lg">&nbsp;</div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container form-col-without-padding">
                    <div class="col-xs-12 col-sm-4">
                        <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcel_Click" />
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <input type="button" id="print" class="btn btn-xs btn-dark-blue btn-block" value="" />
                    </div>
                </div>
            </div>
        </div>
        <asp:Panel ID="supportDetailPanel" runat="server">
            <JP:MiniHeader runat="server" Text="Lang.SupportReport" ID="mGridTitle" />
            <div class="row">
                <div class="col-xs-12">
                    <asp:GridView ID="mGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed"
                        Scrolling="Vertical" runat="server" ShowFooter="True" BorderStyle="None" OnRowDataBound="mGrid_RowDataBound" RowStyle="true"
                        AllowSorting="True">
                        <Columns>
                            <asp:TemplateField HeaderText="Lang.ColRowNr">
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                                <ItemStyle CssClass="row-cell"></ItemStyle>
                                <HeaderStyle CssClass="row-cell"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:BoundField DataField="PersonnelName" HeaderText="Lang.Person" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle Wrap="True" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="MonthlyFee" HeaderText="Lang.MonthlyFee" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" Wrap="True" />
                            </asp:BoundField>
                            <asp:BoundField DataField="PastRemain" HeaderText="Lang.PastRemain" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" Wrap="True" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Lang.Farvardin" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="Month1Pay" runat="server" Text='<%#Eval("Month1Pay") %>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label CssClass="BigGridFooterLabel" ID="TotalMonth1Pay" runat="server" Text="Lang.Sum" />
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Center" Wrap="True" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Lang.Ordibehesht" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="Month2Pay" runat="server" Text='<%#Eval("Month2Pay") %>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label CssClass="BigGridFooterLabel" ID="TotalMonth2Pay" runat="server" Text="Lang.Sum" />
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Center" Wrap="True" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Lang.Khordad" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="Month3Pay" runat="server" Text='<%#Eval("Month3Pay") %>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label CssClass="BigGridFooterLabel" ID="TotalMonth3Pay" runat="server" Text="Lang.Sum" />
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Center" Wrap="True" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Lang.Tir" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="Month4Pay" runat="server" Text='<%#Eval("Month4Pay") %>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label CssClass="BigGridFooterLabel" ID="TotalMonth4Pay" runat="server"></asp:Label>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Center" Wrap="True" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Lang.Mordad" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="Month5Pay" runat="server" Text='<%#Eval("Month5Pay") %>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label CssClass="BigGridFooterLabel" ID="TotalMonth5Pay" runat="server"></asp:Label>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Center" Wrap="True" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Lang.Shahrivar" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="Month6Pay" runat="server" Text='<%#Eval("Month6Pay") %>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label CssClass="BigGridFooterLabel" ID="TotalMonth6Pay" runat="server"></asp:Label>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Center" Wrap="True" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Lang.Mehr" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="Month7Pay" runat="server" Text='<%#Eval("Month7Pay") %>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label CssClass="BigGridFooterLabel" ID="TotalMonth7Pay" runat="server"></asp:Label>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Center" Wrap="True" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Lang.Aban" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="Month8Pay" runat="server" Text='<%#Eval("Month8Pay") %>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label CssClass="BigGridFooterLabel" ID="TotalMonth8Pay" runat="server"></asp:Label>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Center" Wrap="True" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Lang.Azar" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="Month9Pay" runat="server" Text='<%#Eval("Month9Pay") %>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label CssClass="BigGridFooterLabel" ID="TotalMonth9Pay" runat="server"></asp:Label>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Center" Wrap="True" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Lang.Dey" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="Month10Pay" runat="server" Text='<%#Eval("Month10Pay") %>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label CssClass="BigGridFooterLabel" ID="TotalMonth10Pay" runat="server"></asp:Label>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Center" Wrap="True" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Lang.Bahman" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="Month11Pay" runat="server" Text='<%#Eval("Month11Pay") %>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label CssClass="BigGridFooterLabel" ID="TotalMonth11Pay" runat="server"></asp:Label>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Center" Wrap="True" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Lang.Esfand" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="Month12Pay" runat="server" Text='<%#Eval("Month12Pay") %>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label CssClass="BigGridFooterLabel" ID="TotalMonth12Pay" runat="server"></asp:Label>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Center" Wrap="True" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Lang.TotalRemained" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="TotalRemained" runat="server" Text="" />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label CssClass="BigGridFooterLabel" ID="TotalTotalRemained" runat="server"></asp:Label>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Center" Wrap="True"/>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="BigGridHeader" />
                        <RowStyle CssClass="BigGridRow" />
                        <SelectedRowStyle CssClass="GridRowSelected" />
                    </asp:GridView>
                </div>
            </div>
        </asp:Panel>
    <script>
        $(function () {
            $('#print').attr('value', Lang.Print);
            $('#print').click(function () {
                printTable('.ScrollPanelCartable', '../../', $('.mTitle').html());
            });
            $('.GridRow,.GridAltRow').click(function () {
                $(this).toggleClass('selected');
            });
            $("#bar").prepend(breadcrumb({
                title: Lang.SupportReport,
                parent: {
                    title: Lang.Reports,
                    url: 'menu/Reports.aspx'
                }
            }));
            setClassReports();
        });
    </script>
    </div>
</asp:Content>

