﻿using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Office.Interop.Word;
using DataTable = System.Data.DataTable;

public partial class reports_accounting_PersonBook : System.Web.UI.Page
{
    // backup: <asp:ListItem Text="Lang.ListOfDebtorsOrCreditors" Value="2"></asp:ListItem>

    System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
    protected void Page_Load(object sender, EventArgs e)
    {
        msg.Visible = false;
        Page.Title = LangProvider.Instance["PersonBook"];
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");
        ddlBy.SelectedValue = Request.QueryString["baseOn"];
        ddlBy.Enabled = false;
        /// prepare head of page based on radio bottuns checked
        if (GetByValue() == AccController.PersonBookGroupBy.ByProject)
        {
            panelShowCoworkerPerson.Visible = mShowCoworkerPerson.Checked = panelShowFactors.Visible = panelPersonGroups.Visible = false;
            panelShowJustProjectNeed.Visible = true;
            panelShowJustEquipmentNeed.Visible = panelShowJustCostCenterNeed.Visible = false;
            Label5.Visible = panelFromRemain.Visible = Label6.Visible = panelToRemain.Visible = panelShowDelays.Visible = panelShowVouchers.Visible = false;
            mFromRemain.Text = mToRemain.Text = null;
            panelPersons.Visible = panelEquipments.Visible = panelCostCenters.Visible = false;
            mPersons.SelectedIndex = 0;
            panelDropDowns.Visible = CheckPrint.Visible = false;
            panelProjects.Visible = true;
            mProjectGridTitle.Text = LangProvider.Instance["ProjectBook"];
            mPersonGroups.SelectedIndex = 0;
        }
        else if (GetByValue() == AccController.PersonBookGroupBy.ByEquipment)
        {
            panelShowCoworkerPerson.Visible = mShowCoworkerPerson.Checked = panelShowFactors.Visible = false;
            panelShowJustEquipmentNeed.Visible = true;
            panelShowJustProjectNeed.Visible = panelShowJustCostCenterNeed.Visible = panelPersonGroups.Visible = false;
            Label5.Visible = panelFromRemain.Visible = Label6.Visible = panelToRemain.Visible = panelShowVouchers.Visible = panelShowDelays.Visible = false;
            mFromRemain.Text = mToRemain.Text = null;
            panelPersons.Visible = panelProjects.Visible = panelCostCenters.Visible = false;
            mPersons.SelectedIndex = 0;
            panelDropDowns.Visible = CheckPrint.Visible = false;
            panelEquipments.Visible = true;
            mProjectGridTitle.Text = LangProvider.Instance["EquipmentBook"];
            mPersonGroups.SelectedIndex = 0;
        }
        else if (GetByValue() == AccController.PersonBookGroupBy.ByCostCenter)
        {
            panelShowCoworkerPerson.Visible = mShowCoworkerPerson.Checked = panelPersonGroups.Visible = panelShowFactors.Visible = false;
            panelShowJustCostCenterNeed.Visible = true;
            panelShowJustProjectNeed.Visible = panelShowJustEquipmentNeed.Visible = false;
            Label5.Visible = panelFromRemain.Visible = Label6.Visible = panelToRemain.Visible = panelShowVouchers.Visible = panelShowDelays.Visible = false;
            mFromRemain.Text = mToRemain.Text = null;
            panelPersons.Visible = panelEquipments.Visible = panelProjects.Visible = false;
            mPersons.SelectedIndex = 0;
            panelDropDowns.Visible = CheckPrint.Visible = false;
            panelCostCenters.Visible = true;
            mProjectGridTitle.Text = LangProvider.Instance["CostCenterBook"];
            mPersonGroups.SelectedIndex = 0;
        }
        else if (GetByValue() == AccController.PersonBookGroupBy.ByPersonnel)
        {
            panelShowCoworkerPerson.Visible = false;
            panelShowFactors.Visible = panelPersonGroups.Visible = false;
            panelShowJustProjectNeed.Visible = panelShowJustEquipmentNeed.Visible = panelShowJustCostCenterNeed.Visible = mShowJustProjectNeed.Checked = panelShowVouchers.Visible = false;
            Label26.Visible = mPersons.Visible = true;
            Label5.Visible = panelFromRemain.Visible = Label6.Visible = panelToRemain.Visible =
            panelProjects.Visible = panelShowDelays.Visible = panelEquipments.Visible = panelCostCenters.Visible = false;
            mFromRemain.Text = mToRemain.Text = null;
            panelPersons.Visible = CheckPrint.Visible = true;
            panelDropDowns.Visible = false;
            mPersonGroups.SelectedIndex = 0;
        }
        else // Owe, Due, OweDue
        {
            panelShowCoworkerPerson.Visible = mShowCoworkerPerson.Checked = panelShowJustProjectNeed.Visible = panelShowJustEquipmentNeed.Visible
                = mShowJustProjectNeed.Checked = panelShowFactors.Visible = mShowFactors.Checked = panelShowVouchers.Visible
                = mShowVouchers.Checked = panelShowInitRemain.Visible = mShowInitRemain.Checked = panelShowDelays.Visible = panelCostCenters.Visible = false;
            Label5.Visible = panelFromRemain.Visible = Label6.Visible = panelToRemain.Visible = panelPersonGroups.Visible = true;
            panelPersons.Visible = panelProjects.Visible = panelEquipments.Visible = mShowJustCostCenterNeed.Visible = false;
            mPersons.SelectedIndex = 0;
            panelDropDowns.Visible = CheckPrint.Visible = false;
        }
        mGrid.EmptyDataText = LangProvider.Instance["ListIsEmpty"];
        personGrid.EmptyDataText = LangProvider.Instance["ListIsEmpty"];
        checkGrid.EmptyDataText = LangProvider.Instance["ListIsEmpty"];
        mVouchers.EmptyDataText = LangProvider.Instance["ListIsEmpty"];
        mFactors.EmptyDataText = LangProvider.Instance["ListIsEmpty"];
        mProjectGrid.EmptyDataText = LangProvider.Instance["ListIsEmpty"];
        mCoworkerPerson.EmptyDataText = LangProvider.Instance["ListIsEmpty"];

        if (!IsPostBack)
        {
            Page.SetLang();
            int year = cal.GetYear(DateTime.Now);
            //mFromDate.Text = string.Format("{0}/01/01", year - 1300);
            //mToDate.Text = Helper.FormatDate(cal.ToDateTime(year + 1, 1, 1, 1, 1, 1, 1).AddDays(-1), "yy/MM/dd");
            IDataReader financeYearStartDate = DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");//0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            //mFromDate.Text = 
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);

            initial();
            BindDBs();

           // mReportSetting.ReportName = "PersonBook";

            string queryFromDate = Helper.GetString(Request.QueryString["queryfromdate"], Null.NullString);
            string queryToDate = Helper.GetString(Request.QueryString["querytodate"], Null.NullString);
            if (queryFromDate != Null.NullString && queryToDate != Null.NullString)
            {
                mFromDate.Text = queryFromDate;
                mToDate.Text = queryToDate;
            }

            int personID = Helper.GetInt(Request.QueryString["pid"], Null.NullInteger);
            if (personID != Null.NullInteger)
                try
                {
                    mPersons.SelectedValue = personID.ToString();
                    mPersons.Enabled = false;
                    doShow_Click(null, null);
                }
                catch { }
        }
       // mReportSetting.SaveClick += new EventHandler(mReportSetting_SaveClick);
        //mReportSetting.ReportSettingSelect += new EventHandler(mReportSetting_ReportSettingSelect);

        if (!IsPostBack)
        {
            BindFilterCombos();
        }
    }
    void BindDBs()
    {
        mDBList.Items.Clear();
        ListItem item = new ListItem(LangProvider.Instance["ActiveYear"] + " ", "");
        if (string.IsNullOrEmpty(SiteSettings.OtherCatalogName))
            item.Selected = true;
        mDBList.Items.Add(item);
        try
        {
            string path = Server.MapPath("~/files/fdb");
            if (System.IO.Directory.Exists(path))
            {
                string[] files = System.IO.Directory.GetFiles(path, "DB_*.mdf");
                for (int i = 0; i < files.Length; i++)
                {
                    string fileName = System.IO.Path.GetFileNameWithoutExtension(files[i]);
                    if (!fileName.ToLower().EndsWith("_log"))
                    {
                        item = new ListItem(fileName + " ", fileName);
                        if (fileName == SiteSettings.OtherCatalogName)
                            item.Selected = true;
                        mDBList.Items.Add(item);
                    }
                }
            }
        }
        catch { }
        panelDBs.Visible = mDBList.Items.Count > 1;
    }
    private List<string> GetDBs()
    {
        List<string> dbs = new List<string>();
        for (int i = 0; i < mDBList.Items.Count; i++)
            if (mDBList.Items[i].Selected)
                dbs.Add(mDBList.Items[i].Value);
        return dbs;
    }
    void BindFilterCombos()
    {
        using (DbProxy proxy = new DbProxy())
        {
            {
                IDataReader Grouping = proxy.ExecuteReader("SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping  WHERE GroupingType = 0");
                mPersonsDummy.Items.Clear();
                if (Grouping != null)
                {
                    while (Grouping.Read())
                    {
                        if (!Grouping.IsDBNull(0) && !Grouping.IsDBNull(1))
                            mPersonsDummy.Items.Add(new ListItem(Grouping.GetString(1), Grouping.GetInt32(0).ToString()));
                    }
                    Grouping.Close();
                }
            }

            {
                IDataReader Groups = proxy.ExecuteReader("SELECT ID, Title FROM PersonnelGroup");
                mCatsDummy.Items.Clear();
                if (Groups != null)
                {
                    while (Groups.Read())
                        while (Groups.Read())
                        {
                            if (!Groups.IsDBNull(0) && !Groups.IsDBNull(1))
                                mCatsDummy.Items.Add(new ListItem(Groups.GetString(1), Groups.GetInt32(0).ToString()));
                        }
                    Groups.Close();
                }
            }
            {
                IDataReader projects = proxy.ExecuteReader("SELECT ID, FullName  FROM WardView WHERE SectionType=8 ORDER BY Code");
                mWardDummy.Items.Clear();
                if (projects != null)
                {
                    while (projects.Read())
                    {
                        if (!projects.IsDBNull(0) && !projects.IsDBNull(1))
                            mWardDummy.Items.Add(new ListItem(projects.GetString(1), projects.GetInt32(0).ToString()));
                    }
                    projects.Close();
                }
            }
            {
                IDataReader equipments = proxy.ExecuteReader("SELECT ID, FullName  FROM EquipmentView");
                mEqDummy.Items.Clear();
                if (equipments != null)
                {
                    while (equipments.Read())
                    {
                        if (!equipments.IsDBNull(0) && !equipments.IsDBNull(1))
                            mEqDummy.Items.Add(new ListItem(equipments.GetString(1),
                                equipments.GetInt32(0).ToString()));
                    }
                    equipments.Close();
                }
            }
            {
                IDataReader costCenters = proxy.ExecuteReader("SELECT ID, FullName  FROM CostCenterView");
                mCCenterDummy.Items.Clear();
                if (costCenters != null)
                {
                    while (costCenters.Read())
                    {
                        if (!costCenters.IsDBNull(0) && !costCenters.IsDBNull(1))
                            mCCenterDummy.Items.Add(new ListItem(costCenters.GetString(1),
                                costCenters.GetInt32(0).ToString()));
                    }
                    costCenters.Close();
                }
            }

        }
    }

    //void mReportSetting_ReportSettingSelect(object sender, EventArgs e)
    //{
    //    ReportSettingInfo info = mReportSetting.SelectedReportSettings;
    //    if (info == null)
    //        return;
    //    string[] lines = info.Settings.Split('\n');
    //    mPersons.SelectedValue = lines[0].Trim('\r');
    //    mFromDate.Text = lines[1].Trim('\r');
    //    mToDate.Text = lines[2].Trim('\r');
    //}

    //void mReportSetting_SaveClick(object sender, EventArgs e)
    //{
    //    StringBuilder b = new StringBuilder();
    //    b.AppendLine(mPersons.SelectedValue); //0
    //    b.AppendLine(mFromDate.Text); //1
    //    b.AppendLine(mToDate.Text); //2
    //    mReportSetting.Save(b.ToString());
    //}
    void initial()
    {
        IDataReader persons = DbProxy.Instance.ExecuteReader("SELECT ID, ISNULL([dbo].[DisplayPerson](ID, 1), ''), ISNULL(Code, '') FROM Personnel WHERE Enabled = 1 AND Type=101");
        mPersons.Items.Clear();
        mPersons.Items.Add(new ListItem(LangProvider.Instance["AllPersons"], ""));
        if (persons != null)
        {
            while (persons.Read())
            {
                if (!persons.IsDBNull(0) && !persons.IsDBNull(1))
                    mPersons.Items.Add(new ListItem(persons.GetString(1) + " - کد: " + persons.GetString(2),
                        persons.GetInt32(0).ToString()));
            }
            persons.Close();
        }
        mPersons.SelectedIndex = 0;

        IDataReader projects = DbProxy.Instance.ExecuteReader("SELECT ID, FullName  FROM WardView WHERE SectionType=8");
        mProjects.Items.Clear();
        mProjects.Items.Add(new ListItem(LangProvider.Instance["AllProject"], ""));
        if (projects != null)
        {
            while (projects.Read())
            {
                if (!projects.IsDBNull(0) && !projects.IsDBNull(1))
                    mProjects.Items.Add(new ListItem(projects.GetString(1), projects.GetInt32(0).ToString()));
            }
            projects.Close();
        }
        mProjects.SelectedIndex = 0;

        IDataReader equipments = DbProxy.Instance.ExecuteReader("SELECT ID, FullName  FROM EquipmentView");
        mEquipments.Items.Clear();
        mEquipments.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
        if (equipments != null)
        {
            while (equipments.Read())
            {
                if (!equipments.IsDBNull(0) && !equipments.IsDBNull(1))
                    mEquipments.Items.Add(new ListItem(equipments.GetString(1), equipments.GetInt32(0).ToString()));
            }
            equipments.Close();
        }
        mEquipments.SelectedIndex = 0;

        IDataReader costCenters = DbProxy.Instance.ExecuteReader("SELECT ID, FullName  FROM EquipmentView");
        mCostCenters.Items.Clear();
        mCostCenters.Items.Add(new ListItem(LangProvider.Instance["AllGroup"], ""));
        if (costCenters != null)
        {
            while (costCenters.Read())
            {
                if (!costCenters.IsDBNull(0) && !costCenters.IsDBNull(1))
                    mCostCenters.Items.Add(new ListItem(costCenters.GetString(1), costCenters.GetInt32(0).ToString()));
            }
            costCenters.Close();
        }
        mCostCenters.SelectedIndex = 0;

        IDataReader PersonGroups = DbProxy.Instance.ExecuteReader("SELECT ID, Title FROM PersonnelGroup");
        mPersonGroups.Items.Clear();
        mPersonGroups.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
        if (PersonGroups != null)
        {
            while (PersonGroups.Read())
                {
                    if (!PersonGroups.IsDBNull(0) && !PersonGroups.IsDBNull(1))
                        mPersonGroups.Items.Add(new ListItem(PersonGroups.GetString(1), PersonGroups.GetInt32(0).ToString()));
                }
            PersonGroups.Close();
        }
        mPersonGroups.SelectedIndex = 0;

        panelShowInProgressVouchers.Visible = DbProxy.Instance.GetConfig("VoucherWorkflow", -1) == "1";
    }

    string rowStyle = "GridAltRow";
    decimal sumOwe = 0, sumDue = 0, _due = 0, _owe = 0, _remain = 0, _paid = -2;
    List<int> IDs = new List<int>();
    int lastID = -2;
    TopicNature lastNature = TopicNature.Both;
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DateTime dt = new DateTime();
            dt = DateTime.Now;
            DataRowView record = e.Row.DataItem as DataRowView;
            if (record["Date"].ToString() != "")
            {
                dt = Helper.GetDateTime(record["Date"], Helper.NullDate);
            }
            Label lblGridDate = (Label)e.Row.FindControl("lblGridDate");
            if (dt != Helper.NullDate && lblGridDate != null)
            {
                lblGridDate.Text = Helper.FormatDateFull(Helper.GetDateTime(dt, Helper.NullDate));
            }
            int byValue = int.Parse(ddlBy.SelectedValue);
            int id = HasPersonnel() ? Helper.GetInt(record["PersonnelID"], -1) : Helper.GetInt(record["ProjectID"], -1);
            if (lastID != id)
            {
                rowStyle = rowStyle == "GridAltRow" ? "GridRow" : "GridAltRow";
                _remain = _owe = _due = 0;
                lastID = id;
                if (HasPersonnel())
                    lastNature = Helper.GetEnum(DbProxy.Instance.FillScalarString("SELECT Nature FROM Personnel WHERE ID = " + lastID), TopicNature.Both);
            }
            //else
            //{
            //    e.Row.Cells[2].Text = "";
            //}
            int voucherID = Helper.GetInt(record["ID"], Null.NullInteger);
            if (!IDs.Contains(voucherID))
                IDs.Add(voucherID);
            if (!(HasPersonnel()) || Helper.GetInt(mPersons.SelectedValue, Null.NullInteger) != Null.NullInteger)
            {
                // url
                int otherID = Helper.GetInt(record["OtherID"], Null.NullInteger), mode = Helper.GetInt(record["Mode"], Null.NullInteger);

                string storeDraftNo = record["StoreDraftNo"].ToString();
                string purchaseNo = record["PurchaseNo"].ToString();
                string returnNo = record["ReturnNo"].ToString();
                string refuseNo = record["RefuseNo"].ToString();

                if (!string.IsNullOrEmpty(storeDraftNo) && !string.IsNullOrWhiteSpace(storeDraftNo))
                {
                    e.Row.Cells[9].Text = string.Format("<a href='../../list.aspx?table=StoreDraft&view=shopstoredrafts&id={0}'>{1}</a>", otherID, storeDraftNo);
                }
                else if (!string.IsNullOrEmpty(purchaseNo) && !string.IsNullOrWhiteSpace(purchaseNo))
                {
                    e.Row.Cells[9].Text = string.Format("<a href='../../list.aspx?table=Purchase&view=shoppurchases&id={0}'>{1}</a>", otherID, purchaseNo);
                }
                else if (!string.IsNullOrEmpty(returnNo) && !string.IsNullOrWhiteSpace(returnNo))
                {
                    e.Row.Cells[9].Text = string.Format("<a href='../../list.aspx?table=Returns&view=shopreturns&id={0}'>{1}</a>", otherID, returnNo);
                }
                else if (!string.IsNullOrEmpty(refuseNo) && !string.IsNullOrWhiteSpace(refuseNo))
                {
                    e.Row.Cells[9].Text = string.Format("<a href='../../list.aspx?table=Refuse&view=shoprefuses&id={0}'>{1}</a>", otherID, refuseNo);
                }

                if (otherID == Null.NullInteger)
                    e.Row.Cells[2].Text = string.Format("<a href='../../list.aspx?table=DocumentAccounting&id={0}&itemid={1}'>{2}</a>", record["ID"], record["ItemID"], record["No"]);
                else
                    e.Row.Cells[2].Text = string.Format("<a href='../../list.aspx?table=DocumentAccounting&id={0}&itemid={1}' class='no' mode='{3}' otherid='{4}'>{2}</a>", record["ID"], record["ItemID"], record["No"], mode, otherID);
            }

            //if (!string.IsNullOrEmpty(Helper.GetString()))
            _due = SiteSettings.ToMonetary(Helper.GetDecimal(record["Due"], 0), dt);
            _owe = SiteSettings.ToMonetary(Helper.GetDecimal(record["Owe"], 0), dt);

            if (Helper.GetInt(mPersons.SelectedValue, Null.NullInteger) != Null.NullInteger)
            {
                _paid = Helper.GetInt(record["Paid"], Null.NullInteger);
                if (_paid == 0)
                {
                    e.Row.Cells[18].Text = LangProvider.Instance["Paid"];
                }
                else if (_paid == -1)
                {
                    e.Row.Cells[18].Text = LangProvider.Instance["NotConfirmed"];
                }
                else if (_paid == -2)
                {
                    e.Row.Cells[18].Text = "-";
                }
                else
                {
                    e.Row.Cells[18].Text = LangProvider.Instance["Remained"] + " : " + SiteSettings.ToMonetary(_paid, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                }

                decimal oweORdue = 0;
                if (lastNature == TopicNature.Owe)
                {
                    oweORdue = _owe;
                }
                else if (lastNature == TopicNature.Due)
                {
                    oweORdue = _due;
                }

                if (oweORdue > 0 && _paid != -2)
                {
                    rowStyle = _paid == 0 ? "paidRow" : "notPaidRow";
                }
                else
                {
                    rowStyle = "GridAltRow";
                }

            }

            _remain += _due - _owe;

            sumDue += _due;
            sumOwe += _owe;

            e.Row.CssClass = rowStyle;

            ((Label)e.Row.FindControl("owe")).Text = _owe.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("due")).Text = _due.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            decimal _r = _remain;
            ((Label)e.Row.FindControl("remaining")).Text = (_r < 0) ? (-1 * _r).ToString("N4", Helper.NumberCulture).Replace(".0000", "") : _r.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.Cells[17].Text = (_r < 0) ? LangProvider.Instance["Li"] : LangProvider.Instance["Cr"];
            if (lastNature != TopicNature.Both)
                if ((lastNature == TopicNature.Owe && _remain > 0) || (lastNature == TopicNature.Due && _remain < 0))
                    Helper.SignifyNegative(e.Row.Cells[15]);
            string no = Helper.GetString(record["No"], "");
            if (no == "0" && mShowInitRemain.Checked)
            {
                e.Row.Cells[2].Text = "";
                e.Row.Cells[1].Text = LangProvider.Instance["InitRemain"];
            }
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblTotal = (Label)e.Row.FindControl("oweTotal");
            lblTotal.Text = sumOwe.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            lblTotal = (Label)e.Row.FindControl("dueTotal");
            lblTotal.Text = sumDue.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            lblTotal = (Label)e.Row.FindControl("remainingTotal");
            decimal s = sumDue - sumOwe;
            lblTotal.Text = (s < 0) ? (-1 * s).ToString("N4", Helper.NumberCulture).Replace(".0000", "") : s.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            if (lastNature != TopicNature.Both)
                if ((lastNature == TopicNature.Owe && (sumDue - sumOwe) > 0) || (lastNature == TopicNature.Due && (sumDue - sumOwe) < 0))
                    Helper.SignifyNegative(e.Row.Cells[16]);

            lblTotal = (Label)e.Row.FindControl("finalDiagnosis");
            lblTotal.Text = ((sumDue - sumOwe) < 0) ? LangProvider.Instance["Li"] : LangProvider.Instance["Cr"];
        }
    }

    string mCoworkerPersonRowStyle = "GridRow";
    int coworkerPersonID = -2;
    decimal sumDue1 = 0, sumOwe1 = 0;
    protected void mCoworkerPerson_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView record = e.Row.DataItem as DataRowView;
            DateTime dt = Helper.GetDateTime(record["Date"], Helper.NullDate);
            Label lblCoworkerPersonDate = (Label)e.Row.FindControl("lblCoworkerPersonDate");

            if (dt != Helper.NullDate && lblCoworkerPersonDate != null)
            {
                lblCoworkerPersonDate.Text = Helper.FormatDateFull(Helper.GetDateTime(dt, Helper.NullDate));
            }

            int id = HasPersonnel() ? Helper.GetInt(record["PersonnelID"], -1) : Helper.GetInt(record["ProjectID"], -1);
            if (coworkerPersonID != id)
            {
                mCoworkerPersonRowStyle = mCoworkerPersonRowStyle == "GridAltRow" ? "GridRow" : "GridAltRow";
                _owe = _due = 0;
                coworkerPersonID = id;
            }
            if (!(HasPersonnel()) || Helper.GetInt(mPersons.SelectedValue, Null.NullInteger) != Null.NullInteger)
            {
                // url
                int otherID = Helper.GetInt(record["OtherID"], Null.NullInteger), mode = Helper.GetInt(record["Mode"], Null.NullInteger);
                if (otherID == Null.NullInteger)
                    e.Row.Cells[2].Text = string.Format("<a href='../../list.aspx?table=DocumentAccounting&id={0}&itemid={1}'>{2}</a>", record["ID"], record["ItemID"], record["No"]);
                else
                    e.Row.Cells[2].Text = string.Format("<a href='../../list.aspx?table=DocumentAccounting&id={0}&itemid={1}' class='no' mode='{3}' otherid='{4}'>{2}</a>", record["ID"], record["ItemID"], record["No"], mode, otherID);
            }
            //_owe = _due = 0;
            //if (!string.IsNullOrEmpty(Helper.GetString()))
            e.Row.CssClass = mCoworkerPersonRowStyle;
            _due = SiteSettings.ToMonetary(Helper.GetDecimal(record["Due"], 0), dt);
            _owe = SiteSettings.ToMonetary(Helper.GetDecimal(record["Owe"], 0), dt);
            //_remain += _due - _owe;

            sumDue1 += _due;
            sumOwe1 += _owe;

            ((Label)e.Row.FindControl("owe")).Text = _owe.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("due")).Text = _due.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }

        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblTotal = (Label)e.Row.FindControl("oweTotal");
            lblTotal.Text = sumOwe1.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            lblTotal = (Label)e.Row.FindControl("dueTotal");
            lblTotal.Text = sumDue1.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }
    }

    string mProjectGridRowStyle = "GridRow";
    int mProjectGridID = -2;
    decimal sumDue2 = 0, sumOwe2 = 0, totalRemain = 0;
    protected void mProjectGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView record = e.Row.DataItem as DataRowView;
            Label lblProjectDate = (Label)e.Row.FindControl("lblProjectDate");

            DateTime dt = Helper.GetDateTime(record["Date"], Helper.NullDate);
            if (dt != Helper.NullDate && lblProjectDate != null)
            {
                lblProjectDate.Text = Helper.FormatDateFull(Helper.GetDateTime(dt, Helper.NullDate));
            }

            int id = (GetByValue() == AccController.PersonBookGroupBy.ByProject) ? Helper.GetInt(record["ProjectID"], -1) : Helper.GetInt(record["PersonnelID"], -1);
            if (mProjectGridID != id)
            {
                mProjectGridRowStyle = mProjectGridRowStyle == "GridAltRow" ? "GridRow" : "GridAltRow";
                _owe = _due = 0;
                mProjectGridID = id;
            }
            int voucherID = Helper.GetInt(record["ID"], Null.NullInteger);
            if (!IDs.Contains(voucherID))
                IDs.Add(voucherID);
            //if (!(GetByValue() == AccController.PersonBookGroupBy.ByProject) || Helper.GetInt(mProjects.SelectedValue, Null.NullInteger) != Null.NullInteger)
            {
                // url
                int otherID = Helper.GetInt(record["OtherID"], Null.NullInteger), mode = Helper.GetInt(record["Mode"], Null.NullInteger);
                if (otherID == Null.NullInteger)
                    e.Row.Cells[2].Text = string.Format("<a href='../../list.aspx?table=DocumentAccounting&id={0}&itemid={1}'>{2}</a>", record["ID"], record["ItemID"], record["No"]);
                else
                    e.Row.Cells[2].Text = string.Format("<a href='../../list.aspx?table=DocumentAccounting&id={0}&itemid={1}' class='no' mode='{3}' otherid='{4}'>{2}</a>", record["ID"], record["ItemID"], record["No"], mode, otherID);
            }
            e.Row.CssClass = mProjectGridRowStyle;
            _due = SiteSettings.ToMonetary(Helper.GetDecimal(record["Due"], 0), dt);
            _owe = SiteSettings.ToMonetary(Helper.GetDecimal(record["Owe"], 0), dt);
            //_remain += _due - _owe;


            sumDue2 += _due;
            sumOwe2 += _owe;

            ((Label)e.Row.FindControl("owe")).Text = _owe.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("due")).Text = _due.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            totalRemain = sumDue2 - sumOwe2;
            Label lblremaining = (Label)e.Row.FindControl("remaining");
            lblremaining.Text = (totalRemain < 0) ? (-1 * totalRemain).ToString("N4", Helper.NumberCulture).Replace(".0000", "") : totalRemain.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            Label lbldiagnosis = (Label)e.Row.FindControl("diagnosis");
            lbldiagnosis.Text = (totalRemain < 0) ? LangProvider.Instance["Li"] : LangProvider.Instance["Cr"];

        }

        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label sumLable = (Label)e.Row.FindControl("sumLable");
            sumLable.Text = LangProvider.Instance["Sum"];

            Label lblTotal = (Label)e.Row.FindControl("oweTotal");
            lblTotal.Text = sumOwe2.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            lblTotal = (Label)e.Row.FindControl("dueTotal");
            lblTotal.Text = sumDue2.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            Label lblremainingTotal = (Label)e.Row.FindControl("remainingTotal");
            lblremainingTotal.Text = (totalRemain < 0) ? (-1 * totalRemain).ToString("N4", Helper.NumberCulture).Replace(".0000", "") : totalRemain.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            Label lblfinalDiagnosis = (Label)e.Row.FindControl("finalDiagnosis");
            lblfinalDiagnosis.Text = (totalRemain < 0) ? LangProvider.Instance["Li"] : LangProvider.Instance["Cr"];
        }
    }
    protected void doShow_Click(object sender, EventArgs e)
    {
        doExcel.Enabled = true;
        //int year = Helper.GetInt(mYear.Text, 0);
        //if (year < 1370 || year > 1440)
        //    return;
        sumOwe = sumDue = 0;
        IDs = new List<int>();
        DateTime start = Helper.ParseDate(mFromDate.Text, Null.NullDate);
        DateTime end = Helper.ParseDate(mToDate.Text, Null.NullDate);
        if (Null.IsNull(start))
        {
            msg.Text = LangProvider.Instance["InvalidStartDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(end))
        {
            msg.Text = LangProvider.Instance["InvalidEndDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(start) || Null.IsNull(end))
            return;
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);
        decimal? fromRemain = null, toRemain = null;
        if (mFromRemain.Text.Trim().Length > 0)
        {
            fromRemain = Helper.GetDecimal(mFromRemain.Text, decimal.MinValue);
            if (fromRemain.Value == decimal.MinValue)
                fromRemain = null;
            //todo:
            //else
            //    fromRemain /= SiteSettings.MonetaryUnitFactor;
        }
        if (mToRemain.Text.Trim().Length > 0)
        {
            toRemain = Helper.GetDecimal(mToRemain.Text, decimal.MinValue);
            if (toRemain.Value == decimal.MinValue)
                toRemain = null;
            //else
            //    toRemain /= SiteSettings.MonetaryUnitFactor;
        }
        voucherDetailPanel.Visible = HasPersonnel();
        checksPanel.Visible = (GetByValue() == AccController.PersonBookGroupBy.ByPersonnel);
        if (HasPersonnel())
        {
            if (GetByValue() == AccController.PersonBookGroupBy.ByOwe && mShowDelays.Checked)
            {
                delayPanel.Visible = true;
                voucherDetailPanel.Visible = checksPanel.Visible = false;
                //BindDelays();
            }
            else
            {
                BindGrid();
                BindChecks();
                //BindDelays();
            }
        }
        coworkerPanel.Visible = mShowCoworkerPerson.Checked;
        if (mShowCoworkerPerson.Checked)
        {
            DataTable items1 = AccController.GetPersonBook(Null.NullInteger
                        , start
                        , end
                        , 0, int.MaxValue - 1, Helper.GetInt(mPersons.SelectedValue, Null.NullInteger)
                        , Null.NullInteger, SiteSettings.ProgramID, GetByValue()
                        , fromRemain, toRemain, new string[] { }, mShowCoworkerPerson.Checked, null, null, null, false
                        , GetDBs(), ShowAllVouchers(), Null.NullInteger);

            try
            {
                mCoworkerPerson.DataSource = items1;
                mCoworkerPerson.DataBind();
            }
            finally
            {
                if (null != items1)
                    items1.Dispose();
            }
        }
        projectPanel.Visible = false;
        if (GetByValue() == AccController.PersonBookGroupBy.ByProject || GetByValue() == AccController.PersonBookGroupBy.ByEquipment
            || GetByValue() == AccController.PersonBookGroupBy.ByCostCenter)
        {
            projectPanel.Visible = true;
            string filter = GetFilter("");

            DataTable items2 = null;
            if (GetByValue() == AccController.PersonBookGroupBy.ByProject)
            {
                items2 = AccController.GetProjectBook(Null.NullInteger
                        , start
                        , end
                        , Helper.GetInt(mPersons.SelectedValue, Null.NullInteger)
                         , Helper.GetInt(mProjects.SelectedValue, Null.NullInteger), SiteSettings.ProgramID, mShowJustProjectNeed.Checked
                        , filter, GetDBs(), ShowAllVouchers());
                mProjectGrid.Columns[4].HeaderText = LangProvider.Instance["Project"];
            }
            else if (GetByValue() == AccController.PersonBookGroupBy.ByEquipment)
            {
                items2 = AccController.GetEquipmentBook(Null.NullInteger
                        , start
                        , end
                        , Helper.GetInt(mPersons.SelectedValue, Null.NullInteger)
                        , Helper.GetInt(mEquipments.SelectedValue, Null.NullInteger), SiteSettings.ProgramID, mShowJustEquipmentNeed.Checked
                        , filter, GetDBs(), ShowAllVouchers());
                mProjectGrid.Columns[4].HeaderText = LangProvider.Instance["Equipment"];
            }
            else
            {
                items2 = AccController.GetCostCenterBook(Null.NullInteger
                        , start
                        , end
                        , Helper.GetInt(mPersons.SelectedValue, Null.NullInteger)
                        , Helper.GetInt(mCostCenters.SelectedValue, Null.NullInteger), SiteSettings.ProgramID, mShowJustCostCenterNeed.Checked
                        , filter, GetDBs(), ShowAllVouchers());
                mProjectGrid.Columns[4].HeaderText = LangProvider.Instance["CostCenter"];
            }
            try
            {
                mProjectGrid.DataSource = items2;
                mProjectGrid.DataBind();
            }
            finally
            {
                if (null != items2)
                    items2.Dispose();
            }
        }
        vouchersPanel.Visible = mShowVouchers.Checked;
        if (mShowVouchers.Checked && IDs.Count > 0)
            ShowVouchers();
        else
        {
            mVouchers.DataSource = null;
            mVouchers.DataBind();
        }

        factorsPanel.Visible = mShowFactors.Checked;
        if (mShowFactors.Checked && IDs.Count > 0)
            ShowFactors();
        else
        {
            mFactors.DataSource = null;
            mFactors.DataBind();
        }

        // vouchersPanel.Visible = Helper.GetInt(mPersons.SelectedValue, Null.NullInteger) != Null.NullInteger || GetByValue() == AccController.PersonBookGroupBy.ByProject;
        vouchersPanel.Visible = GetByValue() == AccController.PersonBookGroupBy.ByProject;
        if (vouchersPanel.Visible)
            ShowPersonGrid();
        else
        {
            personGrid.DataSource = null;
            personGrid.DataBind();
        }
    }
    void BindGrid()
    {
        if (Helper.GetInt(mPersons.SelectedValue, Null.NullInteger) != Null.NullInteger)
        {
            DbProxy pro = new DbProxy();
            IDataReader personData =
                pro.ExecuteReader(
                    "SELECT ISNULL(HomeAddress, ' '), ISNULL(HomeTelNum, ' '), ISNULL(CellNumber1, ' ') FROM Personnel WHERE ID = " +
                    mPersons.SelectedValue);
            string personInf = "";
            while (personData.Read())
                personInf = LangProvider.Instance["Address"] + " : " + personData.GetString(0) + "/" +
                            LangProvider.Instance["HomeTelNum"] + " : " + personData.GetString(1) + "/" +
                            LangProvider.Instance["Mobile"] + " : " + personData.GetString(2);
            mGridTitle.Text = LangProvider.Instance["MyBook"] + " : " + mPersons.SelectedItem + "/" + personInf;
            personData.Close();
        }
        else
        {
            mGridTitle.Text = LangProvider.Instance["PersonBook"];

        }
        sumOwe = sumDue = 0;
        IDs = new List<int>();
        DateTime start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        DateTime end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);
        decimal? fromRemain = null, toRemain = null;
        if (mFromRemain.Text.Trim().Length > 0)
        {
            fromRemain = Helper.GetDecimal(mFromRemain.Text, decimal.MinValue);
            if (fromRemain.Value == decimal.MinValue)
                fromRemain = null;
        }
        if (mToRemain.Text.Trim().Length > 0)
        {
            toRemain = Helper.GetDecimal(mToRemain.Text, decimal.MinValue);
            if (toRemain.Value == decimal.MinValue)
                toRemain = null;
        }
        if (Helper.GetInt(mPersons.SelectedValue, Null.NullInteger) == Null.NullInteger)
        {
            mGrid.Columns[11].Visible = mGrid.Columns[12].Visible = mGrid.Columns[13].Visible = true;
            mGrid.Columns[1].Visible = mGrid.Columns[2].Visible = mGrid.Columns[3].Visible
                = mGrid.Columns[4].Visible = mGrid.Columns[9].Visible
                = mGrid.Columns[10].Visible = mGrid.Columns[18].Visible = mGrid.Columns[7].Visible = mGrid.Columns[8].Visible = false;
        }
        else
        {
            mGrid.Columns[11].Visible = mGrid.Columns[12].Visible = mGrid.Columns[13].Visible = mGrid.Columns[7].Visible = mGrid.Columns[8].Visible = false;
            mGrid.Columns[1].Visible = mGrid.Columns[2].Visible = mGrid.Columns[3].Visible = mGrid.Columns[4].Visible = mGrid.Columns[9].Visible = mGrid.Columns[10].Visible = mGrid.Columns[18].Visible = true;
        }
        string oweTreePath = DbProxy.Instance.GetConfig("OweTreePath", SiteSettings.ProgramID);
        string dueTreePath = DbProxy.Instance.GetConfig("DueTreePath", SiteSettings.ProgramID);
        string filter = GetFilter("");
        DataTable items = AccController.GetPersonBook(Null.NullInteger
            , start
            , end
            , 0, int.MaxValue - 1, Helper.GetInt(mPersons.SelectedValue, Null.NullInteger)
            , Null.NullInteger, SiteSettings.ProgramID, GetByValue()
            , fromRemain, toRemain, new string[] { oweTreePath, dueTreePath }, false, filter
            , SortCol, SortOrder, mShowInitRemain.Checked, GetDBs(), ShowAllVouchers()
            , Helper.GetInt(mPersonGroups.SelectedValue, Null.NullInteger));
        try
        {
            mGrid.DataSource = items;
            mGrid.DataBind();
        }
        finally
        {
            if (null != items)
                items.Dispose();
        }

    }

    #region Vouchers
    string mVouchersRowStyle = "GridRow";
    int lastVoucherID = -1;
    protected void mVouchers_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView record = e.Row.DataItem as DataRowView;
            int id = Helper.GetInt(record["ID"], Null.NullInteger);
            DateTime dt = Helper.GetDateTime(record["Date"], Helper.NullDate);
            Label lblVouchersDate = (Label)e.Row.FindControl("lblVouchersDate");
            if (dt != Helper.NullDate && lblVouchersDate != null)
            {
                lblVouchersDate.Text = Helper.FormatDateFull(Helper.GetDateTime(dt, Helper.NullDate));
            }
            if (id != lastVoucherID)
            {
                lastVoucherID = id;
                mVouchersRowStyle = mVouchersRowStyle == "GridAltRow" ? "GridRow" : "GridAltRow";
            }
            e.Row.CssClass = mVouchersRowStyle;

            int otherID = Helper.GetInt(record["OtherID"], Null.NullInteger), mode = Helper.GetInt(record["Mode"], Null.NullInteger);

            if (otherID == Null.NullInteger)
                e.Row.Cells[2].Text = string.Format("<a href='../../list.aspx?table=DocumentAccounting&id={0}&itemid={1}'>{2}</a>", record["ID"], record["ItemID"], record["No"]);
            else
                e.Row.Cells[2].Text = string.Format("<a href='../../list.aspx?table=DocumentAccounting&id={0}&itemid={1}' class='no' mode='{3}' otherid='{4}'>{2}</a>", record["ID"], record["ItemID"], record["No"], mode, otherID);

            Label owe = (Label)e.Row.FindControl("owe");
            owe.Text = Helper.GetDecimal(record["Owe"], 0).ToString("N0");

            Label due = (Label)e.Row.FindControl("due");
            due.Text = Helper.GetDecimal(record["Due"], 0).ToString("N0");

            mVoucherssumDue += Helper.GetDecimal(record["Due"], 0);
            mVoucherssumOwe += Helper.GetDecimal(record["Owe"], 0);
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblTotal = (Label)e.Row.FindControl("oweTotal");
            lblTotal.Text = mVoucherssumOwe.ToString("N0");

            lblTotal = (Label)e.Row.FindControl("dueTotal");
            lblTotal.Text = mVoucherssumDue.ToString("N0");
        }
    }
    decimal mVoucherssumOwe = 0, mVoucherssumDue = 0;
    private void ShowVouchers()
    {
        mVoucherssumOwe = mVoucherssumDue = 0;
        string qry = @"SELECT DocumentAccounting.ID, DocumentAccountingItems.ID ItemID, DocumentAccounting.Date, DocumentAccounting.No
, dbo.TreePathToString(DocumentAccountingItems.TreePath, 1) AS TreePathName
, dbo.TreePathToTopicCode(DocumentAccountingItems.TreePath) AS TopicCode
, DocumentAccountingItems.Comments, DocumentAccountingItems.Owe, DocumentAccountingItems.Due 
, [dbo].[DisplayPerson](Personnel.ID, 0) AS PersonnelName, Ward.Name AS WardName, PersonnelID, ProjectID, DocumentAccounting.Mode
, DocumentAccounting.OtherID
, DATEADD(dd, 0, DATEDIFF(dd, 0, Date)) DateDD, CAST(DocumentAccounting.No AS INT) NoInt, DocumentAccountingItems.SortIndex
FROM DocumentAccounting
INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID 
LEFT OUTER JOIN Ward ON DocumentAccountingItems.ProjectID=Ward.ID WHERE 1=1";
        if (!ShowAllVouchers())
            qry += " AND DocumentAccounting.WorkflowStatus = 1";
        if (IDs.Count > 0)
        {
            if (IDs.Count == 1)
                qry += " AND DocumentAccounting.ID = " + IDs[0];
            else
            {
                qry += " AND DocumentAccounting.ID IN (";
                for (int i = 0; i < IDs.Count - 1; i++)
                    qry += IDs[i] + ", ";
                qry += IDs[IDs.Count - 1] + ")";
            }
        }
        qry += " ORDER BY DateDD, NoInt, ID, SortIndex, ItemID";
        DataTable items = DbProxy.Instance.FillData(qry, "vouchers", GetDBs(), "DateDD, NoInt, ID, SortIndex, ItemID");
        try
        {
            mVouchers.DataSource = items;
            mVouchers.DataBind();
        }
        finally
        {
            if (null != items)
                items.Dispose();
        }
    }
    #endregion

    #region personGrid
    protected void personGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView record = e.Row.DataItem as DataRowView;
            int id = Helper.GetInt(record["ID"], Null.NullInteger);
            DateTime dt = Helper.GetDateTime(record["Date"], Helper.NullDate);
            if (dt != Helper.NullDate)
            {
                e.Row.Cells[1].Text = cal.GetMonth(dt).ToString();
                e.Row.Cells[2].Text = cal.GetDayOfMonth(dt).ToString();
            }
            mVouchersRowStyle = mVouchersRowStyle == "GridAltRow" ? "GridRow" : "GridAltRow";
            e.Row.CssClass = mVouchersRowStyle;


            Label owe = (Label)e.Row.FindControl("owe");
            owe.Text = Helper.GetDecimal(record["Owe"], 0).ToString("N0");

            Label due = (Label)e.Row.FindControl("due");
            due.Text = Helper.GetDecimal(record["Due"], 0).ToString("N0");

            mVoucherssumDue += Helper.GetDecimal(record["Due"], 0);
            mVoucherssumOwe += Helper.GetDecimal(record["Owe"], 0);
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblTotal = (Label)e.Row.FindControl("oweTotal");
            lblTotal.Text = mVoucherssumOwe.ToString("N0");

            lblTotal = (Label)e.Row.FindControl("dueTotal");
            lblTotal.Text = mVoucherssumDue.ToString("N0");
        }
    }
    private void ShowPersonGrid()
    {
        mVoucherssumOwe = mVoucherssumDue = 0;
        string qry = @"SELECT DocumentAccounting.ID, DocumentAccountingItems.SortIndex, DocumentAccountingItems.ID ItemsID
, DocumentAccounting.Date, DocumentAccounting.No, dbo.TreePathToString(DocumentAccountingItems.TreePath, 1) AS TreePathName
, dbo.TreePathToTopicCode(DocumentAccountingItems.TreePath) AS TopicCode
, dbo.PersonnelTreePathToString(DocumentAccountingItems.SecPersonnelID, DocumentAccountingItems.SecTreePath, 1) PersonnelTreePathName
, DocumentAccountingItems.Comments, DocumentAccountingItems.Owe, DocumentAccountingItems.Due 
, [dbo].[DisplayPerson](Personnel.ID, 0) AS PersonnelName
, DATEADD(dd, 0, DATEDIFF(dd, 0, DocumentAccounting.Date)) DateDD, CAST(DocumentAccounting.No AS INT) NoInt
FROM DocumentAccounting 
INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID 
WHERE (DocumentAccounting.WorkflowStatus = 1 OR _ShowAllVouchers_ = 1)
_WHERE1_ _IDs_

UNION
SELECT DocumentAccounting.ID, DocumentAccountingItems.SortIndex, DocumentAccountingItems.ID
, DocumentAccounting.Date, DocumentAccounting.No, dbo.TreePathToString(dbo.GetSetting2('CheckTreePath', DocumentAccounting.ProgramID), 1) AS TreePathName
, dbo.TreePathToTopicCode(dbo.GetSetting2('CheckTreePath', DocumentAccounting.ProgramID)) AS TopicCode
, dbo.TreePathToString(Checks.TreePath, 1) PersonnelTreePathName
, DocumentAccountingItems.Comments, DocumentAccountingItems.Owe, DocumentAccountingItems.Due 
, [dbo].[DisplayPerson](Personnel.ID, 0) AS PersonnelName
, DATEADD(dd, 0, DATEDIFF(dd, 0, DocumentAccounting.Date)) DateDD, CAST(DocumentAccounting.No AS INT) NoInt
FROM DocumentAccounting 
INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
INNER JOIN Checks ON DocumentAccountingItems.CheckID=Checks.ID
LEFT OUTER JOIN Personnel ON Checks.PersonnelID=Personnel.ID 
WHERE (DocumentAccounting.WorkflowStatus = 1 OR _ShowAllVouchers_ = 1) AND Checks.TreePath IS NOT NULL 
_WHERE2_ _IDs_

UNION
SELECT DocumentAccounting.ID, DocumentAccountingItems.SortIndex, DocumentAccountingItems.ID
, DocumentAccounting.Date, DocumentAccounting.No, dbo.TreePathToString(dbo.GetSetting2('PayCheckTreePath', DocumentAccounting.ProgramID), 1) AS TreePathName
, dbo.TreePathToTopicCode(dbo.GetSetting2('PayCheckTreePath', DocumentAccounting.ProgramID)) AS TopicCode
, dbo.TreePathToString(PayCheck.TreePath, 1) PersonnelTreePathName
, DocumentAccountingItems.Comments, DocumentAccountingItems.Owe, DocumentAccountingItems.Due 
, [dbo].[DisplayPerson](Personnel.ID, 0) AS PersonnelName
, DATEADD(dd, 0, DATEDIFF(dd, 0, DocumentAccounting.Date)) DateDD, CAST(DocumentAccounting.No AS INT) NoInt
FROM DocumentAccounting 
INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
INNER JOIN PayCheck ON DocumentAccountingItems.PayCheckID=PayCheck.ID
LEFT OUTER JOIN Personnel ON PayCheck.ReceiverID = Personnel.ID 
WHERE (DocumentAccounting.WorkflowStatus = 1 OR _ShowAllVouchers_ = 1) AND PayCheck.TreePath IS NOT NULL 
_WHERE2_ _IDs_".Replace("_ShowAllVouchers_", ShowAllVouchers() ? "1" : "0");
        string w = "";
        qry = qry.Replace(" _IDs_", w);
        int personID = Helper.GetInt(mPersons.SelectedValue, Null.NullInteger);
        if (personID != Null.NullInteger)
            qry = qry.Replace("_WHERE1_", " AND DocumentAccountingItems.SecPersonnelID = " + personID)
                .Replace("_WHERE2_", " AND Personnel.ID = " + personID);
        qry = qry.Replace("_WHERE1_", " ").Replace("_WHERE2_", " ");
        qry += " ORDER BY DateDD, NoInt, ID, SortIndex, ItemsID";
        DataTable items = DbProxy.Instance.FillData(qry, "persons", GetDBs(), "DateDD, NoInt, ID, SortIndex, ItemsID");
        try
        {
            personGrid.DataSource = items;
            personGrid.DataBind();
        }
        finally
        {
            if (null != items)
                items.Dispose();
        }
    }
    #endregion

    #region Factors
    string mFactorsRowStyle = "GridRow";
    int lastMode = -1, lastFactorID = -1;
    decimal amount = 0, price = 0, discount = 0, tax = 0, singleVat = 0, total = 0;
    decimal sumAmount = 0, sumPrice = 0, sumDiscount = 0, sumTax = 0, sumSingleVat = 0, sumTotal = 0;
    decimal fsumAmount = 0, fsumPrice = 0, fsumDiscount = 0, fsumTax = 0, fsumSingleVat = 0, fsumTotal = 0;
    protected void mFactors_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            bool isStartRow = false;
            DataRowView record = e.Row.DataItem as DataRowView;
            DateTime dt = Helper.GetDateTime(record["Date"], Helper.NullDate);
            e.Row.Cells[1].Text = Helper.ToFaFormat(Helper.GetDateTime(dt, Helper.NullDate));
            int id = Helper.GetInt(record["ID"], Null.NullInteger);
            int mode = Helper.GetInt(record["Mode"], Null.NullInteger);
            if (id != lastFactorID || mode != lastMode)
            {
                if (lastMode != -1)
                    isStartRow = true;
                lastFactorID = id;
                lastMode = mode;
                mFactorsRowStyle = mFactorsRowStyle == "GridAltRow" ? "GridRow" : "GridAltRow";
            }
            e.Row.CssClass = mFactorsRowStyle;

            string startRow = "";
            if (isStartRow)
            {
                startRow = string.Format("<div class='subSum' fa='{0}' fp='{1}' fd='{2}' ft='{3}' fs='{4}' tp='{5}'></div>", fsumAmount, fsumPrice, fsumDiscount, fsumTax, fsumSingleVat, fsumTotal);
                fsumAmount = fsumPrice = fsumDiscount = fsumTax = fsumSingleVat = fsumTotal = 0;
            }

            amount = Helper.GetDecimal(record["Amount"], 0);
            discount = Helper.GetDecimal(record["Discount"], 0);
            price = Helper.GetDecimal(record["Price"], 0);
            tax = Helper.GetDecimal(record["Tax"], 0);
            singleVat = Helper.GetDecimal(record["SingleVat"], 0);

            string table = "", title = "";
            if (mode == 4)
            {
                table = "Purchase";
                title = LangProvider.Instance["SalesPurchase"];
                total = (amount * price) - (discount + singleVat + tax);
            }
            else if (mode == 7)
            {
                table = "StoreDraft";
                title = LangProvider.Instance["SalesInvoice"];
                total = (amount * price) + (singleVat + tax) - (discount);
            }
            else if (mode == 6)
            {
                table = "Returns";
                title = LangProvider.Instance["ReturnInvoice"];
                total = (amount * price) - (discount + singleVat + tax);
            }
            else if (mode == 5)
            {
                table = "Refuse";
                title = LangProvider.Instance["RefuseInvoice"];
                total = (amount * price) + (singleVat + tax) - (discount);
            }
            title += startRow;
            e.Row.Cells[7].Text = SiteSettings.ToMonetary(price, DateTime.Now).ToString("N0");
            e.Row.Cells[8].Text = SiteSettings.ToMonetary(discount, DateTime.Now).ToString("N0");
            e.Row.Cells[9].Text = SiteSettings.ToMonetary(tax, DateTime.Now).ToString("N0");
            e.Row.Cells[10].Text = SiteSettings.ToMonetary(singleVat, DateTime.Now).ToString("N0");
            e.Row.Cells[11].Text = SiteSettings.ToMonetary(total, DateTime.Now).ToString("N0");
            e.Row.Cells[2].Text = title;
            e.Row.Cells[3].Text = string.Format("<a href='../../list.aspx?table={0}&menu=accountinga&id={1}'>{2}</a>", table, record["ID"], record["NOSerial"]);

            sumAmount += amount;
            sumDiscount += discount;
            sumPrice += price;
            sumTax += tax;
            sumSingleVat += singleVat;
            sumTotal += total;

            fsumAmount += amount;
            fsumDiscount += discount;
            fsumPrice += price;
            fsumTax += tax;
            fsumSingleVat += singleVat;
            fsumTotal += total;
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblSumNumOrValue = (Label)e.Row.FindControl("SumNumOrValue");
            lblSumNumOrValue.Text = sumAmount.ToString("N0");

            Label lblSumPrice = (Label)e.Row.FindControl("SumPrice");
            lblSumPrice.Text = "";//SiteSettings.ToMonetary(sumPrice, DateTime.Now).ToString("N0");

            Label lblSumDiscount = (Label)e.Row.FindControl("SumDiscount");
            lblSumDiscount.Text = SiteSettings.ToMonetary(sumDiscount, DateTime.Now).ToString("N0");

            Label lblSumTax = (Label)e.Row.FindControl("SumTax");
            lblSumTax.Text = SiteSettings.ToMonetary(sumTax, DateTime.Now).ToString("N0");

            Label lblSumSingleVat = (Label)e.Row.FindControl("SumSingleVat");
            lblSumSingleVat.Text = SiteSettings.ToMonetary(sumSingleVat, DateTime.Now).ToString("N0");

            Label lblSumTotal = (Label)e.Row.FindControl("SumTotal");
            lblSumTotal.Text = SiteSettings.ToMonetary(sumTotal, DateTime.Now).ToString("N0");

            e.Row.Cells[2].Text = string.Format("<div class='subSum' fa='{0}' fp='{1}' fd='{2}' ft='{3}' fs='{4}' tp='{5}'></div>", fsumAmount, fsumPrice, fsumDiscount, fsumTax, fsumSingleVat, fsumTotal);
        }
    }
    private void ShowFactors()
    {
        string qry = @"SELECT 4 Mode, Purchase.ID, ISNULL(Purchase.ReceiptDate, Purchase.Date) Date, Purchase.NOSerial, StoreServiceName, UnitName, UnitCurrencyConverter * Amount AS Amount, Price
, CASE WHEN Discount >= 0 THEN Discount ELSE -1 *(Discount/100 * Price) END Discount
, CASE WHEN Tax >= 0 THEN Tax ELSE -1 * (Tax/100 * Price) END Tax
, CASE WHEN SingleVAT >= 0 THEN SingleVAT ELSE -1 * (SingleVAT/100 * Price) END SingleVAT
FROM PurchaseItemsView LEFT JOIN Purchase ON PurchaseItemsView.PurchaseID=Purchase.ID 
WHERE Purchase.ID IN (SELECT OtherID FROM DocumentAccounting WHERE Mode=4 _MORE_)

UNION

SELECT 7 Mode, StoreDraft.ID, ISNULL(StoreDraft.FactorDate, StoreDraft.Date) Date, StoreDraft.NOSerial, StoreServiceName, UnitName, UnitCurrencyConverter * DeliveredAmount AS Amount, Price
, CASE WHEN Discount >= 0 THEN Discount ELSE -1 *(Discount/100 * Price) END Discount
, CASE WHEN Tax >= 0 THEN Tax ELSE -1 * (Tax/100 * Price) END Tax
, CASE WHEN SingleVAT >= 0 THEN SingleVAT ELSE -1 * (SingleVAT/100 * Price) END SingleVAT 
FROM StoreDraftItemsView LEFT JOIN StoreDraft ON StoreDraftItemsView.StoreDraftID=StoreDraft.ID
WHERE StoreDraft.ID IN (SELECT OtherID FROM DocumentAccounting WHERE Mode=7 _MORE_)

UNION

SELECT 5 Mode, Refuse.ID, Refuse.RefuseDate Date, Refuse.NOSerial, StoreServiceName, UnitName, Num * ISNULL(UnitCurrencyConverter, 1) Amount, Price
, CASE WHEN Discount >= 0 THEN Discount ELSE -1 *(Discount/100 * Price) END Discount
, CASE WHEN Tax >= 0 THEN Tax ELSE -1 * (Tax/100 * Price) END Tax
, CASE WHEN SingleVAT >= 0 THEN SingleVAT ELSE -1 * (SingleVAT/100 * Price) END SingleVAT
FROM RefuseItemView LEFT JOIN Refuse ON RefuseItemView.RefuseID=Refuse.ID
WHERE Refuse.ID IN (SELECT OtherID FROM DocumentAccounting WHERE Mode=5 _MORE_)

UNION

SELECT 6 Mode, Returns.ID, Returns.ReturnDate Date, Returns.NOSerial, StoreServiceName, UnitName, Num * ISNULL(UnitCurrencyConverter, 1) Amount, Price
, CASE WHEN Discount >= 0 THEN Discount ELSE -1 *(Discount/100 * Price) END Discount
, CASE WHEN Tax >= 0 THEN Tax ELSE -1 * (Tax/100 * Price) END Tax
, CASE WHEN SingleVAT >= 0 THEN SingleVAT ELSE -1 * (SingleVAT/100 * Price) END SingleVAT
FROM ReturnItemView LEFT JOIN Returns ON ReturnItemView.ReturnID=Returns.ID
WHERE Returns.ID IN (SELECT OtherID FROM DocumentAccounting WHERE Mode=6 _MORE_)
";
        string more = " ";
        if (!ShowAllVouchers())
            more += " AND DocumentAccounting.WorkflowStatus = 1";
        if (IDs.Count > 0)
        {

            if (IDs.Count == 1)
                more += " AND DocumentAccounting.ID = " + IDs[0];
            else
            {
                more += " AND DocumentAccounting.ID IN (";
                for (int i = 0; i < IDs.Count - 1; i++)
                    more += IDs[i] + ", ";
                more += IDs[IDs.Count - 1] + ")";
            }
        }
        qry += " ORDER BY Mode, Date, NOSerial";
        qry = qry.Replace("_MORE_", more);
        DataTable items = DbProxy.Instance.FillData(qry, "factors", GetDBs(), "Mode, Date, NOSerial");
        try
        {
            mFactors.DataSource = items;
            mFactors.DataBind();
        }
        finally
        {
            if (null != items)
                items.Dispose();
        }
    }
    #endregion

    #region checkGrid
    void BindChecks()
    {
        //SiteSettings.User.PersonnelID
        //using (DbProxy proxy = new DbProxy())
        {
            string qry = null;
            /* string qry = @"SELECT PayCheck.ID, PayCheck.DueDate, PayCheck.Serial, PayCheck.Amount, N'پرداختی' AS Type, Personnel.Name + ' ' + Personnel.Surname AS Person FROM PayCheck 
 LEFT JOIN Personnel ON PayCheck.ReceiverID = Personnel.ID
 WHERE Status=0

 UNION
 SELECT Checks.ID, Checks.DueDate, Checks.[No] AS Serial, Checks.Amount, N'دریافتی' AS Type, Personnel.Name + ' ' + Personnel.Surname AS Person FROM Checks 
 LEFT JOIN Personnel ON Checks.PersonnelID = Personnel.ID
 WHERE Status = 0 ";*/
            if ((GetByValue() == AccController.PersonBookGroupBy.ByPersonnel) && Helper.GetInt(mPersons.SelectedValue, Null.NullInteger) != Null.NullInteger)
                qry = string.Format(@"SELECT PayCheck.ID, PayCheck.DueDate, PayCheck.Serial
, PayCheck.Amount, N'پرداختی' AS Type
, [dbo].[DisplayPerson](Personnel.ID, 0) AS Person, [dbo].[GetCheckStatusName](Status) AS Status
, PayCheck.PayDate FROM PayCheck 
LEFT JOIN Personnel ON PayCheck.ReceiverID = Personnel.ID
WHERE Status NOT IN(1,2) AND ReceiverID = {0}

UNION
SELECT Checks.ID, Checks.DueDate, Checks.[No] AS Serial, Checks.Amount, N'دریافتی' AS Type
, [dbo].[DisplayPerson](Personnel.ID, 0) AS Person, [dbo].[GetCheckStatusName](Status) AS Status
, Checks.IssueDate AS PayDate FROM Checks 
LEFT JOIN Personnel ON Checks.PersonnelID = Personnel.ID
WHERE Status NOT IN(1,2,5) AND PersonnelID = {0}", Helper.GetInt(mPersons.SelectedValue, Null.NullInteger));
            DataTable reader = DbProxy.Instance.FillData(qry, "checks", GetDBs(), null);
            checkGrid.DataSource = reader;
            checkGrid.DataBind();
        }
    }

    protected decimal sumCheckAmount = 0;
    protected void checkGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView record = e.Row.DataItem as DataRowView;
            DateTime pdt = Helper.GetDateTime(record["PayDate"], Helper.NullDate);
            DateTime dt = Helper.GetDateTime(record["DueDate"], Helper.NullDate);
            if (dt != Helper.NullDate)
            {
                e.Row.Cells[1].Text = Helper.ToFaFormat(dt);
            }
            e.Row.Cells[3].Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["Amount"], 0), pdt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            sumCheckAmount += SiteSettings.ToMonetary(Helper.GetDecimal(record["Amount"], 0), pdt);

        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblSumAmount = (Label)e.Row.FindControl("SumAmount");
            lblSumAmount.Text = sumCheckAmount.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }
    }
    #endregion

    #region delayGrid
    void BindDelays()
    {
        //SiteSettings.User.PersonnelID
        // using (DbProxy proxy = new DbProxy())
        {
            string qry = null;
            if ((GetByValue() == AccController.PersonBookGroupBy.ByPersonnel) &&
                Helper.GetInt(mPersons.SelectedValue, Null.NullInteger) != Null.NullInteger)
            {
                delayGrid.Columns[2].Visible = false;
                qry = string.Format(@"SELECT 1 Type, ID , No, Amount, DueDate AS PayDate, DATEDIFF( DAY, DueDate, GETDATE()) AS Delay  
                                    , IsShop, PersonnelName, [dbo].[GetCheckStatusName](Status) AS Status from CheckView 
                                    WHERE DueDate < GETDATE() AND Status IN (0, 3, 4, 6) AND ((PersonnelID = {0} AND AssignorID IS NULL) 
                                    OR (AssignorID = {0}))
                                    UNION
                                    SELECT 2 Type, ID, NOSerial AS No, ((TotalPrice + TotalTax + VAT) - TotalDiscount)
                                    + CASE WHEN SellerShipment = 0 THEN ShipmentPrice ELSE 0 END AS Amount
                                    , PayDate, DATEDIFF( DAY, PayDate, GETDATE()) AS Delay, NULL AS IsShop, BuyerName AS PersonnelName 
                                    , [dbo].[GetStoredraftPayStatusName](PayStatus) AS Status FROM StoreDraftView
                                    WHERE PayMethod IN (2 ,3) AND PayDate < GETDATE() AND PayStatus = 0 AND BuyerID = {0}", Helper.GetInt(mPersons.SelectedValue, Null.NullInteger));

            }
            else if (GetByValue() == AccController.PersonBookGroupBy.ByOwe && mShowDelays.Checked)
            {
                delayGrid.Columns[2].Visible = true;
                string sFilter = GetFilter("storeDraftDelay");
                string chFilter = GetFilter("checkDelay");

                qry = string.Format(
                  @"SELECT 1 Type, ID , No, Amount, DueDate AS PayDate, DATEDIFF( DAY, DueDate, GETDATE()) AS Delay  
                                    , IsShop, PersonnelName, [dbo].[GetCheckStatusName](Status) AS Status  from CheckView 
                                    WHERE DueDate < GETDATE() AND Status IN (0, 3, 4, 6)
                                    UNION
                                    SELECT 2 Type, ID, NOSerial AS No, ((TotalPrice + TotalTax + VAT) - TotalDiscount)
                                    + CASE WHEN SellerShipment = 0 THEN ShipmentPrice ELSE 0 END AS Amount
                                    , PayDate, DATEDIFF( DAY, PayDate, GETDATE()) AS Delay, NULL AS IsShop, BuyerName AS PersonnelName
                                    , [dbo].[GetStoredraftPayStatusName](PayStatus) AS Status FROM StoreDraftView
                                    WHERE PayMethod IN (2 ,3) AND PayDate < GETDATE() AND PayStatus = 0");
                if (!string.IsNullOrEmpty(sFilter) && !string.IsNullOrEmpty(chFilter))
                {
                    qry = string.Format(
                            @"SELECT 1 Type, ID , No, Amount, DueDate AS PayDate, DATEDIFF( DAY, DueDate, GETDATE()) AS Delay  
                                    , IsShop, PersonnelName, [dbo].[GetCheckStatusName](Status) AS Status  from CheckView 
                                    WHERE DueDate < GETDATE() AND Status IN (0, 3, 4, 6)
                                     AND {0}
                                    UNION
                                    SELECT 2 Type, ID, NOSerial AS No, ((TotalPrice + TotalTax + VAT) - TotalDiscount)
                                    + CASE WHEN SellerShipment = 0 THEN ShipmentPrice ELSE 0 END AS Amount
                                    , PayDate, DATEDIFF( DAY, PayDate, GETDATE()) AS Delay, NULL AS IsShop, BuyerName AS PersonnelName
                                    , [dbo].[GetStoredraftPayStatusName](PayStatus) AS Status FROM StoreDraftView
                                    WHERE PayMethod IN (2 ,3) AND PayDate < GETDATE() AND PayStatus = 0 AND {1}"
                                    , chFilter, sFilter);
                }
            }
            DataTable reader = DbProxy.Instance.FillData(qry, "delays", GetDBs(), null);
            delayGrid.DataSource = reader;
            delayGrid.DataBind();
        }
    }

    protected decimal sumDelayAmount = 0;
    protected void delayGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView record = e.Row.DataItem as DataRowView;
            DateTime dt = Helper.GetDateTime(record["PayDate"], Helper.NullDate);
            if (dt != Helper.NullDate)
            {
                e.Row.Cells[5].Text = Helper.ToFaFormat(dt);
            }
            int type = Helper.GetInt(record["Type"], Null.NullInteger);
            string table = "", title = "";
            if (type == 1)
            {
                if (Helper.GetInt(record["Type"], Null.NullInteger) == 1)
                {
                    table = "Checks&view=checksshops";
                }
                else
                {
                    table = "Checks&view=checks";
                }
                title = LangProvider.Instance["Check"];

            }
            else if (type == 2)
            {
                table = "StoreDraft";
                title = LangProvider.Instance["SalesInvoice"];
            }
            e.Row.Cells[1].Text = title;
            e.Row.Cells[3].Text = string.Format("<a href='../../list.aspx?table={0}&id={1}'>{2}</a>", table, record["ID"], record["No"]);
            e.Row.Cells[4].Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["Amount"], 0), DateTime.Now).ToString("N0");
            sumDelayAmount += Helper.GetDecimal(record["Amount"], 0);

        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblSumAmount = (Label)e.Row.FindControl("SumAmount");
            lblSumAmount.Text = SiteSettings.ToMonetary(sumDelayAmount, DateTime.Now).ToString("N0");
        }
    }
    #endregion

    protected AccController.PersonBookGroupBy GetByValue()
    {
        return (AccController.PersonBookGroupBy)(int.Parse(ddlBy.SelectedValue));
    }
    protected bool HasPersonnel()
    {
        AccController.PersonBookGroupBy by = GetByValue();
        return (by == AccController.PersonBookGroupBy.ByPersonnel || by == AccController.PersonBookGroupBy.ByOweDue
                    || by == AccController.PersonBookGroupBy.ByOwe || by == AccController.PersonBookGroupBy.ByDue);
    }

    private bool ShowAllVouchers()
    {
        return mShowInProgressVouchers.Checked;
    }

    protected void doExcel_Click(object sender, EventArgs e)
    {
        Exporter.ExportExcel(Response
            , new List<string>(new string[] { mGridTitle.Text, mProjectGridTitle.Text, checkGridTitle.Text, delayGridTitle.Text, mCoworkerPersonTitle.Text, personGridTitle.Text, mVouchersTitle.Text })
            , new List<GridView>(new GridView[] { mGrid, mProjectGrid, checkGrid, delayGrid, mCoworkerPerson, personGrid, mVouchers }));
    }
    protected void doExcelFactor_Click(object sender, EventArgs e)
    {
        Exporter.ExportExcel(Response
            , new List<string>(new string[] { mFactorsTitle.Text })
            , new List<GridView>(new GridView[] { mFactors }));
    }


    protected void mGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        BindGrid();
    }

    protected string SortCol
    {
        get
        {
            return Helper.GetString(ViewState["SortCol"], null);
        }
        set
        {
            ViewState["SortCol"] = value;
        }
    }
    protected string SortOrder
    {
        get
        {
            return Helper.GetString(ViewState["SortOrder"], "asc");
        }
        set
        {
            ViewState["SortOrder"] = value;
        }
    }

    // ------------------------
    // ids: 1,2,5
    // where: '&1%' AND ('&2%' OR '&5%')
    // ------------------------
    private string GetFilter(string type)
    {
        string mgIDs = "", pgIDs = "", wardIDs = "", eqIDs = "", ccIDs = "";
        string where = "";
        if (!panelDropDowns.Visible)
            return null;

        #region pass 1: find IDs and simplify
        // grouping members : '&1%', '!&1%'
        // category = 1375 AND ( category != 1076 OR category = 1075 ) 
        // --> '&1375%' AND ('!&1076%' OR '&1075%')
        where = mQueryBuilderSql.Value.Trim();
        where += " ";// we need this space at end to find IDs
        while (where.Contains("category = "))
        {
            int index = where.IndexOf("category = ") + "category = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            mgIDs += id + ",";
            where = where.Replace("category = " + id + " ", "'&" + id + "%' ");
        }
        while (where.Contains("category != "))
        {
            int index = where.IndexOf("category != ") + "category != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            mgIDs += id + ",";
            where = where.Replace("category != " + id + " ", "'!&" + id + "%' ");
        }
        if (mgIDs.EndsWith(","))
            mgIDs = mgIDs.Substring(0, mgIDs.Length - ",".Length);


        // personnel groups: '^1%', '!^1%'
        while (where.Contains("pcat = "))
        {
            int index = where.IndexOf("pcat = ") + "pcat = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pgIDs += id + ",";
            where = where.Replace("pcat = " + id + " ", "'^" + id + "%' ");
        }
        while (where.Contains("pcat != "))
        {
            int index = where.IndexOf("pcat != ") + "pcat != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pgIDs += id + ",";
            where = where.Replace("pcat != " + id + " ", "'!^" + id + "%' ");
        }
        if (pgIDs.EndsWith(","))
            pgIDs = pgIDs.Substring(0, pgIDs.Length - ",".Length);

        // Wards: '*1%', '!*1%'
        while (where.Contains("wardcat = "))
        {
            int index = where.IndexOf("wardcat = ") + "wardcat = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index).TrimStart('0'), -1);
            wardIDs += id + ",";
            where = where.Replace("wardcat = " + id + " ", "'*" + id + "%' ");
        }
        while (where.Contains("wardcat != "))
        {
            int index = where.IndexOf("wardcat != ") + "wardcat != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            wardIDs += id + ",";
            where = where.Replace("wardcat != " + id + " ", "'!*" + id + "%' ");
        }
        if (wardIDs.EndsWith(","))
            wardIDs = wardIDs.Substring(0, wardIDs.Length - ",".Length);

        // Equipments: '+1%', '!+1%'
        while (where.Contains("eqcat = "))
        {
            int index = where.IndexOf("eqcat = ") + "eqcat = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            eqIDs += id + ",";
            where = where.Replace("eqcat = " + id + " ", "'+" + id + "%' ");
        }
        while (where.Contains("eqcat != "))
        {
            int index = where.IndexOf("eqcat != ") + "eqcat != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            eqIDs += id + ",";
            where = where.Replace("eqcat != " + id + " ", "'!+" + id + "%' ");
        }
        if (wardIDs.EndsWith(","))
            eqIDs = wardIDs.Substring(0, wardIDs.Length - ",".Length);

        // CostCenterIDs: '$1%', '!$1%'
        while (where.Contains("cccat = "))
        {
            int index = where.IndexOf("cccat = ") + "cccat = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            ccIDs += id + ",";
            where = where.Replace("cccat = " + id + " ", "'$" + id + "%' ");
        }
        while (where.Contains("cccat != "))
        {
            int index = where.IndexOf("cccat != ") + "cccat != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            ccIDs += id + ",";
            where = where.Replace("cccat != " + id + " ", "'!$" + id + "%' ");
        }
        if (wardIDs.EndsWith(","))
            ccIDs = wardIDs.Substring(0, wardIDs.Length - ",".Length);
        #endregion

        if (string.IsNullOrEmpty(where))
            return "";
        if (string.IsNullOrEmpty(pgIDs) && string.IsNullOrEmpty(mgIDs) && string.IsNullOrEmpty(wardIDs) && string.IsNullOrEmpty(eqIDs) && string.IsNullOrEmpty(ccIDs))
            return "";

        if (type == "checkDelay")
        {
            if (!string.IsNullOrEmpty(mgIDs))
            {
                // grouping members : '&1%', '!&1%'
                string[] ids = mgIDs.Split(',');
                for (int i = 0; i < ids.Length; i++)
                {
                    StringBuilder allIds = new StringBuilder();
                    int j = 0;
                    IDataReader childIds =
                        DbProxy.Instance.ExecuteReader("SELECT ID FROM [GetAllGroupingChilds](" + ids[i] + ")");
                    while (childIds.Read())
                    {
                        if (j != 0)
                        {
                            allIds.Append(",");
                        }
                        allIds.Append(childIds.GetValue(0));
                        j++;
                    }
                    where = where.Replace("'&" + ids[i] + "%'",
                        " PersonnelID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds +
                        ")) \r\n");
                    where = where.Replace("'!&" + ids[i] + "%'",
                        " NOT PersonnelID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds +
                        ")) \r\n");
                    //where1 += "AND Personnel.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + "))";
                }
            }

        }
        else if (type == "storeDraftDelay")
        {
            if (!string.IsNullOrEmpty(mgIDs))
            {
                // grouping members : '&1%', '!&1%'
                string[] ids = mgIDs.Split(',');
                for (int i = 0; i < ids.Length; i++)
                {
                    StringBuilder allIds = new StringBuilder();
                    int j = 0;
                    IDataReader childIds =
                        DbProxy.Instance.ExecuteReader("SELECT ID FROM [GetAllGroupingChilds](" + ids[i] + ")");
                    while (childIds.Read())
                    {
                        if (j != 0)
                        {
                            allIds.Append(",");
                        }
                        allIds.Append(childIds.GetValue(0));
                        j++;
                    }
                    where = where.Replace("'&" + ids[i] + "%'",
                        " BuyerID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds +
                        ")) \r\n");
                    where = where.Replace("'!&" + ids[i] + "%'",
                        " NOT BuyerID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds +
                        ")) \r\n");
                    //where1 += "AND Personnel.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + "))";
                }
            }
        }
        else
        {
            // ------------------------
            // ids: 1,2,5
            // where: '&1%' AND ('&2%' OR '&5%')
            // it could be better, but i dont know regex well :(
            // ------------------------
            if (!string.IsNullOrEmpty(mgIDs))
            {
                // grouping members : '&1%', '!&1%'
                string[] ids = mgIDs.Split(',');
                for (int i = 0; i < ids.Length; i++)
                {
                    StringBuilder allIds = new StringBuilder();
                    int j = 0;
                    IDataReader childIds =
                        DbProxy.Instance.ExecuteReader("SELECT ID FROM [GetAllGroupingChilds](" + ids[i] + ")");
                    while (childIds.Read())
                    {
                        if (j != 0)
                        {
                            allIds.Append(",");
                        }
                        allIds.Append(childIds.GetValue(0));
                        j++;
                    }
                    where = where.Replace("'&" + ids[i] + "%'",
                        " Personnel.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds +
                        ")) \r\n");
                    where = where.Replace("'!&" + ids[i] + "%'",
                        " NOT Personnel.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds +
                        ")) \r\n");
                    //where1 += "AND Personnel.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + "))";
                }
            }

            if (!string.IsNullOrEmpty(pgIDs))
            {
                // personnel groups: '^1%', '!^1%'
                string[] ids = pgIDs.Split(',');
                for (int i = 0; i < ids.Length; i++)
                {
                    where = where.Replace("'^" + ids[i] + "%'",
                        " Personnel.ID IN (SELECT ID FROM Personnel WHERE PersonnelGroupID=" + ids[i] + ") \r\n");
                    where = where.Replace("'!^" + ids[i] + "%'",
                        " NOT Personnel.ID IN (SELECT ID FROM Personnel WHERE PersonnelGroupID=" + ids[i] + ") \r\n");
                }
            }


            if (!string.IsNullOrEmpty(wardIDs))
            {
                // Wards: '*1%', '!*1%'
                string[] ids = wardIDs.Split(',');
                for (int i = 0; i < ids.Length; i++)
                {
                    where = where.Replace("'*" + ids[i] + "%'",
                        " Ward.ID IN (SELECT ID FROM [dbo].[GetAllWardChilds](" + ids[i] + ")) \r\n");
                    where = where.Replace("'!*" + ids[i] + "%'",
                        " NOT Ward.ID IN (SELECT ID FROM [dbo].[GetAllWardChilds](" + ids[i] + "))\r\n");
                }
            }

            if (!string.IsNullOrEmpty(eqIDs))
            {
                // Equipments: '+1%', '!+1%'
                string[] ids = eqIDs.Split(',');
                for (int i = 0; i < ids.Length; i++)
                {
                    where = where.Replace("'+" + ids[i] + "%'",
                        " EquipmentView.ID IN (SELECT ID FROM [dbo].[GetAllEquipmentChilds](" + ids[i] + ")) \r\n");
                    where = where.Replace("'!+" + ids[i] + "%'",
                        " NOT EquipmentView.ID IN (SELECT ID FROM [dbo].[GetAllEquipmentChilds](" + ids[i] + "))\r\n");
                }
            }

            if (!string.IsNullOrEmpty(ccIDs))
            {
                // CostCenterIDs: '$1%', '!$1%'
                string[] ids = ccIDs.Split(',');
                for (int i = 0; i < ids.Length; i++)
                {
                    where = where.Replace("'$" + ids[i] + "%'",
                        " CostCenterView.ID IN (SELECT ID FROM [dbo].[GetAllCostCenterChilds](" + ids[i] + ")) \r\n");
                    where = where.Replace("'!$" + ids[i] + "%'",
                        " NOT CostCenterView.ID IN (SELECT ID FROM [dbo].[GetAllCostCenterChilds](" + ids[i] + "))\r\n");
                }
            }
        }

        return where;
    }
}
