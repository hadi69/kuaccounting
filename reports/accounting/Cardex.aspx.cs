﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reports_accounting_Cardex : System.Web.UI.Page
{
    System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
    protected void Page_Load(object sender, EventArgs e)
    {
        mGrid.EmptyDataText = LangProvider.Instance["DocumentIsEmpty"];
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");
        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["Cardex"];
            Page.SetLang();
            int year = cal.GetYear(DateTime.Now);
            //mFromDate.Text = string.Format("{0}/01/01", year - 1300);
            //mToDate.Text = Helper.FormatDate(cal.ToDateTime(year + 1, 1, 1, 1, 1, 1, 1).AddDays(-1), "yy/MM/dd");
            IDataReader financeYearStartDate = DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");//0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            //mFromDate.Text = 
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);

            Init();

            mReportSetting.ReportName = "Cardex";
        }
        mReportSetting.SaveClick += new EventHandler(mReportSetting_SaveClick);
        mReportSetting.ReportSettingSelect += new EventHandler(mReportSetting_ReportSettingSelect);
    }

    void mReportSetting_ReportSettingSelect(object sender, EventArgs e)
    {
        ReportSettingInfo info = mReportSetting.SelectedReportSettings;
        if (info == null)
            return;
        string[] lines = info.Settings.Split('\n');
        mServiceName.Text = lines[0].Trim('\r');
        mFromDate.Text = lines[1].Trim('\r');
        mToDate.Text = lines[2].Trim('\r');
    }

    void mReportSetting_SaveClick(object sender, EventArgs e)
    {
        StringBuilder b = new StringBuilder();
        b.AppendLine(mServiceName.Text); //0
        b.AppendLine(mFromDate.Text); //1
        b.AppendLine(mToDate.Text); //2
        mReportSetting.Save(b.ToString());
    }
    void Init()
    {
        IDataReader persons = DbProxy.Instance.ExecuteReader("SELECT ID, [dbo].[DisplayPerson](ID, 1), ISNULL(Code, '') FROM Personnel WHERE Enabled = 1 AND Type=101");
        mPersons.Items.Clear();
        mPersons.Items.Add(new ListItem(LangProvider.Instance["AllPersons"], ""));
        if (persons != null)
        {
            while (persons.Read())
                mPersons.Items.Add(new ListItem(persons.GetString(1) + " - کد: " + persons.GetString(2), persons.GetInt32(0).ToString()));
            persons.Close();
        }
        mPersons.SelectedIndex = 0;

        IDataReader projects = DbProxy.Instance.ExecuteReader("SELECT ID, Title FROM ServiceType");
        mServiceTypes.Items.Clear();
        mServiceTypes.Items.Add(new ListItem(LangProvider.Instance["AllCommodityGroups"], ""));
        if (projects != null)
        {
            while (projects.Read())
                mServiceTypes.Items.Add(new ListItem(projects.GetString(1), projects.GetInt32(0).ToString()));
            projects.Close();
        }
        mServiceTypes.SelectedIndex = 0;

        IDataReader Grouping = DbProxy.Instance.ExecuteReader("SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping");
        mPZGroup.Items.Clear();
        mPZGroup.Items.Add(new ListItem(LangProvider.Instance["AllGroup"], ""));
        if (Grouping != null)
        {
            while (Grouping.Read())
                mPZGroup.Items.Add(new ListItem(Grouping.IsDBNull(1) ? "" : Grouping.GetString(1), Grouping.GetInt32(0).ToString()));
            Grouping.Close();
        }
        mPZGroup.SelectedIndex = 0;

        IDataReader Grouping1 = DbProxy.Instance.ExecuteReader("SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping");
        mSZGroup.Items.Clear();
        mSZGroup.Items.Add(new ListItem(LangProvider.Instance["AllGroup"], ""));
        if (Grouping1 != null)
        {
            while (Grouping1.Read())
                mSZGroup.Items.Add(new ListItem(Grouping1.IsDBNull(1) ? "" : Grouping1.GetString(1), Grouping1.GetInt32(0).ToString()));
            Grouping1.Close();
        }
        mSZGroup.SelectedIndex = 0;
    }
    string rowStyle = "GridRow";
    decimal f = 0, g = 0, numPurchases = 0, numStoreDrafts = 0, m = 0, profit = 0;
    int lastID = -2;
    protected void BindGrid()
    {
        DateTime start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        DateTime end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);

        /*
         SELECT StoreServicesView.ID, StoreServiceDefView.ServiceName, StoreServiceDefView.StoreTitle, StoreServicesView.StoreID, Cardex.Income, Cardex.Outcome, Cardex.Price, Cardex.Type, Cardex.Date FROM 
Cardex INNER JOIN StoreServicesView ON Cardex.StoreServiceID = StoreServicesView.ID
INNER JOIN StoreServiceDefView ON StoreServiceDefView.ID = StoreServicesView.StoreServiceDefID
WHERE StoreServiceDefView.ServiceName LIKE '%{0}%' 
AND (Cardex.Date IS NULL OR Cardex.Date >= {1}) 
AND (Cardex.Date IS NULL OR Cardex.Date <= {2})
ORDER BY StoreServiceDefView.ServicePriceID, Cardex.ID*/
        string what = mServiceName.Text.Trim().Replace("'", "''");

        string seller = "", buyer = "", retrn = "", service = "";
        int personID = Helper.GetInt(mPersons.SelectedValue, Null.NullInteger);
        if (personID != Null.NullInteger)
        {
            seller = " AND PurchaseItems.PurchaseID IN (SELECT ID FROM Purchase WHERE SellerID = " + personID + ")";
            buyer = " AND StoreDraftItems.StoreDraftID IN (SELECT ID FROM StoreDraft WHERE BuyerID = " + personID + ")";
            retrn = " AND ReturnItem.ReturnID IN (SELECT ID FROM Returns WHERE BuyerID = " + personID + ")";
        }
        int GroupingID = Helper.GetInt(mPZGroup.SelectedValue, Null.NullInteger);
        if (GroupingID != Null.NullInteger)
        {
            seller += " AND PurchaseItems.PurchaseID IN (SELECT ID FROM Purchase WHERE SellerID IN ( SELECT OtherID FROM GroupingMember WHERE GroupingID = " + GroupingID + " AND Type=1))";
            buyer += " AND StoreDraftItems.StoreDraftID IN (SELECT ID FROM StoreDraft WHERE BuyerID IN ( SELECT OtherID FROM GroupingMember WHERE GroupingID = " + GroupingID + " AND Type=1) )";
            retrn += " AND ReturnItem.ReturnID IN (SELECT ID FROM Returns WHERE BuyerID IN ( SELECT OtherID FROM GroupingMember WHERE GroupingID = " + GroupingID + " AND Type=1))";
        }
        int serviceTypeID = Helper.GetInt(mServiceTypes.SelectedValue, Null.NullInteger);
        if (serviceTypeID != Null.NullInteger)
            service = " AND Service.ServiceTypeID = " + serviceTypeID;
        int GroupingID1 = Helper.GetInt(mSZGroup.SelectedValue, Null.NullInteger);
        if (GroupingID1 != Null.NullInteger)
            service += " AND Service.ID IN(SELECT OtherID FROM GroupingMember WHERE GroupingID=" + GroupingID1 + " AND Type=0)";

        string query = string.Format(@"-- 0:Transfer;2:Purchase;4:Return;5:Refuse;6:StoreDraft;10:Opening

-- Purchase
SELECT StoreServiceDefView.ServicePriceID, StoreServiceDefView.ServiceName, StoreServiceDefView.StoreTitle, StoreServicesView.StoreID, Cardex.Income
, Cardex.Outcome, Cardex.Price, Cardex.Type, Cardex.Date, PurchaseItems.Discount
, StoreServicesView.ID, Cardex.ID CardexID, Cardex.No, Cardex.OtherDate, Cardex.OtherID 
, [dbo].[FindMasterID](Cardex.OtherID, Cardex.Type, Cardex.No) AS OtherMasterID 
FROM 
Cardex INNER JOIN StoreServicesView ON Cardex.StoreServiceID = StoreServicesView.ID
INNER JOIN StoreServiceDefView ON StoreServiceDefView.ID = StoreServicesView.StoreServiceDefID
INNER JOIN ServicePrice ON StoreServiceDefView.ServicePriceID=ServicePrice.ID
INNER JOIN Service ON ServicePrice.ServiceID = Service.ID
LEFT JOIN PurchaseItems ON PurchaseItems.ID = Cardex.OtherID
WHERE Cardex.Type=2 AND StoreServiceDefView.ServiceName LIKE N'%{0}%' 
AND (Cardex.Date IS NULL OR Cardex.Date >= {1}) 
AND (Cardex.Date IS NULL OR Cardex.Date <= {2})
-- ignore deleted rows
AND NOT OtherID IN (SELECT OtherID FROM Cardex WHERE Cardex.Type=2  AND Income = 0 AND Outcome > 0)
_SERVICE_
_SELLER_

UNION

-- StoreDraft
SELECT StoreServiceDefView.ServicePriceID, StoreServiceDefView.ServiceName, StoreServiceDefView.StoreTitle, StoreServicesView.StoreID, Cardex.Income
, Cardex.Outcome, Cardex.Price, Cardex.Type, Cardex.Date, StoreDraftItems.Discount
, StoreServicesView.ID, Cardex.ID CardexID, Cardex.No, Cardex.OtherDate, Cardex.OtherID 
, [dbo].[FindMasterID](Cardex.OtherID, Cardex.Type, Cardex.No) AS OtherMasterID 
FROM 
Cardex INNER JOIN StoreServicesView ON Cardex.StoreServiceID = StoreServicesView.ID
INNER JOIN StoreServiceDefView ON StoreServiceDefView.ID = StoreServicesView.StoreServiceDefID
INNER JOIN ServicePrice ON StoreServiceDefView.ServicePriceID=ServicePrice.ID
INNER JOIN Service ON ServicePrice.ServiceID = Service.ID
LEFT JOIN StoreDraftItems ON StoreDraftItems.ID = Cardex.OtherID
WHERE Cardex.Type=6 AND StoreServiceDefView.ServiceName LIKE N'%{0}%' 
AND (Cardex.Date IS NULL OR Cardex.Date >= {1}) 
AND (Cardex.Date IS NULL OR Cardex.Date <= {2})
-- ignore deleted rows
AND NOT OtherID IN (SELECT OtherID FROM Cardex WHERE Cardex.Type=6  AND Outcome = 0 AND Income > 0)
_SERVICE_
_BUYER_

UNION 

SELECT StoreServiceDefView.ServicePriceID, StoreServiceDefView.ServiceName, StoreServiceDefView.StoreTitle, StoreServicesView.StoreID, Cardex.Income
, Cardex.Outcome, Cardex.Price, Cardex.Type, Cardex.Date, 0 Discount
, StoreServicesView.ID, Cardex.ID CardexID, Cardex.No, Cardex.OtherDate, Cardex.OtherID
, [dbo].[FindMasterID](Cardex.OtherID, Cardex.Type, Cardex.No) AS OtherMasterID 
FROM 
Cardex INNER JOIN StoreServicesView ON Cardex.StoreServiceID = StoreServicesView.ID
INNER JOIN StoreServiceDefView ON StoreServiceDefView.ID = StoreServicesView.StoreServiceDefID
INNER JOIN ServicePrice ON StoreServiceDefView.ServicePriceID=ServicePrice.ID
INNER JOIN Service ON ServicePrice.ServiceID = Service.ID
LEFT JOIN ReturnItem ON ReturnItem.ID = Cardex.OtherID
WHERE Cardex.Type = 4 AND StoreServiceDefView.ServiceName LIKE N'%{0}%' 
AND (Cardex.Date IS NULL OR Cardex.Date >= {1}) 
AND (Cardex.Date IS NULL OR Cardex.Date <= {2})
-- ignore deleted rows
AND NOT OtherID IN (SELECT OtherID FROM Cardex WHERE Cardex.Type=4  AND Outcome > 0 AND Income = 0)
_SERVICE_
_RETRN_


", what, Helper.QoutedDateTime(start), Helper.QoutedDateTime(end)).Replace("_SELLER_", seller).Replace("_BUYER_", buyer)
 .Replace("_RETRN_", retrn).Replace("_SERVICE_", service);
        if (!string.IsNullOrEmpty(SortCol))
            query += " ORDER BY " + SortCol + " " + SortOrder;
        else
            query += " ORDER BY ServicePriceID, Cardex.Date, CardexID";
        IDataReader items = DbProxy.Instance.ExecuteReader(query);

        try
        {
            mGrid.DataSource = items;
            mGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Close();
                items.Dispose();
            }
        }
    }
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.Common.DbDataRecord record = e.Row.DataItem as System.Data.Common.DbDataRecord;
            DateTime dt = Helper.GetDateTime(record["Date"], Helper.NullDate);
            if (dt != Helper.NullDate)
            {
                e.Row.Cells[2].Text = Helper.FormatDate(Helper.GetDateTime(dt, Helper.NullDate), "<a title='yyyy/MM/dd-HH:mm:ss'>yyyy/MM/dd</a>");
                //e.Row.Cells[2].Text = Helper.ToFaFormatFull(Helper.GetDateTime(dt, Helper.NullDate));
            }
            int id = Helper.GetInt(record["ServicePriceID"], -1);
            if (lastID != id)
            {
                rowStyle = rowStyle == "GridAltRow" ? "GridRow" : "GridAltRow";
                f = g = numPurchases = numStoreDrafts = m = profit = 0;
                lastID = id;
            }
            else
            {
                if (e.Row.RowIndex > 0)
                    mGrid.Rows[e.Row.RowIndex - 1].Cells[9].Text = "";
            }
            //-- 0:Transfer;2:Purchase;4:Return;5:Refuse;6:StoreDraft;10:Opening
            int type = Helper.GetInt(record["Type"], 0);
            int otherMasterID = Helper.GetInt(record["OtherMasterID"], 0);
            #region display Type
            switch (type)
            {
                case 0:
                    e.Row.Cells[3].Text = LangProvider.Instance["Transition"];
                    e.Row.Cells[4].Text = string.Format("<a href='../../list.aspx?table=Transfer&id={0}'>{1}</a>", otherMasterID, record["No"]);
                    break;
                case 2:
                    e.Row.Cells[3].Text = LangProvider.Instance["Buy"];
                    e.Row.Cells[4].Text = string.Format("<a href='../../list.aspx?table=Purchase&id={0}'>{1}</a>", otherMasterID, record["No"]);
                    break;
                case 4:
                    e.Row.Cells[3].Text = LangProvider.Instance["ReturnInvoice"];
                    e.Row.Cells[4].Text = string.Format("<a href='../../list.aspx?table=returns&id={0}'>{1}</a>", otherMasterID, record["No"]);
                    break;
                case 5:
                    e.Row.Cells[3].Text = LangProvider.Instance["RefuseInvoice"];
                    e.Row.Cells[4].Text = string.Format("<a href='../../list.aspx?table=refuse&id={0}'>{1}</a>", otherMasterID, record["No"]);
                    break;
                case 6:
                    e.Row.Cells[3].Text = LangProvider.Instance["Sell"];
                    e.Row.Cells[4].Text = string.Format("<a href='../../list.aspx?table=StoreDraft&id={0}'>{1}</a>", otherMasterID, record["No"]);
                    break;
                case 10:
                    e.Row.Cells[3].Text = LangProvider.Instance["Opening"];
                    break;
            }
            #endregion

            decimal income = Helper.GetDecimal(record["Income"], 0), outcome = Helper.GetDecimal(record["Outcome"], 0);
            decimal price = SiteSettings.ToMonetary(Helper.GetDecimal(record["Price"], 0), dt);
            decimal discount = SiteSettings.ToMonetary(Helper.GetDecimal(record["Discount"], 0), dt);

            if (discount < 0)
            {
                e.Row.Cells[7].Text = (discount * -1).ToString("N4", Helper.NumberCulture).Replace(".0000", "") + "%";
                discount = (income + outcome) * price * discount / -100;
            }
            else
                e.Row.Cells[7].Text = discount.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            if (type == 2) // purchase
            {
                numPurchases++;
                // قیمت واقعی خرید یک قلم در یک سطر خرید
                f += ((income * price) - discount) / (income > 0 ? income : 1);
            }
            else if (type == 4) // return
            {
                decimal b = numPurchases > 0 ? f / numPurchases : f;
                decimal p = numStoreDrafts > 0 ? g / numStoreDrafts : g;
                //m += income;
                profit -= (income * (p - b));
            }
            else if (type == 6) // storedraft
            {
                numStoreDrafts++;
                // قیمت واقعی فروش یک قلم
                decimal s = 0;
                if (outcome != 0)
                    s =  ((outcome * price) - discount) / outcome;
                g += s;
                decimal b = numPurchases > 0 ? f / numPurchases : f;
                profit += (outcome * s) - ((outcome * b));// + (m * (s - b)));
            }


            e.Row.Cells[6].Text = price.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.Cells[8].Text = ((income + outcome) * price - discount).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            if (outcome > 0)
                e.Row.Cells[6].CssClass = e.Row.Cells[6].CssClass = "neg";

            //if (!string.IsNullOrEmpty(Helper.GetString()))
            e.Row.CssClass = rowStyle;
            e.Row.Cells[9].Text = (profit).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            //Helper.SignifyNegative(e.Row.Cells[9], _remain);

            //((Label)e.Row.FindControl("owe")).Text = _owe.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            //((Label)e.Row.FindControl("due")).Text = _due.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            //((Label)e.Row.FindControl("remaining")).Text = _remain.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }
    }
    protected void mGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        BindGrid();
    }

    protected string SortCol
    {
        get
        {
            return Helper.GetString(ViewState["SortCol"], null);
        }
        set
        {
            ViewState["SortCol"] = value;
        }
    }
    protected string SortOrder
    {
        get
        {
            return Helper.GetString(ViewState["SortOrder"], "asc");
        }
        set
        {
            ViewState["SortOrder"] = value;
        }
    }
    protected void doShow_Click(object sender, EventArgs e)
    {
        doExcel.Enabled = true;
        BindGrid();
        voucherDetailPanel.Visible = true;

        //mSumDue.Text = sumDue.ToString("N0");
        //mSumOwe.Text = sumOwe.ToString("N0");
        //mSumUnit.Text = sumUnit.ToString("N0");
    }
    protected void doExcel_Click(object sender, EventArgs e)
    {
        Exporter.ExportExcel(Response, mGridTitle.Text, mGrid);
    }
}