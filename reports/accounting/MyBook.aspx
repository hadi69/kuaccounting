﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MyBook.aspx.cs" Inherits="reports_accounting_MyBook" %>

<%@ Register Src="../../controls/ReportSetting.ascx" TagName="ReportSetting" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>
    <div class="well padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label4" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.Based</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:DropDownList runat="server" ID="ddlBy" CssClass="chosen-select chosen-rtl" AutoPostBack="True">
                            <asp:ListItem Text="Lang.VoucherFactor" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Lang.TotalBills" Value="1"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <asp:Panel ID="panelPersons" CssClass="col-xs-12 col-sm-3 form-group-container" runat="server">
                <div class="form-group">
                    <asp:Label ID="Label26" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Person</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mPersons" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </asp:Panel>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label1" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromDate" runat="server" CssClass="T2 dateSelect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="mFromDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToDate" runat="server" CssClass="T2 dateSelect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="mToDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>

            <asp:Panel runat="server" ID="normalFilter">
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 form-group-container">
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-4">&nbsp;</div>
                        <div class="col-xs-12 col-sm-8 form-col-without-padding">
                            <asp:CheckBox runat="server" ID="mInitRemain" Text="Lang.InitRemain" CssClass="mInitRemain" Checked="true" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 form-group-container">
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-4">&nbsp;</div>
                        <div class="col-xs-12 col-sm-8 form-col-without-padding">
                            <asp:CheckBox runat="server" ID="mShowRemain" Text="Lang.Tahator" CssClass="mShowRemain" Checked="true" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 form-group-container">
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-4">&nbsp;</div>
                        <div class="col-xs-12 col-sm-8 form-col-without-padding">
                            <asp:CheckBox runat="server" ID="mShowPaperBook" Text="Lang.TopicLevelPaper" CssClass="mShowPaperBook" Checked="false" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-4">&nbsp;</div>
                        <div class="col-xs-12 col-sm-8 form-col-without-padding">
                            <asp:CheckBox runat="server" ID="mShowFactors" Text="Lang.ShowFactorsDetail" CssClass="mShowFactors" />
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <div class="col-xs-12 form-col-without-padding">
                        <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-sm btn-primary padding-15-right padding-15-left doShow" OnClick="doShow_Click" />
                        <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-sm btn-primary padding-15-right padding-15-left" OnClick="doExcel_Click" />
                        <input type="button" id="print" class="btn btn-sm btn-primary padding-15-right padding-15-left" value="" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <asp:Label runat="server" ID="msg" CssClass="label-30 alert alert-danger block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
    </div>
    <asp:Panel ID="normalPanel" runat="server">
        <JP:MiniHeader runat="server" Text="Lang.MyBook" ID="mGridTitle" />

        <asp:Panel ID="voucherDetailPanel" runat="server" CssClass="row">
            <div class="col-xs-12">
                <asp:GridView ID="mGrid" AutoGenerateColumns="False" GridWidth="95%"
                    Width="100%" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra" Scrolling="Vertical" runat="server"
                    ShowFooter="true" BorderStyle="None" OnRowDataBound="mGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    HeaderStyle-CssClass="tr" AllowSorting="true" OnSorting="mGrid_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="TreePathName" HeaderText="Lang.Topic" SortExpression="TreePathName" />
                        <asp:BoundField DataField="TopicCode" HeaderText="Lang.TopicCode" ItemStyle-CssClass="tl"  SortExpression="TopicCode"/>
                        <asp:TemplateField HeaderText="Lang.Comments" SortExpression="Comments">
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%#Eval("Comments") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="Label5" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Liability" SortExpression="Owe">
                            <ItemTemplate>
                                <asp:Label ID="owe" runat="server" Text='<%#Eval("Owe") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="oweTotal" runat="server">Lang.Sum</asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Credit" SortExpression="Due">
                            <ItemTemplate>
                                <asp:Label ID="due" runat="server" Text='<%#Eval("Due") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="dueTotal" runat="server">Lang.Sum</asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Remained">
                            <ItemTemplate>
                                <asp:Label ID="remaining" runat="server" Text='<%#Eval("Due") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="remainingTotal" runat="server">Lang.Sum</asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Diagnosis" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="diagnosis" runat="server" Text='<%#Eval("Due") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="finalDiagnosis" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="TreePathName" HeaderText="Lang.TopicLevel" />
                        <asp:TemplateField HeaderText="Lang.DateDocument" SortExpression="Date">
                            <ItemTemplate>
                                <asp:Label ID="lblGridDate" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="No" HeaderText="Lang.DocumentNumber" ItemStyle-HorizontalAlign="Center" SortExpression="No" />
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </asp:Panel>

        <JP:MiniHeader runat="server" Text="Lang.ChecksView" ID="checkGridTitle" />

        <asp:Panel runat="server" ID="checksPanel" CssClass="row">
            <div class="col-xs-12">
                <asp:GridView ID="checkGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="checkGrid_RowDataBound" GridLines="Both" EmptyDataText="">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.DueDate">
                            <ItemTemplate>
                                <asp:Label ID="lblCheckDueDate" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Serial" HeaderText="Lang.No" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="Amount" HeaderText="Lang.AmountPrice" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="Type" HeaderText="Lang.Type" />
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </asp:Panel>

        <div class="hidden">
            <JP:MiniHeader runat="server" Text="Lang.TopicAndPerson" ID="personGridTitle" />
            <asp:Panel runat="server" ID="personPanel" CssClass="row">
                <div class="col-xs-12">
                    <asp:GridView ID="personGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                        Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="personGrid_RowDataBound" GridLines="Both" EmptyDataText="">
                        <Columns>
                            <asp:TemplateField HeaderText="Lang.Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblPersonDate" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="No" HeaderText="Lang.DocumentNumber" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="TreePathName" HeaderText="Lang.Topic" />
                            <asp:BoundField DataField="TopicCode" HeaderText="Lang.TopicCode" ItemStyle-CssClass="tl" />
                            <asp:BoundField DataField="PersonnelTreePathName" HeaderText="Lang.Behalf" />
                            <asp:BoundField DataField="PersonnelName" HeaderText="Lang.Person" />
                            <asp:TemplateField HeaderText="Lang.Comments">
                                <ItemTemplate>
                                    <asp:Label ID="Label7" runat="server" Text='<%#Eval("Comments") %>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="Label8" runat="server" Text="Lang.Sum" />
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Lang.Liability">
                                <ItemTemplate>
                                    <asp:Label ID="owe" runat="server" Text='<%#Eval("Owe") %>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="oweTotal" runat="server" Text="Lang.Sum" />
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Lang.Credit">
                                <ItemTemplate>
                                    <asp:Label ID="due" runat="server" Text='<%#Eval("Due") %>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="dueTotal" runat="server" Text="Lang.Sum" />
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GridHeader" />
                        <RowStyle CssClass="GridRow" />
                        <SelectedRowStyle CssClass="GridRowSelected" />
                    </asp:GridView>
                </div>
            </asp:Panel>
        </div>

        <asp:Panel runat="server" ID="factorsPanel" Visible="false">
            <JP:MiniHeader runat="server" Text="Lang.FactorsDetail" ID="mFactorsTitle" />
            <div class="row">
                <div class="col-xs-12">
                    <asp:GridView ID="mFactors" AutoGenerateColumns="False" runat="server" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                        BorderStyle="None" OnRowDataBound="mFactors_RowDataBound" GridLines="Both" EmptyDataText="">
                        <Columns>
                            <asp:TemplateField HeaderText="Lang.ColRowNr">
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                                <ItemStyle CssClass="row-cell"></ItemStyle>
                                <HeaderStyle CssClass="row-cell"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Date" HeaderText="Lang.Date" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="Mode" HeaderText="Lang.Type" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="NOSerial" HeaderText="Lang.No" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="StoreServiceName" HeaderText="Lang.CommodityName" />
                            <asp:BoundField DataField="UnitName" HeaderText="Lang.Units" />
                            <asp:BoundField DataField="Amount" HeaderText="Lang.NumOrValue" />
                            <asp:BoundField DataField="Price" HeaderText="Lang.Price" />
                            <asp:BoundField DataField="Discount" HeaderText="Lang.Discount" />
                            <asp:BoundField DataField="Tax" HeaderText="Lang.Tax" />
                        </Columns>
                        <HeaderStyle CssClass="GridHeader" />
                        <RowStyle CssClass="GridRow" />
                        <AlternatingRowStyle CssClass="GridAltRow" />
                        <SelectedRowStyle CssClass="GridRowSelected" />
                    </asp:GridView>
                </div>
            </div>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="totalBillsPanel" runat="server" Visible="false">
        <JP:MiniHeader ID="jpMiniHeaderTotalBills" runat="server" Text="Lang.TotalBills" />
        <asp:GridView ID="mTotalBillsGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
            Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mTotalBillsGrid_RowDataBound" GridLines="Both" EmptyDataText="">
            <Columns>
                <asp:TemplateField HeaderText="Lang.ColRowNr">
                    <ItemTemplate>
                        <%# Container.DataItemIndex + 1 %>
                    </ItemTemplate>
                    <ItemStyle CssClass="row-cell"></ItemStyle>
                    <HeaderStyle CssClass="row-cell"></HeaderStyle>
                </asp:TemplateField>
                <asp:BoundField DataField="Date" HeaderText="Lang.Date" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="No" HeaderText="Lang.No" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="Comments" HeaderText="Lang.Comments" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="Num" HeaderText="Lang.Num" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="Worth" HeaderText="Lang.Worth" ItemStyle-HorizontalAlign="Center" />
                <asp:TemplateField HeaderText="Lang.Liability">
                    <ItemTemplate>
                        <asp:Label ID="owe" runat="server" Text='<%#Eval("Owe") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="oweTotal" runat="server" Text="" />
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lang.Credit">
                    <ItemTemplate>
                        <asp:Label ID="due" runat="server" Text='<%#Eval("Due") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="dueTotal" runat="server" Text="" />
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lang.Remained" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="remaining" runat="server" Text='<%#Eval("Due") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="remainingTotal" runat="server" Text="" Font-Size="Small" /><br />
                        <br />
                        <asp:Label ID="ChecksNotPassed" runat="server" Text="" Font-Size="Small" />
                    </FooterTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle CssClass="GridHeader" />
            <RowStyle CssClass="GridRow" />
            <SelectedRowStyle CssClass="GridRowSelected" />
        </asp:GridView>
    </asp:Panel>

    <script>
        $(function () {
            $('#print').attr('value', Lang.Print);
            $('#print').click(function () {
                printTable('.ScrollPanelCartable', '../../', Lang.PaperBook);
            });
            $('.GridRow,.GridAltRow').click(function () {
                $(this).toggleClass('selected');
            });
            dateSelect('.dateSelect');
            $("#bar").prepend(breadcrumb({
                title: Lang.MyBook,
                parent: {
                    title: Lang.Reports,
                    url: 'menu/reports.aspx'
                }
            }));

            // for factors
            $('a.no').click(function () {
                var url = $(this).attr('href');
                var otherID = $(this).attr('otherid');
                var mode = $(this).attr('mode');
                var table, title;
                if (mode == 4) {
                    table = 'Purchase';
                    title = lang.SalesPurchase;;
                } else if (mode == 7) {
                    table = 'StoreDraft';
                    title = lang.SalesInvoice;
                } else if (mode == 6) {
                    table = 'Returns';
                    title = Lang.ReturnInvoice;
                } else if (mode == 5) {
                    table = 'Refuse';
                    title = Lang.RefuseInvoice;
                }
                showMenu({
                    parent: this, width: '100px',
                    cmds: [[Lang.Document, 'Voucher']
                        , [title, 'Table']
                        , [Lang.Cancel, 'Cancel']],
                    callback: function (cmd) {
                        if (cmd == 'Voucher')
                            document.location = url;
                        else if (cmd == 'Table')
                            document.location = "../../list.aspx?table=" + table + "&menu=accountinga&id=" + otherID;
                    }
                });
                return false;
            });
            setClassReports();
        });
    </script>
</asp:Content>


