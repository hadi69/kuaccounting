﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PersonBook.aspx.cs" Inherits="reports_accounting_PersonBook" %>

<%@ Register Src="../../controls/ReportSetting.ascx" TagName="ReportSetting" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>
    <%--<uc1:ReportSetting ID="mReportSetting" runat="server" />--%>
    <asp:Panel ID="panelDBs" runat="server" CssClass="well callout padding-5">
        <div class="row">
            <div class="col-xs-12">
                <h5 class="dashed-bottom padding-5-bottom yekan-family">
                    <asp:Label runat="server">Lang.DataBase</asp:Label>
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <asp:CheckBoxList runat="Server" ID="mDBList" RepeatLayout="Flow" RepeatDirection="Horizontal" />
            </div>
        </div>
    </asp:Panel>
    <div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label4" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.Based</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding" >
                        <asp:DropDownList runat="server" ID="ddlBy" CssClass="chosen-select chosen-rtl" AutoPostBack="True">
                            <asp:ListItem Text="Lang.Person" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Lang.ListOfOwe" Value="3"></asp:ListItem>
                            <asp:ListItem Text="Lang.ListOfDue" Value="4"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" AssociatedControlID="mFromDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="mFromDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label3" runat="server" AssociatedControlID="mToDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="mToDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
             <asp:Panel ID="panelShowFactors" class="col-xs-12 col-sm-6 col-md-4 form-group-container" runat="server">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4">&nbsp;</div>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox runat="server" ID="mShowFactors" Text="Lang.ShowFactorsDetail" CssClass="mShowFactors" />
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="panelShowInProgressVouchers" runat="server" CssClass="col-xs-12 col-sm-6 col-md-4 form-group-container">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox ID="mShowInProgressVouchers" runat="server" Text="Lang.ShowInProgressVouchers" CssClass=""></asp:CheckBox>
                    </div>
                </div>
            </asp:Panel>
        </div>
        <div class="row">
            <asp:Panel ID="panelPersons" CssClass="col-xs-12 col-sm-8 form-group-container" runat="server">
                <div class="form-group">
                    <asp:Label ID="Label26" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Person</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mPersons" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="panelProjects" CssClass="col-xs-12 col-sm-6 form-group-container" runat="server">
                <div class="form-group">
                    <asp:Label ID="Label1" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Project</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mProjects" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="panelEquipments" CssClass="col-xs-12 col-sm-6 form-group-container" runat="server">
                <div class="form-group">
                    <asp:Label ID="Label7" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Equipment</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mEquipments" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="panelCostCenters" CssClass="col-xs-12 col-sm-6 form-group-container" runat="server">
                <div class="form-group">
                    <asp:Label ID="Label10" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.CostCenter</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mCostCenters" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="panelFromRemain" class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group-container" runat="server">
                <div class="form-group">
                    <asp:Label ID="Label5" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.RemainFrom</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromRemain" runat="server" CssClass="monetary form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="panelToRemain" class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group-container" runat="server">
                <div class="form-group">
                    <asp:Label ID="Label6" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.To</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToRemain" runat="server" CssClass="monetary form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </asp:Panel>
             <asp:Panel ID="panelPersonGroups" CssClass="col-xs-6 col-sm-3 col-md-4 col-lg-4 form-group-container" runat="server">
                <div class="form-group">
                    <asp:Label ID="Label15" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-6">Lang.PersonnelGroup</asp:Label>
                    <div class="col-xs-12 col-sm-6 form-col-without-padding">
                        <asp:DropDownList ID="mPersonGroups" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="panelShowCoworkerPerson" class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container" Visible="False" runat="server">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4">&nbsp;</div>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox runat="server" ID="mShowCoworkerPerson" Text="Lang.ShowCoworkerPerson" CssClass="mShowCoworkerPerson" />
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="panelShowInitRemain" class="col-xs-12 col-sm-6 col-md-4 col-lg-3 col-lg-offset-1 form-group-container display-none" runat="server">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox ID="mShowInitRemain" runat="server" Text="Lang.InitRemain" CssClass="mShowInitRemain"></asp:CheckBox>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="panelShowDelays" class="col-xs-12 col-sm-6 col-md-4 col-lg-3 col-lg-offset-1 form-group-container" runat="server">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox ID="mShowDelays" runat="server" Text="Lang.DelayInPayment" CssClass="mShowDelays"></asp:CheckBox>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="panelShowVouchers" class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container" runat="server">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4">&nbsp;</div>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox runat="server" ID="mShowVouchers" Text="Lang.ShowDocumetsDetail" CssClass="mShowVouchers" />
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="panelShowJustProjectNeed" class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container" runat="server">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4">&nbsp;</div>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox runat="server" ID="mShowJustProjectNeed" Text="Lang.JustProjectNeed" CssClass="mShowJustProjectNeed" />
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="panelShowJustEquipmentNeed" class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container" runat="server">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4">&nbsp;</div>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox runat="server" ID="mShowJustEquipmentNeed" Text="Lang.JustEquipmentNeed" CssClass="mShowJustEquipmentNeed" />
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="panelShowJustCostCenterNeed" class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container" runat="server">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4">&nbsp;</div>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox runat="server" ID="mShowJustCostCenterNeed" Text="Lang.JustCostCenterNeed" CssClass="mShowJustCostCenterNeed" />
                    </div>
                </div>
            </asp:Panel>
        </div>
        <div class="row margin-5-top">
            <div class="col-md-8 col-lg-6 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container form-col-without-padding">
                <div class="col-xs-12 col-sm-3">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
                </div>
                <div class="col-xs-12 col-sm-3">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcel_Click" Enabled="false" />
                </div>
                <div class="col-xs-12 col-sm-3">
                    <input type="button" id="print" class="btn btn-sm btn-dark-blue btn-block" value="" />
                </div>
                <div class="col-xs-12 col-sm-3">
                    <input type="button" runat="server" id="CheckPrint" class="btn btn-sm btn-dark-blue btn-block" value="" />
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="panelDropDowns" runat="server" CssClass="well callout padding-5" Visible="False">
        <input type="hidden" runat="server" id="mQueryBuilder" class="mQueryBuilder" />
        <input type="hidden" runat="server" id="mQueryBuilderSql" class="mQueryBuilderSql" />
        <div class="hidden">
            <asp:DropDownList runat="server" ID="mPersonsDummy" CssClass="mPersonsDummy"></asp:DropDownList>
            <asp:DropDownList runat="server" ID="mCatsDummy" CssClass="mCatsDummy"></asp:DropDownList>
            <asp:DropDownList runat="server" ID="mWardDummy" CssClass="mWardDummy"></asp:DropDownList>
            <asp:DropDownList runat="server" ID="mEqDummy" CssClass="mEqDummy"></asp:DropDownList>
            <asp:DropDownList runat="server" ID="mCCenterDummy" CssClass="mCCenterDummy"></asp:DropDownList>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <h5 class="dashed-bottom padding-5-bottom yekan-family">
                    <asp:Label runat="server">Lang.PersonsGroupLable</asp:Label>
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="ltr builder">
                    <div id="builder"></div>
                </div>
            </div>
        </div>
    </asp:Panel>
    
    <div class="col-xs-12">
        <asp:Label runat="server" ID="msg" CssClass="label-30 alert alert-danger block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
    </div>
    <asp:Panel ID="voucherDetailPanel" runat="server" Visible="false">
        <JP:MiniHeader runat="server" Text="Lang.PersonBook" ID="mGridTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="mGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid personbalancegrid table table-bordered table-condensed"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true" OnSorting="mGrid_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Date">
                            <ItemTemplate>
                                <asp:Label ID="lblGridDate" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="No" HeaderText="Lang.DocumentNumber" ItemStyle-HorizontalAlign="Center" SortExpression="No" />
                        <asp:BoundField DataField="TopicCode" HeaderText="Lang.TopicCode" ItemStyle-CssClass="tl" SortExpression="TopicCode" />
                        <asp:BoundField DataField="TreePathName" HeaderText="Lang.Topic" SortExpression="TreePathName" />
                        <asp:BoundField DataField="PersonCode" HeaderText="Lang.PersonCode" ItemStyle-HorizontalAlign="Center" SortExpression="PersonCode" />
                        <asp:BoundField DataField="PersonnelName" HeaderText="Lang.Person" ItemStyle-HorizontalAlign="Center" SortExpression="PersonnelName" />
                        <asp:BoundField DataField="FIdentify" HeaderText="Lang.FirstGroup" ItemStyle-HorizontalAlign="Center" SortExpression="FIdentify" />
                        <asp:BoundField DataField="SIdentify" HeaderText="Lang.SecondGroup" ItemStyle-HorizontalAlign="Center" SortExpression="SIdentify" />
                        <asp:BoundField DataField="FactorNo" HeaderText="Lang.FactorNo" ItemStyle-HorizontalAlign="Center"/>
                        <asp:BoundField DataField="Comments" HeaderText="Lang.Comments" ItemStyle-HorizontalAlign="Center" SortExpression="Comments" />
                        <asp:BoundField DataField="HomeTelNum" HeaderText="Lang.HomeTelNum" ItemStyle-HorizontalAlign="Center" SortExpression="HomeTelNum" />
                        <asp:BoundField DataField="CellNumber1" HeaderText="Lang.CellNumber" ItemStyle-HorizontalAlign="Center" SortExpression="CellNumber1" />
                        <asp:BoundField DataField="HomeAddress" HeaderText="Lang.Address" ItemStyle-HorizontalAlign="Center" ControlStyle-Width="500px" SortExpression="HomeAddress" />

                        <asp:TemplateField HeaderText="Lang.Liability" SortExpression="Owe">
                            <ItemTemplate>
                                <asp:Label ID="owe" runat="server" Text='<%#Eval("Owe") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="oweTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Credit" SortExpression="Due">
                            <ItemTemplate>
                                <asp:Label ID="due" runat="server" Text='<%#Eval("Due") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="dueTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Remained" SortExpression="Remained">
                            <ItemTemplate>
                                <asp:Label ID="remaining" runat="server" Text='<%#Eval("Due") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="remainingTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Diagnosis" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="diagnosis" runat="server" Text='<%#Eval("Due") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="finalDiagnosis" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Paid" HeaderText="Lang.Status" ItemStyle-HorizontalAlign="Center" ControlStyle-Width="500px" SortExpression="Paid" />
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="projectPanel" runat="server" Visible="false">
        <JP:MiniHeader runat="server" Text="Lang.ProjectBook" ID="mProjectGridTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="mProjectGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mProjectGrid_RowDataBound" GridLines="Both" EmptyDataText="">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Date">
                            <ItemTemplate>
                                <asp:Label ID="lblProjectDate" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="No" HeaderText="Lang.DocumentNumber" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="PersonnelName" HeaderText="Lang.Person" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="WardName" HeaderText="Lang.Project" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="TreePathName" HeaderText="Lang.Topic" />
                        <asp:BoundField DataField="TopicCode" HeaderText="Lang.TopicCode" ItemStyle-CssClass="tl" />
                        <asp:TemplateField HeaderText="Lang.Comments">
                            <ItemTemplate>
                                <asp:Label ID="Label8" runat="server" Text='<%#Eval("Comments") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="sumLable" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Liability">
                            <ItemTemplate>
                                <asp:Label ID="owe" runat="server" Text='<%#Eval("Owe") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="oweTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Credit">
                            <ItemTemplate>
                                <asp:Label ID="due" runat="server" Text='<%#Eval("Due") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="dueTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Remained" SortExpression="Remained">
                            <ItemTemplate>
                                <asp:Label ID="remaining" runat="server" Text='<%#Eval("Due") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="remainingTotal" runat="server" Text="" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Diagnosis" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="diagnosis" runat="server" Text='<%#Eval("Due") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="finalDiagnosis" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel runat="server" ID="checksPanel" Visible="false">
        <JP:MiniHeader runat="server" Text="Lang.ChecksView" ID="checkGridTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="checkGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable checkgrid Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="checkGrid_RowDataBound" GridLines="Both" EmptyDataText="">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DueDate" HeaderText="Lang.DueDate" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="Serial" HeaderText="Lang.No" ItemStyle-HorizontalAlign="Center" />
                        <asp:TemplateField HeaderText="Lang.AmountPrice">
                            <ItemTemplate>
                                <asp:Label ID="Amount" runat="server" ItemStyle-HorizontalAlign="Center" Text='<%#Eval("Amount") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="SumAmount" runat="server" ItemStyle-HorizontalAlign="Center" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Type" HeaderText="Lang.Type" />
                        <asp:BoundField DataField="Status" HeaderText="Lang.Status" />
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <AlternatingRowStyle CssClass="GridAltRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel runat="server" ID="delayPanel" Visible="false">
        <JP:MiniHeader runat="server" Text="Lang.DelayInPayment" ID="delayGridTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="delayGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="delayGrid_RowDataBound" GridLines="Both" EmptyDataText="">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Type" HeaderText="Lang.Type" />
                        <asp:BoundField DataField="PersonnelName" HeaderText="Lang.PersonnelName" />
                        <asp:BoundField DataField="No" HeaderText="Lang.No" />
                        <asp:TemplateField HeaderText="Lang.AmountPrice">
                            <ItemTemplate>
                                <asp:Label ID="Amount" runat="server" Text='<%#Eval("Amount") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="SumAmount" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="PayDate" HeaderText="Lang.PayDate" />
                        <asp:BoundField DataField="Status" HeaderText="Lang.Status" />
                        <asp:BoundField DataField="Delay" HeaderText="Lang.DelayDays" />
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <AlternatingRowStyle CssClass="GridAltRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="coworkerPanel" runat="server" Visible="false">
        <JP:MiniHeader runat="server" Text="Lang.CoworkerPerson" ID="mCoworkerPersonTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="mCoworkerPerson" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mCoworkerPerson_RowDataBound" GridLines="Both" EmptyDataText="">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.DocumentDate">
                            <ItemTemplate>
                                <asp:Label ID="lblCoworkerPersonDate" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="No" HeaderText="Lang.DocumentNumber" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="PersonnelName" HeaderText="Lang.Person" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="WardName" HeaderText="Lang.Project" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="TreePathName" HeaderText="Lang.Topic" />
                        <asp:BoundField DataField="TopicCode" HeaderText="Lang.TopicCode" ItemStyle-CssClass="tl" />
                        <asp:TemplateField HeaderText="Lang.Comments">
                            <ItemTemplate>
                                <asp:Label ID="Label8" runat="server" Text='<%#Eval("Comments") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="Label9" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Liability">
                            <ItemTemplate>
                                <asp:Label ID="owe" runat="server" Text='<%#Eval("Owe") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="oweTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Credit">
                            <ItemTemplate>
                                <asp:Label ID="due" runat="server" Text='<%#Eval("Due") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="dueTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>


    <asp:Panel runat="server" ID="personPanel" Visible="false">
        <JP:MiniHeader runat="server" Text="Lang.TopicAndPerson" ID="personGridTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="personGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="personGrid_RowDataBound" GridLines="Both" EmptyDataText="">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Date" HeaderText="Lang.Month" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="Date" HeaderText="Lang.Date" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="No" HeaderText="Lang.DocumentNumber" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="TreePathName" HeaderText="Lang.Topic" />
                        <asp:BoundField DataField="TopicCode" HeaderText="Lang.TopicCode" ItemStyle-CssClass="tl" />
                        <asp:BoundField DataField="PersonnelTreePathName" HeaderText="Lang.Behalf" />
                        <asp:BoundField DataField="PersonnelName" HeaderText="Lang.Person" />
                        <asp:TemplateField HeaderText="Lang.Comments">
                            <ItemTemplate>
                                <asp:Label ID="Label11" runat="server" Text='<%#Eval("Comments") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="Label12" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Liability">
                            <ItemTemplate>
                                <asp:Label ID="owe" runat="server" Text='<%#Eval("Owe") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="oweTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Credit">
                            <ItemTemplate>
                                <asp:Label ID="due" runat="server" Text='<%#Eval("Due") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="dueTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel runat="server" ID="vouchersPanel" Visible="false">
        <JP:MiniHeader runat="server" Text="Lang.DocumentDetail" ID="mVouchersTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="mVouchers" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mVouchers_RowDataBound" GridLines="Both" EmptyDataText="">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Date">
                            <ItemTemplate>
                                <asp:Label ID="lblVouchersDate" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="No" HeaderText="Lang.DocumentNumber" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="TopicCode" HeaderText="Lang.TopicCode" ItemStyle-CssClass="tl" />
                        <asp:BoundField DataField="TreePathName" HeaderText="Lang.Topic" />
                        <asp:BoundField DataField="FactorNo" HeaderText="Lang.FactorNo" />
                        <asp:BoundField DataField="PersonnelName" HeaderText="Lang.Person" />
                        <asp:TemplateField HeaderText="Lang.Comments">
                            <ItemTemplate>
                                <asp:Label ID="Label13" runat="server" Text='<%#Eval("Comments") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="Label14" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Liability">
                            <ItemTemplate>
                                <asp:Label ID="owe" runat="server" Text='<%#Eval("Owe") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="oweTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Credit">
                            <ItemTemplate>
                                <asp:Label ID="due" runat="server" Text='<%#Eval("Due") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="dueTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel runat="server" ID="factorsPanel" Visible="false">
        <JP:MiniHeader runat="server" Text="Lang.FactorsDetail" ID="mFactorsTitle" />
        <div class="well callout row margin-5-top">
            <div class="col-md-8 col-lg-6 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container form-col-without-padding">
                <div class="col-xs-12 col-sm-3">
                    <asp:Button ID="doExcelFactor" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcelFactor_Click" />
                </div>
                <div class="col-xs-12 col-sm-3">
                    <input type="button" id="printFactor" class="btn btn-xs btn-dark-blue btn-block" value="" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="mFactors" AutoGenerateColumns="False" runat="server" CssClass="FactorsGrid Grid table table-bordered table-condensed zebra"
                    BorderStyle="None" OnRowDataBound="mFactors_RowDataBound" ShowFooter="true" GridLines="Both" EmptyDataText="">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Date" HeaderText="Lang.Date" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="Mode" HeaderText="Lang.Type" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="NOSerial" HeaderText="Lang.No" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="StoreServiceName" HeaderText="Lang.CommodityName" />
                        <asp:BoundField DataField="UnitName" HeaderText="Lang.Units" />
                        <asp:TemplateField HeaderText="Lang.NumOrValue">
                            <ItemTemplate>
                                <asp:Label ID="NumOrValue" runat="server" Text='<%#Eval("Amount") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="SumNumOrValue" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Price">
                            <ItemTemplate>
                                <asp:Label ID="Price" runat="server" Text='<%#Eval("Price") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="SumPrice" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Discount">
                            <ItemTemplate>
                                <asp:Label ID="Discount" runat="server" Text='<%#Eval("Discount") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="SumDiscount" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Tax">
                            <ItemTemplate>
                                <asp:Label ID="Tax" runat="server" Text='<%#Eval("Tax") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="SumTax" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Awarez">
                            <ItemTemplate>
                                <asp:Label ID="SingleVat" runat="server" Text='<%#Eval("SingleVat") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="SumSingleVat" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.TotalS">
                            <ItemTemplate>
                                <asp:Label ID="Total" runat="server" Text='' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="SumTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>



    <script>
        $(function () {
            $('.sidebar').addClass('collapsed');
            function mPersonSelect() {

                if ($("#<%=mPersons.ClientID %>").val() != 0) {
                    $("#<%=panelShowInitRemain.ClientID %>").show();
                } else {
                    $("#<%=panelShowInitRemain.ClientID %>").hide();
                }
                $("#<%=mPersons.ClientID %>").change(function (e) {
                    if (this.selectedIndex != 0) {
                        $("#<%=panelShowInitRemain.ClientID %>").show();
                    } else {
                        $("#<%=mShowInitRemain.ClientID %>").attr('checked', '');
                        $("#<%=panelShowInitRemain.ClientID %>").hide();
                    }
                });
            }
            if ($("#<%=ddlBy.ClientID %>").val() == 0) {
                mPersonSelect();
            }

            $('#print').attr('value', Lang.Print);
            $("#<%=CheckPrint.ClientID %>").attr('value', Lang.CheckPrint);
            $('#print').click(function () {
                var head = Lang.PersonBalance;
                if ($('#<%=mPersons.ClientID %>').val() != '') {
                    head += '/' + Lang.Person + ': ' + $('#<%=mPersons.ClientID %> option:selected').text();
                }
                if ($("#<%=mFromDate.ClientID %>").val() != '') {
                    head += '/' + Lang.FromDate + ': ' + $("#<%=mFromDate.ClientID %>").val();
                }
                if ($("#<%=mToDate.ClientID %>").val() != '') {
                    head += '/' + Lang.ToDate + ': ' + $("#<%=mToDate.ClientID %>").val();
                }
                printTable('.personbalancegrid', '../../', head);
            });
            $('#CheckPrint').click(function () {
                var head = Lang.CheckPrint;
                if ($('#<%=mPersons.ClientID %>').val() != '') {
                    head += '/' + Lang.Person + ': ' + $('#<%=mPersons.ClientID %> option:selected').text();
                }
                if ($("#<%=mFromDate.ClientID %>").val() != '') {
                    head += '/' + Lang.FromDate + ': ' + $("#<%=mFromDate.ClientID %>").val();
                }
                if ($("#<%=mToDate.ClientID %>").val() != '') {
                    head += '/' + Lang.ToDate + ': ' + $("#<%=mToDate.ClientID %>").val();
                }
                printTable('.checkgrid', '../../', head);
            });
            $('#printFactor').attr('value', Lang.Print);
            $('#printFactor').click(function () {
                var head = Lang.FactorsDetail;
                printTable('.FactorsGrid', '../../', head);
            });
            $('.GridRow,.GridAltRow').click(function () {
                $(this).toggleClass('selected');
            });
            dateSelect('.dateselect');
            $('.monetary').keyup(function () {
                $(this).val(numberWithCommas(stripCommas($(this).val())));
            });

            $('a.no').click(function () {
                var url = $(this).attr('href');
                var otherID = $(this).attr('otherid');
                var mode = $(this).attr('mode');
                var table, title;
                if (mode == 4) {
                    table = 'Purchase';
                    title = lang.SalesPurchase;;
                } else if (mode == 7) {
                    table = 'StoreDraft';
                    title = lang.SalesInvoice;
                } else if (mode == 6) {
                    table = 'Returns';
                    title = Lang.ReturnInvoice;
                } else if (mode == 5) {
                    table = 'Refuse';
                    title = Lang.RefuseInvoice;
                }
                showMenu({
                    parent: this, width: '100px',
                    cmds: [[Lang.Document, 'Voucher']
                        , [title, 'Table']
                        , [Lang.Cancel, 'Cancel']],
                    callback: function (cmd) {
                        if (cmd == 'Voucher')
                            document.location = url;
                        else if (cmd == 'Table')
                            document.location = "../../list.aspx?table=" + table + "&menu=accountinga&id=" + otherID;
                    }
                });
                return false;
            });
            $("#bar").prepend(breadcrumb({
                title: Lang.Account,
                parent: {
                    title: Lang.Reports,
                    url: 'menu/reports.aspx'
                }
            }));
            setClassReports();
            var cats = {};
            $('.mPersonsDummy option').each(function () {
                cats[$(this).attr('value')] = $(this).text();
            });
            var pcats = {};
            $('.mCatsDummy option').each(function () {
                pcats[$(this).attr('value')] = $(this).text();
            });
            var wardCats = {};
            $('.mWardDummy option').each(function () {
                wardCats[("00000" + $(this).attr('value'))] = $(this).text();
            });
            var eqCats = {};
            $('.mEqDummy option').each(function () {
                eqCats[$(this).attr('value')] = $(this).text();
            });
            var ccCats = {};
            $('.mCCenterDummy option').each(function () {
                ccCats[$(this).attr('value')] = $(this).text();
            });
            if ($jQuery1_9('#builder').length > 0)
                $jQuery1_9('#builder').queryBuilder({
                    filters: [
                        {
                            id: 'category',
                            label: Lang.PersonsGroupLable,
                            type: 'integer',
                            input: 'select',
                            values: cats,
                            operators: ['equal', 'not_equal']
                        },
                        {
                            id: 'pcat',
                            label: Lang.PersonnelGroup,
                            type: 'integer',
                            input: 'select',
                            values: pcats,
                            operators: ['equal', 'not_equal']
                        },
                        {
                            id: 'wardcat',
                            label: Lang.Project,
                            type: 'integer',
                            input: 'select',
                            values: wardCats,
                            operators: ['equal', 'not_equal']
                        },
                        {
                            id: 'eqcat',
                            label: Lang.Equipment,
                            type: 'integer',
                            input: 'select',
                            values: eqCats,
                            operators: ['equal', 'not_equal']
                        },
                        {
                            id: 'cccat',
                            label: Lang.CostCenter,
                            type: 'integer',
                            input: 'select',
                            values: ccCats,
                            operators: ['equal', 'not_equal']
                        }
                    ]
                });
            // save rules before post back
            $('.doShow').click(function () {
                if ($jQuery1_9('#builder').length > 0) {
                    var rules = $jQuery1_9('#builder').queryBuilder('getRules');
                    var rulesStr = JSON.stringify(rules);
                    // alert(rulesStr);
                    $('.mQueryBuilder').val(rulesStr);
                    $('.mQueryBuilderSql').val($jQuery1_9('#builder').queryBuilder('getSQL', false, false).sql);
                }
            });


            // sub sums for factors
            $('.FactorsGrid .subSum').each(function () {
                // fa='{0}' fp='{1}' fd='{2}' ft='{3}' fs='{4}'
                var me = $(this);
                var row = '<tr class="SumRow factorsumrow"><td class="row-cell">&nbsp;</td><td>&nbsp;</td><td></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>';
                row += '<td>' + numberWithCommas(me.attr('fa')) + '</td>';
                row += '<td>' + numberWithCommas(me.attr('fp')) + '</td>';
                row += '<td>' + numberWithCommas(me.attr('fd')) + '</td>';
                row += '<td>' + numberWithCommas(me.attr('ft')) + '</td>';
                row += '<td>' + numberWithCommas(me.attr('fs')) + '</td>';
                row += '<td>' + numberWithCommas(me.attr('tp')) + '</td>';
                row += '</tr>';
                me.closest('tr').before(row);
            });

            //reload saved rules
            if ($jQuery1_9('#builder').length > 0) {
                $jQuery1_9('#builder').queryBuilder('reset');
                var ruleStr = $('.mQueryBuilder').val();
                if (ruleStr != '') {
                    var rule = eval("(" + ruleStr + ")");
                    $jQuery1_9('#builder').queryBuilder('setRules', rule);
                }
            }
            $jQuery1_9('#builder').on('afterCreateRuleInput.queryBuilder', function (rule) {
                makeChosen();
            });
        });
    </script>
</asp:Content>


