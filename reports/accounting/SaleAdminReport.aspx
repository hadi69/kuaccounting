﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" CodeFile="SaleAdminReport.aspx.cs" Inherits="reports_accounting_SaleAdminReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/controls/ReportSetting.ascx" TagName="ReportSetting" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>
    <uc1:ReportSetting ID="mReportSetting" runat="server" />
    <asp:Panel ID="panelDBs" runat="server" CssClass="well callout padding-5">
        <div class="row">
            <div class="col-xs-12">
                <h5 class="dashed-bottom padding-5-bottom yekan-family">
                    <asp:Label runat="server">Lang.DataBase</asp:Label>
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <asp:CheckBoxList runat="Server" ID="mDBList" RepeatLayout="Flow" RepeatDirection="Horizontal" />
            </div>
        </div>
    </asp:Panel>
    <div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" AssociatedControlID="mFromDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="mFromDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label3" runat="server" AssociatedControlID="mToDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="mToDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="CommodityLable" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Commodity</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mCommodities" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="personLable" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Buyer</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mPersons" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="visitorLabel" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Visitor</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mVisitor" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-md-3  form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label7" runat="server" CssClass="left-aligned-label control-label col-md-4">Lang.Display</asp:Label>
                    <div class="col-md-8 form-col-without-padding">
                        <asp:DropDownList runat="server" ID="DisplayType" CssClass="chosen-select chosen-rtl">
                            <asp:ListItem Text="Lang.Display1" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Lang.Display2" Value="1"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-md-3 form-group-container">
                <div class="form-group">
                    <asp:RadioButton ID="mBasedOnPayMethod" runat="server" GroupName="baseradio" Text="Lang.BasedOnPayMethod" />
                    <asp:RadioButton ID="mBasedOnSaleType" runat="server" GroupName="baseradio" Text="Lang.BasedOnSaleType" />
                </div>
            </div>
            <div class="col-md-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label5" runat="server" CssClass="left-aligned-label control-label col-md-2">Lang.Based</asp:Label>
                    <asp:RadioButton ID="BasedBuyer" CssClass="br basedbuyer" runat="server" GroupName="radio" Text="Lang.Buyer" />
                    <asp:RadioButton ID="BasedVisitor" CssClass="br basedvisitor" runat="server" GroupName="radio" Text="Lang.Visitor" />
                    <asp:RadioButton ID="BasedServiceType" CssClass="br basedservicetype" runat="server" GroupName="radio" Text="Lang.ServiceType" />
                    <asp:RadioButton ID="BasedPersonnelGroup" CssClass="br basedpersonnelgroup" runat="server" GroupName="radio" Text="Lang.PersonnelGroup" />
                    <asp:RadioButton ID="BasedServiceGroupLable" CssClass="br basedservicegrouplable" runat="server" GroupName="radio" Text="Lang.ServiceGroupLable" />
                    <asp:RadioButton ID="BasedPersonsGroupLable" CssClass="br basedpersonsgrouplable" runat="server" GroupName="radio" Text="Lang.PersonsGroupLable" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 col-md-offset-1 form-group-container form-col-without-padding">
                <div class="col-md-4">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
                </div>
                <div class="col-md-4">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcel_Click" Enabled="false" />
                </div>
                <div class="col-md-4">
                    <input type="button" id="print" class="btn btn-xs btn-dark-blue btn-block" value="" />
                </div>
            </div>
            <div class="col-md-6 form-group-container persongroupparents">
                <div class="form-group">
                    <asp:Label ID="Label1" runat="server" CssClass="left-aligned-label control-label col-md-3">Lang.MainGroup</asp:Label>
                    <div class="col-md-9 form-col-without-padding">
                        <asp:DropDownList ID="mPersonGroupParents" OnSelectedIndexChanged="init1" AutoPostBack="True" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-md-6 form-group-container servicegroupparents">
                <div class="form-group">
                    <asp:Label ID="Label4" runat="server" CssClass="left-aligned-label control-label col-md-3">Lang.MainGroup</asp:Label>
                    <div class="col-md-9 form-col-without-padding">
                        <asp:DropDownList ID="mServiceGroupParents" OnSelectedIndexChanged="init1" AutoPostBack="True" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="well callout padding-5">
        <input type="hidden" runat="server" id="mQueryBuilder" class="mQueryBuilder" />
        <input type="hidden" runat="server" id="mQueryBuilderSql" class="mQueryBuilderSql" />
        <div class="hidden">
            <asp:DropDownList runat="server" ID="mCommodityGroupings" CssClass="mCommodityGroupings"></asp:DropDownList>
            <asp:DropDownList runat="server" ID="mPersonGroupings" CssClass="mPersonGroupings"></asp:DropDownList>
            <asp:DropDownList runat="server" ID="mCommodityGroup" CssClass="mCommodityGroup"></asp:DropDownList>
            <asp:DropDownList runat="server" ID="mPersonGroup" CssClass="mPersonGroup"></asp:DropDownList>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <h5 class="dashed-bottom padding-5-bottom yekan-family">
                    <asp:Label runat="server">Lang.PersonsGroupLable</asp:Label>
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="ltr builder">
                    <div id="builder"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <asp:Label runat="server" ID="msg" CssClass="label-30 alert alert-danger block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
    </div>
    <asp:Panel ID="MinmaxGridPanel" runat="server" Visible="false">
        <JP:MiniHeader runat="server" Text="Lang.MinMaxReport" ID="MinmaxGridTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="mMinmaxGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed"
                    Scrolling="Vertical" runat="server" ShowFooter="false" BorderStyle="None" OnRowDataBound="minMaxGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true">
                    <Columns>
                        <asp:BoundField DataField="Type" HeaderText="Lang.Type" ItemStyle-CssClass="tl align-center" />
                        <asp:BoundField DataField="VisitorName" HeaderText="Lang.Visitor" ItemStyle-CssClass="tl align-center" />
                        <asp:BoundField DataField="BuyerName" HeaderText="Lang.Buyer" ItemStyle-CssClass="tl align-center" />
                        <asp:BoundField DataField="ServiceGroup" HeaderText="Lang.ServiceType" ItemStyle-CssClass="tl align-center" />
                        <asp:BoundField DataField="PersonGroup" HeaderText="Lang.PersonnelGroup" ItemStyle-CssClass="tl align-center" />
                        <asp:BoundField DataField="ServiceGrouping" HeaderText="Lang.ServiceType" ItemStyle-CssClass="tl align-center" />
                        <asp:BoundField DataField="PersonGrouping" HeaderText="Lang.PersonnelGroup" ItemStyle-CssClass="tl align-center" />
                        <asp:BoundField DataField="Code" HeaderText="Lang.Code" ItemStyle-CssClass="tl align-center" />
                        <asp:BoundField DataField="ServiceName" HeaderText="Lang.Commodity" ItemStyle-CssClass="tl align-center" />
                        <asp:BoundField DataField="UnitName" HeaderText="Lang.Units" ItemStyle-CssClass="tl align-center" />
                        <asp:BoundField DataField="Num" HeaderText="Lang.TotalAmount" ItemStyle-CssClass="tl align-center" />
                        <asp:BoundField DataField="Price" HeaderText="Lang.TotalPrice" ItemStyle-CssClass="tl align-center" />
                        <asp:BoundField DataField="ProfitInUnit" HeaderText="Lang.ProfitInUnit" ItemStyle-CssClass="tl align-center" />
                        <asp:BoundField DataField="TotalProfit" HeaderText="Lang.TotalProfit" ItemStyle-CssClass="tl align-center" />
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="gridPanel" runat="server" Visible="false">
        <JP:MiniHeader runat="server" Text="Lang.Items" ID="mGridTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="mGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true" OnSorting="mGrid_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="VisitorName" HeaderText="Lang.Visitor" ItemStyle-CssClass="tl align-center" SortExpression="VisitorName" />
                        <asp:BoundField DataField="BuyerName" HeaderText="Lang.Buyer" ItemStyle-CssClass="tl align-center" SortExpression="BuyerName" />
                        <asp:BoundField DataField="ServiceGroup" HeaderText="Lang.ServiceType" ItemStyle-CssClass="tl align-center" SortExpression="ServiceGroup" />
                        <asp:BoundField DataField="PersonGroup" HeaderText="Lang.PersonnelGroup" ItemStyle-CssClass="tl align-center" SortExpression="PersonGroup" />
                        <asp:BoundField DataField="ServiceGrouping" HeaderText="Lang.ServiceType" ItemStyle-CssClass="tl align-center" />
                        <asp:BoundField DataField="PersonGrouping" HeaderText="Lang.PersonnelGroup" ItemStyle-CssClass="tl align-center" />
                        <asp:BoundField DataField="Code" HeaderText="Lang.Code" ItemStyle-CssClass="tl align-center" SortExpression="Code" />
                        <asp:BoundField DataField="ServiceName" HeaderText="Lang.Commodity" ItemStyle-CssClass="tl align-center" SortExpression="ServiceName" />
                        <asp:BoundField DataField="UnitName" HeaderText="Lang.Units" ItemStyle-CssClass="tl align-center" SortExpression="UnitName" />
                        <asp:TemplateField HeaderText="Lang.TotalAmount" SortExpression="Num">
                            <ItemTemplate>
                                <asp:Label ID="Num" runat="server" Text='<%#Eval("Num") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalNum" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.TotalPrice" SortExpression="Price">
                            <ItemTemplate>
                                <asp:Label ID="Price" runat="server" Text='<%#Eval("Price") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalPrice" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ProfitInUnit" HeaderText="Lang.ProfitInUnit" ItemStyle-CssClass="tl align-center" SortExpression="ProfitInUnit" />
                        <asp:TemplateField HeaderText="Lang.TotalProfit" SortExpression="Tax">
                            <ItemTemplate>
                                <asp:Label ID="TotalProfit" runat="server" Text='<%#Eval("TotalProfit") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalTotalProfit" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="BaseOnSaleTypeGridPanel" runat="server" Visible="false">
        <JP:MiniHeader runat="server" Text="Lang.Items" ID="BaseOnSaleTypeGridTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="mBaseOnSaleTypeGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mBaseOnSaleTypeGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true" OnSorting="mBaseOnSaleTypeGrid_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="VisitorName" HeaderText="Lang.Visitor" ItemStyle-CssClass="tl align-center" SortExpression="VisitorName" />
                        <asp:BoundField DataField="BuyerName" HeaderText="Lang.Buyer" ItemStyle-CssClass="tl align-center" SortExpression="BuyerName" />
                        <asp:BoundField DataField="ServiceGroup" HeaderText="Lang.ServiceType" ItemStyle-CssClass="tl align-center" SortExpression="ServiceGroup" />
                        <asp:BoundField DataField="PersonGroup" HeaderText="Lang.PersonnelGroup" ItemStyle-CssClass="tl align-center" SortExpression="PersonGroup" />
                        <asp:BoundField DataField="ServiceGrouping" HeaderText="Lang.ServiceType" ItemStyle-CssClass="tl align-center" />
                        <asp:BoundField DataField="PersonGrouping" HeaderText="Lang.PersonnelGroup" ItemStyle-CssClass="tl align-center" />
                        <asp:BoundField DataField="Code" HeaderText="Lang.Code" ItemStyle-CssClass="tl align-center" SortExpression="Code" />
                        <asp:BoundField DataField="ServiceName" HeaderText="Lang.Commodity" ItemStyle-CssClass="tl align-center" SortExpression="ServiceName" />
                        <asp:BoundField DataField="UnitName" HeaderText="Lang.Units" ItemStyle-CssClass="tl align-center" SortExpression="UnitName" />
                    </Columns>
                    <HeaderStyle CssClass="BigGridHeader" />
                    <RowStyle CssClass="BigGridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
    <script>
        $(function () {
            function groupParentShow() {
                var val = $('.br :checked').val();
                if (val == 'BasedServiceGroupLable') {
                    $('.persongroupparents').hide();
                    $('.servicegroupparents').show();
                } else if (val == 'BasedPersonsGroupLable') {
                    $('.servicegroupparents').hide();
                    $('.persongroupparents').show();
                } else {
                    $('.persongroupparents').hide();
                    $('.servicegroupparents').hide();
                }
                $('.basedpersonsgrouplable').change(function () {
                    $('.servicegroupparents').hide();
                    $('.persongroupparents').show();
                });
                $('.basedservicegrouplable').change(function () {
                    $('.persongroupparents').hide();
                    $('.servicegroupparents').show();
                });
            }
            groupParentShow();
            $('.basedvisitor, .basedbuyer, .basedservicetype, .basedpersonnelgroup').change(function () {
                $('.persongroupparents').hide();
                $('.servicegroupparents').hide();
            });
            $('#print').attr('value', Lang.Print);
            $('#print').click(function () {
                var head = Lang.SaleAdminReport;
                <%--if ($("#<%=ddlBy.ClientID %>").val() == <%= (int)AccController.PersonBookGroupBy.ByTotal %>) {
                    head = '<%=jpMiniHeaderTotalBills.Text %>';
            }--%>
                printTable('.ScrollPanelCartable', '../../', head);
            });
            $('.GridRow,.GridAltRow').click(function () {
                $(this).toggleClass('selected');
            });
            dateSelect('.dateselect');
            $('.monetary').keyup(function () {
                $(this).val(numberWithCommas(stripCommas($(this).val())));
            });
            $("#bar").prepend(breadcrumb({
                title: Lang.SaleAdminReport,
                parent: {
                    title: Lang.Reports,
                    url: 'menu/reports.aspx'
                }
            }));
            setClassReports();
            var sgg = {};
            $('.mCommodityGroupings option').each(function () {
                sgg[$(this).attr('value')] = $(this).text();
            });
            var pgg = {};
            $('.mPersonGroupings option').each(function () {
                pgg[$(this).attr('value')] = $(this).text();
            });
            var cog = {};
            $('.mCommodityGroup option').each(function () {
                cog[$(this).attr('value')] = $(this).text();
            });
            var peg = {};
            $('.mPersonGroup option').each(function () {
                peg[$(this).attr('value')] = $(this).text();
            });
            //if ($jQuery1_9('#builder').length > 0)
            $jQuery1_9('#builder').queryBuilder({
                filters: [
                    {
                        id: 'SGCategory',
                        label: Lang.ServiceGroupLable,
                        type: 'integer',
                        input: 'select',
                        values: sgg,
                        operators: ['equal', 'not_equal']
                    },
                    {
                        id: 'PGCategory',
                        label: Lang.PersonsGroupLable,
                        type: 'integer',
                        input: 'select',
                        values: pgg,
                        operators: ['equal', 'not_equal']
                    },
                    {
                        id: 'SCategory',
                        label: Lang.ServiceType,
                        type: 'integer',
                        input: 'select',
                        values: cog,
                        operators: ['equal', 'not_equal']
                    },
                    {
                        id: 'PCategory',
                        label: Lang.PersonnelGroup,
                        type: 'integer',
                        input: 'select',
                        values: peg,
                        operators: ['equal', 'not_equal']
                    }
                ]
            });
            // save rules before post back
            $('.doShow').click(function () {
                var rules = $jQuery1_9('#builder').queryBuilder('getRules');
                var rulesStr = JSON.stringify(rules);
                // alert(rulesStr);
                $('.mQueryBuilder').val(rulesStr);
                $('.mQueryBuilderSql').val($jQuery1_9('#builder').queryBuilder('getSQL', false, false).sql);
            });
            //reload saved rules
            $jQuery1_9('#builder').queryBuilder('reset');
            var ruleStr = $('.mQueryBuilder').val();
            if (ruleStr != '') {
                var rule = eval("(" + ruleStr + ")");
                try {
                    $jQuery1_9('#builder').queryBuilder('setRules', rule);
                } catch (e) {
                }
            }
            $jQuery1_9('#builder').on('afterCreateRuleInput.queryBuilder', function (rule) {
                makeChosen();
            });

        });
    </script>
</asp:Content>
