﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reports_accounting_FinanceSheet : System.Web.UI.Page
{
    System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
    int incomeID, outcomeID, capitalID, assetID, oweID;
    decimal mProfitSum = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        msg.Visible = false;
        incomeID = Helper.GetInt(DbProxy.Instance.GetConfig("TopicGroupIncome"), Null.NullInteger);
        outcomeID = Helper.GetInt(DbProxy.Instance.GetConfig("TopicGroupOutcome"), Null.NullInteger);
        //capitalID = Helper.GetInt(DbProxy.Instance.GetConfig("TopicGroupCapital"), Null.NullInteger);
        //assetID = Helper.GetInt(DbProxy.Instance.GetConfig("TopicGroupAsset"), Null.NullInteger);
        //oweID = Helper.GetInt(DbProxy.Instance.GetConfig("TopicGroupOwes"), Null.NullInteger);

        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["TotalProfitReport"];
            Page.SetLang();
          //  mCapitalGrid.EmptyDataText = LangProvider.Instance["EmptyData"];
            mTitle.Text = LangProvider.Instance["TotalProfitReport"];
            BindTopics();
            BindWards();

            int year = cal.GetYear(DateTime.Now);
            //mFromDate.Text = string.Format("{0}/01/01", year - 1300);
            //mToDate.Text = Helper.FormatDate(cal.ToDateTime(year + 1, 1, 1, 1, 1, 1, 1).AddDays(-1), "yy/MM/dd");
            IDataReader financeYearStartDate = DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");//0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            //mFromDate.Text = 
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);
        }
    }

    #region Report Settings

    #endregion

    #region Bindings

    void BindWards()
    {
        //IDataReader projects = DbProxy.Instance.ExecuteReader("SELECT ID, Name + '(' + ISNULL(Code, '') + ')' FROM Ward WHERE SectionType=8");
        //mWards.Items.Clear();
        //mWards.Items.Add(new ListItem(LangProvider.Instance["AllProject"], ""));
        //if (projects != null)
        //{
        //    while (projects.Read())
        //    {
        //        if (!projects.IsDBNull(0) && !projects.IsDBNull(1))
        //            mWards.Items.Add(new ListItem(projects.GetString(1), projects.GetInt32(0).ToString()));
        //    }
        //    projects.Close();
        //}
        //mWards.SelectedIndex = 0;

        //IDataReader equipments = DbProxy.Instance.ExecuteReader("SELECT ID, FullName, Code, Title FROM EquipmentView");
        //mEquipment.Items.Clear();
        //mEquipment.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
        //if (projects != null)
        //{
        //    while (equipments.Read())
        //    {
        //        if (!equipments.IsDBNull(0) && !equipments.IsDBNull(1) && !equipments.IsDBNull(2))
        //            mEquipment.Items.Add(new ListItem(equipments.GetString(1) + " - " + equipments.GetString(2),
        //                equipments.GetInt32(0).ToString()));
        //    }
        //    equipments.Close();
        //}
        //mEquipment.SelectedIndex = 0;

        //IDataReader persons = DbProxy.Instance.ExecuteReader("SELECT ID, [dbo].[DisplayPerson](ID, 1), ISNULL(Code, '') FROM Personnel WHERE Enabled = 1 AND Type=101");
        //mPersons.Items.Clear();
        //mPersons.Items.Add(new ListItem(LangProvider.Instance["AllPersons"], ""));
        //if (persons != null)
        //{
        //    while (persons.Read())
        //    {
        //        if (!persons.IsDBNull(0) && !persons.IsDBNull(1) && !persons.IsDBNull(2))
        //            mPersons.Items.Add(new ListItem(persons.GetString(1) + " - کد: " + persons.GetString(2),
        //                persons.GetInt32(0).ToString()));
        //    }
        //    persons.Close();
        //}
        //mPersons.SelectedIndex = 0;
    }

    private void BindTopics()
    {
        //mOweDueTree.Nodes.Clear();
        //string topicGroupWhere = string.Format(" Topic.IsTemp = 1 AND Topic.TopicGroupID = {0}", incomeID);
        //BindRoot(mOweDueTree, topicGroupWhere, LangProvider.Instance["TopicGroupIncome"]);
        //topicGroupWhere = string.Format(" Topic.IsTemp = 1 AND Topic.TopicGroupID = {0}", outcomeID);
        //BindRoot(mOweDueTree, topicGroupWhere, LangProvider.Instance["TopicGroupOutcome"]);

        mProfitTree.Nodes.Clear();
        string topicGroupWhere = string.Format(" Topic.TopicGroupID = {0} ", incomeID);
        BindRoot(mProfitTree, topicGroupWhere, LangProvider.Instance["TopicGroupIncome"]);
        topicGroupWhere = string.Format(" Topic.TopicGroupID = {0} ", outcomeID);
        BindRoot(mProfitTree, topicGroupWhere, LangProvider.Instance["TopicGroupOutcome"]);

        //mCapitalTree.Nodes.Clear();
        //topicGroupWhere = string.Format(" (Topic.TopicGroupID = {0})", capitalID);
        //BindRoot(mCapitalTree, topicGroupWhere, LangProvider.Instance["TopicGroupCapital"]);

        //mBalanceTree.Nodes.Clear();
        //topicGroupWhere = string.Format(" (Topic.TopicGroupID = {0})", assetID);
        //BindRoot(mBalanceTree, topicGroupWhere, LangProvider.Instance["TopicGroupAsset"]);
        //topicGroupWhere = string.Format(" (Topic.TopicGroupID = {0})", oweID);
        //BindRoot(mBalanceTree, topicGroupWhere, LangProvider.Instance["TopicGroupOwes"]);
        //topicGroupWhere = string.Format(" (Topic.TopicGroupID = {0})", capitalID);
        //BindRoot(mBalanceTree, topicGroupWhere, LangProvider.Instance["TopicGroupCapital"]);
    }
    private void BindRoot(TreeView tree, string topicGroupWhere, string rootName)
    {
        TreeNode rootNode = new TreeNode(rootName, "");
        rootNode.Checked = true;
        List<TreeInfo> roots = AccController.GetTrees(0, SiteSettings.ProgramID, null, topicGroupWhere);
        roots.Sort();
        for (int i = 0; i < roots.Count; i++)
        {
            List<TreeNode> nodes = GetTreeNodes(roots[i], topicGroupWhere);
            if (nodes != null)
                for (int j = 0; j < nodes.Count; j++)
                    rootNode.ChildNodes.Add(nodes[j]);
        }

        tree.Nodes.Add(rootNode);
        tree.CollapseAll();
        rootNode.Expand();
    }

    private List<TreeNode> GetTreeNodes(TreeInfo info, string topicGroupWhere)
    {
        TopicInfo topic = AccController.GetTopic(info.TopicID);
        TreeNode node = GetSingleTreeNode(info, topic);
        if (node == null)
            return null;
        List<TreeNode> res = new List<TreeNode>();
        List<TreeInfo> childs = AccController.GetTrees(info, topicGroupWhere);
        childs.Sort();
        if (childs.Count > 0)
            node.ImageUrl = "~/img/programIcon.gif";
        if (childs.Count == 0 || info.Depth == Helper.GetInt(mLevel.SelectedValue, Null.NullInteger))
        {
            res.Add(node);
            return res;
        }
        
        for (int i = 0; i < childs.Count; i++)
        {
            //node.ChildNodes.Add(GetTreeNode(childs[i]));
            List<TreeNode> chs = GetTreeNodes(childs[i], topicGroupWhere);
            if (chs != null)
                res.AddRange(chs);
        }
        return res;
    }
    private TreeNode GetSingleTreeNode(TreeInfo info, TopicInfo topic)
    {
        if (topic == null || info == null)
            return null;
        TreeNode node = new TreeNode(topic.NameCode, info.TreePath);
        node.ImageUrl = "~/img/leaf.gif";
        node.Checked = true;
        return node;
    }

    #endregion

    private DataTable GetItems(string topicGroupWhere, TreeView tree, string orderBy, string orderDir, string tempWhere)
    {
        DateTime start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        DateTime end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);
        //int projectID = Helper.GetInt(mWards.SelectedValue, Null.NullInteger);
        //int equipmentID = Helper.GetInt(mEquipment.SelectedValue, Null.NullInteger);
        //int personnelID = Helper.GetInt(mPersons.SelectedValue, Null.NullInteger);
        int programID = SiteSettings.ProgramID;
        //int depth = Helper.GetInt(mLevel.SelectedValue, Null.NullInteger);

        string where = string.Format(@" DA.WorkflowStatus = 1 AND {0} <= DA.Date AND DA.Date <= {1} "
            , Helper.QoutedDateTime(start), Helper.QoutedDateTime(end));
        //if (projectID != Null.NullInteger)
        //    where += " AND DAI.ProjectID=" + projectID;
        //if (equipmentID != Null.NullInteger)
        //    where += " AND DAI.EquipmentID=" + equipmentID;
        //if (personnelID != Null.NullInteger)
        //    where += " AND DAI.PersonnelID=" + personnelID;
        where += " AND (DAI.TreePath + '-') LIKE (FSODTempTable.TreePath + '-%')";

        if (!string.IsNullOrEmpty(tempWhere))
        {
            where += tempWhere;
        }

        // ------------------------------
        if (programID == -1) // default program = NULL (See SiteSettings.cs)
            where += " AND DA.ProgramID IS NULL";
        else if (programID == 0) // all programs
        {
        }
        else
            where += " AND  DA.ProgramID = " + programID;

        string treeWhere = "";
        if (programID == -1) // default program = NULL (See SiteSettings.cs)
            treeWhere += " AND Topic.ProgramID IS NULL";
        else if (programID == 0) // all programs
        {
        }
        else
            treeWhere += " AND  Topic.ProgramID = " + programID;
        //treeWhere += " AND Trees.Depth = " + depth;
        treeWhere += " AND " + topicGroupWhere;

        #region Apply tree
        List<string> treePathes = new List<string>();
        if (tree.CheckedNodes != null && tree.CheckedNodes.Count > 0)
            for (int i = 0; i < tree.CheckedNodes.Count; i++)
                if (!string.IsNullOrEmpty(tree.CheckedNodes[i].Value))
                    treePathes.Add(tree.CheckedNodes[i].Value);
        if (treePathes.Count > 0)
        {
            if (treePathes.Count == 1)
            {
                //treeWhere += " AND(Trees.TreePath = " + Helper.QoutedString(treePathes[0]) + " OR Trees.TreePath LIKE '" + treePathes[0] + "-%')";
                treeWhere += " AND Trees.TreePath = " + Helper.QoutedString(treePathes[0]);
            }
            else
            {
                treeWhere += " AND (";
                bool first = false;
                for (int i = 0; i < treePathes.Count; i++)
                {
                    if (string.IsNullOrEmpty(treePathes[i]))
                        continue;
                    if (first)
                        treeWhere += " OR ";
                    first = true;
                    //treeWhere += "(Trees.TreePath = " + Helper.QoutedString(treePathes[i]) + " OR Trees.TreePath LIKE '" + treePathes[i] + "-%')";
                    treeWhere += "Trees.TreePath = " + Helper.QoutedString(treePathes[i]);
                }
                treeWhere += ")";
            }
        }
        #endregion

        string qry = string.Format(@"
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSODTempTable]') AND type in (N'U'))
	DROP TABLE FSODTempTable
Create Table FSODTempTable (TreePath nvarchar(900), TreePathName nvarchar(2000), TreePathCodes nvarchar(2000), TopicGroupID int)

INSERT INTO FSODTempTable (TreePath, TreePathName, TreePathCodes, TopicGroupID)
    SELECT TreePath, dbo.TreePathToTopicTitle(TreePath), dbo.TreePathToTopicCode(TreePath), TopicGroupID
    FROM Trees INNER JOIN Topic ON [dbo].[TreePathToProgramID](TreePath) = Topic.ID
    WHERE 1 = 1 {0}

SELECT TreePath, TreePathName, dbo.TreePathToTopicCode(TreePath) AS TreePathCodes, TopicGroupID
, (SELECT SUM(ISNULL(Due, 0) - ISNULL(Owe, 0)) FROM dbo.DocumentAccountingItems DAI INNER JOIN DocumentAccounting DA ON DA.ID = DAI.DocumentAccountingID 
    WHERE {1}) Remained
FROM FSODTempTable"
                , treeWhere, where);

        if (string.IsNullOrEmpty(orderBy))
            qry += " ORDER BY TreePathCodes";
        else
            qry += " ORDER BY " + orderBy + " " + orderDir;
        DataTable items = DbProxy.Instance.FillData(qry, "fck", 60 * 5); // 5 minutes
        return items;
    }
    decimal BindHolder(PlaceHolder holder
        , string col1Title, string col1Where, TreeView col1Tree
        , string col2Title, string col2Where, TreeView col2Tree, string tempWehere)
    {
        holder.Controls.Clear();

        DataTable assets = GetItems(col1Where, col1Tree, null, null, tempWehere);
        DataTable owes = GetItems(col2Where, col2Tree, null, null, tempWehere);

        if (assets == null || owes == null)
            return 0;

        List<DataRow> oweRows = new List<DataRow>();
        DataTable staredSumSerivcePrice = GetStaredSumSerivcePrice();
        DataTable endedSumSerivcePrice = GetEndedSumSerivcePrice();
        for (int i = 0; i < owes.Rows.Count; i++)
        {
            oweRows.Add(owes.Rows[i]);
        }
        DataRow oweRow = owes.NewRow();
        oweRow = owes.NewRow();
        oweRow["TreePathName"] = staredSumSerivcePrice.Rows[0]["TreePathName"];
        oweRow["Remained"] = staredSumSerivcePrice.Rows[0]["Remained"];
        oweRow["TreePath"] = staredSumSerivcePrice.Rows[0]["TreePath"];
        oweRow["TreePathCodes"] = staredSumSerivcePrice.Rows[0]["TreePathCodes"];
        oweRow["TopicGroupID"] = staredSumSerivcePrice.Rows[0]["TopicGroupID"];
        oweRows.Add(oweRow);

        List<DataRow> assetsRows = new List<DataRow>();
        for (int i = 0; i < assets.Rows.Count; i++)
        {
            assetsRows.Add(assets.Rows[i]);
        }
        DataRow assetsRow = assets.NewRow();
        assetsRow = assets.NewRow();
        assetsRow["TreePathName"] = LangProvider.Instance["EndedSumSerivcePrice"];
        assetsRow["Remained"] = endedSumSerivcePrice.Rows[0]["Total"];
        assetsRows.Add(assetsRow);

        decimal sumAsset = 0, sumOwe = 0;
        StringBuilder builder = new StringBuilder();
        string unit = SiteSettings.MonetaryUnit == Null.NullInteger ? LangProvider.Instance["Rial"] : new DbProxy().GetEnumTitle("MonetaryUnit", SiteSettings.MonetaryUnit);

        builder.AppendFormat("<table class='report table table-bordered table-condensed zebra'>");
        #region header row
        builder.AppendFormat("<tr class='theader'>");
        builder.AppendFormat("<td colspan='2'>{0}</td><td>{1}</td><td>{2}</td><td colspan='2'>{3}</td><td>{4}</td><td>{5}</td>", col1Title, unit, LangProvider.Instance["Diagnosis"], col2Title, unit, LangProvider.Instance["Diagnosis"]);
        builder.AppendFormat("</tr>");
        #endregion

        for (int i = 0; i < Math.Max(assetsRows.Count, oweRows.Count); i++)
        {
            DataRow asset = i < assetsRows.Count ? assetsRows[i] : null;
            DataRow owe = i < oweRows.Count ? oweRows[i] : null;
            builder.Append("<tr>");
            if (asset == null)
                builder.AppendFormat("<td></td><td></td><td></td><td></td>");
            else
            {
                builder.AppendFormat("<td>{0}</td><td>{1}</td>", asset["TreePathCodes"], asset["TreePathName"]);
                decimal assetRemain = Helper.GetDecimal(asset, "Remained", 0);
                string cls = "";
                if (IsInverse(Helper.GetInt(asset["TopicGroupID"], 0), assetRemain))
                    cls = " class='neg'";
                builder.AppendFormat("<td{0}>{1}</td>", cls, SiteSettings.ToMonetary(Math.Abs(assetRemain)
                    , DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", ""));
                builder.AppendFormat("<td>{0}</td>", (assetRemain < 0) ? LangProvider.Instance["Li"] : LangProvider.Instance["Cr"]);
            }
            if (owe == null)
                builder.AppendFormat("<td></td><td></td><td></td><td></td>");
            else
            {
                builder.AppendFormat("<td>{0}</td><td>{1}</td>", owe["TreePathCodes"], owe["TreePathName"]);
                decimal oweRemain = Helper.GetDecimal(owe, "Remained", 0);
                string cls = "";
                if (IsInverse(Helper.GetInt(owe["TopicGroupID"], 0), oweRemain))
                    cls = " class='neg'";
                builder.AppendFormat("<td{0}>{1}</td>", cls, SiteSettings.ToMonetary(Math.Abs(oweRemain)
                    , DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", ""));
                builder.AppendFormat("<td>{0}</td>", (oweRemain < 0) ? LangProvider.Instance["Li"] : LangProvider.Instance["Cr"]);
            }
            builder.Append("</tr>");

            if (owe != null)
                sumOwe += SiteSettings.ToMonetary(Helper.GetDecimal(owe, "Remained", 0), DateTime.Now);
            if (asset != null)
                sumAsset += SiteSettings.ToMonetary(Helper.GetDecimal(asset, "Remained", 0), DateTime.Now);
        } 

        #region footer row
        builder.AppendFormat("<tr class='theader'>");
        builder.AppendFormat("<td></td><td>{0}</td><td {1}>{2}</td><td>{3}</td><td></td><td>{4}</td><td{5}>{6}</td><td>{7}</td>"
            , LangProvider.Instance["Sum"]
            , (sumAsset > 0) ? "" : " class='neg'"
            , Math.Abs(sumAsset).ToString("N4", Helper.NumberCulture).Replace(".0000", "")
            , (sumAsset < 0) ? LangProvider.Instance["Li"] : LangProvider.Instance["Cr"]
            , LangProvider.Instance["Sum"]
            , (sumOwe < 0) ? "" : " class='neg'"
            , Math.Abs(sumOwe).ToString("N4", Helper.NumberCulture).Replace(".0000", "")
            , (sumOwe < 0) ? LangProvider.Instance["Li"] : LangProvider.Instance["Cr"]);
        builder.AppendFormat("</tr>");
        #endregion

        #region FinalSum row
        builder.AppendFormat("<tr class='theader'>");
        builder.AppendFormat("<td></td><td></td><td></td><td></td><td></td><td>{0}</td><td{1}>{2}</td><td>{3}</td>"
            , LangProvider.Instance["FinalSum"]
            , (sumAsset + sumOwe > 0) ? "" : " class='neg'"
            , Math.Abs(sumAsset + sumOwe).ToString("N4", Helper.NumberCulture).Replace(".0000", "")
            , (sumAsset + sumOwe < 0) ? LangProvider.Instance["Li"] : LangProvider.Instance["Cr"]);
        builder.AppendFormat("</tr>");
        #endregion

        builder.AppendFormat("</table>");
        holder.Controls.Add(new LiteralControl(builder.ToString()));

        return sumAsset + sumOwe;
    }
    private bool IsInverse(int topicGroupID, decimal remained)
    {
        /*دارایی     بد
        بدهی      بس
        سرمایه   بس
        درامد و فروش  بس
        هزینه و خرید    بد
         */
        if (topicGroupID == assetID || topicGroupID == outcomeID)
            return remained > 0;
        return remained < 0;
    }

    #region OweDue
    //protected void BindOweDue()
    //{
    //    BindHolder(mOweDueHolder
    //                , LangProvider.Instance["TopicGroupIncome"], string.Format(" Topic.IsTemp = 1 AND Topic.TopicGroupID = {0}"
    //                , incomeID), mOweDueTree
    //                , LangProvider.Instance["TopicGroupOutcome"], string.Format(" Topic.IsTemp = 1 AND Topic.TopicGroupID = {0}"
    //                , outcomeID), mOweDueTree
    //                , @" AND DAI.TreePath IN (SELECT TreePath
    //                  FROM Trees INNER JOIN Topic ON [dbo].[TreePathToProgramID](TreePath) = Topic.ID
    //                  WHERE Topic.IsTemp = 1) ");
    //}
    #endregion

    #region Profit
    protected void BindProfit()
    {
        mProfitSum = BindHolder(mProfitHolder
                    , LangProvider.Instance["TopicGroupIncome"], string.Format(" Topic.TopicGroupID = {0}", incomeID), mProfitTree
                    , LangProvider.Instance["TopicGroupOutcome"], string.Format(" Topic.TopicGroupID = {0}", outcomeID), mProfitTree, null);
    }
    #endregion

    #region Capital
    //decimal capitalRemained = 0;
    //protected void BindCapitalGrid()
    //{
    //    string topicGroupWhere = string.Format(" (Topic.TopicGroupID = {0})", capitalID);
    //    DataTable items = GetItems(topicGroupWhere, mCapitalTree, CapitalSortCol, CapitalSortOrder, null);
    //    capitalRemained = 0;
    //    try
    //    {
    //        mCapitalGrid.DataSource = items;
    //        mCapitalGrid.DataBind();
    //    }
    //    finally
    //    {
    //        if (null != items)
    //        {
    //            items.Dispose();
    //        }
    //    }
    //}
    //protected void mCapitalGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        DataRowView record = e.Row.DataItem as DataRowView;
    //        DateTime dt = DateTime.Now;

    //        decimal remained = Helper.GetDecimal(record["Remained"], 0);
    //        capitalRemained += remained;
    //        e.Row.Cells[3].Text = Math.Abs(remained).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
    //        if (IsInverse(Helper.GetInt(record["TopicGroupID"], 0), remained))
    //            Helper.SignifyNegative(e.Row.Cells[3]);
    //        e.Row.Cells[4].Text = (remained < 0) ? LangProvider.Instance["Li"] : LangProvider.Instance["Cr"];
    //    }
    //    if (e.Row.RowType == DataControlRowType.Footer)
    //    {
    //        Label lblTotal = (Label)e.Row.FindControl("remainingTotal");
    //        lblTotal.Text = Math.Abs(capitalRemained).ToString("N4", Helper.NumberCulture).Replace(".0000", "");

    //        lblTotal = (Label)e.Row.FindControl("finalDiagnosis");
    //        lblTotal.Text = (capitalRemained < 0) ? LangProvider.Instance["Li"] : LangProvider.Instance["Cr"];
    //    }
    //}
    //protected void mCapitalGrid_Sorting(object sender, GridViewSortEventArgs e)
    //{
    //    if (e.SortExpression == CapitalSortCol)
    //    {
    //        CapitalSortOrder = CapitalSortOrder == "asc" ? "desc" : "asc";
    //    }
    //    else
    //    {
    //        CapitalSortCol = e.SortExpression;
    //        CapitalSortOrder = "asc";
    //    }
    //    BindCapitalGrid();
    //}
    //protected string CapitalSortCol
    //{
    //    get
    //    {
    //        return Helper.GetString(ViewState["CapitalSortCol"], null);
    //    }
    //    set
    //    {
    //        ViewState["CapitalSortCol"] = value;
    //    }
    //}
    //protected string CapitalSortOrder
    //{
    //    get
    //    {
    //        return Helper.GetString(ViewState["CapitalSortOrder"], "asc");
    //    }
    //    set
    //    {
    //        ViewState["CapitalSortOrder"] = value;
    //    }
    //}
    #endregion

    #region balance
    //void BindBalance()
    //{
    //    PlaceHolder holder = mBalanceHolder;
    //    holder.Controls.Clear();
    //    int programID = SiteSettings.ProgramID;
    //    DataTable assets = GetItems(string.Format(" Topic.TopicGroupID = {0} AND Trees.TreePath NOT IN ( dbo.GetSetting2('ServiceTreePath', {1}))", assetID, (programID < 1) ? "NULL" : programID.ToString()), mBalanceTree, null, null, null);
    //    DataTable owes = GetItems(string.Format(" Topic.TopicGroupID = {0}", oweID), mBalanceTree, null, null, null);
    //    DataTable capitals = GetItems(string.Format(" Topic.TopicGroupID = {0}", capitalID), mBalanceTree, null, null, null);

    //    if (assets == null || owes == null || capitals == null)
    //        return;

    //    List<DataRow> assetRows = new List<DataRow>();
    //    DataTable staredSumSerivcePrice = GetStaredSumSerivcePrice();
    //    DataTable endedSumSerivcePrice = GetEndedSumSerivcePrice();
    //    for (int i = 0; i < assets.Rows.Count; i++)
    //    {
    //        assetRows.Add(assets.Rows[i]);
    //    }
    //    DataRow assetRow = assets.NewRow();
    //    assetRow = assets.NewRow();
    //    assetRow["TreePathName"] = staredSumSerivcePrice.Rows[0]["TreePathName"];
    //    assetRow["Remained"] = -1 * Helper.GetDecimal(endedSumSerivcePrice.Rows[0], "Total", 0);
    //    assetRow["TreePath"] = staredSumSerivcePrice.Rows[0]["TreePath"];
    //    assetRow["TreePathCodes"] = staredSumSerivcePrice.Rows[0]["TreePathCodes"];
    //    assetRow["TopicGroupID"] = staredSumSerivcePrice.Rows[0]["TopicGroupID"];
    //    assetRows.Add(assetRow);

    //    List<DataRow> rows = new List<DataRow>();
    //    decimal oweSum = 0, capitalSum = 0;
    //    for (int i = 0; i < owes.Rows.Count; i++)
    //    {
    //        oweSum += Helper.GetDecimal(owes.Rows[i], "Remained", 0);
    //        rows.Add(owes.Rows[i]);
    //    }
    //    DataRow row = owes.NewRow();
    //    row["TreePathName"] = LangProvider.Instance["Sum"] + " " + LangProvider.Instance["Owes"];
    //    row["Remained"] = oweSum;
    //    rows.Add(row);
    //    for (int i = 0; i < capitals.Rows.Count; i++)
    //    {
    //        capitalSum += Helper.GetDecimal(capitals.Rows[i], "Remained", 0);
    //        rows.Add(capitals.Rows[i]);
    //    }
    //    row = owes.NewRow();
    //    row["TreePathName"] = LangProvider.Instance["ProfitSum"];
    //    row["Remained"] = mProfitSum;
    //    rows.Add(row);
    //    row = owes.NewRow();
    //    row["TreePathName"] = LangProvider.Instance["Sum"] + " " + LangProvider.Instance["Capitals"];
    //    row["Remained"] = capitalSum + mProfitSum;
    //    rows.Add(row);

    //    decimal sumAsset = 0;
    //    StringBuilder builder = new StringBuilder();
    //    string unit = SiteSettings.MonetaryUnit == Null.NullInteger ? LangProvider.Instance["Rial"] : new DbProxy().GetEnumTitle("MonetaryUnit"
    //        , SiteSettings.MonetaryUnit);

    //    builder.AppendFormat("<table class='report table table-bordered table-condensed zebra'>");
    //    #region header row
    //    builder.AppendFormat("<tr class='theader'>");
    //    builder.AppendFormat("<td colspan='2'>{0}</td><td>{1}</td><td>{2}</td><td colspan='2'>{3}</td><td>{4}</td><td>{5}</td>"
    //        , LangProvider.Instance["Assets"], unit, LangProvider.Instance["Diagnosis"]
    //        , LangProvider.Instance["OwesAndCapital"], unit, LangProvider.Instance["Diagnosis"]);
    //    builder.AppendFormat("</tr>");
    //    #endregion

    //    for (int i = 0; i < Math.Max(assets.Rows.Count, rows.Count); i++)
    //    {
    //        DataRow asset = i < assetRows.Count ? assetRows[i] : null;
    //        DataRow owe = i < rows.Count ? rows[i] : null;
    //        builder.Append("<tr>");
    //        if (asset == null)
    //            builder.AppendFormat("<td></td><td></td><td></td><td></td>");
    //        else
    //        {
    //            builder.AppendFormat("<td>{0}</td><td>{1}</td>", asset["TreePathCodes"], asset["TreePathName"]);
    //            decimal assetRemain = Helper.GetDecimal(asset, "Remained", 0);
    //            string cls = "";
    //            if (IsInverse(Helper.GetInt(asset["TopicGroupID"], 0), assetRemain))
    //                cls = " class='neg'";
    //            builder.AppendFormat("<td{0}>{1}</td>", cls, SiteSettings.ToMonetary(Math.Abs(assetRemain), DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", ""));
    //            builder.AppendFormat("<td>{0}</td>", (assetRemain < 0) ? LangProvider.Instance["Li"] : LangProvider.Instance["Cr"]);
    //        }
    //        if (owe == null)
    //            builder.AppendFormat("<td></td><td></td><td></td><td></td>");
    //        else
    //        {
    //            string cls2 = "";
    //            if (Helper.GetInt(owe["TopicGroupID"], 0) == 0) //  it is a sum
    //                cls2 = "class='theader'";
    //            builder.AppendFormat("<td>{0}</td><td {1}>{2}</td>", owe["TreePathCodes"], cls2, owe["TreePathName"]);
    //            decimal oweRemain = Helper.GetDecimal(owe, "Remained", 0);
    //            string cls = "";
    //            if (IsInverse(Helper.GetInt(owe["TopicGroupID"], 0), oweRemain))
    //                cls = " class='neg'";
    //            builder.AppendFormat("<td{0}>{1}</td>", cls, SiteSettings.ToMonetary(Math.Abs(oweRemain), DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", ""));
    //            builder.AppendFormat("<td>{0}</td>", (oweRemain < 0) ? LangProvider.Instance["Li"] : LangProvider.Instance["Cr"]);
    //        }
    //        builder.Append("</tr>");

    //        if (asset != null)
    //            sumAsset += SiteSettings.ToMonetary(Helper.GetDecimal(asset, "Remained", 0), DateTime.Now);
    //    }

    //    #region footer row
    //    builder.AppendFormat("<tr class='theader'>");
    //    builder.AppendFormat("<td></td><td>{0}</td><td{1}>{2}</td><td>{3}</td><td></td><td>{4}</td><td{5}>{6}</td><td>{7}</td>"
    //        , LangProvider.Instance["Sum"]
    //        , (sumAsset < 0) ? "" : " class='neg'"
    //        , Math.Abs(sumAsset).ToString("N4", Helper.NumberCulture).Replace(".0000", "")
    //        , (sumAsset < 0) ? LangProvider.Instance["Li"] : LangProvider.Instance["Cr"]
    //        , LangProvider.Instance["Sum"] + " " + LangProvider.Instance["OwesAndCapital"]
    //        , (oweSum + capitalSum + mProfitSum > 0) ? "" : " class='neg'"
    //        , Math.Abs(oweSum + capitalSum + mProfitSum).ToString("N4", Helper.NumberCulture).Replace(".0000", "")
    //        , (oweSum + capitalSum + mProfitSum < 0) ? LangProvider.Instance["Li"] : LangProvider.Instance["Cr"]);
    //    builder.AppendFormat("</tr>");
    //    #endregion

    //    #region FinalSum row
    //    builder.AppendFormat("<tr class='theader'>");
    //    builder.AppendFormat("<td></td><td></td><td></td><td></td><td></td><td>{0}</td><td{1}>{2}</td><td>{3}</td>"
    //        , LangProvider.Instance["FinalSum"]
    //        //, (sumAsset + oweSum + capitalSum + mProfitSum > 0) ? "" : " class='neg'"
    //        , ""
    //        , Math.Abs(sumAsset + oweSum + capitalSum + mProfitSum).ToString("N4", Helper.NumberCulture).Replace(".0000", "")
    //        , (sumAsset + oweSum + capitalSum + mProfitSum < 0) ? LangProvider.Instance["Li"] : LangProvider.Instance["Cr"]);
    //    builder.AppendFormat("</tr>");
    //    #endregion

    //    builder.AppendFormat("</table>");
    //    holder.Controls.Add(new LiteralControl(builder.ToString()));
    //}
    #endregion

    private DataTable GetStaredSumSerivcePrice()
    {
        //DateTime start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        //DateTime end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        //start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        //end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);
        //int projectID = Helper.GetInt(mWards.SelectedValue, Null.NullInteger);
        //int equipmentID = Helper.GetInt(mEquipment.SelectedValue, Null.NullInteger);
        //int personnelID = Helper.GetInt(mPersons.SelectedValue, Null.NullInteger);
        //string fromVoucherNo = mFromVoucherNo.Text.Trim(), toVoucherNo = mToVoucherNo.Text.Trim();
        int programID = SiteSettings.ProgramID;
        //int depth = Helper.GetInt(mLevel.SelectedValue, Null.NullInteger);
        string qry = string.Format(@"DECLARE @ServiceTreePath NVARCHAR(300)
SET @ServiceTreePath = dbo.GetSetting2('ServiceTreePath', {0})
SELECT Trees.TreePath, dbo.TreePathToTopicTitle(Trees.TreePath) TreePathName , dbo.TreePathToTopicCode(Trees.TreePath) TreePathCodes, TopicGroupID
,(SELECT SUM(ISNULL(Due, 0) - ISNULL(Owe, 0)) 
FROM dbo.DocumentAccountingItems
INNER JOIN DocumentAccounting ON DocumentAccounting.ID = DocumentAccountingItems.DocumentAccountingID
WHERE DocumentAccounting.WorkflowStatus = 1 AND TreePath = Trees.TreePath) Remained
FROM Trees INNER JOIN Topic ON [dbo].[TreePathToProgramID](TreePath) = Topic.ID  WHERE Trees.TreePath = @ServiceTreePath", (programID < 1) ? "NULL" : programID.ToString());

        DataTable items = DbProxy.Instance.FillData(qry, "fck", 60 * 5); // 5 minutes
        return items;
    }

    private DataTable GetEndedSumSerivcePrice()
    {
        //DateTime start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        //DateTime end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        //start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        //end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);
        //int projectID = Helper.GetInt(mWards.SelectedValue, Null.NullInteger);
        //int equipmentID = Helper.GetInt(mEquipment.SelectedValue, Null.NullInteger);
        //int personnelID = Helper.GetInt(mPersons.SelectedValue, Null.NullInteger);
        //string fromVoucherNo = mFromVoucherNo.Text.Trim(), toVoucherNo = mToVoucherNo.Text.Trim();
        int programID = SiteSettings.ProgramID;
        //int depth = Helper.GetInt(mLevel.SelectedValue, Null.NullInteger);
        string qry = string.Format(@"DECLARE @StartDate DATE, @EndDate DATE
   SELECT @StartDate = CONVERT(DATE, StartDate), @EndDate = CONVERT(DATE, EndDate) FROM FinanceYear WHERE Status = 2
      SELECT SUM(AvgPrice) Total FROM (SELECT
	        ISNULL(CASE WHEN ServicePrice.ProfitPercent IS NULL OR ServicePrice.ProfitPercent = 0
	        THEN (ISNULL((SELECT Top 1 Price FROM PurchaseItems 
	              LEFT OUTER JOIN Purchase ON PurchaseItems.PurchaseID = Purchase.ID
	              LEFT OUTER JOIN StoreServices ON PurchaseItems.StoreServiceID = StoreServices.ID
	              LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
	              WHERE  StoreServiceDef.ServicePriceID = ServicePrice.ID
                  ORDER BY Purchase.ID DESC), ServicePrice.TheLPBP))
	        ELSE [dbo].[GetBuyPriceBasedOnPricingMethod](ServicePrice.ID, @StartDate, @EndDate)
	        END * CASE WHEN (ISNULL(StoreServices.Income, 0) - ISNULL(StoreServices.Outcome, 0)) > 0 THEN (ISNULL(StoreServices.Income, 0) - ISNULL(StoreServices.Outcome, 0)) ELSE 0 END
             , 0) AvgPrice FROM StoreServices	         
	             LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
	             LEFT OUTER JOIN Stores ON StoreServiceDef.StoreID = Stores.ID
	             LEFT OUTER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID
	             LEFT OUTER JOIN dbo.Service ON dbo.ServicePrice.ServiceID = dbo.Service.ID
	             WHERE [Service].[Enabled] = 1 AND Stores.Enabled = 1 AND (Stores.IsWaste IS NULL OR Stores.IsWaste = 0)) A");

        DataTable items = DbProxy.Instance.FillData(qry, "fck", 60 * 5); // 5 minutes
        return items;
    }

    protected void doShow_Click(object sender, EventArgs e)
    {
        DateTime start = Helper.ParseDate(mFromDate.Text, Null.NullDate);
        DateTime end = Helper.ParseDate(mToDate.Text, Null.NullDate);
        if (Null.IsNull(start))
        {
            msg.Text = LangProvider.Instance["InvalidStartDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(end))
        {
            msg.Text = LangProvider.Instance["InvalidEndDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(start) || Null.IsNull(end))
            return;
       // BindOweDue();
        BindProfit();
        //BindCapitalGrid();
       // BindBalance();
    }
    protected void doExcel_Click(object sender, EventArgs e)
    {
        //Exporter.ExportExcel(Response, mTitle.Text, mOweDueGrid);
    }
    protected void mLevel_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindTopics();
    }
}