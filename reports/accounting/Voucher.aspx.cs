﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class reports_accounting_Voucher : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DocumentAccountingID = Helper.GetInt(Request.QueryString["id"], Null.NullInteger);
            BindVoucher();
        }
        #region Print
        doPrintVoucher.Attributes.Add("onclick", string.Format("OpenPrint({0});", this.DocumentAccountingID));
        #endregion
    }

    private void BindVoucher()
    {
        VoucherInfo info = AccController.GetVoucher(this.DocumentAccountingID);
        mSection.Text = DbProxy.Instance.FillScalarString("SELECT Name FROM Ward WHERE ID=" + info.WardID);
        sectionLbl.Visible = mSection.Text.Length > 0;
        mReciever.Text = info.Reciever;
        mTitle.Text = info.DoComments;
        mDate.Text = Helper.FormatDate(info.Date, null);

        BindItems();

        decimal sumOwe, sumDue;
        AccController.CalcSums(this.DocumentAccountingID, out sumOwe, out sumDue);
        bool isBalanced = (sumOwe - sumDue) == 0;
    }

    #region Props
    private int DocumentAccountingID
    {
        get
        {
            if (ViewState["DocumentAccountingID"] == null)
                return Null.NullInteger;
            return (int)ViewState["DocumentAccountingID"];
        }
        set
        {
            ViewState["DocumentAccountingID"] = value;
            doPrintVoucher.Attributes.Remove("onclick");
            doPrintVoucher.Attributes.Add("onclick", string.Format("OpenPrint({0});", this.DocumentAccountingID));
            //if (value != Null.NullInteger)
            //    SiteSettings.LastEditedVoucher = value;
        }
    }
    private int WardID
    {
        get
        {
            if (ViewState["WardID"] == null)
                return Null.NullInteger;
            return (int)ViewState["WardID"];
        }
        set
        {
            ViewState["WardID"] = value;
        }
    }
    #endregion


    decimal sumOwe = 0, sumDue = 0, sumUnit = 0;
    private void BindItems()
    {
        sumOwe = sumDue = sumUnit = 0;
        IDataReader items = AccController.GetVoucherDocument(this.DocumentAccountingID);
        try
        {
            mGrid.DataSource = items;
            mGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Close();
                items.Dispose();
            }
        }

        if (this.DocumentAccountingID == Null.NullInteger)
            mPrograms.Text = "";
        else
            mPrograms.Text = AccController.GetVoucherPrograms(this.DocumentAccountingID);

    }
    private void ResetGrid()
    {
        mGrid.PageIndex = 0;
        mGrid.EditIndex = -1;
        mGrid.SelectedIndex = -1;
    }
    string rowStyle = "GridRow";
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (string.IsNullOrEmpty(Helper.GetString((e.Row.DataItem as System.Data.Common.DbDataRecord)["No"])))
                rowStyle = rowStyle == "GridAltRow" ? "GridRow" : "GridAltRow";
            e.Row.CssClass = rowStyle;

            sumDue += Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["Due"], 0);
            sumOwe += Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["Owe"], 0);
            sumUnit += Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["Unit"], 0);

            Label owe = (Label)e.Row.FindControl("owe");
            owe.Text = Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["Owe"], 0).ToString("N0");

            Label due = (Label)e.Row.FindControl("due");
            due.Text = Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["Due"], 0).ToString("N0");

            Label remaining = (Label)e.Row.FindControl("unit");
            remaining.Text = Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["Unit"], 0).ToString("N0");
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblTotal = (Label)e.Row.FindControl("oweTotal");
            lblTotal.Text = sumOwe.ToString("N0");

            lblTotal = (Label)e.Row.FindControl("dueTotal");
            lblTotal.Text = sumDue.ToString("N0");

            lblTotal = (Label)e.Row.FindControl("unitTotal");
            lblTotal.Text = sumUnit.ToString("N0");
        }
    }
}