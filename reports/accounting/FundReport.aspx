﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" CodeFile="FundReport.aspx.cs" Inherits="reports_accounting_FundReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register src="~/controls/ReportSetting.ascx" tagName="ReportSetting" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" Runat="Server">
    <div id="bar">
    </div>
    <div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" AssociatedControlID="mFromDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="mFromDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label3" runat="server" AssociatedControlID="mToDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="mToDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="userLable" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.User</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mUsers" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label8" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Store</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mStores" runat="server" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin-5-top">
            <div class="col-md-8 col-lg-6 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container form-col-without-padding">
                <div class="col-xs-12 col-sm-3">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
                </div>
                <div class="col-xs-12 col-sm-3">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcel_Click" Enabled="false" />
                </div>
                <div class="col-xs-12 col-sm-3">
                    <input type="button" id="print" class="btn btn-xs btn-dark-blue btn-block" value="" />
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <asp:Label runat="server" ID="msg" CssClass="label-30 alert alert-danger block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
    </div>
    <asp:Panel ID="gridPanel" runat="server" Visible="false">
        <JP:MiniHeader runat="server" Text="Lang.FundReport" ID="mGridTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="mGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true" OnSorting="mGrid_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="PersonName" HeaderText="Lang.PersonName" ItemStyle-HorizontalAlign="Center" SortExpression="PersonName" />
                        <asp:BoundField DataField="Type" HeaderText="Lang.Type" ItemStyle-CssClass="tl align-center" SortExpression="Type" />
                        <asp:BoundField DataField="NoSerial" HeaderText="Lang.FactorNo" ItemStyle-CssClass="tl align-center" SortExpression="NoSerial" />
                        <asp:BoundField DataField="VoucherNo" HeaderText="Lang.DocumentAccountingNum" ItemStyle-CssClass="tl align-center" SortExpression="VoucherNo" />
                        <asp:TemplateField HeaderText="Lang.PayMethodCash" SortExpression="PayMethod">
                            <ItemTemplate>
                                <asp:Label ID="CashPrice" runat="server" Text='<%#Eval("CashPrice") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="TotalCash" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.PayMethodBank" SortExpression="PayMethod">
                            <ItemTemplate>
                                <asp:Label ID="BankPrice" runat="server" Text='<%#Eval("BankPrice") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="TotalBank" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.PayMethodCheck" SortExpression="PayMethod">
                            <ItemTemplate>
                                <asp:Label ID="CheckPrice" runat="server" Text='<%#Eval("CheckPrice") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="TotalCheck" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.PayMethodOwe" SortExpression="PayMethod">
                            <ItemTemplate>
                                <asp:Label ID="OwedPrice" runat="server" Text='<%#Eval("OwedPrice") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="TotalOwe" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Date" HeaderText="Lang.Date" ItemStyle-HorizontalAlign="Center" SortExpression="Date" />
                        <asp:BoundField DataField="Date" HeaderText="Lang.Time" ItemStyle-HorizontalAlign="Center" SortExpression="Date" />
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
    <script>
        $(function () {
            $('#print').attr('value', Lang.Print);
            $('#print').click(function () {
                var head = Lang.FundReport;
                <%--if ($("#<%=ddlBy.ClientID %>").val() == <%= (int)AccController.PersonBookGroupBy.ByTotal %>) {
                    head = '<%=jpMiniHeaderTotalBills.Text %>';
            }--%>
                printTable('.ScrollPanelCartable', '../../', head);
            });
            $('.GridRow,.GridAltRow').click(function () {
                $(this).toggleClass('selected');
            });
            dateSelect('.dateselect');
            //$('.monetary').keyup(function () {
            //    $(this).val(numberWithCommas(stripCommas($(this).val())));
            //});
            $("#bar").prepend(breadcrumb({
                title: Lang.FundReport,
                parent: {
                    title: Lang.Reports,
                    url: 'menu/reports.aspx'
                }
            }));
            setClassReports();

            });
    </script>
</asp:Content>

