﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Cardex.aspx.cs" Inherits="reports_accounting_Cardex" %>

<%@ Register Src="../../controls/ReportSetting.ascx" TagName="ReportSetting" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>
    <uc1:ReportSetting ID="mReportSetting" runat="server" />
    <div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label3" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.CommodityName</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox runat="server" ID="mServiceName" CssClass="mServiceName form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label1" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="mFromDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false"  DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="mToDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label4" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.ServiceType</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mServiceTypes" runat="server" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label5" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Person</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mPersons" runat="server" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label7" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned padding-0">Lang.PersonnelZoneGroup</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mPZGroup" runat="server" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label6" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.ServiceZoneGroup</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mSZGroup" runat="server" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-lg-9 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcel_Click" Enabled="false" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <input type="button" id="print" class="btn btn-xs btn-dark-blue btn-block" value="" />
                </div>
            </div>
        </div>
    </div>

    <asp:Panel ID="voucherDetailPanel" runat="server" Visible="False">
        <JP:MiniHeader runat="server" Text="Lang.Cardex" ID="mGridTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="mGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true" OnSorting="mGrid_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ServiceName" HeaderText="Lang.Commodity" ItemStyle-HorizontalAlign="Center" SortExpression="ServiceName" />
                        <asp:BoundField DataField="Date" HeaderText="Lang.Date" ItemStyle-HorizontalAlign="Center" SortExpression="Date" />
                        <asp:BoundField DataField="Type" HeaderText="Lang.Type" ItemStyle-HorizontalAlign="Center" SortExpression="Type" />
                        <asp:BoundField DataField="No" HeaderText="Lang.FactorNo" ItemStyle-HorizontalAlign="Center" SortExpression="No" />
                        <asp:BoundField DataField="Income" HeaderText="Lang.NumberOfInputs" ItemStyle-HorizontalAlign="Center" SortExpression="Income" />
                        <asp:BoundField DataField="Outcome" HeaderText="Lang.NumberOfOutputs" ItemStyle-HorizontalAlign="Center" SortExpression="Outcome" />
                        <asp:BoundField DataField="Price" HeaderText="Lang.Price" ItemStyle-HorizontalAlign="Center" SortExpression="Price" />
                        <asp:BoundField DataField="Discount" HeaderText="Lang.Discount" ItemStyle-HorizontalAlign="Center" SortExpression="Discount" />
                        <asp:BoundField DataField="Price" HeaderText="Lang.Sum" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="Price" HeaderText="Lang.Profit" ItemStyle-HorizontalAlign="Center" />
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
    <script>
        $(function () {
            $('#print').attr('value', Lang.Print);
            $('#print').click(function () {
                printTable('.ScrollPanelCartable', '../../', Lang.Cardex);
            });
            $('.GridRow,.GridAltRow').click(function () {
                $(this).toggleClass('selected');
            })
            dateSelect('.dateselect');
            $('.monetary').keyup(function () {
                $(this).val(numberWithCommas(stripCommas($(this).val())));
            });
            //postback, formatItem, formatResult, more, _autourl, _width, moreQS
            var _autourl = '../../get.aspx?op=Auto&table=Service&qd=serviceauto' + appendTime();
            autoCompleteFree('Service', 'serviceauto', ".mServiceName", "#justfurfun", null, null, null, null, _autourl);
            $("#bar").prepend(breadcrumb({
                title: Lang.Cardex,
                parent: {
                    title: Lang.Reports,
                    url: 'menu/reports.aspx'
                }
            }));
            setClassReports();
        });
    </script>
</asp:Content>


