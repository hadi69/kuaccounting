﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reports_accounting_Services : System.Web.UI.Page
{
    System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
    protected void Page_Load(object sender, EventArgs e)
    {
        msg.Visible = false;
        mGrid.EmptyDataText = LangProvider.Instance["EmptyData"];
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");
        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["ServiceReport"];
            Page.SetLang();
            Init();
            IDataReader financeYearStartDate =
            DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");
            if (financeYearStartDate.Read())
                mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            //mFromDate.Text = 
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);
            //mReportSetting.ReportName = "Services";
            BindDBs();
        }
        //mReportSetting.SaveClick += new EventHandler(mReportSetting_SaveClick);
        //mReportSetting.ReportSettingSelect += new EventHandler(mReportSetting_ReportSettingSelect);


        if (int.Parse(ddlBy.SelectedValue) == 0)
        {
            columnCheckBox.Visible = true;
            IncomeOutcomePanel.Visible = false;
            voucherDetailPanel.Visible = true;
            ShowWithoutOutcome.Visible = true;
            AmountWhere.Visible = true;
        }
        else
        {
            columnCheckBox.Visible = false;
            voucherDetailPanel.Visible = false;
            IncomeOutcomePanel.Visible = true;
            ShowWithoutOutcome.Visible = false;
            AmountWhere.Visible = false;
        }
    }
    void BindDBs()
    {
        mDBList.Items.Clear();
        ListItem item = new ListItem(LangProvider.Instance["ActiveYear"] + " ", "");
        if (string.IsNullOrEmpty(SiteSettings.OtherCatalogName))
            item.Selected = true;
        mDBList.Items.Add(item);
        try
        {
            string path = Server.MapPath("~/files/fdb");
            if (System.IO.Directory.Exists(path))
            {
                string[] files = System.IO.Directory.GetFiles(path, "DB_*.mdf");
                for (int i = 0; i < files.Length; i++)
                {
                    string fileName = System.IO.Path.GetFileNameWithoutExtension(files[i]);
                    if (!fileName.ToLower().EndsWith("_log"))
                    {
                        item = new ListItem(fileName + " ", fileName);
                        if (fileName == SiteSettings.OtherCatalogName)
                            item.Selected = true;
                        mDBList.Items.Add(item);
                    }
                }
            }
        }
        catch { }
        panelDBs.Visible = mDBList.Items.Count > 1;
    }
    private List<string> GetDBs()
    {
        List<string> dbs = new List<string>();
        for (int i = 0; i < mDBList.Items.Count; i++)
            if (mDBList.Items[i].Selected)
                dbs.Add(mDBList.Items[i].Value);
        return dbs;
    }
    //void mReportSetting_ReportSettingSelect(object sender, EventArgs e)
    //{
    //    ReportSettingInfo info = mReportSetting.SelectedReportSettings;
    //    if (info == null)
    //        return;
    //    string[] lines = info.Settings.Split('\n');
    //    mServiceName.Text = lines[0].Trim('\r');
    //    mStores.SelectedValue = lines[1].Trim('\r');
    //}

    //void mReportSetting_SaveClick(object sender, EventArgs e)
    //{
    //    StringBuilder b = new StringBuilder();
    //    b.AppendLine(mServiceName.Text); //0
    //    b.AppendLine(mStores.SelectedValue); //1
    //    mReportSetting.Save(b.ToString());
    //}
    void Init()
    {
        IDataReader stores = DbProxy.Instance.ExecuteReader("SELECT ID, Title FROM Stores");
        mStores.Items.Clear();
        mStores.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
        if (stores != null)
        {
            while (stores.Read())
            {
                if (!stores.IsDBNull(0) && !stores.IsDBNull(1))
                    mStores.Items.Add(new ListItem(stores.GetString(1), stores.GetInt32(0).ToString()));
            }
            stores.Close();
        }
        mStores.SelectedIndex = 0;

        IDataReader serviceType = DbProxy.Instance.ExecuteReader("SELECT ID, FullName FROM ServiceTypeView");
        mServiceType.Items.Clear();
        mServiceType.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
        if (serviceType != null)
        {
            while (serviceType.Read())
            {
                if (!serviceType.IsDBNull(0) && !serviceType.IsDBNull(1))
                    mServiceType.Items.Add(new ListItem(serviceType.GetString(1), serviceType.GetInt32(0).ToString()));
            }
            serviceType.Close();
        }
        mServiceType.SelectedIndex = 0;

        IDataReader services = DbProxy.Instance.ExecuteReader("SELECT ServicePriceView.ID, ServiceFullName FROM ServicePriceView");
        mServiceName.Items.Clear();
        mServiceName.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
        if (services != null)
        {
            while (services.Read())
            {
                if(!services.IsDBNull(0) && !services.IsDBNull(1))
                mServiceName.Items.Add(new ListItem(services.GetString(1), services.GetInt32(0).ToString()));
            }
            services.Close();
        }
        mServiceName.SelectedIndex = 0;

        {
            IDataReader personGroupings = DbProxy.Instance.ExecuteReader("SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping  WHERE GroupingType = 1");

            mServiceGroupings.Items.Clear();
            if (personGroupings != null)
            {
                while (personGroupings.Read())
                {
                    if (!personGroupings.IsDBNull(0) && !personGroupings.IsDBNull(1))
                        mServiceGroupings.Items.Add(new ListItem(personGroupings.GetString(1),
                            personGroupings.GetInt32(0).ToString()));
                }
                personGroupings.Close();
            }
        }

        IDataReader personGroup = DbProxy.Instance.ExecuteReader("SELECT ID, FullName FROM ServiceTypeView");
        mServiceGroup.Items.Clear();
        if (personGroup != null)
        {
            while (personGroup.Read())
            {
                if (!personGroup.IsDBNull(0) && !personGroup.IsDBNull(1))
                    mServiceGroup.Items.Add(new ListItem(personGroup.GetString(1), personGroup.GetInt32(0).ToString()));
            }
            personGroup.Close();
        }
    }

    private decimal totalTotalValue = 0;
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView record = e.Row.DataItem as System.Data.DataRowView;
            DateTime dt = Helper.GetDateTime(record["ExpireDate"], Helper.NullDate);
            if (dt != Helper.NullDate)
            {
                e.Row.Cells[3].Text = Helper.FormatDate(Helper.GetDateTime(dt, Helper.NullDate), null);
            }
            decimal buyPrice = SiteSettings.ToMonetary(Helper.GetDecimal(record["BuyPrice"], 0), DateTime.Now);
            decimal salePrice = SiteSettings.ToMonetary(Helper.GetDecimal(record["SalePrice"], 0), DateTime.Now);
            decimal amount = Helper.GetDecimal(record["Available"], 0);
            decimal totalValue = Helper.GetDecimal(record["TotalValue"], 0);

            totalTotalValue += totalValue;

            e.Row.Cells[4].Text = buyPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.Cells[5].Text = (Helper.GetDecimal(record["ProfitPercent"], 0)).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.Cells[6].Text = salePrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.Cells[10].Text = totalValue.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            (e.Row.FindControl("totalTotalValue") as Label).Text = totalTotalValue.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }
    }

    private decimal totalInitRemainedPrice = 0,
        totalBuyPrice = 0,
        totalSalePrice = 0,
        totalReturnPrice = 0,
        totalRemainPrice = 0;

    private float totalInitRemainedNum = 0,
        totalBuyNum = 0,
        totalSaleNum = 0,
        totalReturnNum = 0,
        totalRemainNum = 0;

    protected void IncomeOutcomeGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView record = e.Row.DataItem as System.Data.DataRowView;
            float initRemainedNum = Helper.GetFloat(record["InitRemainedNum"], 0);
            decimal initRemainedWorth = SiteSettings.ToMonetary(Helper.GetDecimal(record["InitRemainedWorth"], 0), DateTime.Now);
            decimal initRemainedNumPrice = SiteSettings.ToMonetary(Helper.GetDecimal(record["InitRemainedPrice"], 0), DateTime.Now);
            totalInitRemainedPrice += initRemainedNumPrice;
            totalInitRemainedNum += initRemainedNum;

            e.Row.Cells[4].Text = initRemainedNum.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.Cells[5].Text = initRemainedWorth.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.Cells[6].Text = initRemainedNumPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            float BuyNum = Helper.GetFloat(record["BuyNum"], 0);
            decimal BuyWorth = SiteSettings.ToMonetary(Helper.GetDecimal(record["BuyWorth"], 0), DateTime.Now);
            decimal BuyNumPrice = SiteSettings.ToMonetary(Helper.GetDecimal(record["BuyPrice"], 0), DateTime.Now);
            totalBuyPrice += BuyNumPrice;
            totalBuyNum += BuyNum;

            e.Row.Cells[7].Text = BuyNum.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.Cells[8].Text = BuyWorth.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.Cells[9].Text = BuyNumPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            float SaleNum = Helper.GetFloat(record["SaleNum"], 0);
            decimal SaleWorth = SiteSettings.ToMonetary(Helper.GetDecimal(record["SaleWorth"], 0), DateTime.Now);
            decimal SaleNumPrice = SiteSettings.ToMonetary(Helper.GetDecimal(record["SalePrice"], 0), DateTime.Now);
            totalSalePrice += SaleNumPrice;
            totalSaleNum += SaleNum;

            e.Row.Cells[10].Text = SaleNum.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.Cells[11].Text = SaleWorth.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.Cells[12].Text = SaleNumPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            float ReturnNum = Helper.GetFloat(record["ReturnNum"], 0);
            decimal ReturnWorth = SiteSettings.ToMonetary(Helper.GetDecimal(record["ReturnWorth"], 0), DateTime.Now);
            decimal ReturnNumPrice = SiteSettings.ToMonetary(Helper.GetDecimal(record["ReturnPrice"], 0), DateTime.Now);
            totalReturnPrice += ReturnNumPrice;
            totalReturnNum += ReturnNum;

            e.Row.Cells[13].Text = ReturnNum.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.Cells[14].Text = ReturnWorth.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.Cells[15].Text = ReturnNumPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            float RemainNum = Helper.GetFloat(record["RemainNum"], 0);
            decimal RemainWorth = SiteSettings.ToMonetary(Helper.GetDecimal(record["RemainWorth"], 0), DateTime.Now);
            decimal RemainNumPrice = SiteSettings.ToMonetary(Helper.GetDecimal(record["RemainPrice"], 0), DateTime.Now);
            totalRemainPrice += RemainNumPrice;
            totalRemainNum += RemainNum;

            e.Row.Cells[16].Text = RemainNum.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.Cells[17].Text = RemainWorth.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.Cells[18].Text = RemainNumPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }
        if (e.Row.RowType == DataControlRowType.Header)
        {
            GridView HeaderGrid = (GridView)sender;
            GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
            TableCell HeaderCell = new TableCell();
            HeaderCell.Text = LangProvider.Instance["Commodity"];
            HeaderCell.ColumnSpan = 4;
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = LangProvider.Instance["ServiceInitRemain"];
            HeaderCell.ColumnSpan = 3;
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = LangProvider.Instance["Buy"];
            HeaderCell.ColumnSpan = 3;
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = LangProvider.Instance["Sell"];
            HeaderCell.ColumnSpan = 3;
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = LangProvider.Instance["ReturnInvoice"];
            HeaderCell.ColumnSpan = 3;
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = LangProvider.Instance["Remained"];
            HeaderCell.ColumnSpan = 3;
            HeaderGridRow.Cells.Add(HeaderCell);

            IncomeOutcomeGrid.Controls[0].Controls.AddAt(0, HeaderGridRow);
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            (e.Row.FindControl("TotalInitRemainedNum") as Label).Text = totalInitRemainedNum.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("TotalInitRemainedPrice") as Label).Text = totalInitRemainedPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("TotalBuyNum") as Label).Text = totalBuyNum.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("TotalBuyPrice") as Label).Text = totalBuyPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("TotalSaleNum") as Label).Text = totalSaleNum.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("TotalSalePrice") as Label).Text = totalSalePrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("TotalReturnNum") as Label).Text = totalReturnNum.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("TotalReturnPrice") as Label).Text = totalReturnPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("TotalRemainNum") as Label).Text = totalRemainNum.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("TotalRemainPrice") as Label).Text = totalRemainPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }
    }
    protected void doShow_Click(object sender, EventArgs e)
    {
        DateTime start = Helper.ParseDate(mFromDate.Text, Null.NullDate);
        DateTime end = Helper.ParseDate(mToDate.Text, Null.NullDate);
        if (Null.IsNull(start))
        {
            msg.Text = LangProvider.Instance["InvalidStartDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(end))
        {
            msg.Text = LangProvider.Instance["InvalidEndDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(start) || Null.IsNull(end))
            return;
        if (Helper.GetInt(ddlBy.SelectedValue, 0) == 0)
        {
            BindGrid();
        }
        else
        {
            BindIncomeOutcomeGrid();
        }
    }
    void BindGrid()
    {
        doExcel.Enabled = true;
        mGrid.Columns[3].Visible = mShowExpireDate.Checked;
        mGrid.Columns[4].Visible = mShowBuyPrice.Checked;
        mGrid.Columns[5].Visible = mShowProfitPercent.Checked;
        mGrid.Columns[6].Visible = mShowSalePric.Checked;
        mGrid.Columns[7].Visible = mShowIncome.Checked;
        mGrid.Columns[8].Visible = mShowOutcome.Checked;
        mGrid.Columns[9].Visible = mShowAvailable.Checked;
        mGrid.Columns[10].Visible = mShowTotalValue.Checked;

        DateTime start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        DateTime end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);

        string pegIDs, peIDs, gWhere;
        GetGroupWhere(out pegIDs, out peIDs, out gWhere);

        float minAmount = Helper.GetFloat(mFromRemain.Text, -1);
        float maxAmount = Helper.GetFloat(mToRemain.Text, -1);
        string amountWhere = "";

        if (minAmount <= 0 && maxAmount >= 0)
        {
            amountWhere = " WHERE Available <= " + maxAmount + " AND Available >= " + minAmount;
        }
        else if (maxAmount >= 0 && minAmount == -1)
        {
            amountWhere = " WHERE Available <= " + maxAmount;
        }
        else if (maxAmount == -1 && minAmount <= 0)
        {
            amountWhere = " WHERE Available >= " + minAmount;
        }

        if (ShowWithoutOutcome.Checked)
        {
            if (amountWhere != "")
            {
                amountWhere += " AND Income > 0 AND Outcome <= 0 ";
            }
            else
            {
                amountWhere = " WHERE Income > 0 AND Outcome <= 0 ";
            }
        }

        /*
         SELECT 
	CASE ServicePrice.ProfitPercent WHEN NULL THEN ServicePrice.TheLPBP ELSE ServicePrice.FreeBase END BuyPrice
	, CASE ServicePrice.ProfitPercent WHEN NULL THEN ServicePrice.TheLPSP ELSE ServicePrice.FreeBase * ServicePrice.ProfitPercent END SalePrice
	, CAST(CASE WHEN Supplier.IsDefault = 1 THEN Service.Title  ELSE Service.Title + ' - ' + Supplier.CompanyName  END AS nvarchar(2000)) AS ServiceName
	, (SELECT SUM(Income - Outcome) FROM StoreServiceDef WHERE ServicePriceID = ServicePrice.ID) Available
	FROM dbo.ServicePrice
  LEFT OUTER JOIN dbo.Supplier ON dbo.ServicePrice.Model=dbo.Supplier.ID
  LEFT OUTER JOIN dbo.Service ON dbo.ServicePrice.ServiceID=dbo.Service.ID*/

        string where = " WHERE 1 = 1 AND [Service].[Enabled] = 1 ", where1 = string.Format(@" WHERE Cardex.[Date] BETWEEN {0} AND {1}", Helper.QoutedDateTime(start), Helper.QoutedDateTime(end));
        int storeID = Helper.GetInt(mStores.SelectedValue, Null.NullInteger);
        if (storeID != Null.NullInteger)
        {
            where += " AND ServicePrice.ID IN (SELECT ServicePriceID FROM StoreServiceDef WHERE StoreID = " + storeID + ")";
            where1 += " AND StoreServiceDef.StoreID = " + storeID;
        }
        int serviceTypeID = Helper.GetInt(mServiceType.SelectedValue, Null.NullInteger);
        if (serviceTypeID != Null.NullInteger)
            where += " AND Service.ServiceTypeID = " + serviceTypeID;
        //int GroupingID1 = Helper.GetInt(mSZGroup.SelectedValue, Null.NullInteger);
        //if (GroupingID1 != Null.NullInteger)
        //    where += " AND Service.ID IN(SELECT OtherID FROM GroupingMember WHERE GroupingID=" + GroupingID1 + " AND Type=0)";

        if ((!string.IsNullOrEmpty(pegIDs) || !string.IsNullOrEmpty(peIDs)) && !string.IsNullOrEmpty(gWhere))
        {
            // ------------------------
            // ids: 1,2,5
            // where: '&1%' AND ('&2%' OR '&5%')
            // it could be better, but i dont know regex well :(
            // ------------------------
            where += "\r\nAND (\r\n";
            if (!string.IsNullOrEmpty(pegIDs))
            {
                // personnel groups: '&1%', '!&1%'
                string[] pGroupingIDs = pegIDs.Split(',');
                for (int i = 0; i < pGroupingIDs.Length; i++)
                {
                    gWhere = gWhere.Replace("'$" + pGroupingIDs[i] + "%'",
                        " ServicePrice.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (SELECT ID FROM [GetAllGroupingChilds](" + pGroupingIDs[i] + "))) \r\n");
                    gWhere = gWhere.Replace("'!$" + pGroupingIDs[i] + "%'",
                        " NOT ServicePrice.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (SELECT ID FROM [GetAllGroupingChilds](" + pGroupingIDs[i] + "))) \r\n");
                }
            }

            if (!string.IsNullOrEmpty(peIDs))
            {
                // personnel groups: '&1%', '!&1%'
                string[] pgids = peIDs.Split(',');
                for (int i = 0; i < pgids.Length; i++)
                {
                    StringBuilder allIds = new StringBuilder();
                    int j = 0;
                    IDataReader childIds =
                        DbProxy.Instance.ExecuteReader("SELECT ID FROM [GetAllServiceTypeChilds](" + pgids[i] + ")");
                    while (childIds.Read())
                    {
                        if (j != 0)
                        {
                            allIds.Append(",");
                        }
                        allIds.Append(childIds.GetValue(0));
                        j++;
                    }
                    gWhere = gWhere.Replace("'&" + pgids[i] + "%'",
                        "ServiceTypeID IN ( " + allIds + ")\r\n");
                    gWhere = gWhere.Replace("'!&" + pgids[i] + "%'",
                        "NOT ServiceTypeID IN( " + allIds + ")\r\n");
                }
            }

            where += gWhere;
            where += "\r\n)\r\n";
        }

        if (Helper.GetInt(mServiceName.SelectedValue, Null.NullInteger) != Null.NullInteger)
        {
            where += " AND ServicePrice.ID = " + mServiceName.SelectedValue;
        }

        string query = string.Format(@"
DECLARE @@SalePriceDecimalRound INT;
    SET @@SalePriceDecimalRound = CONVERT(INT, dbo.GetSetting('SalePriceDecimalRound'), 4);
    IF  @@SalePriceDecimalRound = 0 OR @@SalePriceDecimalRound IS NULL
    SET @@SalePriceDecimalRound = 4
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EDate]') AND type in (N'U'))
	DROP TABLE EDate
Create TABLE EDate(
         ID INT Identity(1, 1),
                ServicePriceID INT,
                Available FLOAT,
                ExpireDate DATE,
                Income FLOAT,
                Outcome FLOAT     
        );
        INSERT INTO EDate
        SELECT ServicePrice.ID, SUM(ISNULL(Cardex.Income, 0) - ISNULL(Cardex.Outcome, 0)) Available
        , StoreServices.[ExpireDate]
        , SUM(ISNULL(Cardex.Income, 0)) AS Income
        , SUM(ISNULL(Cardex.Outcome, 0)) AS Outcome
        FROM Cardex
        LEFT OUTER JOIN StoreServices ON Cardex.StoreServiceID = StoreServices.ID
        LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
        LEFT OUTER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID
        {0}
        GROUP BY StoreServices.[ExpireDate], ServicePrice.ID
        SELECT BuyPrice, SalePrice, ServiceName, Available, (Available * BuyPrice) AS TotalValue, ExpireDate, Code, ISNULL(ProfitPercent, 0) AS ProfitPercent
        , Income, Outcome FROM ( SELECT
	        CASE WHEN ServicePrice.ProfitPercent IS NULL OR ServicePrice.ProfitPercent = 0
	        THEN (ISNULL((SELECT Top 1 Price FROM PurchaseItems 
	              LEFT OUTER JOIN Purchase ON PurchaseItems.PurchaseID = Purchase.ID
	              LEFT OUTER JOIN StoreServices ON PurchaseItems.StoreServiceID = StoreServices.ID
	              LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
	              WHERE  StoreServiceDef.ServicePriceID = ServicePrice.ID AND Purchase.ReceiptDate  BETWEEN {2} AND {3}
                  ORDER BY Purchase.ID DESC), ServicePrice.TheLPBP) )
	        ELSE [dbo].[GetBuyPriceBasedOnPricingMethod](ServicePrice.ID, {2}, {3})
	        END BuyPrice
	        
	        , CASE WHEN ServicePrice.ProfitPercent IS NULL OR ServicePrice.ProfitPercent = 0
	        THEN (ISNULL((SELECT Top 1 SaleWorth FROM PurchaseItems 
	              LEFT OUTER JOIN Purchase ON PurchaseItems.PurchaseID = Purchase.ID
	              LEFT OUTER JOIN StoreServices ON PurchaseItems.StoreServiceID = StoreServices.ID
	              LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
	              WHERE  StoreServiceDef.ServicePriceID = ServicePrice.ID AND Purchase.ReceiptDate  BETWEEN {2} AND {3}
                  ORDER BY Purchase.ID DESC), ServicePrice.TheLPSP))
	        ELSE ROUND(([dbo].[GetBuyPriceBasedOnPricingMethod](ServicePrice.ID, {2}, {3})
             + ([dbo].[GetBuyPriceBasedOnPricingMethod](ServicePrice.ID, {2}, {3}) * (ServicePrice.ProfitPercent/100))), @@SalePriceDecimalRound)
	        END SalePrice
	        , CAST(CASE WHEN Supplier.IsDefault = 1 THEN Service.Title  ELSE Service.Title + ' - ' + Supplier.CompanyName  END AS nvarchar(2000)) AS ServiceName
	        , EDate.Available, EDate.ExpireDate
            , Service.Code, ServicePrice.ProfitPercent
            , EDate.Income, EDate.Outcome
	        FROM dbo.ServicePrice
	            LEFT OUTER JOIN dbo.EDate ON dbo.ServicePrice.ID=dbo.EDate.ServicePriceID
	            LEFT OUTER JOIN dbo.Supplier ON dbo.ServicePrice.Model=dbo.Supplier.ID
	            LEFT OUTER JOIN dbo.Service ON dbo.ServicePrice.ServiceID=dbo.Service.ID	
	            {1} ) SA {4}
", where1, where, Helper.QoutedDateTime(start), Helper.QoutedDateTime(end), amountWhere);
        if (!string.IsNullOrEmpty(SortCol))
            query += " ORDER BY " + SortCol + " " + SortOrder;
        //IDataReader items = DbProxy.Instance.ExecuteReader(query);
        DataTable items = DbProxy.Instance.FillData(query, "all", GetDBs(), string.IsNullOrEmpty(SortCol) ? "" : (SortCol + " " + SortOrder));

        try
        {
            mGrid.DataSource = items;
            mGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Dispose();
            }
        }

        voucherDetailPanel.Visible = true;

    }
    protected void doExcel_Click(object sender, EventArgs e)
    {
        if (voucherDetailPanel.Visible)
            Exporter.ExportExcel(Response, mGridTitle.Text, mGrid);
        if(IncomeOutcomePanel.Visible)
            Exporter.ExportExcel(Response, IncomeOutcomeGridTitle.Text, IncomeOutcomeGrid);
    }

    protected void mGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        BindGrid();
    }
    // ------------------------
    // ids: 1,2,5
    // where: '&1%' AND ('&2%' OR '&5%')
    // ------------------------
    private void GetGroupWhere(out string pgIDs, out string pIDs, out string where)
    {
        pIDs = "";
        pgIDs = "";
        where = "";

        // grouping members : '&1%', '!&1%'
        // category = 1375 AND ( category != 1076 OR category = 1075 ) 
        // --> '&1375%' AND ('!&1076%' OR '&1075%')
        where = mQueryBuilderSql.Value.Trim();
        where += " "; // we need this space at end to find IDs


        while (where.Contains("PGCategory = "))
        {
            int index = where.IndexOf("PGCategory = ") + "PGCategory = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pgIDs += id + ",";
            where = where.Replace("PGCategory = " + id + " ", "'$" + id + "%' ");
        }
        while (where.Contains("PGCategory != "))
        {
            int index = where.IndexOf("PGCategory != ") + "PGCategory != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pgIDs += id + ",";
            where = where.Replace("PGCategory != " + id + " ", "'!$" + id + "%' ");
        }
        if (pgIDs.EndsWith(","))
            pgIDs = pgIDs.Substring(0, pgIDs.Length - ",".Length);


        while (where.Contains("PCategory = "))
        {
            int index = where.IndexOf("PCategory = ") + "PCategory = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pIDs += id + ",";
            where = where.Replace("PCategory = " + id + " ", "'&" + id + "%' ");
        }
        while (where.Contains("PCategory != "))
        {
            int index = where.IndexOf("PCategory != ") + "PCategory != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pIDs += id + ",";
            where = where.Replace("PCategory != " + id + " ", "'!&" + id + "%' ");
        }
        if (pIDs.EndsWith(","))
            pIDs = pIDs.Substring(0, pIDs.Length - ",".Length);
    }
    protected string SortCol
    {
        get
        {
            return Helper.GetString(ViewState["SortCol"], null);
        }
        set
        {
            ViewState["SortCol"] = value;
        }
    }
    protected string SortOrder
    {
        get
        {
            return Helper.GetString(ViewState["SortOrder"], "asc");
        }
        set
        {
            ViewState["SortOrder"] = value;
        }
    }
    void BindIncomeOutcomeGrid()
    {
        doExcel.Enabled = true;

        DateTime start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        DateTime end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);

        int storeID = Helper.GetInt(mStores.SelectedValue, Null.NullInteger);
        int serviceTypeID = Helper.GetInt(mServiceType.SelectedValue, Null.NullInteger);
        //int GroupingID = Helper.GetInt(mSZGroup.SelectedValue, Null.NullInteger);
        int ServicePriceID = Helper.GetInt(mServiceName.SelectedValue, Null.NullInteger);

        string pegIDs, peIDs, gWhere;
        GetGroupWhere(out pegIDs, out peIDs, out gWhere);
        string where = " WHERE 1 = 1 ";
        if ((!string.IsNullOrEmpty(pegIDs) || !string.IsNullOrEmpty(peIDs)) && !string.IsNullOrEmpty(gWhere))
        {
            // ------------------------
            // ids: 1,2,5
            // where: '&1%' AND ('&2%' OR '&5%')
            // it could be better, but i dont know regex well :(
            // ------------------------
            where += "\r\nAND (\r\n";
            if (!string.IsNullOrEmpty(pegIDs))
            {
                // personnel groups: '&1%', '!&1%'
                string[] pGroupingIDs = pegIDs.Split(',');
                for (int i = 0; i < pGroupingIDs.Length; i++)
                {
                    gWhere = gWhere.Replace("'$" + pGroupingIDs[i] + "%'",
                        " ServicePriceID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (SELECT ID FROM [GetAllGroupingChilds](" + pGroupingIDs[i] + "))) \r\n");
                    gWhere = gWhere.Replace("'!$" + pGroupingIDs[i] + "%'",
                        " NOT ServicePriceID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (SELECT ID FROM [GetAllGroupingChilds](" + pGroupingIDs[i] + "))) \r\n");
                }
            }

            if (!string.IsNullOrEmpty(peIDs))
            {
                // personnel groups: '&1%', '!&1%'
                string[] pgids = peIDs.Split(',');
                for (int i = 0; i < pgids.Length; i++)
                {
                    StringBuilder allIds = new StringBuilder();
                    int j = 0;
                    IDataReader childIds =
                        DbProxy.Instance.ExecuteReader("SELECT ID FROM [GetAllServiceTypeChilds](" + pgids[i] + ")");
                    while (childIds.Read())
                    {
                        if (j != 0)
                        {
                            allIds.Append(",");
                        }
                        allIds.Append(childIds.GetValue(0));
                        j++;
                    }
                    gWhere = gWhere.Replace("'&" + pgids[i] + "%'",
                        "ServiceTypeID IN ( " + allIds + ")\r\n");
                    gWhere = gWhere.Replace("'!&" + pgids[i] + "%'",
                        "NOT ServiceTypeID IN( " + allIds + ")\r\n");
                }
            }

            where += gWhere;
            where += "\r\n)\r\n";
        }


        string query = string.Format(@"SELECT * FROM [dbo].[IncomeOutcomeReportFunc]({0}, {1}, {2}, {3}, {5}) {4}"
            , Helper.QoutedDateTime(start), Helper.QoutedDateTime(end), storeID, ServicePriceID, where, serviceTypeID);
        //if (!string.IsNullOrEmpty(SortCol))
          //  query += " ORDER BY " + SortCol + " " + SortOrder;
        //IDataReader items = DbProxy.Instance.ExecuteReader(query);
        DataTable items = DbProxy.Instance.FillData(query, "all", GetDBs(), "");

        try
        {
            IncomeOutcomeGrid.DataSource = items;
            IncomeOutcomeGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Dispose();
            }
        }
    }
}