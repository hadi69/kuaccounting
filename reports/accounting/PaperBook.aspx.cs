﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

public partial class reports_accounting_PaperBook : System.Web.UI.Page
{
    System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
    protected void Page_Load(object sender, EventArgs e)
    {
        msg.Visible = false;
        lse1.PromptText = LangProvider.Instance["Search"];
        mGrid.EmptyDataText = LangProvider.Instance["DocumentIsEmpty"];
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");
        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["PaperBook"];
            Page.SetLang();
            int year = cal.GetYear(DateTime.Now);
            //mFromDate.Text = string.Format("{0}/01/01", year - 1300);
            //mToDate.Text = Helper.FormatDate(cal.ToDateTime(year + 1, 1, 1, 1, 1, 1, 1).AddDays(-1), "yy/MM/dd");
            IDataReader financeYearStartDate = DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");//0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            //mFromDate.Text = 
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);
            BindTotals();
            BindDBs();

            //mReportSetting.ReportName = "PaperBook";
        }
        //mReportSetting.SaveClick += new EventHandler(mReportSetting_SaveClick);
        //mReportSetting.ReportSettingSelect += new EventHandler(mReportSetting_ReportSettingSelect);
    }
    void BindDBs()
    {
        mDBList.Items.Clear();
        ListItem item = new ListItem(LangProvider.Instance["ActiveYear"] + " ", "");
        if (string.IsNullOrEmpty(SiteSettings.OtherCatalogName))
            item.Selected = true;
        mDBList.Items.Add(item);
        try
        {
            string path = Server.MapPath("~/files/fdb");
            if (System.IO.Directory.Exists(path))
            {
                string[] files = System.IO.Directory.GetFiles(path, "DB_*.mdf");
                for (int i = 0; i < files.Length; i++)
                {
                    string fileName = System.IO.Path.GetFileNameWithoutExtension(files[i]);
                    if (!fileName.ToLower().EndsWith("_log"))
                    {
                        item = new ListItem(fileName + " ", fileName);
                        if (fileName == SiteSettings.OtherCatalogName)
                            item.Selected = true;
                        mDBList.Items.Add(item);
                    }
                }
            }
        }
        catch { }
        panelDBs.Visible = mDBList.Items.Count > 1;
    }

    //void mReportSetting_ReportSettingSelect(object sender, EventArgs e)
    //{
    //    ReportSettingInfo info = mReportSetting.SelectedReportSettings;
    //    if (info == null)
    //        return;
    //    string[] lines = info.Settings.Split('\n');
    //    mTotals.SelectedValue = lines[0].Trim('\r');
    //    mFromDate.Text = lines[1].Trim('\r');
    //    mToDate.Text = lines[2].Trim('\r');
    //    mFromVoucherID.Text = lines[3].Trim('\r');
    //    mToVoucherID.Text = lines[4].Trim('\r');
    //}

    //void mReportSetting_SaveClick(object sender, EventArgs e)
    //{
    //    StringBuilder b = new StringBuilder();
    //    b.AppendLine(mTotals.SelectedValue); //0
    //    b.AppendLine(mFromDate.Text); //1
    //    b.AppendLine(mToDate.Text); //2
    //    b.AppendLine(mFromVoucherID.Text);  //3
    //    b.AppendLine(mToVoucherID.Text);    //4
    //    mReportSetting.Save(b.ToString());
    //}
    void BindTotals()
    {
        List<TopicInfo> totals = AccController.GetTopics(TopicLevel.Total, SiteSettings.ProgramID);
        if (totals == null)
            totals = new List<TopicInfo>();
        totals.Sort();
        TopicInfo all = new TopicInfo();
        all.ID = -1;
        all.Title = LangProvider.Instance["AllTopics"];
        totals.Insert(0, all);
        mTotals.DataValueField = "ID";
        mTotals.DataTextField = "CodeName";
        mTotals.DataSource = totals;
        mTotals.DataBind();

        mTotals.SelectedIndex = 0;
    }
    private List<string> GetDBs()
    {
        List<string> dbs = new List<string>();
        for (int i = 0; i < mDBList.Items.Count; i++)
            if (mDBList.Items[i].Selected)
                dbs.Add(mDBList.Items[i].Value);
        return dbs;
    }
    string rowStyle = "GridRow";
    decimal _due = 0, _owe = 0;
    decimal sumOwe = 0, sumDue = 0, sumUnit = 0;
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView record = e.Row.DataItem as DataRowView;
            DateTime dt = Helper.GetDateTime(record["Date"], Helper.NullDate);
            if (dt != Helper.NullDate)
            {
                if (SiteSettings.DateType == DateType.en || SiteSettings.DateType == DateType.ar)
                {
                    string date = Helper.FormatDate(Helper.GetDateTime(record["Date"], Helper.NullDate), "dd/MM/yyyy");
                    int index = date.IndexOf("/");
                    string day = date.Substring(0, index);
                    date = date.Substring(index + 1, date.Length - (index + 1));
                    index = date.IndexOf("/");
                    string month = date.Substring(0, index);

                    e.Row.Cells[2].Text = month;
                    e.Row.Cells[1].Text = day;
                }
                else
                {
                    e.Row.Cells[2].Text = cal.GetMonth(dt).ToString();
                    e.Row.Cells[1].Text = cal.GetDayOfMonth(dt).ToString();
                }
            }
            //if (!string.IsNullOrEmpty(Helper.GetString()))
                rowStyle = rowStyle == "GridAltRow" ? "GridRow" : "GridAltRow";
            e.Row.CssClass = rowStyle;

            _due = SiteSettings.ToMonetary(Helper.GetDecimal(record["Due"], 0), dt);
            _owe = SiteSettings.ToMonetary(Helper.GetDecimal(record["Owe"], 0), dt);

            int otherID = Helper.GetInt(record["OtherID"], Null.NullInteger), mode = Helper.GetInt(record["Mode"], Null.NullInteger);
            if (otherID == Null.NullInteger)
                e.Row.Cells[3].Text = string.Format("<a href='../../list.aspx?table=DocumentAccounting&id={0}&itemid={1}'>{2}</a>", record["ID"], record["ItemID"], record["No"]);
            else
                e.Row.Cells[4].Text = string.Format("<a href='../../list.aspx?table=DocumentAccounting&id={0}&itemid={1}' class='no' mode='{3}' otherid='{4}'>{2}</a>", record["ID"], record["ItemID"], record["No"], mode, otherID);

            Label owe = (Label)e.Row.FindControl("owe");
            owe.Text = _owe.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            Label due = (Label)e.Row.FindControl("due");
            due.Text = _due.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            //Label unit = (Label)e.Row.FindControl("unit");
            //unit.Text = Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["Unit"], 0).ToString("N0");

            sumDue += _due;
            sumOwe += _owe;
            //sumUnit += Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["Unit"], 0);
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblTotal = (Label)e.Row.FindControl("oweTotal");
            lblTotal.Text = sumOwe.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            lblTotal = (Label)e.Row.FindControl("dueTotal");
            lblTotal.Text = sumDue.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            //lblTotal = (Label)e.Row.FindControl("unitTotal");
            //lblTotal.Text = sumUnit.ToString("N0");
        }
    }
    protected void mGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        BindGrid();
    }
    protected void BindGrid()
    {
        sumOwe = sumDue = sumUnit = 0;
        DateTime start = Helper.ParseDate(mFromDate.Text, Null.NullDate);
        DateTime end = Helper.ParseDate(mToDate.Text, Null.NullDate);
        if (Null.IsNull(start))
        {
            msg.Text = LangProvider.Instance["InvalidStartDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(end))
        {
            msg.Text = LangProvider.Instance["InvalidEndDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(start) || Null.IsNull(end))
            return;
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);
        int userID = Null.NullInteger;
        if (!SiteSettings.User.HasAccess("paperbook_all"))
            userID = SiteSettings.UserID;
        DataTable items = AccController.GetPaperBook(Helper.GetInt(mTotals.SelectedValue, Null.NullInteger)
            , start
            , end
            , Helper.GetInt(mFromVoucherID.Text, 0), Helper.GetInt(mToVoucherID.Text, int.MaxValue - 1)
            , userID, Null.NullInteger, Null.NullInteger, SiteSettings.ProgramID, false, false
            , Helper.GetDecimal(mAmount.Text, -1), SortCol, SortOrder, GetDBs());

        try
        {
            mGrid.DataSource = items;
            mGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Dispose();
            }
        }
    }
    protected string SortCol
    {
        get
        {
            return Helper.GetString(ViewState["SortCol"], null);
        }
        set
        {
            ViewState["SortCol"] = value;
        }
    }
    protected string SortOrder
    {
        get
        {
            return Helper.GetString(ViewState["SortOrder"], "asc");
        }
        set
        {
            ViewState["SortOrder"] = value;
        }
    }
    protected void doShow_Click(object sender, EventArgs e)
    {
        BindGrid();

        voucherDetailPanel.Visible = true;

        doExcel.Enabled = true;
        //mSumDue.Text = sumDue.ToString("N0");
        //mSumOwe.Text = sumOwe.ToString("N0");
        //mSumUnit.Text = sumUnit.ToString("N0");
    }
    protected void doExcel_Click(object sender, EventArgs e)
    {
        Exporter.ExportExcel(Response, mTitle.Text, mGrid);
    }
}