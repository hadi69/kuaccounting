﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

public partial class reports_accounting_FunctionalBalance : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["MonthlyOperationsBalance"];
            Page.SetLang();
            this.Level = Helper.GetInt(Request.QueryString["level"], 0);
            panelCurLevels.Visible = this.Level > 0;
            mTotals.AutoPostBack = this.Level > 0;
            if (this.Level == 0)
                mTitle.Text = LangProvider.Instance["MonthlyOperationsBalanceL1"];
            else if (this.Level == 1)
            {
                mTitle.Text = LangProvider.Instance["MonthlyOperationsBalanceL2"];
                curLevelLbl.Text = LangProvider.Instance["MonthlyOperationsBalanceL3"];
            }
            else if (this.Level > 1)
            {
                mTitle.Text = LangProvider.Instance["MonthlyOperationsBalanceL3"] + (this.Level - 1).ToString();
                curLevelLbl.Text = LangProvider.Instance["Portion"]  + (this.Level - 1).ToString();
            }
            mReportSetting.ReportName = "FineList" + this.Level;

            BindTotals();
            BindWards();
            BindTopics();

            System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
            mYear.Text = cal.GetYear(DateTime.Now).ToString();
            //mMonth.se = cal.GetMonth(DateTime.Now).ToString();

            if (Session["fb_settings"] != null)
                SetSettings(Session["fb_settings"].ToString());
        }
        mReportSetting.SaveClick += new EventHandler(mReportSetting_SaveClick);
        mReportSetting.ReportSettingSelect += new EventHandler(mReportSetting_ReportSettingSelect);
    }

    void mReportSetting_ReportSettingSelect(object sender, EventArgs e)
    {
        ReportSettingInfo info = mReportSetting.SelectedReportSettings;
        if (info == null)
            return;
        SetSettings(info.Settings);
    }

    void mReportSetting_SaveClick(object sender, EventArgs e)
    {
        mReportSetting.Save(GetSettings());
    }
    string GetSettings()
    {
        StringBuilder b = new StringBuilder();
        b.AppendLine(mYear.Text); //0
        b.AppendLine(mMonth.SelectedValue); //1
        b.AppendLine(mFromVoucherID.Text);  //2
        b.AppendLine(mToVoucherID.Text);    //3
        b.AppendLine(mTotals.SelectedValue); //4
        b.Append(mWards.SelectedValue);  //5
        return b.ToString();
    }
    void SetSettings(string settings)
    {
        if (settings == null)
            return;
        string[] lines = settings.Split('\n');
        if (lines.Length > 5)
            try
            {
                mYear.Text = lines[0].Trim('\r');
                mMonth.SelectedValue = lines[1].Trim('\r');
                mFromVoucherID.Text = lines[2].Trim('\r');
                mToVoucherID.Text = lines[3].Trim('\r');
                mTotals.SelectedValue = lines[4].Trim('\r');
                mWards.SelectedValue = lines[5].Trim('\r');
            }
            catch { }
    }
    #region Bindings
    void BindTotals()
    {
        List<TopicInfo> totals = AccController.GetTopics(TopicLevel.Total, SiteSettings.ProgramID);
        if (totals == null)
            totals = new List<TopicInfo>();
        totals.Sort();
        TopicInfo all = new TopicInfo();
        all.ID = Null.NullInteger;
        all.Title = LangProvider.Instance["AllTopics"];
        totals.Insert(0, all);
        mTotals.DataValueField = "ID";
        mTotals.DataTextField = "CodeName";
        mTotals.DataSource = totals;
        mTotals.DataBind();

        mTotals.SelectedIndex = 0;
    }
    void BindWards()
    {
        mWards.DataTextField = "Name";
        mWards.DataValueField = "ID";

        mWards.DataSource = DbProxy.Instance.FillData("SELECT -1 AS ID, '' AS Name UNION SELECT ID, Name + '(' + ISNULL(Code, '') + ')' FROM Ward", "ward"); ;
        mWards.DataBind();

        mWards.SelectedIndex = 0;
    }
    void BindTopics()
    {
        int totalID = Helper.GetInt(mTotals.SelectedValue, Null.NullInteger);
        string code = "";
        if (totalID != Null.NullInteger)
        {
            TopicInfo total = AccController.GetTopic(totalID);
            if (total != null)
                code = total.Code;
        }
        List<TopicInfo> subs = AccController.GetTopicsByCode(code, this.Level);
        if (subs == null)
            subs = new List<TopicInfo>();
        subs.Sort();
        TopicInfo all = new TopicInfo();
        all.ID = -1;
        all.Title = LangProvider.Instance["AllTopics"];
        subs.Insert(0, all);
        mCurLevels.DataValueField = "ID";
        mCurLevels.DataTextField = "CodeName";
        mCurLevels.DataSource = subs;
        mCurLevels.DataBind();

        mCurLevels.SelectedIndex = 0;
    }
    #endregion
    string rowStyle = "GridRow";
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            rowStyle = rowStyle == "GridAltRow" ? "GridRow" : "GridAltRow";
            e.Row.CssClass = rowStyle;

            e.Row.Cells[0].Text = (e.Row.RowIndex + 1).ToString();
            sumDue += Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["Due"], 0);
            sumOwe += Helper.GetDecimal((e.Row.DataItem as System.Data.Common.DbDataRecord)["Owe"], 0);
            remaining = sumDue - sumOwe;
            e.Row.Cells[5].Text = remaining.ToString("N0");
        }

    }

    decimal sumOwe = 0, sumDue = 0, remaining = 0;
    protected void doShow_Click(object sender, EventArgs e)
    {
        Session["fb_settings"] = GetSettings();
        sumOwe = sumDue = remaining = 0;
        int year = Helper.GetInt(mYear.Text, -1);
        int month = Helper.GetInt(mMonth.SelectedValue, -1);
        if (year == -1 || month == -1)
            return;
        if (month == 0)
            BindAllMonthes(year);
        else
            BindSingleMonth(year, month);
    }
    void BindSingleMonth(int year, int month)
    {
        sumOwe = sumDue = remaining = 0;
        IDataReader items = AccController.GetFunctionalBalance(
            this.Level
            , Helper.GetInt(mTotals.SelectedValue, Null.NullInteger)
            , Helper.GetInt(mCurLevels.SelectedValue, Null.NullInteger)
            , year, month
            , Helper.GetInt(mFromVoucherID.Text, 0), Helper.GetInt(mToVoucherID.Text, int.MaxValue - 1)
            , Helper.GetInt(mWards.SelectedValue, Null.NullInteger), mFilterZero.Checked, SiteSettings.ProgramID);

        mPC.Controls.Clear();
        mPC.Controls.Add(new LiteralControl("<div>"));
        mPC.Controls.Add(new LiteralControl("<table class='ScrollPanelCartable table table-bordered table-condensed zebra'>"));
        // --------------------------------------------------------
        // First Row
        mPC.Controls.Add(new LiteralControl("<tr class='GridHeader'>"));
        mPC.Controls.Add(new LiteralControl(string.Format("<th class='myFrozenTop'>{0}</th>", LangProvider.Instance["ColRowNr"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<th class='myFrozenTop'>{0}</th>", LangProvider.Instance["Topic"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<th class='myFrozenTop'>{0}</th>", LangProvider.Instance["TopicCode"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<th class='myFrozenTop' colspan=2>{1}{0}</th>", month - 1, LangProvider.Instance["TheEndOfTheMonthOperations"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<th colspan=2 class='myFrozenTop'>{0}</th>", LangProvider.Instance["OperationsThisMonth"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<th colspan=2 class='myFrozenTop'>{0}</th>", LangProvider.Instance["TheTotalOperationByTheEndOfThisMonth"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<th class='myFrozenTop' colspan=2>{1}{0}</th>", month - 1, LangProvider.Instance["RemainedOperationUntilTheEndOfMonth"])));
        mPC.Controls.Add(new LiteralControl("</tr>"));
        // --------------------------------------------------------
        // Second Row
        mPC.Controls.Add(new LiteralControl("<tr class='GridHeader'>"));
        mPC.Controls.Add(new LiteralControl("<td class='myFrozenTop'>&nbsp;</td>"));
        mPC.Controls.Add(new LiteralControl("<td class='myFrozenTop'>&nbsp;</td>"));
        mPC.Controls.Add(new LiteralControl("<td class='myFrozenTop'>&nbsp;</td>"));
        mPC.Controls.Add(new LiteralControl(string.Format("<td class='myFrozenTop'><asp:Label ID='Label1' runat='server'>{0}</asp:Label></td>", LangProvider.Instance["Liability"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<td class='myFrozenTop'>{0}</td>", LangProvider.Instance["Credit"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<td class='myFrozenTop'>{0}</td>", LangProvider.Instance["Liability"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<td class='myFrozenTop'>{0}</td>", LangProvider.Instance["Credit"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<td class='myFrozenTop'>{0}</td>", LangProvider.Instance["Liability"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<td class='myFrozenTop'>{0}</td>", LangProvider.Instance["Credit"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<td class='myFrozenTop'>{0}</td>", LangProvider.Instance["Liability"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<td class='myFrozenTop'>{0}</td>", LangProvider.Instance["Credit"])));
        mPC.Controls.Add(new LiteralControl("</tr>"));
        // --------------------------------------------------------

        int k = 1;
        decimal[] sums = { 0, 0, 0, 0, 0, 0, 0, 0 };
        while (items.Read())
        {
            mPC.Controls.Add(new LiteralControl(string.Format("<tr class='{0}'>", (k % 2) == 0 ? "GridRow" : "GridAltRow")));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", k++)));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", items.GetString(1))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", items.GetString(2))));

            decimal owe1 = SiteSettings.ToMonetary(items.GetDecimal(3), DateTime.Now), due1 = SiteSettings.ToMonetary(items.GetDecimal(4), DateTime.Now),
                owe2 = SiteSettings.ToMonetary(items.GetDecimal(5), DateTime.Now), due2 = SiteSettings.ToMonetary(items.GetDecimal(6), DateTime.Now);
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", owe1.ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", due1.ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", owe2.ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", due2.ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", (owe2 + owe1).ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", (due2 + due1).ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", Math.Max(0, owe1 - due1).ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", Math.Max(0, due1 - owe1).ToString("N0"))));

            mPC.Controls.Add(new LiteralControl("</tr>"));
            sums[0] += owe1;
            sums[1] += due1;
            sums[2] += owe2;
            sums[3] += due2;
            sums[4] += owe2 + owe1;
            sums[5] += due2 + due1;
            sums[6] += Math.Max(0, owe1 - due1);
            sums[7] += Math.Max(0, due1 - owe1);
        }
        items.Close();
        items.Dispose();

        // ----------------------------------
        // Sums
        mPC.Controls.Add(new LiteralControl("<tr class='GridFooter'>"));
        mPC.Controls.Add(new LiteralControl("<td></td>"));
        mPC.Controls.Add(new LiteralControl("<td></td>"));
        mPC.Controls.Add(new LiteralControl("<td></td>"));
        for (int i = 0; i < sums.Length; i++)
        {
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", sums[i].ToString("N0"))));
        }
        mPC.Controls.Add(new LiteralControl("</tr>"));
        mPC.Controls.Add(new LiteralControl("</table></div>"));

        //mSumDue.Text = sumDue.ToString("N0");
        //mSumOwe.Text = sumOwe.ToString("N0");
        //mRemaining.Text = remaining.ToString("N0");
    }
    void BindAllMonthes(int year)
    {
        Dictionary<string, string> codeTitles = new Dictionary<string, string>();
        Dictionary<string, decimal[]> codeRemains = new Dictionary<string, decimal[]>();
        Dictionary<string, int> codeNums = new Dictionary<string, int>();
        decimal[] sums = new decimal[12];
        for (int month = 1; month <= 12; month++)
        {
            IDataReader items = AccController.GetFunctionalBalance(
                this.Level
                , Helper.GetInt(mTotals.SelectedValue, Null.NullInteger)
                , Helper.GetInt(mCurLevels.SelectedValue, Null.NullInteger)
                , year, month
                , Helper.GetInt(mFromVoucherID.Text, 0), Helper.GetInt(mToVoucherID.Text, int.MaxValue - 1)
                , Helper.GetInt(mWards.SelectedValue, Null.NullInteger), mFilterZero.Checked, SiteSettings.ProgramID);

            while (items.Read())
            {
                string code = items.GetString(2);
                string title = items.GetString(1);

                decimal owe1 = items.GetDecimal(5), due1 = items.GetDecimal(6);
                decimal remain = owe1 - due1;
                if (!codeTitles.ContainsKey(code))
                {
                    codeTitles.Add(code, title);
                    codeRemains.Add(code, new decimal[12]);
                    codeNums.Add(code, 0);
                }
                codeRemains[code][month - 1] += remain;
                codeNums[code]  += items.GetInt32(8);
                sums[month - 1] += remain;
            }
            items.Close();
            items.Dispose();
        }
        

        mPC.Controls.Clear();
        mPC.Controls.Add(new LiteralControl("<div>"));
        mPC.Controls.Add(new LiteralControl("<table class='ScrollPanelCartable table table-bordered table-condensed zebra'>"));
        // --------------------------------------------------------
        // First Row
        mPC.Controls.Add(new LiteralControl("<tr class='GridHeader'>"));
        mPC.Controls.Add(new LiteralControl(string.Format("<th class='myFrozenTop'>{0}</th>", LangProvider.Instance["ColRowNr"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<th class='myFrozenTop'>{0}</th>", LangProvider.Instance["TopicCode"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<th class='myFrozenTop'>{0}</th>", LangProvider.Instance["Topic"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<th class='myFrozenTop'>{0}</th>", LangProvider.Instance["Farvardin"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<th class='myFrozenTop'>{0}</th>", LangProvider.Instance["Ordibehesht"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<th class='myFrozenTop'>{0}</th>", LangProvider.Instance["Khordad"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<th class='myFrozenTop'>{0}</th>", LangProvider.Instance["Tir"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<th class='myFrozenTop'>{0}</th>", LangProvider.Instance["Mordad"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<th class='myFrozenTop'>{0}</th>", LangProvider.Instance["Shahrivar"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<th class='myFrozenTop'>{0}</th>", LangProvider.Instance["Mehr"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<th class='myFrozenTop'>{0}</th>", LangProvider.Instance["Aban"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<th class='myFrozenTop'>{0}</th>", LangProvider.Instance["Azar"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<th class='myFrozenTop'>{0}</th>", LangProvider.Instance["Dey"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<th class='myFrozenTop'>{0}</th>", LangProvider.Instance["Bahman"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<th class='myFrozenTop'>{0}</th>", LangProvider.Instance["Esfand"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<th class='myFrozenTop'>{0}</th>", LangProvider.Instance["Sum"])));
        mPC.Controls.Add(new LiteralControl(string.Format("<th class='myFrozenTop'>{0}</th>", LangProvider.Instance["NumItems"])));
        mPC.Controls.Add(new LiteralControl("</tr>"));

        Dictionary<string, string>.Enumerator enu = codeTitles.GetEnumerator();
        int k = 1;
        while (enu.MoveNext())
        {
            mPC.Controls.Add(new LiteralControl("<tr>"));
            decimal sumAll = codeRemains[enu.Current.Key].Sum();
            mPC.Controls.Add(new LiteralControl(string.Format("<tr class='{0}'>", (k % 2) == 0 ? "GridRow" : "GridAltRow")));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", k++)));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", enu.Current.Key)));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", enu.Current.Value)));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", codeRemains[enu.Current.Key][0].ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", codeRemains[enu.Current.Key][1].ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", codeRemains[enu.Current.Key][2].ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", codeRemains[enu.Current.Key][3].ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", codeRemains[enu.Current.Key][4].ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", codeRemains[enu.Current.Key][5].ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", codeRemains[enu.Current.Key][6].ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", codeRemains[enu.Current.Key][7].ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", codeRemains[enu.Current.Key][8].ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", codeRemains[enu.Current.Key][9].ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", codeRemains[enu.Current.Key][10].ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", codeRemains[enu.Current.Key][11].ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", sumAll.ToString("N0"))));
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", codeNums[enu.Current.Key].ToString("N0"))));
            mPC.Controls.Add(new LiteralControl("</tr>"));
        }
        // ----------------------------------
        // Sums
        mPC.Controls.Add(new LiteralControl("<tr class='GridFooter'>"));
        mPC.Controls.Add(new LiteralControl("<td></td>"));
        mPC.Controls.Add(new LiteralControl("<td></td>"));
        mPC.Controls.Add(new LiteralControl("<td></td>"));
        for (int i = 0; i < sums.Length; i++)
        {
            mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", sums[i].ToString("N0"))));
        }
        mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", sums.Sum().ToString("N0"))));
        mPC.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", codeNums.Sum(a => a.Value).ToString("N0"))));
        mPC.Controls.Add(new LiteralControl("</tr>"));
        mPC.Controls.Add(new LiteralControl("</table></div>"));

        //mSumDue.Text = sumDue.ToString("N0");
        //mSumOwe.Text = sumOwe.ToString("N0");
        //mRemaining.Text = remaining.ToString("N0");
    }
    protected int Level
    {
        get
        {
            if (null == ViewState["Level"])
                return 1;
            return (int)ViewState["Level"];
        }
        set
        {
            ViewState["Level"] = value;
        }
    }
    protected void mTotals_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindTopics();
    }

    protected void doExcel_Click(object sender, EventArgs e)
    {
        doShow_Click(null, null);
        string text = "";
        for (int i = 0; i < mPC.Controls.Count; i++)
            if (mPC.Controls[i] is LiteralControl)
                text += (mPC.Controls[i] as LiteralControl).Text;
        Exporter.ExportExcelSimple(Response, mTitle.Text, text);
    }
}