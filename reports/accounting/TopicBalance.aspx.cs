﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reports_accounting_TopicBalance : System.Web.UI.Page
{
    System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
    protected void Page_Load(object sender, EventArgs e)
    {
        msg.Visible = false;
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");
        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["TopicBalance"];
            Page.SetLang();
            int year = cal.GetYear(DateTime.Now);
            IDataReader financeYearStartDate = DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");//0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);
            Init();
            BindTopics();
        }
    }
    void Init()
    {
        IDataReader persons = DbProxy.Instance.ExecuteReader("SELECT ID, [dbo].[DisplayPerson](ID, 1), ISNULL(Code, '') FROM Personnel WHERE Enabled = 1 AND Type=101");
        mPersons.Items.Clear();
        mPersons.Items.Add(new ListItem(LangProvider.Instance["AllPersons"], ""));
        if (persons != null)
        {
            while (persons.Read())
            {
                if (!persons.IsDBNull(0) && !persons.IsDBNull(1) && !persons.IsDBNull(2))
                    mPersons.Items.Add(new ListItem(persons.GetString(1) + " - کد: " + persons.GetString(2),
                        persons.GetInt32(0).ToString()));
            }
            persons.Close();
        }
        mPersons.SelectedIndex = 0;

        IDataReader projects = DbProxy.Instance.ExecuteReader("SELECT ID, Name  FROM Ward WHERE SectionType=8");
        mProjects.Items.Clear();
        if (projects != null)
        {
            while (projects.Read())
                mProjects.Items.Add(new ListItem(projects.GetString(1), projects.GetInt32(0).ToString()));
            projects.Close();
        }

        IDataReader equipments = DbProxy.Instance.ExecuteReader("SELECT ID, FullName, Code, Title FROM EquipmentView");
        mEquipments.Items.Clear();
        if (projects != null)
        {
            while (equipments.Read())
                mEquipments.Items.Add(new ListItem(equipments.GetString(1) + " - " + equipments.GetString(2), equipments.GetInt32(0).ToString()));
            equipments.Close();
        }
    }

    private void BindTopics()
    {
        mTree.Nodes.Clear();
        TreeNode rootNode = new TreeNode("MasterAccounts".Translate(), "");
        List<TreeInfo> roots = AccController.GetTrees(0, SiteSettings.ProgramID);
        roots.Sort();
        for (int i = 0; i < roots.Count; i++)
        {
            //rootNode.ChildNodes.Add(GetTreeNode(roots[i]));
            TreeNode node = GetTreeNode(roots[i]);
            if (node != null)
                rootNode.ChildNodes.Add(node);
            else
                rootNode.ChildNodes.Add(new TreeNode("INVALID TOPIC : " + roots[i].TreePath));
        }

        mTree.Nodes.Add(rootNode);
        mTree.CollapseAll();
        rootNode.Expand();
    }
    private TreeNode GetTreeNode(TreeInfo info)
    {
        TopicInfo topic = AccController.GetTopic(info.TopicID);
        TreeNode node = GetSingleTreeNode(info, topic);
        if (node == null)
            return null;
        List<TreeInfo> childs = AccController.GetTrees(info);
        childs.Sort();
        if (childs.Count > 0)
            node.ImageUrl = "~/img/programIcon.gif";
        for (int i = 0; i < childs.Count; i++)
        {
            //node.ChildNodes.Add(GetTreeNode(childs[i]));
            TreeNode ch = GetTreeNode(childs[i]);
            if (ch != null)
                node.ChildNodes.Add(ch);
            else
                node.ChildNodes.Add(new TreeNode("INVALID TOPIC : " + childs[i].TreePath));
        }
        return node;
    }
    private TreeNode GetSingleTreeNode(TreeInfo info, TopicInfo topic)
    {
        if (topic == null || info == null)
            return null;
        TreeNode node = new TreeNode(topic.NameCode, info.TreePath);
        node.ImageUrl = "~/img/leaf.gif";
        return node;
    }
    protected void doShow_Click(object sender, EventArgs e)
    {
        BindGrid();

        doExcel.Enabled = true;
    }

    string rowStyle = "GridRow";
    decimal sumOwe = 0, sumDue = 0;
    string lastTreePath = "";
    // int lastPersonnelID = 0, lastProjectID = 0, counter = 0;
    Dictionary<int, string> projects = null, equipments = null;
    Dictionary<string, RepRow> repRows = null;
    class RepRow
    {
        public string TopicCode, TreePathName;
        public Dictionary<int, decimal> Projects = new Dictionary<int, decimal>();
        public Dictionary<int, decimal> Equipments = new Dictionary<int, decimal>();
    }
    protected void BindGrid()
    {
        DateTime start = Helper.ParseDate(mFromDate.Text, Null.NullDate);
        DateTime end = Helper.ParseDate(mToDate.Text, Null.NullDate);
        if (Null.IsNull(start))
        {
            msg.Text = LangProvider.Instance["InvalidStartDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(end))
        {
            msg.Text = LangProvider.Instance["InvalidEndDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(start) || Null.IsNull(end))
            return;
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);
        List<string> treePathes = new List<string>();
        if (mTree.CheckedNodes != null && mTree.CheckedNodes.Count > 0)
            for (int i = 0; i < mTree.CheckedNodes.Count; i++)
            {
                if (!string.IsNullOrEmpty(mTree.CheckedNodes[i].Value))
                    treePathes.Add(mTree.CheckedNodes[i].Value);
            }

        repRows = new Dictionary<string, RepRow>();
        projects = new Dictionary<int, string>();
        equipments = new Dictionary<int, string>();
        // ---------------------------------------
        List<int> projectIDs = new List<int>();
        for (int i = 0; i < mProjects.Items.Count; i++)
            if (mProjects.Items[i].Selected)
            {
                projectIDs.Add(Helper.GetInt(mProjects.Items[i].Value, 0));
                projects.Add(Helper.GetInt(mProjects.Items[i].Value, 0), mProjects.Items[i].Text);
            }
        //if (projectIDs.Count == mProjects.Items.Count)
        //    projectIDs.Clear();


        // ---------------------------------------
        List<int> equipmentIDs = new List<int>();
        for (int i = 0; i < mEquipments.Items.Count; i++)
            if (mEquipments.Items[i].Selected)
            {
                equipmentIDs.Add(Helper.GetInt(mEquipments.Items[i].Value, 0));
                equipments.Add(Helper.GetInt(mEquipments.Items[i].Value, 0), mEquipments.Items[i].Text);
            }
        //if (equipmentIDs.Count == mEquipments.Items.Count)
        //    equipmentIDs.Clear();
        IDataReader reader = AccController.GetTopicBalance(start, end, projectIDs
            , Helper.GetInt(mPersons.SelectedValue, Null.NullInteger), SiteSettings.ProgramID
            , equipmentIDs
            , treePathes.ToArray()
            , mFromVoucherNo.Text.Trim(), mToVoucherNo.Text.Trim()
            , mjustOwe.Checked, mjustDue.Checked, mComments.Text.Trim());
        while (reader.Read())
        {
            string treePath = reader.IsDBNull(0) ? "" : reader.GetString(0);
            RepRow row = null;
            if (!repRows.ContainsKey(treePath))
            {
                row = new RepRow() { TopicCode = reader.IsDBNull(2) ? "" : reader.GetString(2), TreePathName = reader.IsDBNull(1) ? "" : reader.GetString(1) };
                repRows.Add(treePath, row);
            }
            row = repRows[treePath];
            decimal owe = 0, due = 0;
            if (!reader.IsDBNull(3))
                owe = reader.GetDecimal(3);
            if (!reader.IsDBNull(4))
                due = reader.GetDecimal(4);
            if (!reader.IsDBNull(5))
            {
                int eqID = reader.GetInt32(5);
                if (!row.Equipments.ContainsKey(eqID))
                    row.Equipments.Add(eqID, 0);
                row.Equipments[eqID] += owe - due;
            }
            if (!reader.IsDBNull(7))
            {
                int prID = reader.GetInt32(7);
                if (!row.Projects.ContainsKey(prID))
                    row.Projects.Add(prID, 0);
                row.Projects[prID] += owe - due;
            }
        }
        reader.Close();
        reader.Dispose();

        StringBuilder builder = new StringBuilder();
        builder.Append("<table class='ScrollPanelCartable table table-bordered table-condensed full-width table-with-border awesome-table colored'>");
        builder.Append("<tr class='GridHeader'>");
        builder.Append(string.Format("<td>{0}</td>", LangProvider.Instance["TopicLabel"]));
        for (int i = 0; i < projectIDs.Count; i++)
            builder.Append(string.Format("<td>{0}</td>", projects[projectIDs[i]]));
        for (int i = 0; i < equipmentIDs.Count; i++)
            builder.Append(string.Format("<td>{0}</td>", equipments[equipmentIDs[i]]));
        builder.Append("</tr>");

        Dictionary<string, RepRow>.Enumerator enu = repRows.GetEnumerator();
        while (enu.MoveNext())
        {
            RepRow row = enu.Current.Value;
            builder.Append("<tr>");
            builder.Append(string.Format("<td>{0}</td>", row.TreePathName));
            for (int i = 0; i < projectIDs.Count; i++)
            {
                string cls = "", val = "";
                if (row.Projects.ContainsKey(projectIDs[i]))
                {
                    decimal money = SiteSettings.ToMonetary(row.Projects[projectIDs[i]], DateTime.Now);
                    val = money.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                    if (money < 0)
                    {
                        val = (-money).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                        cls = "neg";
                    }
                }
                builder.Append(string.Format("<td class='{0}'>{1}</td>",  cls, val));
            }
            for (int i = 0; i < equipmentIDs.Count; i++)
            {
                string cls = "", val = "";
                if (row.Equipments.ContainsKey(equipmentIDs[i]))
                {
                    decimal money = SiteSettings.ToMonetary(row.Equipments[equipmentIDs[i]], DateTime.Now);
                    val = money.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                    if (money < 0)
                    {
                        val = (-money).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                        cls = "neg";
                    }
                }
                builder.Append(string.Format("<td class='{0}'>{1}</td>", cls, val));
            }
            builder.Append("</tr>");
        }
        builder.Append("</table>");
        holder.Controls.Clear();
        holder.Controls.Add(new LiteralControl(builder.ToString()));
    }
    protected void doExcel_Click(object sender, EventArgs e)
    {
        //Exporter.ExportExcel(Response, mTree.SelectedNode == null ? mTree.Nodes[0].Text : mTree.SelectedNode.Text, mGrid);
    }
}