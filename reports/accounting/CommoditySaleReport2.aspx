﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" CodeFile="CommoditySaleReport2.aspx.cs" Inherits="reports_accounting_CommoditySaleReport2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register src="~/controls/ReportSetting.ascx" tagName="ReportSetting" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" Runat="Server">
    <div id="bar">
    </div>
    <%--<uc1:ReportSetting ID="mReportSetting" runat="server" class="hidden" />--%>
<div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" AssociatedControlID="mFromDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="mFromDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label3" runat="server" AssociatedControlID="mToDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="mToDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="personLable" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Person</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mPersons" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="CommodityLable" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Commodity</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mCommodities" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="visitorLable" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Visitor</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mVisitores" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Labels" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Label</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mLabels" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin-5-top">
            <div class="col-md-5 col-md-offset-7 form-group-container form-col-without-padding">
                <div class="col-md-3">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
                </div>
                <div class="col-md-3">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcel_Click" Enabled="false" />
                </div>
                <div class="col-md-6">
                    <asp:Button  ID="ShowGroupSearch" runat="server" onclick="ShowGroupSearchClick" class="btn btn-xs btn-dark-blue btn-block" Text=" Lang.ShowGroupSearch" />
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="panelDropDowns" runat="server" Visible="False" CssClass="well callout padding-5">
        <input type="hidden" runat="server" id="mQueryBuilder" class="mQueryBuilder" />
        <input type="hidden" runat="server" id="mQueryBuilderSql" class="mQueryBuilderSql" />
        <div class="hidden">
            <asp:DropDownList runat="server" ID="mCommodityGroupings" CssClass="mCommodityGroupings"></asp:DropDownList>
            <asp:DropDownList runat="server" ID="mPersonGroupings" CssClass="mPersonGroupings"></asp:DropDownList>
            <asp:DropDownList runat="server" ID="mCommodityGroup" CssClass="mCommodityGroup"></asp:DropDownList>
            <asp:DropDownList runat="server" ID="mPersonGroup" CssClass="mPersonGroup"></asp:DropDownList>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <h5 class="dashed-bottom padding-5-bottom yekan-family">
                    <asp:Label runat="server">Lang.Groups</asp:Label>
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="ltr builder">
                    <div id="builder"></div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="col-xs-12">
        <asp:Label runat="server" ID="msg" CssClass="label-30 alert alert-danger block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
    </div>
    <asp:Panel ID="gridPanel" runat="server" Visible="false">
        <JP:MiniHeader runat="server" Text="Lang.CommoditySaleReport" ID="mGridTitle" />
        <div class="row">
            <div class="col-md-4">
                <asp:Label ID="NumFactorTotalLabel" runat="server" CssClass="left-aligned-label control-label col-md-4">Lang.NumFactorTotal</asp:Label>
                <div class="col-md-8">
                    <asp:TextBox ID="NumFactorTotal" runat="server" ReadOnly="True" CssClass="form-control input-sm"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-4">
                <asp:Label ID="AmountFactorTotalLabel" runat="server" CssClass="left-aligned-label control-label col-md-4">Lang.AmountFactorTotal</asp:Label>
                <div class="col-md-8">
                    <asp:TextBox ID="AmountFactorTotal" runat="server" ReadOnly="True" CssClass="form-control input-sm"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-4">
                <asp:Label ID="DiscountFactorTotalLabel" runat="server" CssClass="left-aligned-label control-label col-md-4">Lang.DiscountFactorTotal</asp:Label>
                <div class="col-md-8">
                    <asp:TextBox ID="DiscountFactorTotal" runat="server" ReadOnly="True" CssClass="form-control input-sm"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="row">
            <hr />
            <div class="col-md-2">
                <asp:GridView ID="mGrid" AutoGenerateColumns="False" CssClass=" Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true" OnSorting="mGrid_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="NOSerial" HeaderText="Lang.FactorNo" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="align-center" SortExpression="NOSerial" />
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
            <div class="col-md-10 factorheaderCSR2">
                <div class="col-md-12 align-center padding-15-bottom">
                    <span id="FactorNo" class="factornoSR2"></span>
                </div>
                <div class="col-md-12">
                    <div class="col-md-4 form-group-container">
                        <div class="form-group">
                            <asp:Label for="FactorDate" runat="server" CssClass="left-aligned-label col-md-4">Lang.FactorDate</asp:Label>
                            <div class="col-md-8">
                                <input type="text" id="FactorDate" readonly="readonly" name="FactorDate" class='dateSelect form-control input-sm' />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 form-group-container">
                        <div class="form-group">
                            <asp:Label for="BuyerName" runat="server" CssClass="left-aligned-label col-md-4">Lang.BuyerName</asp:Label>
                            <div class="col-md-8">
                                <input type="text" id="BuyerName" readonly="readonly" name="BuyerName" class='form-control input-sm' />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 form-group-container">
                        <div class="form-group">
                            <asp:Label for="VisitorName" runat="server" CssClass="left-aligned-label col-md-4">Lang.Visitor</asp:Label>
                            <div class="col-md-8">
                                <input type="text" id="VisitorName" readonly="readonly" name="VisitorName" class='form-control input-sm' />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-4 form-group-container">
                        <div class="form-group">
                            <asp:Label for="StoreName" runat="server" CssClass="left-aligned-label col-md-4">Lang.Store</asp:Label>
                            <div class="col-md-8">
                                <input type="text" id="StoreName" readonly="readonly" name="StoreName" class='form-control input-sm' />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 form-group-container">
                        <div class="form-group">
                            <asp:Label for="Total" runat="server" CssClass="left-aligned-label col-md-4">Lang.TotalS</asp:Label>
                            <div class="col-md-8">
                                <input type="text" id="Total" readonly="readonly" name="Total" class='form-control input-sm' />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 form-group-container">
                        <div class="form-group">
                            <asp:Label for="TotalDiscount" runat="server" CssClass="left-aligned-label col-md-4">Lang.TotalDiscount</asp:Label>
                            <div class="col-md-8">
                                <input type="text" id="TotalDiscount" readonly="readonly" name="TotalDiscount" class='form-control input-sm' />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10 factorrowsCSR2 hidden">
                <div class="compact detail">
                    <table id="tableItems" class="awesome-table table-with-border zebra">
                        <tr class='th'>
                            <td class="width-20 align-center">
                                <asp:Label runat="server">Lang.ColRowNr</asp:Label>
                            </td>
                            <td>
                                <asp:Label runat="server">Lang.CommodityName</asp:Label>
                            </td>
                            <td class="width-90 align-center">
                                <asp:Label runat="server">Lang.Units</asp:Label>
                            </td>
                            <td class="width-150 align-center">
                                <asp:Label runat="server">Lang.NumOrValue</asp:Label>
                            </td>
                            <td class="acc width-110 align-center">
                                <asp:Label runat="server">Lang.Price</asp:Label>
                            </td>
                            <td class="acc width-60 align-center">
                                <asp:Label runat="server">Lang.Discount</asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </asp:Panel>
    <script>
        $(function () {
            makeChosen();
            var cnt = 0;
            $('#FactorNo').text(Lang.CS2NoSelectedFactor);
           // $('#ShowGroupSearch').attr('value', Lang.ShowGroupSearch);
            $('#print').click(function () {
                var head = Lang.CommoditySaleReport2;
                <%--if ($("#<%=ddlBy.ClientID %>").val() == <%= (int)AccController.PersonBookGroupBy.ByTotal %>) {
                    head = '<%=jpMiniHeaderTotalBills.Text %>';
            }--%>
                printTable('.ScrollPanelCartable', '../../', head);
            });
            //$('#ShowGroupSearch').click(function () {
            //    $('.paneldropdowns').show();
            //    //if ($('.paneldropdowns').hasClass('hidden')) {
            //    //    $('.paneldropdowns').removeClass('hidden');
            //    //} else {
            //    //    $('.paneldropdowns').addClass('hidden');
            //    //}
            //});
            $('.factornolink').click(function () {
                $('.factorrowsCSR2').removeClass('hidden');
                $('.GridRow,.GridAltRow').removeClass('selected');
                $(this).parent().parent().toggleClass('selected');
                $('.factorrow').empty();
                var factorId = $(this).attr('ID');
                var autourl = '../../get.aspx?op=Auto&table=StoreDraft&qd=getfactordata&StoreDraftID=' + factorId + appendTime();
                var data = getServerValues(autourl);
                if (data) {
                    $('#FactorNo').html(Lang.CS2FactorHeader + "<a  target='_blank' href='../../list.aspx?table=StoreDraft&id=" + factorId + "&menu=accountinga'>" + data[0] + "</a>");
                    $('#FactorDate').val(data[1]);
                    $('#BuyerName').val(data[2]);
                    $('#VisitorName').val(data[3]);
                    $('#StoreName').val(data[4]);
                    $('#Total').val(numberWithCommas(toMonetary(data[5])));
                    $('#TotalDiscount').val(numberWithCommas(toMonetary(data[6])));
                }
                fillTable("../../get.aspx?op=GetXml&qd=storedraftitem&StoreDraftID=" + factorId, '#tableItems', addRow);
                cnt = 0;
            });
            dateSelect('.dateselect');
            $('.monetary').keyup(function () {
                $(this).val(numberWithCommas(stripCommas($(this).val())));
            });
            $("#bar").prepend(breadcrumb({
                title: Lang.CommoditySaleReport2,
                parent: {
                    title: Lang.Reports,
                    url: 'menu/reports.aspx'
                }
            }));

            function addRow(node) {
                var discount = getNodeVal(node, "Discount", 0);
                if (toFloat(discount, 0) < 0) {
                    discount = numberWithCommas((toFloat(discount, 0) * -1.0).toFixed(_Context.decimalPoints)) + '%';
                } else {
                    discount = numberWithCommas(toMonetary(discount));
                }
                var row = '<tr class="factorrow" >';
                row += '<td class="align-center row-number" data-th="' + Lang.ColRowNr + '">' + (cnt + 1) + '</td>'; // Row

                row += '<td data-th="' + Lang.CommodityName + '">'
                + '<input type="text" id="se_' + cnt + '" readonly="readonly" title="' + getNodeVal(node, "StoreName") + '" name="StoreServiceName" value="' + getNodeVal(node, "StoreServiceName") + '" class="storeservicename full-width form-control input-sm" /></td>'

                row += '<td class="align-center" data-th="' + Lang.Units + '">' // Units
                + '<input type="text" id="u_' + cnt + '" name="UnitName" readonly="readonly" value="' + getNodeVal(node, "UnitName") + '" class="unitname form-control input-sm" />'
                + '</td>';

                row += '<td data-th="' + Lang.NumOrValue + '" class="align-center">'// NumOrValue
                + '<div class="input-group input-group-sm">'
                + '<input type="text" id="a_' + cnt + '" readonly="readonly" name="DeliveredAmount" value="' + getNodeVal(node, "DeliveredAmount")
                + '" class="validate[custom[number]] amount form-control width-100-percent min-width-60 align-center" />'
                + '<span class="input-group-addon no-left-border no-right-border">X</span>'
                + '<input readonly="readonly" size="1" id="ucc_' + cnt + '" name="UnitCurrencyConverter" value="'
                + getNodeVal(node, "UnitCurrencyConverter", 1) + '" class="form-control width-100-percent min-width-60 align-center" />'
                + '<input type="hidden" id="isv_' + cnt + '" name="IsVirtual" readonly="readonly" value="' + getNodeVal(node, "IsVirtual", 0) + '" class="isvirtual" />'
                + '</div>'
                + '</td>';

                row += '<td class="acc" data-th="' + Lang.Price + '">' // Price
                + '<input type="text" readonly="readonly" id="p_' + cnt + '" name="Price" value="' + numberWithCommas(toMonetary(getNodeVal(node, "Price", 0))) + '" class="price monetary validate[custom[integer]] form-control align-center input-sm" />'
                + '</td>';

                row += '<td class="acc" data-th="' + Lang.Discount + '">' // Discount
                + '<input type="text" id="d_' + cnt + '" readonly="readonly" name="Discount" value="' + discount + '" class="discount percent monetary validate[custom[integer]] form-control input-sm align-center" />'
                + '</td>';

                row += '</tr>';
                cnt++;
                return row;
            }


            setClassReports();
            var sgg = {};
            $('.mCommodityGroupings option').each(function () {
                sgg[$(this).attr('value')] = $(this).text();
            });
            var pgg = {};
            $('.mPersonGroupings option').each(function () {
                pgg[$(this).attr('value')] = $(this).text();
            });
            var cog = {};
            $('.mCommodityGroup option').each(function () {
                cog[$(this).attr('value')] = $(this).text();
            });
            var peg = {};
            $('.mPersonGroup option').each(function () {
                peg[$(this).attr('value')] = $(this).text();
            });
            //if ($jQuery1_9('#builder').length > 0)
            $jQuery1_9('#builder').queryBuilder({
                filters: [
                    {
                        id: 'SGCategory',
                        label: Lang.ServiceGroupLable,
                        type: 'integer',
                        input: 'select',
                        values: sgg,
                        operators: ['equal', 'not_equal']
                    },
                    {
                        id: 'PGCategory',
                        label: Lang.PersonsGroupLable,
                        type: 'integer',
                        input: 'select',
                        values: pgg,
                        operators: ['equal', 'not_equal']
                    },
                    {
                        id: 'SCategory',
                        label: Lang.ServiceType,
                        type: 'integer',
                        input: 'select',
                        values: cog,
                        operators: ['equal', 'not_equal']
                    },
                    {
                        id: 'PCategory',
                        label: Lang.PersonnelGroup,
                        type: 'integer',
                        input: 'select',
                        values: peg,
                        operators: ['equal', 'not_equal']
                    }
                ]
            });
            // save rules before post back
            $('.doShow').click(function () {
                var rules = $jQuery1_9('#builder').queryBuilder('getRules');
                var rulesStr = JSON.stringify(rules);
                // alert(rulesStr);
                $('.mQueryBuilder').val(rulesStr);
                $('.mQueryBuilderSql').val($jQuery1_9('#builder').queryBuilder('getSQL', false, false).sql);
            });
            //reload saved rules
            $jQuery1_9('#builder').queryBuilder('reset');
            var ruleStr = $('.mQueryBuilder').val();
            if (ruleStr != '') {
                var rule = eval("(" + ruleStr + ")");
                try {
                    $jQuery1_9('#builder').queryBuilder('setRules', rule);
                } catch (e) {
                }
            }
            $jQuery1_9('#builder').on('afterCreateRuleInput.queryBuilder', function (rule) {
                makeChosen();
            });
        });
    </script>
</asp:Content>

