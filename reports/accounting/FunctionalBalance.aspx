﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="FunctionalBalance.aspx.cs" Inherits="reports_accounting_FunctionalBalance" %>

<%@ Register Src="../../controls/ReportSetting.ascx" TagName="ReportSetting" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>
    <div class="row margin-5-bottom">
        <div class="col-xs-12">
            <div class="btn-group btn-group-sm btn-group-justified">
                <a href="FunctionalBalance.aspx?level=0" class="btn btn-<%= Level == 0 ? "dark-blue" : "primary" %>">
                    <asp:Label ID="Label2" runat="server">Lang.Level1</asp:Label>
                </a>
                <a href="FunctionalBalance.aspx?level=1" class="btn btn-<%= Level == 1 ? "dark-blue" : "primary" %>">
                    <asp:Label ID='Label1' runat='server'>Lang.Level2</asp:Label>
                </a>
                <a href="FunctionalBalance.aspx?level=2" class="btn btn-<%= Level == 2 ? "dark-blue" : "primary" %>">
                    <asp:Label ID="Label3" runat="server">Lang.Level3</asp:Label>
                </a>
                <a href="FunctionalBalance.aspx?level=3" class="btn btn-<%= Level == 3 ? "dark-blue" : "primary" %>">
                    <asp:Label ID="Label4" runat="server">Lang.Level4</asp:Label>
                </a>
                <a href="FunctionalBalance.aspx?level=4" class="btn btn-<%= Level == 4 ? "dark-blue" : "primary" %>">
                    <asp:Label ID="Label5" runat="server">Lang.Level5</asp:Label>
                </a>
            </div>
        </div>
    </div>

    <uc2:ReportSetting ID="mReportSetting" runat="server" />

    <div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label7" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Project</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mWards" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label6" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Topic</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mTotals" runat="server" CssClass="chosen-select chosen-rtl" AutoPostBack="True" OnSelectedIndexChanged="mTotals_SelectedIndexChanged" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label8" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Year</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <div class="col-xs-12 col-sm-3 form-col-without-padding">
                            <asp:TextBox ID="mYear" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <asp:Label ID="Label9" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-2">Lang.Month</asp:Label>
                        <div class="col-xs-12 col-sm-3 form-col-without-padding">
                            <asp:DropDownList ID="mMonth" runat="server" CssClass="chosen-select chosen-rtl">
                                <asp:ListItem Value="0" Text="Lang.All" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                <asp:ListItem Value="12" Text="12"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-xs-12 col-sm-1">&nbsp;</div>
                        <asp:CheckBox ID="mFilterZero" runat="server" Text="Lang.FilterZeroRemained" Checked="true" CssClass="margin-0" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label10" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDocNo</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromVoucherID" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="fte1" runat="server" TargetControlID="mFromVoucherID"
                            FilterType="Numbers" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label11" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDocNo</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToVoucherID" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="fte2" runat="server" TargetControlID="mToVoucherID"
                            FilterType="Numbers" />
                    </div>
                </div>
            </div>
            <asp:Panel ID="panelCurLevels" class="col-xs-12 col-sm-6 form-group-container" runat="server">
                <div class="form-group">
                    <asp:Label runat="server" ID="curLevelLbl" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned"></asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mCurLevels" runat="server" CssClass="chosen-select chosen-rtl" AutoPostBack="False" />
                    </div>
                </div>
            </asp:Panel>
        </div>
        <div class="row margin-5-top">
            <div class="col-md-8 col-lg-6 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container form-col-without-padding">
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcel_Click" Enabled="false" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <input type="button" id="print" class="btn btn-xs btn-dark-blue btn-block" value="" />
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <JP:MiniHeader ID="mTitle" runat="server" Text="Lang.TotalBook" />
            <asp:Panel ID="voucherDetailPanel" runat="server">
                <asp:PlaceHolder runat="server" ID="mPC"></asp:PlaceHolder>
            </asp:Panel>
        </div>
    </div>
    <script>
        $(function () {
            $('#print').attr('value', Lang.Print);
            $('#print').click(function () {
                printTable('.ScrollPanelCartable', '../../', $('.mTitle').html());
            });
            $('.GridRow,.GridAltRow').click(function () {
                $(this).toggleClass('selected');
            });
            $("#bar").prepend(breadcrumb({
                title: Lang.MonthlyOperationsBalance,
                parent: {
                    title: Lang.Reports,
                    url: 'menu/Reports.aspx'
                }
            }));
            setClassReports();
        });
    </script>
</asp:Content>

