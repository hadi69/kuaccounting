﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="TotalBook.aspx.cs" Inherits="reports_accounting_TotalBook" %>

<%@ Register Src="../../controls/ReportSetting.ascx" TagName="ReportSetting" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>

<%--    <uc2:ReportSetting ID="mReportSetting" runat="server" />--%>
    <asp:Panel ID="panelDBs" runat="server" CssClass="well callout padding-5">
        <div class="row">
            <div class="col-xs-12">
                <h5 class="dashed-bottom padding-5-bottom yekan-family">
                    <asp:Label runat="server">Lang.DataBase</asp:Label>
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <asp:CheckBoxList runat="Server" ID="mDBList" RepeatLayout="Flow" RepeatDirection="Horizontal" />
            </div>
        </div>
     </asp:Panel>
    <div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 form-group-container hidden">
                <div class="form-group">
                    <asp:Label ID="Label4" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Project</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mWards" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <asp:Label ID="Label1" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.TopicName</asp:Label>
                <div class="form-group">
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mTotals" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label ID="Label5" runat="server">Lang.From</asp:Label>
                        <asp:Label ID="Label7" runat="server">Lang.DocumentNumber</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromVoucherNo" runat="server" CssClass="T form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label ID="Label6" runat="server">Lang.To</asp:Label>
                        <asp:Label ID="Label8" runat="server">Lang.DocumentNumber</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToVoucherNo" runat="server" CssClass="T form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="mFromDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label3" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4 col-lg-one-ninth-to-x-third">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 col-lg-eight-ninth-to-x-third form-col-without-padding">
                        <asp:TextBox ID="mToDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="mToDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label runat="server">Lang.Amount</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mAmount" runat="server" CssClass="T form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label9" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-6">Lang.InitRemain</asp:Label>
                    <div class="col-xs-12 col-sm-6 form-col-without-padding">
                        <asp:CheckBox ID="mShowInitRemain" runat="server" CssClass=""></asp:CheckBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin-5-top">
            <div class="col-md-8 col-lg-9 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container padding-0-left">
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcel_Click" Enabled="false" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <input type="button" id="print" class="btn btn-sm btn-dark-blue btn-block" value="" />
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <asp:Label runat="server" ID="msg" CssClass="label-30 alert alert-danger block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
    </div>
    <div class="row">
        <div class="col-xs-12">
            <asp:Panel ID="voucherDetailPanel" runat="server">
                <JP:MiniHeader ID="mTitle" runat="server" Text="Lang.TotalBook" />
                <asp:GridView ID="mGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                    scrolling="Vertical" runat="server" BorderStyle="None"
                    ShowFooter="true" OnRowDataBound="mGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true" OnSorting="mGrid_Sorting" >
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Date" HeaderText="Lang.Day" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="Date" HeaderText="Lang.Month" ItemStyle-HorizontalAlign="Center" SortExpression="Date" />
                        <asp:BoundField DataField="No" HeaderText="Lang.DocumentNumber" ItemStyle-HorizontalAlign="Center" SortExpression="No" />
                        <asp:BoundField DataField="TopicCode" HeaderText="Lang.TopicCode" ItemStyle-CssClass="tl" ItemStyle-HorizontalAlign="Center" SortExpression="TopicCode" />
                        <asp:BoundField DataField="TreePathName" HeaderText="Lang.Topic" ItemStyle-HorizontalAlign="Center" SortExpression="TreePathName" />
                        <asp:BoundField DataField="Comments" HeaderText="Lang.Comments" ItemStyle-HorizontalAlign="Center" SortExpression="Comments" />
                        <asp:BoundField DataField="PersonnelName" HeaderText="Lang.Person" ItemStyle-HorizontalAlign="Center" SortExpression="PersonnelName" />
                        <asp:TemplateField HeaderText="Lang.DocNo" ItemStyle-HorizontalAlign="Center" SortExpression="DocNo">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%#Eval("DocNo") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label runat="server"  />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Liability" ItemStyle-HorizontalAlign="Center" SortExpression="Owe">
                            <ItemTemplate>
                                <asp:Label ID="owe" runat="server" Text='<%#Eval("Owe") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="oweTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Credit" ItemStyle-HorizontalAlign="Center" SortExpression="Due">
                            <ItemTemplate>
                                <asp:Label ID="due" runat="server" Text='<%#Eval("Due") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="dueTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Remained" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="remaining" runat="server" Text='<%#Eval("Due") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="remainingTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Diagnosis" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="diagnosis" runat="server" Text='<%#Eval("Due") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="finalDiagnosis" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                    <FooterStyle CssClass="GridHeader" HorizontalAlign="Center" />
                </asp:GridView>
            </asp:Panel>
        </div>
    </div>
    <script>
        $(function () {
            $('#print').attr('value', Lang.Print);
            $('#print').click(function () {
                var title = Lang.TotalBook;
                if ($('#<%=mTotals.ClientID %>').val() != '') {
                    title += '/' + Lang.Topic + ': ' + $('#<%=mTotals.ClientID %> option:selected').text();
                }
                if ($("#<%=mFromDate.ClientID %>").val() != '') {
                    title += '/' + Lang.FromDate + ': ' + $("#<%=mFromDate.ClientID %>").val();
                }
                if ($("#<%=mToDate.ClientID %>").val() != '') {
                    title += '/' + Lang.ToDate + ': ' + $("#<%=mToDate.ClientID %>").val();
                }
                if ($("#<%=mFromVoucherNo.ClientID %>").val() != '') {
                    title += '/' + Lang.FromDocNo + ': ' + $("#<%=mFromVoucherNo.ClientID %>").val();
                }
                if ($("#<%=mToVoucherNo.ClientID %>").val() != '') {
                    title += '/' + Lang.ToDocNo + ': ' + $("#<%=mToVoucherNo.ClientID %>").val();
                }

                if ($('#<%=mWards.ClientID %>').val() != -1) {
                    title += '/' + Lang.Project + ': ' + $('#<%=mWards.ClientID %> option:selected').text();
                }
                printTable('.ScrollPanelCartable', '../../',title);
            });
            $('.GridRow,.GridAltRow').click(function () {
                $(this).toggleClass('selected');
            })
            $('a.no').click(function () {
                var url = $(this).attr('href');
                var otherID = $(this).attr('otherid');
                var mode = $(this).attr('mode');
                var table, title;
                if (mode == 4) {
                    table = 'Purchase';
                    title = Lang.SalesPurchase;
                } else if (mode == 7) {
                    table = 'StoreDraft';
                    title = Lang.SalesInvoice;
                } else if (mode == 6) {
                    table = 'Returns';
                    title = Lang.ReturnInvoice;
                } else if (mode == 5) {
                    table = 'Refuse';
                    title = Lang.RefuseInvoice;
                }
                showMenu({
                    parent: this, width: '100px',
                    cmds: [["سند", 'Voucher']
                        , [title, 'Table']
                        , [Lang.Cancel, 'Cancel']],
                    callback: function (cmd) {
                        if (cmd == 'Voucher')
                            document.location = url;
                        else if (cmd == 'Table')
                            document.location = "../../list.aspx?table=" + table + "&menu=accountinga&id=" + otherID;
                    }
                });
                return false;
            });
            dateSelect('.dateselect');
            $("#bar").prepend(breadcrumb({
                title: Lang.TotalBook,
                parent: {
                    title: Lang.Reports,
                    url: 'menu/reports.aspx'
                }
            }));
            setClassReports();
        });
    </script>
</asp:Content>
