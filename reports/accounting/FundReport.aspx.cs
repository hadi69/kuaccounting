﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;

public partial class reports_accounting_FundReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        msg.Visible = false;
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");

        mGrid.EmptyDataText = LangProvider.Instance["ListIsEmpty"];

        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["CommoditySaleReport"];
            Page.SetLang();
            IDataReader financeYearStartDate =
                DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");
            //0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            //mFromDate.Text = 
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);
            Initial();
        }

    }

    private void Initial()
    {
        {
            IDataReader stores = DbProxy.Instance.ExecuteReader("SELECT ID, Title FROM Stores");
            mStores.Items.Clear();
            mStores.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
            if (stores != null)
            {
                while (stores.Read())
                {
                    if (!stores.IsDBNull(0) && !stores.IsDBNull(1))
                        mStores.Items.Add(new ListItem(stores.GetString(1), stores.GetInt32(0).ToString()));
                }
                stores.Close();
            }
            mStores.SelectedIndex = 0;

            IDataReader users = DbProxy.Instance.ExecuteReader("SELECT ID, UserName FROM Users WHERE Enabled = 1");
            mUsers.Items.Add(new ListItem(LangProvider.Instance["All"], ""));

            if (users != null)
            {
                while (users.Read())
                {
                    if (!users.IsDBNull(0) && !users.IsDBNull(1))
                        mUsers.Items.Add(new ListItem(users.GetString(1) , users.GetInt32(0).ToString()));
                }
                users.Close();
            }
            mUsers.SelectedIndex = 0;
        }
    }

    protected void doShow_Click(object sender, EventArgs e)
    {
        doExcel.Enabled = true;
        DateTime start = Helper.ParseDate(mFromDate.Text, Null.NullDate);
        DateTime end = Helper.ParseDate(mToDate.Text, Null.NullDate);
        if (Null.IsNull(start))
        {
            msg.Text = LangProvider.Instance["InvalidStartDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(end))
        {
            msg.Text = LangProvider.Instance["InvalidEndDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(start) || Null.IsNull(end))
            return;
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);
        BindGrid(start, end);
    }

    #region Grid

    private decimal  _sumCash, _sumCheck, _sumOwe, _sumBank;

    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.Common.DbDataRecord record = e.Row.DataItem as System.Data.Common.DbDataRecord;
            DateTime dt = DateTime.Now;
            dt = Helper.GetDateTime(record["Date"], Helper.NullDate);
            if (dt != Helper.NullDate)
            {
                e.Row.Cells[9].Text = Helper.FormatDate(Helper.GetDateTime(dt, Helper.NullDate), null);
                e.Row.Cells[10].Text = Helper.FormatDate(Helper.GetDateTime(dt, Helper.NullDate), "HH:mm");
            }

            decimal cash = 0;
            decimal check = 0;
            decimal bank = 0;
            decimal owe = 0;

            e.Row.Cells[5].Text = "0";
            e.Row.Cells[6].Text = "0";
            e.Row.Cells[7].Text = "0";
            e.Row.Cells[8].Text = "0";

            int type = Helper.GetInt(record["Type"], Null.NullInteger);
            switch (type)
            {
                case 1:
                {
                        e.Row.Cells[3].Text = string.Format("<a href='../../list.aspx?table=StoreDraft&view=shopstoredrafts&id={0}&menu=accountinga'>{1}</a>", record["ID"],record["NOSerial"]);
                        e.Row.Cells[4].Text = string.Format("<a href='../../list.aspx?table=DocumentAccounting&id={0}'>{1}</a>", record["VoucherID"], record["VoucherNo"]);
                        int payMethod = Helper.GetInt(record["PayMethod"], Null.NullInteger);
                    switch (payMethod)
                    {
                        case 0: // cash
                        {
                            cash = SiteSettings.ToMonetary(Helper.GetDecimal(record["FinalPrice"], 0), dt);
                                    e.Row.Cells[2].Text = LangProvider.Instance["CashSale"];
                                    e.Row.Cells[5].Text = (cash).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                                     break;
                        }
                            case 1:
                            case 5: // check
                        {
                                    check = SiteSettings.ToMonetary(Helper.GetDecimal(record["FinalPrice"], 0), dt);
                                    e.Row.Cells[2].Text = LangProvider.Instance["CheckSale"];
                                    e.Row.Cells[7].Text = (check).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                                   break;
                        }
                        case 2: // Owe
                        {
                                    owe = SiteSettings.ToMonetary(Helper.GetDecimal(record["FinalPrice"], 0), dt);
                                    e.Row.Cells[2].Text = LangProvider.Instance["OweSale"];
                                    e.Row.Cells[8].Text = (owe).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                                    break;
                        }
                        case 3: // complex
                        {
                                    check = SiteSettings.ToMonetary(Helper.GetDecimal(record["CheckPrice"], 0), dt);
                                    owe = SiteSettings.ToMonetary(Helper.GetDecimal(record["OwedPrice"], 0), dt);
                                    cash = SiteSettings.ToMonetary(Helper.GetDecimal(record["CashPrice"], 0), dt);
                                    bank = SiteSettings.ToMonetary(Helper.GetDecimal(record["BankPrice"], 0), dt);
                                    e.Row.Cells[2].Text = LangProvider.Instance["ComplexSale"];
                                    e.Row.Cells[5].Text = cash.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                                    e.Row.Cells[6].Text = (bank).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                                    e.Row.Cells[7].Text = (check).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                                    e.Row.Cells[8].Text = (owe).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                                    break;
                        }
                            case 4: //bank
                        {
                                    bank = SiteSettings.ToMonetary(Helper.GetDecimal(record["FinalPrice"], 0), dt);
                                    e.Row.Cells[2].Text = LangProvider.Instance["BankSale"];
                                    e.Row.Cells[6].Text = (bank).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                                    break;
                        }
                     }
                    break;
                }

                case 2:
                {
                        e.Row.Cells[3].Text = string.Format("<a href='../../list.aspx?table=Returns&id={0}&menu=accountinga'>{1}</a>", record["ID"], record["NOSerial"]);
                        e.Row.Cells[4].Text = string.Format("<a href='../../list.aspx?table=DocumentAccounting&id={0}'>{1}</a>", record["VoucherID"], record["VoucherNo"]);
                        int isCashReturn = Helper.GetInt(record["IsCashReturn"], Null.NullInteger);
                    if (isCashReturn == 1)
                    {
                            cash = SiteSettings.ToMonetary(-1 * Helper.GetDecimal(record["FinalPrice"], 0), dt);
                            e.Row.Cells[2].Text = LangProvider.Instance["CashReturn"];
                            e.Row.Cells[5].Text = cash.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                        }
                    else
                    {
                            owe = SiteSettings.ToMonetary(Helper.GetDecimal(record["FinalPrice"], 0), dt);
                            e.Row.Cells[2].Text = LangProvider.Instance["OweReturn"];
                            e.Row.Cells[8].Text = owe.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                        }
                    break;
                }

                case 3:
                {
                        cash = SiteSettings.ToMonetary(-1 * Helper.GetDecimal(record["FinalPrice"], 0), dt);
                        e.Row.Cells[3].Text = "";
                        e.Row.Cells[4].Text = string.Format("<a href='../../list.aspx?table=DocumentAccounting&id={0}'>{1}</a>", record["ID"], record["VoucherNo"]);
                        e.Row.Cells[2].Text = LangProvider.Instance["CashOutcome"];
                        e.Row.Cells[5].Text = cash.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                    break;
                }
                case 4:
                    {
                        cash = SiteSettings.ToMonetary(Helper.GetDecimal(record["FinalPrice"], 0), dt);
                        e.Row.Cells[3].Text = "";
                        e.Row.Cells[4].Text = string.Format("<a href='../../list.aspx?table=DocumentAccounting&id={0}'>{1}</a>", record["ID"], record["VoucherNo"]);
                        e.Row.Cells[2].Text = LangProvider.Instance["CashIncome"];
                        e.Row.Cells[5].Text = cash.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                        break;
                    }
                case 5:
                    {
                        bank = SiteSettings.ToMonetary(-1 * Helper.GetDecimal(record["FinalPrice"], 0), dt);
                        e.Row.Cells[3].Text = "";
                        e.Row.Cells[4].Text = string.Format("<a href='../../list.aspx?table=DocumentAccounting&id={0}'>{1}</a>", record["ID"], record["VoucherNo"]);
                        e.Row.Cells[2].Text = LangProvider.Instance["BankOutcome"];
                        e.Row.Cells[6].Text = bank.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                        break;
                    }
                case 6:
                    {
                        bank = SiteSettings.ToMonetary(Helper.GetDecimal(record["FinalPrice"], 0), dt);
                        e.Row.Cells[3].Text = "";
                        e.Row.Cells[4].Text = string.Format("<a href='../../list.aspx?table=DocumentAccounting&id={0}'>{1}</a>", record["ID"], record["VoucherNo"]);
                        e.Row.Cells[2].Text = LangProvider.Instance["BankIncome"];
                        e.Row.Cells[6].Text = bank.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                        break;
                    }
            }
            _sumBank += bank;
            _sumCash += cash;
            _sumCheck += check;
            _sumOwe += owe;

        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            (e.Row.FindControl("totalCash") as Label).Text = _sumCash.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("totalBank") as Label).Text = _sumBank.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("totalOwe") as Label).Text = _sumOwe.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("totalCheck") as Label).Text = _sumCheck.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }
    }

    protected void mGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        DateTime start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        DateTime end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);
            BindGrid(start, end);
    }


    private void BindGrid(DateTime start, DateTime end)
    {
        gridPanel.Visible = true;
        
        int storeId = Helper.GetInt(mStores.SelectedValue, Null.NullInteger);
        int userId = Helper.GetInt(mUsers.SelectedValue, Null.NullInteger);
        string query = "";

        #region report
            query = string.Format(
                    @" DECLARE @ParentFundTreePath nvarchar(500), @Index int, @ParentBankTreePath nvarchar(500);
                       SET @ParentFundTreePath = dbo.GetSetting2('FundTreePath', NULL)
                        SET @Index = CHARINDEX('-', @ParentFundTreePath)
                        IF @Index > 0
                        SET @ParentFundTreePath = SUBSTRING(@ParentFundTreePath, 0, @Index)
                        
                        SET @ParentBankTreePath = dbo.GetSetting2('BankTreePath', NULL)

                       SELECT 1 Type, StoreDraftView.ID AS ID, StoreDraftView.VoucherID , VoucherNo, NoSerial, BuyerName AS PersonName, StoreDraftView.FactorDate AS Date, PayMethod, ISNULL(FinalPrice, 0) FinalPrice
                       , ISNULL(CashPrice, 0) CashPrice, ISNULL(CheckPrice, 0) CheckPrice, ISNULL(OwedPrice, 0) OwedPrice, ISNULL(BankPrice, 0) BankPrice, 0 IsCashReturn FROM StoreDraftView WHERE CONVERT(DATE, StoreDraftView.FactorDate) BETWEEN CONVERT(DATE, {0})
                       AND CONVERT(DATE, {1}) AND  (WorkFlowStatus = 1 OR IsTemp = 1) AND (StoreDraftView.UserID = {2} OR {2} = -1 )
                       AND (StoreDraftView.StoreID = {3} OR {3} = -1 )
                       UNION
                       SELECT 2 Type, ReturnView.ID AS ID, ReturnView.VoucherID, VoucherNo, NoSerial, BuyerName AS PersonName, ReturnView.ReturnDate AS Date, 0 PayMethod,  ISNULL(FinalPrice, 0) AS FinalPrice, 0 CashPrice, 0 CheckPrice, 0 OwedPrice, 0 BankPrice, ISNULL(IsCashReturn, 0) FROM ReturnView WHERE CONVERT(DATE, ReturnView.ReturnDate) BETWEEN CONVERT(DATE, {0})
                       AND CONVERT(DATE, {1}) AND WorkFlowStatus = 1 AND (ReturnView.UserID = {2} OR {2} = -1 )
                       AND (ReturnView.StoreID = {3} OR {3} = -1 )
                       UNION
                       SELECT 3 Type, DocumentAccounting.ID AS ID, '' AS VoucherID, DocumentAccounting.No AS VoucherNo, '' NoSerial, '' AS PersonName, DocumentAccounting.Date AS Date, 0 PayMethod, ISNULL(Due, 0) AS FinalPrice, 0 CashPrice, 0 CheckPrice, 0 OwedPrice, 0 BankPrice, 0 IsCashReturn FROM DocumentAccountingItems
                       LEFT OUTER JOIN DocumentAccounting ON DocumentAccountingItems.DocumentAccountingID = DocumentAccounting.ID
                       WHERE CONVERT(DATE, DocumentAccounting.Date) BETWEEN CONVERT(DATE, {0})
                       AND CONVERT(DATE, {1}) AND DocumentAccounting.WorkFlowStatus = 1 AND (DocumentAccounting.UserID= {2} OR {2} = -1)
                       AND DocumentAccounting.Mode = 0 AND DocumentAccountingItems.TreePath LIKE @ParentFundTreePath + '%' AND ISNULL(DocumentAccountingItems.Due, 0) > 0
                       UNION
                       SELECT 4 Type, DocumentAccounting.ID AS ID, '' AS VoucherID, DocumentAccounting.No AS VoucherNo, '' NoSerial, '' AS PersonName, DocumentAccounting.Date AS Date, 0 PayMethod, ISNULL(Owe, 0) AS FinalPrice, 0 CashPrice, 0 CheckPrice, 0 OwedPrice, 0 BankPrice, 0 IsCashReturn FROM DocumentAccountingItems
                       LEFT OUTER JOIN DocumentAccounting ON DocumentAccountingItems.DocumentAccountingID = DocumentAccounting.ID
                       WHERE CONVERT(DATE, DocumentAccounting.Date) BETWEEN CONVERT(DATE, {0})
                       AND CONVERT(DATE, {1}) AND DocumentAccounting.WorkFlowStatus = 1 AND (DocumentAccounting.UserID= {2} OR {2} = -1)
                       AND DocumentAccounting.Mode = 0 AND DocumentAccountingItems.TreePath LIKE @ParentFundTreePath + '%' AND ISNULL(DocumentAccountingItems.Owe, 0) > 0
                       UNION
                       SELECT 5 Type, DocumentAccounting.ID AS ID, '' AS VoucherID, DocumentAccounting.No AS VoucherNo, '' NoSerial, '' AS PersonName, DocumentAccounting.Date AS Date, 0 PayMethod, ISNULL(Due, 0) AS FinalPrice, 0 CashPrice, 0 CheckPrice, 0 OwedPrice, 0 BankPrice, 0 IsCashReturn FROM DocumentAccountingItems
                       LEFT OUTER JOIN DocumentAccounting ON DocumentAccountingItems.DocumentAccountingID = DocumentAccounting.ID
                       WHERE CONVERT(DATE, DocumentAccounting.Date) BETWEEN CONVERT(DATE, {0})
                       AND CONVERT(DATE, {1}) AND DocumentAccounting.WorkFlowStatus = 1 AND (DocumentAccounting.UserID= {2} OR {2} = -1)
                       AND DocumentAccounting.Mode = 0 AND DocumentAccountingItems.TreePath LIKE @ParentBankTreePath + '%' AND ISNULL(DocumentAccountingItems.Due, 0) > 0
                       UNION
                       SELECT 6 Type, DocumentAccounting.ID AS ID, '' AS VoucherID, DocumentAccounting.No AS VoucherNo, '' NoSerial, '' AS PersonName, DocumentAccounting.Date AS Date, 0 PayMethod, ISNULL(Owe, 0) AS FinalPrice, 0 CashPrice, 0 CheckPrice, 0 OwedPrice, 0 BankPrice, 0 IsCashReturn FROM DocumentAccountingItems
                       LEFT OUTER JOIN DocumentAccounting ON DocumentAccountingItems.DocumentAccountingID = DocumentAccounting.ID
                       WHERE CONVERT(DATE, DocumentAccounting.Date) BETWEEN CONVERT(DATE, {0})
                       AND CONVERT(DATE, {1}) AND DocumentAccounting.WorkFlowStatus = 1 AND (DocumentAccounting.UserID= {2} OR {2} = -1)
                       AND DocumentAccounting.Mode = 0 AND DocumentAccountingItems.TreePath LIKE @ParentBankTreePath + '%' AND ISNULL(DocumentAccountingItems.Owe, 0) > 0

                       ", Helper.QoutedDateTime(start), Helper.QoutedDateTime(end), userId, storeId);

                        if (!string.IsNullOrEmpty(SortCol))
                            query += " ORDER BY Date, " + SortCol + " " + SortOrder;
                        else
                            query += " ORDER BY Date";

        IDataReader items = DbProxy.Instance.ExecuteReader(query);
            try
            {
                mGrid.DataSource = items;
                mGrid.DataBind();
            }
            finally
            {
                if (null != items)
                {
                    items.Close();
                    items.Dispose();
                }
            }

        #endregion
    }

    protected string SortCol
    {
        get { return Helper.GetString(ViewState["SortCol"], null); }
        set { ViewState["SortCol"] = value; }
    }

    protected string SortOrder
    {
        get { return Helper.GetString(ViewState["SortOrder"], "asc"); }
        set { ViewState["SortOrder"] = value; }
    }

    #endregion

    // ------------------------
    // ids: 1,2,5
    // where: '&1%' AND ('&2%' OR '&5%')
    // ------------------------

    protected void doExcel_Click(object sender, EventArgs e)
    {
        Exporter.ExportExcel(Response
            , new List<string>(new string[] { mGridTitle.Text })
            , new List<GridView>(new GridView[] { mGrid }));
    }
}