﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Drawing;

public partial class reports_accounting_accrep : System.Web.UI.Page
{
    protected string reportTitle;
    protected void Page_Load(object sender, EventArgs e)
    {
        mPeriodSelector.PeriodsChange += new EventHandler(mPeriodSelector_PeriodsChange);
        mZedGraphControl.InitPane += graph_InitPane;
        if (!IsPostBack)
        {
            EditID = Helper.GetInt(Request.QueryString["id"], Null.NullInteger);
            BindReport();

            mReportSetting.ReportName = "Accreport" + this.EditID;
        }
        mReportSetting.SaveClick += new EventHandler(mReportSetting_SaveClick);
        mReportSetting.ReportSettingSelect += new EventHandler(mReportSetting_ReportSettingSelect);
    }
    void mReportSetting_ReportSettingSelect(object sender, EventArgs e)
    {
        ReportSettingInfo info = mReportSetting.SelectedReportSettings;
        if (info == null)
            return;
        string[] lines = info.Settings.Split('\n');
        List<PeriodInfo> periods = new List<PeriodInfo>();
        for (int i = 0; i < lines.Length; i++)
            if (lines[i].Trim().Length > 0)
            {
                PeriodInfo period = new PeriodInfo();
                period.SetText(lines[i]);
                periods.Add(period);
            }
        mPeriodSelector.Periods = periods;
        BindReport();
    }

    void mReportSetting_SaveClick(object sender, EventArgs e)
    {
        StringBuilder b = new StringBuilder();
        List<PeriodInfo> periods = mPeriodSelector.Periods;
        for (int i = 0; i < periods.Count; i++)
            b.AppendLine(periods[i].GetText());
        mReportSetting.Save(b.ToString());
    }
    void mPeriodSelector_PeriodsChange(object sender, EventArgs e)
    {
        BindReport();
    }
    DataTable PrepareData(List<PeriodInfo> periods, DataRow report, DataTable items, out int secRowTitleIndex)
    {
        List<DataRow> firstCol = new List<DataRow>(), secondCol = new List<DataRow>();
        if (items != null)
            for (int i = 0; i < items.Rows.Count; i++)
                if (Helper.GetBool(items.Rows[i], "IsSecond", false))
                    secondCol.Add(items.Rows[i]);
                else
                    firstCol.Add(items.Rows[i]);

        secRowTitleIndex = -1;
        DataTable dt = new DataTable();
        dt.Columns.Add("_COL1_Title", typeof(string));
        dt.Columns.Add("_COL1_Visible", typeof(bool)); // visible
        dt.Columns.Add("_COL1_InChart", typeof(bool)); // inchart
        for (int i = 0; i < periods.Count; i++)
            dt.Columns.Add("_ReMAIN1_" + i, typeof(decimal));
        if (secondCol.Count > 0)
        {
            secRowTitleIndex = dt.Columns.Count;
            dt.Columns.Add("_COL2_Title", typeof(string));
            dt.Columns.Add("_COL2_Visible", typeof(bool));// visible
            dt.Columns.Add("_COL2_InChart", typeof(bool));// inchart
            for (int i = 0; i < periods.Count; i++)
                dt.Columns.Add("_ReMAIN2_" + i, typeof(decimal));
        }

        int max = Math.Max(firstCol.Count, secondCol.Count);
        for (int i = 0; i < max; i++)
        {
            DataRow row = dt.NewRow();
            if (i < firstCol.Count)
            {
                row[0] = Helper.GetString(firstCol[i], "Title", "");
                row[1] = Helper.GetBool(firstCol[i], "IsVisible", false);
                row[2] = Helper.GetBool(firstCol[i], "InChart", false);
            }
            if (secondCol.Count > 0)
                if (i < secondCol.Count)
                {
                    row[secRowTitleIndex] = Helper.GetString(secondCol[i], "Title", "");
                    row[secRowTitleIndex + 1] = Helper.GetBool(secondCol[i], "IsVisible", false);
                    row[secRowTitleIndex + 2] = Helper.GetBool(secondCol[i], "InChart", false);
                }
            dt.Rows.Add(row);
        }

        for (int p = 0; p < periods.Count; p++)
        {
            Evaluater firstEval = new Evaluater(firstCol, 0, periods[p].FromDate, periods[p].ToDate);
            Evaluater secondEval = new Evaluater(secondCol, firstCol.Count, periods[p].FromDate, periods[p].ToDate);
            for (int i = 0; i < max; i++)
            {
                // first col
                if (i < firstCol.Count)
                {
                    if (Helper.GetBool(firstCol[i], "IsVisible", false))
                    {
                        decimal remain = firstEval.Evaluate(i);
                        remain = SiteSettings.ToMonetary(remain, DateTime.Now);
                        dt.Rows[i][3 + p] = remain;
                    }
                }
                // second col
                if (secondCol.Count > 0)
                {
                    if (i < secondCol.Count)
                    {
                        if (Helper.GetBool(secondCol[i], "IsVisible", false))
                        {
                            decimal remain = secondEval.Evaluate(i);
                            remain = SiteSettings.ToMonetary(remain, DateTime.Now);
                            dt.Rows[i][secRowTitleIndex + 3 + p] = remain;
                        }
                    }
                }
            }
        }

        return dt;
    }
    void BindReport()
    {
        mHolder.Controls.Clear();
        if (EditID == Null.NullInteger)
            return;
        List<PeriodInfo> periods = mPeriodSelector.Periods;
        #region Make sure the periods is not empty
        if (periods == null || periods.Count == 0)
        {
            PeriodInfo period = new PeriodInfo();
            period.FromDate = DateTime.Now.AddYears(-100);
            period.ToDate = DateTime.Now.AddYears(100);
            period.Title = "AllPeriods".Translate();
            periods = new List<PeriodInfo>();
            periods.Add(period);
        }
        #endregion

        DataRow report = DbProxy.Instance.FillRow(string.Format("SELECT * FROM AccReport WHERE ID={0} ", EditID));
        DataTable items = DbProxy.Instance.FillData(string.Format("SELECT * FROM AccReportCol WHERE AccReportID={0} ORDER BY IsSecond, SortIndex", EditID), "Rep");
        if (items == null || items.Rows.Count == 0)
            return;
        string col1Title = Helper.GetString(report, "FirstColTitle", ""), col2Title = Helper.GetString(report, "SecondColTitle", "");

        StringBuilder builder = new StringBuilder();
        builder.AppendFormat("<span class='Title'><b>{0}</b></span>", Helper.GetString(report, "ReportTitle", ""));

        int secRowTitleIndex;
        DataTable dt = PrepareData(periods, report, items, out secRowTitleIndex);
        builder.AppendFormat("<table class='report table table-bordered table-condensed zebra'>");
        #region header row
        if (periods.Count == 1)
        {
            builder.AppendFormat("<tr class='theader'>");
            builder.AppendFormat("<td>{0}</td><td>{1}</td><td>{2}</td>", col1Title, LangProvider.Instance["Liability"], LangProvider.Instance["Credit"]);
            if (secRowTitleIndex > 0)
                builder.AppendFormat("<td>{0}</td><td>{1}</td><td>{2}</td>", col2Title, LangProvider.Instance["Liability"], LangProvider.Instance["Credit"]);
            builder.AppendFormat("</tr>");
        }
        else
        {
            builder.AppendFormat("<tr class='theader'>");
            builder.AppendFormat("<td rowspan='2'>{0}</td>", col1Title);
            for (int i = 0; i < periods.Count; i++)
                builder.AppendFormat("<td colspan=2>{0}</td>", periods[i].Title);
            if (secRowTitleIndex > 0)
            {
                builder.AppendFormat("<td rowspan='2'>{0}</td>", col2Title);
                for (int i = 0; i < periods.Count; i++)
                    builder.AppendFormat("<td colspan=2>{0}</td>", periods[i].Title);
            }
            builder.AppendFormat("</tr>");

            builder.AppendFormat("<tr class='theader'>");
            for (int i = 0; i < periods.Count; i++)
                builder.AppendFormat("<td>{0}</td><td>{1}</td>", LangProvider.Instance["Liability"], LangProvider.Instance["Credit"]);
            if (secRowTitleIndex > 0)
                for (int i = 0; i < periods.Count; i++)
                    builder.AppendFormat("<td>{0}</td><td>{1}</td>", LangProvider.Instance["Liability"], LangProvider.Instance["Credit"]);
            builder.AppendFormat("</tr>");
        }
        #endregion

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            bool empty = !(bool)dt.Rows[i][1];
            if (empty && secRowTitleIndex > 0)
                empty = !(bool)dt.Rows[i][secRowTitleIndex + 1];
            if (empty)
                continue;
            builder.Append("<tr>");
            builder.AppendFormat("<td>{0}</td>", dt.Rows[i][0]); // first col
            for (int p = 0; p < periods.Count; p++)
            {
                decimal remain = Helper.GetDecimal(dt.Rows[i][3 + p], 0);
                remain = SiteSettings.ToMonetary(remain, DateTime.Now);
                builder.AppendFormat("<td>{0}</td><td>{1}</td>"
                    , remain >= 0 ? "" : Math.Abs(remain).ToString("N4", Helper.NumberCulture).Replace(".0000", "")
                    , remain <= 0 ? "" : remain.ToString("N4", Helper.NumberCulture).Replace(".0000", ""));
            }
            if (secRowTitleIndex > 0)
            {
                builder.AppendFormat("<td>{0}</td>", dt.Rows[i][secRowTitleIndex]); // first col
                for (int p = 0; p < periods.Count; p++)
                {
                    decimal remain = Helper.GetDecimal(dt.Rows[i][secRowTitleIndex + 3 + p], 0);
                    remain = SiteSettings.ToMonetary(remain, DateTime.Now);
                    builder.AppendFormat("<td>{0}</td><td>{1}</td>"
                        , remain >= 0 ? "" : Math.Abs(remain).ToString("N4", Helper.NumberCulture).Replace(".0000", "")
                        , remain <= 0 ? "" : remain.ToString("N4", Helper.NumberCulture).Replace(".0000", ""));
                }
            }
        }

        builder.AppendFormat("</table>");
        mHolder.Controls.Add(new LiteralControl(builder.ToString()));

        reportTitle = Helper.GetString(report, "ReportTitle", "");

        #region Chart
        for (int p = 0; p < periods.Count; p++)
        {
            List<double> ys = new List<double>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if ((bool)dt.Rows[i][2])
                {
                    decimal remain = Helper.GetDecimal(dt.Rows[i][3 + p], 0);
                    remain = SiteSettings.ToMonetary(remain, DateTime.Now);
                    ys.Add(Math.Abs((double)remain));
                }
            }
            double[] xs = new double[ys.Count];
            for (int j = 0; j < ys.Count; j++)
                xs[j] = j;
            ZedGraph.CurveItem item = TableGraphProxy.FromValus(periods[p].Title, GraphMode.Bar, xs, ys.ToArray());
            mZedGraphControl.Curves.Add(item);
        }
        List<string> labels = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if ((bool)dt.Rows[i][2])
                labels.Add((string)dt.Rows[i][0]);
        }
        //for (int i = 0; i < products.Count; i++)
        //    labels[i] = dt.Rows[i][0].ToString();

        mZedGraphControl.Visible = true;
        mZedGraphControl.Title = Helper.GetString(report, "ReportTitle", "");

        mZedGraphControl.XLabels = labels.ToArray();
        mZedGraphControl.XLabel = mZedGraphControl.XLabel + "";
        mZedGraphControl.Visible = true;
        mZedGraphControl.BindGraph();
        #endregion
    }

    private void graph_InitPane(Graphics g, ZedGraph.GraphPane pane)
    {
        //pane.XAxis.Type = ZedGraph.AxisType.Date;// = true;
        //pane.XAxis.UserMakeLabel = MakeLabel;
        pane.XAxis.GridColor = Color.Black;
        pane.BarType = ZedGraph.BarType.Cluster;
        pane.FontSpec.Family = "Tahoma";
        pane.FontSpec.Size = 14;
        pane.XAxis.TitleFontSpec.Family = "Verdana";
        pane.XAxis.TitleFontSpec.Size = 13;
        pane.XAxis.StepAuto = false;
        pane.XAxis.Step = 1;
    }

    protected int EditID
    {
        get
        {
            return Helper.GetInt(ViewState["EditID"], Null.NullInteger);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }

    class Evaluater
    {
        List<DataRow> mRows;
        private int mBaseIndex = 0;
        private DateTime mFrom, mTo;
        public Evaluater(List<DataRow> rows, int baseIndex, DateTime from, DateTime to)
        {
            mRows = rows;
            mBaseIndex = baseIndex;
            mFrom = from;
            mTo = to;
        }
        Dictionary<int, decimal> vals = new Dictionary<int, decimal>();
        List<int> visited = new List<int>();
        public decimal Evaluate(int index)
        {
            // have we calculated it already ?
            if (index < 0 || index > mRows.Count - 1)
                return 0;
            if (vals.ContainsKey(index))
                return vals[index];

            string treePath = Helper.GetString(mRows[index], "TreePath", null);
            if (!string.IsNullOrEmpty(treePath)) // it is a Topic
            {
                string query = string.Format(@"SELECT SUM(DocumentAccountingItems.Due), SUM(DocumentAccountingItems.Owe) FROM DocumentAccountingItems 
INNER JOIN DocumentAccounting ON DocumentAccountingItems.DocumentAccountingID=DocumentAccounting.ID 
WHERE DocumentAccounting.WorkflowStatus = 1 AND (DocumentAccountingItems.TreePath + '-') LIKE '{0}-%' AND DocumentAccounting.Date >= {1} AND DocumentAccounting.Date <= {2}"
                      , treePath, Helper.QoutedDate(mFrom), Helper.QoutedDate(mTo));
                DataRow row = DbProxy.Instance.FillRow(query);
                if (row != null)
                {
                    decimal due = Helper.GetDecimal(row[0], 0), owe = Helper.GetDecimal(row[1], 0);
                    vals.Add(index, due - owe);
                    return due - owe;
                }
                else
                {
                    vals.Add(index, 0);
                    return 0;
                }
            }
            else
            {
                // is it a Formula
                string formula = Helper.GetString(mRows[index], "Formula", null);
                if (!string.IsNullOrEmpty(formula))
                {
                    // check loop
                    if (visited.Contains(index))
                        return 0;
                    visited.Add(index);

                    // a formula like: r1-(r2*3+r3)-r1/2
                    // divide it to () chunks and evaluate each part
                    formula = formula.ToLower();
                    int lastIndex = formula.IndexOf(')');
                    while (lastIndex > 0)
                    {
                        int startIndex = formula.LastIndexOf('(', lastIndex);
                        formula = formula.Substring(0, startIndex) + Evaluate(formula.Substring(startIndex + 1, lastIndex - startIndex - 1)) + formula.Substring(lastIndex + 1);
                        lastIndex = formula.IndexOf(')');
                    }
                    decimal val = Evaluate(formula);
                    vals.Add(index, val);
                    return val;
                }
                else
                {
                    // it is a blank row
                    vals.Add(index, 0);
                }
            }
            return vals[index];
            //
        }
        decimal Evaluate(string formula)
        {
            // +-*/
            string[] plus = formula.Split('+');
            decimal res = 0;
            for (int i = 0; i < plus.Length; i++)
                res += EvaluateMinus(plus[i]);
            return res;
        }
        decimal EvaluateMinus(string formula)
        {
            string[] minus = formula.Split('-');
            if (minus.Length == 1)
                return EvaluateMul(formula);
            decimal res = EvaluateMul(minus[0]);
            for (int i = 1; i < minus.Length; i++)
                res -= EvaluateMul(minus[i]);
            return res;
        }
        decimal EvaluateMul(string formula)
        {
            if (string.IsNullOrEmpty(formula)) // in case of -2
                return 0;
            string[] mul = formula.Split('*');
            decimal res = 1;
            for (int i = 0; i < mul.Length; i++)
                res *= EvaluateDiv(mul[i]);
            return res;
        }
        decimal EvaluateDiv(string formula)
        {
            string[] divs = formula.Split('/');
            if (divs.Length == 1)
                return EvaluateVal(formula);

            decimal res = EvaluateVal(divs[0]);
            for (int i = 1; i < divs.Length; i++)
                res /= EvaluateVal(divs[i]);
            return res;
        }


        decimal EvaluateVal(string val)
        {
            if (string.IsNullOrEmpty(val) || val.Trim().Length == 0)
                return 0;
            if (val[0] == 'r' || val[0] == 'R')
            {
                int index = Helper.GetInt(val.Substring(1), -1) - mBaseIndex;
                return Evaluate(index - 1);
            }
            return decimal.Parse(val);
        }
    }
}