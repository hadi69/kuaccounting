﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reports_accounting_MyBook : System.Web.UI.Page
{
    System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
    TopicNature nature = TopicNature.Both;

    protected void Page_Load(object sender, EventArgs e)
    {
        msg.Visible = false;
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");

        mGrid.EmptyDataText = LangProvider.Instance["DocumentIsEmpty"];
        personGrid.EmptyDataText = LangProvider.Instance["DocumentIsEmpty"];
        checkGrid.EmptyDataText = LangProvider.Instance["DocumentIsEmpty"];

        if (Helper.GetInt(ddlBy.SelectedValue, 0) == 0)
        {
            totalBillsPanel.Visible = false;
            normalFilter.Visible = normalPanel.Visible = true;
        }
        else
        {
            totalBillsPanel.Visible = true;
            normalFilter.Visible = normalPanel.Visible = false;
        }

        if (!IsPostBack)
        {
            if (SiteSettings.DateType == DateType.en)
                mex.Mask = "";
            Page.Title = LangProvider.Instance["PaperBook"];
            Page.SetLang();
            IDataReader financeYearStartDate = DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");//0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            //mFromDate.Text = 
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);

            // show persons only when the user is admin
            panelPersons.Visible = false;
            if (SiteSettings.User.IsAdmin)
            {
                panelPersons.Visible = SiteSettings.User.IsAdmin;
                IDataReader persons = DbProxy.Instance.ExecuteReader("SELECT ID, ISNULL([dbo].[DisplayPerson](ID, 1), ''), ISNULL(Code, '') FROM Personnel WHERE Enabled = 1 AND Type=101");
                mPersons.Items.Clear();
                if (persons != null)
                {
                    while (persons.Read())
                    {
                        if (!persons.IsDBNull(0) && !persons.IsDBNull(1) && !persons.IsDBNull(2))
                            mPersons.Items.Add(new ListItem(persons.GetString(1) + " - کد: " + persons.GetString(2),
                                persons.GetInt32(0).ToString()));
                    }
                    persons.Close();
                }
                mPersons.SelectedValue = SiteSettings.User.PersonnelID.ToString();
            }

            doShow_Click(null, null);
        }
        mGrid.EmptyDataText = LangProvider.Instance["ListIsEmpty"];
    }

    string rowStyle = "GridRow";
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView record = e.Row.DataItem as DataRowView;
            DateTime dt = Helper.GetDateTime(record["Date"], Helper.NullDate);

            Label lblGridDate = (Label)e.Row.FindControl("lblGridDate");
            if (lblGridDate != null)
            {
                lblGridDate.Text = string.Format("<span title='{1}'>{0}</span>", Helper.FormatDate(Helper.GetDateTime(record["Date"], Helper.NullDate), null), Helper.FormatDateFull(Helper.GetDateTime(record["Date"], Helper.NullDate)));
            }

            if (mInitRemain.Checked && Helper.GetString(record["TopicCode"], "") == "")
            {
                e.Row.Cells[1].Text = LangProvider.Instance["InitRemain"];
                e.Row.Cells[2].Text = "";
            }


            //if (!string.IsNullOrEmpty(Helper.GetString()))
            rowStyle = rowStyle == "GridAltRow" ? "GridRow" : "GridAltRow";
            e.Row.CssClass = rowStyle;

            decimal _due = Helper.GetDecimal(record["Due"], 0);
            decimal _owe = Helper.GetDecimal(record["Owe"], 0);
            _remain += _due - _owe;

            ((Label)e.Row.FindControl("owe")).Text = SiteSettings.ToMonetary(_owe, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("due")).Text = SiteSettings.ToMonetary(_due, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            decimal _r = SiteSettings.ToMonetary(_remain, DateTime.Now);
            ((Label)e.Row.FindControl("remaining")).Text = (_r < 0) ? (-1 * _r).ToString("N4", Helper.NumberCulture).Replace(".0000", "") : _r.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            if (!mShowPaperBook.Checked)
            {
                e.Row.Cells[3].Text = LangProvider.Instance["TotalFlowTillNow"];
                e.Row.Cells[8].Text = LangProvider.Instance["TopicLevelTotal"];
            }
            else
                e.Row.Cells[8].Text = LangProvider.Instance["TopicLevelPaper"];

            sumDue += Helper.GetDecimal(record["Due"], 0);
            sumOwe += Helper.GetDecimal(record["Owe"], 0);
            e.Row.Cells[7].Text = (_remain < 0) ? LangProvider.Instance["Li"] : LangProvider.Instance["Cr"];
            if (nature != TopicNature.Both)
                if ((nature == TopicNature.Owe && _remain > 0) || (nature == TopicNature.Due && _remain < 0))
                    Helper.SignifyNegative(e.Row.Cells[9]);
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblTotal = (Label)e.Row.FindControl("oweTotal");
            lblTotal.Text = SiteSettings.ToMonetary(sumOwe, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            lblTotal = (Label)e.Row.FindControl("dueTotal");
            lblTotal.Text = SiteSettings.ToMonetary(sumDue, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            lblTotal = (Label)e.Row.FindControl("remainingTotal");
            decimal r = SiteSettings.ToMonetary(sumDue - sumOwe, DateTime.Now);
            lblTotal.Text = (r < 0) ? (-1 * (r)).ToString("N4", Helper.NumberCulture).Replace(".0000", "") : (r).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            if (nature != TopicNature.Both)
                if ((nature == TopicNature.Owe && (r) > 0) || (nature == TopicNature.Due && (r) < 0))
                    Helper.SignifyNegative(e.Row.Cells[9]);

            lblTotal = (Label)e.Row.FindControl("finalDiagnosis");
            lblTotal.Text = (r < 0) ? LangProvider.Instance["Li"] : LangProvider.Instance["Cr"];
        }
    }
    decimal sumOwe = 0, sumDue = 0, _remain = 0;
    protected void mGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        BindGrid();
    }
    protected void BindGrid()
    {
        sumOwe = sumDue = _remain = 0;
        DateTime start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        DateTime end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);

        mGrid.Columns[9].Visible = mGrid.Columns[10].Visible = mShowPaperBook.Checked;
        mGrid.ShowFooter = mShowRemain.Checked;

        DataTable items = null;
        if (mShowPaperBook.Checked)
            items = AccController.GetPaperBook(Null.NullInteger
                 , start
                 , end
                 , 0, int.MaxValue - 1, Null.NullInteger, PersonnelID
                 , Null.NullInteger, 0, true, mInitRemain.Checked
                 , -1, SortCol, SortOrder);
        else
            items = AccController.GetPaperBookSum(Null.NullInteger
             , start
             , end
             , 0, int.MaxValue - 1, PersonnelID
             , Null.NullInteger, 0, true, mInitRemain.Checked, SortCol, SortOrder);

        try
        {
            mGrid.DataSource = items;
            mGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Dispose();
            }
        }

    }
    protected string SortCol
    {
        get
        {
            return Helper.GetString(ViewState["SortCol"], null);
        }
        set
        {
            ViewState["SortCol"] = value;
        }
    }
    protected string SortOrder
    {
        get
        {
            return Helper.GetString(ViewState["SortOrder"], "asc");
        }
        set
        {
            ViewState["SortOrder"] = value;
        }
    }
    protected void doShow_Click(object sender, EventArgs e)
    {
        sumOwe = sumDue = _remain = 0;
        DateTime start = Helper.ParseDate(mFromDate.Text, Null.NullDate);
        DateTime end = Helper.ParseDate(mToDate.Text, Null.NullDate);
        if (Null.IsNull(start))
        {
            msg.Text = LangProvider.Instance["InvalidStartDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(end))
        {
            msg.Text = LangProvider.Instance["InvalidEndDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(start) || Null.IsNull(end))
            return;
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);

        if (Helper.GetInt(ddlBy.SelectedValue, 0) == 0)
        {
            BindGrid();

            ShowPersonGrid();
            BindChecks();

            factorsPanel.Visible = mShowFactors.Checked;
            if (mShowFactors.Checked)
                ShowFactors();
            else
            {
                mFactors.DataSource = null;
                mFactors.DataBind();
            }
        }
        else
        {
            ShowTotalBills(start, end);
        }

    }
    int PersonnelID
    {
        get
        {
            if (panelPersons.Visible)
                return Helper.GetInt(mPersons.SelectedValue, Null.NullInteger);
            return SiteSettings.User.PersonnelID;
        }
    }
    protected void doExcel_Click(object sender, EventArgs e)
    {
        if (Helper.GetInt(ddlBy.SelectedValue, 0) == 0)
            Exporter.ExportExcel(Response
                , new List<string>(new string[] { mGridTitle.Text, checkGridTitle.Text, personGridTitle.Text })
                , new List<GridView>(new GridView[] { mGrid, checkGrid, personGrid }));
        else
            Exporter.ExportExcel(Response, jpMiniHeaderTotalBills.Text, mTotalBillsGrid);
    }

    #region personGrid
    string mVouchersRowStyle = "GridRow";
    decimal mVoucherssumOwe = 0, mVoucherssumDue = 0;
    protected void personGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.Common.DbDataRecord record = e.Row.DataItem as System.Data.Common.DbDataRecord;
            DateTime dt = Helper.GetDateTime(record["Date"], Helper.NullDate);
            Label lblPersonDate = (Label)e.Row.FindControl("lblPersonDate");
            if (dt != Helper.NullDate && lblPersonDate != null)
            {
                lblPersonDate.Text = Helper.FormatDateFull(Helper.GetDateTime(dt, Helper.NullDate));
            }
            mVouchersRowStyle = mVouchersRowStyle == "GridAltRow" ? "GridRow" : "GridAltRow";
            e.Row.CssClass = mVouchersRowStyle;

            Label owe = (Label)e.Row.FindControl("owe");
            owe.Text = Helper.GetDecimal(record["Owe"], 0).ToString("N0");

            Label due = (Label)e.Row.FindControl("due");
            due.Text = Helper.GetDecimal(record["Due"], 0).ToString("N0");

            mVoucherssumDue += Helper.GetDecimal(record["Due"], 0);
            mVoucherssumOwe += Helper.GetDecimal(record["Owe"], 0);
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblTotal = (Label)e.Row.FindControl("oweTotal");
            lblTotal.Text = mVoucherssumOwe.ToString("N0");

            lblTotal = (Label)e.Row.FindControl("dueTotal");
            lblTotal.Text = mVoucherssumDue.ToString("N0");
        }
    }
    private void ShowPersonGrid()
    {
        // not needed yet
        return;

        mVoucherssumOwe = mVoucherssumDue = 0;
        DateTime start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        DateTime end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);

        string qry = string.Format(@"SELECT DocumentAccounting.ID, DocumentAccountingItems.SortIndex, DocumentAccountingItems.ID
, DocumentAccounting.Date, DocumentAccounting.No, dbo.TreePathToString(DocumentAccountingItems.TreePath, 1) AS TreePathName
, dbo.TreePathToTopicCode(DocumentAccountingItems.TreePath) AS TopicCode
, dbo.PersonnelTreePathToString(DocumentAccountingItems.SecPersonnelID, DocumentAccountingItems.SecTreePath, 1) PersonnelTreePathName
, DocumentAccountingItems.Comments, DocumentAccountingItems.Owe, DocumentAccountingItems.Due 
, [dbo].[DisplayPerson](Personnel.ID, 0) AS PersonnelName
FROM DocumentAccounting 
INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID 
WHERE DocumentAccounting.WorkflowStatus = 1 AND {0} <= DocumentAccounting.Date AND DocumentAccounting.Date <= {1} 
_WHERE1_

UNION
SELECT DocumentAccounting.ID, DocumentAccountingItems.SortIndex, DocumentAccountingItems.ID
, DocumentAccounting.Date, DocumentAccounting.No, dbo.TreePathToString(dbo.GetSetting2('CheckTreePath', DocumentAccounting.ProgramID), 1) AS TreePathName
, dbo.TreePathToTopicCode(dbo.GetSetting2('CheckTreePath', DocumentAccounting.ProgramID)) AS TopicCode
, dbo.TreePathToString(Checks.TreePath, 1) PersonnelTreePathName
, DocumentAccountingItems.Comments, DocumentAccountingItems.Owe, DocumentAccountingItems.Due 
, [dbo].[DisplayPerson](Personnel.ID, 0) AS PersonnelName
FROM DocumentAccounting 
INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
INNER JOIN Checks ON DocumentAccountingItems.CheckID=Checks.ID
LEFT OUTER JOIN Personnel ON Checks.PersonnelID=Personnel.ID 
WHERE DocumentAccounting.WorkflowStatus = 1 AND Checks.TreePath IS NOT NULL AND {0} <= DocumentAccounting.Date AND DocumentAccounting.Date <= {1} 
_WHERE2_

UNION
SELECT DocumentAccounting.ID, DocumentAccountingItems.SortIndex, DocumentAccountingItems.ID
, DocumentAccounting.Date, DocumentAccounting.No, dbo.TreePathToString(dbo.GetSetting2('PayCheckTreePath', DocumentAccounting.ProgramID), 1) AS TreePathName
, dbo.TreePathToTopicCode(dbo.GetSetting2('PayCheckTreePath', DocumentAccounting.ProgramID)) AS TopicCode
, dbo.TreePathToString(PayCheck.TreePath, 1) PersonnelTreePathName
, DocumentAccountingItems.Comments, DocumentAccountingItems.Owe, DocumentAccountingItems.Due 
, [dbo].[DisplayPerson](Personnel.ID, 0) AS PersonnelName
FROM DocumentAccounting 
INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
INNER JOIN PayCheck ON DocumentAccountingItems.PayCheckID=PayCheck.ID
LEFT OUTER JOIN Personnel ON PayCheck.ReceiverID = Personnel.ID 
WHERE DocumentAccounting.WorkflowStatus = 1 AND PayCheck.TreePath IS NOT NULL AND {0} <= DocumentAccounting.Date AND DocumentAccounting.Date <= {1} 
_WHERE2_ 
"
            , Helper.QoutedDateTime(start), Helper.QoutedDateTime(end));

        if (PersonnelID != Null.NullInteger)
            qry = qry.Replace("_WHERE1_", " AND DocumentAccountingItems.SecPersonnelID = " + PersonnelID)
                .Replace("_WHERE2_", " AND Personnel.ID = " + PersonnelID);
        qry = qry.Replace("_WHERE1_", " ").Replace("_WHERE2_", " ");
        qry += " ORDER BY Date, CAST(DocumentAccounting.No AS INT), DocumentAccounting.ID, DocumentAccountingItems.SortIndex, DocumentAccountingItems.ID";
        IDataReader items = DbProxy.Instance.ExecuteReader(qry);
        try
        {
            personGrid.DataSource = items;
            personGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Close();
                items.Dispose();
            }
        }
    }
    #endregion

    #region checkGrid
    void BindChecks()
    {
        using (DbProxy proxy = new DbProxy())
        {
            string qry = string.Format(@"SELECT ID, DueDate, Serial, Amount, N'{1}' AS Type FROM PayCheck WHERE Status=0 AND ReceiverID = {0}
UNION
SELECT ID, DueDate, [No] AS Serial, Amount, N'{2}' AS Type FROM Checks WHERE Status = 0 AND PersonnelID = {0}", PersonnelID, LangProvider.Instance["DAModePayCheck"], LangProvider.Instance["ReceiveCheck"]);
            IDataReader reader = proxy.ExecuteReader(qry);
            checkGrid.DataSource = reader;
            checkGrid.DataBind();
        }
    }
    protected void checkGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.Common.DbDataRecord record = e.Row.DataItem as System.Data.Common.DbDataRecord;

            DateTime dueDate = Helper.GetDateTime(record["DueDate"], Helper.NullDate);

            Label lblCheckDueDate = (Label)e.Row.FindControl("lblCheckDueDate");
            if (dueDate != Helper.NullDate && lblCheckDueDate != null)
            {
                lblCheckDueDate.Text = Helper.ToFaFormat(Helper.GetDateTime(dueDate, Helper.NullDate));
            }
        }
    }
    #endregion

    #region Factors
    string mFactorsRowStyle = "GridRow";
    int lastMode = -1, lastFactorID = -1;
    protected void mFactors_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.Common.DbDataRecord record = e.Row.DataItem as System.Data.Common.DbDataRecord;
            DateTime dt = Helper.GetDateTime(record["Date"], Helper.NullDate);
            e.Row.Cells[1].Text = Helper.ToFaFormat(Helper.GetDateTime(dt, Helper.NullDate));
            int id = Helper.GetInt(record["ID"], Null.NullInteger);
            int mode = Helper.GetInt(record["Mode"], Null.NullInteger);
            if (id != lastFactorID || mode != lastMode)
            {
                lastFactorID = id;
                lastMode = mode;
                mFactorsRowStyle = mFactorsRowStyle == "GridAltRow" ? "GridRow" : "GridAltRow";
            }
            e.Row.CssClass = mFactorsRowStyle;
            e.Row.Cells[7].Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["Price"], 0), DateTime.Now).ToString("N2", Helper.NumberCulture);
            e.Row.Cells[8].Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["Discount"], 0), DateTime.Now).ToString("N2", Helper.NumberCulture);
            e.Row.Cells[9].Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["Tax"], 0), DateTime.Now).ToString("N2", Helper.NumberCulture);

            string table = "", title = "";
            if (mode == 4)
            {
                table = "Purchase";
                title = LangProvider.Instance["SalesPurchase"];
            }
            else if (mode == 7)
            {
                table = "StoreDraft";
                title = LangProvider.Instance["SalesInvoice"];
            }
            else if (mode == 6)
            {
                table = "Returns";
                title = LangProvider.Instance["ReturnInvoice"];
            }
            else if (mode == 5)
            {
                table = "Refuse";
                title = LangProvider.Instance["RefuseInvoice"];
            }
            e.Row.Cells[2].Text = title;
            e.Row.Cells[3].Text = string.Format("<a href='../../list.aspx?table={0}&menu=accountinga&id={1}'>{2}</a>", table, record["ID"], record["NOSerial"]);
        }
    }
    private void ShowFactors()
    {
        // find voucher IDs
        DateTime start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        DateTime end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);

        string qry1 = string.Format(@"SELECT DocumentAccounting.ID FROM DocumentAccounting 
INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID 
LEFT OUTER JOIN Ward ON DocumentAccountingItems.ProjectID=Ward.ID 
WHERE DocumentAccounting.WorkflowStatus = 1 AND {0} <= Date AND Date <= {1} "
            , Helper.QoutedDateTime(start), Helper.QoutedDateTime(end));

        qry1 += " AND DocumentAccountingItems.PersonnelID = " + PersonnelID;
        qry1 += " AND DocumentAccountingItems.TreePath IN (SELECT Trees.TreePath FROM Trees WHERE Trees.PersonNeed=1) ";

        IDataReader dr = DbProxy.Instance.ExecuteReader(qry1);
        List<int> IDs = new List<int>();
        while (dr.Read())
            IDs.Add(dr.GetInt32(0));
        dr.Close();

        string qry = @"SELECT 4 Mode, Purchase.ID, ISNULL(Purchase.ReceiptDate, Purchase.Date) Date, Purchase.NOSerial, StoreServiceName, UnitName, Amount, Price, Discount, Tax 
FROM PurchaseItemsView LEFT JOIN Purchase ON PurchaseItemsView.PurchaseID=Purchase.ID 
WHERE Purchase.ID IN (SELECT OtherID FROM DocumentAccounting WHERE Mode=4 _MORE_)

UNION

SELECT 7 Mode, StoreDraft.ID, ISNULL(StoreDraft.FactorDate, StoreDraft.Date) Date, StoreDraft.NOSerial, StoreServiceName, UnitName, DeliveredAmount Amount, Price, Discount, Tax 
FROM StoreDraftItemsView LEFT JOIN StoreDraft ON StoreDraftItemsView.StoreDraftID=StoreDraft.ID
WHERE StoreDraft.ID IN (SELECT OtherID FROM DocumentAccounting WHERE Mode=7 _MORE_)

UNION

SELECT 5 Mode, Refuse.ID, Refuse.RefuseDate Date, Refuse.NOSerial, StoreServiceName, UnitName, Num Amount, Price, Discount, Tax 
FROM RefuseItemView LEFT JOIN Refuse ON RefuseItemView.RefuseID=Refuse.ID
WHERE Refuse.ID IN (SELECT OtherID FROM DocumentAccounting WHERE Mode=5 _MORE_)

UNION

SELECT 6 Mode, Returns.ID, Returns.ReturnDate Date, Returns.NOSerial, StoreServiceName, UnitName, Num Amount, Price, Discount, Tax 
FROM ReturnItemView LEFT JOIN Returns ON ReturnItemView.ReturnID=Returns.ID
WHERE Returns.ID IN (SELECT OtherID FROM DocumentAccounting WHERE Mode=6 _MORE_)
";
        string more = "";
        if (IDs.Count > 0)
        {

            if (IDs.Count == 1)
                more += " AND DocumentAccounting.ID = " + IDs[0];
            else
            {
                more += " AND DocumentAccounting.ID IN (";
                for (int i = 0; i < IDs.Count - 1; i++)
                    more += IDs[i] + ", ";
                more += IDs[IDs.Count - 1] + ")";
            }
        }
        qry += " ORDER BY Mode, Date, NOSerial";
        qry = qry.Replace("_MORE_", more);
        IDataReader items = DbProxy.Instance.ExecuteReader(qry);
        try
        {
            mFactors.DataSource = items;
            mFactors.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Close();
                items.Dispose();
            }
        }
    }
    #endregion


    #region TotalBills
    string mTotalBillsRowStyle = "GridRow";
    int lastMode1 = -1, lastTotalBillsID = -1;
    decimal checksNotPassed = 0;
    TopicNature totalBillNature = TopicNature.Both;
    protected void mTotalBillsGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.Common.DbDataRecord record = e.Row.DataItem as System.Data.Common.DbDataRecord;
            DateTime dt = Helper.GetDateTime(record["Date"], Helper.NullDate);
            e.Row.Cells[1].Text = Helper.FormatDate(dt, null);
            int id = Helper.GetInt(record["ID"], Null.NullInteger);
            int mode = Helper.GetInt(record["Mode"], Null.NullInteger);
            if (id != lastTotalBillsID || mode != lastMode1)
            {
                lastTotalBillsID = id;
                lastMode1 = mode;
                mTotalBillsRowStyle = mTotalBillsRowStyle == "GridAltRow" ? "GridRow" : "GridAltRow";
            }
            e.Row.CssClass = mTotalBillsRowStyle;
            if (id == 0)
                e.Row.Cells[1].Text = "01-01-...";

            string table = "";
            if (mode == 4)
            {
                table = "Purchase";

            }
            else if (mode == 7)
            {
                table = "StoreDraft";

            }
            else if (mode == 0 || mode == 3 || mode == 8 || mode == 14 || mode == 15)
            {
                table = "DocumentAccounting";
                if (mode == 3)
                    checksNotPassed += SiteSettings.ToMonetary(Helper.GetDecimal(record["Due"], 0), dt);

            }
            else if (mode == 6)
            {
                table = "Returns";

            }
            else if (mode == 5)
            {
                table = "Refuse";

            }
            e.Row.Cells[2].Text = string.Format("<a href='../../list.aspx?table={0}&menu=accountinga&id={1}'>{2}</a>", table, record["ID"], record["NO"]);
            decimal _due = SiteSettings.ToMonetary(Helper.GetDecimal(record["Due"], 0), dt);
            decimal _owe = SiteSettings.ToMonetary(Helper.GetDecimal(record["Owe"], 0), dt);
            _remain += _due - _owe;

            sumDue += _due;
            sumOwe += _owe;

            ((Label)e.Row.FindControl("owe")).Text = _owe.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("due")).Text = _due.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("remaining")).Text = (_remain < 0) ? (-1 * _remain).ToString("N4", Helper.NumberCulture).Replace(".0000", "") : _remain.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            if (totalBillNature == TopicNature.Both)
                totalBillNature = Helper.GetEnum(DbProxy.Instance.FillScalarString("SELECT Nature FROM Personnel WHERE ID = " + Helper.GetInt(mPersons.SelectedValue, -1)), TopicNature.Both);
            if ((totalBillNature == TopicNature.Owe && _remain > 0) || (totalBillNature == TopicNature.Due && _remain < 0))
                Helper.SignifyNegative(e.Row.Cells[8]);
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            //Label lblTotal = (Label)e.Row.FindControl("oweTotal");
            //slblTotal.Text = sumOwe.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            Label lblTotal1 = (Label)e.Row.FindControl("ChecksNotPassed");
            lblTotal1.Text = LangProvider.Instance["ChecksNotPassed"] + " : " + checksNotPassed.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            decimal remain = sumDue - sumOwe;
            if ((totalBillNature == TopicNature.Owe && remain > 0) || (totalBillNature == TopicNature.Due && remain < 0))
                Helper.SignifyNegative(e.Row.Cells[8]);
            string nature = LangProvider.Instance["Creditor"];
            if (remain < 0)
            {
                remain = -remain;
                nature = LangProvider.Instance["Debtor"];
            }
            Label lblTotal = (Label)e.Row.FindControl("remainingTotal");
            lblTotal.Text = LangProvider.Instance["Remained"] + " : " + remain.ToString("N4", Helper.NumberCulture).Replace(".0000", "") + "  " + nature;
        }

    }
    private void ShowTotalBills(DateTime startDate, DateTime endDate)
    {
        string qry;

        DateTime beginDate = new DateTime(startDate.Year, 1, 1, 0, 0, 0, 0);
        qry = @"SELECT '' ID , 0 Mode,  '' Date, '' No, 'مانده قبلی' Comments, 0 Num, 0 Worth, SUM( DocumentAccountingItems.Owe) AS Owe, SUM(DocumentAccountingItems.Due) AS Due
FROM DocumentAccounting 
INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID  WHERE _More_

UNION

SELECT DocumentAccounting.ID,DocumentAccounting.Mode, DocumentAccounting.Date, DocumentAccounting.No, 'دريافت و پرداخت نقدي' Comments, 0 Num, 0 Worth, DocumentAccountingItems.Owe, DocumentAccountingItems.Due 
FROM DocumentAccounting 
INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID  WHERE Mode = 0 _MORE_

UNION

SELECT DocumentAccounting.ID,DocumentAccounting.Mode, DocumentAccounting.Date, DocumentAccounting.No, 'دريافت و پرداخت چک' Comments, 0 Num, 0 Worth, DocumentAccountingItems.Owe, DocumentAccountingItems.Due 
FROM DocumentAccounting 
INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID  WHERE Mode IN(3,8) _MORE_
UNION

SELECT DocumentAccounting.ID,DocumentAccounting.Mode, DocumentAccounting.Date, DocumentAccounting.No, 'برگشت چک' Comments, 0 Num, 0 Worth, DocumentAccountingItems.Owe, DocumentAccountingItems.Due 
FROM DocumentAccounting 
INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID  WHERE OtherID IN(14,15) _MORE_
UNION

SELECT Purchase.ID,4 Mode, ISNULL(Purchase.ReceiptDate, Purchase.Date) Date, Purchase.NOSerial No, StoreServiceName Comments, Amount Num, Price Worth, 0 Owe, Amount * Price  Due   
FROM PurchaseItemsView LEFT JOIN Purchase ON PurchaseItemsView.PurchaseID=Purchase.ID 
WHERE Purchase.ID IN (SELECT OtherID
FROM DocumentAccounting 
INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID  WHERE Mode=4 _MORE_)

UNION

SELECT Purchase.ID,4 Mode, ISNULL(Purchase.ReceiptDate, Purchase.Date) Date, NOSerial No, 'تخفیف' Comments, 0 Num, 0 Worth, TotalDiscount Owe, 0 Due   
FROM Purchase 
WHERE Purchase.ID IN (SELECT OtherID
FROM DocumentAccounting 
INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID  WHERE Mode=4 _MORE_)


UNION

SELECT StoreDraft.ID, 7 Mode, ISNULL(StoreDraft.FactorDate, StoreDraft.Date) Date, StoreDraft.NOSerial No, StoreServiceName  Comments, DeliveredAmount Num, Price Worth, DeliveredAmount * Price Owe, 0 Due  
FROM StoreDraftItemsView LEFT JOIN StoreDraft ON StoreDraftItemsView.StoreDraftID=StoreDraft.ID
WHERE StoreDraft.ID IN (SELECT OtherID
FROM DocumentAccounting 
INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID  WHERE Mode=7 _MORE_ 
 )
 
 UNION

SELECT StoreDraft.ID, 7 Mode, ISNULL(StoreDraft.FactorDate, StoreDraft.Date) Date, NOSerial No, 'تخفیف' Comments, 0 Num, 0 Worth, 0 Owe, TotalDiscount Due   
FROM StoreDraft 
WHERE StoreDraft.ID IN (SELECT OtherID
FROM DocumentAccounting 
INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID  WHERE Mode=7 _MORE_)


UNION

SELECT Refuse.ID, 5 Mode, Refuse.RefuseDate Date, Refuse.NOSerial No, StoreServiceName Comments, Num , Price Worth, Num * Price Owe , 0 Due
FROM RefuseItemView LEFT JOIN Refuse ON RefuseItemView.RefuseID=Refuse.ID
WHERE Refuse.ID IN (SELECT OtherID
FROM DocumentAccounting 
INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID  WHERE Mode=5 _MORE_)

UNION

SELECT Returns.ID, 6 Mode, Returns.ReturnDate Date, Returns.NOSerial No, StoreServiceName  Comments, Num , Price Worth, 0 Owe ,  Num * Price Due
FROM ReturnItemView LEFT JOIN Returns ON ReturnItemView.ReturnID=Returns.ID
WHERE Returns.ID IN (SELECT OtherID
FROM DocumentAccounting 
INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID  WHERE Mode=6 _MORE_)
";

        string more = " AND DocumentAccounting.WorkflowStatus = 1 AND PersonnelID =" + PersonnelID;
        string More = "DocumentAccounting.WorkflowStatus = 1 AND PersonnelID =" + PersonnelID;
        More += " AND Date BETWEEN " + Helper.QoutedDateTime(beginDate) + "AND" + Helper.QoutedDateTime(startDate);
        more += " AND Date BETWEEN " + Helper.QoutedDateTime(startDate) + "AND" + Helper.QoutedDateTime(endDate);
        qry = qry.Replace("_MORE_", more);
        qry = qry.Replace("_More_", More);
        qry += " ORDER BY Date, NO";
        _remain = 0;
        IDataReader items = DbProxy.Instance.ExecuteReader(qry);
        try
        {
            mTotalBillsGrid.DataSource = items;
            mTotalBillsGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Close();
                items.Dispose();
            }
        }

    }
    #endregion

}