﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ShopProfit.aspx.cs" Inherits="reports_accounting_ShopProfit" %>

<%@ Register Src="../../controls/ReportSetting.ascx" TagName="ReportSetting" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>
<%--    <uc1:ReportSetting ID="mReportSetting" runat="server" />--%>
    <div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label1" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="mFromDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="mToDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
             <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label8" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Store</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mStores" runat="server" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label3" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.CommodityName</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mServiceName" runat="server" CssClass="mServiceName chosen-select chosen-rtl"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label4" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.ServiceType</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mServiceTypes" runat="server" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label5" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Person</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mPersons" runat="server" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label9" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.PersonnelGroup</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mPersonGroups" runat="server" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container hidden">
                <div class="form-group">
                    <asp:Label ID="Label7" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned padding-0">Lang.PersonnelZoneGroup</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mPZGroup" runat="server" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container hidden">
                <div class="form-group">
                    <asp:Label ID="Label6" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.ServiceZoneGroup</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mSZGroup" runat="server" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group-container">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4">&nbsp;</div>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox runat="server" ID="mBaseOnFactor" ClientIDMode="Static" Text="Lang.BaseOnFactor" CssClass="mbaseonfactor" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-lg-9 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcel_Click" Enabled="false" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <input type="button" id="print" class="btn btn-xs btn-dark-blue btn-block" value="" />
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="panelDropDowns" runat="server" CssClass="well callout padding-5 hidden">
        <input type="hidden" runat="server" id="mQueryBuilder" class="mQueryBuilder" />
        <input type="hidden" runat="server" id="mQueryBuilderSql" class="mQueryBuilderSql" />
        <div class="hidden">
            <asp:DropDownList runat="server" ID="mCommodityGroupings" CssClass="mCommodityGroupings"></asp:DropDownList>
            <asp:DropDownList runat="server" ID="mPersonGroupings" CssClass="mPersonGroupings"></asp:DropDownList>
            <asp:DropDownList runat="server" ID="mCommodityGroup" CssClass="mCommodityGroup"></asp:DropDownList>
            <asp:DropDownList runat="server" ID="mPersonGroup" CssClass="mPersonGroup"></asp:DropDownList>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <h5 class="dashed-bottom padding-5-bottom yekan-family">
                    <asp:Label runat="server">Lang.Groups</asp:Label>
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="ltr builder">
                    <div id="builder"></div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="col-xs-12">
        <asp:Label runat="server" ID="msg" CssClass="label-30 alert alert-danger block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
    </div>
    <asp:Panel ID="voucherDetailPanel" runat="server" Visible="False">
        <JP:MiniHeader runat="server" Text="Lang.ShopProfit" ID="mGridTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="mGrid" AutoGenerateColumns="False" CssClass=" maingrid ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true" OnSorting="mGrid_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Type" HeaderText="Lang.Type" ItemStyle-HorizontalAlign="Center" SortExpression="Type" />
                        <asp:BoundField DataField="ServiceName" HeaderText="Lang.Commodity" ItemStyle-HorizontalAlign="Center" SortExpression="ServiceName" />
                        <asp:BoundField DataField="FactorDate" HeaderText="Lang.Date" ItemStyle-HorizontalAlign="Center" SortExpression="FactorDate" />
                        <asp:BoundField DataField="NOSerial" HeaderText="Lang.FactorNo" ItemStyle-HorizontalAlign="Center" SortExpression="NOSerial" />
                        <asp:BoundField DataField="DeliveredAmount" HeaderText="Lang.NumOrValue" ItemStyle-HorizontalAlign="Center" SortExpression="DeliveredAmount" />
                        <asp:BoundField DataField="UnitCurrencyConverter" HeaderText="Lang.UnitCurrencyConverter" ItemStyle-HorizontalAlign="Center" SortExpression="UnitCurrencyConverter" />
                        <asp:BoundField DataField="BasePrice" HeaderText="Lang.BuyPrice" ItemStyle-HorizontalAlign="Center" SortExpression="BasePrice" />
                        <asp:BoundField DataField="Price" HeaderText="Lang.Price" ItemStyle-HorizontalAlign="Center" SortExpression="Price" />
                        <asp:BoundField DataField="Discount" HeaderText="Lang.Discount" ItemStyle-HorizontalAlign="Center" SortExpression="Discount" />
                        <asp:TemplateField HeaderText="Lang.Profit" ItemStyle-HorizontalAlign="Center" SortExpression="Profit">
                            <ItemTemplate>
                                <asp:Label ID="Profit" runat="server" Text='<%#Eval("Profit") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="SumProfit" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
    <script>
        $(function () {
            $('#print').attr('value', Lang.Print);
            $('#print').click(function () {
                printTable('.ScrollPanelCartable', '../../', Lang.Cardex);
            });
            $('.GridRow,.GridAltRow').click(function () {
                $(this).toggleClass('selected');
            });
            dateSelect('.dateselect');
            $('.monetary').keyup(function () {
                $(this).val(numberWithCommas(stripCommas($(this).val())));
            });
            // sub sums for factors
            $('.maingrid .subSum').each(function () {
                // fa='{0}' fp='{1}' fd='{2}' ft='{3}' fs='{4}'
                var me = $(this);
                var row = '<tr class="SumRow"><td class="row-cell">&nbsp;</td><td>&nbsp;</td>';
                row += '<td>' + Lang.FactorTotalDiscount + '</td>';
                row += '<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>';
                row += '<td>' + numberWithCommas(me.attr('td')) + '</td>';
                row += '<td>' + numberWithCommas(me.attr('l')) + '</td>';
                row += '</tr>';
                me.closest('tr').before(row);
            });
            //postback, formatItem, formatResult, more, _autourl, _width, moreQS
            var _autourl = '../../get.aspx?op=Auto&table=Service&qd=service2auto' + appendTime();
            autoCompleteFree('Service', 'serviceauto', ".mServiceName", "#justfurfun", null, null, null, null, _autourl);
            $("#bar").prepend(breadcrumb({
                title: Lang.ShopProfit,
                parent: {
                    title: Lang.Reports,
                    url: 'menu/reports.aspx'
                }
            }));
            setClassReports();

            var sgg = {};
            $('.mCommodityGroupings option').each(function () {
                sgg[$(this).attr('value')] = $(this).text();
            });
            var pgg = {};
            $('.mPersonGroupings option').each(function () {
                pgg[$(this).attr('value')] = $(this).text();
            });
            var cog = {};
            $('.mCommodityGroup option').each(function () {
                cog[$(this).attr('value')] = $(this).text();
            });
            var peg = {};
            $('.mPersonGroup option').each(function () {
                peg[$(this).attr('value')] = $(this).text();
            });
            //if ($jQuery1_9('#builder').length > 0)
            $jQuery1_9('#builder').queryBuilder({
                filters: [
                    {
                        id: 'SGCategory',
                        label: Lang.ServiceGroupLable,
                        type: 'integer',
                        input: 'select',
                        values: sgg,
                        operators: ['equal', 'not_equal']
                    },
                    {
                        id: 'PGCategory',
                        label: Lang.PersonsGroupLable,
                        type: 'integer',
                        input: 'select',
                        values: pgg,
                        operators: ['equal', 'not_equal']
                    },
                    {
                        id: 'SCategory',
                        label: Lang.ServiceType,
                        type: 'integer',
                        input: 'select',
                        values: cog,
                        operators: ['equal', 'not_equal']
                    },
                    {
                        id: 'PCategory',
                        label: Lang.PersonnelGroup,
                        type: 'integer',
                        input: 'select',
                        values: peg,
                        operators: ['equal', 'not_equal']
                    }
                ]
            });
            // save rules before post back
            $('.doShow').click(function () {
                var rules = $jQuery1_9('#builder').queryBuilder('getRules');
                var rulesStr = JSON.stringify(rules);
                // alert(rulesStr);
                $('.mQueryBuilder').val(rulesStr);
                $('.mQueryBuilderSql').val($jQuery1_9('#builder').queryBuilder('getSQL', false, false).sql);
            });
            //reload saved rules
            $jQuery1_9('#builder').queryBuilder('reset');
            var ruleStr = $('.mQueryBuilder').val();
            if (ruleStr != '') {
                var rule = eval("(" + ruleStr + ")");
                try {
                    $jQuery1_9('#builder').queryBuilder('setRules', rule);
                } catch (e) {
                }
            }
        });
    </script>
</asp:Content>


