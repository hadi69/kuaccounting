﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Services.aspx.cs" Inherits="reports_accounting_Services" %>

<%@ Register Src="../../controls/ReportSetting.ascx" TagName="ReportSetting" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>
  <%--  <uc1:ReportSetting ID="mReportSetting" runat="server" />--%>
    <asp:Panel ID="panelDBs" runat="server" CssClass="well callout padding-5">
        <div class="row">
            <div class="col-xs-12">
                <h5 class="dashed-bottom padding-5-bottom yekan-family">
                    <asp:Label runat="server">Lang.DataBase</asp:Label>
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <asp:CheckBoxList runat="Server" ID="mDBList" RepeatLayout="Flow" RepeatDirection="Horizontal" />
            </div>
        </div>
    </asp:Panel>
    <div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label7" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.Based</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:DropDownList runat="server" ID="ddlBy" CssClass="chosen-select chosen-rtl" AutoPostBack="True">
                            <asp:ListItem Text="Lang.NormalReport" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Lang.IncomeOutcomeDetail" Value="1"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" AssociatedControlID="mFromDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="mFromDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label3" runat="server" AssociatedControlID="mToDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="mToDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <asp:CheckBox runat="server" ID="ShowWithoutOutcome" Text="Lang.ShowWithoutOutcome" />
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label1" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.CommodityName</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mServiceName" runat="server" CssClass=" mServiceName chosen-select chosen-rtl"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label5" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Store</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mStores" runat="server" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
           <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label6" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.ServiceType</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mServiceType" runat="server" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 form-group-container" id="AmountWhere" runat="server">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label ID="Label8" runat="server">Lang.TotalStock</asp:Label>
                        <asp:Label ID="Label4" runat="server">Lang.From</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-3 form-col-without-padding">
                        <asp:TextBox ID="mFromRemain" runat="server" CssClass="T2 form-control input-sm"></asp:TextBox>
                    </div>
                    <div class="left-aligned-label control-label col-xs-12 col-sm-2">
                        <asp:Label ID="Label9" runat="server">Lang.To</asp:Label>
                    </div>
                    <div class="col-xs-12 col-sm-3 form-col-without-padding">
                        <asp:TextBox ID="mToRemain" runat="server" CssClass="T2 form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11 col-lg-offset-1" runat="server" id="columnCheckBox">
                <asp:CheckBox runat="server" ID="mShowBuyPrice" Text="Lang.BuyPrice" Checked="true" />
                <asp:CheckBox runat="server" ID="mShowProfitPercent" Text="Lang.ProfitPercent" Checked="true" />
                <asp:CheckBox runat="server" ID="mShowSalePric" Text="Lang.SalePrice" Checked="true" />
                <asp:CheckBox runat="server" ID="mShowAvailable" Text="Lang.StoreStock" Checked="true" />
                <asp:CheckBox runat="server" ID="mShowTotalValue" Text="Lang.TotalValue" Checked="true" />
                <asp:CheckBox runat="server" ID="mShowExpireDate" Text="Lang.ExpireDate" Checked="False" />
                <asp:CheckBox runat="server" ID="mShowIncome" Text="Lang.NumberOfInputs" Checked="False" />
                <asp:CheckBox runat="server" ID="mShowOutcome" Text="Lang.NumberOfOutputs" Checked="False" />
            </div>
        </div>
        <div class="row padding-5 hidden">
            <input type="hidden" runat="server" id="mQueryBuilder" class="mQueryBuilder" />
            <input type="hidden" runat="server" id="mQueryBuilderSql" class="mQueryBuilderSql" />
            <div class="hidden">
                <asp:DropDownList runat="server" ID="mServiceGroupings" CssClass="mServiceGroupings"></asp:DropDownList>
                <asp:DropDownList runat="server" ID="mServiceGroup" CssClass="mServiceGroup"></asp:DropDownList>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <h5 class="dashed-bottom padding-5-bottom yekan-family">
                        <asp:Label runat="server">Lang.Groups</asp:Label>
                    </h5>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="ltr builder">
                        <div id="builder"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin-5-top">
            <div class="col-md-8 col-lg-6 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container form-col-without-padding">
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcel_Click" Enabled="false" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <input type="button" id="print" class="btn btn-sm btn-dark-blue btn-block" value="" />
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <asp:Label runat="server" ID="msg" CssClass="label-30 alert alert-danger block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
    </div>
    <asp:Panel ID="voucherDetailPanel" runat="server" Visible="False">
        <JP:MiniHeader runat="server" Text="Lang.ServiceReport" ID="mGridTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="mGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true" OnSorting="mGrid_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Code" HeaderText="Lang.Code" ItemStyle-HorizontalAlign="Center" SortExpression="Code" />
                        <asp:BoundField DataField="ServiceName" HeaderText="Lang.Commodity" ItemStyle-HorizontalAlign="Center" SortExpression="ServiceName" />
                        <asp:BoundField HeaderText="Lang.ExpireDate" ItemStyle-HorizontalAlign="Center" SortExpression="ExpireDate" />
                        <asp:BoundField DataField="BuyPrice" HeaderText="Lang.BuyPrice" ItemStyle-HorizontalAlign="Center" SortExpression="BuyPrice" />
                        <asp:BoundField DataField="ProfitPercent" HeaderText="Lang.ProfitPercent" ItemStyle-HorizontalAlign="Center" SortExpression="ProfitPercent" />
                        <asp:BoundField DataField="SalePrice" HeaderText="Lang.SalePrice" ItemStyle-HorizontalAlign="Center" SortExpression="SalePrice" />
                        <asp:BoundField DataField="Income" HeaderText="Lang.NumberOfInputs" ItemStyle-HorizontalAlign="Center" SortExpression="Income" />
                        <asp:BoundField DataField="Outcome" HeaderText="Lang.NumberOfOutputs" ItemStyle-HorizontalAlign="Center" SortExpression="Outcome" />
                        <asp:BoundField DataField="Available" HeaderText="Lang.StoreStock" ItemStyle-HorizontalAlign="Center" SortExpression="Available" />
                        <asp:TemplateField HeaderText="Lang.TotalValue" SortExpression="TotalValue">
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text="" />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalTotalValue" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="IncomeOutcomePanel" runat="server">
        <JP:MiniHeader runat="server" Text="Lang.IncomeOutcomeReport" ID="IncomeOutcomeGridTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="IncomeOutcomeGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="IncomeOutcomeGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ServiceName" HeaderText="Lang.Commodity" ItemStyle-HorizontalAlign="Center">
                            <ItemStyle Wrap="True" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ServiceCode" HeaderText="Lang.Code" ItemStyle-HorizontalAlign="Center">
                            <ItemStyle Wrap="True" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="UnitName" HeaderText="Lang.Units" ItemStyle-HorizontalAlign="Center">
                            <ItemStyle Wrap="True" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Lang.Num" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="InitRemainedNum" runat="server" Text='<%#Eval("InitRemainedNum") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label CssClass="BigGridFooterLabel" ID="TotalInitRemainedNum" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                            <ItemStyle HorizontalAlign="Center" Wrap="True" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Worth" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="InitRemainedWorth" runat="server" Text='<%#Eval("InitRemainedWorth") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label CssClass="BigGridFooterLabel" ID="TotalInitRemainedWorth" runat="server" Text="" />
                            </FooterTemplate>
                            <ItemStyle HorizontalAlign="Center" Wrap="True" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.AmountPrice" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="InitRemainedPrice" runat="server" Text='<%#Eval("InitRemainedPrice") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label CssClass="BigGridFooterLabel" ID="TotalInitRemainedPrice" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                            <ItemStyle HorizontalAlign="Center" Wrap="True" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Num" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="BuyNum" runat="server" Text='<%#Eval("BuyNum") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label CssClass="BigGridFooterLabel" ID="TotalBuyNum" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                            <ItemStyle HorizontalAlign="Center" Wrap="True" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Worth" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="BuyWorth" runat="server" Text='<%#Eval("BuyWorth") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label CssClass="BigGridFooterLabel" ID="TotalBuyWorth" runat="server" Text="" />
                            </FooterTemplate>
                            <ItemStyle HorizontalAlign="Center" Wrap="True" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.AmountPrice" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="BuyPrice" runat="server" Text='<%#Eval("BuyPrice") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label CssClass="BigGridFooterLabel" ID="TotalBuyPrice" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                            <ItemStyle HorizontalAlign="Center" Wrap="True" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Num" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="SaleNum" runat="server" Text='<%#Eval("SaleNum") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label CssClass="BigGridFooterLabel" ID="TotalSaleNum" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                            <ItemStyle HorizontalAlign="Center" Wrap="True" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Worth" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="SaleWorth" runat="server" Text='<%#Eval("SaleWorth") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label CssClass="BigGridFooterLabel" ID="TotalSaleWorth" runat="server" Text="" />
                            </FooterTemplate>
                            <ItemStyle HorizontalAlign="Center" Wrap="True" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.AmountPrice" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="SalePrice" runat="server" Text='<%#Eval("SalePrice") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label CssClass="BigGridFooterLabel" ID="TotalSalePrice" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                            <ItemStyle HorizontalAlign="Center" Wrap="True" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Num" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="ReturnNum" runat="server" Text='<%#Eval("ReturnNum") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label CssClass="BigGridFooterLabel" ID="TotalReturnNum" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                            <ItemStyle HorizontalAlign="Center" Wrap="True" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Worth" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="ReturnWorth" runat="server" Text='<%#Eval("ReturnWorth") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label CssClass="BigGridFooterLabel" ID="TotalReturnWorth" runat="server" Text="" />
                            </FooterTemplate>
                            <ItemStyle HorizontalAlign="Center" Wrap="True" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.AmountPrice" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="ReturnPrice" runat="server" Text='<%#Eval("ReturnPrice") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label CssClass="BigGridFooterLabel" ID="TotalReturnPrice" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                            <ItemStyle HorizontalAlign="Center" Wrap="True" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Num" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="RemainNum" runat="server" Text='<%#Eval("RemainNum") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label CssClass="BigGridFooterLabel" ID="TotalRemainNum" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                            <ItemStyle HorizontalAlign="Center" Wrap="True" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Worth" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="RemainWorth" runat="server" Text='<%#Eval("RemainWorth") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label CssClass="BigGridFooterLabel" ID="TotalRemainWorth" runat="server" Text="" />
                            </FooterTemplate>
                            <ItemStyle HorizontalAlign="Center" Wrap="True" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.AmountPrice" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="RemainPrice" runat="server" Text='<%#Eval("RemainPrice") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label CssClass="BigGridFooterLabel" ID="TotalRemainPrice" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                            <ItemStyle HorizontalAlign="Center" Wrap="True" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="BigGridHeader" />
                    <RowStyle CssClass="BigGridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>

    <script>
        $(function () {
            $('#print').attr('value', Lang.Print);
            $('#print').click(function () {
                printTable('.ScrollPanelCartable', '../../', Lang.Cardex);
            });
            $('.GridRow,.GridAltRow').click(function () {
                $(this).toggleClass('selected');
            })
            dateSelect('.dateselect');
            $('.monetary').keyup(function () {
                $(this).val(numberWithCommas(stripCommas($(this).val())));
            });
            //postback, formatItem, formatResult, more, _autourl, _width, moreQS
            // var _autourl = '../../get.aspx?op=Auto&table=Service&qd=serviceauto' + appendTime();
            //  autoCompleteFree('Service', 'serviceauto', ".mServiceName", "#justfurfun", null, null, null, null, _autourl);
            $("#bar").prepend(breadcrumb({
                title: Lang.ServiceReport,
                parent: {
                    title: Lang.Reports,
                    url: 'menu/reports.aspx'
                }
            }));
            setClassReports();

            var pgg = {};
            $('.mServiceGroupings option').each(function () {
                pgg[$(this).attr('value')] = $(this).text();
            });
            var peg = {};
            $('.mServiceGroup option').each(function () {
                peg[$(this).attr('value')] = $(this).text();
            });
            //if ($jQuery1_9('#builder').length > 0)
            $jQuery1_9('#builder').queryBuilder({
                filters: [
                    {
                        id: 'PGCategory',
                        label: Lang.CommoditisGroupLable,
                        type: 'integer',
                        input: 'select',
                        values: pgg,
                        operators: ['equal', 'not_equal']
                    },
                    {
                        id: 'PCategory',
                        label: Lang.ServiceType,
                        type: 'integer',
                        input: 'select',
                        values: peg,
                        operators: ['equal', 'not_equal']
                    }
                ]
            });
            // save rules before post back
            $('.doShow').click(function () {
                var rules = $jQuery1_9('#builder').queryBuilder('getRules');
                var rulesStr = JSON.stringify(rules);
                // alert(rulesStr);
                $('.mQueryBuilder').val(rulesStr);
                $('.mQueryBuilderSql').val($jQuery1_9('#builder').queryBuilder('getSQL', false, false).sql);
            });
            //reload saved rules
            $jQuery1_9('#builder').queryBuilder('reset');
            var ruleStr = $('.mQueryBuilder').val();
            if (ruleStr != '') {
                var rule = eval("(" + ruleStr + ")");
                try {
                    $jQuery1_9('#builder').queryBuilder('setRules', rule);
                } catch (e) {
                }
            }
        });
    </script>
</asp:Content>


