﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="isosequence.aspx.cs" Inherits="reports_accounting_isosequence" %>

<%@ Register Src="../../controls/ReportSetting.ascx" TagName="ReportSetting" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>

    <uc2:ReportSetting ID="mReportSetting" runat="server" />

    <div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="mFromDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label3" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4 col-lg-one-ninth-to-x-third">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 col-lg-eight-ninth-to-x-third form-col-without-padding">
                        <asp:TextBox ID="mToDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="mToDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 form-group-container">
                <div class="form-group">
                    <label class="left-aligned-label control-label col-xs-12 col-sm-4"><asp:Label ID="Label4" runat="server">Lang.From</asp:Label> <asp:Label ID="Label6" runat="server">Lang.No</asp:Label></label>
                    <div class="col-xs-12 col-sm-7 form-col-without-padding">
                        <asp:TextBox ID="mFromNo" runat="server" CssClass="T2 form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 form-group-container">
                <div class="form-group">
                    <label class="left-aligned-label control-label col-xs-12 col-sm-4"><asp:Label ID="Label5" runat="server">Lang.To</asp:Label> <asp:Label ID="Label7" runat="server">Lang.No</asp:Label></label>
                    <div class="col-xs-12 col-sm-7 form-col-without-padding">
                        <asp:TextBox ID="mToNo" runat="server" CssClass="T2 form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
             <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label8" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.CommodityName</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mServiceName" runat="server" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label9" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Person</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mPersons" runat="server" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label1" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.Type</asp:Label>
                    <div class="col-xs-12 col-sm-7 form-col-without-padding">
                        <asp:DropDownList ID="mTypes" runat="server" CssClass="input-sm">
                            <asp:ListItem Value="-1" Text="" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Lang.WFTRequest"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Lang.WFTPurchaseRequest"></asp:ListItem>
                            <asp:ListItem Value="3" Text="Lang.WFTPurchase"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Lang.WFTStoreDraft"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Lang.WFTBuildOrder"></asp:ListItem>
                            <asp:ListItem Value="6" Text="Lang.WFTReturn"></asp:ListItem>
                            <asp:ListItem Value="7" Text="Lang.WFTRefuse"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="row margin-5-top">
            <div class="col-md-8 col-lg-9 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container padding-0-left">
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcel_Click" Enabled="false" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <input type="button" id="print" class="btn btn-xs btn-dark-blue btn-block" value="" />
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <asp:Label runat="server" ID="msg" CssClass="label-30 alert alert-danger block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
    </div>
    <div class="row">
        <div class="col-xs-12">
            <asp:Panel ID="voucherDetailPanel" runat="server">
                <JP:MiniHeader ID="mTitle" runat="server" Text="Lang.IsoSequence" />
                <asp:GridView ID="mGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                    scrolling="Vertical" runat="server" BorderStyle="None"
                    ShowFooter="true" OnRowDataBound="mGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true" OnSorting="mGrid_Sorting" >
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:HyperLinkField DataNavigateUrlFormatString="../../list.aspx?table=request&id={0}&menu=accountinga" DataNavigateUrlFields="RequestID" DataTextField="RequestNo" SortExpression="RequestNo" HeaderText="Lang.WFTRequest" />
                        <asp:HyperLinkField DataNavigateUrlFormatString="../../list.aspx?table=purchaserequest&id={0}&menu=accountinga" DataNavigateUrlFields="PurchaseRequestID" DataTextField="PurchaseRequestNo" SortExpression="PurchaseRequestNo" HeaderText="Lang.WFTPurchaseRequest" />
                        <asp:HyperLinkField DataNavigateUrlFormatString="../../list.aspx?table=buildorder&id={0}&menu=accountinga" DataNavigateUrlFields="BuildOrderID" DataTextField="BuildOrderNo" SortExpression="BuildOrderNo" HeaderText="Lang.WFTBuildOrder" />
                        <asp:HyperLinkField DataNavigateUrlFormatString="../../list.aspx?table=purchase&id={0}&menu=accountinga" DataNavigateUrlFields="PurchaseID" DataTextField="PurchaseNo" SortExpression="PurchaseNo" HeaderText="Lang.WFTPurchase" />
                        <asp:HyperLinkField DataNavigateUrlFormatString="../../list.aspx?table=StoreDraft&id={0}&menu=accountinga" DataNavigateUrlFields="StoreDraftID" DataTextField="StoreDraftNo" SortExpression="StoreDraftNo" HeaderText="Lang.WFTStoreDraft" />
                        <asp:HyperLinkField DataNavigateUrlFormatString="../../list.aspx?table=Returns&id={0}&menu=accountinga" DataNavigateUrlFields="ReturnID" DataTextField="ReturnNo" SortExpression="ReturnNo" HeaderText="Lang.WFTReturn" />
                        <asp:HyperLinkField DataNavigateUrlFormatString="../../list.aspx?table=Refuse&id={0}&menu=accountinga" DataNavigateUrlFields="RefuseID" DataTextField="RefuseNo" SortExpression="RefuseNo" HeaderText="Lang.WFTRefuse" />
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                    <FooterStyle CssClass="GridHeader" HorizontalAlign="Center" />
                </asp:GridView>
            </asp:Panel>
        </div>
    </div>
    <script>
        $(function () {
            $('#print').attr('value', Lang.Print);
            $('#print').click(function () {
                var title = Lang.IsoSequence;
                printTable('.ScrollPanelCartable', '../../', title);
            });
            $('.GridRow,.GridAltRow').click(function () {
                $(this).toggleClass('selected');
            })
            
            dateSelect('.dateselect');
            $("#bar").prepend(breadcrumb({
                title: Lang.IsoSequence,
                parent: {
                    title: Lang.Reports,
                    url: 'menu/reports.aspx'
                }
            }));
            setClassReports();
        });
    </script>
</asp:Content>
