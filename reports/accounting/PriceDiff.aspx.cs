﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reports_accounting_PriceDiff : System.Web.UI.Page
{
    System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
    protected void Page_Load(object sender, EventArgs e)
    {
        msg.Visible = false;
        mGrid.EmptyDataText = LangProvider.Instance["EmptyData"];
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");
        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["SalePriceDiff"];
            Page.SetLang();
            int year = cal.GetYear(DateTime.Now);
            //mFromDate.Text = string.Format("{0}/01/01", year - 1300);
            //mToDate.Text = Helper.FormatDate(cal.ToDateTime(year + 1, 1, 1, 1, 1, 1, 1).AddDays(-1), "yy/MM/dd");
            IDataReader financeYearStartDate = DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");//0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            //mFromDate.Text = 
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);

            Init();

            mReportSetting.ReportName = "PriceDiff";
        }
        mReportSetting.SaveClick += new EventHandler(mReportSetting_SaveClick);
        mReportSetting.ReportSettingSelect += new EventHandler(mReportSetting_ReportSettingSelect);
    }

    void mReportSetting_ReportSettingSelect(object sender, EventArgs e)
    {
        ReportSettingInfo info = mReportSetting.SelectedReportSettings;
        if (info == null)
            return;
        string[] lines = info.Settings.Split('\n');
        mServiceName.Text = lines[0].Trim('\r');
        mFromDate.Text = lines[1].Trim('\r');
        mToDate.Text = lines[2].Trim('\r');
    }

    void mReportSetting_SaveClick(object sender, EventArgs e)
    {
        StringBuilder b = new StringBuilder();
        b.AppendLine(mServiceName.Text); //0
        b.AppendLine(mFromDate.Text); //1
        b.AppendLine(mToDate.Text); //2
        mReportSetting.Save(b.ToString());
    }
    void Init()
    {
        IDataReader persons = DbProxy.Instance.ExecuteReader("SELECT ID, [dbo].[DisplayPerson](ID, 1), ISNULL(Code, '') FROM Personnel WHERE Enabled = 1 AND Type=101");
        mPersons.Items.Clear();
        mPersons.Items.Add(new ListItem(LangProvider.Instance["AllPersons"], ""));
        if (persons != null)
        {
            while (persons.Read())
            {
                if (!persons.IsDBNull(0) && !persons.IsDBNull(1) && !persons.IsDBNull(2))
                    mPersons.Items.Add(new ListItem(persons.GetString(1) + " - کد: " + persons.GetString(2),
                        persons.GetInt32(0).ToString()));
            }
            persons.Close();
        }
        mPersons.SelectedIndex = 0;

        IDataReader projects = DbProxy.Instance.ExecuteReader("SELECT ID, FullName FROM ServiceTypeView");
        mServiceTypes.Items.Clear();
        mServiceTypes.Items.Add(new ListItem(LangProvider.Instance["AllCommodityGroups"], ""));
        if (projects != null)
        {
            while (projects.Read())
            {
                if (!projects.IsDBNull(0) && !projects.IsDBNull(1))
                    mServiceTypes.Items.Add(new ListItem(projects.GetString(1), projects.GetInt32(0).ToString()));
            }
            projects.Close();
        }
        mServiceTypes.SelectedIndex = 0;

        IDataReader Grouping = DbProxy.Instance.ExecuteReader("SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping");
        mPZGroup.Items.Clear();
        mSZGroup.Items.Clear();
        mPZGroup.Items.Add(new ListItem(LangProvider.Instance["AllGroup"], ""));
        mSZGroup.Items.Add(new ListItem(LangProvider.Instance["AllGroup"], ""));
        if (Grouping != null)
        {
            while (Grouping.Read())
            {
                if (!Grouping.IsDBNull(0) && !Grouping.IsDBNull(1))
                {
                    mPZGroup.Items.Add(new ListItem(Grouping.IsDBNull(1) ? "" : Grouping.GetString(1), Grouping.GetInt32(0).ToString()));
                    mSZGroup.Items.Add(new ListItem(Grouping.IsDBNull(1) ? "" : Grouping.GetString(1), Grouping.GetInt32(0).ToString()));
                }
            }
            Grouping.Close();
        }
        mPZGroup.SelectedIndex = 0;
        mSZGroup.SelectedIndex = 0;
    }

    private decimal sumDiff = 0, factorTotaldiscount = 0;
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.Common.DbDataRecord record = e.Row.DataItem as System.Data.Common.DbDataRecord;
            DateTime dt = Helper.GetDateTime(record["FactorDate"], Helper.NullDate);
            if (dt != Helper.NullDate)
                e.Row.Cells[3].Text = Helper.FormatDate(Helper.GetDateTime(dt, Helper.NullDate), "<a title='yyyy/MM/dd-HH:mm:ss'>yyyy/MM/dd</a>");
            int storeDraftID = Helper.GetInt(record["StoreDraftID"], 0);

            decimal Diff = SiteSettings.ToMonetary(Helper.GetDecimal(record["Diff"], 0), dt);
            sumDiff += Diff;

            e.Row.Cells[2].Text = Helper.FormatDate(dt, "yyyy-MM-dd");
            e.Row.Cells[3].Text = string.Format("<a href='../../list.aspx?table=StoreDraft&id={0}'>{1}</a>", storeDraftID, record["NOSerial"]);
            e.Row.Cells[6].Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["DesiredSalePrice"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.Cells[7].Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["ActualSalePrice"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.Cells[8].Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["Diff"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            if (factorTotaldiscount > 0)
            {
                e.Row.Cells[2].Text = string.Format("<div class='subSum' td='{0}' l='{1}'></div>", factorTotaldiscount, -1 * factorTotaldiscount);
                sumDiff += -1 * factorTotaldiscount;
                factorTotaldiscount = 0;
            }
            Label lblTotal = (Label)e.Row.FindControl("SumDiff");
            lblTotal.Text = sumDiff.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }
    }
    protected void mGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        BindGrid();
    }
    protected void BindGrid()
    {
        DateTime start = Helper.ParseDate(mFromDate.Text, Null.NullDate);
        DateTime end = Helper.ParseDate(mToDate.Text, Null.NullDate);
        if (Null.IsNull(start))
        {
            msg.Text = LangProvider.Instance["InvalidStartDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(end))
        {
            msg.Text = LangProvider.Instance["InvalidEndDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(start) || Null.IsNull(end))
            return;
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);

        string query = @"
DECLARE @SalePriceDecimalRound INT
SET @SalePriceDecimalRound = CONVERT(INT, dbo.GetSetting('SalePriceDecimalRound'), 4);
IF  @SalePriceDecimalRound = 0 OR @SalePriceDecimalRound IS NULL
	SET @SalePriceDecimalRound = 4
SELECT top 200 StoreDraftItemsView.ID, StoreDraftView.ID StoreDraftID, StoreDraftView.NOSerial, StoreDraftView.FactorDate, StoreDraftView.BuyerName, StoreDraftItemsView.StoreServiceName
, StoreDraftItemsView.DeliveredAmount, StoreDraftItemsView.UnitCurrencyConverter
, dbo.GetSalePrice(StoreDraftItemsView.StoreServiceID, @SalePriceDecimalRound, StoreDraftView.BuyerID, StoreDraftItemsView.SaleTypeID, StoreDraftView.FactorDate) DesiredSalePrice
, StoreDraftItemsView.Price ActualSalePrice
, StoreDraftItemsView.Price - dbo.GetSalePrice(StoreDraftItemsView.StoreServiceID, @SalePriceDecimalRound, StoreDraftView.BuyerID, StoreDraftItemsView.SaleTypeID, StoreDraftView.FactorDate) Diff
		FROM StoreDraftItemsView 
		INNER JOIN StoreDraftView ON StoreDraftItemsView.StoreDraftID = StoreDraftView.ID WHERE 1=1";

        string where = string.Format(" AND StoreDraftView.FactorDate>={0} AND StoreDraftView.FactorDate<={1} ", Helper.QoutedDateTime(start), Helper.QoutedDateTime(end));

        int personID = Helper.GetInt(mPersons.SelectedValue, Null.NullInteger);
        if (personID != Null.NullInteger)
            where += " AND StoreDraftView.BuyerID = " + personID;
        int GroupingID = Helper.GetInt(mPZGroup.SelectedValue, Null.NullInteger);
        if (GroupingID != Null.NullInteger)
            where += " AND StoreDraftView.BuyerID IN ( SELECT OtherID FROM GroupingMember INNER JOIN [Grouping] On [Grouping].ID=GroupingMember.GroupingID  WHERE GroupingID = " + GroupingID + " AND [Grouping].GroupingType = 0)";

        int serviceTypeID = Helper.GetInt(mServiceTypes.SelectedValue, Null.NullInteger);
        if (serviceTypeID != Null.NullInteger)
            where += " AND StoreDraftItemsView.ServiceTypeID = " + serviceTypeID;
        int GroupingID1 = Helper.GetInt(mSZGroup.SelectedValue, Null.NullInteger);
        if (GroupingID1 != Null.NullInteger)
            where += " AND StoreDraftItemsView.ServiceID IN(SELECT OtherID FROM GroupingMember INNER JOIN [Grouping] On [Grouping].ID=GroupingMember.GroupingID  WHERE GroupingID = " + GroupingID1 + " AND [Grouping].GroupingType = 1)";
        string what = mServiceName.Text.Trim().Replace("'", "''");
        if (what.Trim().Length > 0)
            where += " AND StoreDraftItemsView.StoreServiceName LIKE N'%" + what + "%'";
        float diff = Helper.GetFloat(mMinDiffPercent.Text, 0);
        if (diff > 0)
            where += " AND (StoreDraftItemsView.Price - dbo.GetSalePrice(StoreDraftItemsView.StoreServiceID, @SalePriceDecimalRound, StoreDraftView.BuyerID, StoreDraftItemsView.SaleTypeID, StoreDraftView.FactorDate)) / StoreDraftItemsView.Price > " + (diff / 100);
        else if (diff < 0)
            where += " AND (StoreDraftItemsView.Price - dbo.GetSalePrice(StoreDraftItemsView.StoreServiceID, @SalePriceDecimalRound, StoreDraftView.BuyerID, StoreDraftItemsView.SaleTypeID, StoreDraftView.FactorDate)) / StoreDraftItemsView.Price < " + (diff / 100);
        query += where;
        // WHERE StoreDraftItemsView.Price <> dbo.GetSalePrice(StoreDraftItemsView.StoreServiceID, @SalePriceDecimalRound, StoreDraftView.BuyerID, StoreDraftItemsView.SaleTypeID, StoreDraftView.FactorDate)
        if (!string.IsNullOrEmpty(SortCol))
            query += " ORDER BY " + SortCol + " " + SortOrder;
        else
            query += " ORDER BY FactorDate";
        IDataReader items = DbProxy.Instance.ExecuteReader(query);

        try
        {
            mGrid.DataSource = items;
            mGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Close();
                items.Dispose();
            }
        }
    }
    protected string SortCol
    {
        get
        {
            return Helper.GetString(ViewState["SortCol"], null);
        }
        set
        {
            ViewState["SortCol"] = value;
        }
    }
    protected string SortOrder
    {
        get
        {
            return Helper.GetString(ViewState["SortOrder"], "asc");
        }
        set
        {
            ViewState["SortOrder"] = value;
        }
    }
    protected void doShow_Click(object sender, EventArgs e)
    {
        doExcel.Enabled = true;
        BindGrid();

        voucherDetailPanel.Visible = true;
        //mSumDue.Text = sumDue.ToString("N0");
        //mSumOwe.Text = sumOwe.ToString("N0");
        //mSumUnit.Text = sumUnit.ToString("N0");
    }
    protected void doExcel_Click(object sender, EventArgs e)
    {
        Exporter.ExportExcel(Response, mGridTitle.Text, mGrid);
    }
}