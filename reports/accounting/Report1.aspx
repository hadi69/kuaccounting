﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Report1.aspx.cs" Inherits="reports_accounting_Report1" %>
<%@ Import Namespace="System.Web.Services.Protocols" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>
    <asp:Panel ID="panelDBs" runat="server" CssClass="well callout padding-5">
        <div class="row">
            <div class="col-xs-12">
                <h5 class="dashed-bottom padding-5-bottom yekan-family">
                    <asp:Label runat="server">Lang.DataBase</asp:Label>
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <asp:CheckBoxList runat="Server" ID="mDBList" RepeatLayout="Flow" RepeatDirection="Horizontal" />
            </div>
        </div>
    </asp:Panel>
    <div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label26" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Person</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mPersons" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label1" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Project</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mProjects" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label8" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.ReportEquipment</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mEquipments" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label10" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.CostCenter</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mCostCenters" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" AssociatedControlID="mFromDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label3" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label ID="Label4" runat="server">Lang.From</asp:Label>
                        <asp:Label ID="Label7" runat="server">Lang.DocumentNumber</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromVoucherNo" runat="server" CssClass="T2 form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label ID="Label5" runat="server">Lang.To</asp:Label>
                        <asp:Label ID="Label6" runat="server">Lang.DocumentNumber</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToVoucherNo" runat="server" CssClass="T2 form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label runat="server">Lang.Amount</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <div class="input-group input-group-sm">
                            <asp:TextBox ID="mAmount" runat="server" CssClass="T form-control input-sm monetary"></asp:TextBox>
                            <span class="input-group-btn">
                                <input type="button" value="+ -" class="btn btn-info" />
                            </span>
                            <asp:TextBox ID="mPOrM" runat="server" CssClass="T form-control input-sm monetary"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label9" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Comments</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:TextBox ID="mComments" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4">&nbsp;</div>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox runat="server" ID="mnotShowDetail" ClientIDMode="Static" Text="Lang.NotShowDetail" CssClass="mnotShowDetail" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4">&nbsp;</div>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox runat="server" ID="mnotShowDetailByPerson" Text="Lang.NotShowDetailByPerson" CssClass="mnotShowDetailByPerson" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4">&nbsp;</div>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox runat="server" ID="mjustOwe" Text="Lang.justOwe" CssClass="mjustOwe" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4">&nbsp;</div>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox runat="server" ID="mjustDue" Text="Lang.justDue" CssClass="mjustDue" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4">&nbsp;</div>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox runat="server" ID="PWECommentMerge" ClientIDMode="Static" Text="Lang.PWECommentMerge" Checked="True" CssClass="mPWECommentMerge" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container showinitremain">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-8 col-lg-10 form-col-without-padding">
                        <asp:CheckBox ID="mShowInitRemain" Text="Lang.InitRemain" runat="server" CssClass=""></asp:CheckBox>
                    </div>
                </div>
            </div>
            <asp:Panel ID="panelShowInProgressVouchers" runat="server" CssClass="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox ID="mShowInProgressVouchers" runat="server" Text="Lang.ShowInProgressVouchers" CssClass=""></asp:CheckBox>
                    </div>
                </div>
            </asp:Panel>
        </div>
        <div class="row margin-5-top">
            <div class="col-md-8 col-lg-9 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container padding-0-left">
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcel_Click" Enabled="false" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <input type="button" id="print" class="btn btn-xs btn-dark-blue btn-block" value="" />
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-xs-12">
        <asp:Label runat="server" ID="msg" CssClass="label-30 alert alert-danger block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
    </div>
    <table class="full-width">
        <tr>
            <td valign="top">
                <asp:TreeView ID="mTree" runat="server" CssClass="Tree" ShowLines="True" ShowCheckBoxes="All">
                    <SelectedNodeStyle CssClass="TreeSelected" />
                </asp:TreeView>
            </td>
            <td valign="top">
                <asp:GridView ID="mGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" BorderStyle="None" OnRowDataBound="mGrid_RowDataBound" GridLines="Both" EmptyDataText="" ShowFooter="true"
                    AllowSorting="true" OnSorting="mGrid_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ID" ItemStyle-HorizontalAlign="Center" Visible="False" />
                        <asp:HyperLinkField DataTextField="No" HeaderText="Lang.DocumentNumber" DataNavigateUrlFields="ID,ItemID" DataNavigateUrlFormatString="~/list.aspx?table=DocumentAccounting&id={0}&itemid={1}" SortExpression="No" />
                        <asp:TemplateField HeaderText="Lang.DateDocument" SortExpression="Date">
                            <ItemTemplate>
                                <asp:Label ID="lblDocumentDate" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="TreePathName" HeaderText="Lang.Topic" SortExpression="TreePathName" />
                        <asp:BoundField DataField="PersonnelName" HeaderText="Lang.PersonnelName" ItemStyle-HorizontalAlign="Center" SortExpression="PersonnelName" />
                        <asp:BoundField DataField="EquipmentName" HeaderText="Lang.Equipment" ItemStyle-HorizontalAlign="Center" SortExpression="EquipmentName" />
                        <asp:BoundField DataField="WardName" HeaderText="Lang.Project" ItemStyle-HorizontalAlign="Center" SortExpression="WardName" />
                        <asp:BoundField DataField="CostCenterName" HeaderText="Lang.CostCenter" ItemStyle-HorizontalAlign="Center" SortExpression="CostCenterName" />
                        <%--<asp:BoundField DataField="DoComments" HeaderText="Lang.DoComments" />--%>
                        <asp:BoundField DataField="Comments" HeaderText="Lang.Comments" SortExpression="Comments" />
                        <asp:TemplateField HeaderText="Lang.Liability" SortExpression="Owe">
                            <ItemTemplate>
                                <asp:Label ID="owe" runat="server" Text='<%#Eval("Owe") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="oweTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Credit" SortExpression="Due">
                            <ItemTemplate>
                                <asp:Label ID="due" runat="server" Text='<%#Eval("Due") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="dueTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Remained">
                            <ItemTemplate>
                                <asp:Label ID="remaining" runat="server" Text='<%#Eval("Due") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="remainingTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Diagnosis" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="diagnosis" runat="server" Text='<%#Eval("Due") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="finalDiagnosis" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <script>
        $(function () {
            $('.sidebar').addClass('collapsed');
           $("#<%=mnotShowDetailByPerson.ClientID %> , #<%=mnotShowDetail.ClientID %> ").click(function () {
                var elem = $(this);
                if (elem.attr('checked')) {
                    $("#<%=mShowInitRemain.ClientID %>").attr('checked', false);
                    $('.showinitremain').hide();
                }
            });
            $("#<%=mjustDue.ClientID %> , #<%=mjustOwe.ClientID %> , #<%=PWECommentMerge.ClientID %>").click(function () {
                var elem = $(this);
                if (elem.attr('checked')) {
                    $('.showinitremain').show();
                }
            });
            $("#<%=mAmount.ClientID %> , #<%=mPOrM.ClientID %> ").keyup(function (e) {
                if ($(this).val().indexOf('%') >= 0)
                    return;
                var val = toFloat($(this).val(), -1);
                if (val == -1)
                    return;
                if (e.keyCode === 37) // left
                {
                    if (val > 1000) {
                        var parts = $(this).val().toString().replace(/,/g, "").split(".");
                        parts[0] = parseInt(parseInt(parts[0]) / 1000);
                        $(this).val(parts.join("."));
                    }
                }
                else if (e.keyCode === 38) // up
                {
                    $(this).val(val + 1);
                }
                else if (e.keyCode === 39) // right
                {
                    var parts = $(this).val().toString().replace(/,/g, "").split(".");
                    parts[0] = parseInt(parts[0]) * 1000;
                    $(this).val(parts.join("."));
                }
                else if (e.keyCode === 40) // up
                {
                    if (val > 1)
                        $(this).val(val - 1);
                }
                $(this).val(numberWithCommasAllowDP(stripCommas($(this).val())));
            });
            $('#print').attr('value', Lang.Print);
            $('#print').click(function () {
                var title = Lang.Report1;
                if ($("#<%=mFromDate.ClientID %>").val() != '') {
                    title += '/' + Lang.FromDate + ': ' + $("#<%=mFromDate.ClientID %>").val();
                }
                if ($("#<%=mToDate.ClientID %>").val() != '') {
                    title += '/' + Lang.ToDate + ': ' + $("#<%=mToDate.ClientID %>").val();
                }
                if ($("#<%=mFromVoucherNo.ClientID %>").val() != '') {
                    title += '/' + Lang.FromDocNo + ': ' + $("#<%=mFromVoucherNo.ClientID %>").val();
                }
                if ($("#<%=mToVoucherNo.ClientID %>").val() != '') {
                    title += '/' + Lang.ToDocNo + ': ' + $("#<%=mToVoucherNo.ClientID %>").val();
                }
                if ($('#<%=mPersons.ClientID %>').val() != '') {
                    title += '/' + Lang.Person + ': ' + $('#<%=mPersons.ClientID %> option:selected').text();
                }
                if ($('#<%=mProjects.ClientID %>').val() != '') {
                    title += '/' + Lang.Project + ': ' + $('#<%=mProjects.ClientID %> option:selected').text();
                }
                if ($('#<%=mEquipments.ClientID %>').val() != '') {
                    title += '/' + Lang.Equipment + ': ' + $('#<%=mEquipments.ClientID %> option:selected').text();
                }
                if ($('#<%=mCostCenters.ClientID %>').val() != '') {
                    title += '/' + Lang.CostCenter + ': ' + $('#<%=mCostCenters.ClientID %> option:selected').text();
                }
                printTable('.ScrollPanelCartable', '../../', title);
            });

            $("#bar").prepend(breadcrumb({
                title: Lang.Report1,
                parent: {
                    title: Lang.Reports,
                    url: 'menu/reports.aspx'
                }
            }));
            setClassReports();
            dateSelect('.dateselect');
        });
    </script>
</asp:Content>

