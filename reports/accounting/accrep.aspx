﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="accrep.aspx.cs" Inherits="reports_accounting_accrep" %>

<%@ Register Src="../../controls/PeriodSelector.ascx" TagName="PeriodSelector" TagPrefix="uc1" %>
<%@ Register Src="../../controls/ZedGraphControl.ascx" TagName="ZedGraphControl" TagPrefix="uc3" %>
<%@ Register Src="../../controls/ReportSetting.ascx" TagName="ReportSetting" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>

    <uc2:ReportSetting ID="mReportSetting" runat="server" />
    <uc1:PeriodSelector ID="mPeriodSelector" runat="server" />
    <div class="well callout padding-5">
        <div class="row">
            <div class="col-md-8 col-lg-11 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-1 form-group-container">
                <input type="button" id="print" class="btn btn-xs btn-dark-blue btn-block" value="" />
            </div>
        </div>
    </div>
    <div class="row margin-10-bottom">
        <div class="col-xs-12">
            <asp:PlaceHolder runat="server" ID="mHolder"></asp:PlaceHolder>
        </div>
        <div class="col-xs-12">
            <div class="chart">
                <uc3:ZedGraphControl ID="mZedGraphControl" runat="server" Width="1000" Height="500" />
            </div>
        </div>
    </div>
    <script>
        $(function () {
            $('#print').attr('value', Lang.Print);
            $('#print').click(function () {
                printTable('.report', '../../', $('.Title').html(), null, $('.chart').html());
            });
            dateSelect('.dateSelect');
            $("#bar").prepend(breadcrumb({
                title: Lang.CustomReports + ' - <%= reportTitle %>',
                parent: {
                    title: Lang.Reports,
                    url: 'menu/Reports.aspx'
                }
            }));
            setClassReports();
        });
    </script>
</asp:Content>
