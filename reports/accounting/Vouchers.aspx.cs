﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class reports_accounting_Vouchers : System.Web.UI.Page
{
    string mode = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindWards();
            Mode = Request.QueryString["mode"];
            System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
            int year = cal.GetYear(DateTime.Now);
            mFromDate.Text = mToDate.Text = Helper.FormatDate(DateTime.Now, null);
        }
    }
    void BindWards()
    {
        mWards.DataTextField = "Name";
        mWards.DataValueField = "ID";

        mWards.DataSource = DbProxy.Instance.FillData("SELECT -1 AS ID, '' AS Name UNION SELECT ID, Name FROM Ward", "ward"); ;
        mWards.DataBind();

        mWards.SelectedIndex = 0;
    }
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int wardID = Helper.GetInt((e.Row.DataItem as System.Data.Common.DbDataRecord)["WardID"], Null.NullInteger);
            e.Row.Cells[0].Text = Helper.GetString((e.Row.DataItem as System.Data.Common.DbDataRecord)["WardName"], "");// AccController.GetWardName(wardID);
            e.Row.Cells[3].Text = Helper.FormatDate((DateTime)(e.Row.DataItem as System.Data.Common.DbDataRecord)["Date"], "yy/MM/dd");
            e.Row.Cells[4].Text = Helper.GetInt((e.Row.DataItem as System.Data.Common.DbDataRecord)["Status"], Null.NullInteger) == 2 ? "X" : "";
        }
    }

    protected void doShow_Click(object sender, EventArgs e)
    {
        IDataReader items = null;
        if (Mode == "empty")
            items = AccController.GetVouchersNoItem(Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50))
                , Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50))
                , Helper.GetInt(mFromVoucherID.Text, 0), Helper.GetInt(mToVoucherID.Text, int.MaxValue - 1)
                , Helper.GetInt(mWards.SelectedValue, Null.NullInteger));
        else if (Mode == "unfixed")
            items = AccController.GetVouchersUnFixed(Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50))
                , Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50))
                , Helper.GetInt(mFromVoucherID.Text, 0), Helper.GetInt(mToVoucherID.Text, int.MaxValue - 1)
                , Helper.GetInt(mWards.SelectedValue, Null.NullInteger));
        else if (Mode == "fixed")
            items = AccController.GetVouchersFixed(Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50))
                , Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50))
                , Helper.GetInt(mFromVoucherID.Text, 0), Helper.GetInt(mToVoucherID.Text, int.MaxValue - 1)
                , Helper.GetInt(mWards.SelectedValue, Null.NullInteger));
        else if (Mode == "notitle")
            items = AccController.GetVouchersNoTitle(Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50))
                , Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50))
                , Helper.GetInt(mFromVoucherID.Text, 0), Helper.GetInt(mToVoucherID.Text, int.MaxValue - 1)
                , Helper.GetInt(mWards.SelectedValue, Null.NullInteger));
        else if (Mode == "unbalanced")
            items = AccController.GetVouchersUnBalanced(Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50))
                , Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50))
                , Helper.GetInt(mFromVoucherID.Text, 0), Helper.GetInt(mToVoucherID.Text, int.MaxValue - 1)
                , Helper.GetInt(mWards.SelectedValue, Null.NullInteger));
        try
        {
            mGrid.DataSource = items;
            mGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Close();
                items.Dispose();
            }
        }
    }
    private string Mode
    {
        get
        {
            return Helper.GetString(ViewState["Mode"], "empty");
        }
        set
        {
            ViewState["Mode"] = value;
        }
    }
}