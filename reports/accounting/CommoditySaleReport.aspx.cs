﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;

public partial class reports_accounting_CommoditySaleReport : System.Web.UI.Page
{
    private System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();

    protected void Page_Load(object sender, EventArgs e)
    {
        msg.Visible = false;
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");

        mGrid.EmptyDataText = LangProvider.Instance["ListIsEmpty"];

        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["CommoditySaleReport"];
            Page.SetLang();
            IDataReader financeYearStartDate =
                DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");
            //0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            //mFromDate.Text = 
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);
            Initial();

          //  mReportSetting.ReportName = "CommoditySaleReport";
        }

    }

    //private void mReportSetting_ReportSettingSelect(object sender, EventArgs e)
    //{
    //    ReportSettingInfo info = mReportSetting.SelectedReportSettings;
    //    if (info == null)
    //        return;
    //    string[] lines = info.Settings.Split('\n');
    //    mPersons.SelectedValue = lines[0].Trim('\r');
    //    mFromDate.Text = lines[2].Trim('\r');
    //    mToDate.Text = lines[3].Trim('\r');
    //}

    //private void mReportSetting_SaveClick(object sender, EventArgs e)
    //{
    //    StringBuilder b = new StringBuilder();
    //    b.AppendLine(mPersons.SelectedValue); //0
    //    b.AppendLine(mFromDate.Text); //1
    //    b.AppendLine(mToDate.Text); //2
    //    mReportSetting.Save(b.ToString());
    //}

    private void Initial()
    {
        {
            IDataReader persons =
                DbProxy.Instance.ExecuteReader(
                    "SELECT ID, [dbo].[DisplayPerson](ID, 1), ISNULL(Code, '') FROM Personnel WHERE Type=101");
            IDataReader commodities =
                DbProxy.Instance.ExecuteReader(
                    "SELECT ID, ServiceFullName FROM ServicePriceView ORDER BY ServiceFullName");
            IDataReader labels =
                DbProxy.Instance.ExecuteReader(
                    "SELECT ID, [dbo].[DisplayPerson](ID, 1), ISNULL(Code, '') FROM Personnel WHERE Type=101");
            mPersons.Items.Clear();
            mCommodities.Items.Clear();
            mLabels.Items.Clear();

            mPersons.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
            mCommodities.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
            mLabels.Items.Add(new ListItem(LangProvider.Instance["All"], ""));

            if (persons != null)
            {
                while (persons.Read())
                {
                    if (!persons.IsDBNull(0) && !persons.IsDBNull(1) && !persons.IsDBNull(2))
                        mPersons.Items.Add(new ListItem(persons.GetString(1) + " - کد: " + persons.GetString(2),
                        persons.GetInt32(0).ToString()));
                }
                persons.Close();
            }
            mPersons.SelectedIndex = 0;

            if (commodities != null)
            {
                while (commodities.Read())
                {
                    if (!commodities.IsDBNull(0) && !commodities.IsDBNull(1))
                        mCommodities.Items.Add(new ListItem(commodities.IsDBNull(1) ? "" : commodities.GetString(1),
                        commodities.IsDBNull(0) ? "" : commodities.GetInt32(0).ToString()));
                }
                commodities.Close();
            }
            mCommodities.SelectedIndex = 0;
            if (labels != null)
            {
                while (labels.Read())
                {
                    if (!labels.IsDBNull(0) && !labels.IsDBNull(1))
                        mLabels.Items.Add(new ListItem(labels.IsDBNull(1) ? "" : labels.GetString(1),
                        labels.GetInt32(0).ToString()));
                }
                labels.Close();
            }
            mLabels.SelectedIndex = 0;
        }

        IDataReader serviceTypes = DbProxy.Instance.ExecuteReader("SELECT ID, FullName FROM ServiceTypeView");
        mServiceTypes.Items.Clear();
        mServiceTypes.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
        if (serviceTypes != null)
        {
            while (serviceTypes.Read())
            {
                if (!serviceTypes.IsDBNull(0) && !serviceTypes.IsDBNull(1))
                    mServiceTypes.Items.Add(new ListItem(serviceTypes.GetString(1), serviceTypes.GetInt32(0).ToString()));
            }
            serviceTypes.Close();
        }
        mServiceTypes.SelectedIndex = 0;

        IDataReader PersonGroups = DbProxy.Instance.ExecuteReader("SELECT ID, Title FROM PersonnelGroup");
        mPersonGroups.Items.Clear();
        mPersonGroups.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
        if (PersonGroups != null)
        {
            while (PersonGroups.Read())
                while (PersonGroups.Read())
                {
                    if (!PersonGroups.IsDBNull(0) && !PersonGroups.IsDBNull(1))
                        mPersonGroups.Items.Add(new ListItem(PersonGroups.GetString(1), PersonGroups.GetInt32(0).ToString()));
                }
            PersonGroups.Close();
        }
        mPersonGroups.SelectedIndex = 0;
        //IDataReader commodityGroupings =
        //    DbProxy.Instance.ExecuteReader(
        //        "SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping  WHERE GroupingType = 1");
        //IDataReader personGroupings =
        //    DbProxy.Instance.ExecuteReader(
        //        "SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping  WHERE GroupingType = 0");
        //IDataReader commodityGroup = DbProxy.Instance.ExecuteReader("SELECT ID, FullName FROM ServiceTypeView");
        //IDataReader personGroup = DbProxy.Instance.ExecuteReader("SELECT ID, Title FROM PersonnelGroup");
        //mCommodityGroupings.Items.Clear();
        //if (commodityGroupings != null)
        //{
        //    while (commodityGroupings.Read())
        //    {
        //        if (!commodityGroupings.IsDBNull(0) && !commodityGroupings.IsDBNull(1))
        //            mCommodityGroupings.Items.Add(new ListItem(commodityGroupings.GetString(1),
        //                commodityGroupings.GetInt32(0).ToString()));
        //    }
        //    commodityGroupings.Close();
        //}
        //mPersonGroupings.Items.Clear();
        //if (personGroupings != null)
        //{
        //    while (personGroupings.Read())
        //    {
        //        if (!personGroupings.IsDBNull(0) && !personGroupings.IsDBNull(1))
        //            mPersonGroupings.Items.Add(new ListItem(personGroupings.GetString(1),
        //                personGroupings.GetInt32(0).ToString()));
        //    }
        //    personGroupings.Close();
        //}
        //mCommodityGroup.Items.Clear();
        //if (commodityGroup != null)
        //{
        //    while (commodityGroup.Read())
        //    {
        //        if (!commodityGroup.IsDBNull(0) && !commodityGroup.IsDBNull(1))
        //            mCommodityGroup.Items.Add(new ListItem(commodityGroup.GetString(1),
        //                commodityGroup.GetInt32(0).ToString()));
        //    }
        //    commodityGroup.Close();
        //}
        //mPersonGroup.Items.Clear();
        //if (personGroup != null)
        //{
        //    while (personGroup.Read())
        //    {
        //        if (!personGroup.IsDBNull(0) && !personGroup.IsDBNull(1))
        //            mPersonGroup.Items.Add(new ListItem(personGroup.GetString(1), personGroup.GetInt32(0).ToString()));
        //    }
        //    personGroup.Close();
        //}
    }

    //protected void DlBaseChange(object sender, EventArgs e)
    //{
    //    DropDownList comboBox = (DropDownList)sender;
    //    int selected = Int32.Parse(comboBox.SelectedValue);
    //    if (selected == 1)
    //    {
    //        personLable.Text = LangProvider.Instance["ActorName"];
    //        panelDropDowns.Visible = panelInternal.Visible = true;
    //        //  mActorName.Visible = ActorNameLabel.Visible = mTransferee.Visible = TransfereeLabel.Visible = true;
    //    }
    //    else
    //    {
    //        personLable.Text = LangProvider.Instance["Person"];
    //        panelDropDowns.Visible = panelInternal.Visible = false;
    //        //mActorName.Visible = ActorNameLabel.Visible = mTransferee.Visible = TransfereeLabel.Visible = false;
    //    }
    //}

    protected void doShow_Click(object sender, EventArgs e)
    {
        doExcel.Enabled = true;
        DateTime start = Helper.ParseDate(mFromDate.Text, Null.NullDate);
        DateTime end = Helper.ParseDate(mToDate.Text, Null.NullDate);
        if (Null.IsNull(start))
        {
            msg.Text = LangProvider.Instance["InvalidStartDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(end))
        {
            msg.Text = LangProvider.Instance["InvalidEndDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(start) || Null.IsNull(end))
            return;
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);
        if (CommoditySum.Checked && mDividedMonth.Checked)
        {
            string endDate = mToDate.Text;
            string[] date = endDate.Split('/');
            int year = Helper.GetInt(date[0], -1);
            for (int i = 1; i <= 12; i++)
            {
                if (i != 12)
                {
                    BindGrid(cal.ToDateTime(year, i, 1, 0, 0, 0, 0), cal.ToDateTime(year, i + 1, 1, 0, 0, 0, 0), i);
                }
                else
                {
                    BindGrid(cal.ToDateTime(year, i, 1, 0, 0, 0, 0),
                        cal.ToDateTime(year, i, cal.GetDaysInMonth(year, i), 23, 59, 59, 999), i);
                }
            }
        }
        else
        {
            BindGrid(start, end, 12);
        }
    }

    #region Grid

    private decimal _sumTotalTotalPrice, _sumTotalPrice, _sumTotalTotal, _sumTotalDiscount, _sumTotalTax;
    private float _sumTotalAmount;

    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.Common.DbDataRecord record = e.Row.DataItem as System.Data.Common.DbDataRecord;
            DateTime dt = DateTime.Now;
            dt = Helper.GetDateTime(record["FactorDate"], Helper.NullDate);
            Label lblGridDate = (Label) e.Row.FindControl("lblGridDate");
            if (dt != Helper.NullDate && lblGridDate != null)
            {
                lblGridDate.Text = Helper.FormatDate(Helper.GetDateTime(dt, Helper.NullDate), null);
            }
            int month = Helper.GetInt(record["Month"], Null.NullInteger);
            switch (month)
            {
                case 1 :
                    e.Row.Cells[17].Text = LangProvider.Instance["Farvardin"];
                    break;
                case 2:
                    e.Row.Cells[17].Text = LangProvider.Instance["Ordibehesht"];
                    break;
                case 3:
                    e.Row.Cells[17].Text = LangProvider.Instance["Khordad"];
                    break;
                case 4:
                    e.Row.Cells[17].Text = LangProvider.Instance["Tir"];
                    break;
                case 5:
                    e.Row.Cells[17].Text = LangProvider.Instance["Mordad"];
                    break;
                case 6:
                    e.Row.Cells[17].Text = LangProvider.Instance["Shahrivar"];
                    break;
                case 7:
                    e.Row.Cells[17].Text = LangProvider.Instance["Mehr"];
                    break;
                case 8:
                    e.Row.Cells[17].Text = LangProvider.Instance["Aban"];
                    break;
                case 9:
                    e.Row.Cells[17].Text = LangProvider.Instance["Azar"];
                    break;
                case 10:
                    e.Row.Cells[17].Text = LangProvider.Instance["Dey"];
                    break;
                case 11:
                    e.Row.Cells[17].Text = LangProvider.Instance["Bahman"];
                    break;
                case 12:
                    e.Row.Cells[17].Text = LangProvider.Instance["Esfand"];
                    break;
            }
            e.Row.Cells[1].Text =
                string.Format("<a href='../../list.aspx?table=StoreDraft&id={0}&menu=accountinga'>{1}</a>", record["ID"],
                    record["NOSerial"]);
            decimal totalPrice = SiteSettings.ToMonetary(Helper.GetDecimal(record["Price"], 0), dt);
            e.Row.Cells[15].Text = totalPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            decimal totalTotal = SiteSettings.ToMonetary(Helper.GetDecimal(record["Total"], 0), dt);
            e.Row.Cells[12].Text = totalTotal.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            decimal totalDiscount = SiteSettings.ToMonetary(Helper.GetDecimal(record["TotalDiscount"], 0), dt);
            e.Row.Cells[13].Text = totalDiscount.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            decimal totalTax = SiteSettings.ToMonetary(Helper.GetDecimal(record["Tax"], 0), dt);
            e.Row.Cells[14].Text = totalTax.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            decimal totalTotalPrice = SiteSettings.ToMonetary(Helper.GetDecimal(record["TotalPrice"], 0), dt);
            e.Row.Cells[16].Text = totalTotalPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            _sumTotalAmount += Helper.GetFloat(record["TotalAmount"], 0);
            _sumTotalPrice += totalPrice;
            _sumTotalTotalPrice += totalTotalPrice;
            _sumTotalTotal += totalTotal;
            _sumTotalDiscount += totalDiscount;
            _sumTotalTax += totalTax;

        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            (e.Row.FindControl("totalPrice") as Label).Text = _sumTotalPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("totalTotalPrice") as Label).Text = _sumTotalTotalPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("totalTotal") as Label).Text = _sumTotalTotal.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("totalTotalDiscount") as Label).Text = _sumTotalDiscount.ToString("N4", Helper.NumberCulture)
                .Replace(".0000", "");
            (e.Row.FindControl("totalTax") as Label).Text = _sumTotalTax.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("totalTotalAmount") as Label).Text = _sumTotalAmount.ToString();
        }
    }

    protected void mGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        DateTime start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        DateTime end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);
        if (CommoditySum.Checked && mDividedMonth.Checked)
        {
            string endDate = mToDate.Text;
            string[] date = endDate.Split('/');
            int year = Helper.GetInt(date[0], -1);
            for (int i = 1; i <= 12; i++)
            {
                if (i != 12)
                {
                    BindGrid(cal.ToDateTime(year, i, 1, 0, 0, 0, 0), cal.ToDateTime(year, i + 1, 1, 0, 0, 0, 0), i);
                }
                else
                {
                    BindGrid(cal.ToDateTime(year, i, 1, 0, 0, 0, 0),
                        cal.ToDateTime(year, i, cal.GetDaysInMonth(year, i), 23, 59, 59, 999), i);
                }
            }
        }
        else
        {
            BindGrid(start, end, 12);
        }
    }

    private string totalQuery = "SELECT * FROM ( ";

    private void BindGrid(DateTime start, DateTime end, int count)
    {
        gridPanel.Visible = true;

        string cogIDs, pegIDs, coIDs, peIDs, gWhere;
        GetGroupWhere(out cogIDs, out pegIDs, out coIDs, out peIDs, out gWhere);
        int personId = Helper.GetInt(mPersons.SelectedValue, Null.NullInteger);
        int commodityId = Helper.GetInt(mCommodities.SelectedValue, Null.NullInteger);
        int personGroupId = Helper.GetInt(mPersonGroups.SelectedValue, Null.NullInteger);
        int serviceTypeId = Helper.GetInt(mServiceTypes.SelectedValue, Null.NullInteger);
        string query = "";

        #region storedraft

        if (Detail.Checked)
        {
            mGrid.Columns[5].Visible = mGrid.Columns[6].Visible = mGrid.Columns[12].Visible = mGrid.Columns[13].Visible
                = mGrid.Columns[14].Visible = mGrid.Columns[17].Visible = false;
            mGrid.Columns[1].Visible = mGrid.Columns[2].Visible = mGrid.Columns[3].Visible = mGrid.Columns[7].Visible = mGrid.Columns[8].Visible
                        = mGrid.Columns[9].Visible = mGrid.Columns[10].Visible = mGrid.Columns[11].Visible = mGrid.Columns[15].Visible = mGrid.Columns[4].Visible = true;

            query =
                string.Format(
                    @"SELECT {0} [Month], StoreDraftView.ID , NOSerial, FactorDate, BuyerID, BuyerName, StoreDraftView.Comments,
                    StoreServiceID, StoreServiceName, StoreDraftItemsView.UnitName, ServiceView.Code, NULL Total, NULL TotalDiscount, NULL Tax, 
                    UnitCurrencyConverter, DeliveredAmount,(UnitCurrencyConverter * ISNULL(DeliveredAmount, 0)) TotalAmount, Price
                    , (UnitCurrencyConverter * ISNULL(DeliveredAmount, 0) * ISNULL(Price, 0)) AS TotalPrice
                    , StoreDraftItemsView.Label
                    FROM StoreDraftView
                    LEFT OUTER JOIN StoreDraftItemsView ON StoreDraftView.ID = StoreDraftItemsView.StoreDraftID
                    LEFT OUTER JOIN StoreServices ON StoreDraftItemsView.StoreServiceID = StoreServices.ID
                    LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                    LEFT OUTER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID
                    LEFT OUTER JOIN ServiceView ON ServicePrice.ServiceID = ServiceView.ID
                    LEFT OUTER JOIN Personnel ON StoreDraftView.BuyerID = Personnel.ID", count);
        }
        else if (CommoditySum.Checked)
        {
            mGrid.Columns[1].Visible = mGrid.Columns[2].Visible = mGrid.Columns[3].Visible = mGrid.Columns[5].Visible
                =mGrid.Columns[9].Visible =mGrid.Columns[10].Visible = mGrid.Columns[12].Visible = mGrid.Columns[13].Visible
                        = mGrid.Columns[12].Visible = mGrid.Columns[13].Visible = mGrid.Columns[15].Visible = mGrid.Columns[14].Visible = false;
              mGrid.Columns[6].Visible = mGrid.Columns[7].Visible = mGrid.Columns[8].Visible = mGrid.Columns[11].Visible = mGrid.Columns[4].Visible = true;
            if (mDividedMonth.Checked)
            {
                mGrid.Columns[17].Visible = true;
            }
            else
            {
                mGrid.Columns[17].Visible = false;
            }

            query = string.Format(@"
                    SELECT  {0} [Month], NULL ID , NULL NOSerial, NULL FactorDate, NULL BuyerID, NULL BuyerName, NULL Comments,
                    StoreServiceID, StoreServiceName, ServiceView.UnitName, ServiceView.Code, NULL Total, NULL TotalDiscount, NULL Tax,
                    NULL UnitCurrencyConverter, NULL DeliveredAmount, SUM(UnitCurrencyConverter * ISNULL(DeliveredAmount, 0)) AS TotalAmount
                    , NULL Price, SUM(UnitCurrencyConverter * ISNULL(DeliveredAmount, 0) * ISNULL(Price, 0)) AS TotalPrice
                    , StoreDraftItemsView.Label FROM StoreDraftView
                    LEFT OUTER JOIN StoreDraftItemsView ON StoreDraftView.ID = StoreDraftItemsView.StoreDraftID
                    LEFT OUTER JOIN StoreServices ON StoreDraftItemsView.StoreServiceID = StoreServices.ID
                    LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                    LEFT OUTER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID
                    LEFT OUTER JOIN ServiceView ON ServicePrice.ServiceID = ServiceView.ID
                    LEFT OUTER JOIN Personnel ON StoreDraftView.BuyerID = Personnel.ID", count);
        }
        else if (factors.Checked)
        {
            mGrid.Columns[6].Visible = mGrid.Columns[5].Visible = mGrid.Columns[8].Visible = mGrid.Columns[9].Visible
                = mGrid.Columns[10].Visible = mGrid.Columns[11].Visible = mGrid.Columns[15].Visible = mGrid.Columns[4].Visible = mGrid.Columns[17].Visible = mGrid.Columns[7].Visible = false;
            mGrid.Columns[1].Visible = mGrid.Columns[2].Visible = mGrid.Columns[3].Visible = mGrid.Columns[5].Visible = mGrid.Columns[12].Visible
                        = mGrid.Columns[13].Visible = mGrid.Columns[14].Visible = true;

            query = string.Format(@"
                    SELECT {0} [Month],  StoreDraftView.ID , StoreDraftView.NOSerial, StoreDraftView.FactorDate, StoreDraftView.BuyerID, StoreDraftView.BuyerName, StoreDraftView.Comments,
                    NULL StoreServiceID, NULL StoreServiceName, NULL UnitName, NULL Code, StoreDraftView.TotalPrice AS Total
                    ,StoreDraftView.TotalDiscount, (ISNULL(StoreDraftView.TotalTax, 0) + ISNULL(StoreDraftView.VAT, 0)) AS Tax
                    ,NULL UnitCurrencyConverter, NULL DeliveredAmount, NULL TotalAmount, NULL Price
                    ,((ISNULL(StoreDraftView.TotalPrice, 0) + ISNULL(StoreDraftView.TotalTax, 0) + ISNULL(StoreDraftView.VAT, 0)) - (ISNULL(StoreDraftView.TotalDiscount, 0))) AS TotalPrice FROM StoreDraftView
                    LEFT OUTER JOIN Personnel ON StoreDraftView.BuyerID = Personnel.ID", count);
        }
        string where =
            string.Format(
                " WHERE (StoreDraftView.StoreDraftType = 0 OR StoreDraftView.StoreDraftType IS NULL) AND FactorDate>={0} AND FactorDate<={1}",
                Helper.QoutedDateTime(start),
                Helper.QoutedDateTime(end));
        if (commodityId != Null.NullInteger)
        {
            if (factors.Checked)
            {
                where += @"AND StoreDraftView.ID IN (
                        SELECT DISTINCT StoreDraftID FROM StoreDraftItems 
                        LEFT OUTER JOIN StoreServices ON StoreDraftItems.StoreServiceID = StoreServices.ID
                        LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                        WHERE StoreServiceDef.ServicePriceID =" + commodityId + ")";
            }
            else
            {
                where += "AND ServicePrice.ID = " + commodityId;
            }
        }
        //if (labelId != Null.NullInteger)
        //{
        //    if (factors.Checked)
        //    {
        //        where += @"AND StoreDraftView.ID IN (
        //                SELECT DISTINCT StoreDraftID FROM StoreDraftItemsView 
        //                WHERE StoreDraftItemsView.LabelID =" + labelId + ")";
        //    }
        //    else
        //    {
        //        where += "AND StoreDraftItemsView.LabelID = " + labelId;
        //    }
        //}
        if (personId != Null.NullInteger)
        {
            where += " AND BuyerID = " + personId;
        }

        if (personGroupId != Null.NullInteger)
        {
            where += " AND Personnel.PersonnelGroupID = " + personGroupId;
        }

        if (serviceTypeId != Null.NullInteger)
        {
            if (factors.Checked)
            {
                where += @" AND StoreDraftView.ID IN (
                        SELECT DISTINCT StoreDraftID FROM StoreDraftItems 
                        LEFT OUTER JOIN StoreServices ON StoreDraftItems.StoreServiceID = StoreServices.ID
                        LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                        LEFT OUTER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID
                        LEFT OUTER JOIN Service ON ServicePrice.ServiceID = Service.ID
                        WHERE Service.ServiceTypeID IN (SELECT ID FROM [GetAllServiceTypeChilds]( " + serviceTypeId + ")))";
            }
            else
            {
                where += " AND ServiceView.ServiceTypeID IN (SELECT ID FROM [GetAllServiceTypeChilds]( " + serviceTypeId + "))";
            }
        }

        if (mJustShowStoredraftWithVatAndTax.Checked)
        {
            where +=
                " AND ((StoreDraftView.TotalTax IS NOT NULL AND StoreDraftView.TotalTax != 0) OR (StoreDraftView.VAT IS NOT NULL AND StoreDraftView.VAT != 0))";
        }

        if ((!string.IsNullOrEmpty(cogIDs) || !string.IsNullOrEmpty(pegIDs) || !string.IsNullOrEmpty(peIDs) ||
             !string.IsNullOrEmpty(coIDs)) && !string.IsNullOrEmpty(gWhere))
        {
            // ------------------------
            // ids: 1,2,5
            // where: '&1%' AND ('&2%' OR '&5%')
            // it could be better, but i dont know regex well :(
            // ------------------------
            where += "\r\nAND (\r\n";
            if (!string.IsNullOrEmpty(cogIDs))
            {
                //  groupings: '*1%', '!*1%'
                string[] cGroupingIDs = cogIDs.Split(',');
                for (int i = 0; i < cGroupingIDs.Length; i++)
                {
                    StringBuilder allIds = new StringBuilder();
                    int j = 0;
                    IDataReader childIds =
                        DbProxy.Instance.ExecuteReader("SELECT ID FROM [GetAllGroupingChilds](" + cGroupingIDs[i] + ")");
                    while (childIds.Read())
                    {
                        if (j != 0)
                        {
                            allIds.Append(",");
                        }
                        allIds.Append(childIds.GetValue(0));
                        j++;
                    }
                    if (factors.Checked)
                    {
                        gWhere = gWhere.Replace("'*" + cGroupingIDs[i] + "%'",
                            @" StoreDraftView.ID IN (
                        SELECT DISTINCT StoreDraftID FROM StoreDraftItems 
                        LEFT OUTER JOIN StoreServices ON StoreDraftItems.StoreServiceID = StoreServices.ID
                        LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                        LEFT OUTER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID
                        WHERE ServicePrice.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds +
                            "))) \r\n");
                        gWhere = gWhere.Replace("'!*" + cGroupingIDs[i] + "%'",
                            @" StoreDraftView.ID IN (
                        SELECT DISTINCT StoreDraftID FROM StoreDraftItems 
                        LEFT OUTER JOIN StoreServices ON StoreDraftItems.StoreServiceID = StoreServices.ID
                        LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                        LEFT OUTER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID             
                        WHERE NOT ServicePrice.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds +
                            "))) \r\n");
                    }
                    else
                    {
                        gWhere = gWhere.Replace("'*" + cGroupingIDs[i] + "%'",
                            " ServicePrice.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds +
                            ")) \r\n");
                        gWhere = gWhere.Replace("'!*" + cGroupingIDs[i] + "%'",
                            " NOT ServicePrice.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds +
                            ")) \r\n");
                    }
                }
            }
            if (!string.IsNullOrEmpty(pegIDs))
            {
                // personnel groups: '&1%', '!&1%'
                string[] pGroupingIDs = pegIDs.Split(',');
                for (int i = 0; i < pGroupingIDs.Length; i++)
                {
                    StringBuilder allIds = new StringBuilder();
                    int j = 0;
                    IDataReader childIds =
                        DbProxy.Instance.ExecuteReader("SELECT ID FROM [GetAllGroupingChilds](" + pGroupingIDs[i] + ")");
                    while (childIds.Read())
                    {
                        if (j != 0)
                        {
                            allIds.Append(",");
                        }
                        allIds.Append(childIds.GetValue(0));
                        j++;
                    }
                    gWhere = gWhere.Replace("'$" + pGroupingIDs[i] + "%'",
                        "Personnel.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + ")) \r\n");
                    gWhere = gWhere.Replace("'!$" + pGroupingIDs[i] + "%'",
                        " NOT Personnel.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds +
                        ")) \r\n");
                }
            }
            if (!string.IsNullOrEmpty(peIDs))
            {
                // personnel groups: '&1%', '!&1%'
                string[] pgids = peIDs.Split(',');
                for (int i = 0; i < pgids.Length; i++)
                {
                    gWhere = gWhere.Replace("'&" + pgids[i] + "%'",
                        "Personnel.PersonnelGroupID IN (" + pgids[i] + ") \r\n");
                    gWhere = gWhere.Replace("'!&" + pgids[i] + "%'",
                        " NOT Personnel.PersonnelGroupID IN (" + pgids[i] + ") \r\n");
                }
            }

            if (!string.IsNullOrEmpty(coIDs))
            {
                // ServiceType: '^1%', '!^1%'
                string[] cgids = coIDs.Split(',');
                for (int i = 0; i < cgids.Length; i++)
                {
                    StringBuilder allIds = new StringBuilder();
                    int j = 0;
                    IDataReader childIds =
                        DbProxy.Instance.ExecuteReader("SELECT ID FROM [GetAllServiceTypeChilds](" + cgids[i] + ")");
                    while (childIds.Read())
                    {
                        if (j != 0)
                        {
                            allIds.Append(",");
                        }
                        allIds.Append(childIds.GetValue(0));
                        j++;
                    }
                    if (factors.Checked)
                    {
                        gWhere = gWhere.Replace("'^" + cgids[i] + "%'",
                            @" StoreDraftView.ID IN (
                        SELECT DISTINCT StoreDraftID FROM StoreDraftItems 
                        LEFT OUTER JOIN StoreServices ON StoreDraftItems.StoreServiceID = StoreServices.ID
                        LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                        LEFT OUTER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID
                        LEFT OUTER JOIN Service ON ServicePrice.ServiceID = Service.ID
                        WHERE Service.ServiceTypeID IN (" + allIds + ")) \r\n");
                        gWhere = gWhere.Replace("'!^" + cgids[i] + "%'",
                            @" StoreDraftView.ID IN (
                        SELECT DISTINCT StoreDraftID FROM StoreDraftItems 
                        LEFT OUTER JOIN StoreServices ON StoreDraftItems.StoreServiceID = StoreServices.ID
                        LEFT OUTER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
                        LEFT OUTER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID
                        LEFT OUTER JOIN Service ON ServicePrice.ServiceID = Service.ID
                        WHERE NOT Service.ServiceTypeID IN (" + allIds + ")) \r\n");
                    }
                    else
                    {
                        gWhere = gWhere.Replace("'^" + cgids[i] + "%'",
                            " ServiceView.ServiceTypeID IN (" + allIds + ") \r\n");
                        gWhere = gWhere.Replace("'!^" + cgids[i] + "%'",
                            " NOT ServiceView.ServiceTypeID IN (" + allIds + ") \r\n");
                    }
                }

            }

            where += gWhere;
            where += "\r\n)\r\n";
        }
        query += where;
        if (CommoditySum.Checked)
        {
            query +=
                "GROUP BY StoreServiceID, StoreServiceName, ServiceView.Code, ServiceView.UnitName, StoreDraftItemsView.Label ";
        }
        #endregion
        if (count < 12)
        {
                totalQuery += query + " UNION ALL ";
        }
        else
        {
            if (mDividedMonth.Checked)
            {
                totalQuery += query + " ) S ";
            }
            else
            {
                totalQuery = query;
            }
            if (!string.IsNullOrEmpty(SortCol))
                totalQuery += " ORDER BY " + SortCol + " " + SortOrder;
            if (!CommoditySum.Checked && string.IsNullOrEmpty(SortCol))
            {
                totalQuery += " ORDER BY FactorDate, BuyerID, DATEADD(dd, 0, DATEDIFF(dd, 0, FactorDate)), NoSerial, ID";
            }

            IDataReader items = DbProxy.Instance.ExecuteReader(totalQuery);
            try
            {
                mGrid.DataSource = items;
                mGrid.DataBind();
            }
            finally
            {
                if (null != items)
                {
                    items.Close();
                    items.Dispose();
                }
            }
        }
    }

protected string SortCol
    {
        get { return Helper.GetString(ViewState["SortCol"], null); }
        set { ViewState["SortCol"] = value; }
    }

    protected string SortOrder
    {
        get { return Helper.GetString(ViewState["SortOrder"], "asc"); }
        set { ViewState["SortOrder"] = value; }
    }

    #endregion

    // ------------------------
    // ids: 1,2,5
    // where: '&1%' AND ('&2%' OR '&5%')
    // ------------------------
    private void GetGroupWhere(out string cgIDs, out string pgIDs, out string cIDs, out string pIDs, out string where)
    {
        cIDs = "";
        pIDs = "";
        cgIDs = "";
        pgIDs = "";
        where = "";

        // grouping members : '&1%', '!&1%'
        // category = 1375 AND ( category != 1076 OR category = 1075 ) 
        // --> '&1375%' AND ('!&1076%' OR '&1075%')
        where = mQueryBuilderSql.Value.Trim();
        where += " "; // we need this space at end to find IDs
        while (where.Contains("SGCategory = "))
        {
            int index = where.IndexOf("SGCategory = ") + "SGCategory = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            cgIDs += id + ",";
            where = where.Replace("SGCategory = " + id + " ", "'*" + id + "%' ");
        }
        while (where.Contains("SGCategory != "))
        {
            int index = where.IndexOf("SGCategory != ") + "SGCategory != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            cgIDs += id + ",";
            where = where.Replace("SGCategory != " + id + " ", "'!*" + id + "%' ");
        }
        if (cgIDs.EndsWith(","))
            cgIDs = cgIDs.Substring(0, cgIDs.Length - ",".Length);
        while (where.Contains("PGCategory = "))
        {
            int index = where.IndexOf("PGCategory = ") + "PGCategory = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pgIDs += id + ",";
            where = where.Replace("PGCategory = " + id + " ", "'$" + id + "%' ");
        }
        while (where.Contains("PGCategory != "))
        {
            int index = where.IndexOf("PGCategory != ") + "PGCategory != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pgIDs += id + ",";
            where = where.Replace("PGCategory != " + id + " ", "'!$" + id + "%' ");
        }
        if (pgIDs.EndsWith(","))
            pgIDs = pgIDs.Substring(0, pgIDs.Length - ",".Length);
        while (where.Contains("SCategory = "))
        {
            int index = where.IndexOf("SCategory = ") + "SCategory = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            cIDs += id + ",";
            where = where.Replace("SCategory = " + id + " ", "'^" + id + "%' ");
        }
        while (where.Contains("SCategory != "))
        {
            int index = where.IndexOf("SCategory != ") + "SCategory != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            cIDs += id + ",";
            where = where.Replace("SCategory != " + id + " ", "'!^" + id + "%' ");
        }
        if (cIDs.EndsWith(","))
            cIDs = cIDs.Substring(0, cIDs.Length - ",".Length);

        while (where.Contains("PCategory = "))
        {
            int index = where.IndexOf("PCategory = ") + "PCategory = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pIDs += id + ",";
            where = where.Replace("PCategory = " + id + " ", "'&" + id + "%' ");
        }
        while (where.Contains("PCategory != "))
        {
            int index = where.IndexOf("PCategory != ") + "PCategory != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pIDs += id + ",";
            where = where.Replace("PCategory != " + id + " ", "'!&" + id + "%' ");
        }
        if (pIDs.EndsWith(","))
            pIDs = pIDs.Substring(0, pIDs.Length - ",".Length);
       }

    protected void doExcel_Click(object sender, EventArgs e)
    {
        Exporter.ExportExcel(Response
            , new List<string>(new string[] { mGridTitle.Text })
            , new List<GridView>(new GridView[] { mGrid }));
    }
}