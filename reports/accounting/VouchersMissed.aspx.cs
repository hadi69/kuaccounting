﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

public partial class reports_accounting_VouchersMissed : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        msg.Visible = false;
        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["MissedVouchers"];
            System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
            int year = cal.GetYear(DateTime.Now);
            IDataReader financeYearStartDate = DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");//0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            //mFromDate.Text = 
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);
        }
    }

    protected void doShow_Click(object sender, EventArgs e)
    {
        DateTime start = Helper.ParseDate(mFromDate.Text, Null.NullDate);
        DateTime end = Helper.ParseDate(mToDate.Text, Null.NullDate);
        if (Null.IsNull(start))
        {
            msg.Text = LangProvider.Instance["InvalidStartDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(end))
        {
            msg.Text = LangProvider.Instance["InvalidEndDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(start) || Null.IsNull(end))
            return;
        List<int> ids = AccController.GetVoucherNOs(start
            , end
            , Helper.GetInt(mFromVoucherID.Text, 0), Helper.GetInt(mToVoucherID.Text, int.MaxValue - 1));

        int fromNo = Helper.GetInt(mFromVoucherID.Text, Null.NullInteger);
        int toNo = Helper.GetInt(mToVoucherID.Text, Null.NullInteger);
        List<int> res = new List<int>();
        if (ids == null || ids.Count == 0)
        {
            if (fromNo != Null.NullInteger && toNo != Null.NullInteger)
                for (int i = fromNo; i <= toNo; i++)
                    res.Add(i);
        }
        else
        {
            if (fromNo != Null.NullInteger)
                for (int i = fromNo; i < ids[0] - 1; i++)
                    res.Add(i);
            for (int i = ids[0] + 1; i < ids[ids.Count - 1]; i++)
                if (!ids.Contains(i))
                    res.Add(i);
            if (toNo != Null.NullInteger)
                for (int i = ids[ids.Count - 1] + 1; i < ids[0] - 1; i++)
                    res.Add(i);
        }

        StringBuilder b = new StringBuilder();
        if (res.Count > 0)
        {
            b.Append(res[0]);
            for (int i = 1; i < res.Count; i++)
            {
                b.Append(", ");
                b.Append(res[i]);
            }
        }
        mIDs.Text = b.ToString();
    }
}