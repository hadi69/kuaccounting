﻿<%@ Page Language="C#" Title="" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" CodeFile="TrustAndInternalReport.aspx.cs" Inherits="reports_accounting_TrustAndInternalReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register src="~/controls/ReportSetting.ascx" tagName="ReportSetting" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" Runat="Server">
    <div id="bar">
    </div>
    <uc1:ReportSetting ID="mReportSetting" runat="server" />
    <div class="well callout padding-5">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
            <div class="form-group">
                    <asp:Label ID="Based" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.Based</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:DropDownList runat="server" ID="DLBase" CssClass="chosen-select chosen-rtl" OnSelectedIndexChanged="DlBaseChange" AutoPostBack="True">
                           <asp:ListItem Text="Lang.GuildTrust" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Lang.RepresentativeTrust" Value="3"></asp:ListItem>
                            <asp:ListItem Text="Lang.ConsignmentSale" Value="4"></asp:ListItem>
                            <asp:ListItem Text="Lang.GuildTrustFrom" Value="5"></asp:ListItem>
                            <asp:ListItem Text="Lang.ConsignmentBuy" Value="6"></asp:ListItem>
                            <asp:ListItem Text="Lang.InternalUse" Value="1"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
            </div>
         </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" AssociatedControlID="mFromDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="mFromDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label3" runat="server" AssociatedControlID="mToDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="mToDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="personLable" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Person</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mPersons" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>

            <asp:Panel ID="panelInternal" runat="server" Visible="False">
                 <div class="col-xs-12 col-sm-6 form-group-container">
                    <div class="form-group">
                        <asp:Label ID="TransfereeLabel" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Transferee</asp:Label>
                        <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                            <asp:DropDownList ID="mTransferee" runat="server" CssClass="chosen-select chosen-rtl" />
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container display-none">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4">&nbsp;</div>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox runat="server" ID="mAggregate" Text="Lang.Aggregate" CssClass="mAggregate" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin-5-top">
            <div class="col-md-8 col-lg-6 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container form-col-without-padding">
                <div class="col-xs-12 col-sm-3">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
                </div>
                <div class="col-xs-12 col-sm-3">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcel_Click" Enabled="false" />
                </div>
                <div class="col-xs-12 col-sm-3">
                    <input type="button" id="print" class="btn btn-xs btn-dark-blue btn-block" value="" />
                </div>
            </div>
        </div>
    </div>

   <asp:Panel ID="panelDropDowns" runat="server" CssClass="well callout padding-5" Visible="False">
        <input type="hidden" runat="server" id="mQueryBuilder" class="mQueryBuilder" />
        <input type="hidden" runat="server" id="mQueryBuilderSql" class="mQueryBuilderSql" />
        <div class="hidden">
            <asp:DropDownList runat="server" ID="mCompanyUnits" CssClass="mCompanyUnits"></asp:DropDownList>           
        </div>
        <div class="row">
            <div class="col-xs-12">
                <h5 class="dashed-bottom padding-5-bottom yekan-family">
                    <asp:Label runat="server">Lang.CompanyUnit</asp:Label>
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="ltr builder">
                    <div id="builder"></div>
                </div>
            </div>
        </div>
     </asp:Panel>
    <div class="col-xs-12">
        <asp:Label runat="server" ID="msg" CssClass="label-30 alert alert-danger block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
    </div>
    <asp:Panel ID="gridPanel" runat="server" Visible="false">
        <div runat="server" id="printButton">
            <a class="btn btn-xs btn-primary" id="factors-select-all">
                <i class="fa fa-check"></i>
                <asp:Label runat="server" Text="Lang.SelectAll"></asp:Label>
            </a>
            <a class="btn btn-xs btn-primary" id="factors-select-none">
                <i class="fa fa-remove"></i>
                <asp:Label runat="server" Text="Lang.SelectNone"></asp:Label>
            </a>
            <a class="btn btn-xs btn-primary" id="factors-print">
                <i class="fa fa-print"></i>
                <asp:Label runat="server" Text="Lang.PrintFactors"></asp:Label>
            </a>
        </div>
        <JP:MiniHeader runat="server" Text="Lang.DistReport" ID="mGridTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="mGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true" OnSorting="mGrid_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.Print">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkPrintID" runat="server" />
                            </ItemTemplate>
                            <ItemStyle CssClass="align-center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="NOSerial" HeaderText="Lang.No" ItemStyle-HorizontalAlign="Center" SortExpression="NOSerial" />
                        <asp:BoundField DataField="CompanyUnit" HeaderText="Lang.TranstereeUnit" ItemStyle-CssClass="tl align-center" SortExpression="CompanyUnit" />
                        <asp:BoundField DataField="PersonName" HeaderText="Lang.PersonName" ItemStyle-CssClass="tl align-center" SortExpression="PersonName" />
                        <asp:BoundField DataField="PersonnelName" HeaderText="Lang.ActorName" ItemStyle-CssClass="tl align-center" SortExpression="PersonnelName" />
                        <asp:BoundField DataField="AccepterName" HeaderText="Lang.Transferee" ItemStyle-CssClass="tl align-center" SortExpression="AccepterName" />
                       <asp:TemplateField HeaderText="Lang.FactorDate" SortExpression="FactorDate">
                            <ItemTemplate>
                                <asp:Label ID="lblGridDate" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="StoreServiceName" HeaderText="Lang.CommodityName" ItemStyle-CssClass="tl align-center" SortExpression="StoreServiceName" />
                        <asp:BoundField DataField="UnitName" HeaderText="Lang.Units" ItemStyle-CssClass="tl align-center" SortExpression="UnitName" />
                        <asp:BoundField DataField="DeliveredAmount" HeaderText="Lang.NumOrValue" ItemStyle-CssClass="tl align-center" SortExpression="DeliveredAmount" />
                        <asp:BoundField DataField="UnitCurrencyConverter" HeaderText="Lang.UnitCurrencyConverter" ItemStyle-CssClass="tl align-center" SortExpression="UnitCurrencyConverter" />
                        <asp:TemplateField HeaderText="Lang.Price" SortExpression="Price">
                            <ItemTemplate>
                                <asp:Label ID="Price" runat="server" Text='<%#Eval("Price") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalPrice" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.TotalPrice" SortExpression="TotalPrice">
                            <ItemTemplate>
                                <asp:Label ID="TotalPrice" runat="server" Text='<%#Eval("TotalPrice") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalTotalPrice" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
    <script>
        $(function () {

            $('#print').attr('value', Lang.Print);
            $('#print').click(function () {
                var head = Lang.PaperBook;
                <%--if ($("#<%=ddlBy.ClientID %>").val() == <%= (int)AccController.PersonBookGroupBy.ByTotal %>) {
                    head = '<%=jpMiniHeaderTotalBills.Text %>';
            }--%>
                printTable('.ScrollPanelCartable', '../../', head);
            });
            $('.GridRow,.GridAltRow').click(function () {
                $(this).toggleClass('selected');
            });
            dateSelect('.dateselect');
            $('.monetary').keyup(function () {
                $(this).val(numberWithCommas(stripCommas($(this).val())));
            });
            $("#bar").prepend(breadcrumb({
                title: Lang.TrustAndInternalReport,
                parent: {
                    title: Lang.Reports,
                    url: 'menu/reports.aspx'
                }
            }));
            setClassReports();
            var cats = {};
            $('.mCompanyUnits option').each(function () {
                cats[$(this).attr('value')] = $(this).text();
            });
            $jQuery1_9('#builder').queryBuilder({
                filters: [
                    {
                        id: 'category',
                        label: Lang.CompanyUnit,
                        type: 'integer',
                        input: 'select',
                        values: cats,
                        operators: ['equal', 'not_equal']
                    }
                ]
            });
            // save rules before post back
            $('.doShow').click(function () {
                var rules = $jQuery1_9('#builder').queryBuilder('getRules');
                var rulesStr = JSON.stringify(rules);
                // alert(rulesStr);
                $('.mQueryBuilder').val(rulesStr);
                $('.mQueryBuilderSql').val($jQuery1_9('#builder').queryBuilder('getSQL', false, false).sql);
            });
            //reload saved rules
            $jQuery1_9('#builder').queryBuilder('reset');
            var ruleStr = $('.mQueryBuilder').val();
            if (ruleStr != '') {
                var rule = eval("(" + ruleStr + ")");
                try {
                    $jQuery1_9('#builder').queryBuilder('setRules', rule);
                } catch (e) {
                }
            }

            $(document).ready(function() {
                $("#factors-select-all").click(function() {
                    $(".data-print input").attr("checked", "checked");
                });

                $("#factors-select-none").click(function() {
                    $(".data-print input").attr("checked", "");
                });

                // Handle printing
                $("#factors-print").click(function() {
                    var checkedFactors = $(".data-print input:checked"),
                        ids = [],
                        printUrl = '',
                        baseURlLength = 100,
                        totalIds,
                        indexOfPipe = 0;
                    if (checkedFactors.length == 0) {
                        alert(Lang.PleaseCheckAtLeastOneFactor);
                        return;
                    }

                    $.each(checkedFactors, function() {
                        ids.push($(this).attr("data-id"));
                    });
                    printUrl = rootPath + Router.Generate("print-storedraft", { params: { ids: ids.join('|'), title: 'SalesInvoice' }, query: { report: 'simple', print: "true" } });

                    totalIds= ids.join('|');

                    while (indexOfPipe >= 0) {
                        indexOfPipe = totalIds.indexOf('|', 190);
                        if (indexOfPipe >= 0) {
                            window.open(rootPath + Router.Generate("print-storedraft", { params: { ids: totalIds.substr(0, indexOfPipe), title: 'SalesInvoice' }, query: { report: 'simple', print: "true" } }));
                        }

                        totalIds = totalIds.substr(indexOfPipe + 1);
                    }
                    window.open(rootPath + Router.Generate("print-storedraft", { params: { ids: totalIds, title: 'SalesInvoice' }, query: { report: 'simple', print: "true" } }));
                });
            });

        });
    </script>
</asp:Content>

