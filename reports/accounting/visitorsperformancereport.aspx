﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" CodeFile="VisitorsPerformanceReport.aspx.cs" Inherits="reports_accounting_visitorsperformancereport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/controls/ReportSetting.ascx" TagName="ReportSetting" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>
    <uc1:ReportSetting ID="mReportSetting" runat="server" />
    <div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="visitorLable" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Visitor</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mVisitors" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" AssociatedControlID="mFromDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="mFromDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label3" runat="server" AssociatedControlID="mToDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="mToDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-lg-9 visible-md visible-lg col-lg-offset-1">
                <div>&nbsp;</div>
                <asp:CheckBox runat="server" ID="mShowVisitedCustomers" Text="Lang.VisitedCustomers" Checked="true" OnCheckedChanged="OnCheckedChanged" />
                <asp:CheckBox runat="server" ID="mShowNotVisitedCustomers" Text="Lang.NotVisitedCustomers" Checked="true" OnCheckedChanged="OnCheckedChanged" />
                <asp:CheckBox runat="server" ID="mShowConfirmedStoredraft" Text="Lang.ConfirmedStoredraft" Checked="true" OnCheckedChanged="OnCheckedChanged" />
                <asp:CheckBox runat="server" ID="mShowNotConfirmedStoredraft" Text="Lang.NotConfirmedStoredraft" Checked="true" OnCheckedChanged="OnCheckedChanged" />
            </div>
        </div>
        <div class="row margin-5-top">
            <div class="col-md-8 col-lg-6 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 col-lg-offset-7 form-group-container form-col-without-padding">
                <div class="col-xs-12 col-sm-3">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
                </div>
                <div class="col-xs-12 col-sm-3">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcel_Click" Enabled="false" />
                </div>
                <div class="col-xs-12 col-sm-3">
                    <input type="button" id="print" class="btn btn-xs btn-dark-blue btn-block" value="" />
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="VisitedCustomersGridPanel" runat="server" Visible="False">
        <JP:MiniHeader runat="server" Text="Lang.VisitedCustomers" ID="VisitedCustomersGridTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="VisitedCustomersGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="VisitedCustomersGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true" OnSorting="VisitedCustomersGrid_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.VisitorPlanDayDate" SortExpression="VisitorPlanDayDate">
                            <ItemTemplate>
                                <asp:Label ID="lblGridDate" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="CustomerName" HeaderText="Lang.CustomerName" ItemStyle-CssClass="tl align-center" SortExpression="CustomerName" />
                        <asp:BoundField DataField="VisitorName" HeaderText="Lang.Visitor" ItemStyle-CssClass="tl align-center" SortExpression="VisitorName" />
                        <asp:TemplateField HeaderText="Lang.BuyAmount" SortExpression="Price">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("Price") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="TotalPrice" runat="server" Text="0" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="FGroupName" HeaderText="Lang.FirstGroup" ItemStyle-CssClass="tl align-center" SortExpression="FGroupName" />
                        <asp:BoundField DataField="SGroupName" HeaderText="Lang.SecondGroup" ItemStyle-CssClass="tl align-center" SortExpression="SGroupName" />
                        <asp:BoundField DataField="Address" HeaderText="Lang.Address" ItemStyle-CssClass="tl align-center" SortExpression="Address" />
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="NotVisitedCustomersGridPanel" runat="server" Visible="False">
        <JP:MiniHeader runat="server" Text="Lang.NotVisitedCustomers" ID="NotVisitedCustomersGridTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="NotVisitedCustomersGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="NotVisitedCustomersGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true" OnSorting="NotVisitedCustomersGrid_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.VisitorPlanDayDate" SortExpression="VisitorPlanDayDate">
                            <ItemTemplate>
                                <asp:Label ID="lblGridDate" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="CustomerName" HeaderText="Lang.CustomerName" ItemStyle-CssClass="tl align-center" SortExpression="CustomerName" />
                        <asp:BoundField DataField="FGroupName" HeaderText="Lang.FirstGroup" ItemStyle-CssClass="tl align-center" SortExpression="FGroupName" />
                        <asp:BoundField DataField="SGroupName" HeaderText="Lang.SecondGroup" ItemStyle-CssClass="tl align-center" SortExpression="SGroupName" />
                        <asp:BoundField DataField="Address" HeaderText="Lang.Address" ItemStyle-CssClass="tl align-center" SortExpression="Address" />
                        <asp:BoundField DataField="Comments" HeaderText="Lang.NotVisitReason" ItemStyle-CssClass="tl align-center" SortExpression="Comments" />
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="ConfirmedStoredraftGridPanel" runat="server" Visible="False">
        <JP:MiniHeader runat="server" Text="Lang.ConfirmedStoredraft" ID="ConfirmedStoredraftGridTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="ConfirmedStoredraftGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="ConfirmedStoredraftGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true" OnSorting="ConfirmedStoredraftGrid_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="NOSerial" HeaderText="Lang.No" ItemStyle-HorizontalAlign="Center" SortExpression="NOSerial" />
                        <asp:TemplateField HeaderText="Lang.FactorDate" SortExpression="FactorDate">
                            <ItemTemplate>
                                <asp:Label ID="lblGridDate" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="BuyerName" HeaderText="Lang.BuyerName" ItemStyle-CssClass="tl align-center" SortExpression="BuyerName" />
                        <asp:BoundField DataField="Comments" HeaderText="Lang.Comments" ItemStyle-CssClass="tl align-center" SortExpression="Comments" />
                        <asp:TemplateField HeaderText="Lang.TotalAmount" SortExpression="TotalAmount">
                            <ItemTemplate>
                                <asp:Label ID="TotalAmount" runat="server" Text='<%#Eval("TotalAmount") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalTotalAmount" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Pursant" SortExpression="Pursant">
                            <ItemTemplate>
                                <asp:Label ID="Pursant" runat="server" Text='<%#Eval("Pursant") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalPursant" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.FinalPrice" SortExpression="TotalPrice">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("TotalPrice") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalTotalPrice" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="NotConfirmedStoredraftGridPanel" runat="server" Visible="False">
        <JP:MiniHeader runat="server" Text="Lang.NotConfirmedStoredraft" ID="NotConfirmedStoredraftGridTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="NotConfirmedStoredraftGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="NotConfirmedStoredraftGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true" OnSorting="NotConfirmedStoredraftGrid_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="NOSerial" HeaderText="Lang.No" ItemStyle-HorizontalAlign="Center" SortExpression="NOSerial" />
                        <asp:TemplateField HeaderText="Lang.FactorDate" SortExpression="FactorDate">
                            <ItemTemplate>
                                <asp:Label ID="lblGridDate" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="BuyerName" HeaderText="Lang.BuyerName" ItemStyle-CssClass="tl align-center" SortExpression="BuyerName" />
                        <asp:BoundField DataField="Comments" HeaderText="Lang.Comments" ItemStyle-CssClass="tl align-center" SortExpression="Comments" />
                        <asp:TemplateField HeaderText="Lang.TotalAmount" SortExpression="TotalAmount">
                            <ItemTemplate>
                                <asp:Label ID="TotalAmount" runat="server" Text='<%#Eval("TotalAmount") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalTotalAmount" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Pursant" SortExpression="Pursant">
                            <ItemTemplate>
                                <asp:Label ID="Pursant" runat="server" Text='<%#Eval("Pursant") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalPursant" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.FinalPrice" SortExpression="TotalPrice">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("TotalPrice") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalTotalPrice" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
    <script>
        $(function () {

            $('#print').attr('value', Lang.Print);
            $('#print').click(function () {
                var head = Lang.VisitorsPerformanceReport;
                <%--if ($("#<%=ddlBy.ClientID %>").val() == <%= (int)AccController.PersonBookGroupBy.ByTotal %>) {
                    head = '<%=jpMiniHeaderTotalBills.Text %>';
            }--%>
                printTable('.ScrollPanelCartable', '../../', head);
            });
            $('.GridRow,.GridAltRow').click(function () {
                $(this).toggleClass('selected');
            });
            dateSelect('.dateselect');
            $('.monetary').keyup(function () {
                $(this).val(numberWithCommas(stripCommas($(this).val())));
            });
            $("#bar").prepend(breadcrumb({
                title: Lang.VisitorsPerformanceReport,
                parent: {
                    title: Lang.MDistribution,
                    url: 'menu/distribution.aspx'
                }
            }));
            setClassDistribution();

        });
    </script>
</asp:Content>

