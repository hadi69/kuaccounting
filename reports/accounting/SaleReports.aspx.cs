﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;


public partial class reports_accounting_SalesReports : System.Web.UI.Page
{
    DropDownList[] personsDLA;
    DropDownList[] CommoditiesDLA;
    System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();

    protected void Page_Load(object sender, EventArgs e)
    {
        msg.Visible = false;
        // umed
        CreateDropDowns();

        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["SaleReports"];
            Page.SetLang();
            int year = cal.GetYear(DateTime.Now);
            //mFromDate.Text = string.Format("{0}/01/01", year - 1300);
            //mToDate.Text = Helper.FormatDate(cal.ToDateTime(year + 1, 1, 1, 1, 1, 1, 1).AddDays(-1), "yy/MM/dd");
            IDataReader financeYearStartDate = DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");//0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            //mFromDate.Text = 
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);
            Init();
        }
    }
    void Init()
    {
        IDataReader cooperationsType = DbProxy.Instance.ExecuteReader("SELECT ID, Title FROM CooperationType");
        Cooperations.Items.Clear();
        Cooperations.Items.Add(new ListItem(LangProvider.Instance["AllCooperation"], ""));
        if (cooperationsType != null)
        {
            while (cooperationsType.Read())
            {
                if (!cooperationsType.IsDBNull(0) && !cooperationsType.IsDBNull(1))
                    Cooperations.Items.Add(new ListItem(cooperationsType.GetString(1), cooperationsType.GetInt32(0).ToString()));
            }
            cooperationsType.Close();
        }

        IDataReader persons = DbProxy.Instance.ExecuteReader("SELECT ID, [dbo].[DisplayPerson](ID, 1), ISNULL(Code, '') FROM Personnel WHERE Enabled = 1 AND Type=101");
        Person.Items.Clear();
        Person.Items.Add(new ListItem(LangProvider.Instance["AllPersons"], ""));
        if (persons != null)
        {
            while (persons.Read())
            {
                if (!persons.IsDBNull(0) && !persons.IsDBNull(1) && !persons.IsDBNull(2))
                    Person.Items.Add(new ListItem(persons.GetString(1) + " - کد: " + persons.GetString(2),
                        persons.GetInt32(0).ToString()));
            }
            persons.Close();
        }
        Person.SelectedIndex = 0;

        IDataReader Commodities = DbProxy.Instance.ExecuteReader("SELECT ID, ServiceName+'_'+ModelName ServiceName FROM ServicePriceView ORDER BY ServiceName");
        Commodity.Items.Clear();
        Commodity.Items.Add(new ListItem(LangProvider.Instance["AllCommodities"], ""));
        if (Commodities != null)
        {
            while (Commodities.Read())
            {
                if (!Commodities.IsDBNull(0) && !Commodities.IsDBNull(1))
                    Commodity.Items.Add(new ListItem(Commodities.IsDBNull(1) ? "" : Commodities.GetString(1),
                        Commodities.IsDBNull(0) ? "" : Commodities.GetInt32(0).ToString()));
            }
            Commodities.Close();
        }
        Commodity.SelectedIndex = 0;

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        // umed
        CreateDropDowns();
    }
    void CreateDropDowns()
    {
        string brTag = "<br/>";
        int numOfPersonsDl = Convert.ToInt32(PersonsGroupTextBox.Text);
        int numOfCommoditiesDL = Convert.ToInt32(CommoditisGroupTextBox.Text);
        personsDLA = new DropDownList[numOfPersonsDl];
        CommoditiesDLA = new DropDownList[numOfCommoditiesDL];
        string containerCssClass = "col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container";
        string dropDownCssClass = "chosen-select chosen-rtl";
        //umed
        PersonsGroupDiv.Controls.Clear();
        if (numOfPersonsDl > 0 && byPersons.Checked)
        {
            for (int i = 0; i <= numOfPersonsDl - 1; i++)
            {
                DropDownList PersonsDL = new DropDownList();
                PersonsDL.CssClass = dropDownCssClass;
                Panel panelContainer = new Panel();
                panelContainer.CssClass = containerCssClass;

                IDataReader Grouping = DbProxy.Instance.ExecuteReader("SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping  WHERE GroupingType = 0");
                PersonsDL.Items.Clear();
                PersonsDL.Items.Add(new ListItem(LangProvider.Instance["AllGroup"], ""));
                if (Grouping != null)
                {
                    while (Grouping.Read())
                    {
                        if (!Grouping.IsDBNull(0) && !Grouping.IsDBNull(1))
                            PersonsDL.Items.Add(new ListItem(Grouping.GetString(1), Grouping.GetInt32(0).ToString()));
                    }
                    Grouping.Close();
                }

                PersonsDL.ID = "PersonsDL" + i;
                panelContainer.Controls.Add(PersonsDL);
                PersonsGroupDiv.Controls.Add(panelContainer);
                personsDLA[i] = PersonsDL;

            }
        }

        //umed
        CommodityGroupDiv.Controls.Clear();
        if (numOfCommoditiesDL > 0 && byCommodities.Checked)
        {
            for (int i = 0; i <= numOfCommoditiesDL - 1; i++)
            {
                Panel panelContainer = new Panel();
                panelContainer.CssClass = containerCssClass;
                

                DropDownList CommoditiesDL = new DropDownList();
                CommoditiesDL.CssClass = dropDownCssClass;

                IDataReader Grouping = DbProxy.Instance.ExecuteReader("SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping WHERE GroupingType = 1");
                CommoditiesDL.Items.Clear();
                CommoditiesDL.Items.Add(new ListItem(LangProvider.Instance["AllGroup"], ""));
                if (Grouping != null)
                {
                    while (Grouping.Read())
                        CommoditiesDL.Items.Add(new ListItem(Grouping.GetString(1), Grouping.GetInt32(0).ToString()));
                    Grouping.Close();
                }
                CommoditiesDL.ID = "CommoditiesDL" + i;
                panelContainer.Controls.Add(CommoditiesDL);
                CommodityGroupDiv.Controls.Add(panelContainer);
                CommoditiesDLA[i] = CommoditiesDL;
            }
        }

        if ((numOfCommoditiesDL > 0 && byCommodities.Checked) || (numOfPersonsDl > 0 && byPersons.Checked))
        {
            panelDropDowns.Visible = true;
        }
    }
    protected void OnSelectIndexChanged(object sender, EventArgs e)
    {

    }
    string rowStyle = "GridRow";
    decimal sumPrice = 0, sumNum = 0, _price = 0, _num = 0;
    int lastID = -2;
    protected void BindGrid()
    {
        DateTime start = Helper.ParseDate(mFromDate.Text, Null.NullDate);
        DateTime end = Helper.ParseDate(mToDate.Text, Null.NullDate);
        if (Null.IsNull(start))
        {
            msg.Text = LangProvider.Instance["InvalidStartDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(end))
        {
            msg.Text = LangProvider.Instance["InvalidEndDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(start) || Null.IsNull(end))
            return;
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);

        int cooperationTypeID = Helper.GetInt(Cooperations.SelectedValue, Null.NullInteger);
        int PersonID = Helper.GetInt(Person.SelectedValue, Null.NullInteger);
        int CommodityID = Helper.GetInt(Commodity.SelectedValue, Null.NullInteger);
        int numOfPersonsDl = Convert.ToInt32(PersonsGroupTextBox.Text);
        int numOfCommodityDL = Convert.ToInt32(CommoditisGroupTextBox.Text);
        StringBuilder personGroupIds = new StringBuilder();
        StringBuilder commodityGroupIds = new StringBuilder();
        string cooperations = "", persons = "", commodities = "", person = "", commodity = "";

        if (PersonID > 0 && byPerson.Checked)
        {
            person = string.Format(@"AND PersonnelView.ID = {0}", PersonID);
        }

        if (CommodityID > 0 && byCommodity.Checked)
        {
            commodity = string.Format(@"AND ServicePriceView.ID = {0}", CommodityID);
        }

        if (cooperationTypeID > 0)
        {
            cooperations = string.Format(@"AND CooperationType.ID = {0}", cooperationTypeID);
        }

        if (cooperationTypeID > 0)
        {
            cooperations = string.Format(@"AND CooperationType.ID = {0}", cooperationTypeID);
        }

        if (numOfPersonsDl > 0 && byPersons.Checked)
        {
            for (int i = 0; i < numOfPersonsDl; i++)
            {
                if (i == numOfPersonsDl - 1)
                {
                    personGroupIds.Append(personsDLA[i].SelectedValue);
                }
                else
                {
                    personGroupIds.Append(personsDLA[i].SelectedValue + ",");
                }
            }
            persons = string.Format(@"AND PersonnelView.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN({0}))", personGroupIds);
        }
        if (numOfCommodityDL > 0 && byCommodities.Checked)
        {
            for (int i = 0; i < numOfCommodityDL; i++)
            {
                if (i == numOfCommodityDL - 1)
                {
                    commodityGroupIds.Append(CommoditiesDLA[i].SelectedValue);
                }
                else
                {
                    commodityGroupIds.Append(CommoditiesDLA[i].SelectedValue + ",");
                }
            }
            commodities = string.Format(@"AND ServicePriceView.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN({0}))", commodityGroupIds);

        }

        string query = string.Format(@"SELECT ServicePriceView.ID ServicePriceID, [dbo].[DisplayPerson](PersonnelView.ID, 0) PersonName, StoreDraft.FactorDate, StoreServiceDefView.ServiceName, DeliveredAmount, Price, DeliveredAmount*Price TotalPrice FROM StoreDraftItems 
INNER JOIN StoreDraft ON StoreDraft.ID = StoreDraftItems.StoreDraftID
INNER JOIN StoreDraftPerson ON StoreDraftPerson.StoreDraftID = StoreDraft.ID
INNER JOIN PersonnelView ON PersonnelView.ID = StoreDraftPerson.PartnerID
LEFT JOIN CooperationType ON CooperationType.ID = PersonnelView.CooperationTypeID
INNER JOIN StoreServices ON StoreServices.ID = StoreDraftItems.StoreServiceID
INNER JOIN StoreServiceDefView ON StoreServiceDefView.ID = StoreServices.StoreServiceDefID
INNER JOIN ServicePriceView ON ServicePriceView.ID = StoreServiceDefView.ServicePriceID
WHERE StoreDraft.ID IN (SELECT ID FROM StoreDraft WHERE WorkflowStatus = 1)
AND StoreDraft.FactorDate >= {0}
AND StoreDraft.FactorDate <= {1}
_COOPERATIONS_
_PERSONS_
_COMMODITIES_
_PERSON_
_COMMODITY_
", Helper.QoutedDateTime(start), Helper.QoutedDateTime(end)).Replace("_COOPERATIONS_", cooperations).Replace("_PERSONS_", persons).Replace("_COMMODITIES_", commodities).Replace("_PERSON_", person).Replace("_COMMODITY_", commodity);
        if (!string.IsNullOrEmpty(SortCol))
            query += " ORDER BY " + SortCol + " " + SortOrder;
        IDataReader items = DbProxy.Instance.ExecuteReader(query);

        try
        {
            mGrid.DataSource = items;
            mGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Close();
                items.Dispose();
            }
        }

    }
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.Common.DbDataRecord record = e.Row.DataItem as System.Data.Common.DbDataRecord;
            DateTime dt = Helper.GetDateTime(record["FactorDate"], Helper.NullDate);
            if (dt != Helper.NullDate)
                e.Row.Cells[2].Text = Helper.FormatDateFull(Helper.GetDateTime(dt, Helper.NullDate));

            int id = Helper.GetInt(record["ServicePriceID"], -1);
            if (lastID != id)
            {
                rowStyle = rowStyle == "GridAltRow" ? "GridRow" : "GridAltRow";
                lastID = id;
            }
            decimal deliveredAmount = Helper.GetDecimal(record["DeliveredAmount"], 0);
            decimal price = SiteSettings.ToMonetary(Helper.GetDecimal(record["Price"], 0), dt);

            e.Row.Cells[5].Text = price.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.Cells[6].Text = ((deliveredAmount) * price).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            if (deliveredAmount > 0)
                e.Row.Cells[5].CssClass = e.Row.Cells[5].CssClass = "neg";

            e.Row.CssClass = rowStyle;
            _num = Helper.GetDecimal(record["DeliveredAmount"], 0);
            _price = SiteSettings.ToMonetary(Helper.GetDecimal(record["Price"], 0) * Helper.GetDecimal(record["DeliveredAmount"], 0), dt);
            sumNum += _num;
            sumPrice += _price;

        }

        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblTotal = (Label)e.Row.FindControl("SumNum");
            lblTotal.Text = sumNum.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            lblTotal = (Label)e.Row.FindControl("SumPrice");
            lblTotal.Text = sumPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }
    }
    protected void mGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        BindGrid();
    }

    protected string SortCol
    {
        get
        {
            return Helper.GetString(ViewState["SortCol"], null);
        }
        set
        {
            ViewState["SortCol"] = value;
        }
    }
    protected string SortOrder
    {
        get
        {
            return Helper.GetString(ViewState["SortOrder"], "asc");
        }
        set
        {
            ViewState["SortOrder"] = value;
        }
    }
    protected void doShow_Click(object sender, EventArgs e)
    {
        BindGrid();
    }
    protected void doExcel_Click(object sender, EventArgs e)
    {
        Exporter.ExportExcel(Response, mTitle.Text, mGrid);
    }
}
