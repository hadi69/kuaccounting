﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PaperBook.aspx.cs" Inherits="reports_accounting_PaperBook" %>

<%@ Register Src="../../controls/ReportSetting.ascx" TagName="ReportSetting" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>
<%--    <uc1:ReportSetting ID="mReportSetting" runat="server" />--%>
     <asp:Panel ID="panelDBs" runat="server" CssClass="well callout padding-5">
        <div class="row">
            <div class="col-xs-12">
                <h5 class="dashed-bottom padding-5-bottom yekan-family">
                    <asp:Label runat="server">Lang.DataBase</asp:Label>
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <asp:CheckBoxList runat="Server" ID="mDBList" RepeatLayout="Flow" RepeatDirection="Horizontal" />
            </div>
        </div>
     </asp:Panel>
    <div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
                <div class="form-group">
                    <span class="left-aligned-label control-label col-xs-12 col-sm-4">
                        <asp:Label runat="server">Lang.Amount</asp:Label>
                    </span>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mAmount" runat="server" CssClass="T form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label1" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="mFromDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="mToDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-lg-3 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 form-group-container pull-left">
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcel_Click" Enabled="false" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <input type="button" id="print" class="btn btn-sm btn-dark-blue btn-block" value="" />

                    <asp:TextBox ID="mFromVoucherID" runat="server" CssClass="T" Width="65px" Visible="false"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="fte1" runat="server" TargetControlID="mFromVoucherID" FilterType="Numbers" />
                    <asp:TextBox ID="mToVoucherID" runat="server" CssClass="T" Width="65px" Visible="false"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="fte2" runat="server" TargetControlID="mToVoucherID" FilterType="Numbers" />
                    <asp:DropDownList ID="mTotals" runat="server" CssClass="Combo" Width="220px" Visible="false" />
                    <cc1:ListSearchExtender runat="server" ID="lse1" TargetControlID="mTotals" PromptText=""></cc1:ListSearchExtender>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-xs-12">
        <asp:Label runat="server" ID="msg" CssClass="label-30 alert alert-danger block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
    </div>
    <asp:Panel ID="voucherDetailPanel" runat="server" Visible="False">
        <JP:MiniHeader runat="server" Text="Lang.PaperBook" ID="mTitle" />
        <asp:GridView ID="mGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
            Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mGrid_RowDataBound" GridLines="Both" EmptyDataText="Lang.DocumentIsEmpty"
            AllowSorting="true" OnSorting="mGrid_Sorting">
            <Columns>
                <asp:TemplateField HeaderText="Lang.ColRowNr">
                    <ItemTemplate>
                        <%# Container.DataItemIndex + 1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Date" HeaderText="Lang.Day" ItemStyle-HorizontalAlign="Center" SortExpression="Date" />
                <asp:BoundField DataField="Date" HeaderText="Lang.Month" ItemStyle-HorizontalAlign="Center" SortExpression="Date" />
                <asp:BoundField DataField="No" HeaderText="Lang.DocumentNumber" ItemStyle-HorizontalAlign="Center" SortExpression="No" />
                <asp:BoundField DataField="TopicCode" HeaderText="Lang.TopicCode" ItemStyle-CssClass="tl" ItemStyle-HorizontalAlign="Center" SortExpression="TopicCode" />
                <asp:BoundField DataField="TreePathName" HeaderText="Lang.Topic" ItemStyle-HorizontalAlign="Center" SortExpression="TreePathName" />
                <asp:TemplateField HeaderText="Lang.Comments" ItemStyle-HorizontalAlign="Center" SortExpression="Comments">
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%#Eval("Comments") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="sum" runat="server" Text=""></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="PersonnelName" HeaderText="Lang.Person" ItemStyle-HorizontalAlign="Center" SortExpression="PersonnelName" />
                <asp:TemplateField HeaderText="Lang.Liability" ItemStyle-HorizontalAlign="Center" SortExpression="Owe">
                    <ItemTemplate>
                        <asp:Label ID="owe" runat="server" Text='<%#Eval("Owe") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="oweTotal" runat="server" Text="Lang.Sum" />
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lang.Credit" ItemStyle-HorizontalAlign="Center" SortExpression="Due">
                    <ItemTemplate>
                        <asp:Label ID="due" runat="server" Text='<%#Eval("Due") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="dueTotal" runat="server" Text="Lang.Sum" />
                    </FooterTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle CssClass="GridHeader" />
            <RowStyle CssClass="GridRow" />
            <SelectedRowStyle CssClass="GridRowSelected" />
        </asp:GridView>
    </asp:Panel>
    <script>
        $(function () {
            $('#sum').attr('text', Lang.Print);
            $('#print').attr('value', Lang.Print);
            $('#print').click(function () {
                var title = Lang.PaperBook;
                if ($("#<%=mFromDate.ClientID %>").val() != '') {
                    title += '/' + Lang.FromDate + ': ' + $("#<%=mFromDate.ClientID %>").val();
                }
                if ($("#<%=mToDate.ClientID %>").val() != '') {
                    title += '/' + Lang.ToDate + ': ' + $("#<%=mToDate.ClientID %>").val();
                }
                printTable('.ScrollPanelCartable', '../../', title);
            });

            $('.GridRow,.GridAltRow').click(function () {
                $(this).toggleClass('selected');
            })
            $('a.no').click(function () {
                var url = $(this).attr('href');
                var otherID = $(this).attr('otherid');
                var mode = $(this).attr('mode');
                var table, title;
                if (mode == 4) {
                    table = 'Purchase';
                    title = Lang.SalesPurchase;
                } else if (mode == 7) {
                    table = 'StoreDraft';
                    title = ' Lang.SalesInvoice';
                } else if (mode == 6) {
                    table = 'Returns';
                    title = Lang.ReturnInvoice;
                } else if (mode == 5) {
                    table = 'Refuse';
                    title = Lang.RefuseInvoice;
                }
                showMenu({
                    parent: this, width: '100px',
                    cmds: [["سند", 'Voucher']
                        , [title, 'Table']
                        , [Lang.Cancel, 'Cancel']],
                    callback: function (cmd) {
                        if (cmd == 'Voucher')
                            document.location = url;
                        else if (cmd == 'Table')
                            document.location = "../../list.aspx?table=" + table + "&menu=accountinga&id=" + otherID;
                    }
                });
                return false;
            });
            dateSelect('.dateselect');
            $("#bar").prepend(breadcrumb({
                title: Lang.PaperBook,
                parent: {
                    title: Lang.Reports,
                    url: 'menu/reports.aspx'
                }
            }));
            setClassReports();
        });
    </script>
</asp:Content>

