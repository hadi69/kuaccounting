﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" CodeFile="CommoditySaleReport.aspx.cs" Inherits="reports_accounting_CommoditySaleReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register src="~/controls/ReportSetting.ascx" tagName="ReportSetting" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" Runat="Server">
    <div id="bar">
    </div>
<%--    <uc1:ReportSetting ID="mReportSetting" runat="server" />--%>
    <div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" AssociatedControlID="mFromDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="mFromDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label3" runat="server" AssociatedControlID="mToDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="mToDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="personLable" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Person</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mPersons" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
             <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label9" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.PersonnelGroup</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mPersonGroups" runat="server" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="CommodityLable" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Commodity</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mCommodities" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
             <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label4" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.ServiceType</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mServiceTypes" runat="server" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container hidden">
                <div class="form-group">
                    <asp:Label ID="Labels" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Label</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mLabels" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4">&nbsp;</div>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox runat="server" ID="mJustShowStoredraftWithVatAndTax" Text="Lang.JustShowStoredraftWithVatAndTax" CssClass="mJustShowStoredraftWithVatAndTax" />
                    </div>
                </div>
            </div>
            <div class="col-xs-4 col-sm-6 form-group-container" align="center">
                <div class="form-group">
                    <asp:RadioButton ID="Detail" runat="server" GroupName="radio" Text="Lang.CommodityDetail" Checked="True" />
                    <asp:RadioButton ID="CommoditySum" runat="server" GroupName="radio" Text="Lang.CommoditySum" />
                    <asp:RadioButton ID="factors" runat="server" GroupName="radio" Text="Lang.factors" />
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container dividedmonth">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-4">&nbsp;</div>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:CheckBox runat="server" ID="mDividedMonth" Text="Lang.DividedMonth" CssClass="mDividedMonth" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin-5-top">
            <div class="col-md-8 col-lg-6 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container form-col-without-padding">
                <div class="col-xs-12 col-sm-3">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
                </div>
                <div class="col-xs-12 col-sm-3">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcel_Click" Enabled="false" />
                </div>
                <div class="col-xs-12 col-sm-3">
                    <input type="button" id="print" class="btn btn-xs btn-dark-blue btn-block" value="" />
                </div>
            </div>
        </div>
    </div>
   <asp:Panel ID="panelDropDowns" runat="server" CssClass="well callout padding-5 hidden">
        <input type="hidden" runat="server" id="mQueryBuilder" class="mQueryBuilder" />
        <input type="hidden" runat="server" id="mQueryBuilderSql" class="mQueryBuilderSql" />
        <div class="hidden">
            <asp:DropDownList runat="server" ID="mCommodityGroupings" CssClass="mCommodityGroupings"></asp:DropDownList> 
            <asp:DropDownList runat="server" ID="mPersonGroupings" CssClass="mPersonGroupings"></asp:DropDownList>
            <asp:DropDownList runat="server" ID="mCommodityGroup" CssClass="mCommodityGroup"></asp:DropDownList> 
            <asp:DropDownList runat="server" ID="mPersonGroup" CssClass="mPersonGroup"></asp:DropDownList>          
        </div>
        <div class="row">
            <div class="col-xs-12">
                <h5 class="dashed-bottom padding-5-bottom yekan-family">
                    <asp:Label runat="server">Lang.Groups</asp:Label>
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="ltr builder">
                    <div id="builder"></div>
                </div>
            </div>
        </div>
     </asp:Panel>
    <div class="col-xs-12">
        <asp:Label runat="server" ID="msg" CssClass="label-30 alert alert-danger block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
    </div>
    <asp:Panel ID="gridPanel" runat="server" Visible="false">
        <JP:MiniHeader runat="server" Text="Lang.CommoditySaleReport" ID="mGridTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="mGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true" OnSorting="mGrid_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="NOSerial" HeaderText="Lang.No" ItemStyle-HorizontalAlign="Center" SortExpression="NOSerial" />
                        <asp:TemplateField HeaderText="Lang.FactorDate" SortExpression="FactorDate">
                            <ItemTemplate>
                                <asp:Label ID="lblGridDate" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="BuyerName" HeaderText="Lang.BuyerName" ItemStyle-CssClass="tl align-center" SortExpression="BuyerName" />
                        <asp:BoundField DataField="Label" HeaderText="Lang.Label" ItemStyle-CssClass="tl align-center" SortExpression="Label" />
                        <asp:BoundField DataField="Comments" HeaderText="Lang.Comments" ItemStyle-CssClass="tl align-center" SortExpression="Comments" />
                        <asp:BoundField DataField="Code" HeaderText="Lang.CommodityCode" ItemStyle-CssClass="tl align-center" SortExpression="Code" />
                        <asp:BoundField DataField="StoreServiceName" HeaderText="Lang.CommodityName" ItemStyle-CssClass="tl align-center" SortExpression="StoreServiceName" />
                        <asp:BoundField DataField="UnitName" HeaderText="Lang.Units" ItemStyle-CssClass="tl align-center" SortExpression="UnitName" />
                        <asp:BoundField DataField="DeliveredAmount" HeaderText="Lang.NumOrValue" ItemStyle-CssClass="tl align-center" SortExpression="DeliveredAmount" />
                        <asp:BoundField DataField="UnitCurrencyConverter" HeaderText="Lang.UnitCurrencyConverter" ItemStyle-CssClass="tl align-center" SortExpression="UnitCurrencyConverter" />
                        <asp:TemplateField HeaderText="Lang.TotalAmount" SortExpression="TotalAmount">
                            <ItemTemplate>
                                <asp:Label ID="TotalAmount" runat="server" Text='<%#Eval("TotalAmount") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalTotalAmount" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Sum" SortExpression="Total">
                            <ItemTemplate>
                                <asp:Label ID="Total" runat="server" Text='<%#Eval("Total") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalTotal" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Discount" SortExpression="TotalDiscount">
                            <ItemTemplate>
                                <asp:Label ID="TotalDiscount" runat="server" Text='<%#Eval("TotalDiscount") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalTotalDiscount" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Tax" SortExpression="Tax">
                            <ItemTemplate>
                                <asp:Label ID="Tax" runat="server" Text='<%#Eval("Tax") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalTax" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.Price" SortExpression="Price">
                            <ItemTemplate>
                                <asp:Label ID="Price" runat="server" Text='<%#Eval("Price") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalPrice" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lang.FinalPrice" SortExpression="TotalPrice">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("TotalPrice") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="totalTotalPrice" runat="server" Text="Lang.Sum" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Month" HeaderText="Lang.Month" ItemStyle-CssClass="tl align-center" SortExpression="Month" />
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
    <script>
        $(function () {
            function sumCommodity() {
                if ($("#<%=CommoditySum.ClientID %>").attr('checked')) {
                    $(".dividedmonth").show();
                } else {
                    $(".dividedmonth").hide();
                }
                $("#<%=CommoditySum.ClientID %>").click(function () {
                        $(".dividedmonth").show();
                });
                $("#<%=Detail.ClientID %>").click(function () {
                    $(".dividedmonth").hide();
                    $("#<%=mDividedMonth.ClientID %>").attr('checked', false);
                });
                $("#<%=factors.ClientID %>").click(function () {
                    $(".dividedmonth").hide();
                    $("#<%=mDividedMonth.ClientID %>").attr('checked', false);
                });
            }

            sumCommodity();
            $('#print').attr('value', Lang.Print);
            $('#print').click(function () {
                var head = Lang.CommoditySaleReport;
                <%--if ($("#<%=ddlBy.ClientID %>").val() == <%= (int)AccController.PersonBookGroupBy.ByTotal %>) {
                    head = '<%=jpMiniHeaderTotalBills.Text %>';
            }--%>
                printTable('.ScrollPanelCartable', '../../', head);
            });
            $('.GridRow,.GridAltRow').click(function () {
                $(this).toggleClass('selected');
            });
            dateSelect('.dateselect');
            $('.monetary').keyup(function () {
                $(this).val(numberWithCommas(stripCommas($(this).val())));
            });
            $("#bar").prepend(breadcrumb({
                title: Lang.CommoditySaleReport,
                parent: {
                    title: Lang.Reports,
                    url: 'menu/reports.aspx'
                }
            }));
            setClassReports();
            var sgg = {};
            $('.mCommodityGroupings option').each(function () {
                sgg[$(this).attr('value')] = $(this).text();
            });
            var pgg = {};
            $('.mPersonGroupings option').each(function () {
                pgg[$(this).attr('value')] = $(this).text();
            });
            var cog = {};
            $('.mCommodityGroup option').each(function () {
                cog[$(this).attr('value')] = $(this).text();
            });
            var peg = {};
            $('.mPersonGroup option').each(function () {
                peg[$(this).attr('value')] = $(this).text();
            });
            //if ($jQuery1_9('#builder').length > 0)
            $jQuery1_9('#builder').queryBuilder({
                filters: [
                    {
                        id: 'SGCategory',
                        label: Lang.ServiceGroupLable,
                        type: 'integer',
                        input: 'select',
                        values: sgg,
                        operators: ['equal', 'not_equal']
                    },
                    {
                        id: 'PGCategory',
                        label: Lang.PersonsGroupLable,
                        type: 'integer',
                        input: 'select',
                        values: pgg,
                        operators: ['equal', 'not_equal']
                    },
                    {
                        id: 'SCategory',
                        label: Lang.ServiceType,
                        type: 'integer',
                        input: 'select',
                        values: cog,
                        operators: ['equal', 'not_equal']
                    },
                    {
                        id: 'PCategory',
                        label: Lang.PersonnelGroup,
                        type: 'integer',
                        input: 'select',
                        values: peg,
                        operators: ['equal', 'not_equal']
                    }
                ]
            });
            // save rules before post back
            $('.doShow').click(function () {
                var rules = $jQuery1_9('#builder').queryBuilder('getRules');
                var rulesStr = JSON.stringify(rules);
                // alert(rulesStr);
                $('.mQueryBuilder').val(rulesStr);
                $('.mQueryBuilderSql').val($jQuery1_9('#builder').queryBuilder('getSQL', false, false).sql);
            });
            //reload saved rules
            $jQuery1_9('#builder').queryBuilder('reset');
            var ruleStr = $('.mQueryBuilder').val();
            if (ruleStr != '') {
                var rule = eval("(" + ruleStr + ")");
                try {
                    $jQuery1_9('#builder').queryBuilder('setRules', rule);
                } catch (e) {
                }
            }
            $jQuery1_9('#builder').on('afterCreateRuleInput.queryBuilder', function (rule) {
                makeChosen();
            });

            $(document).ready(function() {
                $("#factors-select-all").click(function() {
                    $(".data-print input").attr("checked", "checked");
                });

                $("#factors-select-none").click(function() {
                    $(".data-print input").attr("checked", "");
                });

                // Handle printing
                $("#factors-print").click(function() {
                    var checkedFactors = $(".data-print input:checked"),
                        ids = [],
                        printUrl = '',
                        baseURlLength = 100,
                        totalIds,
                        indexOfPipe = 0;
                    if (checkedFactors.length == 0) {
                        alert(Lang.PleaseCheckAtLeastOneFactor);
                        return;
                    }

                    $.each(checkedFactors, function() {
                        ids.push($(this).attr("data-id"));
                    });
                    printUrl = rootPath + Router.Generate("print-storedraft", { params: { ids: ids.join('|'), title: 'SalesInvoice' }, query: { report: 'simple', print: "true" } });

                    totalIds= ids.join('|');

                    while (indexOfPipe >= 0) {
                        indexOfPipe = totalIds.indexOf('|', 190);
                        if (indexOfPipe >= 0) {
                            window.open(rootPath + Router.Generate("print-storedraft", { params: { ids: totalIds.substr(0, indexOfPipe), title: 'SalesInvoice' }, query: { report: 'simple', print: "true" } }));
                        }

                        totalIds = totalIds.substr(indexOfPipe + 1);
                    }
                    window.open(rootPath + Router.Generate("print-storedraft", { params: { ids: totalIds, title: 'SalesInvoice' }, query: { report: 'simple', print: "true" } }));
                });
            });

        });
    </script>
</asp:Content>

