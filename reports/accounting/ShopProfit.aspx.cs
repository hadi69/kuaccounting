﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reports_accounting_ShopProfit : System.Web.UI.Page
{
    System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
    protected void Page_Load(object sender, EventArgs e)
    {
        msg.Visible = false;
        mGrid.EmptyDataText = LangProvider.Instance["EmptyData"];
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");
        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["ShopProfit"];
            Page.SetLang();
            int year = cal.GetYear(DateTime.Now);
            //mFromDate.Text = string.Format("{0}/01/01", year - 1300);
            //mToDate.Text = Helper.FormatDate(cal.ToDateTime(year + 1, 1, 1, 1, 1, 1, 1).AddDays(-1), "yy/MM/dd");
            IDataReader financeYearStartDate = DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");//0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            //mFromDate.Text = 
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);

            Init();

           // mReportSetting.ReportName = "ShopProfit";
        }
       // mReportSetting.SaveClick += new EventHandler(mReportSetting_SaveClick);
       // mReportSetting.ReportSettingSelect += new EventHandler(mReportSetting_ReportSettingSelect);
    }

    //void mReportSetting_ReportSettingSelect(object sender, EventArgs e)
    //{
    //    ReportSettingInfo info = mReportSetting.SelectedReportSettings;
    //    if (info == null)
    //        return;
    //    string[] lines = info.Settings.Split('\n');
    //    mServiceName.Text = lines[0].Trim('\r');
    //    mFromDate.Text = lines[1].Trim('\r');
    //    mToDate.Text = lines[2].Trim('\r');
    //}

    //void mReportSetting_SaveClick(object sender, EventArgs e)
    //{
    //    StringBuilder b = new StringBuilder();
    //    b.AppendLine(mServiceName.Text); //0
    //    b.AppendLine(mFromDate.Text); //1
    //    b.AppendLine(mToDate.Text); //2
    //    mReportSetting.Save(b.ToString());
    //}
    private void Init()
    {
        IDataReader persons = DbProxy.Instance.ExecuteReader("SELECT ID, [dbo].[DisplayPerson](ID, 1), ISNULL(Code, '') FROM Personnel WHERE Enabled = 1 AND Type=101");
        mPersons.Items.Clear();
        mPersons.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
        if (persons != null)
        {
            while (persons.Read())
            {
                if (!persons.IsDBNull(0) && !persons.IsDBNull(1))
                    mPersons.Items.Add(new ListItem(persons.GetString(1) + " - کد: " + persons.GetString(2),
                        persons.GetInt32(0).ToString()));
            }
            persons.Close();
        }
        mPersons.SelectedIndex = 0;

        IDataReader stores = DbProxy.Instance.ExecuteReader("SELECT ID, Title FROM Stores");
        mStores.Items.Clear();
        mStores.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
        if (stores != null)
        {
            while (stores.Read())
            {
                if (!stores.IsDBNull(0) && !stores.IsDBNull(1))
                    mStores.Items.Add(new ListItem(stores.GetString(1), stores.GetInt32(0).ToString()));
            }
            stores.Close();
        }
        mStores.SelectedIndex = 0;

        IDataReader serviceTypes = DbProxy.Instance.ExecuteReader("SELECT ID, FullName FROM ServiceTypeView");
        mServiceTypes.Items.Clear();
        mServiceTypes.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
        if (serviceTypes != null)
        {
            while (serviceTypes.Read())
            {
                if (!serviceTypes.IsDBNull(0) && !serviceTypes.IsDBNull(1))
                    mServiceTypes.Items.Add(new ListItem(serviceTypes.GetString(1), serviceTypes.GetInt32(0).ToString()));
            }
            serviceTypes.Close();
        }
        mServiceTypes.SelectedIndex = 0;

        IDataReader services = DbProxy.Instance.ExecuteReader("SELECT ServicePriceView.ID, ServiceFullName FROM ServicePriceView");
        mServiceName.Items.Clear();
        mServiceName.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
        if (services != null)
        {
            while (services.Read())
            {
                if (!services.IsDBNull(0) && !services.IsDBNull(1))
                    mServiceName.Items.Add(new ListItem(services.GetString(1), services.GetInt32(0).ToString()));
            }
            services.Close();
        }
        mServiceName.SelectedIndex = 0;

        IDataReader PersonGroups = DbProxy.Instance.ExecuteReader("SELECT ID, Title FROM PersonnelGroup");
        mPersonGroups.Items.Clear();
        mPersonGroups.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
        if (PersonGroups != null)
        {
            while (PersonGroups.Read())
                while (PersonGroups.Read())
                {
                    if (!PersonGroups.IsDBNull(0) && !PersonGroups.IsDBNull(1))
                        mPersonGroups.Items.Add(new ListItem(PersonGroups.GetString(1), PersonGroups.GetInt32(0).ToString()));
                }
            PersonGroups.Close();
        }
        mPersonGroups.SelectedIndex = 0;

        //IDataReader Grouping = DbProxy.Instance.ExecuteReader("SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping");
        //mPZGroup.Items.Clear();
        //mSZGroup.Items.Clear();
        //mPZGroup.Items.Add(new ListItem(LangProvider.Instance["AllGroup"], ""));
        //mSZGroup.Items.Add(new ListItem(LangProvider.Instance["AllGroup"], ""));
        //if (Grouping != null)
        //{
        //    while (Grouping.Read())
        //    {
        //        if (!Grouping.IsDBNull(0) && !Grouping.IsDBNull(1))
        //        {
        //            mPZGroup.Items.Add(new ListItem(Grouping.IsDBNull(1) ? "" : Grouping.GetString(1), Grouping.GetInt32(0).ToString()));
        //            mSZGroup.Items.Add(new ListItem(Grouping.IsDBNull(1) ? "" : Grouping.GetString(1), Grouping.GetInt32(0).ToString()));

        //        }
        //    }
        //    Grouping.Close();
        //}
        //mPZGroup.SelectedIndex = 0;
        //mSZGroup.SelectedIndex = 0;

        //IDataReader Grouping1 = DbProxy.Instance.ExecuteReader("SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping");
        //if (Grouping1 != null)
        //{
        //    while (Grouping1.Read())
        //        mSZGroup.Items.Add(new ListItem(Grouping1.IsDBNull(1) ? "" : Grouping1.GetString(1), Grouping1.GetInt32(0).ToString()));
        //    Grouping1.Close();
        //}

    //    IDataReader commodityGroupings =
    //DbProxy.Instance.ExecuteReader(
    //    "SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping  WHERE GroupingType = 1");
    //    IDataReader personGroupings =
    //        DbProxy.Instance.ExecuteReader(
    //            "SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping  WHERE GroupingType = 0");
    //    IDataReader commodityGroup = DbProxy.Instance.ExecuteReader("SELECT ID, FullName FROM ServiceTypeView");
    //    IDataReader personGroup = DbProxy.Instance.ExecuteReader("SELECT ID, Title FROM PersonnelGroup");
    //    mCommodityGroupings.Items.Clear();
    //    if (commodityGroupings != null)
    //    {
    //        while (commodityGroupings.Read())
    //        {
    //            if (!commodityGroupings.IsDBNull(0) && !commodityGroupings.IsDBNull(1))
    //                mCommodityGroupings.Items.Add(new ListItem(commodityGroupings.GetString(1), commodityGroupings.GetInt32(0).ToString()));
    //         }
    //        commodityGroupings.Close();
    //    }
    //    mPersonGroupings.Items.Clear();
    //    if (personGroupings != null)
    //    {
    //        while (personGroupings.Read())
    //        {
    //            if (!personGroupings.IsDBNull(0) && !personGroupings.IsDBNull(1))
    //                mPersonGroupings.Items.Add(new ListItem(personGroupings.GetString(1),
    //                personGroupings.GetInt32(0).ToString()));
    //        }
    //        personGroupings.Close();
    //    }
    //    mCommodityGroup.Items.Clear();
    //    if (commodityGroup != null)
    //    {
    //        while (commodityGroup.Read())
    //        {
    //            if (!commodityGroup.IsDBNull(0) && !commodityGroup.IsDBNull(1))
    //                mCommodityGroup.Items.Add(new ListItem(commodityGroup.GetString(1),
    //                    commodityGroup.GetInt32(0).ToString()));
    //        }
    //        commodityGroup.Close();
    //    }
    //    mPersonGroup.Items.Clear();
    //    if (personGroup != null)
    //    {
    //        while (personGroup.Read())
    //        {
    //            if (!personGroup.IsDBNull(0) && !personGroup.IsDBNull(1))
    //                mPersonGroup.Items.Add(new ListItem(personGroup.GetString(1), personGroup.GetInt32(0).ToString()));
    //        }
    //        personGroup.Close();
    //    }
    }

    private decimal sumProfit = 0, factorTotaldiscount = 0;
    private int lastId = -1; int type = 0;
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.Common.DbDataRecord record = e.Row.DataItem as System.Data.Common.DbDataRecord;
            DateTime dt = Helper.GetDateTime(record["FactorDate"], Helper.NullDate);
            if (dt != Helper.NullDate)
            {
                e.Row.Cells[3].Text = Helper.FormatDate(Helper.GetDateTime(dt, Helper.NullDate), "<a title='yyyy/MM/dd-HH:mm:ss'>yyyy/MM/dd</a>");
            }
            int id = Helper.GetInt(record["ID"], 0);
            if (lastId == -1)
            {
                lastId = id;
            }
            if ((id != lastId) && (type == 1))
            {
                if (factorTotaldiscount > 0)
                {
                    e.Row.Cells[2].Text = record["ServiceName"] +
                                          string.Format("<div class='subSum' td='{0}' l='{1}'></div>",
                                              factorTotaldiscount, -1 * factorTotaldiscount);
                    sumProfit += -1 * factorTotaldiscount;
                    factorTotaldiscount = 0;
                }
                lastId = id;
            }
            factorTotaldiscount = SiteSettings.ToMonetary(Helper.GetDecimal(record["FactorTotalDiscount"], 0), dt);
            type = Helper.GetInt(record["Type"], 0);
            if (type == 1)
                e.Row.Cells[1].Text = LangProvider.Instance["StoreDraft"];
            else if (type == 2)
                e.Row.Cells[1].Text = LangProvider.Instance["Return"];

            decimal profit = SiteSettings.ToMonetary(Helper.GetDecimal(record["Profit"], 0), dt);
            sumProfit += profit;

            if (type == 1)
                e.Row.Cells[4].Text = string.Format("<a href='../../list.aspx?table=StoreDraft&view=shopstoredrafts&id={0}'>{1}</a>", record["ID"], record["NOSerial"]);
            else
                e.Row.Cells[4].Text = string.Format("<a href='../../list.aspx?table=Returns&view=shopreturns&id={0}'>{1}</a>", record["ID"], record["NOSerial"]);
            e.Row.Cells[7].Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["BasePrice"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.Cells[8].Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["Price"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.Cells[9].Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["Discount"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.Cells[10].Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["Profit"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            //((Label)e.Row.FindControl("owe")).Text = _owe.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            //((Label)e.Row.FindControl("due")).Text = _due.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            //((Label)e.Row.FindControl("remaining")).Text = _remain.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            if (factorTotaldiscount > 0)
            {
                e.Row.Cells[2].Text = string.Format("<div class='subSum' td='{0}' l='{1}'></div>", factorTotaldiscount, -1 * factorTotaldiscount);
                sumProfit += -1 * factorTotaldiscount;
                factorTotaldiscount = 0;
            }
            Label lblTotal = (Label)e.Row.FindControl("SumProfit");
            lblTotal.Text = sumProfit.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }
    }
    protected void mGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        BindGrid();
    }
    protected void BindGrid()
    {
        string cogIDs, pegIDs, coIDs, peIDs, gWhere;
        GetGroupWhere(out cogIDs, out pegIDs, out coIDs, out peIDs, out gWhere);
        DateTime start = Helper.ParseDate(mFromDate.Text, Null.NullDate);
        DateTime end = Helper.ParseDate(mToDate.Text, Null.NullDate);
        if (Null.IsNull(start))
        {
            msg.Text = LangProvider.Instance["InvalidStartDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(end))
        {
            msg.Text = LangProvider.Instance["InvalidEndDate"];
            msg.Visible = true;
        }
        if (Null.IsNull(start) || Null.IsNull(end))
            return;
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);

        string where = string.Format(" WHERE StoreDraft.FactorDate>={0} AND StoreDraft.FactorDate<={1} ", Helper.QoutedDateTime(start), Helper.QoutedDateTime(end));

        int personID = Helper.GetInt(mPersons.SelectedValue, Null.NullInteger);
        if (personID != Null.NullInteger)
            where += " AND StoreDraft.BuyerID = " + personID;

        int storeId = Helper.GetInt(mStores.SelectedValue, Null.NullInteger);
        if (storeId != Null.NullInteger)
            where += " AND StoreServiceDef.StoreID = " + storeId;
        //int GroupingID = Helper.GetInt(mPZGroup.SelectedValue, Null.NullInteger);
        //if (GroupingID != Null.NullInteger)
        //    where += " AND StoreDraft.BuyerID IN ( SELECT OtherID FROM GroupingMember INNER JOIN [Grouping] On [Grouping].ID=GroupingMember.GroupingID  WHERE GroupingID = " + GroupingID + " AND [Grouping].GroupingType = 0)";

        int serviceTypeID = Helper.GetInt(mServiceTypes.SelectedValue, Null.NullInteger);
        if (serviceTypeID != Null.NullInteger)
            where += " AND Service.ServiceTypeID IN (SELECT ID FROM [GetAllServiceTypeChilds]( " + serviceTypeID + "))";

        int personGroupID = Helper.GetInt(mPersonGroups.SelectedValue, Null.NullInteger);
        if (personGroupID != Null.NullInteger)
            where += " AND Personnel.PersonnelGroupID = " + personGroupID;

        int servicePriceID = Helper.GetInt(mServiceName.SelectedValue, Null.NullInteger);
        if (servicePriceID != Null.NullInteger)
            where += " AND ServicePriceView.ID =" + servicePriceID;

        if ((!string.IsNullOrEmpty(cogIDs) || !string.IsNullOrEmpty(pegIDs) || !string.IsNullOrEmpty(peIDs) ||
     !string.IsNullOrEmpty(coIDs)) && !string.IsNullOrEmpty(gWhere))
        {
            // ------------------------
            // ids: 1,2,5
            // where: '&1%' AND ('&2%' OR '&5%')
            // it could be better, but i dont know regex well :(
            // ------------------------
            where += "\r\nAND (\r\n";
            if (!string.IsNullOrEmpty(cogIDs))
            {
                //  groupings: '*1%', '!*1%'
                string[] cGroupingIDs = cogIDs.Split(',');
                for (int i = 0; i < cGroupingIDs.Length; i++)
                {
                    StringBuilder allIds = new StringBuilder();
                    int j = 0;
                    IDataReader childIds =
                        DbProxy.Instance.ExecuteReader("SELECT ID FROM [GetAllGroupingChilds](" + cGroupingIDs[i] + ")");
                    while (childIds.Read())
                    {
                        if (j != 0)
                        {
                            allIds.Append(",");
                        }
                        allIds.Append(childIds.GetValue(0));
                        j++;
                    }
                    gWhere = gWhere.Replace("'*" + cGroupingIDs[i] + "%'",
                        @" ServicePriceView.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds +
                        ")) \r\n");
                    gWhere = gWhere.Replace("'!*" + cGroupingIDs[i] + "%'",
                        @" ServicePriceView.ID NOT IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds +
                        ")) \r\n");
                }
            }
            if (!string.IsNullOrEmpty(pegIDs))
            {
                // personnel groups: '&1%', '!&1%'
                string[] pGroupingIDs = pegIDs.Split(',');
                for (int i = 0; i < pGroupingIDs.Length; i++)
                {
                    StringBuilder allIds = new StringBuilder();
                    int j = 0;
                    IDataReader childIds =
                        DbProxy.Instance.ExecuteReader("SELECT ID FROM [GetAllGroupingChilds](" + pGroupingIDs[i] + ")");
                    while (childIds.Read())
                    {
                        if (j != 0)
                        {
                            allIds.Append(",");
                        }
                        allIds.Append(childIds.GetValue(0));
                        j++;
                    }
                    gWhere = gWhere.Replace("'$" + pGroupingIDs[i] + "%'",
                        "StoreDraft.BuyerID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + ")) \r\n");
                    gWhere = gWhere.Replace("'!$" + pGroupingIDs[i] + "%'",
                        " NOT StoreDraft.BuyerID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds +
                        ")) \r\n");
                }
            }
            if (!string.IsNullOrEmpty(peIDs))
            {
                // personnel groups: '&1%', '!&1%'
                string[] pgids = peIDs.Split(',');
                for (int i = 0; i < pgids.Length; i++)
                {
                    gWhere = gWhere.Replace("'&" + pgids[i] + "%'",
                        "Personnel.PersonnelGroupID IN (" + pgids[i] + ") \r\n");
                    gWhere = gWhere.Replace("'!&" + pgids[i] + "%'",
                        " NOT Personnel.PersonnelGroupID IN (" + pgids[i] + ") \r\n");
                }
            }

            if (!string.IsNullOrEmpty(coIDs))
            {
                // ServiceType: '^1%', '!^1%'
                string[] cgids = coIDs.Split(',');
                for (int i = 0; i < cgids.Length; i++)
                {
                    StringBuilder allIds = new StringBuilder();
                    int j = 0;
                    IDataReader childIds =
                        DbProxy.Instance.ExecuteReader("SELECT ID FROM [GetAllServiceTypeChilds](" + cgids[i] + ")");
                    while (childIds.Read())
                    {
                        if (j != 0)
                        {
                            allIds.Append(",");
                        }
                        allIds.Append(childIds.GetValue(0));
                        j++;
                    }
                    gWhere = gWhere.Replace("'^" + cgids[i] + "%'",
                        @" Service.ServiceTypeID IN (" + allIds + ") \r\n");
                    gWhere = gWhere.Replace("'!^" + cgids[i] + "%'",
                        @" NOT Service.ServiceTypeID IN (" + allIds + ") \r\n");
                }

            }

            where += gWhere;
            where += "\r\n)\r\n";
        }


        string query = string.Format(@"SELECT 1 AS Type
, ServiceFullName AS ServiceName, 
StoreDraftItems.Price, StoreDraftItems.DeliveredAmount, StoreDraftItems.UnitCurrencyConverter
, CASE WHEN ISNULL(StoreDraftItems.Discount, 0) > = 0 THEN ISNULL(StoreDraftItems.Discount, 0) 
   ELSE (((-1 * ISNULL(StoreDraftItems.Discount, 0)) * StoreDraftItems.Price * StoreDraftItems.DeliveredAmount * ISNULL(StoreDraftItems.UnitCurrencyConverter, 1)) / 100) END Discount
, StoreDraftItems.BasePrice
, StoreDraft.FactorDate, StoreDraft.NoSerial, StoreDraft.ID
, (StoreDraftItems.Price * StoreDraftItems.DeliveredAmount - ISNULL(StoreDraftItems.BasePrice, 0) * StoreDraftItems.DeliveredAmount) * ISNULL(StoreDraftItems.UnitCurrencyConverter, 1)
- (CASE WHEN ISNULL(StoreDraftItems.Discount, 0) > = 0 THEN ISNULL(StoreDraftItems.Discount, 0) 
   ELSE (((-1 * ISNULL(StoreDraftItems.Discount, 0)) * StoreDraftItems.Price * StoreDraftItems.DeliveredAmount * ISNULL(StoreDraftItems.UnitCurrencyConverter, 1)) / 100) END) Profit
,CASE WHEN ISNULL(StoreDraft.FactorTotalDiscount, 0) > = 0 THEN ISNULL(StoreDraft.FactorTotalDiscount, 0) 
   ELSE (((-1 * ISNULL(StoreDraft.FactorTotalDiscount, 0)) * StoreDraft.TotalPrice) / 100) END FactorTotalDiscount 
 FROM StoreDraftItems
INNER JOIN StoreServices ON StoreServices.ID = StoreDraftItems.StoreServiceID
INNER JOIN StoreDraft ON StoreDraft.ID = StoreDraftItems.StoreDraftID
INNER JOIN Personnel ON StoreDraft.BuyerID = Personnel.ID
INNER JOIN StoreServiceDef ON StoreServiceDef.ID = StoreServices.StoreServiceDefID
INNER JOIN ServicePriceView ON ServicePriceView.ID = StoreServiceDef.ServicePriceID 
INNER JOIN Service ON Service.ID = ServicePriceView.ServiceID
LEFT OUTER JOIN Supplier ON ServicePriceView.Model=Supplier.ID
{0}

UNION

SELECT 2 AS Type
, ServiceFullName AS ServiceName
, ReturnItem.Price, ReturnItem.Num, ISNULL(ReturnItem.UnitCurrencyConverter, 1) UnitCurrencyConverter
, CASE WHEN ISNULL(ReturnItem.Discount, 0) > = 0 THEN ISNULL(ReturnItem.Discount, 0) 
   ELSE (((-1 * ISNULL(ReturnItem.Discount, 0)) * ReturnItem.Price * ReturnItem.Num * ISNULL(ReturnItem.UnitCurrencyConverter, 1)) / 100) END Discount
, ISNULL(StoreDraftItems.BasePrice, ReturnItem.BuyPrice) BasePrice
, [Returns].ReturnDate, [Returns].NoSerial, [Returns].ID
, - ReturnItem.Num * ISNULL(ReturnItem.UnitCurrencyConverter, 1) * ISNULL(
 ((StoreDraftItems.Price * StoreDraftItems.DeliveredAmount - ISNULL(StoreDraftItems.BasePrice, 0) * StoreDraftItems.DeliveredAmount)
 -(CASE WHEN ISNULL(ReturnItem.Discount, 0) > = 0 THEN ISNULL(ReturnItem.Discount, 0) 
 ELSE (((-1 * ISNULL(ReturnItem.Discount, 0)) * ReturnItem.Price * ReturnItem.Num * ISNULL(ReturnItem.UnitCurrencyConverter, 1)) / 100) END)) / StoreDraftItems.DeliveredAmount
, (ReturnItem.Price - ReturnItem.BuyPrice)) Profit, 0 FactorTotalDiscount
 FROM ReturnItem
INNER JOIN [Returns] ON [Returns].ID = ReturnItem.ReturnID
INNER JOIN Personnel ON [Returns].BuyerID = Personnel.ID
LEFT JOIN StoreDraftItems ON StoreDraftItems.StoreServiceID=ReturnItem.StoreServiceID AND StoreDraftItems.StoreDraftID=[Returns].StoreDraftID
INNER JOIN StoreServices ON StoreServices.ID = ReturnItem.StoreServiceID
INNER JOIN StoreServiceDef ON StoreServiceDef.ID = StoreServices.StoreServiceDefID
INNER JOIN ServicePriceView ON ServicePriceView.ID = StoreServiceDef.ServicePriceID 
INNER JOIN Service ON Service.ID = ServicePriceView.ServiceID
LEFT OUTER JOIN Supplier ON ServicePriceView.Model=Supplier.ID
{1}
", where, where.Replace("StoreDraft", "[Returns]").Replace("FactorDate", "ReturnDate"));

        if (mBaseOnFactor.Checked)
        {
          query=string.Format(@"SELECT 1 AS Type
, StoreDraft.FactorDate  AS FactorDate
, StoreDraft.NOSerial, StoreDraft.ID
, SUM(ISNULL(StoreDraftItems.Price, 0) * StoreDraftItems.DeliveredAmount * StoreDraftItems.UnitCurrencyConverter) AS Price
, SUM(ISNULL(StoreDraftItems.BasePrice, 0) * StoreDraftItems.DeliveredAmount * StoreDraftItems.UnitCurrencyConverter) AS BasePrice

, (SUM((ISNULL(StoreDraftItems.Price, 0) * StoreDraftItems.DeliveredAmount - ISNULL(StoreDraftItems.BasePrice, 0) * StoreDraftItems.DeliveredAmount)
 * ISNULL(StoreDraftItems.UnitCurrencyConverter, 1)
- (CASE WHEN ISNULL(StoreDraftItems.Discount, 0) > = 0 THEN ISNULL(StoreDraftItems.Discount, 0) 
   ELSE (((-1 * ISNULL(StoreDraftItems.Discount, 0)) * StoreDraftItems.Price * StoreDraftItems.DeliveredAmount
    * ISNULL(StoreDraftItems.UnitCurrencyConverter, 1)) / 100) END)))
- (CASE WHEN ISNULL(StoreDraft.FactorTotalDiscount, 0) >= 0 THEN ISNULL(StoreDraft.FactorTotalDiscount, 0) 
   ELSE (((-1 * ISNULL(StoreDraft.FactorTotalDiscount, 0)) * StoreDraft.TotalPrice) / 100) END) Profit

, (SUM(CASE WHEN ISNULL(StoreDraftItems.Discount, 0) > = 0 THEN ISNULL(StoreDraftItems.Discount, 0) 
   ELSE (((-1 * ISNULL(ISNULL(StoreDraftItems.Discount, 0), 0)) * StoreDraftItems.Price * StoreDraftItems.DeliveredAmount * ISNULL(StoreDraftItems.UnitCurrencyConverter, 1)) / 100) END))
  + (CASE WHEN StoreDraft.FactorTotalDiscount >= 0 THEN StoreDraft.FactorTotalDiscount 
   ELSE (((-1 * StoreDraft.FactorTotalDiscount) * StoreDraft.TotalPrice) / 100) END) Discount
, 0 FactorTotalDiscount 
 FROM StoreDraftItems
INNER JOIN StoreServices ON StoreServices.ID = StoreDraftItems.StoreServiceID
INNER JOIN StoreDraft ON StoreDraft.ID = StoreDraftItems.StoreDraftID
INNER JOIN Personnel ON StoreDraft.BuyerID = Personnel.ID
INNER JOIN StoreServiceDef ON StoreServiceDef.ID = StoreServices.StoreServiceDefID
INNER JOIN ServicePriceView ON ServicePriceView.ID = StoreServiceDef.ServicePriceID 
INNER JOIN Service ON Service.ID = ServicePriceView.ServiceID
LEFT OUTER JOIN Supplier ON ServicePriceView.Model=Supplier.ID
{0}

GROUP BY StoreDraft.ID, StoreDraft.NOSerial, StoreDraft.FactorDate, StoreDraft.TotalPrice, StoreDraft.FactorTotalDiscount

UNION

SELECT 2 AS Type
, [Returns].ReturnDate AS FactorDate
, [Returns].NOSerial, [Returns].ID
, SUM(ISNULL(ReturnItem.Price, 0) * ReturnItem.Num * ReturnItem.UnitCurrencyConverter) AS Price
, SUM(ISNULL(StoreDraftItems.BasePrice, ReturnItem.BuyPrice) * ReturnItem.Num * ReturnItem.UnitCurrencyConverter) AS BasePrice
, SUM(- ReturnItem.Num * ISNULL(ReturnItem.UnitCurrencyConverter, 1) * ISNULL(
 ((StoreDraftItems.Price * StoreDraftItems.DeliveredAmount - ISNULL(StoreDraftItems.BasePrice, 0) * StoreDraftItems.DeliveredAmount)
 -(CASE WHEN ISNULL(ReturnItem.Discount, 0) > = 0 THEN ISNULL(ReturnItem.Discount, 0) 
 ELSE (((-1 * ISNULL(ReturnItem.Discount, 0)) * ReturnItem.Price * ReturnItem.Num * ISNULL(ReturnItem.UnitCurrencyConverter, 1)) / 100) END)) / StoreDraftItems.DeliveredAmount
, (ReturnItem.Price - ReturnItem.BuyPrice))) Profit
, SUM(CASE WHEN ISNULL(ISNULL(ReturnItem.Discount, 0), 0) > = 0 THEN ISNULL(ReturnItem.Discount, 0) 
   ELSE (((-1 * ISNULL(ReturnItem.Discount, 0)) * ReturnItem.Price * ReturnItem.Num * ISNULL(ReturnItem.UnitCurrencyConverter, 1)) / 100) END) Discount
, 0 FactorTotalDiscount 
FROM ReturnItem
INNER JOIN [Returns] ON [Returns].ID = ReturnItem.ReturnID
INNER JOIN Personnel ON [Returns].BuyerID = Personnel.ID
LEFT JOIN StoreDraftItems ON StoreDraftItems.StoreServiceID=ReturnItem.StoreServiceID AND StoreDraftItems.StoreDraftID=[Returns].StoreDraftID
INNER JOIN StoreServices ON StoreServices.ID = ReturnItem.StoreServiceID
INNER JOIN StoreServiceDef ON StoreServiceDef.ID = StoreServices.StoreServiceDefID
INNER JOIN ServicePriceView ON ServicePriceView.ID = StoreServiceDef.ServicePriceID 
INNER JOIN Service ON Service.ID = ServicePriceView.ServiceID
LEFT OUTER JOIN Supplier ON ServicePriceView.Model=Supplier.ID
{1}
GROUP BY [Returns].ID, [Returns].NOSerial, [Returns].ReturnDate
", where, where.Replace("StoreDraft", "[Returns]").Replace("FactorDate", "ReturnDate"));
  }
        if (!string.IsNullOrEmpty(SortCol))
            query += " ORDER BY " + SortCol + " " + SortOrder;
        else
            query += " ORDER BY FactorDate";
        IDataReader items = DbProxy.Instance.ExecuteReader(query);

        try
        {
            mGrid.DataSource = items;
            mGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Close();
                items.Dispose();
            }
        }
    }
    protected string SortCol
    {
        get
        {
            return Helper.GetString(ViewState["SortCol"], null);
        }
        set
        {
            ViewState["SortCol"] = value;
        }
    }
    protected string SortOrder
    {
        get
        {
            return Helper.GetString(ViewState["SortOrder"], "asc");
        }
        set
        {
            ViewState["SortOrder"] = value;
        }
    }
    protected void doShow_Click(object sender, EventArgs e)
    {
        doExcel.Enabled = true;
        if (mBaseOnFactor.Checked)
        {
            mGrid.Columns[2].Visible = mGrid.Columns[5].Visible = mGrid.Columns[6].Visible = false;
        }
        else
        {
            mGrid.Columns[2].Visible = mGrid.Columns[5].Visible = mGrid.Columns[6].Visible = true;
        }
        BindGrid();

        voucherDetailPanel.Visible = true;
        //mSumDue.Text = sumDue.ToString("N0");
        //mSumOwe.Text = sumOwe.ToString("N0");
        //mSumUnit.Text = sumUnit.ToString("N0");
    }

    private void GetGroupWhere(out string cgIDs, out string pgIDs, out string cIDs, out string pIDs, out string where)
    {
        cIDs = "";
        pIDs = "";
        cgIDs = "";
        pgIDs = "";
        where = "";

        // grouping members : '&1%', '!&1%'
        // category = 1375 AND ( category != 1076 OR category = 1075 ) 
        // --> '&1375%' AND ('!&1076%' OR '&1075%')
        where = mQueryBuilderSql.Value.Trim();
        where += " "; // we need this space at end to find IDs
        while (where.Contains("SGCategory = "))
        {
            int index = where.IndexOf("SGCategory = ") + "SGCategory = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            cgIDs += id + ",";
            where = where.Replace("SGCategory = " + id + " ", "'*" + id + "%' ");
        }
        while (where.Contains("SGCategory != "))
        {
            int index = where.IndexOf("SGCategory != ") + "SGCategory != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            cgIDs += id + ",";
            where = where.Replace("SGCategory != " + id + " ", "'!*" + id + "%' ");
        }
        if (cgIDs.EndsWith(","))
            cgIDs = cgIDs.Substring(0, cgIDs.Length - ",".Length);
        while (where.Contains("PGCategory = "))
        {
            int index = where.IndexOf("PGCategory = ") + "PGCategory = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pgIDs += id + ",";
            where = where.Replace("PGCategory = " + id + " ", "'$" + id + "%' ");
        }
        while (where.Contains("PGCategory != "))
        {
            int index = where.IndexOf("PGCategory != ") + "PGCategory != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pgIDs += id + ",";
            where = where.Replace("PGCategory != " + id + " ", "'!$" + id + "%' ");
        }
        if (pgIDs.EndsWith(","))
            pgIDs = pgIDs.Substring(0, pgIDs.Length - ",".Length);
        while (where.Contains("SCategory = "))
        {
            int index = where.IndexOf("SCategory = ") + "SCategory = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            cIDs += id + ",";
            where = where.Replace("SCategory = " + id + " ", "'^" + id + "%' ");
        }
        while (where.Contains("SCategory != "))
        {
            int index = where.IndexOf("SCategory != ") + "SCategory != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            cIDs += id + ",";
            where = where.Replace("SCategory != " + id + " ", "'!^" + id + "%' ");
        }
        if (cIDs.EndsWith(","))
            cIDs = cIDs.Substring(0, cIDs.Length - ",".Length);

        while (where.Contains("PCategory = "))
        {
            int index = where.IndexOf("PCategory = ") + "PCategory = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pIDs += id + ",";
            where = where.Replace("PCategory = " + id + " ", "'&" + id + "%' ");
        }
        while (where.Contains("PCategory != "))
        {
            int index = where.IndexOf("PCategory != ") + "PCategory != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pIDs += id + ",";
            where = where.Replace("PCategory != " + id + " ", "'!&" + id + "%' ");
        }
        if (pIDs.EndsWith(","))
            pIDs = pIDs.Substring(0, pIDs.Length - ",".Length);
    }
    protected void doExcel_Click(object sender, EventArgs e)
    {
        Exporter.ExportExcel(Response, mGridTitle.Text, mGrid);
    }
}