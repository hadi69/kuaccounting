﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Vouchers.aspx.cs" Inherits="reports_accounting_Vouchers" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cnt" Runat="Server">
 <div class="Title" style="text-align: center">
    <%= "DocumentsWihoutPrice".Translate() %>
 </div>
    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="CommandRow">
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td class="Name" style="text-align: left">
                <%= "FromDate".Translate() %>:
            </td>
            <td>
                <asp:TextBox ID="mFromDate" runat="server" CssClass="T2" Width="65"></asp:TextBox>
                <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="mFromDate" Mask="9999/99/99"
                    MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                    MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
            </td>
            <td class="Name" style="text-align: left">
                <%= "ToDate".Translate() %>:
            </td>
            <td>
                <asp:TextBox ID="mToDate" runat="server" CssClass="T2" Width="65"></asp:TextBox>
                <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="mToDate" Mask="9999/99/99"
                    MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                    MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="Name" style="text-align: left">
                <%= "Ward".Translate() %>:
            </td>
            <td>
                <asp:DropDownList ID="mWards" runat="server" CssClass="Combo" Width="150px" /></td>
            <td class="Name" style="text-align: left">
                <%= "FromDocNo".Translate() %>:
            </td>
            <td>
                <asp:TextBox ID="mFromVoucherID" runat="server" CssClass="T" Width="65px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="fte1" runat="server" TargetControlID="mFromVoucherID"
                    FilterType="Numbers" />
            </td>
            <td class="Name" style="text-align: left">
                <%= "ToDocNo".Translate() %>:
            </td>
            <td>
                <asp:TextBox ID="mToVoucherID" runat="server" CssClass="T" Width="65px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="fte2" runat="server" TargetControlID="mToVoucherID"
                    FilterType="Numbers" />
            </td>
            <td>
                <asp:Button ID="doShow" runat="server" Text='Lang.DisplayBy' CssClass="Button" OnClick="doShow_Click" />
                <input type="button" id="print" class='B' value='<%= "Print".Translate() %>' />
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td style="text-align: left">
                <asp:GridView ID="mGrid" AutoGenerateColumns="False" GridWidth="95%"
                    Width="100%" CssClass="ScrollPanelCartable" Scrolling="Vertical" runat="server"
                    BorderStyle="None" OnRowDataBound="mGrid_RowDataBound" GridLines="Both" EmptyDataText="Lang.DocumentIsEmpty"
                    AlternatingRowStyle-CssClass="GridAltRow" RowStyle-CssClass="GridRow">
                    <Columns>
                        <asp:BoundField DataField="WardID" HeaderText="Lang.Ward" />
                        <asp:BoundField DataField="No" HeaderText="Lang.DocumentNumber" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="DoComments" HeaderText="Lang.DocumentDescription" />
                        <asp:BoundField DataField="Date" HeaderText="Lang.DateDocument" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="FixerID" HeaderText="Lang.DAStatusDetermined" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="Reciever" HeaderText="Lang.ReceiverName" />
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <script>
        $(function () {
            $('#print').click(function () {
                printTable('.ScrollPanelCartable', '../../', Lang.SDocuments);
            });
        });
    </script>
</asp:Content>

