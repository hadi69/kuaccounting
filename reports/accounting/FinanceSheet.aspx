﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="FinanceSheet.aspx.cs" Inherits="reports_accounting_FinanceSheet" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>

    <div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromDate" runat="server" CssClass="form-control input-sm dateselect"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="mFromDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label3" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToDate" runat="server" CssClass="form-control input-sm dateselect"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="mToDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label5" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.TopicLevel</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mLevel" runat="server" CssClass="chosen-select chosen-rtl" OnSelectedIndexChanged="mLevel_SelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem Value="0" Text="Lang.Level1"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Lang.Level2"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Lang.Level3"></asp:ListItem>
                            <asp:ListItem Value="3" Text="Lang.Level4"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Lang.Level5"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
<%--            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label26" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Person</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mPersons" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label4" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Project</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mWards" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label1" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.EquipmentShort</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mEquipment" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>--%>
        </div>
        <div class="row">
        </div>
        <br/>
        <div class="row margin-5-top">
            <div class="col-md-4 col-lg-5 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 form-group-container form-col-without-padding">
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow reportbutton" OnClick="doShow_Click" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block reportbutton" OnClick="doExcel_Click" Enabled="false" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <input type="button" id="print" class="btn btn-xs btn-dark-blue btn-block reportbutton" value="" />
                </div>
            </div>
             <div class="col-md-4 col-lg-5 visible-md visible-lg">&nbsp;</div>
        </div>
    </div>
    <div class="col-xs-12">
        <asp:Label runat="server" ID="msg" CssClass="label-30 alert alert-danger block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
    </div>
    <div class="row">
        <div class="col-xs-12">
            <asp:Panel ID="voucherDetailPanel" runat="server" Width="100%">
                <JP:MiniHeader ID="mTitle" runat="server" />
                <div class="">
                    <ul class="nav nav-tabs">
<%--                        <li ><a data-toggle="tab" href="#owedue">
                            <asp:Label runat="server">Lang.FinanceOweDue</asp:Label></a></li>--%>
                        <li class="active"><a data-toggle="tab" href="#profit">
                            <asp:Label ID="Label10" runat="server">Lang.FinanceProfit</asp:Label></a></li>
<%--                        <li><a data-toggle="tab" href="#balance">
                            <asp:Label ID="Label11" runat="server">Lang.FinanceBalance</asp:Label></a></li>
                        <li><a data-toggle="tab" href="#capital">
                            <asp:Label ID="Label12" runat="server">Lang.FinanceCapital</asp:Label></a></li>--%>
                    </ul>

                    <div class="tab-content">
<%--                        <div id="owedue" class="tab-pane fade in active">
                            <table class="full-width">
                                <tr>
                                    <td valign="top">
                                        <asp:TreeView ID="mOweDueTree" runat="server" CssClass="Tree" ShowLines="True" ShowCheckBoxes="All">
                                            <SelectedNodeStyle CssClass="TreeSelected" />
                                        </asp:TreeView>
                                    </td>
                                    <td valign="top">
                                        <asp:PlaceHolder runat="server" ID="mOweDueHolder"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </div>--%>
                        <div id="profit" class="tab-pane fade in active">
                            <table class="full-width">
                                <tr>
                                    <td valign="top">
                                        <asp:TreeView ID="mProfitTree" runat="server" CssClass="Tree" ShowLines="True" ShowCheckBoxes="All">
                                            <SelectedNodeStyle CssClass="TreeSelected" />
                                        </asp:TreeView>
                                    </td>
                                    <td valign="top">
                                        <asp:PlaceHolder runat="server" ID="mProfitHolder"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </div>
<%--                        <div id="balance" class="tab-pane fade">
                            <div>
                               <h5><asp:Label runat="server" Text="Lang.AssetOwesCapital" /></h5>
                            </div>
                            <table class="full-width">
                                <tr>
                                    <td valign="top">
                                        <asp:TreeView ID="mBalanceTree" runat="server" CssClass="Tree" ShowLines="True" ShowCheckBoxes="All">
                                            <SelectedNodeStyle CssClass="TreeSelected" />
                                        </asp:TreeView>
                                    </td>
                                    <td valign="top">
                                        <asp:PlaceHolder runat="server" ID="mBalanceHolder"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </div>--%>
<%--                        <div id="capital" class="tab-pane fade">
                            <table class="full-width">
                                <tr>
                                    <td valign="top">
                                        <asp:TreeView ID="mCapitalTree" runat="server" CssClass="Tree" ShowLines="True" ShowCheckBoxes="All">
                                            <SelectedNodeStyle CssClass="TreeSelected" />
                                        </asp:TreeView>
                                    </td>
                                    <td valign="top">
                                        <asp:GridView ID="mCapitalGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                                            Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mCapitalGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                                            AllowSorting="true" OnSorting="mCapitalGrid_Sorting">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Lang.ColRowNr">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="TreePathCodes" HeaderText="Lang.TopicCode" ItemStyle-CssClass="tl" ItemStyle-HorizontalAlign="Center" SortExpression="TopicCode" />
                                                <asp:BoundField DataField="TreePathName" HeaderText="Lang.Topic" ItemStyle-CssClass="tr" ItemStyle-HorizontalAlign="Center" SortExpression="TreePathName" />
                                                <asp:TemplateField HeaderText="Lang.Remained" ItemStyle-HorizontalAlign="Center" SortExpression="Remained">
                                                    <ItemTemplate>
                                                        <asp:Label ID="remaining" runat="server" Text='<%#Eval("Remained") %>' />
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="remainingTotal" runat="server" Text="Lang.Sum" />
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Lang.Diagnosis" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="diagnosis" runat="server" />
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="finalDiagnosis" runat="server"></asp:Label>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="GridHeader" />
                                            <RowStyle CssClass="GridRow" />
                                            <SelectedRowStyle CssClass="GridRowSelected" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </div>--%>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
    <script>
        $(function () {
            $('#print').attr('value', Lang.Print);
            $('#print').click(function () {
                var title = Lang.TotalProfitReport;
                if ($('#<%=mLevel.ClientID %>').val() != '') {
                    title += '/' + Lang.Level + ': ' + $('#<%=mLevel.ClientID %> option:selected').text();
                }
<%--                if ($('#<%=mPersons.ClientID %>').val() != '') {
                    title += '/' + Lang.Person + ': ' + $('#<%=mPersons.ClientID %> option:selected').text();
                }
                if ($('#<%=mWards.ClientID %>').val() != '') {
                    title += '/' + Lang.Project + ': ' + $('#<%=mWards.ClientID %> option:selected').text();
                }
                if ($('#<%=mEquipment.ClientID %>').val() != '') {
                    title += '/' + Lang.Equipment + ': ' + $('#<%=mEquipment.ClientID %> option:selected').text();
                }--%>
                printTable('.ScrollPanelCartable', '../../', $('.Title').html(), title);
            });
            $('.GridRow,.GridAltRow').click(function () {
                $(this).toggleClass('selected');
            })
            dateSelect('.dateselect');
            $jQuery1_9('.ScrollPanelCartable').treegrid({ initialState: 'collapsed' });
            $("#bar").prepend(breadcrumb({
                title: Lang.TotalProfitReport,
                parent: {
                    title: Lang.Reports,
                    url: 'menu/Reports.aspx'
                }
            }));
            setClassReports();
        });
    </script>
</asp:Content>
