﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="persons.aspx.cs" Inherits="reports_app_persons" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" Runat="Server">
    <div id="bar">
    </div>
    <div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" AssociatedControlID="mFromDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="mFromDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label3" runat="server" AssociatedControlID="mToDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="mToDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="personLable" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Buyer</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:TextBox ID="mPersons" runat="server" CssClass="" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-3">&nbsp;</div>
                    <asp:RadioButton ID="mTypeSum" runat="server" GroupName="baseradio" Text="Lang.Aggregate" />
                    <asp:RadioButton ID="mTypeDetail" runat="server" GroupName="baseradio" Text="Lang.Items"/>
                    <asp:RadioButton ID="mTypeBoth" runat="server" GroupName="baseradio" Text="Lang.Both" Checked="true"/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-3">&nbsp;</div>
                    <asp:CheckBox ID="mPayStatusNotPaid" runat="server" Text="Lang.BasketPayStatusNotPaid" Checked="false" />
                    <asp:CheckBox ID="mPayStatusInPayment" runat="server" Text="Lang.BasketPayStatusInPayment" Checked="false" />
                    <asp:CheckBox ID="mPayStatusPaid" runat="server" Text="Lang.BasketPayStatusPaid" Checked="true" />
                </div>
            </div>
        </div>
        <div class="row margin-5-top">
            <div class="col-md-8 col-lg-6 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container form-col-without-padding">
                <div class="col-xs-12 col-sm-3">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
                </div>
                <div class="col-xs-12 col-sm-3">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcel_Click" Enabled="false" />
                </div>
                <div class="col-xs-12 col-sm-3">
                    <input type="button" id="print" class="btn btn-xs btn-dark-blue btn-block" value="" />
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="mSumGridPanel" runat="server">
        <JP:MiniHeader runat="server" Text="Lang.Aggregate" ID="mSumGridPanelTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="mSumGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed"
                    Scrolling="Vertical" runat="server" ShowFooter="false" BorderStyle="None" OnRowDataBound="mSumGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true" OnSorting="mSumGrid_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="BuyerName" SortExpression="BuyerName" HeaderText="Lang.Buyer" ItemStyle-CssClass="tl align-center" />
                        <asp:BoundField DataField="TotalBasketPrice" SortExpression="TotalBasketPrice" HeaderText="Lang.TotalBasketPrice" ItemStyle-CssClass="tl align-center" />
                        <asp:BoundField DataField="TotalCouponPrice" SortExpression="TotalCouponPrice" HeaderText="Lang.TotalCouponPrice" ItemStyle-CssClass="tl align-center" />
                        <asp:BoundField DataField="TotalCash" SortExpression="TotalCash" HeaderText="Lang.TotalCash" ItemStyle-CssClass="tl align-center" />
                        <asp:BoundField DataField="NumBaskets" SortExpression="NumBaskets" HeaderText="Lang.NumBaskets" ItemStyle-CssClass="tl align-center" />
                      </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="mItemsGridPanel" runat="server">
        <JP:MiniHeader runat="server" Text="Lang.Items" ID="mItemsGridTitle" />
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="mItemsGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mItemsGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true" OnSorting="mItemsGrid_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle CssClass="row-cell"></ItemStyle>
                            <HeaderStyle CssClass="row-cell"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="BuyerName" HeaderText="Lang.Buyer" ItemStyle-CssClass="tl align-center" SortExpression="BuyerName" />
                        <asp:BoundField DataField="BasketDate" HeaderText="Lang.BasketDate" ItemStyle-CssClass="tl align-center" SortExpression="BasketDate" />
                        <asp:BoundField DataField="PayMethod" HeaderText="Lang.PayMethod" ItemStyle-CssClass="tl align-center" SortExpression="PayMethod" />
                        <asp:BoundField DataField="PayStatus" HeaderText="Lang.PayStatus" ItemStyle-CssClass="tl align-center" SortExpression="PayStatus" />
                        <asp:BoundField DataField="PaymentDate" HeaderText="Lang.PaymentDate" ItemStyle-CssClass="tl align-center" SortExpression="PaymentDate" />
                        <asp:BoundField DataField="TotalBasketPrice" SortExpression="TotalBasketPrice" HeaderText="Lang.TotalBasketPrice" ItemStyle-CssClass="tl align-center" />
                        <asp:BoundField DataField="TotalCouponPrice" SortExpression="TotalCouponPrice" HeaderText="Lang.TotalCouponPrice" ItemStyle-CssClass="tl align-center" />
                        <asp:BoundField DataField="TotalCash" SortExpression="TotalCash" HeaderText="Lang.TotalCash" ItemStyle-CssClass="tl align-center" />
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
    <script>
        $(function () {
            $('#print').attr('value', Lang.Print);
            $('#print').click(function () {
                var head = Lang.AppReportPersons;
                printTable('.ScrollPanelCartable', '../../', head);
            });
            $('.GridRow,.GridAltRow').click(function () {
                $(this).toggleClass('selected');
            });
            dateSelect('.dateselect');
            $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.AppReportPersons } }));
        });
    </script>
</asp:Content>
