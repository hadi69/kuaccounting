﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reports_app_persons : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");

        mSumGrid.EmptyDataText = LangProvider.Instance["ListIsEmpty"];
        mItemsGrid.EmptyDataText = LangProvider.Instance["ListIsEmpty"];
        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["AppReportPersons"];
            Page.SetLang();
                mFromDate.Text = Helper.FormatDate(DateTime.Now.AddDays(-1), null);
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);
            Initial();
        }

    }


    private void Initial()
    {
        {
            //IDataReader persons = DbProxy.Instance.ExecuteReader("SELECT ID, ISNULL([dbo].[DisplayPerson](ID, 1), ''), ISNULL(Code, '') FROM Personnel WHERE Enabled = 1 AND Type=101");
            //mPersons.Items.Clear();

            //mPersons.Items.Add(new ListItem(LangProvider.Instance["AllPersons"], ""));

            //if (persons != null)
            //{
            //    while (persons.Read())
            //    {
            //        mPersons.Items.Add(new ListItem(persons.GetString(1) + " - کد: " + persons.GetString(2), persons.GetInt32(0).ToString()));
            //    }
            //    persons.Close();
            //}
            //mPersons.SelectedIndex = 0;
        }
    }


    protected void doShow_Click(object sender, EventArgs e)
    {
        doExcel.Enabled = true;
        mItemsGridPanel.Visible = mTypeDetail.Checked || mTypeBoth.Checked;
        mSumGridPanel.Visible = mTypeSum.Checked || mTypeBoth.Checked;

        if (mTypeSum.Checked || mTypeBoth.Checked)
            BindSumGrid();
        if (mTypeDetail.Checked || mTypeBoth.Checked)
            BindItemsGrid();
    }

    protected void mSumGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView record = e.Row.DataItem as DataRowView;
            //e.Row.Cells[10].Text = SiteSettings.ToMonetary(Helper.GetDecimal(record["ProfitInUnit"], 0), dt).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            e.Row.Cells[2].Text = Helper.GetDecimal(record, "TotalBasketPrice", 0).ToString("N2", Helper.NumberCulture).Replace(".00", "");
            e.Row.Cells[3].Text = Helper.GetDecimal(record, "TotalCouponPrice", 0).ToString("N2", Helper.NumberCulture).Replace(".00", "");
            e.Row.Cells[4].Text = Helper.GetDecimal(record, "TotalCash", 0).ToString("N2", Helper.NumberCulture).Replace(".00", "");
        }
    }

    protected void mItemsGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView record = e.Row.DataItem as DataRowView;
            e.Row.Cells[2].Text = Helper.FormatDate(Helper.GetDateTime(record["BasketDate"], Null.NullDate), "yyyy-MM-dd HH:mm");
            e.Row.Cells[5].Text = Helper.FormatDate(Helper.GetDateTime(record["PaymentDate"], Null.NullDate), "yyyy-MM-dd HH:mm");
            //1:Online;2:Coupon;3:Cash;4:InPlace
            int payMethod = Helper.GetInt(record["PayMethod"], 0);
            switch (payMethod)
            {
                case 1:
                    e.Row.Cells[3].Text = LangProvider.Instance["BasketPayMethodOnline"];
                    break;
                case 2:
                    e.Row.Cells[3].Text = LangProvider.Instance["BasketPayMethodCoupon"];
                    break;
                case 3:
                    e.Row.Cells[3].Text = LangProvider.Instance["BasketPayMethodCash"];
                    break;
                case 4:
                    e.Row.Cells[3].Text = LangProvider.Instance["BasketPayMethodInPlace"];
                    break;
            }
            //1:NotPaid;2:InPayment;3:Paid
            int payStatus = Helper.GetInt(record["PayStatus"], 0);
            switch (payStatus)
            {
                case 1:
                    e.Row.Cells[4].Text = LangProvider.Instance["BasketPayStatusNotPaid"];
                    break;
                case 2:
                    e.Row.Cells[4].Text = LangProvider.Instance["BasketPayStatusInPayment"];
                    break;
                case 3:
                    e.Row.Cells[4].Text = LangProvider.Instance["BasketPayStatusPaid"];
                    break;
            }
            e.Row.Cells[6].Text = Helper.GetDecimal(record, "TotalBasketPrice", 0).ToString("N2", Helper.NumberCulture).Replace(".00", "");
            e.Row.Cells[7].Text = Helper.GetDecimal(record, "TotalCouponPrice", 0).ToString("N2", Helper.NumberCulture).Replace(".00", "");
            e.Row.Cells[8].Text = Helper.GetDecimal(record, "TotalCash", 0).ToString("N2", Helper.NumberCulture).Replace(".00", "");
        }
    }


    protected void mSumGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SumSortCol)
        {
            SumSortOrder = SumSortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SumSortCol = e.SortExpression;
            SumSortOrder = "asc";
        }
        BindSumGrid();
    }

    protected void mItemsGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == ItemsSortCol)
        {
            ItemsSortOrder = ItemsSortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            ItemsSortCol = e.SortExpression;
            ItemsSortOrder = "asc";
        }
        BindItemsGrid();
    }

    private void BindSumGrid()
    {
        DateTime start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        DateTime end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);

        //int personId = Helper.GetInt(mPersons.SelectedValue, Null.NullInteger);
        string personName = mPersons.Text.Trim();
       
        #region SaleAdminReport
        string query = @"SELECT BasketView.UserName BuyerName
	            , SUM(TotalPrice + ISNULL(ShipmentPrice, 0)) TotalBasketPrice
	            , SUM(ISNULL(CouponPrice, 0)) TotalCouponPrice
	            , SUM(TotalPrice + ISNULL(ShipmentPrice, 0) - ISNULL(CouponPrice, 0)) TotalCash
	            , COUNT(ID) NumBaskets
            FROM BasketView ";
        string where = string.Format(" WHERE {0} <= BasketDate AND BasketDate <= {1} ", Helper.QoutedDateTime(start), Helper.QoutedDateTime(end));
        if (personName.Length > 0)
            where += " AND UserName LIKE " + Helper.Likenize(personName);
        if (mPayStatusNotPaid.Checked || mPayStatusInPayment.Checked || mPayStatusPaid.Checked)
        {
            where += "AND ( 0 = 1 ";
            if (mPayStatusNotPaid.Checked)
                where += " OR PayStatus=1";
            if (mPayStatusInPayment.Checked)
                where += " OR PayStatus=2";
            if (mPayStatusPaid.Checked)
                where += " OR PayStatus=3";
            where += ")";
            // all
        }
        where += " AND VendorID=" + SiteSettings.UserID;
        query += where;
        query += " GROUP BY BasketView.UserName";

        if (!string.IsNullOrEmpty(SumSortCol))
            query += " ORDER BY " + SumSortCol + " " + SumSortOrder;

        #endregion

        DataTable items = DbProxy.Instance.FillData(query, "all");
        try
        {
            mSumGrid.DataSource = items;
            mSumGrid.DataBind();
        }
        finally
        {
            if (null != items)
                items.Dispose();
        }
    }

    private void BindItemsGrid()
    {
        DateTime start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        DateTime end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);

        //int personId = Helper.GetInt(mPersons.SelectedValue, Null.NullInteger);
        string personName = mPersons.Text.Trim();

        #region SaleAdminReport
        string query = @"SELECT BasketView.UserName BuyerName, BasketDate, PayMethod, PayStatus, PaymentDate
	            , (TotalPrice + ISNULL(ShipmentPrice, 0)) TotalBasketPrice
	            , (ISNULL(CouponPrice, 0)) TotalCouponPrice
	            , (TotalPrice + ISNULL(ShipmentPrice, 0) - ISNULL(CouponPrice, 0)) TotalCash
            FROM BasketView ";
        string where = string.Format(" WHERE {0} <= BasketDate AND BasketDate <= {1} ", Helper.QoutedDateTime(start), Helper.QoutedDateTime(end));
        if (personName.Length > 0)
            where += " AND UserName LIKE " + Helper.Likenize(personName);
        if (mPayStatusNotPaid.Checked || mPayStatusInPayment.Checked || mPayStatusPaid.Checked)
        {
            where += "AND ( 0 = 1 ";
            if (mPayStatusNotPaid.Checked)
                where += " OR PayStatus=1";
            if (mPayStatusInPayment.Checked)
                where += " OR PayStatus=2";
            if (mPayStatusPaid.Checked)
                where += " OR PayStatus=3";
            where += ")";
            // all
        }
        where += " AND VendorID=" + SiteSettings.UserID;
        query += where;

        if (!string.IsNullOrEmpty(ItemsSortCol))
            query += " ORDER BY " + ItemsSortCol + " " + ItemsSortOrder;

        #endregion

        DataTable items = DbProxy.Instance.FillData(query, "all");
        try
        {
            mItemsGrid.DataSource = items;
            mItemsGrid.DataBind();
        }
        finally
        {
            if (null != items)
                items.Dispose();
        }
    }

    protected string SumSortCol
    {
        get { return Helper.GetString(ViewState["SumSortCol"], null); }
        set { ViewState["SumSortCol"] = value; }
    }

    protected string SumSortOrder
    {
        get { return Helper.GetString(ViewState["SumSortOrder"], "asc"); }
        set { ViewState["SumSortOrder"] = value; }
    }
    protected string ItemsSortCol
    {
        get { return Helper.GetString(ViewState["ItemsSortCol"], null); }
        set { ViewState["ItemsSortCol"] = value; }
    }

    protected string ItemsSortOrder
    {
        get { return Helper.GetString(ViewState["ItemsSortOrder"], "asc"); }
        set { ViewState["ItemsSortOrder"] = value; }
    }

    protected void doExcel_Click(object sender, EventArgs e)
    {
        Exporter.ExportExcel(Response
            , new List<string>(new string[] { mSumGridPanelTitle.Text, mItemsGridTitle.Text })
            , new List<GridView>(new GridView[] { mSumGrid, mItemsGrid}));
    }
}