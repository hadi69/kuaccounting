﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="reports_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <a href='../list.aspx?table=reports'>
        <asp:Label Text="Lang.MReportBuilders" runat="server" />
    </a><br />
    <asp:PlaceHolder runat="server" ID="mList"></asp:PlaceHolder>
    <script type="text/javascript">
        $(function () {
            setClassReport();
        });
    </script>
</asp:Content>
