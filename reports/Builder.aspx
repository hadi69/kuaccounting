﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true"
    CodeFile="Builder.aspx.cs" Inherits="reports_Builder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- Delete this -->

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 form-group-container col-without-padding">
                <div class="form-group">
                    <asp:Label runat="server" AssociatedControlID="mName" CssClass="left-aligned-label control-label col-xs-12 col-sm-1">Lang.ReportName</asp:Label>
                    <div class="col-xs-12 col-sm-11 form-col-without-padding">
                        <asp:TextBox runat="server" ID="mName" Width="400" CssClass="form-control input-sm"></asp:TextBox>
                        <asp:RequiredFieldValidator ControlToValidate="mName" ErrorMessage='' ValidationGroup='Save' runat="server" ID="e1"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-without-padding">
                <table class="table table-bordered table-striped table-condensed">
                    <tr>
                        <td>
                            <asp:Label Text="Lang.Tables" runat="server" />
                            <input type="button" value='<%= "SelectTable".Translate() %>' id="selectTable" class="btn btn-sm btn-primary" />
                        </td>
                        <td>
                            <asp:Label Text="Lang.FormDeclareItems" runat="server" />
                            <input type="button" value='<%= "SelectField".Translate() %>' id="selectField" class="btn btn-sm btn-primary" />
                        </td>
                        <td>
                            <asp:Label Text="Lang.AggFields" runat="server" />
                            <input type="button" value='<%= "SelectAggField".Translate() %>' id="selectAggField" class="btn btn-sm btn-primary" />
                        </td>
                        <td>
                            <asp:Label Text="Lang.Conditions" runat="server" />
                            <input type="button" value='<%= "NewCondition".Translate() %>' id="selectFilter" class="btn btn-sm btn-primary" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="mSelectedTables" runat="server" AutoGenerateColumns="False" OnRowDataBound="mSelectedTables_RowDataBound"
                                OnRowDeleting="mSelectedTables_RowDeleting" CssClass="table table-bordered table-striped table-condensed margin-0-bottom">
                                <Columns>
                                    <asp:BoundField DataField="Name" HeaderText="Lang.TableName" ReadOnly="True" />
                                    <asp:BoundField DataField="Title" HeaderText="Lang.Name" ReadOnly="True" />
                                    <asp:CommandField DeleteText="Lang.ColDel" ShowDeleteButton="True" />
                                </Columns>
                            </asp:GridView>
                        </td>
                        <td>
                            <asp:GridView ID="mSelectedFields" runat="server" AutoGenerateColumns="False" OnRowDataBound="mSelectedFields_RowDataBound"
                                OnRowDeleting="mSelectedFields_RowDeleting" CssClass="table table-bordered table-striped table-condensed margin-0-bottom">
                                <Columns>
                                    <asp:BoundField DataField="FullTitle" HeaderText="Lang.Name" ReadOnly="True" />
                                    <asp:BoundField DataField="FullName" HeaderText="Lang.Name" ReadOnly="True" />
                                    <asp:CommandField DeleteText="Lang.ColDel" ShowDeleteButton="True" />
                                </Columns>
                            </asp:GridView>
                        </td>
                        <td>
                            <asp:GridView ID="mSelectedAggFields" runat="server" AutoGenerateColumns="False"
                                OnRowDataBound="mSelectedAggFields_RowDataBound" OnRowDeleting="mSelectedAggFields_RowDeleting" CssClass="table table-bordered table-striped table-condensed margin-0-bottom">
                                <Columns>
                                    <asp:BoundField DataField="Title" HeaderText="Lang.headline" ReadOnly="True" />
                                    <asp:BoundField DataField="FullName" HeaderText="Lang.Name" ReadOnly="True" />
                                    <asp:BoundField DataField="Type" HeaderText="Lang.Type" ReadOnly="True" />
                                    <asp:CommandField DeleteText="Lang.ColDel" ShowDeleteButton="True" />
                                    <asp:BoundField DataField="FullName" HeaderText="" />
                                </Columns>
                            </asp:GridView>
                        </td>
                        <td>
                            <asp:GridView ID="mSelectedFilters" runat="server" AutoGenerateColumns="False" OnRowDataBound="mSelectedFilters_RowDataBound"
                                OnRowDeleting="mSelectedFilters_RowDeleting" CssClass="table table-bordered table-striped table-condensed margin-0-bottom">
                                <Columns>
                                    <asp:BoundField DataField="FullName" HeaderText="Lang.FieldFullName" ReadOnly="True" />
                                    <asp:BoundField DataField="Operation" HeaderText="Lang.FilterType" ReadOnly="True" />
                                    <asp:BoundField DataField="FilterValue" HeaderText='Lang.Filter' Visible="true" />
                                    <asp:CommandField DeleteText="Lang.ColDel" ShowDeleteButton="True" />
                                    <asp:BoundField DataField="FullName" HeaderText="" />
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Label Text="Lang.FinalQuery" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="ltr">
                            <asp:TextBox runat="server" ID="mQuery" TextMode="MultiLine" Rows="9" CssClass="form-control"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div>
        <asp:Button runat="server" ID="doCreateQuery" Text='Lang.CreateQuery' OnClick='doCreateQuery_Click' CssClass="btn btn-xs btn-primary" />
        <asp:Button runat="server" ID="doDisplay" Text='Lang.Show' OnClick='doDisplay_Click' CssClass="btn btn-xs btn-primary" />
        <asp:Button runat="server" ID="doSave" Text='Lang.SaveQuery' OnClick='doSave_Click' ValidationGroup='Save' CssClass="btn btn-xs btn-success" />
    </div>
    <div>
        <asp:Label runat="server" ID="mMsg"></asp:Label>
    </div>
    <asp:GridView ID="mGrid" runat="server" AllowPaging="True" AllowSorting="True" OnRowDataBound="mGrid_RowDataBound"
        OnSorting="mGrid_Sorting" PageSize="40" Width="100%" OnPageIndexChanging="mGrid_PageIndexChanging">
        <PagerSettings Position="TopAndBottom" FirstPageText="Lang.FirstPage" LastPageText="Lang.LastPage"
            Mode="NumericFirstLast" />
        <Columns>
            <asp:BoundField DataField="ID" HeaderText="Lang.ColRowNr" />
        </Columns>
        <EmptyDataTemplate>
            Lang.NoResultsToDisplay
        </EmptyDataTemplate>
    </asp:GridView>
    <div class="hidden">
        <input type="hidden" runat="server" id="mNewTables" class='NewTables' />
        <input type="hidden" runat="server" id="mNewFields" class='NewFields' />
        <input type="hidden" runat="server" id="mNewAggFields" class='NewAggFields' />
        <input type="hidden" runat="server" id="mNewFilters" class='NewFilters' />
        <asp:Button runat="server" ID="doAddTable" CssClass='doAddTable' OnClick='doAddTable_Click' />
        <asp:Button runat="server" ID="doAddField" CssClass='doAddField' OnClick='doAddField_Click' />
        <asp:Button runat="server" ID="doAddAggField" CssClass='doAddAggField' OnClick='doAddAggField_Click' />
        <asp:Button runat="server" ID="doAddFilter" CssClass='doAddFilter' OnClick='doAddFilter_Click' />
    </div>
    <script language="javascript">
        function callbackTables(v, m, f) {
            if (v != 'OK')
                return true;
            var tableNames = f.tableName;
            $('.NewTables').val(tableNames);
            $('.doAddTable').click();
            return true;
        }
        function callbackFields(v, m, f) {
            if (v != 'OK')
                return true;
            var filedNames = f.fieldName;
            //alert(filedNames);
            $('.NewFields').val(filedNames);
            $('.doAddField').click();
            return true;
        }
        function callbackAggFields(v, m, f) {
            if (v != 'OK')
                return true;
            var aggField = f.aggField;
            $('.NewAggFields').val(f.aggTitle + '|' + f.aggField + '|' + f.aggType + '|' + f.aggIndex);
            //alert($('.NewAggFields').val());
            $('.doAddAggField').click();
            return true;
        }
        function callbackFilters(v, m, f) {
            if (v != 'OK')
                return true;

            var filterField = f.filterField;
            $('.NewFilters').val(f.filterField + '|' + f.filterType + '|' + f.filterValue + '|' + f.filterIndex);
            //alert($('.NewFilters').val());
            $('.doAddFilter').click();
            return true;
        }
        function search(table, input) {
            var key = $(input).val();
            $('tr', table).each(function () {
                if (key == '')
                    $(this).show();
                else {
                    var tr = $(this).html();
                    if (tr.indexOf(key) > 0)
                        $(this).show();
                    else
                        $(this).hide();
                }
            });
        }

        function bindSearch() {
            $('#searchTable').unbind('keyup');
            $('#searchTable').keyup(function () {
                search('#tables', '#searchTable');
            });

            $('#searchField').unbind('keyup');
            $('#searchField').keyup(function () {
                search('#fields', '#searchField');
            });
        }
        function selectTables() {
            var html = "<div class='popupTitle'>" + Lang.SelectTable + "</div><div class='rtl tr'>" + Lang.Search + "<input type='text' id='searchTable' /><br />";
            html += "<div style='overflow: -moz-scrollbars-vertical; overflow-y: scroll; height: 200px'><table border=1 cellspacing=0 cellpadding=2 id='tables' width='500'>";
            html += "<tr><td>" + Lang.Name + "</td><td>" + Lang.TableName + "</td></tr>";
            var _url = 'builder.aspx?op=selecttables' + appendTime();
            $.ajax({
                url: _url,
                type: 'GET',
                async: false,
                success: function (tables) {
                    html += tables;
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert('Status=' + xhr.status + ' StatusText=' + xhr.statusText + ' thrownError=' + thrownError);
                    html = "ERROR";
                }
            });
            //alert(html);
            if (html == "ERROR")
                return;
            html += "</table></div></div>";
            $.prompt(html, {
                submit: callbackTables,
                buttons: { تأیید: 'OK', انصراف: 'Cancel' },
                focus: 1,
                loaded: bindSearch
            });
        }
        function selectFields() {
            var html = "<div class='popupTitle'>" + Lang.SelectField + "</div><div class='rtl tr'>" + Lang.Search + "<input type='text' id='searchField' /><br />";
            html += "<div style='overflow: -moz-scrollbars-vertical; overflow-y: scroll; height: 400px'><table border=1 cellspacing=0 cellpadding=2 id='fields' width='500'>";
            html += "<tr><td>" + Lang.Name + "</td><td>" + Lang.TableName + "</td></tr>";
            var _url = 'builder.aspx?op=selectfields' + appendTime();
            $.ajax({
                url: _url,
                type: 'GET',
                async: false,
                success: function (tables) {
                    html += tables;
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert('Status=' + xhr.status + ' StatusText=' + xhr.statusText + ' thrownError=' + thrownError);
                    html = "ERROR";
                }
            });
            //alert(html);
            if (html == "ERROR")
                return;
            html += "</table></div></div>";
            $.prompt(html, {
                submit: callbackFields,
                buttons: { تأیید: 'OK', انصراف: 'Cancel' },
                focus: 1,
                loaded: bindSearch
            });
        }
        function selectAggFields(td) {
            var html = "<div class='popupTitle'>" + Lang.NewComputationalField + "</div><div class='rtl tr'><table border='1' cellpadding='0' cellspacing='0' width='300'>";
            html += "<tr><td>" + Lang.headline + "</td><td><input name='aggTitle' type='text' id='aggTitle' style='width:160px;'></td></tr>";
            html += "<tr><td>" + Lang.RelatedField + "</td><td><select name='aggField' id='aggField' style='width:160px;'>";
            var _url = 'builder.aspx?op=selectaggfields' + appendTime();
            $.ajax({
                url: _url,
                type: 'GET',
                async: false,
                success: function (aggFields) {
                    html += aggFields;
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert('Status=' + xhr.status + ' StatusText=' + xhr.statusText + ' thrownError=' + thrownError);
                    html = "ERROR";
                }
            });
            //alert(html);
            if (html == "ERROR")
                return;
            html += "</select>";
            html += "</td></tr>";
            html += "<tr><td>" + Lang.OperationType + "</td><td><select name='aggType' id='aggType' style='width:160px;'>";
            html += "<option selected='selected' value='0'>" + Lang.Num + "</option>";
            html += "<option value='1'>" + Lang.Sum + "</option>";
            html += "<option value='2'>" + Lang.Mean + "</option>";
            html += "<option value='3'>" + Lang.SMaximum + "</option>";
            html += "<option value='4'>" + Lang.SMinimum + "</option>";
            html += "</select>";
            html += "<input name='aggIndex' type='hidden' value='0' id='aggIndex' /></td></tr></table></table></div>";
            $.prompt(html, {
                submit: callbackAggFields,
                buttons: { تأیید: 'OK', انصراف: 'Cancel' },
                focus: 1,
                loaded: function () {
                    bindSearch();
                    if (td) {
                        $('#aggTitle').val($(td).prev().prev().prev().prev().text());
                        $('#aggField').val($(td).prev().prev().prev().text());
                        var t = $(td).prev().prev().text();
                        if (t == 'Count')
                            $('#aggType').val(0);
                        else if (t == 'Sum')
                            $('#aggType').val(1);
                        else if (t == 'Avg')
                            $('#aggType').val(2);
                        else if (t == 'Max')
                            $('#aggType').val(3);
                        else if (t == 'Min')
                            $('#aggType').val(4);
                        $('#aggIndex').val($(td).closest('table').find('tr').index($(td).parent()));
                    }
                }
            });
        }
        function selectFilters(td) {
            var html = "<div class='popupTitle'>" + Lang.NewCondition + "</div><div class='rtl tr'><table border='1' cellpadding='0' cellspacing='0' width='300'>";
            html += "<tr><td>" + Lang.RelatedField + "</td><td><select name='filterField' id='filterField' style='width:160px;'>";
            var _url = 'builder.aspx?op=selectfilters' + appendTime();
            $.ajax({
                url: _url,
                type: 'GET',
                async: false,
                success: function (fields) {
                    html += fields;
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert('Status=' + xhr.status + ' StatusText=' + xhr.statusText + ' thrownError=' + thrownError);
                    html = "ERROR";
                }
            });
            //alert(html);
            if (html == "ERROR")
                return;
            html += "</select>";
            html += "</td></tr>";
            html += "<tr><td>" + Lang.FilterType + "</td><td><select name='filterType' id='filterType' style='width:160px;'>";
            html += "<option value='='>" + Lang.Equal + "</option>";
            html += "<option value='&lt;'>" + Lang.LessThan + "</option>";
            html += "<option value='&gt;'>" + Lang.GreaterThan + "</option>";
            html += "<option value='&lt;='>" + Lang.LessThanOrEqual + "</option>";
            html += "<option value='&gt;='>" + Lang.GreaterThanOrEqual + "</option>";
            html += "<option value='&lt;&gt;'>" + Lang.NotEqual + "</option>";
            html += "<option value='like'>" + Lang.Like + "</option>";
            html += "</select>";
            html += "</td></tr>";
            html += "<tr><td>" + Lang.Filter + "</td><td><input name='filterValue' type='text' id='filterValue' style='width:160px;'><input name='filterIndex' type='hidden' value='0' id='filterIndex' /></td></tr>";
            html += "</table></table></div>";
            $.prompt(html, {
                submit: callbackFilters,
                buttons: { تأیید: 'OK', انصراف: 'Cancel' },
                focus: 1,
                loaded: function () {
                    bindSearch();
                    if (td) {
                        $('#filterField').val($(td).prev().prev().prev().prev().text());
                        $('#filterType').val($(td).prev().prev().prev().text());
                        $('#filterValue').val($(td).prev().prev().text());
                        $('#filterIndex').val($(td).closest('table').find('tr').index($(td).parent()));
                    }
                }
            });
        }
        $(function () {
            $('#selectTable').click(function () {
                selectTables();
                return false;
            });
            $('#selectField').click(function () {
                selectFields();
                return false;
            });
            $('#selectAggField').click(function () {
                selectAggFields();
                return false;
            });
            $('#selectFilter').click(function () {
                selectFilters();
                return false;
            });
            $('.deleteButton').click(function () {
                return confirm(Lang.ConfirmDelete);
            });
            $('.ef').click(function () {
                selectFilters($(this).closest('td'));
                return false;
            });
            $('.ea').click(function () {
                selectAggFields($(this).closest('td'));
                return false;
            });
        });
        $(function () {
            setClassReport();
        });
        $(document).ready(function () {
            $("#bar").prepend(breadcrumb({
                title: Lang.AddNewDynamicReport,
                grandParent: {
                    title: Lang.MSys,
                    url: '<%= VirtualPathUtility.ToAbsolute("~") %>' + 'menu/sys.aspx'
                },
                parent: {
                    title: Lang.DynamicReport,
                    url: '<%= VirtualPathUtility.ToAbsolute("~") %>' + 'list.aspx?table=reports'
                }
            }));
        });
    </script>
</asp:Content>
