﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true"
    CodeFile="ShowReport.aspx.cs" Inherits="reports_ShowReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%=Report.JavaScript%>
    <%=ReportCSS%>
    
    <title>
        <%=Report.ReportFile%></title>
    <%=Meta%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <h1><%=Name%></h1>
    <table>
        <tr>
            <td>
                <%=Report.ParameterHtml%><hr>
            </td>
        </tr>
        <tr>
            <td>
                <%if (!Report.NoShow)
                  { %>
                <%=Report.Html%>
                <%} %>
            </td>
        </tr>
        <%
            if (Report.MaxErrorSeverity > 4 && !Report.NoShow)
            {
        %>
        <tr>
            <td>
                <table width="100%" border="1" bordercolor="#000080" bgcolor="#FFFFE0" style="border-style: solid;
                    border-collapse: collapse;">
                    <thead bgcolor="#00FFFF" class="TableHeader">
                        <tr>
                            <th align="left">
                                Errors
                            </th>
                        </tr>
                    </thead>
                    <%
                        foreach (string err in Report.Errors)
                        {
                    %>
                    <tr>
                        <td>
                            <%=err%>
                        </td>
                    </tr>
                    <%
                        }
                    %>
                </table>
            </td>
        </tr>
        <%
            }
            else
            {
                if (Report.NoShow)
                {%>
        <tr>
            <td>
                <table width="100%" border="1" bordercolor="#000080" bgcolor="#FFFFE0" style="border-style: solid;
                    border-collapse: collapse;">
                    <tr>
                        <td>
                            Please input any report parameters and then press the 'Run Report' button
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <%   
            }
            }
        %>
    </table>
    <script type="text/javascript">
        $(function () {
            setClassReport();
        });
    </script>
</asp:Content>
