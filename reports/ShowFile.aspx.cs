﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reports_ShowFile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string type = Request.QueryString["type"];
        string unique = Request.QueryString["unique"];
        string key = string.Format(Request.ApplicationPath + "/reports/ShowFile.aspx?type={0}&unique={1}", type, unique);

        object buffer = Context.Session[key];
        if (buffer == null)
        {
            Context.Response.StatusCode = 404;		// must have been thrown out of request
            return;
        }
        Context.Session.Remove(key);

        Response.ContentType = GetMimeType(type);
        Response.BinaryWrite(buffer as byte[]);
    }

    private string GetMimeType(string type)
    {
        switch (type.ToLower())
        {
            case "bmp":
                return "image/bmp";
            case "jpeg":
            case "jpe":
            case "jpg":
            case "jfif":
                return "image/jpeg";
            case "gif":
                return "image/gif";
            case "png":
                return "image/png";
            case "tif":
            case "tiff":
                return "image/tiff";
            case "pdf":
                return "application/pdf";
            case "xml":
            case "rdl":
                return "application/xml";
            default:
                return "";
        }
    }

}