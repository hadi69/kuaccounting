﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

public partial class reports_Builder : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        mMsg.Text = "";
        #region Operations
        string op = Request.QueryString["op"];
        if (op == "selecttables")
        {
            Response.Write(HandleSelectTables());
            Response.End();
        }
        else if (op == "selectfields")
        {
            Response.Write(HandleSelectFields());
            Response.End();
        }
        else if (op == "selectaggfields")
        {
            Response.Write(HandleSelectAggFields());
            Response.End();
        }
        else if (op == "selectfilters")
        {
            Response.Write(HandleSelectFilters());
            Response.End();
        }
        #endregion
        if (!IsPostBack)
        {
            this.EditID = Helper.GetInt(Request.QueryString["id"], -1);
            BindInitData();
        }
    }
    void BindInitData()
    {
        ReportFile file = DbProxy.Instance.GetReport(this.EditID);
        if (file != null)
        {
            mName.Text = file.Name;
            mQuery.Text = file.Query;
            this.Tables = file.Tables;
            this.Fields = file.Fields;
            this.AggregateFields = file.AggregateFields;
            this.Filters = file.Filters;
        }
        else
        {
            mName.Text = "";
            mQuery.Text = "";
            this.Tables = new List<TableInfo>();
            this.Fields = new List<FieldInfo>();
            this.AggregateFields = new List<AggregateField>();
            this.Filters = new List<Filter>();
        }
        BindTables();
        BindFields();
        BindAggFields();
        BindFilters();
    }
    ReportFile CreateReport()
    {
        List<TableInfo> tables = this.Tables;
        List<FieldInfo> fields = this.Fields;
        List<AggregateField> aggFields = this.AggregateFields;
        List<Filter> filters = this.Filters;
        if (tables.Count == 0 || (fields.Count == 0 && aggFields.Count == 0))
            throw new Exception("no table or field");
        ReportFile file = new ReportFile();
        file.ID = this.EditID;
        file.Tables.AddRange(tables);
        file.Fields.AddRange(fields);
        file.AggregateFields.AddRange(aggFields);
        file.Filters.AddRange(filters);
        file.Name = mName.Text;
        file.Query = mQuery.Text;
        return file;
    }
    protected void doSave_Click(object sender, EventArgs e)
    {
        try
        {
            if (mName.Text.Trim().Length == 0)
                throw new Exception("no name");
            ReportFile file = CreateReport();
            if (DbProxy.Instance.SaveReport(file))
            {
                this.EditID = file.ID;
                mMsg.Text = "done";
            }
            else
                mMsg.Text = "failed";
            // todo: save it
        }
        catch (Exception ex)
        {
            mMsg.Text = ex.Message;
        }
    }

    protected void doDisplay_Click(object sender, EventArgs e)
    {
        if (mQuery.Text.Trim().Length == 0)
            doCreateQuery_Click(null, null);
        if (mQuery.Text.Trim().Length == 0)
            return;
        RunReport();
    }
    protected void doCreateQuery_Click(object sender, EventArgs e)
    {
        try
        {
            ReportFile file = CreateReport();
            ReportEngine engine = new ReportEngine(file);
            mQuery.Text = engine.CreateQuery();
        }
        catch (Exception ex)
        {
            mMsg.Text = ex.Message;
        }
    }
    protected void doAddTable_Click(object sender, EventArgs e)
    {
        string neuNames = mNewTables.Value;
        if (string.IsNullOrEmpty(neuNames))
            return;
        List<TableInfo> cur = this.Tables;
        string[] tables = neuNames.Split(',');
        for (int i = 0; i < tables.Length; i++)
        {
            TableInfo table = TableController.Instance[tables[i]];
            if (table != null && !cur.Contains(table))
                cur.Add(table);
        }
        this.Tables = cur;
        BindTables();
    }
    protected void doAddField_Click(object sender, EventArgs e)
    {
        string neuNames = mNewFields.Value;
        if (string.IsNullOrEmpty(neuNames))
            return;
        List<FieldInfo> cur = this.Fields;
        string[] fields = neuNames.Split(',');
        for (int i = 0; i < fields.Length; i++)
        {
            FieldInfo field = TableController.FindField(fields[i]);
            if (field != null && !cur.Contains(field))
                cur.Add(field);
        }
        this.Fields = cur;
        BindFields();
    }
    protected void doAddAggField_Click(object sender, EventArgs e)
    {
        string neu = mNewAggFields.Value;
        if (string.IsNullOrEmpty(neu))
            return;
        List<AggregateField> cur = this.AggregateFields;
        //aggTitle + '|' + aggField + '|' + aggType
        string[] split = neu.Split('|');
        AggregateField aggField = new AggregateField();
        aggField.Title = split[0];
        aggField.FullName = split[1];
        aggField.Type = Helper.GetEnum(split[2], AggregateType.Count);
        int index = -1;
        if (split.Length > 3)
            index = Helper.GetInt(split[3], 0) - 1;
        if (index >= 0)
            cur[index] = aggField;
        else
            cur.Add(aggField);
        this.AggregateFields = cur;
        BindAggFields();
    }
    protected void doAddFilter_Click(object sender, EventArgs e)
    {
        string neu = mNewFilters.Value;
        if (string.IsNullOrEmpty(neu))
            return;
        List<Filter> cur = this.Filters;
        //f.filterField + '|' + f.filterType + '|' + f.filterValue
        string[] split = neu.Split('|');
        Filter filter = new Filter();
        filter.FullName = split[0];
        filter.Operation = split[1];
        filter.FilterValue = split[2];
        int index = -1;
        if (split.Length > 3)
            index = Helper.GetInt(split[3], 0) - 1;
        if (index >= 0)
            cur[index] = filter;
        else
            cur.Add(filter);
        this.Filters = cur;
        BindFilters();
    }
    #region Bind data
    void BindTables()
    {
        List<TableInfo> tables = this.Tables;
        mSelectedTables.DataSource = tables;
        mSelectedTables.DataBind();
    }
    void BindFields()
    {
        List<FieldInfo> fields = this.Fields;
        mSelectedFields.DataSource = fields;
        mSelectedFields.DataBind();
    }
    void BindAggFields()
    {
        List<AggregateField> fields = this.AggregateFields;
        mSelectedAggFields.DataSource = fields;
        mSelectedAggFields.DataBind();
    }
    void BindFilters()
    {
        List<Filter> cur = this.Filters;
        mSelectedFilters.DataSource = cur;
        mSelectedFilters.DataBind();
    }
    #endregion

    string HandleSelectTables()
    {
        List<TableInfo> tables = this.Tables;
        List<TableInfo> neu = null;
        if (tables.Count == 0) // no table selected yet --> show all
        {
            neu = new List<TableInfo>();
            Dictionary<string, TableInfo>.Enumerator enu = TableController.Instance.Tables.GetEnumerator();
            while (enu.MoveNext())
                neu.Add(enu.Current.Value);
        }
        else
            neu = ReportEngine.FindRelatedTables(tables);
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < neu.Count; i++)
            builder.AppendFormat("<tr><td><input type='{2}' id='t{1}' name='tableName' value='{1}' /><label for='t{1}'>{0}</label></td><td>{1}</td></tr>", neu[i].Title, neu[i].Name
                , tables.Count == 0 ? "radio" : "checkbox");
        return builder.ToString();
    }
    string HandleSelectFields()
    {
        List<TableInfo> tables = this.Tables;
        List<FieldInfo> fields = this.Fields;
        List<FieldInfo> neu = new List<FieldInfo>();
        if (tables.Count == 0) // no table selected yet --> show none
        {
        }
        else
        {
            for (int i = 0; i < tables.Count; i++)
                for (int j = 0; j < tables[i].Fields.Count; j++)
                    if (!fields.Contains(tables[i].Fields[j]))
                        neu.Add(tables[i].Fields[j]);
        }
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < neu.Count; i++)
            builder.AppendFormat("<tr><td><input type='checkbox' id='t{2}' name='fieldName' value='{2}' /><label for='t{2}'>{0} - {1}</label></td><td>{2}</td></tr>", neu[i].TableTitle, neu[i].Title, neu[i].FullName);
        return builder.ToString();
    }
    string HandleSelectAggFields()
    {
        return ShowFieldsAsSelect();
    }
    string HandleSelectFilters()
    {
        return ShowFieldsAsSelect();
    }
    string ShowFieldsAsSelect()
    {
        List<TableInfo> tables = this.Tables;
        List<FieldInfo> fields = this.Fields;
        List<FieldInfo> neu = new List<FieldInfo>();
        if (tables.Count == 0) // no table selected yet --> show none
        {
        }
        else
        {
            for (int i = 0; i < tables.Count; i++)
                for (int j = 0; j < tables[i].Fields.Count; j++)
                    if (!fields.Contains(tables[i].Fields[j]))
                        neu.Add(tables[i].Fields[j]);
        }
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < neu.Count; i++)
            builder.AppendFormat("<option value='{1}'>{0}</option>", neu[i].FullTitle, neu[i].FullName);
        return builder.ToString();
    }

    #region Tables Grid
    protected void mSelectedTables_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TableInfo table = e.Row.DataItem as TableInfo;
            if (table != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = table.Name.ToString();

                // Handle the Delete Button Click and show a Confirmation Message
                TableCell cc = e.Row.Cells[e.Row.Cells.Count - 1];
                LinkButton delete = (LinkButton)cc.Controls[0];
                delete.CssClass = "deleteButton";
            }
        }
    }
    protected void mSelectedTables_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (e.RowIndex < 0)
            return;
        string name = mSelectedTables.Rows[e.RowIndex].Cells[0].Text;
        if (string.IsNullOrEmpty(name))
            return;

        TableInfo table = TableController.Instance[name];
        if (table == null)
            return;

        List<TableInfo> cur = this.Tables;
        cur.Remove(table);
        this.Tables = cur;

        // remove all its fields
        List<FieldInfo> curFields = this.Fields;
        if (curFields != null && curFields.Count > 0)
        {
            for (int i = 0; i < table.Fields.Count; i++)
                curFields.Remove(table.Fields[i]);
        }
        this.Fields = curFields;

        //// Remove Aggrigate Fields
        //// todo: do it

        //// Remove Fields in Filters too
        //DataTable dt = GetFiltersDB();
        //for (int r = 0; r < dt.Rows.Count; r++)
        //{
        //    if (dt.Rows[r][0].ToString().StartsWith(table.Name + "."))
        //    {
        //        dt.Rows.RemoveAt(r);
        //        r--;
        //    }
        //}
        //mFilters.DataSource = dt;
        //mFilters.DataBind();

        // Remove Fields in CurveItems
        // todo: do it

        BindTables();
        BindFields();
    }
    #endregion

    #region Fields Grid
    protected void mSelectedFields_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            FieldInfo field = e.Row.DataItem as FieldInfo;
            if (field != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = field.FullName.ToString();

                // Handle the Delete Button Click and show a Confirmation Message
                TableCell cc = e.Row.Cells[e.Row.Cells.Count - 1];
                LinkButton delete = (LinkButton)cc.Controls[0];
                delete.CssClass = "deleteButton";
            }
        }
    }
    protected void mSelectedFields_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (e.RowIndex < 0)
            return;
        string name = mSelectedFields.Rows[e.RowIndex].Cells[0].Text;
        if (string.IsNullOrEmpty(name))
            return;

        FieldInfo field = TableController.FindField(name);
        if (field == null)
            return;

        List<FieldInfo> curFields = this.Fields;
        curFields.Remove(field);
        this.Fields = curFields;

        BindFields();
    }
    #endregion

    #region Agg Fields Grid
    protected void mSelectedAggFields_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            AggregateField field = e.Row.DataItem as AggregateField;
            if (field != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = field.Title;
                e.Row.Cells[1].Text = field.FullName.ToString();
                e.Row.Cells[e.Row.Cells.Count - 1].Text = "<a href='#' class='ea'>" + "ColEdit".Translate() + "</a>";

                // Handle the Delete Button Click and show a Confirmation Message
                TableCell cc = e.Row.Cells[e.Row.Cells.Count - 2];
                LinkButton delete = (LinkButton)cc.Controls[0];
                delete.CssClass = "deleteButton";
            }
        }
    }
    protected void mSelectedAggFields_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (e.RowIndex < 0)
            return;
        string name = mSelectedAggFields.Rows[e.RowIndex].Cells[1].Text;
        if (string.IsNullOrEmpty(name))
            return;

        List<AggregateField> cur = this.AggregateFields;
        for (int i = 0; i < cur.Count; i++)
            if (cur[i].FullName == name)
            {
                cur.RemoveAt(i);
                break;
            }

        this.AggregateFields = cur;

        BindAggFields();
    }
    #endregion

    #region Filters Grid
    protected void mSelectedFilters_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Filter filter = e.Row.DataItem as Filter;
            if (filter != null)
            {
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = filter.FullName.ToString();
                e.Row.Cells[e.Row.Cells.Count - 1].Text = "<a href='#' class='ef'>" + "ColEdit".Translate() + "</a>";

                // Handle the Delete Button Click and show a Confirmation Message
                TableCell cc = e.Row.Cells[e.Row.Cells.Count - 2];
                LinkButton delete = (LinkButton)cc.Controls[0];
                delete.CssClass = "deleteButton";
            }
        }
    }
    protected void mSelectedFilters_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (e.RowIndex < 0)
            return;
        string name = mSelectedFilters.Rows[e.RowIndex].Cells[0].Text;
        if (string.IsNullOrEmpty(name))
            return;

        List<Filter> cur = this.Filters;
        for (int i = 0; i < cur.Count; i++)
            if (cur[i].FullName == name)
            {
                cur.RemoveAt(i);
                break;
            }

        this.Filters = cur;

        BindFilters();
    }

    #endregion

    #region Report
    private SortDirection SortDirection
    {
        get
        {
            if (ViewState["SortDirection"] == null)
                return SortDirection.Ascending;
            return (SortDirection)ViewState["SortDirection"];
        }
        set
        {
            ViewState["SortDirection"] = value;
        }
    }
    private string SortExpression
    {
        get
        {
            if (ViewState["SortExpression"] == null)
                return "";
            return (string)ViewState["SortExpression"];
        }
        set
        {
            ViewState["SortExpression"] = value;
        }
    }
    protected void RunReport()
    {
        DataTable results = GetData();
        if (results == null)
        {
            mGrid.DataSource = null;
            mGrid.DataBind();
            return;
        }

        mGrid.DataSource = results;
        if (results != null && results.Columns.Count > 0)
        {
            //object o = mGrid.Columns[0];
            (mGrid.Columns[0] as BoundField).DataField = results.Columns[0].ColumnName;
        }
        else
            mGrid.DataSource = null;
        mGrid.DataBind();

        //ShowChart(results);
    }
    private DataTable GetData()
    {
        // check for tables and fields
        if (mQuery.Text.Trim().Length == 0)
            return null;

        DataTable results = DbProxy.Instance.FillData(mQuery.Text, "Results");
        if ((results != null) && results.Rows.Count > 0 && !string.IsNullOrEmpty(this.SortExpression))
            if (this.SortDirection == SortDirection.Ascending)
                results.DefaultView.Sort = this.SortExpression;
            else
                results.DefaultView.Sort = this.SortExpression + " DESC";

        return results;
    }
    protected void mGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (this.SortExpression == e.SortExpression)
            this.SortDirection = this.SortDirection == SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending;
        else
            this.SortDirection = e.SortDirection;
        this.SortExpression = e.SortExpression;
        mGrid.PageIndex = 0;
        RunReport();
    }
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            //List<FieldInfo> fields = this.Fields;
            //if (null != SelectedFieldHashCodes)
            //    for (int i = 0; i < SelectedFieldHashCodes.Count; i++)
            //        if (e.Row.Cells[i + 1].Controls.Count > 0)
            //        {
            //            LinkButton link = e.Row.Cells[i + 1].Controls[0] as LinkButton;
            //            if (link != null)
            //                link.Text = ArshamKoosha.Reporting.Settings.Instance.FindField(SelectedFieldHashCodes[i]).Title;
            //        }
            //int start = null != SelectedFieldHashCodes ? SelectedFieldHashCodes.Count : 0;
            //if (null != AggregateFields)
            //    for (int i = 0; i < AggregateFields.Count; i++)
            //        if (e.Row.Cells[start + i + 1].Controls.Count > 0)
            //        {
            //            LinkButton link = e.Row.Cells[start + i + 1].Controls[0] as LinkButton;
            //            if (link != null)
            //                link.Text = AggregateFields[i].Title;
            //        }

        }
        else if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[0].Text = (1 + e.Row.RowIndex + mGrid.PageIndex * mGrid.PageSize).ToString();
            DataRowView view = e.Row.DataItem as DataRowView;
            if (view != null)
            {
                //for (int i = 0; i < view.Row.Table.Columns.Count; i++)
                //{
                //    if (i < mFields.Count)
                //    {
                //        //Type type = view[i].GetType();
                //        if (!(view[i] is bool))
                //            e.Row.Cells[i + 1].Text = mFields[i].Format(view[i]);
                //    }
                //}
            }
        }
    }
    protected void mGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        mGrid.PageIndex = e.NewPageIndex;
        RunReport();
    }
    #endregion

    #region Props
    List<TableInfo> Tables
    {
        get
        {
            List<TableInfo> res = new List<TableInfo>();
            string names = Helper.GetString(Session["ReportBuilder.Tables"], "");
            if (string.IsNullOrEmpty(names))
                return res;
            string[] tables = names.Split('|');
            for (int i = 0; i < tables.Length; i++)
            {
                TableInfo table = TableController.Instance[tables[i]];
                if (table != null)
                    res.Add(table);
            }
            return res;
        }
        set
        {
            string names = "";
            if (value != null && value.Count > 0)
            {
                for (int i = 0; i < value.Count; i++)
                    names += value[i].Name + "|";
                names = names.Substring(0, names.Length - 1);
            }
            Session["ReportBuilder.Tables"] = names;

        }
    }
    List<FieldInfo> Fields
    {
        get
        {
            List<FieldInfo> res = new List<FieldInfo>();
            string names = Helper.GetString(Session["ReportBuilder.Fields"], "");
            if (string.IsNullOrEmpty(names))
                return res;
            string[] fields = names.Split('|');
            for (int i = 0; i < fields.Length; i++)
            {
                FieldInfo field = TableController.FindField(fields[i]);
                if (field != null)
                    res.Add(field);
            }
            return res;
        }
        set
        {
            string names = "";
            if (value != null && value.Count > 0)
            {
                for (int i = 0; i < value.Count; i++)
                    names += value[i].FullName + "|";
                names = names.Substring(0, names.Length - 1);
            }
            Session["ReportBuilder.Fields"] = names;
        }
    }
    List<AggregateField> AggregateFields
    {
        get
        {
            if (Session["ReportBuilder.AggregateFields"] == null)
                return new List<AggregateField>();
            return (List<AggregateField>)Session["ReportBuilder.AggregateFields"];
        }
        set
        {
            Session["ReportBuilder.AggregateFields"] = value;
        }
    }
    List<Filter> Filters
    {
        get
        {
            if (Session["ReportBuilder.Filters"] == null)
                return new List<Filter>();
            return (List<Filter>)Session["ReportBuilder.Filters"];
        }
        set
        {
            Session["ReportBuilder.Filters"] = value;
        }
    }
    int EditID
    {
        get
        {
            return Helper.GetInt(ViewState["EditID"], -1);
        }
        set
        {
            ViewState["EditID"] = value;
        }
    }
    #endregion


}