﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class reports_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            BindData();
    }
    void BindData()
    {
        string pfile = this.MapPathSecure("~/reports/files");

        DirectoryInfo di;
        FileSystemInfo[] afsi;
        List<ReportFile> files = new List<ReportFile>();
        try
        {
            di = new DirectoryInfo(pfile);
            afsi = di.GetFileSystemInfos();
            foreach (FileSystemInfo fsi in afsi)
            {
                if ((fsi.Attributes & FileAttributes.Directory) == 0 &&
                    fsi.Extension.ToLower() != ".rdl")	// only show report files
                    continue;

                files.Add(new ReportFile(Path.GetFileNameWithoutExtension(fsi.Name), fsi.Name));
            }
        }
        catch (Exception ex)
        {
            //tw.Write("<p> No reports!  Exception={0}", ex.Message);
            return;
        }

        for (int i = 0; i < files.Count; i++)
        {
            mList.Controls.Add(new LiteralControl(string.Format("<a href='ShowReport.aspx?rs:url=files/{0}'>{1}</a><br />", files[i].FileName, files[i].Title)));
        }
        //mList.DataSource = files;
        //mList.DataBind();
    }
    private class ReportFile
    {
        private string mTitle, mFileName;
        public ReportFile(string title, string fileName)
        {
            mTitle = title;
            mFileName = fileName;
        }
        public string Title { get { return mTitle; } set { mTitle = value; } }
        public string FileName { get { return mFileName; } set { mFileName = value; } }
    }
}