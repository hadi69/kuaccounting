﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml;
using System.Text;
using System.Data.SqlClient;

public partial class set : System.Web.UI.Page
{
    DbProxy mDbProxy = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        mDbProxy = new DbProxy();
        object o = Request.RawUrl;
        Response.Clear();
        object o1 = Request.Url;
        object o2 = Request.UrlReferrer;
        int t = Request.Files.Count;
        mDbProxy.LastException = null;
        string op = Helper.GetString(Request.QueryString["op"], "RunQuery").ToLower();
        if (op == "RunQuery".ToLower())
            RunQuery(false);
        else if (op == "RunScalar".ToLower())
            RunQuery(true);
        else if (op == "Multi".ToLower())
            RunMulti();
        else if (op == "Master".ToLower())
            RunMasterDetail();
        else if (op == "Master2".ToLower())
            RunMasterDetail2();
        else if (op == "Masters".ToLower())
            RunMastersDetail();
        else if (op == "Delete".ToLower())
            RunDelete();
        else if (op == "MultiDelete".ToLower())
            RunMultiDelete();
        // else if (op == "SendToSepas".ToLower())
        //   SendToSepas();
        else if (op == "SetForm".ToLower())
        {
            try
            {
                new FormEngine(Request).SaveForm();
            }
            catch (Exception ex)
            {
                Response.Write("Error: " + ex.Message);
            }
        }
        else if (op == "SMS".ToLower())
            StartSendSMS();
        else if (op == "UpdateLang".ToLower())
        {
            UpdateLang();
        }
        else if (op == "SetMonetaryUnit".ToLower())
            SetMonetaryUnit();
        else if (op == "SetProgram".ToLower())
            SetProgram();
        else if (op == "SetDB".ToLower())
            SetDB();
        else if (op == "ToVoucherDB".ToLower())
            ToVoucherDB();
        else if (op == "CalcOrder".ToLower())
        {
            XmlDocument xml = CalcOrder();
            Response.Write(xml.OuterXml);
        }
        else if (op == "InsertValues".ToLower())
        {
            RunInsertValues();
        }
        else if (op == "InsertFishValues".ToLower())
        {
            string fishId = Request.QueryString["fishId"];
            InsertFishValues(fishId, Helper.GetRequestFields(Request.QueryString["fields"].Replace('-', '=').Replace('|', '&')));
        }
        else if (op == "Details".ToLower())
            RunDetails();
        Response.End();
    }

    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        try
        {
            if (mDbProxy != null)
                mDbProxy.Dispose();
        }
        catch { }
    }

    #region logging
    static Type dateType = typeof(DateTime);
    protected string GetIPAddress()
    {
        string ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipAddress))
        {
            string[] addresses = ipAddress.Split(',');
            if (addresses.Length != 0)
            {
                return addresses[0];
            }
        }

        return Request.ServerVariables["REMOTE_ADDR"];
    }
    string LogRow(DataRow row)
    {
        StringBuilder builder = new StringBuilder();
        StringBuilder more = new StringBuilder();
        for (int c = 0; c < row.Table.Columns.Count; c++)
        {
            string val = Helper.GetString(row[c], null);
            if (!string.IsNullOrEmpty(val))
            {
                string colName = row.Table.Columns[c].ColumnName;
                if (colName == "SyncVer")
                    continue;
                string colDisplay = colName.EndsWith("TreePathName")
                    ? LangProvider.Instance[colName.Substring(0, colName.Length - 4)] : LangProvider.Instance[colName];
                if (colName.EndsWith("ID") || colName.EndsWith("TreePath"))
                    more.AppendFormat("{0}={1}, ", row.Table.Columns[c].ColumnName, row[c]);
                else if (row.Table.Columns[c].DataType == dateType)
                    builder.AppendFormat("{0}={1}\r\n", colDisplay
                        , Helper.FormatDate(Helper.GetDateTime(row[c], Null.NullDate), null));
                else
                    builder.AppendFormat("{0}={1}\r\n", colDisplay, row[c]);
            }
        }
        builder.AppendLine(LangProvider.Instance["More"] + ":" + more.ToString());
        return builder.ToString();
    }
    string LogRowData(DataRow row)
    {
        StringBuilder builder = new StringBuilder();
        for (int c = 0; c < row.Table.Columns.Count; c++)
        {
            string val = Helper.GetString(row[c], null);
            if (!string.IsNullOrEmpty(val))
            {
                string colName = row.Table.Columns[c].ColumnName;
                if (colName == "SyncVer")
                    continue;
                if (colName.EndsWith("ID") || colName.EndsWith("TreePath"))
                {
                    // more.AppendFormat("{0}={1}, ", row.Table.Columns[c].ColumnName, row[c]);
                }
                else if (row.Table.Columns[c].DataType == dateType)
                    builder.AppendFormat("{0}, ", Helper.FormatDate(Helper.GetDateTime(row[c], Null.NullDate), null));
                else
                    builder.AppendFormat("{0}, ", row[c]);
            }
        }
        return builder.ToString();
    }
    string LogRowDiff(DataRow src, DataRow dest)
    {
        StringBuilder builder = new StringBuilder();
        StringBuilder more = new StringBuilder();
        bool any = false;
        for (int c = 0; c < src.Table.Columns.Count; c++)
        {
            string valSrc = Helper.GetString(src[c], null);
            string valDest = Helper.GetString(dest[c], null);
            if (string.IsNullOrEmpty(valSrc) && string.IsNullOrEmpty(valDest))
                continue;
            if (string.IsNullOrEmpty(valSrc) && Helper.GetDouble(valDest, -1) == 0)
                continue;

            string colName = src.Table.Columns[c].ColumnName;
            if (colName == "SyncVer")
                continue;
            if (colName == "StoreServiceName" || colName == "ServiceName")
                builder.AppendFormat("{0}={1}\r\n", LangProvider.Instance["CommodityName"], src[c]);
            if (colName == "ID")
                builder.AppendFormat("ID={0}\r\n", src[c]);
            if (src.Table.Columns[c].DataType == dateType)
            {
                valSrc = Helper.FormatDate(Helper.GetDateTime(src[c], Null.NullDate), null);
                valDest = Helper.FormatDate(Helper.GetDateTime(dest[c], Null.NullDate), null);
            }
            if (valSrc == valDest)
                continue;
            any = true;
            string colDisplay = colName.EndsWith("TreePathName")
                    ? LangProvider.Instance[colName.Substring(0, colName.Length - 4)] : LangProvider.Instance[colName];

            if (colName.EndsWith("ID") || colName.EndsWith("TreePath"))
                more.AppendFormat("{0}: {1} --> {2}, ", colName, valSrc, valDest);
            else
                builder.AppendFormat("{0}: {1} --> {2}\r\n", colDisplay, valSrc, valDest);
        }
        if (!any)
            return "";
        if (more.Length > 0)
            builder.AppendLine(LangProvider.Instance["More"] + ":" + more.ToString());
        return builder.ToString();
    }
    string LogDeletePrepare(List<TableInfo> masterInfos, TableInfo[] tInfos, string qry, int tableID)
    {
        string tableName = null;
        #region find Table name
        if (masterInfos != null && masterInfos.Count > 0)
        {
            tableName = LangProvider.Instance[masterInfos[0].Name];
            if (tableName == masterInfos[0].Name)
                tableName = masterInfos[0].Title;
        }
        else
        {
            string table = Request.QueryString["table"];
            string masterTable = Request.QueryString["mtable"];
            if (table != null && table.ToLower() == "logchange")
                return null;
            tableName = LangProvider.Instance[string.IsNullOrEmpty(masterTable) ? table : masterTable];
        }
        #endregion
        try
        {
            using (DataSet ds = mDbProxy.FillDataSet(qry))
            {
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    DataTable dt = ds.Tables[i];
                    if (i == 0)
                        builder.AppendLine(tableName);
                    else
                    {
                        //builder.AppendLine(LangProvider.Instance[dt.TableName]);
                    }
                    foreach (DataRow row in dt.Rows)
                    {
                        builder.AppendLine(LangProvider.Instance["DeleteRecoed"]);
                        builder.AppendLine(LogRow(row));
                        if (i == 0)
                            builder.AppendLine("*********************************************************************");
                        else
                            builder.AppendLine("----------------------------------------");
                    }
                }
                return builder.ToString();
            }
        }
        catch (Exception ex)
        {
            ExceptionLog.Instance.Add(ex);
        }
        return null;
    }
    void LogDeleteCommit(List<TableInfo> masterInfos, TableInfo[] tInfos, string data, int tableID, string comments)
    {
        string tableName = null;
        #region find Table name
        if (masterInfos != null && masterInfos.Count > 0)
        {
            tableName = masterInfos[0].Name;// LangProvider.Instance[masterInfos[0].Name];
            //if (tableName == masterInfos[0].Name)
            //    tableName = masterInfos[0].Title;
        }
        else
        {
            string table = Request.QueryString["table"];
            string masterTable = Request.QueryString["mtable"];
            if (table != null && table.ToLower() == "logchange")
                return;
            tableName = string.IsNullOrEmpty(masterTable) ? table : masterTable;// LangProvider.Instance[string.IsNullOrEmpty(masterTable) ? table : masterTable];
        }
        #endregion
        mDbProxy.RunQuery(string.Format("INSERT INTO LogChange (UserID, LogDate, Type, TableName, ShortData, Data, IP, TableID) VALUES ({0}, GETDATE(), {1}, {2}, {3}, {4}, {5}, {6})"
                    , SiteSettings.UserID, (int)LogType.Delete, Helper.QoutedString(tableName)
                    , Helper.QoutedString(comments), Helper.QoutedString(data)
                    , Helper.QoutedString(GetIPAddress()), tableID), true);
    }
    void LogInsert(List<TableInfo> masterInfos, TableInfo[] tInfos, string qry, int tableID)
    {
        LogAll(masterInfos, tInfos, qry, LogType.Insert, tableID);
    }
    void LogAll(List<TableInfo> masterInfos, TableInfo[] tInfos, string qry, LogType logType, int tableID)
    {
        string tableName = null;
        if (masterInfos != null && masterInfos.Count > 0)
        {
            tableName = masterInfos[0].Name;// LangProvider.Instance[masterInfos[0].Name];
            //if (tableName == masterInfos[0].Name)
            //    tableName = masterInfos[0].Title;
        }
        else
        {
            string table = Request.QueryString["table"];
            string masterTable = Request.QueryString["mtable"];
            if (table != null && table.ToLower() == "logchange")
                return;
            tableName = string.IsNullOrEmpty(masterTable) ? table : masterTable;// LangProvider.Instance[string.IsNullOrEmpty(masterTable) ? table : masterTable];
        }
        try
        {
            using (DataSet ds = mDbProxy.FillDataSet(qry))
            {
                StringBuilder builder = new StringBuilder();
                List<string> detailRows = new List<string>();
                string noSerial = "";
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    DataTable dt = ds.Tables[i];
                    if (i == 0)
                        builder.AppendLine(tableName);
                    else
                    {
                        //builder.AppendLine(LangProvider.Instance[dt.TableName]);
                    }
                    foreach (DataRow row in dt.Rows)
                    {
                        if (logType == LogType.Delete)
                            builder.AppendLine(LangProvider.Instance["DeleteRecoed"]);
                        else
                            builder.AppendLine(LangProvider.Instance["NewRecord"]);
                        builder.AppendLine(LogRow(row));
                        if (i == 0)
                        {
                            noSerial = Helper.GetString(row, "NOSerial", "");
                            if (!string.IsNullOrEmpty(noSerial))
                                noSerial = " - " + LangProvider.Instance["ReceiptNo"] + ": " + noSerial;
                            builder.AppendLine("*********************************************************************");
                        }
                        else
                            builder.AppendLine("----------------------------------------");
                        if (i > 0)
                        {
                            if (logType == LogType.Delete)
                                detailRows.Add(LangProvider.Instance["DeleteRecoed"] + ": " + LogRowData(row) + noSerial);
                            else
                                detailRows.Add(LangProvider.Instance["NewRecord"] + ": " + LogRowData(row) + noSerial);
                        }
                    }
                }
                mDbProxy.RunQuery(string.Format("INSERT INTO LogChange (UserID, LogDate, Type, TableName, ShortData, Data, IP, TableID) VALUES ({0}, GETDATE(), {1}, {2}, {3}, {4}, {5}, {6})"
                    , SiteSettings.UserID, (int)logType, Helper.QoutedString(tableName)
                    , Helper.QoutedString(noSerial), Helper.QoutedString(builder.ToString())
                    , Helper.QoutedString(GetIPAddress()), tableID), true);
                for (int i = 0; i < detailRows.Count; i++)
                    mDbProxy.RunQuery(string.Format("INSERT INTO LogChange (UserID, LogDate, Type, TableName, ShortData, Data, IP, TableID) VALUES ({0}, GETDATE(), {1}, {2}, {3}, {4}, {5}, {6})"
                        , SiteSettings.UserID, (int)LogType.UpdateDetail, Helper.QoutedString(tableName)
                        , Helper.QoutedString(detailRows[i]), Helper.QoutedString("")
                        , Helper.QoutedString(GetIPAddress()), tableID), true);
            }
        }
        catch (Exception ex)
        {
            ExceptionLog.Instance.Add(ex);
        }
    }
    void LogUpdate(List<TableInfo> masterInfos, TableInfo[] tInfos, string qry, DataSet before, int tableID)
    {
        string tableName = null;
        if (masterInfos != null && masterInfos.Count > 0)
        {
            tableName = masterInfos[0].Name;// LangProvider.Instance[masterInfos[0].Name];
            //if (tableName == masterInfos[0].Name)
            //    tableName = masterInfos[0].Title;
        }
        else
        {
            string table = Request.QueryString["table"];
            string masterTable = Request.QueryString["mtable"];
            if (table != null && table.ToLower() == "logchange")
                return;
            tableName = string.IsNullOrEmpty(masterTable) ? table : masterTable;// LangProvider.Instance[string.IsNullOrEmpty(masterTable) ? table : masterTable];
        }
        try
        {
            List<string> detailRows = new List<string>();
            using (DataSet ds = mDbProxy.FillDataSet(qry))
            {
                StringBuilder builder = new StringBuilder();
                StringBuilder more = new StringBuilder();
                Type dateType = typeof(DateTime);
                string noSerial = "";
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    if (i == 0)
                        builder.AppendLine(tableName);
                    else
                    {
                        //builder.AppendLine(LangProvider.Instance[dt.TableName]);
                    }
                    Dictionary<int, DataRow> srcRows = new Dictionary<int, DataRow>();
                    Dictionary<int, DataRow> destRows = new Dictionary<int, DataRow>();
                    foreach (DataRow row in before.Tables[i].Rows)
                        srcRows.Add(Helper.GetInt(row["ID"], -1), row);
                    foreach (DataRow row in ds.Tables[i].Rows)
                        destRows.Add(Helper.GetInt(row["ID"], -1), row);
                    Dictionary<int, DataRow>.Enumerator enu = srcRows.GetEnumerator();
                    while (enu.MoveNext())
                        if (destRows.ContainsKey(enu.Current.Key))
                        {
                            string dif = LogRowDiff(enu.Current.Value, destRows[enu.Current.Key]);
                            if (dif.Length > 0)
                            {
                                builder.AppendLine(LangProvider.Instance["ModifiedRecord"] + ":");
                                builder.AppendLine(dif);
                                if (i == 0)
                                {
                                    noSerial = Helper.GetString(enu.Current.Value, "NOSerial", "");
                                    if (!string.IsNullOrEmpty(noSerial))
                                        noSerial = " - " + LangProvider.Instance["ReceiptNo"] + ": " + noSerial;
                                    builder.AppendLine("*********************************************************************");
                                }
                                else
                                    builder.AppendLine("----------------------------------------");
                                if (i > 0)
                                    detailRows.Add(LangProvider.Instance["ModifiedRecord"] + ": " + dif + noSerial);
                            }
                        }
                        else
                        {
                            string del = LogRow(enu.Current.Value);
                            builder.AppendLine(LangProvider.Instance["DeleteRecoed"] + ":");
                            builder.AppendLine(del);
                            if (i == 0)
                                builder.AppendLine("*********************************************************************");
                            else
                                builder.AppendLine("----------------------------------------");
                            if (i > 0)
                                detailRows.Add(LangProvider.Instance["DeleteRecoed"] + ": " + LogRowData(enu.Current.Value) + noSerial);
                        }
                    enu = destRows.GetEnumerator();
                    while (enu.MoveNext())
                        if (!srcRows.ContainsKey(enu.Current.Key))
                        {
                            string neu = LogRow(enu.Current.Value);
                            builder.AppendLine(LangProvider.Instance["NewRecord"] + ":");
                            builder.AppendLine(neu);
                            if (i == 0)
                                builder.AppendLine("*********************************************************************");
                            else
                                builder.AppendLine("----------------------------------------");
                            if (i > 0)
                                detailRows.Add(LangProvider.Instance["NewRecord"] + ": " + LogRowData(enu.Current.Value) + noSerial);
                        }
                }
                mDbProxy.RunQuery(string.Format("INSERT INTO LogChange (UserID, LogDate, Type, TableName, ShortData, Data, IP, TableID) VALUES ({0}, GETDATE(), {1}, {2}, {3}, {4}, {5}, {6})"
                    , SiteSettings.UserID, (int)LogType.Update, Helper.QoutedString(tableName), Helper.QoutedString(noSerial)
                    , Helper.QoutedString(builder.ToString())
                    , Helper.QoutedString(GetIPAddress()), tableID), true);
                for (int i = 0; i < detailRows.Count; i++)
                    mDbProxy.RunQuery(string.Format("INSERT INTO LogChange (UserID, LogDate, Type, TableName, ShortData, Data, IP, TableID) VALUES ({0}, GETDATE(), {1}, {2}, {3}, {4}, {5}, {6})"
                        , SiteSettings.UserID, (int)LogType.UpdateDetail, Helper.QoutedString(tableName), Helper.QoutedString(detailRows[i])
                        , Helper.QoutedString("")
                        , Helper.QoutedString(GetIPAddress()), tableID), true);
            }
        }
        catch (Exception ex)
        {
            ExceptionLog.Instance.Add(ex);
        }
    }
    #endregion

    #region Old logging method
    string PrepareLogQuery(string qry, List<TableInfo> tInfos, int id, string logQuery)
    {
        string table = Request.QueryString["table"];
        string masterTable = Request.QueryString["mtable"];
        if (table != null && table.ToLower() == "logchange")
            return "";
        string tableName = LangProvider.Instance[string.IsNullOrEmpty(masterTable) ? table : masterTable];

        if (!string.IsNullOrEmpty(logQuery))
        {
            logQuery = logQuery.Replace("@SCOPE_IDENTITY", id.ToString());
            logQuery = Helper.ParseQueryVars(logQuery, tInfos, Request);

            DataTable dt = mDbProxy.FillData(logQuery, "d");
            if (dt != null && dt.Rows.Count > 0)
            {
                string shortData = "";
                for (int i = 0; i < dt.Columns.Count; i++)
                    shortData += Helper.GetString(dt.Rows[0][i], "") + " ";
                shortData = shortData.Trim();
                return shortData;
            }
        }
        return "";
    }
    void LogQuery(string qry, List<TableInfo> tInfos, int id, string logQuery, string beforeData)
    {
        string table = Request.QueryString["table"];
        string masterTable = Request.QueryString["mtable"];
        if (table != null && table.ToLower() == "logchange")
            return;
        string tableName = LangProvider.Instance[string.IsNullOrEmpty(masterTable) ? table : masterTable];

        if (!string.IsNullOrEmpty(logQuery))
        {
            logQuery = logQuery.Replace("@SCOPE_IDENTITY", id.ToString());
            logQuery = Helper.ParseQueryVars(logQuery, tInfos, Request);

            DataTable dt = mDbProxy.FillData(logQuery, "d");
            if (dt != null && dt.Rows.Count > 0)
            {
                string shortData = "";
                for (int i = 0; i < dt.Columns.Count; i++)
                    shortData += Helper.GetString(dt.Rows[0][i], "") + " ";
                shortData = shortData.Trim();
                if (!string.IsNullOrEmpty(beforeData))
                    shortData = LangProvider.Instance["Before"] + ":\r\n" + beforeData + "\r\n" + LangProvider.Instance["After"] + ":\r\n" + shortData;
                LogType logType = DetectLogType(qry);
                mDbProxy.RunQuery(string.Format("INSERT INTO LogChange (UserID, LogDate, Type, TableName, ShortData, Data) VALUES ({0}, GETDATE(), {1}, {2}, {3}, {4})"
                    , SiteSettings.UserID, (int)logType, Helper.QoutedString(tableName), Helper.QoutedString(shortData), Helper.QoutedString(qry)), true);
                return;
            }
        }

        LogBareQuery(qry);
    }
    void LogBareQuery(string qry)
    {
        string table = Request.QueryString["table"];
        string masterTable = Request.QueryString["mtable"];
        if (table != null && table.ToLower() == "logchange")
            return;
        string tableName = LangProvider.Instance[string.IsNullOrEmpty(masterTable) ? table : masterTable];

        LogType logType = DetectLogType(qry);
        mDbProxy.RunQuery(string.Format("INSERT INTO LogChange (UserID, LogDate, Type, TableName, ShortData, Data) VALUES ({0}, GETDATE(), {1}, {2}, '', {3})"
            , SiteSettings.UserID, (int)logType, Helper.QoutedString(tableName), Helper.QoutedString(qry)), true);
    }
    LogType DetectLogType(string qry)
    {
        string op = Helper.GetString(Request.QueryString["op"], "RunQuery").ToLower();
        if (op == "Delete".ToLower())
            return LogType.Delete;
        if (op == "MultiDelete".ToLower())
            return LogType.MultiDelete;
        if (string.IsNullOrEmpty(qry))
            return LogType.Unknown;
        qry = qry.ToLower();
        int update = qry.IndexOf("update ");
        int insert = qry.LastIndexOf("insert ");
        if (insert >= 0 && insert > update)
            return LogType.Insert;
        else if (update >= 0)
            return LogType.Update;
        return LogType.Unknown;
    }
    #endregion

    // on error --> return Error: .....
    private void RunQuery(bool scalar)
    {
        string table = Request.QueryString["table"];
        string q = Request.QueryString["qd"];
        string extra = Request.QueryString["extra"]; // extra query 

        List<TableInfo> tInfos = null;
        if (!string.IsNullOrEmpty(table) && table.Contains(","))
        {
            tInfos = new List<TableInfo>();
            string[] tables = table.Split(',');
            for (int i = 0; i < tables.Length; i++)
            {
                TableInfo tInfo = TableController.Instance[tables[i]];
                if (tInfo != null)
                    tInfos.Add(tInfo);
            }
        }
        else
        {
            TableInfo tInfo = TableController.Instance[table];
            if (tInfo != null)
            {
                tInfos = new List<TableInfo>();
                tInfos.Add(tInfo);
            }
        }

        if (SiteSettings.IsDbReadonly)
        {
            if (tInfos == null || tInfos.Count == 0 || tInfos[0].ReadOnly)
            {
                Response.Write(LangProvider.Instance["SetAspxM1"]);
                return;
            }
        }
        string qry = null;
        QueryInfo qInfo = null;
        if (!string.IsNullOrEmpty(q))
        {
            qInfo = QueryController.Instance[q];
            if (qInfo != null)
                if (!string.IsNullOrEmpty(qInfo.Query))
                    qry = qInfo.Query;
        }
        if (string.IsNullOrEmpty(qry))
        {
            Response.Write("Error: " + "NO QUERY");
            return;
        }

        qry = qry.Replace("@Table", table);
        try
        {
            qry = Helper.ParseQueryVars(qry, tInfos, Request);
        }
        catch (Exception ex)
        {
            Response.Write("Error: " + ex.Message);
            Response.End();
        }
        #region old
        /*
        try
        {
            string beforeData = null;
            if (qInfo != null && qInfo.BeforeQuery != null)
                beforeData = PrepareLogQuery(qry, tInfos, -1, qInfo.BeforeQuery);
            mDbProxy.BeginTransaction();
            if (scalar)
            {
                object obj = mDbProxy.FillScalar(qry);
                if (obj == null || obj is DBNull)
                {
                    mDbProxy.RollbackTransaction();
                    if (mDbProxy.LastException != null)
                    {
                        ExceptionLog.Instance.Add(mDbProxy.LastException, qry);
                        Response.Write("Error: " + mDbProxy.LastException.Message);// + " Query: " + qry);
                    }
                    else
                        Response.Write("Error: Failed");
                }
                else
                {
                    string extraQry = null;
                    if (!string.IsNullOrEmpty(extra))
                    {
                        QueryInfo eqInfo = QueryController.Instance[extra];
                        if (eqInfo != null)
                            if (!string.IsNullOrEmpty(eqInfo.Query))
                                extraQry = eqInfo.Query;
                    }
                    if (!string.IsNullOrEmpty(extraQry))
                    {
                        extraQry = extraQry.Replace("@ID", obj.ToString());
                        extraQry = Helper.ParseQueryVars(extraQry, tInfos, Request);
                        if (mDbProxy.RunQuery(extraQry, true) <= 0)
                        {
                            if (mDbProxy.LastException != null)
                                throw mDbProxy.LastException;
                        }
                    }

                    mDbProxy.CommitTransaction();
                    LogQuery(qry, tInfos, Helper.GetInt(obj, -1), qInfo.AfterQuery, beforeData);
                    if (qInfo != null)
                        qInfo.RunTrigger(Helper.GetInt(obj, -1));
                    Response.Write(obj.ToString());
                }
            }
            else
            {
                if (mDbProxy.RunQuery(qry, true) >= 0)
                {
                    mDbProxy.CommitTransaction();
                    LogQuery(qry, tInfos, -1, qInfo.AfterQuery, beforeData);
                    Response.Write("OK");
                }
                else
                {
                    mDbProxy.RollbackTransaction();
                    if (mDbProxy.LastException != null)
                    {
                        ExceptionLog.Instance.Add(mDbProxy.LastException, qry);
                        Response.Write("Error: " + Helper.GetError(mDbProxy.LastException.Message, tInfos != null && tInfos.Count > 0 ? tInfos[0] : null));
                    }
                    else
                        Response.Write("Error: Failed");
                }
            }
        }
        catch (Exception ex)
        {
            Response.Write("Error: " + Helper.GetError(ex.Message, tInfos != null && tInfos.Count > 0 ? tInfos[0] : null));
        }
        */
        #endregion

        try
        {
            string beforeData = null;
            bool done = true;
            int retID = -1;
            if (qInfo != null && qInfo.BeforeQuery != null)
                beforeData = PrepareLogQuery(qry, tInfos, -1, qInfo.BeforeQuery);
            int iteration = 0;
            while (true)
            {
                mDbProxy.LastException = null;
                mDbProxy.BeginTransaction();
                if (scalar)
                {
                    object obj = mDbProxy.FillScalar(qry);
                    if (obj == null || obj is DBNull)
                        done = false;
                    else
                    {
                        retID = Helper.GetInt(obj, -1);
                        string extraQry = null;
                        if (!string.IsNullOrEmpty(extra))
                        {
                            QueryInfo eqInfo = QueryController.Instance[extra];
                            if (eqInfo != null)
                                if (!string.IsNullOrEmpty(eqInfo.Query))
                                    extraQry = eqInfo.Query;
                        }
                        if (!string.IsNullOrEmpty(extraQry))
                        {
                            extraQry = extraQry.Replace("@ID", obj.ToString());
                            extraQry = Helper.ParseQueryVars(extraQry, tInfos, Request);
                            if (mDbProxy.RunQuery(extraQry, true) <= 0)
                                if (mDbProxy.LastException != null)
                                    done = false;
                        }
                    }
                }
                else
                {
                    if (mDbProxy.RunQuery(qry, true) < 0)
                        done = false;
                }

                if (!done)
                {
                    mDbProxy.RollbackTransaction();
                    if (mDbProxy.LastException != null)
                    {
                        SqlException sqlEx = mDbProxy.LastException as SqlException;
                        // 1205 SQL_DEADLOCK_ERROR_CODE
                        // -2   SQL_TIMEOUT_ERROR_CODE
                        if (sqlEx != null && sqlEx.Number == 1205 && iteration < 2)
                        {
                            // retry
                            iteration++;
                        }
                        else
                        {
                            ExceptionLog.Instance.Add(mDbProxy.LastException, qry);
                            Response.Write("Error: " + Helper.GetError(mDbProxy.LastException.Message, tInfos != null && tInfos.Count > 0 ? tInfos[0] : null));
                            break;
                        }
                    }
                    else
                    {
                        Response.Write("Error: Failed");
                        break;
                    }
                }
                else
                {
                    mDbProxy.CommitTransaction();
                    LogQuery(qry, tInfos, retID, qInfo.AfterQuery, beforeData);
                    if (qInfo != null && retID > 0)
                        qInfo.RunTrigger(retID);
                    if (retID > 0)
                        Response.Write(retID);
                    else
                        Response.Write("OK");
                    break;
                }
            }
        }
        catch (Exception ex)
        {
            Response.Write("Error: " + Helper.GetError(ex.Message, tInfos != null && tInfos.Count > 0 ? tInfos[0] : null));
        }
        Response.End();
    }
    private void RunMulti()
    {
        string table = Request.QueryString["table"];
        string uq = Request.QueryString["uqd"]; // update query
        string iq = Request.QueryString["iqd"]; // insert query

        TableInfo tInfo = TableController.Instance[table];
        if (SiteSettings.IsDbReadonly)
        {
            if (tInfo == null || tInfo.ReadOnly)
            {
                Response.Write(LangProvider.Instance["SetAspxM1"]);
                return;
            }
        }
        string uqry = null; // update query
        if (!string.IsNullOrEmpty(uq))
        {
            QueryInfo qInfo = QueryController.Instance[uq];
            if (qInfo != null)
                if (!string.IsNullOrEmpty(qInfo.Query))
                    uqry = qInfo.Query;
        }
        string iqry = null; // update query
        if (!string.IsNullOrEmpty(iq))
        {
            QueryInfo qInfo = QueryController.Instance[iq];
            if (qInfo != null)
                if (!string.IsNullOrEmpty(qInfo.Query))
                    iqry = qInfo.Query;
        }
        if (string.IsNullOrEmpty(uqry))
        {
            Response.Write("NO Update QUERY");
            return;
        }
        if (string.IsNullOrEmpty(iqry))
        {
            Response.Write("NO Insert QUERY");
            return;
        }
        if (string.IsNullOrEmpty(Request.Form["ID"]))
        {
            Response.Write("NO IDs");
            return;
        }
        uqry = uqry.Replace("@Table", table);
        iqry = iqry.Replace("@Table", table);
        string[] ids = Request.Form["ID"].Split(',');
        int count = 0;//, uCount = 0;
        string res = "";
        mDbProxy.BeginTransaction();
        for (int i = 0; i < ids.Length; i++)
        {
            int id = Helper.GetInt(ids[i], -1);
            string qry = id == -1 ? iqry : uqry;
            //int count = id == -1 ? iCount++ : uCount++;
            int index = -1;
            while ((index = qry.IndexOf('@')) >= 0)
            {
                int last = qry.IndexOfAny(new char[] { ' ', '"', '\'', ',', '(', ')', '\r', '\n' }, index + 1);
                string field = last > 0 ? qry.Substring(index + 1, last - index - 1) : qry.Substring(index + 1);
                string val = Helper.GetString(Request.Form[field], null);
                if (val != null)
                {
                    if (val.Contains(",")) // multi value
                        val = val.Split(',')[count];
                }
                else if (tInfo != null && tInfo.GetField(field) != null && tInfo.GetField(field).Type == "bit")
                {
                    val = Helper.GetString(Request.Form[field + i], null);
                }
                if (tInfo != null)
                    val = tInfo.PrepareValue(field, val);
                qry = qry.Replace("@" + field, val);
            }
            count++;
            try
            {
                if (mDbProxy.RunQuery(qry, true) > 0)
                    res += "OK,";
                else
                {
                    res += "Error: " + mDbProxy.LastException.Message + " Query: " + qry;
                    mDbProxy.RollbackTransaction();
                    break;
                }
            }
            catch (Exception ex)
            {
                res += "Error: " + ex.Message + " Query: " + qry;
                mDbProxy.RollbackTransaction();
                break;
            }
        }
        mDbProxy.CommitTransaction();
        Response.Write(res);
        Response.End();
    }

    private void RunMasterDetail()
    {
        string masterTable = Request.QueryString["mtable"];
        string masterQ = Request.QueryString["mq"]; // master query 
        string table = Request.QueryString["table"];
        string extra = Request.QueryString["extra"]; // extra query 
        string uq = Request.QueryString["uqd"]; // update query
        string iq = Request.QueryString["iqd"]; // insert query
        string dq = Request.QueryString["dqd"]; // delete query
        string required = Request.QueryString["req"]; // required field

        List<TableInfo> masterInfos = null;
        if (!string.IsNullOrEmpty(masterTable) && masterTable.Contains(","))
        {
            masterInfos = new List<TableInfo>();
            string[] tables = masterTable.Split(',');
            for (int i = 0; i < tables.Length; i++)
            {
                TableInfo tableInfo = TableController.Instance[tables[i]];
                if (tableInfo != null)
                    masterInfos.Add(tableInfo);
            }
        }
        else
        {
            TableInfo tableInfo = TableController.Instance[masterTable];
            if (tableInfo != null)
            {
                masterInfos = new List<TableInfo>();
                masterInfos.Add(tableInfo);
            }
        }
        if (SiteSettings.IsDbReadonly)
        {
            if (masterInfos == null || masterInfos.Count == 0 || masterInfos[0].ReadOnly)
            {
                Response.Write(LangProvider.Instance["SetAspxM1"]);
                return;
            }
        }
        TableInfo tInfo = TableController.Instance[table];

        string masterQry = null, uqry = null, iqry = null, dqry = null;
        string insertLog = null, updateLog = null;

        #region find queries
        QueryInfo qInfo = null;
        if (!string.IsNullOrEmpty(masterQ))
        {
            qInfo = QueryController.Instance[masterQ];
            if (qInfo != null)
                if (!string.IsNullOrEmpty(qInfo.Query))
                {
                    masterQry = qInfo.Query;
                    insertLog = qInfo.InsertLog;
                    updateLog = qInfo.UpdateLog;
                }
        }
        if (string.IsNullOrEmpty(masterQry))
        {
            Response.Write("NO QUERY");
            return;
        }
        if (!string.IsNullOrEmpty(uq))
        {
            QueryInfo uqInfo = QueryController.Instance[uq];
            if (uqInfo != null)
                if (!string.IsNullOrEmpty(uqInfo.Query))
                    uqry = uqInfo.Query;
        }
        if (string.IsNullOrEmpty(uqry))
        {
            Response.Write("NO Update QUERY");
            return;
        }
        if (!string.IsNullOrEmpty(iq))
        {
            QueryInfo iqInfo = QueryController.Instance[iq];
            if (iqInfo != null)
                if (!string.IsNullOrEmpty(iqInfo.Query))
                    iqry = iqInfo.Query;
        }
        if (string.IsNullOrEmpty(iqry))
        {
            Response.Write("NO Insert QUERY");
            return;
        }
        if (!string.IsNullOrEmpty(dq))
        {
            QueryInfo dqInfo = QueryController.Instance[dq];
            if (dqInfo != null)
                if (!string.IsNullOrEmpty(dqInfo.Query))
                    dqry = dqInfo.Query;
        }
        if (string.IsNullOrEmpty(dqry))
        {
            Response.Write("NO Delete QUERY");
            return;
        }
        #endregion

        masterQry = masterQry.Replace("@Table", masterTable);
        masterQry = Helper.ParseQueryVars(masterQry, masterInfos, Request);

        // logging: keep the logging outside of transaction
        DataSet before = null;
        if (!string.IsNullOrEmpty(updateLog))
            before = mDbProxy.FillDataSet(updateLog.Replace("@MasterID", Helper.GetString(Request.Form["MasterID"], null)));
        string beforeData = null;
        if (qInfo != null && qInfo.BeforeQuery != null)
            beforeData = PrepareLogQuery(masterQry, masterInfos, -1, qInfo.BeforeQuery);

        mDbProxy.BeginTransaction();
        int masterID = -1;
        try
        {
            // Important: the MasterQuery should return ID
            masterID = Helper.GetInt(mDbProxy.FillScalar(masterQry), -1);
            if (masterID == -1)
            {
                if (mDbProxy.LastException == null)
                    throw new Exception("Error: Master Query '" + masterQ + "' should return an ID");
                throw new Exception("Error: " + mDbProxy.LastException.Message);// + " Master Query: " + masterQry);
            }

            // ----------------------------------------------
            // ----------------------------------------------
            if (Request.Form["ID"] == null)// (string.IsNullOrEmpty(Request.Form["ID"]))
            {
                mDbProxy.CommitTransaction();
                if (!string.IsNullOrEmpty(insertLog))
                    LogInsert(masterInfos, new TableInfo[] { tInfo }, insertLog.Replace("@MasterID", masterID.ToString()), masterID);
                else if (!string.IsNullOrEmpty(updateLog) && before != null)
                {
                    LogUpdate(masterInfos, new TableInfo[] { tInfo }, updateLog.Replace("@MasterID", masterID.ToString()), before, masterID);
                    before.Dispose();
                }
                else
                    LogQuery(masterQry, masterInfos, masterID, qInfo.AfterQuery, beforeData);
                Response.Write(masterID.ToString());
                // No IDs

                // Response.Write("NO IDs");
                return;
            }
            uqry = uqry.Replace("@Table", table).Replace("@MasterID", masterID.ToString()).Replace("@_ID_", "@ID");
            iqry = iqry.Replace("@Table", table).Replace("@MasterID", masterID.ToString());
            dqry = dqry.Replace("@Table", table).Replace("@MasterID", masterID.ToString()).Replace("@_ID_", "@ID");

            string[] ids = Request.Form["ID"].Split(',');
            int numItems = 0;
            string res = masterID.ToString();
            string detailError = SaveDetail(ids, iqry, uqry, dqry, required, tInfo, out numItems);

            if (detailError.Contains("Error:"))
                throw new Exception(detailError);
            //if (numItems <= 0)
            //    throw new Exception(LangProvider.Instance["SetAspxM2"]);

            string extraQry = null;
            if (!string.IsNullOrEmpty(extra))
            {
                QueryInfo eqInfo = QueryController.Instance[extra];
                if (eqInfo != null)
                    if (!string.IsNullOrEmpty(eqInfo.Query))
                        extraQry = eqInfo.Query;
            }
            if (!string.IsNullOrEmpty(extraQry))
            {
                extraQry = extraQry.Replace("@MasterID", masterID.ToString());
                extraQry = Helper.ParseQueryVars(extraQry, tInfo, Request);
                if (mDbProxy.RunQuery(extraQry, true) > 0)
                {
                    //res += "Extra OK";
                }
                else
                {
                    if (mDbProxy.LastException != null)
                        throw mDbProxy.LastException;
                    //else
                    //    throw new Exception("Extra Failed");
                }
            }

            mDbProxy.CommitTransaction();

            if (qInfo != null)
                qInfo.RunTrigger(masterID);

            if (!string.IsNullOrEmpty(insertLog))
                LogInsert(masterInfos, new TableInfo[] { tInfo }, insertLog.Replace("@MasterID", masterID.ToString()), masterID);
            else if (!string.IsNullOrEmpty(updateLog) && before != null)
            {
                LogUpdate(masterInfos, new TableInfo[] { tInfo }, updateLog.Replace("@MasterID", masterID.ToString()), before, masterID);
                before.Dispose();
            }
            else
                LogQuery(masterQry, masterInfos, masterID, qInfo.AfterQuery, beforeData);

            Response.Write(res);
        }
        catch (Exception ex)
        {
            Response.Write("Error: " + ex.Message.Replace("\n", "<br />"));//+ " Master Query: " + masterQry
            mDbProxy.RollbackTransaction();
        }
        finally
        {
        }

        Response.End();
    }
    private void RunMasterDetail2()
    {
        // master
        string masterTable = Request.QueryString["mtable"];
        string masterQ = Request.QueryString["mq"]; // master query 

        // detail 1
        string table1 = Request.QueryString["table1"];
        string uq1 = Request.QueryString["uqd1"]; // update query
        string iq1 = Request.QueryString["iqd1"]; // insert query
        string dq1 = Request.QueryString["dqd1"]; // delete query
        string required1 = Request.QueryString["req1"]; // required field

        // detail 2
        string table2 = Request.QueryString["table2"];
        string uq2 = Request.QueryString["uqd2"]; // update2 query
        string iq2 = Request.QueryString["iqd2"]; // insert2 query
        string dq2 = Request.QueryString["dqd2"]; // delete2 query
        string required2 = Request.QueryString["req2"]; // required field

        TableInfo masterInfo = TableController.Instance[masterTable];
        if (SiteSettings.IsDbReadonly)
        {
            if (masterInfo == null || masterInfo.ReadOnly)
            {
                Response.Write(LangProvider.Instance["SetAspxM1"]);
                return;
            }
        }
        TableInfo tInfo1 = TableController.Instance[table1];
        TableInfo tInfo2 = TableController.Instance[table2];

        string masterQry = null, uqry1 = null, iqry1 = null, dqry1 = null;
        string uqry2 = null, iqry2 = null, dqry2 = null;
        #region find queries
        // Master
        if (!string.IsNullOrEmpty(masterQ))
        {
            QueryInfo qInfo = QueryController.Instance[masterQ];
            if (qInfo != null)
                if (!string.IsNullOrEmpty(qInfo.Query))
                    masterQry = qInfo.Query;
        }
        if (string.IsNullOrEmpty(masterQry))
        {
            Response.Write("NO QUERY");
            return;
        }
        // Detail 1
        if (!string.IsNullOrEmpty(uq1))
        {
            QueryInfo qInfo = QueryController.Instance[uq1];
            if (qInfo != null)
                if (!string.IsNullOrEmpty(qInfo.Query))
                    uqry1 = qInfo.Query;
        }
        if (string.IsNullOrEmpty(uqry1))
        {
            Response.Write("NO Update QUERY 1");
            return;
        }
        if (!string.IsNullOrEmpty(iq1))
        {
            QueryInfo qInfo = QueryController.Instance[iq1];
            if (qInfo != null)
                if (!string.IsNullOrEmpty(qInfo.Query))
                    iqry1 = qInfo.Query;
        }
        if (string.IsNullOrEmpty(iqry1))
        {
            Response.Write("NO Insert QUERY 1");
            return;
        }
        if (!string.IsNullOrEmpty(dq1))
        {
            QueryInfo qInfo = QueryController.Instance[dq1];
            if (qInfo != null)
                if (!string.IsNullOrEmpty(qInfo.Query))
                    dqry1 = qInfo.Query;
        }
        if (string.IsNullOrEmpty(dqry1))
        {
            Response.Write("NO Delete QUERY 1");
            return;
        }
        // Detail 2
        if (!string.IsNullOrEmpty(uq2))
        {
            QueryInfo qInfo = QueryController.Instance[uq2];
            if (qInfo != null)
                if (!string.IsNullOrEmpty(qInfo.Query))
                    uqry2 = qInfo.Query;
        }
        if (string.IsNullOrEmpty(uqry2))
        {
            Response.Write("NO Update QUERY 2");
            return;
        }
        if (!string.IsNullOrEmpty(iq2))
        {
            QueryInfo qInfo = QueryController.Instance[iq2];
            if (qInfo != null)
                if (!string.IsNullOrEmpty(qInfo.Query))
                    iqry2 = qInfo.Query;
        }
        if (string.IsNullOrEmpty(iqry2))
        {
            Response.Write("NO Insert QUERY 2");
            return;
        }
        if (!string.IsNullOrEmpty(dq2))
        {
            QueryInfo qInfo = QueryController.Instance[dq2];
            if (qInfo != null)
                if (!string.IsNullOrEmpty(qInfo.Query))
                    dqry2 = qInfo.Query;
        }
        if (string.IsNullOrEmpty(dqry2))
        {
            Response.Write("NO Delete QUERY 2");
            return;
        }
        #endregion

        masterQry = masterQry.Replace("@Table", masterTable);
        mDbProxy.BeginTransaction();
        masterQry = Helper.ParseQueryVars(masterQry, masterInfo, Request);
        int masterID = -1;
        try
        {
            // Important: the MasterQuery should return ID
            masterID = Helper.GetInt(mDbProxy.FillScalar(masterQry), -1);
            if (masterID == -1)
                throw new Exception("Error: " + mDbProxy.LastException.Message);// + " Masters Query: " + masterQry);

            // ----------------------------------------------
            // ----------------------------------------------
            if (string.IsNullOrEmpty(Request.Form["ID"]) && string.IsNullOrEmpty(Request.Form["ID2"]))
            {
                mDbProxy.CommitTransaction();
                // No IDs

                // Response.Write("NO IDs");
                return;
            }
            uqry1 = uqry1.Replace("@Table", table1).Replace("@MasterID", masterID.ToString()).Replace("@_ID_", "@ID");
            iqry1 = iqry1.Replace("@Table", table1).Replace("@MasterID", masterID.ToString());
            dqry1 = dqry1.Replace("@Table", table1).Replace("@MasterID", masterID.ToString()).Replace("@_ID_", "@ID");
            uqry2 = uqry2.Replace("@Table", table2).Replace("@MasterID", masterID.ToString()).Replace("@_ID_", "@ID2");
            iqry2 = iqry2.Replace("@Table", table2).Replace("@MasterID", masterID.ToString());
            dqry2 = dqry2.Replace("@Table", table2).Replace("@MasterID", masterID.ToString()).Replace("@_ID_", "@ID2");

            int numItems1 = 0, numItems2 = 0;
            string res = masterID.ToString();
            if (!string.IsNullOrEmpty(Request.Form["ID"]))
            {
                string[] ids1 = Request.Form["ID"].Split(',');
                res += SaveDetail(ids1, iqry1, uqry1, dqry1, required1, tInfo1, out numItems1);
            }
            if (!string.IsNullOrEmpty(Request.Form["ID2"]))
            {
                string[] ids2 = Request.Form["ID2"].Split(',');
                res += SaveDetail(ids2, iqry2, uqry2, dqry2, required2, tInfo2, out numItems2);
            }
            //if (numItems1 <= 0 && numItems2 <= 0)
            //    throw new Exception("لیست آیتم ها نباید خالی باشد");

            mDbProxy.CommitTransaction();
            Response.Write(res);
        }
        catch (Exception ex)
        {
            Response.Write("Error: " + ex.Message);//+ " Master Query: " + masterQry
            mDbProxy.RollbackTransaction();
        }
        finally
        {
        }

        Response.End();
    }
    private void RunMastersDetail()
    {
        string masterTable = Request.QueryString["mtable"];
        string masterQ = Request.QueryString["mq"]; // master query 
        string insertLog = null, updateLog = null;
        string extra = Request.QueryString["extra"]; // extra query 
        string[] tables = Request.QueryString["tables"].Split(','); // detail fields
        string[] uqs = Request.QueryString["uqds"].Split(','); // update queries
        string[] iqs = Request.QueryString["iqds"].Split(','); // insert queries
        string[] dqs = Request.QueryString["dqds"].Split(','); // delete queries
        string[] requireds = Request.QueryString["reqs"].Split(','); // required fields


        //TableInfo masterInfo = TableController.Instance[masterTable];
        List<TableInfo> masterInfos = null;
        if (!string.IsNullOrEmpty(masterTable) && masterTable.Contains(","))
        {
            masterInfos = new List<TableInfo>();
            string[] masterTables = masterTable.Split(',');
            for (int i = 0; i < masterTables.Length; i++)
            {
                TableInfo tInfo = TableController.Instance[masterTables[i]];
                if (tInfo != null)
                    masterInfos.Add(tInfo);
            }
        }
        else
        {
            TableInfo masterInfo = TableController.Instance[masterTable];
            if (masterInfo != null)
            {
                masterInfos = new List<TableInfo>();
                masterInfos.Add(masterInfo);
            }
        }
        if (SiteSettings.IsDbReadonly)
        {
            if (masterInfos == null || masterInfos.Count == 0 || masterInfos[0].ReadOnly)
            {
                Response.Write(LangProvider.Instance["SetAspxM1"]);
                return;
            }
        }
        TableInfo[] tInfos = new TableInfo[tables.Length];
        for (int i = 0; i < tables.Length; i++)
            tInfos[i] = TableController.Instance[tables[i]];


        string masterQry = null;
        string[] uqrys = null, iqrys = null, dqrys = null;

        #region find queries
        QueryInfo qInfo = null;
        if (!string.IsNullOrEmpty(masterQ))
        {
            qInfo = QueryController.Instance[masterQ];
            if (qInfo != null)
                if (!string.IsNullOrEmpty(qInfo.Query))
                {
                    masterQry = qInfo.Query;
                    insertLog = qInfo.InsertLog;
                    updateLog = qInfo.UpdateLog;
                }
        }
        if (string.IsNullOrEmpty(masterQry))
        {
            Response.Write("NO Master QUERY");
            return;
        }
        try
        {
            uqrys = LoadQueries(uqs);
            iqrys = LoadQueries(iqs);
            dqrys = LoadQueries(dqs);
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
            return;
        }
        #endregion

        masterQry = masterQry.Replace("@Table", masterTable);
        masterQry = Helper.ParseQueryVars(masterQry, masterInfos, Request);

        // logging: keep the logging outside of transaction
        DataSet before = null;
        if (!string.IsNullOrEmpty(updateLog))
            before = mDbProxy.FillDataSet(updateLog.Replace("@MasterID", Helper.GetString(Request.Form["MasterID"], null)));
        string beforeData = null;
        if (qInfo != null && qInfo.BeforeQuery != null)
            beforeData = PrepareLogQuery(masterQry, masterInfos, -1, qInfo.BeforeQuery);

        mDbProxy.BeginTransaction();
        int masterID = -1;
        try
        {
            // Important: the MasterQuery should return ID
            masterID = Helper.GetInt(mDbProxy.FillScalar(masterQry), -1);
            if (masterID == -1)
                throw new Exception("Error: " + mDbProxy.LastException.Message);// + " Master Query: " + masterQry);

            // ----------------------------------------------
            // Run detail queries
            int numItems = 0;
            string res = masterID.ToString();
            for (int i = 0; i < tables.Length; i++)
            {
                string idName = i == 0 ? "ID" : ("ID" + (i + 1)); // ID, ID2, ID3, ID4, ID5, ..., IDn
                uqrys[i] = uqrys[i].Replace("@Table", tables[i]).Replace("@MasterID", masterID.ToString()).Replace("@_ID_", "@" + idName);
                iqrys[i] = iqrys[i].Replace("@Table", tables[i]).Replace("@MasterID", masterID.ToString());
                dqrys[i] = dqrys[i].Replace("@Table", tables[i]).Replace("@MasterID", masterID.ToString()).Replace("@_ID_", "@" + idName);

                int numItemsi = 0;
                if (Request.Form[idName] != null)
                //if (!string.IsNullOrEmpty(Request.Form[idName]))
                {
                    string[] ids1 = Request.Form[idName].Split(',');
                    res += SaveDetail(ids1, iqrys[i], uqrys[i], dqrys[i], requireds[i], tInfos[i], out numItemsi);
                    numItems += numItemsi;
                }
            }
            //if (numItems <= 0)
            //    throw new Exception("لیست آیتم ها نباید خالی باشد");


            string extraQry = null;
            if (!string.IsNullOrEmpty(extra))
            {
                QueryInfo eqInfo = QueryController.Instance[extra];
                if (eqInfo != null)
                    if (!string.IsNullOrEmpty(eqInfo.Query))
                        extraQry = eqInfo.Query;
            }
            if (!string.IsNullOrEmpty(extraQry))
            {
                extraQry = extraQry.Replace("@MasterID", masterID.ToString());
                extraQry = Helper.ParseQueryVars(extraQry, new List<TableInfo>(tInfos), Request);
                if (mDbProxy.RunQuery(extraQry, true) > 0)
                    res += "Extra OK";
                else
                {
                    if (mDbProxy.LastException != null)
                        throw mDbProxy.LastException;
                    //else
                    //    throw new Exception("Extra Failed");
                }
            }
            mDbProxy.CommitTransaction();
            if (!string.IsNullOrEmpty(insertLog))
                LogInsert(masterInfos, tInfos, insertLog.Replace("@MasterID", masterID.ToString()), masterID);
            else if (!string.IsNullOrEmpty(updateLog) && before != null)
            {
                LogUpdate(masterInfos, tInfos, updateLog.Replace("@MasterID", masterID.ToString()), before, masterID);
                before.Dispose();
            }
            else
                LogQuery(masterQry, masterInfos, masterID, qInfo.AfterQuery, beforeData);
            Response.Write(res);
        }
        catch (Exception ex)
        {
            Response.Write("Error: " + ex.Message.Replace("\n", "<br />"));//+ " Master Query: " + masterQry
            mDbProxy.RollbackTransaction();
        }
        finally
        {
        }

        Response.End();
    }

    private void RunDetails()
    {
        string masterTable = Request.QueryString["mtable"];
        string masterQ = Request.QueryString["mq"]; // master query 
        string insertLog = null, updateLog = null;
        string extra = Request.QueryString["extra"]; // extra query 
        string[] tables = Request.QueryString["tables"].Split(','); // detail fields
        string[] uqs = Request.QueryString["uqds"].Split(','); // update queries
        string[] iqs = Request.QueryString["iqds"].Split(','); // insert queries
        string[] dqs = Request.QueryString["dqds"].Split(','); // delete queries
        string[] requireds = Request.QueryString["reqs"].Split(','); // required fields



        TableInfo[] tInfos = new TableInfo[tables.Length];
        for (int i = 0; i < tables.Length; i++)
            tInfos[i] = TableController.Instance[tables[i]];
        if (SiteSettings.IsDbReadonly)
        {
            if (tInfos == null || tInfos.Length == 0 || tInfos[0].ReadOnly)
            {
                Response.Write(LangProvider.Instance["SetAspxM1"]);
                return;
            }
        }

        string[] uqrys = null, iqrys = null, dqrys = null;

        #region find queries
        try
        {
            uqrys = LoadQueries(uqs);
            iqrys = LoadQueries(iqs);
            dqrys = LoadQueries(dqs);
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
            return;
        }
        #endregion



        try
        {

            // ----------------------------------------------
            // Run detail queries
            int numItems = 0;
            string res = "";
            for (int i = 0; i < tables.Length; i++)
            {
                string idName = i == 0 ? "ID" : ("ID" + (i + 1)); // ID, ID2, ID3, ID4, ID5, ..., IDn
                uqrys[i] = uqrys[i].Replace("@Table", tables[i]).Replace("@_ID_", "@" + idName);
                uqrys[i] = SiteSettings.ReplaceOptions(uqrys[i]);
                iqrys[i] = iqrys[i].Replace("@Table", tables[i]);
                iqrys[i] = SiteSettings.ReplaceOptions(iqrys[i]);
                dqrys[i] = dqrys[i].Replace("@Table", tables[i]).Replace("@_ID_", "@" + idName);
                iqrys[i] = SiteSettings.ReplaceOptions(iqrys[i]);

                int numItemsi = 0;
                if (Request.Form[idName] != null)
                //if (!string.IsNullOrEmpty(Request.Form[idName]))
                {
                    string[] ids1 = Request.Form[idName].Split(',');
                    res += SaveDetail(ids1, iqrys[i], uqrys[i], dqrys[i], requireds[i], tInfos[i], out numItemsi);
                    numItems += numItemsi;
                }
            }
            //if (numItems <= 0)
            //    throw new Exception("لیست آیتم ها نباید خالی باشد");


            string extraQry = null;
            if (!string.IsNullOrEmpty(extra))
            {
                QueryInfo eqInfo = QueryController.Instance[extra];
                if (eqInfo != null)
                    if (!string.IsNullOrEmpty(eqInfo.Query))
                        extraQry = eqInfo.Query;
            }
            if (!string.IsNullOrEmpty(extraQry))
            {
                extraQry = Helper.ParseQueryVars(extraQry, new List<TableInfo>(tInfos), Request);
                if (mDbProxy.RunQuery(extraQry, true) > 0)
                    res += "Extra OK";
                else
                {
                    if (mDbProxy.LastException != null)
                        throw mDbProxy.LastException;
                    //else
                    //    throw new Exception("Extra Failed");
                }
            }
            mDbProxy.CommitTransaction();
            Response.Write(res);
        }
        catch (Exception ex)
        {
            Response.Write("Error: " + ex.Message.Replace("\n", "<br />"));//+ " Master Query: " + masterQry
            mDbProxy.RollbackTransaction();
        }
        finally
        {
        }

        Response.End();
    }
    private string SaveDetail(string[] ids, string iqry, string uqry, string dqry, string required, TableInfo tInfo, out int numItems)
    {
        int count = 0;
        numItems = 0;
        string res = "";
        for (int i = 0; i < ids.Length; i++)
        {
            int id = Helper.GetInt(ids[i], -1);
            string qry = id == -1 ? iqry : uqry;
            numItems++;
            // todo: see 'req (requiredField)' and for <=0 --> delete
            if (!string.IsNullOrEmpty(required))
            {
                string req = Helper.GetString(Request.Form[required], null, count);
                //int reqVal = Helper.GetInt(Helper.GetString(Request.Form[required], null, count), -1);
                //if (reqVal < 1)
                if (string.IsNullOrEmpty(req) || req == "-1")
                {
                    qry = dqry;
                    numItems--;
                    if (id == -1)
                    {
                        count++;
                        continue; //it is not existed
                    }
                }
            }
            int index = -1;
            while ((index + 1 < qry.Length) && (index = qry.IndexOf('@', index + 1)) >= 0)
            //while ((index = qry.IndexOf('@')) >= 0)
            {
                // @@ --> @
                if (qry[index + 1] == '@' || (index > 0 && qry[index - 1] == '@'))
                {
                    index += 2;
                    continue;
                }
                int last = qry.IndexOfAny(new char[] { ' ', '"', '\'', ',', '(', ')', '\r', '\n', ';', '=' }, index + 1);
                string field = last > 0 ? qry.Substring(index + 1, last - index - 1) : qry.Substring(index + 1);
                string val = Helper.GetString(Request.Form[field], null);
                if (val == null && tInfo != null)
                {
                    FieldInfo f = tInfo.GetField(field);
                    if (f != null && f.Type == "bit")
                    {
                        // checkbox value is not transfered if it is not checked, so maybe we have renamed the field
                        // ex: IsFranchise --> IsFranchise0, IsFranchise1, ..., IsFranchise20
                        val = Helper.GetString(Request.Form[field + count.ToString()], null);
                    }
                }
                if (val != null)
                    if (val.Contains(",")) // multi value
                        val = val.Split(',')[count];
                if (tInfo != null)
                    val = tInfo.PrepareValue(field, val);
                //qry = qry.Replace("@" + field, val);
                qry = qry.Replace("@" + field + " ", val + " ").Replace("@" + field + "\"", val + "\"").Replace("@" + field + "'", val + "'")
                    .Replace("@" + field + "(", val + "(").Replace("@" + field + ")", val + ")").Replace("@" + field + ",", val + ",")
                    .Replace("@" + field + "\n", val + "\n")
                    .Replace("@" + field + "\r", val + "\r").Replace("@" + field + ";", val + ";");
                if (qry.EndsWith("@" + field))
                    qry = qry.Replace("@" + field, val);
            }
            qry = qry.Replace("@@", "@");
            qry = qry.Replace("iAmcOMMA", ",");
            count++;
            mDbProxy.LastException = null;
            if (mDbProxy.RunQuery(qry, true) > 0)
                res += ", OK";
            else if (mDbProxy.LastException != null)
                res += "Error: " + mDbProxy.LastException.Message + " Query: " + qry;
        }
        return res;
    }
    private string[] LoadQueries(string[] names)
    {
        string[] res = new string[names.Length];
        for (int i = 0; i < names.Length; i++)
        {
            QueryInfo qInfo = QueryController.Instance[names[i].Trim()];
            if (qInfo != null)
                if (!string.IsNullOrEmpty(qInfo.Query))
                    res[i] = qInfo.Query;
            if (string.IsNullOrEmpty(res[i]))
                throw new Exception(string.Format("NO QUERY for index={0} Name={1}", i, names[i]));
        }
        return res;
    }
    private void RunDelete()
    {
        string table = Request.QueryString["table"];
        TableInfo tInfo = TableController.Instance[table];
        if (SiteSettings.IsDbReadonly)
        {
            if (tInfo == null || tInfo.ReadOnly)
            {
                Response.Write(LangProvider.Instance["SetAspxM1"]);
                return;
            }
        }
        if (SiteSettings.User == null || !SiteSettings.User.HasAccess(table, AccessType.Delete))
        {
            Response.Write(LangProvider.Instance["SetAspxM4"]);
            return;
        }
        List<TableInfo> tInfos = new List<TableInfo>();
        if (tInfo != null)
            tInfos.Add(tInfo);
        string qry = null, deleteLog = null;
        QueryInfo qInfo = null;
        if (!string.IsNullOrEmpty(tInfo.DeleteQuery))
        {
            qInfo = QueryController.Instance[tInfo.DeleteQuery];
            if (qInfo != null)
                if (!string.IsNullOrEmpty(qInfo.Query))
                {
                    qry = qInfo.Query;
                    deleteLog = qInfo.DeleteLog;
                }
        }
        if (string.IsNullOrEmpty(qry))
        {
            qry = "DELETE FROM @Table WHERE ID=@ID";
            deleteLog = "SELECT * FROM @Table WHERE ID=@MasterID".Replace("@Table", table);
            //Response.Write("NO QUERY");
            //return;
        }

        qry = qry.Replace("@Table", table).Replace("@_ID_", "@ID");
        qry = Helper.ParseQueryVars(qry, tInfo, Request);
        int id = Helper.GetInt(Request.QueryString["ID"], -1);

        // logging: keep the logging outside of transaction
        string beforeData = null;
        if (!string.IsNullOrEmpty(deleteLog))
            beforeData = LogDeletePrepare(tInfos, null, deleteLog.Replace("@MasterID", id.ToString()), id);
        else if (qInfo != null && qInfo.BeforeQuery != null)
            beforeData = PrepareLogQuery(qry, tInfos, -1, qInfo.BeforeQuery);

        try
        {
            mDbProxy.BeginTransaction();
            int num = mDbProxy.RunQuery(qry, true);
            if (num > 0)
            {
                mDbProxy.CommitTransaction();

                try
                {
                    if (!string.IsNullOrEmpty(deleteLog))
                        LogDeleteCommit(tInfos, null, beforeData, id, Helper.GetString(Request.QueryString["comment"], ""));
                    else
                        LogQuery(qry, tInfos, -1, qInfo == null || qInfo.AfterQuery == null ? null : qInfo.AfterQuery, beforeData);
                }
                catch { }
                if (qInfo != null)
                    qInfo.RunTrigger(Helper.GetInt(Helper.FindParameter("ID", Request), -1));
                Response.Write("OK");
            }
            else
            {
                mDbProxy.RollbackTransaction();
                if (num == -1 && mDbProxy.LastException != null)
                    Response.Write("Error: " + mDbProxy.LastException.Message + " Query: " + qry);
                else
                    Response.Write(LangProvider.Instance["SetAspxM5"]);
            }
        }
        catch (Exception ex)
        {
            Response.Write("Error: " + ex.Message + " Query: " + qry);
        }
        Response.End();
    }
    private void RunMultiDelete()
    {
        string table = Request.QueryString["table"];
        TableInfo tInfo = TableController.Instance[table];
        if (SiteSettings.IsDbReadonly)
        {
            if (tInfo == null || tInfo.ReadOnly)
            {
                Response.Write(LangProvider.Instance["SetAspxM1"]);
                return;
            }
        }
        if (SiteSettings.User == null || !SiteSettings.User.HasAccess(table, AccessType.Delete))
        {
            Response.Write(LangProvider.Instance["SetAspxM4"]);
            return;
        }
        List<TableInfo> tInfos = new List<TableInfo>();
        if (tInfo != null)
            tInfos.Add(tInfo);
        string delQry = null;
        string deleteLog = null;
        QueryInfo qInfo = null;
        if (!string.IsNullOrEmpty(tInfo.DeleteQuery))
        {
            qInfo = QueryController.Instance[tInfo.DeleteQuery];
            if (qInfo != null)
                if (!string.IsNullOrEmpty(qInfo.Query))
                {
                    delQry = qInfo.Query;
                    deleteLog = qInfo.DeleteLog;
                }
        }
        if (string.IsNullOrEmpty(delQry))
        {
            delQry = "DELETE FROM @Table WHERE ID=@ID";
            deleteLog = "SELECT * FROM @Table WHERE ID=@MasterID".Replace("@Table", table);
        }

        string[] ids = Request.QueryString["ids"].Split(',');
        delQry = delQry.Replace("@Table", table).Replace("@_ID_", "@ID");

        // logging: keep the logging outside of transaction
        List<string> beforeDatas = new List<string>();
        for (int i = 0; i < ids.Length; i++)
        {
            if (!string.IsNullOrEmpty(deleteLog))
                beforeDatas.Add(LogDeletePrepare(tInfos, null, deleteLog.Replace("@MasterID", ids[i]), Helper.GetInt(ids[i], -1)));
            else if (qInfo != null && qInfo.BeforeQuery != null)
            {
                string qry = delQry.Replace("@ID", ids[i]);
                qry = Helper.ParseQueryVars(qry, tInfo, Request);
                beforeDatas.Add(PrepareLogQuery(qry, tInfos, -1, qInfo.BeforeQuery.Replace("@ID", ids[i])));
            }
            else
                beforeDatas.Add("");
        }

        string res = "";
        mDbProxy.BeginTransaction();
        bool rolledBack = false;
        try
        {
            for (int i = 0; i < ids.Length; i++)
            {
                int id = Helper.GetInt(ids[i], -1);
                string qry = delQry;
                qry = qry.Replace("@ID", ids[i]);
                qry = Helper.ParseQueryVars(qry, tInfo, Request);

                if (mDbProxy.RunQuery(qry, true) > 0)
                    res += "OK,";
                else
                    throw new Exception(mDbProxy.LastException.Message + " Query: " + qry);

                if (qInfo != null)
                    qInfo.RunTrigger(id);
            }
        }
        catch (Exception ex)
        {
            rolledBack = true;
            res += "Error: " + ex.Message;
            mDbProxy.RollbackTransaction();
        }
        if (!rolledBack)
        {
            mDbProxy.CommitTransaction();
            try
            {
                for (int i = 0; i < ids.Length; i++)
                {
                    if (!string.IsNullOrEmpty(deleteLog))
                        LogDeleteCommit(tInfos, null, beforeDatas[i], Helper.GetInt(ids[i], -1), Helper.GetString(Request.QueryString["comment"], ""));
                    else
                    {
                        string qry = delQry.Replace("@ID", ids[i]);
                        qry = Helper.ParseQueryVars(qry, tInfo, Request);
                        LogQuery(qry, tInfos, -1, qInfo == null || qInfo.AfterQuery == null ? null : qInfo.AfterQuery.Replace("@ID", ids[i]), beforeDatas[i]);
                    }
                }
            }
            catch { }
        }

        Response.Write(res);
        Response.End();
    }

    /* void SendToSepas()
     {
         if (SiteSettings.IsDbReadonly)
         {
             Response.Write(LangProvider.Instance["SetAspxM1"]);
             return;
         }
         Response.Write(SendToSepasInternal());
         Response.End();
     }
     string SendToSepasInternal()
     {
         int id = Helper.GetInt(HttpContext.Current.Request.QueryString["fileid"], -1);
         if (id == -1)
             return "Error: Invalid File";
         try
         {
             using (BillPatient bHelper = new BillPatient())
             {
                 string res = bHelper.SendFile(id, false);
                 if (null == res)
                     return "OK";
                 return "Error: " + res;
             }
         }
         catch (Exception ex)
         {
             Logger.Log(ex);
             return "Error: See Error Log";
         }
     }*/
    /// <summary>
    /// get querystrings and send sms by them 
    /// </summary>
    void StartSendSMS()
    {
        string recipient = Request.QueryString["recipient"];
        string message = Request.QueryString["message"];
        string fileID = Request.QueryString["fileID"];
        if (string.IsNullOrEmpty(fileID) && !string.IsNullOrEmpty(recipient))
            Helper.SendSMS(recipient, message);
        else if (!string.IsNullOrEmpty(fileID))
        {
            object mobile = DbProxy.Instance.FillScalar("SELECT TOP 1 Mobile FROM Patient WHERE ID IN (SELECT PatientID FROM Files WHERE ID=" + fileID + ")");
            if (mobile != null)
                Helper.SendSMS(mobile.ToString(), message);
        }
        //برای خطا باید آن باید چه چیزی بنویسم؟

    }
    void SetMonetaryUnit()
    {
        int id = Helper.GetInt(Request.QueryString["id"], -1);
        if (id <= 0)
            Response.Write("Error");
        else
        {
            SiteSettings.MonetaryUnit = id;
            Response.Write(SiteSettings.MonetaryUnit);
        }
        Response.End();
    }
    void SetProgram()
    {
        int id = Helper.GetInt(Request.QueryString["id"], -2);
        if (id <= -2)
            Response.Write("Error");
        else
        {
            SiteSettings.ProgramID = id;
            Response.Write(SiteSettings.ProgramID);
        }
        Response.End();
    }
    void SetDB()
    {
        string catalogName = Request.QueryString["catalog"];
        {
            if (catalogName == LangProvider.Instance["ActiveYear"])
                SiteSettings.OtherCatalogName = "";
            else
                SiteSettings.OtherCatalogName = catalogName;
            Response.Write(catalogName);
        }
        Response.End();
    }
    /// <summary>
    /// copy an Voucher to VoucherDB
    /// </summary>
    void ToVoucherDB()
    {
        try
        {
            ToVoucherDBInternal();
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        Response.End();
    }
    void ToVoucherDBInternal()
    {
        // Check setting
        string cntStr = mDbProxy.GetConfig("VoucherDB", null, -1);
        if (string.IsNullOrEmpty(cntStr))
        {
            Response.Write("Error: missing VoucherDB in settings");
            return;
        }
        int voucherID = Helper.GetInt(Request.QueryString["id"], Null.NullInteger);
        DataRow row = mDbProxy.FillRow(@"SELECT DocumentAccounting.*, Users.Username UserName, U2.Username LockerName, Program.Code ProgramCode FROM DocumentAccounting
LEFT JOIN Users ON DocumentAccounting.UserID = Users.ID
LEFT JOIN Users U2 ON DocumentAccounting.LockerID = U2.ID
LEFT JOIN Program ON DocumentAccounting.ProgramID = Program.ID WHERE DocumentAccounting.ID = " + voucherID);
        if (row == null)
        {
            Response.Write("Error: Invalid Voucher");
            return;
        }
        DataTable items = mDbProxy.FillData(@"SELECT DocumentAccountingItems.*
, dbo.TreePathToTopicCode(DocumentAccountingItems.TreePath) TreePathCode
, dbo.TreePathToTopicCode(DocumentAccountingItems.SecTreePath)  SecTreePathCode
, Personnel.Code PersonnelCode 
, P2.Code SecPersonnelCode
, Ward.Code WardCode
FROM DocumentAccountingItems
LEFT JOIN Personnel ON DocumentAccountingItems.PersonnelID = Personnel.ID
LEFT JOIN Personnel P2 ON DocumentAccountingItems.SecPersonnelID = P2.ID
LEFT JOIN Ward ON DocumentAccountingItems.ProjectID = Ward.ID WHERE DocumentAccountingItems.DocumentAccountingID=" + voucherID, "items");

        // Delete existing items in VoucherDB
        string no = Helper.GetString(row, "No", "");
        string delQuery = string.Format(@"DELETE FROM DocumentAccountingItems WHERE DocumentAccountingID IN (SELECT ID FROM DocumentAccounting WHERE No={0});
DELETE FROM DocumentAccounting WHERE No={0}", Helper.QoutedString(no));
        using (DbProxy voucherDB = new DbProxy())
        {
            voucherDB.ConnectionString = cntStr;
            voucherDB.RunQuery(delQuery);

            // insert voucher
            string insQuery = string.Format(@"
DECLARE @UserID INT, @LockerID INT, @ProgramID INT;
SET @UserID = NULL;
SET @LockerID = NULL;
SET @ProgramID = NULL;
SELECT @UserID = ID FROM Users WHERE Username = {0};
SELECT @LockerID = ID FROM Users WHERE Username = {1};
SELECT @ProgramID = ID FROM Program WHERE Code = {2};
INSERT INTO DocumentAccounting (No, Date, UserID, LockerID, DoComments, Reciever, TotalOwe, TotalDue, Mode, FinanceYearID, Status, ProgramID) 
         VALUES ({3}, {4}, @UserID, @LockerID, {5}, {6}, {7}, {8}, {9}, {10}, {11}, @ProgramID);
         SELECT SCOPE_IDENTITY();"
                , Helper.QoutedString(Helper.GetString(row, "UserName", "DonkeyTurk"))
                , Helper.QoutedString(Helper.GetString(row, "LockerName", "DonkeyTurk"))
                , Helper.QoutedString(Helper.GetString(row, "ProgramCode", "DonkeyTurk"))
                , Helper.QoutedString(Helper.GetString(row, "No", ""))
                , Helper.QoutedDateTime(Helper.GetDateTime(row, "Date", DateTime.Now))
                , Helper.QoutedString(Helper.GetString(row, "DoComments", ""))
                , Helper.QoutedString(Helper.GetString(row, "Reciever", ""))
                , Helper.GetDecimal(row, "TotalOwe", 0)
                , Helper.GetDecimal(row, "TotalDue", 0)
                , Helper.GetInt(row, "Mode", 0)
                , Helper.GetInt(row, "FinanceYearID", 0)
                , Helper.GetInt(row, "Status", 0));
            int destID = Helper.GetInt(voucherDB.FillScalar(insQuery), Null.NullInteger);
            if (destID == Null.NullInteger)
            {
                if (voucherDB.LastException != null)
                    Response.Write("Error: " + voucherDB.LastException.Message);
                else
                    Response.Write("Error: Failed to insert");
                return;
            }

            // insert items
            if (items != null)
                for (int i = 0; i < items.Rows.Count; i++)
                {
                    int CheckID = Helper.GetInt(items.Rows[i], "CheckID", Null.NullInteger)
                        , PayCheckID = Helper.GetInt(items.Rows[i], "PayCheckID", Null.NullInteger);
                    insQuery = string.Format(@"
DECLARE @PersonnelID INT, @SecPersonnelID INT, @TreePath nvarchar(2000), @SecTreePath nvarchar(2000), @WardID INT, @ProgramID INT;
SET @PersonnelID = NULL;
SET @SecPersonnelID = NULL;
SET @TreePath = NULL;
SET @SecTreePath = NULL;
SET @WardID = NULL;
SET @ProgramID = NULL;
SELECT @ProgramID = ID FROM Program WHERE Code = {14};
SELECT @PersonnelID = ID FROM Personnel WHERE Code = {0};
SELECT @SecPersonnelID = ID FROM Personnel WHERE Code = {1};
SELECT @TreePath = TreePath FROM Trees INNER JOIN Topic ON [dbo].[TreePathToProgramID](TreePath) = Topic.ID WHERE ((ProgramID IS NULL AND @ProgramID IS NULL) OR ProgramID=@ProgramID) AND dbo.TreePathToTopicCode(TreePath) = {2}
SELECT @SecTreePath = TreePath FROM Trees INNER JOIN Topic ON [dbo].[TreePathToProgramID](TreePath) = Topic.ID WHERE ((ProgramID IS NULL AND @ProgramID IS NULL) OR ProgramID=@ProgramID) AND dbo.TreePathToTopicCode(TreePath) = {3}
SELECT @WardID = ID FROM Ward WHERE Code = {4};
INSERT INTO DocumentAccountingItems (DocumentAccountingID, TreePath, PersonnelID, SecTreePath, SecPersonnelID, ProjectID, SortIndex, Owe, Due, DocNo, DocDate, Comments, CheckID, PayCheckID) 
            VALUES ({5}, @TreePath, @PersonnelID, @SecTreePath, @SecPersonnelID, @WardID, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13});"
                        , Helper.QoutedString(Helper.GetString(items.Rows[i], "PersonnelCode", ""))
                        , Helper.QoutedString(Helper.GetString(items.Rows[i], "SecPersonnelCode", ""))
                        , Helper.QoutedString(Helper.GetString(items.Rows[i], "TreePathCode", ""))
                        , Helper.QoutedString(Helper.GetString(items.Rows[i], "SecTreePathCode", ""))
                        , Helper.QoutedString(Helper.GetString(items.Rows[i], "WardCode", ""))
                        , destID
                    , Helper.GetInt(items.Rows[i], "SortIndex", 0)
                    , Helper.GetDecimal(items.Rows[i], "Owe", 0)
                    , Helper.GetDecimal(items.Rows[i], "Due", 0)
                    , Helper.QoutedString(Helper.GetString(items.Rows[i], "DocNo", ""))
                    , Helper.QoutedDateTime(Helper.GetDateTime(items.Rows[i], "DocDate", Null.NullDate))
                    , Helper.QoutedString(Helper.GetString(items.Rows[i], "Comments", ""))
                    , "NULL" /* CheckID == Null.NullInteger ? "NULL" : CheckID.ToString()*/
                    , "NULL" /*PayCheckID == Null.NullInteger ? "NULL" : PayCheckID.ToString()*/
                    , Helper.QoutedString(Helper.GetString(row, "ProgramCode", "DonkeyTurk")));
                    voucherDB.RunQuery(insQuery);
                }
        }
    }
    private void UpdateLang()
    {
        try
        {
            string key = Request.Form["Key"];
            string translation = Request.Form["Translation"];
            string lang = Request.QueryString["lang"];

            LangProvider.Instance.Update(lang, key, translation);
            Response.Write("OK");
        }
        catch (Exception ex)
        {
            Response.Write("Error: " + ex.Message);
        }
        Response.End();
    }
    private XmlDocument CalcOrder()
    {
        XmlDocument xml = new XmlDocument();
        xml.LoadXml("<?xml version='1.0' encoding='UTF-8'?><Tables></Tables>");
        XmlNode tNode = xml.CreateNode(XmlNodeType.Element, "T", "");
        string fi = "";
        int constid = string.IsNullOrEmpty(Request.QueryString["cid"]) ? -1 : int.Parse(Request.QueryString["cid"]);
        FormulaEvaluater.FishID = string.IsNullOrEmpty(Request.QueryString["fishid"]) ? -1 : int.Parse(Request.QueryString["fishid"]);
        FormulaEvaluater.ConstID = constid;
        FormulaEvaluater.OrderID = string.IsNullOrEmpty(Request.QueryString["orderid"]) ? -1 : int.Parse(Request.QueryString["orderid"]);
        FormulaEvaluater.FormFieldsInfo = new List<FormField>();
        FormulaEvaluater.OrderValueInfos = new List<OrderValueInfo>();
        FormulaEvaluater.FishValueInfos = new List<FishValueInfo>();

        string request = Request.Form.ToString();
        string field = null, subRequest = request.Substring(0);
        double val = 0;
        List<string[]> fields = new List<string[]>();
        if (!string.IsNullOrEmpty(Request.QueryString["fields"]))
        {
            fi = Request.QueryString["fields"].Replace('-', '=').Replace('|', '&');
            fields = Helper.GetRequestFields(fi);

        }
        else
        {
            fields = Helper.GetRequestFields(request);
        }

        int i = 0;
        while (i < fields.Count)
        {

            string[] f = fields[i];
            FieldsType FieldType = Helper.GetFieldType(f[0]);
            if (double.TryParse(f[1], out val) || string.IsNullOrEmpty(f[1]))
            {
                if (FieldType != FieldsType.Boolean && FieldType != FieldsType.Numeric && FieldType != FieldsType.None)
                {
                    XmlNode node = xml.CreateNode(XmlNodeType.Element, "R", "");
                    FormulaEvaluater.FormFields = fields;
                    field = f[0];
                    val = FormulaEvaluater.CalcValue(field);
                    if (val >= 0)
                        f[1] = val.ToString();
                    fields[i] = f;
                    //request=request.Replace(field + "=", field + "=" + val);
                    node.Attributes.Append(XmlHelper.CreateXmlAttr(xml, "Name", f[0]));
                    node.Attributes.Append(XmlHelper.CreateXmlAttr(xml, "Value", f[1]));
                    tNode.AppendChild(node);
                }
            }
            i++;
        }
        xml.DocumentElement.AppendChild(tNode);

        return xml;
    }
    public void RunInsertValues()
    {
        string table = Request.QueryString["table"];
        string q = Request.QueryString["qd"];
        string id = Request.QueryString["id"];
        TableInfo tInfo = TableController.Instance[table];
        QueryInfo qInfo = null;
        if (!string.IsNullOrEmpty(q))
            qInfo = QueryController.Instance[q];
        string qry = qInfo.Query;
        try
        {
            qry = Helper.ParseQueryVars(qry, tInfo, Request);
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
            Response.End();
        }
        try
        {
            mDbProxy.BeginTransaction();
            object obj = mDbProxy.FillScalar(qry);
            if (obj == null || obj is DBNull)
            {
                mDbProxy.RollbackTransaction();
                if (mDbProxy.LastException != null)
                {
                    ExceptionLog.Instance.Add(mDbProxy.LastException, qry);
                    Response.Write("Error: " + mDbProxy.LastException.Message);// + " Query: " + qry);
                }
                else
                    Response.Write("Error: Failed");
            }
            else
            {

                mDbProxy.CommitTransaction();
                if (table == "Orders")
                    InsertOrderValues(obj.ToString(), Helper.GetRequestFields(Request.Form.ToString()));
                if (table == "Fish")
                    InsertFishValues(obj.ToString(), Helper.GetRequestFields(Request.Form.ToString()));
                Response.Write(obj.ToString());
            }
        }
        catch (Exception ex)
        {
            Response.Write(Helper.GetError(ex.Message, tInfo != null ? tInfo : null));
        }
        Response.End();
    }
    void InsertOrderValues(string orderid, List<string[]> fields)
    {
        int cntValue = Convert.ToInt32(mDbProxy.FillScalar("SELECT COUNT(*) FROM OrderValues WHERE OrderID=" + orderid));
        DataTable infos = mDbProxy.FillData(@"SELECT OrderDefFields.ID, Fields.Name
                                                      FROM OrderDefFields  
                                                      INNER JOIN Fields ON OrderDefFields.FieldID=Fields.ID
                                                      INNER JOIN Orders ON OrderDefFields.OrderDefID=Orders.OrderDefID
                                                      WHERE Orders.ID=" + orderid, "infos");
        Helper.GetRequestFields(Request.Form.ToString());
        for (int i = 0; i < infos.Rows.Count; i++)
        {
            for (int j = 0; j < fields.Count; j++)
            {
                string[] f = fields[j];
                if (f[0] == Helper.GetString(infos.Rows[i], "Name", ""))
                {
                    string OrderDefFieldID = Helper.GetString(infos.Rows[i], "ID", "");
                    bool b = false;
                    if (!Helper.HasOrderValue(OrderDefFieldID, orderid))
                        b = DbProxy.Instance.ExecuteCommand(string.Format(@"INSERT INTO OrderValues (OrderID, OrderDefFieldID, Value) VALUES ({0}, {1}, {2})", orderid, Helper.GetString(infos.Rows[i], "ID", ""), f[1]));
                    else
                        b = DbProxy.Instance.ExecuteCommand(string.Format(@"UPDATE OrderValues SET OrderID={0}, OrderDefFieldID={1}, Value={2} 
                                                                            WHERE OrderDefFieldID IN (SELECT OrderDefFields.ID
                                                                                                      FROM OrderDefFields  
                                                                                                      INNER JOIN Fields ON OrderDefFields.FieldID=Fields.ID
                                                                                                      INNER JOIN Orders ON OrderDefFields.OrderDefID=Orders.OrderDefID
                                                                                                      WHERE Orders.ID={0} AND Fields.Name=N'{3}') AND OrderID={0}", orderid, OrderDefFieldID, f[1] == "" ? "NULL" : f[1], f[0]));
                    if (b == true)
                        fields.Remove(f);
                    break;
                }
            }
        }
    }
    void InsertFishValues(string fishid, List<string[]> fields)
    {
        DataTable infos = mDbProxy.FillData(@"SELECT FishDefFields.ID, Fields.Name
                                                      FROM FishDefFields  
                                                      INNER JOIN Fields ON FishDefFields.FieldID=Fields.ID
                                                      INNER JOIN Fish ON FishDefFields.FishDefID=Fish.FishDefID
                                                      WHERE Fish.ID=" + fishid, "infos");

        for (int i = 0; i < infos.Rows.Count; i++)
        {
            for (int j = 0; j < fields.Count; j++)
            {
                string[] f = fields[j];
                if (f[0] == Helper.GetString(infos.Rows[i], "Name", ""))
                {
                    string FishDefFieldID = Helper.GetString(infos.Rows[i], "ID", "");
                    bool b = false;
                    if (!Helper.HasFishValue(FishDefFieldID, fishid))
                        b = DbProxy.Instance.ExecuteCommand(string.Format(@"INSERT INTO FishValues (FishID, FishDefFieldID, Value) VALUES ({0}, {1}, {2})", fishid, FishDefFieldID, Helper.GetDecimal(f[1], 0)));
                    else
                        b = DbProxy.Instance.ExecuteCommand(string.Format(@"UPDATE FishValues SET FishID={0}, FishDefFieldID={1}, Value={2} 
                                                                                    WHERE FishDefFieldID IN (SELECT FishDefFields.ID
                                                                                                             FROM FishDefFields  
                                                                                                             INNER JOIN Fields ON FishDefFields.FieldID=Fields.ID
                                                                                                             INNER JOIN Fish ON FishDefFields.FishDefID=Fish.FishDefID
                                                                                                             WHERE Fish.ID={0} AND Fields.Name=N'{3}') AND FishID={4}", fishid, FishDefFieldID, f[1] == "" ? "NULL" : f[1], f[0], fishid));
                    if (b == true)
                        fields.Remove(f);
                    break;
                }
            }
        }
    }

}