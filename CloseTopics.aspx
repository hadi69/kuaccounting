﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="CloseTopics.aspx.cs" Inherits="CloseTopics" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-10 col-md-8 col-md-offset-2 form-group-container margin-10-top">
            <div class="form-group">
                <label class="control-label col-xs-12 col-sm-4">&nbsp;</label>
                <div class="col-xs-12 col-sm-8">
                    <asp:Label runat="server" ID="msg" CssClass="alert block" Visible="False" EnableViewState="False" />
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 col-md-8 col-md-offset-2 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.TheTotalValueOfStock</asp:Label>
                <div class="col-xs-12 col-sm-8">
                    <asp:TextBox runat="server" ID="TheTotalValueOfStock" CssClass="TheTotalValueOfStock form-control input-sm monetary"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 col-md-8 col-md-offset-2 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ChooseProfit</asp:Label>
                <div class="col-xs-12 col-sm-8">
                    <asp:TextBox runat="server" ID="mProfitName" CssClass="ProfitName form-control input-sm" ReadOnly="True"></asp:TextBox>
                    <input type='hidden' runat="server" id="mProfitID" class="ProfitID" />
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 col-md-8 col-md-offset-2 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ChooseCapital</asp:Label>
                <div class="col-xs-12 col-sm-8">
                    <asp:TextBox runat="server" ID="mCapitalName" CssClass="CapitalName form-control input-sm" ReadOnly="True"></asp:TextBox>
                    <input type='hidden' runat="server" id="mCapitalID" class="CapitalID" />
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 col-md-8 col-md-offset-2 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ChooseClosingBalance</asp:Label>
                <div class="col-xs-12 col-sm-8">
                    <asp:TextBox runat="server" ID="mClosingBalanceName" CssClass="ClosingBalanceName form-control input-sm" ReadOnly="True"></asp:TextBox>
                    <input type='hidden' runat="server" id="mClosingBalanceID" class="ClosingBalanceID" />
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 col-md-8 col-md-offset-2 form-group-container margin-10-top">
            <div class="form-group">
                <label class="control-label col-xs-12 col-sm-4">&nbsp;</label>
                <div class="col-xs-12 col-sm-8">
                    <asp:Button runat="server" ID="doStart" Text="Lang.DoStart" OnClick="doStart_Click" CssClass="btn btn-primary btn-sm dostrat" />
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            setClassManager();
            //autoComplete('Trees', 'treesauto', '.ProfitName', '.ProfitID', null);
            //autoComplete('Trees', 'treesauto', '.CapitalName', '.CapitalID', null);
            //autoComplete('Trees', 'treesauto', '.ClosingBalanceName', '.ClosingBalanceID', null);

            var storesvalue = getServerValue('calcstoresvalue', 'Stores', '');
            $('.TheTotalValueOfStock').val(numberWithCommas(storesvalue));
            $('.monetary').keyup(handleMonetaryKeys);
            $('.dostrat').click(function () {
                $('.TheTotalValueOfStock').val(fromMonetary($(this).val().replace(/\,/g, '')));
            });

            setDefaultByTitle('Trees', 'settingtreesauto', '.ProfitName', '.ProfitID', 'SumProfitTreePath');
            setDefaultByTitle('Trees', 'settingtreesauto', '.CapitalName', '.CapitalID', 'WealthTreePath');
            setDefaultByTitle('Trees', 'settingtreesauto', '.ClosingBalanceName', '.ClosingBalanceID', 'ClosingBalanceTreePath');

            $('#<%= TheTotalValueOfStock.ClientID %>').keyup(function () {
                $('#<%= TheTotalValueOfStock.ClientID %>').val(numberWithCommas($('#<%= TheTotalValueOfStock.ClientID %>').val()));
            });

            $('#<%= doStart.ClientID %>').click(function () {
                $('#<%= TheTotalValueOfStock.ClientID %>').val($('#<%= TheTotalValueOfStock.ClientID %>').val().replace(/\,/g, ''));
                return confirm(Lang.ConfirmCloseTopics);
            });

            $("#bar").prepend(breadcrumb({
                title: Lang.CloseTopics,
                parent: {
                    title: Lang.Management,
                    url: "menu/admin.aspx"
                }
            }));
        });
    </script>
</asp:Content>

