﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="setup.aspx.cs" Inherits="setup" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <title></title>
    <script src="js/jquery-1.4.1-vsdoc.js" language="javascript" type="text/javascript"></script>
    <script src="js/plugins.js" language="javascript" type="text/javascript"></script>
    <script src="js/JsFarsiCalendar.js" type="text/javascript" language="JavaScript"></script>
    <script src="js/shortcut.js" language="javascript" type="text/javascript"></script>
    <link href="styles/style.css" rel="stylesheet" type="text/css" />
    <link href="styles/flexigrid.pack.css" rel="stylesheet" type="text/css" />
    <link href="styles/formly.css" rel="stylesheet" type="text/css" />
    <link href="styles/impromptu.css" rel="stylesheet" type="text/css" />
    <link href="styles/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <link href="styles/calendar.css" rel="stylesheet" type="text/css" />
    <link href="styles/tabs.css" rel="stylesheet" type="text/css" />
    <link href="styles/messagebar.css" rel="stylesheet" type="text/css" />
    <link href="styles/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
    <link href="styles/jkmegamenu.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style='margin: 0px auto 3px auto; text-align: center'>
            <img src='img/logoright.png' />
        </div>
    <div class='main'>
        <div style='width:100%'>
            <div style='margin: 0px auto 0px auto; width: 456px; height: 522px; padding: 0px;direction: rtl;'>
                <div style='background-image: url(img/login.gif); background-repeat: no-repeat; border: solid 0px red;
                    padding: 0px; margin: 0 auto; width: 456px; height: 522px'>
                    <div id="detailFor1m" style='padding-top: 220px; padding-right: 40px'>
                        <label for='mUsername' style='display: inline-block; min-width: 50px; text-align: left;'>
                            <%= "ChooseServer".Translate() %>
                        </label>
                        <asp:DropDownList runat="server" ID="mDBs" CssClass="round2" style='display: inline-block; width: 200px; text-align: left;'>
                        </asp:DropDownList>
                        <br />
                        <label for='mServer' style='display: inline-block; min-width: 50px; text-align: left;'>
                            <%= "ServerName".Translate() %>
                        </label>
                        <asp:TextBox runat="server" ID="mServer" CssClass="round2" />
                        <br />
                        <label for='mUsername' style='display: inline-block; min-width: 50px; text-align: left;'>
                            <%= "Username".Translate() %>
                        </label>
                        <asp:TextBox runat="server" ID="mUsername" CssClass="round2" Width="80px" Text="sa" />
                        <label for='mPass' style='display: inline-block; min-width: 50px; text-align: left;'>
                            <%= "Password".Translate() %>
                        </label>
                        <asp:TextBox runat="server" ID="mPass" CssClass="round2" Width="80px" MaxLength="20" />
                        <asp:Button runat="server" ID="doTest" OnClick="doTest_Click" Text='Lang.STest' CssClass="round2">
                        </asp:Button>
                        <br />
                        <asp:Label runat="server" ID="mMsg" ForeColor="red"></asp:Label>
                        <br />
                    </div>
                </div>
            </div>
            <div class="clear">
            </div>
        </div>
        <div class="footer">
            <JP:Copyright runat="server" />
        </div>
    </div>
    </form>
</body>
</html>
