﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true"
    CodeFile="profile.aspx.cs" Inherits="profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div class="row">
        <JP:PageHeader runat="server" Text="Lang.UserProfile" />
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container margin-10-top">
            <div class="form-group">
                <asp:Label runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.UserName</asp:Label>
                <div class="col-xs-12 col-sm-8">
                    <input type="hidden" id="ID" name="ID" value="@ID" />
                    <asp:TextBox ID="Username" runat="server" CssClass="form-control input-sm" Enabled="False"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container margin-10-top">
            <div class="form-group">
                <asp:Label runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FullName</asp:Label>
                <div class="col-xs-12 col-sm-8">
                    <asp:TextBox ID="Fullname" runat="server" CssClass="form-control input-sm" Enabled="False"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container margin-10-top">
            <div class="form-group">
                <div class="left-aligned-label control-label col-xs-12 col-sm-4">
                    <asp:Label runat="server" AssociatedControlID="OldPassword">Lang.OldPassword</asp:Label>
                    <asp:RequiredFieldValidator runat="server" ID="rq2" ControlToValidate="OldPassword" SetFocusOnError="True" Display="Dynamic" />
                </div>
                <div class="col-xs-12 col-sm-8">
                    <asp:TextBox TextMode="Password" runat="server" ID="OldPassword" CssClass="form-control input-sm ltr" />
                    <span class="red">*</span>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container margin-10-top">
            <div class="form-group">
                <div class="left-aligned-label control-label col-xs-12 col-sm-4">
                    <asp:Label runat="server" AssociatedControlID="Password">Lang.Password</asp:Label>
                    <asp:RequiredFieldValidator runat="server" ID="rq1" ControlToValidate="Password" ErrorMessage="" SetFocusOnError="True" Display="Dynamic" />
                </div>
                <div class="col-xs-12 col-sm-8">
                    <asp:TextBox TextMode="Password" runat="server" ID="Password" CssClass="form-control input-sm ltr" />
                    <span class="red">*</span>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container margin-10-top">
            <div class="form-group">
                <div class="left-aligned-label control-label col-xs-12 col-sm-4">
                    <asp:Label runat="server" AssociatedControlID="Password2">Lang.Password2</asp:Label>
                    <asp:RequiredFieldValidator runat="server" ID="Re" ControlToValidate="Password2" ErrorMessage="" SetFocusOnError="True" Display="Dynamic" />
                </div>
                <div class="col-xs-12 col-sm-8">
                    <asp:TextBox TextMode="Password" runat="server" ID="Password2" CssClass="form-control input-sm ltr" />
                    <span class="red">*</span>
                    <asp:CompareValidator runat="server" ID="cmp" ControlToValidate="Password" ControlToCompare="Password2" Operator="Equal" ErrorMessage='' />
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">
            <div class="form-group">
                <label class="control-label col-xs-12 col-sm-4">&nbsp;</label>
                <div class="col-xs-12 col-sm-8">
                    <asp:Button runat="server" ID="doUpdate" Text='Lang.Confirm' OnClick="doUpdate_Click" CssClass="btn btn-sm btn-primary padding-30-left padding-30-right" />
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container margin-10-top">
            <div class="form-group">
                <label class="control-label col-xs-12 col-sm-4">&nbsp;</label>
                <div class="col-xs-12 col-sm-8">
                    <asp:Label runat="server" ID="mMsg" CssClass="alert block" EnableViewState="False" Visible="False" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
