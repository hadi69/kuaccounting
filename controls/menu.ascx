﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="menu.ascx.cs" Inherits="controls_menu" %>
<!--Mega Menu Anchor-->
<%--<li>
    <a id="sysa" href="<%= ResolveClientUrl("~/")%>menu/sys.aspx" class='menua' title='<%= "MManagement".Translate() %>' access="mainmenu,sys" level="3">
        <img src='<%= UIHelper.DummyImage() %>' alt='<%= "MManagement".Translate() %>' class="navbar-img sprites sprites-system-small" />
        <asp:Label Text="Lang.MManagement" runat="server" />
    </a>
</li>--%>
<li>
    <a id="managera" href="<%= ResolveClientUrl("~/")%>menu/admin.aspx" class='menua' title='<%= "Management".Translate() %>' access="mainmenu,admin">
        <img src='<%= UIHelper.DummyImage() %>' alt='<%= "Management".Translate() %>' class="navbar-img sprites sprites-admin-small" />
        <asp:Label Text="Lang.Management" runat="server" />
    </a>
</li>
<li>
    <a id="accountinga" href="<%= ResolveClientUrl("~/")%>menu/accounting.aspx" class='menua' title='<%= "Accounting".Translate() %>' access="mainmenu,accounting">
        <img src='<%= UIHelper.DummyImage() %>' alt='<%= "Accounting".Translate() %>' class="navbar-img sprites sprites-abacus-small" />
        <asp:Label Text="Lang.Accounting" runat="server" />
    </a>
</li>
<li>
    <a id="salesa" href="<%= ResolveClientUrl("~/")%>menu/sales.aspx" class='menua' title='<%= "SellAndBuy".Translate() %>' access="mainmenu,sales">
        <img src='<%= UIHelper.DummyImage() %>' alt='<%= "SellAndBuy".Translate() %>' class="navbar-img sprites sprites-cart-small" />
        <asp:Label Text="Lang.SellAndBuy" runat="server" />
    </a>
</li>
<li>
    <a id="reportsa" href="<%= ResolveClientUrl("~/")%>menu/reports.aspx" class='menua' title='<%= "Reports".Translate() %>' access="mainmenu,reports">
        <img src='<%= UIHelper.DummyImage() %>' alt='<%= "Reports".Translate() %>' class="navbar-img sprites sprites-report-small" />
        <asp:Label Text="Lang.Reports" runat="server" />
    </a>
</li>
<div id="submenu" class="megamenu popupOptions" style='position: absolute'>
    
</div>

<script type="text/javascript">
    $(function () {
        function enableMenu() {
            var access = $(this).attr("access");
            if (access && access.length > 0) {
                var formButton = access.split(",");
                if (formButton.length == 2) {
                    if (!hasAccess("link", formButton[0], formButton[1])) {
                        $(this).attr("href", "#");
                        $(this).addClass('disabled');
                    }
                }
            }

            // check level
            var level = $(this).attr("level");
            if (!level)
                level = 1;
            if (level > _Context.level)
                $(this).parent().hide();
        }
        //if (!_Context.isAdmin)
            $(".menua").each(enableMenu);


        if (_Context.isAccountingVer)
            $('.noacc').hide();
    });
</script>
