﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using System.Text;

public partial class controls_AccountsFilter : System.Web.UI.UserControl
{
    bool initStep = true, setFocus = true;
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        initStep = true;
        CreateCombos();
        if (!IsPostBack)
            BindDropDowns();
        initStep = false;

    }
    private List<Button> mSets = new List<Button>(), mdoAlls = new List<Button>();
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    private List<ListBox> mLists = new List<ListBox>();
    private void CreateCombos()
    {
        mPlaceHolder.Controls.Clear();
        int max = AccController.GetTreeDepth() + 1;
        if (max < 1)
            return;
        mPlaceHolder.Controls.Add(new LiteralControl("<table cellpadding='0' cellspacing='0' border=1>"));
        // an empty row for search
        mPlaceHolder.Controls.Add(new LiteralControl("<tr><td></td><td></td><td></td></tr>"));
        int i = 0;
        while (i < max)
        {
            #region Title
            mPlaceHolder.Controls.Add(new LiteralControl("<tr>"));
            for (int j = 0; j < 3; j++, i++)
            {
                mPlaceHolder.Controls.Add(new LiteralControl("<td class='N'>"));
                if (i < max)
                {
                    if (i == 0)
                        mPlaceHolder.Controls.Add(new LiteralControl("TotalBookAccount".Translate()));
                    else if (i == 1)
                        mPlaceHolder.Controls.Add(new LiteralControl("MoeinBookAccount".Translate()));
                    //else if (i == 2)
                    //    mPlaceHolder.Controls.Add(new LiteralControl(""));
                    else
                        mPlaceHolder.Controls.Add(new LiteralControl(string.Format("RetailBookAccount".Translate() + "{0}", i - 1)));
                }
                else
                    mPlaceHolder.Controls.Add(new LiteralControl("&nbsp;"));
                mPlaceHolder.Controls.Add(new LiteralControl("</td>"));
            }
            mPlaceHolder.Controls.Add(new LiteralControl("</tr>"));
            #endregion

            #region ListBoxes
            i -= 3;
            mPlaceHolder.Controls.Add(new LiteralControl("<tr>"));
            for (int j = 0; j < 3; j++, i++)
            {
                mPlaceHolder.Controls.Add(new LiteralControl("<td>"));
                if (i < max)
                {
                    ListBox drop = new ListBox();
                    drop.ID = "c" + i;
                    drop.CssClass = "Combo";
                    drop.Width = new Unit(330);
                    drop.SelectionMode = ListSelectionMode.Multiple;

                    ListSearchExtender ex = new ListSearchExtender();
                    //AjaxControlToolkit.ListSearchExtender ex = new AjaxControlToolkit.ListSearchExtender();
                    ex.ID = ex.BehaviorID = "ex" + i;
                    ex.TargetControlID = drop.ID;

                    ex.PromptText = "Search".Translate();
                    ex.PromptCssClass = "lsex";
                    ex.PromptPosition = ListSearchPromptPosition.Top;
                    //ex.MyX = -50;
                    //ex.MyY = -15;

                    mPlaceHolder.Controls.Add(drop);
                    mPlaceHolder.Controls.Add(ex);
                    mLists.Add(drop);
                }
                else
                    mPlaceHolder.Controls.Add(new LiteralControl("&nbsp;"));
                mPlaceHolder.Controls.Add(new LiteralControl("</td>"));
            }
            mPlaceHolder.Controls.Add(new LiteralControl("</tr>"));
            #endregion

            #region Set
            i -= 3;
            mPlaceHolder.Controls.Add(new LiteralControl("<tr>"));
            for (int j = 0; j < 3; j++, i++)
            {
                mPlaceHolder.Controls.Add(new LiteralControl("<td align=left>"));
                if (i < max)
                {
                    Button set = new Button();
                    set.ID = "s" + i;
                    set.Text = "Confirm".Translate();
                    set.CssClass = "B";
                    set.Width = new Unit(25);
                    set.Click += new EventHandler(set_Click);
                    //set.Enabled = canEdit;

                    //mPlaceHolder.Controls.Add(set);
                    mSets.Add(set);

                    Button all = new Button();
                    all.ID = "a" + i;
                    all.Text = "All".Translate();
                    all.CssClass = "B";
                    all.Width = new Unit(25);
                    all.Click += new EventHandler(all_Click);
                    //all.Enabled = canEdit;

                    mPlaceHolder.Controls.Add(all);
                    mdoAlls.Add(all);
                }
                else
                    mPlaceHolder.Controls.Add(new LiteralControl("&nbsp;"));
                mPlaceHolder.Controls.Add(new LiteralControl("</td>"));
            }
            mPlaceHolder.Controls.Add(new LiteralControl("</tr>"));
            #endregion
        }

        mPlaceHolder.Controls.Add(new LiteralControl("</table>"));
    }

    void BindDropDowns()
    {
        for (int i = 0; i < mLists.Count; i++)
        {
            //mLists[i].DataTextField = "CodeName";
            //mLists[i].DataValueField = "ID";
            mLists[i].Items.Clear();
            List<int> ids = new List<int>();
            List<TreeInfo> trees = AccController.GetTrees(i, SiteSettings.ProgramID);
            for (int t = 0; t < trees.Count; t++)
            {
                int id = trees[t].TopicID;
                if (!ids.Contains(id))
                {
                    ids.Add(id);
                    mLists[i].Items.Add(new ListItem(trees[t].TopicCodeName, id.ToString()));
                }
            }
        }
    }
    bool firstEvent = true;

    void all_Click(object sender, EventArgs e)
    {
        Button all = sender as Button;
        int depth = Helper.GetInt(all.ID.Substring(1), 0);
        ListBox drop = mLists[depth];
        for (int i = 0; i < drop.Items.Count; i++)
            drop.Items[i].Selected = false;
    }
    void set_Click(object sender, EventArgs e)
    {
        return;
        Button edit = sender as Button;
        int depth = Helper.GetInt(edit.ID.Substring(1), 0);
        if (depth == mSets.Count - 1)
            return;
        ListBox drop = mLists[depth];
        List<TreeInfo> childs = null;
        for (int i = 0; i < drop.Items.Count; i++)
        {
            if (!drop.Items[i].Selected)
                continue;
            //string tree
        }
        if (string.IsNullOrEmpty(drop.SelectedValue) || drop.SelectedItem.Text.StartsWith("---"))
            childs = new List<TreeInfo>();
        else
            childs = AccController.GetTrees(depth + 1, drop.SelectedValue);
        if (childs == null)
            childs = new List<TreeInfo>();

        childs.Sort();
        TreeInfo none = new TreeInfo();
        none.TreePath = "";
        childs.Insert(0, none);

        if (depth < mLists.Count - 1)
        {
            mLists[depth + 1].Items.Clear();
            mLists[depth + 1].SelectedValue = null;
            mLists[depth + 1].SelectedIndex = -1;
            mLists[depth + 1].DataSource = childs;
            mLists[depth + 1].DataBind();

            // --------------------------
            // Add childs from others too
            if (depth > 1 && !string.IsNullOrEmpty(mLists[depth].SelectedValue) && !string.IsNullOrEmpty(mLists[0].SelectedValue))
                if (!mLists[depth].SelectedItem.Text.StartsWith("---"))
                {
                    int totalID = TreeInfo.GetTotalID(mLists[0].SelectedValue);
                    int elaborativeID = TreeInfo.GetElaborativeID(mLists[1].SelectedValue);
                    int accountID = TreeInfo.GetTopicID(mLists[depth].SelectedValue);
                    List<TreeInfo> anothers = AccController.GetTrees(depth + 1, totalID, elaborativeID, accountID);
                    if (anothers != null)
                        for (int i = 0; i < anothers.Count; i++)
                        {
                            bool found = false;
                            for (int j = 0; j < childs.Count; j++)
                                if (anothers[i].TopicID == childs[j].TopicID)
                                {
                                    found = true;
                                    break;
                                }
                            if (!found)
                                mLists[depth + 1].Items.Add(new ListItem("---" + anothers[i].TopicCodeName, anothers[i].TreePath));
                        }
                }
        }
        if (!initStep)
            if (firstEvent)
            {
                if (drop.AutoPostBack)
                {
                    firstEvent = false;
                    if (setFocus && (depth < mLists.Count - 1))
                        JsTools.SetFocusPartial(mLists[depth + 1], "SetFocusDrop");
                }
            }
    }

    private void Enable(bool enable, ControlCollection col)
    {
        for (int i = 0; i < col.Count; i++)
        {
            if (col[i] is TextBox)
                (col[i] as TextBox).Enabled = enable;
            else if (col[i] is Button)
                (col[i] as Button).Enabled = enable;
            else if (col[i] is ListBox)
                (col[i] as ListBox).Enabled = enable;
            Enable(enable, col[i].Controls);
        }
    }

    public string GetIDs(int depth)
    {
        StringBuilder b = new StringBuilder();
        if (depth < mLists.Count)
            for (int i = 0; i < mLists[depth].Items.Count; i++)
            {
                if (mLists[depth].Items[i].Selected)
                {
                    int id = Helper.GetInt(mLists[depth].Items[i].Value, 0);
                    if (b.Length == 0)
                        b.Append(id);
                    else
                        b.Append("," + id);
                }
            }
        return b.ToString();
    }
    public void SetIDs(int depth, string ids)
    {
        ids = ids == null ? "" : "," + ids + ",";
        if (depth < mLists.Count)
            for (int i = 0; i < mLists[depth].Items.Count; i++)
                mLists[depth].Items[i].Selected = ids.Contains("," + mLists[depth].Items[i].Value + ",");
    }
    public int MaxDepth
    {
        get
        {
            if (mLists != null)
                return mLists.Count;
            return Null.NullInteger;
        }

    }

    public ListBox GetDropDown(int depth)
    {
        return mLists[depth];
    }
}