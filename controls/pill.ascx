﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="pill.ascx.cs" Inherits="controls_pill" %>

<div class="col-xs-12 col-md-6 col-lg-3 pill-container">
    <a href="<%= ResolveClientUrl("~/") + Href %>" access="<%= Access %>" level="<%= Level %>" title="<%= Text %>">
        <div class="pill">
            <% if (HasIcon) { %>
                <div class="icon">
                    <img src="<%= ResolveClientUrl("~/")%>img/dummy.png" class="sprites <%= IconClass %>"/>
                </div>
            <% } %>
			<div class="text">
				<asp:Label ID="Label10" runat="server" Text=""></asp:Label>
			</div>
        </div>
    </a>
</div>