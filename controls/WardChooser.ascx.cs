﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class controls_WardChooser : System.Web.UI.UserControl
{
    public event EventHandler WardSelect;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            BindWards();
    }
    private void BindWards()
    {
        mWards.DataTextField = "Name";
        mWards.DataValueField = "ID";

        mWards.DataSource = DbProxy.Instance.FillData("SELECT -1 AS ID, '' AS Name UNION SELECT ID, Name FROM Ward", "ward");
        mWards.DataBind();
    }
    public int WardID
    {
        get
        {
            return Helper.GetInt(mWards.SelectedValue, Null.NullInteger);
        }
        set
        {
            try
            {
                mWards.SelectedValue = value.ToString();
            }
            catch { }
        }
    }
    protected void mWards_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (null != WardSelect)
            WardSelect(this, e);
    }
    public bool Enabled
    {
        get
        {
            return mWards.Enabled;
        }
        set
        {
            mWards.Enabled = value;
        }
    }
}