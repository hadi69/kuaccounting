﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class controls_VoucherNavigator : System.Web.UI.UserControl
{

    public event EventHandler Navigate;
    public event EventHandler NewClick;
    protected void Page_Load(object sender, EventArgs e)
    {
        JsTools.DefaultButton(this.Page, mCur, doSet.ClientID);
        doNew.Visible = ShowNew;
        (this.Page.Master as IMessageBox).OkClick += new EventHandler(Modules_VoucherNavigator_OkClick);
    }

    void Modules_VoucherNavigator_OkClick(object sender, EventArgs e)
    {
        // called when asked for new Voucher for unexisted DocumentAccountingID
        doNew_Click(null, null);
        mCur.Text = ValueRefused.ToString();
    }

    protected void doFirst_Click(object sender, EventArgs e)
    {
        MoveFirst();
    }
    protected void doPre_Click(object sender, EventArgs e)
    {
        MovePre();
    }
    protected void doNext_Click(object sender, EventArgs e)
    {
        MoveNext();

    }
    protected void doLast_Click(object sender, EventArgs e)
    {
        MoveLast();
    }
    protected void doNew_Click(object sender, EventArgs e)
    {
        OldValue = Value;
        Value = Null.NullInteger;
        if (null != NewClick)
            NewClick(this, null);
    }

    public void MovePre()
    {
        if (string.IsNullOrEmpty(this.TableName) || string.IsNullOrEmpty(this.FieldName))
            Value = Null.NullInteger;
        else
        {
            string where = this.Where == "" ? "" : " AND " + Where;
            string qry = string.Format("SELECT TOP 1 {0} FROM {1} WHERE {0}<{2} {3} ORDER BY {0} DESC", FieldName, TableName, Value, where);
            object obj = DbProxy.Instance.FillScalar(qry);
            int val = Helper.GetInt(obj, Null.NullInteger);
            if (val == Null.NullInteger)
                return;
            Value = val;
        }
    }
    public void MoveNext()
    {
        if (string.IsNullOrEmpty(this.TableName) || string.IsNullOrEmpty(this.FieldName))
            Value = Null.NullInteger;
        else
        {
            string where = this.Where == "" ? "" : " AND " + Where;
            string qry = string.Format("SELECT TOP 1 {0} FROM {1} WHERE {0}>{2} {3} ORDER BY {0} ASC", FieldName, TableName, Value, where);
            object obj = DbProxy.Instance.FillScalar(qry);
            int val = Helper.GetInt(obj, Null.NullInteger);
            if (val == Null.NullInteger)
                return;
            Value = val;
        }
    }
    public void MoveLast()
    {
        if (string.IsNullOrEmpty(this.TableName) || string.IsNullOrEmpty(this.FieldName))
            Value = Null.NullInteger;
        else
        {
            string where = this.Where == "" ? "" : " WHERE " + Where;
            string qry = string.Format("SELECT MAX({0}) FROM {1} {2}", FieldName, TableName, where);
            object obj = DbProxy.Instance.FillScalar(qry);
            Value = Helper.GetInt(obj, Null.NullInteger);
        }
    }
    public void MoveFirst()
    {
        if (string.IsNullOrEmpty(this.TableName) || string.IsNullOrEmpty(this.FieldName))
            Value = Null.NullInteger;
        else
        {
            string where = this.Where == "" ? "" : " AND " + Where;
            string qry = string.Format("SELECT MIN({0}) FROM {1} {2}", FieldName, TableName, where);
            object obj = DbProxy.Instance.FillScalar(qry);
            Value = Helper.GetInt(obj, Null.NullInteger);
        }
    }

    public void CancelNew()
    {
        Value = OldValue;
    }
    #region props
    public int Value
    {
        get
        {
            if (ViewState["Value"] == null)
                return Null.NullInteger;
            return (int)ViewState["Value"];
        }
        set
        {
            if (value != Value)
            {
                ViewState["Value"] = value;
                mCur.Text = value == Null.NullInteger ? "" : value.ToString();
                OnNavigate();
            }
        }
    }
    private int OldValue
    {
        get
        {
            if (ViewState["OldValue"] == null)
                return Null.NullInteger;
            return (int)ViewState["OldValue"];
            //return Tool.GetInt(mCur.Text, Null.NullInteger);
        }
        set
        {
            ViewState["OldValue"] = value;
        }
    }
    public int ValueForNew
    {
        get
        {
            return Helper.GetInt(mCur.Text, Null.NullInteger);
        }
    }

    public bool ShowNew
    {
        get
        {
            if (ViewState["ShowNew"] == null)
                return false;
            return (bool)ViewState["ShowNew"];
        }
        set
        {
            ViewState["ShowNew"] = value;
            if (doNew != null)
                doNew.Visible = value;
        }
    }
    private int ValueRefused
    {
        get
        {
            if (ViewState["ValueRefused"] == null)
                return Null.NullInteger;
            return (int)ViewState["ValueRefused"];
        }
        set
        {
            ViewState["ValueRefused"] = value;
        }
    }
    public string TableName
    {
        get
        {
            return "DocumentAccounting";
        }
    }
    public string FieldName
    {
        get
        {
            if (ViewState["FieldName"] == null)
                return "ID";
            return (string)ViewState["FieldName"];
        }
        set
        {
            ViewState["FieldName"] = value;
        }
    }
    public string Where
    {
        get
        {
            if (ViewState["Where"] == null)
                return "";
            return (string)ViewState["Where"];
        }
        set
        {
            ViewState["Where"] = value;
        }
    }
    #endregion
    public void CareDelete()
    {
        MoveNext();
        if (Value != Null.NullInteger)
            return;
        MovePre();
        if (Value != Null.NullInteger)
            return;
        mCur.Text = "";
    }
    protected void doSet_Click(object sender, EventArgs e)
    {
        if (Value == Null.NullInteger && OldValue != Null.NullInteger)
            return; // it is New Mode, bypass enter
        if (string.IsNullOrEmpty(this.TableName) || string.IsNullOrEmpty(this.FieldName))
            return;
        string where = this.Where == "" ? "" : " AND " + Where;
        string qry = string.Format("SELECT TOP 1 {0} FROM {1} WHERE {0}={2} {3} ORDER BY {0} DESC", FieldName, TableName, Helper.GetInt(mCur.Text, Null.NullInteger), where);
        object obj = DbProxy.Instance.FillScalar(qry);
        int val = Helper.GetInt(obj, Null.NullInteger);
        if (val == Null.NullInteger)
        {
            ValueRefused = Helper.GetInt(mCur.Text, Null.NullInteger);
            if (ValueRefused != Null.NullInteger)
                (this.Page.Master as IMessageBox).ShowOkCancel(WarningLevel.Warning, string.Format("DocumentNumber".Translate()
                    + "{0} " + "DoesNotExistDoYouWantToCreateIt".Translate(), mCur.Text), "Warning".Translate());
            mCur.Text = Value == Null.NullInteger ? "" : Value.ToString();
            Value = Value;
            return;
        }
        Value = val;
        OnNavigate();
    }
    protected void OnNavigate()
    {
        if (Navigate != null)
            Navigate(this, null);
        mIsFixedLabel.Visible = AccController.IsFixedVoucher(Value);
    }
    public void UpdateLabels(bool isFixed, bool isUnbalanced)
    {
        mIsFixedLabel.Visible = isFixed;
        mUnbalancedLabel.Visible = isUnbalanced;
    }

}