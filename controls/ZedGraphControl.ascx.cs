﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZedGraph;

public partial class controls_ZedGraphControl : System.Web.UI.UserControl
{
    protected ZedGraphWeb zGraph;
    public static string TempGraphDir = "~/temp/TempCharts";
    public event ZedGraphWebControlEventHandler InitPane;
    protected virtual void OnInitPane(Graphics g, GraphPane pane)
    {
        if (InitPane != null)
            InitPane(g, pane);
    }
    private void Page_Load(object sender, System.EventArgs e)
    {
        //// Put user code to initialize the page here

        //// -----------------------------------
        //// Remove Old Generated Files
        //Random rnd = new Random();
        //if (rnd.Next(20) < 5)
        //{
        //    System.Threading.ThreadStart t = new System.Threading.ThreadStart(DoRemoveOldFiles);
        //    t.DynamicInvoke(null);
        //}
        //// -----------------------------------
        //// Get Temporary File Path
        //string fileName = GetTempFileName();
        //CreateGraph(fileName);
        //mImage.ImageUrl = string.Concat(TempGraphDir, "/", fileName);
    }
    public void BindGraph()
    {
        //// -----------------------------------
        //// Remove Old Generated Files
        //Random rnd = new Random();
        //if (rnd.Next(20) < 5)
        //{
        //    System.Threading.ThreadStart t = new System.Threading.ThreadStart(DoRemoveOldFiles);
        //    t.DynamicInvoke(null);
        //}
        // -----------------------------------
        // Get Temporary File Path
        string fileName = GetTempFileName();
        CreateGraph(fileName);
        mImage.ImageUrl = string.Concat(TempGraphDir, "/", fileName);
    }

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    ///		Required method for Designer support - do not modify
    ///		the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new System.EventHandler(this.Page_Load);

    }
    #endregion

    /// <summary>
    /// Method to create a <see cref="ZedGraph.GraphPane"/> class for the control.
    /// </summary>
    /// <param name="OutputStream">A <see cref="Stream"/> in which to output the ZedGraph
    /// <see cref="System.Drawing.Image"/>.</param>
    protected bool CreateGraph(string fileName)
    {
        //TODO: fix/verify the height/width values are okay like this
        RectangleF rect = new RectangleF(0, 0, this.Width - 1, this.Height - 1);
        GraphPane pane = new GraphPane(rect, title, xlabel, ylabel);
        pane.ClusterScaleWidth = 100;

        //TODO: add all the goodies from visual mode
        pane.IsShowTitle = this.IsShowTitle;

        Bitmap image = new Bitmap(this.Width, this.Height);
        Graphics g = Graphics.FromImage(image);
        // Use callback to gather more settings
        OnInitPane(g, pane);

        // --------------------------------
        // Add Proxies To Pane
        for (int i = 0; i < mCurves.Count; i++)
            if (mCurves[i] != null)
                pane.CurveList.Add(mCurves[i]);

        if (XLabels != null)
        {
            pane.XAxis.Type = AxisType.Text;
            pane.XAxis.TextLabels = XLabels;
        }
        pane.AxisChange(g);

        // --------------------------------
        // Render the graph to a bitmap
        g.Clear(Color.FromArgb(255, 255, 255, 255));
        pane.Draw(g);

        // --------------------------------
        // Try to Save The Graph To File
        try
        {
            string path = Server.MapPath(TempGraphDir);
            path = Path.Combine(path, fileName);
            image.Save(path, this.ImageFormat);
            return true;
        }
        catch (Exception ex)
        {
            string exStr = ex.Message;
            exStr = ex.StackTrace;
            return false;
        }
        finally
        {
            g.Dispose();
            image.Dispose();
        }
    }

    private string GetTempFileName()
    {
        string lngStr = DateTime.Now.Ticks.ToString();
        string path = Server.MapPath(TempGraphDir);
        string pageName = Path.GetFileNameWithoutExtension(Page.Request.Url.AbsolutePath);
        string ext = this.FileExtension;
        for (int i = 0; i < 10; i++)
        {
            string res = string.Concat(lngStr, pageName, i.ToString(), ext);
            if (!File.Exists(Path.Combine(path, res)))
                return res;
        }
        Random rnd = new Random();
        lngStr = DateTime.Now.Ticks.ToString();
        return string.Concat(lngStr, pageName, rnd.Next(int.MaxValue - 10), ext);
    }
    private void DoRemoveOldFiles()
    {
        string[] files = Directory.GetFiles(Server.MapPath(TempGraphDir));
        if (files != null && files.Length > 0)
        {
            Array.Sort(files);
            for (int i = 0; i < files.Length / 2; i++)
                try
                {
                    File.Delete(files[i]);
                    System.Diagnostics.Debug.WriteLine("Deleted :: " + i + " :: " + files[i]);
                }
                catch (Exception ex)
                {
                    string exStr = ex.Message;
                    exStr = ex.StackTrace;
                }
        }

    }


    #region TableProxy Manipolation
    protected CurveList mCurves = new CurveList();
    public CurveList Curves
    {
        get
        {
            return mCurves;
        }
    }
    /// <summary>
    /// Applied to Pane.XAxis.TextLabels
    /// If this property is set, the Pane.XAxis.Type will be
    /// set to AxisType.Text
    /// </summary>
    public string[] XLabels = null;
    #endregion
    #region Attributes
    /// <summary>
    /// private field to store the control width.  Use the public property
    /// <see cref="Width"/> to access this value.
    /// </summary>
    private int width = 400;
    /// <summary>
    /// Gets or sets the width of the <see cref="ZedGraph.GraphPane.PaneRect"/>.
    /// </summary>
    /// <value>The width in output device pixels</value>
    [Bindable(true), Category("Layout"), NotifyParentProperty(true), DefaultValue(400)]
    public int Width
    {
        get { return width; }
        set { width = value; }
    }

    /// <summary>
    /// private field to store the control height.  Use the public property
    /// <see cref="Height"/> to access this value.
    /// </summary>
    private int height = 250;
    /// <summary>
    /// Gets or sets the Height of the <see cref="ZedGraph.GraphPane.PaneRect"/>.
    /// </summary>
    /// <value>The height in output device pixels</value>
    [Bindable(true), Category("Layout"), NotifyParentProperty(true), DefaultValue(250)]
    public int Height
    {
        get { return height; }
        set { height = value; }
    }

    /// <summary>
    /// private field to store the graph title.  Use the public property
    /// <see cref="Title"/> to access this value.
    /// </summary>
    private string title = string.Empty;
    /// <summary>
    /// Gets or sets the Title of the <see cref="ZedGraph.GraphPane"/>.
    /// </summary>
    /// <value>A title <see cref="string"/></value>
    [Bindable(true), Category("Appearance"), NotifyParentProperty(true), DefaultValue("")]
    public string Title
    {
        get { return title; }
        set { title = value; }
    }

    /// <summary>
    /// private field to store the X axis label.  Use the public property
    /// <see cref="XLabel"/> to access this value.
    /// </summary>
    private string xlabel = string.Empty;
    /// <summary>
    /// Gets or sets the X Axis label of the <see cref="ZedGraph.GraphPane.XAxis"/>.
    /// </summary>
    /// <value>An X axis label <see cref="string"/></value>
    [Bindable(true), Category("Appearance"), NotifyParentProperty(true), DefaultValue("")]
    public string XLabel
    {
        get { return xlabel; }
        set { xlabel = value; }
    }

    /// <summary>
    /// private field to store the Y axis label.  Use the public property
    /// <see cref="YLabel"/> to access this value.
    /// </summary>
    private string ylabel = string.Empty;
    /// <summary>
    /// Gets or sets the Y Axis label of the <see cref="ZedGraph.GraphPane.YAxis"/>.
    /// </summary>
    /// <value>A Y axis label <see cref="string"/></value>
    [Bindable(true), Category("Appearance"), NotifyParentProperty(true), DefaultValue("")]
    public string YLabel
    {
        get { return ylabel; }
        set { ylabel = value; }
    }

    /// <summary>
    /// private field that determines if the pane title is visible.  Use the public property
    /// <see cref="IsShowTitle"/> to access this value.
    /// </summary>
    private bool isshowtitle = true;
    /// <summary>
    /// Gets or sets the value that determines if the <see cref="ZedGraph.GraphPane.Title"/>
    /// is visible.
    /// </summary>
    /// <value>true to show the pane title, false otherwise</value>
    [Bindable(true), Category("Appearance"), NotifyParentProperty(true), DefaultValue("true")]
    public bool IsShowTitle
    {
        get { return isshowtitle; }
        set { isshowtitle = value; }
    }

    /// <summary>
    /// private field that determines the output format for the control.  Use the public property
    /// <see cref="OutputFormat"/> to access this value.
    /// </summary>
    private ZedGraphWeb.ZedGraphWebFormat outputformat = ZedGraphWeb.ZedGraphWebFormat.Jpeg;
    /// <summary>
    /// Gets or sets the value that determines the output format for the control, in the
    /// form of a <see cref="ZedGraphWebFormat"/> enumeration.  This is typically Gif, Jpeg,
    /// Png, or Icon.
    /// </summary>
    /// <value>A <see cref="ZedGraphWebFormat"/> enumeration.</value>
    [Bindable(true), Category("Appearance"), NotifyParentProperty(true), DefaultValue("Jpeg")]
    public ZedGraphWeb.ZedGraphWebFormat OutputFormat
    {
        get { return outputformat; }
        set { outputformat = value; }
    }
    /// <summary>
    /// Gets the <see cref="OutputFormat"/> property, translated to an
    /// <see cref="ImageFormat"/> enumeration.
    /// </summary>
    /// <value>An <see cref="ImageFormat"/> enumeration representing the image type
    /// to be output.</value>
    protected ImageFormat ImageFormat
    {
        get
        {
            switch (outputformat)
            {
                case ZedGraphWeb.ZedGraphWebFormat.Gif:
                    return ImageFormat.Gif;
                case ZedGraphWeb.ZedGraphWebFormat.Jpeg:
                    return ImageFormat.Jpeg;
                case ZedGraphWeb.ZedGraphWebFormat.Icon:
                    return ImageFormat.Icon;
                case ZedGraphWeb.ZedGraphWebFormat.Png:
                    return ImageFormat.Png;
            }
            return ImageFormat.Gif;
        }
    }

    protected string FileExtension
    {
        get
        {
            switch (outputformat)
            {
                case ZedGraphWeb.ZedGraphWebFormat.Gif:
                    return ".gif";
                case ZedGraphWeb.ZedGraphWebFormat.Jpeg:
                    return ".jpg";
                case ZedGraphWeb.ZedGraphWebFormat.Icon:
                    return ".ico";
                case ZedGraphWeb.ZedGraphWebFormat.Png:
                    return ".png";
            }
            return ".gif";
        }
    }
    #endregion

}

public enum GraphMode : byte
{
    Bar,
    Line,
    Curve,
}
public class TableGraphProxy
{
    //public static CurveItem FromTable(string title, GraphMode mode, DataTable table, string xField, string yField, bool xIsDate, bool yIsDate)
    //{
    //    if (table == null || table.Rows.Count == 0)
    //        return null;

    //    // --------------------------------------------
    //    // Get x/y values
    //    double[] xs = new double[table.Rows.Count];
    //    double[] ys = new double[table.Rows.Count];
    //    for (int i = 0; i < xs.Length; i++)
    //    {
    //        xs[i] = (xIsDate) ? Tool.GetXLDays(table.Rows[i][xField]) :
    //            Tool.GetDouble(table.Rows[i][xField]);
    //        ys[i] = (yIsDate) ? Tool.GetXLDays(table.Rows[i][yField]) :
    //            Tool.GetDouble(table.Rows[i][yField]);
    //    }

    //    // --------------------------------------------
    //    // Create the curve item according to GraphMode
    //    // for GraphMode.Curve & GraphMode.Line use LineItem and
    //    // for GraphMode.Bar & GraphMode.BarStack use BarItem
    //    if (mode == GraphMode.Curve || mode == GraphMode.Line)
    //    {
    //        LineItem curve = new LineItem(title, xs, ys, Color.Green, SymbolType.Default);
    //        curve.Line.Width = 1.5F;
    //        curve.Line.Fill = LineFills[NumCreatedProxies++ % LineFills.Length];
    //        if (mode == GraphMode.Curve)
    //        {
    //            curve.Line.IsSmooth = true;
    //            curve.Line.SmoothTension = 0.6F;
    //        }
    //        curve.Symbol.Fill = new Fill(Color.White);
    //        curve.Symbol.Size = 10;

    //        return curve;
    //    }
    //    else // GraphMode.Bar || GraphMode.BarStack
    //    {
    //        BarItem curve = new BarItem(title, xs, ys, Color.SteelBlue);
    //        curve.Bar.Fill = BarFills[NumCreatedProxies++ % BarFills.Length];

    //        return curve;
    //    }
    //}

    //public static CurveItem FromTable(string title, GraphMode mode, DataTable table, string xField, string yField, bool xIsDate, bool yIsDate, string textField, out string[] texts)
    //{
    //    texts = null;
    //    if (table == null || table.Rows.Count == 0)
    //        return null;

    //    // --------------------------------------------
    //    // Get x/y values
    //    double[] xs = new double[table.Rows.Count];
    //    double[] ys = new double[table.Rows.Count];
    //    texts = new string[table.Rows.Count];
    //    for (int i = 0; i < xs.Length; i++)
    //    {
    //        xs[i] = (xIsDate) ? Tool.GetXLDays(table.Rows[i][xField]) :
    //            Tool.GetDouble(table.Rows[i][xField]);
    //        ys[i] = (yIsDate) ? Tool.GetXLDays(table.Rows[i][yField]) :
    //            Tool.GetDouble(table.Rows[i][yField]);
    //        texts[i] = Tool.GetString(table.Rows[i][textField]);
    //    }

    //    // --------------------------------------------
    //    // Create the curve item according to GraphMode
    //    // for GraphMode.Curve & GraphMode.Line use LineItem and
    //    // for GraphMode.Bar & GraphMode.BarStack use BarItem
    //    if (mode == GraphMode.Curve || mode == GraphMode.Line)
    //    {
    //        LineItem curve = new LineItem(title, xs, ys, Color.Green, SymbolType.Default);
    //        curve.Line.Width = 1.5F;
    //        curve.Line.Fill = LineFills[NumCreatedProxies++ % LineFills.Length];
    //        if (mode == GraphMode.Curve)
    //        {
    //            curve.Line.IsSmooth = true;
    //            curve.Line.SmoothTension = 0.6F;
    //        }
    //        curve.Symbol.Fill = new Fill(Color.Red);
    //        curve.Symbol.Size = 10;

    //        return curve;
    //    }
    //    else // GraphMode.Bar || GraphMode.BarStack
    //    {
    //        BarItem curve = new BarItem(title, xs, ys, Color.SteelBlue);
    //        curve.Bar.Fill = BarFills[NumCreatedProxies++ % BarFills.Length];

    //        return curve;
    //    }
    //}
    //public static CurveItem[] FromTables(string[] titles, GraphMode mode, DataTable[] tables, string textField, string yField, out string[] texts)
    //{
    //    GraphMode[] modes = new GraphMode[titles.Length];
    //    string[] textFields = new string[titles.Length];
    //    string[] yFields = new string[titles.Length];
    //    for (int i = 0; i < titles.Length; i++)
    //    {
    //        modes[i] = mode;
    //        textFields[i] = textField;
    //        yFields[i] = yField;
    //    }
    //    return FromTables(titles, modes, tables, textFields, yFields, out texts);
    //}
    ///// <summary>
    ///// Accepts a series of tables, and assumess the the returning CurveItems are
    ///// used in a GraphPane with GraphPane.XAxis.Type set to AxisType.Text.
    ///// Assumed that all arrays have same length
    ///// </summary>
    ///// <param name="titles"></param>
    ///// <param name="modes"></param>
    ///// <param name="tables"></param>
    ///// <param name="textFields"></param>
    ///// <param name="yFields"></param>
    ///// <param name="texts"></param>
    ///// <returns></returns>
    //public static CurveItem[] FromTables(string[] titles, GraphMode[] modes, DataTable[] tables, string[] textFields, string[] yFields, out string[] texts)
    //{
    //    texts = null;
    //    // --------------------------------------------
    //    // Get texts first
    //    System.Collections.Hashtable hash = new System.Collections.Hashtable();
    //    for (int tbl = 0; tbl < tables.Length; tbl++)
    //        for (int row = 0; row < tables[tbl].Rows.Count; row++)
    //        {
    //            string text = Tool.GetString(tables[tbl].Rows[row][textFields[tbl]]);
    //            if (!hash.Contains(text))
    //                hash.Add(text, 0);
    //        }
    //    if (hash.Count == 0)
    //        return null;
    //    texts = new string[hash.Count];
    //    System.Collections.IEnumerator en = hash.Keys.GetEnumerator();
    //    int tx = 0;
    //    while (en.MoveNext())
    //        texts[tx++] = en.Current as string;
    //    Array.Sort(texts);

    //    // --------------------------------------------
    //    // Create CurveItems
    //    CurveItem[] res = new CurveItem[tables.Length];
    //    for (int tbl = 0; tbl < tables.Length; tbl++)
    //    {
    //        double[] ys = new double[texts.Length];
    //        for (int i = 0; i < texts.Length; i++)
    //            ys[i] = GetValue(tables[tbl], textFields[tbl], yFields[tbl], texts[i]);

    //        res[tbl] = FromValus(titles[tbl], modes[tbl], ys, ys);
    //    }

    //    return res;
    //}
    //private static double GetValue(DataTable table, string xField, string yField, string xValue)
    //{
    //    xValue = xValue.ToLower();
    //    for (int i = 0; i < table.Rows.Count; i++)
    //        if (xValue.Equals(Tool.GetString(table.Rows[i][xField]).ToLower()))
    //            return Tool.GetDouble(table.Rows[i][yField]);

    //    return 0;
    //}
    //public static CurveItem[] FromTable(string[] titles, GraphMode mode, DataTable table, string textField, string[] yFields, out string[] texts)
    //{
    //    texts = null;
    //    if (table == null || table.Rows.Count == 0)
    //        return null;
    //    // --------------------------------------------
    //    // Get texts & xs values
    //    texts = new string[table.Rows.Count];
    //    double[] xs = new double[table.Rows.Count];
    //    for (int row = 0; row < table.Rows.Count; row++)
    //    {
    //        texts[row] = Tool.GetString(table.Rows[row][textField]);
    //        xs[row] = row;
    //    }

    //    // --------------------------------------------
    //    // Create CurveItems
    //    CurveItem[] res = new CurveItem[yFields.Length];
    //    for (int f = 0; f < yFields.Length; f++)
    //    {
    //        double[] ys = new double[texts.Length];
    //        for (int i = 0; i < texts.Length; i++)
    //            ys[i] = Tool.GetDouble(table.Rows[i][yFields[f]]);

    //        res[f] = FromValus(titles[f], mode, xs, ys);
    //    }

    //    return res;
    //}

    public static ZedGraph.CurveItem FromValus(string title, GraphMode mode, double[] xs, double[] ys)
    {
        // --------------------------------------------
        // Create the curve item according to GraphMode
        // for GraphMode.Curve & GraphMode.Line use LineItem and
        // for GraphMode.Bar & GraphMode.BarStack use BarItem
        if (mode == GraphMode.Curve || mode == GraphMode.Line)
        {
            LineItem curve = new LineItem(title, xs, ys, Color.Green, SymbolType.Default);
            curve.Line.Width = 1.5F;
            curve.Line.Fill = LineFills[NumCreatedProxies++ % LineFills.Length];
            if (mode == GraphMode.Curve)
            {
                curve.Line.IsSmooth = true;
                curve.Line.SmoothTension = 0.6F;
            }
            curve.Symbol.Fill = new Fill(Color.White);
            curve.Symbol.Size = 10;

            return curve;
        }
        else // GraphMode.Bar || GraphMode.BarStack
        {
            BarItem curve = new BarItem(title, xs, ys, Color.SteelBlue);
            curve.Bar.Fill = BarFills[NumCreatedProxies++ % BarFills.Length];

            return curve;
        }
    }

    #region Static Fields
    private static long NumCreatedProxies = 0;
    public readonly static Fill[] LineFills = new Fill[]{
																new Fill( Color.White, Color.White, 90F )};
    //																new Fill( Color.White, Color.FromArgb( 60, 190, 50), 90F ),
    //																new Fill( Color.White, Color.FromArgb( 160, 230, 145, 205), 90F )};
    public readonly static Fill[] BarFills = new Fill[]{
															   new Fill( Color.RosyBrown, Color.White, Color.RosyBrown),
															   new Fill( Color.RoyalBlue, Color.White, Color.RoyalBlue),
															   new Fill( Color.Green, Color.White, Color.Green),
															   new Fill( Color.Yellow, Color.White, Color.Yellow),
															   new Fill( Color.Purple, Color.White, Color.Purple),
															   new Fill( Color.Fuchsia, Color.White, Color.Fuchsia),
															   new Fill( Color.Red, Color.White, Color.Red),
															   new Fill( Color.DarkBlue, Color.White, Color.DarkBlue),
															   new Fill( Color.DimGray, Color.White, Color.DimGray),
															   new Fill( Color.YellowGreen, Color.White, Color.YellowGreen),
															   new Fill( Color.SteelBlue, Color.White, Color.SteelBlue),
															   new Fill( Color.Gold, Color.White, Color.Gold),
															   new Fill( Color.DeepPink, Color.White, Color.DeepPink),
															   new Fill( Color.Blue, Color.White, Color.Blue),
															   new Fill( Color.Bisque, Color.White, Color.Bisque),
															   new Fill( Color.Azure, Color.White, Color.Azure),
															   new Fill( Color.Indigo, Color.White, Color.Indigo),
															   new Fill( Color.DarkCyan, Color.White, Color.DarkCyan),
															   new Fill( Color.LightSalmon, Color.White, Color.LightSalmon),
															   new Fill( Color.LightYellow, Color.White, Color.LightYellow),
															   new Fill( Color.MediumOrchid, Color.White, Color.MediumOrchid),
															   new Fill( Color.Lime, Color.White, Color.Lime),
															   new Fill( Color.Linen, Color.White, Color.Linen),
															   new Fill( Color.MediumTurquoise, Color.White, Color.MediumTurquoise),
															   new Fill( Color.MediumPurple, Color.White, Color.MediumPurple),
															   new Fill( Color.SandyBrown, Color.White, Color.SandyBrown),
															   new Fill( Color.Salmon, Color.White, Color.Salmon),
															   new Fill( Color.Tan, Color.White, Color.Tan),
															   new Fill( Color.Red, Color.White, Color.Red)};
    public readonly static Fill[] PaneFills = new Fill[]{
														new Fill( Color.WhiteSmoke, Color.Lavender, 0F)};
    public readonly static Fill[] AxisFills = new Fill[]{
																new Fill( Color.FromArgb( 255, 255, 245), Color.FromArgb( 255, 255, 190), 90F )};
    #endregion
}
