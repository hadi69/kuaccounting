﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopicProxy.ascx.cs" Inherits="controls_TopicProxy" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<cc1:ModalPopupExtender ID="mExtender" runat="server" PopupControlID="mPanel" PopupDragHandleControlID="mDrag"
    OkControlID="mCancel" CancelControlID="mCancel" TargetControlID='mDrag' BackgroundCssClass="modalBackground"
    RepositionMode="RepositionOnWindowResize" Y="50">
</cc1:ModalPopupExtender>
<asp:Panel runat="server" ID="mPanel" CssClass="awesome-popup" Width="700px" Style="display: nonew">
    <asp:Panel ID="mDrag" runat="server" CssClass="awesome-popup-title">
        <asp:Label ID='mTitle' runat="server"></asp:Label>
    </asp:Panel>
    <div class="awesome-popup-inner">
        <div class="container-fluid">
            <div class="row">
                <asp:Panel ID="panelValidationErrors" CssClass="col-xs-12" runat="server" Visible="False">
                    <div class="alert alert-danger" role="alert">
                        <asp:Label ID="mError" runat="server"></asp:Label>
                    </div>
                </asp:Panel>
                <div class="col-xs-12 col-sm-6 form-group-container">
                    <div class="form-group">
                        <asp:Label runat="server" Text="Lang.AccountName" CssClass="left-aligned-label col-xs-12 col-sm-4"></asp:Label>
                        <div class="col-xs-12 col-sm-8 form-col-without-padding">
                            <asp:TextBox runat="server" ID="mName" CssClass="form-control input-sm mName"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 form-group-container">
                    <div class="form-group">
                        <asp:Label runat="server" Text="Lang.TopicCode" CssClass="left-aligned-label col-xs-12 col-sm-4"></asp:Label>
                        <div class="col-xs-12 col-sm-8 form-col-without-padding">
                            <asp:TextBox runat="server" ID="mCode" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 form-group-container">
                    <div class="form-group">
                        <asp:Label runat="server" Text="Lang.Nature" CssClass="left-aligned-label col-xs-12 col-sm-4"></asp:Label>
                        <div class="col-xs-12 col-sm-8 form-col-without-padding">
                            <asp:DropDownList ID="mNature" runat="server" CssClass="chosen-select chosen-rtl">
                                <asp:ListItem Value="0" Text="Lang.Debtor"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Lang.Creditor"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Lang.Both"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 form-group-container">
                    <div class="form-group">
                        <asp:Label runat="server" Text="Lang.TopicGroup" CssClass="left-aligned-label col-xs-12 col-sm-4"></asp:Label>
                        <div class="col-xs-12 col-sm-8 form-col-without-padding">
                            <asp:TextBox runat="server" ID="mTopicGroupName" CssClass="TopicGroupName form-control input-sm"></asp:TextBox>
                            <input type='hidden' runat="server" id="mTopicGroupID" class="TopicGroupID" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 form-group-container">
                    <div class="form-group">
                        <asp:Label runat="server" Text="Lang.ShortcutCode" CssClass="left-aligned-label col-xs-12 col-sm-4 padding-0-right"></asp:Label>
                        <div class="col-xs-12 col-sm-8 form-col-without-padding">
                            <asp:TextBox runat="server" ID="mShortcut" CssClass="form-control input-sm" ></asp:TextBox>
                            <asp:TextBox runat="server" ID="mTreePath" ReadOnly="true" CssClass="Value ltr" Width="150px" Height="20px" Visible="false" ></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 form-group-container hidden-xs">
                    <div class="form-group">
                        &nbsp;
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 form-group-container display-none">
                    <div class="form-group">
                        <div class="col-sm-4">&nbsp;</div>
                        <div class="col-xs-12 col-sm-8 form-col-without-padding">
                            <label class="pull-left">
                                <asp:Label runat="server" Text="Lang.ProjectIsNeeded" CssClass="left-aligned-label no-trailer"></asp:Label>
                                <asp:CheckBox runat="server" ID="mProjectNeed" />
                                <div class="switch"></div>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 form-group-container">
                    <div class="form-group">
                        <div class="col-sm-4">&nbsp;</div>
                        <div class="col-xs-12 col-sm-8 form-col-without-padding">
                            <label class="pull-left">
                                <asp:Label runat="server" Text="Lang.PersonIsNeeded" CssClass="left-aligned-label no-trailer"></asp:Label>
                                <asp:CheckBox runat="server" ID="mPersonNeed" />
                                <div class="switch"></div>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 form-group-container display-none">
                    <div class="form-group">
                        <div class="col-sm-4">&nbsp;</div>
                        <div class="col-xs-12 col-sm-8 form-col-without-padding">
                            <label class="pull-left">
                                <asp:Label runat="server" Text="Lang.EquipmentNeed" CssClass="left-aligned-label no-trailer"></asp:Label>
                                <asp:CheckBox runat="server" ID="mEquipmentNeed" />
                                <div class="switch"></div>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 form-group-container display-none">
                    <div class="form-group">
                        <div class="col-sm-4">&nbsp;</div>
                        <div class="col-xs-12 col-sm-8 form-col-without-padding">
                            <label class="pull-left">
                                <asp:Label runat="server" Text="Lang.CostCenterNeed" CssClass="left-aligned-label no-trailer"></asp:Label>
                                <asp:CheckBox runat="server" ID="mCostCenterNeed" />
                                <div class="switch"></div>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 form-group-container display-none">
                    <div class="form-group">
                        <div class="col-sm-4">&nbsp;</div>
                        <div class="col-xs-12 col-sm-8 form-col-without-padding">
                            <label class="pull-left">
                                <asp:Label runat="server" Text="Lang.IsTemporaryTopic" CssClass="left-aligned-label no-trailer"></asp:Label>
                                <asp:CheckBox runat="server" ID="mIsTemp" />
                                <div class="switch"></div>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 form-group-container">
                    <div class="form-group">
                        <div class="col-sm-4">&nbsp;</div>
                        <div class="col-xs-12 col-sm-8 form-col-without-padding">
                            <label class="pull-left">
                                <asp:Label runat="server" Text="Lang.HasSubTopics" CssClass="left-aligned-label no-trailer"></asp:Label>
                                <asp:CheckBox runat="server" ID="mCanHaveSubTree"/>
                                <div class="switch"></div>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 form-group-container display-none" id="AliasDiv">
                    <div class="form-group">
                        <asp:Label ID="ParentAlias" runat="server" Text="Lang.ParentAlias" CssClass="left-aligned-label col-xs-12 col-sm-4 padding-0-right"></asp:Label>
                        <div class="col-xs-12 col-sm-8 form-col-without-padding">
                            <asp:TextBox runat="server" ID="mAlias" CssClass="form-control input-sm" ></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div id="UpdateChildsDiv" class="col-xs-12 col-sm-6 form-group-container">
                    <div class="form-group">
                        <div class="col-sm-2">&nbsp;</div>
                        <div class="col-xs-12 col-sm-10 form-col-without-padding">
                            <label class="pull-left">
                                <asp:Label runat="server" Text="Lang.UpdateChilds" CssClass="left-aligned-label no-trailer"></asp:Label>
                                <asp:CheckBox runat="server" ID="mUpdateChilds"/>
                                <div class="switch"></div>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="dashed margin-5-top margin-10-bottom"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-4 form-group-container">
                    <div class="form-group align-center">
                        <asp:Label runat="server" Text="Lang.Owe" CssClass="left-aligned-label"></asp:Label>
                        <asp:Label runat="server" ID="mOwe" CssClass="label label-primary" />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 form-group-container">
                    <div class="form-group align-center">
                        <asp:Label runat="server" Text="Lang.Due" CssClass="left-aligned-label"></asp:Label>
                        <asp:Label runat="server" ID="mDue" CssClass="label label-primary" />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 form-group-container">
                    <div class="form-group align-center">
                        <asp:Label runat="server" Text="Lang.Remaining" CssClass="left-aligned-label"></asp:Label>
                        <asp:Label runat="server" ID="mRemaining" CssClass="label label-primary" />
                    </div>
                </div>
            </div>
            <div class="row margin-10-top">
                <div class="col-xs-12 align-center">
                    <asp:Button ID="mOk" runat="server" Text="Lang.Confirm F2" OnClick="mOk_Click" CssClass="btn btn-xs btn-success"></asp:Button>
                    <asp:Button ID="mNew" runat="server" Text="Lang.ConfirmAndNew F3" OnClick="mNew_Click" CssClass="btn btn-xs btn-primary"></asp:Button>
                    <asp:Button ID="mCancel" runat="server" Text="Lang.Cancel ESC" CssClass="btn btn-xs btn-primary"/>
                </div>
            </div>
        </div>
        <table width="100%" class="hidden">
            <tr>
                <td class="Name" valign="top">
                    <asp:Label runat="server" ID="mParentsLbl">Lang.AddTo</asp:Label>
                </td>
                <td class="Value" colspan="3">
                    <asp:CheckBoxList ID="mParents" runat="server" CssClass="Value" RepeatColumns="2" Visible="false"
                        Width="100%">
                    </asp:CheckBoxList>
                </td>
            </tr>
            <tr class="hidden">
                <td colspan="2">
                    <asp:Label runat="server">Lang.PeopleRelatedToDocument</asp:Label>
                </td>
                <td colspan="2">
                    <table border="1" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:Label Text="Lang.ChoosePerson" runat="server" />
                                <asp:TextBox ID="PersonnelName" runat="server" CssClass="PersonnelName"></asp:TextBox>
                                <input type='hidden' runat="server" id="PersonnelID" class="PersonnelID" />
                                <asp:Button runat="server" ID="doAddPersonnel" Text='Lang.DoAdd' OnClick="doAddPersonnel_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="mG" AutoGenerateColumns="False" runat="server" BorderStyle="None"
                                    OnRowDataBound="mGrid_RowDataBound" EmptyDataText="Lang.Empty" ShowHeader="False"
                                    OnRowDeleting="mG_RowDeleting">
                                    <Columns>
                                        <asp:BoundField DataField="ID" HeaderText="ID" />
                                        <asp:BoundField DataField="Name" HeaderText="Lang.Name" />
                                        <asp:BoundField DataField="Surname" HeaderText="Lang.Surname" />
                                        <asp:BoundField DataField="Code" HeaderText="Lang.Code" />
                                        <asp:CommandField DeleteText="Lang.ColDel" ShowDeleteButton="True" />
                                    </Columns>
                                    <HeaderStyle CssClass="GridHeader" />
                                    <RowStyle CssClass="GridRow" />
                                    <SelectedRowStyle CssClass="GridRowSelected" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="Name" colspan="2">
                </td>
                <td class="Name" colspan="1">
                </td>
                <td class="Name" colspan="1">
                </td>
            </tr>
            <tr>
                <td class="Name" style="text-align: left">
                </td>
                <td style="text-align: left">
                </td>
            </tr>
        </table>
    </div>
</asp:Panel>
<script>
    $(function () {
        //$('#dialog').hide();
        //alert($('.mName').val());
        $(".mName").unautocomplete();
        $(".mCode").unautocomplete();
        if ($('.mName').val() == '') {
            //autoCompleteFree('Topic', 'topicauto', ".mName", "#justfurfun", null, formatItemNameCode);
            autoCompleteFree('Topic', 'topicauto', ".mCode", "#justfurfun", null, formatItemNameCode, function (data) { return data[2]; });
        }
        autoCompleteFree('TopicGroup', 'penumauto', ".TopicGroupName", ".TopicGroupID", null, formatItemNameCode);
        autoCompleteFree('Personnel', 'personnelaccauto', ".PersonnelName", ".PersonnelID", null, formatItemNameCode);

        // Add keyboard shorcuts to the buttons
        shortcut.add('ESC', function() {
            $("#<%= mCancel.ClientID %>").click();
        });
        shortcut.add('F3', function() {
            $("#<%= mNew.ClientID %>").click();
        });
        shortcut.add('F2', function() {
            $("#<%= mOk.ClientID %>").click();
        });
        function hasSubTree() {
            if (!$("#<%= mCanHaveSubTree.ClientID %>").attr('checked')) {
               // $("#AliasDiv").hide();
                $("#UpdateChildsDiv").hide();
            } else {
               // $("#AliasDiv").show();
                $("#UpdateChildsDiv").show();
            }
        }

        $("#<%= mCanHaveSubTree.ClientID %>").click(hasSubTree);
        hasSubTree();
        $(".mName").focus();
    });
</script>
