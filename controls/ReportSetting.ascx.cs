﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class controls_ReportSetting : System.Web.UI.UserControl
{
    public event EventHandler SaveClick;
    public event EventHandler ReportSettingSelect;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            BindSettings();
        JsTools.HandleDeleteButton(doDelete);
    }
    private void BindSettings()
    {
        mSettings.DataSource = AccController.GetReportSettings(SiteSettings.UserID, this.ReportName);
        mSettings.DataBind();
    }

    protected void doSave_Click(object sender, EventArgs e)
    {
        if (SaveClick != null)
            SaveClick(this, null);
    }
    public string ReportName
    {
        get
        {
            return Helper.GetString(ViewState["ReportName"], null);
        }
        set
        {
            ViewState["ReportName"] = value;
            if (mSettings != null)
                BindSettings();
        }
    }
    public string Title
    {
        get
        {
            return mTitle.Text;
        }
    }
    public ReportSettingInfo SelectedReportSettings
    {
        get
        {
            int id = Helper.GetInt(mSettings.SelectedValue, Null.NullInteger);
            if (id == Null.NullInteger)
                return null;
            return AccController.GetReportSetting(id);
        }
    }
    protected void doDelete_Click(object sender, EventArgs e)
    {
        int id = Helper.GetInt(mSettings.SelectedValue, Null.NullInteger);
        if (AccController.DeleteReportSetting(id))
            BindSettings();
    }
    protected void doSelect_Click(object sender, EventArgs e)
    {
        if (ReportSettingSelect != null)
            ReportSettingSelect(this, null);
    }
    public void Save(string settings)
    {
        if (string.IsNullOrEmpty(ReportName))
            throw new Exception("ReportName could not be empty.");
        ReportSettingInfo info = new ReportSettingInfo();
        info.ReportName = this.ReportName;
        info.Title = this.Title;
        info.Settings = settings;
        info.UserID = SiteSettings.UserID;
        if (AccController.Add(info))
        {
            mTitle.Text = "";
            BindSettings();
            mSettings.SelectedValue = info.ID.ToString();
            if (ReportSettingSelect != null)
                ReportSettingSelect(this, null);
        }
    }
}