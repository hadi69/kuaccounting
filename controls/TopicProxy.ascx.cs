﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stimulsoft.Report;

public partial class controls_TopicProxy : System.Web.UI.UserControl
{
    private TopicInfo mInfo = null;
    private TreeInfo mChild;
    public event EventHandler Insert, Update, CodeChange;
    protected void Page_Load(object sender, EventArgs e)
    {
        mProjectNeed.InputAttributes["class"] = "ios-switch";
        mPersonNeed.InputAttributes["class"] = "ios-switch";
        mEquipmentNeed.InputAttributes["class"] = "ios-switch";
        mIsTemp.InputAttributes["class"] = "ios-switch";
        mCostCenterNeed.InputAttributes["class"] = "ios-switch";
        mCanHaveSubTree.InputAttributes["class"] = "ios-switch";
        mUpdateChilds.InputAttributes["class"] = "ios-switch";
    }

    public void ShowValidationError(string message)
    {
        panelValidationErrors.Visible = mError.Visible = true;
        mError.Text = message;
    }

    public void ClearValidationErrors()
    {
        mError.Text = "";
        panelValidationErrors.Visible = false;
    }

    private string EditTreePath
    {
        get
        {
            if (ViewState["EditTreePath"] == null)
                return "";
            return (string)ViewState["EditTreePath"];
        }
        set
        {
            ViewState["EditTreePath"] = value;
        }
    }
    public string ParentTreePath
    {
        get
        {
            if (ViewState["ParentTreePath"] == null)
                return "";
            return (string)ViewState["ParentTreePath"];
        }
        set
        {
            ViewState["ParentTreePath"] = value;
        }
    }
    public TopicInfo Info
    {
        get
        {
            return mInfo;
        }
    }
    public TreeInfo Child
    {
        get
        {
            return mChild;
        }
    }
    void SavePersonnels(string treePath)
    {
        if (string.IsNullOrEmpty(PersonnelIDs))
            return;
        string[] ids = PersonnelIDs.Split(',');
        for (int i = 0; i < ids.Length; i++)
        {
            AccController.AddTreePersonnel(treePath, Helper.GetInt(ids[i], Null.NullInteger));
        }
        //if ()
    }
    protected void mOk_Click(object sender, EventArgs e)
    {
        ClearValidationErrors();
        if (Helper.GetInt(mTopicGroupID.Value, Null.NullInteger) == Null.NullInteger)
        {
            ShowValidationError(LangProvider.Instance.ParseLang("Lang.PleaseChooseATopicGroup"));
            Show();
            return;
        }
        if (EditTreePath != "")
        {
            #region Edit Mode
            // edit mode : just update the account
            TreeInfo treeInfo = AccController.GetTree(EditTreePath);
            mInfo = AccController.GetTopic(treeInfo.TopicID);
            mInfo.Title = mName.Text;
            string oldCode = mInfo.Code;
            mInfo.Code = mCode.Text;
            mInfo.Nature = (TopicNature)Helper.GetInt(mNature.SelectedValue, 0);
            mInfo.TopicGroupID = Helper.GetInt(mTopicGroupID.Value, Null.NullInteger);
            if (SiteSettings.ProgramID > 0)
                mInfo.ProgramID = SiteSettings.ProgramID;

            treeInfo.PersonNeed = mPersonNeed.Checked;
            treeInfo.ProjectNeed = mProjectNeed.Checked;
            treeInfo.EquipmentNeed = mEquipmentNeed.Checked;
            treeInfo.CanHaveSubTree = mCanHaveSubTree.Checked;
            treeInfo.CostCenterNeed = mCostCenterNeed.Checked;
            treeInfo.Shortcut = mShortcut.Text;
            mInfo.Alias = mAlias.Text;
            mInfo.IsTemp = mIsTemp.Checked;
            AccController.Update(treeInfo);

            if (!AccController.Update(mInfo))
            {
                ShowValidationError(LangProvider.Instance.ParseLang("Lang.TopicUpdateFailedMaybeNameOrCodeIsDuplicate"));
                Show();
                return;
            }
            else
            {
                // update child codes-- topic
                if (oldCode != mInfo.Code)
                {
                    string query = string.Format("UPDATE Topic SET Code = '{0}' + SUBSTRING(Code, LEN('{1}') + 1, 4000), TopicGroupID={2}, Nature={3} WHERE Code LIKE '{1}%'"
                        , mInfo.Code, oldCode, mInfo.TopicGroupID, (int)mInfo.Nature);
                    if (mInfo.ProgramID <=0) // default program = NULL (See SiteSettings.cs)
                        query += " AND ProgramID IS NULL";
                    else
                        query += " AND ProgramID = " + mInfo.ProgramID;
                    DbProxy.Instance.ExecuteCommand(query);
                }
                // update child codes --- tree
                if (mUpdateChilds.Checked)
                {
                    string qry = string.Format("UPDATE Topic SET TopicGroupID={1}, Nature={2} WHERE Code LIKE '{0}%'"
                   , oldCode, mInfo.TopicGroupID, (int)mInfo.Nature);
                    if (mInfo.ProgramID <= 0) // default program = NULL (See SiteSettings.cs)
                        qry += " AND ProgramID IS NULL";
                    else
                        qry += " AND ProgramID = " + mInfo.ProgramID;
                    DbProxy.Instance.ExecuteCommand(qry);

                    qry = string.Format("UPDATE Trees SET PersonNeed={0}, ProjectNeed={1}, EquipmentNeed={2}, CostCenterNeed={3}  WHERE TreePath LIKE {4} + '-%'"
                        , treeInfo.PersonNeed ? 1 : 0, treeInfo.ProjectNeed ? 1 : 0, treeInfo.EquipmentNeed ? 1 : 0, treeInfo.CostCenterNeed ? 1 : 0, Helper.QoutedString(treeInfo.TreePath));
                     DbProxy.Instance.RunQuery(qry);

                }
            }
            if (Update != null)
                Update(this, null);
            if (oldCode != mInfo.Code)
                if (CodeChange != null)
                    CodeChange(this, null);
            return;
            #endregion
        }

        if (mParents.Items.Count == 0 || mPanel.Visible == false || string.IsNullOrEmpty(ParentTreePath))
        {
            // It is going to be added to root (ie. it is a Program)
            #region no parent
            string error = AccController.AddTopicTree(mName.Text, mCode.Text
                , (TopicNature)Helper.GetInt(mNature.SelectedValue, 1), Helper.GetInt(mTopicGroupID.Value, Null.NullInteger)
                , SiteSettings.ProgramID
                , mPersonNeed.Checked, mProjectNeed.Checked, mEquipmentNeed.Checked, mCanHaveSubTree.Checked, mShortcut.Text, mAlias.Text, ParentTreePath
                , out mInfo, out mChild, mCostCenterNeed.Checked);
            if (!string.IsNullOrEmpty(error))
            {
                ShowValidationError(error);
                Show();
                return;
            }
            SavePersonnels(mChild.TreePath);
            if (Insert != null)
                Insert(this, null);
            #endregion
            if (mInfo != null && mInfo.IsTemp != mIsTemp.Checked)
            {
                mInfo.IsTemp = mIsTemp.Checked;
                AccController.Update(mInfo);
            }
            return;
        }
        bool first = true;
        for (int i = 0; i < mParents.Items.Count; i++)
        {
            if (!mParents.Items[i].Selected)
                continue;
            ParentTreePath = mParents.Items[i].Value;
            string error = AccController.AddTopicTree(mName.Text, mCode.Text
                , (TopicNature)Helper.GetInt(mNature.SelectedValue, 1), Helper.GetInt(mTopicGroupID.Value, Null.NullInteger)
                , SiteSettings.ProgramID
                , mPersonNeed.Checked, mProjectNeed.Checked, mEquipmentNeed.Checked, mCanHaveSubTree.Checked, mShortcut.Text, mAlias.Text, ParentTreePath
                , out mInfo, out mChild, mCostCenterNeed.Checked);
            if (first)
            {
                first = false;
                if (mCode.Text.Trim().Length > 0)
                    mName.Text = "";
                if (mInfo != null && mInfo.IsTemp != mIsTemp.Checked)
                {
                    mInfo.IsTemp = mIsTemp.Checked;
                    AccController.Update(mInfo);
                }
            }
            if (!string.IsNullOrEmpty(error))
            {
                ShowValidationError(error);
                Show();
                return;
            }

            SavePersonnels(mChild.TreePath);
            if (Insert != null)
                Insert(this, null);
        }
    }
    protected void mNew_Click(object sender, EventArgs e)
    {
        //TreeInfo treeInfo = AccController.GetTree(EditTreePath);
        //if (treeInfo != null)
        mOk_Click(null, null);
        if (mError.Text == "")
            Add(ParentTreePath, AccController.GetTreePartialPath(ParentTreePath));
    }
    public void Edit(string editTreePath)
    {
        mParents.Visible = mParentsLbl.Visible = false;
        // mAddChilds.Visible = false;
        EditTreePath = editTreePath;
        PersonnelIDs = "";
        mPersonNeed.Checked = mProjectNeed.Checked = mEquipmentNeed.Checked = mIsTemp.Checked = mCanHaveSubTree.Checked = false;
        mNew.Visible = false;
        if (EditTreePath != "")
        {
            AccController.UpdateDueOwe(EditTreePath);
            TreeInfo treeInfo = AccController.GetTree(EditTreePath);
            TopicInfo mInfo = AccController.GetTopic(treeInfo.TopicID);
            mName.Text = mInfo.Title;
            mCode.Text = mInfo.Code;
            mNature.SelectedValue = ((int)mInfo.Nature).ToString();
            mPersonNeed.Checked = treeInfo.PersonNeed;
            mProjectNeed.Checked = treeInfo.ProjectNeed;
            mEquipmentNeed.Checked = treeInfo.EquipmentNeed;
            mCostCenterNeed.Checked = treeInfo.CostCenterNeed;
            mIsTemp.Checked = mInfo.IsTemp;
            mCanHaveSubTree.Checked = treeInfo.CanHaveSubTree;
            mShortcut.Text = treeInfo.Shortcut;
            mAlias.Text = mInfo.Alias;
            if (treeInfo.Depth == 0)
            {
                mTitle.Text = "EditTotalAccount".Translate() + mInfo.Title;
                // mNature.Enabled = true;
                //mProgramName.Enabled = true;
            }
            else if (treeInfo.Depth == 1)
            {
                mTitle.Text = "EditMoeinAccount".Translate() + mInfo.Title;
                //mNature.Enabled = true;
                // mProgramName.Enabled = false;
            }
            else if (treeInfo.Depth > 1)
            {
                mTitle.Text = "EditJozAccount".Translate() + mInfo.Title;
                // mNature.Enabled = false;
                // mProgramName.Enabled = false;
            }
            mNature.Enabled = true;
            mTreePath.Text = EditTreePath;
            mTopicGroupID.Value = mInfo.TopicGroupID.ToString();
            mTopicGroupName.Text = mInfo.TopicGroupName;
            mOwe.Text = treeInfo.Owe.ToString("N0");
            mDue.Text = treeInfo.Due.ToString("N0");
            mRemaining.Text = treeInfo.Remaining.ToString("N0");

            BindPersonnel();

            Show();
        }
    }

    public void Add(string parentTreePath, string parentName)
    {
        mParents.Visible = mParentsLbl.Visible = false;// true;
        // mAddChilds.Visible = true;
        EditTreePath = "";
        ParentTreePath = parentTreePath;

        mName.Text = "";
        mCode.Text = "";
        mOwe.Text = mDue.Text = mRemaining.Text = "0";
        mNew.Visible = true;
        // show automated code
        TreeInfo treeInfo = AccController.GetTree(parentTreePath);
        if (treeInfo != null)
        {
            List<TreeInfo> childs = AccController.GetTrees(treeInfo);
            int numChars = Helper.GetInt(DbProxy.Instance.GetConfig("TopicChars", -1), 2);
            string separator = DbProxy.Instance.GetConfig("TopicSeparator", "");
            if (childs == null || childs.Count == 0)
                mCode.Text = treeInfo.TopicCode + separator + "1".PadLeft(numChars, '0');
            else
            {
                int max = 0;
                string parentCode = treeInfo.TopicCode;
                for (int i = 0; i < childs.Count; i++)
                {
                    if (separator.Length == 1)
                    {
                        string[] split = childs[i].TopicCode.Split(separator[0]);
                        int code = Helper.GetInt(split[split.Length - 1], Null.NullInteger);
                        if (code > max)
                            max = code;
                    }
                    else try
                        {
                            int code = Helper.GetInt(childs[i].TopicCode.Substring(parentCode.Length + separator.Length), Null.NullInteger);
                            if (code > max)
                                max = code;
                        }
                        catch { }
                }
                max++;
                //if (code != Null.NullInteger)
                mCode.Text = treeInfo.TopicCode + separator + max.ToString().PadLeft(numChars, '0');
            }
            TopicInfo topic = AccController.GetTopic(treeInfo.TopicID);
            if (topic != null && topic.TopicGroupID > 0)
            {
                mTopicGroupName.Text = topic.TopicGroupName;
                mTopicGroupID.Value = topic.TopicGroupID.ToString();
            }
        }

        if (treeInfo == null)
        {
            mTitle.Text = "AddTotalAccount".Translate();
            //mProgramName.Enabled = true;
        }
        else if (treeInfo.Depth == 0)
        {
            mTitle.Text = "AddMoeinAccountTo".Translate() + " " + parentName;
            // mProgramName.Enabled = false;
        }
        else if (treeInfo.Depth >= 1)
        {
            mTitle.Text = "AddJozAccountTo".Translate() + " " + parentName;

            mNature.Enabled = false;
            // mProgramName.Enabled = false;
        }
        mNature.Enabled = true;
        if (treeInfo != null)
        {
            TopicInfo topic = AccController.GetTopic(treeInfo.TopicID);
            mNature.SelectedValue = ((int)topic.Nature).ToString();
        }
        BindParents(treeInfo);
        // Clear personnel
        PersonnelIDs = "";
        BindPersonnel();

        Show();
    }
    private void BindParents(TreeInfo treeInfo)
    {
        if (treeInfo == null)
        {
            mParents.Visible = mParentsLbl.Visible = false;
            mParents.DataSource = null;
            mParents.DataBind();
            //mAddChilds.Visible = false;
        }
        else
        {
            mParents.Visible = mParentsLbl.Visible = false;// true;
            // mAddChilds.Visible = true;
            List<TreeInfo> parents = AccController.GetTrees(treeInfo.Depth, treeInfo.ParentTreePath);
            mParents.DataSource = parents;
            mParents.DataTextField = "TopicNameCode";
            mParents.DataValueField = "TreePath";
            mParents.DataBind();
            for (int i = 0; i < mParents.Items.Count; i++)
            {
                if (mParents.Items[i].Value == treeInfo.TreePath)
                    mParents.Items[i].Selected = true;
            }
        }
    }
    void Show()
    {
        mExtender.Show();
        string script = @"
        <script>
         $(function () {
             $('#dialog').attr('title',  $('#mTitle').html());
             //$('#mTitle').hide();
             $('#dialog').dialog();
         });
  </script>".Replace("mTitle", mTitle.ClientID); ;
        Page.RegisterStartupScript("fuckTurkey", script);
    }

    void BindPersonnel()
    {
        if (string.IsNullOrEmpty(EditTreePath))
            mG.DataSource = AccController.GetTempPersonnel(PersonnelIDs);
        else
            mG.DataSource = AccController.GetTreePersonnel(EditTreePath);
        mG.DataBind();
    }
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView row = e.Row.DataItem as System.Data.DataRowView;
            e.Row.Cells[0].Text = row["ID"].ToString();

            JsTools.HandleDeleteButton(e, null);
        }
    }
    protected void mG_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Helper.GetInt(mG.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        if (string.IsNullOrEmpty(EditTreePath))
        {
            PersonnelIDs = ("," + PersonnelIDs + ",").Replace("," + id + ",", "").Trim(',');
            BindPersonnel();
        }
        else
        {
            if (AccController.RemoveTreePersonnel(id))
                BindPersonnel();
            else
                ShowValidationError("Error!");
        }
        Show();
    }
    protected void doAddPersonnel_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(PersonnelID.Value))
        {
            if (!string.IsNullOrEmpty(EditTreePath))
            {
                if (AccController.AddTreePersonnel(EditTreePath, Helper.GetInt(PersonnelID.Value, Null.NullInteger)))
                {
                    PersonnelID.Value = "";
                    PersonnelName.Text = "";
                    BindPersonnel();
                }
            }
            else
            {
                if (string.IsNullOrEmpty(PersonnelIDs))
                    PersonnelIDs = PersonnelID.Value;
                else
                    PersonnelIDs += "," + PersonnelID.Value;
                PersonnelID.Value = "";
                PersonnelName.Text = "";
            }
            BindPersonnel();
        }
        Show();
    }
    private string PersonnelIDs
    {
        get
        {
            return Helper.GetString(ViewState["PersonnelIDs"], "");
        }
        set
        {
            ViewState["PersonnelIDs"] = value;
        }
    }

}