﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VoucherNavigator.ascx.cs" Inherits="controls_VoucherNavigator" %>
<table cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td class="Name" style="text-align: left">
            <asp:Label Text="Lang.DocumentNumber" runat="server" />
        </td>
        <td style="height: 24px">
            <asp:Button ID="doFirst" runat="server" Text="<<" CssClass="Button" Width="25px"
                OnClick="doFirst_Click" ToolTip="Lang.First" AccessKey="/" />
        </td>
        <td style="height: 24px">
            <asp:Button ID="doPre" runat="server" Text="<" OnClick="doPre_Click" CssClass="Button"
                Width="25px" ToolTip="Lang.Prev" AccessKey="." />
        </td>
        <td style="height: 24px">
            <asp:TextBox ID="mCur" runat="server" Text="" CssClass="Text mCur" Width="70px" />
        </td>
        <td style="height: 24px">
            <asp:Button ID="doNext" runat="server" Text=">" OnClick="doNext_Click" CssClass="Button"
                Width="25px" ToolTip="Lang.Next" AccessKey="و" />
        </td>
        <td style="height: 24px">
            <asp:Button ID="doLast" runat="server" Text=">>" OnClick="doLast_Click" CssClass="Button"
                Width="25px" ToolTip="Lang.Last" AccessKey="ئ" /><div style="visibility: hidden; display: none">
                    <asp:Button ID="doSet" runat="server" OnClick="doSet_Click" CssClass='doSet' /></div>
        </td>
        <td style="height: 24px">
            <asp:Button ID="doNew" runat="server" Text=">*" OnClick="doNew_Click" CssClass="Button"
                Width="25px" ToolTip="Lang.FSNew" Visible="false" AccessKey="د" />
        </td>
        <td class="Red">
            <asp:Label runat="server" ID='mIsFixedLabel' Text="Lang.IsFixed" Visible="false"></asp:Label></td>
        <td class="Red">
            <asp:Label runat="server" ID='mUnbalancedLabel' Text="Lang.UnbalancedLabel" Visible="false"></asp:Label></td>
    </tr>
</table>
<script>
    $(function () {
        $('.mCur').keyup(function (e) {
            if (e.keyCode == 40) {
                $('.doSet').click();
                return false;
            }
        });
    });
</script>