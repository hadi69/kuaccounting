﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class controls_PageHead : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Text = LangProvider.Instance.ParseLang(Text);
    }

    public string Text { get; set; }
}