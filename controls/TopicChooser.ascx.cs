﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class controls_TopicChooser : System.Web.UI.UserControl
{
    public event EventHandler TopicSelected;
    protected void Page_Load(object sender, EventArgs e)
    {
        mError.Visible = false;
    }

    public int SelectedTopicID
    {
        get
        {
            if (ViewState["SelectedTopicID"] == null)
                return Null.NullInteger;
            return (int)ViewState["SelectedTopicID"];
        }
        set
        {
            ViewState["SelectedTopicID"] = value;
        }
    }

    protected void mOkName_Click(object sender, EventArgs e)
    {
        TopicInfo info = AccController.GetTopicByTitle(mName.Text, SiteSettings.ProgramID);
        if (info == null)
        {
            Show();
            mError.Visible = true;
            return;
        }
        SelectedTopicID = info.ID;
        if (TopicSelected != null)
            TopicSelected(this, null);
    }
    protected void mOkCode_Click(object sender, EventArgs e)
    {
        TopicInfo info = AccController.GetTopicByCode(mCode.Text, SiteSettings.ProgramID);
        if (info == null)
        {
            Show();
            mError.Visible = true;
            return;
        }
        SelectedTopicID = info.ID;
        if (TopicSelected != null)
            TopicSelected(this, null);
    }

    public void Show()
    {
        mCode.Text = "";
        mName.Text = "";
        mExtender.Show();
    }
}