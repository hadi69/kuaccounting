﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class controls_DropDownMenu : System.Web.UI.UserControl
{
    protected DataTable reports;
    protected void Page_Load(object sender, EventArgs e)
    {
        reports = DbProxy.Instance.FillData(
            SiteSettings.ReplaceOptions("SELECT ID, ReportTitle FROM AccReport WHERE __PrOgrAmconD__ ORDER BY ReportTitle"), "Rep");
        if (reports != null && reports.Rows.Count > 0)
        {
            mCustomReports.Visible = true;
        }
    }
}