﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class controls_PeriodSelector : System.Web.UI.UserControl
{
    public event EventHandler PeriodsChange;
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    void BindPeriods(List<PeriodInfo> periods)
    {
        if (periods == null || periods.Count == 0)
        {
            mGrid.DataSource = null;
        }
        else
        {
            mGrid.DataSource = periods;
            panelTitle.Visible = true;
        }
        mGrid.DataBind();
    }

    public List<PeriodInfo> Periods
    {
        get
        {
            if (mGrid.Rows.Count == 0)
                return null;
            List<PeriodInfo> periods = new List<PeriodInfo>();
            for (int i = 0; i < mGrid.Rows.Count; i++)
            {
                PeriodInfo info = new PeriodInfo();
                info.ID = Helper.GetInt(mGrid.Rows[i].Cells[0].Text, Null.NullInteger);
                info.FromDate = Helper.ParseDate(mGrid.Rows[i].Cells[1].Text, DateTime.Now);
                info.ToDate = Helper.ParseDate(mGrid.Rows[i].Cells[2].Text, DateTime.Now);
                info.Title = mGrid.Rows[i].Cells[3].Text;
                periods.Add(info);
            }
            periods.Sort();
            return periods;
        }
        set
        {
            BindPeriods(value);
        }
    }

    protected void addPeriod_Click(object sender, EventArgs e)
    {
        PeriodInfo info = new PeriodInfo();
        info.ID = 1;
        info.FromDate = Helper.ParseDate(fromDate.Text, Null.NullDate);
        info.ToDate = Helper.ParseDate(toDate.Text, Null.NullDate);
        if (Null.IsNull(info.FromDate))
            info.FromDate = DateTime.Now.AddYears(-100);
        if (Null.IsNull(info.ToDate))
            info.ToDate = DateTime.Now;
        info.Title = title.Text;
        if (string.IsNullOrEmpty(info.Title))
        {
            System.Globalization.PersianCalendar calendar = new System.Globalization.PersianCalendar();
            info.Title = string.Format("From".Translate() + "{0} " + "To".Translate() + " {1}", calendar.GetYear(info.FromDate), calendar.GetYear(info.ToDate));
        }
        List<PeriodInfo> periods = this.Periods;
        if (periods != null && periods.Count > 0)
            for (int i = 0; i < periods.Count; i++)
            {
                if (periods[i].ID >= info.ID)
                    info.ID = periods[i].ID + 1;
            }
        else
            periods = new List<PeriodInfo>();
        periods.Add(info);
        periods.Sort();
        BindPeriods(periods);
        if (PeriodsChange != null)
            PeriodsChange(this, null);
    }
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            PeriodInfo info = e.Row.DataItem as PeriodInfo;
            if (info != null)
            {
                // ---------------------------------
                // Set the ID as Text to Cells[0]
                e.Row.Cells[0].Text = info.ID.ToString();
                e.Row.Cells[1].Text = Helper.FormatDate(info.FromDate, null);
                e.Row.Cells[2].Text = Helper.FormatDate(info.ToDate, null);
                e.Row.Cells[3].Text = info.Title;
            }

            TableCell cc = e.Row.Cells[e.Row.Cells.Count - 1];
            LinkButton delete = (LinkButton)cc.Controls[0];
            delete.Attributes.Add("OnClick", string.Format("return ConfirmDel('{0}');", "AreYouSureYouWantToDeleteThisPeriod"));
        }
    }
    protected void mGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (e.RowIndex < 0)
            return;
        int id = Helper.GetInt(mGrid.Rows[e.RowIndex].Cells[0].Text, Null.NullInteger);
        if (Null.NullInteger == id)
            return;
        List<PeriodInfo> periods = this.Periods;
        if (periods != null && periods.Count > 0)
            for (int i = 0; i < periods.Count; i++)
            {
                if (periods[i].ID == id)
                {
                    periods.RemoveAt(i);
                    break;
                }
            }
        BindPeriods(periods);
        if (PeriodsChange != null)
            PeriodsChange(this, null);
    }
}
public class PeriodInfo : IComparable<PeriodInfo>
{
    private int mID;
    private DateTime mFromDate;
    private DateTime mToDate;
    private string mTitle;

    public string GetText()
    {
        return string.Format("{0}_{1}_{2}_{3}", mID, mFromDate.ToOADate(), mToDate.ToOADate(), mTitle.Replace("_", "mnJerHelLm"));
    }
    public void SetText(string csv)
    {
        if (csv != null)
        {

            string[] words = csv.Split('_');
            if (words.Length == 4)
            {
                mID = Helper.GetInt(words[0], mID);
                mFromDate = DateTime.FromOADate(Helper.GetDouble(words[1], mFromDate.ToOADate()));
                mToDate = DateTime.FromOADate(Helper.GetDouble(words[2], mToDate.ToOADate()));
                mTitle = words[3].Replace("mnJerHelLm", "_");
            }
        }
    }
    public int ID
    {
        get { return mID; }
        set { mID = value; }
    }

    public DateTime FromDate
    {
        get { return mFromDate; }
        set { mFromDate = value; }
    }

    public DateTime ToDate
    {
        get { return mToDate; }
        set { mToDate = value; }
    }

    public string Title
    {
        get { return mTitle; }
        set { mTitle = value; }
    }


    #region IComparable<PeriodInfo> Members

    public int CompareTo(PeriodInfo other)
    {
        return this.mFromDate.CompareTo(other.mFromDate);
    }

    #endregion

}