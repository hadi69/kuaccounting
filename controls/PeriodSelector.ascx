﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PeriodSelector.ascx.cs" Inherits="controls_PeriodSelector" %>

<div class="well padding-5">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">
                    <strong>Lang.Periods</strong>
                    Lang.headline
                </asp:Label>
                <div class="col-xs-12 col-sm-8 form-col-without-padding">
                    <asp:TextBox ID="title" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                <div class="col-xs-12 col-sm-8 form-col-without-padding">
                    <asp:TextBox ID="fromDate" runat="server" CssClass="dateSelect form-control input-sm" />
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
            <div class="form-group">
                <asp:Label CssClass="left-aligned-label control-label col-xs-12 col-sm-4" runat="server">Lang.ToDate</asp:Label>
                <div class="col-xs-12 col-sm-8 form-col-without-padding">
                    <asp:TextBox ID="toDate" runat="server" CssClass="dateSelect form-control input-sm" />
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 form-group-container">
            <div class="form-group">
                <asp:Button ID="addPeriod" runat="server" Text="Lang.NewPeriod" OnClick="addPeriod_Click" CssClass="btn btn-xs btn-primary btn-block" />
            </div>
        </div>
    </div>
</div>
<div class="row">
    <asp:Panel ID="panelTitle" class="col-xs-12" runat="server" Visible="False" EnableViewState="False">
        <h5 class="dashed-bottom padding-10-bottom margin-0-top yekan-family">
            <asp:Label runat="server" Text="Lang.Periods"></asp:Label>
        </h5>
    </asp:Panel>
    <div class="col-xs-12">
        <asp:GridView ID="mGrid" runat="server" AutoGenerateColumns="False" OnRowDataBound="mGrid_RowDataBound"
            OnRowDeleting="mGrid_RowDeleting" CssClass="table table-bordered table-condensed zebra">
            <Columns>
                <asp:BoundField DataField="ID" ReadOnly="True" Visible="False" />
                <asp:BoundField DataField="FromDate" HeaderText="Lang.FromDate" ReadOnly="True" />
                <asp:BoundField DataField="ToDate" HeaderText="Lang.ToDate" ReadOnly="True" />
                <asp:BoundField DataField="Title" HeaderText="Lang.Title" ReadOnly="True" />
                <asp:CommandField DeleteText="Lang.ColDel" ShowDeleteButton="True">
                    <ItemStyle CssClass="deleteButton" />
                </asp:CommandField>
            </Columns>
        </asp:GridView>
    </div>
</div>
