﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportSetting.ascx.cs" Inherits="controls_ReportSetting" %>

<div class="well padding-5 margin-5-bottom">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-5 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" Text="Lang.SaveCurrentSettings" CssClass="left-aligned-label control-label col-xs-12 col-sm-4" AssociatedControlID="mTitle"></asp:Label>
                <div class="col-xs-12 col-sm-5 form-col-without-padding">
                    <asp:TextBox ID="mTitle" runat="server" CssClass="form-control input-sm" Width="200"></asp:TextBox>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <asp:Button runat="server" ID="doSave" CssClass="btn btn-sm btn-primary btn-block" Text='Lang.SaveQuery' OnClick="doSave_Click" />
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" Text="Lang.CallPreviousSettings" CssClass="left-aligned-label control-label col-xs-12 col-sm-6" AssociatedControlID="mTitle"></asp:Label>
                <div class="col-xs-12 col-sm-6 form-col-without-padding">
                    <asp:DropDownList ID="mSettings" runat="server" DataTextField="Title" DataValueField="ID" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 form-group-container">
            <div class="col-xs-6">
                <asp:Button runat="server" ID="doSelect" CssClass="btn btn-sm btn-primary btn-block" Text='Lang.Select' OnClick="doSelect_Click" />
            </div>
            <div class="col-xs-6">
                <asp:Button runat="server" ID="doDelete" CssClass="btn btn-sm btn-danger btn-block" Text='Lang.ColDel' OnClick="doDelete_Click"/>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        makeChosen();
    });
</script>
