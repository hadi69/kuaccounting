﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class controls_pill : System.Web.UI.UserControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Label10.Text = this.Text;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // This line translates the text in "this.Text" property. This is neccessary for translating control's "a[title]".
        Text = LangProvider.Instance.ParseLang(Text);
    }

    public string Access { get; set; }
    public string Level { get; set; }
    public string Href { get; set; }
    public string IconClass { get; set; }
    public string Text { get; set; }
    public bool HasIcon { get { return !string.IsNullOrEmpty(IconClass); } }
}