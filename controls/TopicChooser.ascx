﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopicChooser.ascx.cs" Inherits="controls_TopicChooser" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<cc1:ModalPopupExtender ID="mExtender" runat="server" PopupControlID="mPanel" PopupDragHandleControlID="mDrag"
    OkControlID="mCancel" CancelControlID="mCancel" TargetControlID='mDrag' BackgroundCssClass="modalBackground"
    RepositionMode="RepositionOnWindowResize" Y="50">
</cc1:ModalPopupExtender>
<asp:Panel runat="server" ID="mPanel" CssClass="modalPopup" Width="600px" Style="display: none">
    <asp:Panel ID="mDrag" runat="server" CssClass="modalPopupDrag" Width="100%" Height="20">
        <asp:Label Text="Lang.ChooseTopic" runat="server" />
    </asp:Panel>
    <table border="0" width="100%">
        <tr>
            <td class="Name">
                <asp:Label Text="Lang.ChooseByAccountName" runat="server" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="mName" CssClass="mName1"></asp:TextBox>
            </td>
            <td class="Name">
                <asp:Button ID='mOkName' runat="server" Text='Lang.Confirm' OnClick="mOkName_Click" CssClass="Button" Width="60px" /></td>
        </tr>
        <tr>
            <td class="Name">
                <asp:Label Text="Lang.ChooseByAccountCode" runat="server" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="mCode" CssClass="mCode1"></asp:TextBox>
            </td>
            <td class="Name">
                <asp:Button ID='mOkCode' runat="server" Text='Lang.Confirm' OnClick="mOkCode_Click" CssClass="Button"
                    Width="60px" /></td>
        </tr>
        <tr>
            <td class="Name" colspan="2">
                <asp:Label ID="mError" runat="server" Text="Lang.CodeOrNameDoesNotExist" CssClass="Error"></asp:Label></td>
            <td class="Name" colspan="1">
                <asp:Button ID='mCancel' runat="server" Text='Lang.Cancel' CssClass="Button" Width="60px" /></td>
        </tr>
    </table>
</asp:Panel>
<script>
    $(function () {
        //$('#dialog').hide();
        autoCompleteFree('Topic', 'titlecodeauto', ".mName1", "#justfurfun", null, formatItemNameCode);
        autoCompleteFree('Topic', 'titlecodeauto', ".mCode1", "#justfurfun", null, formatItemNameCode, function (data) { return data[2]; });
    });
</script>
