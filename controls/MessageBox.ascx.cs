﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class controls_MessageBox : System.Web.UI.UserControl
{
    public event EventHandler OkClick;
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public void Show()
    {
        popupMsgExtender.Show();
    }
    public void Show(WarningLevel warningLevel, string message)
    {
        switch (warningLevel)
        {
            case WarningLevel.Success:
                Show(warningLevel, message, LangProvider.Instance["SuccessTitle"]);
                break;
            case WarningLevel.Warning:
                Show(warningLevel, message, LangProvider.Instance["WarningTitle"]);
                break;
            case WarningLevel.Error:
                Show(warningLevel, message, LangProvider.Instance["ErrorTitle"]);
                break;
            default:
                throw new Exception(string.Format("WarningLevel.{0} Not Supported", warningLevel.ToString()));
                break;
        }
    }
    public void Show(WarningLevel warningLevel, string message, string title)
    {
        popupMsgTitle.Text = title;
        popupMsgMessage.Text = message;
        popupMsgExtender.Show();
    }
    public string Title
    {
        get
        {
            return popupMsgTitle.Text;
        }
        set
        {
            popupMsgTitle.Text = value;
        }
    }
    public string Message
    {
        get
        {
            return popupMsgMessage.Text;
        }
        set
        {
            popupMsgMessage.Text = value;
        }
    }
    public string TargetControlID
    {
        get
        {
            return popupMsgExtender.TargetControlID;
        }
        set
        {
            popupMsgExtender.TargetControlID = value;
        }
    }
    public Unit Width
    {
        get
        {
            return popupMsgPanel.Width;
        }
        set
        {
            popupMsgPanel.Width = value;
        }
    }
    public bool OkCancelMode
    {
        get
        {
            return popupMsgOk.Visible;
        }
        set
        {
            popupMsgOk.Visible = value;
        }
    }
    protected void popupMsgOk_Click(object sender, EventArgs e)
    {
        if (null != OkClick)
            OkClick(this, null);
    }
}