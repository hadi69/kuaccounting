﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DropDownMenu.ascx.cs" Inherits="controls_DropDownMenu" %>
<ul class="nav navbar-nav main-menu">
    <li class="dropdown">
        <a href="#" class="menua dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" access="mainmenu,sys" level="3">
            <asp:Label runat="server">Lang.MSys</asp:Label>
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">            
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=MonetaryUnit&menu=sysa" level="3">
                    <img alt="Lang.Project" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-monetaryunit-16" />
                    <asp:Label runat="server">
                        Lang.MonetaryUnit
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Conversion&menu=sysa" level="3">
                    <img alt="Lang.Project" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-conversion-16" />
                    <asp:Label runat="server">
                        Lang.Conversion
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=subsystem" level="3">
                    <img alt="Lang.Project" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-system-16" />
                    <asp:Label runat="server">
                        Lang.Subsystem
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=form" level="3">
                    <img alt="Lang.Project" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-form-16" />
                    <asp:Label runat="server">
                        Lang.SystemForms
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Workflow" level="3">
                    <img alt="Lang.Project" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-workflow-16" />
                    <asp:Label runat="server">
                        Lang.Workflow
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=reports" level="3">
                    <img alt="Lang.Project" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-report-16" />
                    <asp:Label runat="server">
                        Lang.DynamicReport
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?view=langs" level="3">
                    <img alt="Lang.Project" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-lang-16" />
                    <asp:Label runat="server">
                        Lang.LangEdit
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=logs" level="3">
                    <img alt="Lang.Project" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-logs-16" />
                    <asp:Label runat="server">
                        Lang.ErrorReport
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Program&menu=sysa" level="3">
                    <img alt="Lang.Project" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-program-16" />
                    <asp:Label runat="server">
                        Lang.IndependentAccounts
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=VisitorPlan" level="3">
                    <img alt="" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-visitor-16" />
                    <asp:Label runat="server">
                        Lang.VisitorPlan
                    </asp:Label>
                </a>
            </li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" class="menua dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" access="mainmenu,admin">
            <asp:Label Text="Lang.MManagement" runat="server" />
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">            
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=config">
                    <img alt="Lang.Project" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-config-16" />
                    <asp:Label runat="server">
                        Lang.Settings
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=FinanceYear">
                    <img alt="" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-financeyear-16" />
                    <asp:Label runat="server">
                        Lang.FinanceYear
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>OpenTopics.aspx">
                    <img alt="" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-opentopic-16" />
                    <asp:Label runat="server">
                        Lang.ActivateYear
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>CloseTopics.aspx">
                    <img alt="" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-closetopic-16" />
                    <asp:Label runat="server">
                        Lang.CloseTopics
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>dbadmin.aspx">
                    <img alt="" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-dbadmin-16" />
                    <asp:Label runat="server">
                        Lang.DataBaseAdmin
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=roles">
                    <img alt="" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-role-16" />
                    <asp:Label runat="server">
                        Lang.Permission
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=users">
                    <img alt="" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-users-16" />
                    <asp:Label runat="server">
                        Lang.Users
                    </asp:Label>
                </a>
            </li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" class="menua dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" access="mainmenu,accounting">
            <asp:Label runat="server">Lang.MAccounting</asp:Label>
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li>
                <a access="accounting,topic" href="<%= ResolveClientUrl("~/") %>topics.aspx">
                    <img src="<%= UIHelper.DummyImage() %>" alt="" class="sprites sprites-topics-16" />
                    <asp:Label runat="server">
                        Lang.TopicAccount
                    </asp:Label>
                </a>
            </li>
            <li>
                <a access="accounting,topicgroup" href="<%= ResolveClientUrl("~/") %>list.aspx?table=TopicGroup&menu=accountinga">
                    <img src="<%= UIHelper.DummyImage() %>" alt="" class="sprites sprites-topicgroup-16" />
                    <asp:Label runat="server">
                        Lang.TopicGroup
                    </asp:Label>
                </a>
            </li>
            <li>
                <a access="accounting,voucher" href="<%= ResolveClientUrl("~/") %>list.aspx?table=DocumentAccounting">
                    <img src="<%= UIHelper.DummyImage() %>" alt="" class="sprites sprites-voucher-16" />
                    <asp:Label runat="server">
                        Lang.DocumentAccounting
                    </asp:Label>
                </a>
            </li>
            <li>
                <a access="accounting,chart" href="<%= ResolveClientUrl("~/") %>list.aspx?table=PersonnelGroup&menu=accountinga">
                    <img src="<%= UIHelper.DummyImage() %>" alt="" class="sprites sprites-personnelgroup-16" />
                    <asp:Label runat="server">
                        Lang.PersonnelGroup
                    </asp:Label>
                </a>
            </li>
            <li>
                <a access="accounting,person" href="<%= ResolveClientUrl("~/") %>list.aspx?table=Personnel&view=persons">
                    <img src="<%= UIHelper.DummyImage() %>" alt="" class="sprites sprites-person-16" />
                    <asp:Label runat="server">
                        Lang.Persons
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Ward&view=projects&menu=sysa" level="2">
                    <img alt="Lang.Project" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-ward-16" />
                    <asp:Label runat="server">
                       Lang.Project
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Equipment&menu=accountinga" level="2">
                    <img alt="Lang.Equipment" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-ward-16" />
                    <asp:Label runat="server">
                       Lang.Equipment
                    </asp:Label>
                </a>
            </li>
            <li>
                <a access="accounting,importvoucher" href="<%= ResolveClientUrl("~/") %>VoucherImport.aspx" level="2">
                    <img src="<%= UIHelper.DummyImage() %>" alt="" class="sprites sprites-getprice-16" />
                    <asp:Label runat="server">
                        Lang.ImportVoucher
                    </asp:Label>
                </a>
            </li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" class="menua dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" access="mainmenu,sales">
            <asp:Label runat="server">Lang.SellAndBuy</asp:Label>
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Purchase&menu=accountinga" access="sales,purchaseinvoice" level="2">
                    <img alt="" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-purchase-16" />
                    <asp:Label runat="server">
                        Lang.SalesPurchase
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=StoreDraft&menu=accountinga" access="sales,salesinvoice" level="2">
                    <img alt="" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-storedraft-16" />
                    <asp:Label runat="server">
                        Lang.SalesInvoice
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Refuse&menu=accountinga" access="sales,refuseinvoice" level="2">
                    <img alt="" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-refuse-16" />
                    <asp:Label runat="server">
                        Lang.RefuseInvoice
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Returns&menu=accountinga" access="sales,returninvoice" level="2">
                    <img alt="" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-return-16" />
                    <asp:Label runat="server">
                        Lang.ReturnInvoice
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Grouping&view=browse&menu=basicsa" access="sales,browse" level="2">
                    <img alt="" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-invoice-16" />
                    <asp:Label runat="server">
                        Lang.Browse
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Grouping&menu=basicsa" level="2">
                    <img alt="" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-invoice-16" />
                    <asp:Label runat="server">
                        Lang.Grouping
                    </asp:Label>
                </a>
            </li>            
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=coding" level="2">
                    <img alt="" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-invoice-16" />
                    <asp:Label runat="server">
                        Lang.Coding
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=saletype" access="sales,salesinvoice" level="2">
                    <img alt="" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-storedraft-16" />
                    <asp:Label runat="server">
                        Lang.SaleType
                    </asp:Label>
                </a>
            </li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" class="menua dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" access="mainmenu,bank">
            <asp:Label runat="server">Lang.BankAndCheck</asp:Label>
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=BankAccount" access="bank,bankaccount">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-bankaccount-16" />
                    <asp:Label runat="server">
                        Lang.BankAccount
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=BankAccountCheck" access="bank,bankaccountcheck">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-bankaccountcheck-16" />
                    <asp:Label runat="server">
                        Lang.BankAccountCheck
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Checks&view=checksshops&qd=checksshop" access="bank,checksshops">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-check-16" />
                    <asp:Label runat="server">
                        Lang.AutoChecksRegister
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=PayCheck" access="bank,paycheck">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-paycheck-16" />
                    <asp:Label runat="server">
                        Lang.PayCheck
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Checks" access="bank,checks">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-check-16" />
                    <asp:Label runat="server">
                        Lang.ReceivedChecks
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=BankPrint" access="bank,bankprint">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-bankprint-16" />
                    <asp:Label runat="server">
                        Lang.PrintTemplate
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>BankDifference.aspx" access="bank,bankdifference" level="2">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-bankprint-16" />
                    <asp:Label runat="server">
                        Lang.BankDifference
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Loan" access="bank,loan" level="2">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-loans-16" />
                    <asp:Label runat="server">
                        Lang.Loans
                    </asp:Label>
                </a>
            </li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" class="menua dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" access="mainmenu,store">
            <asp:Label runat="server">Lang.MStore</asp:Label>
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Units&menu=storea" access="stores,units">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-units-16" alt="" />
                    <asp:Label runat="server">
                        Lang.MeasurementUnits
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=ServiceType&menu=storea" access="stores,servicetype">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-producttype-16" alt="" />
                    <asp:Label runat="server">
                        Lang.ServiceType
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Service&menu=storea" access="stores,service">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-product-16" alt="" />
                    <asp:Label runat="server">
                        Lang.Commodity
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=StoreCategory&menu=storea" access="stores,storecategory">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-storecategory-16" alt="" />
                    <asp:Label runat="server">
                        Lang.StoreNature
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Stores&menu=storea" access="stores,store">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-store-16" alt="" />
                    <asp:Label runat="server">
                        Lang.Store
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Transfer&menu=storea" access="storeforms,transfer" level="2">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-transfer-16" alt="" />
                    <asp:Label runat="server">
                        Lang.WFTTransfer
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Purchase&menu=storea" access="storeforms,purchase">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-purchase-16" alt="" />
                    <asp:Label runat="server">
                        Lang.WFTPurchase
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=StoreDraft&menu=storea" access="storeforms,storedraft">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-storedraft-16" alt="" />
                    <asp:Label runat="server">
                        Lang.WFTStoreDraft
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Refuse&menu=storea" access="storeforms,refuse">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-refuse-16" alt="" />
                    <asp:Label runat="server">
                        Lang.WFTRefuse
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Returns&menu=storea" access="storeforms,returns">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-return-16" alt="" />
                    <asp:Label runat="server">
                        Lang.WFTReturn
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=PurchaseRequest&menu=storea" access="storeforms,purchaserequest" level="2">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-purchaserequest-16" alt="" />
                    <asp:Label runat="server">
                        Lang.WFTPurchaseRequest
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Request&menu=storea" access="storeforms,request" level="2">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-request-16" alt="" />
                    <asp:Label runat="server">
                        Lang.WFTRequest
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Supplier&menu=storea" access="stores,supplier">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-supplier-16" alt="" />
                    <asp:Label runat="server">
                        Lang.Suppliers
                    </asp:Label>
                </a>
            </li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" class="menua dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" access="mainmenu,reports">
            <asp:Label runat="server">Lang.Reports</asp:Label>
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=accreport" access="reports,report">
                    <img src="<%= UIHelper.DummyImage() %>" alt="" class="sprites sprites-accreport-16" />
                    <asp:Label runat="server">
                        Lang.CreatAccountingReport
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>reports/accounting/MyBook.aspx" access="reports,mybook">
                    <img src="<%= UIHelper.DummyImage() %>" alt="" class="sprites sprites-invoice-16" />
                    <asp:Label runat="server">
                        Lang.MyBook
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>reports/accounting/PersonBook.aspx" access="reports,personbook">
                    <img src="<%= UIHelper.DummyImage() %>" alt="" class="sprites sprites-bill-16" />
                    <asp:Label runat="server">
                        Lang.PersonBook
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>reports/accounting/Cardex.aspx" access="reports,cardex">
                    <img src="<%= UIHelper.DummyImage() %>" alt="" class="sprites sprites-cardex-16" />
                    <asp:Label runat="server">
                        Lang.Cardex
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>reports/accounting/Report1.aspx" access="reports,report1">
                    <img src="<%= UIHelper.DummyImage() %>" alt="" class="sprites sprites-custom-report-16" />
                    <asp:Label runat="server">
                        Lang.Report1
                    </asp:Label>
                </a>
            </li>


            <li>
                <a href="<%= ResolveClientUrl("~/") %>reports/accounting/TotalBook.aspx" access="reports,totalbook">
                    <img src="<%= UIHelper.DummyImage() %>" alt="" class="sprites sprites-totalbook-16" />
                    <asp:Label runat="server">
                        Lang.TotalBook
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>reports/accounting/PaperBook.aspx" access="reports,paperbook">
                    <img src="<%= UIHelper.DummyImage() %>" alt="" class="sprites sprites-paperbook-16" />
                    <asp:Label runat="server">
                        Lang.PaperBook
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>reports/accounting/FunctionalBalance.aspx?level=0" access="reports,functionalbalance">
                    <img src="<%= UIHelper.DummyImage() %>" alt="" class="sprites sprites-functionalbalance-16" />
                    <asp:Label runat="server">
                        Lang.MonthlyOperationsBalance
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>reports/accounting/ExpBalance.aspx" access="reports,expbalance">
                    <img src="<%= UIHelper.DummyImage() %>" alt="" class="sprites sprites-functionalbalance-16" />
                    <asp:Label runat="server">
                        Lang.ExpBalance
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>reports/accounting/ExpBalance8.aspx" access="reports,expbalance8">
                    <img src="<%= UIHelper.DummyImage() %>" alt="" class="sprites sprites-functionalbalance-16" />
                    <asp:Label runat="server">
                        Lang.ExpBalance8
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>reports/accounting/SaleReports.aspx" access="reports,salereports">
                    <img src="<%= UIHelper.DummyImage() %>" alt="" class="sprites sprites-custom-report-16" />
                    <asp:Label runat="server">
                        Lang.SaleReports
                    </asp:Label>
                </a>
            </li>
            <asp:PlaceHolder runat="server" ID="mCustomReports" Visible="False">
                <li class="divider"></li>
                <% foreach (System.Data.DataRow row in reports.Rows)
                   { %>
                <li>
                    <a href="<%= ResolveClientUrl("~/") %>reports/accounting/accrep.aspx?id=<%= row[0].ToString() %>" alt="">
                        <img src="<%= UIHelper.DummyImage() %>" alt="" class="sprites sprites-report-general-16" />
                        <span><%= row[1].ToString() %></span>
                    </a>
                </li>
                <% } %>
            </asp:PlaceHolder>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" class="menua dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" access="mainmenu,distribution">
            <asp:Label runat="server">Lang.MDistribution</asp:Label>
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Offers&menu=basicsa" access="distribution,offers">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-invoice-16" alt="" />
                    <asp:Label runat="server">
                        Lang.Offers
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Grouping&view=groupsearch&menu=basicsa" access="distribution,groupsearch">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-invoice-16" alt="" />
                    <asp:Label runat="server">
                        Lang.GroupSearch
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Awards&view=awards&menu=basicsa" access="distribution,awards">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-awards-16" alt="" />
                    <asp:Label runat="server">
                        Lang.Awards
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=VisitorCustomersStatus&view=visitorcustomersstatuss" access="visitor,dailyplan">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-visitor-16" alt="" />
                    <asp:Label runat="server">
                        Lang.DailyPlan
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>help/notfound.htm" access="visitor,performancereport">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-performancereport-16" alt="" />
                    <asp:Label runat="server">
                        Lang.PerformanceReport
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>help/notfound.htm" access="visitor,visitoroffers">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-visitor-16" alt="" />
                    <asp:Label runat="server">
                        Lang.VisitorOffers
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>help/notfound.htm" access="visitor,contactcenter">
                    <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-contactcenter-16" alt="" />
                    <asp:Label runat="server">
                        Lang.ContactCenter
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=HoliDays&view=holidays&menu=sysa">
                    <img alt="Lang.Project" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-holiday-16" />
                    <asp:Label runat="server">
                        Lang.Holidays
                    </asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=CooperationType&menu=sysa">
                    <img alt="Lang.Project" src="<%= UIHelper.DummyImage() %>" class="sprites sprites-cooperationtype-16" />
                    <asp:Label runat="server">
                        Lang.CooperationType
                    </asp:Label>
                </a>
            </li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" class="menua dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" access="mainmenu,salary">
            <asp:Label runat="server">Lang.MSalary</asp:Label>
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Personnel&menu=salarysa&view=staffs" access="salary,personnel">
                    <asp:Label runat="server">Lang.Personnel</asp:Label>
                </a>
            </li>
            <li class="divider"></li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=ConstItems&menu=salarysa" access="salary,constitems">
                    <asp:Label runat="server">Lang.SalaryItems</asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=ConstDefinitions&menu=salarysa" access="salary,constdefination">
                    <asp:Label runat="server">Lang.SalaryCoefficientsGroup</asp:Label>
                </a>
            </li>
            <li class="divider"></li>
            <li class="dropdown dropdown-submenu">
                <a href="#" class="menua dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    <asp:Label runat="server">Lang.MBasics</asp:Label>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Enums&menu=salarysa" access="salary,enums">
                            <asp:Label runat="server">Lang.DefineBasics</asp:Label>
                        </a>
                    </li>
                    <li>
                        <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Ward&menu=salarysa&view=sections" access="salary,sections">
                            <asp:Label runat="server">Lang.Ward</asp:Label>
                        </a>
                    </li>
                    <li>
                        <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Job&menu=salarysa" access="salary,job">
                            <asp:Label runat="server">Lang.Job</asp:Label>
                        </a>
                    </li>
                    <li>
                        <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=EmploymentType&menu=salarysa" access="salary,employmenttype">
                            <asp:Label runat="server">Lang.EmpType</asp:Label>
                        </a>
                    </li>
                    <li>
                        <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Marital&menu=salarysa" access="salary,marital">
                            <asp:Label runat="server">Lang.MaritalStatus</asp:Label>
                        </a>
                    </li>
                    <li>
                        <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Gender&menu=salarysa" access="salary,gender">
                            <asp:Label runat="server">Lang.Gender</asp:Label>
                        </a>
                    </li>
                    <li>
                        <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Country&menu=salarysa" access="salary,country">
                            <asp:Label runat="server">Lang.Country</asp:Label>
                        </a>
                    </li>
                    <li>
                        <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Province&menu=salarysa" access="salary,province">
                            <asp:Label runat="server">Lang.ProvinceName</asp:Label>
                        </a>
                    </li>
                    <li>
                        <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=City&menu=salarysa" access="salary,city">
                            <asp:Label runat="server">Lang.City</asp:Label>
                        </a>
                    </li>
                    <li>
                        <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Config&view=companyinfos" access="salary,companyinfo">
                            <asp:Label runat="server">
                                Lang.CompanyInfo
                            </asp:Label>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="divider"></li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=OrderDefs&menu=salarysa" access="salary,orderdefs">
                    <asp:Label runat="server">Lang.DefineOrderDefs</asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Fields&menu=salarysa" access="salary,fields">
                    <asp:Label runat="server">Lang.FormDeclareItems</asp:Label>
                </a>
            </li>
            <li class="divider"></li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=FishDefs&menu=salarysa" access="salary,fishdefs">
                    <asp:Label runat="server">Lang.FishDef</asp:Label>
                </a>
            </li>

            <li class="divider"></li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=FundType&menu=salarysa" access="salary,fundtype">
                    <asp:Label runat="server">Lang.FundTypeName</asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>list.aspx?table=Funds&menu=salarysa" access="salary,funds">
                    <asp:Label runat="server">Funds</asp:Label>
                </a>
            </li>
            <li class="divider"></li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>FishDocument.aspx" access="salary,tax">
                    <asp:Label runat="server">Lang.FishDocument</asp:Label>
                </a>
            </li>
            <li class="divider"></li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>Tax.aspx" access="salary,tax">
                    <asp:Label runat="server">Lang.TaxAffairs</asp:Label>
                </a>
            </li>
            <li>
                <a href="<%= ResolveClientUrl("~/") %>Insurance.aspx" access="salary,insurance">
                    <asp:Label runat="server">Lang.Insurance</asp:Label>
                </a>
            </li>
        </ul>
    </li>
</ul>


<script type="text/javascript">
    $(function () {
        //if (!_Context.isAdmin)
        $(".navbar-nav a").each(enableMenu);
    });
    </script>