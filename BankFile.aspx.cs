﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BankFile : System.Web.UI.Page
{
    public static System.Globalization.CultureInfo NumberCulture = new System.Globalization.CultureInfo("en-US");
    string folderName = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Page.SetLang();
            BindYear();
            BindInitData();
            BindBankFileHistory();
        }
    }

    void BindYear()
    {
        PersianCalendar calendar = new PersianCalendar();
        int year = calendar.GetYear(DateTime.Now);
        ddlYear.Items.Add((year - 1).ToString());
        ddlYear.Items.Add(year.ToString());
        ddlYear.Items.Add((year + 1).ToString());
        ddlYear.SelectedIndex = 1;
    }
    void BindInitData()
    {
        {
            DataTable bankTable =
                DbProxy.Instance.FillData(
                    string.Format(@"SELECT ID, BankName+' - '+BranchName AS Title FROM BankAccount"), "banks");
            ddlBank.DataValueField = "ID";
            ddlBank.DataTextField = "Title";
            ddlBank.DataSource = bankTable;
            ddlBank.DataBind();
        }

        {
            DataTable settlementTypeTable =
                DbProxy.Instance.FillData(
                    string.Format(
                        @"SELECT EnumValues.ID, EnumValues.Title FROM EnumValues INNER JOIN Enums ON EnumValues.EnumID=Enums.ID WHERE Enums.Name=N'SettlementType'"),
                    "settlementType");
            ddlSettlementType.DataValueField = "ID";
            ddlSettlementType.DataTextField = "Title";
            ddlSettlementType.DataSource = settlementTypeTable;
            ddlSettlementType.DataBind();
        }
    }

    void BindBankFileHistory()
    {
        DataTable bankFileTable = DbProxy.Instance.FillData(@"SELECT * FROM BankFileHistory ORDER BY ID DESC",
            "bankFileTable");
        mGrid.DataSource = bankFileTable;
        mGrid.DataBind();
    }
    protected void doCreateFile_Click(object sender, EventArgs e)
    {
        string folder = GetFolder();
        CreateBankFile(folder);
        mFile.Visible = true;
    }

    string GetFolder()
    {
        string folder = Server.MapPath("~/temp/Bank");
        folderName = Helper.FormatDate(DateTime.Now, "yyyy-MM-dd-fff");
        folder = Path.Combine(folder, folderName);

        if (!Directory.Exists(folder))
        {
            Directory.CreateDirectory(folder);
        }
        return folder;
    }

    void CreateBankFile(string folder)
    {
        DataRow bank = DbProxy.Instance.FillRow(string.Format(@"SELECT * FROM BankAccount WHERE ID={0}", ddlBank.SelectedValue));
        string fileName = Helper.GetString(bank, "CompanyBankCode", "000000") +
                              Helper.GetString(bank, "BranchCode", "0000") + "." + ddlMonth.SelectedValue;
            string file = Path.Combine(folder, fileName);
            if (!file.ToLower().EndsWith(".txt"))
                file += ".txt";
        StreamWriter sw = new StreamWriter(file, true, Encoding.UTF8);
        try
        {
            string SettlementTypeCode = Helper.GetEnumValuesCode(ddlSettlementType.SelectedValue);

            DataTable fishs = DbProxy.Instance.FillData("SELECT ID, PersonnelID FROM Fish WHERE FishYear=" + ddlYear.Text + " AND FishMonth=" + ddlMonth.Text + " AND Status=1", "fishs");

            int SumPay =
                Helper.GetInt(
                    FishValuesView.FindSumValue(Helper.GetInt(ddlYear.SelectedItem.Text, 0),
                        Helper.GetInt(ddlMonth.SelectedValue, 0), Helper.GetField("FTotalPayed", ReportFieldType.Account)),
                    0);

            string No = DbProxy.Instance.FillScalarString("SELECT dbo.NeuBankFileNo(NULL)");
            string payDate = txtPayDate.Text.Substring(2).Replace("/", "");
            string Identify = Helper.GetString(bank, "BranchCode", "0000") +
                              Helper.GetString(bank, "CompanyBankCode", "000000") + No +
                              payDate + GenerateRecord(Helper.GetString(SumPay), 15) + GenerateRecord(Helper.GetString(fishs.Rows.Count), 5) + SettlementTypeCode + "000";

            sw.WriteLine(Identify);

            // insert to BankFileHistory table
            {
                string date = Helper.ParseDate(txtPayDate.Text, Helper.NullDate).ToString("yyyy-MM-dd", NumberCulture);
                DbProxy.Instance.RunQuery(
                    string.Format(
                        @"INSERT INTO BankFileHistory (No, BankAccountID, Date, Year, Month) Values (N'{0}',{1},'{2}',{3},{4}); SELECT SCOPE_IDENTITY();",
                        No, ddlBank.SelectedValue, date, ddlYear.SelectedItem, ddlMonth.SelectedValue));
            }

            for (int i = 0; i < fishs.Rows.Count; i++)
            {
                int fishId = Helper.GetInt(Helper.GetString(fishs.Rows[i], "ID", ""), -1);
                DataRow personnelInfo =
                    DbProxy.Instance.FillRow(
                        "SELECT * FROM Personnel WHERE ID=" + Helper.GetString(fishs.Rows[i], "PersonnelID", ""));
                string row = GenerateRecord(Helper.GetString(i + 1),5); //Row
                string AccountNo = Helper.GetString(personnelInfo, "AccountNo", "0000000000000");
                string TotalPay = Helper.GetString(FishValuesView.FindFishValuesByFieldName(fishId, Helper.GetField("FTotalPayed", ReportFieldType.Account))
                        .Value, "000000000000000");

                string Item = row + GenerateRecord(AccountNo,13) + GenerateRecord(TotalPay,15) + "000000000000000";
                sw.WriteLine(Item);
            }
            mFile.Text += LangProvider.Instance.ParseLang("Lang.FileCreated") + ": <a href='temp/Bank/" + folderName + "/" + fileName+".txt" + "' download='" + fileName+".txt" + "'>" + LangProvider.Instance.ParseLang("Lang.Download") + "</a><br />";
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
        }
        finally
        {
            if (sw != null)
            {
                sw.Flush();
                sw.Close();
                sw.Dispose();
            }
        }
    }

    string GenerateRecord(string value,int length)
    {
        string res = value;
        int defvallength = length-Helper.GetString(value, "").Length;
        for (int i = 0; i < defvallength; i++)
        {
            res = "0" + res;
        }
        
        return res;
    }

    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView row = e.Row.DataItem as DataRowView;
            // url2
            e.Row.Cells[1].Text = row["ID"].ToString();
            e.Row.Cells[2].Text = DbProxy.Instance.FillScalarString("SELECT BankName+' - '+BranchName FROM BankAccount WHERE ID="+row["BankAccountID"]);
            e.Row.Cells[4].Text = Helper.ToFaFormat(Helper.GetDateTime(row["Date"], DateTime.Now));
        }
    }
    protected void mGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Helper.GetInt(mGrid.Rows[e.RowIndex].Cells[1].Text, -1);
        QueryInfo qinfo = QueryController.Instance["bankfilehistorydelete"];
        int num = DbProxy.Instance.RunQuery(qinfo.Query.Replace("@ID", id.ToString()));
        if (num > 0)
        {
            lblMessage.Text = LangProvider.Instance["RecordDeletedSuccess"];
            lblMessage.Visible = true;
        }
        BindBankFileHistory();
    }
}