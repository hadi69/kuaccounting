﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DoAssessment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.SetLang();
        mMsg.Value = "";
        FromDate.Value = Helper.FormatDate(DateTime.Now.AddMonths(-1), null);
        ToDate.Value = Helper.FormatDate(DateTime.Now, null);
    }
    protected void doPursantDocument_Click(object sender, EventArgs e)
    {
        string[] personIDs = Helper.GetString(Request["PersonID"], "").Split(',');
        string[] pursantAmounts = Helper.GetString(Request["PursantAmount"], "").Split(',');
        DbProxy proxy = new DbProxy();
        //proxy.RunQuery("UPDATE SaleTypeItem SET Deleted = 1");

        int voucherID = -1;
        int fincanceYearID = -1;
        int ok = -1;
        for (int i = 0; i < personIDs.Length; i++)
        {
            int personId = Helper.GetInt(personIDs[Math.Min(i, personIDs.Length - 1)], -1);
            decimal pursantAmount = Helper.GetDecimal(pursantAmounts[Math.Min(i, pursantAmounts.Length - 1)], 0);

            if (i == 0 && personId > 0)
            {
                fincanceYearID = Helper.GetInt(proxy.FillScalar("SELECT ID FROM FinanceYear WHERE Status=2"), -1);
                voucherID = Helper.GetInt(proxy.FillScalar(@"
                DECLARE @VoucherID int;
	            INSERT INTO DocumentAccounting(FinanceYearID, [No], Mode, OtherID, Date, UserID, Status, DoComments, ProgramID) 
			            VALUES (@FinanceYearID, dbo.[NeuVoucherNo] (NULL, @FinanceYearID, NULL, NULL), 0, NULL, GETDATE(), NULL, 0, N'سند پورسانت', NULL)
	            SET @VoucherID = SCOPE_IDENTITY()
                 SELECT @VoucherID;".Replace("@FinanceYearID", fincanceYearID.ToString())), -1);
            }

            if (pursantAmount > 0)
            {
                ok = Helper.GetInt(proxy.FillScalar(string.Format(@" DECLARE @OweTreePath NVARCHAR(250), @DueTreePath NVARCHAR(250)
                    	SET @OweTreePath = dbo.GetSetting2('SalePursantPayTreePath', NULL);
                        SET @DueTreePath = dbo.GetSetting2('SalePursantTreePath', NULL);
                        INSERT INTO DocumentAccountingItems(DocumentAccountingID, TreePath, PersonnelID, Owe, Due)
	                        VALUES({0}, @OweTreePath, NULL, {1} , 0);
                        INSERT INTO DocumentAccountingItems(DocumentAccountingID, TreePath, PersonnelID, Owe, Due)
	                        VALUES({0}, @DueTreePath, {2}, 0 , {1}); SELECT 2;
                 ", voucherID, pursantAmount, personId)), -1);
            }
        }
        proxy.FillScalar(String.Format(@"IF NOT EXISTS (SELECT ID FROM DocumentAccountingItems WHERE DocumentAccountingID = {0})
                                        DELETE FROM DocumentAccounting WHERE ID = {0}", voucherID));
        //proxy.RunQuery("DELETE FROM SaleTypeItem WHERE Deleted = 1");
        proxy.Dispose();
        if (ok > 0)
        {
            mMsg.Value = string.Format(LangProvider.Instance["Success"]);
        }
        else
        {
            mMsg.Value = string.Format(LangProvider.Instance["DocumentDoesNotCreate"]);
        }

    }
}