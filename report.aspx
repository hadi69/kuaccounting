﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" EnableViewState="false"
    CodeFile="report.aspx.cs" Inherits="report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
  
    <div>
        <a class='report' href="pazireshebimaranbastari"><%= "PaziresheBimaraneBastari".Translate() %></a><br />
    </div>
   
    <div>
        <a class='report' href="files"><%= "BimaraneMojudDarBakhshha".Translate() %></a><br />
    </div>
    <div>
        <a class='report' href="clincfilereport"><%= "ListPaziresheDarmangah".Translate() %></a><br />
    </div> 
    <div>
        <a class='report' href="amarbimaranbakhsh"><%= "AmareBimaraneMojudDarBakhsh".Translate() %></a><br />
    </div> 
     <div>
        <a class='report' href="amaramlkardepaziresh"><%= "AmareAmalkardePaziresh".Translate() %></a><br />
    </div>
     <div>
        <a class='report' href="bedstates"><%= "WaziateTakhthayeBastary".Translate() %></a><br />
    </div> 
    
     <div>
        <a class='report' href="listpharmecypay"><%= "ListHaghFaniDarukhane".Translate() %></a><br />
    </div> 
    <div>
        <a class='report' href="complateoperationpharmacy"><%= "GozaresheAmalkardeKoliDarukhane".Translate() %></a><br />
    </div>
    <div>
        <a class='report' href="operationstore"><%= "GozaresheAmalkardeKoliAnbar".Translate() %></a><br />
    </div>
    
     <div>
        <a class='report' href="listofregistereddocumentstorage"><%= "ListAsnadeSabtShodeAnbar".Translate() %></a><br />
    </div>
     <div>
        <a class='report' href="kartexkala"><%= "KardexKalaDaruWaTajhizateMasrafi".Translate() %></a><br />
    </div>
     <div>
        <a class='report' href="stockstore"><%= "GozaresheMojudiAnbar".Translate() %></a><br />
    </div>
    
     <div>
        <a class='report' href="thelabreport"><%= "GozaresheAmalkardeAzmayeshgah".Translate() %></a><br />
    </div>
      <div>
        <a class='report' href="performanceunderlaboratoryunits"><%= "AmalkardeWahedhayeZirMajmueyeAzmayeshgah".Translate() %></a><br />
    </div>
      <div>
        <a class='report' href="testrequestedstatistics"> <%= "AmareTestHayeDarkhastShodeh".Translate() %></a><br />
    </div>
     <div>
        <a class='report' href="allrequestesforaparticulartest"><%= "AmareDarkhasteYekTestKhas".Translate() %></a><br />
    </div>

    <script language=javascript type="text/javascript">
        $(function () {
            function showcallback(v, m, f) {
                if (v == 'OK') {
                    dataString = $("#detailForm").serialize();
                    var _url = 'reports/'+ _Context.view +'.aspx?' + dataString + appendTime();
                    //alert(_url + " --> " + dataString);
                    popup(_url);
                }
            }
            $('.report').click(function () {
                var view = $(this).attr('href');
                _Context.view = view;
                var _url = 'get.aspx?op=GetReportFilter&table=Moves&view=' + view + appendTime();
                $.ajax({
                    url: _url,
                    type: 'GET',
                    success: function (html) {
                        $.prompt(html, {
                            submit: showcallback,
                            buttons: { انصراف: 'Cancel', 'نمایش': 'OK' },
                            focus: 2,
                            top: '1%'
                        });
                        $('#SectionName').focus();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert('Status=' + xhr.status + ' StatusText=' + xhr.statusText + ' thrownError=' + thrownError);
                    }
                });
                return false;
            });
            $('#_showPopup').click(function () {

            });
        });
    </script>
</asp:Content>
