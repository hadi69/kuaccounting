﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class OpenTopics : System.Web.UI.Page
{
    DbProxy mDbProxy = new DbProxy();
    protected void Page_Load(object sender, EventArgs e)
    {
        mDbProxy.LastException = null;
        if (!IsPostBack)
        {
            Page.SetLang();
            CheckAllAreClosed();
        }
    }
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        try
        {
            if (mDbProxy != null)
                mDbProxy.Dispose();
        }
        catch { }
    }

    void CheckAllAreClosed()
    {
        try
        {
            DataRow financeYear = mDbProxy.FillRow("SELECT * FROM FinanceYear WHERE Status=2");
            if (financeYear == null)
                return;
            string closedsPrograms = "," + Helper.GetString(financeYear, "ClosedPrograms", "") + ",";
            // close the year if all programs are closed
            bool allClosed = false;
            closedsPrograms = "," + closedsPrograms + ",";
            if (closedsPrograms.Contains(",-1,")) // default program
            {
                allClosed = true;
                DataTable programs = DbProxy.Instance.FillData("SELECT ID FROM Program", "programs");
                if (programs != null && programs.Rows.Count > 0)
                    for (int i = 0; i < programs.Rows.Count; i++)
                        if (!closedsPrograms.Contains("," + programs.Rows[i][0].ToString() + ","))
                        {
                            allClosed = false;
                            break;
                        }
            }

            if (allClosed)
            {
                int curfinanceYearID = (int)financeYear["ID"];
                mDbProxy.RunQuery("UPDATE FinanceYear SET Status=3 WHERE ID = " + curfinanceYearID);
                SiteSettings.Invalidate();
            }
        }
        catch { }
    }

    protected void doStart_Click(object sender, EventArgs e)
    {
        DataRow financeYear = null;
        if (mIsTemp.Checked)
        {
            financeYear = mDbProxy.FillRow("SELECT * FROM FinanceYear WHERE Status=2");
            if (financeYear == null)
            {
                ShowMessage(LangProvider.Instance["NoActiveFinancialYear"]);
                return;
            }
        }
        else
        {
            financeYear = mDbProxy.FillRow("SELECT * FROM FinanceYear WHERE Status=3");
            if (financeYear == null)
            {
                CheckAllAreClosed();
                financeYear = mDbProxy.FillRow("SELECT * FROM FinanceYear WHERE Status=3");
            }
            if (financeYear == null)
            {
                ShowMessage(LangProvider.Instance["NotClosedYear"]);
                return;
            }
        }

        // validate
        int curfinanceYearID = (int)financeYear["ID"];
        int newFinanceYearID = Helper.GetInt(mNewFinanceYearID.Value.Trim(), 0);
        string openingBalanceTreePath = mOpeningBalanceID.Value.Trim();
        string closingBalanceTreePath = mClosingBalanceID.Value.Trim();
        #region validate
        if (newFinanceYearID <= 0)
        {
            ShowMessage(LangProvider.Instance["ChooseNewYear"]);
            return;
        }
        if (newFinanceYearID == (int)financeYear["ID"])
        {
            ShowMessage(LangProvider.Instance["SameNewYear"]);
            return;
        }
        DataRow newFinanceYear = mDbProxy.FillRow("SELECT * FROM FinanceYear WHERE ID=" + newFinanceYearID);
        if (newFinanceYear == null)
        {
            ShowMessage(LangProvider.Instance["WrongNewYear"]);
            return;
        }
        #endregion

        if (mIsTemp.Checked)
        {
            // Make the old FinanceYear temp closed
            mDbProxy.RunQuery("UPDATE FinanceYear SET TempClosed=1 WHERE ID = " + curfinanceYearID);
        }

        string dbName = string.Format("DB_{0}", financeYear["Title"]);
        try
        {
            if (!Backup(dbName))
            {
                mDbProxy.RunQuery("UPDATE FinanceYear SET TempClosed=NULL WHERE ID = " + curfinanceYearID);
                ShowMessage(LangProvider.Instance["BackupError"] + ": <br />");
                return;
            }
        }
        catch (Exception ex)
        {
            ShowMessage(LangProvider.Instance["BackupError"] + ": <br />" + ex.Message);
        }
        try
        {
            mDbProxy.BeginTransaction();

            // Make the new FinanceYear active
            //first make current year inactive/closed
            mDbProxy.RunQuery("UPDATE FinanceYear SET Status=1 WHERE ID = " + curfinanceYearID);

            if (!mIsTemp.Checked)
            {
                // Create Opening Vouchers for all Programs
                mDbProxy.RunSP("MakeOpeningVoucher", new string[] { "FinanceYearID", "ProgramID", "OpeningBalanceTreePath", "ClosingBalanceTreePath" }
                    , new object[] { newFinanceYearID, SiteSettings.ProgramID, openingBalanceTreePath, closingBalanceTreePath }, 60000);
                DataTable programs = DbProxy.Instance.FillData("SELECT ID FROM Program", "programs");
                if (programs != null && programs.Rows.Count > 0)
                    for (int i = 0; i < programs.Rows.Count; i++)
                        mDbProxy.RunSP("MakeOpeningVoucher", new string[] { "FinanceYearID", "ProgramID", "OpeningBalanceTreePath", "ClosingBalanceTreePath" }
                            , new object[] { newFinanceYearID.ToString(), Helper.GetInt(programs.Rows[i], "ID", -1), openingBalanceTreePath, closingBalanceTreePath }, 60000);
            }

            if (-1 == mDbProxy.RunSP("EmptyDB", new string[] { "NewFinanceYearID", "Vouchers", "TempTopics" }
                , new object[] { newFinanceYearID, 0, 0}, 60000))
                throw new Exception(LangProvider.Instance["EmptyDbError"] + " : " + mDbProxy.LastException.Message);
            #region empty accounting tables
           /* if (-1 == mDbProxy.RunQuery(@"
ALTER TABLE Workflow DISABLE TRIGGER WorkflowInsertUpdate
ALTER TABLE FinanceYear DISABLE TRIGGER FinanceYearInsertUpdate
ALTER TABLE DocumentAccountingItems DISABLE TRIGGER DocumentAccountingItemsInsertUpdate
ALTER TABLE DocumentAccountingItems DISABLE TRIGGER DocumentAccountingItemsDelete
ALTER TABLE TaskImpl DISABLE TRIGGER TaskImplInsert
ALTER TABLE StoreServices DISABLE TRIGGER StoreServicesAll
ALTER TABLE Trees DISABLE TRIGGER TreesDelete
ALTER TABLE Checks DISABLE TRIGGER ChecksInsertUpdate
ALTER TABLE PayCheck DISABLE TRIGGER PayCheckInsertUpdate
ALTER TABLE Purchase DISABLE TRIGGER PurchaseInsertUpdate
ALTER TABLE Purchase DISABLE TRIGGER PurchaseDelete
ALTER TABLE PurchaseItems DISABLE TRIGGER PurchaseItemsDelete
ALTER TABLE PurchaseItems DISABLE TRIGGER PurchaseItemsInsertUpdate
ALTER TABLE StoreDraft DISABLE TRIGGER StoreDraftInsertUpdate
ALTER TABLE StoreDraft DISABLE TRIGGER StoreDraftDelete
ALTER TABLE StoreDraftItems DISABLE TRIGGER StoreDraftItemsDelete
ALTER TABLE StoreDraftItems DISABLE TRIGGER StoreDraftItemsInsertUpdate
ALTER TABLE PurchaseRequest DISABLE TRIGGER PurchaseRequestInsertUpdate
ALTER TABLE PurchaseRequest DISABLE TRIGGER PurchaseRequestDelete
ALTER TABLE Request DISABLE TRIGGER RequestInsertUpdate
ALTER TABLE Request DISABLE TRIGGER RequestDelete
ALTER TABLE Returns DISABLE TRIGGER ReturnsInsertUpdate
ALTER TABLE Returns DISABLE TRIGGER ReturnsDelete
ALTER TABLE ReturnItem DISABLE TRIGGER ReturnItemDelete
ALTER TABLE ReturnItem DISABLE TRIGGER ReturnItemInsertUpdate
ALTER TABLE Refuse DISABLE TRIGGER RefuseInsertUpdate
ALTER TABLE Refuse DISABLE TRIGGER RefuseDelete
ALTER TABLE RefuseItem DISABLE TRIGGER RefuseItemDelete
ALTER TABLE RefuseItem DISABLE TRIGGER RefuseItemInsertUpdate
ALTER TABLE Transfer DISABLE TRIGGER TransferInsertUpdate
ALTER TABLE Transfer DISABLE TRIGGER TransferDelete
ALTER TABLE TransferItem DISABLE TRIGGER TransferItemDelete
ALTER TABLE TransferItem DISABLE TRIGGER TransferItemInsertUpdate
ALTER TABLE Cardex DISABLE TRIGGER CardexInsertUpdate
ALTER TABLE Service DISABLE TRIGGER ServiceUpdate
UPDATE DocumentAccountingItems SET CheckID = NULL, PayCheckID = NULL
UPDATE StoreDraft SET CheckID = NULL
UPDATE Purchase SET PayCheckID = NULL
DELETE FROM PayCheck
DELETE FROM Checks
DELETE FROM RequestItems
DELETE FROM Request
DELETE FROM PurchaseRequestItems
DELETE FROM PurchaseRequest
DELETE FROM ReturnItem
DELETE FROM [Returns]
DELETE FROM RefuseItem
DELETE FROM Refuse
DELETE FROM PurchaseItems
DELETE FROM Purchase
DELETE FROM StoreDraftItems
DELETE FROM StoreDraft"))*/
                if (-1 == mDbProxy.RunQuery(@"
ALTER TABLE Workflow DISABLE TRIGGER WorkflowInsertUpdate
ALTER TABLE FinanceYear DISABLE TRIGGER FinanceYearInsertUpdate
ALTER TABLE DocumentAccountingItems DISABLE TRIGGER DocumentAccountingItemsInsertUpdate
ALTER TABLE DocumentAccountingItems DISABLE TRIGGER DocumentAccountingItemsDelete
ALTER TABLE TaskImpl DISABLE TRIGGER TaskImplInsert
ALTER TABLE StoreServices DISABLE TRIGGER StoreServicesAll
ALTER TABLE Trees DISABLE TRIGGER TreesDelete
ALTER TABLE Checks DISABLE TRIGGER ChecksInsert
ALTER TABLE Checks DISABLE TRIGGER ChecksUpdate
ALTER TABLE PayCheck DISABLE TRIGGER PayCheckInsertUpdate
ALTER TABLE Purchase DISABLE TRIGGER PurchaseInsertUpdate
ALTER TABLE Purchase DISABLE TRIGGER PurchaseDelete
ALTER TABLE PurchaseItems DISABLE TRIGGER PurchaseItemsDelete
ALTER TABLE PurchaseItems DISABLE TRIGGER PurchaseItemsInsertUpdate
ALTER TABLE StoreDraft DISABLE TRIGGER StoreDraftInsertUpdate
ALTER TABLE StoreDraft DISABLE TRIGGER StoreDraftDelete
ALTER TABLE StoreDraftItems DISABLE TRIGGER StoreDraftItemsDelete
ALTER TABLE StoreDraftItems DISABLE TRIGGER StoreDraftItemsInsertUpdate
ALTER TABLE PurchaseRequest DISABLE TRIGGER PurchaseRequestInsertUpdate
ALTER TABLE PurchaseRequest DISABLE TRIGGER PurchaseRequestDelete
ALTER TABLE Request DISABLE TRIGGER RequestInsertUpdate
ALTER TABLE Request DISABLE TRIGGER RequestDelete
ALTER TABLE Returns DISABLE TRIGGER ReturnsInsertUpdate
ALTER TABLE Returns DISABLE TRIGGER ReturnsDelete
ALTER TABLE ReturnItem DISABLE TRIGGER ReturnItemDelete
ALTER TABLE ReturnItem DISABLE TRIGGER ReturnItemInsertUpdate
ALTER TABLE Refuse DISABLE TRIGGER RefuseInsertUpdate
ALTER TABLE Refuse DISABLE TRIGGER RefuseDelete
ALTER TABLE RefuseItem DISABLE TRIGGER RefuseItemDelete
ALTER TABLE RefuseItem DISABLE TRIGGER RefuseItemInsertUpdate
ALTER TABLE Transfer DISABLE TRIGGER TransferInsertUpdate
ALTER TABLE Transfer DISABLE TRIGGER TransferDelete
ALTER TABLE TransferItem DISABLE TRIGGER TransferItemDelete
ALTER TABLE TransferItem DISABLE TRIGGER TransferItemInsertUpdate
ALTER TABLE Cardex DISABLE TRIGGER CardexInsertUpdate
ALTER TABLE Service DISABLE TRIGGER ServiceUpdate
ALTER TABLE BuildOrder DISABLE TRIGGER BuildOrderInsertUpdate
ALTER TABLE BuildOrder DISABLE TRIGGER BuildOrderDelete
ALTER TABLE [Transfer] DISABLE TRIGGER TransferInsertUpdate
ALTER TABLE [Transfer] DISABLE TRIGGER TransferDelete
ALTER TABLE TransferItem DISABLE TRIGGER TransferItemDelete
ALTER TABLE TaskImpl DISABLE TRIGGER TaskImplInsert
ALTER TABLE Loan DISABLE TRIGGER LoanInsertUpdate
ALTER TABLE Installment DISABLE TRIGGER InstallmentInsertUpdate
UPDATE Loan SET LastYear = 1 WHERE VoucherID IS NOT NULL
UPDATE Loan SET VoucherID = NULL WHERE VoucherID IS NOT NULL
UPDATE Installment SET LastYear = 1 WHERE InstallmentVoucherID IS NOT NULL
UPDATE Installment SET InstallmentVoucherID = NULL WHERE InstallmentVoucherID IS NOT NULL
UPDATE DocumentAccountingItems SET CheckID = NULL, PayCheckID = NULL
UPDATE StoreDraft SET CheckID = NULL
UPDATE Purchase SET PayCheckID = NULL
DELETE FROM PayCheck WHERE Status IN(1,2,8) AND BankAccountCheckID IN 
(SELECT ID FROM BankAccountCheck WHERE BankAccountCheck.Finished = 1)--وصول شده ، برگشتی، ضمانتی پس گرفته شده
UPDATE PayCheck SET Amount = 0, ReceiverID = NULL, Status = 11 WHERE Status IN(1,2,8) AND 
BankAccountCheckID NOT IN (SELECT ID FROM BankAccountCheck WHERE BankAccountCheck.Finished = 1)-- چک  های وصول شده یا برگشتی از دسته چک های که هنوز تمام نشده اند رابه جای حذف باطل میکنیم
DELETE FROM Checks WHERE Status IN(1,2,5,9)-- وصول شده ، برگشتی ، خوابیده ی وصول شده ، ضمانتی پس داده شده
DELETE FROM TransferItem
DELETE FROM [Transfer]
DELETE FROM RequestItems
DELETE FROM Request
DELETE FROM ReturnItem
DELETE FROM [Returns]
DELETE FROM StoreDraftPerson
DELETE FROM StoreDraftItems
DELETE FROM StoreDraft
DELETE FROM RefuseItem
DELETE FROM Refuse
DELETE FROM PurchaseItems
DELETE FROM Purchase
DELETE FROM BuildOrderItem WHERE BuildOrderItem.BuildOrderID NOT IN (SELECT BuildOrderID FROM BuildOrderItem WHERE (BuildOrderItem.NumRequest - BuildOrderItem.Producted) > 1)
DELETE FROM BuildOrder WHERE ID NOT IN (SELECT BuildOrderID FROM BuildOrderItem WHERE (BuildOrderItem.NumRequest - BuildOrderItem.Producted) > 0)
DELETE FROM PurchaseRequestItems
DELETE FROM PurchaseRequest
DELETE FROM DocumentAccountingDocument
DELETE FROM TaskImpl
DELETE FROM TaxHistory
DELETE FROM WorkflowImpl
DELETE FROM DocumentAccountingItems WHERE DocumentAccountingID IN (SELECT ID FROM DocumentAccounting WHERE FinanceYearID IS NULL OR FinanceYearID<>" + newFinanceYearID + ") DELETE FROM DocumentAccounting WHERE FinanceYearID IS NULL OR  FinanceYearID<>" + newFinanceYearID + ""))
                    throw new Exception(LangProvider.Instance["FactorDelError"] + " : " + mDbProxy.LastException.Message);
            #endregion
            //if (mEmptyPersons.Checked)
              //  if (mDbProxy.RunQuery("DELETE FROM Personnel WHERE [Type] = 101") < 0)
                //    msg.Text += LangProvider.Instance["PersonDelError"] + "<br />";

            #region reenable triggers
            mDbProxy.RunQuery(@"
ALTER TABLE Workflow ENABLE TRIGGER WorkflowInsertUpdate
ALTER TABLE FinanceYear ENABLE TRIGGER FinanceYearInsertUpdate
ALTER TABLE DocumentAccountingItems ENABLE TRIGGER DocumentAccountingItemsInsertUpdate
ALTER TABLE DocumentAccountingItems ENABLE TRIGGER DocumentAccountingItemsDelete
ALTER TABLE TaskImpl ENABLE TRIGGER TaskImplInsert
ALTER TABLE StoreServices ENABLE TRIGGER StoreServicesAll
ALTER TABLE Trees ENABLE TRIGGER TreesDelete
ALTER TABLE Checks ENABLE TRIGGER ChecksInsert
ALTER TABLE Checks ENABLE TRIGGER ChecksUpdate
ALTER TABLE PayCheck ENABLE TRIGGER PayCheckInsertUpdate
ALTER TABLE Purchase ENABLE TRIGGER PurchaseInsertUpdate
ALTER TABLE Purchase ENABLE TRIGGER PurchaseDelete
ALTER TABLE PurchaseItems ENABLE TRIGGER PurchaseItemsDelete
ALTER TABLE PurchaseItems ENABLE TRIGGER PurchaseItemsInsertUpdate
ALTER TABLE StoreDraft ENABLE TRIGGER StoreDraftInsertUpdate
ALTER TABLE StoreDraft ENABLE TRIGGER StoreDraftDelete
ALTER TABLE StoreDraftItems ENABLE TRIGGER StoreDraftItemsDelete
ALTER TABLE StoreDraftItems ENABLE TRIGGER StoreDraftItemsInsertUpdate
ALTER TABLE PurchaseRequest ENABLE TRIGGER PurchaseRequestInsertUpdate
ALTER TABLE PurchaseRequest ENABLE TRIGGER PurchaseRequestDelete
ALTER TABLE Request ENABLE TRIGGER RequestInsertUpdate
ALTER TABLE Request ENABLE TRIGGER RequestDelete
ALTER TABLE Returns ENABLE TRIGGER ReturnsInsertUpdate
ALTER TABLE Returns ENABLE TRIGGER ReturnsDelete
ALTER TABLE ReturnItem ENABLE TRIGGER ReturnItemDelete
ALTER TABLE ReturnItem ENABLE TRIGGER ReturnItemInsertUpdate
ALTER TABLE Refuse ENABLE TRIGGER RefuseInsertUpdate
ALTER TABLE Refuse ENABLE TRIGGER RefuseDelete
ALTER TABLE RefuseItem ENABLE TRIGGER RefuseItemDelete
ALTER TABLE RefuseItem ENABLE TRIGGER RefuseItemInsertUpdate
ALTER TABLE Transfer ENABLE TRIGGER TransferInsertUpdate
ALTER TABLE Transfer ENABLE TRIGGER TransferDelete
ALTER TABLE TransferItem ENABLE TRIGGER TransferItemDelete
ALTER TABLE TransferItem ENABLE TRIGGER TransferItemInsertUpdate
ALTER TABLE Cardex ENABLE TRIGGER CardexInsertUpdate
ALTER TABLE BuildOrder ENABLE TRIGGER BuildOrderInsertUpdate
ALTER TABLE BuildOrder ENABLE TRIGGER BuildOrderDelete
ALTER TABLE [Transfer] ENABLE TRIGGER TransferInsertUpdate
ALTER TABLE [Transfer] ENABLE TRIGGER TransferDelete
ALTER TABLE TransferItem ENABLE TRIGGER TransferItemDelete
ALTER TABLE Service ENABLE TRIGGER ServiceUpdate
ALTER TABLE TaskImpl ENABLE TRIGGER TaskImplInsert
ALTER TABLE Loan ENABLE TRIGGER LoanInsertUpdate
ALTER TABLE Installment ENABLE TRIGGER InstallmentInsertUpdate");
            #endregion
            mDbProxy.RunQuery("UPDATE FinanceYear SET Status=2 WHERE ID = " + newFinanceYearID);
            mDbProxy.CommitTransaction();


            SiteSettings.FinanceYearStartDate = Helper.GetDateTime(newFinanceYear, "StartDate", Helper.NullDate);
            SiteSettings.FinanceYearEndDate = Helper.GetDateTime(newFinanceYear, "EndDate", Helper.NullDate);
            ShowMessage("<br />" + LangProvider.Instance["NewDbDone"] + "<br />");
        }
        catch (Exception ex)
        {
            ShowMessage(LangProvider.Instance["NewDbError"] + " : <br />" + ex.Message);
            mDbProxy.RollbackTransaction();
        }

        // don't detach
        //// Detach
        //string qry = string.Format("EXEC sp_detach_db '{0}', 'true'", dbName);
        //if (mDbProxy.RunQuery(qry, 5000) <= 0)
        //    msg.Text += "خطا در جدا کردن پشتیبان : <br />";
    }
    private bool Backup(string dbName)
    {
        try
        {
            string path = Server.MapPath("~/files/FDB");
            if (!System.IO.Directory.Exists(path))
                System.IO.Directory.CreateDirectory(path);


            path = System.IO.Path.Combine(path, dbName + ".bak");
            SqlAdmin sqlAdmin = new SqlAdmin();
            if (sqlAdmin.Backup(mDbProxy.ConnectionString, mDbProxy.CatalogName, path) != SqlAdmin.Operation_Result.Ok)
            {
                ShowMessage(LangProvider.Instance["BackupError"] + ": <br />");
                Exception ee = sqlAdmin.LastException;
                while (ee != null)
                {
                    ShowMessage(ee.Message + "<br />");
                    ee = ee.InnerException;
                }
                return false;
            }
            ShowMessage(string.Format(LangProvider.Instance["BackupDonePath"] + "<br />", path));
            mDbProxy.RunQuery(string.Format(@"IF EXISTS (SELECT * FROM sys.databases WHERE name='{0}') DROP DATABASE {0}", dbName));

            // restore
            string oldCont = mDbProxy.ConnectionString;


            try
            {
                string cnt = mDbProxy.ConnectionString;
                string oldCat = mDbProxy.CatalogName;
                string logicalFile = oldCat, logicalLog = oldCat + "_Log";
                DataTable dt = mDbProxy.FillData(string.Format("RESTORE FILELISTONLY  FROM DISK='{0}'", path), "fck");
                if (dt != null && dt.Rows.Count > 0)
                {
                    logicalFile = Helper.GetString(dt.Rows[0], "LogicalName", logicalFile);
                    logicalLog = logicalFile + "_Log";
                    if (dt.Rows.Count > 1)
                        logicalLog = Helper.GetString(dt.Rows[1], "LogicalName", logicalLog);
                }
                cnt = cnt.Replace(mDbProxy.CatalogName, "master");
                string catalog = dbName;
                string cmd = "CREATE DATABASE [" + catalog + "]; \r\n";
                cmd += "ALTER DATABASE [" + catalog + "] SET Single_User WITH Rollback Immediate; \r\n";
                //cmd += "RESTORE DATABASE [" + catalog + "] FROM DISK='" + path + "' WITH REPLACE ; \r\n";
                cmd += "RESTORE DATABASE [" + catalog + "] FROM DISK='" + path + "'  WITH MOVE '" + logicalFile + "' TO '" + path.Replace(".bak", ".mdf") + "'"
                    + ", MOVE '" + logicalLog + "' TO '" + path.Replace(".bak", "_log.mdf") + "', REPLACE; \r\n";
                cmd += "ALTER DATABASE [" + catalog + "] SET Multi_User;";

                mDbProxy.CloseConnection();
                mDbProxy.ConnectionString = cnt;
                mDbProxy.LastException = null;
                mDbProxy.RunQuery(cmd);
                if (mDbProxy.LastException != null)
                {
                    ShowMessage(LangProvider.Instance["RestoreError"] + " : <br />");
                    Exception ee = mDbProxy.LastException;
                    while (ee != null)
                    {
                        ShowMessage(ee.Message + "<br />");
                        ee = ee.InnerException;
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(LangProvider.Instance["RestoreError"] + ": <br />" + ex.Message);
            }
            finally
            {
                mDbProxy.CloseConnection();
                mDbProxy.ConnectionString = oldCont;
            }

            return true;
        }
        catch (Exception ex2)
        {
            ShowMessage(LangProvider.Instance["BackupError"] + " : " + ex2.Message);
            return false;
        }
    }

    private void ShowMessage(string message, string type="danger")
    {
        if (type != "success" && type != "danger")
        {
            type = "danger";
        }

        msg.Text += message;
        msg.Visible = true;
        msg.CssClass += " alert-" + type;
    }
    protected void mIsTemp_CheckedChanged(object sender, EventArgs e)
    {
        if (mIsTemp.Checked)
            mOpeningBalanceNameDiv.Visible = mClosingBalanceNameDiv.Visible = false;
        else
            mOpeningBalanceNameDiv.Visible = mClosingBalanceNameDiv.Visible = true;
    }
}