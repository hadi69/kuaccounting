﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GetColumn.aspx.cs" Inherits="sandbox_GetColumn" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="/js/jquery-1.9.1.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            Query: <asp:TextBox runat="server" ID="txtQuery"></asp:TextBox>
            <asp:Button runat="server" ID="btnGo" OnClick="btnGo_Click" Text="GO"/>
        </div>
        <br />
        <div>
            Result: <asp:TextBox runat="server" ID="txtResult" TextMode="MultiLine" style="width: 100%; height: 450px;"></asp:TextBox>
        </div>
    </div>
    </form>
    <script>
        $("#<%= txtResult.ClientID %>").focus(function () {
            var $this = $(this);
            $this.select();

            // Work around Chrome's little problem
            $this.mouseup(function () {
                // Prevent further mouseup intervention
                $this.unbind("mouseup");
                return false;
            });
        });
    </script>
</body>
</html>
