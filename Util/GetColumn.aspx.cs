﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.CSharp;

public partial class sandbox_GetColumn : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnGo_Click(object sender, EventArgs e)
    {
        DataSet dataSet = DbProxy.Instance.FillDataSet(txtQuery.Text);
        if (dataSet.Tables.Count > 0)
        {
            string result = "";
            SortedList<string, Type> columns = new SortedList<string, Type>();
            foreach (DataColumn column in dataSet.Tables[0].Columns)
            {
                columns.Add(column.ColumnName, column.DataType);
            }
            foreach (KeyValuePair<string, Type> column in columns)
            {
                result += "public " + GetColumnType(column.Value) + " " + column.Key + "{ get; set; }\n";

                if (column.Value == typeof(DateTime))
                {
                    result += "public string " + column.Key + "Jalali { get; set; }\n";
                }
            }

            txtResult.Text = result;
        }
    }

    private static string GetColumnType(Type type)
    {
        var compiler = new CSharpCodeProvider();
        var T = new CodeTypeReference(type);

        return compiler.GetTypeOutput(T);
    }
}