﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PurchaseView
/// </summary>
public class TransferView: BaseMap
{
    public TransferView()
    {
    }

    public TransferView(DataRow row)
        : base(row)
    {
    }

    // Properties
    public System.DateTime Date { get; set; }
    public string DateJalali { get; set; }
    public int DesiredEntryTaskID { get; set; }
    public int DestStoreID { get; set; }
    public string DestStoreName { get; set; }
    public int FinanceYearID { get; set; }
    public string FinanceYearName { get; set; }
    public int ID { get; set; }
    public string NOSerial { get; set; }
    public int NumPrints { get; set; }
    public string PreTaskName { get; set; }
    public string Reason { get; set; }
    public int SourceStoreID { get; set; }
    public string SourceStoreName { get; set; }
    public int StoreKeeperID { get; set; }
    public string StoreKeeperName { get; set; }
    public int TaskImplID { get; set; }
    public string TaskName { get; set; }
    public int WorkFlow { get; set; }
    public string WorkflowColor { get; set; }
    public int WorkflowStatus { get; set; }



    // Other properties
    public string WorkFlowStatusText
    {
        get
        {
            if (WorkflowStatus == 0)
            {
                return LangProvider.Instance.ParseLang("Lang.WFStatusInProgress");
            }

            if (WorkflowStatus == 1)
            {
                return LangProvider.Instance.ParseLang("Lang.WFStatusFinal");
            }

            return "";
        }
    }

    //public decimal GrandTotal
    //{
    //    get
    //    {
    //        decimal result = TotalPrice - TotalDiscount + TotalTax + VAT;
    //        if (!BuyerShipment)
    //        {
    //            result += ShipmentPrice;
    //        }

    //        return result;
    //    }
    //}

    // Relations
    public List<TransferItemsView> Items { get; set; }

    public static TransferView Find(int id)
    {
        const string queryName = "transfer";
        DataRow row = QueryById(id, queryName);

        if (row != null)
        {
            TransferView transferView = new TransferView(row);
            transferView.Items = TransferItemsView.FindByTransferId(id);

            return transferView;
        }
        else
        {
            return null;
        }
    }
}