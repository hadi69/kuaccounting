﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for LoansView
/// </summary>
public class LoansView:BaseMap
{
	public LoansView()
	{
	}

    public LoansView(DataRow row) : base(row) { }
    public decimal Amount { get; set; }
    public decimal BenefitPercent { get; set; }
    public string Comments { get; set; }
    public int FundID { get; set; }
    public string FundName { get; set; }
    public int ID { get; set; }
    public int InstallmentNum { get; set; }
    public decimal MonthlyPay { get; set; }
    public int PersonnelID { get; set; }
    public string PersonnelName { get; set; }
    public string StartDateJalali { get; set; }
    public int Status { get; set; }
    public decimal TotalPaied { get; set; }

    public static List<LoansView> FindLoansByFishId(int id)
    {
        List<LoansView> loans=new List<LoansView>();
        Dictionary<string,string> parameters=new Dictionary<string, string>{{"FishID",id.ToString()}};

        DataRowCollection rows = Query("getloansbyfishid", parameters);
        if (rows != null)
        {
            foreach (DataRow dataRow in rows)
            {
                loans.Add(new LoansView(dataRow));
            }
            
        }
        return loans;
    }

}