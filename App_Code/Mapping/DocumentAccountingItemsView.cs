﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DocumentAccountingItemsView
/// </summary>
public class DocumentAccountingItemsView: BaseMap
{
    public DocumentAccountingItemsView()
    { }

    public DocumentAccountingItemsView(DataRow row)
        :base(row)
    { }

    // Fields
    public int CheckID { get; set; }
    public string CheckName { get; set; }
    public string Comments { get; set; }
    public DateTime DocDate { get; set; }
    public string DocDateJalali { get; set; }
    public string DocNo { get; set; }
    public int DocumentAccountingID { get; set; }
    public decimal Due { get; set; }
    public int FileServiceID { get; set; }
    public int ID { get; set; }
    public int OtherMasterID { get; set; }
    public decimal Owe { get; set; }
    public int PayCheckID { get; set; }
    public string PayCheckName { get; set; }
    public bool PersonNeed { get; set; }
    public int PersonnelID { get; set; }
    public string PersonnelName { get; set; }
    public string PersonnelTreePathName { get; set; }
    public string ProgramCode { get; set; }
    public int EquipmentID { get; set; }
    public string EquipmentName { get; set; }
    public int ProjectID { get; set; }
    public string ProjectName { get; set; }
    public bool ProjectNeed { get; set; }
    public int SecPersonnelID { get; set; }
    public string SecTreePath { get; set; }
    public int SortIndex { get; set; }
    public long SyncVer { get; set; }
    public string TopicCode { get; set; }
    public string TopicTitle { get; set; }
    public string TreePath { get; set; }
    public string TreePathName { get; set; }

    public static List<DocumentAccountingItemsView> FindByDocumentAccountingId(int documentAccountingId)
    {
        Dictionary<string, string> parameters = new Dictionary<string, string> { { "DocumentAccountingID", documentAccountingId.ToString("") } };
        DataRowCollection rows = Query("documentaccountingitem", parameters);
        if (rows != null)
        {
            List<DocumentAccountingItemsView> items = new List<DocumentAccountingItemsView>();
            foreach (DataRow dataRow in rows)
            {
                items.Add(new DocumentAccountingItemsView(dataRow));
            }

            return items;
        }
        else
        {
            return null;
        }
    }
}