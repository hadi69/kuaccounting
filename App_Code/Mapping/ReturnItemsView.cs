﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for StoreDraftItemsView
/// </summary>
public class ReturnItemsView: BaseMap
{
    public ReturnItemsView()
    {
    }

    public ReturnItemsView(DataRow row)
        : base(row)
    { }

    public decimal BuyPrice { get; set; }
    public string Comments { get; set; }
    public decimal Discount { get; set; }
    public int ID { get; set; }
    public double Num { get; set; }
    public decimal Price { get; set; }
    public int ReturnID { get; set; }
    public decimal SalesManCommission { get; set; }
    public decimal SingleVAT { get; set; }
    public int StoreServiceID { get; set; }
    public string StoreServiceName { get; set; }
    public long SyncVer { get; set; }
    public int TabletID { get; set; }
    public int TabletSyncVer { get; set; }
    public int TabletUniqueID { get; set; }
    public decimal Tax { get; set; }
    public int UnitID { get; set; }
    public string UnitName { get; set; }
    public double UnitCurrencyConverter { get; set; }


    // Other fields
    public decimal ActualDiscount
    {
        get
        {
            if (Discount >= 0)
            {
                return SiteSettings.ToMonetary(Discount, rv.ReturnDate);
            }
            else
            {
                return Math.Round((TotalPrice * Math.Abs(Discount)) / 100);
            }
        }
    }
    public decimal ActualTax
    {
        get
        {
            if (Tax >= 0)
            {
                return SiteSettings.ToMonetary(Tax, rv.ReturnDate);
            }
            else
            {
                return ((TotalPrice - ActualDiscount) * Math.Abs(Tax)) / 100;
            }
        }
    }

    public decimal ActualSingleVAT
    {
        get
        {
            if (SingleVAT >= 0)
            {
                return SiteSettings.ToMonetary(SingleVAT, rv.ReturnDate);
            }
            else
            {
                return ((TotalPrice - ActualDiscount) * Math.Abs(SingleVAT)) / 100;
            }
        }
    }

    public Decimal TotalPrice
    {
        get
        {
            return SiteSettings.ToMonetary(Price, rv.ReturnDate) * Convert.ToDecimal(Num) * Helper.GetDecimal(UnitCurrencyConverter, 1);
        }
    }

    public Decimal ConvertedPrice
    {
        get
        {
            return SiteSettings.ToMonetary(Price, rv.ReturnDate);
        }
    }

    public ReturnView rv
    {
        get { return ReturnView.Find(ReturnID); }
    }

    public static List<ReturnItemsView> FindByReturnId(int returnId)
    {
        Dictionary<string, string> parameters = new Dictionary<string, string> { { "ReturnID", returnId.ToString("") } };
        DataRowCollection rows = BaseMap.Query("returnitem", parameters);
        if (rows != null)
        {
            List<ReturnItemsView> items = new List<ReturnItemsView>();
            foreach (DataRow dataRow in rows)
            {
                items.Add(new ReturnItemsView(dataRow));
            }

            return items;
        }
        else
        {
            return null;
        }
    }
}