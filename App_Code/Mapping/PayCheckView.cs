﻿using System;
using System.Activities.Expressions;
using System.Data;

public class PayCheckView: BaseMap
{
    public PayCheckView()
    {
    }

    public PayCheckView(DataRow row)
        :base(row)
    { }
    public decimal Amount { get; set; }
    public string AmountText
    {
        get { return Helper.NumberToText(Convert.ToInt64(Amount)); }
    }
    public DateTime DueDate { get; set; }
    public string DueDateJalali { get; set; }
    public string DueDateJalaliText
    {
        get { return Helper.ToFaFormatLong(DueDate); }
    }
    public int ReceiverID { get; set; }
    public string ReceiverName{get;set;    }
    public int BankPrintID { get; set; }
    public string BankPrintTitle { get; set; }
    public string BaseTemplate { get; set; }
    public int UserID { get; set; }
    public string No { get; set; }
    public DateTime PayDate { get; set; }
    public string PayDateJalali { get; set; }
    public string Serial { get; set; }
    public string Comments { get; set; }
    public string BranchName { get; set; }
    public string RealBankName { get; set; }
    public string ReceiverRealName { get; set; }
    public string PersonForPrint { get; set; }
    public bool AddToReceiverName { get; set; }

    public string RecieverOrPersonForReceiptPrint
    {
        get
        {
            if (!String.IsNullOrEmpty(PersonForPrint))
            {
                return PersonForPrint;
            }
            else
            {
                return ReceiverName;
            }
        }
    }

    public string RecieverOrPersonForPrint
    {
        get
        {
            if (!String.IsNullOrEmpty(PersonForPrint))
            {
                if (AddToReceiverName)
                {
                    return PersonForPrint + "-" + Comments;
                }
                else
                {
                    return PersonForPrint;
                }
            }
            else
            {
                if (AddToReceiverName)
                {
                    return ReceiverName + "-" + Comments;
                }
                else
                {
                    return ReceiverName;
                }
            }
        }
    }

    // Methods
    public static PayCheckView Find(int id)
    {
        string queryName = "paycheck";
        DataRow row = QueryById(id, queryName);

        if (row != null)
        {
            PayCheckView payCheckView = new PayCheckView(row);

            return payCheckView;
        }
        else
        {
            return null;
        }
    }
}
