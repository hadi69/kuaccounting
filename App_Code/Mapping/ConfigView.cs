﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ConfigView
/// </summary>
public class ConfigView: BaseMap
{
	public ConfigView()
	{
	}

    public ConfigView(DataRow row)
        : base(row)
    { }

    public int ID { get; set; }
    public string Name { get; set; }
    public string Display { get; set; }
    public int Type { get; set; }
    public string Value { get; set; }
    public bool Hidden { get; set; }
    public int Section { get; set; }
    public string DisplayValue { get; set; }
    public int ProgramID { get; set; }

    public static ConfigView FindByName(string name)
    {
        Dictionary<string, string> parameters = new Dictionary<string,string>{{ "Name", Helper.QoutedString(name) }};
        DataRowCollection rows = Query("getconfigviewitem", parameters);

        if (rows.Count > 0)
        {
            return new ConfigView(rows[0]);
        }
        else
        {
            return new ConfigView();
        }
    }
}