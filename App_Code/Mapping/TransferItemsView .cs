﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PurchaseItemsView
/// </summary>
public class TransferItemsView: BaseMap
{
    public TransferItemsView()
    {
    }

    public TransferItemsView(DataRow row)
        : base(row)
    {
    }

    public string Comments { get; set; }
    public int DestStoreServiceID { get; set; }
    public int ID { get; set; }
    public double Num { get; set; }
    public decimal Price { get; set; }
    public int ServiceVariateID { get; set; }
    public int StoreServiceID { get; set; }
    public string StoreServiceName { get; set; }
    public int TransferID { get; set; }
    public double UnitCurrencyConverter { get; set; }
    public int UnitID { get; set; }
    public string UnitName { get; set; }


    // Other fields
    //public decimal TotalPrice
    //{
    //    get { return Price*Convert.ToDecimal(Amount)* Helper.GetDecimal(UnitCurrencyConverter, 1); }
    //}


    public static List<TransferItemsView> FindByTransferId(int transferId)
    {
        Dictionary<string, string> parameters = new Dictionary<string, string>{{"TransferID", transferId.ToString("")}};
        DataRowCollection rows = Query("transferitem", parameters);
        if (rows != null)
        {
            return (from DataRow dataRow in rows select new TransferItemsView(dataRow)).ToList();
        }
        else
        {
            return null;
        }
    }
}