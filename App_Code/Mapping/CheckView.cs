﻿using System;
using System.Activities.Expressions;
using System.Data;


/// <summary>
/// Summary description for CheckView
/// </summary>
public class CheckView:BaseMap
{
    public CheckView()
    {
    }

    public CheckView(DataRow row)
        :base(row)
    { }
    public string AccountNo { get; set; }
    public int Amount { get; set; }
    public string AmountText
    {
        get { return Helper.NumberToText(Convert.ToInt64(Amount)); }
    }
    public int AssignorID { get; set; }
    public string AssignorName { get; set; }
    public int BankAccountID { get; set; }
    public string BankAccountName { get; set; }
    public string Branch { get; set; }
    public string Comments { get; set; }
    public System.DateTime DueDate { get; set; }
    public string DueDateJalali { get; set; }
    public string DueDateJalaliText
    {
        get { return Helper.ToFaFormatLong(DueDate); }
    }
    public string DueTreePath { get; set; }
    public string DueTreePathName { get; set; }
    public string FileName { get; set; }
    public System.DateTime FinalDate { get; set; }
    public string FinalDateJalali { get; set; }
    public int FinanceYearID { get; set; }
    public System.DateTime FollowUpTime { get; set; }
    public string FollowUpTimeJalali { get; set; }
    public int FromStoreDraft { get; set; }
    public int ID { get; set; }
    public bool IsGuarantee { get; set; }
    public int IsShop { get; set; }
    public System.DateTime IssueDate { get; set; }
    public string IssueDateJalali { get; set; }
    public string IssueDateJalaliText
    {
        get { return Helper.ToFaFormatLong(IssueDate); }
    }
    public string No { get; set; }
    public string OweTreePath { get; set; }
    public string OweTreePathName { get; set; }
    public int PersonnelID { get; set; }
    public string PersonnelName { get; set; }
    public int ProgramID { get; set; }
    public int ReceiveDest { get; set; }
    public string ReceiverBankName { get; set; }
    public string ReceiverBankTreePath { get; set; }
    public string ReceiverPerson { get; set; }
    public int ReceiverPersonID { get; set; }
    public string Serial { get; set; }
    public int Status { get; set; }
    public long SyncVer { get; set; }
    public int TabletID { get; set; }
    public int TabletSyncVer { get; set; }
    public int TabletUniqueID { get; set; }
    public string TreePath { get; set; }
    public string TreePathName { get; set; }
    public int UserID { get; set; }
    public int VisitorID { get; set; }
    public string VisitorName { get; set; }

    public string AssignorOrPersonName
    {
        get
        {
            if (!String.IsNullOrEmpty(AssignorName))
            {
                return AssignorName;
            }
            else
            {
                return PersonnelName;
            }
        }
    }

    public static CheckView Find(int id)
    {
        string queryName = "check";
        DataRow row = QueryById(id, queryName);

        if (row != null)
        {
            CheckView CheckView = new CheckView(row);

            return CheckView;
        }
        else
        {
            return null;
        }
    }

}