﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Service
/// </summary>
public class ServiceView: BaseMap
{
    public ServiceView()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public ServiceView(DataRow row)
        :base(row)
    {}

    public int ID{ get; set; }
    public string Code{ get; set; }
    public string BarCode{ get; set; }
    public string BarCodeesalat{ get; set; }
    public string BarCodeGomrok{ get; set; }
    public int CommodityGroupID{ get; set; }
    public int ServiceTypeID{ get; set; }
    public string Title{ get; set; }
    public string FormName{ get; set; }
    public string Title2{ get; set; }
    public string Acronym{ get; set; }
    public int UnitID{ get; set; }
    public int TwoUnitID{ get; set; }
    public string Comments{ get; set; }
    public bool Enabled{ get; set; }
    public int DrugTypeID{ get; set; }
    public string Doze{ get; set; }
    public int ConsumptionTypeID{ get; set; }
    public bool Global{ get; set; }
    public bool Midwife{ get; set; }
    public bool Expert{ get; set; }
    public bool Drug{ get; set; }
    public bool Hoteling{ get; set; }
    public int TestTypeID{ get; set; }
    public int ResultType{ get; set; }
    public string DefaultValue{ get; set; }
    public int Days{ get; set; }
    public bool Emergency{ get; set; }
    public int RSTypeID{ get; set; }
    public int RSNormalID{ get; set; }
    public int QuicklyAccessCode{ get; set; }
    public int SurgeryTypeID{ get; set; }
    public int SurgeryGroupID{ get; set; }
    public string EnumTable{ get; set; }
    public int StoringID{ get; set; }
    public int ServiceCodingID{ get; set; }
    public bool IsVirtual{ get; set; }
    public string StroingDesc{ get; set; }
    public int MaxUsage{ get; set; }
    public int Pricing{ get; set; }
    public bool IsYaraneh{ get; set; }
    public string ServiceTypeName{ get; set; }
    public int ServiceTypeType{ get; set; }
    public string UnitName{ get; set; }
    public string DrugTypeName{ get; set; }
    public string ConsumptionTypeName{ get; set; }
    public string RSTypeName{ get; set; }
    public string CommodityGroupName{ get; set; }
    public string StoringName{ get; set; }
    public string ServiceCodingName{ get; set; }
    public string TwoUnitName{ get; set; }

    public static ServiceView Find(int id)
    {
        return new ServiceView(QueryById(id, "service"));
    }
}