﻿using System.Data;

public class BankPrint: BaseMap
{
    public BankPrint()
    {
    }

    public BankPrint(DataRow row)
        :base(row)
    { }
    public int ID { get; set; }
    public long SyncVer { get; set; }
    public string Template { get; set; }
    public string Title { get; set; }

    // Methods
    public static BankPrint Find(int id)
    {
        string querName = "bankprint";
        DataRow row = QueryById(id, querName);

        if (row != null)
        {
            BankPrint bankPrint = new BankPrint(row);

            return bankPrint;
        }
        else
        {
            return null;
        }
    }

}