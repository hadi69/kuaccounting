﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for StoreDraftItemsView
/// </summary>
public class PurchaseRequestItemsView: BaseMap
{
    public PurchaseRequestItemsView()
    {
    }

    public PurchaseRequestItemsView(DataRow row)
        : base(row)
    { }

    public double Amount { get; set; }
    public string Comment { get; set; }
    public int ID { get; set; }
    public decimal Price { get; set; }
    public int PurchaseRequestID { get; set; }
    public string ServiceName { get; set; }
    public int ServicePriceID { get; set; }
    public int UnitID { get; set; }
    public string UnitName { get; set; }
    public float UnitCurrencyConverter { get; set; }

    public Decimal TotalPrice
    {
        get
        {
            return Price * Convert.ToDecimal(Amount) * Helper.GetDecimal(UnitCurrencyConverter, 1);
        }
    }

    public static List<PurchaseRequestItemsView> FindByPurchaseRequestId(int purchaseRequestId)
    {
        Dictionary<string, string> parameters = new Dictionary<string, string> { { "PurchaseRequestID", purchaseRequestId.ToString("") } };
        DataRowCollection rows = BaseMap.Query("purchaserequestitem", parameters);
        if (rows != null)
        {
            List<PurchaseRequestItemsView> items = new List<PurchaseRequestItemsView>();
            foreach (DataRow dataRow in rows)
            {
                items.Add(new PurchaseRequestItemsView(dataRow));
            }

            return items;
        }
        else
        {
            return null;
        }
    }
}