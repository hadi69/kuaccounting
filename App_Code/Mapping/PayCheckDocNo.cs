﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PayCheckDocNo
/// </summary>
public class PayCheckDocNo: BaseMap
{
    public PayCheckDocNo()
    {
    }

    public PayCheckDocNo(DataRow row)
        :base(row)
    { }

    public int ID { get; set; }
    public string No { get; set; }
    public int NumericNo { get { return int.Parse(No); } }

    // Methods
    public static PayCheckDocNo Find(int id)
    {
        string queryName = "paycheckvouchers";
        DataRowCollection rows = Query(queryName, new Dictionary<string, string>{{"Title", id.ToString("")}});

        if (rows != null && rows.Count > 0)
        {
            PayCheckDocNo payCheckDocNo = new PayCheckDocNo(rows[0]);

            return payCheckDocNo;
        }
        else
        {
            return null;
        }
    }
}