﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for StoreDraftItemsView
/// </summary>
public class StoreDraftItemsView: BaseMap
{
    public StoreDraftItemsView()
    {
    }

    public StoreDraftItemsView(DataRow row): base(row)
    { }

    public int ID { get; set; }
    public int StoreDraftID { get; set; }
    public int DistReportID { get; set; }
    public double RequestedAmount { get; set; }
    public double DeliveredAmount { get; set; }
    public int UnitID { get; set; }
    public Decimal Price { get; set; }
    public Decimal Discount { get; set; }
    public Decimal Tax { get; set; }
    public string Comments { get; set; }
    public Decimal SingleVAT { get; set; }
    public int Source { get; set; }
    public double UnitCurrencyConverter { get; set; }
    public Decimal SalesManCommission { get; set; }
    public Decimal BasePrice { get; set; }
    public Decimal Profit { get; set; }
    public int SaleTypeID { get; set; }
    public string StoreServiceName { get; set; }
    public string UnitName { get; set; }
    public string MainUnitName { get; set; }
    public string SaleTypeName { get; set; }
    public int ServiceID { get; set; }
    public string Code { get; set; }

    public Decimal ItemCashPrice { get; set; }
    public Decimal ItemBankPrice { get; set; }
    public Decimal ItemCheck1Price { get; set; }
    public Decimal ItemCheck2Price { get; set; }
    public string Label { get; set; }
    public int LabelID { get; set; }

    // Other fields
    public decimal ActualDiscount
    {
        get
        {
            if (Discount >= 0)
            {
                return Discount;
            }
            else
            {
                return Math.Round((TotalPrice * Math.Abs(Discount)) / 100);
            }
        }
    }
    public decimal ActualTax
    {
        get
        {
            if (Tax >= 0)
            {
                return Tax;
            }
            else
            {
                return ((TotalPrice-ActualDiscount) * Math.Abs(Tax)) / 100;
            }
        }
    }

    public double DistGrateDeliverAmount
    {
        get
        {
            if (UnitCurrencyConverter > 1)
            {
                return Math.Floor(DeliveredAmount);
            }
            else
            {
                return 0;
            }
        }
    }
    public double DistDeliverAmount
    {
        get
        {
            if (UnitCurrencyConverter > 1)
            {
                return (DeliveredAmount % Math.Floor(DeliveredAmount)) * UnitCurrencyConverter;
            }
            else
            {
                return DeliveredAmount;
            }
        }
    }

    public decimal ActualSingleVAT
    {
        get
        {
            if (SingleVAT >= 0)
            {
                return SingleVAT;
            }
            else
            {
                return ((TotalPrice-ActualDiscount) * Math.Abs(SingleVAT)) / 100;
            }
        }
    }

    public Decimal TotalPrice
    {
        get
        {
            return Price * Convert.ToDecimal(DeliveredAmount) * Helper.GetDecimal(UnitCurrencyConverter, 1);
        }
    }

    public decimal ItemCashDiscount
    {
        get
        {
            if (TotalPrice - ItemCashPrice > 0)
            {
                return TotalPrice - ItemCashPrice; 
            }
            else
            {
                return 0;
            }
        }
    }

    public decimal ItemBankDiscount
    {
        get
        {
            if (TotalPrice - ItemBankPrice > 0)
            {
                return TotalPrice - ItemBankPrice;
            }
            else
            {
                return 0;
            }
        }
    }

    public decimal ItemCheck1Discount
    {
        get
        {
            if (TotalPrice - ItemCheck1Price > 0)
            {
                return TotalPrice - ItemCheck1Price;
            }
            else
            {
                return 0;
            }
        }
    }

    public decimal ItemCheck2Discount
    {
        get
        {
            if (TotalPrice - ItemCheck2Price > 0)
            {
                return TotalPrice - ItemCheck2Price;
            }
            else
            {
                return 0;
            }
        }
    }

    public static List<StoreDraftItemsView> FindByStoreDraftId(int storeDraftId)
    {
        Dictionary<string, string> parameters = new Dictionary<string, string>{{"StoreDraftID", storeDraftId.ToString("")}};
        DataRowCollection rows = BaseMap.Query("storedraftitem", parameters);
        if (rows != null)
        {
            List<StoreDraftItemsView> items = new List<StoreDraftItemsView>();
            foreach (DataRow dataRow in rows)
            {
                items.Add(new StoreDraftItemsView(dataRow));
            }

            return items;
        }
        else
        {
            return null;
        }
    }
}