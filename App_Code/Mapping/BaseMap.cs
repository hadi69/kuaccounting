﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using Microsoft.SqlServer.Management.Common;

/// <summary>
/// Base class for all mappings
/// </summary>
public class BaseMap
{
    public BaseMap()
    {
    }

    public BaseMap(DataRow row)
    {
        ParseRow(row);
    }

    public BaseMap ParseRow(DataRow row)
    {
        if (row == null)
        {
            return this;
        }
        if (row.Table == null)
        {
            throw new InvalidArgumentException("The Table property of the DataRow can not be null");
        }

        foreach (DataColumn column in row.Table.Columns)
        {
            // Set the value for this column
            SetValue(column.ColumnName, row[column.ColumnName]);

            // If this column is of type "DateTime"
            if (column.DataType == typeof (DateTime))
            {
                SetValue(column.ColumnName + "Jalali", 
                    Helper.FormatDate(Helper.GetDateTime(row[column.ColumnName], Helper.NullDate), null));
                SetValue(column.ColumnName + "Time",
                    Helper.ToFaFormat(Helper.GetDateTime(row[column.ColumnName], Helper.NullDate), "HH : mm"));
            }
        }
        return this;
    }

    public static DataRow QueryById(int id, string queryName)
    {
        Dictionary<string, string> parameters = new Dictionary<string, string> {{"ID", id.ToString("")}};
        DataRowCollection rows = BaseMap.Query(queryName, parameters);
        if (rows != null && rows.Count > 0)
        {
            return rows[0];
        }
        else
        {
            return null;
        }
    }

    public static DataTable QueryToTable(string queryName, Dictionary<string, string> parameters)
    {
        QueryInfo qInfo = QueryController.Instance[queryName];
        string query = "";
        if (qInfo != null && !string.IsNullOrEmpty(qInfo.Query))
        {
            query = qInfo.Query;
        }

        if (parameters != null)
        {
            foreach (string param in parameters.Keys)
            {
                query = query.Replace("@" + param, parameters[param]);
            }
        }
        DataSet dataSet = DbProxy.Instance.FillDataSet(query);
        if (dataSet.Tables.Count > 0)
        {
            return dataSet.Tables[0];
        }
        else
        {
            return null;
        }
    }

    public static DataRowCollection Query(string queryName, Dictionary<string, string> parameters)
    {
        DataTable table = QueryToTable(queryName, parameters);

        if (table != null && table.Rows.Count > 0)
        {
            return table.Rows;
        }
        else
        {
            return null;
        }
    }

    private void SetValue(string name, object value)
    {
        try
        {
            if (value.GetType() != typeof (System.DBNull))
            {
                PropertyInfo propertyInfo = GetType().GetProperty(name);
                propertyInfo.SetValue(this, value, null);
            }
        }
        catch (NullReferenceException exception)
        {
            Logger.Log(exception, "Field name: " + name);
        }
    }


    public string AppName
    {
        get { return LangProvider.Instance.ParseLang("Lang.AppName"); }
    }
}