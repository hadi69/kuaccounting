﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for FishView
/// </summary>
public class FishView:BaseMap
{
	public FishView()
	{
	}

    public FishView(DataRow row)
        : base(row)
    { }

    // Fields mapped from database
    public int ID { get; set; }
    public int PersonnelID { get; set; }
    public int OrderID { get; set; }
    public int FishDefID { get; set; }
    public int Status { get; set; }
    public int FishYear { get; set; }
    public int FishMonth { get; set; }
    public string DateJalali { get; set; }
    public string PersonnelName { get; set; }
    public string FishDefName { get; set; }
    public string OrderName { get; set; }

    // Wages
    public decimal InsuranceWork { get; set; }
    public decimal DayWage { get; set; }
    public decimal InsuranceWage { get; set; }
    public decimal IncludedBenefit { get; set; }
    public decimal IncludedWage { get; set; }
    public decimal TotalIncludedUnIncluded { get; set; }
    public decimal InsuranceWorker { get; set; }

    public decimal Insurance20 { get; set; }

    public decimal Insurance3 { get; set; }

    public decimal Insurance10 { get; set; }
    public decimal InsurancePrice { get; set; }
    public decimal TotalPayed { get; set; }
    public decimal Tax { get; set; }

    //Personnel
    public string PasportNo { get; set; }
    public string NationalCode { get; set; }
    public string Father { get; set; }
    public string InsuranceNo { get; set; }
    public string Job { get; set; }
    public string StartDateJalali { get; set; }
    public string EndDateJalali { get; set; }
    public string AccountNo { get; set; }
    public string BankCardNo { get; set; }
    public string PersonneliNum { get; set; }
    public bool HasInsurance { get; set; }

    // Methods
    public static FishView Find(int id)
    {
        string querName = "fish";
        DataRow row = BaseMap.QueryById(id, querName);

        if (row != null)
        {
            FishView fishView = new FishView(row);
            
           

            return fishView;
        }
        else
        {
            return null;
        }
    }

    public static List<FishView> FindFishViewByMonth(int year, int month, int section)
    {
        List<FishView> flist = new List<FishView>();
        string qry = "SELECT * FROM FishView  WHERE FishYear=" + year + " AND FishMonth=" + month;
        if (section > 0)
        {
            qry += " AND WardID=" + section;
        }
        DataTable fishs = DbProxy.Instance.FillData(qry, "fishs");
        if (fishs!=null && fishs.Rows.Count > 0)
        {
            for (int i = 0; i < fishs.Rows.Count; i++)
            {
                FishView fishView = new FishView(fishs.Rows[i]);

                DataTable personnelInfo =
                    DbProxy.Instance.FillData(
                        "SELECT * FROM Personnel WHERE ID=" + Helper.GetInt(fishs.Rows[i],"PersonnelID", -1)
                        , "personnelInfo");
                //Insurance
                fishView.InsuranceWork = FishValuesView.FindFishValuesByFieldName(fishView.ID, Helper.GetField("WorkDays", ReportFieldType.Insurance)).Value;
                fishView.DayWage = FishValuesView.FindFishValuesByFieldName(fishView.ID, Helper.GetField("DayWage",ReportFieldType.Insurance)).Value;
                fishView.InsuranceWage = FishValuesView.FindFishValuesByFieldName(fishView.ID, Helper.GetField("InsuranceWage",ReportFieldType.Insurance)).Value;
                fishView.IncludedBenefit = FishValuesView.FindFishValuesByFieldName(fishView.ID, Helper.GetField("BenefitWage",ReportFieldType.Insurance)).Value;
                fishView.IncludedWage = FishValuesView.FindFishValuesByFieldName(fishView.ID, Helper.GetField("MonthWage",ReportFieldType.Insurance)).Value;
                fishView.TotalIncludedUnIncluded = FishValuesView.FindFishValuesByFieldName(fishView.ID, Helper.GetField("TotalPrice",ReportFieldType.Insurance)).Value;
                fishView.InsuranceWorker = FishValuesView.FindFishValuesByFieldName(fishView.ID, Helper.GetField("Insurance7",ReportFieldType.Insurance)).Value;
                fishView.Insurance20 = FishValuesView.FindFishValuesByFieldName(fishView.ID, Helper.GetField("Insurance20", ReportFieldType.Insurance)).Value;
                fishView.Insurance10 = FishValuesView.FindFishValuesByFieldName(fishView.ID, Helper.GetField("Insurance10", ReportFieldType.Insurance)).Value;
                fishView.Insurance3 = FishValuesView.FindFishValuesByFieldName(fishView.ID, Helper.GetField("Insurance3", ReportFieldType.Insurance)).Value;
                fishView.InsurancePrice = FishValuesView.FindFishValuesByFieldName(fishView.ID, Helper.GetField("Isnurance30",ReportFieldType.Insurance)).Value;
                fishView.Tax = FishValuesView.FindFishValuesByFieldName(fishView.ID, Helper.GetField("Tax", ReportFieldType.Tax)).Value;
                fishView.TotalPayed = FishValuesView.FindFishValuesByFieldName(fishView.ID, Helper.GetField("TotalPayed",ReportFieldType.Insurance)).Value;
                fishView.PasportNo = PersonnelView.Find(fishView.PersonnelID).IDCardNo;
                fishView.NationalCode = PersonnelView.Find(fishView.PersonnelID).NatinoalCode;
                fishView.Father = PersonnelView.Find(fishView.PersonnelID).Father;
                fishView.InsuranceNo = PersonnelView.Find(fishView.PersonnelID).InsuranceNo;
                fishView.Job = PersonnelView.Find(fishView.PersonnelID).JobName;
                fishView.StartDateJalali = Helper.ToFaFormat(PersonnelView.Find(fishView.PersonnelID).StartDate);
                fishView.EndDateJalali = Helper.ToFaFormat(PersonnelView.Find(fishView.PersonnelID).EndDate);
                fishView.AccountNo = PersonnelView.Find(fishView.PersonnelID).AccountNo;

                //string InsuranceTypeCode = Helper.GetEnumValuesCode(Helper.GetString(personnelInfo.Rows[0], "InsuranceTypeID", ""));
                //if (InsuranceTypeCode == "1")
                //    flist.Add(fishView);
                if (Helper.GetBool(fishView.HasInsurance, false))
                    flist.Add(fishView);
            }
        }
        return flist;
    }

    public static List<FishView> FindTotalPaidByMonth(int year, int month)
    {
        List<FishView> flist = new List<FishView>();
        Dictionary<string, string> parameters = new Dictionary<string, string>
        {
            {"FishMonth", month.ToString()},
            {"FishYear", year.ToString()}
        };
        DataRowCollection rows = Query("getfishview", parameters);
        if (rows != null && rows.Count > 0)
        {
            for (int i = 0; i < rows.Count; i++)
            {
                FishView fishView = new FishView(rows[i]);

                fishView.TotalPayed = FishValuesView.FindFishValuesByFieldName(fishView.ID, Helper.GetField("FTotalPayed", ReportFieldType.Account)).Value;
                fishView.AccountNo = PersonnelView.Find(fishView.PersonnelID).AccountNo;
                fishView.BankCardNo = PersonnelView.Find(fishView.PersonnelID).BankCardNo;
                flist.Add(fishView);
            }
        }
        return flist;
    }

    public static List<FishView> FindFishViews(int year, int month, string fromNum, string toNum)
    {
        List<FishView> flist = new List<FishView>();
        string query = string.Format(@"SELECT FishView.*, PersonneliNum FROM FishView INNER JOIN Personnel ON FishView.PersonnelID=Personnel.ID WHERE FishYear={0} AND FishMonth={1}", year, month);
        if (!string.IsNullOrEmpty(fromNum))
        {
            query += " AND PersonneliNum>=" + fromNum;
        }
        if (!string.IsNullOrEmpty(toNum))
        {
            query += " AND PersonneliNum<=" + toNum;
        }

        query += " ORDER BY PersonneliNum";

        IDataReader dr = DbProxy.Instance.ExecuteReader(query);
        flist = ObjectBinder.FillCollection<FishView>(dr);
        return flist;
    } 
}