﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Stimulsoft.Report;

/// <summary>
/// Mapping DataTable to POCO for StoreDraftView
/// </summary>
public class StoreDraftView : BaseMap
{
    public StoreDraftView()
    {
    }

    public StoreDraftView(DataRow row)
        : base(row)
    {
    }

    // Fields mapped from database
    public int ID { get; set; }
    public DateTime Date { get; set; }
    public string DateJalali { get; set; }
    public int PersonnelID { get; set; }
    public int StoreID { get; set; }
    public string Comments { get; set; }
    public int FileServiceID { get; set; }
    public int WardID { get; set; }
    public int AccepterID { get; set; }
    public int FinanceYearID { get; set; }
    public bool IsTrust { get; set; }
    public string NOSerial { get; set; }
    public int UserID { get; set; }
    public int WorkflowStatus { get; set; }
    public int BuyerID { get; set; }
    public string OweTreePath { get; set; }
    public string DueTreePath { get; set; }
    public Decimal TotalPrice { get; set; }
    public Decimal TotalDiscount { get; set; }
    public Decimal FactorTotalDiscount { get; set; }
    public Decimal TotalTax { get; set; }
    public Decimal VAT { get; set; }
    public Decimal ShipmentPrice { get; set; }
    public bool SellerShipment { get; set; }
    public int VisitorID { get; set; }
    public Decimal Pursant { get; set; }
    public int PayMethod { get; set; }
    public bool NoPrintFactorDate { get; set; }
    public string PersonForPrint { get; set; }

    public string PayMethodText
    {
        get
        {
            switch (PayMethod)
            {
                case 0:
                    return LangProvider.Instance.ParseLang("Lang.PayMethodCash");
                case 1:
                    return LangProvider.Instance.ParseLang("Lang.PayMethodCheck1");
                case 2:
                    return LangProvider.Instance.ParseLang("Lang.PayMethodOwe");
                case 3:
                    return LangProvider.Instance.ParseLang("Lang.PayMethodComplex");
                case 4:
                    return LangProvider.Instance.ParseLang("Lang.PayMethodBank");
                case 5:
                    return LangProvider.Instance.ParseLang("Lang.PayMethodCheck2");
                default:
                    return "";
            }
        }
    }

    public string CheckProps { get; set; }
    public DateTime FactorDate { get; set; }
    public string FactorDateJalali { get; set; }
    public string FactorDateTime { get; set; }
    public int StoreKeeperID { get; set; }
    public int AccountantID { get; set; }
    public int CheckID { get; set; }
    public bool IsInternal { get; set; }
    public int ProgramID { get; set; }
    public Decimal CashPrice { get; set; }
    public string CashTreePath { get; set; }
    public Decimal CheckPrice { get; set; }
    public string CheckTreePath { get; set; }
    public Decimal OwedPrice { get; set; }
    public string OwedTreePath { get; set; }
    public string ShipmentTreePath { get; set; }
    public int DesiredEntryTaskID { get; set; }
    public Decimal OfferDiscount { get; set; }
    public int PayAllowedDays { get; set; }
    public string OweTreePathName { get; set; }
    public string OweTopicCode { get; set; }
    public string OweTopicTitle { get; set; }
    public string DueTreePathName { get; set; }
    public string DueTopicCode { get; set; }
    public string DueTopicTitle { get; set; }
    public string PersonnelName { get; set; }
    public string StoreName { get; set; }
    public string WardName { get; set; }
    public string AccepterName { get; set; }
    public string TaskName { get; set; }
    public string FinanceYearName { get; set; }
    public string VisitorName { get; set; }
    public string BuyerName { get; set; }
    public string StoreKeeperName { get; set; }
    public string AccountantName { get; set; }
    public string CheckName { get; set; }
    public double TotalAmount { get; set; }
    public int VoucherID { get; set; }
    public string VoucherNo { get; set; }
    public string CashTreePathName { get; set; }
    public string CashTopicCode { get; set; }
    public string CashTopicTitle { get; set; }
    public string CheckTreePathName { get; set; }
    public string CheckTopicCode { get; set; }
    public string CheckTopicTitle { get; set; }
    public string OwedTreePathName { get; set; }
    public string OwedTopicCode { get; set; }
    public string OwedTopicTitle { get; set; }
    public string ShipmentTreePathName { get; set; }
    public string ShipmentTopicCode { get; set; }
    public decimal OfferCashPrice { get; set; }
    public decimal OfferBankPrice { get; set; }
    public decimal OfferCheck1Price { get; set; }
    public decimal OfferCheck2Price { get; set; }
    public string BuyerNameComment { get; set; }
    public DateTime PayDate { get; set; }
    public string PayDateJalali { get; set; }


    // Other fields

    public string WorkFlowStatusText
    {
        get
        {
            if (WorkflowStatus == 0)
            {
                return LangProvider.Instance.ParseLang("Lang.WFStatusInProgress");
            }

            if (WorkflowStatus == 1)
            {
                return LangProvider.Instance.ParseLang("Lang.WFStatusFinal");
            }

            return "";
        }
    }

    public string IsTrusText
    {
        get { return Helper.BoolToYesOrNo(IsTrust); }
    }

    public string IsInternalText
    {
        get { return Helper.BoolToYesOrNo(IsInternal); }
    }

    /// <summary>
    /// جمع تعداد/مقدار
    /// </summary>
    public double SumDeliveredAmount
    {
        get
        {
            return Items.Sum(storeDraftItemsView =>
                storeDraftItemsView.DeliveredAmount * storeDraftItemsView.UnitCurrencyConverter);
        }
    }

    public string LabelsName
    {
        get
        {
            string labelsName = "";
            foreach (var item in Items)
            {
                if (!string.IsNullOrEmpty(item.Label) && !string.IsNullOrWhiteSpace(item.Label))
                {
                    if (!labelsName.Contains(item.Label))
                    {
                        PersonnelView labelInfo = PersonnelView.Find(item.LabelID);
                        labelsName += " | " + item.Label + " ، " + labelInfo.HomeAddress;
                    }
                }
            }
            return labelsName;
        }
    }

    /// <summary>
    /// مبلغ نهایی
    /// </summary>
    public decimal SumTotalPrice
    {
        get { return SiteSettings.ToMonetary(Items.Sum(storeDraftItemsView => storeDraftItemsView.TotalPrice), FactorDate);}
    }

    public string GrandTotalText
    {
        get
        {
            decimal result = SumTotalPrice - SumActualDiscount + SumActualSingleVAT + SumActualTax;

            if (!SellerShipment)
            {
                result += ShipmentPrice;
            }

            return Helper.NumberToText(Convert.ToInt64(result));
        }
    }

    public decimal CashDiscount
    {
        get
        {
            if (OfferCashPrice > 0)
            {
                return SiteSettings.ToMonetary(TotalPrice - OfferCashPrice, FactorDate);
            }
            else
            {
                return 0;
            }
        }
    }

    public decimal BankDiscount
    {
        get
        {
            if (OfferBankPrice > 0)
            {
                return SiteSettings.ToMonetary(TotalPrice - OfferBankPrice, FactorDate);
            }
            else
            {
                return 0;
            }
        }
    }

    public decimal Check1Discount
    {
        get
        {
            if (OfferCheck1Price > 0)
            {
                return SiteSettings.ToMonetary(TotalPrice - OfferBankPrice, FactorDate);
            }
            else
            {
                return 0;
            }
        }
    }

    public decimal Check2Discount
    {
        get
        {
            if (OfferCheck2Price > 0)
            {
                return SiteSettings.ToMonetary(TotalPrice - OfferCheck2Price, FactorDate);
            }
            else
            {
                return 0;
            }
        }
    }

    public decimal OfferCashPriceTotal
    {
        get
        {
            decimal result = OfferCashPrice - (SumActualDiscount - SiteSettings.ToMonetary(OfferDiscount, FactorDate)) + SumActualSingleVAT + SumActualTax;

            if (!SellerShipment)
            {
                result += ShipmentPrice;
            }

            return result;
        }
    }

    public decimal OfferBankPriceTotal
    {
        get
        {
            decimal result = OfferBankPrice - (SumActualDiscount - SiteSettings.ToMonetary(OfferDiscount, FactorDate)) + SumActualSingleVAT + SumActualTax;

            if (!SellerShipment)
            {
                result += ShipmentPrice;
            }

            return result;
        }
    }
    public decimal OfferCheck1PriceTotal
    {
        get
        {
            decimal result = OfferCheck1Price - (SumActualDiscount - SiteSettings.ToMonetary(OfferDiscount, FactorDate)) + SumActualSingleVAT + SumActualTax;

            if (!SellerShipment)
            {
                result += ShipmentPrice;
            }

            return result;
        }
    }
    public decimal OfferCheck2PriceTotal
    {
        get
        {
            decimal result = OfferCheck2Price - (SumActualDiscount - SiteSettings.ToMonetary(OfferDiscount, FactorDate)) + SumActualSingleVAT + SumActualTax;

            if (!SellerShipment)
            {
                result += SiteSettings.ToMonetary(ShipmentPrice, FactorDate);
            }

            return result;
        }
    }

    /// <summary>
    /// تخفیف کل
    /// </summary>
    public decimal SumDiscount
    {
        get { return SiteSettings.ToMonetary(Items.Sum(storeDraftItemsView => storeDraftItemsView.Discount), FactorDate) + OfferDiscount;}
    }

    /// <summary>
    /// تخفیف کل واقعی (با حتساب درصد یا عادی بودن تخفیف)
    /// </summary>
    public decimal SumActualDiscount
    {
        get { return SiteSettings.ToMonetary(Items.Sum(storeDraftItemsView => storeDraftItemsView.ActualDiscount) + OfferDiscount, FactorDate) + ActualFactorTotalDiscount; }
    }

    public decimal ActualFactorTotalDiscount
    {
        get
        {
            if (FactorTotalDiscount >= 0)
            {
                return SiteSettings.ToMonetary(FactorTotalDiscount, FactorDate);
            }
            else
            {
                return SiteSettings.ToMonetary(Math.Round((TotalPrice * Math.Abs(FactorTotalDiscount)) / 100), FactorDate);
            }
        }
    }

    /// <summary>
    /// مالیات کل
    /// </summary>
    public decimal SumTax
    {
        get { return SiteSettings.ToMonetary(Items.Sum(storeDraftItemsView => storeDraftItemsView.Tax), FactorDate); }
    }

    /// <summary>
    /// مالیات کل واقعی (با حتساب درصد یا عادی بودن مالیات)
    /// </summary>
    public decimal SumActualTax
    {
        get { return SiteSettings.ToMonetary(Items.Sum(storeDraftItemsView => storeDraftItemsView.ActualTax), FactorDate); }
    }

    /// <summary>
    /// عوارض کل
    /// </summary>
    public decimal SumSingleVAT
    {
        get { return SiteSettings.ToMonetary(Items.Sum(storeDraftItemsView => storeDraftItemsView.SingleVAT), FactorDate); }
    }

    /// <summary>
    /// عوارض کل واقعی (با حتساب درصد یا عادی بودن عوارض)
    /// </summary>
    public decimal SumActualSingleVAT
    {
        get { return SiteSettings.ToMonetary(Items.Sum(storeDraftItemsView => storeDraftItemsView.ActualSingleVAT), FactorDate); }
    }

    /// <summary>
    /// مبلغ نهایی
    /// </summary>
    public decimal GrandTotal
    {
        get
        {
            decimal result = SumTotalPrice - SumActualDiscount + SumActualSingleVAT + SumActualTax;

            if (!SellerShipment)
            {
                result += SiteSettings.ToMonetary(ShipmentPrice, FactorDate);
            }

            return result;
        }
    }

    public string ShellerShipmentText
    {
        get { return Helper.BoolToYesOrNo(SellerShipment); }
    }

    // Relations
    public List<StoreDraftItemsView> Items { get; set; }

    // Methods
    public static StoreDraftView Find(int id)
    {
        string querName = "storedraft";
        DataRow row = BaseMap.QueryById(id, querName);

        if (row != null)
        {
            StoreDraftView storeDraftView = new StoreDraftView(row);
            storeDraftView.Items = StoreDraftItemsView.FindByStoreDraftId(id);

            return storeDraftView;
        }
        else
        {
            return null;
        }
    }
}