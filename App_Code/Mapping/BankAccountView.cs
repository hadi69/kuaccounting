﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BankAccountView
/// </summary>
public class BankAccountView:BaseMap
{
    public BankAccountView()
    {
    }

    public BankAccountView(DataRow row):base(row){}


    public string AccountNo { get; set; }
    public string AccountType { get; set; }
    public bool AdditionalInformation { get; set; }
    public int BankAccountType { get; set; }
    public string BankName { get; set; }
    public int BankPrintID { get; set; }
    public string BankPrintName { get; set; }
    public string BranchCode { get; set; }
    public string BranchName { get; set; }
    public string CardExpireDateJalali { get; set; }
    public string CardNo { get; set; }
    public string Comments { get; set; }
    public string CompanyBankCode { get; set; }
    public string CustomerNo { get; set; }
    public string Cvv2 { get; set; }
    public bool Enabled { get; set; }
    public int ID { get; set; }
    public string Password { get; set; }
    public int ProgramID { get; set; }
    public string ProgramName { get; set; }
    public string Sheba { get; set; }
    public long SyncVer { get; set; }
    public decimal TreeDue { get; set; }
    public decimal TreeOwe { get; set; }
    public string TreePath { get; set; }
    public string TreePathName { get; set; }
    public decimal TreeRemain { get; set; }
    public string UserName { get; set; }

    public static BankAccountView FindByID(int id)
    {
        Dictionary<string, string> parameters = new Dictionary<string, string> { { "ID", id.ToString() } };
        DataRowCollection rows = Query("bankaccount", parameters);

        if (rows.Count > 0)
        {
            return new BankAccountView(rows[0]);
        }
        else
        {
            return new BankAccountView();
        }
    }
}