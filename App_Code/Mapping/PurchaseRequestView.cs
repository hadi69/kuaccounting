﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

/// <summary>
/// Mapping DataTable to POCO for ReturnView
/// </summary>
public class PurchaseRequestView : BaseMap
{
    public PurchaseRequestView()
    {
    }

    public PurchaseRequestView(DataRow row)
        : base(row)
    {
    }

    // Fields mapped from database
    public bool Accepted { get; set; }
    public string Comments { get; set; }
    public System.DateTime ConfirmDate { get; set; }
    public string ConfirmDateJalali { get; set; }
    public int ConfirmerID { get; set; }
    public string ConfirmerName { get; set; }
    public int DesiredEntryTaskID { get; set; }
    public int ID { get; set; }
    public int ImportanceDegree { get; set; }
    public string NOSerial { get; set; }
    public int NumPrints { get; set; }
    public int PersonnelID { get; set; }
    public string PersonnelName { get; set; }
    public string PreTaskName { get; set; }
    public int PurchaseID { get; set; }
    public string PurchaseTitle { get; set; }
    public System.DateTime RequestDate { get; set; }
    public string RequestDateJalali { get; set; }
    public int StoreID { get; set; }
    public string StoreName { get; set; }
    public int TaskImplID { get; set; }
    public string TaskName { get; set; }
    public double TotalAmount { get; set; }
    public decimal TotalPrice { get; set; }
    public int WorkFlow { get; set; }
    public string WorkflowColor { get; set; }
    public int WorkflowStatus { get; set; }




    // Other fields
    public string WorkFlowStatusText
    {
        get
        {
            if (WorkflowStatus == 0)
            {
                return LangProvider.Instance.ParseLang("Lang.WFStatusInProgress");
            }

            if (WorkflowStatus == 1)
            {
                return LangProvider.Instance.ParseLang("Lang.WFStatusFinal");
            }

            return "";
        }
    }
    
    /// <summary>
    /// جمع تعداد/مقدار
    /// </summary>
    public double SumAmount
    {
        get
        {
            return Items.Sum(PurchaseRequestItemsView =>
               PurchaseRequestItemsView.Amount);
        }
    }

    /// <summary>
    /// مبلغ نهایی
    /// </summary>
    public decimal SumTotalPrice
    {
        get { return Items.Sum(PurchaseRequestItemsView => PurchaseRequestItemsView.TotalPrice); }
    }
    /// <summary>
    /// مبلغ نهایی
    /// </summary>
    public decimal GrandTotal
    {
        get
        {
            decimal result = SumTotalPrice;


            return result;
        }
    }


    // Relations
    public List<PurchaseRequestItemsView> Items { get; set; }

    // Methods
    public static PurchaseRequestView Find(int id)
    {
        string querName = "purchaserequest";
        DataRow row = BaseMap.QueryById(id, querName);

        if (row != null)
        {
            PurchaseRequestView PurchaseRequestView = new PurchaseRequestView(row);
            PurchaseRequestView.Items = PurchaseRequestItemsView.FindByPurchaseRequestId(id);

            return PurchaseRequestView;
        }
        else
        {
            return null;
        }
    }
}