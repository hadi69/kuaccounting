﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

/// <summary>
/// Mapping DataTable to POCO for DistReportPrintView
/// </summary>
public class DistReportPrintView : BaseMap
{
    public DistReportPrintView()
    {
    }

    public DistReportPrintView(DataRow row)
        : base(row)
    {
    }

    public int ID { get; set; }
    public decimal BuyerRemain { get; set; }
    public decimal FinalPrice { get; set; }
    public string BuyerName { get; set; }
    public string FactorDateJalali { get; set; }
    public DateTime FactorDate { get; set; }
    public DateTime ReportDate { get; set; }
    public string ReportDateJalali { get; set; }

    // Methods
    public static DistReportPrintView Find(int id)
    {
        string querName = "distreportprintrow";
        DataRow row = BaseMap.QueryById(id, querName);
        if (row != null)
        {
            DistReportPrintView distReportPrintView = new DistReportPrintView(row);
            return distReportPrintView;
        }
        else
        {
            return null;
        }
    }
}