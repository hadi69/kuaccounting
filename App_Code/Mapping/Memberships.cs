﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Memberships
/// </summary>
public class Memberships:BaseMap
{
	public Memberships()
	{
	}

    public Memberships(DataRow row) : base(row) { }

    public int FundID { get; set; }
    public string FundName { get; set; }
    public int ID { get; set; }
    public decimal MonthlyPay { get; set; }
    public int PersonnelID { get; set; }
    public string PersonnelName { get; set; }
    public int Status { get; set; }
    public decimal TotalPaied { get; set; }

}