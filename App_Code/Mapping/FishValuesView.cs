﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for FishValuesView
/// </summary>
public class FishValuesView:BaseMap
{
	public FishValuesView()
	{
	}

    public FishValuesView(DataRow row):base(row){}

    // Fields mapped from database
    public string FieldName { get; set; }
    public string FieldTitle { get; set; }
    public int DecimalPlaces { get; set; }
    public int ID { get; set; }
    public int FishID { get; set; }
    public int FishDefFieldID { get; set; }
    public decimal Value { get; set; }
    public int FishMonth { get; set; }
    public int FishYear { get; set; }

    public static List<FishValuesView> FindByFishId(int FishId)
    {
        Dictionary<string, string> parameters = new Dictionary<string, string> { { "FishID", FishId.ToString("") } };
        DataRowCollection rows = BaseMap.Query("fishvalue", parameters);
        if (rows != null)
        {
            List<FishValuesView> items = new List<FishValuesView>();
            foreach (DataRow dataRow in rows)
            {
                items.Add(new FishValuesView(dataRow));
            }

            return items;
        }
        else
        {
            return null;
        }
    }
    public static FishValuesView FindFishValuesByFieldName(int FishId, string FieldName)
    {
        Dictionary<string, string> parameters = new Dictionary<string, string>
        {
            {"FishID", FishId.ToString("")},
            {"FieldName", "N'"+FieldName+"'"}
        };
        DataRowCollection rows = BaseMap.Query("fishvaluebyname", parameters);
        if (rows != null)
        {
            
            foreach (DataRow dataRow in rows)
            {
                dataRow["Value"] = Helper.GetDecimal(Helper.GetDecimalPlaceValue(dataRow["Value"].ToString(),
                    Helper.GetInt(dataRow["DecimalPlaces"], 0)), 0);
                return new FishValuesView(rows[0]);
            }
        }
        return new FishValuesView();
    }
    public static decimal FindSumValue(int year, int month, string fieldname)
    {
        DataTable sumValue =
            DbProxy.Instance.FillData("SELECT SUM(Value) AS SumValue, DecimalPlaces FROM FishValuesView WHERE FishYear=" + year + " AND FishMonth=" +
                                        month + " AND FieldName=N'" + fieldname + "' GROUP BY DecimalPlaces", "table");
        if (sumValue!=null && sumValue.Rows.Count>0)
        {
            return Helper.GetDecimal(Helper.GetDecimalPlaceValue(Helper.GetString(sumValue.Rows[0],"SumValue","0"),Helper.GetInt(sumValue.Rows[0],"DecimalPlaces",0)),0);
        }
        else
        {
            return 0;
        }
    }
    public static List<FishValuesView> FindFishValuesByFieldShowType(int FishId, FieldShowType FieldShowType)
    {
        List<FishValuesView> fishvalues = new List<FishValuesView>();
        Dictionary<string, string> parameters = new Dictionary<string, string>
        {
            {"FishID", FishId.ToString("")},
            {"FieldShowType", ((int)FieldShowType).ToString()}
        };
        DataRowCollection rows = BaseMap.Query("fishvaluebyfieldshowtype", parameters);
        if (rows != null)
        {
            foreach (DataRow dataRow in rows)
            {
                dataRow["Value"] = Helper.GetDecimalPlaceValue(dataRow["Value"].ToString(), Helper.GetInt(dataRow["DecimalPlaces"],0));
                fishvalues.Add(new FishValuesView(dataRow));
            }
        }
        return fishvalues;
    }
    public static decimal FindSumValuesByFieldShowType(int FishId, FieldShowType FieldShowType)
    {

        Dictionary<string, string> parameters = new Dictionary<string, string>
        {
            {"FishID", FishId.ToString("")},
            {"FieldShowType", ((int)FieldShowType).ToString()}
        };
        DataRowCollection rows = BaseMap.Query("sumvaluebyfieldshowtype", parameters);
        if (rows != null)
        {
            return Helper.GetDecimal(rows[0], "SumValue", 0);

        }
        return 0;
    }

    public static DataTable FindPersonnelSumValue(int year, int month, string fieldname)
    {
        DataTable sumvalueTable =
            DbProxy.Instance.FillData("SELECT SUM(Value) AS SumValue, Count(ID) AS PersonnelCoumt FROM FishValuesView WHERE FishYear=" + year + " AND FishMonth=" +
                                        month + " AND FieldName=N'" + fieldname + "' AND Status=1","sumvalue");
        return sumvalueTable;
    }

    public static List<FishValuesView> FindFishValuesViewsByShowType(int year, int month, FieldShowType fieldShowType)
    {
        List<FishValuesView> fishvalues = new List<FishValuesView>();
        string query =
            string.Format(@"SELECT * FROM FishValuesView WHERE FieldShowType={0} AND FishYear={1} AND FishMonth={2}", fieldShowType, year, month);
        DataRowCollection rows = DbProxy.Instance.FillData(query, "fishvalues").Rows;
        if (rows != null)
        {
            foreach (DataRow dataRow in rows)
            {
                dataRow["Value"] = Helper.GetDecimalPlaceValue(dataRow["Value"].ToString(), Helper.GetInt(dataRow["DecimalPlaces"], 0));
                fishvalues.Add(new FishValuesView(dataRow));
            }
        }
        return fishvalues;
    }
}