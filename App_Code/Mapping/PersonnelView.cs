﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PersonnelView
/// </summary>
public class PersonnelView: BaseMap
{
    public PersonnelView()
    {}

    public PersonnelView(DataRow row)
        :base(row)
    {}

    public string AccountNo { get; set; }
    public int AccountTypeID { get; set; }
    public bool AdditionalInformation { get; set; }
    public int AttendanceCode { get; set; }
    public string BankCardNo { get; set; }
    public bool Bedridden { get; set; }
    public System.DateTime BirthDate { get; set; }
    public string BirthDateJalali { get; set; }
    public int CategoryID { get; set; }
    public string CategoryName { get; set; }
    public string CellNumber1 { get; set; }
    public string CellNumber2 { get; set; }
    public int ChartID { get; set; }
    public decimal CheckCredit { get; set; }
    public string Code { get; set; }
    public string Coding { get; set; }
    public string Comments { get; set; }
    public int CooperationTypeID { get; set; }
    public string CooperationTypeName { get; set; }
    public int DarsadJanbaziID { get; set; }
    public int DegreeFieldID { get; set; }
    public int DepartmentID { get; set; }
    public decimal DiscountPercent { get; set; }
    public int EducationID { get; set; }
    public int EducationLevelID { get; set; }
    public string EducationLevelName { get; set; }
    public string EducationName { get; set; }
    public string Email { get; set; }
    public string EmergencyAddress { get; set; }
    public int EmergencyNum { get; set; }
    public int EmployeeStatusID { get; set; }
    public string EmployeeStatusName { get; set; }
    public System.DateTime EmploymentDate { get; set; }
    public string EmploymentDateJalali { get; set; }
    public int EmploymentTypeID { get; set; }
    public string EmploymentTypeName { get; set; }
    public bool Enabled { get; set; }
    public System.DateTime EndDate { get; set; }
    public string EndDateJalali { get; set; }
    public string Father { get; set; }
    public decimal Floor { get; set; }
    public int GallantryLevelID { get; set; }
    public string GallantryLevelName { get; set; }
    public int GallantryStatusID { get; set; }
    public string GallantryStatusName { get; set; }
    public int GenderID { get; set; }
    public string GenderName { get; set; }
    public bool GetMessage { get; set; }
    public string HomeAddress { get; set; }
    public string HomeTelNum { get; set; }
    public int ID { get; set; }
    public string IDCardNo { get; set; }
    public string IDCardSerial { get; set; }
    public string InsuranceNo { get; set; }
    public int InsuranceOrganizationID { get; set; }
    public string InsuranceOrganizationName { get; set; }
    public int InsuranceTypeID { get; set; }
    public string InsuranceTypeName { get; set; }
    public int IsarTypeID { get; set; }
    public System.DateTime IssueDate { get; set; }
    public string IssueDateJalali { get; set; }
    public int IssuePlaceID { get; set; }
    public string IssuePlaceName { get; set; }
    public int JobCategoryID { get; set; }
    public string JobCategoryName { get; set; }
    public int JobID { get; set; }
    public string JobName { get; set; }
    public double KPercent { get; set; }
    public int MaritalID { get; set; }
    public string MaritalName { get; set; }
    public System.DateTime MarriageDate { get; set; }
    public string MarriageDateJalali { get; set; }
    public decimal MaxCredit { get; set; }
    public int MilitaryStatusID { get; set; }
    public string MilitaryStatusName { get; set; }
    public int ModateEsaratID { get; set; }
    public int ModateKhedmatID { get; set; }
    public string Name { get; set; }
    public string NatinoalCode { get; set; }
    public int NationalityID { get; set; }
    public string NationalityName { get; set; }
    public int Nature { get; set; }
    public int NumChildren { get; set; }
    public int NumDependants { get; set; }
    public bool Oncall { get; set; }
    public string PCoding { get; set; }
    public string Person { get; set; }
    public int PersonnelGroupID { get; set; }
    public string PersonnelGroupName { get; set; }
    public string PersonneliNum { get; set; }
    public string PIdentify { get; set; }
    public int PlaceofbirthID { get; set; }
    public string PlaceofbirthName { get; set; }
    public string PostCode { get; set; }
    public int PostID { get; set; }
    public string PostName { get; set; }
    public System.DateTime RetirementDate { get; set; }
    public string RetirementDateJalali { get; set; }
    public int Source { get; set; }
    public int SpecialtyID { get; set; }
    public string SpecialtyName { get; set; }
    public System.DateTime StartDate { get; set; }
    public string StartDateJalali { get; set; }
    public string Surname { get; set; }
    public long SyncVer { get; set; }
    public double TaxPercent { get; set; }
    public bool Teacher { get; set; }
    public int Type { get; set; }
    public int UnTaxPersonID { get; set; }
    public string UnTaxPersonName { get; set; }
    public string Url { get; set; }
    public int WardID { get; set; }
    public string WardName { get; set; }
    public int WorkGroup { get; set; }

    // Additional fields
    public decimal Remain { get; set; }
    public decimal NotPassedChecks { get; set; }

    public string FullName
    {
        get { return Name + " " + Surname; }
    }

    public string FullAddressAndIdentity
    {
        get
        {
            string result =  (String.IsNullOrEmpty(PIdentify) || String.IsNullOrEmpty(HomeAddress) ? "" : "(")
                +   (String.IsNullOrEmpty(PIdentify) ? "" : PIdentify )
                +   (String.IsNullOrEmpty(PIdentify) || String.IsNullOrEmpty(HomeAddress) ? "" : ")")
                +   (String.IsNullOrEmpty(PIdentify) || String.IsNullOrEmpty(HomeAddress) ? "" : " - ")
                +   (String.IsNullOrEmpty(HomeAddress) ? "" : HomeAddress);

            return String.IsNullOrEmpty(result) ? "<i>" + LangProvider.Instance.ParseLang("Lang.NotSpecified") + "</i>" : result;
        }
    }

    public static PersonnelView Find(int id)
    {
        PersonnelView personnelView = new PersonnelView(QueryById(id, "personnel"));

        DataRowCollection rowPersonnelRemain = Query("calculatepersonnelremain",
            new Dictionary<string, string> { { "BuyerID", id.ToString("") }, { "FactorDate", Helper.QoutedDateTime(SiteSettings.FinanceYearEndDate.HasValue ? SiteSettings.FinanceYearEndDate.Value : DateTime.Now).Replace("'", "") } });
        if (rowPersonnelRemain.Count == 1)
        {
            personnelView.ParseRow(rowPersonnelRemain[0]);
        }

        DataRowCollection rowsNotPassedChecks = Query("calculatepersonnelnotpassedchecks",
            new Dictionary<string, string> { { "BuyerID", id.ToString("") }, { "FactorDate", Helper.QoutedDateTime(SiteSettings.FinanceYearEndDate.HasValue ? SiteSettings.FinanceYearEndDate.Value : DateTime.Now).Replace("'", "") } });
        if (rowsNotPassedChecks.Count == 1)
        {
            personnelView.ParseRow(rowsNotPassedChecks[0]);
        }

        return personnelView;
    }

    public static PersonnelView FindBaseOnDate(int id, DateTime date)
    {
        PersonnelView personnelView = new PersonnelView(QueryById(id, "personnel"));

        DataRowCollection rowPersonnelRemain = Query("calculatepersonnelremain",
            new Dictionary<string, string> { { "BuyerID", id.ToString("") }, { "FactorDate", Helper.QoutedDateTime(date).Replace("'", "")} });
        if (rowPersonnelRemain.Count == 1)
        {
            personnelView.ParseRow(rowPersonnelRemain[0]);
        }

        DataRowCollection rowsNotPassedChecks = Query("calculatepersonnelnotpassedchecks",
            new Dictionary<string, string> { { "BuyerID", id.ToString("") }, { "FactorDate", Helper.QoutedDateTime(date).Replace("'", "") } });
        if (rowsNotPassedChecks.Count == 1)
        {
            personnelView.ParseRow(rowsNotPassedChecks[0]);
        }

        return personnelView;
    }
}