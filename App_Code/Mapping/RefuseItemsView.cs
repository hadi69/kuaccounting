﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for StoreDraftItemsView
/// </summary>
public class RefuseItemsView: BaseMap
{
    public RefuseItemsView()
    {
    }

    public RefuseItemsView(DataRow row)
        : base(row)
    { }

    public string Comments { get; set; }
    public decimal Discount { get; set; }
    public int ID { get; set; }
    public double Num { get; set; }
    public decimal Price { get; set; }
    public int RefuseID { get; set; }
    public decimal SingleVAT { get; set; }
    public int StoreServiceID { get; set; }
    public string StoreServiceName { get; set; }
    public decimal Tax { get; set; }
    public double UnitCurrencyConverter { get; set; }
    public int UnitID { get; set; }
    public string UnitName { get; set; }


    // Other fields
    public decimal ActualDiscount
    {
        get
        {
            if (Discount >= 0)
            {
                return SiteSettings.ToMonetary(Discount, rv.RefuseDate);
            }
            else
            {
                return Math.Round((TotalPrice * Math.Abs(Discount)) / 100);
            }
        }
    }
    public decimal ActualTax
    {
        get
        {
            if (Tax >= 0)
            {
                return SiteSettings.ToMonetary(Tax, rv.RefuseDate);
            }
            else
            {
                return ((TotalPrice - ActualDiscount) * Math.Abs(Tax)) / 100;
            }
        }
    }

    public decimal ActualSingleVAT
    {
        get
        {
            if (SingleVAT >= 0)
            {
                return SiteSettings.ToMonetary(SingleVAT, rv.RefuseDate);
            }
            else
            {
                return ((TotalPrice - ActualDiscount) * Math.Abs(SingleVAT)) / 100;
            }
        }
    }

    public Decimal TotalPrice
    {
        get
        {
            return SiteSettings.ToMonetary(Price, rv.RefuseDate) * Convert.ToDecimal(Num) * Helper.GetDecimal(UnitCurrencyConverter, 1);
        }
    }

    public Decimal ConvertedPrice
    {
        get
        {
            return SiteSettings.ToMonetary(Price, rv.RefuseDate);
        }
    }
    public RefuseView rv
    {
        get { return RefuseView.Find(RefuseID); }
    }

    public static List<RefuseItemsView> FindByRefuseId(int refuseId)
    {
        Dictionary<string, string> parameters = new Dictionary<string, string> { { "RefuseID", refuseId.ToString("") } };
        DataRowCollection rows = BaseMap.Query("refuseitem", parameters);
        if (rows != null)
        {
            List<RefuseItemsView> items = new List<RefuseItemsView>();
            foreach (DataRow dataRow in rows)
            {
                items.Add(new RefuseItemsView(dataRow));
            }

            return items;
        }
        else
        {
            return null;
        }
    }
}