﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PurchaseView
/// </summary>
public class PurchaseView: BaseMap
{
    public PurchaseView()
    {
    }

    public PurchaseView(DataRow row)
        : base(row)
    {
    }

    // Properties
    public int AccountantID { get; set; }
    public string AccountantName { get; set; }
    public bool BuyerShipment { get; set; }
    public decimal CashPrice { get; set; }
    public string CashTopicCode { get; set; }
    public string CashTopicTitle { get; set; }
    public string CashTreePath { get; set; }
    public string CashTreePathName { get; set; }
    public decimal CheckPrice { get; set; }
    public string CheckTopicCode { get; set; }
    public string CheckTopicTitle { get; set; }
    public string CheckTreePath { get; set; }
    public string CheckTreePathName { get; set; }
    public string Comments { get; set; }
    public System.DateTime Date { get; set; }
    public string DateJalali { get; set; }
    public int DesiredEntryTaskID { get; set; }
    public decimal DuedPrice { get; set; }
    public string DuedTopicCode { get; set; }
    public string DuedTopicTitle { get; set; }
    public string DuedTreePath { get; set; }
    public string DuedTreePathName { get; set; }
    public string DueTopicCode { get; set; }
    public string DueTopicTitle { get; set; }
    public string DueTreePath { get; set; }
    public string DueTreePathName { get; set; }
    public int FinanceYearID { get; set; }
    public string FinanceYearName { get; set; }
    public int ID { get; set; }
    public bool IsTrust { get; set; }
    public string NOSerial { get; set; }
    public string OweTopicCode { get; set; }
    public string OweTopicTitle { get; set; }
    public string OweTreePath { get; set; }
    public string OweTreePathName { get; set; }
    public decimal PayAmount { get; set; }
    public int PayCheckID { get; set; }
    public string PayCheckName { get; set; }
    public System.DateTime PayDate { get; set; }
    public string PayDateJalali { get; set; }
    public int PayMethod { get; set; }
    public int PayMethodID { get; set; }
    public string PayMethodName { get; set; }
    public System.DateTime PaySerialCode { get; set; }
    public string PaySerialCodeJalali { get; set; }
    public int ProgramID { get; set; }
    public decimal PurchaseDiscount { get; set; }
    public System.DateTime ReceiptDate { get; set; }
    public string ReceiptDateJalali { get; set; }
    public int SellerID { get; set; }
    public string SellerName { get; set; }
    public string SellerAddress { get; set; }
    public decimal ShipmentPrice { get; set; }
    public string ShipmentTopicCode { get; set; }
    public string ShipmentTopicTitle { get; set; }
    public string ShipmentTreePath { get; set; }
    public string ShipmentTreePathName { get; set; }
    public int StoreID { get; set; }
    public int StoreKeeperID { get; set; }
    public string StoreKeeperName { get; set; }
    public string StoreName { get; set; }
    public int SupplierID { get; set; }
    public string SupplierName { get; set; }
    public string TaskName { get; set; }
    public double TotalAmount { get; set; }
    public decimal TotalDiscount { get; set; }
    public decimal TotalPrice { get; set; }
    public decimal TotalTax { get; set; }
    public decimal VAT { get; set; }
    public int VoucherID { get; set; }
    public string VoucherNo { get; set; }
    public int WorkflowStatus { get; set; }
    public Decimal FactorTotalDiscount { get; set; }


    // Other properties
    public string WorkFlowStatusText
    {
        get
        {
            if (WorkflowStatus == 0)
            {
                return LangProvider.Instance.ParseLang("Lang.WFStatusInProgress");
            }

            if (WorkflowStatus == 1)
            {
                return LangProvider.Instance.ParseLang("Lang.WFStatusFinal");
            }

            return "";
        }
    }

    public string IsTrustText
    {
        get { return Helper.BoolToYesOrNo(IsTrust); }
    }

    public string BuyerShipmentText
    {
        get { return Helper.BoolToYesOrNo(BuyerShipment); }
    }

    public decimal GrandTotal
    {
        get
        {
            decimal result = SiteSettings.ToMonetary((TotalPrice + TotalTax + VAT), ReceiptDate) - SumActualDiscount;
            if (!BuyerShipment)
            {
                result += ConvertedShipmentPrice;
            }

            return result;
        }
    }

    public decimal ConvertedTotalPrice
    {
        get
        {
            return SiteSettings.ToMonetary(TotalPrice, ReceiptDate);
        }
    }

    public decimal ConvertedShipmentPrice
    {
        get
        {
            return SiteSettings.ToMonetary(ShipmentPrice, ReceiptDate);
        }
    }

    public decimal ActualFactorTotalDiscount
    {
        get
        {
            if (FactorTotalDiscount >= 0)
            {
                return SiteSettings.ToMonetary(FactorTotalDiscount, ReceiptDate);
            }
            else
            {
                return SiteSettings.ToMonetary((TotalPrice * Math.Abs(FactorTotalDiscount)) / 100, ReceiptDate);
            }
        }
    }

    public decimal SumActualDiscount
    {
        get { return Items.Sum(PurchaseItemsView => PurchaseItemsView.ActualDiscount) + ActualFactorTotalDiscount; }
    }

    // Relations
    public List<PurchaseItemsView> Items { get; set; }

    public static PurchaseView Find(int id)
    {
        const string queryName = "purchase";
        DataRow row = QueryById(id, queryName);

        if (row != null)
        {
            PurchaseView purchaseView = new PurchaseView(row);
            purchaseView.Items = PurchaseItemsView.FindByPurchaseId(id);

            return purchaseView;
        }
        else
        {
            return null;
        }
    }
}