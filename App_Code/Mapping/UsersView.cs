﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UsersView
/// </summary>
public class UsersView: BaseMap
{
    public UsersView()
    {
    }

    public UsersView(DataRow row)
        :base(row)
    { }

    public int ID { get; set; }
    public string Username { get; set; }
    public string Password { get; set; }
    public string Fullname { get; set; }
    public string Comments { get; set; }
    public int PersonnelID { get; set; }
    public bool Enabled { get; set; }
    public bool IsAdmin { get; set; }
    public string PersonnelName { get; set; }
    public string StoreDraftFooterComment { get; set; }
    public string StoreDraftHeaderComment { get; set; }
    public static UsersView Find(int id)
    {
        return new UsersView(QueryById(id, "user"));
    }
}