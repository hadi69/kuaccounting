﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

/// <summary>
/// Mapping DataTable to POCO for ReturnView
/// </summary>
public class RefuseView : BaseMap
{
    public RefuseView()
    {
    }

    public RefuseView(DataRow row)
        : base(row)
    {
    }

    public int AccountantID { get; set; }
    public string AccountantName { get; set; }
    public string Comments { get; set; }
    public string DesiredDocNo { get; set; }
    public int DesiredEntryTaskID { get; set; }
    public int DocItemID { get; set; }
    public string DueTreePath { get; set; }
    public string DueTreePathName { get; set; }
    public int FinanceYearID { get; set; }
    public string FinanceYearName { get; set; }
    public int ID { get; set; }
    public bool IsTrust { get; set; }
    public string NOSerial { get; set; }
    public int NumPrints { get; set; }
    public string OweTreePath { get; set; }
    public string OweTreePathName { get; set; }
    public string PreTaskName { get; set; }
    public int ProgramID { get; set; }
    public int PurchaseID { get; set; }
    public System.DateTime RefuseDate { get; set; }
    public string RefuseDateJalali { get; set; }
    public int SellerID { get; set; }
    public string SellerName { get; set; }
    public decimal ShipmentPrice { get; set; }
    public int StoreID { get; set; }
    public int StoreKeeperID { get; set; }
    public string StoreKeeperName { get; set; }
    public string StoreName { get; set; }
    public int TaskImplID { get; set; }
    public string TaskName { get; set; }
    public decimal TotalAwarez { get; set; }
    public decimal TotalDiscount { get; set; }
    public decimal TotalPrice { get; set; }
    public decimal TotalTax { get; set; }
    public int VoucherID { get; set; }
    public string VoucherNo { get; set; }
    public int WorkFlow { get; set; }
    public string WorkflowColor { get; set; }
    public int WorkflowStatus { get; set; }
    public Decimal FactorTotalDiscount { get; set; }




    // Other fields
    public string WorkFlowStatusText
    {
        get
        {
            if (WorkflowStatus == 0)
            {
                return LangProvider.Instance.ParseLang("Lang.WFStatusInProgress");
            }

            if (WorkflowStatus == 1)
            {
                return LangProvider.Instance.ParseLang("Lang.WFStatusFinal");
            }

            return "";
        }
    }

    public string IsTrusText
    {
        get { return Helper.BoolToYesOrNo(IsTrust); }
    }

    public decimal ActualFactorTotalDiscount
    {
        get
        {
            if (FactorTotalDiscount >= 0)
            {
                return SiteSettings.ToMonetary(FactorTotalDiscount, RefuseDate);
            }
            else
            {
                return SiteSettings.ToMonetary(Math.Round((TotalPrice * Math.Abs(FactorTotalDiscount)) / 100), RefuseDate);
            }
        }
    }


    /// <summary>
    /// جمع تعداد/مقدار
    /// </summary>
    public double SumAmount
    {
        get
        {
            return Items.Sum(refuseItemsView =>
               refuseItemsView.Num);
        }
    }

    /// <summary>
    /// مبلغ نهایی
    /// </summary>
    public decimal SumTotalPrice
    {
        get { return Items.Sum(refuseItemsView => refuseItemsView.TotalPrice); }
    }

    /// <summary>
    /// تخفیف کل
    /// </summary>
    public decimal SumDiscount
    {
        get { return Items.Sum(refuseItemsView => refuseItemsView.Discount) + ActualFactorTotalDiscount; }
    }

    /// <summary>
    /// تخفیف کل واقعی (با حتساب درصد یا عادی بودن تخفیف)
    /// </summary>
    public decimal SumActualDiscount
    {
        get { return Items.Sum(refuseItemsView => refuseItemsView.ActualDiscount); }
    }

    /// <summary>
    /// مالیات کل
    /// </summary>
    public decimal SumTax
    {
        get { return Items.Sum(refuseItemsView => refuseItemsView.Tax); }
    }

    /// <summary>
    /// مالیات کل واقعی (با حتساب درصد یا عادی بودن مالیات)
    /// </summary>
    public decimal SumActualTax
    {
        get { return Items.Sum(refuseItemsView => refuseItemsView.ActualTax); }
    }

    /// <summary>
    /// عوارض کل
    /// </summary>
    public decimal SumSingleVAT
    {
        get { return Items.Sum(refuseItemsView => refuseItemsView.SingleVAT); }
    }

    /// <summary>
    /// عوارض کل واقعی (با حتساب درصد یا عادی بودن عوارض)
    /// </summary>
    public decimal SumActualSingleVAT
    {
        get { return Items.Sum(refuseItemsView => refuseItemsView.ActualSingleVAT); }
    }

    /// <summary>
    /// مبلغ نهایی
    /// </summary>
    public decimal GrandTotal
    {
        get
        {
            decimal result = SumTotalPrice - SumActualDiscount + SumActualSingleVAT + SumActualTax + ConvertedShipmentPrice;


            return result;
        }
    }

    public Decimal ConvertedShipmentPrice
    {
        get
        {
            return SiteSettings.ToMonetary(ShipmentPrice, RefuseDate);
        }
    }


    // Relations
    public List<RefuseItemsView> Items { get; set; }

    // Methods
    public static RefuseView Find(int id)
    {
        string querName = "refuse";
        DataRow row = BaseMap.QueryById(id, querName);

        if (row != null)
        {
            RefuseView RefuseView = new RefuseView(row);
            RefuseView.Items = RefuseItemsView.FindByRefuseId(id);

            return RefuseView;
        }
        else
        {
            return null;
        }
    }
}