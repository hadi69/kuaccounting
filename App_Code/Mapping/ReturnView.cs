﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

/// <summary>
/// Mapping DataTable to POCO for ReturnView
/// </summary>
public class ReturnView : BaseMap
{
    public ReturnView()
    {
    }

    public ReturnView(DataRow row)
        : base(row)
    {
    }

    // Fields mapped from database
    public int AccountantID { get; set; }
    public string AccountantName { get; set; }
    public int BuyerID { get; set; }
    public string BuyerName { get; set; }
    public string Comments { get; set; }
    public string DesiredDocNo { get; set; }
    public int DesiredEntryTaskID { get; set; }
    public string DueTreePath { get; set; }
    public string DueTreePathName { get; set; }
    public int FinanceYearID { get; set; }
    public string FinanceYearName { get; set; }
    public int ID { get; set; }
    public bool IsTrust { get; set; }
    public string NOSerial { get; set; }
    public int NumPrints { get; set; }
    public string OweTreePath { get; set; }
    public string OweTreePathName { get; set; }
    public string PreTaskName { get; set; }
    public int ProgramID { get; set; }
    public decimal Pursant { get; set; }
    public System.DateTime ReturnDate { get; set; }
    public string ReturnDateJalali { get; set; }
    public bool ReturnFromPastYear { get; set; }
    public decimal ShipmentPrice { get; set; }
    public int StoreDraftID { get; set; }
    public string StoreDraftName { get; set; }
    public int StoreID { get; set; }
    public int StoreKeeperID { get; set; }
    public string StoreKeeperName { get; set; }
    public string StoreName { get; set; }
    public long SyncVer { get; set; }
    public int TabletID { get; set; }
    public int TabletSyncVer { get; set; }
    public int TabletUniqueID { get; set; }
    public string TaskName { get; set; }
    public decimal TotalAwarez { get; set; }
    public decimal TotalDiscount { get; set; }
    public decimal TotalPrice { get; set; }
    public decimal TotalTax { get; set; }
    public int VisitorID { get; set; }
    public string VisitorName { get; set; }
    public int VoucherID { get; set; }
    public string VoucherNo { get; set; }
    public int WardID { get; set; }
    public string WardName { get; set; }
    public int WardPersonlID { get; set; }
    public string WardPersonlName { get; set; }
    public int WorkFlow { get; set; }
    public string WorkflowColor { get; set; }
    public int WorkflowStatus { get; set; }
    public Decimal FactorTotalDiscount { get; set; }



    // Other fields
    public string WorkFlowStatusText
    {
        get
        {
            if (WorkflowStatus == 0)
            {
                return LangProvider.Instance.ParseLang("Lang.WFStatusInProgress");
            }

            if (WorkflowStatus == 1)
            {
                return LangProvider.Instance.ParseLang("Lang.WFStatusFinal");
            }

            return "";
        }
    }

    public decimal ActualFactorTotalDiscount
    {
        get
        {
            if (FactorTotalDiscount >= 0)
            {
                return SiteSettings.ToMonetary(FactorTotalDiscount, ReturnDate);
            }
            else
            {
                return SiteSettings.ToMonetary(Math.Round((TotalPrice * Math.Abs(FactorTotalDiscount)) / 100), ReturnDate);
            }
        }
    }

    public string IsTrusText
    {
        get { return Helper.BoolToYesOrNo(IsTrust); }
    }


    /// <summary>
    /// جمع تعداد/مقدار
    /// </summary>
    public double SumAmount
    {
        get
        {
            return Items.Sum(returnItemsView =>
               returnItemsView.Num);
        }
    }

    /// <summary>
    /// مبلغ نهایی
    /// </summary>
    public decimal SumTotalPrice
    {
        get { return Items.Sum(returnItemsView => returnItemsView.TotalPrice); }
    }

    /// <summary>
    /// تخفیف کل
    /// </summary>
    public decimal SumDiscount
    {
        get { return Items.Sum(returnItemsView => returnItemsView.Discount); }
    }

    /// <summary>
    /// تخفیف کل واقعی (با حتساب درصد یا عادی بودن تخفیف)
    /// </summary>
    public decimal SumActualDiscount
    {
        get { return Items.Sum(returnItemsView => returnItemsView.ActualDiscount) + FactorTotalDiscount; }
    }

    /// <summary>
    /// مالیات کل
    /// </summary>
    public decimal SumTax
    {
        get { return Items.Sum(returnItemsView => returnItemsView.Tax); }
    }

    /// <summary>
    /// مالیات کل واقعی (با حتساب درصد یا عادی بودن مالیات)
    /// </summary>
    public decimal SumActualTax
    {
        get { return Items.Sum(returnItemsView => returnItemsView.ActualTax); }
    }

    /// <summary>
    /// عوارض کل
    /// </summary>
    public decimal SumSingleVAT
    {
        get { return Items.Sum(returnItemsView => returnItemsView.SingleVAT); }
    }

    /// <summary>
    /// عوارض کل واقعی (با حتساب درصد یا عادی بودن عوارض)
    /// </summary>
    public decimal SumActualSingleVAT
    {
        get { return Items.Sum(returnItemsView => returnItemsView.ActualSingleVAT); }
    }

    /// <summary>
    /// مبلغ نهایی
    /// </summary>
    public decimal GrandTotal
    {
        get
        {
            decimal result = SumTotalPrice - SumActualDiscount + SumActualSingleVAT + SumActualTax + ConvertedShipmentPrice;


            return result;
        }
    }

    public Decimal ConvertedShipmentPrice
    {
        get
        {
            return SiteSettings.ToMonetary(ShipmentPrice, ReturnDate);
        }
    }


    // Relations
    public List<ReturnItemsView> Items { get; set; }

    // Methods
    public static ReturnView Find(int id)
    {
        string querName = "return";
        DataRow row = BaseMap.QueryById(id, querName);

        if (row != null)
        {
            ReturnView ReturnView = new ReturnView(row);
            ReturnView.Items = ReturnItemsView.FindByReturnId(id);

            return ReturnView;
        }
        else
        {
            return null;
        }
    }
}