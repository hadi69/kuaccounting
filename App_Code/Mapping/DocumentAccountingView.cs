﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DocumentAccountingView
/// </summary>
public class DocumentAccountingView: BaseMap
{
    public DocumentAccountingView()
    { }

    public DocumentAccountingView(DataRow row)
        :base(row)
    { }

    // Fields
    public DateTime Date { get; set; }
    public string DateJalali { get; set; }
    public string DescribeVoucher { get; set; }
    public string DoComments { get; set; }
    public int FinanceYearID { get; set; }
    public int FixerID { get; set; }
    public string FixerName { get; set; }
    public int ID { get; set; }
    public string ItemComments { get; set; }
    public int LockerID { get; set; }
    public string LockerName { get; set; }
    public int Mode { get; set; }
    public string No { get; set; }
    public int NumItems { get; set; }
    public int OtherID { get; set; }
    public int ProgramID { get; set; }
    public string Reciever { get; set; }
    public int Status { get; set; }
    public long SyncVer { get; set; }
    public decimal TotalDue { get; set; }
    public decimal TotalOwe { get; set; }
    public int UserID { get; set; }
    public string UserName { get; set; }
    public bool ValidVoucher { get; set; }
    public int WardID { get; set; }
    public string WardName { get; set; }

    // Relations
    public List<DocumentAccountingItemsView> Items { get; set; }

    // Methods
    public static DocumentAccountingView Find(int id)
    {
        const string queryName = "documentaccounting";
        DataRow row = QueryById(id, queryName);

        if (row != null)
        {
            DocumentAccountingView documentAccountingView = new DocumentAccountingView(row);
            documentAccountingView.Items = DocumentAccountingItemsView.FindByDocumentAccountingId(id);

            return documentAccountingView;
        }
        else
        {
            return null;
        }
    }
}
