﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PurchaseItemsView
/// </summary>
public class PurchaseItemsView: BaseMap
{
    public PurchaseItemsView()
    {
    }

    public PurchaseItemsView(DataRow row)
        : base(row)
    {
    }

    public double Amount { get; set; }
    public int BestUseTime { get; set; }
    public decimal Discount { get; set; }
    public System.DateTime ExpireDate { get; set; }
    public string ExpireDateJalali { get; set; }
    public int ID { get; set; }
    public decimal Price { get; set; }
    public System.DateTime ProductionDate { get; set; }
    public string ProductionDateJalali { get; set; }
    public int PurchaseID { get; set; }
    public decimal Realpercent { get; set; }
    public decimal SalePrice { get; set; }
    public decimal SingleVAT { get; set; }
    public int StoreServiceID { get; set; }
    public string StoreServiceName { get; set; }
    public decimal Tax { get; set; }
    public double UnitCurrencyConverter { get; set; }
    public int UnitID { get; set; }
    public string UnitName { get; set; }
    public int UseTime { get; set; }

    // Other fields
    public decimal TotalPrice
    {
        get { return Price*Convert.ToDecimal(Amount)* Helper.GetDecimal(UnitCurrencyConverter, 1); }
    }
    public decimal ConvertedTotalPrice
    {
        get
        {
            return ConvertedPrice * Convert.ToDecimal(Amount) * Helper.GetDecimal(UnitCurrencyConverter, 1);
        }
    }

    public decimal ConvertedPrice
    {
        get
        {
            PurchaseView pv = PurchaseView.Find(PurchaseID);
            return SiteSettings.ToMonetary(Price, pv.ReceiptDate);
        }
    }

    public decimal ActualDiscount
    {
        get
        {
            PurchaseView pv = PurchaseView.Find(PurchaseID);
            if (Discount >= 0)
            {
                return SiteSettings.ToMonetary(Discount, pv.ReceiptDate);
            }
            else
            {
                return SiteSettings.ToMonetary((TotalPrice * Math.Abs(Discount)) / 100, pv.ReceiptDate);
            }
        }
    }


    public static List<PurchaseItemsView> FindByPurchaseId(int purchaseId)
    {
        Dictionary<string, string> parameters = new Dictionary<string, string>{{"PurchaseID", purchaseId.ToString("")}};
        DataRowCollection rows = Query("purchaseitem", parameters);
        if (rows != null)
        {
            return (from DataRow dataRow in rows select new PurchaseItemsView(dataRow)).ToList();
        }
        else
        {
            return null;
        }
    }
}