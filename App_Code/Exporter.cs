﻿using System;
using System.Collections.Generic;
using System.Web;
using Word = Microsoft.Office.Interop.Word;
using System.Data;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Web.UI.WebControls;
using System.Text;

/// <summary>
/// Summary description for Exporter
/// </summary>
public class Exporter
{
    private static string StartFolder
    {
        get
        {
            return System.Web.HttpContext.Current.Server.MapPath("~/templates");
        }
    }
    public static void CreateWord(string templateName, string filePath, int start, DataTable visits
        , int year, int month, int page, int total)
    {
        object oMissing = System.Reflection.Missing.Value;
        object oEndOfDoc = "\\endofdoc"; /* \endofdoc is a predefined bookmark */

        //Start Word and create a new document.
        Word._Application oWord;
        Word._Document oDoc;

        oWord = new Word.Application();
        oWord.Visible = true;

        string path = System.IO.Path.Combine(StartFolder, templateName);
        if (System.IO.File.Exists(path))
        {
            object oTemplate = path;
            oDoc = oWord.Documents.Add(ref oTemplate, ref oMissing, ref oMissing, ref oMissing);
        }
        else
            oDoc = oWord.Documents.Add(ref oMissing, ref oMissing, ref oMissing, ref oMissing);

        for (int i = 0; i < 48; i++)
        {
            int index = start + i;
            DataRow vs = index < visits.Rows.Count ? visits.Rows[index] : null;
            string iStr = (i + 1).ToString().PadLeft(2, '0');
            if (vs != null)
            {
                int fileID = Helper.GetInt(vs, "FileID", 0);
                Replace(oDoc, "R" + iStr, (index + 1).ToString().PadLeft(2, '0'));
                Replace(oDoc, "N" + iStr, string.Format("{0} {1}", vs["PatientName"], vs["PatientSurname"]));
                Replace(oDoc, "D" + iStr, Helper.FormatDate(Helper.GetDateTime(vs, "AdmissionDate", DateTime.Now), null));
                //Booklet book = Helper.GetBooklet(vs.BookletID);
                object bookletSerialNumber = DbProxy.Instance.FillScalar(@"SELECT PatientInsurance.BookletSerialNumber FROM FilePatientInsurance 
INNER JOIN PatientInsurance ON FilePatientInsurance.PatientInsuranceID=PatientInsurance.ID
INNER JOIN Insurance ON PatientInsurance.InsuranceID=Insurance.ID  
WHERE FilePatientInsurance.FileID=" + fileID);
                Replace(oDoc, "B" + iStr, Helper.GetString(bookletSerialNumber, ""));
            }
            else
            {
                Replace(oDoc, "R" + iStr, "");
                Replace(oDoc, "N" + iStr, "");
                Replace(oDoc, "D" + iStr, "");
                Replace(oDoc, "B" + iStr, "");
            }
        }
        Replace(oDoc, "MN", month.ToString());
        Replace(oDoc, "YR", year.ToString());
        Replace(oDoc, "PG", page.ToString());
        Replace(oDoc, "TPG", total.ToString());

        if (filePath.ToLower().Contains("nos1"))
            Replace(oDoc, "NS1", "");
        else if (filePath.ToLower().Contains("nos2"))
            Replace(oDoc, "NS2", "");
        else if (filePath.ToLower().Contains("nos3"))
            Replace(oDoc, "NS3", "");
        else if (filePath.ToLower().Contains("nos4"))
            Replace(oDoc, "NS4", "");
        Replace(oDoc, "NS1", "");
        Replace(oDoc, "NS2", "");
        Replace(oDoc, "NS3", "");
        Replace(oDoc, "NS4", "");

        object filename = filePath;

        oDoc.SaveAs(ref filename, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
              ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
              ref oMissing, ref oMissing, ref oMissing, ref oMissing);
        oDoc.Close(ref oMissing, ref oMissing, ref oMissing);
        oWord.Quit(ref oMissing, ref oMissing, ref oMissing);
    }
    //public static void CreateWord03(string templateName, string filePath, int start, string fileName, DataTable visits
    //    , int year, int month, int page, int total)
    //{
    //    object oMissing = System.Reflection.Missing.Value;
    //    object oEndOfDoc = "\\endofdoc"; /* \endofdoc is a predefined bookmark */

    //    //Start Word and create a new document.
    //    Word._Application oWord;
    //    Word._Document oDoc;

    //    oWord = new Word.Application();
    //    oWord.Visible = true;

    //    string path = System.IO.Path.Combine(StartFolder, templateName);
    //    if (System.IO.File.Exists(path))
    //    {
    //        object oTemplate = path;
    //        oDoc = oWord.Documents.Add(ref oTemplate, ref oMissing, ref oMissing, ref oMissing);
    //    }
    //    else
    //        oDoc = oWord.Documents.Add(ref oMissing, ref oMissing, ref oMissing, ref oMissing);

    //    for (int i = 0; i < 18; i++)
    //    {
    //        int index = start + i;
    //        Visit vs = index < visits.Count ? visits[index] : null;
    //        string iStr = (i + 1).ToString().PadLeft(2, '0');
    //        if (vs != null)
    //        {
    //            Replace(oDoc, "R" + iStr, (index + 1).ToString().PadLeft(2, '0'));
    //            Replace(oDoc, "N" + iStr, string.Format("{0} {1}", vs.Person.Name, vs.Person.Surname));
    //            Replace(oDoc, "D" + iStr, vs.VisitDate3);
    //            Booklet book = Helper.GetBooklet(vs.BookletID);
    //            Replace(oDoc, "B" + iStr, book.Code);
    //            decimal price = 0;
    //            string services = "";
    //            for (int j = 0; j < vs.VisitServices.Count; j++)
    //            {
    //                price += vs.VisitServices[j].PriceOrgTotal;
    //                services = string.Format("{0} {1}", services, vs.VisitServices[j].Service.Title, vs.VisitServices[j].Service.IntlCode);
    //            }
    //            Replace(oDoc, "V" + iStr, services);
    //            Replace(oDoc, "P" + iStr, price.ToString());
    //        }
    //        else
    //        {
    //            Replace(oDoc, "R" + iStr, "");
    //            Replace(oDoc, "N" + iStr, "");
    //            Replace(oDoc, "D" + iStr, "");
    //            Replace(oDoc, "B" + iStr, "");
    //            Replace(oDoc, "V" + iStr, "");
    //            Replace(oDoc, "P" + iStr, "");
    //        }
    //    }
    //    Replace(oDoc, "MN", month.ToString());
    //    Replace(oDoc, "YR", year.ToString());
    //    Replace(oDoc, "PG", page.ToString());
    //    Replace(oDoc, "TPG", total.ToString());

    //    if (fileName.ToLower().Contains("nos1"))
    //        Replace(oDoc, "NS1", "");
    //    else if (fileName.ToLower().Contains("nos2"))
    //        Replace(oDoc, "NS2", "");
    //    else if (fileName.ToLower().Contains("nos3"))
    //        Replace(oDoc, "NS3", "");
    //    else if (fileName.ToLower().Contains("nos4"))
    //        Replace(oDoc, "NS4", "");
    //    Replace(oDoc, "NS1", "");
    //    Replace(oDoc, "NS2", "");
    //    Replace(oDoc, "NS3", "");
    //    Replace(oDoc, "NS4", "");

    //    object filename = filePath;

    //    oDoc.SaveAs(ref filename, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
    //          ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
    //          ref oMissing, ref oMissing, ref oMissing, ref oMissing);

    //}
    public static void CreateWord(string templateName, string filePath,
        int nos1, int nos2, int nos3, int nos4, int nos5
        , int nos1x, int nos2x, int nos3x, int nos4x, int nos5x
        , string xp1, string xp2, string xp3, string xp4, string xp5, decimal price, int year, int month
        , int numFunds, string code)
    {
        object oMissing = System.Reflection.Missing.Value;
        object oEndOfDoc = "\\endofdoc"; /* \endofdoc is a predefined bookmark */

        //Start Word and create a new document.
        Word._Application oWord;
        Word._Document oDoc;

        oWord = new Word.Application();
        oWord.Visible = true;

        string path = System.IO.Path.Combine(StartFolder, templateName);
        if (System.IO.File.Exists(path))
        {
            object oTemplate = path;
            oDoc = oWord.Documents.Add(ref oTemplate, ref oMissing, ref oMissing, ref oMissing);
        }
        else
            oDoc = oWord.Documents.Add(ref oMissing, ref oMissing, ref oMissing, ref oMissing);

        Replace(oDoc, "V1", nos1.ToString());
        Replace(oDoc, "V2", nos2.ToString());
        Replace(oDoc, "V3", nos3.ToString());
        Replace(oDoc, "V4", nos4.ToString());
        Replace(oDoc, "V6", nos5.ToString());
        Replace(oDoc, "V5", (nos1 + nos2 + nos3 + nos4 + nos5).ToString());

        Replace(oDoc, "X1", nos1x.ToString());
        Replace(oDoc, "X2", nos2x.ToString());
        Replace(oDoc, "X3", nos3x.ToString());
        Replace(oDoc, "X4", nos4x.ToString());
        Replace(oDoc, "X6", nos5x.ToString());
        Replace(oDoc, "X5", (nos1x + nos2x + nos3x + nos4x + nos5x).ToString());

        Replace(oDoc, "XP1", xp1);
        Replace(oDoc, "XP2", xp2);
        Replace(oDoc, "XP3", xp3);
        Replace(oDoc, "XP4", xp4);
        Replace(oDoc, "XP6", xp5);
        Replace(oDoc, "XP5", price.ToString());
        Replace(oDoc, "mPrice", price.ToString());

        Replace(oDoc, "mYear", year.ToString());
        Replace(oDoc, "mMonth", month.ToString());

        Replace(oDoc, "mNumVisits", (nos1 + nos2 + nos3 + nos4 + nos5 + nos1x + nos2x + nos3x + nos4x + nos5x).ToString());

        Replace(oDoc, "CODE", code);
        Replace(oDoc, "NF", numFunds.ToString());

        object filename = filePath;

        oDoc.SaveAs(ref filename, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
              ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
              ref oMissing, ref oMissing, ref oMissing, ref oMissing);

    }
    static void Replace(Word._Document oDoc, string src, string dest)
    {
        try
        {
            object oBookMark = src;
            oDoc.Bookmarks.get_Item(ref oBookMark).Range.Text = dest;
        }
        catch (Exception ex)
        {
            // Logger.Log(ex);
        }
    }
    public static void ExportExcel(HttpResponse response, IDataReader reader)
    {
        ExportExcel(response, reader, null);
    }
    public static void ExportExcel(HttpResponse response, IDataReader reader, HttpRequest request)
    {
        ExportExcel(response, reader, request, null);
    }
    public static void ExportExcel(HttpResponse response, IDataReader reader, HttpRequest request, string[] topRows)
    {
        using (ExcelPackage p = new ExcelPackage())
        {
            //Here setting some document properties
            p.Workbook.Properties.Author = "Jahan Peywand";
            p.Workbook.Properties.Title = "HIS Report";

            //Create a sheet
            p.Workbook.Worksheets.Add("List");
            ExcelWorksheet ws = p.Workbook.Worksheets[1];
            ws.Name = "List"; //Setting Sheet's name
            ws.Cells.Style.Font.Size = 11; //Default font size for whole sheet
            ws.Cells.Style.Font.Name = "Calibri"; //Default Font name for whole sheet


            //Merging cells and create a center heading for out table
            int rowIndex = 1;
            if (topRows == null)
            {
                if (request != null)
                    ws.Cells[rowIndex, 1].Value = Helper.GetString(request["t"], "Grid Export");
                else
                    ws.Cells[rowIndex, 1].Value = "Grid Export";

                ws.Cells[1, 1, 1, reader.FieldCount].Merge = true;
                ws.Cells[1, 1, 1, reader.FieldCount].Style.Font.Bold = true;
                ws.Cells[1, 1, 1, reader.FieldCount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            }
            else
            {
                for (int i = 0; i < topRows.Length; i++)
                {
                    string[] cols = topRows[i].Split('\t');
                    for (int j = 0; j < cols.Length; j++)
                        ws.Cells[rowIndex + i, j + 1].Value = cols[j];
                }
                rowIndex += topRows.Length - 1;
            }

            int colIndex = 1;
            rowIndex++;
            string hide = "";
            if (request != null)
                hide = request["hide"];
            //Creating Headings
            List<int> hiddenFields = new List<int>();
            List<int> dateFields = new List<int>();
            List<int> boolFields = new List<int>();
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (!string.IsNullOrEmpty(hide))
                    if ((";" + hide + ";").ToLower().Contains(reader.GetName(i).ToLower()))
                    {
                        hiddenFields.Add(i);
                        continue;
                    }
                Type tp = reader.GetFieldType(i);
                if (tp == typeof(DateTime))
                    //if (tp.Name == "DateTime")
                    dateFields.Add(i);
                else if (tp == typeof(Boolean))
                    boolFields.Add(i);
                var cell = ws.Cells[rowIndex, colIndex];

                //Setting the background color of header cells to Gray
                var fill = cell.Style.Fill;
                fill.PatternType = ExcelFillStyle.Solid;
                fill.BackgroundColor.SetColor(Color.Gray);

                //Setting Top/left,right/bottom borders.
                var border = cell.Style.Border;
                border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                //Setting Value in cell
                cell.Value = /*"Heading " +*/ LangProvider.Instance[reader.GetName(i)];

                colIndex++;
            }
            while (reader.Read())
            {
                colIndex = 1;
                rowIndex++;
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    if (hiddenFields.Contains(i))
                        continue;
                    var cell = ws.Cells[rowIndex, colIndex];
                    //Setting Value in cell
                    // handle enumeration: 0^Person^1^Service
                    bool done = false;
                    try
                    {
                        if (request != null)
                        {
                            string enu = request[reader.GetName(i)];
                            if (!string.IsNullOrEmpty(enu))
                            {
                                object val = reader.GetValue(i);
                                if (val != null)
                                {
                                    enu = "^" + enu + "^";
                                    int index = enu.IndexOf("^" + val.ToString() + "^");
                                    if (index >= 0)
                                    {
                                        index += val.ToString().Length + 2;
                                        string enuVal = enu.Substring(index, enu.IndexOf('^', index + 1) - index);
                                        cell.Value = LangProvider.Instance[enuVal];
                                        done = true;
                                    }
                                }
                            }
                        }
                    }
                    catch { }
                    // check for DateTime
                    if (dateFields.Contains(i))
                        try
                        {
                            cell.Value = Helper.FormatDate(Helper.GetDateTime(reader.GetDateTime(i), Helper.NullDate), null);
                            done = true;
                        }
                        catch { }
                    // check for bool
                    else if (boolFields.Contains(i))
                        try
                        {
                            cell.Value = reader.GetBoolean(i) ? "X" : "";
                            done = true;
                        }
                        catch { }
                    if (!done)
                        cell.Value = reader.GetValue(i);

                    //Setting borders of cell
                    var border = cell.Style.Border;
                    border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;
                    colIndex++;
                }
            }

            //colIndex = 0;
            //foreach (DataColumn dc in dt.Columns) //Creating Headings
            //{
            //    colIndex++;
            //    var cell = ws.Cells[rowIndex, colIndex];

            //    //Setting Sum Formula
            //    cell.Formula = "Sum(" +
            //                    ws.Cells[3, colIndex].Address +
            //                    ":" +
            //                    ws.Cells[rowIndex - 1, colIndex].Address +
            //                    ")";

            //    //Setting Background fill color to Gray
            //    cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
            //    cell.Style.Fill.BackgroundColor.SetColor(Color.Gray);
            //}

            response.Clear();
            response.ClearHeaders();
            response.ContentType = "application/vnd.xls";
            response.AddHeader("Content-Disposition", "attachment; filename=\"list.xlsx\"");
            Byte[] bin = p.GetAsByteArray();
            response.AddHeader("Content-Length", bin.Length.ToString());
            response.OutputStream.Write(bin, 0, bin.Length);
            response.Flush();
        }
    }
    public static void ExportExcel(HttpResponse response, string title, GridView grid)
    {
        ExportExcel(response, new List<string>(new string[] { title }), new List<GridView>(new GridView[] { grid }));
    }
    public static void ExportExcel(HttpResponse response, List<string> titles, List<GridView> grids)
    {
        using (ExcelPackage p = new ExcelPackage())
        {
            //Here setting some document properties
            p.Workbook.Properties.Author = "Jahan Peywand";
            p.Workbook.Properties.Title = "HIS Report";

            for (int g = 0; g < grids.Count; g++)
            {
                if (grids[g] == null || grids[g].Rows.Count == 0)
                    continue;

                //Create a sheet
                //p.Workbook.Worksheets.Add(string.IsNullOrEmpty(titles[g]) ? ("List " + g) : titles[g]);
                p.Workbook.Worksheets.Add("List " + g);
                ExcelWorksheet ws = p.Workbook.Worksheets[p.Workbook.Worksheets.Count]; // it starts from 1
                ws.Name = "List"; //Setting Sheet's name
                ws.Cells.Style.Font.Size = 11; //Default font size for whole sheet
                ws.Cells.Style.Font.Name = "Calibri"; //Default Font name for whole sheet
                if (string.IsNullOrEmpty(titles[g]))
                    ws.Cells[1, 1].Value = grids[g].ID + " Export";
                else
                    ws.Cells[1, 1].Value = titles[g];

                // count columns
                int colCount = 0;
                for (int i = 0; i < grids[g].Columns.Count; i++)
                    if (grids[g].Columns[i].Visible)
                        colCount++;

                //Merging cells and create a center heading for out table
                ws.Cells[1, 1, 1, colCount].Merge = true;
                ws.Cells[1, 1, 1, colCount].Style.Font.Bold = true;
                ws.Cells[1, 1, 1, colCount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                // header
                int colIndex = 1;
                int rowIndex = 2;
                List<int> byPass = new List<int>();
                for (int i = 0; i < grids[g].Columns.Count; i++)
                {
                    // check visiblity
                    if (!grids[g].Columns[i].Visible)
                    {
                        byPass.Add(i);
                        continue;
                    }
                    //check commands
                    if (grids[g].Columns[g] is ButtonField)
                    {
                        byPass.Add(i);
                        continue;
                    }

                    var cell = ws.Cells[rowIndex, colIndex];

                    //Setting the background color of header cells to Gray
                    var fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(Color.Gray);

                    //Setting Top/left,right/bottom borders.
                    var border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                    cell.Value = grids[g].Columns[i].HeaderText;

                    colIndex++;
                }

                // content
                for (int r = 0; r < grids[g].Rows.Count; r++)
                {
                    colIndex = 1;
                    rowIndex++;
                    for (int i = 0; i < grids[g].Columns.Count; i++)
                    {
                        if (byPass.Contains(i))
                            continue;
                        var cell = ws.Cells[rowIndex, colIndex];
                        string val = grids[g].Rows[r].Cells[i].Text;
                        if (grids[g].Rows[r].Cells[i].Controls.Count > 0)
                        {
                            for (int c = 0; c < grids[g].Rows[r].Cells[i].Controls.Count; c++)
                                if (grids[g].Rows[r].Cells[i].Controls[c] is Label)
                                    val += (grids[g].Rows[r].Cells[i].Controls[c] as Label).Text;
                                else if (grids[g].Rows[r].Cells[i].Controls[c] is System.Web.UI.DataBoundLiteralControl)
                                    val += (grids[g].Rows[r].Cells[i].Controls[c] as System.Web.UI.DataBoundLiteralControl).Text.Trim();
                                else if (grids[g].Rows[r].Cells[i].Controls[c] is HyperLink)
                                    val += (grids[g].Rows[r].Cells[i].Controls[c] as HyperLink).Text.Trim();
                        }
                        if (val == "&nbsp;")
                            val = "";
                        // handle link
                        if (val.StartsWith("<a "))
                            try
                            {
                                int start = val.IndexOf('>');
                                int end = val.IndexOf('<', start);
                                val = val.Substring(start + 1, end - start - 1);
                            }
                            catch { }
                        // check for numeric value
                        bool done = false;
                        if (val == "0")
                        {
                            done = true;
                            cell.Value = 0;
                        }
                        else if (!string.IsNullOrEmpty(val) && char.IsNumber(val[0]) && val[0] != '0')
                        {
                            double d;
                            if (double.TryParse(val.Replace(",", ""), out d))
                            {
                                done = true;
                                cell.Value = d;
                            }
                        }
                        if (!done)
                            cell.Value = val;

                        //Setting borders of cell
                        var border = cell.Style.Border;
                        border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;
                        colIndex++;
                    }
                }
                // footer
                if (grids[g].FooterRow == null)
                    continue;
                colIndex = 1;
                rowIndex++;
                for (int i = 0; i < grids[g].Columns.Count; i++)
                {
                    if (byPass.Contains(i))
                        continue;
                    if (grids[g].FooterRow.Cells[i] != null)
                    {
                        var cell = ws.Cells[rowIndex, colIndex];
                        string val = grids[g].FooterRow.Cells[i].Text;
                        if (grids[g].FooterRow.Cells[i].Controls.Count > 0)
                        {
                            for (int c = 0; c < grids[g].FooterRow.Cells[i].Controls.Count; c++)
                                if (grids[g].FooterRow.Cells[i].Controls[c] is Label)
                                    val += (grids[g].FooterRow.Cells[i].Controls[c] as Label).Text;
                        }
                        if (val == "&nbsp;")
                            val = "";
                        // handle link
                        if (val.StartsWith("<a "))
                            try
                            {
                                int start = val.IndexOf('>');
                                int end = val.IndexOf('<', start);
                                val = val.Substring(start + 1, end - start - 1);
                            }
                            catch { }
                        cell.Value = val;

                        //Setting borders of cell
                        var border = cell.Style.Border;
                        border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;
                    }
                    colIndex++;
                }
            }


            if (p.Workbook.Worksheets.Count > 0)
            {
                response.Clear();
                response.ClearHeaders();
                response.ContentType = "application/vnd.xls";
                response.AddHeader("Content-Disposition", "attachment; filename=\"accountingExport.xlsx\"");
                Byte[] bin = p.GetAsByteArray();
                response.AddHeader("Content-Length", bin.Length.ToString());
                response.OutputStream.Write(bin, 0, bin.Length);
                response.Flush();
            }
        }
    }
    public static void ExportExcelSimple(HttpResponse response, string title, string content)
    {
        response.Clear();
        response.ClearHeaders();
        response.ContentType = "application/vnd.xls";
        response.AddHeader("Content-Disposition", "attachment; filename=\"accountingExport.xls\"");
        string write = title + "<br />" + content;
        //byte[] bytes = System.Text.UTF8Encoding.Default.GetBytes(write);
        //response.AddHeader("Content-Length", bytes.Length.ToString());
        response.Write(title + "<br />");
        response.Write(content);
        //response.OutputStream.Write(bytes, 0, bytes.Length);
        response.Flush();
        response.End();
    }
}