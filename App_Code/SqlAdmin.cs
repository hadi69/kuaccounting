﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Web;
using Microsoft.SqlServer.Management.Smo;
using System.IO;

public class SqlAdmin
{
    public Exception LastException;
    public enum Operation_Result
    {
        Ok,
        Failed,
        Canceled,
    }

    Server mServer = null;
    Backup mBackup = null;
    Restore mRestore = null;

    public Operation_Result Backup(string connectionStr, string dbName, string fileName)
    {
        mServer = new Server();
        mBackup = new Backup();
        try
        {
            //    mBackup.PercentComplete += new PercentCompleteEventHandler(mBackup_PercentComplete);
            //    mBackup.Complete += new Microsoft.SqlServer.Management.Common.ServerMessageEventHandler(mBackup_Complete);
            mServer.ConnectionContext.ConnectionString = connectionStr;
            mServer.ConnectionContext.Connect();
            mBackup.Action = BackupActionType.Database; 
            mBackup.Database = dbName; 
            mBackup.Devices.AddDevice(fileName, DeviceType.File); //mBackup.Files = "[" + fileName + "]";
            mBackup.Initialize = true; 
            //    mBackup.PercentCompleteNotification = 1;
            //    ProgressForm.SetCaption(mProgressForm, "Performing backup action on server...");
            mBackup.SqlBackup(mServer);
        }
        catch (Exception ex)
        {
            LastException = ex;
            try
            {
                mServer.ConnectionContext.Disconnect(); // mServer.DisConnect();
            }
            catch (Exception ex1)
            {
                LastException = ex1;
            }
            mServer = null;
            return Operation_Result.Failed;
        }
        finally
        {
            if (mServer != null)
                mServer.ConnectionContext.Disconnect();
            mBackup = null;
            mServer = null;
        }

        return Operation_Result.Ok;
    }

    void mBackup_Complete(object sender, Microsoft.SqlServer.Management.Common.ServerMessageEventArgs e)
    {
        try
        {
            //if (mProgressForm != null)
            //{
            //    ProgressForm.SetValue(mProgressForm, mProgressForm.Progress.Maximum);
            //    ProgressForm.CloseForm(mProgressForm);
            //}
        }
        catch (Exception ex)
        {
        }
    }

    void mBackup_PercentComplete(object sender, PercentCompleteEventArgs e)
    {
        try
        {
            //if (mProgressForm != null)
            //{
            //    ProgressForm.SetValue(mProgressForm, e.Percent);
            //    if (mProgressForm.Canceled)
            //    {
            //        mBackup.Abort();
            //        ProgressForm.CloseForm(mProgressForm);
            //    }
            //}
        }
        catch (Exception ex)
        {
            LastException = ex;
        }
    }

    public Operation_Result Restore(string connectionStr, string srcDBName, string dbName, string fileName, string destFolder)
    {
        mServer = new Server();
        mRestore = new Restore();
        try
        {
            //    mBackup.PercentComplete += new PercentCompleteEventHandler(mBackup_PercentComplete);
            //    mBackup.Complete += new Microsoft.SqlServer.Management.Common.ServerMessageEventHandler(mBackup_Complete);
            mServer.ConnectionContext.ConnectionString = connectionStr;
            mServer.ConnectionContext.Connect();
            mRestore.Action = RestoreActionType.Database;
            mRestore.Database = dbName;
            mRestore.RelocateFiles.Add(new RelocateFile(srcDBName, System.IO.Path.Combine(destFolder, dbName + ".mdf")));
            mRestore.RelocateFiles.Add(new RelocateFile(srcDBName + "_Log", System.IO.Path.Combine(destFolder, dbName + ".ldf")));
            mRestore.Devices.AddDevice(fileName, DeviceType.File);
            mRestore.ReplaceDatabase = true;
            //mRestore.Initialize = true;
            //    mBackup.PercentCompleteNotification = 1;
            //    ProgressForm.SetCaption(mProgressForm, "Performing backup action on server...");
            mRestore.SqlRestore(mServer);
        }
        catch (Exception ex)
        {
            LastException = ex;
            try
            {
                mServer.ConnectionContext.Disconnect(); // mServer.DisConnect();
            }
            catch (Exception ex1)
            {
                LastException = ex1;
            }
            mServer = null;
            return Operation_Result.Failed;
        }
        finally
        {
            if (mServer != null)
                mServer.ConnectionContext.Disconnect();
            mBackup = null;
            mServer = null;
        }

        return Operation_Result.Ok;
    }
    public Operation_Result Restore(string connectionStr, string dbName, string fileName)
    {
        mServer = new Server();
        mRestore = new Restore();
        try
        {
            mServer.ConnectionContext.ConnectionString = connectionStr;
            mServer.ConnectionContext.Connect();
            mRestore.Action = RestoreActionType.Database;
            mRestore.Database = dbName;
            mRestore.Devices.AddDevice(fileName, DeviceType.File);
            mRestore.SqlRestore(mServer);
        }
        catch (Exception ex)
        {
            LastException = ex;
            try
            {
                mServer.ConnectionContext.Disconnect(); // mServer.DisConnect();
            }
            catch (Exception ex1)
            {
                LastException = ex1;
            }
            mServer = null;
            return Operation_Result.Failed;
        }
        finally
        {
            if (mServer != null)
                mServer.ConnectionContext.Disconnect();
            mBackup = null;
            mServer = null;
        }

        return Operation_Result.Ok;
    }

    public bool BackupHisDB()
    {
        string path = System.Web.HttpContext.Current.Server.MapPath("~/files/DB");
        if (!System.IO.Directory.Exists(path))
            System.IO.Directory.CreateDirectory(path);
        path = System.IO.Path.Combine(path, string.Format("DB_{0}.bak", DateTime.Now.ToString("yyyyMMdd_HHmm")));
        using (DbProxy proxy = new DbProxy())
        {
            proxy.RunQuery("TRUNCATE TABLE Logs", true);
            proxy.RunQuery("TRUNCATE TABLE Temp", true);
            proxy.RunQuery(string.Format("DBCC SHRINKDATABASE ({0}, TRUNCATEONLY)", proxy.CatalogName), true);
            if (Backup(proxy.ConnectionString, proxy.CatalogName, path) == SqlAdmin.Operation_Result.Ok)
            {
                #region backup files
                string src = System.Web.HttpContext.Current.Server.MapPath("~/files");
                string dest = path.Replace(".bak", "");
                List<string> excludes = new List<string>();
                excludes.Add("c0mment.txt");
                Helper.DirectoryCopy(Path.Combine(src, "dadocuments"), Path.Combine(dest, "dadocuments"), true, excludes, true);
                #endregion
                return true;
            }
            else
                return false;
        }
    }
}