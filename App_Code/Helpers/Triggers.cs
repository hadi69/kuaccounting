﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using fyiReporting.RDL;
using System.Data;
using System.Net.Mail;

/// <summary>
/// Summary description for Triggers
/// </summary>
public class Triggers
{
    /// <summary>
    /// sample trigger
    /// </summary>
    /// <param name="id">record id: the return value from query</param>
    public static void BankPrintInsert(int id)
    {
        string defaultReportPath = HostingEnvironment.MapPath("~/app/print/BankPrint/fa/Report.mrt");
        string reportHome = HostingEnvironment.MapPath("~/app/print/BankPrint/fa/");

        BankPrint bankPrint = BankPrint.Find(id);

        if (bankPrint != null && !String.IsNullOrEmpty(bankPrint.Template))
        {
            File.Copy(reportHome + bankPrint.Template + ".mrt", reportHome + id + ".mrt");
        }
        else if (defaultReportPath != null && reportHome != null)
        {
            File.Copy(defaultReportPath, reportHome + id + ".mrt");
        }
    }
    public static void BankPrintUpdate(int id)
    {

    }
    public static void BankPrintDelete(int id)
    {
        string reportHome = HostingEnvironment.MapPath("~/app/print/BankPrint/fa/");
        try
        {
            File.Delete(reportHome + id + ".mrt");
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
        }
    }
    public static void DocumentaccountingItemInsert(int id)
    {
        DataRow row = DbProxy.Instance.FillRow("SELECT * FROM DocumentAccountingItems WHERE ID = " + id);
        if (row == null || Helper.GetInt(row, "PersonnelID", Null.NullInteger) <= 0)
            return;
        int pID = Helper.GetInt(row, "PersonnelID", Null.NullInteger);
        if (pID <= 0)
            return;
        decimal owe = Helper.GetDecimal(row, "Owe", 0), due = Helper.GetDecimal(row, "Due", 0);
        if (owe == 0 && due == 0)
            return;
        string value = (owe + due).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        TreeInfo t = AccController.GetTree(Helper.GetString(row, "TreePath", ""));
        if (t == null || !t.PersonNeed)
            return;
        DataRow person = DbProxy.Instance.FillRow("SELECT Name, Surname, CellNumber1 FROM Personnel WHERE ID = " + pID);
        if (person == null)
            return;
        string msg = owe > 0 ? LangProvider.Instance["OweSms"] : LangProvider.Instance["DueSms"];
        string name = Helper.GetString(person, "Name", ""), surname = Helper.GetString(person, "Surname", ""), cellNo = Helper.GetString(person, "CellNumber1", "");
        if (string.IsNullOrEmpty(cellNo))
            return;
        msg = string.Format(msg, name, surname, value);
        SmsHelper.SendSms(name + " " + surname, cellNo, msg);
    }
    public static void DocumentaccountingItemUpdate(int id)
    {
        DataRow row = DbProxy.Instance.FillRow("SELECT * FROM DocumentAccountingItems WHERE ID = " + id);
        if (row == null || Helper.GetInt(row, "PersonnelID", Null.NullInteger) <= 0)
            return;
        int pID = Helper.GetInt(row, "PersonnelID", Null.NullInteger);
        if (pID <= 0)
            return;
        decimal owe = Helper.GetDecimal(row, "Owe", 0), due = Helper.GetDecimal(row, "Due", 0);
        if (owe == 0 && due == 0)
            return;
        string value = (owe + due).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        TreeInfo t = AccController.GetTree(Helper.GetString(row, "TreePath", ""));
        if (t == null || !t.PersonNeed)
            return;
        DataRow person = DbProxy.Instance.FillRow("SELECT Name, Surname, CellNumber1 FROM Personnel WHERE ID = " + pID);
        if (person == null)
            return;
        string msg = owe > 0 ? LangProvider.Instance["OweSms"] : LangProvider.Instance["DueSms"];
        string name = Helper.GetString(person, "Name", ""), surname = Helper.GetString(person, "Surname", ""), cellNo = Helper.GetString(person, "CellNumber1", "");
        if (string.IsNullOrEmpty(cellNo))
            return;
        msg = string.Format(msg, name, surname, value);
        SmsHelper.SendSms(name + " " + surname, cellNo, msg);
    }
    public static void ConfigUpdate(int id)
    {
        DataRow row = DbProxy.Instance.FillRow("SELECT * FROM Config WHERE ID = " + id);
        if (row == null)
            return;
        string name = Helper.GetString(row, "Name", null);
        if (string.IsNullOrEmpty(name))
            return;
        if (name.ToLower() == "AppendCommentToPerson".ToLower())
        {
            string value = Helper.GetString(row, "Value", null);
            string query = null;
            if (value == "1")
                query = @"
-- =============================================
-- Author:		umed
-- Create date: 1394-04-31
-- Description:	Display Personnel full name according to config (AppendCommentToPerson)
-- =============================================
ALTER FUNCTION [dbo].[DisplayPerson]
(
	@ID INT, @ShowIdentify BIT
)
RETURNS nvarchar(4000)
AS
BEGIN	
	DECLARE @Res nvarchar(4000)
	IF (@ShowIdentify = 1)
	    SELECT @Res = Name + ' ' + Surname + ' - ' + ISNULL(Comments, '') + ' ' + ISNULL(PIdentify, '') FROM Personnel WHERE ID = @ID
    ELSE
      SELECT @Res = Name + ' ' + Surname + ' - ' + ISNULL(Comments, '') FROM Personnel WHERE ID = @ID
	RETURN @Res
END";
            else
                query = @"
-- =============================================
-- Author:		umed
-- Create date: 1394-04-31
-- Description:	Display Personnel full name according to config (AppendCommentToPerson)
-- =============================================
ALTER FUNCTION [dbo].[DisplayPerson]
(
	@ID INT, @ShowIdentify BIT
)
RETURNS nvarchar(4000)
AS
BEGIN	
	DECLARE @Res nvarchar(4000)
	IF (@ShowIdentify = 1)
      SELECT @Res = Name + ' ' + Surname + ' ' + ISNULL(PIdentify, '') FROM Personnel WHERE ID = @ID
    ELSE
        SELECT @Res = Name + ' ' + Surname FROM Personnel WHERE ID = @ID
	RETURN @Res
END";
            DbProxy.Instance.ExecuteCommand(query);
        }
    }
    public static void AdvertisementInsertUpdate(int id)
    {
        try
        {
            DataRow row = DbProxy.Instance.FillRow("SELECT * FROM Advertisement WHERE ID = " + id);
            if (row == null || Helper.GetInt(row, "UserID", Null.NullInteger) <= 0)
                return;
            DataRow userRow = DbProxy.Instance.FillRow("SELECT * FROM Users WHERE ID = " + Helper.GetInt(row, "UserID", Null.NullInteger));
            int status = Helper.GetInt(row, "Status", Null.NullInteger);
            string msg = Helper.GetString(row, "AdminComments", "");
            string email = Helper.GetString(row, "Email", "");
            if (string.IsNullOrEmpty(email) || !email.Contains('@'))
                return;
            string title = "";
            if (status == 1)
                title = "آگهی شما ثبت شد";
            if (status == 2)
                title = "آگهی شما مورد پذیرش قرار نگرفت";
            if (status == 3)
                title = "آگهی شما منتشر شد";
            if (status == 4)
                title = "آگهی شما فروخته شد";
            if (status == 5)
                title = "آکهی شما منقضی شد";
            msg = Helper.GetString(userRow, "Fullname", Helper.GetString(userRow, "Username", "کاربر")) + " گرامی\r\n<br />" + title + "\r\n<br />" + msg;
            MailMessage mailMsg = EmailSender.CreateMessage();
            mailMsg.Subject = title;
            mailMsg.IsBodyHtml = true;
            mailMsg.Body = msg;
            mailMsg.To.Add(email);
            EmailSender.SendEmial(mailMsg, true);
        }
        catch (Exception ex)
        {
            ExceptionLog.Instance.Add(ex);
        }
    }
}
