﻿using System;
using System.Collections.Generic;
// Helper extenstion method to translate a string in C#
// Usage:
// You can use it in two ways:
// "Lang.Name".Translate()
// Or without the phrase "Lang."
// "Name".Translate()
// Both will work
using System.Text;

public static class Extensions
{
    public static string Translate(this String str)
    {
        str = str.StartsWith("Lang.") ? str : "Lang." + str;

        return LangProvider.Instance.ParseLang(str);
    }

    // This is not a good place for this extension (the class to which the extension is attached)
    // But I have to clean up other people's mess
    // I have to declare it here.
    // Ka Umed, xwa betnase!!!!!
    // :-)
    public static void StartFormGroupContainer(this StringBuilder builder)
    {
        builder.AppendFormat("<div class='col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container'>");
    }
    public static void EndFormGroupContainer(this StringBuilder builder)
    {
        builder.AppendFormat("</div>");
    }

    public static void StartLargeFormGroupContainer(this StringBuilder builder)
    {
        builder.AppendFormat("<div class='col-xs-12 form-group-container'>");
    }
    public static void EndLargeFormGroupContainer(this StringBuilder builder)
    {
        builder.AppendFormat("</div>");
    }
    public static void StartFormGroup(this StringBuilder builder)
    {
        builder.AppendFormat("<div class='form-group'>");
    }
    public static void EndFormGroup(this StringBuilder builder)
    {
        builder.AppendFormat("</div>");
    }

    public static void StartRow(this StringBuilder builder)
    {
        builder.AppendFormat("<div class='row'>");
    }
    public static void EndRow(this StringBuilder builder)
    {
        builder.AppendFormat("</div>");
    }


    public static void StartContainer(this StringBuilder builder)
    {
        builder.AppendFormat("<div class='container'>");
    }
    public static void EndContainer(this StringBuilder builder)
    {
        builder.AppendFormat("</div>");
    }

    public static void StartForm(this StringBuilder builder, string id)
    {
        builder.AppendFormat("<form class='padding-10' id='{0}'>", id);
    }
    public static void EndForm(this StringBuilder builder)
    {
        builder.AppendFormat("</form>");
    }
    public static void StartControl(this StringBuilder builder)
    {
        builder.AppendFormat("<div class='col-xs-12 col-sm-8 form-col-without-padding'>");
    }
    public static void EndControl(this StringBuilder builder)
    {
        builder.AppendFormat("</div>");
    }

    public static void StartLargeControl(this StringBuilder builder)
    {
        builder.AppendFormat("<div class='col-xs-12 col-eleven margin-10-bottom form-col-without-padding'>");
    }
    public static void EndLargeControl(this StringBuilder builder)
    {
        builder.AppendFormat("</div>");
    }
    public static void AddLabel(this StringBuilder builder, string forWhom, string title)
    {
        builder.AppendFormat("<label for='{0}' class='left-aligned-label control-label col-xs-12 col-sm-4'>{1}</label>", forWhom, title);
    }

    public static void AddLargeLabel(this StringBuilder builder, string forWhom, string title)
    {
        builder.AppendFormat("<label for='{0}' class='left-aligned-label control-label col-xs-12 col-one'>{1}</label>", forWhom, title);
    }
    public static void AddTextInput(this StringBuilder builder, string id, string name, string value, string cssClass)
    {
        builder.StartControl();
        builder.AppendFormat(
            "<input type='text' id='{0}' name='{1}' value='{2}' class='form-control input-sm {3}' />",
            id, name, value, cssClass);
        builder.EndControl();
    }
    public static void AddCheckBox(this StringBuilder builder, string id, string name, bool checkedValue, string cssClass)
    {
        builder.StartControl();
        builder.AppendFormat(
            "<input type='checkbox' id='{0}' name='{1}' {2} class='pull-right {3}' />",
            id, name, checkedValue ? "checked='checked'" : "", cssClass);
        builder.EndControl();
    }
    public static void AddDateInput(this StringBuilder builder, string id, string name, string value, string cssClass)
    {
        builder.AddTextInput(id, name, value, "dateSelect " + cssClass);
    }
    public static void AddHiddenInput(this StringBuilder builder, string id, string name, string value)
    {
        builder.AppendFormat("<input type='hidden' id='{0}' name='{1}' value='{2}'>", id, name, value);
    }
    public static void AddEmptyLabel(this StringBuilder builder)
    {
        builder.AppendFormat("<div class='col-sm-4'>&nbsp;</div>");
    }
    public static void AddRadioButton(this StringBuilder builder, string title, string id, string name, string value, bool checkedValue)
    {
        builder.AppendFormat("<div class='pull-right margin-15-left'><label for='{0}'>{1}</label><input type='radio' id='{2}' name='{3}' value='{4}' {5} /></div>",
            name, title, id, name, value, checkedValue ? "checked='checked'" : "");
    }

    public static void AddSelect(this StringBuilder builder, string id, string name, string[] options, decimal value, string cssClass)
    {
        builder.StartControl();
        builder.AppendFormat("<select id='{0}' name='{1}' class='chosen-select chosen-rtl {2}'>", id, name, cssClass);
        for (int j = 0; j < options.Length; j++)
        {
            string[] nameVal = options[j].Split('=');
            if (nameVal.Length == 2)
                builder.AppendFormat("<option value='{0}' {2}>{1}</option>", nameVal[0], nameVal[1], Helper.GetDecimal(nameVal[0], 0) == value ? "selected" : "");
        }
        builder.Append("</select>");
        builder.EndControl();
    }
    public static void AddXml(this StringBuilder builder, string id, string name, string[] options, string value, string cssClass)
    {
        builder.StartControl();
        builder.AppendFormat("<select id='{0}' name='{1}' class='chosen-select chosen-rtl {2}'>", id, name, cssClass);
        for (int j = 0; j < options.Length; j++)
        {
            builder.AppendFormat("<option value='{0}' {1}>{0}</option>", options[j], options[j] == value ? "selected" : "");
        }
        builder.Append("</select>");
        builder.EndControl();
    }
    public static void AddTextFile(this StringBuilder builder, string id, string name, string[] options, string value, string cssClass)
    {
        builder.StartControl();
        builder.AppendFormat("<select id='{0}' name='{1}' class='chosen-select chosen-rtl {2}'>", id, name, cssClass);
        for (int j = 0; j < options.Length; j++)
            builder.AppendFormat("<option value='{0}' {1}>{0}</option>", options[j], options[j] == value ? "selected" : "");
        builder.Append("</select>");
        builder.EndControl();
    }
    public static void AddButton(this StringBuilder builder, string id, string value, string cssClass)
    {
        builder.AppendFormat("<input type=button id='{0}' class='btn btn-sm btn-primary {1}' value='{2}' />", id, cssClass, value);
    }

    public static void AddInputGroup(this StringBuilder builder, string id, string name, string value, string cssClass, string hiddenId, string hiddenName, string hiddenValue, string btnId, string btnValue, string btnCssClass)
    {
        builder.AppendFormat(
            "<div class='input-group input-group-sm'>"
         + "<input type='text' id='{0}' name='{1}' value='{2}' class='form-control {3}' autocomplete='off'>"
         + "<input type='hidden' id='{4}' name='{5}' value='{6}'>"
         + "<span class='input-group-btn'>"
         + "<input type='button' id='{7}' value='{8}' class='btn btn-primary {9}'>"
         + "</span>"
         + "</div>",
         id, name, value, cssClass, hiddenId, hiddenName, hiddenValue, btnId, btnValue, btnCssClass
        );
    }
    public static string Join<T>(this string joinWith, IEnumerable<T> list)
    {
        if (list == null)
            throw new ArgumentNullException("list");
        if (joinWith == null)
            throw new ArgumentNullException("joinWith");

        var stringBuilder = new StringBuilder();
        var enumerator = list.GetEnumerator();

        if (!enumerator.MoveNext())
            return string.Empty;

        while (true)
        {
            stringBuilder.Append(enumerator.Current);
            if (!enumerator.MoveNext())
                break;

            stringBuilder.Append(joinWith);
        }

        return stringBuilder.ToString();
    }
}
