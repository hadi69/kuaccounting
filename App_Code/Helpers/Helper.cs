﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Net;
using System.Text;
using System.IO;
using System.Globalization;
using System.Web.DynamicData;
using System.Web.Hosting;
using System.Web.UI.WebControls;
using System.Web.UI;
using LibGit2Sharp;
using Microsoft.Web.Administration;
using System.Web.Hosting;

/// <summary>
/// Helper methods to convert object to DataTypes.
/// Any converting exception will be logged into ExceptionLog.Instance
/// </summary>
public static class Helper
{
    public static System.Globalization.CultureInfo NumberCulture = new System.Globalization.CultureInfo("en-US");

    /// <summary>
    /// used internally to Convert Hijri Date to String and viceversa
    /// </summary>
    static System.Globalization.PersianCalendar persianCalendar = new System.Globalization.PersianCalendar();
    static DateTime emptyDateTime = new DateTime();
    public static DateTime NullDate = Null.NullDate;
    public static string FormatDate(DateTime date, string format)
    {
        switch (SiteSettings.DateType)
        {
            case DateType.fa:
                if (string.IsNullOrEmpty(format))
                    format = "yyyy/MM/dd";
                return ToFaFormat(date, format);
            case DateType.en:
            case DateType.ar:
                if (date != emptyDateTime && date != NullDate)
                {
                    if (string.IsNullOrEmpty(format))
                        format = "dd/MM/yyyy";
                    return date.ToString(format);
                }
                return "";
        }
        return date.ToString();
    }
    public static DateTime ParseDate(string text, DateTime defaultValue)
    {
        switch (SiteSettings.DateType)
        {
            case DateType.fa:
                return ParseFaDate(text, defaultValue);
            case DateType.en:
            case DateType.ar:
                return ParseEnDate(text, defaultValue);
        }
        return defaultValue;
    }
    /// <summary>
    /// format a Hijri Date like 1347/11/02
    /// </summary>
    /// <param name="date">date</param>
    /// <returns>formatted string of the Date</returns>
    public static string ToFaFormat(DateTime date)
    {
        if (date != emptyDateTime && date != NullDate)
            return string.Format("{0}/{1}/{2}", persianCalendar.GetYear(date), persianCalendar.GetMonth(date), persianCalendar.GetDayOfMonth(date));
        return "";
    }
    public static string ToFaFormat(DateTime date, string format)
    {
        if (date == emptyDateTime || date == NullDate)
            return "";
        try
        {
            string[] ymd = ToFaFormat(date).Split('/');
            int yy = int.Parse(ymd[0]);
            int mm = int.Parse(ymd[1]);
            int dd = int.Parse(ymd[2]);
            if (format.Contains("yy") && !format.Contains("yyyy"))
                yy = yy % 100;
            format = format.Replace("yyyy", yy.ToString("0000"))
                .Replace("yy", yy.ToString("00"))
                .Replace("MM", mm.ToString("00"))
                .Replace("dd", dd.ToString("00"))
                .Replace("HH", date.Hour.ToString("00"))
                .Replace("mm", date.Minute.ToString("00"))
                .Replace("ss", date.Second.ToString("00"))
                .Replace("fff", date.Millisecond.ToString("000"))
                .Replace("ff", (date.Millisecond / 10).ToString("00"))
                .Replace("f", (date.Millisecond / 100).ToString("0"));
            return format;
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    public static string FormatDateFull(DateTime date)
    {
        if (date == emptyDateTime || date == NullDate)
            return "";
        if (SiteSettings.DateType == DateType.fa)
            return Helper.ToFaFormat(date, "yyyy/MM/dd-HH:mm:ss");
        return date.ToString("dd/MM/yyyy-HH:mm:ss");
    }

    // Take a DateTime object and return something like this:
    // بیست و سوم اردیبهشت هزار و سیصد و نود و چهار
    public static string ToFaFormatLong(DateTime date)
    {
        return GetDayOfMonthName(date) + " " + ToPersianMonthName(date) + " " + "Month".Translate() + " " + NumberToText(persianCalendar.GetYear(date));
    }


    // Take a date and return the day of month in Persian
    public static string GetDayOfMonthName(DateTime date)
    {
        int dayOfMonth = persianCalendar.GetDayOfMonth(date);

        return (new[] { "DateFirst", "DateSecond", "DateThird", "DateFourth", "DateFifth", "DateSixth", "DateSeventh", "DateEighth", "DateNinth", "DateTenth", "DateEleventh", "DateTwelfth", "DateThirteenth", "DateFourteenth", "DateFifteenth", "DateSixteenth", "DateSeventeenth", "DateEighteenth", "DateNineteenth", "DateTwentieth", "DateTwentyFirst", "DateTwentySecond", "DateTwentyThird", "DateTwentyFourth", "DateTwentyFifth", "DateTwentySixth", "DateTwentySeventh", "DateTwentyEighth", "DateTwentyNinth", "DateThirtieth", "DateThirtyFirst" }[dayOfMonth - 1].Translate());
    }

    /// <summary>
    /// Parse a String to DateTime
    /// </summary>
    /// <param name="text">formatted string of the Hijri Date like 1347/11/02</param>
    /// <param name="defaultValue">default return value on fail</param>
    /// <returns>
    ///     if possible returns a DateTime
    ///     else returns defaultValue
    /// </returns>
    public static DateTime ParseFaDate(string text, DateTime defaultValue)
    {
        if (string.IsNullOrEmpty(text))
            return defaultValue;

        try
        {
            text = text.Replace("<span dir='ltr'>", "");
            text = text.Replace("</span>", "");
            string[] ymd = text.Split('/', '\\', '-', '.');
            if (ymd.Length != 3)
            {
                if (text.Length == 8)
                    ymd = new string[] { text.Substring(0, 4), text.Substring(4, 2), text.Substring(6) };
                else if (ymd.Length == 2) // only year, month
                    ymd = new string[] { ymd[0], ymd[1], "1" };
                else if (ymd.Length == 1) // only year
                    ymd = new string[] { ymd[0], "1", "1" };
                else
                    return defaultValue;
            }

            // check for dmy
            if (Convert.ToInt32(ymd[2]) > 31)
                ymd = new string[] { ymd[2], ymd[1], ymd[0] };
            int year = Convert.ToInt32(ymd[0]);
            if (year < 100)
                year += 1300;
            return persianCalendar.ToDateTime(year, Convert.ToInt32(ymd[1])
            , Convert.ToInt32(ymd[2]), DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, 0);
        }
        catch { }
        return defaultValue;
    }
    public static DateTime ParseFaDateTime(string text, DateTime defaultValue)
    {
        if (string.IsNullOrEmpty(text))
            return defaultValue;

        try
        {
            text = text.Replace("<span dir='ltr'>", "");
            text = text.Replace("</span>", "");
            string[] ymd = text.Split('/', '\\', '-', '.', ' ', ':');
            if (ymd.Length < 5)
                return ParseFaDate(text, defaultValue);

            // check for dmy
            if (Convert.ToInt32(ymd[2]) > 31)
                ymd = new string[] { ymd[2], ymd[1], ymd[0] };
            int year = Convert.ToInt32(ymd[0]);
            if (year < 100)
                year += 1300;
            return persianCalendar.ToDateTime(year, Convert.ToInt32(ymd[1])
                , Convert.ToInt32(ymd[2]), Convert.ToInt32(ymd[3]), Convert.ToInt32(ymd[4]), ymd.Length > 5 ? Convert.ToInt32(ymd[5]) : 0, 0);
        }
        catch { }
        return defaultValue;
    }
    public static DateTime ParseEnDate(string text, DateTime defaultValue)
    {
        if (string.IsNullOrEmpty(text))
            return defaultValue;
        text = text.Replace("<span dir='ltr'>", "");
        text = text.Replace("</span>", "");
        string[] ymd = text.Split('/', '\\', '-', '.');
        //dd/mm/yyyy
        if (ymd.Length != 3)
        {
            if (text.Length == 8)
                //ymd = new string[] { text.Substring(0, 4), text.Substring(4, 2), text.Substring(6) };
                ymd = new string[] { text.Substring(4), text.Substring(2, 2), text.Substring(0, 2) };
            else
                return defaultValue;
        }
        else
            Array.Reverse(ymd);
        try
        {
            int year = Convert.ToInt32(ymd[0]);
            if (year < 100)
                year += 2000;
            return new DateTime(year, Convert.ToInt32(ymd[1])
            , Convert.ToInt32(ymd[2]), 0, 0, 0, 0);
        }
        catch { }
        return defaultValue;
    }


    public static DateTime ParseReversEnDate(string text, DateTime defaultValue)
    {
        if (string.IsNullOrEmpty(text))
            return defaultValue;
        text = text.Replace("<span dir='ltr'>", "");
        text = text.Replace("</span>", "");
        string[] ymd = text.Split('/', '\\', '-', '.');
        //dd/mm/yyyy
        if (ymd.Length != 3)
        {
            if (text.Length == 8)
                ymd = new string[] { text.Substring(0, 4), text.Substring(4, 2), text.Substring(6) };
            //ymd = new string[] { text.Substring(4), text.Substring(2, 2), text.Substring(0, 2) };
            else
                return defaultValue;
        }
        else
            Array.Reverse(ymd);
        try
        {
            int year = Convert.ToInt32(ymd[0]);
            if (year < 100)
                year += 2000;
            return new DateTime(year, Convert.ToInt32(ymd[1])
            , Convert.ToInt32(ymd[2]), 0, 0, 0, 0);
        }
        catch { }
        return defaultValue;
    }
    public static DateTime ParseTabletDate(string text, DateTime defaultValue)
    {
        if (string.IsNullOrEmpty(text))
            return defaultValue;
        // yyyy-MM-dd
        // yyyy-MM-dd HH:mm:ss.fff
        string[] ymd = text.Split('-', ' ', ':', '.');
        try
        {
            if (ymd.Length == 3)
                return new DateTime(Convert.ToInt32(ymd[0]), Convert.ToInt32(ymd[1]), Convert.ToInt32(ymd[2]), 0, 0, 0, 0);
            else
                return new DateTime(Convert.ToInt32(ymd[0]), Convert.ToInt32(ymd[1]), Convert.ToInt32(ymd[2])
                    , Convert.ToInt32(ymd[3]), Convert.ToInt32(ymd[4]), Convert.ToInt32(ymd[5]), Convert.ToInt32(ymd[6]));
        }
        catch { }
        return defaultValue;
    }
    public static string DisplayTime(DateTime date)
    {
        return date.ToString("HH:mm");
    }
    static DateTime AppendTime(DateTime dt, string time)
    {
        if (string.IsNullOrEmpty(time))
            return dt;
        string[] hm = time.Split(':');
        if (hm == null || hm.Length != 2)
            return dt;
        int h = GetInt(hm[0], -1);
        int m = GetInt(hm[1], -1);
        if (h == 24)
            h = 0;
        if (h < 0 || h > 23)
            return dt;
        if (m == 60)
            m = 0;
        if (m < 0 || m > 59)
            return dt;
        return new DateTime(dt.Year, dt.Month, dt.Day, h, m, 0);
    }
    public static string ToPersianDayName(DateTime date)
    {
        return date.DayOfWeek.ToString().Translate();
    }
    public static string ToPersianMonthName(DateTime date)
    {
        int month = persianCalendar.GetMonth(date);
        return ToPersianMonthName(month);
    }
    public static string ToPersianMonthName(int persianMonth)
    {
        switch (persianMonth)
        {
            case 1:
                return "Farvardin".Translate();
            case 2:
                return "Ordibehesht".Translate();
            case 3:
                return "Khordad".Translate();
            case 4:
                return "Tir".Translate();
            case 5:
                return "Mordad".Translate();
            case 6:
                return "Shahrivar".Translate();
            case 7:
                return "Mehr".Translate();
            case 8:
                return "Aban".Translate();
            case 9:
                return "Azar".Translate();
            case 10:
                return "Dey".Translate();
            case 11:
                return "Bahman".Translate();
            case 12:
                return "Esfand".Translate();
            default:
                break;
        }
        return "";
    }
    public static byte[] ToBytes(params int[] list)
    {
        byte[] buf = new byte[list.Length * 4];
        for (int i = 0; i < list.Length; i++)
            System.BitConverter.GetBytes(list[i]).CopyTo(buf, i * 4);
        return buf;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="buf"></param>
    /// <param name="startIndex"></param>
    /// <param name="list"></param>
    /// <returns>end index</returns>
    public static int ToBytes(byte[] buf, int startIndex, params int[] list)
    {
        for (int i = 0; i < list.Length; i++)
            System.BitConverter.GetBytes(list[i]).CopyTo(buf, startIndex + i * 4);
        return startIndex + list.Length * 4;
    }
    public static string GetString(object obj)
    {
        if (obj == null || obj is DBNull)
            return null;
        return obj.ToString();
    }
    public static string GetString(object obj, string defaultValue)
    {
        if (obj == null || obj is DBNull)
            return defaultValue;
        return obj.ToString();
    }
    /// <summary>
    /// for multivalues from Request
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="defaultValue"></param>
    /// <param name="index"></param>
    /// <returns></returns>
    public static string GetString(object obj, string defaultValue, int index)
    {
        if (obj == null || obj is DBNull)
            return defaultValue;
        string res = obj.ToString();
        if (res != null)
            if (res.Contains(",")) // multi value
                res = res.Split(',')[index];
        return res;
    }
    public static string GetString(DataRow row, string columnName, string defaultValue)
    {
        if (row == null)
            return defaultValue;
        try
        {
            object obj = row[columnName];
            if (obj == null || obj is DBNull)
                return defaultValue;
            return obj.ToString();
        }
        catch (Exception ex)
        {
            //if (!ex.Message.Contains("belong to table"))
            //    ExceptionLog.Instance.Add(ex);
        }
        return defaultValue;
    }
    public static object GetObject(DataRow row, string columnName)
    {
        try
        {
            object obj = row[columnName];
            if (obj == null || obj is DBNull)
                return null;
            return obj;
        }
        catch (Exception ex)
        {
            //if (!ex.Message.Contains("belong to table"))
            //    ExceptionLog.Instance.Add(ex);
        }
        return null;
    }
    public static int GetInt(string str, int defaultValue)
    {
        if (string.IsNullOrEmpty(str))
            return defaultValue;
        try
        {
            int res;
            if (int.TryParse(str, NumberStyles.Integer, NumberCulture, out res))
                return res;
            //return Convert.ToInt32(str, NumberCulture);
        }
        catch (Exception ex)
        {
            //ExceptionLog.Instance.Add(ex);
        }
        return defaultValue;
    }
    public static int GetInt(object obj, int defaultValue)
    {
        if (obj == null || obj is DBNull)
            return defaultValue;
        try
        {
            return Convert.ToInt32(obj, NumberCulture);
        }



        catch (Exception ex)
        {
            //ExceptionLog.Instance.Add(ex);
        }
        return defaultValue;
    }
    public static decimal GetDecimal(object obj, decimal defaultValue)
    {
        if (obj == null || obj is DBNull)
            return defaultValue;
        try
        {
            return Convert.ToDecimal(obj, NumberCulture);
        }
        catch (Exception ex)
        {
            //ExceptionLog.Instance.Add(ex);
        }
        return defaultValue;
    }
    public static float GetFloat(object obj, float defaultValue)
    {
        if (obj == null || obj is DBNull)
            return defaultValue;
        try
        {
            return Convert.ToSingle(obj, NumberCulture);
        }
        catch (Exception ex)
        {
            //ExceptionLog.Instance.Add(ex);
        }
        return defaultValue;
    }
    public static double GetDouble(object obj, double defaultValue)
    {
        if (obj == null || obj is DBNull)
            return defaultValue;
        try
        {
            return Convert.ToDouble(obj, NumberCulture);
        }
        catch (Exception ex)
        {
            ExceptionLog.Instance.Add(new Exception("obj=" + obj, ex));
        }
        return defaultValue;
    }
    public static double TryGetDouble(object obj, double defaultValue)
    {
        if (obj == null || obj is DBNull)
            return defaultValue;
        try
        {
            return Convert.ToDouble(obj, NumberCulture);
        }
        catch (Exception ex)
        {
            //ExceptionLog.Instance.Add(new Exception("obj=" + obj, ex));
        }
        return defaultValue;
    }
    public static long GetLong(object obj, long defaultValue)
    {
        if (obj == null || obj is DBNull)
            return defaultValue;
        try
        {
            return Convert.ToInt64(obj, NumberCulture);
        }
        catch (Exception ex)
        {
            ExceptionLog.Instance.Add(new Exception("obj=" + obj, ex));
        }
        return defaultValue;
    }
    public static DateTime GetDateTime(object obj, DateTime defaultValue)
    {
        if (obj == null || obj is DBNull)
            return defaultValue;
        try
        {
            return Convert.ToDateTime(obj, NumberCulture);
        }
        catch (Exception ex)
        {
            //ExceptionLog.Instance.Add(ex);
        }
        return defaultValue;
    }
    public static T GetEnum<T>(string str, T defaultValue)
    {
        if (string.IsNullOrEmpty(str))
            return defaultValue;
        try
        {
            return (T)Enum.Parse(typeof(T), str, true);
        }
        catch (Exception ex)
        {
            //ExceptionLog.Instance.Add(ex);
        }
        return defaultValue;
        //return (T)Enum.Parse(typeof(T), defaultValue, true);
    }
    public static T GetEnum<T>(DataRow row, string columnName, T defaultValue)
    {
        try
        {
            object obj = row[columnName];
            if (obj == null || obj is DBNull)
                return defaultValue;
            return GetEnum(obj.ToString(), defaultValue);
        }
        catch (Exception ex)
        {
            //if (!ex.Message.Contains("belong to table"))
            //    ExceptionLog.Instance.Add(ex);
        }
        return defaultValue;
    }
    public static bool GetBool(object obj)
    {
        return GetBool(obj, false);
    }
    public static bool GetBool(object obj, bool defaultValue)
    {
        if (obj == null || obj is DBNull)
            return defaultValue;
        try
        {
            return Convert.ToBoolean(obj, NumberCulture);
        }
        catch (Exception ex)
        {
            //ExceptionLog.Instance.Add(ex);
        }
        return defaultValue;
    }
    public static string QoutedString(string str)
    {
        return QoutedString(str, false);
    }
    public static string QoutedString(string str, bool nullable)
    {
        if (str == null)
            return nullable ? "null" : "''";
        return string.Concat("N'", NormalizeChars(str.Replace("'", "''")), "'");
    }
    public static string Likenize(string str)
    {
        if (str == null)
            return "'%%'";
        return string.Concat("N'%", NormalizeChars(str.Replace("'", "''")), "%'");
    }

    public static bool DateIsNothing(DateTime date)
    {
        return date.ToOADate() < 5;
    }

    public static string QoutedDate(DateTime dt)
    {
        if (dt == NullDate)
            return "NULL";
        return "'" + dt.ToString("yyyy-MM-dd", NumberCulture) + "'";
        //return ((dt.ToOADate() - 2).ToString(NumberCulture));
        //return "'" + dt.ToShortDateString() + "'";
    }
    public static string QoutedDateTime(DateTime dt)
    {
        if (dt == NullDate)
            return "NULL";
        return "'" + dt.ToString("yyyy-MM-dd HH:mm:ss.fff", NumberCulture) + "'";
        //return ((dt.ToOADate() - 2).ToString(NumberCulture));
        //return "'" + dt.ToString() + "'";
    }

    public static string NormalizeChars(string str)
    {
        if (string.IsNullOrEmpty(str))
            return str;
        return str.Replace('ي', 'ی').Replace('ك', 'ک');
    }

    #region Get values from DataRow
    public static int GetInt(DataRow row, string column, int defaultValue)
    {
        try
        {
            return GetInt(row[column], defaultValue);
        }
        catch (Exception ex)
        {
            //if (!ex.Message.Contains("belong to table"))
            //    ExceptionLog.Instance.Add(ex);
        }
        return defaultValue;
    }
    public static decimal GetDecimal(DataRow row, string column, decimal defaultValue)
    {
        try
        {
            return GetDecimal(row[column], defaultValue);
        }
        catch (Exception ex)
        {
            //if (!ex.Message.Contains("belong to table"))
            //    ExceptionLog.Instance.Add(ex);
        }
        return defaultValue;
    }
    public static decimal GetDecimal(DataRowView row, string column, decimal defaultValue)
    {
        try
        {
            return GetDecimal(row[column], defaultValue);
        }
        catch (Exception ex)
        {
            //if (!ex.Message.Contains("belong to table"))
            //    ExceptionLog.Instance.Add(ex);
        }
        return defaultValue;
    }
    public static float GetFloat(DataRow row, string column, float defaultValue)
    {
        try
        {
            return GetFloat(row[column], defaultValue);
        }
        catch (Exception ex)
        {
            //if (!ex.Message.Contains("belong to table"))
            //    ExceptionLog.Instance.Add(ex);
        }
        return defaultValue;
    }
    public static double GetDouble(DataRow row, string column, double defaultValue)
    {
        try
        {
            return GetDouble(row[column], defaultValue);
        }
        catch (Exception ex)
        {
            //if (!ex.Message.Contains("belong to table"))
            //    ExceptionLog.Instance.Add(ex);
        }
        return defaultValue;
    }
    public static long GetLong(DataRow row, string column, long defaultValue)
    {
        try
        {
            return GetLong(row[column], defaultValue);
        }
        catch (Exception ex)
        {
            //if (!ex.Message.Contains("belong to table"))
            //    ExceptionLog.Instance.Add(ex);
        }
        return defaultValue;
    }

    public static DateTime GetDateTime(DataRow row, string column, DateTime defaultValue)
    {
        if (row == null)
            return defaultValue;
        try
        {
            return GetDateTime(row[column], defaultValue);
        }
        catch (Exception ex)
        {
            //if (!ex.Message.Contains("belong to table"))
            //    ExceptionLog.Instance.Add(ex);
        }
        return defaultValue;
    }
    public static bool GetBool(DataRow row, string column, bool defaultValue)
    {
        try
        {
            return GetBool(row[column], defaultValue);
        }
        catch (Exception ex)
        {
            //if (!ex.Message.Contains("belong to table"))
            //    ExceptionLog.Instance.Add(ex);
        }
        return defaultValue;
    }
    #endregion

    #region Pars Vars
    public static string ParseVars(string content, TableInfo tInfo, DataRow row, HttpRequest request)
    {
        if (tInfo == null)
            return ParseVars(content, (List<TableInfo>)null, row, request);
        List<TableInfo> tInfos = new List<TableInfo>();
        tInfos.Add(tInfo);
        return ParseVars(content, tInfos, row, request);
    }
    public static string ParseVars(string content, List<TableInfo> tInfos, DataRow row, HttpRequest request)
    {
        if (!content.Contains("@"))
            return content;

        int index = -1;
        while ((index + 1 < content.Length) && (index = content.IndexOf('@', index + 1)) >= 0)
        {
            int last = content.IndexOfAny(new char[] { ' ', '"', '\'', '<', '&', '\r', '\n' }, index + 1);
            string field = last > 0 ? content.Substring(index + 1, last - index - 1) : content.Substring(index + 1);
            object obj = null;
            if (row != null)
                obj = GetObject(row, field);
            TableInfo tInfo = null;
            if (tInfos != null)
                for (int i = 0; i < tInfos.Count; i++)
                    if (tInfos[i].GetField(field) != null)
                    {
                        tInfo = tInfos[i];
                        break;
                    }
            if (obj == null && (row == null || tInfo == null || tInfo.GetField(field) == null))
                if (request != null)
                {
                    obj = GetString(request.QueryString[field], null);
                    if (obj == null)
                        obj = GetString(request.Form[field], null);
                }
            string val = tInfo != null ? tInfo.DisplayValue(field, obj) : obj == null ? "" : obj.ToString();
            // Handle datetime
            if (null != tInfo)
            {
                FieldInfo f = tInfo.GetField(field);
                if (f != null && !string.IsNullOrEmpty(f.Type) && f.Type.ToLower() == "datetime")
                    content = content.Replace("@" + field + "_Time", DisplayTime(Helper.GetDateTime(obj, DateTime.Now)));
            }
            if (val != null && val.Contains("@"))
                val = val.Replace("@", "_AT_at_");
            if (val != null)
                val = val.Replace("'", "&#39;").Replace("\"", "&#34;");
            field = "@" + field;
            content = content.Replace(field + "'", val + "'").Replace(field + "\"", val + "\"").Replace(field + " ", val + " ").Replace(field + ",", val + ",").Replace(field + "\r", val + "\r")
                .Replace(field + "\n", val + "\n")
                .Replace(field + "<", val + "<").Replace(field + "(", val + "(").Replace(field + ")", val + ")").Replace(field + ";", val + ";");
            if (content.EndsWith(field))
                content = content.Replace(field, val);
        }
        content = content.Replace("_AT_at_", "@");
        return content;
    }
    public static string ParseQueryVars(string qry, TableInfo tInfo, DataRow row)
    {
        if (!qry.Contains("@"))
            return qry;
        qry = SiteSettings.ReplaceOptions(qry);

        int index = -1;
        while ((index + 1 < qry.Length) && (index = qry.IndexOf('@', index + 1)) >= 0)
        {
            // @@ --> @
            if (qry[index + 1] == '@' || qry[index - 1] == '@')
            {
                index += 2;
                continue;
            }
            int last = qry.IndexOfAny(new char[] { ' ', '"', '\'', ',', '(', ')', '\r', '\n', ';' }, index + 1);
            string field = last > 0 ? qry.Substring(index + 1, last - index - 1) : qry.Substring(index + 1);
            object obj = null;
            if (row != null)
                obj = GetObject(row, field);
            string val = obj == null ? "NULL" : Helper.QoutedString(obj.ToString());
            val = val.Replace(".0000'", "'").Replace(".000'", "'").Replace(".00'", "'").Replace(".0'", "'");

            // Handle datetime
            if (null != tInfo)
            {
                FieldInfo f = tInfo.GetField(field);
                if (f != null && !string.IsNullOrEmpty(f.Type) && f.Type.ToLower() == "datetime")
                    qry = qry.Replace("@" + field + "_Time", DisplayTime(Helper.GetDateTime(obj, DateTime.Now)));
            }
            if (val != null && val.Contains("@"))
                val = val.Replace("@", "_AT_at_");
            field = "@" + field;
            qry = qry.Replace(field + " ", val + " ").Replace(field + "\"", val + "\"").Replace(field + "'", val + "'")
                 .Replace(field + "(", val + "(").Replace(field + ")", val + ")").Replace(field + ",", val + ",")
                 .Replace(field + "\n", val + "\n")
                 .Replace(field + "\r", val + "\r").Replace(field + ";", val + ";");
            if (qry.EndsWith(field))
                qry = qry.Replace(field, val);
        }
        qry = qry.Replace("@@", "@");
        qry = qry.Replace("_AT_at_", "@");
        return qry; ;
    }
    public static string ParseQueryVars(string qry, TableInfo tInfo, HttpRequest request)
    {
        if (tInfo == null)
            return ParseQueryVars(qry, (List<TableInfo>)null, request);
        List<TableInfo> tInfos = new List<TableInfo>();
        tInfos.Add(tInfo);
        return ParseQueryVars(qry, tInfos, request);
    }
    public static string ParseQueryVars(string qry, List<TableInfo> tInfos, HttpRequest request)
    {
        qry = SiteSettings.ReplaceOptions(qry);
        int index = -1;
        while ((index + 1 < qry.Length) && (index = qry.IndexOf('@', index + 1)) >= 0)
        {
            // Console.WriteLine(index);
            // @@ --> @

            if (qry[index + 1] == '@' || qry[index - 1] == '@')
            {
                index += 2;
                continue;
            }
            int last = qry.IndexOfAny(new char[] { ' ', '"', '\'', ',', '(', ')', '\r', '\n', ';' }, index + 1);
            string field = last > 0 ? qry.Substring(index + 1, last - index - 1) : qry.Substring(index + 1);
            string val = Helper.GetString(request.Form[field], null);
            if (val == null)
                val = Helper.GetString(request.QueryString[field], null);
            if (field.EndsWith("_ISDATE"))
            {
                if (request.Form[field.Replace("_ISDATE", "")] != null)
                    val = Helper.QoutedDate(Helper.ParseDate(request.Form[field.Replace("_ISDATE", "")], Helper.NullDate));
                else
                    val = Helper.QoutedDate(Helper.ParseDate(request.QueryString[field.Replace("_ISDATE", "")], Helper.NullDate));
            }
            else if (field.EndsWith("_ISDATES"))
            {
                if (request.Form[field.Replace("_ISDATES", "")] != null)
                    val = request.Form[field.Replace("_ISDATES", "")];
                else
                    val = request.QueryString[field.Replace("_ISDATES", "")];
                // 1391/01/02, 1391/02/03, 1391/02/04 --> '2012/01/
                string[] vals = val.Split(',');
                val = "";
                for (int i = 0; i < vals.Length; i++)
                    val += Helper.ParseDate(vals[i], Helper.NullDate).ToString("yyyy-MM-dd", NumberCulture) + ",";
                val = val.Substring(0, val.Length - 1); // remove last comma
                val = Helper.QoutedString(val);
            }
            else if (tInfos != null && tInfos.Count > 0)
            {
                bool any = false;
                for (int i = 0; i < tInfos.Count; i++)
                {
                    if (tInfos[i] == null)
                        continue;
                    FieldInfo f = tInfos[i].GetField(field);
                    if (f == null)
                        continue;
                    val = tInfos[i].PrepareValue(field, val);
                    // Handle datetime
                    if (f != null && !string.IsNullOrEmpty(f.Type) && f.Type.ToLower() == "datetime")
                    {
                        DateTime dt;
                        if (DateTime.TryParse(val.Replace("'", ""), out dt))
                        {
                            string time = Helper.GetString(request.Form[field + "_Time"], null);
                            if (string.IsNullOrEmpty(time))
                                time = Helper.GetString(request.QueryString[field + "_Time"], null);
                            if (!string.IsNullOrEmpty(time))
                                val = QoutedDateTime(AppendTime(dt, time));
                        }
                    }
                    any = true;
                    break;
                }
                if (!any)
                    val = tInfos[0].PrepareValue(field, val);
            }
            //qry = qry.Replace("@" + field, val);
            // todo: we
            if (val != null && val.Contains("@"))
                val = val.Replace("@", "_AT_at_");
            field = "@" + field;
            qry = qry.Replace(field + " ", val + " ").Replace(field + "\"", val + "\"").Replace(field + "'", val + "'")
                .Replace(field + "(", val + "(").Replace(field + ")", val + ")").Replace(field + ",", val + ",")
                .Replace(field + "\n", val + "\n")
                .Replace(field + "\r", val + "\r").Replace(field + ";", val + ";");
            if (qry.EndsWith(field))
                qry = qry.Replace(field, val);
        }
        qry = qry.Replace("@@", "@");
        qry = qry.Replace("_AT_at_", "@");
        qry = qry.Replace("iAmcOMMA", ",");
        return qry;
    }
    public static string ParseFieldVars(string content, List<TableInfo> tInfos, DataRow row, DataTable ftable, HttpRequest request)
    {
        if (!content.Contains("@"))
            return content;

        int index = -1;
        while ((index + 1 < content.Length) && (index = content.IndexOf('@', index + 1)) >= 0)
        {
            int last = content.IndexOfAny(new char[] { ' ', '"', '\'', '<', '&', '\r', '\n' }, index + 1);
            string field = last > 0 ? content.Substring(index + 1, last - index - 1) : content.Substring(index + 1);
            string val = null;
            for (int i = 0; i < ftable.Rows.Count; i++)
            {
                int dec = int.Parse(Helper.GetString(ftable.Rows[i], "DecimalPlaces", ""));
                if (field == Helper.GetString(ftable.Rows[i], "FieldName", ""))
                {
                    val = Helper.GetString(ftable.Rows[i], "Value", "");
                    val = GetDecimalPlaceValue(val, dec);
                    ftable.Rows.Remove(ftable.Rows[i]);
                    break;
                }
            }
            if (string.IsNullOrEmpty(val))
            {
                object obj = null;
                if (row != null)
                    obj = GetObject(row, field);
                TableInfo tInfo = null;
                if (tInfos != null)
                    for (int i = 0; i < tInfos.Count; i++)
                        if (tInfos[i].GetField(field) != null)
                        {
                            tInfo = tInfos[i];
                            break;
                        }
                if (obj == null && (row == null || tInfo == null || tInfo.GetField(field) == null))
                    obj = GetString(request.QueryString[field], null);
                val = tInfo != null ? tInfo.DisplayValue(field, obj) : obj == null ? "" : obj.ToString();
                // Handle datetime
                if (null != tInfo)
                {
                    FieldInfo f = tInfo.GetField(field);
                    if (f != null && !string.IsNullOrEmpty(f.Type) && f.Type.ToLower() == "datetime")
                        content = content.Replace("@" + field + "_Time", DisplayTime(Helper.GetDateTime(obj, DateTime.Now)));
                }
                if (val != null && val.Contains("@"))
                    val = val.Replace("@", "_AT_at_");
            }
            field = "@" + field;
            content = content.Replace(field + "'", val + "'").Replace(field + "\"", val + "\"").Replace(field + " ", val + " ").Replace(field + ",", val + ",").Replace(field + "\r", val + "\r")
                .Replace(field + "\n", val + "\n")
                .Replace(field + "<", val + "<").Replace(field + "(", val + "(").Replace(field + ")", val + ")").Replace(field + ";", val + ";");
            if (content.EndsWith(field))
                content = content.Replace(field, val);
        }
        content = content.Replace("_AT_at_", "@");

        return content;
    }
    public static string FindParameter(string param, HttpRequest request)
    {
        string val = Helper.GetString(request.Form[param], null);
        if (val == null)
            val = Helper.GetString(request.QueryString[param], null);
        return val;
    }
    #endregion

    public static string GetError(string error, TableInfo info)
    {
        // Violation of UNIQUE KEY constraint 'IX_Language_Code'
        if (error.StartsWith("Violation of UNIQUE KEY constraint '"))
        {
            string fieldName = error.Substring("Violation of UNIQUE KEY constraint 'IX_".Length);
            int startIndex = fieldName.IndexOf('_');
            int endIndex = fieldName.IndexOf('\'');
            if (startIndex != -1 && endIndex != -1 && endIndex > startIndex)
            {
                fieldName = fieldName.Substring(startIndex + 1, endIndex - startIndex - 1);
                FieldInfo fInfo = info == null ? null : info.GetField(fieldName);
                if (fInfo != null && !string.IsNullOrEmpty(fInfo.Title))
                    return "DuplicateValueInField".Translate() + " : <br />" + fInfo.Title;
                if (fieldName == "Code")
                    fieldName = "Code".Translate();
                else if (fieldName == "Title")
                    fieldName = "Name".Translate();
                return "DuplicateValueInField".Translate() + " : <br />" + fieldName;
            }
            return "DuplicateValueInOneOfTheFields".Translate();
        }
        return "Error: " + error;
    }
    public static int SendSMS(string recipient, string message)
    {
        try
        {
            string POST_DATA = "username=" + DbProxy.Instance.GetConfig("SMSUsername", "")
                + "&password=" + DbProxy.Instance.GetConfig("SMSPassword", "")
                + "&message=" + message + "&recipient=" + recipient
                + "&number=" + DbProxy.Instance.GetConfig("PrivateNumber", "");
            byte[] Data = Encoding.UTF8.GetBytes(POST_DATA);
            HttpWebRequest HTTP_Request = (HttpWebRequest)WebRequest.Create(DbProxy.Instance.GetConfig("SMSUrl", "http://www.2972.ir/api"));
            HTTP_Request.Method = "POST";
            HTTP_Request.ContentType = "application/x-www-form-urlencoded";
            HTTP_Request.ContentLength = Data.Length;
            Stream NewStream = HTTP_Request.GetRequestStream();
            NewStream.Write(Data, 0, Data.Length);
            NewStream.Close();

            WebResponse Response = HTTP_Request.GetResponse();
            Stream ResponseStream = Response.GetResponseStream();
            StreamReader ResponseReader = new StreamReader(ResponseStream);
            return int.Parse(ResponseReader.ReadToEnd());
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
            return -1;
        }
    }

    static public string ConvertToGerigorian(string strdate)
    {
        string[] strList = strdate.Split('/');
        string str = "";
        if (strList.Length == 3)
        {
            try
            {
                if (strList[0].Length == 2)
                {
                    strList[0] = "13" + strList[0];
                }
                int PYear = Convert.ToInt32(strList[0]);
                int PMonth = Convert.ToInt32(strList[1]);
                int PDay = Convert.ToInt32(strList[2]);
                System.Globalization.PersianCalendar pc = new PersianCalendar();
                str = pc.ToDateTime(PYear, PMonth, PDay, 1, 1, 1, 1, GregorianCalendar.ADEra).ToShortDateString();
            }
            catch
            {
                if (strList[0].Length == 2)
                {
                    strList[0] = "13" + strList[0];
                }
                int PYear = Convert.ToInt32(strList[0]);
                int PMonth = Convert.ToInt32(strList[1]);
                if (strList[2].Length > 2)
                {
                    strList[2] = strList[2].Substring(2, 2);
                }
                int PDay = Convert.ToInt32(strList[2]);
                System.Globalization.PersianCalendar pc = new PersianCalendar();
                try
                {
                    str = pc.ToDateTime(PYear, PMonth, PDay, 1, 1, 1, 1, GregorianCalendar.ADEra).ToShortDateString();
                }
                catch
                {
                    string strDay = strList[2].Substring(2, 2);
                    PDay = Convert.ToInt32(strDay);
                    str = pc.ToDateTime(PYear, PMonth, PDay, 1, 1, 1, 1, GregorianCalendar.ADEra).ToShortDateString();
                }
            }
            str = str.Trim();
            string[] s = str.Split('/');
            str = s[2] + "-" + s[0] + "-" + s[1];
        }
        return str.Trim();
        //return ParsePersianDate(strdate, DateTime.Now).ToString("yyyy-MM-dd");
    }

    public static void UpdateNodeText(TreeNodeCollection nodes, string oldText, string newText)
    {
        for (int i = 0; i < nodes.Count; i++)
        {
            if (nodes[i].Text == oldText)
                nodes[i].Text = newText;
            UpdateNodeText(nodes[i].ChildNodes, oldText, newText);
        }
    }
    public static TreeNode FindNode(TreeNodeCollection nodes, string treePath)
    {
        for (int i = 0; i < nodes.Count; i++)
        {
            if (nodes[i].Value == treePath)
                return nodes[i];
            TreeNode res = FindNode(nodes[i].ChildNodes, treePath);
            if (res != null)
                return res;
        }
        return null;
    }

    public static void SignifyNegative(TableCell tableCell)
    {
        tableCell.CssClass = "neg";
    }

    public static bool ButtonIsClicked(Button button, HttpRequest request)
    {
        string clientID = button.ClientID.Replace('_', '$');

        for (int i = 0; i < request.Form.AllKeys.Length; i++)
        {
            if (request.Form.AllKeys[i] == clientID)
                return true;
        }
        clientID = '$' + button.ID;
        for (int i = 0; i < request.Form.AllKeys.Length; i++)
        {
            if (null != request.Form.AllKeys[i] && request.Form.AllKeys[i].EndsWith(clientID))
                return true;
        }
        return false;
    }
    public static string ToString(int value, int len)
    {
        string res = value.ToString().PadLeft(len, '0');
        //while (res.Length < len)
        //    res = "0" + res;
        return res;
    }

    public static Control FindControl(ControlCollection controls, Type type)
    {
        if (controls == null || controls.Count == 0)
            return null;
        for (int i = 0; i < controls.Count; i++)
        {
            if (controls[i].GetType().Equals(type))
                return controls[i];
            if (type.IsAssignableFrom(controls[i].GetType()))
                return controls[i];
            System.Web.UI.Control res = FindControl(controls[i].Controls, type);
            if (res != null)
                return res;
        }
        return null;
    }

    public static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs, List<string> excludes, bool overwrite)
    {
        if (!Directory.Exists(sourceDirName))
            return;
        if (excludes != null)
            for (int i = 0; i < excludes.Count; i++)
                excludes[i] = excludes[i].ToLower();

        // Get the subdirectories for the specified directory.
        DirectoryInfo dir = new DirectoryInfo(sourceDirName);
        DirectoryInfo[] dirs = dir.GetDirectories();

        if (!dir.Exists)
        {
            throw new DirectoryNotFoundException(
                "Source directory does not exist or could not be found: "
                + sourceDirName);
        }

        // If the destination directory doesn't exist, create it.             
        if (!Directory.Exists(destDirName))
            Directory.CreateDirectory(destDirName);

        // Get the files in the directory and copy them to the new location.
        FileInfo[] files = dir.GetFiles();
        for (int i = 0; i < files.Length; i++)
        {
            FileInfo file = files[i];
            if (excludes == null || !excludes.Contains(file.Name.ToLower()))
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, overwrite);
            }
        }

        // If copying subdirectories, copy them and their contents to new location. 
        if (copySubDirs)
        {
            for (int i = 0; i < dirs.Length; i++)
            {
                DirectoryInfo subdir = dirs[i];
                if (excludes == null || !excludes.Contains(subdir.Name.ToLower()))
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    // maxFolders only applies to first level
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs, excludes, overwrite);
                }
            }
        }
    }
    public static int GetStableHash(string s, int hashlength)
    {
        uint hash = 0;
        var bytes = System.Text.Encoding.ASCII.GetBytes(s);
        foreach (byte b in bytes)
        {
            hash += b;
            hash += (hash << 10);
            hash ^= (hash >> 6);
        }
        // final avalanche
        hash += (hash << 3);
        hash ^= (hash >> 11);
        hash += (hash << 15);

        return (int)(hash % hashlength);
    }
    public static List<string[]> GetRequestFields(string request)
    {
        List<string[]> fields = new List<string[]>();
        int Index = request.IndexOf('&');
        string subRequest = null;
        while (request.Contains("="))
        {
            if (Index > 0)
            {
                subRequest = request.Substring(0, Index);
                request = request.Substring(Index + 1, request.Length - Index - 1);
                Index = request.IndexOf('&');
            }
            else
            {
                subRequest = request;
                request = "";
            }
            string[] f = null;
            f = subRequest.Split('=');
            fields.Add(f);

        }

        return fields;
    }

    public static string BoolToYesOrNo(bool value)
    {
        return value ? LangProvider.Instance.ParseLang("Lang.BoolYes") : LangProvider.Instance.ParseLang("Lang.BoolNo");
    }

    public static string GetDecimalPlaceValue(string val, int dec)
    {
        if (string.IsNullOrEmpty(val))
            return val;
        if (val.Contains("."))
        {
            if (dec == 0)
                val = val.Substring(0, val.IndexOf('.'));
            else
                val = val.Substring(0, val.IndexOf('.') + dec + 1);
        }
        return val;
    }
    public static decimal GetFishValue(DataTable fishValues, string fieldName, decimal defaultValue)
    {
        string val = null;
        if (fishValues == null)
            return defaultValue;
        try
        {
            for (int i = 0; i < fishValues.Rows.Count; i++)
            {
                if (Helper.GetString(fishValues.Rows[i], "FieldName", "") == fieldName)
                {
                    val = Helper.GetString(fishValues.Rows[i], "Value", "");
                    int dec = int.Parse(Helper.GetString(fishValues.Rows[i], "DecimalPlaces", ""));
                    val = Helper.GetDecimalPlaceValue(val, dec);
                    return decimal.Parse(val);
                }
            }
        }
        catch (Exception ex)
        {
            ExceptionLog.Instance.Add(ex);
        }
        return defaultValue;
    }
    public static string GetEnumValuesCode(string EnumValueID)
    {
        if (string.IsNullOrEmpty(EnumValueID))
            return "";
        try
        {
            return DbProxy.Instance.FillScalar("SELECT Code FROM EnumValues WHERE ID=" + EnumValueID).ToString();
        }
        catch (Exception ex)
        {
            ExceptionLog.Instance.Add(ex);
        }
        return "";
    }
    public static string GetEnumValuesTitle(string EnumValueID)
    {
        if (string.IsNullOrEmpty(EnumValueID))
            return "";
        try
        {
            return DbProxy.Instance.FillScalar("SELECT Title FROM EnumValues WHERE ID=" + EnumValueID).ToString();
        }
        catch (Exception ex)
        {
            ExceptionLog.Instance.Add(ex);
        }
        return "";
    }
    public static bool HasFishValue(string ffid, string fishid)
    {
        int cnt = 0;
        cnt = Convert.ToInt32(DbProxy.Instance.FillScalar("SELECT COUNT(*) FROM FishValues WHERE FishDefFieldID=" + ffid + " AND FishID=" + fishid));
        if (cnt > 0)
            return true;
        return false;
    }
    public static bool HasOrderValue(string ffid, string orderid)
    {
        int cnt = 0;
        cnt = Convert.ToInt32(DbProxy.Instance.FillScalar("SELECT COUNT(*) FROM OrderValues WHERE OrderDefFieldID=" + ffid + " AND OrderID=" + orderid));
        if (cnt > 0)
            return true;
        return false;
    }
    public static string RemoveAccent(this string txt)
    {
        byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);
        return System.Text.Encoding.ASCII.GetString(bytes);
    }

    public static string Slugify(this string phrase)
    {
        string str = phrase.RemoveAccent().ToLower();
        str = System.Text.RegularExpressions.Regex.Replace(str, @"[^a-z0-9\s-\.]", ""); // Remove all non valid chars          
        str = System.Text.RegularExpressions.Regex.Replace(str, @"\s+", " ").Trim(); // convert multiple spaces into one space  
        str = System.Text.RegularExpressions.Regex.Replace(str, @"\s", "-"); // //Replace spaces by dashes
        return str;
    }
    public static string GetDomain()
    {
        ServerManager mgr = new ServerManager();
        String siteName = HostingEnvironment.ApplicationHost.GetSiteName();
        Site currentSite = mgr.Sites[siteName];

        String applicationAlias = HostingEnvironment.ApplicationVirtualPath;
        //String applicationName = applicationAlias.Substring(1);
        //Application app = currentSite.Applications[applicationAlias];

        return
            HttpContext.Current.Request.Url.Scheme
            + System.Uri.SchemeDelimiter
            + HttpContext.Current.Request.Url.Host
            + (HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + HttpContext.Current.Request.Url.Port)
            + applicationAlias;
    }

    public static FieldsType GetFieldType(string field)
    {
        string fieldType = null;
        string query = string.Format("SELECT FieldType FROM Fields WHERE Name=N'{0}'", field);
        fieldType = DbProxy.Instance.FillScalarString(query);
        return GetEnum(fieldType, FieldsType.None);
    }

    public static string NumberToText(long number, bool prependAnd = false)
    {
        string result = "";
        if (number < 0)
        {
            result = "Minus".Translate() + " " + NumberToText(-number);
        }
        else if (number == 0)
        {
            result = "";
        }
        else if (number <= 19)
        {
            result = (new[]
            {
                "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight",
                "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen",
                "Seventeen", "Eighteen", "Nineteen"
            }[number - 1]).Translate() + " ";
        }
        else if (number <= 99)
        {
            result = (new[] { "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" }[number / 10 - 2]).Translate() + NumberToText(number % 10, true);
        }
        else if (number <= 999)
        {
            result = (new[] { "OneHundred", "TwoHundred", "ThreeHundred", "FourHundred", "FiveHundred", "SixHundred", "SevenHundred", "EightHundred", "NineHundred" }[number / 100 - 1]).Translate() + NumberToText(number % 100, true);
        }
        else if (number <= 999999)
        {
            result = NumberToText(number / 1000) + " " + "Thousand".Translate() + NumberToText(number % 1000, true);
        }
        else if (number <= 999999999)
        {
            result = NumberToText(number / 1000000) + " " + "Million".Translate() + NumberToText(number % 1000000, true);
        }
        else if (number <= 999999999999)
        {
            result = NumberToText(number / 1000000000) + " " + "Billion".Translate() + NumberToText(number % 1000000000, true);
        }

        return (prependAnd && number != 0 ? " " + "And".Translate() + " " : "") + result;
    }

    public static Signature GetSignature()
    {
        return new Signature("Accounting Release", "jpco.release@gmail.com", new DateTimeOffset(DateTime.Now));
    }

    public static Commit CommitFiles(string message, string[] filePaths)
    {
        string repoPath = HostingEnvironment.MapPath("~");
        var repo = new Repository(repoPath);
        foreach (string filePath in filePaths)
        {
            repo.Stage(GetRootedPath(filePath));
        }

        try
        {
            return repo.Commit(message, GetSignature(), GetSignature());
        }
        catch (EmptyCommitException)
        {
            // If there is nothing to commit, do nothing!
            return null;
        }
    }

    public static Commit CommitSingleFile(string message, string filePath)
    {
        return CommitFiles(message, new[] { filePath });
    }

    public static string GetRootedPath(string path)
    {
        if (Path.IsPathRooted(path))
        {
            return path;
        }

        return HostingEnvironment.MapPath(path);
    }
    #region ReportFields

    public static string GetField(string Name, ReportFieldType Type)
    {
        int fieldID = DbProxy.Instance.GetReportField(Name, SiteSettings.ProgramID, Type);
        string fieldName = "";
        try
        {
            fieldName = DbProxy.Instance.FillScalar(@"SELECT Name FROM Fields WHERE ID=" + fieldID).ToString();

        }
        catch (Exception)
        {

            return fieldName;
        }
        return fieldName;
    }
    #endregion

    public static int GetFieldID(string field)
    {
        int ID = -1;
        string query = string.Format("SELECT ID FROM Fields WHERE Name=N'{0}'", field);
        ID = Helper.GetInt(DbProxy.Instance.FillScalar(query), -1);
        return ID;
    }
}
/// <summary>
/// Provide utilities methods related to <see cref="Control"/> objects
/// </summary>
public static class ControlUtilities
{
    /// <summary>
    /// Find the first ancestor of the selected control in the control tree
    /// </summary>
    /// <typeparam name="TControl">Type of the ancestor to look for</typeparam>
    /// <param name="control">The control to look for its ancestors</param>
    /// <returns>The first ancestor of the specified type, or null if no ancestor is found.</returns>
    public static TControl FindAncestor<TControl>(this Control control) where TControl : Control
    {
        if (control == null) throw new ArgumentNullException("control");

        Control parent = control;
        do
        {
            parent = parent.Parent;
            var candidate = parent as TControl;
            if (candidate != null)
            {
                return candidate;
            }
        } while (parent != null);
        return null;
    }

    /// <summary>
    /// Finds all descendants of a certain type of the specified control.
    /// </summary>
    /// <typeparam name="TControl">The type of descendant controls to look for.</typeparam>
    /// <param name="parent">The parent control where to look into.</param>
    /// <returns>All corresponding descendants</returns>
    public static IEnumerable<TControl> FindDescendants<TControl>(this Control parent) where TControl : Control
    {
        if (parent == null) throw new ArgumentNullException("control");

        if (parent.HasControls())
        {
            foreach (Control childControl in parent.Controls)
            {
                var candidate = childControl as TControl;
                if (candidate != null) yield return candidate;

                foreach (var nextLevel in FindDescendants<TControl>(childControl))
                {
                    yield return nextLevel;
                }
            }
        }
    }

    public static void SetLang(this Control parent)
    {
        if (SiteSettings.DateType == DateType.en)
        {
            var dates = parent.FindDescendants<AjaxControlToolkit.MaskedEditExtender>();
            foreach (var dte in dates)
                dte.Mask = "99/99/9999";
        }

        var labels = parent.FindDescendants<Label>();
        foreach (Label lbl in labels)
            lbl.Text = LangProvider.Instance.ParseLang(lbl.Text);

        var buttons = parent.FindDescendants<Button>();
        foreach (Button btn in buttons)
        {
            btn.Text = LangProvider.Instance.ParseLang(btn.Text);
            btn.ToolTip = LangProvider.Instance.ParseLang(btn.ToolTip);
        }

        var checkboxes = parent.FindDescendants<CheckBox>();
        foreach (CheckBox chk in checkboxes)
            chk.Text = LangProvider.Instance.ParseLang(chk.Text);

        var drops = parent.FindDescendants<DropDownList>();
        foreach (DropDownList drop in drops)
            foreach (ListItem item in drop.Items)
                item.Text = LangProvider.Instance.ParseLang(item.Text);

        var chkboxs = parent.FindDescendants<CheckBoxList>();
        foreach (CheckBoxList chkbox in chkboxs)
            foreach (ListItem item in chkbox.Items)
                item.Text = LangProvider.Instance.ParseLang(item.Text);

        var grids = parent.FindDescendants<GridView>();
        foreach (GridView grid in grids)
        {
            grid.EmptyDataText = LangProvider.Instance.ParseLang(grid.EmptyDataText);

            foreach (DataControlField col in grid.Columns)
            {
                col.HeaderText = LangProvider.Instance.ParseLang(col.HeaderText);
                if (col is CommandField)
                {
                    (col as CommandField).SelectText = LangProvider.Instance.ParseLang((col as CommandField).SelectText);
                    (col as CommandField).DeleteText = LangProvider.Instance.ParseLang((col as CommandField).DeleteText);
                    (col as CommandField).EditText = LangProvider.Instance.ParseLang((col as CommandField).EditText);
                }
            }
            var a = grid.EmptyDataTemplate;
        }
    }

}