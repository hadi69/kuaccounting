﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Summary description for EmailSender
/// </summary>
public class EmailSender
{
    public static MailMessage CreateMessage()
    {
        MailMessage message = new MailMessage();
        if (!string.IsNullOrEmpty(ConfigurationSettings.AppSettings["SMTP.FromEmailAddress"]))
            message.From = new MailAddress(ConfigurationSettings.AppSettings["SMTP.FromEmailAddress"]);
        else
            message.From = new MailAddress("info@iramallnovin.com");
        message.SubjectEncoding = message.BodyEncoding = new System.Text.UTF8Encoding();
        message.IsBodyHtml = true;

        return message;
    }
    public static void SendEmial(MailMessage message, bool sendAsync)
    {
        System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
        // ---------------------------------
        // the Host
        if (!string.IsNullOrEmpty(ConfigurationSettings.AppSettings["SMTP.Host"]))
            client.Host = ConfigurationSettings.AppSettings["SMTP.Host"];

        // ---------------------------------
        // the Port
        if (!string.IsNullOrEmpty(ConfigurationSettings.AppSettings["SMTP.Port"]))
            client.Host = ConfigurationSettings.AppSettings["SMTP.Port"];

        // ---------------------------------
        // the UserName/Password
        NetworkCredential credential = new NetworkCredential();
        if (!string.IsNullOrEmpty(ConfigurationSettings.AppSettings["SMTP.User"]))
            credential.UserName = ConfigurationSettings.AppSettings["SMTP.User"];
        if (!string.IsNullOrEmpty(ConfigurationSettings.AppSettings["SMTP.Password"]))
            credential.Password = ConfigurationSettings.AppSettings["SMTP.Password"];
        if (!string.IsNullOrEmpty(ConfigurationSettings.AppSettings["SMTP.Domain"]))
            credential.Password = ConfigurationSettings.AppSettings["SMTP.Domain"];
        if (string.IsNullOrEmpty(credential.Password) && string.IsNullOrEmpty(credential.Password)
            && string.IsNullOrEmpty(credential.Domain))
        {
            //client.UseDefaultCredentials = true;
        }
        else
            client.Credentials = credential;

        // ---------------------------------
        // SSL
        if (!string.IsNullOrEmpty(ConfigurationSettings.AppSettings["SMTP.UseSSL"]))
            client.EnableSsl = Helper.GetBool(ConfigurationSettings.AppSettings["SMTP.UseSSL"], false);

        if (sendAsync)
            client.SendAsync(message, null);
        else
            client.Send(message);
    }
}