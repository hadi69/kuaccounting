﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SmsHelper
/// </summary>
public class SmsHelper
{
    public static bool SendSms(string name, string cellNo, string msg)
	{
        // Status 0 means None or Temp
        // Status 1 means New or Ready To Send
        string qry = string.Format(@"DECLARE @ProjectID INT
SET @ProjectID = NULL
SELECT TOP 1 @ProjectID = ID FROM Projects
IF (@ProjectID IS NOT NULL)
BEGIN
	INSERT INTO Items (ProjectID, Person, CellNo, [Message], AddDate, [Status])
	VALUES (@ProjectID, {0}, {1}, {2}, GETDATE(), 0);
	SELECT SCOPE_IDENTITY();
END
ELSE
	SELECT -1"
            , Helper.QoutedString(name), Helper.QoutedString(cellNo), Helper.QoutedString(msg));
        object res = DbProxy.Instance.FillScalar(qry);
        return Helper.GetInt(res, -1) > 0;
	}
    public static bool SendToIramal(string name, string cellNo, string msg, bool isOrder, bool isAdv)
    {
        try
        {
            SMSService.service service = new SMSService.service();
            if (!string.IsNullOrWhiteSpace(cellNo))
            {
                string natli = service.AddSms("iramall", "iramallnovin", name, cellNo, msg);
            }

            if (isOrder)
            // send SMS to admin 1
            {
                string mobile1 = DbProxy.Instance.GetConfig("AppAdminMobile1", "", 0);
                if (!string.IsNullOrWhiteSpace(mobile1))
                {
                    string msg1 = DbProxy.Instance.GetConfig("AppAdminSMS1", "", 0);
                    if (string.IsNullOrWhiteSpace(msg1))
                        msg1 = msg;
                    service.AddSms("iramall", "iramallnovin", "Admin 1", mobile1, msg1.Replace("_NAME_", name).Replace("_CELL_", cellNo));
                }
            }

            if (isAdv)
            // send SMS to admin 2
            {
                string mobile2 = DbProxy.Instance.GetConfig("AppAdminMobile2", "", 0);
                if (!string.IsNullOrWhiteSpace(mobile2))
                {
                    string msg2 = DbProxy.Instance.GetConfig("AppAdminSMS2", "", 0);
                    if (string.IsNullOrWhiteSpace(msg2))
                        msg2 = msg;
                    service.AddSms("iramall", "iramallnovin", "Admin 2", mobile2, msg2.Replace("_NAME_", name).Replace("_CELL_", cellNo));
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
            return false;
        }
    }

}