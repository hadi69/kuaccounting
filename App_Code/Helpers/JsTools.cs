﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI;

/// <summary>
/// Summary description for JsTools
/// </summary>
public class JsTools
{
    public static void DefaultButton(Page page, WebControl textControl, string defaultButtonClientID)
    {
        // Sets default buttons.
        // Created by Janus Kamp Hansen - http://www.kamp-hansen.dk
        StringBuilder sScript = new StringBuilder();
        sScript.Append("<SCRIPT language='javascript'>\n");
        sScript.Append("function fnTrapKD(btn){\n");
        sScript.Append(" if (document.all){\n");
        sScript.Append("   if (event.keyCode == 13)\n");
        sScript.Append("   { \n");
        sScript.Append("     event.returnValue=false;\n");
        sScript.Append("     event.cancel = true;\n");
        sScript.Append("     btn.click();\n");
        sScript.Append("   } \n");
        sScript.Append(" } \n");
        sScript.Append("}\n");
        sScript.Append("</SCRIPT>\n");

        textControl.Attributes.Add("onkeydown", "fnTrapKD(document.all." + defaultButtonClientID + ")");
        page.RegisterStartupScript("ForceDefaultToScript", sScript.ToString());
    }
    public static void ShowMsg(Page pg, string msg)
    {
        pg.RegisterClientScriptBlock("_Show_Msg", string.Format("<script language='JavaScript'>alert('{0}');</script>", msg));
    }
    public static void SetFocus1(Control control)
    {
        StringBuilder sb = new StringBuilder();

        sb.Append("\r\n<script language='JavaScript'>\r\n");
        sb.Append("<!--\r\n");
        sb.Append("function SetFocus()\r\n");
        sb.Append("{\r\n");
        sb.Append("\tdocument.");

        Control p = control.Parent;
        while (p != null && !(p is System.Web.UI.HtmlControls.HtmlForm))
            p = p.Parent;

        sb.Append(p.ClientID);
        sb.Append("['");
        sb.Append(control.UniqueID);
        sb.Append("'].focus();\r\n");
        sb.Append("}\r\n");
        sb.Append("window.onload = SetFocus;\r\n");
        sb.Append("// -->\r\n");
        sb.Append("</script>");

        control.Page.RegisterClientScriptBlock("SetFocus", sb.ToString());
    }
    public static void SetFocus(Control control)
    {
        StringBuilder sb = new StringBuilder();

        sb.Append("\r\n<script language='JavaScript'>\r\n");
        sb.Append("<!--\r\n");
        sb.Append("function SetFocus()\r\n");
        sb.Append("{\r\n");
        sb.Append("\t$get('");
        sb.Append(control.ClientID);
        sb.Append("').focus();\r\n");
        sb.Append("}\r\n");
        sb.Append("window.onload = SetFocus;\r\n");
        sb.Append("// -->\r\n");
        sb.Append("</script>");
        //control.Page.un
        if (control.Page.ClientScript.IsClientScriptBlockRegistered("SetFocus"))
            control.Page.RegisterClientScriptBlock("SetFocus2", sb.ToString().Replace("SetFocus", "SetFocus2"));
        else
            control.Page.RegisterClientScriptBlock("SetFocus", sb.ToString());
    }
    public static void SetFocus(Control control, int index)
    {
        string func = string.Format("SetFocus{0}", index);
        StringBuilder sb = new StringBuilder();

        sb.Append("\r\n<script language='JavaScript'>\r\n");
        sb.Append("<!--\r\n");
        sb.Append(string.Format("function {0}()\r\n", func));
        sb.Append("{\r\n");
        sb.Append("\t$get('");
        sb.Append(control.ClientID);
        sb.Append("').focus();\r\n");
        sb.Append("}\r\n");
        sb.Append(string.Format("window.onload = {0};\r\n", func));
        sb.Append("// -->\r\n");
        sb.Append("</script>");
        //control.Page.un
        if (control.Page.ClientScript.IsClientScriptBlockRegistered(func))
            control.Page.RegisterClientScriptBlock(func + "2", sb.ToString().Replace(func, func + 2));
        else
            control.Page.RegisterClientScriptBlock(func, sb.ToString());
    }
    public static void SetFocusPartial(Control control, string funcName)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("\t$get('");
        sb.Append(control.ClientID);
        sb.Append("').focus();\r\n");
        ScriptManager.RegisterClientScriptBlock(control.Page, typeof(Page), funcName, sb.ToString(), true);
    }
    public static string GetConfirmDel(string msg)
    {
        return string.Format("return confirm('{0}');", msg);
    }
    public static void RegisterScanScript(LinkButton ctl, string fileName, bool workAround)
    {
        StringBuilder builder = new StringBuilder();
        builder.Append("<script language='javascript' type='text/javascript'>\n");
        builder.Append("function runScanApp()\n");
        builder.Append("{\n");
        builder.Append("    var browser=navigator.appVersion;\n");
        builder.Append("    if (browser.indexOf('Windows NT 5.2')>-1)\n");
        builder.Append("    {\n");
        builder.Append("      runScanAppXP();\n");
        builder.Append("    }\n");
        builder.Append("    else if (browser.indexOf('Windows NT 5.1')>-1)\n");
        builder.Append("    {\n");
        builder.Append("      runScanAppXP();\n");
        builder.Append("    }\n");
        builder.Append("    else\n");
        builder.Append("    {\n");
        builder.Append(string.Format("      window.open('file:///C:/Scanner.exe {0}', 1);\n", fileName));
        builder.Append("    }\n");
        builder.Append("}\n");
        builder.Append("function runScanAppXP() \n");
        builder.Append("{ \n");
        builder.Append("    var shell = new ActiveXObject('WScript.shell');\n");
        builder.Append(string.Format("    shell.run('c:\\Scanner.exe {0}', 1, true); \n", fileName));
        builder.Append("}\n");
        builder.Append("    </script>\n");
        if (workAround)
        {
            builder.Replace("runScanApp", "runScanApp2");
            ctl.OnClientClick = "runScanApp2()";
        }
        else
        {
            ctl.OnClientClick = "runScanApp()";
        }
        ctl.Page.RegisterStartupScript("ScaningScript", builder.ToString());
    }
    public static void BindDefaultKeyCatcher(Page page)
    {
        StringBuilder builder = new StringBuilder();
        builder.Append("<script language='javascript' type='text/javascript'>\n");
        builder.Append("<!--\n");
        builder.Append("  function keyCatcher() {\n");
        builder.Append("    var e2 = event.srcElement.tagName;\n");
        builder.Append("    if (event.keyCode == 8 && e2 != 'INPUT' && e2 != 'TEXTAREA') // && e2 != 'SELECT'\n");
        builder.Append("    {\n");
        builder.Append("        if (e2 != 'input' && e2 != 'textarea') \n");
        builder.Append("        {\n");
        builder.Append("            event.cancelBubble = true;\n");
        builder.Append("            event.returnValue = false;\n");
        builder.Append("        }\n");
        builder.Append("    }\n");
        builder.Append("}\n");
        builder.Append("  document.onkeydown = keyCatcher;\n");
        builder.Append("-->\n");
        builder.Append("    </script>\n");
        page.RegisterStartupScript("DefaultKeyCatcherScript", builder.ToString());
    }
    public static void BindHotKeys(Page page, List<string> clientIDs, List<int> keyCodes)
    {
        if (clientIDs == null || clientIDs.Count == 0 || keyCodes == null || clientIDs.Count != keyCodes.Count)
            return;
        StringBuilder builder = new StringBuilder();
        builder.Append("<script language='javascript' type='text/javascript'>\n");
        builder.Append("<!--\n");
        builder.Append("function HotKeyEvent() {\n");
        builder.Append("    var e2 = event.srcElement.tagName\n");
        builder.Append("    if (e2 == 'INPUT' || e2 == 'TEXTAREA' || e2 == 'SELECT' || e2 == 'input' || e2 == 'textarea' || e2 == 'select')\n");
        builder.Append("    {\n");
        builder.Append("        return;\n");
        builder.Append("    }\n");
        builder.Append("    key = event.keyCode;\n");
        builder.Append("    alert(key);\n");
        builder.Append("    switch(key) {\n");
        for (int i = 0; i < clientIDs.Count; i++)
        {
            builder.AppendFormat("case {0}:\n", keyCodes[i]);
            builder.AppendFormat("var b = $get('{0}');\n", clientIDs[i]);
            builder.AppendFormat("b.click();\n");
            builder.AppendFormat("break;\n");
        }
        //case 33:
        //var b = document.getElementById("Button1");
        //b.click();
        //break;
        builder.Append("    }\n");
        builder.Append("}\n");

        builder.Append("document.onkeypress = HotKeyEvent;\n");
        builder.Append("-->\n");
        builder.Append("    </script>\n");
        page.RegisterStartupScript("HotKeyScript", builder.ToString());
    }
    public static void HandleDeleteButton(GridViewRowEventArgs e, string msg)
    {
        try
        {
            // Handle the Delete Button Click and show a Confirmation Message
            TableCell cc = e.Row.Cells[e.Row.Cells.Count - 1];
            LinkButton delete = (LinkButton)cc.Controls[0];
            if (string.IsNullOrEmpty(msg))
                msg = "AreYouSureYouWantToDeleteThisRecord".Translate();
            delete.Attributes.Add("OnClick", GetConfirmDel(msg));
            delete.CssClass = "deleteButton";
        }
        catch
        {
        }
    }
    public static void HandleDeleteButton(LinkButton button, string msg)
    {
        button.Attributes.Add("OnClick", GetConfirmDel(msg));
    }
    public static void HandleDeleteButton(LinkButton button)
    {
        // Handle the Delete Button Click and show a Confirmation Message
        button.Attributes.Add("OnClick", GetConfirmDel("AreYouSureYouWantToDeleteThisRecord".Translate()));
    }
    public static void HandleDeleteButton(Button button)
    {
        HandleDeleteButton(button, "AreYouSureYouWantToDeleteThisRecord".Translate());
    }
    public static void HandleDeleteButton(Button button, string msg)
    {
        // Handle the Delete Button Click and show a Confirmation Message
        button.Attributes.Add("OnClick", GetConfirmDel(msg));
    }
}