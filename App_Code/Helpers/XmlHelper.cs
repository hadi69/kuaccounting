﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Xml;

public class XmlHelper
{
    //public static XmlAttribute CreateXmlAttr(XmlDocument xmlDoc, string attrName, Color attrValue)
    //{
    //    XmlAttribute attr = xmlDoc.CreateAttribute(attrName);
    //    attr.Value = attrValue.ToArgb().ToString();
    //    return attr;
    //}
    //public static XmlAttribute CreateXmlAttr(XmlDocument xmlDoc, string attrName, Font attrValue)
    //{
    //    XmlAttribute attr = xmlDoc.CreateAttribute(attrName);
    //    TypeConverter tc = TypeDescriptor.GetConverter(typeof(Font));
    //    attr.Value = tc.ConvertToString(attrValue);
    //    return attr;
    //}
    public static XmlAttribute CreateXmlAttr(XmlDocument xmlDoc, string attrName, object attrValue)
    {
        XmlAttribute attr = xmlDoc.CreateAttribute(attrName);
        attr.Value = attrValue == null ? "" : attrValue.ToString();
        return attr;
    }
    public static XmlAttribute CreateXmlAttr(XmlDocument xmlDoc, string attrName, int attrValue)
    {
        XmlAttribute attr = xmlDoc.CreateAttribute(attrName);
        attr.Value = attrValue.ToString(Helper.NumberCulture);
        return attr;
    }
    public static XmlAttribute CreateXmlAttr(XmlDocument xmlDoc, string attrName, float attrValue)
    {
        XmlAttribute attr = xmlDoc.CreateAttribute(attrName);
        attr.Value = attrValue == null ? "" : attrValue.ToString(Helper.NumberCulture);
        return attr;
    }
    public static XmlAttribute CreateXmlAttr(XmlDocument xmlDoc, string attrName, double attrValue)
    {
        XmlAttribute attr = xmlDoc.CreateAttribute(attrName);
        attr.Value = attrValue.ToString(Helper.NumberCulture);
        return attr;
    }
    //public static XmlNode CreateFontNode(XmlDocument xmlDoc, Font font, string nodeName)
    //{
    //    XmlNode xNode = xmlDoc.CreateNode(XmlNodeType.Element, nodeName, "");
    //    xNode.Attributes.SetNamedItem(CreateXmlAttr(xmlDoc, "Font", font));
    //    xNode.Attributes.SetNamedItem(CreateXmlAttr(xmlDoc, "FontName", font.Name));
    //    xNode.Attributes.SetNamedItem(CreateXmlAttr(xmlDoc, "FontSize", font.Size));
    //    xNode.Attributes.SetNamedItem(CreateXmlAttr(xmlDoc, "FontBold", font.Bold));
    //    xNode.Attributes.SetNamedItem(CreateXmlAttr(xmlDoc, "FontItalic", font.Italic));
    //    return xNode;
    //}
    public static string GetString(XmlNode node, string attrName, string defaultValue)
    {
        if (node.Attributes[attrName] == null)
            return defaultValue;
        try
        {
            return node.Attributes[attrName].Value;
        }
        catch (Exception ex)
        {
            LogOutterException(ex);
        }
        return defaultValue;
    }
    public static bool GetBool(XmlNode node, string attrName, bool defaultValue)
    {
        if (node.Attributes[attrName] == null)
            return defaultValue;
        try
        {
            if (node.Attributes[attrName].Value == "1")
                return true;
            if (node.Attributes[attrName].Value == "0")
                return false;
            return bool.Parse(node.Attributes[attrName].Value.ToLower());
        }
        catch (Exception ex)
        {
            LogOutterException(ex);
        }
        return defaultValue;
    }
    public static float GetSingle(XmlNode node, string attrName, float defaultValue)
    {
        if (node.Attributes[attrName] == null)
            return defaultValue;
        try
        {
            return float.Parse(node.Attributes[attrName].Value, Helper.NumberCulture);
        }
        catch (Exception ex)
        {
            LogOutterException(ex);
        }
        return defaultValue;
    }
    public static int GetInt(XmlNode node, string attrName, int defaultValue)
    {
        //ExceptionLog.Instance.Add(new Exception("TEST"));
        if (node.Attributes[attrName] == null)
            return defaultValue;
        try
        {
            return int.Parse(node.Attributes[attrName].Value, Helper.NumberCulture);
        }
        catch (Exception ex)
        {
            LogOutterException(ex);
        }
        return defaultValue;
    }
    public static byte GetByte(XmlNode xNode, string attrName, byte defaultValue)
    {
        if (xNode.Attributes[attrName] == null)
            return defaultValue;
        try
        {
            return Convert.ToByte(xNode.Attributes[attrName].Value, Helper.NumberCulture);
        }
        catch (Exception ex)
        {
            LogOutterException(ex);
        }
        return defaultValue;
    }
    public static double GetDouble(XmlNode node, string attrName, double defaultValue)
    {
        if (node.Attributes[attrName] == null)
            return defaultValue;
        try
        {
            return Convert.ToDouble(node.Attributes[attrName].Value, Helper.NumberCulture);
        }
        catch (Exception ex)
        {
            LogOutterException(ex);
        }
        return defaultValue;
    }
    public static T GetEnum<T>(XmlNode node, string attrName, T defaultValue)
    {
        if (node.Attributes[attrName] == null)
            return defaultValue;
        try
        {
            return (T)Enum.Parse(typeof(T), node.Attributes[attrName].Value);
        }
        catch (Exception ex)
        {
            LogOutterException(ex);
        }
        return defaultValue;
    }
    //public static Color GetColor(XmlNode xNode, string attrName, Color defaultValue)
    //{
    //    if (xNode.Attributes[attrName] == null)
    //        return defaultValue;
    //    try
    //    {
    //        return Color.FromArgb(Convert.ToInt32(xNode.Attributes[attrName].Value, Helper.NumberCulture));
    //    }
    //    catch (Exception ex)
    //    {
    //        LogOutterException(ex);
    //    }
    //    return defaultValue;
    //}
    //public static Font GetFont(XmlNode xNode, string attrName, Font defaultValue)
    //{
    //    if (xNode.Attributes[attrName] == null)
    //        return defaultValue;
    //    try
    //    {
    //        TypeConverter tc = TypeDescriptor.GetConverter(typeof(Font));
    //        Font res = (Font)tc.ConvertFromString(GetString(xNode, attrName, ""));
    //        return res;
    //    }
    //    catch (Exception ex)
    //    {
    //        LogOutterException(ex);
    //    }
    //    return defaultValue;
    //}
    //public static Font GetFont(XmlNode xNode, Font defaultFont)
    //{
    //    try
    //    {
    //        string fontName = xNode.Attributes["FontName"].Value;
    //        float size = float.Parse(xNode.Attributes["FontSize"].Value);
    //        bool bold = bool.Parse(xNode.Attributes["FontBold"].Value);
    //        bool italic = bool.Parse(xNode.Attributes["FontItalic"].Value);
    //        if (bold && italic)
    //            return new Font(fontName, size, FontStyle.Bold | FontStyle.Italic);
    //        if (bold)
    //            return new Font(fontName, size, FontStyle.Bold);
    //        if (italic)
    //            return new Font(fontName, size, FontStyle.Italic);
    //        return new Font(fontName, size);
    //    }
    //    catch (Exception ex)
    //    {
    //        LogOutterException(ex);
    //    }
    //    return defaultFont;
    //}
    public static XmlDocument GetXmlDocWithElement(string rootElementName, string version)
    {
        XmlDocument xmlDoc = new XmlDocument();
        string xmlStr = string.Format("<?xml version='1.0' encoding='UTF-8'?><{0} Version='{1}'></{0}>", rootElementName, version);
        xmlDoc.LoadXml(xmlStr);

        return xmlDoc;
    }
    private static void LogOutterException(Exception ex)
    {
        //System.Diagnostics.StackTrace stackTrace = new System.Diagnostics.StackTrace(true);
        //StringBuilder trace = new StringBuilder();
        //for (int i = 0; i < stackTrace.FrameCount; i++)
        //{
        //    System.Diagnostics.StackFrame sf = stackTrace.GetFrame(i);                
        //    System.Reflection.MethodBase method = sf.GetMethod();
        //    string typeName = method.ReflectedType.Name;
        //    if (String.Compare(typeName, "XmlHelper") != 0)
        //        trace.AppendLine(string.Format("{0} : {1} - FileName:{2} Line: {3}", typeName, method.ToString(), sf.GetFileName(), sf.GetFileLineNumber()));
        //}
        //Exception eex = new Exception(trace.ToString(), ex);
        ExceptionLog.Instance.Add(ex, "XmlHelper");

    }
}