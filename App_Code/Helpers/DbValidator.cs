﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Data;

/// <summary>
/// Summary description for DbValidator
/// </summary>
public class DbValidator : IDisposable
{
    private DbProxy mDbProxy = null;
    #region Database Information
    public List<string> GetTableNames()
    {
        List<string> arr = new List<string>();
        DataTable dt = GetUserTableIDs();
        if (dt == null)
            return (arr);
        for (int i = 0; i < dt.Rows.Count; i++)
            arr.Add(GetObjectNameWithID((int)dt.Rows[i]["id"]));
        return (arr);
    }
    public List<string> GetViewNames()
    {
        List<string> arr = new List<string>();
        DataTable dt = GetViewIDs();
        if (dt == null)
            return (arr);
        for (int i = 0; i < dt.Rows.Count; i++)
            arr.Add(GetObjectNameWithID((int)dt.Rows[i]["id"]));
        return (arr);
    }
    public int GetTableID(string tableName)
    {
        string strSQL = " select id from dbo.sysobjects where id = object_id(N'[dbo].[" + tableName + "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1";
        int res = Helper.GetInt(mDbProxy.FillScalar(strSQL), 0);
        return (res);
    }
    public int GetViewID(string ViewName)
    {
        string strSQL = " select id from dbo.sysobjects where id = object_id(N'[dbo].[" + ViewName + "]') and OBJECTPROPERTY(id, N'IsView') = 1";
        int res = Helper.GetInt(mDbProxy.FillScalar(strSQL), 0);
        return (res);
    }
    public DataTable GetUserTableIDs()
    {
        string strSQL = " select id from dbo.sysobjects where type='U'";
        DataTable res = mDbProxy.FillData(strSQL, "IDs");
        return (res);
    }
    public DataTable GetViewIDs()
    {
        string strSQL = " select id from dbo.sysobjects where type='V' and Category=0";//just user views
        DataTable res = mDbProxy.FillData(strSQL, "IDs");
        return (res);
    }
    public string GetObjectNameWithID(int ID)
    {
        string strSQL = " select name from dbo.sysobjects where id=" + ID;
        string res = mDbProxy.FillScalarString(strSQL);
        return (res);
    }
    public DataTable GetTableColumns(string tableName)
    {
        string strSQL = " EXEC sp_Columns " + Helper.QoutedString(tableName);
        DataTable res = mDbProxy.FillData(strSQL, "c");
        return (res);
    }
    public bool ColumnIsIdentity(string tableName, string columnName)
    {
        return (true);
    }
    public string GetViewDefinition(string ViewName)
    {
        string strSQL = " EXEC sp_helptext " + Helper.QoutedString(ViewName);
        DataTable dt = mDbProxy.FillData(strSQL, "Views");
        if (dt == null)
            return ("");
        else
        {
            string res = "";
            for (int i = 0; i < dt.Rows.Count; i++)//ignore Create View *** + AS
            {
                res = res + dt.Rows[i]["Text"].ToString().Trim();
                //if (!res.EndsWith(".") && !res.EndsWith("+") && !res.EndsWith("-") && !res.EndsWith("/") && !res.EndsWith("(") && !res.EndsWith(")"))
                res += " ";
            }
            int index = res.IndexOf("AS", 0);
            if (index >= 0)
                res = res.Substring(index + 2);
            return (res.Trim());
        }
    }
    #endregion
    public List<string> Messages = new List<string>();
    public DbValidator(string dbName)
    {
        if (string.IsNullOrEmpty(dbName))
            mDbProxy = new DbProxy();
        else
            mDbProxy = new DbProxy(dbName);
    }
    void ShowMsg(Exception ex)
    {
        if (ex == null)
            return;
        ShowMsg(ex.Message, "Exception");
    }
    void ShowMsg(string msg, string caption)
    {
        Messages.Add(string.Format("{0} : {1}", caption, msg));
    }
    #region Design TableInfo Synchronization

    public bool CreateNewView(string ViewName, string Qry)
    {
        string strSQL = "";
        if (GetViewID(ViewName) > 0)
        {
            if (Qry == GetViewDefinition(ViewName))
                return (true);
            strSQL = "DROP VIEW " + ViewName;
            mDbProxy.RunQuery(strSQL, 30, true);
        }
        else
            ShowMsg("Create New View : " + ViewName, "Database Synchronizer");
        strSQL = "CREATE VIEW " + ViewName + " AS " + Qry;
        return mDbProxy.RunQuery(strSQL, 30, true) > 0;
    }
    public bool CreateNewTable(string tableName, bool identity)
    {
        ShowMsg("Create New TableInfo : " + tableName, "Database Synchronizer");
        string strSQL = "CREATE TABLE dbo." + tableName + " ( ID int NOT NULL IDENTITY (1, 1)) ";
        if (!identity)
            strSQL = "CREATE TABLE " + tableName + " ( ID int NOT NULL ) ";
        return mDbProxy.RunQuery(strSQL, 30, true) > 0;
    }
    public bool DeleteTable(string tableName)
    {
        ShowMsg("Delete TableInfo : " + tableName, "Database Synchronizer");
        string strSQL = "DROP TABLE dbo." + tableName;
        return mDbProxy.RunQuery(strSQL, 30, true) > 0;
    }
    public bool DeleteView(string ViewName)
    {
        ShowMsg("Delete View : " + ViewName, "Database Synchronizer");
        string strSQL = "DROP VIEW " + ViewName;
        return mDbProxy.RunQuery(strSQL, 30, true) > 0;
    }
    public bool UpdateTable(TableInfo table)
    {
        if (!InsertColumns(table))
            return (false);
        if (!DeleteExtraColumns(table))
            return (false);
        if (!UpdateColumns(table))
            return (false);
        // Run queries
        List<string> queries = TableController.GetSetupQueries(table.Name);
        if (queries != null && queries.Count > 0)
            for (int i = 0; i < queries.Count; i++)
                try
                {
                    mDbProxy.RunQuery(queries[i], 300, true);
                    // ShowMsg("OK", table.Name + " Setup Query");
                }
                catch (Exception ex)
                {
                    ShowMsg(ex.Message, table.Name + " Setup Query");
                }

        return (true);
    }
    public bool DeleteColumn(string tableName, string columnName)
    {
        ShowMsg("Delete Column  : " + columnName, "Database Synchronizer [" + tableName + "]");
        string strSQL = "alter table " + tableName + " drop column [" + columnName + "]";
        int res = mDbProxy.RunQuery(strSQL, 30, true);
        return (res > 0);
    }
    public bool InsertColumn(string tableName, FieldInfo col)
    {
        ShowMsg("Insert Column  : " + col.Name, "Database Synchronizer [" + tableName + "]");
        string strSQL = "alter table " + tableName + " add [" + col.Name + "] " + col.Type.Replace("()", "") + (col.Nullable ? " NULL " : " NOT NULL ");
        if (col.Identity)
            strSQL += " IDENTITY (1, 1) ";
        int res = mDbProxy.RunQuery(strSQL, 30, true);
        return (res > 0);
    }
    public bool InsertColumn(string tableName, string fieldName, string type)
    {
        string strSQL = string.Format("alter table {0} add [{1}] {2} NULL", tableName, fieldName, type.Replace("()", ""));
        int res = mDbProxy.RunQuery(strSQL, true);
        return (res > 0);
    }
    public bool UpdateColumn(string tableName, FieldInfo col, DataRow dRow)
    {
        if (col.Identity)
            return true;
        string type = col.Type.Contains("(") ? col.Type.Substring(0, col.Type.IndexOf("(")) : col.Type;
        string precesion = col.Type.Contains("(") ? col.Type.Substring(col.Type.IndexOf("(") + 1, col.Type.IndexOf(")") - col.Type.IndexOf("(") - 1) : "";
        if (type == "int" && dRow["TYPE_NAME"].ToString() == "int")
            return true; // precesion is not required
        if (type == "bigint" && dRow["TYPE_NAME"].ToString() == "bigint")
            return true; // precesion is not required
        if (type == "bit" && dRow["TYPE_NAME"].ToString() == "bit")
            return true; // precesion is not required
        if (type == "datetime" && dRow["TYPE_NAME"].ToString() == "datetime")
            return true; // precesion is not required
        if (type == "float" && dRow["TYPE_NAME"].ToString() == "float")
            return true; // precesion is not required

        if (type == dRow["TYPE_NAME"].ToString() && type == "ntext")
            return true; // precision is not important
        string rowPrecesion = Helper.GetString(dRow, "PRECISION", "");
        if (type == "numeric")
        {
            string scale = Helper.GetString(dRow, "SCALE", "");
            if (!string.IsNullOrEmpty(scale))
                if (scale != "0" || precesion.Contains(","))
                {
                    rowPrecesion += "," + scale;
                    precesion = precesion.Replace(" ", "");
                }
        }
        if (type != dRow["TYPE_NAME"].ToString() || precesion != rowPrecesion)
        {
            ShowMsg("Edit Column  : " + col.Name + "  Set Data Type=" + col.Type.Replace("()", "").Replace("identity", "").Trim() + " (" + precesion + ")", "Database Synchronizer [" + tableName + "]");
            string strSQL = "alter table " + tableName + " alter column [" + col.Name + "] " + col.Type + (col.Nullable ? " NULL " : " NOT NULL ");
            if (col.Identity)
                strSQL += " IDENTITY (1, 1) ";
            int res = mDbProxy.RunQuery(strSQL, 30, true);
            if (res <= 0)
                return (false);
        }
        return (true);
    }
    //public bool UpdateViews(string viewFilter)
    //{
    //    //List<string> sViews = GetViewNames();
    //    //for (int i = 0; i < sViews.Count; i++)
    //    //{
    //    //    if (viewFilter.Trim() != "*" && !sViews[i].ToLower().Contains(viewFilter.ToLower()))
    //    //        continue;
    //    //    if (!CreateNewView(sViews[i], GetViewDefinition(SourceServer, SourceCatalog, SourceUser, SourcePassword, (string)sViews[i])))
    //    //        return (false);
    //    //}
    //    return (true);
    //}
    public bool InsertNewTables(List<TableInfo> tables, string tableFilter)
    {
        if (tableFilter != "*")
            return (true);
        //List<string> sTables = new List<string>();
        //for (int i = 0; i < tables.Count; i++)
        //    sTables.Add(tables[i].Name);
        List<string> dTables = GetTableNames();
        for (int i = 0; i < tables.Count; i++)
            if (!dTables.Contains(tables[i].Name))
                if (!CreateNewTable(tables[i].Name, tables[i].GetField("ID").Identity))
                {
                    ShowMsg(mDbProxy.LastException);
                    return (false);
                }
        return (true);
    }

    public bool DropConstraints(string tableName)
    {
        // remove Constraints (Diagram) = foreign keys
        string drop = @"DECLARE @sql NVARCHAR(MAX) = N'';

SELECT @sql += N'
ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_object_id))
    + '.' + QUOTENAME(OBJECT_NAME(parent_object_id)) + 
    ' DROP CONSTRAINT ' + QUOTENAME(name) + ';'
FROM sys.foreign_keys WHERE OBJECT_NAME(parent_object_id)='TABLENAME';

-- PRINT @sql;
EXEC sp_executesql @sql;".Replace("TABLENAME", tableName);
        try
        {
            mDbProxy.RunQuery(drop, true);
        }
        catch (Exception ex)
        {
            ShowMsg(ex.Message, tableName + " DROP foreign_keys");
        }

        // remove uniuqe keys
        drop = @"DECLARE @sql NVARCHAR(MAX) = N'';
SELECT @sql += N'
    ALTER TABLE ' + OBJECT_NAME(so.parent_obj) + ' DROP CONSTRAINT ' + so.name + ';'
FROM sysobjects so
WHERE OBJECT_NAME(so.parent_obj)='TABLENAME' AND so.xtype = 'UQ'
-- PRINT @sql;
EXEC sp_executesql @sql;".Replace("TABLENAME", tableName);
        try
        {
            mDbProxy.RunQuery(drop, true);
        }
        catch (Exception ex)
        {
            ShowMsg(ex.Message, tableName + " DROP Constraints");
        }

        return (true);
    }

    public bool CreateConstraints(TableInfo table)
    {
        // Create Constraints (Diagram)
        for (int i = 0; i < table.Fields.Count; i++)
        {
            if (!string.IsNullOrEmpty(table.Fields[i].Foriegn))
            {
                string qry = string.Format(@"ALTER TABLE dbo.{0} ADD CONSTRAINT
FK_{2}{0}_{1} FOREIGN KEY
    ({1}) REFERENCES dbo.{2}
    (ID) 
ON UPDATE NO ACTION 
ON DELETE {3}", table.Name, table.Fields[i].Name, table.Fields[i].Foriegn
              , table.Fields[i].CascadeDelete ? "CASCADE" : "NO ACTION");
                try
                {
                    mDbProxy.RunQuery(qry, true);
                    // ShowMsg("OK", table.Name + " Create Constraint: " + table.Fields[i].Foriegn);
                }
                catch (Exception ex)
                {
                    ShowMsg(ex.Message, table.Name + " Create Constraint: " + table.Fields[i].Foriegn);
                }
            }
        }
        return (true);
    }

    public bool UpdateTables(List<TableInfo> tables, string tableFilter)
    {
        // update tables
        List<string> dTables = GetTableNames();
        for (int i = 0; i < tables.Count; i++)
        {
            if (tableFilter.Trim() != "*" && !tables[i].Name.ToLower().Contains(tableFilter.ToLower()))
                continue;
            if (dTables.Contains(tables[i].Name))
                if (!UpdateTable(tables[i]))
                {
                    ShowMsg(mDbProxy.LastException);
                    return (false);
                }
        }

        return (true);
    }
    private void DropAllProcedures()
    {
        try
        {
            mDbProxy.RunQuery(@"-- Drop all procedures
declare @procName varchar(500)
declare cur cursor 
for select [name] from sys.objects where type in (N'P', N'PC')
open cur
fetch next from cur into @procName
while @@fetch_status = 0
begin
    exec('drop procedure ' + @procName)
    fetch next from cur into @procName
end
close cur
deallocate cur", true);
        }
        catch (Exception ex)
        {
            ShowMsg(ex.Message, "Drop all procedures");
        }
    }
    private void DropAllTriggers()
    {
        try
        {
            mDbProxy.RunQuery(@" -- Drop all triggers
DECLARE @SQLCmd nvarchar(1000)
DECLARE @Trig sysname
DECLARE @owner sysname
DECLARE @uid int

DECLARE TGCursor CURSOR FOR
SELECT name, uid FROM sysobjects WHERE type = 'TR'
OPEN TGCursor
FETCH NEXT FROM TGCursor INTO @Trig, @uid
WHILE @@FETCH_STATUS = 0
BEGIN

SET @SQLCmd = N'DROP TRIGGER [' + user_name(@uid) + '].[' + @Trig + ']'
EXEC sp_executesql @SQLCmd
PRINT @SQLCmd

FETCH next FROM TGCursor INTO @Trig, @uid
END

CLOSE TGCursor
DEALLOCATE TGCursor", true);
        }
        catch (Exception ex)
        {
            ShowMsg(ex.Message, "Drop all triggers");
        }
    }
    private void DropAllFunctions()
    {
        try
        {
            mDbProxy.RunQuery(@" -- Drop all functions
declare @procName varchar(500)
declare cur cursor 
for select [name] from sys.objects where type in (N'FN', N'IF', N'TF', N'FS', N'FT')
open cur
fetch next from cur into @procName
while @@fetch_status = 0
begin
    exec('drop FUNCTION ' + @procName)
    fetch next from cur into @procName
end
close cur
deallocate cur", true);
        }
        catch (Exception ex)
        {
            ShowMsg(ex.Message, "Drop all functions");
        }
    }
    private bool RunDefaultSetups(List<TableInfo> tables, List<string> queries, string tableFilter, bool log)
    {
        string okTables = "";
        int numOKs = 0;
        if (queries == null || queries.Count == 0)
            return true;
        foreach (string query in queries)
        {
            if (string.IsNullOrEmpty(query))
                continue;
            if (query.Contains("@Table"))
            {
                for (int i = 0; i < tables.Count; i++)
                {
                    if (tableFilter.Trim() != "*" && !tables[i].Name.ToLower().Contains(tableFilter.ToLower()))
                        continue;
                    string qry = query.Replace("@Table", tables[i].Name);
                    try
                    {
                        mDbProxy.LastException = null;
                        mDbProxy.RunQuery(qry, true);
                        if (mDbProxy.LastException == null)
                            okTables += tables[i].Name + ", ";
                        else if (mDbProxy.LastException.Message != null && !mDbProxy.LastException.Message.ToLower().Contains("already has a primary key defined on it"))
                            if (log)
                                ShowMsg("Error", tables[i].Name + " Setup Query : " + mDbProxy.LastException.Message + "<br />" + qry);
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message != null && !ex.Message.ToLower().Contains("already has a primary key defined on it"))
                            if (log)
                                ShowMsg(ex.Message, tables[i].Name + " Setup Query");
                    }
                }
            }
            else
                try
                {
                    if (query.Contains("CalcRule86"))
                    {
                        string uery = query;
                    }
                    mDbProxy.LastException = null;
                    mDbProxy.RunQuery(query, true);
                    if (mDbProxy.LastException == null)
                        numOKs++;
                    else if (mDbProxy.LastException.Message != null && !mDbProxy.LastException.Message.ToLower().Contains("already has a primary key defined on it"))
                        if (log)
                            ShowMsg("Error", " Setup Query : " + mDbProxy.LastException.Message + "<br />" + query);
                }
                catch (Exception ex)
                {
                    if (log)
                        ShowMsg(ex.Message, " Setup Query" + "<br />" + query);
                }
        }
        //if (okTables.Length > 0)
        //    ShowMsg(okTables.TrimEnd(',', ' '), "OK setup queries");
        if (numOKs > 0)
            if (log)
                ShowMsg(numOKs + " setup queries", "OK setup queries");
        return (true);
    }
    public bool DeleteExtraTables(List<TableInfo> tables, string tableFilter)
    {
        if (tableFilter != "*")
            return (true);
        List<string> sTables = new List<string>();
        for (int i = 0; i < tables.Count; i++)
            sTables.Add(tables[i].Name);
        List<string> dTables = GetTableNames();
        for (int i = 0; i < dTables.Count; i++)
            if (!sTables.Contains(dTables[i]))
                if (!DeleteTable(dTables[i]))
                {
                    ShowMsg(mDbProxy.LastException);
                    return false;
                }
        return (true);
    }
    public bool DeleteExtraColumns(TableInfo table)
    {
        DataTable dCols = GetTableColumns(table.Name);
        for (int i = 0; i < dCols.Rows.Count; i++)
        {
            bool find = false;
            for (int j = 0; j < table.Fields.Count; j++)
            {
                if (dCols.Rows[i]["COLUMN_NAME"].ToString() == table.Fields[j].Name)
                {
                    find = true;
                    continue;
                }
            }
            if (!find)
                DeleteColumn(table.Name, (string)dCols.Rows[i]["COLUMN_NAME"]);
        }
        dCols.Dispose();
        return (true);
    }
    public bool InsertColumns(TableInfo table)
    {
        DataTable dCols = GetTableColumns(table.Name);
        for (int i = 0; i < table.Fields.Count; i++)
        {
            bool find = false;
            for (int j = 0; j < dCols.Rows.Count; j++)
                if (table.Fields[i].Name == dCols.Rows[j]["COLUMN_NAME"].ToString())
                {
                    find = true;
                    continue;
                }
            if (!find)
                InsertColumn(table.Name, table.Fields[i]);
        }
        dCols.Dispose();
        return (true);
    }
    public bool UpdateColumns(TableInfo table)
    {
        DataTable dCols = GetTableColumns(table.Name);
        for (int i = 0; i < dCols.Rows.Count; i++)
            for (int j = 0; j < table.Fields.Count; j++)
                if (dCols.Rows[i]["COLUMN_NAME"].ToString() == table.Fields[j].Name)
                    UpdateColumn(table.Name, table.Fields[j], dCols.Rows[i]);
        dCols.Dispose();
        return (true);
    }
    public bool SynchronizeTableDesign(List<TableInfo> tables, string tableFilter)
    {
        bool old = DbProxy.AutoDisconnect;
        DbProxy.AutoDisconnect = false;
        try
        {
            List<string> dTables = GetTableNames();
            // drop constraints
            for (int i = 0; i < dTables.Count; i++)
            {
                if (tableFilter.Trim() != "*" && !dTables[i].ToLower().Contains(tableFilter.ToLower()))
                    continue;
                if (!DropConstraints(dTables[i]))
                {
                    ShowMsg(mDbProxy.LastException);
                    return (false);
                }
            }

            DropAllTriggers();

            if (!DeleteExtraTables(tables, tableFilter))
                return (false);
            if (!InsertNewTables(tables, tableFilter))
                return (false);
            if (!UpdateTables(tables, tableFilter))
                return (false);

            // create constraints
            for (int i = 0; i < tables.Count; i++)
            {
                if (tableFilter.Trim() != "*" && !tables[i].Name.ToLower().Contains(tableFilter.ToLower()))
                    continue;
                if (!CreateConstraints(tables[i]))
                {
                    ShowMsg(mDbProxy.LastException);
                    return (false);
                }
            }

            // views
            // procedures
            // functions

            DropAllProcedures();
            DropAllFunctions();

            RunDefaultSetups(tables, TableController.GetViews(), tableFilter, false);
            RunDefaultSetups(tables, TableController.GetDefaultProcedures(), tableFilter, false);
            RunDefaultSetups(tables, TableController.GetFunctions(), tableFilter, false);

            DropAllProcedures();
            DropAllFunctions();

            RunDefaultSetups(tables, TableController.GetDefaultProcedures(), tableFilter, true);
            RunDefaultSetups(tables, TableController.GetFunctions(), tableFilter, true);
            RunDefaultSetups(tables, TableController.GetViews(), tableFilter, true);


            // triggers at end
            RunDefaultSetups(tables, TableController.GetTriggers(), tableFilter, true);

            //if (!RunDefaultSetups(tables, tableFilter))
            //    return (false);

            if (SiteSettings.EnableSync)
                for (int i = 0; i < dTables.Count; i++)
                {
                    if (tableFilter.Trim() != "*" && !dTables[i].ToLower().Contains(tableFilter.ToLower()))
                        continue;
                    mDbProxy.RunQuery("UPDATE " + dTables[i] + " SET SyncVer = 0 WHERE SyncVer IS NULL", true);
                }
            if (SiteSettings.EnableSync)
                CreateSyncTriggers(tables, tableFilter);
        }
        finally
        {
            DbProxy.AutoDisconnect = old;
        }
        return (true);
    }
    private bool CreateSyncTriggers(List<TableInfo> tables, string tableFilter)
    {
        for (int i = 0; i < tables.Count; i++)
        {
            if (tableFilter.Trim() != "*" && !tables[i].Name.ToLower().Contains(tableFilter.ToLower()))
                continue;
            if (tables[i].Name.ToLower() == "SyncVers".ToLower())
                continue;

            TableInfo info = TableController.Instance[tables[i].Name];
            if (info == null || info.SyncLevel == SyncLevel.None)
                continue;
            // insert
            string qry = string.Format(@"-- =============================================
-- Author:		Umed
-- Create date: 1394/02/31
-- changed on 1394/09/04: single --> multi
-- Description:	Handle Sync
-- =============================================
CREATE TRIGGER [dbo].[Sync{0}Insert]
   ON  [dbo].[{0}]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @Counter BIGINT, @ID INT, @Exists BIT;
	
	SET @Counter = NULL
	SET @Exists = 1
	
	SELECT @Counter = [Counter] FROM SyncVers WHERE TableName = '{0}'
	IF @Counter IS NULL
	BEGIN
		SET @Counter = 1;
		SET @Exists = 0;
	END
	
	DECLARE items{0}Cur CURSOR LOCAL FOR
		SELECT ID FROM INSERTED
	OPEN items{0}Cur
	FETCH NEXT FROM items{0}Cur INTO @ID
	WHILE @@FETCH_STATUS = 0 BEGIN
		SET @Counter = @Counter + 1
		UPDATE {0} SET SyncVer = @Counter WHERE ID = @ID
		FETCH NEXT FROM items{0}Cur INTO @ID
	END
	CLOSE items{0}Cur
	DEALLOCATE items{0}Cur

	IF @Exists = 1
		UPDATE SyncVers SET [Counter] = @Counter WHERE TableName = '{0}'
	ELSE
		INSERT INTO SyncVers ([Counter], TableName) VALUES (@Counter, '{0}')
	
END ", tables[i].Name);
            mDbProxy.RunQuery(qry, 30, true);

            // update
            qry = string.Format(@"-- =============================================
-- Author:		Umed
-- Create date: 1394/02/31
-- changed on 1394/09/04: single --> multi
-- Description:	Handle Sync
-- =============================================
CREATE TRIGGER [dbo].[Sync{0}Update]
   ON  [dbo].[{0}]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @Counter BIGINT, @ID INT, @Exists BIT;
	
	-- Prevent Recurssion
	IF UPDATE(SyncVer)
      RETURN;
    IF trigger_nestlevel() > @LEVEL RETURN
    
	SET @Counter = NULL
	SET @Exists = 1
	
	SELECT @Counter = [Counter] FROM SyncVers WHERE TableName = '{0}'
	IF @Counter IS NULL
	BEGIN
		SET @Counter = 1;
		SET @Exists = 0;
	END
	
	DECLARE items{0}Cur CURSOR LOCAL FOR
		SELECT ID FROM INSERTED
	OPEN items{0}Cur
	FETCH NEXT FROM items{0}Cur INTO @ID
	WHILE @@FETCH_STATUS = 0 BEGIN
		SET @Counter = @Counter + 1
		UPDATE {0} SET SyncVer = @Counter WHERE ID = @ID
		FETCH NEXT FROM items{0}Cur INTO @ID
	END
	CLOSE items{0}Cur
	DEALLOCATE items{0}Cur

	IF @Exists = 1
		UPDATE SyncVers SET [Counter] = @Counter WHERE TableName = '{0}'
	ELSE
		INSERT INTO SyncVers ([Counter], TableName) VALUES (@Counter, '{0}')
END  ", tables[i].Name);
            if (tables[i].Name.ToLower() == "storeservices")
                qry = qry.Replace("@LEVEL", "3");
            else if (tables[i].Name.ToLower() == "storeservicedef" || tables[i].Name.ToLower() == "serviceprice"
                || tables[i].Name.ToLower() == "service")
                qry = qry.Replace("@LEVEL", "4");
            else
                qry = qry.Replace("@LEVEL", "1");
            mDbProxy.RunQuery(qry, 30, true);
        }
        return true;
    }
    public bool EnsureColumn(string tableName, string colname, string typeName)
    {
        try
        {
            using (DataTable cols = GetTableColumns(tableName))
            {
                for (int i = 0; i < cols.Rows.Count; i++)
                    if (cols.Rows[i]["COLUMN_NAME"].ToString() == colname)
                        return true;
                string strSQL = "alter table " + tableName + " add [" + colname + "] " + typeName.Replace("()", "");
                int res = mDbProxy.RunQuery(strSQL, 30, true);
                return (res > 0);
            }
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
            return false;
        }
    }

    #endregion


    #region IDisposable Members

    public void Dispose()
    {
        if (mDbProxy != null)
            mDbProxy.Dispose();
    }

    #endregion
}