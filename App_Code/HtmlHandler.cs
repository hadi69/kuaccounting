﻿using System;
using System.Web;

namespace MyHandlers
{
    public class HtmlHandler : IHttpHandler
    {
        public HtmlHandler()
        {
        }
        #region Implementation of IHttpHandler
        public void ProcessRequest(System.Web.HttpContext context)
        {
            string path = context.Request.Path;
            string file = context.Server.MapPath(path);
            if (System.IO.File.Exists(file))
                context.Response.WriteFile(file);
            else
                context.Response.WriteFile("~/help/notfound.htm");
            //HttpResponse objResponse = context.Response;
            //objResponse.Write("<html><body><h1>Hello 15Seconds   Reader ");
            //objResponse.Write("</body></html>");
        }
        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
        #endregion
    }
}