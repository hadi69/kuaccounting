﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Stimulsoft.Report;
using Stimulsoft.Report.Web;
using System.Data;

/// <summary>
/// Summary description for BaseReport
/// </summary>
public class BaseReport: System.Web.UI.Page
{
    protected HtmlForm form = new HtmlForm();
    protected Label lblMessage = new Label();
    protected StiWebDesigner reportDesigner = new StiWebDesigner();
    protected StiWebViewer reportViewer = new StiWebViewer();
    protected string reportPath;
    protected bool ShowDesigner
    {
        get
        {
            return (HttpContext.Current.Request.QueryString["design"] != null && HttpContext.Current.Request.QueryString["design"] == "true");
        }
    }
    protected bool printDirectly;
    protected StiReport report = new StiReport();
    private Dictionary<string, object> data = new Dictionary<string, object>(); 
    private DataSet dataSet = new DataSet("Tables");
    private Dictionary<string, object> variables = new Dictionary<string, object>();
    private Dictionary<string, string> reportNames = new Dictionary<string, string>();
    protected string defaultReportSlug = "Default";

    protected bool ChangesShouldBeCommited
    {
        get
        {
            string env = ConfigurationManager.AppSettings.Get("ENV");
            if (env != null && env == "prod")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public BaseReport()
    {
        form.Controls.Add(lblMessage);
        form.Controls.Add(reportViewer);
        form.Controls.Add(reportDesigner);
        Controls.Add(form);

        lblMessage.Visible = reportDesigner.Visible = reportViewer.Visible = false;

        printDirectly = (HttpContext.Current.Request.QueryString["print"] != null && 
            HttpContext.Current.Request.QueryString["print"] == "true");

        reportDesigner.SaveReport += reportDesigner_SaveReport;
    }

    void reportDesigner_SaveReport(object sender, StiWebDesigner.StiSaveReportEventArgs e)
    {
        if (String.IsNullOrEmpty(reportPath))
        {
            SetReportPath();
        }

        e.Report.Save(MapReportPath(reportPath));

        if (ChangesShouldBeCommited)
        {
            Helper.CommitSingleFile("Change the report", Path.Combine("~", reportPath));
        }
    }

    public void SetPageTitle(string title)
    {
        Title = reportDesigner.BrowserTitle = LangProvider.Instance.ParseLang(title);
        AddVariable("ReportTitle", Title);
    }

    public void ShowError(string message)
    {
        lblMessage.Text = message;
        lblMessage.Visible = true;
    }

    public void AddData(string name, object val)
    {
        data.Add(name, val);
    }

    public void AddTable(string name, DataTable table)
    {
        if (String.IsNullOrEmpty(name))
        {
            throw new Exception("Table name can not be empty. Please specify the 'name' argument");
        }
        table.TableName = name;
        dataSet.Tables.Add(table.Copy());
    }

    public void AddVariable(string name, object val)
    {
        variables.Add(name, val);
    }

    // Override this method and add your own logic, if you want to manipulate the report once it has been loaded
    public virtual void ManipulateReport()
    {}

    public void ShowReport()
    {
        report.Load(MapReportPath(reportPath));

        ManipulateReport();

        report.RegData(dataSet);

        foreach (KeyValuePair<string, object> keyValuePair in data)
        {
            report.RegBusinessObject(keyValuePair.Key, keyValuePair.Value);
        }

        foreach (KeyValuePair<string, object> keyValuePair in variables)
        {
            report.Dictionary.Variables.Add(keyValuePair.Key, keyValuePair.Value);
        }


        // Add common variables
        report.Dictionary.Variables.Add("Today", Helper.ToFaFormat(DateTime.Now));

        if (ShowDesigner)
        {
            report.Dictionary.SynchronizeBusinessObjects();
            reportDesigner.Report = report;
            reportDesigner.Design();
            reportDesigner.Visible = true;
        }
        else
        {
            reportViewer.Report = report;
            reportViewer.Visible = true;

            if (printDirectly)
            {
                reportViewer.PrintToDirect();
            }
        }
    }

    public virtual void SetReportPath()
    {
        throw new NotImplementedException("You need to override the SetReportPath and set the reportPath property there!");
    }

    public void AddReport(string slug, string reportFilePath)
    {
        if (!reportNames.ContainsKey(slug))
        {
            reportNames.Add(slug, reportFilePath);
        }
    }

    public string GetReportPathBasedOnType()
    {
        if (HttpContext.Current.Request.QueryString["report"] != null)
        {
            string slug = HttpContext.Current.Request.QueryString["report"];

            if (reportNames.ContainsKey(slug))
            {
                return reportNames[slug];
            }
        }

        return reportNames[defaultReportSlug];
    }

    public static string MapReportPath(string path)
    {
        if (Path.IsPathRooted(path))
        {
            return path;
        }
        else
        {
            return HostingEnvironment.MapPath(Path.Combine("~", path));
        }
    }
}