﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Reflection;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using System.Collections;

/// <summary>
/// Class that hydrates custom business objects with data. Please
/// note that this utility class can only be used on objects with "simple"
/// data types. If the object contains "complex" data types such as 
/// ArrayLists, HashTables, Custom Objects, etc... then the developer 
/// will need to write custom code for the hydration of these "complex" 
/// data types.
/// </summary>
public class ObjectBinder
{
    public static List<PropertyInfo> GetPropertyInfo(Type type)
    {
        // Use the cache because the reflection used later is expensive
        List<PropertyInfo> properties = System.Web.HttpContext.Current.Cache[type.FullName] as List<PropertyInfo>;
        if (properties == null)
        {
            properties = new List<PropertyInfo>(type.GetProperties());
            System.Web.HttpContext.Current.Cache.Add(type.FullName, properties,
                null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.Default, null);
        }
        return properties;
    }

    private static int[] GetOrdinals(List<PropertyInfo> properties, IDataReader dr)
    {
        int[] ordinals = new int[properties.Count];

        if (dr != null)
            for (int i = 0; i < properties.Count; i++)
            {
                ordinals[i] = -1;
                try
                {
                    ordinals[i] = dr.GetOrdinal(properties[i].Name);
                }
                catch
                {
                    // property does not exist in datareader
                }
            }
        return ordinals;
    }

    private static T CreateObject<T>(IDataReader dr, List<PropertyInfo> properties, int[] ordinals)
    {
        T t = Activator.CreateInstance<T>();

        // fill object with values from datareader
        for (int i = 0; i < properties.Count; i++)
        {
            #region Check Posiibility
            if (!properties[i].CanWrite)
                continue;
            object value = Null.SetNull(properties[i]);

            // see if the property exists in datareader 
            // or it is DBNull
            if (ordinals[i] == -1 || (dr.GetValue(ordinals[i]) is DBNull))
            {
                try
                {
                    properties[i].SetValue(t, value, null);
                }
                catch (TargetParameterCountException ex)
                {
                    // it is propabley not a Table Field ralted property
                    // it is a custom property like indexer, just ignore it
                }
                continue;
            }
            #endregion

            try
            {
                // try implicit conversion first
                properties[i].SetValue(t, dr.GetValue(ordinals[i]), null);
            }
            catch
            {
                // business object info class member data type does not match datareader member data type
                try
                {
                    // need to handle enumeration conversions differently than other base types
                    if (properties[i].PropertyType.BaseType.Equals(typeof(System.Enum)))
                    {
                        // check if value is numeric and if not convert to integer ( supports databases like Oracle )
                        if (IsNumeric(dr.GetValue(ordinals[i])))
                            properties[i].SetValue(t, System.Enum.ToObject(properties[i].PropertyType, Convert.ToInt32(dr.GetValue(ordinals[i]))), null);
                        else
                            properties[i].SetValue(t, System.Enum.ToObject(properties[i].PropertyType, dr.GetValue(ordinals[i])), null);
                    }
                    else
                        // try explicit conversion
                        properties[i].SetValue(t, Convert.ChangeType(dr.GetValue(ordinals[i]), properties[i].PropertyType), null);
                }
                catch
                {
                    properties[i].SetValue(t, Convert.ChangeType(dr.GetValue(ordinals[i]), properties[i].PropertyType), null);
                }
            }
        }

        return t;
    }
    private static bool IsNumeric(object obj)
    {
        double tmp;
        return double.TryParse(obj.ToString(), out tmp);
    }
    public static T FillObject<T>(IDataReader dr)
    {
        return FillObject<T>(dr, true);
    }

    public static T FillObject<T>(IDataReader dr, bool ManageDataReader)
    {
        T t = default(T);
        // get properties for type
        List<PropertyInfo> properties = GetPropertyInfo(typeof(T));

        // get ordinal positions in datareader
        int[] ordinals = GetOrdinals(properties, dr);

        if (ManageDataReader)
        {
            // read datareader                    
            if (dr.Read())
                // create custom business object
                t = CreateObject<T>(dr, properties, ordinals);
            // close datareader
            if (dr != null)
                dr.Close();
        }
        else
            // create custom business object
            t = CreateObject<T>(dr, properties, ordinals);

        return t;
    }

    public static List<T> FillCollection<T>(IDataReader dr)
    {
        List<T> res = new List<T>();
        T t;

        // get properties for type
        List<PropertyInfo> properties = GetPropertyInfo(typeof(T));

        // get ordinal positions in datareader
        int[] ordinals = GetOrdinals(properties, dr);

        // iterate datareader
        while (dr.Read())
        {
            // fill business object
            t = CreateObject<T>(dr, properties, ordinals);
            // add to collection
            res.Add(t);
        }

        // close datareader
        if (dr != null)
            dr.Close();

        return res;
    }

    public static IList<T> FillCollection<T>(IDataReader dr, IList<T> list)
    {
        T t;

        // get properties for type
        List<PropertyInfo> properties = GetPropertyInfo(typeof(T));

        // get ordinal positions in datareader
        int[] ordinals = GetOrdinals(properties, dr);

        // iterate datareader
        while (dr.Read())
        {
            // fill business object
            t = CreateObject<T>(dr, properties, ordinals);
            // add to collection
            list.Add(t);
        }

        // close datareader
        if (dr != null)
            dr.Close();

        return list;
    }

    public static T InitializeObject<T>(T t)
    {
        object value;

        // get properties for type
        List<PropertyInfo> properties = GetPropertyInfo(typeof(T));

        // initialize properties
        for (int i = 0; i < properties.Count; i++)
            if (properties[i].CanWrite)
            {
                value = Null.SetNull(properties[i]);
                properties[i].SetValue(t, value, null);
            }

        return t;
    }

    public static XmlDocument Serialize(Object obj)
    {
        XmlSerializer xmlSerializer = new XmlSerializer(obj.GetType());
        StringBuilder builder = new StringBuilder();
        TextWriter writer = new StringWriter(builder);

        xmlSerializer.Serialize(writer, obj);

        StringReader reader = new StringReader(writer.ToString());

        DataSet ds = new DataSet();
        ds.ReadXml(reader);

        XmlDocument xmlSerializedObject = new XmlDocument();
        xmlSerializedObject.LoadXml(ds.GetXml());

        return xmlSerializedObject;
    }

    public static T CloneObject<T>(T t) //Implements System.ICloneable.Clone
    {
        T clone = Activator.CreateInstance<T>();
        //clone = DotNetNuke.Framework.Reflection.CreateObject(objectToClone.GetType().AssemblyQualifiedName, ObjectToClone.GetType().AssemblyQualifiedName)

        List<PropertyInfo> properties = GetPropertyInfo(typeof(T));
        for (int i = 0; i < properties.Count; i++)
        {
            if (!properties[i].CanWrite)
                continue;

            Type iCloneType = properties[i].PropertyType.GetInterface("ICloneable", true);
            // ---------------------------------
            // Cloning
            if (iCloneType != null)
            {
                ICloneable iClone = properties[i].GetValue(t, null) as ICloneable;
                properties[i].SetValue(clone, iClone.Clone(), null);
            }
            else
                properties[i].SetValue(clone, properties[i].GetValue(t, null), null);

            // -----------------------------------
            // Enumerating
            Type iEnumerableType = properties[i].PropertyType.GetInterface("IEnumerable", true);
            if (iEnumerableType != null)
            {
                IEnumerable iEnum = properties[i].GetValue(t, null) as IEnumerable;

                Type iListType = properties[i].PropertyType.GetInterface("IList", true);
                Type iDicType = properties[i].PropertyType.GetInterface("IDictionary", true);
                int j = 0;

                if (iListType != null)
                {
                    #region Process IList
                    IList list = properties[i].GetValue(clone, null) as IList;
                    foreach (Object obj in iEnum)
                    {
                        iCloneType = obj.GetType().GetInterface("ICloneable", true);
                        if (iCloneType != null)
                            list[j] = (obj as ICloneable).Clone();
                        j++;
                    }
                    #endregion
                }
                else if (iDicType != null)
                {
                    #region Process IDictionary
                    IDictionary dic = properties[i].GetValue(clone, null) as IDictionary;
                    j = 0;

                    foreach (DictionaryEntry de in iEnum)
                    {
                        iCloneType = de.Value.GetType().GetInterface("ICloneable", true);
                        if (iCloneType != null)
                            dic[de.Key] = (de.Value as ICloneable).Clone();
                        j++;
                    }
                    #endregion
                }
            }
        }
        return clone;
    }

    internal static object GetPropertyValue<T>(T t, string propertyName)
    {
        propertyName = propertyName.ToLower();
        // Use the cache because the reflection used later is expensive
        List<PropertyInfo> properties = GetPropertyInfo(t.GetType());
        if (properties != null)
            for (int i = 0; i < properties.Count; i++)
                if (properties[i].Name.ToLower().Equals(propertyName))
                    return properties[i].GetValue(t, null);

        return null;
    }
}