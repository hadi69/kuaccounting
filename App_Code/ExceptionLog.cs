﻿using System;
using System.Collections.Generic;

using System.Web;

/// <summary>
/// Summary description for ExceptionLog
/// </summary>
public class ExceptionLog
{
    bool byPass = false;
    public static bool Pause = false;
    public static ExceptionLog Instance = new ExceptionLog();
    public ExceptionLog()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void Add(Exception ex)
    {
        if (Pause)
            return;
        // prevent loop
        if (byPass)
            return;
        byPass = true;
        try
        {
            string qry = string.Format("INSERT INTO Logs (Message, MoreInfo, StackTrace, LogDate) VALUES ({0}, {1}, {2}, {3})"
                , Helper.QoutedString(ex.Message), "NULL", Helper.QoutedString(ex.StackTrace), Helper.QoutedDateTime(DateTime.Now));
            DbProxy.Instance.RunQuery(qry);
        }
        catch (Exception exx)
        {
            string fck = exx.Message;
        }
        finally
        {
            byPass = false;
        }
    }

    public void Add(Exception ex, string moreInfo)
    {
        if (Pause)
            return;
        // prevent loop
        if (byPass)
            return;
        byPass = true;
        try
        {
            string qry = null;
            if (ex != null)
                qry = string.Format("INSERT INTO Logs (Message, MoreInfo, StackTrace, LogDate) VALUES ({0}, {1}, {2}, {3})"
                    , Helper.QoutedString(ex.Message), Helper.QoutedString(moreInfo), Helper.QoutedString(ex.StackTrace), Helper.QoutedDateTime(DateTime.Now));
            else
                qry = string.Format("INSERT INTO Logs (Message, MoreInfo, StackTrace, LogDate) VALUES ({0}, {1}, {2}, {3})"
                , Helper.QoutedString(ex.Message), Helper.QoutedString(moreInfo), Helper.QoutedString(ex.StackTrace), Helper.QoutedDateTime(DateTime.Now));
            DbProxy.Instance.RunQuery(qry);
        }
        catch (Exception exx)
        {
            string fck = exx.Message;
        }
        finally
        {
            byPass = false;
        }
    }
}