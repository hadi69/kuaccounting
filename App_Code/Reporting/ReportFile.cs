﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Xml;
using System.Data;

/// <summary>
/// Used for save/load of a report in Xml format
/// </summary>
public class ReportFile
{
    #region Fields
    /// <summary>
    /// List of tables
    /// </summary>
    private List<TableInfo> mTables = new List<TableInfo>();
    /// <summary>
    /// List of fields
    /// </summary>
    private List<FieldInfo> mFields = new List<FieldInfo>();
    /// <summary>
    /// List of filters
    /// </summary>
    private List<Filter> mFilters = new List<Filter>();
    /// <summary>
    /// List of aggregateFields (ex. sum, avg)
    /// </summary>
    private List<AggregateField> mAggregateFields = new List<AggregateField>();
    /// <summary>
    /// List of curveItems (items in graph)
    /// </summary>
    private List<CurveItem> mCurveItems = new List<CurveItem>();
    /// <summary>
    /// FilterType ("AND", "OR")
    /// </summary>
    private string mFilterType = "AND";
    /// <summary>
    /// Report ID
    /// </summary>
    private int mID;
    private string mQuery = "";

    /// <summary>
    /// Report Name
    /// </summary>
    private string mName;
    #endregion

    #region Props
    public List<TableInfo> Tables
    {
        get { return mTables; }
        //set { mTables = value; }
    }

    public List<FieldInfo> Fields
    {
        get { return mFields; }
        //set { mFields = value; }
    }

    public List<Filter> Filters
    {
        get { return mFilters; }
        //set { mFilters = value; }
    }

    public List<AggregateField> AggregateFields
    {
        get { return mAggregateFields; }
        //set { mAggregateFields = value; }
    }
    public List<CurveItem> CurveItems
    {
        get { return mCurveItems; }
        set { mCurveItems = value; }
    }
    public string FilterType
    {
        get { return mFilterType; }
        set { mFilterType = value; }
    }
    public string Query
    {
        get { return mQuery; }
        set { mQuery = value; }
    }
    #endregion

    public ReportFile()
    {
    }
    public ReportFile(DataRow dr)
    {
        mID = Helper.GetInt(dr, "ID", -1);
        mName = Helper.GetString(dr, "Name", "");
        mQuery = Helper.GetString(dr, "Query", "");
        string xml = Helper.GetString(dr, "XmlDoc", "");
        if (!string.IsNullOrEmpty(xml))
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xml);
            LoadFromXml(xmlDoc);
        }
    }
    /// <summary>
    /// Load from a Xml file
    /// </summary>
    /// <param name="filePath"></param>
    /// <returns></returns>
    public bool Load(string filePath)
    {
        try
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(filePath);
            LoadFromXml(xmlDoc);
        }
        catch (Exception ex)
        {

        }
        return true;
    }
    /// <summary>
    /// Save to a Xml file
    /// </summary>
    /// <param name="filePath"></param>
    /// <returns></returns>
    public bool Save(string filePath)
    {
        XmlDocument xmlDoc = SaveToXml();

        return true;
    }
    /// <summary>
    /// Load from a XmlDocument
    /// </summary>
    /// <param name="xmlDoc"></param>
    private void LoadFromXml(XmlDocument xmlDoc)
    {
        XmlNode root = xmlDoc.DocumentElement;

        mName = XmlHelper.GetString(root, "Name", "");
        mFilterType = XmlHelper.GetString(root, "FilterType", "AND");
        mQuery = XmlHelper.GetString(root, "Query", "");

        // ---------------------------------
        // Tables
        XmlNode tables = root.SelectSingleNode("Tables");
        mTables = new List<TableInfo>();
        if (tables != null)
            for (int i = 0; i < tables.ChildNodes.Count; i++)
            {
                string tableName = tables.ChildNodes[i].InnerText;
                TableInfo table = TableController.Instance[tableName];
                if (table != null)
                    mTables.Add(table);
            }

        // ---------------------------------
        // Fields: Unit.ID, Patient.Name
        XmlNode fields = root.SelectSingleNode("Fields");
        mFields = new List<FieldInfo>();
        if (fields != null)
            for (int i = 0; i < fields.ChildNodes.Count; i++)
            {
                string fullName = fields.ChildNodes[i].InnerText;
                FieldInfo field = TableController.FindField(fullName);
                if (field != null)
                    mFields.Add(field);
            }

        // ---------------------------------
        // Filters
        XmlNode filters = root.SelectSingleNode("Filters");
        mFilters = new List<Filter>();
        if (filters != null)
            for (int i = 0; i < filters.ChildNodes.Count; i++)
            {
                Filter filter = new Filter(filters.ChildNodes[i]);
                filter.Field = TableController.FindField(filter.FullName);
                if (filter.Field != null)
                    mFilters.Add(filter);
            }

        // ---------------------------------
        // AggregateFields
        XmlNode aggregateFields = root.SelectSingleNode("AggregateFields");
        mAggregateFields = new List<AggregateField>();
        if (aggregateFields != null)
            for (int i = 0; i < aggregateFields.ChildNodes.Count; i++)
            {
                AggregateField agg = new AggregateField(aggregateFields.ChildNodes[i]);
                mAggregateFields.Add(agg);
            }

        // ---------------------------------
        // mCurveItems
        XmlNode curveItems = root.SelectSingleNode("CurveItems");
        mCurveItems = new List<CurveItem>();
        if (curveItems != null)
            for (int i = 0; i < curveItems.ChildNodes.Count; i++)
            {
                CurveItem ci = new CurveItem(curveItems.ChildNodes[i]);
                mCurveItems.Add(ci);
            }
    }
    /// <summary>
    /// Save to a XmlDocument
    /// </summary>
    /// <returns></returns>
    public XmlDocument SaveToXml()
    {
        XmlDocument xmlDoc = XmlHelper.GetXmlDocWithElement("ReportFile", "1");
        XmlNode root = xmlDoc.DocumentElement;
        root.Attributes.SetNamedItem(XmlHelper.CreateXmlAttr(xmlDoc, "Name", mName));
        root.Attributes.SetNamedItem(XmlHelper.CreateXmlAttr(xmlDoc, "FilterType", mFilterType));
        root.Attributes.SetNamedItem(XmlHelper.CreateXmlAttr(xmlDoc, "Query", mQuery));

        // ---------------------------------
        // Tables
        XmlNode tables = xmlDoc.CreateNode(XmlNodeType.Element, "Tables", "");
        if (null != mTables)
            for (int i = 0; i < mTables.Count; i++)
            {
                XmlNode table = xmlDoc.CreateNode(XmlNodeType.Element, "Table", "");
                table.InnerText = mTables[i].Name;
                tables.AppendChild(table);
            }
        root.AppendChild(tables);

        // ---------------------------------
        // Fields
        XmlNode fields = xmlDoc.CreateNode(XmlNodeType.Element, "Fields", "");
        if (null != mFields)
            for (int i = 0; i < mFields.Count; i++)
            {
                XmlNode field = xmlDoc.CreateNode(XmlNodeType.Element, "Field", "");
                field.InnerText = mFields[i].FullName;
                fields.AppendChild(field);
            }
        root.AppendChild(fields);

        // ---------------------------------
        // Fields
        XmlNode filters = xmlDoc.CreateNode(XmlNodeType.Element, "Filters", "");
        if (null != mFilters)
            for (int i = 0; i < mFilters.Count; i++)
            {
                XmlNode filter = mFilters[i].GetData(xmlDoc);
                filters.AppendChild(filter);
            }
        root.AppendChild(filters);

        // ---------------------------------
        // AggregateFields
        XmlNode aggregateFields = xmlDoc.CreateNode(XmlNodeType.Element, "AggregateFields", "");
        if (null != mAggregateFields)
            for (int i = 0; i < mAggregateFields.Count; i++)
            {
                XmlNode agg = mAggregateFields[i].GetData(xmlDoc);
                aggregateFields.AppendChild(agg);
            }
        root.AppendChild(aggregateFields);

        // ---------------------------------
        // CurveItems
        XmlNode curveItems = xmlDoc.CreateNode(XmlNodeType.Element, "CurveItems", "");
        if (null != mCurveItems)
            for (int i = 0; i < mCurveItems.Count; i++)
            {
                XmlNode ci = mCurveItems[i].GetData(xmlDoc);
                curveItems.AppendChild(ci);
            }
        root.AppendChild(curveItems);
        return xmlDoc;
    }

    /// <summary>
    /// Report Name
    /// </summary>
    public string Name
    {
        get { return mName; }
        set { mName = value; }
    }
    /// <summary>
    /// Report ID
    /// </summary>
    public int ID
    {
        get { return mID; }
        set { mID = value; }
    }
    /// <summary>
    /// Report settings in Xml format
    /// </summary>
    public string XmlData
    {
        get
        {
            try
            {
                XmlDocument xmlDoc = SaveToXml();
                return xmlDoc.InnerXml;
            }
            catch
            {
            }
            return null;
        }
        set
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(value);
                LoadFromXml(xmlDoc);
            }
            catch { }
        }
    }

    private string filterCnj = "AND";

    public string FilterCnj
    {
        get { return filterCnj; }
        set { filterCnj = value; }
    }
}