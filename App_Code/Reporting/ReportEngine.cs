﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Text;

/// <summary>
/// Summary description for ReportEngine
/// </summary>
public class ReportEngine
{
    ReportFile mFile = null;
    public ReportEngine(ReportFile file)
    {
        mFile = file;
    }
    public static List<TableInfo> FindRelatedTables(List<TableInfo> tables)
    {
        /*
           <table Name="ServiceItems">
            <Fields>
              <f n="ID" nl="False" tp="int" identity="true" l="false" />
              <f n="ServiceID"  tp="int" foriegn="Service" />
              <f n="ItemEnumTable"  tp="nvarchar(200)" />
              <f n="ServiceUnitID"  tp="int" foriegn="Units" />
            </Fields>
          </table>
         */
        List<TableInfo> res = new List<TableInfo>();
        foreach (TableInfo table in tables)
            foreach (FieldInfo field in table.Fields)
            {
                if (!string.IsNullOrEmpty(field.Foriegn))
                {
                    TableInfo info = TableController.Instance[field.Foriegn];
                    if (info != null && !res.Contains(info) && !tables.Contains(info))
                        res.Add(info);
                }
            }
        return res;
    }
    /// <summary>
    /// find a related table in a list of tables, the relation may be right-to-left or vice versa
    /// </summary>
    /// <param name="tables"></param>
    /// <param name="table"></param>
    /// <param name="relation"></param>
    /// <returns></returns>
    private string FindRelation(TableInfo table)
    {
        /*
           <table Name="ServiceItems">
            <Fields>
              <f n="ID" nl="False" tp="int" identity="true" l="false" />
              <f n="ServiceID"  tp="int" foriegn="Service" />
              <f n="ItemEnumTable"  tp="nvarchar(200)" />
              <f n="ServiceUnitID"  tp="int" foriegn="Units" />
            </Fields>
          </table>
         */
        for (int i = 0; i < mFile.Tables.Count; i++)
            for (int j = 0; j < mFile.Tables[i].Fields.Count; j++)
            {
                if (string.Equals(mFile.Tables[i].Fields[j].Foriegn, table.Name, StringComparison.InvariantCultureIgnoreCase))
                {
                    string relation = string.Format("{0}={1}.ID", mFile.Tables[i].Fields[j].FullName, table.Name);
                    return relation;
                }
            }
        return null;
    }
    /// <summary>
    /// get report using specified tables, fields and filters
    /// </summary>
    /// <param name="tables"></param>
    /// <param name="fields"></param>
    /// <param name="aggregateFields"></param>
    /// <param name="filters"></param>
    /// <param name="filterCnj"></param>
    /// <returns></returns>
    public DataTable GetData()
    {
        string query = CreateQuery();
        if (string.IsNullOrEmpty(query))
            return null;
        return DbProxy.Instance.FillData(query, "Results");
        //  return Data.DataProviderHelper.Instance.ExecuteReader(query);
    }
    /// <summary>
    /// create query to get the report using specified tables, fields and filters
    /// </summary>
    /// <param name="tables"></param>
    /// <param name="fields"></param>
    /// <param name="aggregateFields"></param>
    /// <param name="filters"></param>
    /// <param name="filterCnj"></param>
    /// <returns></returns>
    public string CreateQuery()
    {
        StringBuilder fromSection = new StringBuilder();
        StringBuilder groupBySection = new StringBuilder();
        StringBuilder joinWhere = new StringBuilder();

        //List<TableInfo> usedTables = new List<TableInfo>();
        //List<TableInfo> detectedTables = new List<TableInfo>();
        //usedTables.AddRange(mFile.Tables);

        #region Build From Section
        fromSection.AppendFormat("{0} ", mFile.Tables[0].Name);
        for (int i = 1; i < mFile.Tables.Count; i++)
        {
            TableInfo table = mFile.Tables[i];
            string relation = FindRelation(table);
            if (relation == null)
                return null;
            fromSection.AppendFormat("\r\n INNER JOIN {0} ON {1}", table.Name, relation);

            // todo: do this if we wanted more control on the join
            //if (!string.IsNullOrEmpty(relation.Where))
            //    if (joinWhere.Length > 0)
            //        joinWhere.AppendFormat(" AND {0} ", relation.Where);
            //    else
            //        joinWhere.Append(relation.Where);
        }
        #endregion

        // Build Fields Section
        #region Fields
        StringBuilder fieldsBuilder = new StringBuilder();
        for (int i = 0; i < mFile.Fields.Count; i++)
        {
            fieldsBuilder.AppendFormat(", {0} AS [{1}]", mFile.Fields[i].FullName, mFile.Fields[i].FullTitle);
            if (mFile.AggregateFields != null && mFile.AggregateFields.Count > 0)
                groupBySection.AppendFormat(", {0}", mFile.Fields[i].FullName);
        }
        if (groupBySection.Length > 0)
        {
            // remove starting ", "
            groupBySection.Remove(0, 2);
            groupBySection.Insert(0, "GROUP BY ");
        }
        #endregion

        // Build AggregateFields Section
        #region AggregateFields
        if (mFile.AggregateFields != null && mFile.AggregateFields.Count > 0)
            for (int i = 0; i < mFile.AggregateFields.Count; i++)
            {
                AggregateField ag = mFile.AggregateFields[i];
                FieldInfo field = TableController.FindField(ag.FullName);
                string fieldFullName = field.FullName;
                fieldsBuilder.AppendFormat(", {0} AS [{1}]", AggregateField.FormatField(fieldFullName, ag.Type), ag.Title);
            }
        #endregion
        // remove starting ", "
        fieldsBuilder.Remove(0, 2);

        // Build WHERE Section
        StringBuilder whereBuilder = new StringBuilder();
        #region WhereSection
        if (mFile.Filters != null && mFile. Filters.Count > 0)
        {
            whereBuilder.AppendFormat("WHERE {0}", GetCondition(mFile.Filters[0]));
            for (int i = 1; i < mFile.Filters.Count; i++)
                whereBuilder.AppendFormat(" {0} {1}", mFile.FilterCnj, GetCondition(mFile.Filters[i]));

            //// there may be some filters that are not used in fields and need to refer to another table
            //for (int i = 0; i < filters.Count; i++)
            //    if (!string.IsNullOrEmpty(filters[i].Field.BindField))
            //    {
            //        Table other = FindTable(filters[i].Field.BindTable);
            //        if (other == null)
            //            other = new Table(filters[i].Field.BindTable);
            //        if (!usedTables.Contains(other))
            //        {
            //            usedTables.Add(other);
            //            fromSection.AppendFormat(" LEFT JOIN {0} ON {1}", filters[i].Field.BindTable, filters[i].Field.Binding);
            //        }
            //    }
        }
        #endregion
        if (joinWhere.Length > 0)
            if (whereBuilder.Length > 0)
                whereBuilder.AppendFormat(" AND {0} ", joinWhere.ToString());
            else
                whereBuilder.AppendFormat("WHERE {0}", joinWhere.ToString());

        return string.Format("SELECT {0} \r\nFROM {1} {2} {3}", fieldsBuilder.ToString(), fromSection.ToString(), whereBuilder.ToString(), groupBySection.ToString());
    }

    /// <summary>
    /// Create where condition out of a filter
    /// </summary>
    /// <param name="filter"></param>
    /// <returns></returns>
    private string GetCondition(Filter filter)
    {
        FieldInfo field = TableController.FindField(filter.FullName);
        if (filter.CreateFilterQuery != null)
            return filter.CreateFilterQuery(filter.FilterValue);

        switch (GetFieldType( field.Type))
        {
            case FieldType.Int:
                return string.Format("{0}{1}{2}", filter.FullName, filter.Operation, filter.FilterValue);
            case FieldType.String:
                if (filter.Operation.ToLower().Equals("like"))
                    return string.Format("{0} LIKE N'%{2}%'", filter.FullName, filter.Operation, filter.FilterValue);
                else
                    return string.Format("{0}{1}N'{2}'", filter.FullName, filter.Operation, filter.FilterValue);
            case FieldType.Date:
                DateTime date = Helper.ParseDate(filter.FilterValue, Helper.NullDate);
                if (Helper.NullDate == date)
                    return "";
                return string.Format("{0} {1} '{2}'", filter.FullName, filter.Operation, date.ToShortDateString());
            case FieldType.Bool:
                return string.Format("{0}{1}{2}", filter.FullName, filter.Operation, filter.FilterValue);
        }
        throw new Exception("FieldType not supported");
        return null;
    }
    static FieldType GetFieldType(string dbFieldType)
    {
        if (string.IsNullOrEmpty(dbFieldType))
            return FieldType.None;
        if (dbFieldType == "int")
            return FieldType.Int;
        if (dbFieldType == "bit")
            return FieldType.Bool;
        if (dbFieldType == "datetime")
            return FieldType.Date;
        if (dbFieldType.StartsWith("nvarchar") || dbFieldType.StartsWith("varchar") 
            || dbFieldType == "ntext" || dbFieldType == "text")
            return FieldType.String;
        return FieldType.None;
    }
    ///// <summary>
    ///// get data from biniding table for a field.
    ///// ex:
    /////     Product.UintID : the Unit should be read from another table Unit
    ///// </summary>
    ///// <param name="field"></param>
    ///// <returns></returns>
    //public List<BindData> GetBindData(Field field)
    //{
    //    if (field == null || string.IsNullOrEmpty(field.BindTable) || string.IsNullOrEmpty(field.BindField))
    //        return null;
    //    string query = string.Format("SELECT ID, {0} AS Name FROM {1}", field.BindField, field.BindTable);
    //    IDataReader dr = Data.DataProviderHelper.Instance.ExecuteReader(query);
    //    return ObjectBinder.FillCollection<BindData>(dr);
    //}
}