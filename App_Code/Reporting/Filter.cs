﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Xml;

public class Filter
{
    #region Fields
    /// <summary>
    /// FullName of the related field
    /// </summary>
    private string mFullName;

    /// <summary>
    /// Filter operation (ex. =, >,...0
    /// </summary>
    private string mOperation;
    /// <summary>
    /// Filter text for strings
    /// </summary>
    private string mFilterText;
    /// <summary>
    /// filter value
    /// </summary>
    private string mFilterValue;
    #endregion
    /// <summary>
    /// Related field
    /// </summary>
    public FieldInfo Field = null;

    public Filter()
    {
    }
    public Filter(XmlNode xNode)
    {
        mFullName = XmlHelper.GetString(xNode, "FullName", "");
        mOperation = XmlHelper.GetString(xNode, "Operation", "");
        mFilterText = XmlHelper.GetString(xNode, "FilterText", "");
        mFilterValue = XmlHelper.GetString(xNode, "FilterValue", "");
    }
    /// <summary>
    /// Save settings in Xml format
    /// </summary>
    /// <param name="xmlDoc"></param>
    /// <returns></returns>
    public XmlNode GetData(XmlDocument xmlDoc)
    {
        XmlNode node = xmlDoc.CreateNode(XmlNodeType.Element, "Filter", "");
        node.Attributes.SetNamedItem(XmlHelper.CreateXmlAttr(xmlDoc, "FullName", mFullName));
        node.Attributes.SetNamedItem(XmlHelper.CreateXmlAttr(xmlDoc, "Operation", mOperation));
        node.Attributes.SetNamedItem(XmlHelper.CreateXmlAttr(xmlDoc, "FilterText", mFilterText));
        node.Attributes.SetNamedItem(XmlHelper.CreateXmlAttr(xmlDoc, "FilterValue", mFilterValue));

        return node;
    }

    #region Props
    /// <summary>
    /// FullName of the related field
    /// </summary>
    public string FullName
    {
        get { return mFullName; }
        set { mFullName = value; }
    }
    /// <summary>
    /// Filter operation (ex. =, >,...0
    /// </summary>
    public string Operation
    {
        get { return mOperation; }
        set { mOperation = value; }
    }
    /// <summary>
    /// Filter value
    /// </summary>
    public string FilterValue
    {
        get { return mFilterValue; }
        set { mFilterValue = value; }
    }
    /// <summary>
    /// Filter text for strings
    /// </summary>
    //public string FilterText
    //{
    //    get
    //    {
    //        return mFilterText;
    //    }
    //    set { mFilterText = value; }
    //}
    #endregion

    public CreateFilterQuery CreateFilterQuery;
}