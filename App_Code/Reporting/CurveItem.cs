﻿using System.Xml;
public class CurveItem
{
    #region Fields
    /// <summary>
    /// Title (ie. farsi name)
    /// </summary>
    private string mTitle;
    /// <summary>
    /// Curve Type (ex: line, bar, ...)
    /// </summary>
    private string mCurveType;
    /// <summary>
    /// Related Field
    /// </summary>
    private string mField;
    #endregion

    /// <summary>
    /// Default constructor
    /// </summary>
    public CurveItem()
    {
    }
    /// <summary>
    /// Constructor from an XmlNode
    /// </summary>
    /// <param name="xNode"></param>
    public CurveItem(XmlNode xNode)
    {
        mTitle = XmlHelper.GetString(xNode, "Title", "");
        mCurveType = XmlHelper.GetString(xNode, "CurveType", "");
        mField = XmlHelper.GetString(xNode, "Field", "");
    }
    /// <summary>
    /// Save settings to an XmlDocument
    /// </summary>
    /// <param name="xmlDoc"></param>
    /// <returns></returns>
    public XmlNode GetData(XmlDocument xmlDoc)
    {
        XmlNode node = xmlDoc.CreateNode(XmlNodeType.Element, "CurveItem", "");
        node.Attributes.SetNamedItem(XmlHelper.CreateXmlAttr(xmlDoc, "Title", mTitle));
        node.Attributes.SetNamedItem(XmlHelper.CreateXmlAttr(xmlDoc, "CurveType", mCurveType));
        node.Attributes.SetNamedItem(XmlHelper.CreateXmlAttr(xmlDoc, "Field", mField));

        return node;
    }

    #region Props
    /// <summary>
    /// Title (ie. farsi name)
    /// </summary>
    public string Title
    {
        get { return mTitle; }
        set { mTitle = value; }
    }
    /// <summary>
    /// Curve Type (ex: line, bar, ...)
    /// </summary>
    public string CurveType
    {
        get { return mCurveType; }
        set { mCurveType = value; }
    }
    /// <summary>
    /// Related Field
    /// </summary>
    public string Field
    {
        get { return mField; }
        set { mField = value; }
    }

    #endregion
}