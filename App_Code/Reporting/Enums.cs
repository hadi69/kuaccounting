﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// List of Aggregate Types
/// </summary>
public enum AggregateType
{
    Count = 0,
    Sum = 1,
    Avg = 2,
    Max = 3,
    Min = 4
}
public enum FieldType
{
    None,
    Int,
    String,
    Date,
    Bool
}
public delegate string CreateFilterQuery(string filterValue);
