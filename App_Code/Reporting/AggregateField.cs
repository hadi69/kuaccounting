﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Xml;

/// <summary>
/// An AggregateField consist of a Field and an Aggregate command like SUM
/// </summary>
public class AggregateField
{
    #region fields
    /// <summary>
    /// Title (ie. farsi name)
    /// </summary>
    private string mTitle;
    /// <summary>
    /// Fullname of related Field
    /// </summary>
    private string mFullName;

    /// <summary>
    /// AggregareType like COUNT
    /// </summary>
    private AggregateType mType;
    #endregion

    /// <summary>
    /// Default constructor
    /// </summary>
    public AggregateField()
    {
    }
    /// <summary>
    /// Constructor from an XmlNode
    /// </summary>
    /// <param name="xNode"></param>
    public AggregateField(XmlNode xNode)
    {
        mFullName = XmlHelper.GetString(xNode, "FullName", "");
        mTitle = XmlHelper.GetString(xNode, "Title", "");
        mType = XmlHelper.GetEnum<AggregateType>(xNode, "Type", AggregateType.Count);
    }
    /// <summary>
    /// Save Settings to an XmlNode
    /// </summary>
    /// <param name="xmlDoc"></param>
    /// <returns></returns>
    public XmlNode GetData(XmlDocument xmlDoc)
    {
        XmlNode node = xmlDoc.CreateNode(XmlNodeType.Element, "Filter", "");
        node.Attributes.SetNamedItem(XmlHelper.CreateXmlAttr(xmlDoc, "FullName", mFullName));
        node.Attributes.SetNamedItem(XmlHelper.CreateXmlAttr(xmlDoc, "Title", mTitle));
        node.Attributes.SetNamedItem(XmlHelper.CreateXmlAttr(xmlDoc, "Type", mType));

        return node;
    }
    /// <summary>
    /// format aggregate command and fielname. ex. COUNT(ID)
    /// </summary>
    /// <param name="fieldName"></param>
    /// <param name="type"></param>
    /// <returns></returns>
    public static string FormatField(string fieldName, AggregateType type)
    {
        switch (type)
        {
            case AggregateType.Count:
                return string.Format("COUNT({0})", fieldName);
            case AggregateType.Sum:
                return string.Format("SUM({0})", fieldName);
            case AggregateType.Avg:
                return string.Format("AVG({0})", fieldName);
            case AggregateType.Max:
                return string.Format("MAX({0})", fieldName);
            case AggregateType.Min:
                return string.Format("MIN({0})", fieldName);
            default:
                throw new Exception(string.Format("AggregateType.{0} not supported", type));
                break;
        }
        return "";
    }

    #region Props
    /// <summary>
    /// Title (ie. farsi name)
    /// </summary>
    public string Title
    {
        get { return mTitle; }
        set { mTitle = value; }
    }
    /// <summary>
    /// AggregareType like COUNT
    /// </summary>
    public AggregateType Type
    {
        get { return mType; }
        set { mType = value; }
    }
    /// <summary>
    /// Fullname of related Field
    /// </summary>
    public string FullName
    {
        get { return mFullName; }
        set { mFullName = value; }
    }
    #endregion
}