﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for LangProvider
/// </summary>
public class LangProvider
{
    private static DateTime lastModifiedDate;
    private static string path = System.Web.HttpContext.Current.Server.MapPath("~/js/lang/fa.js");
    public static LangProvider Instance = new LangProvider();

    private static bool NewerVersionAvailable()
    {
        DateTime currentModifiedDate = System.IO.File.GetLastWriteTime(path);
        return currentModifiedDate > lastModifiedDate;
    }

    public static Dictionary<string, string> LoadDict(string lang)
    {
        // Load default lang
        Dictionary<string, string> res = new Dictionary<string, string>();
        lastModifiedDate = System.IO.File.GetLastWriteTime(path);
        string[] lines = System.IO.File.ReadAllLines(System.Web.HttpContext.Current.Server.MapPath("~/js/lang/fa.js"));
        for (int i = 0; i < lines.Length; i++)
        {
            string line = lines[i].Trim().TrimEnd(',');
            if (line.StartsWith("//"))
                continue;
            if (line.IndexOf(':') <= 0)
                continue;
            string[] keyVal = line.Split(':');
            string key = keyVal[0].Trim();
            string val = keyVal[1].Trim().Replace("'", "");
            if (!res.ContainsKey(key))
                res.Add(key, val);
        }

        // load translations from requested language
        List<string> keys = new List<string>();
        if (!string.IsNullOrEmpty(lang) && lang != "fa")
        {
            path = System.Web.HttpContext.Current.Server.MapPath("~/js/lang/" + lang + ".js");
            lines = System.IO.File.ReadAllLines(path);
            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i].Trim().TrimEnd(',');
                if (line.StartsWith("//"))
                    continue;
                if (line.IndexOf(':') <= 0)
                    continue;
                string[] keyVal = line.Split(':');
                string key = keyVal[0].Trim();
                string val = keyVal[1].Trim().Replace("'", "");
                if (!res.ContainsKey(key))
                    res.Add(key, val);
                else
                    res[key] = val;
                keys.Add(key);
            }
        }
        // check for missing keys in new language
        if (keys.Count > 0 && keys.Count != res.Count)
        {
            System.Text.StringBuilder nwe = new System.Text.StringBuilder();
            Dictionary<string, string>.Enumerator enu = res.GetEnumerator();
            while (enu.MoveNext())
                if (!keys.Contains(enu.Current.Key))
                    nwe.AppendLine(string.Format("{0}: '{1}', ", enu.Current.Key, enu.Current.Value));
            if (nwe.Length > 0)
            {
                path = System.Web.HttpContext.Current.Server.MapPath("~/js/lang/" + lang + ".js");
                string content = System.IO.File.ReadAllText(path);
                int start = content.IndexOf("KEYS_START");
                if (start > 0)
                {
                    start = content.IndexOf('\n', start) + 1;
                    string part1 = content.Substring(0, start);
                    string part2 = content.Substring(start);

                    content = part1 + nwe.ToString() + part2;
                    System.IO.File.WriteAllText(path, content);
                }
            }
        }
        return res;
    }
    
    Dictionary<string, string> Dict
    {
        get
        {
            if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Application == null)
                return null;

            // The languages are stored in Application and are shared among all users
            //  LangProvider_Dict_fa
            //  LangProvider_Dict_ku
            string key = "LangProvider_Dict_" + SiteSettings.Lang;
            Dictionary<string, string> res = SiteSettings.GetApplication(key) as Dictionary<string, string>;
            if (res == null || NewerVersionAvailable())
                SiteSettings.SetApplication(key, res = LoadDict(SiteSettings.Lang));
            return res;
        }
    }
    public string this[string key]
    {
        get
        {
            Dictionary<string, string> dict = this.Dict;
            if (dict == null || !dict.ContainsKey(key))
                return key;
            return dict[key];
        }
        set
        {
            Dictionary<string, string> dict = this.Dict;
            if (dict == null || !dict.ContainsKey(key))
                return;
            if (value == dict[key])
                return;

            dict[key] = value;
            // save changes
            string path = System.Web.HttpContext.Current.Server.MapPath("~/js/lang/" + SiteSettings.Lang + ".js");
            string[] lines = System.IO.File.ReadAllLines(path);
            
            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i].Trim().TrimEnd(',');
                if (line.StartsWith("//"))
                    continue;
                if (line.IndexOf(':') <= 0)
                    continue;
                string[] keyVal = line.Split(':');
                if (key == keyVal[0].Trim())
                {
                    lines[i] = key + ": '" + value + "',";
                    break;
                }
            }
            System.IO.File.WriteAllLines(path, lines);
        }
    }
    public void Update(string lang, string key, string value)
    {
        Dictionary<string, string> dict;
        if (lang == SiteSettings.Lang)
            dict = this.Dict;
        else
            dict = LoadDict(lang);
        if (dict == null || !dict.ContainsKey(key))
            return;
        if (value == dict[key])
            return;
        dict[key] = value;
        string path = System.Web.HttpContext.Current.Server.MapPath("~/js/lang/" + lang + ".js");
        string[] lines = System.IO.File.ReadAllLines(path);

        for (int i = 0; i < lines.Length; i++)
        {
            string line = lines[i].Trim().TrimEnd(',');
            if (line.StartsWith("//"))
                continue;
            if (line.IndexOf(':') <= 0)
                continue;
            string[] keyVal = line.Split(':');
            if (key == keyVal[0].Trim())
            {
                lines[i] = key + ": '" + value + "',";
                break;
            }
        }
        System.IO.File.WriteAllLines(path, lines);
    }
    /// <summary>
    /// Parses a string (mostly html content) and replaces any string starting with Lang. to its translation
    /// ex: Lang.MBasics will be replaced اطلاعات پایه or سەرەکی
    /// </summary>
    /// <param name="content"></param>
    /// <returns></returns>
    public string ParseLang(string content)
    {
        if (!content.Contains("Lang."))
            return content;

        // do not change anything in javascript sections --> <script ....>....</script>
        // save the scripts
        Dictionary<string, string> scripts = new Dictionary<string, string>();
        int index = -1;
        while ((index + 1 < content.Length) && (index = content.IndexOf("<script", index + 1)) >= 0)
        {
            int last = content.IndexOf("</script>");
            string script = content.Substring(index, last + "</script>".Length - index);
            string key = "__SCRiPt__" + scripts.Count;
            scripts.Add(key, script);
            content = content.Replace(script, key);
        }
        index = -1;
        while ((index + 1 < content.Length) && (index = content.IndexOf("Lang.", index + 1)) >= 0)
        {
            int last = content.IndexOfAny(new char[] { ' ', '"', '\'', '<', '&', '\r', '\n' }, index + 1);
            string field = last > 0 ? content.Substring(index + 5, last - index - 5) : content.Substring(index + 5);
            string val = this[field];
            field = "Lang." + field;
            content = content.Replace(field + "'", val + "'").Replace(field + "\"", val + "\"").Replace(field + " ", val + " ").Replace(field + ",", val + ",").Replace(field + "\r", val + "\r")
                .Replace(field + "\n", val + "\n")
                .Replace(field + "<", val + "<").Replace(field + "(", val + "(").Replace(field + ")", val + ")").Replace(field + ";", val + ";");
            if (content.EndsWith(field))
                content = content.Replace(field, val);
        }

        // restore scripts
        Dictionary<string, string>.Enumerator enu = scripts.GetEnumerator();
        while (enu.MoveNext())
            content = content.Replace(enu.Current.Key, enu.Current.Value);
        return content;
    }
    public static string[] GetLangs()
    {
        // return all *.js files in folder ~/js/lang
        string path = System.Web.HttpContext.Current.Server.MapPath("~/js/lang");
        string[] files = System.IO.Directory.GetFiles(path, "*.js");
        for (int i = 0; i < files.Length; i++)
            files[i] = System.IO.Path.GetFileNameWithoutExtension(files[i]);
        return files;
    }
}