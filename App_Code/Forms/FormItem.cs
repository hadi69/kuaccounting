﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;

public class FormItem
{
    private int mID, mFormDeclareItemID;
    private decimal mNumericVal;
    private string mTextVal, mXmlVal;
    private DateTime mDateVal;
    public FormItem()
    {
        mNumericVal = 0;
        mTextVal = null;
        mXmlVal = null;
        mDateVal = Helper.NullDate;
    }
    public FormItem(DataRow row, FormDeclareItem info)
    {
        this.FormDeclareItem = info;
        mID = Helper.GetInt(row, "ID", -1);
        mFormDeclareItemID = Helper.GetInt(row, "FormDeclareItemID", -1);
        mNumericVal = Helper.GetDecimal(row, "NumericVal", 0);
        mTextVal = Helper.GetString(row, "TextVal", "");
        mXmlVal = Helper.GetString(row, "XmlVal", "");
        mDateVal = Helper.GetDateTime(row, "DateVal", Helper.NullDate);
    }

    public int ID { get { return mID; } set { mID = value; } }
    public int FormDeclareItemID { get { return mFormDeclareItemID; } set { mFormDeclareItemID = value; } }
    public decimal NumericVal { get { return mNumericVal; } set { mNumericVal = value; } }
    public string TextVal { get { return mTextVal; } set { mTextVal = value; } }
    public string XmlVal { get { return mXmlVal; } set { mXmlVal = value; } }
    public DateTime DateVal { get { return mDateVal; } set { mDateVal = value; } }
    public FormDeclareItem FormDeclareItem;
    public string Value
    {
        get
        {
            switch (this.FormDeclareItem.ItemType)
            {
                case ItemType.Numeric:
                case ItemType.Enum:
                case ItemType.Bool:
                case ItemType.Radio:
                    return mNumericVal.ToString();
                case ItemType.Report:
                case ItemType.Table:
                case ItemType.Form:
                    switch (this.FormDeclareItem.DataFieldType)
                    {
                        case ItemType.Numeric:
                            return ((int)mNumericVal).ToString();
                        case ItemType.Text:
                            return mTextVal;
                        case ItemType.Date:
                            return Helper.FormatDate(mDateVal, null);
                        case ItemType.Bool:
                            return mNumericVal.ToString();
                    }
                    return mNumericVal.ToString();
                case ItemType.Text:
                    return mTextVal;
                case ItemType.Date:
                    return Helper.FormatDate(mDateVal, null);
                case ItemType.Subform:
                    return mXmlVal;
                case ItemType.File:
                    return mTextVal;
                case ItemType.None:
                    return "";
            }
            return "";
        }
    }
}