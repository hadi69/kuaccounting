﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for FormDeclareInfp
/// </summary>
public class FormDeclareItem
{
    private int mID, mSortIndex;
    private string mMore, mTitle;
    private ItemType mItemType = ItemType.None;
    private bool mRequired = false;

    // Virtual Fields : read them from the field 'More' according to ItemType
    //<f n="ItemType" tp="int" comment="0:Numeric;1:Text;2:Date;3:Bool;4:Radio;5:Enum;6:Table;7:Report;8:XML;9:TextFile;10:Subform;11:Form" />

    #region for Number / Text
    // More: Regex|ErrorMessage --> ddd|عدد 3 رقمی
    public string Regex
    {
        get
        {
            if (string.IsNullOrEmpty(mMore) || mMore.IndexOf('`') < 1)
                return "";
            return mMore.Split('`')[0];
        }
        set
        {
            mMore = value + "`" + ErrorMessage;
        }
    }
    public string ErrorMessage
    {
        get
        {
            // More: HasTime|DateType --> true|system
            if (string.IsNullOrEmpty(mMore) || mMore.IndexOf('`') < 1)
                return "";
            return mMore.Split('`')[1];
        }
        set
        {
            mMore = Regex + "`" + value;
        }
    }
    #endregion

    #region for Date
    // More: HasTime|DateType --> true|system
    public bool HasTime
    {
        get
        {
            if (string.IsNullOrEmpty(mMore) || mMore.IndexOf('`') < 1)
                return false;
            return Helper.GetBool(mMore.Split('`')[0], false);
        }
        set
        {
            mMore = value.ToString() + "`" + DateType;
        }
    }
    public DateType DateType
    {
        get
        {
            // More: HasTime|DateType --> true|system
            if (string.IsNullOrEmpty(mMore) || mMore.IndexOf('`') < 1)
                return SiteSettings.DateType;
            return Helper.GetEnum(mMore.Split('`')[1], DateType.System);
        }
        set
        {
            mMore = HasTime.ToString() + "`" + value.ToString();
        }
    }
    #endregion

    #region for Enum, Radio
    // More: enums in several lines --> 
    //      0=big
    //      1=medium
    //      2=small
    public string[] EnumValues
    {
        get
        {
            if (string.IsNullOrEmpty(mMore))
                return null;
            return mMore.Split('\n');
        }
        set
        {
            if (value == null || value.Length == 0)
                mMore = "";
            else
                mMore = string.Join("\n", value);
        }
    }
    #endregion

    #region for Table & Report
    // More: Table/Report/FormName Name | DataField | DisplayFields | DataFieldType --> Patient | ID | Name + ' ' + Surname | Numeric
    public string TableName
    {
        get
        {
            if (string.IsNullOrEmpty(mMore))
                return "";
            string[] split = mMore.Split('`');
            if (split.Length != 4)
                return "";
            return split[0];
        }
        set
        {
            if (value == null)
                mMore = "";
            else
            {
                mMore = value + "`" + DataField + "`" + DisplayFields + "`" + DataFieldType;
            }
        }
    }
    public TableInfo Table
    {
        get
        {
            if (string.IsNullOrEmpty(mMore))
                return null;
            string[] split = mMore.Split('`');
            if (split.Length != 4)
                return null;
            return TableController.Instance[split[0]];
        }
        set
        {
            if (value == null)
                mMore = "";
            else
            {
                mMore = value.Name + "`" + DataField + "`" + DisplayFields + "`" + DataFieldType;
            }
        }
    }
    public ReportFile Report
    {
        get
        {
            if (string.IsNullOrEmpty(mMore))
                return null;
            string[] split = mMore.Split('`');
            if (split.Length != 4)
                return null;
            return DbProxy.Instance.GetReport(split[0]); // name
            //return DbProxy.Instance.GetReport(Helper.GetInt(split[0], -1));
        }
        set
        {
            if (value == null)
                mMore = "";
            else
            {
                mMore = value.Name + "`" + DataField + "`" + DisplayFields + "`" + DataFieldType;
            }
        }
    }
    //public int FormID
    //{
    //    get
    //    {
    //        if (string.IsNullOrEmpty(mMore))
    //            return "";
    //        string[] split = mMore.Split('`');
    //        if (split.Length != 4)
    //            return "";
    //        return split[0];
    //    }
    //    set
    //    {
    //        if (value == null)
    //            mMore = "";
    //        else
    //        {
    //            mMore = value + "`" + DataField + "`" + DisplayFields + "`" + DataFieldType;
    //        }
    //    }
    //}
    public FormDeclare Form
    {
        get
        {
            if (string.IsNullOrEmpty(mMore))
                return null;
            string[] split = mMore.Split('`');
            if (split.Length != 4)
                return null;
            return DbProxy.Instance.GetFormDeclareByName(split[0]); // name
            //return DbProxy.Instance.GetReport(Helper.GetInt(split[0], -1));
        }
        set
        {
            if (value == null)
                mMore = "";
            else
            {
                mMore = value.ID + "`" + DataField + "`" + DisplayFields + "`" + DataFieldType;
            }
        }
    }
    public string DataField
    {
        get
        {
            if (string.IsNullOrEmpty(mMore))
                return "";
            string[] split = mMore.Split('`');
            if (split.Length != 4)
                return "";
            return split[1];
        }
        set
        {
            string part0 = "";
            if (mItemType == ItemType.Table)
            {
                TableInfo table = this.Table;
                if (table != null)
                    part0 = table.Name;
            }
            else if (mItemType == ItemType.Report)
            {
                ReportFile report = this.Report;
                if (report != null)
                    part0 = report.ID.ToString();
            }
            mMore = part0 + "`" + value + "`" + DisplayFields + "`" + DataFieldType;
        }
    }
    public string DisplayFields
    {
        get
        {
            if (string.IsNullOrEmpty(mMore))
                return "";
            string[] split = mMore.Split('`');
            if (split.Length != 4)
                return "";
            return split[2];
        }
        set
        {
            string part0 = "";
            if (mItemType == ItemType.Table)
            {
                TableInfo table = this.Table;
                if (table != null)
                    part0 = table.Name;
            }
            else if (mItemType == ItemType.Report)
            {
                ReportFile report = this.Report;
                if (report != null)
                    part0 = report.ID.ToString();
            }
            mMore = part0 + "`" + DataField + "`" + value + "`" + DataFieldType;
        }
    }
    /// <summary>
    /// Numeric, Text, Date, Bool
    /// </summary>
    public ItemType DataFieldType
    {
        get
        {
            if (string.IsNullOrEmpty(mMore))
                return ItemType.Numeric;
            string[] split = mMore.Split('`');
            if (split.Length != 4)
                return ItemType.Numeric;
            return Helper.GetEnum(split[3], ItemType.Numeric);
        }
        set
        {
            string part0 = "";
            if (mItemType == ItemType.Table)
            {
                TableInfo table = this.Table;
                if (table != null)
                    part0 = table.Name;
            }
            else if (mItemType == ItemType.Report)
            {
                ReportFile report = this.Report;
                if (report != null)
                    part0 = report.ID.ToString();
            }
            mMore = part0 + "`" + DataField + "`" + DisplayFields + "`" + value.ToString();
        }
    }
    public DataTable GetValues(string title)
    {
        string query = "";
        if (mItemType == ItemType.Table)
            query = string.Format("SELECT {0}, {1} FROM {2} WHERE {1} LIKE '%{3}%'", DataField, DisplayFields, TableName, title);
        else if (mItemType == ItemType.Report)
        {
            ReportFile report = this.Report;
            query = string.Format("SELECT {0}, {1} FROM ({2}) MyTemp WHERE {1} LIKE '%{3}%'", DataField, DisplayFields, report.Query, title);
        }
        else if (mItemType == ItemType.Form)
        {
            FormDeclare form = this.Form;
            FormDeclareItem dataField = DbProxy.Instance.GetFormDeclareItem(form.ID, this.DataField);
            FormDeclareItem displayField = DbProxy.Instance.GetFormDeclareItem(form.ID, this.DisplayFields);
            query = string.Format("SELECT FormID, ISNULL(TextVal, NumericVal) FROM FormItem WHERE FormDeclareItemID={0} AND ISNULL(TextVal, NumericVal) LIKE N'%{1}%'"
                , displayField.ID, title);
            DataTable dtLikes = DbProxy.Instance.FillData(query, "fck");
            if (dtLikes == null || dtLikes.Rows.Count == 0)
                return null;
            DataTable res = new DataTable();
            res.Columns.Add("DataField", typeof(string));
            res.Columns.Add("DisplayField", typeof(string));
            for (int i = 0; i < dtLikes.Rows.Count; i++)
            {
                string val;
                if (DataFieldType == ItemType.Numeric)
                    val = DbProxy.Instance.FillScalarString(string.Format("SELECT NumericVal FROM FormItem WHERE FormID={0} AND FormDeclareItemID={1}"
                        , dtLikes.Rows[i][0], dataField.ID));
                else
                    val = DbProxy.Instance.FillScalarString(string.Format("SELECT TextVal FROM FormItem WHERE FormID={0} AND FormDeclareItemID={1}"
                        , dtLikes.Rows[i][0], dataField.ID));
                res.Rows.Add(val, dtLikes.Rows[i][1]);
            }
            return res;
        }
        else
            return null;
        DataTable dt = DbProxy.Instance.FillData(query, "fck");
        return dt;
    }
    #endregion

    #region for XML
    // More : FileName | Selector --> cities.xml | name
    // the files are in ~/files/data
    public string XmlFileName
    {
        get
        {
            if (string.IsNullOrEmpty(mMore) || mMore.IndexOf('`') < 1)
                return "";
            return mMore.Split('`')[0];
        }
        set
        {
            mMore = value + "`" + XmlSelector;
        }
    }
    public string XmlSelector
    {
        get
        {
            if (string.IsNullOrEmpty(mMore) || mMore.IndexOf('`') < 1)
                return "";
            return mMore.Split('`')[1];
        }
        set
        {
            mMore = XmlFileName + "`" + value;
        }
    }
    public string[] XmlValues
    {
        get
        {
            return null;
        }
    }
    #endregion

    #region for TextFile
    // More : FileName --> cities.txt
    // the files are in ~/files/data
    public string TextFileName
    {
        get
        {
            return mMore;
        }
        set
        {
            mMore = value;
        }
    }
    public string[] TextValues
    {
        get
        {
            if (string.IsNullOrEmpty(mMore))
                return null;
            string path = System.IO.Path.Combine(DataRoot, mMore);
            if (System.IO.File.Exists(path))
                return System.IO.File.ReadAllLines(path);
            return null;
        }
    }
    #endregion

    #region for Subform
    // More : SubformID --> cities.txt
    // the files are in ~/files/data
    public string SubformName
    {
        get
        {
            return mMore;
        }
        set
        {
            mMore = value;
        }
    }
    #endregion

    #region for File
    // More : FolderName --> cities.txt
    // the files are in ~/files/data
    public string FolderName
    {
        get
        {
            return mMore;
        }
        set
        {
            mMore = value;
        }
    }
    #endregion
    public FormDeclareItem()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public FormDeclareItem(DataRow row)
    {
        mID = Helper.GetInt(row, "ID", -1);
        mSortIndex = Helper.GetInt(row, "SortIndex", -1);
        mTitle = Helper.GetString(row, "Title", "");
        mMore = Helper.GetString(row, "More", "");
        mItemType = (ItemType)Helper.GetInt(row, "ItemType", (int)ItemType.None);
        mRequired = Helper.GetBool(row, "Required", mRequired);
    }

    public int ID { get { return mID; } set { mID = value; } }
    public int SortIndex { get { return mSortIndex; } set { mSortIndex = value; } }
    public string Title { get { return mTitle; } set { mTitle = value; } }
    public string More { get { return mMore; } set { mMore = value; } }
    public ItemType ItemType { get { return mItemType; } set { mItemType = value; } }
    public bool Required { get { return mRequired; } set { mRequired = value; } }

    string DataRoot
    {
        get
        {
            return System.Web.HttpContext.Current.Server.MapPath("~/files/data");
        }
    }
    // Sample: regex[ddd,عدد 3 رقمی]

}