﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Text;
using System.Xml;

/// <summary>
/// Summary description for FormEngine
/// </summary>
public class FormEngine
{
    HttpRequest mRequest = null;
    public FormEngine(HttpRequest request)
    {
        mRequest = request;
    }
    public string GetForm()
    {
        try
        {
            string res = GetFormInternal();
            return res;
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
            ExceptionLog.Instance.Add(ex);
            return "Error:" + ex.Message + "<br />" + ex.StackTrace.Replace("\n", "<br />");
        }
    }
    string GetFormInternal()
    {
        int formDeclareID = Helper.GetInt(mRequest.QueryString["FormDeclareID"], -1);
        int id = Helper.GetInt(mRequest.QueryString["ID"], -1);
        int otherID = Helper.GetInt(mRequest.QueryString["OtherID"], -1);
        DataRow dr = null;
        List<FormDeclareItem> declareItems = null;

        #region init & validate
        if (id != -1)
        {
            dr = DbProxy.Instance.FillRow("SELECT * FROM Forms WHERE ID=" + id);
            if (dr == null)
                return "Error: Form not found";
            formDeclareID = Helper.GetInt(dr, "FormDeclareID", formDeclareID);
            otherID = Helper.GetInt(dr, "OtherID", -1);
        }

        FormDeclare declare = DbProxy.Instance.GetFormDeclare(formDeclareID);
        if (declare == null)
            return "Error: Form not found";

        declareItems = DbProxy.Instance.GetFormDeclareItems(declare.ID);
        if (declareItems == null || declareItems.Count == 0)
            return string.Format("Error: Form width code {0} has no items", declare.Code);
        #endregion

        DataTable items = id == -1 ? null : DbProxy.Instance.FillData("SELECT * FROM FormItem WHERE FormID=" + id, "");

        StringBuilder script = new StringBuilder();
        StringBuilder builder = new StringBuilder();
        builder.AppendFormat("<div class='popupTitle'>{0}</div>", declare.Name);
        builder.StartContainer();
        builder.StartForm("declareForm");
        builder.StartRow();

        builder.StartFormGroupContainer();
        builder.StartFormGroup();
        builder.AddHiddenInput("FormDeclareID", "FormDeclareID", formDeclareID.ToString(""));
        builder.AddHiddenInput("ID", "ID", id.ToString(""));
        builder.AddHiddenInput("OtherID", "OtherID", otherID.ToString(""));
        builder.AddHiddenInput("PersonnelID", "PersonnelID", mRequest.QueryString["PersonnelID"]);
        //builder.AddLabel("FD_FormDate", "Date".Translate());
        //builder.AddDateInput("FD_FormDate", "FormDate", Helper.FormatDate(Helper.GetDateTime(dr, "FormDate", DateTime.Now), null), "");
        builder.EndFormGroup();
        builder.EndFormGroupContainer();

        if (Helper.GetString(mRequest.QueryString["OtherTitle"], "").Length > 0)
        {
            builder.StartFormGroupContainer();
            builder.StartFormGroup();
            builder.AddLabel("FD_OtherTitle", ToString(declare.FormType));
            builder.AddTextInput("FD_OtherTitle", "FD_OtherTitle", Helper.GetString(mRequest.QueryString["OtherTitle"], ""), "");
            builder.EndFormGroup();
            builder.EndFormGroupContainer();
        }

        int counter = 0;
        for (int i = 0; i < declareItems.Count; i++)
        {
            // find current item
            FormItem item = FindFormItem(items, declareItems[i]);
            if (declareItems[i].ItemType != ItemType.Subform)
            {
                builder.StartFormGroupContainer();
                builder.StartFormGroup();
                builder.AddLabel("FI_" + declareItems[i].ID, declareItems[i].Title);
                builder.AppendLine(CreateField(declareItems[i], item == null ? "" : item.Value, script, ""));
                builder.EndFormGroup();
                builder.EndFormGroupContainer();
            }
            else
            {
                builder.StartLargeFormGroupContainer();
                builder.StartFormGroup();
                builder.AddLargeLabel("FI_" + declareItems[i].ID, declareItems[i].Title);
                builder.AppendLine(CreateField(declareItems[i], item == null ? "" : item.Value, script, ""));
                builder.EndFormGroup();
                builder.EndLargeFormGroupContainer();
            }

            counter++;
            if (declareItems[i].ItemType == ItemType.Subform)
            {
                counter = 0;
            }
            else if (counter == 2)
            {
                counter = 0;
            }
        }
        builder.EndRow();
        builder.EndForm();
        builder.EndContainer();

        string counters = "";
        if (subFormCounts.Count > 0)
        {
            Dictionary<string, int>.Enumerator enu = subFormCounts.GetEnumerator();
            while (enu.MoveNext())
                counters += string.Format("var {0} = {1};", enu.Current.Key, enu.Current.Value);
        }


        builder.Append(@"<script language='javascript'>
        $(function () {
            _COUNTERS_
            dateSelect('.dateSelect');
            makeChosen();
            function autoPostPack(event, data, formatted) {
              $(event.target).next().val(data[0]);
            }
            _SCRIPT_
            insertValidation();
        });
    </script>".Replace("_SCRIPT_", script.ToString()).Replace("_COUNTERS_", counters));

        return builder.ToString();
    }

    private static FormItem FindFormItem(DataTable items, FormDeclareItem declareItem)
    {
        if (items != null && items.Rows.Count > 0)
            for (int j = 0; j < items.Rows.Count; j++)
                if (Helper.GetInt(items.Rows[j], "FormDeclareItemID", -1) == declareItem.ID)
                    return new FormItem(items.Rows[j], declareItem);
        return null;
    }
    Dictionary<string, int> subFormCounts = new Dictionary<string, int>();
    const string BracketOpen = "brAckoP", BracketClose = "brAckcL";
    // todo: value muss objekt sein

    private string CreateField(FormDeclareItem declare, string value, StringBuilder script, string parentCounterName)
    {
        StringBuilder builder = new StringBuilder();
        //string value = item == null ? "" : item.Value;
        switch (declare.ItemType)
        {
            case ItemType.Numeric:
                #region Numeric
                {
                    string validation = "";
                    if (declare.Required)
                        validation = "required,";
                    if (string.IsNullOrEmpty(declare.Regex))
                        validation += "number";
                    else
                        validation += string.Format("regex[{0},{1}]", declare.Regex.Replace("[", BracketOpen).Replace("]", BracketClose), declare.ErrorMessage);

                    validation = "validate[" + validation + "]"; // e.g. validate[number]

                    builder.AddTextInput("FI_" + declare.ID, "FI_" + declare.ID, value, validation);
                    return builder.ToString();
                }
                #endregion
            case ItemType.Text:
                #region Text
                {
                    string validation = "";
                    if (!string.IsNullOrEmpty(declare.Regex))
                        validation = string.Format("{0}regex[{1},{2}]", declare.Required ? "required," : ""
                            , declare.Regex.Replace("[", BracketOpen).Replace("]", BracketClose), declare.ErrorMessage);
                    if (!String.IsNullOrEmpty(validation))
                    {
                        validation = "validate[" + validation + "]"; // e.g. validate[number]
                    }

                    builder.AddTextInput("FI_" + declare.ID, "FI_" + declare.ID, value, validation);
                    return builder.ToString();
                }
                #endregion
            case ItemType.Date:
                #region Date
                {
                    string dateClass = "dateSelect";
                    if (declare.DateType == DateType.en && SiteSettings.DateType != DateType.en)
                        dateClass = "dateSelectEn";
                    else if (declare.DateType == DateType.fa && SiteSettings.DateType != DateType.fa)
                        dateClass = "dateSelectFa";
                    else if (declare.DateType == DateType.ar && SiteSettings.DateType != DateType.ar)
                        dateClass = "dateSelectAr";
                    builder.AddTextInput("FI_" + declare.ID, "FI_" + declare.ID, value, dateClass);
                    // todo: the time ?
                    if (declare.HasTime)
                        builder.AddTextInput("FI_" + declare.ID + "_Time", "FI_" + declare.ID + "_Time", value, "validate[required]");
                    return builder.ToString();
                }
                #endregion
            case ItemType.Bool:
                builder.AddCheckBox("FI_" + declare.ID, "FI_" + declare.ID, Helper.GetDecimal(value, 0) == 1, "");
                return builder.ToString();
            case ItemType.Radio:
                #region Radio
                {
                    // 0=متخصص زنان و زایمان;1=مامای تحصیل کرده;2=فرد دوره دیده;3=فرد دوره ندیده
                    string[] nameVals = declare.EnumValues;
                    builder.StartControl();
                    for (int j = 0; j < nameVals.Length; j++)
                    {
                        string[] nameVal = nameVals[j].Split('=');
                        if (nameVal.Length == 2)
                        {
                            builder.AddRadioButton(nameVal[1], "FI_" + declare.ID + "_" + j, "FI_" + declare.ID, nameVal[0], Helper.GetDecimal(nameVal[0], 0) == Helper.GetDecimal(value, 0));
                        }
                    }
                    builder.EndControl();
                    return builder.ToString();
                }
                #endregion
            case ItemType.Enum:
                #region Enum
                {
                    builder.AddSelect("FI_" + declare.ID, "FI_" + declare.ID, declare.EnumValues, Helper.GetDecimal(value, 0), "");
                    return builder.ToString();
                }
                #endregion
            case ItemType.Table:
                #region Table
                {
                    TableInfo table = declare.Table;
                    if (table != null)
                    {
                        string valueName = "";
                        if (!string.IsNullOrEmpty(value) && table != null)
                        {
                            // `Value` is the DataField --> Find DisplayFields
                            string query = string.Format("SELECT {0} FROM {1} WHERE  {2}='{3}'", declare.DisplayFields, declare.Table.Name, declare.DataField, value);
                            object obj = DbProxy.Instance.FillScalar(query);
                            if (obj != null)
                                valueName = Helper.GetString(obj, valueName);
                        }
                        string btnNewClass = "";
                        if (table.Name.ToLower() == "storedraft")
                            btnNewClass = "newstoredraft";
                        else if (table.Name.ToLower() == "personnel")
                            btnNewClass = "newpersonnel";
                        builder.AddInputGroup("FI_" + declare.ID + "Name", "FI_" + declare.ID + "Name", valueName, "FI_" + declare.ID + "Name", "FI_" + declare.ID, "FI_" + declare.ID, value, "FI_" + declare.ID + "NNew", "...", "FI_" + declare.ID + "New " + btnNewClass);
                        script.AppendFormat("$('.FI_{0}Name').unautocomplete();autoCompleteForm('{0}', '.FI_{0}Name', 'fuckTurkey', autoPostPack);\r\n", declare.ID);
                    }
                    else
                        builder.Append("NO TABLE");
                    return builder.ToString();
                }
                #endregion
            case ItemType.Report:
                #region Report
                {
                    ReportFile file = declare.Report;
                    if (file != null)
                    {
                        string valueName = "";
                        if (!string.IsNullOrEmpty(value))
                        {
                            // `Value` is the DataField --> Find DisplayFields
                            string query = string.IsNullOrEmpty(file.Query) ? new ReportEngine(file).CreateQuery() : file.Query;
                            query = string.Format("SELECT {0} FROM ({1}) FuckTurkey WHERE {2}='{3}'", declare.DisplayFields, query, declare.DataField, value);
                            object obj = DbProxy.Instance.FillScalar(query);
                            if (obj != null)
                                valueName = Helper.GetString(obj, valueName);
                        }
                        builder.AddTextInput("FI_" + declare.ID + "Name", "FI_" + declare.ID + "Name", valueName, "FI_" + declare.ID + "Name");
                        builder.AddHiddenInput("FI_" + declare.ID, "FI_" + declare.ID, value);
                        script.AppendFormat("$('.FI_{0}Name').unautocomplete();autoCompleteForm('{0}', '.FI_{0}Name', 'fuckTurkey', autoPostPack);\r\n", declare.ID);
                    }
                    else
                        builder.Append("NO REPORT");
                    return builder.ToString();
                }
                #endregion
            case ItemType.XML:
                #region XML
                {
                    // 0=متخصص زنان و زایمان;1=مامای تحصیل کرده;2=فرد دوره دیده;3=فرد دوره ندیده
                    builder.AddXml("FI_" + declare.ID, "FI_" + declare.ID, declare.XmlValues, value, "");
                    return builder.ToString();
                }
                #endregion
            case ItemType.TextFile:
                #region TextFile
                {
                    builder.AddTextFile("FI_" + declare.ID, "FI_" + declare.ID, declare.TextValues, value, "");
                    return builder.ToString();
                }
                #endregion
            case ItemType.Subform:
                #region Subform
                List<FormDeclareItem> subItems = DbProxy.Instance.GetFormDeclareItemsByName(declare.SubformName);
                if (subItems != null)
                {
                    string counterName = "fd_count" + declare.ID;
                    subFormCounts.Add(counterName, 0);
                    builder.StartLargeControl();
                    builder.AppendFormat("<table id='tableSub{0}' class='full-width table-with-border zebra'><tr class='th'>", declare.ID);
                    for (int si = 0; si < subItems.Count; si++)
                        builder.AppendFormat("<td>{0}</td>", subItems[si].Title);
                    builder.AppendFormat("<td width='29' class='center'><img src='img/add.gif' class='addSub{0}' /></td>", declare.ID);
                    builder.Append("</tr>");

                    // todo: add existing rows
                    if (!string.IsNullOrEmpty(value))
                    {
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(value);
                        foreach (XmlNode node in xmlDoc.DocumentElement.ChildNodes)
                        {
                            builder.Append("<tr>");
                            for (int si = 0; si < subItems.Count; si++)
                            {
                                string val = XmlHelper.GetString(node, "i" + subItems[si].ID, null);
                                builder.AppendFormat("<td>{0}</td>\r\n", CreateField(subItems[si], val, script, counterName));
                            }
                            builder.AppendFormat("<td><img src='img/delete.gif' id='r_{0}' class='removeSub{1}' /></td>\r\n", subFormCounts[counterName]++, declare.ID.ToString());
                            builder.Append("</tr>\r\n");
                        }
                    }
                    builder.Append("</table>");
                    builder.EndLargeControl();

                    // Script to add new row to the table
                    StringBuilder script2 = new StringBuilder();
                    script.Append("function addSub{0}() {\r\nvar row = '<tr>';".Replace("{0}", declare.ID.ToString()));
                    for (int si = 0; si < subItems.Count; si++)
                    {
                        script.AppendFormat("row += \"<td>{0}</td>\";\r\n", CreateField(subItems[si], null, script2, counterName));
                    }
                    script.Append("row += '<td><img src=\"img/delete.gif\" id=\"r_' + fd_cnt + '\" class=\"removeSub{0}\" /></td>';\r\n"
                        .Replace("fd_cnt", counterName).Replace("{0}", declare.ID.ToString()));
                    //script.Append(script2.ToString());
                    script.Append("row += '</tr>'; fd_cnt++; return row;\r\n}\r\n".Replace("fd_cnt", counterName));
                    script.Append(@"
$('.addSub{0}').click(function () {
    var table = $(this).closest('table');
    var tableID = table.attr('id');
    $('tr:last', table).after(addSub{0}());
    var lastRow = $('tr:last', table);
    SCRIPT2;                
    $('.removeSub{0}', lastRow).click(function () {
    $(this).parent().parent().remove();
        //removeSub();
    });
});
$('.removeSub{0}').click(function () {
    $(this).parent().parent().remove();
    //removeSub();
});".Replace("{0}", declare.ID.ToString()).Replace("fd_cnt", counterName).Replace("SCRIPT2", script2.ToString()));
                }
                else
                    builder.AppendFormat("<label>Form with code {0} not found</label>", declare.More);
                return builder.ToString();
                #endregion
            case ItemType.Form:
                #region Form
                {
                    FormDeclare form = declare.Form;
                    if (form != null)
                    {
                        string valueName = "";
                        if (!string.IsNullOrEmpty(value))
                        {
                            // `Value` is the DataField --> Find DisplayFields
                            FormDeclareItem dataField = DbProxy.Instance.GetFormDeclareItem(form.ID, declare.DataField);
                            FormDeclareItem displayField = DbProxy.Instance.GetFormDeclareItem(form.ID, declare.DisplayFields);
                            string query ;
                            if (declare.DataFieldType == ItemType.Numeric)
                                query = string.Format("SELECT FormID FROM FormItem WHERE FormDeclareItemID={0} AND NumericVal={1}"
                                , dataField.ID, value);
                            else
                                query = string.Format("SELECT FormID FROM FormItem WHERE FormDeclareItemID={0} AND TextVal={1}"
                                , dataField.ID, Helper.QoutedString(value));
                            int formID = Helper.GetInt(DbProxy.Instance.FillScalar(query), -1);
                            query = string.Format("SELECT ISNULL(TextVal, NumericVal) FROM FormItem WHERE FormDeclareItemID={0} AND FormID={1}"
                                , displayField.ID, formID);
                            object obj = DbProxy.Instance.FillScalar(query);
                            if (obj != null)
                                valueName = Helper.GetString(obj, valueName);
                        }
                        builder.AddTextInput("FI_" + declare.ID + "Name", "FI_" + declare.ID + "Name", valueName, "FI_" + declare.ID + "Name");
                        builder.AddHiddenInput("FI_" + declare.ID, "FI_" + declare.ID, value);
                        script.AppendFormat("$('.FI_{0}Name').unautocomplete();autoCompleteForm('{0}', '.FI_{0}Name', 'fuckTurkey', autoPostPack);\r\n", declare.ID);
                    }
                    else
                        builder.Append("NO REPORT");
                    return builder.ToString();
                }
                #endregion
            case ItemType.File:
                #region File
                {
                    builder.StartControl();
                    if (string.IsNullOrEmpty(parentCounterName))
                    {
                        builder.AppendFormat("<a href='files/{1}/{2}' target='_blank' class='btn btn-sm btn-primary'>{2}</a><input type='file' id='ChooseFile_{0}' name='ChooseFile_{0}' class='' /><a href='#' id='StartUpload_{0}' class='btn btn-sm btn-primary'>{3}</a><input type='hidden' id='FI_{0}' name='FI_{0}' value='{2}' />"
                         , declare.ID, declare.FolderName, value, LangProvider.Instance["Send"]);
                        script.AppendFormat("uploadify('#ChooseFile_{0}', '#FI_{0}', '#StartUpload_{0}', '{1}');\r\n", declare.ID, declare.FolderName);
                    }
                    else // subform
                    {
                        builder.AppendFormat("<a href='files/{1}/{2}' target='_blank' class='btn btn-sm btn-primary'>{2}</a><input type='file' id='ChooseFile_{0}_\" + {4} + \"' name='ChooseFile_{0}' class='' /><a href='#' id='StartUpload_{0}_\" + {4} + \"' class='btn btn-sm btn-primary'>{3}</a><input type='hidden' id='FI_{0}_\" + {4} + \"' name='FI_{0}' value='{2}' />"
                         , declare.ID, declare.FolderName, value, LangProvider.Instance["Send"], parentCounterName);
                        script.AppendFormat("uploadify('#ChooseFile_{0}_' + ({2} - 1), '#FI_{0}_' + ({2} - 1), '#StartUpload_{0}_' + ({2} - 1), '{1}');\r\n", declare.ID, declare.FolderName, parentCounterName);
                    }
                    builder.EndControl();
                    
                    return builder.ToString();
                }
                #endregion
            case ItemType.None:
                // has no field
                break;

        }
        return "";
    }
    public void SaveForm()
    {
        int formDeclareID = Helper.GetInt(mRequest.Form["FormDeclareID"], -1);
        int id = Helper.GetInt(mRequest.Form["ID"], -1);
        int otherID = Helper.GetInt(mRequest.Form["OtherID"], -1);
        DataRow dr = null;
        List<FormDeclareItem> declareItems = null;
        #region init & validate
        if (id != -1)
        {
            dr = DbProxy.Instance.FillRow("SELECT * FROM Forms WHERE ID=" + id);
            if (dr == null)
                throw new Exception("Form not found");
            formDeclareID = Helper.GetInt(dr, "FormDeclareID", formDeclareID);
            otherID = Helper.GetInt(dr, "OtherID", -1);
        }

        FormDeclare declare = DbProxy.Instance.GetFormDeclare(formDeclareID);
        if (declare == null)
            throw new Exception("Form not found");

        declareItems = DbProxy.Instance.GetFormDeclareItems(formDeclareID);
        if (declareItems == null || declareItems.Count == 0)
            throw new Exception(string.Format("Form width code {0} has no items", declare.Code));
        #endregion
        DbProxy dbProxy = new DbProxy();
        dbProxy.BeginTransaction();
        try
        {
            // Master table : FileForm
            #region save/update master
            string masterQry;
            if (id == -1)
                masterQry = string.Format("INSERT INTO Forms (OtherID, FormDeclareID, FormDate, PersonnelID, ConfirmerID, Comments) VALUES ({0}, {1}, @FormDate, @PersonnelID, @ConfirmerID, @Comments); SELECT SCOPE_IDENTITY();"
                    , otherID, formDeclareID);
            else
                masterQry = string.Format("UPDATE Forms SET FormDate=@FormDate, PersonnelID=@PersonnelID, ConfirmerID=@ConfirmerID, Comments=@Comments WHERE ID={0};SELECT {0};"
                    , id);
            masterQry = Helper.ParseQueryVars(masterQry, TableController.Instance["Forms"], mRequest);
            id = Helper.GetInt(DbProxy.Instance.FillScalar(masterQry), -1);
            if (id == -1)
                throw new Exception("Failed to save Forms");
            #endregion

            // Items
            // delete old items
            DbProxy.Instance.ExecuteCommand("DELETE FROM FormItem WHERE FormID=" + id);

            // save new ones
            for (int i = 0; i < declareItems.Count; i++)
            {
                // FI_{0}
                string field = string.Format("FI_{0}", declareItems[i].ID);
                field = mRequest.Form[field];

                FormItem item = GetValue(declareItems[i]);

                //string req = Helper.GetString(mRequest.Form[required], null, count);

                string insert = string.Format("INSERT INTO FormItem (FormID, NumericVal, TextVal, XmlVal, DateVal, FormDeclareItemID) VALUES ({0}, {1}, {2}, {3}, {4}, {5})"
                    , id, item.NumericVal, Helper.QoutedString(item.TextVal), Helper.QoutedString(item.XmlVal), Helper.QoutedDate(item.DateVal), declareItems[i].ID);
                DbProxy.Instance.RunQuery(insert);
            }
            dbProxy.CommitTransaction();
        }
        catch (Exception ex)
        {
            dbProxy.RollbackTransaction();
            Logger.Log(ex);
        }
    }
    FormItem GetValue(FormDeclareItem declare)
    {
        FormItem item = new FormItem();

        // FI_{0}
        string field = string.Format("FI_{0}", declare.ID);
        field = mRequest.Form[field];

        switch (declare.ItemType)
        {
            case ItemType.Numeric:
                item.NumericVal = Helper.GetDecimal(field, item.NumericVal);
                break;
            case ItemType.Text:
                item.TextVal = field;
                break;
            case ItemType.Date:
                item.DateVal = Helper.ParseDate(field, Helper.NullDate);
                // todo: handle DateType & time
                break;
            case ItemType.Bool:
                item.NumericVal = field == null ? 0 : (field.ToLower() == "on" || field.ToLower() == "checked" || field.ToLower() == "1") ? 1 : 0;
                break;
            case ItemType.Radio:
            case ItemType.Enum:
                item.NumericVal = Helper.GetDecimal(field, item.NumericVal);
                break;
            case ItemType.Table:
            case ItemType.Report:
            case ItemType.Form:
                switch (declare.DataFieldType)
                {
                    case ItemType.Numeric:
                    case ItemType.Bool:
                        item.NumericVal = Helper.GetDecimal(field, item.NumericVal);
                        break;
                    case ItemType.Text:
                        item.TextVal = field;
                        break;
                    case ItemType.Date:
                        item.DateVal = Helper.ParseDate(field, Helper.NullDate);
                        break;
                }
                break;
            case ItemType.XML:
            case ItemType.TextFile:
            case ItemType.File:
                item.TextVal = field;
                break;
            case ItemType.Subform:
                // create Xml
                List<FormDeclareItem> subItems = DbProxy.Instance.GetFormDeclareItemsByName(declare.SubformName);
                if (subItems == null || subItems.Count == 0)
                    return item;
                // find the max rows
                int numItems = 0;
                for (int si = 0; si < subItems.Count; si++)
                {
                    string subID = string.Format("FI_{0}", subItems[si].ID);
                    string subValue = mRequest.Form[subID];
                    if (!string.IsNullOrEmpty(subValue))
                        numItems = Math.Max(numItems, subValue.Split(',').Length);
                }
                if (numItems == 0)
                    return item;
                XmlDocument xmlDoc = XmlHelper.GetXmlDocWithElement("sub", "1");
                for (int i = 0; i < numItems; i++)
                {
                    XmlNode node = xmlDoc.CreateNode(XmlNodeType.Element, "n" + i, null);
                    for (int si = 0; si < subItems.Count; si++)
                    {
                        string subID = string.Format("FI_{0}", subItems[si].ID);
                        string subValue = mRequest.Form[subID];
                        if (!string.IsNullOrEmpty(subValue))
                        {
                            string[] split = subValue.Split(',');
                            string val = split[Math.Min(split.Length - 1, i)];
                            node.Attributes.SetNamedItem(XmlHelper.CreateXmlAttr(xmlDoc, "i" + subItems[si].ID, val));
                        }
                    }
                    xmlDoc.DocumentElement.AppendChild(node);
                }
                item.XmlVal = xmlDoc.OuterXml;
                break;
        }
        return item;
    }
    string ToString(FormType type)
    {
        switch (type)
        {
            case FormType.Patient:
                return LangProvider.Instance["FormTypePatient"];
            case FormType.File:
                return LangProvider.Instance["FormTypeFile"];
            case FormType.Personnel:
                return LangProvider.Instance["FormTypePersonnel"];
            case FormType.Ward:
                return LangProvider.Instance["FormTypeWard"];
            case FormType.Service:
                return LangProvider.Instance["FormTypeService"];
        }
        return "";
    }

    public static string BuildMenu(int formType, string access)
    {
        string menu = "";
        DataTable forms = DbProxy.Instance.FillData("SELECT ID, Name, Code FROM FormDeclare WHERE FormType=" + formType + " ORDER BY Name", "forms");
        if (forms != null && forms.Rows.Count > 0)
        {
            for (int i = 0; i < forms.Rows.Count; i++)
                menu += string.Format(@"<div class='col-xs-12 col-md-6 col-lg-4 pill-container'>
    <a href='../list.aspx?table=Forms&view=freeforms&FormDeclareID={0}' access='{2}' title='{1}'>
        <div class='pill'>            
                <div class='icon'>
                    <img src='../img/dummy.png' class='sprites sprites-return-32'>
                </div>            
			<div class='text'>
				<span>{1}</span>
			</div>
        </div>
    </a>
</div>", forms.Rows[i][0], forms.Rows[i][1], access);
        }
        return menu;
    }
}