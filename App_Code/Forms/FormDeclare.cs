﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;

public class FormDeclare
{
    public int ID = -1;
    public string Name, Code, ViewName, Layout;
    public FormType FormType = FormType.File;
    //TerminologyID
    //Display
    //Enabled
    //Version
    //CreatorID
    //CreationDate
    //ConfirmerID
    //ConfirmDate
    public FormDeclare()
    {
    }
    public FormDeclare(DataRow row)
    {
        ID = Helper.GetInt(row, "ID", -1);
        Name = Helper.GetString(row, "Name", "");
        Code = Helper.GetString(row, "Code", "");
        ViewName = Helper.GetString(row, "ViewName", "");
        Layout = Helper.GetString(row, "Layout", "");
        FormType = (FormType)Helper.GetInt(row, "FormType", (int)FormType.File);
    }
}