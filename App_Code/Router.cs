﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Newtonsoft.Json;

/// <summary>
/// Summary description for Router
/// </summary>
public static class Router
{
    private static List<JpRoute> _routes = new List<JpRoute>();
    public static void RegisterRoutes(RouteCollection routes)
    {
        AddRoute(
            "service-image-list",
            "Api/ServiceImage/{id}",
            "~/webapi/ServiceImage/List.aspx"
        );

        string[] allowedMethods = { "POST" };
        AddRoute(
            "delete-service-image",
            "Api/ServiceImage/{ServiceID}/{id}",
            "~/webapi/ServiceImage/Delete.aspx",
            false,
            new RouteValueDictionary(),
            new RouteValueDictionary{{"httpMethod", new HttpMethodConstraint(allowedMethods)}}
        );

        AddRoute(
            "advertisement-image-list",
            "Api/AdvertisementImage/{id}",
            "~/webapi/AdvertisementImage/AdvertisementImageList.aspx"
        );
        AddRoute(
            "delete-advertisement-image",
            "Api/AdvertisementImage/{AdvertisementID}/{id}",
            "~/webapi/AdvertisementImage/AdvertisementImageDelete.aspx",
            false,
            new RouteValueDictionary(),
            new RouteValueDictionary { { "httpMethod", new HttpMethodConstraint(allowedMethods) } }
        );

        // Print routes
        AddRoute(
            "print-storedraft",
            "print/storedraft/{ids}/{title}",
            "~/app/print/StoreDraft/StoreDraft.aspx"
        );

        AddRoute(
            "print-distreport",
            "print/distreportprint/{title}",
            "~/app/print/DistReportPrint/DistReportPrint.aspx"
        );

        AddRoute(
            "print-purchase",
            "print/purchase/{id}",
            "~/app/print/Purchase/Purchase.aspx"
        );
        AddRoute(
        "print-transfer",
        "print/transfer/{id}",
        "~/app/print/Transfer/Transfer.aspx"
    );

        // TaxIntroduction
        AddRoute(
            "print-tax-introduction",
            "print/tax-introduction/{year}/{month}",
            "~/app/print/TaxIntroduction/TaxIntroduction.aspx"
        );

        // TaxSummary
        AddRoute(
            "print-tax-summary",
            "print/tax-summary/{year}/{month}",
            "~/app/print/TaxSummary/TaxSummary.aspx"
        );
        
        //Fish
        AddRoute(
            "print-fish",
            "print/fish/{id}",
            "~/app/print/Fish/Fish.aspx"
        );

        //Insurance
        AddRoute(
            "print-insurance",
            "print/insurance/{year}/{month}/{section}",
            "~/app/print/Insurance/Insurance.aspx"
        );

        //PaidWage
        AddRoute(
            "print-paid-wage",
            "print/paidwage/{year}/{month}",
            "~/app/print/PaidWage/PaidWage.aspx"
        );

        AddRoute(
            "print-documentaccounting",
            "print/documentaccounting/{from}/{to}",
            "~/app/print/DocumentAccounting/DocumentAccounting.aspx"
        );

        // Edit BankPrint
        AddRoute(
            "edit-bank-print",
            "print/bankprint/{id}/edit",
            "~/app/print/BankPrint/EditBankPrint.aspx"
        );

        AddRoute(
            "print-bank-print",
            "print/bankprint/{id}",
            "~/app/print/BankPrint/BankPrint.aspx"
        );

        AddRoute(
            "print-wftstoredraft",
            "print/wftstoredraft/{id}",
            "~/app/print/WFTStoreDraft/WFTStoreDraft.aspx"
        );

        AddRoute(
            "print-paycheckreceipt",
            "print/paycheckreceipt/{id}",
            "~/app/print/PayCheckReceipt/PayCheckReceipt.aspx"
        );

        AddRoute(
            "print-banksalary-melibank",
            "print/banksalary/{id}/{year}/{month}",
            "~/app/print/BankSalary/MeliBank.aspx"
            );

        AddRoute(
            "print-fieldreport",
            "print/fieldreport/{year}/{month}",
            "~/app/print/FieldReport/FieldReport.aspx"
            );

        AddRoute(
            "print-fishset",
            "print/fishset/{year}/{month}/{fromnum}/{tonum}",
            "~/app/print/PrintFishSet/PrintFishSet.aspx"
            );

        AddRoute(
            "print-loanreport",
            "print/loanreport/{year}/{month}",
            "~/app/print/LoanReport/LoanReport.aspx"
            );
        AddRoute(
            "print-purchaserequest",
            "print/PurchaseRequest/{ids}/{title}",
            "~/app/print/PurchaseRequest/PurchaseRequest.aspx"
        );

        AddRoute(
        "print-return",
        "print/returns/{ids}/{title}",
        "~/app/print/returns/Returns.aspx"
       );

        AddRoute(
        "print-refuse",
        "print/refuse/{ids}/{title}",
        "~/app/print/refuse/Refuse.aspx"
       );

        AddRoute(
            "print-docreceipt",
            "print/docreceipt/{No}/{Amount}/{Discount}/{CheckAmount}/{Type}/{Person}/{CheckSerial}/{PersonId}/{TopicName}",
            "~/app/print/DocReceipt/DocReceipt.aspx"
        );

        AddRoute(
            "print-checkreceipt",
            "print/checkreceipt/{id}",
            "~/app/print/CheckReceipt/CheckReceipt.aspx"
        );

        AddRoute(
        "print-servicebarcode",
        "print/servicebarcode/{ids}/{numOfRow}/{hasServiceVariat}",
        "~/app/print/ServiceBarCode/ServiceBarCode.aspx"
        );

        foreach (JpRoute route in _routes)
        {
            routes.MapPageRoute(route.RouteName, route.RouteUrl, route.PhysicalFile, route.CheckPhysicalUrlAccess, route.Defaults, route.Constraints);
        }
    }

    public static string Serialize()
    {
        return JsonConvert.SerializeObject(_routes);
    }

    private static JpRoute AddRoute(string routeName, string routeUrl, string physicalFile, bool checkPhysicalUrlAccess = true,
        RouteValueDictionary defaults = null, RouteValueDictionary constraints = null, RouteValueDictionary dataTokens = null)
    {
        if (routeUrl == null)
        {
            throw new ArgumentNullException("routeUrl");
        }
        JpRoute route = new JpRoute(routeName, routeUrl, physicalFile, checkPhysicalUrlAccess, defaults, constraints, dataTokens);
        _routes.Add(route);
        return route;
    }
}

class JpRoute
{
    public JpRoute(string routeName, string routeUrl, string physicalFile, bool checkPhysicalUrlAccess,
        RouteValueDictionary defaults , RouteValueDictionary constraints , RouteValueDictionary dataTokens )
    {
        RouteName = routeName;
        RouteUrl = routeUrl;
        PhysicalFile = physicalFile;
        CheckPhysicalUrlAccess = checkPhysicalUrlAccess;
        Defaults = defaults;
        Constraints = constraints;
        DataTokens = dataTokens;
    }
    public string RouteName { get; set; }
    public string RouteUrl { get; set; }
    public string PhysicalFile { get; set; }
    public bool CheckPhysicalUrlAccess { get; set; }
    public RouteValueDictionary Defaults { get; set; }
    public RouteValueDictionary Constraints { get; set; }
    public RouteValueDictionary DataTokens { get; set; }
}