﻿using System;
using System.Collections.Generic;
using System.Web;

public enum DateType
{
    System, // means SiteSettings.DateType
    fa,
    en,
    ar
}

public enum ItemType
{
    None = -1,
    Numeric = 0,
    Text = 1,
    Date = 2,
    Bool = 3,
    Radio = 4,
    Enum = 5,
    Table = 6,
    Report = 7,
    XML = 8,
    TextFile = 9,
    Subform = 10,
    Form = 11,
    File = 12
}

public enum FormType
{
    Patient = 0,
    File = 1,
    Personnel = 2,
    Ward = 3,
    Service = 4,
    Free = 5
}
public enum WarningLevel
{
    Success,
    Warning,
    Error
}

public enum LogType
{
    Login = 0,
    Logout = 1,
    Query = 2,
    Insert = 3,
    Update = 4,
    Delete = 5,
    MultiDelete = 6,
    Unknown = 7,
    UpdateDetail = 8,
}

public enum FieldsType
{
    None=0,
    Numeric=1,
    Boolean=2,
    Enum=3,
    RealEnum=4,
    Formula=5,
    Query=6
}

public enum FieldShowType
{
    Normal = 0,
    Function = 1,
    Benefit = 2,
    Deduction = 3,
    Total = 4
}

public enum ReportFieldType
{
    Insurance=1,
    Tax=2,
    Account=3
}