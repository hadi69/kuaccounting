﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Xml;

/// <summary>
/// Summary description for QueryInfo
/// </summary>
public class QueryInfo
{
    public string Name = "";
    public string Select = "";
    public string From = "";
    public string Query = "";
    public string Where = "";
    public string BeforeQuery = "";
    public string AfterQuery = "";
    public string InsertLog = null;
    public string UpdateLog = null;
    public string DeleteLog = null;
    public string TriggerMethod = null;
    public QueryInfo(XmlNode node)
    {
        Name = XmlHelper.GetString(node, "n", null);
        Query = XmlHelper.GetString(node, "q", null);
        From = XmlHelper.GetString(node, "f", From);
        Select = XmlHelper.GetString(node, "s", null);
        Where = XmlHelper.GetString(node, "w", null);
        BeforeQuery = XmlHelper.GetString(node, "bl", null);
        AfterQuery = XmlHelper.GetString(node, "al", null);
        TriggerMethod = XmlHelper.GetString(node, "t", null);
        InsertLog = XmlHelper.GetString(node, "il", null);
        UpdateLog = XmlHelper.GetString(node, "ul", null);
        DeleteLog = XmlHelper.GetString(node, "dl", null);
        if (AfterQuery == "bl")
            AfterQuery = BeforeQuery;
    }

    public void RunTrigger(int id)
    {
        if (string.IsNullOrEmpty(TriggerMethod))
            return;
        try
        {
            Type t = typeof(Triggers);
            System.Reflection.MethodInfo method = t.GetMethod(TriggerMethod);
            if (method != null)
                method.Invoke(null, new object[] { id });
        }
        catch (Exception ex)
        {
            Logger.Log("ex");
        }
    }
}

public class QueryController
{
    public static QueryController Instance = new QueryController();
    private QueryController()
    {
    }
    public Dictionary<string, QueryInfo> Queries
    {
        get
        {
            if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Application == null)
                return null;
            // todo : remove following true
            if (SiteSettings.DevelopeMode || System.Web.HttpContext.Current.Application["QueryController_Queries"] == null)
                System.Web.HttpContext.Current.Application["QueryController_Queries"] = LoadQueries();
            return (Dictionary<string, QueryInfo>)System.Web.HttpContext.Current.Application["QueryController_Queries"];
        }
    }
    public QueryInfo this[string name]
    {
        get
        {
            if (string.IsNullOrEmpty(name))
                return null;
            name = name.ToLower();
            Dictionary<string, QueryInfo> queries = Queries;
            if (queries != null && queries.ContainsKey(name))
                return queries[name];
            return null;
        }
    }
    private static string[] FilePath
    {
        get
        {
            if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Server == null)
                return null;
            return new string[] { System.Web.HttpContext.Current.Server.MapPath("~/App_Data/queries.xml")
            , System.Web.HttpContext.Current.Server.MapPath("~/App_Data/autos.xml")};
            //return new string[]{System.Web.HttpContext.Current.Server.MapPath("~/user0/data/queries.xml")
            //    , System.Web.HttpContext.Current.Server.MapPath("~/user1/data/queries.xml")
            //    , System.Web.HttpContext.Current.Server.MapPath("~/user2/data/queries.xml")};
        }
    }
    private static Dictionary<string, QueryInfo> LoadQueries()
    {
        string[] pathes = FilePath;
        if (pathes == null)
            return null;
        Dictionary<string, QueryInfo> res = new Dictionary<string, QueryInfo>();
        foreach (string path in pathes)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);
            for (int i = 0; i < xmlDoc.DocumentElement.ChildNodes.Count; i++)
            {
                QueryInfo info = new QueryInfo(xmlDoc.DocumentElement.ChildNodes[i]);
                if (!res.ContainsKey(info.Name.ToLower()))
                    res.Add(info.Name.ToLower(), info);
            }
        }
        return res;
    }
    public void Reload()
    {
        if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Application == null)
            return;
        System.Web.HttpContext.Current.Application.Remove("QueryController_Queries");
        System.Web.HttpContext.Current.Application["QueryController_Queries"] = null;
    }
}