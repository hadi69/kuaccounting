﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Xml;
using System.Text;

public class TableInfo
{
    public string List, Detail, Print, ListQuery, DetailQuery, DeleteQuery;
    string mName, mTitle;
    public string Name { get { return mName; } set { mName = value; } }
    public string Title { get { return mTitle; } set { mTitle = value; } }
    public SyncLevel SyncLevel = SyncLevel.None;
    /*
     *      st += 1;    فاکتور فروش و برنامه روزانه
            st += 2;    فاکتور برگشت کالا
            st += 4;    انبار، کالاها، بخشنامه ها
            st += 8;    اشخاص، کاربران
            st += 16;   چکهای دریافتی
            st += 32;   چکهای پرداختی
            st += 64;   اسناد حسابداری، سرفصلها
            st += 128;  سایر جداول
     */
    public int SyncType = 0;
    public List<FieldInfo> Fields = new List<FieldInfo>();
    public bool ReadOnly = false;
    public TableInfo(XmlNode node)
    {
        if (node == null)
            return;
        Name = XmlHelper.GetString(node, "Name", null);
        Title = LangProvider.Instance[XmlHelper.GetString(node, "Title", Name)];
        List = XmlHelper.GetString(node, "List", null);
        Detail = XmlHelper.GetString(node, "Detail", null);
        Print = XmlHelper.GetString(node, "Print", Detail);
        ListQuery = XmlHelper.GetString(node, "LQ", null);
        DetailQuery = XmlHelper.GetString(node, "DQ", null);
        DeleteQuery = XmlHelper.GetString(node, "RQ", null);
        SyncLevel = (SyncLevel)XmlHelper.GetInt(node, "s", (int)SyncLevel.Both);
        SyncType = XmlHelper.GetInt(node, "st", 128); // 128 = others
        ReadOnly = XmlHelper.GetBool(node, "ro", ReadOnly);
        XmlNode fields = node.SelectSingleNode("Fields");
        if (fields != null)
            for (int i = 0; i < fields.ChildNodes.Count; i++)
            {
                FieldInfo field = new FieldInfo(fields.ChildNodes[i]);
                field.Owner = this;
                Fields.Add(field);
            }

        if (SiteSettings.EnableSync && this.SyncLevel != SyncLevel.None)
        {
            Fields.Add(new FieldInfo(null) { Name = "SyncVer", Type = "bigint", Nullable = true });
            Fields.Add(new FieldInfo(null) { Name = "TabletUniqueID", Type = "int", Nullable = true });
            Fields.Add(new FieldInfo(null) { Name = "TabletSyncVer", Type = "int", Nullable = true });
            Fields.Add(new FieldInfo(null) { Name = "TabletID", Type = "int", Nullable = true });
        }
    }
    public FieldInfo GetField(string fieldName)
    {
        fieldName = fieldName.ToLower();
        for (int i = 0; i < Fields.Count; i++)
            if (Fields[i].Name.ToLower() == fieldName)
                return Fields[i];
        return null;
    }
    public string PrepareValue(string fieldName, string value)
    {
        return PrepareValue(fieldName, value, false);
    }
    public string PrepareValue(string fieldName, string value, bool tabletDate)
    {
        FieldInfo f = GetField(fieldName);
        if (f == null)
            return value == null ? "" : value;
        if (f.Nullable && string.IsNullOrEmpty(value))
        {
            if (!string.IsNullOrEmpty(f.Type) && f.Type.ToLower().StartsWith("bit"))
                return "0";
            return "NULL";
        }
        //  it occures from tablet synchronizations
        if (f.Nullable && !string.IsNullOrEmpty(f.Foriegn) && (value == "0" || value == "-1"))
            return "NULL";
        if (string.IsNullOrEmpty(f.Type))
            return value == null ? "" : value;
        string type = f.Type.ToLower();
        if (type.StartsWith("nvarchar") || type.StartsWith("varchar") || type.StartsWith("ntext") || type.StartsWith("text"))
            return f.Encoded ? Helper.QoutedString(EncDec.Encrypt(value, GetEnDeP()), f.Nullable) : Helper.QoutedString(value, f.Nullable);
        if (type.StartsWith("bit"))
            return value == null ? "0" : (value.ToLower() == "on" || value.ToLower() == "checked" || value.ToLower() == "1" || value.ToLower() == "true") ? "1" : "0";
        if (type.StartsWith("date"))
            return Helper.QoutedDateTime(tabletDate ? Helper.ParseTabletDate(value, Helper.NullDate) : Helper.ParseDate(value, Helper.NullDate));
        // numeric
        return value == null ? "" : value;
    }
    private string GetEnDeP()
    {
        byte[] b = new byte[] { 116, 117, 114, 107, 107, 101, 114, 101, 64, 107, 110, 107, 109, 107, 122, 122, 113, 110, 97, 33, 36, 37, 49 };
        return System.Text.UTF8Encoding.UTF8.GetString(b);
    }
    public string DisplayValue(string fieldName, object value)
    {
        FieldInfo f = GetField(fieldName);
        if (f == null || string.IsNullOrEmpty(f.Type))
            return value == null ? "" : value.ToString();
        string type = f.Type.ToLower();
        if (type.StartsWith("nvarchar") || type.StartsWith("varchar") || type.StartsWith("ntext") || type.StartsWith("text"))
            try
            {
                return value == null ? "" : f.Encoded ? EncDec.Decrypt(value.ToString(), GetEnDeP()) : value.ToString();
            }
            catch
            {
                return value == null ? "" : value.ToString();
            }
        if (type.StartsWith("bit"))
            return value == null || value.ToString() == "0" || value.ToString().ToLower() == "false" ? "" : "checked";
        if (type.StartsWith("date"))
            return Helper.FormatDate(Helper.GetDateTime(value, Helper.NullDate), null);
        // numeric
        return value == null ? "" : value.ToString();
    }
    public override bool Equals(object obj)
    {
        if (obj is TableInfo)
            return Name == (obj as TableInfo).Name;
        return base.Equals(obj);
    }
    public override int GetHashCode()
    {
        return Name.GetHashCode();
    }

    #region BuildWhere
    private string BuildWhereConditionBare(string columnName, string columnValue)
    {
        FieldInfo field = this.GetField(columnName);
        if (field == null)
        {
            // check <= and >=
            bool cast2int = false;
            if (columnName.EndsWith("_cast2int"))
            {
                cast2int = true;
                columnName = columnName.Substring(0, columnName.Length - "_cast2int".Length);
            }
            if (columnName.EndsWith("_von"))
            {
                string baseCol = columnName.Substring(0, columnName.Length - "_von".Length);
                field = this.GetField(baseCol);
                if (field != null)
                {
                    if (cast2int)
                        return "CAST(" + baseCol + " AS INT) >=" + columnValue;
                    if (field.Type == "int" || (field.Type != null && field.Type.ToLower().StartsWith("numeric"))) // todo : other numerics
                        return baseCol + ">=" + columnValue;
                    if (field.Type == "datetime")
                        return "CONVERT(date," + baseCol + ") >= '" + Helper.ParseDate(columnValue, DateTime.Now.AddYears(-10)).ToString("yyyy-MM-dd") + "'";
                    // anything else
                    return "'" + baseCol + "' >= '" + columnValue + "'";
                }
                else
                {
                    // it is from another table, maybe from a View
                    if (cast2int)
                        return "CAST(" + baseCol + " AS INT) >=" + columnValue;
                    return baseCol + ">=" + columnValue;
                }
            }
            if (columnName.EndsWith("_bis"))
            {
                string baseCol = columnName.Substring(0, columnName.Length - "_bis".Length);
                field = this.GetField(baseCol);
                if (field != null)
                {
                    if (cast2int)
                        return "CAST(" + baseCol + " AS INT) <=" + columnValue;
                    if (field.Type == "int" || (field.Type != null && field.Type.ToLower().StartsWith("numeric"))) // todo : other numerics
                        return baseCol + "<=" + columnValue;
                    if (field.Type == "datetime")
                        return "CONVERT(date," + baseCol + ") <= '" + Helper.ParseDate(columnValue, DateTime.Now.AddYears(+10)).ToString("yyyy-MM-dd") + "'";
                    // anything else
                    return "'" + baseCol + "' <= '" + columnValue + "'";
                }
                else
                {
                    // it is from another table, maybe from a View
                    if (cast2int)
                        return "CAST(" + baseCol + " AS INT) <=" + columnValue;
                    return baseCol + "<=" + columnValue;
                }
            }
            //return whereCondition;
            if (Helper.TryGetDouble(columnValue, -1) == -1)
                return columnName + " LIKE N'%" + Helper.NormalizeChars(columnValue) + "%'";
            return columnName + "='" + columnValue + "'";
        }
        if (field.Type == "int") // todo : other numerics
            return columnName + "=" + columnValue;
        if (field.Type.StartsWith("nvarchar") || field.Type.StartsWith("ntext")) // todo: other strings
            return columnName + " LIKE N'%" + Helper.NormalizeChars(columnValue) + "%'";
        if (field.Type == "datetime")
            return "CONVERT(date," + columnName + ")='" + Helper.ConvertToGerigorian(columnValue) + "'";
        return columnName + " LIKE N'%" + Helper.NormalizeChars(columnValue) + "%'";
        return string.Empty;
    }
    public string BuildWhereCondition(string columnName, string columnValue)
    {
        string whereCondition = BuildWhereConditionBare(columnName, columnValue);
        return string.IsNullOrEmpty(whereCondition) ? whereCondition : (" WHERE " + whereCondition);
    }
    private string BuildWhereCondition(string[] columnNames, string columnValue)
    {
        if (columnNames == null || columnNames.Length == 0)
            return string.Empty;
        string whereCondition = " WHERE (";
        //string[] columnName = columnNames.Split('+');
        bool any = false;
        for (int i = 0; i < columnNames.Length; i++)
        {
            if (string.IsNullOrEmpty(columnNames[i]))
                continue;
            if (any)
                whereCondition += " OR ";
            whereCondition += "(" + BuildWhereConditionBare(columnNames[i], columnValue) + ")";
            any = true;
        }
        whereCondition += ")";
        return any ? whereCondition : string.Empty;
    }
    public string BuildWhereConditionFromForm()
    {
        string whereCondition = " WHERE (";
        bool any = false;
        bool isOr = HttpContext.Current.Request.Form["comb"] == "or";
        for (int i = 0; i < HttpContext.Current.Request.Form.AllKeys.Length; i++)
        {
            if (!HttpContext.Current.Request.Form.AllKeys[i].StartsWith("_q_"))
                continue;
            if (any)
                whereCondition += isOr ? " OR " : " AND ";

            string colName = HttpContext.Current.Request.Form.AllKeys[i].Substring("_q_".Length);
            string colVal = HttpContext.Current.Request.Form[HttpContext.Current.Request.Form.AllKeys[i]];
            if (!string.IsNullOrEmpty(colVal))
            {
                if (colName.EndsWith("_multi"))
                {
                    colName = colName.Substring(0, colName.Length - "_multi".Length);
                    string[] colVals = colVal.Split(',');
                    for (int c = 0; c < colVals.Length; c++)
                    {
                        whereCondition += "(" + BuildWhereConditionBare(colName, colVals[c]) + ")";
                        if (c < colVals.Length - 1)
                            whereCondition += isOr ? " OR " : " AND ";
                    }
                }
                else
                    whereCondition += "(" + BuildWhereConditionBare(colName, colVal) + ")";

                any = true;
            }
        }
        whereCondition += ")";
        return any ? whereCondition : string.Empty;
    }
    public string BuildWhereConditionFromUrl()
    {
        string whereCondition = " WHERE (";
        bool any = false;
        bool isOr = HttpContext.Current.Request.QueryString["comb"] == "or";
        for (int i = 0; i < HttpContext.Current.Request.QueryString.AllKeys.Length; i++)
        {
            if (!HttpContext.Current.Request.QueryString.AllKeys[i].StartsWith("_q_"))
                continue;
            if (any)
                whereCondition += isOr ? " OR " : " AND ";
            string colName = HttpContext.Current.Request.QueryString.AllKeys[i].Substring("_q_".Length);
            string colVal = HttpContext.Current.Request.QueryString[HttpContext.Current.Request.QueryString.AllKeys[i]];

            if (colName.EndsWith("_multi"))
            {
                colName = colName.Substring(0, colName.Length - "_multi".Length);
                string[] colVals = colVal.Split(',');
                for (int c = 0; c < colVals.Length; c++)
                {
                    whereCondition += "(" + BuildWhereConditionBare(colName, colVals[c]) + ")";
                    if (c < colVals.Length - 1)
                        whereCondition += isOr ? " OR " : " AND ";
                }
            }
            else
                whereCondition += "(" + BuildWhereConditionBare(colName, colVal) + ")";
            any = true;
        }
        whereCondition += ")";
        return any ? whereCondition : string.Empty;
    }
    #endregion
}
public class FieldInfo
{
    string mName, mTitle;
    public string Name { get { return mName; } set { mName = value; } }
    public string Title { get { return mTitle; } set { mTitle = value; } }
    public string Type;
    public bool Nullable = false;
    public bool Identity = false;
    public string Foriegn = null;
    public bool Encoded = false;
    public bool CascadeDelete = false;
    public string Format = null;
    public TableInfo Owner = null;
    public FieldInfo(XmlNode node)
    {
        if (node != null)
        {
            Name = XmlHelper.GetString(node, "n", null);
            Title = LangProvider.Instance[XmlHelper.GetString(node, "t", Name)];
            Type = XmlHelper.GetString(node, "tp", null);
            Nullable = XmlHelper.GetBool(node, "nl", true);
            Identity = XmlHelper.GetBool(node, "identity", false);
            Foriegn = XmlHelper.GetString(node, "foriegn", null);
            Encoded = XmlHelper.GetBool(node, "enc", Encoded);
            CascadeDelete = XmlHelper.GetBool(node, "d", CascadeDelete);
            Format = XmlHelper.GetString(node, "f", Format);
        }
    }
    public string Display
    {
        get
        {
            return string.IsNullOrEmpty(Title) ? Name : Title;
        }
    }

    /// <summary>
    /// Full name = Table.Name + Field.Name
    /// </summary>
    public string FullName
    {
        get
        {
            return string.Format("{0}.{1}", Owner.Name, Name);
        }
    }
    public string FullTitle
    {
        get
        {
            return string.Format("{0}.{1}", Owner.Title, string.IsNullOrEmpty(mTitle) ? mName : mTitle);
        }
    }
    public string TableTitle
    {
        get
        {
            return Owner.Title;
        }
    }
    public override bool Equals(object obj)
    {
        if (obj is FieldInfo)
            return FullName == (obj as FieldInfo).FullName;
        return base.Equals(obj);
    }
    public override int GetHashCode()
    {
        return FullName.GetHashCode();
    }
}

public class TableController
{
    public static TableController Instance = new TableController();
    private TableController()
    {
    }
    public Dictionary<string, TableInfo> Tables
    {
        get
        {
            if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Application == null)
                return null;
            // todo : remove following true
            if (SiteSettings.DevelopeMode || System.Web.HttpContext.Current.Application["TableController_Tables"] == null)
                System.Web.HttpContext.Current.Application["TableController_Tables"] = LoadTables();
            return (Dictionary<string, TableInfo>)System.Web.HttpContext.Current.Application["TableController_Tables"];
        }
    }
    public TableInfo this[string name]
    {
        get
        {
            if (string.IsNullOrEmpty(name))
                return null;
            name = name.ToLower();
            Dictionary<string, TableInfo> tables = this.Tables;
            if (tables != null && tables.ContainsKey(name))
                return tables[name];
            return null;
        }
    }
    private static string[] TableFilePath
    {
        get
        {
            if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Server == null)
                return null;
            return new string[] { System.Web.HttpContext.Current.Server.MapPath("~/App_Data/tables.xml") };
        }
    }
    private static string[] ProceduresFilePaths
    {
        get
        {
            if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Server == null)
                return null;
            return new string[] { System.Web.HttpContext.Current.Server.MapPath("~/App_Data/general.xml")
                , System.Web.HttpContext.Current.Server.MapPath("~/App_Data/procedures.xml")
                , System.Web.HttpContext.Current.Server.MapPath("~/App_Data/accounting.xml")};
        }
    }
    private static string[] TriggersFilePaths
    {
        get
        {
            if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Server == null)
                return null;
            return new string[] { System.Web.HttpContext.Current.Server.MapPath("~/App_Data/triggers.xml") };
        }
    }
    private static string[] FunctionsFilePath
    {
        get
        {
            if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Server == null)
                return null;
            return new string[] { System.Web.HttpContext.Current.Server.MapPath("~/App_Data/functions.xml")
                , System.Web.HttpContext.Current.Server.MapPath("~/App_Data/no.xml")};
        }
    }
    private static string[] ViewsFilePath
    {
        get
        {
            if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Server == null)
                return null;
            return new string[] { System.Web.HttpContext.Current.Server.MapPath("~/App_Data/views.xml") };
        }
    }
    private static Dictionary<string, TableInfo> LoadTables()
    {
        string[] pathes = TableFilePath;
        if (pathes == null)
            return null;
        Dictionary<string, TableInfo> res = new Dictionary<string, TableInfo>();
        foreach (string path in pathes)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);
            for (int i = 0; i < xmlDoc.DocumentElement.ChildNodes.Count; i++)
                if (xmlDoc.DocumentElement.ChildNodes[i].Name == "table")
                {
                    TableInfo info = new TableInfo(xmlDoc.DocumentElement.ChildNodes[i]);
                    if (!res.ContainsKey(info.Name.ToLower()))
                        res.Add(info.Name.ToLower(), info);
                }
        }
        if (SiteSettings.EnableSync)
        {
            // SyncVers to hold last SyncVer of tables
            TableInfo info = new TableInfo(null) { Name = "SyncVers", Title = "SyncVers" };
            info.Fields.Add(new FieldInfo(null) { Name = "ID", Type = "int", Identity = true });
            info.Fields.Add(new FieldInfo(null) { Name = "TableName", Type = "nvarchar(200)" });
            info.Fields.Add(new FieldInfo(null) { Name = "Counter", Type = "bigint" });
            res.Add(info.Name.ToLower(), info);

            // SyncOrder to hold the order of tables for synchronization
            info = new TableInfo(null) { Name = "SyncOrder", Title = "SyncOrder" };
            info.Fields.Add(new FieldInfo(null) { Name = "ID", Type = "int", Identity = true });
            info.Fields.Add(new FieldInfo(null) { Name = "TableName", Type = "nvarchar(200)" });
            info.Fields.Add(new FieldInfo(null) { Name = "Index", Type = "int" });
            info.Fields.Add(new FieldInfo(null) { Name = "Dependents", Type = "nvarchar(4000)" });
            info.Fields.Add(new FieldInfo(null) { Name = "SyncType", Type = "int", Nullable = true });
            res.Add(info.Name.ToLower(), info);
        }
        return res;
    }
    static XmlNode GetTableNode(XmlDocument xmlDoc, string name)
    {
        if (xmlDoc == null || name == null)
            return null;
        name = name.ToUpper();
        string str = "descendant::{0}[translate(@{1},'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')='{2}']";
        //XmlNode node = xmlDoc.DocumentElement.SelectSingleNode("descendant::table[@Name='" + path + "']", null);
        XmlNode node = xmlDoc.DocumentElement.SelectSingleNode(string.Format(str, "table", "Name", name), null);
        return node;
    }
    public static List<string> GetSetupQueries(string name)
    {
        string[] pathes = TableFilePath;
        if (pathes == null)
            return null;
        List<string> res = new List<string>();
        foreach (string path in pathes)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);
            XmlNode node = GetTableNode(xmlDoc, name);
            if (node == null)
                continue;
            node = node.SelectSingleNode("Setups");
            if (node == null || node.ChildNodes.Count == 0)
                continue;
            for (int i = 0; i < node.ChildNodes.Count; i++)
                res.Add(node.ChildNodes[i].InnerText);
        }
        return res;
    }
    public static List<string> GetDefaultProcedures()
    {
        string[] pathes = ProceduresFilePaths;
        if (pathes == null)
            return null;
        List<string> res = new List<string>();
        foreach (string p in pathes)
        {
            string path = p;//.Replace("tables.xml", "setups.xml");
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);
            XmlNode node = xmlDoc.DocumentElement;
            if (node == null || node.ChildNodes.Count == 0)
                continue;
            for (int i = 0; i < node.ChildNodes.Count; i++)
                res.Add(node.ChildNodes[i].InnerText);
        }


        if (SiteSettings.EnableSync)
        {
            List<string> order = new List<string>();
            Dictionary<string, string> dependents = new Dictionary<string, string>();
            Dictionary<string, TableInfo>.Enumerator enu = Instance.Tables.GetEnumerator();
            while (enu.MoveNext())
            {
                TableInfo info = enu.Current.Value;
                string me = info.Name.ToLower();
                if (!order.Contains(me))
                {
                    order.Add(me);
                    dependents.Add(me, "");
                }
                foreach (FieldInfo field in info.Fields)
                {
                    if (string.IsNullOrEmpty(field.Foriegn))
                        continue;
                    // the foriegn must come before me
                    string other = field.Foriegn.ToLower();
                    if (!order.Contains(other))
                    {
                        order.Insert(order.IndexOf(me), other);
                        dependents.Add(other, "");
                    }
                    else if (order.IndexOf(other) > order.IndexOf(me))
                    {
                        order.Remove(other);
                        order.Insert(order.IndexOf(me), other);
                    }
                    if (info.SyncLevel != SyncLevel.None)
                        dependents[other] += me + "." + field.Name + ";";
                }
            }
            StringBuilder orderQry = new StringBuilder();
            orderQry.AppendLine("DELETE FROM SyncOrder");
            for (int i = 0; i < order.Count; i++)
            {
                TableInfo info = Instance[order[i]];
                if (info == null || info.SyncLevel == SyncLevel.None)
                    continue;
                orderQry.AppendLine(string.Format("INSERT INTO SyncOrder (TableName, [Index], Dependents, SyncType) VALUES('{0}', {1}, '{2}', {3})"
                    , order[i], i, dependents[order[i]], info.SyncType));
            }
            res.Add(orderQry.ToString());
        }
        return res;
    }
    public static List<string> GetTriggers()
    {
        string[] pathes = TriggersFilePaths;
        if (pathes == null)
            return null;
        List<string> res = new List<string>();
        foreach (string p in pathes)
        {
            string path = p;//.Replace("tables.xml", "setups.xml");
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);
            XmlNode node = xmlDoc.DocumentElement;
            if (node == null || node.ChildNodes.Count == 0)
                continue;
            for (int i = 0; i < node.ChildNodes.Count; i++)
                res.Add(node.ChildNodes[i].InnerText);
        }

        return res;
    }
    public static List<string> GetFunctions()
    {
        string[] pathes = FunctionsFilePath;
        if (pathes == null)
            return null;
        List<string> res = new List<string>();
        foreach (string p in pathes)
        {
            string path = p;//.Replace("tables.xml", "setups.xml");
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);
            XmlNode node = xmlDoc.DocumentElement;
            if (node == null || node.ChildNodes.Count == 0)
                continue;
            for (int i = 0; i < node.ChildNodes.Count; i++)
                res.Add(node.ChildNodes[i].InnerText);
        }

        return res;
    }
    public static List<string> GetViews()
    {
        string[] pathes = ViewsFilePath;
        if (pathes == null)
            return null;
        List<string> res = new List<string>();
        foreach (string p in pathes)
        {
            string path = p;//.Replace("tables.xml", "setups.xml");
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);
            XmlNode node = xmlDoc.DocumentElement;
            if (node == null || node.ChildNodes.Count == 0)
                continue;
            for (int i = 0; i < node.ChildNodes.Count; i++)
                res.Add(node.ChildNodes[i].InnerText);
        }

        return res;
    }
    public static FieldInfo FindField(string fullName)
    {
        string[] split = fullName.Split('.');
        string tableName = split[0];
        string fieldName = split[1];
        TableInfo table = TableController.Instance[tableName];
        if (table != null)
        {
            FieldInfo field = table.GetField(fieldName);
            if (field != null)
                return field;
        }
        return null;
    }
    public void Reload()
    {
        if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Application == null)
            return;
        System.Web.HttpContext.Current.Application.Remove("TableController_Tables");
        System.Web.HttpContext.Current.Application["TableController_Tables"] = null;
    }

}

public enum SyncLevel
{
    /// <summary>
    /// No sync
    /// </summary>
    None = 0,
    /// <summary>
    /// Sync from/to Tablet
    /// </summary>
    Both = 1,
    /// <summary>
    /// Sync just from Server to tablet
    /// </summary>
    Outward = 2,
    /// <summary>
    /// Sync just from tablet to server
    /// </summary>
    InWard = 3
}