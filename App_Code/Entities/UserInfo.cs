﻿using System;
using System.Collections.Generic;

using System.Web;

/// <summary>
/// Summary description for UserInfo
/// </summary>
public class UserInfo
{
    public int ID;
    public string Username;
    public string Password;
    public string Fullname;
    private bool mIsAdmin;
    private bool mIsStoreUser;
    public bool Enabled;
    public int PersonnelID;
    public string PersonnelName;
    public string Mobile;
    private string mAccess;
    private bool mOnVacation = false;
    public string Comments;
    public string StartTime;
    public string EndTime;
    public DateTime RegisterDate;
    public string Access
    {
        get
        {
            return mAccess;
        }
        set
        {
            mAccess = value;
            if (!string.IsNullOrEmpty(mAccess))
            {
                mAccess = mAccess.ToLower();
                if (!mAccess.StartsWith(";"))
                    mAccess = ";" + mAccess;
                if (!mAccess.EndsWith(";"))
                    mAccess += ";";
            }
        }
    }
    public bool IsAdmin
    {
        get
        {
            return mIsAdmin;
        }
        set
        {
            mIsAdmin = value;
        }
    }
    public bool IsStoreUser
    {
        get
        {
            return mIsStoreUser;
        }
        set
        {
            mIsStoreUser = value;
        }
    }
    public bool OnVacation
    {
        get
        {
            return mOnVacation;
        }
        set
        {
            mOnVacation = value;
        }
    }
    private string mWardIDs;
    public string WardIDs
    {
        get
        {
            return mWardIDs;
        }
        set
        {
            mWardIDs = value;
            if (!string.IsNullOrEmpty(mWardIDs))
                mWardIDs = mWardIDs.ToLower().Trim(',').Trim();
            else
                mWardIDs = "";
        }
    }
    
    public bool HasAccess(string table, AccessType access)
    {
        return true;
        if (mIsAdmin)
            return true;
        if (string.IsNullOrEmpty(mAccess) || string.IsNullOrEmpty(table))
            return false;
        table = table.ToLower();
        return mAccess.Contains(";" + table + "_");
        switch (access)
        {
            case AccessType.View:
                table += "_v";
                break;
            case AccessType.Add:
                table += "_a";
                break;
            case AccessType.Edit:
                table += "_e";
                break;
            case AccessType.Delete:
                table += "_d";
                break;
            default:
                return false;
        }
        return mAccess.Contains(";" + table + ";");
    }
    public bool HasAccess(string what)
    {
        if (mIsAdmin)
            return true;
        if (string.IsNullOrEmpty(mAccess) || string.IsNullOrEmpty(what))
            return false;
        what = what.ToLower();
        return mAccess.Contains(";" + what + ";");
    }
}

public enum AccessType
{
    View, Add, Edit, Delete
}