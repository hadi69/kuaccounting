﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Xml;
using System.Data;
using Newtonsoft.Json;
using System.Web.Mvc;
using System.Text;

/// <summary>
/// Summary description for service
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class service : System.Web.Services.WebService
{

    public service()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod()]
    public string GetSyncRows()
    {
        System.Diagnostics.Stopwatch stopWatch = System.Diagnostics.Stopwatch.StartNew();
        //Type dateType = typeof(DateTime);
        StringBuilder builder = new StringBuilder();
        // to pass the start xml line
        builder.AppendLine("");

        //Fields: ID,Name,Surname,....
        //NumRows
        //Row1.Field1
        //Row1.Field2
        //Row1.Field3
        //Row2.....
        string table = HttpContext.Current.Request.QueryString["table"];
        using (DbProxy proxy = new DbProxy())
        {
            try
            {
                int ver = Helper.GetInt(HttpContext.Current.Request.QueryString["ver"], -1);
                string userName = Helper.GetString(HttpContext.Current.Request.QueryString["u"], null);
                string pass = Helper.GetString(HttpContext.Current.Request.QueryString["p"], null);
                int visitorID = -1; // -1 means all.
                if (!string.IsNullOrEmpty(userName) || !string.IsNullOrEmpty(pass))
                {
                    visitorID = 0;
                    UserInfo userInfo = proxy.GetUser(userName, pass);
                    if (userInfo != null)
                        visitorID = userInfo.PersonnelID;
                }
                string query = string.Format("SELECT * FROM {0} WHERE SyncVer > {1}", table, ver);
                if (table.ToLower() == "SyncOrder".ToLower())
                    query = "SELECT * FROM SyncOrder";
                else if (table.ToLower() == "PersonnelRep".ToLower())
                {
                    System.Globalization.PersianCalendar pCalendar = new System.Globalization.PersianCalendar();
                    DateTime lastMonth = pCalendar.ToDateTime(pCalendar.GetYear(DateTime.Now), pCalendar.GetMonth(DateTime.Now), 1, 0, 0, 0, 0).AddMonths(-1);
                    #region Prepare report
                    query = @"
-- DELETE deleted persons
DELETE FROM PersonnelRep WHERE NOT PersonnelID IN (SELECT ID FROM Personnel);
-- update existing persons
UPDATE PersonnelRep SET
	SumDue = ISNULL((SELECT SUM(ISNULL(Due, 0)) FROM DocumentAccountingItems WHERE DocumentAccountingItems.PersonnelID = PersonnelRep.PersonnelID
            AND TreePath IN (SELECT TreePath FROM Trees WHERE PersonNeed=1)), 0)
	, SumOwe = ISNULL((SELECT SUM(ISNULL(Owe, 0)) FROM DocumentAccountingItems WHERE DocumentAccountingItems.PersonnelID = PersonnelRep.PersonnelID
            AND TreePath IN (SELECT TreePath FROM Trees WHERE PersonNeed=1)), 0)
	, NumNewChecks = ISNULL((SELECT COUNT(Checks.ID) FROM Checks WHERE Checks.PersonnelID = PersonnelRep.PersonnelID
            AND NOT Checks.Status IN (1 , 2, 4, 5, 7, 9)), 0) 
    , SumNewChecks = ISNULL((SELECT SUM(ISNULL(CONVERT(numeric(18,0), Checks.Amount), 0)) FROM Checks WHERE Checks.PersonnelID = PersonnelRep.PersonnelID
        AND NOT Checks.Status IN (1 , 2, 4, 5, 7, 9)), 0) 
    , NumRefusedChecks = ISNULL((SELECT COUNT(Checks.ID) FROM Checks WHERE Checks.PersonnelID = PersonnelRep.PersonnelID
        AND (Checks.Status = 2)), 0) 
    , SumRefusedChecks = ISNULL((SELECT SUM(ISNULL(CONVERT(numeric(18,0), Checks.Amount), 0)) FROM Checks WHERE Checks.PersonnelID = PersonnelRep.PersonnelID
        AND (Checks.Status = 2)), 0) 
    , SumBuy2Month = ISNULL((SELECT SUM(ISNULL(StoreDraft.TotalPrice, 0)) FROM StoreDraft WHERE StoreDraft.BuyerID = PersonnelRep.PersonnelID AND WorkflowStatus=1
        AND (StoreDraft.FactorDate >= lastMonth)), 0) 
    , SumBuy12Month = ISNULL((SELECT SUM(ISNULL(StoreDraft.TotalPrice, 0)) FROM StoreDraft WHERE StoreDraft.BuyerID = PersonnelRep.PersonnelID AND WorkflowStatus=1
        AND (StoreDraft.FactorDate >= GETDATE() - 365)), 0) 
-- insert new personnel
INSERT INTO PersonnelRep (PersonnelID, SumDue, SumOwe, NumNewChecks, SumNewChecks, NumRefusedChecks, SumRefusedChecks, SumBuy2Month, SumBuy12Month)
	SELECT Personnel.ID
		, ISNULL((SELECT SUM(ISNULL(Due, 0)) FROM DocumentAccountingItems WHERE PersonnelID=Personnel.ID
            AND TreePath IN (SELECT TreePath FROM Trees WHERE PersonNeed=1)), 0) SumDue
        , ISNULL((SELECT SUM(ISNULL(Owe, 0)) FROM DocumentAccountingItems WHERE PersonnelID=Personnel.ID
            AND TreePath IN (SELECT TreePath FROM Trees WHERE PersonNeed=1)), 0) SumOwe
        , ISNULL((SELECT COUNT(Checks.ID) FROM Checks WHERE Checks.PersonnelID=Personnel.ID
            AND NOT Checks.Status IN (1 , 2, 4, 5, 7, 9)), 0) NumNewChecks
        , ISNULL((SELECT SUM(ISNULL(CONVERT(numeric(18,0), Checks.Amount), 0)) FROM Checks WHERE Checks.PersonnelID=Personnel.ID
            AND NOT Checks.Status IN (1 , 2, 4, 5, 7, 9)), 0) SumNewChecks
        , ISNULL((SELECT COUNT(Checks.ID) FROM Checks WHERE Checks.PersonnelID=Personnel.ID
            AND (Checks.Status = 2)), 0) NumRefusedChecks
        , ISNULL((SELECT SUM(ISNULL(CONVERT(numeric(18,0), Checks.Amount), 0)) FROM Checks WHERE Checks.PersonnelID=Personnel.ID
            AND (Checks.Status = 2)), 0) SumRefusedChecks
        , ISNULL((SELECT SUM(ISNULL(StoreDraft.TotalPrice, 0)) FROM StoreDraft WHERE StoreDraft.BuyerID=Personnel.ID AND WorkflowStatus=1
            AND (StoreDraft.FactorDate >= lastMonth)), 0) SumBuy2Month
        , ISNULL((SELECT SUM(ISNULL(StoreDraft.TotalPrice, 0)) FROM StoreDraft WHERE StoreDraft.BuyerID=Personnel.ID AND WorkflowStatus=1
            AND (StoreDraft.FactorDate >= GETDATE() - 365)), 0) SumBuy12Month
	FROM Personnel 
	WHERE NOT Personnel.ID IN (SELECT PersonnelID FROM PersonnelRep)
SELECT * FROM PersonnelRep".Replace("lastMonth", Helper.QoutedDateTime(lastMonth));
                    #endregion
                }
                else if (table.ToLower() == "VisitorRep".ToLower())
                {
                    #region prepare visitor report
                    query = string.Format(@"
-- DELETE deleted persons
DELETE FROM VisitorRep WHERE NOT PersonnelID IN (SELECT ID FROM Personnel);
-- update existing persons
UPDATE VisitorRep SET
    SumVisitorChecks = ISNULL((SELECT SUM(ISNULL(Checks.Amount, 0)) FROM Checks WHERE Checks.VisitorID = VisitorRep.PersonnelID
        AND (Checks.Status = 6)), 0) 
	WHERE PersonnelID = {0}
-- insert new personnel
INSERT INTO VisitorRep (PersonnelID, SumVisitorChecks)
	SELECT Personnel.ID
        ,ISNULL((SELECT SUM(ISNULL(Checks.Amount, 0)) FROM Checks WHERE Checks.PersonnelID=Personnel.ID
            AND (Checks.Status = 2)), 0) SumVisitorChecks
	FROM Personnel 
	WHERE Personnel.ID = {0} AND NOT Personnel.ID IN (SELECT PersonnelID FROM VisitorRep) 
SELECT * FROM VisitorRep WHERE PersonnelID = {0}", visitorID);
                    #endregion
                }
                else if (table.ToLower() == "SaleTypeItem".ToLower())
                    query += " AND (Deleted IS NULL OR Deleted = 0)";
                if (visitorID >= 0)
                {
                    #region filter by visitor
                    if (table.ToLower() == "Checks".ToLower())
                        query += " AND VisitorID = " + visitorID;
                    else if (table.ToLower() == "VisitorPlan".ToLower())
                        query += " AND EndDate >= (GETDATE() - 1) AND VisitorID = " + visitorID;
                    else if (table.ToLower() == "VisitorPlanDays".ToLower())
                        query += " AND VisitDate >= (GETDATE() - 1) AND VisitorPlanID IN (SELECT ID FROM VisitorPlan WHERE VisitorID = " + visitorID + ")";
                    else if (table.ToLower() == "DailyCustomers".ToLower())
                        query += " AND VisitorPlanDaysID IN (SELECT VisitorPlanDays.ID FROM VisitorPlanDays INNER JOIN VisitorPlan ON VisitorPlanDays.VisitorPlanID = VisitorPlan.ID WHERE VisitDate >= (GETDATE() - 1) AND VisitorPlan.VisitorID = " + visitorID + ")";
                    else if (table.ToLower() == "VisitorCustomersStatus".ToLower())
                        query += " AND DailyCustomersID IN (SELECT DailyCustomers.ID FROM DailyCustomers INNER JOIN VisitorPlanDays ON DailyCustomers.VisitorPlanDaysID = VisitorPlanDays.ID INNER JOIN VisitorPlan ON VisitorPlanDays.VisitorPlanID = VisitorPlan.ID WHERE VisitDate >= (GETDATE() - 1) AND VisitorPlan.VisitorID = " + visitorID + ")";
                    else if (table.ToLower() == "VisitorFixedRoutes".ToLower())
                        query += " AND (ToDate IS NULL OR ToDate >= (GETDATE() - 1)) AND VisitorID = " + visitorID;
                    else if (table.ToLower() == "VisitorFixedRoutesItem".ToLower())
                        query += " AND VisitorFixedRoutesID IN (SELECT ID FROM VisitorFixedRoutes WHERE (ToDate IS NULL OR ToDate >= (GETDATE() - 1)) AND VisitorID = " + visitorID + ")";
                    else if (table.ToLower() == "VisitorFixedCustomers".ToLower())
                        query += " AND VisitorFixedRoutesItemID IN (SELECT ID FROM VisitorFixedRoutesItem WHERE VisitorFixedRoutesID IN (SELECT ID FROM VisitorFixedRoutes WHERE (ToDate IS NULL OR ToDate >= (GETDATE() - 1)) AND VisitorID = " + visitorID + "))";
                    else if (table.ToLower() == "Personnel".ToLower())
                        //  we want no SyncVer for this table
                        query = string.Format(@" SELECT * FROM Personnel WHERE (
ID = {0} 
OR ID IN (SELECT CustomerID FROM VisitorFixedCustomers 
	INNER JOIN VisitorFixedRoutesItem ON VisitorFixedCustomers.VisitorFixedRoutesItemID = VisitorFixedRoutesItem.ID
	INNER JOIN VisitorFixedRoutes ON VisitorFixedRoutesItem.VisitorFixedRoutesID = VisitorFixedRoutes.ID 
	WHERE VisitorFixedRoutes.VisitorID = {0})
OR ID IN (SELECT StoreDraft.BuyerID FROM StoreDraft 
	INNER JOIN dbo.WorkflowImpl ON dbo.StoreDraft.ID=dbo.WorkflowImpl.RelatedID
	INNER JOIN dbo.Workflow ON dbo.WorkflowImpl.WorkflowID=Workflow.ID
	INNER JOIN dbo.TaskImpl ON dbo.WorkflowImpl.ID=dbo.TaskImpl.WorkflowImplID
	WHERE StoreDraft.DistAgentID = {0} AND TaskImpl.ExitDate IS NULL AND Workflow.Type = 6	
		AND TaskID IN (SELECT OutputFromServerTaskID FROM TabletWorkFlow WHERE Type = 1 AND WorkFlowType = 6)
	)
)"
                            , visitorID);
                    else if (table.ToLower() == "PersonnelRep".ToLower())
                        query += string.Format(@" WHERE PersonnelID = {0} 
OR PersonnelID IN (SELECT CustomerID FROM VisitorFixedCustomers 
	INNER JOIN VisitorFixedRoutesItem ON VisitorFixedCustomers.VisitorFixedRoutesItemID = VisitorFixedRoutesItem.ID
	INNER JOIN VisitorFixedRoutes ON VisitorFixedRoutesItem.VisitorFixedRoutesID = VisitorFixedRoutes.ID 
	WHERE VisitorFixedRoutes.VisitorID = {0})
OR PersonnelID IN (SELECT StoreDraft.BuyerID FROM StoreDraft 
	INNER JOIN dbo.WorkflowImpl ON dbo.StoreDraft.ID=dbo.WorkflowImpl.RelatedID
	INNER JOIN dbo.Workflow ON dbo.WorkflowImpl.WorkflowID=Workflow.ID
	INNER JOIN dbo.TaskImpl ON dbo.WorkflowImpl.ID=dbo.TaskImpl.WorkflowImplID
	WHERE StoreDraft.DistAgentID = {0} AND TaskImpl.ExitDate IS NULL AND Workflow.Type = 6	
		AND TaskID IN (SELECT OutputFromServerTaskID FROM TabletWorkFlow WHERE Type = 1 AND WorkFlowType = 6)
	)"
                            , visitorID);
                    #endregion
                }

                // TabletWorkflow and DistReport
                #region DistReport
                if (table.ToLower() == "StoreDraft".ToLower())
                {
                    if (visitorID <= 0)
                        query = "SELECT * FROM StoreDraft WHERE 1 = 0";
                    else
                        // we want no SyncVer for this table
                        query = string.Format(@"SELECT * FROM StoreDraft WHERE ID IN (SELECT StoreDraft.ID FROM StoreDraft 
	INNER JOIN dbo.WorkflowImpl ON dbo.StoreDraft.ID=dbo.WorkflowImpl.RelatedID
	INNER JOIN dbo.Workflow ON dbo.WorkflowImpl.WorkflowID=Workflow.ID
	INNER JOIN dbo.TaskImpl ON dbo.WorkflowImpl.ID=dbo.TaskImpl.WorkflowImplID
	WHERE StoreDraft.DistAgentID = {0} AND TaskImpl.ExitDate IS NULL AND Workflow.Type = 6	
		AND TaskID IN (SELECT OutputFromServerTaskID FROM TabletWorkFlow WHERE Type = 1 AND WorkFlowType = 6)
	)"
                            , visitorID);
                }
                else if (table.ToLower() == "StoreDraftItems".ToLower())
                {
                    if (visitorID <= 0)
                        query = "SELECT * FROM StoreDraftItems WHERE 1 = 0";
                    else
                        // we want no SyncVer for this table
                        query = string.Format(@" SELECT * FROM StoreDraftItems WHERE StoreDraftID IN (SELECT StoreDraft.ID FROM StoreDraft 
	INNER JOIN dbo.WorkflowImpl ON dbo.StoreDraft.ID=dbo.WorkflowImpl.RelatedID
	INNER JOIN dbo.Workflow ON dbo.WorkflowImpl.WorkflowID=Workflow.ID
	INNER JOIN dbo.TaskImpl ON dbo.WorkflowImpl.ID=dbo.TaskImpl.WorkflowImplID
	WHERE StoreDraft.DistAgentID = {0} AND TaskImpl.ExitDate IS NULL AND Workflow.Type = 6	
		AND TaskID IN (SELECT OutputFromServerTaskID FROM TabletWorkFlow WHERE Type = 1 AND WorkFlowType = 6)
	)"
                            , visitorID);
                }
                #endregion

                if (table.ToLower() == "PayCheck".ToLower())
                    query += " AND Status <> 1 AND Status <> 2"; //0:New;1:Received;2:Refused;7:Guarantee;8:TakeBack
                else if (table.ToLower() == "Checks".ToLower())
                {
                    if (visitorID <= 0)
                        query += " AND Status <> 1 AND Status <> 2"; //0:New;1:Received;2:Refused;3:Sleep;4:Expend;5:SleepReceive;6:Visitor;7:Guarantee;9:GiveBack
                    else
                        // we want no SyncVer for this table
                        query += string.Format(@" AND ((Status <> 1 AND Status <> 2) OR ID IN
(
SELECT CheckID FROM StoreDraft WHERE ID IN (SELECT StoreDraft.ID FROM StoreDraft 
	INNER JOIN dbo.WorkflowImpl ON dbo.StoreDraft.ID=dbo.WorkflowImpl.RelatedID
	INNER JOIN dbo.Workflow ON dbo.WorkflowImpl.WorkflowID=Workflow.ID
	INNER JOIN dbo.TaskImpl ON dbo.WorkflowImpl.ID=dbo.TaskImpl.WorkflowImplID
	WHERE StoreDraft.DistAgentID = {0} AND TaskImpl.ExitDate IS NULL AND Workflow.Type = 6	
		AND TaskID IN (SELECT OutputFromServerTaskID FROM TabletWorkFlow WHERE Type = 1 AND WorkFlowType = 6)
	)
))"
                            , visitorID);

                }
                else if (table.ToLower() == "Service".ToLower())
                    query += " AND Enabled = 1";
                else if (table.ToLower() == "ServicePrice".ToLower())
                    query += " AND ServiceID IN (SELECT ID FROM Service WHERE Enabled = 1)";
                else if (table.ToLower() == "StoreServiceDef".ToLower())
                    query += " AND ServicePriceID IN (SELECT ID FROM ServicePrice WHERE ServiceID IN (SELECT ID FROM Service WHERE Enabled = 1))";
                else if (table.ToLower() == "StoreServices".ToLower())
                    query += " AND StoreServiceDefID IN (SELECT StoreServiceDef.ID FROM StoreServiceDef INNER JOIN ServicePrice ON StoreServiceDef.ServicePriceID=ServicePrice.ID WHERE ServiceID IN (SELECT ID FROM Service WHERE Enabled = 1))";
                DataTable dt = proxy.FillData(query, "ohMy");
                if (dt == null)
                {
                    dt = null;
                }

                // Fields
                builder.AppendLine("fIelDS:");
                for (int i = 0; i < dt.Columns.Count - 1; i++)
                    builder.Append("[" + dt.Columns[i].ColumnName + "],");
                builder.AppendLine("[" + dt.Columns[dt.Columns.Count - 1].ColumnName + "]");

                TableInfo info = TableController.Instance[table];
                if (table.ToLower() != "SyncOrder".ToLower() && info != null && (info.SyncLevel == SyncLevel.None || info.SyncLevel == SyncLevel.InWard))
                {
                    // NumRows
                    builder.AppendLine("0");
                }
                else
                {
                    // NumRows
                    builder.AppendLine(dt.Rows.Count.ToString());

                    if (table.ToLower() == "users".ToLower())
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            for (int j = 0; j < dt.Columns.Count; j++)
                                if (dt.Columns[j].ColumnName.ToLower() == "password")
                                {
                                    string password = Helper.GetString(dt.Rows[i][j], "");
                                    try
                                    {
                                        password = EncDec.Decrypt(password, GetEnDeP());
                                    }
                                    catch { }
                                    builder.AppendLine(Helper.GetString(password, "").Replace("\r", " ").Replace("\n", " "));
                                }
                                else
                                    builder.AppendLine(Helper.GetString(dt.Rows[i][j], "").Replace("\r", " ").Replace("\n", " "));
                        }
                    }
                    else
                    {
                        // Rows
                        Type boolType = typeof(bool), dateType = typeof(DateTime);
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            for (int j = 0; j < dt.Columns.Count; j++)
                            {
                                if (dt.Columns[j].DataType == dateType && dt.Rows[i][j] != null && !(dt.Rows[i][j] is DBNull))
                                {
                                    DateTime date = Helper.GetDateTime(dt.Rows[i][j], Null.NullDate);
                                    if (date != Null.NullDate)
                                        builder.AppendLine(date.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                                    else
                                        builder.AppendLine("");
                                }
                                else if (dt.Columns[j].DataType == boolType && dt.Rows[i][j] != null && !(dt.Rows[i][j] is DBNull))
                                    builder.AppendLine((bool)dt.Rows[i][j] ? "1" : "0");
                                else
                                    builder.AppendLine(Helper.GetString(dt.Rows[i][j], "").Replace("\r", " ").Replace("\n", " "));
                            }
                        }
                    }
                }
                dt.Dispose();
            }
            catch (Exception ex)
            {
                ExceptionLog.Instance.Add(ex);
            }
            builder.AppendLine("get_sync_rows_eof" + proxy.FillScalarString("SELECT Counter FROM SyncVers WHERE TableName = '" + table + "'"));
        }
        string res = builder.ToString();
        stopWatch.Stop();
        // ExceptionLog.Instance.Add(new Exception("GetSyncRows " + table + " : " + stopWatch.ElapsedMilliseconds));
        return res;
    }
    [WebMethod()]
    public string SetSyncRows()
    {
        // we get an XML from tablet:
        /*
        <?xml version="1.0" encoding="utf-8" ?>
        <Table name="StoreDraft">
          <R>
              <ID>1</ID>                    <--- ID in offline Database
              <Date>2010-01-12</Date> 
              <PersonnelID>5</PersonnelID>  <--- this is must be in Online Database, so we should synchronize table Personnel first
              <StoreID>7</StoreID>          <--- this is must be in Online Database, so we should synchronize table Stores first
          </R>
        </Table>
         */
        // the result is a mapping of offline IDs to new IDs
        //  OfflineID=OnlineID
        //  1=2001
        //  2=2020
        //  3=2021
        System.Diagnostics.Stopwatch stopWatch = System.Diagnostics.Stopwatch.StartNew();
        string xmlStr = HttpContext.Current.Request.Form["data"];
        XmlDocument xmlDoc = new XmlDocument();
        try
        {
            xmlDoc.LoadXml(xmlStr);
        }
        catch (Exception ex)
        {
            ExceptionLog.Instance.Add(ex, "Sync Error");
            return "<Invalid data: Error " + ex.Message + ">";
        }
        //string tableName = XmlHelper.GetString(xmlDoc.DocumentElement, "name", null);
        string tableName = HttpContext.Current.Request.QueryString["table"];
        int tabletUniqueID = Helper.GetInt(HttpContext.Current.Request.QueryString["tuid"], -1);
        int tabletSyncVer = Helper.GetInt(HttpContext.Current.Request.QueryString["tsync"], -1);

        TableInfo info = TableController.Instance[tableName];
        if (info == null)
            return null;


        List<string> fields = new List<string>();
        List<string> values = new List<string>();
        StringBuilder mapping = new StringBuilder();
        using (DbProxy proxy = new DbProxy())
            try
            {
                // tabletUniqueID is PersonnelID --> find userID
                int visitorUserID = Helper.GetInt(proxy.FillScalar("SELECT ID FROM Users WHERE PersonnelID = " + tabletUniqueID), -1);

                proxy.BeginTransaction();
                int storeID = Null.NullInteger, financeYearID = Null.NullInteger, isShop = 0;
                if (info.Name == "StoreDraft")
                    storeID = Helper.GetInt(proxy.FillScalar("SELECT TOP 1 ID FROM Stores"), storeID);
                if (info.Name == "StoreDraft" || info.Name == "Purchase" || info.Name == "Refuse" || info.Name == "Returns"
                    || info.Name == "DocumentAccounting" || info.Name == "Checks" || info.Name == "PayCheck")
                    financeYearID = Helper.GetInt(proxy.FillScalar("SELECT ID FROM FinanceYear WHERE Status=2"), financeYearID);
                if (info.Name == "Checks" || info.Name == "PayCheck")
                    isShop = Helper.GetInt(proxy.GetConfig("AppCheckIsShop", 0), isShop);
                for (int i = 0; i < xmlDoc.DocumentElement.ChildNodes.Count; i++)
                {
                    string offlineID = "";
                    bool isUpdate = false;
                    values.Clear();
                    fields.Clear();
                    for (int j = 0; j < xmlDoc.DocumentElement.ChildNodes[i].ChildNodes.Count; j++)
                    {
                        XmlNode node = xmlDoc.DocumentElement.ChildNodes[i].ChildNodes[j];
                        string field = node.Name.ToLower();
                        if (field == "tabletuniqueid" || field == "tabletsyncver" || field == "tabletid")
                            continue;
                        string value = node.InnerText;
                        if (field == "id")
                        {
                            offlineID = value;
                            fields.Add("TabletUniqueID");
                            values.Add(tabletUniqueID.ToString());
                            fields.Add("TabletSyncVer");
                            values.Add(tabletSyncVer.ToString());
                            fields.Add("TabletID");
                            values.Add(offlineID);
                        }
                        else if (field == "syncver")
                            isUpdate = value == "-2";
                        else
                        {
                            value = info.PrepareValue(field, value, true);
                            fields.Add(field);
                            if (field == "noserial")
                            {
                                #region noserial
                                if (info.Name == "StoreDraft")
                                    values.Add(string.Format("dbo.NeuStoreDraftNo({0}, NULL, NULL, NULL, 0)", value));
                                else if (info.Name == "Purchase")
                                    values.Add(string.Format("dbo.NeuPurchaseNo({0}, NULL, NULL, NULL)", value));
                                else if (info.Name == "Refuse")
                                    values.Add(string.Format("dbo.NeuRefuseNo({0}, NULL, NULL, NULL)", value));
                                else if (info.Name == "Returns")
                                    values.Add(string.Format("dbo.NeuReturnNo({0}, NULL, NULL, NULL)", value));
                                else
                                    values.Add(value);
                                #endregion
                            }
                            else if (field == "no")
                            {
                                if (info.Name == "DocumentAccounting")
                                    values.Add(string.Format("dbo.NeuVoucherNo({0}, NULL, NULL, NULL)", value));
                                else if (info.Name == "Checks")
                                    values.Add(string.Format("[dbo].[NeuCheckNo]({0}, NULL, NULL)", value));
                            }
                            else if (field == "isshop")
                            {
                                if (info.Name == "Checks" || info.Name == "PayCheck")
                                    values.Add(isShop.ToString());
                            }
                            else if (field == "code" && info.Name == "Personnel")
                            {
                                values.Add(string.Format("dbo.NeuPersonnelCode({0})", value));
                            }
                            else
                                values.Add(value);
                        }
                    }
                    #region storedraft
                    // storedraft.factordate
                    if (info.Name == "StoreDraft")
                    {
                        if (!fields.Contains("factordate") && fields.Contains("date"))
                        {
                            fields.Add("factordate");
                            values.Add(values[fields.IndexOf("date")]);
                        }
                        if (!fields.Contains("storeid") && storeID != Null.NullInteger)
                        {
                            fields.Add("storeid");
                            values.Add(storeID.ToString());
                        }
                        if (fields.Contains("storeid") && values[fields.IndexOf("storeid")] == "NULL" && storeID != Null.NullInteger)
                        {
                            values[fields.IndexOf("storeid")] = storeID.ToString();
                        }
                        if (!fields.Contains("noserial"))
                        {
                            fields.Add("noserial");
                            values.Add("dbo.NeuStoreDraftNo(NULL, NULL, NULL, NULL, 0)");
                        }
                        if (!fields.Contains("financeyearid"))
                        {
                            fields.Add("FinanceYearID");
                            values.Add(financeYearID.ToString());
                        }
                        if (fields.Contains("unitcurrencyconverter"))
                        {
                            int index = fields.IndexOf("unitcurrencyconverter");
                            if (Helper.GetDouble(values[index], -1) <= 0)
                            {
                                values.RemoveAt(index);
                                fields.RemoveAt(index);
                            }
                        }

                        if (fields.Contains("distagentid"))
                        {
                            int index = fields.IndexOf("distagentid");
                            values[index] = "NULL";
                        }
                    }
                    #endregion
                    if (info.Name == "Personnel")
                    {
                        if (!fields.Contains("code"))
                        {
                            fields.Add("code");
                            values.Add("dbo.NeuPersonnelCode(NULL)");
                        }
                    }
                    #region DocumentAccounting
                    if (info.Name == "DocumentAccounting")
                    {
                        if (!fields.Contains("no"))
                        {
                            fields.Add("no");
                            values.Add("dbo.NeuVoucherNo(NULL, NULL, NULL, NULL)");
                        }
                        if (!fields.Contains("financeyearid"))
                        {
                            fields.Add("FinanceYearID");
                            values.Add(financeYearID.ToString());
                        }
                    }
                    #endregion

                    #region Checks / PayCheck
                    if (info.Name == "Checks" || info.Name == "PayCheck")
                    {
                        if (!fields.Contains("financeyearid"))
                        {
                            fields.Add("FinanceYearID");
                            values.Add(financeYearID.ToString());
                        }
                        if (!fields.Contains("isshop"))
                        {
                            fields.Add("IsShop");
                            values.Add(isShop.ToString());
                        }
                        if (!fields.Contains("no") && info.Name == "Checks")
                        {
                            fields.Add("No");
                            values.Add("[dbo].[NeuCheckNo](NULL, NULL, NULL)");
                        }
                        if (!fields.Contains("no") && info.Name == "PayCheck")
                        {
                            fields.Add("No");
                            values.Add("[dbo].[NeuPaycheckNo](NULL, NULL, NULL)");
                        }
                    }
                    #endregion

                    string query = "";
                    if (isUpdate)
                    {
                        query = string.Format("UPDATE {0} SET ", tableName);
                        for (int f = 0; f < fields.Count; f++)
                            query += string.Format("{0} = {1}, ", fields[f], values[f]);
                        query = query.Substring(0, query.Length - 2) + " WHERE ID = " + offlineID + "; SELECT " + offlineID;
                    }
                    else
                    {
                        // Check for previous sync
                        if (tabletUniqueID > 0)
                        {
                            int id = Helper.GetInt(proxy.FillScalar(string.Format("SELECT ID FROM {0} WHERE TabletUniqueID={1} AND TabletSyncVer={2} AND TabletID={3}"
                                , tableName, tabletUniqueID, tabletSyncVer, offlineID)), Null.NullInteger);
                            if (id > 0)
                            {
                                query = string.Format("UPDATE {0} SET ", tableName);
                                for (int f = 0; f < fields.Count; f++)
                                    query += string.Format("{0} = {1}, ", fields[f], values[f]);
                                query = query.Substring(0, query.Length - 2) + " WHERE ID = " + id + "; SELECT " + id;
                            }
                        }
                        if (query == "")
                            query = string.Format("INSERT INTO {0} ({1}) VALUES({2}); SELECT SCOPE_IDENTITY();"
                                , tableName
                                , string.Join(", ", fields)
                                , string.Join(", ", values));
                    }
                    proxy.LastException = null;
                    int onlineID = Helper.GetInt(proxy.FillScalar(query), Null.NullInteger);
                    string exMsg = "";
                    if (onlineID == Null.NullInteger)
                    {
                        ExceptionLog.Instance.Add(proxy.LastException, "Set Sync :\r\n" + query);
                        exMsg = "Error: ";
                        if (proxy.LastException != null)
                            exMsg += proxy.LastException.Message + " ";
                    }
                    else
                    {
                        HandleTabletWorkflow(proxy, info, onlineID, isUpdate, visitorUserID);
                        if (info.Name == "StoreDraft")
                            proxy.RunQuery("DELETE FROM StoreDraftItems WHERE StoreDraftID=" + onlineID);
                    }
                    proxy.RunQuery(string.Format("INSERT INTO [Temp] (Comment1, Comment2, Val1, Val2, ActionDate) VALUES ('{0}', {1}, {2}, {3}, GETDATE())"
                        , tableName, Helper.QoutedString(exMsg + query), onlineID, offlineID));
                    mapping.AppendLine(offlineID + "=" + onlineID);
                }
                proxy.CommitTransaction();
            }
            catch (Exception ex)
            {
                mapping.Clear();
                try
                {
                    proxy.RollbackTransaction();
                }
                catch { }
                ExceptionLog.Instance.Add(ex, "Sync Error");
                return "<Error Occured:" + ex.Message + ">";
            }
        mapping.AppendLine("set_sync_rows_eof");

        string res = "\r\n" + mapping.ToString().TrimEnd() + "\r\n";
        stopWatch.Stop();
        //ExceptionLog.Instance.Add(new Exception("SetSyncRows " + info.Name + " : " + stopWatch.ElapsedMilliseconds));
        return res;
    }
    private void HandleTabletWorkflow(DbProxy proxy, TableInfo info, int id, bool isUpdate, int visitorUserID)
    {
        if (info.Name == "StoreDraft")
        {
            // move it desired Task that is defined in TabletWorkFlow
            int nextTaskID = Null.NullInteger;
            string query;
            #region prepare query
            if (isUpdate)
                query = string.Format(@"-- For existing StoreDrafts
SELECT TOP 1 TaskID FROM TabletWorkFlowItems 
	INNER JOIN TabletWorkFlow ON TabletWorkFlowItems.TabletWorkFlowID = TabletWorkFlow.ID
	WHERE TabletWorkFlow.WorkFlowType = 6 AND TabletWorkFlow.Type = 1
	AND EXISTS (
		SELECT StoreDraftPerson.PartnerID FROM StoreDraftPerson 
			INNER JOIN Personnel ON StoreDraftPerson.PartnerID = Personnel.ID		
			WHERE StoreDraftPerson.StoreDraftID = {0} 
				AND Personnel.CooperationTypeID = TabletWorkFlowItems.CooperationTypeIDs
				AND Personnel.ID IN (
					SELECT OtherID FROM GroupingMember WHERE GroupingID IN (SELECT ID FROM [dbo].[GetAllGroupingChilds](TabletWorkFlowItems.GroupingID))
				)
		)", id);
            else
                query = string.Format(@"-- For new StoreDrafts
SELECT TOP 1 TaskID FROM TabletWorkFlowItems 
	INNER JOIN TabletWorkFlow ON TabletWorkFlowItems.TabletWorkFlowID = TabletWorkFlow.ID
	WHERE TabletWorkFlow.WorkFlowType = 6 AND TabletWorkFlow.Type = 0
	AND EXISTS (
		SELECT StoreDraftPerson.PartnerID FROM StoreDraftPerson 
			INNER JOIN Personnel ON StoreDraftPerson.PartnerID = Personnel.ID		
			WHERE StoreDraftPerson.StoreDraftID = {0} 
				AND Personnel.CooperationTypeID = TabletWorkFlow.CooperationTypeID
				AND Personnel.ID IN (
					SELECT OtherID FROM GroupingMember WHERE GroupingID IN (SELECT ID FROM [dbo].[GetAllGroupingChilds](TabletWorkFlowItems.GroupingID))
				)
		)", id);
            #endregion
            nextTaskID = Helper.GetInt(proxy.FillScalar(query), nextTaskID);
            if (nextTaskID != Null.NullInteger)
                proxy.RunQuery(string.Format("EXEC [dbo].[DoTransition] 6, {0}, {1}, {2}, NULL", id, nextTaskID, visitorUserID <= 0 ? "" : visitorUserID.ToString()));
        }
        else if (info.Name == "Returns")
        {
            // move it desired Task that is defined in TabletWorkFlow
            int nextTaskID = Null.NullInteger;
            string query;
            #region prepare query
            if (isUpdate)
                query = string.Format(@"-- For existing Returns
SELECT TOP 1 TaskID, * FROM TabletWorkFlowItems 
	INNER JOIN TabletWorkFlow ON TabletWorkFlowItems.TabletWorkFlowID = TabletWorkFlow.ID
	WHERE TabletWorkFlow.WorkFlowType = 4 AND TabletWorkFlow.Type = 1
	AND 
	(
		EXISTS (
		SELECT [Returns].BuyerID FROM [Returns]
			INNER JOIN Personnel ON [Returns].BuyerID = Personnel.ID		
			WHERE [Returns].ID = {0}
				AND Personnel.CooperationTypeID = TabletWorkFlowItems.CooperationTypeIDs
				AND Personnel.ID IN (
					SELECT OtherID FROM GroupingMember WHERE GroupingID IN (SELECT ID FROM [dbo].[GetAllGroupingChilds](TabletWorkFlowItems.GroupingID))
				)
		)
		OR EXISTS (
		SELECT [Returns].VisitorID FROM [Returns]
			INNER JOIN Personnel ON [Returns].VisitorID = Personnel.ID		
			WHERE [Returns].ID = {0}
				AND Personnel.CooperationTypeID = TabletWorkFlowItems.CooperationTypeIDs
				AND Personnel.ID IN (
					SELECT OtherID FROM GroupingMember WHERE GroupingID IN (SELECT ID FROM [dbo].[GetAllGroupingChilds](TabletWorkFlowItems.GroupingID))
				)
		)
	)", id);
            else
                query = string.Format(@"-- For new Returns
SELECT TOP 1 TaskID, * FROM TabletWorkFlowItems 
	INNER JOIN TabletWorkFlow ON TabletWorkFlowItems.TabletWorkFlowID = TabletWorkFlow.ID
	WHERE TabletWorkFlow.WorkFlowType = 4 AND TabletWorkFlow.Type = 0
	AND 
	(
		EXISTS (
		SELECT [Returns].BuyerID FROM [Returns]
			INNER JOIN Personnel ON [Returns].BuyerID = Personnel.ID		
			WHERE [Returns].ID = {0}
				AND Personnel.CooperationTypeID = TabletWorkFlow.CooperationTypeID
				AND Personnel.ID IN (
					SELECT OtherID FROM GroupingMember WHERE GroupingID IN (SELECT ID FROM [dbo].[GetAllGroupingChilds](TabletWorkFlowItems.GroupingID))
				)
		)
		OR EXISTS (
		SELECT [Returns].VisitorID FROM [Returns]
			INNER JOIN Personnel ON [Returns].VisitorID = Personnel.ID		
			WHERE [Returns].ID = {0}
				AND Personnel.CooperationTypeID = TabletWorkFlow.CooperationTypeID
				AND Personnel.ID IN (
					SELECT OtherID FROM GroupingMember WHERE GroupingID IN (SELECT ID FROM [dbo].[GetAllGroupingChilds](TabletWorkFlowItems.GroupingID))
				)
		)
	)", id);
            #endregion
            nextTaskID = Helper.GetInt(proxy.FillScalar(query), nextTaskID);
            if (nextTaskID != Null.NullInteger)
                proxy.RunQuery(string.Format("EXEC [dbo].[DoTransition] 4, {0}, {1}, {2}, NULL", id, nextTaskID, visitorUserID <= 0 ? "" : visitorUserID.ToString()));
        }
    }
    [WebMethod()]
    public XmlDocument OfflineEntryOld()
    {
        XmlDocument xmlDoc = XmlHelper.GetXmlDocWithElement("Table", "1");
        try
        {
            string q = HttpContext.Current.Request.QueryString["q"];
            if (q == "tables.xml")
                xmlDoc.Load(Server.MapPath("~/app_data/tables.xml"));
            else if (q == "sqlite.xml")
                xmlDoc.Load(Server.MapPath("~/app_data/sqlite.xml"));
        }
        catch (Exception ex)
        {
            ExceptionLog.Instance.Add(ex);
        }
        return xmlDoc;
    }
    [WebMethod()]
    public string OfflineEntry()
    {
        try
        {
            string q = HttpContext.Current.Request.QueryString["q"];
            if (q == "tables.xml")
            {
                StringBuilder builder = new StringBuilder();
                Dictionary<string, TableInfo>.Enumerator enu = TableController.Instance.Tables.GetEnumerator();
                while (enu.MoveNext())
                {
                    if (enu.Current.Value.Name.ToLower() != "SyncOrder".ToLower() && enu.Current.Value.SyncLevel == SyncLevel.None)
                        continue;
                    // to pass the start xml line
                    builder.AppendLine("");
                    builder.AppendLine("tAblE:" + enu.Current.Value.Name);
                    for (int i = 0; i < enu.Current.Value.Fields.Count; i++)
                        builder.AppendLine(string.Format("{0};{1};{2};{3}"
                            , enu.Current.Value.Fields[i].Name
                            , enu.Current.Value.Fields[i].Type
                            , enu.Current.Value.Fields[i].Identity ? 1 : 0
                            , enu.Current.Value.Fields[i].Nullable ? 1 : 0));
                }
                return builder.ToString();
            }
        }
        catch (Exception ex)
        {
            ExceptionLog.Instance.Add(ex);
        }
        return "";
    }

    [WebMethod(EnableSession = true)]
    public XmlDocument GetList()
    {
        try
        {
            return GetListInternal();
        }
        catch (Exception ex)
        {
            ExceptionLog.Instance.Add(ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public ContentResult GetJson()
    {
        try
        {
            return new ContentResult { Content = CreateJson(), ContentType = "application/json" };
        }
        catch (Exception ex)
        {
            ExceptionLog.Instance.Add(ex);
            throw;
        }
    }

    public string CreateJson()
    {
        string table = HttpContext.Current.Request.QueryString["table"];
        string q = HttpContext.Current.Request.QueryString["qd"];
        TableInfo info = null;
        if (null != table)
        {
            info = TableController.Instance[table];
        }
        if (info == null)
        {
            return null;
        }
        int page = Helper.GetInt(HttpContext.Current.Request.Form["page"], 1);
        int rp = Helper.GetInt(HttpContext.Current.Request.Form["rp"], 10000);
        string sortname = Helper.GetString(HttpContext.Current.Request.Form["sortname"], "ID");
        //key1 , for do order by on query which returns data for grids print & excel
        string orderby = Helper.GetString(HttpContext.Current.Request.QueryString["orderby"], "");
        if (orderby != "")
        {
            sortname = orderby;
        }

        string select = "*", from = table, w = "";
        if (!string.IsNullOrEmpty(q))
        {
            QueryInfo qInfo = QueryController.Instance[q];
            if (qInfo != null)
            {
                if (!string.IsNullOrEmpty(qInfo.From))
                    from = qInfo.From.Replace("@Table", table);
                if (!string.IsNullOrEmpty(qInfo.Select))
                    select = qInfo.Select;
                if (!string.IsNullOrEmpty(qInfo.Where))
                {
                    w = qInfo.Where.Replace("_PersonnelID_", SiteSettings.User.PersonnelID.ToString())
                        .Replace("_UserID_", SiteSettings.UserID.ToString())
                        .Replace("_IsAdmin_", SiteSettings.User.IsAdmin ? "1" : "0");
                    w = Helper.ParseVars(w, info, null, HttpContext.Current.Request);
                }
            }
        }

        string whereCondition = "";
        whereCondition = info.BuildWhereConditionFromForm();

        string more = HttpContext.Current.Request.QueryString["more"];
        if (!string.IsNullOrEmpty(more))
        {
            whereCondition = (string.IsNullOrEmpty(whereCondition) ? " WHERE " : (whereCondition + " AND ")) + SiteSettings.ReplaceOptions(more);
        }
        if (!string.IsNullOrEmpty(w))
        {
            whereCondition = (string.IsNullOrEmpty(whereCondition) ? " WHERE " : (whereCondition + " AND ")) + w;
        }
        string sortorder = Helper.GetString(HttpContext.Current.Request.Form["sortorder"], "asc");
        string sortExp = (sortname + " " + sortorder).Trim();
        if (sortname != null && sortname.EndsWith("_cast2int"))
        {
            sortExp = ("CAST(" + sortname.Substring(0, sortname.Length - "_cast2int".Length) + " AS INT) " + sortorder).Trim();
        }
        int start = ((page - 1) * rp);


        string totalQry = string.Format("SELECT COUNT(*) FROM {0} {1}", from, whereCondition);
        int total = Helper.GetInt(DbProxy.Instance.FillScalar(totalQry), 0);
        string qry;
        if (select.ToLower().Contains("zeilenummer"))
        {
            string select2 = select.Replace("ZeileNummer,", "");
            qry = string.Format(@"
WITH TableRows AS 
(
	SELECT ROW_NUMBER() OVER (ORDER BY {0}) AS ZeileNummer,
		{7} FROM {2}
	{3}
)
SELECT {1} FROM TableRows WHERE ZeileNummer between {4} And {5} ORDER BY {6}"
   , sortExp, select, from, whereCondition, start + 1, start + rp, string.IsNullOrEmpty(sortExp) ? "ID" : sortExp, select2);
        }
        else
            qry = string.Format(@"
WITH TableRows AS 
(
	SELECT ROW_NUMBER() OVER (ORDER BY {0}) AS ZeileNummer,
		{1} FROM {2}
	{3}
)
SELECT {1} FROM TableRows WHERE ZeileNummer between {4} And {5} ORDER BY {6}"
            , sortExp, select, from, whereCondition, start + 1, start + rp, string.IsNullOrEmpty(sortExp) ? "ID" : sortExp);
        DataTable dt = DbProxy.Instance.FillData(qry, info.Name);

        return Toolbelt.SerializeDataTable(dt);
    }

    public XmlDocument GetListInternal()
    {
        System.Threading.Thread.CurrentThread.CurrentCulture = Helper.NumberCulture;
        string table = HttpContext.Current.Request.QueryString["table"];
        string q = HttpContext.Current.Request.QueryString["qd"];
        TableInfo info = null;
        if (null != table)
            info = TableController.Instance[table];
        if (info == null)
            return null;
        int page = Helper.GetInt(HttpContext.Current.Request.Form["page"], 1);
        int rp = Helper.GetInt(HttpContext.Current.Request.Form["rp"], 10000);
        string sortname = Helper.GetString(HttpContext.Current.Request.Form["sortname"], "ID");
        //key1 , for do order by on query which returns data for grids print & excel
        string orderby = Helper.GetString(HttpContext.Current.Request.QueryString["orderby"], "");
        if (orderby != "")
        {
            sortname = orderby;
        }

        string select = "*", from = table, w = "";
        if (!string.IsNullOrEmpty(q))
        {
            QueryInfo qInfo = QueryController.Instance[q];
            if (qInfo != null)
            {
                if (!string.IsNullOrEmpty(qInfo.From))
                    from = qInfo.From.Replace("@Table", table);
                if (!string.IsNullOrEmpty(qInfo.Select))
                    select = qInfo.Select;
                if (!string.IsNullOrEmpty(qInfo.Where))
                {
                    w = qInfo.Where.Replace("_PersonnelID_", SiteSettings.User.PersonnelID.ToString())
                        .Replace("_UserID_", SiteSettings.UserID.ToString())
                        .Replace("_IsAdmin_", SiteSettings.User.IsAdmin ? "1" : "0");
                    w = Helper.ParseVars(w, info, null, HttpContext.Current.Request);
                }
            }
        }

        string whereCondition = "";
        //// ----------------------------
        //// first method : only one q. ex: title=x
        //if (HttpContext.Current.Request.Form["qtype"] != null && HttpContext.Current.Request.Form["query"] != null && HttpContext.Current.Request.Form["query"].ToString() != string.Empty)
        //    whereCondition = BuildWhereCondition(info, HttpContext.Current.Request.Form["qtype"].ToString(), HttpContext.Current.Request.Form["query"].ToString());

        //// ----------------------------
        //// second method : more than one q. ex: title+code=q
        //if (HttpContext.Current.Request.Form["qtype"] != null && HttpContext.Current.Request.Form["query"] != null && HttpContext.Current.Request.Form["query"].ToString() != string.Empty)
        //    whereCondition = BuildWhereCondition(info, HttpContext.Current.Request.Form["qtype"].ToString().Split('+'), HttpContext.Current.Request.Form["query"].ToString());

        // ----------------------------
        // third method : using filter
        if (HttpContext.Current.Request.QueryString["_qMode"] == "q")
            whereCondition = info.BuildWhereConditionFromUrl();
        else
            whereCondition = info.BuildWhereConditionFromForm();

        string more = HttpContext.Current.Request.QueryString["more"];
        if (!string.IsNullOrEmpty(more))
            whereCondition = (string.IsNullOrEmpty(whereCondition) ? " WHERE " : (whereCondition + " AND ")) + SiteSettings.ReplaceOptions(more);
        if (!string.IsNullOrEmpty(w))
            whereCondition = (string.IsNullOrEmpty(whereCondition) ? " WHERE " : (whereCondition + " AND ")) + w;
        string sortorder = Helper.GetString(HttpContext.Current.Request.Form["sortorder"], "asc");
        string sortExp = (sortname + " " + sortorder).Trim();
        if (sortname != null && sortname.EndsWith("_cast2int"))
            sortExp = ("CAST(" + sortname.Substring(0, sortname.Length - "_cast2int".Length) + " AS INT) " + sortorder).Trim();
        int start = ((page - 1) * rp);


        string totalQry = string.Format("SELECT COUNT(*) FROM {0} {1}", from, whereCondition);
        int total = Helper.GetInt(DbProxy.Instance.FillScalar(totalQry), 0);
        string qry;
        if (select.ToLower().Contains("zeilenummer"))
        {
            string select2 = select.Replace("ZeileNummer,", "");
            qry = string.Format(@"
WITH TableRows AS 
(
	SELECT ROW_NUMBER() OVER (ORDER BY {0}) AS ZeileNummer,
		{7} FROM {2}
	{3}
)
SELECT {1} FROM TableRows WHERE ZeileNummer between {4} And {5} ORDER BY {6}"
   , sortExp, select, from, whereCondition, start + 1, start + rp, string.IsNullOrEmpty(sortExp) ? "ID" : sortExp, select2);
        }
        else
            qry = string.Format(@"
WITH TableRows AS 
(
	SELECT ROW_NUMBER() OVER (ORDER BY {0}) AS ZeileNummer,
		{1} FROM {2}
	{3}
)
SELECT {1} FROM TableRows WHERE ZeileNummer between {4} And {5} ORDER BY {6}"
            , sortExp, select, from, whereCondition, start + 1, start + rp, string.IsNullOrEmpty(sortExp) ? "ID" : sortExp);
        DataTable dt = DbProxy.Instance.FillData(qry, "fck");

        /*
         <rows>
          <page>10</page>
          <total>100</total>
          <row id="0">
            <cell>0</cell>
            <cell>n0</cell>
          </row>
          <row id="1">
            <cell>1</cell>
            <cell>n1</cell>
          </row>
        </rows>
        */
        System.Text.StringBuilder builder = new System.Text.StringBuilder();

        builder.Append("<?xml version='1.0' encoding='utf-8' ?>");
        builder.Append("<rows>");
        builder.Append("<page>" + page + " </page>");
        builder.Append("<total>" + total + " </total>");
        bool useID = dt.Columns.Contains("ID");
        Type dateType = typeof(DateTime);
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (useID)
                builder.AppendFormat("<row id='{0}'>", dt.Rows[i]["ID"]);
            else
                builder.Append("<row>");
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                if (info != null)
                {
                    FieldInfo f = info.GetField(dt.Columns[j].ColumnName);
                    if ((f != null && f.Type.ToLower().StartsWith("date")) || (f == null && dt.Columns[j].DataType == dateType))
                    {
                        builder.Append(string.Format("<cell>{0}</cell>", Helper.FormatDate(Helper.GetDateTime(dt.Rows[i][j], Helper.NullDate), null)));
                        continue;
                    }
                    else if (f != null && f.Encoded)
                    {
                        try
                        {
                            builder.Append(string.Format("<cell>{0}</cell>", EncDec.Decrypt(Helper.GetString(dt.Rows[i][j], null), GetEnDeP())));
                        }
                        catch
                        {
                            builder.Append(string.Format("<cell>{0}</cell>", Helper.GetString(dt.Rows[i][j], null)));
                        }
                        continue;
                        //EncDec.Decrypt(value.ToString(), GetEnDeP())
                    }
                }
                builder.Append(string.Format("<cell>{0}</cell>", HttpUtility.HtmlEncode(Helper.GetString(dt.Rows[i][j], ""))));
            }
            builder.Append("</row>");
        }
        builder.Append("</rows>");
        string xml = builder.ToString().Replace("&", "&amp;");
        XmlDocument newDoc = new XmlDocument();
        newDoc.LoadXml(xml);
        return newDoc;
    }
    private string GetEnDeP()
    {
        //string res = "turkkere@knkmkzzqna!$%1";
        //byte[] b = System.Text.UTF8Encoding.UTF8.GetBytes(res);
        //string tt = "";
        //for (int i = 0; i < b.Length; i++)
        //{
        //    tt += b[i].ToString() + ", ";
        //}
        byte[] b = new byte[] { 116, 117, 114, 107, 107, 101, 114, 101, 64, 107, 110, 107, 109, 107, 122, 122, 113, 110, 97, 33, 36, 37, 49 };
        return System.Text.UTF8Encoding.UTF8.GetString(b);
    }
    [WebMethod(EnableSession = true)]
    public XmlDocument GetQueryList()
    {
        System.Threading.Thread.CurrentThread.CurrentCulture = Helper.NumberCulture;
        string table = HttpContext.Current.Request.QueryString["table"];
        string q = HttpContext.Current.Request.QueryString["qd"];
        TableInfo tInfo = null;
        if (null != table)
            tInfo = TableController.Instance[table];
        if (tInfo == null)
            return null;

        string whereCondition = "";
        string filterWhereCondition = "";
        int whereIndex = 0;
        int whithoutWhereLength = 0;
        if (HttpContext.Current.Request.Form["qtype"] != null && HttpContext.Current.Request.Form["query"] != null && HttpContext.Current.Request.Form["query"].ToString() != string.Empty)
            whereCondition = tInfo.BuildWhereCondition(HttpContext.Current.Request.Form["qtype"].ToString(), HttpContext.Current.Request.Form["query"].ToString());
        string more = HttpContext.Current.Request.QueryString["more"];
        if (!string.IsNullOrEmpty(more))
            whereCondition = (string.IsNullOrEmpty(whereCondition) ? " WHERE " : (whereCondition + " AND ")) + more;
        string sortorder = Helper.GetString(HttpContext.Current.Request.Form["sortorder"], "asc");

        filterWhereCondition = tInfo.BuildWhereConditionFromForm();
        if (!string.IsNullOrEmpty(filterWhereCondition))
        {
            whereIndex = filterWhereCondition.IndexOf("WHERE") + 5;
            whithoutWhereLength = filterWhereCondition.Length - whereIndex;

        }

        whereCondition += (string.IsNullOrEmpty(whereCondition) ? filterWhereCondition : whereCondition + " AND " + filterWhereCondition.Substring(whereIndex, whithoutWhereLength));



        string select = "SELECT * FROM " + table;
        if (!string.IsNullOrEmpty(q))
        {
            QueryInfo qInfo = QueryController.Instance[q];
            if (qInfo != null)
            {
                if (!string.IsNullOrEmpty(qInfo.Query))
                {
                    select = qInfo.Query;
                    select = Helper.ParseQueryVars(select, tInfo, HttpContext.Current.Request);
                }
            }
        }
        DataTable dt = DbProxy.Instance.FillData(select + whereCondition, "fck");

        /*
         <rows>
          <page>10</page>
          <total>100</total>
          <row id="0">
            <cell>0</cell>
            <cell>n0</cell>
          </row>
          <row id="1">
            <cell>1</cell>
            <cell>n1</cell>
          </row>
        </rows>
        */
        System.Text.StringBuilder builder = new System.Text.StringBuilder();

        builder.Append("<?xml version='1.0' encoding='utf-8' ?>");
        builder.Append("<rows>");
        builder.Append("<page>1</page>");
        builder.Append("<total>" + dt.Rows.Count + "</total>");
        bool useID = dt.Columns.Contains("ID");
        Type dateType = typeof(DateTime);
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (useID)
                builder.AppendFormat("<row id='{0}'>", dt.Rows[i]["ID"]);
            else
                builder.Append("<row>");
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                if (tInfo != null)
                {
                    FieldInfo f = tInfo.GetField(dt.Columns[j].ColumnName);
                    if ((f != null && f.Type.ToLower().StartsWith("date")) || dt.Columns[j].DataType == dateType)
                    {
                        builder.Append(string.Format("<cell>{0}</cell>", Helper.FormatDate(Helper.GetDateTime(dt.Rows[i][j], Helper.NullDate), null)));
                        continue;
                    }
                }
                builder.Append(string.Format("<cell>{0}</cell>", dt.Rows[i][j]));
            }
            builder.Append("</row>");
        }
        builder.Append("</rows>");
        XmlDocument newDoc = new XmlDocument();
        newDoc.LoadXml(builder.ToString());
        return newDoc;
    }
    [WebMethod]
    public XmlDocument GetLangList()
    {
        try
        {
            return GetLangListInternal();
        }
        catch (Exception ex)
        {
            ExceptionLog.Instance.Add(ex);
            throw;
        }
    }

    private XmlDocument GetLangListInternal()
    {
        string lang = HttpContext.Current.Request.QueryString["lang"];
        string q = HttpContext.Current.Request.QueryString["qd"];
        int page = Helper.GetInt(HttpContext.Current.Request.Form["page"], 1);
        int rp = Helper.GetInt(HttpContext.Current.Request.Form["rp"], 1);
        string sortname = Helper.GetString(HttpContext.Current.Request.Form["sortname"], "ID");

        Dictionary<string, string> dict = LangProvider.LoadDict(lang);
        if (dict == null || dict.Count == 0)
            return null;

        Dictionary<string, string> fadict = lang == "fa" ? dict : LangProvider.LoadDict("fa");

        string sortorder = Helper.GetString(HttpContext.Current.Request.Form["sortorder"], "asc");
        string sortExp = (sortname + " " + sortorder).Trim();
        int start = ((page - 1) * rp);
        int end = page * rp;

        // todo : consider filtering
        //page=1&rp=15&sortname=Code&sortorder=asc&query=&qtype=&_q_Key=MBlood&comb=and
        string key = HttpContext.Current.Request["_q_Key"];
        string translation = HttpContext.Current.Request["_q_Translation"];
        string fatitle = HttpContext.Current.Request["_q_FaTitle"];
        if (key != null)
            key = key.ToLower();
        if (translation != null)
            translation = translation.ToLower();
        if (fatitle != null)
            fatitle = fatitle.ToLower();
        Dictionary<string, string>.Enumerator enu = dict.GetEnumerator();
        // find total
        int total = dict.Count;
        if (!string.IsNullOrEmpty(key) || !string.IsNullOrEmpty(translation) || !string.IsNullOrEmpty(fatitle))
        {
            total = 0;
            while (enu.MoveNext())
            {
                if (!string.IsNullOrEmpty(key))
                    if (!enu.Current.Key.ToLower().Contains(key))
                        continue;
                if (!string.IsNullOrEmpty(translation))
                    if (!enu.Current.Value.ToLower().Contains(translation))
                        continue;
                if (!string.IsNullOrEmpty(fatitle))
                {
                    string fa = fadict.ContainsKey(enu.Current.Key) ? fadict[enu.Current.Key] : "";
                    if (!fa.ToLower().Contains(fatitle))
                        continue;
                }
                total++;
            }
            enu = dict.GetEnumerator();
        }
        int counter = 0;
        // Skip starting 
        while (counter < start && enu.MoveNext())
        {
            if (!string.IsNullOrEmpty(key))
                if (!enu.Current.Key.ToLower().Contains(key))
                    continue;
            if (!string.IsNullOrEmpty(translation))
                if (!enu.Current.Value.ToLower().Contains(translation))
                    continue;
            if (!string.IsNullOrEmpty(fatitle))
            {
                string fa = fadict.ContainsKey(enu.Current.Key) ? fadict[enu.Current.Key] : "";
                if (!fa.ToLower().Contains(fatitle))
                    continue;
            }
            counter++;
        }

        /*
        <rows>
         <page>10</page>
         <total>100</total>
         <row id="0">
           <cell>0</cell>
           <cell>n0</cell>
         </row>
         <row id="1">
           <cell>1</cell>
           <cell>n1</cell>
         </row>
       </rows>
       */
        System.Text.StringBuilder builder = new System.Text.StringBuilder();
        builder.Append("<?xml version='1.0' encoding='utf-8' ?>");
        builder.Append("<rows>");
        builder.Append("<page>" + page + " </page>");
        builder.Append("<total>" + total + " </total>");
        while (counter < end && enu.MoveNext())
        {
            if (!string.IsNullOrEmpty(key))
                if (!enu.Current.Key.ToLower().Contains(key))
                    continue;
            if (!string.IsNullOrEmpty(translation))
                if (!enu.Current.Value.ToLower().Contains(translation))
                    continue;
            string fa = fadict.ContainsKey(enu.Current.Key) ? fadict[enu.Current.Key] : "";
            if (!string.IsNullOrEmpty(fatitle))
            {
                if (!fa.ToLower().Contains(fatitle))
                    continue;
            }
            builder.AppendFormat("<row id='{0}'>", enu.Current.Key);
            builder.Append(string.Format("<cell>{0}</cell><cell>{1}</cell><cell>{2}</cell><cell>{3}</cell>", counter++, enu.Current.Key, fa, enu.Current.Value));
            builder.Append("</row>");
        }
        builder.Append("</rows>");
        string xml = builder.ToString().Replace("&", "&amp;");
        XmlDocument newDoc = new XmlDocument();
        newDoc.LoadXml(xml);
        return newDoc;
    }
}
