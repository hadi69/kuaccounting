﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;
using Microsoft.SqlServer.Management.Sdk.Sfc;
using Microsoft.SqlServer.Management.Smo;
using System.IO;
using System.Text;
using DataTable = System.Data.DataTable;


/// <summary>
/// Summary description for OrderFormProvider
/// </summary>
public class SalaryFormProvider
{
    HttpRequest mRequest = null;

	public SalaryFormProvider(HttpRequest request)
	{
	    mRequest = request;
	}

    public string GetOrderForm()
    {
        try
        {
            if (mRequest.QueryString["type"] == "Order")
            {
                return FillOrderForm();
            }
            else if(mRequest.QueryString["type"]=="Fish")
            {
                return FillFishForm();
            }
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
            ExceptionLog.Instance.Add(ex);
            return "Error:" + ex.Message + "<br />" + ex.StackTrace.Replace("\n", "<br />");
        }
        return "";
    }

    string FillOrderForm()
    {
        int OrderDefID = Helper.GetInt(mRequest.QueryString["OrderDefID"], -1);
        DataRow row = DbProxy.Instance.FillRow(@"SELECT * FROM OrderDefs WHERE ID=" + OrderDefID);
        DataTable dt = DbProxy.Instance.FillData(@"SELECT Fields.* FROM Fields INNER JOIN OrderDefFields ON OrderDefFields.FieldID=Fields.ID WHERE OrderDefID="+OrderDefID+" ORDER BY FieldShowType, Priority","dt");
        if (dt == null)
        {
            return "Error:Fields not found";
        }
        string orderName = Helper.GetString(row, "Title", "Lang.Orders");
        StringBuilder builder=new StringBuilder();
        builder.AppendFormat("<div class='popupTitle'>{0}</div>", orderName);
        builder.Append(@"<div class='container'>
        <form id='detailFormOrder'>
          <div class='col-xs-12 form-group-container bg-info font-20 height-25'>
                <div class='col-xs-4 form-group-container bg-info font-20 height-25'>
                    <span id='FirstPerson' class='glyphicon glyphicon-step-forward'></span>
                    <span id='PrevPerson' class='glyphicon glyphicon-fast-forward'></span>
                    <span id='PrevOrder' class='glyphicon glyphicon-forward'></span>
                </div>
                <div class='col-xs-4 form-group-container bg-info font-20 height-25'>
                    
                </div>
                <div class='col-xs-4 form-group-container bg-info font-20 height-25 align-left'>
                    <span id='NextOrder' class='glyphicon glyphicon-backward'></span>
                    <span id='NextPerson' class='glyphicon glyphicon-fast-backward'></span>
                    <span id='LastPerson' class='glyphicon glyphicon-step-backward'></span>
                </div>
            </div>
            <div class='row'>
                <div class='col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container'>
                    <div class='form-group'>
                        <input type='hidden' id='ID' name='ID' value='@ID' />
                        <label for='Title' class='left-aligned-label control-label col-xs-12 col-sm-4'>Lang.Title</label>
                        <div class='col-xs-12 col-sm-8 form-col-without-padding'>
                            <input type='text' id='Title' name='Title' value='@Title' class='form-control input-sm' />
                        </div>
                    </div>
                </div>
                <div class='col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container'>
                    <div class='form-group'>
                        <label for='PersonnelName' class='left-aligned-label control-label col-xs-12 col-sm-4'>Lang.Person</label>
                        <div class='col-xs-12 col-sm-8 form-col-without-padding'>
                            <input type='text' id='PersonnelName' name='PersonnelName' value='@PersonnelName' class='validate[required] form-control input-sm' />
                            <input type='hidden' id='PersonnelID' name='PersonnelID' value='@PersonnelID' />
                        </div>
                    </div>
                </div>
                <div class='col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container'>
                    <div class='form-group'>
                        <label for='OrderDefName' class='left-aligned-label control-label col-xs-12 col-sm-4'>Lang.OrderDefs</label>
                        <div class='col-xs-12 col-sm-8 form-col-without-padding'>
                            <input type='text' id='OrderDefName' name='OrderDefName' value='@OrderDefName' class='validate[required] form-control input-sm' />
                            <input type='hidden' id='OrderDefID' name='OrderDefID' value='@OrderDefID' />
                        </div>
                    </div>
                </div>
                <div class='col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container'>
                    <div class='form-group'>
                        <label for='ConstName' class='left-aligned-label control-label col-xs-12 col-sm-4'>Lang.Consts</label>
                        <div class='col-xs-12 col-sm-8 form-col-without-padding'>
                            <input type='text' id='ConstName' name='ConstName' value='@ConstName' class='form-control input-sm' />
                            <input type='hidden' id='ConstID' name='ConstID' value='@ConstID' />
                        </div>
                    </div>
                </div>
                <div class='col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container'>
                    <div class='form-group'>
                        <label for='OrderDate' class='left-aligned-label control-label col-xs-12 col-sm-4'>Lang.OrdersDate</label>
                        <div class='col-xs-12 col-sm-8 form-col-without-padding'>
                            <input type='text' id='OrderDate' name='OrderDate' value='@OrderDate' class='dateSelect validate[required] form-control input-sm' />
                        </div>
                    </div>
                </div>
            </div>
            <div class='row'>");
        string FieldName = "", FieldTitle = "",FieldID="";
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            FieldShowType fieldShowType = Helper.GetEnum(dt.Rows[i], "FieldShowType", FieldShowType.Normal);
            FieldName = Helper.GetString(dt.Rows[i], "Name", "");
            FieldTitle = Helper.GetString(dt.Rows[i], "Title", "");
            FieldID = Helper.GetString(dt.Rows[i], "ID", "");
                builder.AppendFormat(@"<div class='col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container'>
                    <div class='form-group'>
                        <label for='{0}' class='left-aligned-label control-label col-xs-12 col-sm-6'>{1}</label>
                        <div class='col-xs-12 col-sm-6 form-col-without-padding'>
                           <div class='input-group input-group-sm'>
                              <input type='text' id='{0}' name='{0}' value='@{0}' class='validate[custom[integer]] monetary form-control input-sm'/>
                              <div class='input-group-btn'>
                                    <input type='button' value='...' class='btn btn-primary efield' data-id='{2}'/>
                              </div>
                           </div>
                        </div>
                    </div>
                </div>", FieldName, FieldTitle,FieldID);
        }
        builder.Append(@"</div>
                            </form>
                        </div>");
        builder.Append(@"<script type='text\javascript'>
                          $(function () {
        dateSelect('.dateSelect');
        if ($('#OrderDate').val() == '')
            $('#OrderDate').val(_Context.today);
        insertValidation();
        autoComplete('Personnel', 'staffauto', '#PersonnelName', '#PersonnelID');
        autoComplete('OrderDefs', 'enumauto1', '#OrderDefName', '#OrderDefID');
        autoComplete('Consts', 'enumauto1', '#ConstName', '#ConstID');
        commaInputs();
        });
        </script>");

        return builder.ToString();
    }

    string FillFishForm()
    {
        int FishDefID = Helper.GetInt(mRequest.QueryString["FishDefID"], -1);
        DataRow row = DbProxy.Instance.FillRow(@"SELECT * FROM FishDefs WHERE ID=" + FishDefID);
        string FishDefName = Helper.GetString(row, "Title", "Lang.Fish");
        StringBuilder builder=new StringBuilder();
        builder.AppendFormat(@"<div class='popupTitle'>{0}</div>
    <div class='container'>
        <form id='detailFormFish'>
            <div class='col-xs-12 form-group-container bg-info font-20 height-25'>
                <div class='col-xs-4 form-group-container bg-info font-20 height-25'>
                    <span id='FirstPerson' class='glyphicon glyphicon-step-forward'></span>
                    <span id='PrevPerson' class='glyphicon glyphicon-fast-forward'></span>
                    <span id='PrevFish' class='glyphicon glyphicon-forward'></span>
                </div>
                <div class='col-xs-4 form-group-container bg-info font-20 height-25'>
                    
                </div>
                <div class='col-xs-4 form-group-container bg-info font-20 height-25 align-left'>
                    <span id='NextFish' class='glyphicon glyphicon-backward'></span>
                    <span id='NextPerson' class='glyphicon glyphicon-fast-backward'></span>
                    <span id='LastPerson' class='glyphicon glyphicon-step-backward'></span>
                </div>
            </div>
            <div class='row'>
                <div class='col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container'>
                    <div class='form-group'>
                        <input type='hidden' id='ID' name='ID' value='@ID' />
                        <input type='hidden' id='ProgramID' name='ProgramID' value='_ProgramID_' />
                        <label for='PersonnelName' class='left-aligned-label control-label col-xs-12 col-sm-4'>Lang.Person</label>
                        <div class='col-xs-12 col-sm-8 form-col-without-padding'>
                            <input type='text' id='PersonnelName' name='PersonnelName' value='@PersonnelName' class='validate[required] form-control input-sm' />
                            <input type='hidden' id='PersonnelID' name='PersonnelID' value='@PersonnelID' />
                        </div>
                    </div>
                </div>
                <div class='col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container'>
                    <div class='form-group'>
                        <label for='FishDefName' class='left-aligned-label control-label col-xs-12 col-sm-6'>Lang.FishDefs</label>
                        <div class='col-xs-12 col-sm-6 form-col-without-padding'>
                            <input type='text' id='FishDefName' name='FishDefName' value='@FishDefName' class='validate[required] form-control input-sm' />
                            <input type='hidden' id='FishDefID' name='FishDefID' value='@FishDefID' />
                        </div>
                    </div>
                </div>
                <div class='col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container'>
                    <div class='form-group'>
                        <label for='OrderName' class='left-aligned-label control-label col-xs-12 col-sm-4'>Lang.Orders</label>
                        <div class='col-xs-12 col-sm-8 form-col-without-padding'>
                            <input type='text' id='OrderName' name='OrderName' value='@OrderName' class='form-control input-sm' />
                            <input type='hidden' id='OrderID' name='OrderID' value='@OrderID' />
                        </div>
                    </div>
                </div>
                <div class='col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container'>
                    <div class='form-group'>
                        <label for='FishYear' class='left-aligned-label control-label col-xs-12 col-sm-4'>Lang.Year</label>
                        <div class='col-xs-12 col-sm-8 form-col-without-padding'>
                            <input type='text' id='FishYear' name='FishYear' value='@FishYear' placeholder='yyyy' class='validate[required] form-control input-sm' />
                        </div>
                    </div>
                </div>
                <div class='col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container'>
                    <div class='form-group'>
                        <label for='FishMonth' class='left-aligned-label control-label col-xs-12 col-sm-4'>Lang.Month</label>
                        <div class='col-xs-12 col-sm-8 form-col-without-padding'>
                            <select id='FishMonth' name='FishMonth' v='@FishMonth' class='chosen-select chosen-rtl'>
                                <option value='1'>Lang.Farvardin</option>
                                <option value='2'>Lang.Ordibehesht</option>
                                <option value='3'>Lang.Khordad</option>
                                <option value='4'>Lang.Tir</option>
                                <option value='5'>Lang.Mordad</option>
                                <option value='6'>Lang.Shahrivar</option>
                                <option value='7'>Lang.Mehr</option>
                                <option value='8'>Lang.Aban</option>
                                <option value='9'>Lang.Azar</option>
                                <option value='10'>Lang.Dey</option>
                                <option value='11'>Lang.Bahman</option>
                                <option value='12'>Lang.Esfand</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class='col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container'>
                    <div class='form-group'>
                        <label for='Date' class='left-aligned-label control-label col-xs-12 col-sm-4'>Lang.FishDate</label>
                        <div class='col-xs-12 col-sm-8 form-col-without-padding'>
                            <input type='text' id='Date' name='Date' value='@Date' class='dateSelect validate[required] form-control input-sm' />
                        </div>
                    </div>
                </div>
                <div class='col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container'>
                    <div class='form-group'>
                        <label for='Status' class='left-aligned-label control-label col-xs-12 col-sm-4'>Lang.Status</label>
                        <div class='col-xs-12 col-sm-8 form-col-without-padding'>
                            <select id='Status' name='Status' v='@Status' class='chosen-select chosen-rtl'>
                                <option value='0'>Lang.Temp</option>
                                <option value='1'>Lang.Final</option>
                            </select>
                        </div>
                    </div>
                    <input type='hidden' id='LockStatus' name='LockStatus' value='@LockStatus' />
                </div>
                <div class='col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container'>
                    <div class='form-group'>
                        <label class='block'>
                            <span class='control-label left-aligned-label col-sm-4'>Lang.HasInsurance</span>
                            <div class='col-xs-12 col-sm-8 form-col-without-padding'>
                                <input type='checkbox' id='HasInsurance' name='HasInsurance' v='@HasInsurance' class='ios-switch' />
                                <div class='switch'></div>
                            </div>
                        </label>
                    </div>
                </div>
            </div>
            <div class='row margin-10-top'>", FishDefName);
        string FieldName = "",FieldTitle = "", FieldID="";
        DataTable functionTable =DbProxy.Instance.FillData(@"SELECT Fields.* FROM Fields INNER JOIN FishDefFields ON Fields.ID=FishDefFields.FieldID WHERE FishDefID=" + FishDefID + " AND FieldShowType=1 ORDER BY Priority", "functionTable");
        if (functionTable.Rows.Count > 0)
        {
            builder.Append(@"<div class='col-xs-12 col-sm-6 col-lg-3 padding-5-top padding-5-bottom'>");
            for (int i = 0; i < functionTable.Rows.Count; i++)
            {
                FieldName = Helper.GetString(functionTable.Rows[i], "Name", "");
                FieldTitle = Helper.GetString(functionTable.Rows[i], "Title", "");
                FieldID = Helper.GetString(functionTable.Rows[i], "ID", "");
                builder.AppendFormat(@"<div class='col-xs-12 col-without-padding'>
                        <div class='form-group'>
                            <label for='{0}' class='left-aligned-label control-label col-xs-12 col-sm-7'>{1}</label>
                            <div class='col-xs-12 col-sm-5 form-col-without-padding'>
                                <div class='input-group input-group-sm'>
                              <input type='text' id='{0}' name='{0}' value='@{0}' class='form-control input-sm'/>
                              <div class='input-group-btn'>
                                    <input type='button' value='...' class='btn btn-primary efield' data-id='{2}'/>
                              </div>
                           </div>
                            </div>
                        </div>
                    </div>", FieldName,FieldTitle,FieldID);
            }
            builder.Append(@"</div>");
        }
        DataTable benefitTable = DbProxy.Instance.FillData(@"SELECT Fields.* FROM Fields INNER JOIN FishDefFields ON Fields.ID=FishDefFields.FieldID WHERE FishDefID=" + FishDefID + " AND FieldShowType=2 ORDER BY Priority", "benefitTable");
        if (benefitTable.Rows.Count > 0)
        {
            builder.Append(@"<div class='col-xs-12 col-sm-6 col-lg-3 padding-5-top padding-5-bottom' style='border-left: 3px double #999; border-right: 3px double #999;'>");
            for (int i = 0; i < benefitTable.Rows.Count; i++)
            {
                FieldName = Helper.GetString(benefitTable.Rows[i], "Name", "");
                FieldTitle = Helper.GetString(benefitTable.Rows[i], "Title", "");
                FieldID = Helper.GetString(benefitTable.Rows[i], "ID", "");
                builder.AppendFormat(@"<div class='col-xs-12 col-without-padding'>
                        <div class='form-group'>
                            <label for='{0}' class='left-aligned-label control-label col-xs-12 col-sm-6'>{1}</label>
                            <div class='col-xs-12 col-sm-6 form-col-without-padding'>
                                <div class='input-group input-group-sm'>
                              <input type='text' id='{0}' name='{0}' value='@{0}' class='validate[custom[integer]] monetary form-control input-sm'/>
                              <div class='input-group-btn'>
                                    <input type='button' value='...' class='btn btn-primary efield' data-id='{2}'/>
                              </div>
                           </div>
                            </div>
                        </div>
                    </div>", FieldName,FieldTitle,FieldID);
            }
            builder.Append(@"</div>");
        }
        DataTable deductionTable = DbProxy.Instance.FillData(@"SELECT Fields.* FROM Fields INNER JOIN FishDefFields ON Fields.ID=FishDefFields.FieldID WHERE FishDefID=" + FishDefID + " AND FieldShowType=3 ORDER BY Priority", "deductionTable");
        if (deductionTable.Rows.Count > 0)
        {
            builder.Append(@"<div class='col-xs-12 col-sm-6 col-lg-3 padding-5-top padding-5-bottom' style='border-left: 3px double #999;'>");
            for (int i = 0; i < deductionTable.Rows.Count; i++)
            {
                FieldName = Helper.GetString(deductionTable.Rows[i], "Name", "");
                FieldTitle = Helper.GetString(deductionTable.Rows[i], "Title", "");
                FieldID = Helper.GetString(deductionTable.Rows[i], "ID", "");
                builder.AppendFormat(@"<div class='col-xs-12 col-without-padding'>
                        <div class='form-group'>
                            <label for='{0}' class='left-aligned-label control-label col-xs-12 col-sm-6'>{1}</label>
                            <div class='col-xs-12 col-sm-6 form-col-without-padding'>
                                <div class='input-group input-group-sm'>
                              <input type='text' id='{0}' name='{0}' value='@{0}' class='validate[custom[integer]] monetary form-control input-sm'/>
                              <div class='input-group-btn'>
                                    <input type='button' value='...' class='btn btn-primary efield' data-id='{2}'/>
                              </div>
                           </div>
                            </div>
                        </div>
                    </div>", FieldName,FieldTitle,FieldID);
            }
            builder.Append(@"</div>");
        }
        DataTable totalTable = DbProxy.Instance.FillData(@"SELECT Fields.* FROM Fields INNER JOIN FishDefFields ON Fields.ID=FishDefFields.FieldID WHERE FishDefID=" + FishDefID + " AND FieldShowType=4 ORDER BY Priority", "totalTable");
        if (totalTable.Rows.Count > 0)
        {
            builder.Append(@"<div class='col-xs-12 col-sm-6 col-lg-3 padding-5-top padding-5-bottom'>");
            for (int i = 0; i < totalTable.Rows.Count; i++)
            {
                FieldName = Helper.GetString(totalTable.Rows[i], "Name", "");
                FieldTitle = Helper.GetString(totalTable.Rows[i], "Title", "");
                FieldID = Helper.GetString(totalTable.Rows[i], "ID", "");
                builder.AppendFormat(@"<div class='col-xs-12 col-without-padding'>
                        <div class='form-group'>
                            <label for='{0}' class='left-aligned-label control-label col-xs-12 col-sm-5'>{1}</label>
                            <div class='col-xs-12 col-sm-7 form-col-without-padding'>
                                <div class='input-group input-group-sm'>
                              <input type='text' id='{0}' name='{0}' value='@{0}' class='validate[custom[integer]] monetary form-control input-sm'/>
                              <div class='input-group-btn'>
                                    <input type='button' value='...' class='btn btn-primary efield' data-id='{2}'/>
                              </div>
                           </div>
                            </div>
                        </div>
                    </div>", FieldName,FieldTitle,FieldID);
            }
            builder.Append(@"</div>");
        }
        builder.Append(@"</div>
        </form>
    </div>");
        return builder.ToString();
    }
}