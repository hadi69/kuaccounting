﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ReportSettingInfo
/// </summary>
public class ReportSettingInfo
{
    public int ID { get; set; }
    public int UserID { get; set; }
    public string ReportName { get; set; }
    public string Title { get; set; }
    public string Settings { get; set; }
}