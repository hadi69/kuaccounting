﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

public interface IVoucherPage
{
    DropDownList GetDropDown(int depth);
    GridView Grid { get; }
    AjaxControlToolkit.ModalPopupExtender TopicChooser { get; }
    void ShowTopicChooser(int depth, string parentTreePath);
}

public interface IMessageBox
{
    event EventHandler OkClick;
    void Show(WarningLevel warningLevel, string message);
    void Show(WarningLevel warningLevel, string message, string title);
    void ShowOkCancel(WarningLevel warningLevel, string message, string title);
}