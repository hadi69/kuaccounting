﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Nature of Topic
/// </summary>
public enum TopicNature
{
    Owe = 0, 
    Due = 1,
    Both = 2
}

public enum TopicLevel
{
    Total = 0,
    Elaborative = 1,
    Sub = 2
}

public enum VoucherMode
{
    Normal = 0,
    Bedridden = 1,
    Clinic = 2,
    Check = 3,
    Purchase = 4,
    Refuse = 5,
    Return = 6,
    StoreDraft = 7
}

public enum DisplayMode
{
    Empty = 0,
    View = 1,
    Edit = 2,
    Insert = 3
}