﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using System.Globalization;

/// <summary>
/// Summary description for AccController
/// </summary>
public class AccController
{
    #region Topic
    const string TopicSelect = @"SELECT Topic.*, TopicGroup.Title TopicGroupName, Program.Title ProgramName FROM Topic 
LEFT JOIN TopicGroup ON Topic.TopicGroupID=TopicGroup.ID 
LEFT JOIN Program ON Topic.ProgramID=Program.ID ";
    public static List<TopicInfo> GetTopics()
    {
        using (DbProxy proxy = new DbProxy())
        {
            IDataReader dr = proxy.ExecuteReader(TopicSelect);
            return ObjectBinder.FillCollection<TopicInfo>(dr);
        }
    }
    public static List<TopicInfo> GetTopics(string title)
    {
        using (DbProxy proxy = new DbProxy())
        {
            IDataReader dr = proxy.ExecuteReader(string.Format("{0} WHERE Topic.[Name] LIKE N'%{1}%'", TopicSelect, title));
            return ObjectBinder.FillCollection<TopicInfo>(dr);
        }
    }
    public static List<TopicInfo> GetTopics(string title, TopicLevel level)
    {
        using (DbProxy proxy = new DbProxy())
        {
            IDataReader dr = proxy.ExecuteReader(
                string.Format("{0} WHERE Topic.[Title] LIKE N'%{1}%' AND Topic.Level={2}", TopicSelect, title, (int)level));
            return ObjectBinder.FillCollection<TopicInfo>(dr);
        }
    }
    public static List<TopicInfo> GetTopicsByCode(string codeStart)
    {
        using (DbProxy proxy = new DbProxy())
        {
            IDataReader dr = proxy.ExecuteReader(string.Format("{0} WHERE Topic.[Code] LIKE N'{1}%'", TopicSelect, codeStart));
            return ObjectBinder.FillCollection<TopicInfo>(dr);
        }
    }
    public static List<TopicInfo> GetTopicsByCode(string codeStart, TopicLevel level)
    {
        using (DbProxy proxy = new DbProxy())
        {
            IDataReader dr = proxy.ExecuteReader(
                string.Format("{0} WHERE Topic.[Code] LIKE N'{1}%' AND Topic.Level={2}", TopicSelect, codeStart, (int)level));
            return ObjectBinder.FillCollection<TopicInfo>(dr);
        }
    }
    public static List<TopicInfo> GetTopicsByCode(string codeStart, int level)
    {
        using (DbProxy proxy = new DbProxy())
        {
            IDataReader dr = proxy.ExecuteReader(
                string.Format("{0} WHERE Topic.[Code] LIKE N'{1}%' AND Topic.Level={2}", TopicSelect, codeStart, level));
            return ObjectBinder.FillCollection<TopicInfo>(dr);
        }
    }
    public static List<TopicInfo> GetTopics(TopicLevel level, int programID)
    {
        using (DbProxy proxy = new DbProxy())
        {
            string query = TopicSelect + " WHERE Topic.Level=" + (int)level;
            if (programID == -1) // default program = NULL (See SiteSettings.cs)
                query += " AND Topic.ProgramID IS NULL";
            else if (programID == 0) // all programs
            {
            }
            else
                query += " AND  Topic.ProgramID = " + programID;
            query += " ORDER BY Topic.Code";
            IDataReader dr = proxy.ExecuteReader(query);
            return ObjectBinder.FillCollection<TopicInfo>(dr);
        }
    }
    public static TopicInfo GetTopic(int id)
    {
        if (id == Null.NullInteger)
            return null;
        // some speed up
        if (TopicBag == null)
        {
            TopicBag = new Dictionary<int, TopicInfo>();
            List<TopicInfo> all = GetTopics();
            for (int i = 0; i < all.Count; i++)
                TopicBag.Add(all[i].ID, all[i]);
        }
        if (TopicBag.ContainsKey(id))
            return TopicBag[id];

        using (DbProxy proxy = new DbProxy())
        {
            IDataReader dr = proxy.ExecuteReader(string.Format("{0} WHERE Topic.ID={1}", TopicSelect, id));
            return ObjectBinder.FillObject<TopicInfo>(dr);
        }
    }
    public static TopicInfo GetTopicByTitle(string title, int programID)
    {
        if (string.IsNullOrEmpty(title))
            return null;
        title = Helper.NormalizeChars(title).Trim();
        using (DbProxy proxy = new DbProxy())
        {
            string query = string.Format("{0} WHERE Topic.[Title]=N'{1}'", TopicSelect, title);
            if (programID == -1) // default program = NULL (See SiteSettings.cs)
                query += " AND Topic.ProgramID IS NULL";
            else if (programID == 0) // all programs
            {
            }
            else
                query += " AND  Topic.ProgramID = " + programID;
            IDataReader dr = proxy.ExecuteReader(query);
            return ObjectBinder.FillObject<TopicInfo>(dr);
        }
    }
    public static TopicInfo GetTopicByCode(string code, int programID)
    {
        if (string.IsNullOrEmpty(code))
            return null;
        using (DbProxy proxy = new DbProxy())
        {
            string query = string.Format("{0} WHERE Topic.[Code]=N'{1}'", TopicSelect, code);
            if (programID == -1) // default program = NULL (See SiteSettings.cs)
                query += " AND Topic.ProgramID IS NULL";
            else if (programID == 0) // all programs
            {
            }
            else
                query += " AND  Topic.ProgramID = " + programID;
            IDataReader dr = proxy.ExecuteReader(query);
            return ObjectBinder.FillObject<TopicInfo>(dr);
        }
    }
    public static bool Update(TopicInfo info)
    {
        info.Title = Helper.NormalizeChars(info.Title);
        TopicInfo other = GetTopicByCode(info.Code, SiteSettings.ProgramID);
        if (other != null && other.ID != info.ID)
            return false;
        //topics can have similar title
        //other = GetTopicByTitle(info.Title, SiteSettings.ProgramID);
        //if (other != null && other.ID != info.ID)
        //    return false;

        TopicBag = null;
        string qry = string.Format("UPDATE Topic SET Title={0}, Code={1}, Enabled={2}, Comments={3}, Nature={4}, Level={5}, Owe={6}, Due={7}, TopicGroupID={8}, ProgramID={9}, IsTemp={10}, Alias={11} WHERE ID={12}"
            , Helper.QoutedString(info.Title), Helper.QoutedString(info.Code), info.Enabled ? 1 : 0, Helper.QoutedString(info.Comments), (int)info.Nature
            , (int)info.Level, info.Owe, info.Due, info.TopicGroupID, info.ProgramID <= 0 ? "NULL" : info.ProgramID.ToString()
            , info.IsTemp ? 1 : 0
            , string.IsNullOrEmpty(info.Alias) ? "NULL" : Helper.QoutedString(info.Alias), info.ID);
        return DbProxy.Instance.ExecuteCommand(qry);

    }
    public static bool Add(TopicInfo info)
    {
        TopicBag = null;
        TopicInfo existingInfo;
        return Add(info, out existingInfo);
    }
    public static bool Add(TopicInfo info, out TopicInfo existingInfo)
    {
        existingInfo = null;
        if (SiteSettings.IsDbReadonly)
            return false;
        info.Title = Helper.NormalizeChars(info.Title);
        if (null != (existingInfo = GetTopicByCode(info.Code, SiteSettings.ProgramID)))
            return false;
        TopicBag = null;
        string qry = string.Format("INSERT INTO Topic (Title, Code, Enabled, Comments, Nature, Level, Owe, Due, TopicGroupID, ProgramID, IsTemp, Alias) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}); SELECT SCOPE_IDENTITY();"
           , Helper.QoutedString(info.Title), Helper.QoutedString(info.Code), info.Enabled ? 1 : 0, Helper.QoutedString(info.Comments), (int)info.Nature
           , (int)info.Level, info.Owe, info.Due, info.TopicGroupID <= 0 ? "NULL" : info.TopicGroupID.ToString()
           , info.ProgramID <= 0 ? "NULL" : info.ProgramID.ToString(), info.IsTemp ? 1 : 0, string.IsNullOrEmpty(info.Alias) ? "NULL" : Helper.QoutedString(info.Alias));
        object id = DbProxy.Instance.FillScalar(qry);
        if (Null.IsNull(id))
            return false;
        info.ID = Convert.ToInt32(id);
        return true;
    }
    public static bool DeleteTopic(int id)
    {
        TopicBag = null;
        return DbProxy.Instance.RunQuery("DELETE FROM Topic WHERE ID=" + id) > 0;
    }
    public static bool IsTopicInTree(int topicID)
    {
        object count = DbProxy.Instance.FillScalar(string.Format(
            "SELECT COUNT(*) FROM Trees WHERE TreePath LIKE '%-{0}-%' OR TreePath LIKE '{0}-%' OR TreePath Like '%-{0}'"
            , topicID));
        if (count == null || count is DBNull)
            return false;
        return Helper.GetInt(count, Null.NullInteger) > 0;
    }
    #endregion

    public static string AddTopicTree(string title, string code, TopicNature nature, int topicGroupID, int programID
        , bool personNeed, bool projectNeed, bool equipmentNeed, bool canHaveSubTree, string shortcut, string alias, string parentTreePath
        , out TopicInfo info, out TreeInfo child, bool costCenterNeed)
    {
        info = null;
        child = null;
        bool isNewTopic = false;

        #region Validating
        if (title.Trim().Length == 0 || code.Trim().Length == 0)
        {
            return LangProvider.Instance.ParseLang("Lang.EnterAccountCodeAndName");
            //if (info.se
        }
        if (title.Trim().Length == 0 && code.Trim().Length > 0)
        {
            info = GetTopicByCode(code.Trim(), SiteSettings.ProgramID);
            if (info == null)
                return LangProvider.Instance.ParseLang("Lang.ThereIsNoOtherAccountWithTheSameCodeEnterNameToAddAccount");
            //if (info.se
        }
        else if (title.Trim().Length > 0 && code.Trim().Length == 0)
        {
            info = GetTopicByTitle(title.Trim(), SiteSettings.ProgramID);
            if (info == null)
                return LangProvider.Instance.ParseLang("Lang.ThereIsNoOtherAccountWithTheSameCodeEnterCodeToAddAccount");
        }
        else
        {
            isNewTopic = true;
            info = new TopicInfo();
            info.Title = title;
            info.Code = code;
            info.Nature = nature;
            info.TopicGroupID = topicGroupID;
            info.ProgramID = programID;
            info.Alias = alias;
            TreeInfo treeInfo = GetTree(parentTreePath);
            if (treeInfo == null)
                info.Level = TopicLevel.Total;
            else if (treeInfo.Depth == 0)
                info.Level = TopicLevel.Elaborative;
            else if (treeInfo.Depth >= 1)
                info.Level = TopicLevel.Sub;
            TopicInfo existingInfo;
            if (!Add(info, out existingInfo))
            {
                if (existingInfo != null)
                    return string.Format("{0}<br>" + LangProvider.Instance.ParseLang("Lang.AvailableAccount") + ":<br>-" + LangProvider.Instance.ParseLang("Lang.Name")
                        + ": {1}<br>-" + LangProvider.Instance.ParseLang("Lang.Code") + ": {2}"
                        , LangProvider.Instance.ParseLang("Lang.AccountWasNotAddedMaybeCodeOrNameIsDuplicate"), existingInfo.Title, existingInfo.Code);
                else if (info.TopicGroupID <= 0)
                    return LangProvider.Instance.ParseLang("Lang.PleaseSelectAccountGroup");
                else
                    return LangProvider.Instance.ParseLang("Lang.UnknownError");
            }
        }
        #endregion

        child = new TreeInfo();
        child.PersonNeed = personNeed;
        child.ProjectNeed = projectNeed;
        child.EquipmentNeed = equipmentNeed;
        child.CanHaveSubTree = canHaveSubTree;
        child.CostCenterNeed = costCenterNeed;
        child.Shortcut = shortcut;
        if (parentTreePath == "")
        {
            child.Depth = 0;
            child.TreePath = info.ID.ToString();
        }
        else
        {
            TreeInfo parent = GetTree(parentTreePath);
            child.Depth = parent.Depth + 1;
            child.TreePath = string.Concat(parent.TreePath, "-", info.ID.ToString());
        }
        if (!Add(child))
        {
            // Rolllback adding new Account
            if (isNewTopic)
                DeleteTopic(info.ID);
            // todo: msg = error (circular dependency)
            return LangProvider.Instance.ParseLang("Lang.CyclicAccountError");
        }

        return "";
    }

    #region Tree
    public static List<TreeInfo> GetTrees()
    {
        using (DbProxy proxy = new DbProxy())
        {
            IDataReader dr = proxy.ExecuteReader("SELECT * FROM Trees");
            return ObjectBinder.FillCollection<TreeInfo>(dr);
        }
    }
    public static IDataReader GetTreeNames(int programID)
    {
        string query = "SELECT Trees.TreePath, dbo.TreePathToString(Trees.TreePath, 1) + '(' + ISNULL(Topic.Code, '') + ')' AS TreePathName FROM Trees INNER JOIN Topic ON [dbo].[TreePathToProgramID](TreePath) = Topic.ID";
        if (programID == -1) // default program = NULL (See SiteSettings.cs)
            query += " AND Topic.ProgramID IS NULL";
        else if (programID == 0) // all programs
        {
        }
        else
            query += " AND  Topic.ProgramID = " + programID;
        IDataReader dr = DbProxy.Instance.ExecuteReader(query);
        return dr;
    }
    public static List<TreeInfo> GetTrees(int depth, int programID, string parent = null, string topicCondition = null)
    {
        using (DbProxy proxy = new DbProxy())
        {
            string query = "SELECT Trees.* FROM Trees INNER JOIN Topic ON [dbo].[TreePathToProgramID](TreePath) = Topic.ID WHERE 1=1 ";
            if (depth != Null.NullInteger)
                query += " AND Trees.Depth=" + depth;
            if (programID == -1) // default program = NULL (See SiteSettings.cs)
                query += " AND Topic.ProgramID IS NULL";
            else if (programID == 0) // all programs
            {
            }
            else
                query += " AND  Topic.ProgramID = " + programID;
            if (!string.IsNullOrEmpty(parent))
                query += string.Format(" AND TreePath LIKE '{0}-%'", parent);
            if (!string.IsNullOrEmpty(topicCondition))
                query += string.Format(" AND {0}", topicCondition);
            IDataReader dr = proxy.ExecuteReader(query);
            return ObjectBinder.FillCollection<TreeInfo>(dr);
        }
    }
    public static List<TreeInfo> GetTrees(TreeInfo parent, string topicCondition = null)
    {
        using (DbProxy proxy = new DbProxy())
        {
            string query = string.Format("SELECT Trees.* FROM Trees INNER JOIN Topic ON [dbo].[TreePathToTopicID](TreePath) = Topic.ID WHERE Depth={0} AND TreePath LIKE '{1}-%'"
                , parent.Depth + 1, parent.TreePath);
            if (!string.IsNullOrEmpty(topicCondition))
                query += string.Format(" AND {0}", topicCondition);
            IDataReader dr = proxy.ExecuteReader(query);
                ;
            return ObjectBinder.FillCollection<TreeInfo>(dr);
        }
    }
    public static List<TreeInfo> GetTrees(int depth, string parent)
    {
        using (DbProxy proxy = new DbProxy())
        {
            IDataReader dr;
            if (string.IsNullOrEmpty(parent))
                dr = proxy.ExecuteReader(
                    string.Format("SELECT * FROM Trees WHERE Depth={0}", depth));
            else
                dr = proxy.ExecuteReader(
                    string.Format("SELECT * FROM Trees WHERE Depth={0} AND TreePath LIKE '{1}-%'", depth, parent));
            return ObjectBinder.FillCollection<TreeInfo>(dr);
        }
    }
    public static List<TreeInfo> GetTrees(int depth, int totalID, int elaborativeID, int parentID)
    {
        string query = string.Format("SELECT * FROM Trees WHERE Depth={0} AND TreePath LIKE '{1}-{2}-%'", depth, totalID, elaborativeID);
        using (DbProxy proxy = new DbProxy())
        {
            IDataReader dr = proxy.ExecuteReader(query);
            List<TreeInfo> res = ObjectBinder.FillCollection<TreeInfo>(dr);
            return ChooseWithParent(res, parentID);
        }
    }
    private static List<TreeInfo> ChooseWithParent(List<TreeInfo> res, int parentID)
    {
        if (res == null || res.Count == 0)
            return res;
        List<TreeInfo> arr = new List<TreeInfo>();
        List<int> accountIDs = new List<int>();
        string ending = string.Concat("-", parentID);
        for (int i = 0; i < res.Count; i++)
        {
            if (TreeInfo.GetParentTreePath(res[i].TreePath).EndsWith(ending))
            {
                int accID = TreeInfo.GetTopicID(res[i].TreePath);
                if (!accountIDs.Contains(accID))
                {
                    arr.Add(res[i]);
                    accountIDs.Add(accID);
                }
            }
        }
        return arr;
    }
    public static int GetTreeDepth()
    {
        object max = DbProxy.Instance.FillScalar("SELECT MAX(Depth) FROM Trees");
        return Helper.GetInt(max, 0);
    }
    public static TreeInfo GetTree(string treePath)
    {
        if (string.IsNullOrEmpty(treePath))
            return null;
        using (DbProxy proxy = new DbProxy())
        {
            IDataReader dr = proxy.ExecuteReader(string.Format("SELECT * FROM Trees WHERE TreePath='{0}'", treePath));
            return ObjectBinder.FillObject<TreeInfo>(dr);
        }
    }

    public static TreeInfo GetTreeFromCodes(string codePath)
    {
        if (string.IsNullOrEmpty(codePath))
            return null;
        try
        {
            StringBuilder treePath = new StringBuilder();
            string[] codes = codePath.Split('-');
            for (int i = 0; i < codes.Length; i++)
            {
                treePath.Append(GetTopicByCode(codes[i], SiteSettings.ProgramID).ID);
                if (i < codes.Length - 1)
                    treePath.Append("-");
            }
            using (DbProxy proxy = new DbProxy())
            {
                IDataReader dr = proxy.ExecuteReader(string.Format("SELECT * FROM Trees WHERE TreePath='{0}'", treePath.ToString()));
                return ObjectBinder.FillObject<TreeInfo>(dr);
            }
        }
        catch { }
        return null;
    }
    public static bool Update(TreeInfo info)
    {
        string qry = string.Format("UPDATE Trees SET PersonNeed={0}, ProjectNeed={1}, EquipmentNeed={2}, CanHaveSubTree={3}, Shortcut={4}, CostCenterNeed={6}  WHERE TreePath={5}"
            , info.PersonNeed ? 1 : 0, info.ProjectNeed ? 1 : 0, info.EquipmentNeed ? 1 : 0, info.CanHaveSubTree ? 1 : 0
            , Helper.QoutedString(info.Shortcut)
            , Helper.QoutedString(info.TreePath), info.CostCenterNeed ? 1 : 0);
        return DbProxy.Instance.RunQuery(qry) > 0;
    }
    public static bool UpdateDueOwe(string treePath)
    {
        string qry = string.Format(@"UPDATE Trees SET 
Due=(SELECT SUM(ISNULL(Due, 0)) FROM dbo.DocumentAccountingItems INNER JOIN DocumentAccounting ON DocumentAccounting.ID = DocumentAccountingItems.DocumentAccountingID WHERE DocumentAccounting.WorkflowStatus = 1 AND (DocumentAccountingItems.TreePath + '-') LIKE (Trees.TreePath + '-%'))
, Owe=(SELECT SUM(ISNULL(Owe, 0)) FROM dbo.DocumentAccountingItems INNER JOIN DocumentAccounting ON DocumentAccounting.ID = DocumentAccountingItems.DocumentAccountingID WHERE DocumentAccounting.WorkflowStatus = 1 AND (DocumentAccountingItems.TreePath + '-') LIKE (Trees.TreePath + '-%'))
WHERE Trees.TreePath='{0}'", treePath);
        return DbProxy.Instance.RunQuery(qry) > 0;
    }
    public static bool UpdateDueOwes()
    {
        string qry = @"UPDATE Trees SET 
Due=(SELECT SUM(ISNULL(Due, 0)) FROM dbo.DocumentAccountingItems INNER JOIN DocumentAccounting ON DocumentAccounting.ID = DocumentAccountingItems.DocumentAccountingID WHERE DocumentAccounting.WorkflowStatus = 1 AND (DocumentAccountingItems.TreePath + '-') LIKE (Trees.TreePath + '-%'))
, Owe=(SELECT SUM(ISNULL(Owe, 0)) FROM dbo.DocumentAccountingItems INNER JOIN DocumentAccounting ON DocumentAccounting.ID = DocumentAccountingItems.DocumentAccountingID WHERE DocumentAccounting.WorkflowStatus = 1 AND (DocumentAccountingItems.TreePath + '-') LIKE (Trees.TreePath + '-%'))";
        return DbProxy.Instance.RunQuery(qry) > 0;
    }
    public static bool Add(TreeInfo info)
    {
        if (null != GetTree(info.TreePath))
            return false;
        string qry = string.Format("INSERT INTO Trees (TreePath, Depth, PersonNeed, ProjectNeed, EquipmentNeed, Owe, Due, CanHaveSubTree, Shortcut, CostCenterNeed) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9})"
            , Helper.QoutedString(info.TreePath), info.Depth, info.PersonNeed ? 1 : 0, info.ProjectNeed ? 1 : 0, info.EquipmentNeed ? 1 : 0
            , info.Owe, info.Due
            , info.CanHaveSubTree ? 1 : 0, Helper.QoutedString(info.Shortcut), info.CostCenterNeed ? 1 : 0);
        return DbProxy.Instance.RunQuery(qry) > 0;
    }
    public static bool DeleteTree(string treePath)
    {
        TreeInfo tree = GetTree(treePath);
        if (tree == null)
            return false;
        if (IsTreeUsed(tree))
            return false;
        if (DbProxy.Instance.RunQuery(
            string.Format("DELETE FROM Trees WHERE TreePath='{0}'", treePath)) > 0)
        {
            // Delete the Account Too
            int topicID = tree.TopicID;
            if (!IsTopicInTree(topicID))
                DeleteTopic(topicID);
            // Delete Childs Too
            DbProxy.Instance.RunQuery(
                string.Format("DELETE FROM Trees WHERE Depth={0} AND TreePath LIKE '{1}-%'", tree.Depth + 1, tree.TreePath));
            return true;
        }
        return false;
    }
    public static bool IsTreeUsed(TreeInfo info)
    {
        List<TreeInfo> childs = GetTrees(info);
        if (childs != null && childs.Count > 0)
            return true;
        string qry = string.Format("SELECT COUNT(*) FROM DocumentAccountingItems WHERE TreePath='{0}' OR TreePath LIKE '{0}-%'", info.TreePath);
        object count = DbProxy.Instance.FillScalar(qry);
        if (count == null || count is DBNull)
            return false;
        return Helper.GetInt(count, -1) > 0;
    }
    public static bool HasChildTree(string treePath)
    {
        return HasChildTree(GetTree(treePath));
    }
    public static bool HasChildTree(TreeInfo info)
    {
        if (null == info)
            return false;
        object count = DbProxy.Instance.FillScalar(
            string.Format("SELECT COUNT(*) FROM Trees WHERE Depth={0} AND TreePath LIKE '{1}-%'", info.Depth + 1, info.TreePath));
        int numChilds = Helper.GetInt(count, 0);
        return numChilds > 0;
    }
    public static bool IsOneToLeaf(string treePath)
    {
        return IsOneToLeaf(GetTree(treePath));
    }
    public static bool IsOneToLeaf(TreeInfo info)
    {
        if (null == info)
            return false;

        object count = DbProxy.Instance.FillScalar(
            string.Format("SELECT COUNT(*) FROM Trees WHERE Depth={0} AND TreePath LIKE '{1}-%'", info.Depth + 2, info.TreePath));
        int numChilds = Helper.GetInt(count, 0);
        return numChilds == 0;
    }
    public static bool CopyChilds(string srcTreePath, string dstTreePath, out string errors)
    {
        errors = "";
        TreeInfo src = GetTree(srcTreePath), dst = GetTree(dstTreePath);
        if (src == null || dst == null)
            return false;
        List<TreeInfo> childs = GetTrees(src);
        if (childs == null || childs.Count == 0)
            return false;
        bool res = CopyChilds(src, dst, ref errors);
        if (errors != "")
            errors = LangProvider.Instance.ParseLang("Lang.GroupCyclicAccountError") + errors;
        return res;
    }
    private static bool CopyChilds(TreeInfo src, TreeInfo dst, ref string errors)
    {
        List<TreeInfo> childs = GetTrees(src);
        if (childs == null || childs.Count == 0)
            return true;
        for (int i = 0; i < childs.Count; i++)
        {
            TreeInfo child = new TreeInfo();
            child.Depth = dst.Depth + 1;
            child.TreePath = string.Concat(dst.TreePath, "-", childs[i].TopicID.ToString());
            if (!Add(child))
                errors += childs[i].TopicNameCode + ",";
            else
                CopyChilds(childs[i], child, ref errors);
        }
        return true;
    }
    #endregion

    #region Voucher
    //public static List<VoucherInfo> GetVouchers()
    //{
    //    using (DbProxy proxy = new DbProxy())
    //    {
    //        IDataReader dr = proxy.ExecuteReader("SELECT * FROM DocumentAccounting");
    //        return ObjectBinder.FillCollection<VoucherInfo>(dr);
    //    }
    //}
    //public static List<VoucherInfo> GetVouchers(int wardID)
    //{
    //    if (wardID == Null.NullInteger)
    //        return GetVouchers();
    //    using (DbProxy proxy = new DbProxy())
    //    {
    //        IDataReader dr = proxy.ExecuteReader("SELECT * FROM DocumentAccounting WHERE WardID=" + wardID);
    //        return ObjectBinder.FillCollection<VoucherInfo>(dr);
    //    }
    //}
    public static List<VoucherInfo> GetVouchers(string treePath)
    {
        string qry = string.Format("SELECT * FROM DocumentAccounting WHERE ID IN (SELECT DISTINCT DocumentAccountingID FROM DocumentAccountingItems WHERE TreePath='{0}' OR TreePath LIKE '{0}-%')", treePath);
        using (DbProxy proxy = new DbProxy())
        {
            IDataReader dr = proxy.ExecuteReader(qry);
            return ObjectBinder.FillCollection<VoucherInfo>(dr);
        }
    }
    //public static List<VoucherInfo> GetVoucherTitles(string title)
    //{
    //    using (DbProxy proxy = new DbProxy())
    //    {
    //        IDataReader dr = proxy.ExecuteReader(
    //            string.Format("SELECT ID, Title FROM DocumentAccounting WHERE Title LIKE N'%{0}%'", title));
    //        return ObjectBinder.FillCollection<VoucherInfo>(dr);
    //    }
    //}
    //public static List<string> GetVoucherTitlesDistinct(string title)
    //{
    //    using (DbProxy proxy = new DbProxy())
    //    {
    //        List<string> res = new List<string>();
    //        IDataReader dr = proxy.ExecuteReader(
    //            string.Format("SELECT Title FROM DocumentAccounting WHERE Title LIKE N'%{0}%'", title));
    //        Dictionary<string, string> hash = new Dictionary<string, string>();
    //        while (dr.Read())
    //        {
    //            string str = dr.GetString(0);
    //            if (!hash.ContainsKey(str))
    //            {
    //                res.Add(str);
    //                hash.Add(str, str);
    //            }
    //        }
    //        dr.Close();
    //        dr.Dispose();
    //        hash.Clear();
    //        hash = null;
    //        return res;
    //    }
    //}
    public static string GetVoucherTitle(int id)
    {
        object title = DbProxy.Instance.RunQuery("SELECT Title FROM DocumentAccounting WHERE ID=" + id);
        return Helper.GetString(title, "");
    }
    public static VoucherInfo GetVoucher(int id)
    {
        if (Null.NullInteger == id)
            return null;
        using (DbProxy proxy = new DbProxy())
        {
            IDataReader dr = proxy.ExecuteReader(string.Format("SELECT * FROM DocumentAccounting WHERE ID={0}", id));
            return ObjectBinder.FillObject<VoucherInfo>(dr);
        }
    }
    //public static int GetFinanceYear()
    //{
    //    int ID = (int)DbProxy.Instance.FillScalar(string.Format("SELECT ID FROM FinanceYear WHERE Status=2"));
    //    return ID;
    //}
    //public static VoucherInfo GetLastVoucher()
    //{
    //    using (DbProxy proxy = new DbProxy())
    //    {
    //        IDataReader dr = proxy.ExecuteReader("SELECT TOP 1 * FROM DocumentAccounting ORDER BY ID DESC");
    //        return ObjectBinder.FillObject<VoucherInfo>(dr);
    //    }
    //}
    //public static bool Update(VoucherInfo info)
    //{
    //    decimal sumOwe, sumDue;
    //    AccController.CalcSums(info.ID, out sumOwe, out sumDue);
    //    info.TotalOwe = sumOwe;
    //    info.TotalDue = sumDue;
    //    string qry = string.Format("UPDATE DocumentAccounting SET No={0}, Date={1}, UserID={2}, FixerID={3}, LockerID={4}, DoComments={5}, TotalOwe={6}, TotalDue={7}, Mode={8}, OtherID={9}, WardID={10} WHERE ID={11};"
    //        , Helper.QoutedString(info.No), Helper.QoutedDateTime(info.Date), info.UserID, info.FixerID, info.LockerID, Helper.QoutedString(info.DoComments), info.TotalOwe, info.TotalDue, (int)info.Mode, info.OtherID, info.WardID, info.ID);
    //    return DbProxy.Instance.RunQuery(qry) > 0;
    //}
    public static bool UpdateVoucherModifier(int voucherID, int userID, string comments)
    {
       // DbProxy.Instance.RunQuery(string.Format("UPDATE DocumentAccounting SET UserID={0} WHERE ID={1}", userID, voucherID));
        ModifierInfo info = new ModifierInfo();
        info.DocumentAccountingID = voucherID;
        info.UserID = userID;
        info.Date = DateTime.Now;
        info.Comments = comments;
        return Add(info);
    }
    public static bool LockVoucher(VoucherInfo info)
    {
        info.LockerID = SiteSettings.UserID;
        info.FixerID = Null.NullInteger;
        info.Status = 1;
        string qry = string.Format("UPDATE DocumentAccounting SET FixerID={0}, LockerID={1}, Status={2} WHERE ID={3};"
            , info.FixerID == -1 ? "NULL" : info.FixerID.ToString(), info.LockerID, info.Status, info.ID);
        return DbProxy.Instance.RunQuery(qry) > 0;
    }
    //public static bool DeleteVoucher(int id)
    //{
    //    DeleteItems(id);
    //    return DbProxy.Instance.RunQuery("DELETE FROM DocumentAccounting WHERE ID=" + id) > 0;
    //}
    public static void CalcSums(int voucherID, out decimal sumOwe, out decimal sumDue)
    {
        CalcSums(voucherID, Null.NullInteger, out sumOwe, out sumDue);
    }
    public static void CalcSums(int voucherID, int excludeItemID, out decimal sumOwe, out decimal sumDue)
    {
        sumOwe = sumDue = 0;
        if (Null.NullInteger == voucherID)
            return;
        using (DbProxy proxy = new DbProxy())
        {
            IDataReader dr;
            if (excludeItemID == Null.NullInteger)
                dr = proxy.ExecuteReader(string.Format("SELECT SUM(ISNULL(Owe, 0)), Sum(ISNULL(Due, 0)) FROM DocumentAccountingItems INNER JOIN DocumentAccounting ON DocumentAccounting.ID = DocumentAccountingItems.DocumentAccountingID WHERE DocumentAccounting.WorkflowStatus = 1 AND DocumentAccountingID={0}", voucherID));
            else
                dr = proxy.ExecuteReader(string.Format("SELECT SUM(ISNULL(Owe, 0)), Sum(ISNULL(Due, 0)) FROM DocumentAccountingItems INNER JOIN DocumentAccounting ON DocumentAccounting.ID = DocumentAccountingItems.DocumentAccountingID WHERE DocumentAccounting.WorkflowStatus = 1 AND DocumentAccountingID={0} AND ID<>{1}", voucherID, excludeItemID));
            if (dr.Read())
            {
                try
                {
                    sumOwe = (decimal)dr.GetDecimal(0);
                }
                catch { }
                try
                {
                    sumDue = (decimal)dr.GetDecimal(1);
                }
                catch { }
            }
            dr.Close();
        }
    }
    public static bool IsFixedVoucher(int id)
    {
        if (Null.NullInteger == id)
            return false;
        object fixer = DbProxy.Instance.FillScalar(string.Format("SELECT FixerID FROM DocumentAccounting WHERE ID={0}", id));
        return Helper.GetInt(fixer, Null.NullInteger) > 0;
    }
    public static bool LockVoucher(int voucherID, int userID)
    {
        if (voucherID == Null.NullInteger || userID == Null.NullInteger)
            return false;
        DbProxy.Instance.RunQuery("UPDATE DocumentAccounting SET LockerID=-1 WHERE LockerID=" + userID);
        return DbProxy.Instance.RunQuery(
            string.Format("UPDATE DocumentAccounting SET LockerID={0} WHERE ID={1}", userID, voucherID)) > 0;
    }
    public static bool UnLockVouchers(int userID)
    {
        if (userID == Null.NullInteger)
            return false;
        return DbProxy.Instance.RunQuery("UPDATE DocumentAccounting SET LockerID=-1 WHERE LockerID=" + userID) > 0;
    }
    #endregion

    #region Item
    //public static List<ItemInfo> GetItems(int voucherID)
    //{
    //    using (DbProxy proxy = new DbProxy())
    //    {
    //        IDataReader dr = proxy.ExecuteReader("SELECT * FROM DocumentAccountingItems WHERE DocumentAccountingID=" + voucherID + " ORDER BY SortIndex, ID");
    //        return ObjectBinder.FillCollection<ItemInfo>(dr);
    //    }
    //}
    //public static List<ItemInfo> GetItems(int voucherID, int startIndex, int endIndex)
    //{
    //    using (DbProxy proxy = new DbProxy())
    //    {
    //        if (endIndex < 0)
    //            endIndex = Int32.MaxValue - 10;
    //        IDataReader dr = proxy.ExecuteReader(string.Format("SELECT * FROM DocumentAccountingItems WHERE DocumentAccountingID={0} AND {1}<=SortIndex AND SortIndex<={2} ORDER BY SortIndex"
    //            , voucherID, startIndex, endIndex));
    //        return ObjectBinder.FillCollection<ItemInfo>(dr);
    //    }
    //}
    //public static List<ItemInfo> GetItemsByProject(int projectID)
    //{
    //    if (projectID == Null.NullInteger)
    //        return null;
    //    using (DbProxy proxy = new DbProxy())
    //    {
    //        IDataReader dr = proxy.ExecuteReader("SELECT * FROM DocumentAccountingItems WHERE ProjectID=" + projectID);
    //        return ObjectBinder.FillCollection<ItemInfo>(dr);
    //    }
    //}
    //public static ItemInfo GetItem(int id)
    //{
    //    using (DbProxy proxy = new DbProxy())
    //    {
    //        IDataReader dr = proxy.ExecuteReader(string.Format("SELECT * FROM DocumentAccountingItems WHERE ID={0}", id));
    //        return ObjectBinder.FillObject<ItemInfo>(dr);
    //    }
    //}
    //public static ItemInfo GetItemByDocumentNo(string docNo)
    //{
    //    if (string.IsNullOrEmpty(docNo))
    //        return null;
    //    using (DbProxy proxy = new DbProxy())
    //    {
    //        IDataReader dr = proxy.ExecuteReader(string.Format("SELECT * FROM DocumentAccountingItems WHERE DocNo=N'{0}'", docNo));
    //        return ObjectBinder.FillObject<ItemInfo>(dr);
    //    }
    //}
    //public static List<ItemInfo> GetItemsByDocumentNo(string docNo)
    //{
    //    if (string.IsNullOrEmpty(docNo))
    //        return null;
    //    using (DbProxy proxy = new DbProxy())
    //    {
    //        IDataReader dr = proxy.ExecuteReader(string.Format("SELECT * FROM DocumentAccountingItems WHERE DocNo=N'{0}'", docNo));
    //        return ObjectBinder.FillCollection<ItemInfo>(dr);
    //    }
    //}
    //public static bool HasItems(int voucherID)
    //{
    //    object count = DbProxy.Instance.FillScalar("SELECT COUNT(ID) FROM DocumentAccountingItems WHERE DocumentAccountingID=" + voucherID);
    //    return Helper.GetInt(count, Null.NullInteger) > 0;
    //}
    public static bool HasItems(string treePath)
    {
        object count = DbProxy.Instance.FillScalar("SELECT COUNT(ID) FROM DocumentAccountingItems WHERE TreePath=" + Helper.QoutedString(treePath));
        return Helper.GetInt(count, Null.NullInteger) > 0;
    }
    //public static ItemInfo GetLastItem(int voucherID)
    //{
    //    using (DbProxy proxy = new DbProxy())
    //    {
    //        IDataReader dr = proxy.ExecuteReader(string.Format("SELECT TOP 1 * FROM DocumentAccountingItems WHERE DocumentAccountingID={0} ORDER BY SortIndex DESC", voucherID));
    //        return ObjectBinder.FillObject<ItemInfo>(dr);
    //    }
    //}
    //public static ItemInfo GetLastItem(int voucherID, int curItemID)
    //{
    //    if (curItemID == Null.NullInteger)
    //        return GetLastItem(voucherID);
    //    using (DbProxy proxy = new DbProxy())
    //    {
    //        IDataReader dr = proxy.ExecuteReader(string.Format("SELECT TOP 1 * FROM DocumentAccountingItems WHERE DocumentAccountingID={0} AND ID<{1} ORDER BY SortIndex DESC", voucherID, curItemID));
    //        return ObjectBinder.FillObject<ItemInfo>(dr);
    //    }
    //}
    //public static bool Update(ItemInfo info)
    //{
    //    string qry = string.Format("UPDATE DocumentAccountingItems SET DocumentAccountingID={0}, TreePath={1}, PersonnelID={2}, FileServiceID={3}, ProjectID={4}, SortIndex={5}, Owe={6}, Due={7}, DocNo={8}, DocDate={9}, Comments={10} WHERE ID={11}"
    //        , info.DocumentAccountingID, Helper.QoutedString(info.TreePath), info.PersonnelID, info.FileServiceID, info.ProjectID, info.SortIndex, info.Owe, info.Due
    //        , Helper.QoutedString(info.DocNo), Helper.QoutedDate(info.DocDate), Helper.QoutedString(info.Comments), info.ID);
    //    return DbProxy.Instance.RunQuery(qry) > 0;
    //}
    //public static bool Add(ItemInfo info)
    //{
    //    if (SiteSettings.IsDbReadonly)
    //        return false;
    //    // find last SortIndex
    //    if (info.SortIndex < 1)
    //    {
    //        info.SortIndex = Helper.GetInt(DbProxy.Instance.FillScalar(
    //            string.Format("SELECT MAX(SortIndex) FROM DocumentAccountingItems WHERE DocumentAccountingID={0}", info.DocumentAccountingID))
    //            , Null.NullInteger);
    //        if (info.SortIndex == Null.NullInteger)
    //            info.SortIndex = 0;
    //        else
    //            info.SortIndex++;
    //    }
    //    ////--------HADI--------
    //    if (IsPrerson(info.TreePath) && info.PersonnelID == null)
    //    {

    //        return false;
    //    }
    //    if (IsProject(info.TreePath) && info.ProjectID == null)
    //    {
    //        return false;
    //    }
    //    ////
    //    string qry = string.Format("INSERT INTO DocumentAccountingItems (DocumentAccountingID, TreePath, PersonnelID, FileServiceID, ProjectID, SortIndex, Owe, Due, DocNo, DocDate, Comments) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10});SELECT SCOPE_IDENTITY();"
    //        , info.DocumentAccountingID, Helper.QoutedString(info.TreePath), info.PersonnelID, info.FileServiceID, info.ProjectID, info.SortIndex, info.Owe, info.Due
    //        , Helper.QoutedString(info.DocNo), Helper.QoutedDate(info.DocDate), Helper.QoutedString(info.Comments));
    //    object id = DbProxy.Instance.FillScalar(qry);
    //    if (Null.IsNull(id))
    //        return false;
    //    info.ID = Convert.ToInt32(id);
    //    return true;
    //}
    //public static bool Insert(ItemInfo info, int rowIndex)
    //{
    //    if (Add(info))
    //    {
    //        MoveTo(info.ID, rowIndex);
    //        return true;
    //    }
    //    return false;
    //}
    //public static bool DeleteItem(int id)
    //{
    //    return DbProxy.Instance.RunQuery("DELETE FROM DocumentAccountingItems WHERE ID=" + id) > 0;
    //}
    //public static bool DeleteItems(int voucherID)
    //{
    //    return DbProxy.Instance.RunQuery("DELETE FROM DocumentAccountingItems WHERE DocumentAccountingID=" + voucherID) > 0;
    //}
    //public static bool DeleteItems(int voucherID, int fromSortIndex, int toSortIndex)
    //{
    //    return DbProxy.Instance.RunQuery(string.Format("DELETE FROM DocumentAccountingItems WHERE DocumentAccountingID={0} AND {1}<=SortIndex AND SortIndex<={2}"
    //        , voucherID, fromSortIndex, toSortIndex)) > 0;
    //}
    //public static bool MoveUp(int id)
    //{
    //    using (DbProxy proxy = new DbProxy())
    //    {
    //        IDataReader dr = proxy.ExecuteReader(
    //             string.Format("SELECT ID, SortIndex FROM DocumentAccountingItems WHERE DocumentAccountingID=(SELECT DocumentAccountingID FROM DocumentAccountingItems WHERE ID={0}) ORDER BY SortIndex", id));
    //        List<ItemInfo> items = ObjectBinder.FillCollection<ItemInfo>(dr);
    //        if (items == null || items.Count < 2 || items[0].ID == id)
    //            return false;

    //        for (int i = 0; i < items.Count; i++)
    //        {
    //            if (items[i].ID == id)
    //            {
    //                proxy.RunQuery(string.Format("UPDATE DocumentAccountingItems SET SortIndex={0} WHERE ID={1}", i - 1, items[i].ID));
    //                proxy.RunQuery(string.Format("UPDATE DocumentAccountingItems SET SortIndex={0} WHERE ID={1}", i, items[i - 1].ID));
    //            }
    //            else if (items[i].SortIndex != i)
    //                proxy.RunQuery(string.Format("UPDATE DocumentAccountingItems SET SortIndex={0} WHERE ID={1}", i, items[i].ID));
    //        }
    //        return true;
    //    }
    //}
    //public static bool MoveDown(int id)
    //{
    //    using (DbProxy proxy = new DbProxy())
    //    {
    //        IDataReader dr = proxy.ExecuteReader(
    //             string.Format("SELECT ID, SortIndex FROM DocumentAccountingItems WHERE DocumentAccountingID=(SELECT DocumentAccountingID FROM DocumentAccountingItems WHERE ID={0}) ORDER BY SortIndex", id));
    //        List<ItemInfo> items = ObjectBinder.FillCollection<ItemInfo>(dr);
    //        if (items == null || items.Count < 2 || items[items.Count - 1].ID == id)
    //            return false;

    //        for (int i = 0; i < items.Count; i++)
    //        {
    //            if (items[i].ID == id)
    //            {
    //                proxy.RunQuery(string.Format("UPDATE DocumentAccountingItems SET SortIndex={0} WHERE ID={1}", i + 1, items[i].ID));
    //                proxy.RunQuery(string.Format("UPDATE DocumentAccountingItems SET SortIndex={0} WHERE ID={1}", i, items[i + 1].ID));
    //                i++;
    //            }
    //            else if (items[i].SortIndex != i)
    //                proxy.RunQuery(string.Format("UPDATE DocumentAccountingItems SET SortIndex={0} WHERE ID={1}", i, items[i].ID));
    //        }
    //        return true;
    //    }
    //}

    //public static bool MoveTo(int id, int index)
    //{
    //    DataTable dt = DbProxy.Instance.FillData(string.Format("SELECT ID FROM DocumentAccountingItems WHERE DocumentAccountingID=(SELECT DocumentAccountingID FROM DocumentAccountingItems WHERE ID={0}) ORDER BY SortIndex", id), "fck");
    //    DbProxy.Instance.RunQuery(string.Format("UPDATE DocumentAccountingItems SET SortIndex={0} WHERE ID={1}", index, id));
    //    int _index = 0;
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        int _id = (int)dt.Rows[i][0];
    //        if (_id == id)
    //            continue;
    //        if (_index == index)
    //            _index++;
    //        DbProxy.Instance.RunQuery(string.Format("UPDATE DocumentAccountingItems SET SortIndex={0} WHERE ID={1}", _index, _id));
    //        _index++;
    //    }
    //    return true;
    //}
    #endregion

    #region Modifier
    //public static List<ModifierInfo> GetModifiers(int voucherID)
    //{
    //    using (DbProxy proxy = new DbProxy())
    //    {
    //        IDataReader dr = proxy.ExecuteReader("SELECT * FROM Modifier WHERE DocumentAccountingID=" + voucherID);
    //        return ObjectBinder.FillCollection<ModifierInfo>(dr);
    //    }
    //}
    //public static ModifierInfo GetModifier(int id)
    //{
    //    using (DbProxy proxy = new DbProxy())
    //    {
    //        IDataReader dr = proxy.ExecuteReader(string.Format("SELECT * FROM Modifier WHERE ID={0}", id));
    //        return ObjectBinder.FillObject<ModifierInfo>(dr);
    //    }
    //}
    //public static bool Update(ModifierInfo info)
    //{
    //    string qry = string.Format("UPDATE Modifier SET Date={0}, Comments={1} WHERE ID={2}"
    //        , Helper.QoutedDateTime(info.Date), Helper.QoutedString(info.Comments), info.ID);
    //    return DbProxy.Instance.RunQuery(qry) > 0;
    //}
    public static bool Add(ModifierInfo info)
    {
        string qry = string.Format("INSERT INTO Modifier (DocumentAccountingID, UserID, Date, Comments) VALUES ({0}, {1}, {2}, {3}); SELECT SCOPE_IDENTITY();"
            , info.DocumentAccountingID, info.UserID, Helper.QoutedDateTime(info.Date), Helper.QoutedString(info.Comments));
        object id = DbProxy.Instance.RunQuery(qry);
        if (Null.IsNull(id))
            return false;
        info.ID = Convert.ToInt32(id);
        return true;
    }
    //public static bool DeleteModifier(int id)
    //{
    //    return DbProxy.Instance.RunQuery("DELETE FROM Modifier WHERE ID=" + id) > 0;
    //}
    #endregion

    #region ReportSettings
    public static List<ReportSettingInfo> GetReportSettings()
    {
        using (DbProxy proxy = new DbProxy())
        {
            IDataReader dr = proxy.ExecuteReader("SELECT * FROM ReportSettings ORDER BY Title");
            return ObjectBinder.FillCollection<ReportSettingInfo>(dr);
        }
    }
    public static List<ReportSettingInfo> GetReportSettings(int userID, string reportName)
    {
        using (DbProxy proxy = new DbProxy())
        {
            IDataReader dr = proxy.ExecuteReader(string.Format("SELECT * FROM ReportSettings WHERE UserID ={0} AND ReportName={1} ORDER BY Title"
                , userID, Helper.QoutedString(reportName)));
            return ObjectBinder.FillCollection<ReportSettingInfo>(dr);
        }
    }
    public static ReportSettingInfo GetReportSetting(int id)
    {
        using (DbProxy proxy = new DbProxy())
        {
            IDataReader dr = proxy.ExecuteReader(string.Format("SELECT * FROM ReportSettings WHERE ID={0}", id));
            return ObjectBinder.FillObject<ReportSettingInfo>(dr);
        }
    }
    public static ReportSettingInfo GetReportSetting(int userID, string reportName, string title)
    {
        using (DbProxy proxy = new DbProxy())
        {
            IDataReader dr = proxy.ExecuteReader(string.Format("SELECT * FROM ReportSettings WHERE UserID ={0} AND ReportName={1} AND Title={2}"
                , userID, Helper.QoutedString(reportName), Helper.QoutedString(title)));
            return ObjectBinder.FillObject<ReportSettingInfo>(dr);
        }
    }
    public static bool Update(ReportSettingInfo info)
    {
        string qry = string.Format("UPDATE ReportSettings SET Title={0}, Settings={1} WHERE ID={2}"
            , Helper.QoutedString(info.Title), Helper.QoutedString(info.Settings), info.ID);
        return DbProxy.Instance.RunQuery(qry) > 0;
    }
    public static bool Add(ReportSettingInfo info)
    {
        if (SiteSettings.IsDbReadonly)
            return false;
        // do not save duplicate infos
        ReportSettingInfo old = GetReportSetting(info.UserID, info.ReportName, info.Title);
        if (old != null)
        {
            old.Settings = info.Settings;
            return Update(old);
        }
        string qry = string.Format("INSERT INTO ReportSettings (UserID, ReportName, Title, Settings) VALUES({0}, {1}, {2}, {3});SELECT SCOPE_IDENTITY();"
            , info.UserID, Helper.QoutedString(info.ReportName), Helper.QoutedString(info.Title), Helper.QoutedString(info.Settings));
        object id = DbProxy.Instance.FillScalar(qry);
        if (Null.IsNull(id))
            return false;
        info.ID = Convert.ToInt32(id);
        return true;
    }
    public static bool DeleteReportSetting(int id)
    {
        return DbProxy.Instance.RunQuery("DELETE FROM ReportSettings WHERE ID=" + id) > 0;
    }
    #endregion

    public static string GetTreeFullPath(string treePath)
    {
        if (string.IsNullOrEmpty(treePath))
            return "";
        string[] ids = treePath.Split('-');
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < ids.Length; i++)
        {
            builder.Append(GetTopic(Helper.GetInt(ids[i], 0)).Title);
            if (i < ids.Length - 1)
                builder.Append(" - ");
        }
        return builder.ToString();
    }
    public static string GetTreePartialPath(string treePath)
    {
        if (string.IsNullOrEmpty(treePath))
            return "";
        string[] ids = treePath.Split('-');
        if (ids.Length < 3)
            return GetTreeFullPath(treePath);
        StringBuilder builder = new StringBuilder();
        builder.Append(GetTopic(Helper.GetInt(ids[0], 0)).Title);
        builder.Append("-...-");
        builder.Append(GetTopic(Helper.GetInt(ids[ids.Length - 1], 0)).Title);

        return builder.ToString();
    }
    public static string GetTreeFullPathCode(string treePath)
    {
        if (string.IsNullOrEmpty(treePath))
            return "";
        string[] ids = treePath.Split('-');
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < ids.Length; i++)
        {
            builder.Append(GetTopic(Helper.GetInt(ids[i], 0)).Code);
            if (i < ids.Length - 1)
                builder.Append(" - ");
        }
        return builder.ToString();
    }

    public static string GetTreeElaborativePath(string treePath)
    {
        if (string.IsNullOrEmpty(treePath))
            return "";
        string[] ids = treePath.Split('-');
        if (ids.Length < 3)
            return "";
        StringBuilder builder = new StringBuilder();
        for (int i = 2; i < ids.Length; i++)
        {
            builder.Append(GetTopic(Helper.GetInt(ids[i], 0)).Title);
            if (i < ids.Length - 1)
                builder.Append(" - ");
        }
        return builder.ToString();
    }

    private static Dictionary<int, TopicInfo> TopicBag = null;
    public static void ClearBags()
    {
        TopicBag = null;
    }
    public static string GetTopicTitle(int id)
    {
        if (id == Null.NullInteger)
            return "";
        TopicInfo topic = GetTopic(id);
        return topic == null ? "" : topic.Title;
    }
    public static string GetTopicCode(int id)
    {
        if (id == Null.NullInteger)
            return "";
        TopicInfo topic = GetTopic(id);
        return topic == null ? "" : topic.Code;
    }
    public static string GetTopicCode(string id)
    {
        return GetTopicCode(Helper.GetInt(id, Null.NullInteger));
    }
    public static int GetTopicIDByCode(string code, string errorMsg)
    {
        if (string.IsNullOrEmpty(code))
            throw new Exception(errorMsg);
        using (DbProxy proxy = new DbProxy())
        {
            IDataReader dr = proxy.ExecuteReader(string.Format("SELECT * FROM Topic WHERE [Code]=N'{0}'", code));
            TopicInfo info = ObjectBinder.FillObject<TopicInfo>(dr);
            if (null == info)
                throw new Exception(errorMsg);
            return info.ID;
        }
    }
    public static int GetTopicID(string codeName, string errorMsg)
    {
        if (string.IsNullOrEmpty(codeName))
            throw new Exception(errorMsg);
        using (DbProxy proxy = new DbProxy())
        {
            Int32 dummy;
            IDataReader dr;
            if (Int32.TryParse(codeName, out dummy))
                dr = proxy.ExecuteReader(string.Format("SELECT * FROM Topic WHERE [Code]=N'{0}'", codeName));
            else
                dr = proxy.ExecuteReader(string.Format("SELECT * FROM Topic WHERE [Name]=N'{0}'", codeName));
            TopicInfo info = ObjectBinder.FillObject<TopicInfo>(dr);
            if (null == info)
                throw new Exception(errorMsg);
            return info.ID;
        }
    }

    #region Reports
    public static IDataReader GetVoucherDocument(int voucherID)
    {
        string qry = string.Format("SELECT * FROM  [dbo].[GetVoucherDocument]({0})", voucherID);
        IDataReader dr = DbProxy.Instance.ExecuteReader(qry);
        return dr;
    }
    public static string GetVoucherPrograms(int voucherID)
    {
        if (Null.NullInteger == voucherID)
            return "";
        object obj = DbProxy.Instance.FillScalar("GetVoucherPrograms", voucherID);
        return Helper.GetString(obj, "");
    }
    public static DataTable GetPaperBook(int totalID, DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int userID, int personID, int projectID
    , int programID, bool personNeed, bool initRemain
        , decimal Amount)
    {
        return GetPaperBook(totalID, startDate, endDate, fromVoucherID, toVoucherID, userID, personID, projectID
            , programID, personNeed, initRemain, Amount, null, "");
    }
    public static DataTable GetPaperBook(int totalID, DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int userID, int personID, int projectID
    , int programID, bool personNeed, bool initRemain
        , decimal Amount, string sortCol, string sortOrder)
    {
        return GetPaperBook(totalID, startDate, endDate, fromVoucherID, toVoucherID, userID, personID, projectID
            , programID, personNeed, initRemain
            , Amount, sortCol, sortOrder, null);
    }
    public static DataTable GetPaperBook(int totalID, DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int userID, int personID, int projectID
    , int programID, bool personNeed, bool initRemain
        , decimal Amount, string sortCol, string sortOrder, List<string> dbs)
    {
        string qry1 = string.Format(@"SELECT DocumentAccounting.ID, DocumentAccountingItems.ID ItemID, DocumentAccounting.Date, DocumentAccounting.No No, dbo.TreePathToTopicTitle(DocumentAccountingItems.TreePath) AS TreePathName
, dbo.TreePathToTopicCode(DocumentAccountingItems.TreePath) AS TopicCode
, DocumentAccountingItems.Comments, ISNULL(DocumentAccountingItems.Owe, 0) AS Owe, ISNULL(DocumentAccountingItems.Due, 0) AS Due
, Personnel.Name + ' ' + Personnel.Surname AS PersonnelName, Ward.Name AS WardName, PersonnelID, ProjectID, DocumentAccounting.Mode
, DocumentAccounting.OtherID, DocumentAccountingItems.SortIndex, DATEADD(dd, 0, DATEDIFF(dd, 0, DocumentAccounting.Date))
FROM DocumentAccounting 
INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID 
LEFT OUTER JOIN Ward ON DocumentAccountingItems.ProjectID=Ward.ID 
WHERE DocumentAccounting.WorkflowStatus = 1 AND {0} <= Date AND Date <= {1} "
            , Helper.QoutedDateTime(startDate), Helper.QoutedDateTime(endDate));
        if (personID != Null.NullInteger)
            qry1 += " AND DocumentAccountingItems.PersonnelID = " + personID;
        if (projectID != Null.NullInteger)
            qry1 += " AND DocumentAccountingItems.ProjectID = " + projectID;

        // ------------------------------
        if (programID == -1) // default program = NULL (See SiteSettings.cs)
            qry1 += " AND DocumentAccounting.ProgramID IS NULL";
        else if (programID == 0) // all programs
        {
        }
        else
            qry1 += " AND  DocumentAccounting.ProgramID = " + programID;
        
        if (userID != Null.NullInteger)
            qry1 += " AND DocumentAccounting.UserID = " + userID;

        if (personNeed)
            qry1 += " AND DocumentAccountingItems.TreePath IN (SELECT Trees.TreePath FROM Trees WHERE Trees.PersonNeed=1) ";

        if (Amount >= 0)
            qry1 += " AND  (ISNULL(DocumentAccountingItems.Due, 0)=" + Amount + " OR ISNULL(DocumentAccountingItems.Owe, 0) =" + Amount + ")";


        if (initRemain)
        {
            string qry2 = string.Format(@"SELECT NULL, 1, '1990-01-01', -1 No, '' TreePathName, '' TopicCode
, '' Comments, SUM(ISNULL(DocumentAccountingItems.Owe, 0)) Owe, SUM(ISNULL(DocumentAccountingItems.Due, 0)) Due
, '' PersonnelName, '' WardName, 1 PersonnelID, 1 ProjectID, 1 Mode, 1 OtherID, -1, NULL
FROM DocumentAccounting 
INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID 
LEFT OUTER JOIN Ward ON DocumentAccountingItems.ProjectID=Ward.ID 
WHERE DocumentAccounting.WorkflowStatus = 1 AND Date < {0} "
            , Helper.QoutedDateTime(startDate));
            if (personID != Null.NullInteger)
                qry2 += " AND DocumentAccountingItems.PersonnelID = " + personID;
            if (projectID != Null.NullInteger)
                qry2 += " AND DocumentAccountingItems.ProjectID = " + projectID;

            // ------------------------------
            if (programID == -1) // default program = NULL (See SiteSettings.cs)
                qry2 += " AND DocumentAccounting.ProgramID IS NULL";
            else if (programID == 0) // all programs
            {
            }
            else
                qry2 += " AND  DocumentAccounting.ProgramID = " + programID;

            if (userID != Null.NullInteger) // default program = NULL (See SiteSettings.cs)
                qry2 += " AND DocumentAccounting.UserID = " + userID;

            if (personNeed)
                qry2 += " AND DocumentAccountingItems.TreePath IN (SELECT Trees.TreePath FROM Trees WHERE Trees.PersonNeed=1) ";

            qry1 = qry1 + " UNION " + qry2;
        }
        if (!string.IsNullOrEmpty(sortCol))
            qry1 += " ORDER BY " + sortCol + " " + sortOrder;
        else
            qry1 += " ORDER BY  DATEADD(dd, 0, DATEDIFF(dd, 0, Date)), CAST(DocumentAccounting.No AS INT), DocumentAccounting.ID, DocumentAccountingItems.SortIndex";

        DataTable dt = DbProxy.Instance.FillData(qry1, "book", dbs, string.IsNullOrEmpty(sortCol) ? null : (sortCol + " " + sortOrder));
        return dt;
    }
    public static DataTable GetPaperBookSum(int totalID, DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int personID, int projectID
    , int programID, bool personNeed, bool initRemain)
    {
        return GetPaperBookSum(totalID, startDate, endDate, fromVoucherID, toVoucherID, personID, projectID
            , programID, personNeed, initRemain, null, "");
    }
    public static DataTable GetPaperBookSum(int totalID, DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int personID, int projectID
    , int programID, bool personNeed, bool initRemain, string sortCol, string sortOrder)
    {
        return GetPaperBookSum(totalID, startDate, endDate, fromVoucherID, toVoucherID, personID, projectID
            , programID, personNeed, initRemain, sortCol, sortOrder, null);
    }
    public static DataTable GetPaperBookSum(int totalID, DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int personID, int projectID
    , int programID, bool personNeed, bool initRemain, string sortCol, string sortOrder, List<string> dbs)
    {
        string qry1 = string.Format(@"SELECT 1 ID, 1 ItemID, NULL Date, NULL No, TreePath, dbo.TreePathToTopicTitle(DocumentAccountingItems.TreePath) AS TreePathName
, dbo.TreePathToTopicCode(DocumentAccountingItems.TreePath) AS TopicCode
, NULL Comments, SUM(ISNULL(DocumentAccountingItems.Owe, 0)) Owe, SUM(ISNULL(DocumentAccountingItems.Due, 0)) Due
FROM DocumentAccounting 
INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
WHERE DocumentAccounting.WorkflowStatus = 1 AND {0} <= Date AND Date <= {1} "
            , Helper.QoutedDateTime(startDate), Helper.QoutedDateTime(endDate));
        if (personID != Null.NullInteger)
            qry1 += " AND DocumentAccountingItems.PersonnelID = " + personID;
        if (projectID != Null.NullInteger)
            qry1 += " AND DocumentAccountingItems.ProjectID = " + projectID;

        // ------------------------------
        if (programID == -1) // default program = NULL (See SiteSettings.cs)
            qry1 += " AND DocumentAccounting.ProgramID IS NULL";
        else if (programID == 0) // all programs
        {
        }
        else
            qry1 += " AND  DocumentAccounting.ProgramID = " + programID;
        #region new solution
        if (personNeed)
            qry1 += " AND DocumentAccountingItems.TreePath IN (SELECT Trees.TreePath FROM Trees WHERE Trees.PersonNeed=1) ";
        #endregion

        qry1 += " GROUP BY TreePath ";

        if (initRemain)
        {
            string qry2 = string.Format(@"SELECT 1 ID, 1 ItemID, NULL Date, NULL No, '' TreePath, '' AS TreePathName
, '' AS TopicCode
, NULL Comments, SUM(ISNULL(DocumentAccountingItems.Owe, 0)) Owe, SUM(ISNULL(DocumentAccountingItems.Due, 0)) Due
FROM DocumentAccounting 
INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
WHERE DocumentAccounting.WorkflowStatus = 1 AND Date < {0} "
            , Helper.QoutedDateTime(startDate));
            if (personID != Null.NullInteger)
                qry2 += " AND DocumentAccountingItems.PersonnelID = " + personID;
            if (projectID != Null.NullInteger)
                qry2 += " AND DocumentAccountingItems.ProjectID = " + projectID;

            // ------------------------------
            if (programID == -1) // default program = NULL (See SiteSettings.cs)
                qry2 += " AND DocumentAccounting.ProgramID IS NULL";
            else if (programID == 0) // all programs
            {
            }
            else
                qry2 += " AND  DocumentAccounting.ProgramID = " + programID;
            #region new solution
            if (personNeed)
                qry2 += " AND DocumentAccountingItems.TreePath IN (SELECT Trees.TreePath FROM Trees WHERE Trees.PersonNeed=1) ";
            #endregion

            qry1 = qry1 + " UNION " + qry2;
        }
        if (!string.IsNullOrEmpty(sortCol))
            qry1 += " ORDER BY " + sortCol + " " + sortOrder;
        else
            qry1 += " ORDER BY TreePath";

        DataTable dt = DbProxy.Instance.FillData(qry1, "book", dbs, string.IsNullOrEmpty(sortCol) ? null : (sortCol + " " + sortOrder));
        return dt;
    }
    #region old
    //    public static IDataReader GetNewPersonBook(int totalID, DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int personID, int projectID
    //        , int programID, bool groupByListOFDOrC, bool groupByPersonnel, decimal? fromRemain, decimal? toRemain, string[] treePathes, bool isCoworkerPerson
    //        , string pgIDs, string pgWhere, string sortCol, string sortOrder)
    //    {
    //        string where1 = string.Format(@" {0} <= Date AND Date <= {1} ", Helper.QoutedDateTime(startDate), Helper.QoutedDateTime(endDate));

    //        #region new solution
    //        if (isCoworkerPerson)
    //            where1 += " AND DocumentAccountingItems.TreePath IN (SELECT Trees.TreePath FROM Trees WHERE Trees.PersonNeed=0) ";
    //        else
    //            where1 += " AND DocumentAccountingItems.TreePath IN (SELECT Trees.TreePath FROM Trees WHERE Trees.PersonNeed=1) ";
    //        #endregion

    //        string qry = @"SELECT DocumentAccounting.ID, DocumentAccountingItems.ID ItemID, DocumentAccounting.OtherID, DocumentAccounting.Mode, DocumentAccounting.Date, DocumentAccounting.No, dbo.TreePathToTopicTitle(DocumentAccountingItems.TreePath) AS TreePathName
    //, dbo.TreePathToTopicCode(DocumentAccountingItems.TreePath) AS TopicCode
    //, DocumentAccountingItems.Comments, DocumentAccountingItems.Owe, DocumentAccountingItems.Due 
    //, Personnel.Name + ' ' + Personnel.Surname AS PersonnelName, Ward.Name AS WardName, PersonnelID, ProjectID
    //FROM DocumentAccounting 
    //INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
    //LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID 
    //LEFT OUTER JOIN Ward ON DocumentAccountingItems.ProjectID=Ward.ID ";

    //        bool grouped = false;
    //        if (groupByPersonnel || groupByListOFDOrC)
    //        {
    //            if (personID != Null.NullInteger || isCoworkerPerson)
    //                qry = @"SELECT DocumentAccounting.ID, DocumentAccountingItems.ID ItemID, DocumentAccounting.OtherID, DocumentAccounting.Mode, DocumentAccounting.Date, DocumentAccounting.No, dbo.TreePathToTopicTitle(DocumentAccountingItems.TreePath) AS TreePathName
    //, dbo.TreePathToTopicCode(DocumentAccountingItems.TreePath) AS TopicCode
    //, DocumentAccountingItems.Comments, DocumentAccountingItems.Owe, DocumentAccountingItems.Due, (DocumentAccountingItems.Due - DocumentAccountingItems.Owe) Remained
    //, Personnel.Name + ' ' + Personnel.Surname AS PersonnelName, Ward.Name AS WardName, PersonnelID, ProjectID
    //FROM DocumentAccounting 
    //INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
    //LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID 
    //LEFT OUTER JOIN Ward ON DocumentAccountingItems.ProjectID=Ward.ID ";
    //            else
    //            {
    //                qry = @"SELECT NULL ID, NULL ItemID, NULL OtherID, NULL Mode, NULL AS Date, NULL No, NULL TreePathName, NULL AS TopicCode
    //, NULL Comments, SUM(ISNULL(DocumentAccountingItems.Owe, 0)) Owe, SUM(ISNULL(DocumentAccountingItems.Due, 0)) Due, SUM(ISNULL(DocumentAccountingItems.Due, 0) - ISNULL(DocumentAccountingItems.Owe, 0)) Remained
    //, Personnel.Name + ' ' + Personnel.Surname AS PersonnelName, NULL WardName, PersonnelID, Personnel.HomeTelNum, Personnel.CellNumber1, Personnel.HomeAddress
    //FROM DocumentAccounting 
    //INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
    //LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID 
    //LEFT OUTER JOIN Ward ON DocumentAccountingItems.ProjectID=Ward.ID ";
    //                grouped = true;
    //            }
    //        }
    //        // ------------------------------
    //        if (programID == -1) // default program = NULL (See SiteSettings.cs)
    //            where1 += " AND DocumentAccounting.ProgramID IS NULL";
    //        else if (programID == 0) // all programs
    //        {
    //        }
    //        else
    //            where1 += " AND  DocumentAccounting.ProgramID = " + programID;

    //        string where2 = "";
    //        if (fromRemain != null)
    //        {
    //            if (groupByPersonnel || groupByListOFDOrC)
    //                where2 += string.Format(" AND DocumentAccountingItems.PersonnelID IN (SELECT PersonnelID FROM DocumentAccountingItems LEFT JOIN DocumentAccounting ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID WHERE {0} GROUP BY PersonnelID", where1);
    //            else
    //                where2 += string.Format(" AND DocumentAccountingItems.ProjectID IN (SELECT ProjectID FROM DocumentAccountingItems LEFT JOIN DocumentAccounting ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID WHERE {0} GROUP BY ProjectID", where1);
    //            where2 += string.Format(" HAVING SUM(ISNULL(DocumentAccountingItems.Due, 0)-ISNULL(DocumentAccountingItems.Owe, 0)) >= {0} )", fromRemain.Value);
    //        }
    //        if (toRemain != null)
    //        {
    //            if (groupByPersonnel || groupByListOFDOrC)
    //                where2 += string.Format(" AND DocumentAccountingItems.PersonnelID IN (SELECT PersonnelID FROM DocumentAccountingItems LEFT JOIN DocumentAccounting ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID WHERE {0} GROUP BY PersonnelID ", where1);
    //            else
    //                where2 += string.Format(" AND DocumentAccountingItems.ProjectID IN (SELECT ProjectID FROM DocumentAccountingItems LEFT JOIN DocumentAccounting ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID WHERE {0} GROUP BY ProjectID ", where1);
    //            where2 += string.Format("  HAVING SUM(ISNULL(DocumentAccountingItems.Due, 0)-ISNULL(DocumentAccountingItems.Owe, 0)) <= {0} )", toRemain.Value);
    //        }
    //        where1 += where2;
    //        if (personID != Null.NullInteger)
    //            where1 += " AND DocumentAccountingItems.PersonnelID = " + personID;
    //        else if (groupByPersonnel || groupByListOFDOrC)
    //            where1 += " AND DocumentAccountingItems.PersonnelID IS NOT NULL ";
    //        if (projectID != Null.NullInteger)
    //            where1 += " AND DocumentAccountingItems.ProjectID = " + projectID;
    //        else if (!(groupByPersonnel || groupByListOFDOrC))
    //            where1 += " AND DocumentAccountingItems.ProjectID IS NOT NULL ";

    //        if (groupByListOFDOrC && !string.IsNullOrEmpty(pgIDs) && !string.IsNullOrEmpty(pgWhere))
    //        {
    //            // ------------------------
    //            // ids: 1,2,5
    //            // where: '&1%' AND ('&2%' OR '&5%')
    //            // ------------------------
    //            string[] ids = pgIDs.Split(',');
    //            where1 += "\r\nAND (\r\n";
    //            for (int i = 0; i < ids.Length; i++)
    //            {
    //                StringBuilder allIds = new StringBuilder();
    //                int j = 0;
    //                IDataReader childIds = DbProxy.Instance.ExecuteReader("SELECT ID FROM [GetAllGroupingChilds](" + ids[i] + ")");
    //                while (childIds.Read())
    //                {
    //                    if (j != 0)
    //                    {
    //                        allIds.Append(",");
    //                    }
    //                    allIds.Append(childIds.GetValue(0));
    //                    j++;
    //                }
    //                pgWhere = pgWhere.Replace("'&" + ids[i] + "%'", " Personnel.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + ")) \r\n");
    //                pgWhere = pgWhere.Replace("'!&" + ids[i] + "%'", " NOT Personnel.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + ")) \r\n");
    //                //where1 += "AND Personnel.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + "))";
    //            }
    //            where1 += pgWhere;
    //            where1 += "\r\n)\r\n";
    //        }

    //        qry += " WHERE " + where1;
    //        if (grouped)
    //        {
    //            qry += @" GROUP BY Personnel.Name + ' ' + Personnel.Surname, PersonnelID, Personnel.HomeTelNum, Personnel.CellNumber1, Personnel.HomeAddress";
    //            if (!string.IsNullOrEmpty(sortCol))
    //                qry += " ORDER BY " + sortCol + " " + sortOrder;
    //        }
    //        else
    //        {
    //            if (!string.IsNullOrEmpty(sortCol))
    //                qry += " ORDER BY " + sortCol + " " + sortOrder;
    //            else if (groupByPersonnel || groupByListOFDOrC)
    //                qry += " ORDER BY DocumentAccountingItems.PersonnelID, DATEADD(dd, 0, DATEDIFF(dd, 0, Date)), CAST(DocumentAccounting.No AS INT), DocumentAccountingItems.ID";
    //            else
    //                qry += " ORDER BY DocumentAccountingItems.ProjectID, DATEADD(dd, 0, DATEDIFF(dd, 0, Date)), CAST(DocumentAccounting.No AS INT), DocumentAccountingItems.ID";
    //        }

    //        IDataReader dr = DbProxy.Instance.ExecuteReader(qry);
    //        return dr;
    //    }
    #endregion
    public enum PersonBookGroupBy
    {
        ByPersonnel = 0,
        ByProject = 1,
        ByOweDue = 2,
        ByOwe = 3,
        ByDue = 4,
        //ByTotal = 5,
        ByEquipment = 6,
        ByCostCenter = 7
    }

    public static DataTable GetPersonBook(int totalID, DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int personID, int projectID
        , int programID, PersonBookGroupBy by, decimal? fromRemain, decimal? toRemain, string[] treePathes, bool isCoworkerPerson
        , string filter, string sortCol, string sortOrder, bool initRemain, List<string> dbs, bool showAllVouchers, int personGroupID)
    {
        string where1 = string.Format(@" (DocumentAccountingView.WorkflowStatus = 1 OR {0} = 1) AND {1} <= Date AND Date <= {2} "
            , showAllVouchers ? 1 : 0, Helper.QoutedDateTime(startDate), Helper.QoutedDateTime(endDate));
        string where3 = string.Format(@" (DocumentAccountingView.WorkflowStatus = 1 OR {0} = 1) AND Date < {1} "
            , showAllVouchers ? 1 : 0, Helper.QoutedDateTime(startDate));

        #region new solution
        if (isCoworkerPerson)
            where1 += " AND DocumentAccountingItems.TreePath IN (SELECT Trees.TreePath FROM Trees WHERE Trees.PersonNeed=0) ";
        else
            where1 += " AND DocumentAccountingItems.TreePath IN (SELECT Trees.TreePath FROM Trees WHERE Trees.PersonNeed=1) ";
        #endregion

        string qry2 = "", qry3 = "";
        string qry = @"SELECT DocumentAccountingView.ID, DocumentAccountingItems.ID ItemID, DocumentAccountingView.OtherID, DocumentAccountingView.Mode, DocumentAccountingView.Date, DocumentAccountingView.No, dbo.TreePathToTopicTitle(DocumentAccountingItems.TreePath) AS TreePathName
        , dbo.TreePathToTopicCode(DocumentAccountingItems.TreePath) AS TopicCode
        , DocumentAccountingItems.Comments, ISNULL(DocumentAccountingItems.Owe, 0) AS Owe, ISNULL(DocumentAccountingItems.Due, 0) AS Due 
        , Personnel.Name + ' ' + Personnel.Surname AS PersonnelName, Personnel.Code AS PersonCode, Ward.Name AS WardName, PersonnelID, ProjectID
        , '' StoreDraftNo, '' PurchaseNo, '' ReturnNo
        , '' RefuseNo, '' FactorNo 
        FROM DocumentAccountingView 
        INNER JOIN DocumentAccountingItems ON DocumentAccountingView.ID=DocumentAccountingItems.DocumentAccountingID
        LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID 
        LEFT OUTER JOIN Ward ON DocumentAccountingItems.ProjectID=Ward.ID";

        bool grouped = false;
        if (by == PersonBookGroupBy.ByPersonnel || by == PersonBookGroupBy.ByOwe || by == PersonBookGroupBy.ByDue || by == PersonBookGroupBy.ByOweDue)
        {
            if (personID != Null.NullInteger || isCoworkerPerson)
            {
                qry = @"SELECT DocumentAccountingView.ID, DocumentAccountingItems.ID ItemID, DocumentAccountingView.OtherID, DocumentAccountingView.Mode, DocumentAccountingView.Date, DocumentAccountingView.No, dbo.TreePathToTopicTitle(DocumentAccountingItems.TreePath) AS TreePathName
                    , dbo.TreePathToTopicCode(DocumentAccountingItems.TreePath) AS TopicCode
                    , DocumentAccountingItems.Comments, ISNULL(DocumentAccountingItems.Owe, 0) AS Owe, ISNULL(DocumentAccountingItems.Due, 0) AS Due, (ISNULL(DocumentAccountingItems.Due, 0) - ISNULL(DocumentAccountingItems.Owe, 0)) Remained
                    , Personnel.Name + ' ' + Personnel.Surname AS PersonnelName, Personnel.Code AS PersonCode, Ward.Name AS WardName, PersonnelID, ProjectID
                    , [dbo].[PaidOrNotPaidOwe](DocumentAccountingItems.ID, 0) Paid
                    , DocumentAccountingView.StoreDraftNo, DocumentAccountingView.PurchaseNo, DocumentAccountingView.ReturnNo
                    , DocumentAccountingView.RefuseNo, '' FactorNo
                    FROM DocumentAccountingView 
                    INNER JOIN DocumentAccountingItems ON DocumentAccountingView.ID=DocumentAccountingItems.DocumentAccountingID
                    LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID 
                    LEFT OUTER JOIN Ward ON DocumentAccountingItems.ProjectID=Ward.ID 
                    LEFT OUTER JOIN EquipmentView ON DocumentAccountingItems.EquipmentID=EquipmentView.ID";
                if (initRemain)
                {
                    qry2 = @"SELECT 0 ID, 0 ItemID, 0 OtherID, 0 Mode, '' Date, 0 No, '' TreePathName
                    , '' TopicCode
                    , '' Comments, SUM(ISNULL(DocumentAccountingItems.Owe, 0)) Owe, SUM(ISNULL(DocumentAccountingItems.Due, 0)) Due, 0 Remained
                    , '' PersonnelName, '' AS PersonCode, '' WardName, 0 PersonnelID, 0 ProjectID, -2 Paid
                    , '' StoreDraftNo, '' PurchaseNo, '' ReturnNo
                    , '' RefuseNo, '' FactorNo 
                    FROM DocumentAccountingView 
                    INNER JOIN DocumentAccountingItems ON DocumentAccountingView.ID=DocumentAccountingItems.DocumentAccountingID
                    LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID 
                    LEFT OUTER JOIN Ward ON DocumentAccountingItems.ProjectID=Ward.ID
                    LEFT OUTER JOIN EquipmentView ON DocumentAccountingItems.EquipmentID=EquipmentView.ID";
                }
            }
            else
            {
                qry = @"SELECT NULL ID, NULL ItemID, NULL OtherID, NULL Mode, NULL AS Date, NULL No, NULL TreePathName, NULL AS TopicCode
                    , NULL Comments, SUM(ISNULL(DocumentAccountingItems.Owe, 0)) Owe, SUM(ISNULL(DocumentAccountingItems.Due, 0)) Due, SUM(ISNULL(DocumentAccountingItems.Due, 0) - ISNULL(DocumentAccountingItems.Owe, 0)) Remained
                    , Personnel.Name + ' ' + Personnel.Surname AS PersonnelName, Personnel.Code AS PersonCode, NULL WardName, PersonnelID
                    , Personnel.HomeTelNum, Personnel.CellNumber1, Personnel.HomeAddress
                    , CASE WHEN CHARINDEX('-',Pidentify,2) > 0 THEN SUBSTRING(PIdentify,2,CHARINDEX('-',PIdentify,2)-2) ELSE NULL END FIdentify
                    , CASE WHEN CHARINDEX('-',Pidentify,2) > 0 THEN reverse(SUBSTRING(reverse(PIdentify),1,CHARINDEX('-',reverse(PIdentify),2)-1)) ELSE NULL END SIdentify
                    , '' StoreDraftNo, '' PurchaseNo, '' ReturnNo
                    , '' RefuseNo, '' FactorNo 
                    FROM DocumentAccountingView 
                    INNER JOIN DocumentAccountingItems ON DocumentAccountingView.ID=DocumentAccountingItems.DocumentAccountingID
                    LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID 
                    LEFT OUTER JOIN Ward ON DocumentAccountingItems.ProjectID=Ward.ID
                    LEFT OUTER JOIN EquipmentView ON DocumentAccountingItems.EquipmentID=EquipmentView.ID ";
                qry3 = @"
                union
                SELECT DISTINCT NULL ID, NULL ItemID, NULL OtherID, NULL Mode, NULL AS Date, NULL No, NULL TreePathName, NULL AS TopicCode
                , NULL Comments, 0 Owe, 0 Due, 0 Remained
                , Personnel.Name + ' ' + Personnel.Surname AS PersonnelName, Personnel.Code AS PersonCode, NULL WardName, OtherID AS PersonnelID
                , Personnel.HomeTelNum, Personnel.CellNumber1, Personnel.HomeAddress
                , CASE WHEN CHARINDEX('-',Pidentify,2) > 0 THEN SUBSTRING(PIdentify,2,CHARINDEX('-',PIdentify,2)-2) ELSE NULL END FIdentify
                , CASE WHEN CHARINDEX('-',Pidentify,2) > 0 THEN reverse(SUBSTRING(reverse(PIdentify),1,CHARINDEX('-',reverse(PIdentify),2)-1)) ELSE NULL END SIdentify
                , '' StoreDraftNo, '' PurchaseNo, '' ReturnNo
                , '' RefuseNo, '' FactorNo                
                 FROM GroupingMember
                INNER JOIN Grouping ON Grouping.ID=GroupingMember.GroupingID
                LEFT OUTER JOIN Personnel ON GroupingMember.OtherID=Personnel.ID
                WHERE NOT Personnel.ID IN (SELECT PersonnelID FROM DocumentAccountingItems where NOT PersonnelID IS NULL ) AND GroupingType = 0";
                grouped = true;
            }
        }
        // ------------------------------
        if (programID == -1) // default program = NULL (See SiteSettings.cs)
        {
            where1 += " AND DocumentAccountingView.ProgramID IS NULL";
            where3 += " AND DocumentAccountingView.ProgramID IS NULL";
        }
        else if (programID == 0) // all programs
        {
        }
        else
        {
            where1 += " AND  DocumentAccountingView.ProgramID = " + programID;
            where3 += " AND  DocumentAccountingView.ProgramID = " + programID;
        }
        string where2 = "";
        if (by == PersonBookGroupBy.ByDue)
            if (fromRemain == null)
                fromRemain = 0;
        if (by == PersonBookGroupBy.ByOwe)
        { // for that user able to enter +number
            if ((toRemain >= 0) || fromRemain >= 0)
            {
                toRemain = -1 * toRemain;
                fromRemain = -1 * fromRemain;
                decimal? to = toRemain;
                toRemain = fromRemain;
                fromRemain = to;
            }
            else if (fromRemain == null && toRemain == null)
            {
                toRemain = 0;
            }
        }
        if (fromRemain != null)
        {
            if (by == PersonBookGroupBy.ByPersonnel || by == PersonBookGroupBy.ByOwe || by == PersonBookGroupBy.ByDue || by == PersonBookGroupBy.ByOweDue)
                where2 += string.Format(" AND DocumentAccountingItems.PersonnelID IN (SELECT PersonnelID FROM DocumentAccountingItems LEFT JOIN DocumentAccountingView ON DocumentAccountingView.ID=DocumentAccountingItems.DocumentAccountingID WHERE {0} GROUP BY PersonnelID", where1);
            else
                where2 += string.Format(" AND DocumentAccountingItems.ProjectID IN (SELECT ProjectID FROM DocumentAccountingItems LEFT JOIN DocumentAccountingView ON DocumentAccountingView.ID=DocumentAccountingItems.DocumentAccountingID WHERE {0} GROUP BY ProjectID", where1);
            where2 += string.Format(" HAVING SUM(ISNULL(DocumentAccountingItems.Due, 0)-ISNULL(DocumentAccountingItems.Owe, 0)) >= {0} )", fromRemain.Value);
        }
        if (toRemain != null)
        {
            if (by == PersonBookGroupBy.ByPersonnel || by == PersonBookGroupBy.ByOwe || by == PersonBookGroupBy.ByDue || by == PersonBookGroupBy.ByOweDue)
                where2 += string.Format(" AND DocumentAccountingItems.PersonnelID IN (SELECT PersonnelID FROM DocumentAccountingItems LEFT JOIN DocumentAccountingView ON DocumentAccountingView.ID=DocumentAccountingItems.DocumentAccountingID WHERE {0} GROUP BY PersonnelID ", where1);
            else
                where2 += string.Format(" AND DocumentAccountingItems.ProjectID IN (SELECT ProjectID FROM DocumentAccountingItems LEFT JOIN DocumentAccountingView ON DocumentAccountingView.ID=DocumentAccountingItems.DocumentAccountingID WHERE {0} GROUP BY ProjectID ", where1);
            where2 += string.Format("  HAVING SUM(ISNULL(DocumentAccountingItems.Due, 0)-ISNULL(DocumentAccountingItems.Owe, 0)) <= {0} )", toRemain.Value);
        }
        where1 += where2;
        if (personID != Null.NullInteger)
        {
            where1 += " AND DocumentAccountingItems.PersonnelID = " + personID;
            where3 += " AND DocumentAccountingItems.PersonnelID = " + personID;
        }
        else if (by == PersonBookGroupBy.ByPersonnel || by == PersonBookGroupBy.ByOweDue)
        {
            where1 += " AND DocumentAccountingItems.PersonnelID IS NOT NULL ";
            where3 += " AND DocumentAccountingItems.PersonnelID IS NOT NULL ";
        }
        if (personGroupID != Null.NullInteger)
        {
            where1 += " AND Personnel.PersonnelGroupID = " + personGroupID;
            where3 += " AND Personnel.PersonnelGroupID = " + personGroupID;
        }
        //else if (by == PersonBookGroupBy.ByOwe)
        //    where1 += " AND DocumentAccountingItems.PersonnelID IS NOT NULL AND ISNULL(DocumentAccountingItems.Owe, 0) > 0 ";
        //else if (by == PersonBookGroupBy.ByDue)
        //    where1 += " AND DocumentAccountingItems.PersonnelID IS NOT NULL AND ISNULL(DocumentAccountingItems.Due, 0) > 0 ";
        if (projectID != Null.NullInteger)
        {
            where1 += " AND DocumentAccountingItems.ProjectID = " + projectID;
            where3 += " AND DocumentAccountingItems.ProjectID = " + projectID;
        }
        else if (by == PersonBookGroupBy.ByProject)
        {
            where1 += " AND DocumentAccountingItems.ProjectID IS NOT NULL ";
            where3 += " AND DocumentAccountingItems.ProjectID IS NOT NULL ";
        }
        if (!string.IsNullOrEmpty(filter))
        {
            where1 += "\r\nAND (\r\n" + filter + "\r\n)\r\n";
            qry3 += "\r\nAND (\r\n" + filter + "\r\n)\r\n";
        }

        qry += " WHERE " + where1;
        if (initRemain)
        {
            qry2 += " WHERE " + where3;
            qry += " UNION " + qry2;
        }

        if (grouped)
        {
            qry += @" GROUP BY Personnel.Name + ' ' + Personnel.Surname, Personnel.Code, PersonnelID, Personnel.HomeTelNum, Personnel.CellNumber1, Personnel.HomeAddress, PIdentify";
            if (!string.IsNullOrEmpty(sortCol))
            {
                if (((toRemain == 0 || toRemain == null) && by == PersonBookGroupBy.ByOwe) || ((fromRemain == 0 || fromRemain == null) && by == PersonBookGroupBy.ByDue))
                {
                    qry3 += " ORDER BY " + sortCol + " " + sortOrder;
                }
                else
                {
                    qry += " ORDER BY " + sortCol + " " + sortOrder;
                }
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(sortCol))
                qry += " ORDER BY " + sortCol + " " + sortOrder;
            else if (by == PersonBookGroupBy.ByPersonnel || by == PersonBookGroupBy.ByOwe || by == PersonBookGroupBy.ByDue || by == PersonBookGroupBy.ByOweDue)
                qry += " ORDER BY DocumentAccountingItems.PersonnelID, Date, CAST(DocumentAccountingView.No AS INT), DocumentAccountingItems.ID";
            else
                qry += " ORDER BY DocumentAccountingItems.ProjectID, Date, CAST(DocumentAccountingView.No AS INT), DocumentAccountingItems.ID";
        }

        //if (by == PersonBookGroupBy.ByOwe || by == PersonBookGroupBy.ByDue)
        //{
        //    if (((toRemain == 0 || toRemain == null) && by == PersonBookGroupBy.ByOwe) || ((fromRemain == 0 || fromRemain == null) && by == PersonBookGroupBy.ByDue))
        //    {
        //        qry += qry3;
        //    }
        //}
        DataTable dr = DbProxy.Instance.FillData(qry, "book", dbs, string.IsNullOrEmpty(sortCol) ? null : (sortCol + " " + sortOrder));
        return dr;
    }

    public static DataTable GetProjectBook(int totalID, DateTime startDate, DateTime endDate, int personID, int projectID
        , int programID, bool justProjectNeed, string filter, List<string> dbs, bool showAllVouchers)
    {
        string where1 = string.Format(@" (DocumentAccounting.WorkflowStatus = 1 OR {0} = 1) AND {1} <= Date AND Date <= {2} "
            , showAllVouchers ? 1 : 0, Helper.QoutedDateTime(startDate), Helper.QoutedDateTime(endDate));

        #region new solution
        if (justProjectNeed)
            where1 += " AND DocumentAccountingItems.TreePath IN (SELECT Trees.TreePath FROM Trees WHERE Trees.ProjectNeed=1) ";
        #endregion

        string qry = "";
        if (projectID == Null.NullInteger && filter.IndexOf("Ward") < 0)
        {
            qry = @"SELECT NULL ID, NULL ItemID, NULL OtherID, NULL Mode
                , NULL Date, NULL No, NULL TreePathName
                , NULL TopicCode
                , NULL Comments, SUM(ISNULL(DocumentAccountingItems.Owe, 0)) AS Owe, SUM(ISNULL(DocumentAccountingItems.Due, 0)) AS Due
                , NULL PersonnelName, Ward.Name AS WardName, NULL PersonnelID, ProjectID
                , NULL DateDD
                FROM DocumentAccounting 
                INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
                LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID 
                LEFT OUTER JOIN Ward ON DocumentAccountingItems.ProjectID=Ward.ID 
                LEFT OUTER JOIN EquipmentView ON DocumentAccountingItems.EquipmentID=EquipmentView.ID";
        }
        else
        {
            qry = @"SELECT DocumentAccounting.ID, DocumentAccountingItems.ID ItemID, DocumentAccounting.OtherID, DocumentAccounting.Mode
                , DocumentAccounting.Date, DocumentAccounting.No, dbo.TreePathToString(DocumentAccountingItems.TreePath, 1) AS TreePathName
                , dbo.TreePathToTopicCode(DocumentAccountingItems.TreePath) AS TopicCode
                , DocumentAccountingItems.Comments, ISNULL(DocumentAccountingItems.Owe, 0) AS Owe, ISNULL(DocumentAccountingItems.Due, 0) AS Due
                , Personnel.Name + ' ' + Personnel.Surname AS PersonnelName, Ward.Name AS WardName, PersonnelID, ProjectID
                , DATEADD(dd, 0, DATEDIFF(dd, 0, Date)) DateDD
                FROM DocumentAccounting 
                INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
                LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID 
                LEFT OUTER JOIN Ward ON DocumentAccountingItems.ProjectID=Ward.ID 
                LEFT OUTER JOIN EquipmentView ON DocumentAccountingItems.EquipmentID=EquipmentView.ID ";
        }

        // ------------------------------
        if (programID == -1) // default program = NULL (See SiteSettings.cs)
            where1 += " AND DocumentAccounting.ProgramID IS NULL";
        else if (programID == 0) // all programs
        {
        }
        else
            where1 += " AND  DocumentAccounting.ProgramID = " + programID;

        if (!string.IsNullOrEmpty(filter))
            where1 += "\r\nAND (\r\n" + filter + "\r\n)\r\n";

        if (projectID != Null.NullInteger)
            where1 += " AND DocumentAccountingItems.projectID = " + projectID;
        else
            where1 += " AND DocumentAccountingItems.projectID IS NOT NULL ";

        if (projectID == Null.NullInteger && filter.IndexOf("Ward") < 0)
        {
            where1 += " AND DocumentAccountingItems.ProjectID IS NOT NULL  Group BY DocumentAccountingItems.ProjectID, Ward.Name";
        }
        else
        {
            where1 += "  ORDER BY DocumentAccountingItems.ProjectID, DateDD, DocumentAccounting.No, ItemID";
        }

        qry += " WHERE " + where1;
       

        DataTable dr = DbProxy.Instance.FillData(qry, "pbook", dbs, "ProjectID, DateDD, No, ItemID");
        return dr;
    }
    public static DataTable GetEquipmentBook(int totalID, DateTime startDate, DateTime endDate, int personID, int equipmentID
        , int programID, bool justEquipmentNeed, string filter, List<string> dbs, bool showAllVouchers)
    {
        string where1 = string.Format(@" (DocumentAccounting.WorkflowStatus = 1 OR {0} = 1) AND {1} <= Date AND Date <= {2} "
            , showAllVouchers ? 1 : 0, Helper.QoutedDateTime(startDate), Helper.QoutedDateTime(endDate));

        #region new solution
        if (justEquipmentNeed)
            where1 += " AND DocumentAccountingItems.TreePath IN (SELECT Trees.TreePath FROM Trees WHERE Trees.EquipmentNeed=1) ";
        #endregion
        string qry = "";
        if (equipmentID == Null.NullInteger && filter.IndexOf("EquipmentView") < 0)
        {
            qry = @"SELECT NULL ID, NULL ItemID, NULL OtherID, NULL Mode
                , NULL Date, NULL No, NULL TreePathName
                , NULL TopicCode
                , NULL Comments, SUM(ISNULL(DocumentAccountingItems.Owe, 0)) AS Owe, SUM(ISNULL(DocumentAccountingItems.Due, 0)) AS Due
                , NULL PersonnelName, EquipmentView.FullName AS WardName, NULL PersonnelID, EquipmentID
                , NULL DateDD
                FROM DocumentAccounting 
                INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
                LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID 
                LEFT OUTER JOIN Ward ON DocumentAccountingItems.ProjectID=Ward.ID 
                LEFT OUTER JOIN EquipmentView ON DocumentAccountingItems.EquipmentID=EquipmentView.ID ";
        }
        else
        {
            qry = @"SELECT DocumentAccounting.ID, DocumentAccountingItems.ID ItemID, DocumentAccounting.OtherID, DocumentAccounting.Mode
                , DocumentAccounting.Date, DocumentAccounting.No, dbo.TreePathToString(DocumentAccountingItems.TreePath, 1) AS TreePathName
                , dbo.TreePathToTopicCode(DocumentAccountingItems.TreePath) AS TopicCode
                , DocumentAccountingItems.Comments, ISNULL(DocumentAccountingItems.Owe, 0) AS Owe, ISNULL(DocumentAccountingItems.Due, 0) AS Due
                , Personnel.Name + ' ' + Personnel.Surname AS PersonnelName, EquipmentView.FullName AS WardName, PersonnelID, EquipmentID
                , DATEADD(dd, 0, DATEDIFF(dd, 0, Date)) DateDD
                FROM DocumentAccounting 
                INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
                LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID 
                LEFT OUTER JOIN Ward ON DocumentAccountingItems.ProjectID=Ward.ID 
                LEFT OUTER JOIN EquipmentView ON DocumentAccountingItems.EquipmentID=EquipmentView.ID ";
        }

        // ------------------------------
        if (programID == -1) // default program = NULL (See SiteSettings.cs)
            where1 += " AND DocumentAccounting.ProgramID IS NULL";
        else if (programID == 0) // all programs
        {
        }
        else
            where1 += " AND  DocumentAccounting.ProgramID = " + programID;

        if (!string.IsNullOrEmpty(filter))
            where1 += "\r\nAND (\r\n" + filter + "\r\n)\r\n";


        if (equipmentID != Null.NullInteger)
            where1 += " AND DocumentAccountingItems.EquipmentID = " + equipmentID;
        else
            where1 += " AND DocumentAccountingItems.EquipmentID IS NOT NULL ";

        if (equipmentID == Null.NullInteger && filter.IndexOf("EquipmentView") < 0)
        {
            where1 += " AND DocumentAccountingItems.EquipmentID IS NOT NULL  Group BY DocumentAccountingItems.EquipmentID, EquipmentView.FullName";
        }
        else
        {
            where1 += "  ORDER BY DocumentAccountingItems.EquipmentID, DateDD, DocumentAccounting.No, ItemID";
        }
        qry += " WHERE " + where1;

        DataTable dr = DbProxy.Instance.FillData(qry, "ebook", dbs, "EquipmentID, DateDD, No, ItemID");
        return dr;
    }

    public static DataTable GetCostCenterBook(int totalID, DateTime startDate, DateTime endDate, int personID, int costCenterID
        , int programID, bool justCostCenterNeed, string filter, List<string> dbs, bool showAllVouchers)
    {
        string where1 = string.Format(@" (DocumentAccounting.WorkflowStatus = 1 OR {0} = 1) AND {1} <= Date AND Date <= {2} "
            , showAllVouchers ? 1 : 0, Helper.QoutedDateTime(startDate), Helper.QoutedDateTime(endDate));

        #region new solution
        if (justCostCenterNeed)
            where1 += " AND DocumentAccountingItems.TreePath IN (SELECT Trees.TreePath FROM Trees WHERE Trees.CostCenterNeed=1) ";
        #endregion
        string qry = "";
        if (costCenterID == Null.NullInteger && filter.IndexOf("CostCenterView") < 0)
        {
            qry = @"SELECT NULL ID, NULL ItemID, NULL OtherID, NULL Mode
                , NULL Date, NULL No, NULL TreePathName
                , NULL TopicCode
                , NULL Comments, SUM(ISNULL(DocumentAccountingItems.Owe, 0)) AS Owe, SUM(ISNULL(DocumentAccountingItems.Due, 0)) AS Due
                , NULL PersonnelName, CostCenterView.FullName AS WardName, NULL PersonnelID, DocumentAccountingItems.CostCenterID
                , NULL DateDD
                FROM DocumentAccounting 
                INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
                LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID 
                LEFT OUTER JOIN Ward ON DocumentAccountingItems.ProjectID=Ward.ID 
                LEFT OUTER JOIN CostCenterView ON DocumentAccountingItems.CostCenterID=CostCenterView.ID";
        }
        else
        {
            qry = @"SELECT DocumentAccounting.ID, DocumentAccountingItems.ID ItemID, DocumentAccounting.OtherID, DocumentAccounting.Mode
                , DocumentAccounting.Date, DocumentAccounting.No, dbo.TreePathToString(DocumentAccountingItems.TreePath, 1) AS TreePathName
                , dbo.TreePathToTopicCode(DocumentAccountingItems.TreePath) AS TopicCode
                , DocumentAccountingItems.Comments, ISNULL(DocumentAccountingItems.Owe, 0) AS Owe, ISNULL(DocumentAccountingItems.Due, 0) AS Due
                , Personnel.Name + ' ' + Personnel.Surname AS PersonnelName, CostCenterView.FullName AS WardName, PersonnelID, DocumentAccountingItems.CostCenterID
                , DATEADD(dd, 0, DATEDIFF(dd, 0, Date)) DateDD
                FROM DocumentAccounting 
                INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID
                LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID 
                LEFT OUTER JOIN Ward ON DocumentAccountingItems.ProjectID=Ward.ID 
                LEFT OUTER JOIN CostCenterView ON DocumentAccountingItems.CostCenterID=CostCenterView.ID";
        }
        // ------------------------------
        if (programID == -1) // default program = NULL (See SiteSettings.cs)
            where1 += " AND DocumentAccounting.ProgramID IS NULL";
        else if (programID == 0) // all programs
        {
        }
        else
            where1 += " AND  DocumentAccounting.ProgramID = " + programID;

        if (!string.IsNullOrEmpty(filter))
            where1 += "\r\nAND (\r\n" + filter + "\r\n)\r\n";


        if (costCenterID != Null.NullInteger)
            where1 += " AND DocumentAccountingItems.CostCenterID = " + costCenterID;
        else
            where1 += " AND DocumentAccountingItems.CostCenterID IS NOT NULL ";

        if (costCenterID == Null.NullInteger && filter.IndexOf("CostCenterView") < 0)
        {
            where1 += " AND DocumentAccountingItems.CostCenterID IS NOT NULL  Group BY DocumentAccountingItems.CostCenterID, CostCenterView.FullName";
        }
        else
        {
            where1 += "  ORDER BY DocumentAccountingItems.CostCenterID, DateDD, DocumentAccounting.No, ItemID";
        }
        qry += " WHERE " + where1;

        DataTable dr = DbProxy.Instance.FillData(qry, "ebook", dbs, "CostCenterID, DateDD, No, ItemID");
        return dr;
    }

    public static DataTable GetBook(string parentTreePath, DateTime startDate, DateTime endDate, int userID, int projectID, int personID, int programID, int equipmentID, int costCenterID, string[] treePathes
        , string fromVoucherNo, string toVoucherNo, bool notShowDetail, bool notShowDetailByPerson, bool justOwe, bool justDue, string comments)
    {
        return GetBook(parentTreePath, startDate, endDate, userID, projectID, personID, programID, equipmentID, costCenterID, treePathes
            , fromVoucherNo, toVoucherNo, null, null, false, notShowDetail, notShowDetailByPerson, justOwe, justDue, comments
            , -1, -1, -1);
    }
    public static DataTable GetBook(string parentTreePath, DateTime startDate, DateTime endDate, int userID, int projectID, int personID, int programID, int equipmentID, int costCenterID, string[] treePathes
        , string fromVoucherNo, string toVoucherNo, string sortCol, string sortOrder, bool showInitRemain, bool notShowDetail, bool notShowDetailByPerson, bool justOwe, bool justDue, string comments
        , decimal Amount, decimal POrM, int minLevel)
    {
        return GetBook(parentTreePath, startDate, endDate, userID, projectID, personID, programID, equipmentID, costCenterID, treePathes
        , fromVoucherNo, toVoucherNo, sortCol, sortOrder, showInitRemain, notShowDetail, notShowDetailByPerson, justOwe, justDue, comments
        , Amount, POrM, null, minLevel, false);
    }
    public static DataTable GetBook(string parentTreePath, DateTime startDate, DateTime endDate, int userID, int projectID, int personID, int programID, int equipmentID, int costCenterID, string[] treePathes
        , string fromVoucherNo, string toVoucherNo, string sortCol, string sortOrder, bool showInitRemain, bool notShowDetail, bool notShowDetailByPerson, bool justOwe, bool justDue, string comments
        , decimal Amount, decimal POrM, List<string> dbs, int minLevel, bool showAllVouchers)
    {
        string where = "", qry3 = "";
        string qry = string.Format(@"SELECT DocumentAccounting.ID, DocumentAccountingItems.ID ItemID, DocumentAccounting.Date, DocumentAccounting.No, DocumentAccounting.DoComments, DocumentAccountingItems.TreePath
                , [dbo].[TreePathToString2Level](DocumentAccountingItems.TreePath) AS TreePathName
                , dbo.TreePathToTopicCode(DocumentAccountingItems.TreePath) AS TopicCode
                , DocumentAccountingItems.Comments, ISNULL(DocumentAccountingItems.Owe, 0) AS Owe, ISNULL(DocumentAccountingItems.Due, 0) AS Due, DocumentAccountingItems.DocNo
                , Personnel.Name + ' ' + Personnel.Surname PersonnelName, DocumentAccounting.Mode, DocumentAccounting.OtherID
                , DATEADD(dd, 0, DATEDIFF(dd, 0, Date)) DAY1, CAST(DocumentAccounting.No AS INT) NO1
                , Equipment.Title EquipmentName, CostCenter.Name CostCenterName
                , Ward.Name WardName
                FROM DocumentAccounting 
                INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID 
                LEFT JOIN Personnel ON DocumentAccountingItems.PersonnelID = Personnel.ID
                LEFT JOIN Equipment ON DocumentAccountingItems.EquipmentID = Equipment.ID
                LEFT JOIN CostCenter ON DocumentAccountingItems.CostCenterID = CostCenter.ID
                LEFT JOIN Ward ON DocumentAccountingItems.ProjectID = Ward.ID
                WHERE {0} <= DocumentAccounting.Date AND DocumentAccounting.Date <= {1} "
               , Helper.QoutedDateTime(startDate), Helper.QoutedDateTime(endDate));
        if (!showAllVouchers)
            qry += " AND DocumentAccounting.WorkflowStatus = 1";
        if (notShowDetail)
        {
            qry3 = string.Format(@"
                DECLARE @cnt int = {2}
                 SELECT NULL ID, NULL ItemID, NULL  Date, NULL No, NULL DoComments, NULL TreePath
                , [dbo].[TreePathToStringParentAndNextLevel](DocumentAccountingItems.TreePath, @cnt) AS TreePathName
                , dbo.[TreePathToTopicCodeOfParentFirstChild](DocumentAccountingItems.TreePath, @cnt) AS TopicCode
                , NULL Comments, SUM(ISNULL(DocumentAccountingItems.Owe, 0)) Owe, SUM(ISNULL(DocumentAccountingItems.Due, 0)) Due, NULL DocNo
                , NULL PersonnelName, NULL Mode, NULL OtherID
                , NULL DAY1, NULL NO1
                , NULL EquipmentName, NULL CostCenterName
                , NULL WardName
                FROM DocumentAccounting 
                INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID 
                LEFT JOIN Personnel ON DocumentAccountingItems.PersonnelID = Personnel.ID
                LEFT JOIN Equipment ON DocumentAccountingItems.EquipmentID = Equipment.ID
                LEFT JOIN CostCenter ON DocumentAccountingItems.CostCenterID = CostCenter.ID
                LEFT JOIN Ward ON DocumentAccountingItems.ProjectID = Ward.ID
                WHERE {0} <= DocumentAccounting.Date AND DocumentAccounting.Date <= {1} "
              , Helper.QoutedDateTime(startDate), Helper.QoutedDateTime(endDate), minLevel);
            if (!showAllVouchers)
                qry3 += " AND DocumentAccounting.WorkflowStatus = 1";

            qry = string.Format(@"SELECT NULL ID, NULL ItemID, NULL  Date, NULL No, NULL DoComments, DocumentAccountingItems.TreePath
                , [dbo].[TreePathToString2Level](DocumentAccountingItems.TreePath) AS TreePathName
                , dbo.TreePathToTopicCode(DocumentAccountingItems.TreePath) AS TopicCode
                , NULL Comments, SUM(ISNULL(DocumentAccountingItems.Owe, 0)) Owe, SUM(ISNULL(DocumentAccountingItems.Due, 0)) Due, NULL DocNo
                , NULL PersonnelName, NULL Mode, NULL OtherID
                , NULL DAY1, NULL NO1
                , NULL EquipmentName, NULL CostCenterName
                , NULL WardName
                FROM DocumentAccounting 
                INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID 
                LEFT JOIN Personnel ON DocumentAccountingItems.PersonnelID = Personnel.ID
                LEFT JOIN Equipment ON DocumentAccountingItems.EquipmentID = Equipment.ID
                LEFT JOIN CostCenter ON DocumentAccountingItems.CostCenterID = CostCenter.ID
                LEFT JOIN Ward ON DocumentAccountingItems.ProjectID = Ward.ID
                WHERE {0} <= DocumentAccounting.Date AND DocumentAccounting.Date <= {1} "
               , Helper.QoutedDateTime(startDate), Helper.QoutedDateTime(endDate));
            if (!showAllVouchers)
                qry += " AND DocumentAccounting.WorkflowStatus = 1";
        }
        if (notShowDetailByPerson)
        {
            qry = string.Format(@"SELECT NULL ID, NULL ItemID, NULL  Date, NULL No, NULL DoComments, DocumentAccountingItems.TreePath, [dbo].[TreePathToString2Level](DocumentAccountingItems.TreePath) AS TreePathName
                , dbo.TreePathToTopicCode(DocumentAccountingItems.TreePath) AS TopicCode
                , NULL Comments, SUM(ISNULL(DocumentAccountingItems.Owe, 0)) Owe, SUM(ISNULL(DocumentAccountingItems.Due, 0)) Due, NULL DocNo
                , Personnel.Name + ' ' + Personnel.Surname PersonnelName, NULL Mode, NULL OtherID
                , NULL DAY1, NULL NO1
                , NULL EquipmentName, NULL CostCenterName
                , NULL WardName
                FROM DocumentAccounting 
                INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID 
                LEFT JOIN Personnel ON DocumentAccountingItems.PersonnelID = Personnel.ID
                LEFT JOIN Equipment ON DocumentAccountingItems.EquipmentID = Equipment.ID
                LEFT JOIN CostCenter ON DocumentAccountingItems.CostCenterID = CostCenter.ID
                LEFT JOIN Ward ON DocumentAccountingItems.ProjectID = Ward.ID
                WHERE {0} <= DocumentAccounting.Date AND DocumentAccounting.Date <= {1} "
               , Helper.QoutedDateTime(startDate), Helper.QoutedDateTime(endDate));
            if (!showAllVouchers)
                qry += " AND DocumentAccounting.WorkflowStatus = 1";
        }

        if (userID != Null.NullInteger)
            where += " AND DocumentAccounting.UserID = " + userID;
        if (projectID != Null.NullInteger)
            where += " AND ProjectID IN (SELECT ID FROM [dbo].[GetAllWardChilds] (" + projectID + "))";
        if (equipmentID != Null.NullInteger)
            where += " AND Equipment.ID IN (SELECT ID FROM [dbo].[GetAllEquipmentChilds](" + equipmentID + "))";
        if (costCenterID != Null.NullInteger)
            where += " AND CostCenter.ID IN (SELECT ID FROM [dbo].[GetAllCostCenterChilds](" + costCenterID + "))";
        if (!string.IsNullOrEmpty(parentTreePath))
            where += " AND (DocumentAccountingItems.TreePath = '" + parentTreePath + "' OR DocumentAccountingItems.TreePath LIKE '" + parentTreePath + "-%') ";
        if (personID != Null.NullInteger)
        {
            where += " AND DocumentAccountingItems.PersonnelID=" + personID;
            if (treePathes == null || treePathes.Length <= 0)
            {
                where += " AND DocumentAccountingItems.TreePath IN (SELECT TreePath FROM Trees WHERE PersonNeed = 1)";
            }
        }
       
        // ------------------------------
        if (programID == -1) // default program = NULL (See SiteSettings.cs)
            where += " AND DocumentAccounting.ProgramID IS NULL";
        else if (programID == 0) // all programs
        {
        }
        else
            where += " AND  DocumentAccounting.ProgramID = " + programID;
        if (!string.IsNullOrEmpty(fromVoucherNo))
        {
            int toInt = Helper.GetInt(fromVoucherNo, Null.NullInteger);
            if (toInt != Null.NullInteger)
                where += " AND  CAST(DocumentAccounting.No AS INT) >= " + toInt;
            else
                where += " AND  DocumentAccounting.No >= '" + fromVoucherNo + "'";
        }
        if (!string.IsNullOrEmpty(toVoucherNo))
        {
            int toInt = Helper.GetInt(toVoucherNo, Null.NullInteger);
            if (toInt != Null.NullInteger)
                where += " AND  CAST(DocumentAccounting.No AS INT) <= " + toInt;
            else
                where += " AND  DocumentAccounting.No <= '" + toVoucherNo + "'";
        }
        if (justOwe)
        {
            where += " AND  ISNULL(DocumentAccountingItems.Due, 0) <= 0 ";
        }
        if (justDue)
        {
            where += " AND  ISNULL(DocumentAccountingItems.Owe, 0) <= 0 ";
        }
        if (!string.IsNullOrEmpty(comments))
        {
            where += " AND  DocumentAccountingItems.Comments LIKE N'%" + comments + "%' ";
        }
        if (Amount > 0 && POrM > 0)
        {
            where += " AND  (ISNULL(DocumentAccountingItems.Due, 0) BETWEEN " + (Amount - POrM) + " AND " + (Amount + POrM)
                + " OR ISNULL(DocumentAccountingItems.Owe, 0) BETWEEN " + (Amount - POrM) + " AND " + (Amount + POrM) + ")";
        }
        else if (Amount >= 0)
        {
            where += " AND  (ISNULL(DocumentAccountingItems.Due, 0)=" + Amount + " OR ISNULL(DocumentAccountingItems.Owe, 0) =" + Amount + ")";
        }


        #region old solution
        if (treePathes != null && treePathes.Length > 0)
        {
            if (treePathes.Length == 1)
            {
                where += " AND(DocumentAccountingItems.TreePath = " + Helper.QoutedString(treePathes[0]) + " OR DocumentAccountingItems.TreePath LIKE '" + treePathes[0] + "-%')";
            }
            else
            {
                where += " AND (";
                bool first = false;
                for (int i = 0; i < treePathes.Length; i++)
                {
                    if (string.IsNullOrEmpty(treePathes[i]))
                        continue;
                    if (first)
                        where += " OR ";
                    first = true;
                    where += "(DocumentAccountingItems.TreePath = " + Helper.QoutedString(treePathes[i]) + " OR DocumentAccountingItems.TreePath LIKE '" + treePathes[i] + "-%')";
                }
                where += ")";
            }
        }
        #endregion

        qry += where;


        if (showInitRemain)
        {
            string qry2 = string.Format(@"SELECT 0 ID, 0 ItemID, NULL Date, '0' NO, '' DoComments , '' TreePath, '' TreePathName
, '' TopicCode, '' Comments, SUM(ISNULL(Owe, 0)) AS Owe, SUM(ISNULL(Due, 0)) AS Due, '' DocNo, '' PersonnelName, 0 Mode, 0 OtherID, 0 DAY1, 0 NO1, '' EquipmentName, '' CostCenterName, '' WardName
FROM DocumentAccounting 
INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID 
LEFT JOIN Personnel ON DocumentAccountingItems.PersonnelID = Personnel.ID
WHERE DocumentAccounting.Date < {0} "
            , Helper.QoutedDateTime(startDate));
            if (!showAllVouchers)
                qry2 += " AND DocumentAccounting.WorkflowStatus = 1";
            if (userID != Null.NullInteger)
                qry2 += " AND DocumentAccounting.UserID = " + userID;
            //if (projectID != Null.NullInteger)
            //    qry2 += " AND ProjectID=" + projectID;
            if (projectID != Null.NullInteger)
                qry2 += " AND ProjectID IN (SELECT ID FROM [dbo].[GetAllWardChilds] (" + projectID + "))";
            if (equipmentID != Null.NullInteger)
                qry2 += " AND Equipment.ID IN (SELECT ID FROM [dbo].[GetAllEquipmentChilds](" + equipmentID + "))";
            if (costCenterID != Null.NullInteger)
                qry2 += " AND CostCenter.ID IN (SELECT ID FROM [dbo].[GetAllCostCenterChilds](" + costCenterID + "))";
            if (!string.IsNullOrEmpty(parentTreePath))
                qry2 += " AND (DocumentAccountingItems.TreePath = '" + parentTreePath + "' OR DocumentAccountingItems.TreePath LIKE '" + parentTreePath + "-%') ";

            if (treePathes != null && treePathes.Length > 0)
            {
                if (treePathes.Length == 1)
                {
                    qry2 += " AND (DocumentAccountingItems.TreePath = " + Helper.QoutedString(treePathes[0]) + " OR DocumentAccountingItems.TreePath LIKE '" + treePathes[0] + "-%')";
                }
                else
                {
                    qry2 += " AND (";
                    bool first = false;
                    for (int i = 0; i < treePathes.Length; i++)
                    {
                        if (string.IsNullOrEmpty(treePathes[i]))
                            continue;
                        if (first)
                            qry2 += " OR ";
                        first = true;
                        qry2 += "(DocumentAccountingItems.TreePath = " + Helper.QoutedString(treePathes[i]) + " OR DocumentAccountingItems.TreePath LIKE '" + treePathes[i] + "-%')";
                    }
                    qry2 += ")";
                }
            }
            if (justOwe)
            {
                qry2 += " AND  ISNULL(DocumentAccountingItems.Due, 0) <= 0 ";
            }
            if (justDue)
            {
                qry2 += " AND  ISNULL(DocumentAccountingItems.Owe, 0) <= 0 ";
            }
            if (personID != Null.NullInteger)
                qry2 += " AND DocumentAccountingItems.PersonnelID=" + personID;
            // ------------------------------
            if (programID == -1) // default program = NULL (See SiteSettings.cs)
                qry2 += " AND DocumentAccounting.ProgramID IS NULL";
            else if (programID == 0) // all programs
            {
            }
            else
                qry2 += " AND  DocumentAccounting.ProgramID = " + programID;
            if (!string.IsNullOrEmpty(fromVoucherNo))
            {
                int toInt = Helper.GetInt(fromVoucherNo, Null.NullInteger);
                if (toInt != Null.NullInteger)
                    qry2 += " AND  CAST(DocumentAccounting.No AS INT) >= " + toInt;
                else
                    qry2 += " AND  DocumentAccounting.No >= '" + fromVoucherNo + "'";
            }
            if (!string.IsNullOrEmpty(toVoucherNo))
            {
                int toInt = Helper.GetInt(toVoucherNo, Null.NullInteger);
                if (toInt != Null.NullInteger)
                    qry2 += " AND  CAST(DocumentAccounting.No AS INT) <= " + toInt;
                else
                    qry2 += " AND  DocumentAccounting.No <= '" + toVoucherNo + "'";
            }
            qry = qry + " UNION " + qry2;
        }
        if (!notShowDetail && !notShowDetailByPerson)
        {
            qry += " ORDER BY DocumentAccountingItems.TreePath, DAY1, NO1, ID";
        }
        else
        {
            if (notShowDetailByPerson)
            {
                qry += " GROUP by DocumentAccountingItems.TreePath, Personnel.Name + ' ' + Personnel.Surname ";
                qry += " ORDER BY DocumentAccountingItems.TreePath";
            }
            if (notShowDetail)
            {
                qry3 += where;
                qry3 += " GROUP by [dbo].[TreePathToStringParentAndNextLevel](DocumentAccountingItems.TreePath, @cnt), dbo.[TreePathToTopicCodeOfParentFirstChild](DocumentAccountingItems.TreePath, @cnt)";
                //qry += " GROUP by DocumentAccountingItems.TreePath ";
                qry = qry3; //+ " UNION " + qry;

            }
        }
        if (!string.IsNullOrEmpty(sortCol))
        {
            if (qry.Contains(" ORDER BY "))
            {
                qry += ", " + sortCol + " " + sortOrder;
            }
            else
            {
                qry += " ORDER BY " + sortCol + " " + sortOrder;
            }
        }
        DataTable dt = DbProxy.Instance.FillData(qry, "book", dbs, string.IsNullOrEmpty(sortCol) ? null : (sortCol + " " + sortOrder));
        return dt;
    }
    public static IDataReader GetTopicBalance(DateTime startDate, DateTime endDate, List<int> projectIDs, int personID, int programID, List<int> equipmentIDs
        , string[] treePathes, string fromVoucherNo, string toVoucherNo, bool justOwe, bool justDue, string comments)
    {
        string qry = string.Format(@"SELECT DocumentAccountingItems.TreePath
                , dbo.TreePathToTopicTitle(DocumentAccountingItems.TreePath) AS TreePathName
                , dbo.TreePathToTopicCode(DocumentAccountingItems.TreePath) AS TopicCode
                , ISNULL(DocumentAccountingItems.Owe,  0) AS Owe, ISNULL(DocumentAccountingItems.Due, 0) AS Due
                , Equipment.ID, Equipment.Title EquipmentName
                , Ward.ID, Ward.Name WardName
                FROM DocumentAccounting 
                INNER JOIN DocumentAccountingItems ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID 
                LEFT JOIN Personnel ON DocumentAccountingItems.PersonnelID = Personnel.ID
                LEFT JOIN Equipment ON DocumentAccountingItems.EquipmentID = Equipment.ID
                LEFT JOIN Ward ON DocumentAccountingItems.ProjectID = Ward.ID
                WHERE DocumentAccounting.WorkflowStatus = 1 AND {0} <= DocumentAccounting.Date AND DocumentAccounting.Date <= {1} "
               , Helper.QoutedDateTime(startDate), Helper.QoutedDateTime(endDate));


        if (projectIDs != null && projectIDs.Count > 0)
            qry += " AND ProjectID IN (" + ",".Join(projectIDs) + ")";
        if (equipmentIDs != null && equipmentIDs.Count > 0)
            qry += " AND Equipment.ID IN (" + ",".Join(equipmentIDs) + ")";
        if (personID != Null.NullInteger)
            qry += " AND DocumentAccountingItems.PersonnelID=" + personID;

        // ------------------------------
        if (programID == -1) // default program = NULL (See SiteSettings.cs)
            qry += " AND DocumentAccounting.ProgramID IS NULL";
        else if (programID == 0) // all programs
        {
        }
        else
            qry += " AND  DocumentAccounting.ProgramID = " + programID;
        if (!string.IsNullOrEmpty(fromVoucherNo))
        {
            int toInt = Helper.GetInt(fromVoucherNo, Null.NullInteger);
            if (toInt != Null.NullInteger)
                qry += " AND  CAST(DocumentAccounting.No AS INT) >= " + toInt;
            else
                qry += " AND  DocumentAccounting.No >= '" + fromVoucherNo + "'";
        }
        if (!string.IsNullOrEmpty(toVoucherNo))
        {
            int toInt = Helper.GetInt(toVoucherNo, Null.NullInteger);
            if (toInt != Null.NullInteger)
                qry += " AND  CAST(DocumentAccounting.No AS INT) <= " + toInt;
            else
                qry += " AND  DocumentAccounting.No <= '" + toVoucherNo + "'";
        }
        if (justOwe)
        {
            qry += " AND  ISNULL(DocumentAccountingItems.Due, 0) <= 0 ";
        }
        if (justDue)
        {
            qry += " AND  ISNULL(DocumentAccountingItems.Owe, 0) <= 0 ";
        }
        if (!string.IsNullOrEmpty(comments))
        {
            qry += " AND  DocumentAccountingItems.Comments LIKE '%" + comments + "%' ";
        }

        #region treePathes
        if (treePathes != null && treePathes.Length > 0)
        {
            if (treePathes.Length == 1)
            {
                qry += " AND(DocumentAccountingItems.TreePath = " + Helper.QoutedString(treePathes[0]) + " OR DocumentAccountingItems.TreePath LIKE '" + treePathes[0] + "-%')";
            }
            else
            {
                qry += " AND (";
                bool first = false;
                for (int i = 0; i < treePathes.Length; i++)
                {
                    if (string.IsNullOrEmpty(treePathes[i]))
                        continue;
                    if (first)
                        qry += " OR ";
                    first = true;
                    qry += "(DocumentAccountingItems.TreePath = " + Helper.QoutedString(treePathes[i]) + " OR DocumentAccountingItems.TreePath LIKE '" + treePathes[i] + "-%')";
                }
                qry += ")";
            }
        }
        #endregion
      
        IDataReader dr = DbProxy.Instance.ExecuteReader(qry);
        return dr;
    }
    public static IDataReader GetExpBalance(string treePath, DateTime startDate, DateTime endDate, int projectID, int programID
        , int mode, int equipmentID, int personnelID, string fromVoucherNo, string toVoucherNo, bool onlyTop = false, bool simple = false)
    {
        string where = string.Format(@" DA.WorkflowStatus = 1 AND {0} <= DA.Date AND DA.Date <= {1} "
            , Helper.QoutedDateTime(startDate), Helper.QoutedDateTime(endDate));
        if (projectID != Null.NullInteger)
            where += " AND DAI.ProjectID=" + projectID;
        if (equipmentID != Null.NullInteger)
            where += " AND DAI.EquipmentID=" + equipmentID;
        if (personnelID != Null.NullInteger)
            where += " AND DAI.PersonnelID=" + personnelID;
        if (!string.IsNullOrEmpty(fromVoucherNo))
        {
            where += " AND DA.No >= " + fromVoucherNo;
        }
        if (!string.IsNullOrEmpty(toVoucherNo))
        {
            where += " AND DA.No <= " + toVoucherNo;
        }
        if (!string.IsNullOrEmpty(treePath))
        {
            //if (simple)
            //{
                where += " AND DAI.TreePath LIKE " + Helper.QoutedString(treePath + "%");
            //}
            //else
            //{
            //    where += " AND DAI.TreePath=" + Helper.QoutedString(treePath);
            //}
        }

        if (mode != Null.NullInteger)
            where += " AND DA.Mode=" + mode;

        // ------------------------------
        if (programID == -1) // default program = NULL (See SiteSettings.cs)
            where += " AND DA.ProgramID IS NULL";
        else if (programID == 0) // all programs
        {
        }
        else
            where += " AND  DA.ProgramID = " + programID;

        if (onlyTop)
            // for each root Treepath: sum of all its subtrees
            where += " AND ExpTempTable.TreePath = SUBSTRING(DAI.TreePath, 1, PathLen)";
        else
            // for each Treepath: only its items
            where += " AND ExpTempTable.TreePath = DAI.TreePath";

        string where2 = "";
        if (programID == -1) // default program = NULL (See SiteSettings.cs)
            where2 += " AND Topic.ProgramID IS NULL";
        else if (programID == 0) // all programs
        {
        }
        else
            where2 += " AND  Topic.ProgramID = " + programID;
        if (!string.IsNullOrEmpty(treePath))
        {
            //if (simple)
            //{
                where2 += " AND Trees.TreePath LIKE " + Helper.QoutedString(treePath + "%");
            //}
            //else
            //{
            //    where2 += " AND Trees.TreePath=" + Helper.QoutedString(treePath);
            //}
        }
        string where3 = "";
        if (simple)
        {
            if (!string.IsNullOrEmpty(treePath))
            {
                where3 = " WHERE ExpTempTable.TreePath NOT LIKE " + Helper.QoutedString(treePath + "-%-%") + " AND ExpTempTable.TreePath != " + Helper.QoutedString(treePath);
            }
            else
            {
                where3 = " WHERE CHARINDEX('-',  ExpTempTable.TreePath) = 0";
            }
        }

        string qry = string.Format(@"
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExpTempTable]') AND type in (N'U'))
	DROP TABLE ExpTempTable
Create Table ExpTempTable (TreePath nvarchar(900), TreePathName nvarchar(2000), TreePathCodes nvarchar(2000), PathLen int)

INSERT INTO ExpTempTable (TreePath, TreePathName, TreePathCodes, PathLen)
SELECT TreePath, dbo.TreePathToTopicTitle(TreePath), dbo.TreePathToTopicCode(TreePath), LEN(Trees.TreePath)
FROM Trees INNER JOIN Topic ON [dbo].[TreePathToProgramID](TreePath) = Topic.ID
WHERE 1 = 1 {0}

SELECT TreePath, TreePathName
, SumOwe=(SELECT SUM(ISNULL(Owe, 0)) FROM dbo.DocumentAccountingItems DAI INNER JOIN DocumentAccounting DA ON DA.ID = DAI.DocumentAccountingID 
       WHERE {1} )
, SumDue=(SELECT SUM(ISNULL(Due, 0)) FROM dbo.DocumentAccountingItems DAI INNER JOIN DocumentAccounting DA ON DA.ID = DAI.DocumentAccountingID 
       WHERE {1})
, dbo.TreePathToTopicCode(TreePath) AS TreePathCodes
FROM ExpTempTable {2}"
                , where2, where, where3);

        //if (onlyTop)
        //    qry += " AND NOT TreePath LIKE '%-%' ";
        qry += " ORDER BY TreePathCodes";
        IDataReader dr = DbProxy.Instance.ExecuteReader(qry, 60 * 5); // 5 minutes
        return dr;
    }
    public static IDataReader GetExpBalanceBaseOn(string treePath, DateTime startDate, DateTime endDate, int projectID, int programID
            , int equipmentID, int personnelID, string fromVoucherNo, string toVoucherNo, bool gBPerson = true, bool gBProject = false, bool gBEquipment = false)
    {
        string select = "";
        string groupBy = "";
        if (gBPerson)
        {
            select = " Personnel.Code, Personnel.Name + ' ' + Personnel.Surname AS Name ";
            groupBy = " GROUP BY Personnel.Code, Personnel.Name, Personnel.Surname ";
        }
        else if (gBProject)
        {
            select = " Ward.Code, Ward.Name ";
            groupBy = " GROUP BY Ward.Code, Ward.Name ";
        }
        else
        {
            select = " Equipment.Code, Equipment.Title AS Name ";
            groupBy = " GROUP BY Equipment.Code, Equipment.Title ";
        }
        string where = string.Format(@"WHERE DocumentAccounting.WorkflowStatus = 1 AND {0} <= DocumentAccounting.Date AND DocumentAccounting.Date <= {1} "
            , Helper.QoutedDateTime(startDate), Helper.QoutedDateTime(endDate));
        if (projectID != Null.NullInteger)
            where += " AND DocumentAccountingItems.ProjectID=" + projectID;
        if (equipmentID != Null.NullInteger)
            where += " AND DocumentAccountingItems.EquipmentID=" + equipmentID;
        if (personnelID != Null.NullInteger)
            where += " AND DocumentAccountingItems.PersonnelID=" + personnelID;
        if (!string.IsNullOrEmpty(fromVoucherNo))
        {
            where += " AND DocumentAccounting.No >= " + fromVoucherNo;
        }
        if (!string.IsNullOrEmpty(toVoucherNo))
        {
            where += " AND DocumentAccounting.No <= " + toVoucherNo;
        }
        if (!string.IsNullOrEmpty(treePath))
        {
            where += " AND DocumentAccountingItems.TreePath LIKE " + Helper.QoutedString(treePath + "%");
        }
        string qry = string.Format(@"SELECT {0}
                , SUM(ISNULL(Owe, 0)) AS Owe, SUM(ISNULL(Due, 0)) AS Due from DocumentAccountingItems
                LEFT OUTER JOIN DocumentAccounting ON DocumentAccountingItems.DocumentAccountingID = DocumentAccounting.ID
                LEFT OUTER JOIN Personnel ON DocumentAccountingItems.PersonnelID = Personnel.ID
                LEFT OUTER JOIN Ward ON DocumentAccountingItems.ProjectID = Ward.ID
                LEFT OUTER JOIN Equipment ON DocumentAccountingItems.EquipmentID = Equipment.ID {1}{2}"
                , select, where, groupBy);

        IDataReader dr = DbProxy.Instance.ExecuteReader(qry, 60 * 5); // 5 minutes
        return dr;
    }

//    public static IDataReader GetExpBalance_OLD(string treePath, DateTime startDate, DateTime endDate, int projectID, int programID, int mode, bool onlyTop = false)
//    {
//        string where = string.Format(@" {0} <= DocumentAccounting.Date AND DocumentAccounting.Date <= {1} "
//            , Helper.QoutedDateTime(startDate), Helper.QoutedDateTime(endDate));
//        if (projectID != Null.NullInteger)
//            where += " AND DocumentAccountingItems.ProjectID=" + projectID;
//        if (!string.IsNullOrEmpty(treePath))
//            where += " AND DocumentAccountingItems.TreePath=" + Helper.QoutedString(treePath);

//        if (mode != Null.NullInteger)
//            where += " AND DocumentAccounting.Mode=" + mode;

//        // ------------------------------
//        if (programID == -1) // default program = NULL (See SiteSettings.cs)
//            where += " AND DocumentAccounting.ProgramID IS NULL";
//        else if (programID == 0) // all programs
//        {
//        }
//        else
//            where += " AND  DocumentAccounting.ProgramID = " + programID;

//        if (onlyTop)
//            // for each root Treepath: sum of all its subtrees
//            where += " AND (DocumentAccountingItems.TreePath + '-') LIKE (Trees.TreePath + '-%')";
//        else
//            // for each Treepath: only its items
//            where += " AND DocumentAccountingItems.TreePath = Trees.TreePath";

//        string qry = string.Format(@"SELECT TreePath, dbo.TreePathToString(TreePath, 1) AS TreePathName
//, SumOwe=(SELECT SUM(Owe) FROM dbo.DocumentAccountingItems INNER JOIN DocumentAccounting ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID 
//       WHERE {0} )
//, SumDue=(SELECT SUM(Due) FROM dbo.DocumentAccountingItems INNER JOIN DocumentAccounting ON DocumentAccounting.ID=DocumentAccountingItems.DocumentAccountingID 
//       WHERE {0} AND (DocumentAccountingItems.TreePath + '-') LIKE (Trees.TreePath + '-%'))
//, dbo.TreePathToTopicCode(TreePath) AS TreePathCodes
//FROM Trees
//INNER JOIN Topic ON [dbo].[TreePathToProgramID](TreePath) = Topic.ID
//WHERE 1=1"
//                , where);
//        if (programID == -1) // default program = NULL (See SiteSettings.cs)
//            qry += " AND Topic.ProgramID IS NULL";
//        else if (programID == 0) // all programs
//        {
//        }
//        else
//            qry += " AND  Topic.ProgramID = " + programID;
//        if (!string.IsNullOrEmpty(treePath))
//            qry += " AND TreePath=" + Helper.QoutedString(treePath);
//        //if (onlyTop)
//        //    qry += " AND NOT TreePath LIKE '%-%' ";
//        qry += " ORDER BY TreePathCodes";
//        IDataReader dr = DbProxy.Instance.ExecuteReader(qry, 60 * 5); // 5 minutes
//        return dr;
//    }
    public static IDataReader GetVouchersReport(string title, string reciever, string documentNo
        , string programIDs, string totalIDs, string moeinIDs
        , string elab1IDs, string elab2IDs, string elab3IDs, string elab4IDs
        , DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int projectID
        , decimal oweStart, decimal oweEnd, decimal dueStart, decimal dueEnd)
    {
        IDataReader dr = DbProxy.Instance.ExecuteReader("GetVouchersReport", title, reciever, documentNo
            , programIDs, totalIDs, moeinIDs, elab1IDs, elab2IDs, elab3IDs, elab4IDs
            , startDate, endDate, fromVoucherID, toVoucherID, projectID
            , oweStart, oweEnd, dueStart, dueEnd);
        return dr;
    }
    //public static IDataReader GetFineList(int depth
    //    , string programIDs, string totalIDs, string moeinIDs
    //    , string elab1IDs, string elab2IDs, string elab3IDs, string elab4IDs
    //    , DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int projectID, bool filterZero)
    //{
    //    IDataReader dr = DbProxy.Instance.ExecuteReader("GetFineListSP", depth
    //        , programIDs, totalIDs, moeinIDs, elab1IDs, elab2IDs, elab3IDs, elab4IDs
    //        , startDate, endDate, fromVoucherID, toVoucherID, projectID, filterZero ? 1 : 0);
    //    return dr;
    //}
    //public static IDataReader GetFineList2(int depth, int programID, DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int projectID, bool filterZero)
    //{
    //    string qry;
    //    if (filterZero)
    //        qry = string.Format("SELECT *, [dbo].[Subtract](TopicID, Owe, Due) AS Remaining  FROM [dbo].[GetFineList2](@Depth, @ProgramID, @StartDate, @EndDate, @FromVoucherID, @ToVoucherID, @ProjectID) WHERE Owe IS NULL OR Owe<>Due"
    //             , depth, programID, Helper.QoutedDate(startDate), Helper.QoutedDate(endDate), fromVoucherID, toVoucherID, projectID);
    //    else
    //        qry = string.Format("SELECT *, [dbo].[Subtract](TopicID, Owe, Due) AS Remaining FROM [dbo].[GetFineList2](@Depth, @ProgramID, @StartDate, @EndDate, @FromVoucherID, @ToVoucherID, @ProjectID)"
    //            , depth, programID, Helper.QoutedDate(startDate), Helper.QoutedDate(endDate), fromVoucherID, toVoucherID, projectID);
    //    IDataReader dr = DbProxy.Instance.ExecuteReader(qry);
    //    return dr;
    //}
    public static IDataReader GetFineVouchers(int depth, int programID, int accountID, DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int projectID)
    {
        IDataReader dr = DbProxy.Instance.ExecuteReader("GetFineVouchers", depth, programID, accountID, startDate, endDate, fromVoucherID, toVoucherID, projectID);
        return dr;
    }
    public static IDataReader GetFineVouchersComplete(int depth, int programID, int accountID, DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int projectID)
    {
        IDataReader dr = DbProxy.Instance.ExecuteReader("GetFineVouchersComplete", depth, programID, accountID, startDate, endDate, fromVoucherID, toVoucherID, projectID);
        return dr;
    }
    public static IDataReader GetFineComplex(int depth, string totalIDs, DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int projectID)
    {
        string qry = string.Format("SELECT * FROM [dbo].[GetFineComplex]({0}, {1}, {2}, {3}, {4}, {5}, {6})"
            , depth, Helper.QoutedString(totalIDs), Helper.QoutedDate(startDate), Helper.QoutedDate(endDate), fromVoucherID, toVoucherID, projectID);
        IDataReader dr = DbProxy.Instance.ExecuteReader(qry);
        return dr;
    }
    //public static IDataReader GetFinancesReport(int programID, int moeinID, DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int projectID)
    //{
    //    string qry = string.Format("SELECT * FROM [dbo].[GetFinancesReport]({0}, {1}, {2}, {3}, {4}, {5}, {6})"
    //        , programID, moeinID, Helper.QoutedDate(startDate), Helper.QoutedDate(endDate), fromVoucherID, toVoucherID, projectID);
    //    IDataReader dr = DbProxy.Instance.ExecuteReader(qry);
    //    return dr;
    //}
    //public static IDataReader GetFinancesReport2(int totalID, int moeinID, DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int projectID)
    //{
    //    string qry = string.Format("SELECT * FROM [dbo].[GetFinancesReport2]({0}, {1}, {2}, {3}, {4}, {5}, {6})"
    //       , totalID, moeinID, Helper.QoutedDate(startDate), Helper.QoutedDate(endDate), fromVoucherID, toVoucherID, projectID);
    //    IDataReader dr = DbProxy.Instance.ExecuteReader(qry);
    //    return dr;
    //}
    //public static bool GetAccountAmounts(int accountID, DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int projectID, out decimal owe, out decimal due, out decimal remaining)
    //{
    //    owe = due = remaining = 0;
    //    IDataReader dr = DbProxy.Instance.ExecuteReader("GetAccountAmounts", accountID, startDate, endDate, fromVoucherID, toVoucherID, projectID);
    //    if (dr == null)
    //        return false;
    //    bool ret;
    //    if (ret = dr.Read())
    //    {
    //        owe = dr.GetDecimal(0);
    //        due = dr.GetDecimal(0);
    //        remaining = dr.GetDecimal(0);
    //    }
    //    dr.Close();
    //    dr.Dispose();
    //    return ret;
    //}
    //public static decimal GetAccountAmount(int accountID, DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int projectID)
    //{
    //    string qry = string.Format("SELECT [dbo].[GetAccountAmount]({0}, {1}, {2}, {3}, {4}, {5})"
    //        , accountID, Helper.QoutedDate(startDate), Helper.QoutedDate(endDate), fromVoucherID, toVoucherID, projectID);
    //    object val = DbProxy.Instance.FillScalar(qry);
    //    return Helper.GetDecimal(val, 0);
    //}
    //public static decimal GetAccountAmountOwe(int accountID, DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int projectID)
    //{
    //    string qry = string.Format("SELECT [dbo].[GetAccountAmountOwe]({0}, {1}, {2}, {3}, {4}, {5})"
    //        , accountID, Helper.QoutedDate(startDate), Helper.QoutedDate(endDate), fromVoucherID, toVoucherID, projectID);
    //    object val = DbProxy.Instance.FillScalar(qry);
    //    return Helper.GetDecimal(val, 0);
    //}
    //public static decimal GetAccountAmountDue(int accountID, DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int projectID)
    //{
    //    string qry = string.Format("SELECT [dbo].[GetAccountAmountDue]({0}, {1}, {2}, {3}, {4}, {5})"
    //        , accountID, Helper.QoutedDate(startDate), Helper.QoutedDate(endDate), fromVoucherID, toVoucherID, projectID);
    //    object val = DbProxy.Instance.FillScalar(qry);
    //    return Helper.GetDecimal(val, 0);
    //}
    public static IDataReader GetFunctionalBalance(int depth, int totalID, int topicID, int year, int month, int fromVoucherID, int toVoucherID, int projectID, bool filterZero, int programID)
    {
        PersianCalendar cal = new PersianCalendar();
        DateTime startDate1 = cal.ToDateTime(year - 100, 1, 1, 0, 0, 0, 0);
        DateTime endDate1 = cal.ToDateTime(year, month, 1, 23, 59, 59, 999).AddDays(-1);
        DateTime startDate2 = cal.ToDateTime(year, month, 1, 0, 0, 0, 0);
        DateTime endDate2 = cal.ToDateTime(year, month, cal.GetDaysInMonth(year, month), 23, 59, 59, 999);
        string qry = string.Format("SELECT * FROM [dbo].[GetFunctionalBalance]({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11})"
            , depth, totalID, topicID, Helper.QoutedDateTime(startDate1), Helper.QoutedDateTime(endDate1), Helper.QoutedDateTime(startDate2), Helper.QoutedDateTime(endDate2), fromVoucherID, toVoucherID
            , projectID, filterZero ? 1 : 0, programID);
        IDataReader dr = DbProxy.Instance.ExecuteReader(qry, 5 * 60); // 5 minutes
        return dr;
    }
    //public static IDataReader GetDifference(int depth, int totalID1, int totalID2, string accountIDs1, string accountIDs2, DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int projectID)
    //{
    //    IDataReader dr = DbProxy.Instance.ExecuteReader("GetDifferenceSP"
    //        , depth, totalID1, totalID2, accountIDs1, accountIDs2, startDate, endDate, fromVoucherID, toVoucherID, projectID);
    //    return dr;
    //}
    public static IDataReader GetVouchersNoItem(DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int wardID)
    {
        //SELECT Name FROM Ward WHERE ID
        string qry = string.Format(@"SELECT A.ID, A.No, A.DoComments, A.Date, A.FixerID, A.WardID, A.Reciever, A.Status, Ward.Name WardName FROM DocumentAccounting A
LEFT JOIN Ward ON A.WardID=Ward.ID
		WHERE {0} <= A.Date AND A.Date <= {1}
			AND {2} <= A.ID AND A.ID <= {3}
			AND NOT A.ID IN (SELECT DISTINCT DocumentAccountingID FROM DocumentAccountingItems) AND ({4}=-1 OR WardID={4})"
            , Helper.QoutedDate(startDate), Helper.QoutedDate(endDate), fromVoucherID, toVoucherID, wardID);
        IDataReader dr = DbProxy.Instance.ExecuteReader(qry);
        return dr;
    }
    public static IDataReader GetVouchersFixed(DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int wardID)
    {
        string qry = string.Format(@"SELECT A.ID, A.No, A.DoComments, A.Date, A.FixerID, A.WardID, A.Reciever, A.Status, Ward.Name WardName FROM DocumentAccounting A
LEFT JOIN Ward ON A.WardID=Ward.ID
		WHERE Status=2 AND {0} <= A.Date AND A.Date <= {1}
			AND {2} <= A.ID AND A.ID <= {3} AND (A.FixerID IS NOT NULL) AND (A.FixerID > 0)
			AND ({4}=-1 OR A.WardID={4})"
            , Helper.QoutedDate(startDate), Helper.QoutedDate(endDate), fromVoucherID, toVoucherID, wardID);
        IDataReader dr = DbProxy.Instance.ExecuteReader(qry);
        return dr;
    }
    public static IDataReader GetVouchersUnFixed(DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int wardID)
    {
        string qry = string.Format(@"SELECT A.ID, A.No, A.DoComments, A.Date, A.FixerID, A.WardID, A.Reciever, A.Status, Ward.Name WardName FROM DocumentAccounting A
LEFT JOIN Ward ON A.WardID=Ward.ID
		WHERE Status<>2 AND {0} <= A.Date AND A.Date <= {1}
			AND {2} <= A.ID AND A.ID <= {3}
			AND ({4}=-1 OR A.WardID={4})"
            , Helper.QoutedDate(startDate), Helper.QoutedDate(endDate), fromVoucherID, toVoucherID, wardID);
        IDataReader dr = DbProxy.Instance.ExecuteReader(qry);
        return dr;
    }
    public static IDataReader GetVouchersNoTitle(DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int wardID)
    {
        string qry = string.Format(@"SELECT A.ID, A.No, A.DoComments, A.Date, A.FixerID, A.WardID, A.Reciever, A.Status, Ward.Name WardName FROM DocumentAccounting A
LEFT JOIN Ward ON A.WardID=Ward.ID
		WHERE {0} <= A.Date AND A.Date <= {1}
			AND {2} <= A.ID AND A.ID <= {3} AND (A.DoComments IS NULL OR LEN(A.DoComments)=0)
			AND ({4}=-1 OR A.WardID={4})"
            , Helper.QoutedDate(startDate), Helper.QoutedDate(endDate), fromVoucherID, toVoucherID, wardID);
        IDataReader dr = DbProxy.Instance.ExecuteReader(qry);
        return dr;
    }
    public static IDataReader GetVouchersUnBalanced(DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID, int wardID)
    {
        IDataReader dr = DbProxy.Instance.ExecuteReader("GetVouchersUnBalanced", startDate, endDate, fromVoucherID, toVoucherID, wardID);
        return dr;
    }
    public static List<int> GetVouchersMissed(DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID)
    {
        List<int> ids = GetVoucherIDs(startDate, endDate, fromVoucherID, toVoucherID);
        List<int> res = new List<int>();
        if (ids.Count > 0)
        {
            for (int i = ids[0] + 1; i < ids[ids.Count - 1]; i++)
                if (!ids.Contains(i))
                    res.Add(i);
        }
        return res;
    }
    public static List<int> GetVoucherIDs(DateTime startDate, DateTime endDate, int fromVoucherID, int toVoucherID)
    {
        string qry = string.Format(@"SELECT ID FROM DocumentAccounting 
		WHERE {0} <= Date AND Date <= {1}
			AND {2} <= ID AND ID <= {3} ORDER BY ID", Helper.QoutedDate(startDate), Helper.QoutedDate(endDate), fromVoucherID, toVoucherID);
        List<int> ids = new List<int>();
        using (DbProxy proxy = new DbProxy())
        {
            IDataReader dr = proxy.ExecuteReader(qry);
            if (dr != null)
            {
                while (dr.Read())
                    ids.Add(dr.GetInt32(0));
                dr.Close();
                dr.Dispose();
            }
            return ids;
        }
    }
    public static List<int> GetVoucherNOs(DateTime startDate, DateTime endDate, int fromVoucherNo, int toVoucherNo)
    {
        string qry = string.Format(@"SELECT CAST(No AS INT) FROM DocumentAccounting 
		WHERE {0} <= Date AND Date <= {1}
			AND {2} <= CAST(No AS INT) AND CAST(No AS INT) <= {3} ORDER BY CAST(No AS INT)"
            , Helper.QoutedDate(startDate), Helper.QoutedDate(endDate), fromVoucherNo, toVoucherNo);
        List<int> nos = new List<int>();
        using (DbProxy proxy = new DbProxy())
        {
            IDataReader dr = proxy.ExecuteReader(qry);
            if (dr != null)
            {
                while (dr.Read())
                    nos.Add(dr.GetInt32(0));
                dr.Close();
                dr.Dispose();
            }
            return nos;
        }
    }
    #endregion


    public static bool ChangeTree(string treePath, int accountID)
    {
        TreeInfo info = GetTree(treePath);
        string newPath = info.TreePath.Substring(0, info.TreePath.LastIndexOf('-') + 1) + accountID;
        ChangeTree(info, newPath);
        return true;
    }
    private static void ChangeTree(TreeInfo info, string newTreePath)
    {
        List<TreeInfo> childs = GetTrees(info);
        if (null != childs)
            for (int i = 0; i < childs.Count; i++)
            {
                string childNewPath = newTreePath + childs[i].TreePath.Substring(info.TreePath.Length);
                ChangeTree(childs[i], childNewPath);
            }
        // Update Tree itself
        DbProxy.Instance.RunQuery(string.Format("UPDATE Trees SET TreePath='{0}' WHERE TreePath='{1}'"
                    , newTreePath, info.TreePath));
        // Updates all items which used the Tree
        DbProxy.Instance.RunQuery(string.Format("UPDATE DocumentAccountingItems SET TreePath='{0}' WHERE TreePath='{1}'"
                    , newTreePath, info.TreePath));
    }

    public static string GetWardName(int wardID)
    {
        return DbProxy.Instance.FillScalarString("SELECT Name FROM Ward WHERE ID=" + wardID);
    }
    public static string GetProgramName(int id)
    {
        if (id <= 0)
            return "";
        return DbProxy.Instance.FillScalarString("SELECT Title FROM Program WHERE ID=" + id);
    }
    public static string GetPatientNameByFileService(int fileServiceID)
    {
        return DbProxy.Instance.FillScalarString(@"SELECT Patient.Name + ' ' + Patient.Surname FROM FileService INNER JOIN Files ON Files.ID=FileService.FileID
INNER JOIN Patient ON Patient.ID=Files.PatientID WHERE FileService.ID=" + fileServiceID);
    }
    public static string GetPersonnelName(int personnelID)
    {
        return DbProxy.Instance.FillScalarString(@"SELECT (Name + ' '+ Surname) AS NameCode FROM Personnel  WHERE ID=" + personnelID);
        return DbProxy.Instance.FillScalarString(@"SELECT (Name + ' '+ Surname +'(' + Code+')') AS NameCode FROM Personnel  WHERE ID=" + personnelID);
    }
    //get person or project
    public static bool IsPrerson(string TreePath)
    {
        return Helper.GetBool(DbProxy.Instance.FillScalar(string.Format("SELECT PersonNeed FROM Trees WHERE TreePath='{0}'", TreePath)), false);
    }
    public static bool IsProject(string TreePath)
    {
        return Helper.GetBool(DbProxy.Instance.FillScalar(string.Format("SELECT ProjectNeed FROM Trees WHERE TreePath='{0}'", TreePath)), false);
    }

    public static bool IsEquipment(string TreePath)
    {
        return Helper.GetBool(DbProxy.Instance.FillScalar(string.Format("SELECT EquipmentNeed FROM Trees WHERE TreePath='{0}'", TreePath)), false);
    }

    public static DataTable GetTreePersonnel(string treePath)
    {
        return DbProxy.Instance.FillData(@"SELECT TreePersonnel.ID, Name, Surname, Code FROM TreePersonnel 
    INNER JOIN Personnel ON TreePersonnel.PersonnelID=Personnel.ID 
    WHERE TreePath=" + Helper.QoutedString(treePath), "TreePersonnel");
    }
    public static DataTable GetTempPersonnel(string ids)
    {
        if (string.IsNullOrEmpty(ids))
            return null;
        return DbProxy.Instance.FillData(@"SELECT ID, Name, Surname, Code FROM  Personnel WHERE ID IN (" + ids + ")", "TreePersonnel");
    }
    public static bool AddTreePersonnel(string treePath, int personnelID)
    {
        if (string.IsNullOrEmpty(treePath))
            return false;
        if (personnelID == Null.NullInteger)
            return false;
        string qry = string.Format("IF NOT EXISTS (SELECT ID FROM TreePersonnel WHERE TreePath={0} AND PersonnelID={1}) INSERT INTO TreePersonnel (TreePath, PersonnelID) VALUES ({0}, {1})"
            , Helper.QoutedString(treePath), personnelID);
        return DbProxy.Instance.RunQuery(qry) > 0;
    }
    public static bool RemoveTreePersonnel(int id)
    {
        string qry = "DELETE FROM TreePersonnel WHERE ID=" + id;
        return DbProxy.Instance.RunQuery(qry) > 0;
    }
}
