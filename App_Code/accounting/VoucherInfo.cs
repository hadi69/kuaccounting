﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for VoucherInfo
/// </summary>
public class VoucherInfo
{
    public int ID { get; set; }
    public string No { get; set; }
    public DateTime Date { get; set; }
    public bool Closed { get; set; }
    public int UserID { get; set; }
    public int FixerID { get; set; }
    public int LockerID { get; set; }
    public string DoComments { get; set; }
    public decimal TotalOwe { get; set; }
    public decimal TotalDue { get; set; }
    public VoucherMode Mode { get; set; }
    public int OtherID { get; set; }
    public int WardID { get; set; }
    public int ProgramID { get; set; }
    public string Reciever { get; set; }
    public int FinanceYearID { get; set; }
    public int Status { get; set; }
    public bool Fixed
    {
        get
        {
            return FixerID > 0;
        }
    }
}