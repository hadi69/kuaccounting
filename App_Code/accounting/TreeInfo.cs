﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class TreeInfo : IComparable<TreeInfo>
{
    public string TreePath { get; set; }
    public int Depth { get; set; }
    public bool PersonNeed { get; set; }
    public bool ProjectNeed { get; set; }
    public bool EquipmentNeed { get; set; }
    public bool CostCenterNeed { get; set; }
    public decimal Owe { get; set; }
    public decimal Due { get; set; }
    public bool CanHaveSubTree { get; set; }
    public bool Removable { get; set; }
    public decimal Remaining
    {
        get { return Owe - Due; }
    }
    public string Shortcut { get; set; }
    public string ParentAlias { get; set; }
    public int TopicID
    {
        get
        {
            return GetTopicID(TreePath);
        }
    }
    public string ParentTreePath
    {
        get
        {
            return GetParentTreePath(TreePath);
        }
    }
    public string TopicName
    {
        get
        {
            TopicInfo info = AccController.GetTopic(this.TopicID);
            if (info != null)
                return info.Title;
            return "";
        }
    }
    public string TopicCode
    {
        get
        {
            TopicInfo info = AccController.GetTopic(this.TopicID);
            if (info != null)
                return info.Code;
            return "";
        }
    }
    public string TopicCodeName
    {
        get
        {
            TopicInfo info = AccController.GetTopic(this.TopicID);
            if (info != null)
                return info.CodeName;
            return "";
        }
    }
    public string TopicNameCode
    {
        get
        {
            TopicInfo info = AccController.GetTopic(this.TopicID);
            if (info != null)
                return info.NameCode;
            return "";
        }
    }
    public string TopicCodeNameFlow2
    {
        get
        {
            int accID = this.TopicID;
            TopicInfo info = AccController.GetTopic(accID);
            if (info == null)
                return "";
            return info.CodeNameFlow;
            //decimal owe, due, remaining;
            //AccController.GetAccountAmounts(accID, DateTime.Now.AddYears(-10), DateTime.Now.AddYears(10), 0, Int32.MaxValue - 5, Null.NullInteger, out owe, out due, out remaining);
        }
    }
    public string TopicCodeNameFlow
    {
        get
        {
            int accID = this.TopicID;
            TopicInfo info = AccController.GetTopic(accID);
            if (info == null)
                return "";
            return string.Format("{0} {1} " + "AbbrvOwe".Translate() + ":{2}  " + "AbbrvDue".Translate() + ":{3}  " + "AbbrvRemain".Translate() + ":{4}",
                info.Code, info.Title.PadRight(50, '-'), Owe.ToString("N0"), Due.ToString("N0"), Remaining.ToString("N0"));
        }
    }
    public string Total
    {
        get
        {
            TopicInfo info = AccController.GetTopic(GetTotalID(TreePath));
            if (info != null)
                return info.Title;
            return "";
        }
    }
    public string Elaborative
    {
        get
        {
            TopicInfo info = AccController.GetTopic(GetElaborativeID(TreePath));
            if (info != null)
                return info.Title;
            return "";
        }
    }
    public TreeInfo()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public int TotalID
    {
        get
        {
            return GetTotalID(TreePath);
        }
    }
    public int ElaborativeID
    {
        get
        {
            return GetElaborativeID(TreePath);
        }
    }
    public static string GetParentTreePath(string treePath)
    {
        if (treePath == null || treePath.IndexOf('-') < 0)
            return null;
        string str = treePath.Substring(0, treePath.LastIndexOf('-'));
        return str;
    }
    public static int GetTopicID(string treePath)
    {
        if (string.IsNullOrEmpty(treePath))
            return Null.NullInteger;
        if (treePath.IndexOf('-') < 0)
            return Helper.GetInt(treePath, Null.NullInteger);
        string str = treePath.Substring(treePath.LastIndexOf('-') + 1);
        return Helper.GetInt(str, Null.NullInteger);
    }
    public static int GetTotalID(string treePath)
    {
        if (treePath.IndexOf('-') < 0)
            return Helper.GetInt(treePath, Null.NullInteger);
        string str = treePath.Substring(0, treePath.IndexOf('-'));
        return Helper.GetInt(str, Null.NullInteger);
    }
    public static int GetElaborativeID(string treePath)
    {
        if (treePath.IndexOf('-') < 0)
            return Null.NullInteger;
        string str = treePath.Substring(treePath.IndexOf('-') + 1);
        if (str.IndexOf('-') < 0)
            return Helper.GetInt(str, Null.NullInteger);
        str = str.Substring(0, str.IndexOf('-'));
        return Helper.GetInt(str, Null.NullInteger);
    }
    public override string ToString()
    {
        return AccController.GetTreeFullPath(TreePath);
    }
    public override bool Equals(object obj)
    {
        if (obj is TreeInfo)
            return TreePath.Equals((obj as TreeInfo).TreePath);
        return base.Equals(obj);
    }
    #region IComparable<TreeInfo> Members

    public int CompareTo(TreeInfo other)
    {
        int mine = Helper.GetInt(TopicCode, Null.NullInteger);
        int his = Helper.GetInt(other.TopicCode, Null.NullInteger);
        if (mine != Null.NullInteger && his != Null.NullInteger)
            return mine.CompareTo(his);
        return TopicCode.CompareTo(other.TopicCode);
    }

    #endregion
}
