﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

public class TopicInfo : IComparable<TopicInfo>
{
    public int ID { get; set; }
    public int TopicGroupID { get; set; }
    public string TopicGroupName { get; set; }
    public int ProgramID { get; set; }
    public string ProgramName { get; set; }
    public string Title { get; set; }
    public string Code { get; set; }
    public TopicLevel Level { get; set; }
    public TopicNature Nature { get; set; }
    public bool IsTemp { get; set; }
    public bool Enabled { get; set; }    
    public string Comments { get; set; }
    public decimal Owe { get; set; }
    public decimal Due { get; set; }
    public string Alias { get; set; }

    public string NameCode
    {
        get
        {
            return string.Format("{0}-{1}", Code, Title);
        }
    }
    public string CodeName
    {
        get
        {
            return string.Format("{0} {1}", Code, Title);
        }
    }
    public string CodeNameFlow
    {
        get
        {
            //string codeName = CodeName;

            return string.Format("{0} {1} " + "AbbrvOwe".Translate() + ":{2}  " + "AbbrvDue".Translate() + ":{3}  " + "AbbrvRemain".Translate() + ":{4}",
                Code, Title.PadRight(50, '-'), Owe.ToString("N0"), Due.ToString("N0"), Remaining.ToString("N0"));
        }
    }
    public decimal Remaining
    {
        get { return Owe - Due; }
    }
    public TopicInfo() { }
  
    public override bool Equals(object obj)
    {
        if (obj is TopicInfo)
            return ID == (obj as TopicInfo).ID;
        return base.Equals(obj);
    }

    #region IComparable<AccountInfo> Members

    public int CompareTo(TopicInfo other)
    {
        int mine = Helper.GetInt(Code, Null.NullInteger);
        int his = Helper.GetInt(other.Code, Null.NullInteger);
        if (mine != Null.NullInteger && his != Null.NullInteger)
            return mine.CompareTo(his);
        return Code.CompareTo(other.Code);
    }

    #endregion
}