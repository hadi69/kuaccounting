﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ItemInfo
/// </summary>
public class ItemInfo
{
    public int ID { get; set; }
    public int DocumentAccountingID { get; set; }
    public string TreePath { get; set; }
    public int PersonnelID { get; set; }
    public int FileServiceID { get; set; }
    public int ProjectID { get; set; }
    public int SortIndex { get; set; }
    public decimal Owe { get; set; }
    public decimal Due { get; set; }
    public string DocNo { get; set; }
    public DateTime DocDate { get; set; }
    public string Comments { get; set; }

    public int TotalID
    {
        get
        {

            return TreeInfo.GetTotalID(TreePath);
        }
    }
    public int ElaborativeID
    {
        get
        {
            return TreeInfo.GetElaborativeID(TreePath);
        }
    }
}