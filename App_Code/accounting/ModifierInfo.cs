﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ModifierInfo
/// </summary>
public class ModifierInfo
{
    public int ID { get; set; }
    public int DocumentAccountingID { get; set; }
    public int UserID { get; set; }
    public DateTime Date { get; set; }
    public string Comments { get; set; }
}