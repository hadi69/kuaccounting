﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for AppHandler
/// </summary>
public class AppHandler
{
    #region entities
    class ServicePriceInfo
    {
        public int ID { get; set; }
        public int VendorID { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public decimal NumCoupons { get; set; }
        public string UnitName { get; set; }
        public string FeaturedImage { get; set; }
        public List<string> Thumbnails { get; set; }
        public List<string> Images { get; set; }
        public int Available { get; set; }
        public string Comments { get; set; }
        public string GroupTitle { get; set; }
        //public string[] Groups { get; set; }
    }

    public class Advertisement
    {
        public int ID;
        public int UserID;
        public int PID; //ProvinceID
        public int CID;//CityID
        public int NID; //TheNeighbourhoodID
        public int Type;
        public string T;//Title
        public string RG; // RegisterDate;
        //public DateTime ReleaseDate;
        public string X; //ExpirationDate
        public int S; //Status
        public int GT; //GeneralType
        public decimal P;//Price
        public string C;//Comments

        public int CY; //CarYear
        public int CF; //CarFunction
        public int CFT;//CarFuelType
        public int CG;//CarGearbox
        public int CB;//CarBodyCondition

        public double EA; //EstateArea
        public int ENR; //EstateNumOfRooms
        public bool EE; //EstateEvidence
        public bool EC;//EstateCountrySide
        public decimal ET;//EstateTrust
        public string Ad; // Address
        public string Tel;
        public string Em;

        public string EF; // EstateFacility
        public string Br; // Brand
        public int Op; // Operator
        public int Ge; // Gender

        // for upload
        public string Pic1, Pic2, Pic3;
        public int Group1, Group2, Group3;
        public List<string> Images { get; set; }
        public List<string> ThumbImages { get; set; }
    }
    class BasketInfo
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public int VendorID { get; set; }
        public DateTime BasketDate { get; set; }
        public int Status { get; set; }
        public int PayStatus { get; set; }
        public int PayMethod { get; set; }
        public String Comments { get; set; }
        public String Code { get; set; }
        public List<BasketItemInfo> Items { get; set; }
        public float TotalNum { get; set; }
        public double TotalPrice { get; set; }
        public double CouponPrice { get; set; }
        public string DeliveryAddress { get; set; }
        public string DeliveryTime { get; set; }
        public string SendOption { get; set; }
        public double ShipmentPrice { get; set; }
    }
    class BasketItemInfo
    {
        public int ID { get; set; }
        public int BasketID { get; set; }
        public int ServicePriceID { get; set; }
        public float Num { get; set; }
        public double Price { get; set; }
    }

    class CustomrReportInfo
    {
        public int ID { get; set; }
        public decimal Due { get; set; }
        public decimal Owe { get; set; }
        public string Date { get; set; }
        public string StoreDraftNo { get; set; }
        public string CheckNo { get; set; }
    }
    #endregion

    public static string ConvertRelativeUrlToAbsoluteUrl(string relativeUrl)
    {
        return string.Format("http{0}://{1}{2}",
            (System.Web.HttpContext.Current.Request.IsSecureConnection) ? "s" : "",
            System.Web.HttpContext.Current.Request.Url.Host,
            System.Web.VirtualPathUtility.ToAbsolute(relativeUrl)
        );
    }
    public static string HandleVersion(HttpRequest request)
    {
        int vendorID = Helper.GetInt(request.QueryString["vid"], Null.NullInteger);
        string appName = Helper.GetString(request.QueryString["app"], "JPMarket");
        var stringWriter = new StringWriter();
        var jsonWriter = new Newtonsoft.Json.JsonTextWriter(stringWriter);
        var serializer = new Newtonsoft.Json.JsonSerializer();
        string ver = "", url = "";
        string path = System.Web.HttpContext.Current.Server.MapPath("~/files/app");
        if (Directory.Exists(path))
        {
            string[] files = Directory.GetFiles(path, appName + "*.apk");
            double max = 0;
            for (int i = 0; i < files.Length; i++)
            {
                string fileName = Path.GetFileName(files[i]);
                double v = Helper.GetDouble(fileName.Replace(".apk", "").Replace(appName, ""), 0);
                if (v > max)
                {
                    ver = v.ToString("0.00");
                    max = v;
                    url = ConvertRelativeUrlToAbsoluteUrl("~/files/app/" + fileName);
                }
            }

        }
        jsonWriter.WriteStartObject();
        jsonWriter.WritePropertyName("Ver");
        serializer.Serialize(jsonWriter, ver);
        jsonWriter.WritePropertyName("Url");
        serializer.Serialize(jsonWriter, url);
        //serializer.Serialize(jsonWriter, "http://192.168.1.100/accounting/files/app-debug.apk");
        using (DbProxy proxy = new DbProxy())
        {
            jsonWriter.WritePropertyName("AboutusImage");
            serializer.Serialize(jsonWriter, vendorID <= 0 ? proxy.GetConfig("AppAboutusImage", "", 0) : proxy.GetUserConfig("AppAboutusImage", "", vendorID));
            jsonWriter.WritePropertyName("SplashImage");
            serializer.Serialize(jsonWriter, vendorID <= 0 ? proxy.GetConfig("AppSplashImage", "", 0) : proxy.GetUserConfig("AppSplashImage", "", vendorID));
            jsonWriter.WritePropertyName("EmptyGroupImage");
            serializer.Serialize(jsonWriter, proxy.GetConfig("AppEmptyGroupImage", "", 0));
            jsonWriter.WritePropertyName("SearchImage");
            serializer.Serialize(jsonWriter, vendorID <= 0 ? proxy.GetConfig("AppSearchImage", "", 0) : proxy.GetUserConfig("AppSearchImage", "", vendorID));
            jsonWriter.WritePropertyName("FavoriteImage");
            serializer.Serialize(jsonWriter, vendorID <= 0 ? proxy.GetConfig("AppFavoriteImage", "", 0) : proxy.GetUserConfig("AppFavoriteImage", "", vendorID));
            jsonWriter.WritePropertyName("BasketImage");
            serializer.Serialize(jsonWriter, vendorID <= 0 ? proxy.GetConfig("AppBasketImage", "", 0) : proxy.GetUserConfig("AppBasketImage", "", vendorID));
            jsonWriter.WritePropertyName("ShowAvailable");
            serializer.Serialize(jsonWriter, vendorID <= 0 ? proxy.GetConfig("AppShowAvailable", "1", 0) : proxy.GetUserConfig("AppShowAvailable", "1", vendorID));
            jsonWriter.WritePropertyName("PayOnline");
            serializer.Serialize(jsonWriter, vendorID <= 0 ? proxy.GetConfig("AppPayOnline", "1", 0) : proxy.GetUserConfig("AppPayOnline", "1", vendorID));
            
            
            string color = vendorID <= 0 ? proxy.GetConfig("AppSearchColor", "FFFFFF", 0) : proxy.GetUserConfig("AppSearchColor", "FFFFFF", vendorID);
            int colorVal = 0xFFFFFF;
            int.TryParse(color, System.Globalization.NumberStyles.HexNumber, Helper.NumberCulture, out colorVal);
            jsonWriter.WritePropertyName("SearchColor");
            serializer.Serialize(jsonWriter, colorVal);
            //serializer.Serialize(jsonWriter, proxy.GetConfig("AppPayOnline", "1", 0));

            jsonWriter.WritePropertyName("MonetaryUnit");
            string baseMonetaryUnitID = proxy.GetConfig("MonetaryUnit", "", 0);
            string monetaryUnitID = vendorID <= 0 ? baseMonetaryUnitID : proxy.GetUserConfig("AppMonetaryUnit", baseMonetaryUnitID, vendorID);
            serializer.Serialize(jsonWriter, proxy.FillScalarString("SELECT Title FROM MonetaryUnit WHERE ID=" + monetaryUnitID));
        }
        jsonWriter.WriteEndObject();

        return stringWriter.ToString();
    }
    public static string HandleUserConfig(HttpRequest request)
    {
        int userID = Helper.GetInt(request.QueryString["uid"], Null.NullInteger);
        var stringWriter = new StringWriter();
        var jsonWriter = new Newtonsoft.Json.JsonTextWriter(stringWriter);
        var serializer = new Newtonsoft.Json.JsonSerializer();

        jsonWriter.WriteStartObject();
        using (DbProxy proxy = new DbProxy())
        {
            jsonWriter.WritePropertyName("NumCopouns");
            string query = string.Format(@"DECLARE @DiscountIn numeric(18, 2), @DiscountOut numeric(18, 2);
SELECT @DiscountIn = SUM(ISNULL(DiscountCard.Num, 0)) FROM DiscountCard WHERE DiscountCard.UserID = {0};
SELECT @DiscountOut = SUM(ISNULL(Basket.NumCoupons, 0)) FROM Basket WHERE Basket.UserID = {0} AND Status <> 3  AND PayStatus = 3; -- Status=3 = Canceled, PayStatus=3 Paid
SELECT @DiscountIn - @DiscountOut;"
                      , userID);
            int numCopouns = (int)Helper.GetFloat(proxy.FillScalar(query), 0);
            serializer.Serialize(jsonWriter, numCopouns);
        }
        jsonWriter.WriteEndObject();

        return stringWriter.ToString();
    }
    public static string HandleVendorConfig(HttpRequest request)
    {
        int vendorID = Helper.GetInt(request.QueryString["vid"], Null.NullInteger);
        int userID = Helper.GetInt(request.QueryString["uid"], Null.NullInteger);
        var stringWriter = new StringWriter();
        var jsonWriter = new Newtonsoft.Json.JsonTextWriter(stringWriter);
        var serializer = new Newtonsoft.Json.JsonSerializer();

        jsonWriter.WriteStartObject();
        using (DbProxy proxy = new DbProxy())
        {
            jsonWriter.WritePropertyName("OrderWaitTime");
            serializer.Serialize(jsonWriter, vendorID <= 0 ? proxy.GetConfig("AppOrderWaitTime", "", 0) : proxy.GetUserConfig("AppOrderWaitTime", "", vendorID));
            jsonWriter.WritePropertyName("OrderStartTime");
            serializer.Serialize(jsonWriter, vendorID <= 0 ? proxy.GetConfig("AppOrderStartTime", "", 0) : proxy.GetUserConfig("AppOrderStartTime", "", vendorID));
            jsonWriter.WritePropertyName("OrderEndTime");
            serializer.Serialize(jsonWriter, vendorID <= 0 ? proxy.GetConfig("AppOrderEndTime", "", 0) : proxy.GetUserConfig("AppOrderEndTime", "", vendorID));
            jsonWriter.WritePropertyName("SendOptions");
            serializer.Serialize(jsonWriter, vendorID <= 0 ? proxy.GetConfig("AppSendOptions", "", 0) : proxy.GetUserConfig("AppSendOptions", "", vendorID));

            jsonWriter.WritePropertyName("BasketShipmentPrice");
            serializer.Serialize(jsonWriter, Helper.GetDecimal(proxy.GetUserConfig("BasketShipmentPrice", "", vendorID), 0));
            jsonWriter.WritePropertyName("BasketPrice1");
            serializer.Serialize(jsonWriter, Helper.GetDecimal(proxy.GetUserConfig("BasketPrice1", "", vendorID), -1));
            jsonWriter.WritePropertyName("BasketShipmenPercent1");
            serializer.Serialize(jsonWriter, Helper.GetDecimal(proxy.GetUserConfig("BasketShipmenPercent1", "", vendorID), 100));
            jsonWriter.WritePropertyName("BasketPrice2");
            serializer.Serialize(jsonWriter, Helper.GetDecimal(proxy.GetUserConfig("BasketPrice2", "", vendorID), -1));
            jsonWriter.WritePropertyName("BasketShipmenPercent2");
            serializer.Serialize(jsonWriter, Helper.GetDecimal(proxy.GetUserConfig("BasketShipmenPercent2", "", vendorID), 100));

            /*
            jsonWriter.WritePropertyName("NumCopouns");
            if (userID > 0)
            {
                string query = string.Format(@"DECLARE @DiscountIn numeric(18, 2), @DiscountOut numeric(18, 2);
SELECT @DiscountIn = SUM(ISNULL(DiscountCard.Num, 0)) FROM DiscountCard WHERE DiscountCard.VendorID = {0} AND DiscountCard.UserID = {1};
SELECT @DiscountOut = SUM(ISNULL(Basket.NumCoupons, 0)) FROM Basket WHERE Basket.VendorID = {0} AND Basket.UserID = {1} AND Status <> 3 -- 3 = Canceled
SELECT @DiscountIn - @DiscountOut;"
                    , vendorID, userID);
                int numCopouns = (int)Helper.GetFloat(proxy.FillScalar(query), 0);
                serializer.Serialize(jsonWriter, numCopouns);
            }
            else
                serializer.Serialize(jsonWriter, 0);
             */
        }
        jsonWriter.WriteEndObject();

        return stringWriter.ToString();
    }
    public static string HandleVendorComments(HttpRequest request)
    {
        int vendorID = Helper.GetInt(request.QueryString["vid"], Null.NullInteger);
        int userID = Helper.GetInt(request.QueryString["uid"], Null.NullInteger);

        using (DbProxy proxy = new DbProxy())
        {
            return proxy.GetUserConfig("AppComments", "", vendorID);
        }
    }
    public static string HandleProvince(HttpRequest request)
    {
        var stringWriter = new StringWriter();
        var jsonWriter = new Newtonsoft.Json.JsonTextWriter(stringWriter);
        var serializer = new Newtonsoft.Json.JsonSerializer();
        using (DbProxy proxy = new DbProxy())
        {
            int vendorID = Helper.GetInt(request.QueryString["vid"], Null.NullInteger);
            string query = "SELECT ID, Title FROM Province WHERE Enabled = 1";
            jsonWriter.WriteStartArray();
            IDataReader reader = proxy.ExecuteReader(query);
            while (reader.Read())
            {
                jsonWriter.WriteStartObject();
                jsonWriter.WritePropertyName("ID");
                serializer.Serialize(jsonWriter, reader.GetInt32(0));
                jsonWriter.WritePropertyName("Title");
                serializer.Serialize(jsonWriter, reader.GetString(1));
                jsonWriter.WriteEndObject();
            }
            reader.Close();
            jsonWriter.WriteEndArray();
        }

        return stringWriter.ToString();
    }
    public static string HandleCity(HttpRequest request)
    {
        var stringWriter = new StringWriter();
        var jsonWriter = new Newtonsoft.Json.JsonTextWriter(stringWriter);
        var serializer = new Newtonsoft.Json.JsonSerializer();
        using (DbProxy proxy = new DbProxy())
        {
            int provinceID = Helper.GetInt(request.QueryString["pid"], Null.NullInteger);
            int vendorID = Helper.GetInt(request.QueryString["vid"], Null.NullInteger);
            string query = "SELECT ID, Title, ProvinceID FROM City WHERE Enabled = 1";
            if (provinceID > 0)
                query += " AND ProvinceID = " + provinceID;
            jsonWriter.WriteStartArray();
            IDataReader reader = proxy.ExecuteReader(query);
            while (reader.Read())
            {
                jsonWriter.WriteStartObject();
                jsonWriter.WritePropertyName("ID");
                serializer.Serialize(jsonWriter, reader.GetInt32(0));
                jsonWriter.WritePropertyName("Title");
                serializer.Serialize(jsonWriter, reader.GetString(1));
                jsonWriter.WritePropertyName("ProvinceID");
                serializer.Serialize(jsonWriter, reader.IsDBNull(2) ? 0 : reader.GetInt32(2));
                jsonWriter.WriteEndObject();
            }
            reader.Close();
            jsonWriter.WriteEndArray();
        }

        return stringWriter.ToString();
    }
    public static string HandleNeighbourhood(HttpRequest request)
    {
        var stringWriter = new StringWriter();
        var jsonWriter = new Newtonsoft.Json.JsonTextWriter(stringWriter);
        var serializer = new Newtonsoft.Json.JsonSerializer();
        using (DbProxy proxy = new DbProxy())
        {
            int cityID = Helper.GetInt(request.QueryString["cid"], Null.NullInteger);
            int vendorID = Helper.GetInt(request.QueryString["vid"], Null.NullInteger);
            string query = "SELECT ID, Title, CityID FROM TheNeighbourhood WHERE Enabled = 1";
            if (cityID > 0)
                query += " AND CityID = " + cityID;
            jsonWriter.WriteStartArray();
            IDataReader reader = proxy.ExecuteReader(query);
            while (reader.Read())
            {
                jsonWriter.WriteStartObject();
                jsonWriter.WritePropertyName("ID");
                serializer.Serialize(jsonWriter, reader.GetInt32(0));
                jsonWriter.WritePropertyName("Title");
                serializer.Serialize(jsonWriter, reader.GetString(1));
                jsonWriter.WritePropertyName("CityID");
                serializer.Serialize(jsonWriter, reader.IsDBNull(2) ? 0 : reader.GetInt32(2));
                jsonWriter.WriteEndObject();
            }
            reader.Close();
            jsonWriter.WriteEndArray();
        }

        return stringWriter.ToString();
    }
    //public static string HandleVendors(HttpRequest request)
    //{
    //    var stringWriter = new StringWriter();
    //    var jsonWriter = new Newtonsoft.Json.JsonTextWriter(stringWriter);
    //    var serializer = new Newtonsoft.Json.JsonSerializer();
    //    using (DbProxy proxy = new DbProxy())
    //    {
    //        int provinceID = Helper.GetInt(request.QueryString["pid"], Null.NullInteger);
    //        int cityID = Helper.GetInt(request.QueryString["cid"], Null.NullInteger);
    //        string query = "SELECT ID, Fullname FROM Users WHERE IsStoreUser = 1";
    //        if (provinceID > 0)
    //            query += " AND ProvinceID = " + provinceID;
    //        if (cityID > 0)
    //            query += " AND CityID = " + cityID;
    //        jsonWriter.WriteStartArray();
    //        IDataReader reader = proxy.ExecuteReader(query);
    //        while (reader.Read())
    //        {
    //            jsonWriter.WriteStartObject();
    //            jsonWriter.WritePropertyName("ID");
    //            serializer.Serialize(jsonWriter, reader.GetInt32(0));
    //            jsonWriter.WritePropertyName("Title");
    //            serializer.Serialize(jsonWriter, reader.GetString(1));
    //            jsonWriter.WriteEndObject();
    //        }
    //        reader.Close();
    //        jsonWriter.WriteEndArray();
    //    }

    //    return stringWriter.ToString();
    //}
    public static string HandleGrouping(HttpRequest request)
    {
        using (DbProxy proxy = new DbProxy())
        {
            int parentID = Helper.GetInt(request.QueryString["pid"], Null.NullInteger);
            int vendorID = Helper.GetInt(request.QueryString["vid"], Null.NullInteger);
            int userID = Helper.GetInt(request.QueryString["uid"], Null.NullInteger);
            int provinceID = Helper.GetInt(request.QueryString["prid"], Null.NullInteger);
            int cityID = Helper.GetInt(request.QueryString["ctid"], Null.NullInteger);
            int neighbourhoodID = Helper.GetInt(request.QueryString["nid"], Null.NullInteger);
            bool root = Helper.GetInt(request.QueryString["root"], Null.NullInteger) == 1;
            string subWhere = "1=1";
            if (provinceID > 0)
                subWhere += " AND U1.ProvinceID = " + provinceID;
            if (cityID > 0)
                subWhere += " AND U1.CityID = " + cityID;
            if (neighbourhoodID > 0)
                subWhere += " AND U1.TheNeighbourhoodID = " + neighbourhoodID;
            string query = @"
-- Vendors
SELECT [Grouping].ID, [Grouping].Title, [Grouping].ParentID, [Grouping].Icon, [Grouping].GroupingType, [Grouping].GroupingSubType
, Users.ID VendorID, Users.ProvinceID, Users.CityID, Users.TheNeighbourhoodID
, (SELECT TOP 1 UserConfig.[Value] FROM UserConfig WHERE UserConfig.[Name]='AppSplashImage' AND UserConfig.UserID=Users.ID) SplashImage
, (SELECT COUNT(G1.ID)  FROM [Grouping] G1 INNER JOIN Users U1 ON G1.ID = U1.RelatedGroupID  WHERE _SUBWHERE_ AND G1.ID IN (SELECT ID FROM [GetAllGroupingChilds]([Grouping].ID))) NumChildren
, (SELECT SUM(ISNULL(DiscountCard.Num, 0)) FROM DiscountCard WHERE DiscountCard.VendorID = Users.ID AND DiscountCard.UserID = _USERID_) DiscountIn
, (SELECT SUM(ISNULL(Basket.NumCoupons, 0)) FROM Basket WHERE Basket.VendorID = Users.ID AND Basket.UserID = _USERID_ AND Status <> 3 AND PayStatus = 3 ) DiscountOut -- 3 = Canceled
, Personnel.HomeTelNum, Personnel.Email, Personnel.HomeAddress, Personnel.Latittude, Personnel.Longitude
FROM [Grouping] INNER JOIN Users ON [Grouping].ID = Users.RelatedGroupID
INNER JOIN Personnel ON Users.PersonnelID = Personnel.ID
WHERE (GroupingType = 1) AND Users.Enabled = 1 _PARENT_

UNION

-- Other groups
SELECT [Grouping].ID, [Grouping].Title, [Grouping].ParentID, [Grouping].Icon, [Grouping].GroupingType, [Grouping].GroupingSubType
, NULL VendorID, NULL ProvinceID, NULL CityID, NULL TheNeighbourhoodID
, NULL SplashImage
, (SELECT COUNT(G1.ID)  FROM [Grouping] G1 INNER JOIN Users U1 ON G1.ID = U1.RelatedGroupID  WHERE _SUBWHERE_ AND G1.ID IN (SELECT ID FROM [GetAllGroupingChilds]([Grouping].ID))) NumChildren
, 0 DiscountIn
, 0 DiscountOut -- 3 = Canceled
, NULL HomeTelNum, NULL Email, NULL HomeAddress, NULL Latittude, NULL Longitude
FROM [Grouping]
WHERE (GroupingType = 1) AND NOT [Grouping].ID IN (SELECT RelatedGroupID FROM Users WHERE NOT RelatedGroupID IS NULL) _PARENT_

UNION

-- Advertisements
SELECT [Grouping].ID, [Grouping].Title, [Grouping].ParentID, [Grouping].Icon, [Grouping].GroupingType, [Grouping].GroupingSubType
, 0 VendorID, 0 ProvinceID, 0 CityID, 0 TheNeighbourhoodID
, NULL SplashImage
, (SELECT COUNT(OtherID) FROM GroupingMember WHERE GroupingID IN (SELECT ID FROM [GetAllGroupingChilds]([Grouping].ID))) NumChildren
, NULL DiscountIn
, NULL DiscountOut -- 3 = Canceled
, NULL HomeTelNum, NULL Email, NULL HomeAddress, NULL Latittude, NULL Longitude
FROM [Grouping] 
WHERE (GroupingType = 4)  _PARENT_".Replace("_USERID_", userID.ToString()).Replace("_SUBWHERE_", subWhere); // 0:person;1:servicprice;2:visitor;3:supplier;4:Advertisement
            if (root)
                query = query.Replace(" _PARENT_", " AND (ParentID IS NULL OR ParentID = 0)");
            if (parentID > 0)
                query = query.Replace(" _PARENT_", " AND ParentID = " + parentID);
            else if (vendorID > 0)
            {
                // specific vendor
                parentID = Helper.GetInt(proxy.FillScalar("SELECT RelatedGroupID FROM Users WHERE ID=" + vendorID), 0);
                query = query.Replace(" _PARENT_", " AND ParentID IN (SELECT ID FROM [GetAllGroupingChilds](" + parentID + ")) ");
            }
            query = query.Replace(" _PARENT_", "");
            IDataReader reader = proxy.ExecuteReader(query);
            return ReturnGroups(vendorID, parentID, reader);
        }
    }
    public static string HandleVendors(HttpRequest request)
    {
        using (DbProxy proxy = new DbProxy())
        {
            int parentID = Helper.GetInt(request.QueryString["pid"], Null.NullInteger);
            int vendorID = Helper.GetInt(request.QueryString["vid"], Null.NullInteger);
            int userID = Helper.GetInt(request.QueryString["uid"], Null.NullInteger);
            bool root = Helper.GetInt(request.QueryString["root"], Null.NullInteger) == 1;
            string what = Helper.GetString(request.QueryString["q"], null); // for services
            string query = @"
SELECT [Grouping].ID, [Grouping].Title, [Grouping].ParentID, [Grouping].Icon, [Grouping].GroupingType, [Grouping].GroupingSubType
, Users.ID VendorID, Users.ProvinceID, Users.CityID, Users.TheNeighbourhoodID
, (SELECT TOP 1 UserConfig.[Value] FROM UserConfig WHERE UserConfig.[Name]='AppSplashImage' AND UserConfig.UserID=Users.ID) SplashImage
, (SELECT COUNT(OtherID) FROM GroupingMember WHERE GroupingID IN (SELECT ID FROM [GetAllGroupingChilds]([Grouping].ID))) NumChildren
, (SELECT SUM(ISNULL(DiscountCard.Num, 0)) FROM DiscountCard WHERE DiscountCard.VendorID = Users.ID AND DiscountCard.UserID = _USERID_) DiscountIn
, (SELECT SUM(ISNULL(Basket.NumCoupons, 0)) FROM Basket WHERE Basket.VendorID = Users.ID AND Basket.UserID = _USERID_ AND Status <> 3 AND PayStatus = 3) DiscountOut -- 3 = Canceled
, Personnel.HomeTelNum, Personnel.Email, Personnel.HomeAddress, Personnel.Latittude, Personnel.Longitude
FROM [Grouping] INNER JOIN Users ON [Grouping].ID = Users.RelatedGroupID 
LEFT JOIN Personnel ON Users.PersonnelID = Personnel.ID
WHERE GroupingType = 1 ".Replace("_USERID_", userID.ToString()); // 0:person;1:servicprice;2:visitor;3:supplier;4:Advertisement
            if (root)
                query += " AND (ParentID IS NULL OR ParentID = 0)";
            if (parentID > 0)
                query += " AND ParentID = " + parentID;
            else if (vendorID > 0)
            {
                // specific vendor
                parentID = Helper.GetInt(proxy.FillScalar("SELECT RelatedGroupID FROM Users WHERE ID=" + vendorID), 0);
                query += " AND ParentID = " + parentID;
            }
            if (!string.IsNullOrEmpty(what))
                query += string.Format(" AND [dbo].[GroupingHasService]([Grouping].ID, {0}) = 1", Helper.QoutedString(what));
            IDataReader reader = proxy.ExecuteReader(query);
            return ReturnGroups(vendorID, parentID, reader);
        }
    }
    public static string HandleGroupingByID(HttpRequest request)
    {
        using (DbProxy proxy = new DbProxy())
        {
            string ids = Helper.GetString(request.QueryString["ids"], null);
            int vendorID = Helper.GetInt(request.QueryString["vid"], Null.NullInteger);
            int userID = Helper.GetInt(request.QueryString["uid"], Null.NullInteger);
            string query = @"
SELECT [Grouping].ID, [Grouping].Title, [Grouping].ParentID, [Grouping].Icon, [Grouping].GroupingType, [Grouping].GroupingSubType
, Users.ID VendorID, Users.ProvinceID, Users.CityID, Users.TheNeighbourhoodID
, (SELECT TOP 1 UserConfig.[Value] FROM UserConfig WHERE UserConfig.[Name]='AppSplashImage' AND UserConfig.UserID=Users.ID) SplashImage
, (SELECT COUNT(G1.ID)  FROM [Grouping] G1 INNER JOIN Users U1 ON G1.ID = U1.RelatedGroupID  WHERE G1.ID IN (SELECT ID FROM [GetAllGroupingChilds]([Grouping].ID))) NumChildren
, (SELECT SUM(ISNULL(DiscountCard.Num, 0)) FROM DiscountCard WHERE DiscountCard.VendorID = Users.ID AND DiscountCard.UserID = _USERID_) DiscountIn
, (SELECT SUM(ISNULL(Basket.NumCoupons, 0)) FROM Basket WHERE Basket.VendorID = Users.ID AND Basket.UserID = _USERID_ AND Status <> 3 AND PayStatus = 3) DiscountOut -- 3 = Canceled
, Personnel.HomeTelNum, Personnel.Email, Personnel.HomeAddress, Personnel.Latittude, Personnel.Longitude
FROM [Grouping] LEFT JOIN Users ON [Grouping].ID = Users.RelatedGroupID 
LEFT JOIN Personnel ON Users.PersonnelID = Personnel.ID
WHERE (GroupingType = 1 OR GroupingType = 4)".Replace("_USERID_", userID.ToString()); // 0:person;1:servicprice;2:visitor;3:supplier;4:Advertisement
            if (!string.IsNullOrEmpty(ids))
                query += string.Format("AND [Grouping].ID IN ({0})", ids);

            IDataReader reader = proxy.ExecuteReader(query);
            return ReturnGroups(vendorID, -1, reader);
        }
    }
    private static string ReturnGroups(int vendorID, int parentID, IDataReader reader)
    {
        var stringWriter = new StringWriter();
        var jsonWriter = new Newtonsoft.Json.JsonTextWriter(stringWriter);
        var serializer = new Newtonsoft.Json.JsonSerializer();
        jsonWriter.WriteStartArray();
        while (reader.Read())
        {
            jsonWriter.WriteStartObject();
            jsonWriter.WritePropertyName("ID");
            serializer.Serialize(jsonWriter, reader.GetInt32(0));
            jsonWriter.WritePropertyName("Title");
            serializer.Serialize(jsonWriter, reader.GetString(1));
            jsonWriter.WritePropertyName("ParentID");
            int pID = reader.IsDBNull(2) ? 0 : reader.GetInt32(2);
            if (vendorID > 0 && parentID == pID)
                serializer.Serialize(jsonWriter, 0);
            else
                serializer.Serialize(jsonWriter, pID);
            jsonWriter.WritePropertyName("I");
            serializer.Serialize(jsonWriter, reader.IsDBNull(3) ? "" : ("files/grouping/" + reader.GetString(3)));
            jsonWriter.WritePropertyName("G");
            serializer.Serialize(jsonWriter, reader.IsDBNull(4) ? 0 : reader.GetInt32(4));
            jsonWriter.WritePropertyName("SG");
            serializer.Serialize(jsonWriter, reader.IsDBNull(5) ? 0 : reader.GetInt32(5));
            jsonWriter.WritePropertyName("V");
            int vID = reader.IsDBNull(6) ? 0 : reader.GetInt32(6);
            serializer.Serialize(jsonWriter, vID);
            jsonWriter.WritePropertyName("P");
            serializer.Serialize(jsonWriter, reader.IsDBNull(7) ? 0 : reader.GetInt32(7));
            jsonWriter.WritePropertyName("C");
            serializer.Serialize(jsonWriter, reader.IsDBNull(8) ? 0 : reader.GetInt32(8));
            jsonWriter.WritePropertyName("N");
            serializer.Serialize(jsonWriter, reader.IsDBNull(9) ? 0 : reader.GetInt32(9));
            jsonWriter.WritePropertyName("SP");
            serializer.Serialize(jsonWriter, reader.IsDBNull(10) ? "" : ("files/userconfig/" + reader.GetString(10)));
            jsonWriter.WritePropertyName("NC");
            serializer.Serialize(jsonWriter, reader.IsDBNull(11) ? 0 : reader.GetInt32(11));
            jsonWriter.WritePropertyName("DI");
            serializer.Serialize(jsonWriter, reader.IsDBNull(12) ? 0 : (int)reader.GetInt32(12));
            jsonWriter.WritePropertyName("DO");
            serializer.Serialize(jsonWriter, reader.IsDBNull(13) ? 0 : Convert.ToInt32(reader.GetValue(13)));


            jsonWriter.WritePropertyName("PT"); // HomeTelNum
            serializer.Serialize(jsonWriter, reader.IsDBNull(14) ? "" : reader.GetString(14));
            jsonWriter.WritePropertyName("PE"); // Email
            serializer.Serialize(jsonWriter, reader.IsDBNull(15) ? "" : reader.GetString(15));
            jsonWriter.WritePropertyName("PA"); // HomeAddress
            serializer.Serialize(jsonWriter, reader.IsDBNull(16) ? "" : reader.GetString(16));
            jsonWriter.WritePropertyName("PL"); // Latittude
            serializer.Serialize(jsonWriter, reader.IsDBNull(17) ? "" : reader.GetString(17));
            jsonWriter.WritePropertyName("PG"); // Longitude
            serializer.Serialize(jsonWriter, reader.IsDBNull(18) ? "" : reader.GetString(18));
            jsonWriter.WriteEndObject();
        }
        reader.Close();
        jsonWriter.WriteEndArray();

        return stringWriter.ToString();
    }
    public static string HandleServics(HttpRequest request)
    {
        var stringWriter = new StringWriter();
        var jsonWriter = new Newtonsoft.Json.JsonTextWriter(stringWriter);
        var serializer = new Newtonsoft.Json.JsonSerializer();
        using (DbProxy proxy = new DbProxy())
        {
            int parentID = Helper.GetInt(request.QueryString["pid"], Null.NullInteger);
            int vendorID = Helper.GetInt(request.QueryString["vid"], Null.NullInteger);
            string what = Helper.GetString(request.QueryString["q"], null);
            int page = Helper.GetInt(request.QueryString["page"], 1);
            bool featured = Helper.GetInt(request.QueryString["featured"], 0) == 1;
            int rp = Helper.GetInt(request.QueryString["rp"], 20);
            int start = ((page - 1) * rp);
            string sortname1 = Helper.GetString(request.QueryString["sortname"], "[dbo].[GetGroupingFullTitle](ServicePriceView.ID, 1)"); // ServiceName
            string sortname = Helper.GetString(request.QueryString["sortname"], "GroupingFullTitle"); // ServiceName
            string sortorder = Helper.GetString(request.QueryString["sortorder"], "asc");
            string sortExp1 = (sortname1 + " " + sortorder).Trim() + " ";
            string sortExp = (sortname + " " + sortorder).Trim() + " ";
            string query;
            if (vendorID == -2 || vendorID > 0) // Bazar or Specific
            {
                sortname = Helper.GetString(request.QueryString["sortname"], "AppAvailable desc, GroupingFullTitle"); // ServiceName
                sortExp = (sortname + " " + sortorder).Trim() + " ";
                query = @"
 ServicePriceView.ID, ISNULL(ServiceView.Code, ISNULL(ServicePriceView.SCoding ,'' )) Coding, ServiceName, ServiceFullName
, UnitName, ServiceView.Comments
, ServicePriceView.AppAvailable
, ROUND(FreeBase * (1 + ISNULL(ProfitPercent, 0) / 100), 2) ProfitSale, ProfitPercent, ServicePriceView.TheLPSP SalePrice, ServicePriceView.ServiceID
, ServiceView.FeaturedImage
, [dbo].[GetGroupingFullTitle](ServicePriceView.ID, 1) GroupingFullTitle
, ServicePriceView.DefaultDiscount, [dbo].[FindVendor](ServicePriceView.ID) VendorID, ServiceView.Featured
FROM ServicePriceView
INNER JOIN ServiceView ON ServiceView.ID=ServicePriceView.ServiceID 
WHERE ServiceView.[Enabled] = 1
";
            }
            else // Full version
            {
                query = @"
 ServicePriceView.ID, ISNULL(ServiceView.Code, ISNULL(ServicePriceView.SCoding ,'' )) Coding, ServiceName, ServiceFullName
, UnitName, ServiceView.Comments
, ISNULL(StoreServices.Income, 0) - ISNULL(StoreServices.Outcome, 0) Available
, ROUND(FreeBase * (1 + ISNULL(ProfitPercent, 0) / 100), 2) ProfitSale, ProfitPercent, ServicePriceView.TheLPSP SalePrice, ServicePriceView.ServiceID
, ServiceView.FeaturedImage
, [dbo].[GetGroupingFullTitle](ServicePriceView.ID, 1) GroupingFullTitle
, ServicePriceView.DefaultDiscount, [dbo].[FindVendor](ServicePriceView.ID) VendorID, ServiceView.Featured
FROM StoreServices
INNER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID 
INNER JOIN ServicePriceView ON StoreServiceDef.ServicePriceID = ServicePriceView.ID 
INNER JOIN ServiceView ON ServiceView.ID=ServicePriceView.ServiceID 
WHERE ServiceView.[Enabled] = 1
";
            }
            if (featured)
            {
                query += " AND ServiceView.Featured > 0 AND FeaturedImage IS NOT NULL AND FeaturedImage <> ''";
                sortname = sortname1 = "Featured ";
                sortExp1 = (sortname1 + " " + sortorder).Trim() + " ";
                sortExp = (sortname + " " + sortorder).Trim() + " ";
            }
            if (parentID > 0)
                query += string.Format("AND ServicePriceView.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (SELECT ID FROM [GetAllGroupingChilds]({0})))", parentID);
            else if (vendorID > 0)
            {
                // specific vendor
                parentID = Helper.GetInt(proxy.FillScalar("SELECT RelatedGroupID FROM Users WHERE ID=" + vendorID), 0);
                query += string.Format("AND ServicePriceView.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (SELECT ID FROM [GetAllGroupingChilds]({0})))", parentID);
            }
            if (proxy.GetConfig("StoreDraftHideZeroService", "0", -1) != "0")
                if (vendorID == -2 || vendorID > 0) // Bazar or Specific
                    query += "AND (ServicePriceView.AppAvailable > 0)";
                else
                    query += "AND ((StoreServicesView.Income - StoreServicesView.Outcome) > 0)";
            if (!string.IsNullOrEmpty(what))
                query += "AND ServicePriceView.ServiceFullName LIKE " + Helper.Likenize(what);
            query = string.Format(@"
WITH TableRows AS 
(
	SELECT ROW_NUMBER() OVER (ORDER BY {0}) AS ZeileNummer,
		{1}
)
SELECT * FROM TableRows WHERE ZeileNummer between {2} And {3} ORDER BY {4}"
       , sortExp1, query, start + 1, start + rp, sortExp);
            List<ServicePriceInfo> res = new List<ServicePriceInfo>();
            List<int> serviceIDs = new List<int>();
            Dictionary<int, ServicePriceInfo> dict = new Dictionary<int, ServicePriceInfo>();
            decimal discountCardPrice = Helper.GetDecimal(proxy.GetConfig("DiscountCardPrice", "0", -1), 0);
            IDataReader reader = proxy.ExecuteReader(query);
            while (reader.Read())
            {
                ServicePriceInfo info = new ServicePriceInfo();
                int serviceID = reader.GetInt32(11);
                // do not add duplicate services
                if (serviceIDs.Contains(serviceID))
                    continue;
                serviceIDs.Add(serviceID);
                info.ID = reader.GetInt32(1);
                info.Code = reader.IsDBNull(2) ? "" : reader.GetString(2);
                info.Title = reader.IsDBNull(3) ? "" : reader.GetString(3); // 3 or 4
                info.UnitName = reader.IsDBNull(5) ? "" : reader.GetString(5);
                info.Comments = reader.IsDBNull(6) ? "" : reader.GetString(6);
                info.Available = reader.IsDBNull(7) ? 0 : (int)reader.GetDouble(7);
                if (!reader.IsDBNull(9)) // ProfitPercent
                    info.Price = reader.IsDBNull(8) ? 0 : reader.GetDecimal(8); // ProfitSale
                else
                    info.Price = reader.IsDBNull(10) ? 0 : reader.GetDecimal(10); // SalePrice
                info.FeaturedImage = reader.IsDBNull(12) ? "" : ("files/services/" + reader.GetString(12));
                info.GroupTitle = reader.IsDBNull(13) ? "" : reader.GetString(13);
                decimal numCopouns = reader.IsDBNull(14) ? 0 : reader.GetDecimal(14);
                if (discountCardPrice == 0)
                    numCopouns = 0;
                if (numCopouns != 0)
                {
                    if (numCopouns > 0)
                        numCopouns /= discountCardPrice;
                    else
                        numCopouns = (-1 * numCopouns / 100) * info.Price / discountCardPrice;
                }
                info.NumCoupons = Math.Round(numCopouns, 0);
                info.VendorID = reader.IsDBNull(15) ? 0 : reader.GetInt32(15);
                info.Images = new List<string>();
                info.Thumbnails = new List<string>();
                res.Add(info);
                if (!dict.ContainsKey(serviceID))
                    dict.Add(serviceID, info);
            }
            reader.Close();

            // get images
            if (dict.Count > 0)
            {
                string ids = string.Join(",", dict.Keys);
                query = string.Format(@"SELECT ServiceID, ThumbImage, MainImage FROM ServiceImage WHERE ServiceID IN ({0})", ids);
                reader = proxy.ExecuteReader(query);
                while (reader.Read())
                {
                    int serviceID = reader.GetInt32(0);
                    ServicePriceInfo info = dict[serviceID];
                    if (!reader.IsDBNull(2))
                        info.Images.Add(reader.GetString(2));
                    if (!reader.IsDBNull(1))
                        info.Thumbnails.Add(reader.GetString(1));
                }
                reader.Close();
            }
            serializer.Serialize(jsonWriter, res);
        }

        return stringWriter.ToString();
    }
    public static string HandleServicesByID(HttpRequest request)
    {
        var stringWriter = new StringWriter();
        var jsonWriter = new Newtonsoft.Json.JsonTextWriter(stringWriter);
        var serializer = new Newtonsoft.Json.JsonSerializer();
        using (DbProxy proxy = new DbProxy())
        {
            string ids = Helper.GetString(request.QueryString["ids"], null);
            int vendorID = Helper.GetInt(request.QueryString["vid"], Null.NullInteger);
            string query;
            if (vendorID == -2 || vendorID > 0) // Bazar or Specific
            {
                query = @"
SELECT 1 ZeileNummer, ServicePriceView.ID, ISNULL(ServiceView.Code, ISNULL(ServicePriceView.SCoding ,'' )) Coding, ServiceName, ServiceFullName
, UnitName, ServiceView.Comments
, ServicePriceView.AppAvailable
, ROUND(FreeBase * (1 + ISNULL(ProfitPercent, 0) / 100), 2) ProfitSale, ProfitPercent, ServicePriceView.TheLPSP SalePrice, ServicePriceView.ServiceID
, ServiceView.FeaturedImage
, [dbo].[GetGroupingFullTitle](ServicePriceView.ID, 1) GroupingFullTitle
, ServicePriceView.DefaultDiscount, [dbo].[FindVendor](ServicePriceView.ID) VendorID
FROM ServicePriceView
INNER JOIN ServiceView ON ServiceView.ID=ServicePriceView.ServiceID 
WHERE ServiceView.[Enabled] = 1
";
            }
            else
            {
                query = @"
SELECT 1 ZeileNummer, ServicePriceView.ID, ISNULL(ServiceView.Code, ISNULL(ServicePriceView.SCoding ,'' )) Coding, ServiceName, ServiceFullName
, UnitName, ServiceView.Comments
, ISNULL(StoreServices.Income, 0) - ISNULL(StoreServices.Outcome, 0) Available
, ROUND(FreeBase * (1 + ISNULL(ProfitPercent, 0) / 100), 2) ProfitSale, ProfitPercent, ServicePriceView.TheLPSP SalePrice, ServicePriceView.ServiceID
, ServiceView.FeaturedImage
, [dbo].[GetGroupingFullTitle](ServicePriceView.ID, 1) GroupingFullTitle
, ServicePriceView.DefaultDiscount, [dbo].[FindVendor](ServicePriceView.ID) VendorID
FROM StoreServices
INNER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID 
INNER JOIN ServicePriceView ON StoreServiceDef.ServicePriceID = ServicePriceView.ID 
INNER JOIN ServiceView ON ServiceView.ID=ServicePriceView.ServiceID 
WHERE ServiceView.[Enabled] = 1
";
            }
            if (!string.IsNullOrEmpty(ids))
                query += string.Format("AND ServicePriceView.ID IN ({0})", ids);
            if (proxy.GetConfig("StoreDraftHideZeroService", "0", -1) != "0")
                if (vendorID == -2 || vendorID > 0) // Bazar or Specific
                    query += "AND (ServicePriceView.AppAvailable > 0)";
                else
                    query += "AND ((StoreServicesView.Income - StoreServicesView.Outcome) > 0)";

            List<ServicePriceInfo> res = new List<ServicePriceInfo>();
            Dictionary<int, ServicePriceInfo> dict = new Dictionary<int, ServicePriceInfo>();
            IDataReader reader = proxy.ExecuteReader(query);
            while (reader.Read())
            {
                ServicePriceInfo info = new ServicePriceInfo();
                int serviceID = reader.GetInt32(11);
                info.ID = reader.GetInt32(1);
                info.Code = reader.IsDBNull(2) ? "" : reader.GetString(2);
                info.Title = reader.IsDBNull(3) ? "" : reader.GetString(3); // 3 or 4
                info.UnitName = reader.IsDBNull(5) ? "" : reader.GetString(5);
                info.Comments = reader.IsDBNull(6) ? "" : reader.GetString(6);
                info.Available = reader.IsDBNull(7) ? 0 : (int)reader.GetDouble(7);
                if (!reader.IsDBNull(9)) // ProfitPercent
                    info.Price = reader.IsDBNull(8) ? 0 : reader.GetDecimal(8); // ProfitSale
                else
                    info.Price = reader.IsDBNull(10) ? 0 : reader.GetDecimal(10); // SalePrice
                info.FeaturedImage = reader.IsDBNull(12) ? "" : ("files/services/" + reader.GetString(12));
                info.GroupTitle = reader.IsDBNull(13) ? "" : reader.GetString(13);
                info.NumCoupons = reader.IsDBNull(14) ? 0 : reader.GetDecimal(14);
                info.VendorID = reader.IsDBNull(15) ? 0 : reader.GetInt32(15);
                info.Images = new List<string>();
                info.Thumbnails = new List<string>();
                res.Add(info);
                if (!dict.ContainsKey(serviceID))
                    dict.Add(serviceID, info);
            }
            reader.Close();

            // get images
            if (dict.Count > 0)
            {
                ids = string.Join(",", dict.Keys);
                query = string.Format(@"SELECT ServiceID, ThumbImage, MainImage FROM ServiceImage WHERE ServiceID IN ({0})", ids);
                reader = proxy.ExecuteReader(query);
                while (reader.Read())
                {
                    int serviceID = reader.GetInt32(0);
                    ServicePriceInfo info = dict[serviceID];
                    if (!reader.IsDBNull(2))
                        info.Images.Add(reader.GetString(2));
                    if (!reader.IsDBNull(1))
                        info.Thumbnails.Add(reader.GetString(1));
                }
                reader.Close();
            }
            serializer.Serialize(jsonWriter, res);
        }

        return stringWriter.ToString();
    }
    public static string HandleRelatedServics(HttpRequest request)
    {
        var stringWriter = new StringWriter();
        var jsonWriter = new Newtonsoft.Json.JsonTextWriter(stringWriter);
        var serializer = new Newtonsoft.Json.JsonSerializer();
        using (DbProxy proxy = new DbProxy())
        {
            int servicePriceID = Helper.GetInt(request.QueryString["id"], Null.NullInteger);
            int serviceID = Helper.GetInt(proxy.FillScalar("SELECT ServiceID FROM ServicePrice WHERE ID=" + servicePriceID), Null.NullInteger);
            int vendorID = Helper.GetInt(request.QueryString["vid"], Null.NullInteger);
            int type = Helper.GetInt(request.QueryString["type"], Null.NullInteger);
            string sortname = Helper.GetString(request.QueryString["sortname"], "ServiceName");
            string sortorder = Helper.GetString(request.QueryString["sortorder"], "asc");
            string sortExp = (sortname + " " + sortorder).Trim() + " ";
            string query;
            if (vendorID == -2 || vendorID > 0) // Bazar or Specific
            {
                query = @"
 SELECT 1 ZeileNummer, ServicePriceView.ID, ISNULL(ServiceView.Code, ISNULL(ServicePriceView.SCoding ,'' )) Coding, ServiceName, ServiceFullName
, UnitName, ServiceView.Comments
, ServicePriceView.AppAvailable
, ROUND(FreeBase * (1 + ISNULL(ProfitPercent, 0) / 100), 2) ProfitSale, ProfitPercent, ServicePriceView.TheLPSP SalePrice, ServicePriceView.ServiceID
, ServiceView.FeaturedImage
, [dbo].[GetGroupingFullTitle](ServicePriceView.ID, 1) GroupingFullTitle
, ServicePriceView.DefaultDiscount, [dbo].[FindVendor](ServicePriceView.ID) VendorID
FROM ServicePriceView
INNER JOIN ServiceView ON ServiceView.ID=ServicePriceView.ServiceID 
WHERE ServiceView.[Enabled] = 1
";
            }
            else // Full version
            {
                query = @"
SELECT 1 ZeileNummer, ServicePriceView.ID, ISNULL(ServiceView.Code, ISNULL(ServicePriceView.SCoding ,'' )) Coding, ServiceName, ServiceFullName
, UnitName, ServiceView.Comments
, ISNULL(StoreServices.Income, 0) - ISNULL(StoreServices.Outcome, 0) Available
, ROUND(FreeBase * (1 + ISNULL(ProfitPercent, 0) / 100), 2) ProfitSale, ProfitPercent, ServicePriceView.TheLPSP SalePrice, ServicePriceView.ServiceID
, ServiceView.FeaturedImage
, [dbo].[GetGroupingFullTitle](ServicePriceView.ID, 1) GroupingFullTitle
, ServicePriceView.DefaultDiscount, [dbo].[FindVendor](ServicePriceView.ID) VendorID
FROM StoreServices
INNER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID 
INNER JOIN ServicePriceView ON StoreServiceDef.ServicePriceID = ServicePriceView.ID 
INNER JOIN ServiceView ON ServiceView.ID=ServicePriceView.ServiceID 
WHERE ServiceView.[Enabled] = 1
";
            }

            if (type == Null.NullInteger)
                query += string.Format(@"AND (ServicePriceView.ServiceID IN (SELECT RelatedID FROM ServiceRelation WHERE ServiceID = {0}) OR ServicePriceView.ServiceID IN (SELECT ServiceID FROM ServiceRelation WHERE RelatedID = {0}))", serviceID);
            else
                query += string.Format("AND ServicePriceView.ServiceID IN (SELECT RelatedID FROM ServiceRelation WHERE ServiceID = {0} AND [Type]={1})", serviceID, type);

            if (proxy.GetConfig("StoreDraftHideZeroService", "0", -1) != "0")
                if (vendorID == -2 || vendorID > 0) // Bazar or Specific
                    query += "AND (ServicePriceView.AppAvailable > 0)";
                else
                    query += "AND ((StoreServicesView.Income - StoreServicesView.Outcome) > 0)";

            List<ServicePriceInfo> res = new List<ServicePriceInfo>();
            List<int> serviceIDs = new List<int>();
            Dictionary<int, ServicePriceInfo> dict = new Dictionary<int, ServicePriceInfo>();
            IDataReader reader = proxy.ExecuteReader(query);
            while (reader.Read())
            {
                ServicePriceInfo info = new ServicePriceInfo();
                serviceID = reader.GetInt32(11);
                // do not add duplicate services
                if (serviceIDs.Contains(serviceID))
                    continue;
                serviceIDs.Add(serviceID);
                info.ID = reader.GetInt32(1);
                info.Code = reader.IsDBNull(2) ? "" : reader.GetString(2);
                info.Title = reader.IsDBNull(3) ? "" : reader.GetString(3); // 3 or 4
                info.UnitName = reader.IsDBNull(5) ? "" : reader.GetString(5);
                info.Comments = reader.IsDBNull(6) ? "" : reader.GetString(6);
                info.Available = reader.IsDBNull(7) ? 0 : (int)reader.GetDouble(7);
                if (!reader.IsDBNull(9)) // ProfitPercent
                    info.Price = reader.IsDBNull(8) ? 0 : reader.GetDecimal(8); // ProfitSale
                else
                    info.Price = reader.IsDBNull(10) ? 0 : reader.GetDecimal(10); // SalePrice
                info.FeaturedImage = reader.IsDBNull(12) ? "" : ("files/services/" + reader.GetString(12));
                info.GroupTitle = reader.IsDBNull(13) ? "" : reader.GetString(13);
                info.NumCoupons = reader.IsDBNull(14) ? 0 : reader.GetDecimal(14);
                info.VendorID = reader.IsDBNull(15) ? 0 : reader.GetInt32(15);
                info.Images = new List<string>();
                info.Thumbnails = new List<string>();
                res.Add(info);
                if (!dict.ContainsKey(serviceID))
                    dict.Add(serviceID, info);
            }
            reader.Close();

            // get images
            if (dict.Count > 0)
            {
                string ids = string.Join(",", dict.Keys);
                query = string.Format(@"SELECT ServiceID, ThumbImage, MainImage FROM ServiceImage WHERE ServiceID IN ({0})", ids);
                reader = proxy.ExecuteReader(query);
                while (reader.Read())
                {
                    serviceID = reader.GetInt32(0);
                    ServicePriceInfo info = dict[serviceID];
                    if (!reader.IsDBNull(2))
                        info.Images.Add(reader.GetString(2));
                    if (!reader.IsDBNull(1))
                        info.Thumbnails.Add(reader.GetString(1));
                }
                reader.Close();
            }
            serializer.Serialize(jsonWriter, res);
        }

        return stringWriter.ToString();
    }
    public static string HandleAdvertisements(HttpRequest request)
    {
        var stringWriter = new StringWriter();
        var jsonWriter = new Newtonsoft.Json.JsonTextWriter(stringWriter);
        var serializer = new Newtonsoft.Json.JsonSerializer();
        using (DbProxy proxy = new DbProxy())
        {
            int parentID = Helper.GetInt(request.QueryString["pid"], Null.NullInteger);
            int userID = Helper.GetInt(request.QueryString["uid"], Null.NullInteger);
            int provinceID = Helper.GetInt(request.QueryString["prid"], Null.NullInteger);
            int cityID = Helper.GetInt(request.QueryString["cid"], Null.NullInteger);
            int neighbourhoodID = Helper.GetInt(request.QueryString["nid"], Null.NullInteger);
            int superType = Helper.GetInt(request.QueryString["st"], Null.NullInteger);
            string what = Helper.GetString(request.QueryString["q"], null);
            int page = Helper.GetInt(request.QueryString["page"], 1);
            int rp = 20;
            int start = ((page - 1) * rp);
            string sortname = Helper.GetString(request.QueryString["sortname"], "ReleaseDate");
            string sortorder = Helper.GetString(request.QueryString["sortorder"], "desc");
            string sortExp = (sortname + " " + sortorder).Trim() + " ";
            string query = @"
 ID, ProvinceID, CityID, TheNeighbourhoodID, 
Type, Title, RegisterDate, ReleaseDate, ExpirationDate, 
Status, GeneralType, Price, Comments, 
CarYear, CarFunction, CarFuelType, CarGearbox, CarBodyCondition, 
EstateArea, EstateNumOfRooms, EstateEvidence, EstateCountrySide, EstateTrust, Address, Tel, Email
, Gender, Operator, Brand, EstateFacility, UserID
, (SELECT GroupingID FROM GroupingMember INNER JOIN [Grouping] ON GroupingMember.GroupingID = [Grouping].ID WHERE OtherID = Advertisement.ID AND GroupingType = 4) Group1
FROM Advertisement
WHERE ExpirationDate >= GETDATE() AND ReleaseDate <= GETDATE() 
";

            if (parentID > 0)
                query += string.Format("AND Advertisement.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (SELECT ID FROM [GetAllGroupingChilds]({0})))", parentID);
            if (userID > 0)
                query += "AND Advertisement.UserID = " + userID;
            else
                query += "AND (Status=3 OR Status=4)";

            if (superType == 1) // SUPER_TYPE_ESTATE
            {
                int rootID = Helper.GetInt(proxy.FillScalar("SELECT TOP 1 ID FROM [Grouping] WHERE (ParentID IS NULL OR ParentID = 0) AND GroupingType = 4 AND 10 <= GroupingSubType AND GroupingSubType < 20 "), parentID);
                query += string.Format("AND Advertisement.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (SELECT ID FROM [GetAllGroupingChilds]({0})))", rootID);
            }
            else if (superType == 2) // SUPER_TYPE_CAR
            {
                int rootID = Helper.GetInt(proxy.FillScalar("SELECT TOP 1 ID FROM [Grouping] WHERE (ParentID IS NULL OR ParentID = 0) AND GroupingType = 4 AND 20 <= GroupingSubType AND GroupingSubType < 29 "), parentID);
                query += string.Format("AND Advertisement.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (SELECT ID FROM [GetAllGroupingChilds]({0})))", rootID);
            }
            else if (superType == 3) // SUPER_TYPE_OTHER
            {
                int rootEstateID = Helper.GetInt(proxy.FillScalar("SELECT TOP 1 ID FROM [Grouping] WHERE (ParentID IS NULL OR ParentID = 0) AND GroupingType = 4 AND 10 <= GroupingSubType AND GroupingSubType < 19 "), parentID);
                int rootCarID = Helper.GetInt(proxy.FillScalar("SELECT TOP 1 ID FROM [Grouping] WHERE (ParentID IS NULL OR ParentID = 0) AND GroupingType = 4 AND 20 <= GroupingSubType AND GroupingSubType < 29 "), parentID);
                query += string.Format(@"AND Advertisement.ID IN (SELECT OtherID FROM GroupingMember WHERE 
                    NOT GroupingID IN (SELECT ID FROM [GetAllGroupingChilds]({0}))
                    AND NOT GroupingID IN (SELECT ID FROM [GetAllGroupingChilds]({1}))
                )", rootEstateID, rootCarID);
            }

            if (provinceID > 0)
                query += "AND (Advertisement.ProvinceID IS NULL OR Advertisement.ProvinceID <= 0 OR Advertisement.ProvinceID = " + provinceID + ")";
            if (cityID > 0)
                query += "AND (Advertisement.CityID IS NULL OR Advertisement.CityID <= 0 OR Advertisement.CityID = " + cityID + ")";
            if (neighbourhoodID > 0)
                query += "AND (Advertisement.TheNeighbourhoodID IS NULL OR Advertisement.TheNeighbourhoodID <= 0 OR Advertisement.TheNeighbourhoodID = " + neighbourhoodID + ")";

            if (!string.IsNullOrEmpty(what))
                query += "AND Advertisement.Title LIKE " + Helper.Likenize(what);
            query = string.Format(@"
WITH TableRows AS 
(
	SELECT ROW_NUMBER() OVER (ORDER BY {0}) AS ZeileNummer,
		{1}
)
SELECT * FROM TableRows WHERE ZeileNummer between {2} And {3} ORDER BY {0}"
       , sortExp, query, start + 1, start + rp);
            List<Advertisement> res = new List<Advertisement>();
            List<int> advIDs = new List<int>();
            Dictionary<int, Advertisement> dict = new Dictionary<int, Advertisement>();
            IDataReader reader = proxy.ExecuteReader(query);
            while (reader.Read())
            {
                Advertisement info = new Advertisement();
                int id = reader.GetInt32(1);
                if (advIDs.Contains(id))
                    continue;
                advIDs.Add(id);
                info.ID = reader.GetInt32(1);
                info.PID = reader.IsDBNull(2) ? 0 : reader.GetInt32(2);
                info.CID = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);
                info.NID = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
                info.Type = reader.IsDBNull(5) ? 0 : reader.GetInt32(5);
                info.T = reader.IsDBNull(6) ? "" : reader.GetString(6);
                info.RG = reader.IsDBNull(7) ? "" : Helper.FormatDate(reader.GetDateTime(7), "yyyy-MM-dd");
                //info.ReleaseDate = reader.IsDBNull(8) ? Null.NullDate : reader.GetDateTime(8);
                info.X = reader.IsDBNull(9) ? "" : Helper.FormatDate(reader.GetDateTime(9), "yyyy-MM-dd");
                info.S = reader.IsDBNull(10) ? 0 : reader.GetInt32(10);
                info.GT = reader.IsDBNull(11) ? 0 : reader.GetInt32(11);
                info.P = reader.IsDBNull(12) ? 0 : reader.GetDecimal(12);
                info.C = reader.IsDBNull(13) ? "" : reader.GetString(13);

                info.CY = reader.IsDBNull(14) ? 0 : reader.GetInt32(14);
                info.CF = reader.IsDBNull(15) ? 0 : reader.GetInt32(15);
                info.CFT = reader.IsDBNull(16) ? 0 : reader.GetInt32(16);
                info.CG = reader.IsDBNull(17) ? 0 : reader.GetInt32(17);
                info.CB = reader.IsDBNull(18) ? 0 : reader.GetInt32(18);

                info.EA = reader.IsDBNull(19) ? 0 : reader.GetDouble(19);
                info.ENR = reader.IsDBNull(20) ? 0 : reader.GetInt32(20);
                info.EE = reader.IsDBNull(21) ? false : reader.GetBoolean(21);
                info.EC = reader.IsDBNull(22) ? false : reader.GetBoolean(22);
                info.ET = reader.IsDBNull(23) ? 0 : reader.GetDecimal(23);
                info.Ad = reader.IsDBNull(24) ? "" : reader.GetString(24);
                info.Tel = reader.IsDBNull(25) ? "" : reader.GetString(25);
                info.Em = reader.IsDBNull(26) ? "" : reader.GetString(26);

                info.Ge = reader.IsDBNull(27) ? 0 : reader.GetInt32(27);
                info.Br = reader.IsDBNull(28) ? "" : reader.GetString(28);
                info.Op = reader.IsDBNull(29) ? 0 : reader.GetInt32(29);
                info.EF = reader.IsDBNull(30) ? "" : reader.GetString(30);

                info.UserID = reader.IsDBNull(31) ? 0 : reader.GetInt32(31);
                info.Group1 = reader.IsDBNull(32) ? 0 : reader.GetInt32(32);

                info.Images = new List<string>();
                info.ThumbImages = new List<string>();
                //info.Thumbnails = new List<string>();
                res.Add(info);
                if (!dict.ContainsKey(id))
                    dict.Add(id, info);
            }
            reader.Close();

            // get images
            if (dict.Count > 0)
            {
                string ids = string.Join(",", dict.Keys);
                query = string.Format(@"SELECT AdvertisementID, ThumbImage, MainImage FROM AdvertisementImage WHERE AdvertisementID IN ({0}) ORDER BY AdvertisementImage.ID ASC", ids);
                reader = proxy.ExecuteReader(query);
                while (reader.Read())
                {
                    int id = reader.GetInt32(0);
                    Advertisement info = dict[id];
                    if (!reader.IsDBNull(2))
                        info.Images.Add(reader.GetString(2));
                    if (!reader.IsDBNull(1))
                        info.ThumbImages.Add(reader.GetString(1));
                }
                reader.Close();
            }
            serializer.Serialize(jsonWriter, res);
        }

        return stringWriter.ToString();
    }
    public static string HandleAdvertisementsByID(HttpRequest request)
    {
        var stringWriter = new StringWriter();
        var jsonWriter = new Newtonsoft.Json.JsonTextWriter(stringWriter);
        var serializer = new Newtonsoft.Json.JsonSerializer();
        using (DbProxy proxy = new DbProxy())
        {
            string ids = Helper.GetString(request.QueryString["ids"], null);
            string what = Helper.GetString(request.QueryString["q"], null);
            int page = Helper.GetInt(request.QueryString["page"], 1);
            int rp = 20;
            int start = ((page - 1) * rp);
            string sortname = Helper.GetString(request.QueryString["sortname"], "ID");
            string sortorder = Helper.GetString(request.QueryString["sortorder"], "desc");
            string sortExp = (sortname + " " + sortorder).Trim() + " ";
            string query = @"
 ID, ProvinceID, CityID, TheNeighbourhoodID, 
Type, Title, RegisterDate, ReleaseDate, ExpirationDate, 
Status, GeneralType, Price, Comments, 
CarYear, CarFunction, CarFuelType, CarGearbox, CarBodyCondition, 
EstateArea, EstateNumOfRooms, EstateEvidence, EstateCountrySide, EstateTrust, Address, Tel, Email
, Gender, Operator, Brand, EstateFacility, UserID
, (SELECT GroupingID FROM GroupingMember INNER JOIN [Grouping] ON GroupingMember.GroupingID = [Grouping].ID WHERE OtherID = Advertisement.ID AND GroupingType=4) Group1
FROM Advertisement
WHERE ExpirationDate >= GETDATE() AND ReleaseDate <= GETDATE() 
";
            if (!string.IsNullOrEmpty(ids))
                query += string.Format("AND [Advertisement].ID IN ({0})", ids);
            if (!string.IsNullOrEmpty(what))
                query += "AND Advertisement.Title LIKE " + Helper.Likenize(what);
            query = string.Format(@"
WITH TableRows AS 
(
	SELECT ROW_NUMBER() OVER (ORDER BY {0}) AS ZeileNummer,
		{1}
)
SELECT * FROM TableRows WHERE ZeileNummer between {2} And {3} ORDER BY {0}"
       , sortExp, query, start + 1, start + rp);
            List<Advertisement> res = new List<Advertisement>();
            List<int> advIDs = new List<int>();
            Dictionary<int, Advertisement> dict = new Dictionary<int, Advertisement>();
            IDataReader reader = proxy.ExecuteReader(query);
            while (reader.Read())
            {
                Advertisement info = new Advertisement();
                int id = reader.GetInt32(1);
                if (advIDs.Contains(id))
                    continue;
                advIDs.Add(id);
                info.ID = reader.GetInt32(1);
                info.PID = reader.IsDBNull(2) ? 0 : reader.GetInt32(2);
                info.CID = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);
                info.NID = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
                info.Type = reader.IsDBNull(5) ? 0 : reader.GetInt32(5);
                info.T = reader.IsDBNull(6) ? "" : reader.GetString(6);
                info.RG = reader.IsDBNull(7) ? "" : Helper.FormatDate(reader.GetDateTime(7), "yyyy-MM-dd");
                //info.ReleaseDate = reader.IsDBNull(8) ? Null.NullDate : reader.GetDateTime(8);
                info.X = reader.IsDBNull(9) ? "" : Helper.FormatDate(reader.GetDateTime(9), "yyyy-MM-dd");
                info.S = reader.IsDBNull(10) ? 0 : reader.GetInt32(10);
                info.GT = reader.IsDBNull(11) ? 0 : reader.GetInt32(11);
                info.P = reader.IsDBNull(12) ? 0 : reader.GetDecimal(12);
                info.C = reader.IsDBNull(13) ? "" : reader.GetString(13);

                info.CY = reader.IsDBNull(14) ? 0 : reader.GetInt32(14);
                info.CF = reader.IsDBNull(15) ? 0 : reader.GetInt32(15);
                info.CFT = reader.IsDBNull(16) ? 0 : reader.GetInt32(16);
                info.CG = reader.IsDBNull(17) ? 0 : reader.GetInt32(17);
                info.CB = reader.IsDBNull(18) ? 0 : reader.GetInt32(18);

                info.EA = reader.IsDBNull(19) ? 0 : reader.GetDouble(19);
                info.ENR = reader.IsDBNull(20) ? 0 : reader.GetInt32(20);
                info.EE = reader.IsDBNull(21) ? false : reader.GetBoolean(21);
                info.EC = reader.IsDBNull(22) ? false : reader.GetBoolean(22);
                info.ET = reader.IsDBNull(23) ? 0 : reader.GetDecimal(23);
                info.Ad = reader.IsDBNull(24) ? "" : reader.GetString(24);
                info.Tel = reader.IsDBNull(25) ? "" : reader.GetString(25);
                info.Em = reader.IsDBNull(26) ? "" : reader.GetString(26);

                info.Ge = reader.IsDBNull(27) ? 0 : reader.GetInt32(27);
                info.Br = reader.IsDBNull(28) ? "" : reader.GetString(28);
                info.Op = reader.IsDBNull(29) ? 0 : reader.GetInt32(29);
                info.EF = reader.IsDBNull(30) ? "" : reader.GetString(30);

                info.UserID = reader.IsDBNull(31) ? 0 : reader.GetInt32(31);
                info.Group1 = reader.IsDBNull(32) ? 0 : reader.GetInt32(32);

                info.Images = new List<string>();
                info.ThumbImages = new List<string>();
                //info.Thumbnails = new List<string>();
                res.Add(info);
                if (!dict.ContainsKey(id))
                    dict.Add(id, info);
            }
            reader.Close();

            // get images
            if (dict.Count > 0)
            {
                string _ids = string.Join(",", dict.Keys);
                query = string.Format(@"SELECT AdvertisementID, ThumbImage, MainImage FROM AdvertisementImage WHERE AdvertisementID IN ({0}) ORDER BY AdvertisementImage.ID ASC", _ids);
                reader = proxy.ExecuteReader(query);
                while (reader.Read())
                {
                    int id = reader.GetInt32(0);
                    Advertisement info = dict[id];
                    if (!reader.IsDBNull(2))
                        info.Images.Add(reader.GetString(2));
                    if (!reader.IsDBNull(1))
                        info.ThumbImages.Add(reader.GetString(1));
                }
                reader.Close();
            }
            serializer.Serialize(jsonWriter, res);
        }

        return stringWriter.ToString();
    }
    public static string HandleAddAdvertisement(HttpRequest request)
    {
        string data = request.Form["data"];
        int len = (int)request.InputStream.Length;
        if (len > 0 && string.IsNullOrEmpty(data))
        {
            byte[] bytes = new byte[len];
            request.InputStream.Read(bytes, 0, bytes.Length);
            string content = new System.Text.UTF8Encoding().GetString(bytes);
            if (content.Contains("data="))
                data = System.Web.HttpUtility.UrlDecode(content.Split('=')[1]);
        }
        if (string.IsNullOrEmpty(data))
            return "ERROR: empty basket";
        using (DbProxy proxy = new DbProxy())
        {
            try
            {
                List<Advertisement> advs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Advertisement>>(data);
                Advertisement adv = advs[0];
                int oldID = adv.ID;

                string query = string.Format(@"INSERT INTO Advertisement (Type, Title, RegisterDate, ReleaseDate, ExpirationDate, Status, GeneralType, Price, Comments, ProvinceID, CityID, TheNeighbourhoodID, CarYear
                    , CarFunction, CarFuelType, CarGearbox, CarBodyCondition
                    , EstateArea, EstateNumOfRooms, EstateEvidence, EstateCountrySide, EstateTrust, UserID, Address, Tel, Email, EstateFacility, Gender)
                     VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}
                    , {13}, {14}, {15}, {16}
                    , {17}, {18}, {19}, {20}, {21}, {22}, {23}, {24}, {25}, {26}, {27});
                     SELECT SCOPE_IDENTITY();"
                    , adv.Type, Helper.QoutedString(adv.T), "GETDATE()", "GETDATE()", Helper.QoutedDate(DateTime.Now.AddMonths(1)) // adv.X
                    , 1 /*adv.S Status = NEW*/, adv.GT, adv.P, Helper.QoutedString(adv.C)
                    , adv.PID > 0 ? adv.PID.ToString() : "NULL", adv.CID > 0 ? adv.CID.ToString() : "NULL", adv.NID > 0 ? adv.NID.ToString() : "NULL"
                    , adv.CY, adv.CF, adv.CFT, adv.CG, adv.CB
                    , adv.EA, adv.ENR, adv.EE ? 1 : 0, adv.EC ? 1 : 0, adv.ET, adv.UserID
                    , Helper.QoutedString(adv.Ad), Helper.QoutedString(adv.Tel), Helper.QoutedString(adv.Em), Helper.QoutedString(adv.EF), adv.Ge);
                proxy.BeginTransaction();
                int id = Helper.GetInt(proxy.FillScalar(query), Null.NullInteger);
                if (id == Null.NullInteger)
                {
                    proxy.RollbackTransaction();
                    return "ERROR: unable to insert advertisement";
                }

                // delete old images and records
                if (oldID > 0)
                {
                    DataTable dt = proxy.FillData(@"SELECT AdvertisementID, ThumbImage, MainImage FROM AdvertisementImage WHERE AdvertisementID =" + oldID, "hop");
                    if (dt != null)
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            try
                            {
                                string p = System.Web.HttpContext.Current.Server.MapPath("~/" + Helper.GetString(dt.Rows[i][1], "ohohmygod.jj"));
                                if (File.Exists(p))
                                    File.Delete(p);
                            }
                            catch { }
                            try
                            {
                                string p = System.Web.HttpContext.Current.Server.MapPath("~/" + Helper.GetString(dt.Rows[i][2], "ohohmygod.jj"));
                                if (File.Exists(p))
                                    File.Delete(p);
                            }
                            catch { }
                        }
                    proxy.RunQuery(string.Format("DELETE FROM AdvertisementImage WHERE AdvertisementID = {0}; DELETE FROM Advertisement WHERE ID = {0}", oldID));
                    proxy.RunQuery(string.Format("DELETE FROM GroupingMember WHERE OtherID = {0} AND GroupinID IN (SELECT ID FROM [Grouping] WHERE GroupingType = 4)", oldID));
                }
                // insert images
                if (!string.IsNullOrEmpty(adv.Pic1))
                {
                    byte[] base64EncodedBytes = System.Convert.FromBase64String(adv.Pic1);
                    SaveAdvImage(proxy, id, "pic1.jpg", base64EncodedBytes);
                }
                if (!string.IsNullOrEmpty(adv.Pic2))
                {
                    byte[] base64EncodedBytes = System.Convert.FromBase64String(adv.Pic2);
                    SaveAdvImage(proxy, id, "pic2.jpg", base64EncodedBytes);
                }
                if (!string.IsNullOrEmpty(adv.Pic3))
                {
                    byte[] base64EncodedBytes = System.Convert.FromBase64String(adv.Pic3);
                    SaveAdvImage(proxy, id, "pic3.jpg", base64EncodedBytes);
                }
                if (adv.Group1 > 0)
                    proxy.RunQuery(string.Format("INSERT INTO GroupingMember (GroupingID, OtherID)  VALUES({0}, {1})", adv.Group1, id));
                if (adv.Group2 > 0)
                    proxy.RunQuery(string.Format("INSERT INTO GroupingMember (GroupingID, OtherID)  VALUES({0}, {1})", adv.Group2, id));
                if (adv.Group3 > 0)
                    proxy.RunQuery(string.Format("INSERT INTO GroupingMember (GroupingID, OtherID)  VALUES({0}, {1})", adv.Group3, id));
                proxy.CommitTransaction();

                Triggers.AdvertisementInsertUpdate(id);
                SmsHelper.SendToIramal(null, null, "ایرامال: آگهی جدید ثبت شد.", false, true);
                return "DONE";

            }
            catch (Exception ex)
            {
                proxy.RollbackTransaction();
                return "ERROR: " + ex.Message;
            }
        }
    }
    public static string HandleDeleteAdvertisement(HttpRequest request)
    {
        int id = Helper.GetInt(request.QueryString["id"], Null.NullInteger);

        if (id <= 0)
            return "ERROR: invalid Advertisement";
        using (DbProxy proxy = new DbProxy())
        {
            try
            {

                proxy.BeginTransaction();
                DataTable dt = proxy.FillData(@"SELECT AdvertisementID, ThumbImage, MainImage FROM AdvertisementImage WHERE AdvertisementID =" + id, "hop");
                if (dt != null)
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string p = System.Web.HttpContext.Current.Server.MapPath("~/" + Helper.GetString(dt.Rows[i][1], "ohohmygod.jj"));
                        if (File.Exists(p))
                            File.Delete(p);
                        p = System.Web.HttpContext.Current.Server.MapPath("~/" + Helper.GetString(dt.Rows[i][2], "ohohmygod.jj"));
                        if (File.Exists(p))
                            File.Delete(p);
                    }
                proxy.RunQuery(string.Format("DELETE FROM AdvertisementImage WHERE AdvertisementID = {0}; DELETE FROM Advertisement WHERE ID = {0}", id));
                proxy.RunQuery(string.Format("DELETE FROM GroupingMember WHERE OtherID = {0} AND GroupinID IN (SELECT ID FROM [Grouping] WHERE GroupingType = 4)", id));

                proxy.CommitTransaction();
                return "DONE";

            }
            catch (Exception ex)
            {
                proxy.RollbackTransaction();
                return "ERROR: " + ex.Message;
            }
        }
    }
    private static void SaveAdvImage(DbProxy proxy, int advID, string picName, byte[] imageData)
    {
        String filename = advID + picName;
        string tempPath = "files/advertisement/" + advID.ToString(); // System.Configuration.ConfigurationManager.AppSettings["FolderPath"];
        string savePath = System.Web.HttpContext.Current.Server.MapPath("~/" + tempPath);
        if (!Directory.Exists(savePath))
        {
            Directory.CreateDirectory(savePath);
        }
        string actualFileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + "_" + filename;
        string actualFile = savePath + '\\' + actualFileName;
        string thumbnailFileName = "thumb_" + actualFileName;
        string thumbnail = savePath + '\\' + thumbnailFileName;
        FileStream fileStream = new FileStream(actualFile, FileMode.OpenOrCreate);
        fileStream.Write(imageData, 0, imageData.Length);
        fileStream.Close();

        ImageUtilities.ScaleImage(actualFile, thumbnail, 150, 120);

        string mainImage = tempPath + "/" + actualFileName;
        string thumbImage = tempPath + "/" + thumbnailFileName;

        string query = "INSERT INTO AdvertisementImage (AdvertisementID, Title, MainImage, ThumbImage) Values({0}, '{1}', '{2}', '{3}'); Select SCOPE_IDENTITY();";
        query = String.Format(query, advID, picName, mainImage, thumbImage);
        object id = proxy.FillScalar(query);
    }
    public static string HandleRegister(HttpRequest request)
    {
        string fullName = Helper.GetString(request.QueryString["f"], null);
        string userName = Helper.GetString(request.QueryString["u"], null);
        string address = Helper.GetString(request.QueryString["a"], null);
        string mobile = Helper.GetString(request.QueryString["m"], null);
        int vendorID = Helper.GetInt(request.QueryString["vid"], Null.NullInteger);

        if (string.IsNullOrEmpty(userName))
            userName = "u" + mobile;
        if (string.IsNullOrEmpty(mobile))
            return "ERROR: لطفا شماره موبایل را وارد کنید";
        var stringWriter = new StringWriter();
        var jsonWriter = new Newtonsoft.Json.JsonTextWriter(stringWriter);
        var serializer = new Newtonsoft.Json.JsonSerializer();
        using (DbProxy proxy = new DbProxy())
        {
            UserInfo info = proxy.GetUser(userName);
            if (info != null)
            {
                if (info.Mobile != mobile)
                    return "ERROR: نام کاربری قبلا با یک شماره موبایل دیگر استفاده شده است";
            }
            info = proxy.GetUserByMobile(mobile);
            if (info != null)
            {
                if (info.Username != userName && (userName != "u" + mobile))
                    return "ERROR: شماره موبایل قبلا با یک نام کاربری دیگر استفاده شده است";
            }
            string password = GeneratePassword();
            if (info != null)
            {
                // dont set new password if the RegisterDate is new
                if (!Null.IsNull(info.RegisterDate) && info.RegisterDate.AddMinutes(10) > DateTime.Now)
                    password = info.Password;
                else
                {
                    info.Password = password;
                    proxy.UpdateUserPass(info.ID, info.Password);
                }
            }
            else
            {
                string query = string.Format(@"INSERT INTO Users (Username, Password, Fullname, Enabled, IsAdmin, Comments, PersonnelID, Mobile, RegisterDate) 
         VALUES ({0}, {1}, {2}, 1, 0, {3}, NULL, {4}, {5});
         SELECT SCOPE_IDENTITY();", Helper.QoutedString(userName), Helper.QoutedString(password), Helper.QoutedString(fullName), Helper.QoutedString(address)
                                  , Helper.QoutedString(mobile), Helper.QoutedDateTime(DateTime.Now));
                int userID = Helper.GetInt(proxy.FillScalar(query), Null.NullInteger);
                info = proxy.GetUser(userID);
            }
            jsonWriter.WriteStartArray();
            jsonWriter.WriteStartObject();
            jsonWriter.WritePropertyName("UserID");
            serializer.Serialize(jsonWriter, info.ID);
            jsonWriter.WritePropertyName("UserName");
            serializer.Serialize(jsonWriter, info.Username);
            jsonWriter.WritePropertyName("Password");
            serializer.Serialize(jsonWriter, password);
            jsonWriter.WriteEndObject();
            jsonWriter.WriteEndArray();

            string msg = "ایرامال_ کد فعالسازی: " + password;
            if (vendorID > 0)
            {
                UserInfo vendor = proxy.GetUser(vendorID);
                if (vendor != null)
                    msg = vendor.Fullname + "_ کد فعالسازی: " + password;
            }
            if (!SmsHelper.SendToIramal(fullName, mobile, msg, false, false))
                if (!SmsHelper.SendToIramal(fullName, mobile, msg, false, false))
                    return "ERROR: سیستم پیام کوتاه خارج از سرویس می باشد لطفا بعدا دوباره سعی کنید.";
        }

        return stringWriter.ToString();
    }
    public static string HandleLogin(HttpRequest request)
    {
        string mobile = Helper.GetString(request.QueryString["m"], null);
        int vendorID = Helper.GetInt(request.QueryString["vid"], Null.NullInteger);
        if (string.IsNullOrEmpty(mobile))
            return "ERROR: لطفا شماره موبایل را وارد کنید";
        var stringWriter = new StringWriter();
        var jsonWriter = new Newtonsoft.Json.JsonTextWriter(stringWriter);
        var serializer = new Newtonsoft.Json.JsonSerializer();
        using (DbProxy proxy = new DbProxy())
        {
            UserInfo info = proxy.GetUserByMobile(mobile);
            if (info == null)
            {
                return "ERROR: این شماره موبایل قبلا ثبت نشده است";
            }
            // dont set new password if the RegisterDate is new
            if (!Null.IsNull(info.RegisterDate) && info.RegisterDate.AddMinutes(10) > DateTime.Now)
            { }
            else
            {
                info.Password = GeneratePassword();
                proxy.UpdateUserPass(info.ID, info.Password);
            }

            jsonWriter.WriteStartArray();
            jsonWriter.WriteStartObject();
            jsonWriter.WritePropertyName("UserID");
            serializer.Serialize(jsonWriter, info.ID);
            jsonWriter.WritePropertyName("UserName");
            serializer.Serialize(jsonWriter, info.Username);
            jsonWriter.WritePropertyName("Password");
            serializer.Serialize(jsonWriter, info.Password);
            jsonWriter.WritePropertyName("Fullname");
            serializer.Serialize(jsonWriter, info.Fullname);
            jsonWriter.WritePropertyName("Address");
            serializer.Serialize(jsonWriter, info.Comments);
            jsonWriter.WriteEndObject();
            jsonWriter.WriteEndArray();

            string msg = "ایرامال_ کد فعالسازی: " + info.Password;
            if (vendorID > 0)
            {
                UserInfo vendor = proxy.GetUser(vendorID);
                if (vendor != null)
                    msg = vendor.Fullname + "_ کد فعالسازی: " + info.Password;
            }
            if (!SmsHelper.SendToIramal(info.Fullname, mobile, msg, false, false))
                if (!SmsHelper.SendToIramal(info.Fullname, mobile, msg, false, false))
                    return "ERROR: سیستم پیام کوتاه خارج از سرویس می باشد لطفا بعدا دوباره سعی کنید.";
        }

        return stringWriter.ToString();
    }
    public static string HandleCheckPass(HttpRequest request)
    {
        int userID = Helper.GetInt(request.QueryString["uid"], Null.NullInteger);
        string pass = Helper.GetString(request.QueryString["p"], null);
        using (DbProxy proxy = new DbProxy())
        {
            UserInfo info = proxy.GetUser(userID);
            if (info == null)
                return "ERROR: Invalid user";
            if (info.Password != pass)
                return "ERROR: Invalid code";
        }
        return "OK";
    }
    public static string HandleAddBasket(HttpRequest request)
    {
        string data = request.Form["data"];
        int len = (int)request.InputStream.Length;
        if (len > 0 && string.IsNullOrEmpty(data))
        {
            byte[] bytes = new byte[len];
            request.InputStream.Read(bytes, 0, bytes.Length);
            string content = new System.Text.UTF8Encoding().GetString(bytes);
            if (content.Contains("data="))
                data = System.Web.HttpUtility.UrlDecode(content.Split('=')[1]);
        }
        if (string.IsNullOrEmpty(data))
            return "ERROR: empty basket";
        using (DbProxy proxy = new DbProxy())
        {
            try
            {
                List<BasketInfo> baskets = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BasketInfo>>(data);
                BasketInfo basket = baskets[0];
                UserInfo user = proxy.GetUser(basket.UserID);
                if (user == null)
                    return "ERROR: invalid user";
                if (basket.Items.Count == 0)
                    return "ERROR: invalid user";

                // check order times
                string orderWaitTime = basket.VendorID <= 0 ? proxy.GetConfig("AppOrderWaitTime", "", 0) : proxy.GetUserConfig("AppOrderWaitTime", "", basket.VendorID);
                if (!string.IsNullOrEmpty(orderWaitTime))
                {
                    DateTime orderDate = Helper.ParseFaDateTime(basket.DeliveryTime, DateTime.Now);
                    int minutes = GetMinutes(orderWaitTime);
                    //int orderTime = GetMinutes(basket.DeliveryTime);
                    //orderDate = orderDate.AddMinutes(orderTime);
                    //int now = DateTime.Now.Hour * 60 + DateTime.Now.Minute;
                    if (DateTime.Now.AddMinutes(minutes) > orderDate)
                        return "ERROR: حداقل زمان سفارش " + orderWaitTime + " بعد از زمان جاری می باشد.";
                }
                string orderStartTime = basket.VendorID <= 0 ? proxy.GetConfig("AppOrderStartTime", "", 0) : proxy.GetUserConfig("AppOrderStartTime", "", basket.VendorID);
                string orderEndTime = basket.VendorID <= 0 ? proxy.GetConfig("AppOrderEndTime", "", 0) : proxy.GetUserConfig("AppOrderEndTime", "", basket.VendorID);
                if (!string.IsNullOrEmpty(orderStartTime) && !string.IsNullOrEmpty(orderEndTime))
                {
                    int minutes = GetMinutes(basket.DeliveryTime);
                    int startMinutes = GetMinutes(orderStartTime);
                    int endMinutes = GetMinutes(orderEndTime);
                    if (minutes < startMinutes || minutes > endMinutes)
                        return string.Format("ERROR: زمان سفارش باید بین  {0} و {1} باشد", orderStartTime, orderEndTime);
                }

                basket.BasketDate = DateTime.Now;
                basket.Status = 1;
                if (string.IsNullOrEmpty(basket.Comments))
                    basket.Comments = user.Username + " " + user.Mobile;
                else
                    basket.Comments += "\n" + user.Username + " " + user.Mobile;
                basket.TotalNum = basket.Items.Sum(item => item.Num);
                basket.TotalPrice = basket.Items.Sum(item => item.Num * item.Price);
                int minCopouns = Helper.GetInt(proxy.GetUserConfig("AppMinCopouns", "", basket.VendorID), 0);
                string query = string.Format(@"DECLARE @DiscountCardPrice INT, @DiscountIn NUMERIC(18, 2), @DiscountOut NUMERIC(18, 2), @RemainDiscount INT, @NumCoupons INT;
SET @DiscountCardPrice = dbo.ToInt( dbo.GetSetting('DiscountCardPrice'), 0);
SET @DiscountIn = 0;
SET @DiscountOut = 0;
SELECT @DiscountIn = SUM(ISNULL(DiscountCard.Num, 0)) FROM DiscountCard WHERE DiscountCard.UserID = {0}; -- AND DiscountCard.VendorID = {8};
SELECT @DiscountOut = SUM(ISNULL(Basket.NumCoupons, 0)) FROM Basket WHERE Basket.UserID = {0} AND Status <> 3 AND PayStatus = 3; -- Basket.VendorID = {8} ;  --Status=3 = Canceled, PayStatus=3 Paid
SET @NumCoupons = 0;
SET @RemainDiscount = @DiscountIn - @DiscountOut;
IF (@RemainDiscount * @DiscountCardPrice) >= minCopouns
    SET @NumCoupons = @RemainDiscount;
WHILE @NumCoupons >= 0 AND (@NumCoupons * @DiscountCardPrice) > {5}
    SET @NumCoupons = @NumCoupons - 1;
IF @NumCoupons < 0 SET @NumCoupons = 0

INSERT INTO Basket (UserID, Status, BasketDate, Code, Comments, TotalNum, TotalPrice, DeliveryAddress, DeliveryTime, VendorID, SendOption, PayStatus, PayMethod, TempNumCoupons, TempCouponPrice, ShipmentPrice) 
                     VALUES ({0}, {1}, {2}, [dbo].[NeuBasketNo](NULL), {3}, {4}, {5}, {6}, {7}, {8}, {9}, 1, 3, @NumCoupons, @NumCoupons * @DiscountCardPrice, {10});
                     SELECT SCOPE_IDENTITY();".Replace("minCopouns", minCopouns.ToString())
                    , basket.UserID, basket.Status, Helper.QoutedDateTime(basket.BasketDate), Helper.QoutedString(basket.Comments), basket.TotalNum, basket.TotalPrice
                    , Helper.QoutedString(basket.DeliveryAddress), Helper.QoutedString(basket.DeliveryTime)
                    , basket.VendorID > 0 ? basket.VendorID.ToString() : "NULL", Helper.QoutedString(basket.SendOption)
                    , basket.ShipmentPrice);

                proxy.BeginTransaction();
                basket.ID = Helper.GetInt(proxy.FillScalar(query), Null.NullInteger);
                if (basket.ID == Null.NullInteger)
                    return "ERROR: unable to insert basket";
                for (int i = 0; i < basket.Items.Count; i++)
                {
                    query = string.Format(@"INSERT INTO BasketItem (BasketID, ServicePriceID, Num, Price) VALUES ({0}, {1}, {2}, {3});"
                        , basket.ID, basket.Items[i].ServicePriceID, basket.Items[i].Num, basket.Items[i].Price);
                    proxy.RunQuery(query);
                }
                string code = proxy.FillScalarString("SELECT Code FROM Basket WHERE ID=" + basket.ID);
                string couponPrice = proxy.FillScalarString("SELECT TempCouponPrice FROM Basket WHERE ID=" + basket.ID);
                proxy.CommitTransaction();

                /*
                // send SMS to vendor
                try
                {
                    UserInfo vendor = proxy.GetUser(basket.VendorID);
                    if (vendor != null && !string.IsNullOrEmpty(vendor.Mobile))
                        SmsHelper.SendToIramal(vendor.Username, vendor.Mobile, "ایرامال: شما یک سفارش جدید دارید.");
                }
                catch (Exception ex1)
                {
                    string fuckTurkey = ex1.Message;
                }
                */
                return code + "," + ((int)Helper.GetDouble(couponPrice, 0)).ToString();

            }
            catch (Exception ex)
            {
                proxy.RollbackTransaction();
                return "ERROR: " + ex.Message;
            }
        }
    }
    public static string HandleSendBasketSMS(HttpRequest request)
    {
        string basketCode = Helper.GetString(request.QueryString["code"], "");
        using (DbProxy proxy = new DbProxy())
        {
            try
            {
                // send SMS to vendor
                try
                {
                    int vendorID = Helper.GetInt(proxy.FillScalar("SELECT TOP 1 VendorID FROM Basket WHERE Code=" + Helper.QoutedString(basketCode)), -1);
                    UserInfo vendor = proxy.GetUser(vendorID);
                    if (vendor != null && !string.IsNullOrEmpty(vendor.Mobile))
                        SmsHelper.SendToIramal(vendor.Username, vendor.Mobile, "ایرامال: شما یک سفارش جدید پرداخت حضوری دارید.", true, false);
                }
                catch (Exception ex1)
                {
                    string fuckTurkey = ex1.Message;
                }
                return "1";
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.Message;
            }
        }
    }
    public static string HandleBaskets(HttpRequest request)
    {
        var stringWriter = new StringWriter();
        var jsonWriter = new Newtonsoft.Json.JsonTextWriter(stringWriter);
        var serializer = new Newtonsoft.Json.JsonSerializer();
        using (DbProxy proxy = new DbProxy())
        {
            int userID = Helper.GetInt(request.QueryString["uid"], Null.NullInteger);
            int vendorID = Helper.GetInt(request.QueryString["vid"], Null.NullInteger);
            string query = "SELECT ID, VendorID, Status, BasketDate, Code, Comments, TotalNum, TotalPrice, PayStatus, PayMethod, CouponPrice FROM Basket WHERE 1=1";
            //if (userID > 0)
            query += " AND UserID =" + userID;
            if (vendorID > 0)
                query += " AND VendorID =" + vendorID;
            query += " ORDER BY BasketDate DESC";
            List<BasketInfo> res = new List<BasketInfo>();
            IDataReader reader = proxy.ExecuteReader(query);
            while (reader.Read())
            {
                BasketInfo info = new BasketInfo();
                info.ID = reader.GetInt32(0);
                info.VendorID = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);
                info.Status = reader.IsDBNull(2) ? 1 : reader.GetInt32(2);
                info.BasketDate = reader.IsDBNull(3) ? DateTime.Now : reader.GetDateTime(3);
                info.Code = reader.IsDBNull(4) ? "" : reader.GetString(4);
                info.Comments = reader.IsDBNull(5) ? "" : reader.GetString(5);
                info.TotalNum = reader.IsDBNull(6) ? 0 : (float)reader.GetDouble(6);
                info.TotalPrice = reader.IsDBNull(7) ? 0 : (double)reader.GetDecimal(7);
                info.PayStatus = reader.IsDBNull(8) ? 1 : reader.GetInt32(8);
                info.PayMethod = reader.IsDBNull(9) ? 1 : reader.GetInt32(9);
                info.CouponPrice = reader.IsDBNull(10) ? 0 : (double)reader.GetDecimal(10);
                res.Add(info);
            }
            reader.Close();
            serializer.Serialize(jsonWriter, res);
        }

        return stringWriter.ToString();
    }
    public static string HandleBasket(HttpRequest request)
    {
        var stringWriter = new StringWriter();
        var jsonWriter = new Newtonsoft.Json.JsonTextWriter(stringWriter);
        var serializer = new Newtonsoft.Json.JsonSerializer();
        using (DbProxy proxy = new DbProxy())
        {
            int basketID = Helper.GetInt(request.QueryString["bid"], Null.NullInteger);
            string query = @"
SELECT ServicePriceView.ID, ISNULL(ServiceView.Code, ISNULL(ServicePriceView.SCoding ,'' )) Coding, ServiceName, ServiceFullName
, UnitName, ServiceView.Comments
, BasketItem.Num, BasketItem.Price
, ServicePriceView.ServiceID
, [dbo].[GetGroupingFullTitle](ServicePriceView.ID, 1) GroupingFullTitle
, ServicePriceView.DefaultDiscount, [dbo].[FindVendor](ServicePriceView.ID) VendorID
FROM ServicePriceView
INNER JOIN BasketItem ON ServicePriceView.ID = BasketItem.ServicePriceID
INNER JOIN ServiceView ON ServiceView.ID=ServicePriceView.ServiceID 
WHERE BasketItem.BasketID = " + basketID;

            List<ServicePriceInfo> res = new List<ServicePriceInfo>();
            Dictionary<int, ServicePriceInfo> dict = new Dictionary<int, ServicePriceInfo>();
            IDataReader reader = proxy.ExecuteReader(query);
            while (reader.Read())
            {
                ServicePriceInfo info = new ServicePriceInfo();
                int serviceID = reader.GetInt32(8);
                info.ID = reader.GetInt32(0);
                info.Code = reader.IsDBNull(1) ? "" : reader.GetString(1);
                info.Title = reader.IsDBNull(2) ? "" : reader.GetString(2); // 2 or 3
                info.UnitName = reader.IsDBNull(4) ? "" : reader.GetString(4);
                info.Comments = reader.IsDBNull(5) ? "" : reader.GetString(5);
                info.Available = reader.IsDBNull(6) ? 0 : (int)reader.GetDouble(6);
                info.Price = reader.IsDBNull(7) ? 0 : reader.GetDecimal(7);
                info.GroupTitle = reader.IsDBNull(9) ? "" : reader.GetString(9);
                info.NumCoupons = reader.IsDBNull(10) ? 0 : reader.GetDecimal(10);
                info.VendorID = reader.IsDBNull(11) ? 0 : reader.GetInt32(11);
                info.Images = new List<string>();
                //info.Thumbnails = new List<string>();
                res.Add(info);
                if (!dict.ContainsKey(serviceID))
                    dict.Add(serviceID, info);
            }
            reader.Close();

            // get images
            if (dict.Count > 0)
            {
                string ids = string.Join(",", dict.Keys);
                query = string.Format(@"SELECT ServiceID, ThumbImage, MainImage FROM ServiceImage WHERE ServiceID IN ({0})", ids);
                reader = proxy.ExecuteReader(query);
                while (reader.Read())
                {
                    int serviceID = reader.GetInt32(0);
                    ServicePriceInfo info = dict[serviceID];
                    if (!reader.IsDBNull(2))
                        info.Images.Add(reader.GetString(2));
                    //if (!reader.IsDBNull(1))
                    //    info.Thumbnails.Add(reader.GetString(1));
                }
                reader.Close();
            }
            serializer.Serialize(jsonWriter, res);
        }

        return stringWriter.ToString();
    }
    public static string HandleMessages(HttpRequest request)
    {
        var stringWriter = new StringWriter();
        var jsonWriter = new Newtonsoft.Json.JsonTextWriter(stringWriter);
        var serializer = new Newtonsoft.Json.JsonSerializer();
        using (DbProxy proxy = new DbProxy())
        {
            int vendorID = Helper.GetInt(request.QueryString["vid"], Null.NullInteger);
            int userID = Helper.GetInt(request.QueryString["uid"], Null.NullInteger);
            bool all = Helper.GetInt(request.QueryString["all"], Null.NullInteger) == 1;
            string query = @"SELECT VendorsMessages.ID, VendorsMessages.VendorID, VendorsMessages.Msg, VendorsMessages.Date, VendorsMessages.Seen
, [Grouping].Title FROM VendorsMessages 
LEFT JOIN Users ON VendorsMessages.VendorID = Users.ID
LEFT JOIN [Grouping] ON [Grouping].ID = Users.RelatedGroupID
WHERE VendorsMessages.Msg IS NOT NULL";
            if (!all)
                query += " AND (Seen IS NULL OR Seen = 0)";
            if (vendorID > 0)
                query += " AND VendorsMessages.VendorID = " + vendorID;
            if (userID > 0)
                query += " AND VendorsMessages.UserID = " + userID;
            query += " ORDER BY VendorsMessages.Date DESC";

            jsonWriter.WriteStartArray();
            IDataReader reader = proxy.ExecuteReader(query);
            while (reader.Read())
            {
                jsonWriter.WriteStartObject();
                jsonWriter.WritePropertyName("ID");
                serializer.Serialize(jsonWriter, reader.GetInt32(0));
                jsonWriter.WritePropertyName("VendorID");
                serializer.Serialize(jsonWriter, reader.IsDBNull(1) ? 0 : reader.GetInt32(1));
                jsonWriter.WritePropertyName("Msg");
                serializer.Serialize(jsonWriter, reader.GetString(2));
                jsonWriter.WritePropertyName("Date");
                serializer.Serialize(jsonWriter, reader.IsDBNull(3) ? "" : Helper.FormatDate(reader.GetDateTime(3), "yyyy-MM-dd HH:mm"));

                jsonWriter.WritePropertyName("Seen");
                serializer.Serialize(jsonWriter, reader.IsDBNull(4) ? 0 : (reader.GetBoolean(4) ? 1 : 0));
                jsonWriter.WritePropertyName("Vendor");
                serializer.Serialize(jsonWriter, reader.IsDBNull(5) ? "" : reader.GetString(5));
                jsonWriter.WriteEndObject();
            }
            reader.Close();
            jsonWriter.WriteEndArray();
        }

        return stringWriter.ToString();
    }
    public static string HandleMarkMessages(HttpRequest request)
    {
        string ids = Helper.GetString(request.QueryString["ids"], null);
        string query = string.Format("UPDATE VendorsMessages SET Seen=1 WHERE ID IN ({0})", ids);
        if (DbProxy.Instance.RunQuery(query) > 0)
            return "OK";
        return "ERROR:error";
    }
    private static string GeneratePassword()
    {
        string res = "";
        Random rnd = new Random();
        for (int i = 0; i < 5; i++)
        {
            if (i > 0)
                res += rnd.Next(0, 10);
            else
                res += rnd.Next(1, 10);
        }
        return res;
    }
    // 1:20 --> 80,  30 --> 30
    private static int GetMinutes(string time)
    {
        if (time.Contains(' '))
            time = time.Split(' ')[1]; // date-time --> time
        string[] hm = time.Split(':');
        int minutes = 0;
        if (hm.Length == 1)
            minutes = Helper.GetInt(hm[0], 0);
        else if (hm.Length == 2)
            minutes = Helper.GetInt(hm[0], 0) * 60 + Helper.GetInt(hm[1], 0);
        return minutes;
    }
    public static string HandleCustomerReport(HttpRequest request)
    {
        var stringWriter = new StringWriter();
        var jsonWriter = new Newtonsoft.Json.JsonTextWriter(stringWriter);
        var serializer = new Newtonsoft.Json.JsonSerializer();
        using (DbProxy proxy = new DbProxy())
        {
            int personID = Helper.GetInt(request.QueryString["pid"], Null.NullInteger);
            string query = string.Format(@"
SELECT DocumentAccountingItems.ID, DocumentAccountingItems.Due, DocumentAccountingItems.Owe, DocumentAccounting.Date 
	, (SELECT TOP 1 StoreDraft.NOSerial FROM StoreDraft WHERE StoreDraft.ID = DocumentAccountingItems.OtherMasterID AND DocumentAccounting.Mode = 7) StoreDraftNo
	, Checks.No ChecksNo
	, (SELECT TOP 1 Checks.No FROM Checks WHERE Checks.ID = Documentaccounting.OtherID AND DocumentAccounting.Mode = 3) CheckNo
FROM DocumentAccountingItems 
	INNER JOIN DocumentAccounting ON DocumentAccountingItems.DocumentAccountingID = DocumentAccounting.ID
	LEFT JOIN Checks ON DocumentAccountingItems.CheckID = Checks.ID
WHERE DocumentAccountingItems.PersonnelID = {0} 
	AND DocumentAccountingItems.TreePath IN (SELECT TreePath FROM Trees WHERE PersonNeed=1)
ORDER BY DocumentAccounting.Date ASC
", personID);
            List<CustomrReportInfo> res = new List<CustomrReportInfo>();
            IDataReader reader = proxy.ExecuteReader(query);
            while (reader.Read())
            {
                CustomrReportInfo info = new CustomrReportInfo();
                info.ID = reader.GetInt32(0);
                info.Due = reader.IsDBNull(1) ? 0 : reader.GetDecimal(1);
                info.Owe = reader.IsDBNull(2) ? 0 : reader.GetDecimal(2);
                info.Date = reader.IsDBNull(3) ? "" : reader.GetDateTime(3).ToString("yyyy-MM-dd HH:mm:ss.fff");
                info.StoreDraftNo = reader.IsDBNull(4) ? "" : reader.GetString(4);
                info.CheckNo = reader.IsDBNull(5) ? (reader.IsDBNull(6) ? "" : reader.GetString(6)) : reader.GetString(5);
                res.Add(info);
            }
            reader.Close();

            serializer.Serialize(jsonWriter, res);
        }

        return stringWriter.ToString();
    }
    public static string HandleStoreDraft(HttpRequest request)
    {
        // DocumentAccountingItems.ID
        int itemID = Helper.GetInt(request.QueryString["itemid"], Null.NullInteger);
        StringBuilder builder = new StringBuilder();
        builder.AppendLine("<html lang='fa' dir='rtl'>");
        builder.AppendLine("<link href='styles/tablet.css' rel='stylesheet' type='text/css' />");
        builder.AppendLine("<body><div style='direction:rtl'>");
        using (DbProxy proxy = new DbProxy())
        {
            string query = "SELECT DocumentAccountingItems.OtherMasterID FROM DocumentAccountingItems WHERE DocumentAccountingItems.ID = " + itemID;
            int id = Helper.GetInt(proxy.FillScalar(query), Null.NullInteger);
            DataRow row = proxy.FillRow("SELECT * FROM StoreDraft WHERE ID = " + id);
            if (row == null)
                builder.Append("INVALID DATA");
            else
            {
                builder.Append("<table cellspacing='0' cellpadding='4' border='1' dir='rtl'>");
                builder.AppendFormat("<tr><td>ش فاکتور:</td><td>{0}</td><td>جمع:</td><td>{1}</td><td colspan='2'>{2}</td></tr>"
                    , Helper.GetString(row, "NOSerial", "")
                    , Helper.GetDecimal(row, "TotalPrice", 0).ToString("N2", Helper.NumberCulture).Replace(".00", "")
                    , Helper.FormatDate(Helper.GetDateTime(row, "FactorDate", Null.NullDate), "yyyy-MM-dd"));
                builder.AppendFormat("<tr><td>عوارض</td><td>{0}</td><td>مالیات:</td><td>{1}</td><td>تخفیف</td><td>{2}</td></tr>", Helper.GetDecimal(row, "VAT", 0).ToString("N2", Helper.NumberCulture).Replace(".00", ""), Helper.GetDecimal(row, "TotalTax", 0).ToString("N2", Helper.NumberCulture).Replace(".00", ""), Helper.GetDecimal(row, "TotalDiscount", 0).ToString("N2", Helper.NumberCulture).Replace(".00", ""));
                builder.Append("</table>");
            }
            DataTable dt = proxy.FillData("SELECT * FROM StoreDraftItemsView WHERE StoreDraftID = " + id, "view");
            builder.Append("<table cellspacing='0' cellpadding='4' border='1' dir='rtl'>");
            builder.Append("<tr><td>ردیف</td><td>نام کالا</td><td>مقدار</td><td>واحد</td></tr>");
            if (dt != null)
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    builder.AppendFormat("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>"
                        , i + 1
                        , Helper.GetString(dt.Rows[i], "StoreServiceName", "")
                        , Helper.GetDouble(dt.Rows[i], "DeliveredAmount", 0).ToString("N2", Helper.NumberCulture).Replace(".00", "")
                        , Helper.GetString(dt.Rows[i], "UnitName", ""));

                }
            builder.Append("</table>");
        }
        builder.AppendLine("</div></body>");

        return builder.ToString();
    }
    public static string HandleCheck(HttpRequest request)
    {
        // DocumentAccountingItems.ID
        int itemID = Helper.GetInt(request.QueryString["itemid"], Null.NullInteger);
        StringBuilder builder = new StringBuilder();
        builder.AppendLine("<html lang='fa' dir='rtl'>");
        builder.AppendLine("<link href='styles/tablet.css' rel='stylesheet' type='text/css' />");
        builder.AppendLine("<body><div style='direction:rtl'>");
        using (DbProxy proxy = new DbProxy())
        {
            string query = "SELECT DocumentAccountingItems.CheckID FROM DocumentAccountingItems WHERE DocumentAccountingItems.ID = " + itemID;
            int id = Helper.GetInt(proxy.FillScalar(query), Null.NullInteger);
            if (id == Null.NullInteger)
            {
                query = "SELECT Documentaccounting.OtherID FROM DocumentAccountingItems INNER JOIN DocumentAccounting ON DocumentAccountingItems.DocumentAccountingID = DocumentAccounting.ID WHERE DocumentAccounting.Mode = 3 AND DocumentAccountingItems.ID = " + itemID;
                id = Helper.GetInt(proxy.FillScalar(query), Null.NullInteger);
            }

            DataRow row = proxy.FillRow("SELECT * FROM CheckView WHERE ID = " + id);
            if (row == null)
                builder.Append("INVALID DATA");
            else
            {
                builder.Append("<table cellspacing='0' cellpadding='4' border='1' dir='rtl'>");
                builder.AppendFormat("<tr><td>سریال چک:</td><td>{0}</td><td>مبلغ:</td><td>{1}</td></tr>"
                    , Helper.GetString(row, "No", "")
                    , Helper.GetDecimal(row, "Amount", 0).ToString("N2", Helper.NumberCulture).Replace(".00", ""));
                builder.AppendFormat("<tr><td>بانک:</td><td>{0}</td><td>شعبه:</td><td>{1}</td></tr>"
                    , Helper.GetString(row, "BankAccountName", "")
                    , Helper.GetString(row, "Branch", ""));
                builder.AppendFormat("<tr><td>صاحب چک:</td><td>{0}</td><td>واگذار کننده:</td><td>{1}</td></tr>"
                    , Helper.GetString(row, "PersonnelName", "")
                    , Helper.GetString(row, "AssignorName", ""));
                builder.AppendFormat("<tr><td>صدور:</td><td>{0}</td><td>سررسید:</td><td>{1}</td></tr>"
                    , Helper.FormatDate(Helper.GetDateTime(row, "IssueDate", Null.NullDate), "yyyy-MM-dd")
                    , Helper.FormatDate(Helper.GetDateTime(row, "DueDate", Null.NullDate), "yyyy-MM-dd"));
                builder.AppendFormat("<tr><td colspan=2>شماره حساب:</td><td colspan=2>{0}</td></tr>"
                    , Helper.GetString(row, "AccountNo", ""));
                builder.AppendFormat("<tr><td colspan=2>توصیحات:</td><td colspan=2>{0}</td></tr>"
                    , Helper.GetString(row, "Comments", ""));
                builder.Append("</table>");
            }
        }
        builder.AppendLine("</div></body>");

        return builder.ToString();
    }
    public static string HandleVisitorReport(HttpRequest request)
    {
        int visitorID = Helper.GetInt(request.QueryString["id"], Null.NullInteger);
        StringBuilder builder = new StringBuilder();
        builder.AppendLine("<html lang='fa' dir='rtl'>");
        builder.AppendLine("<link href='styles/tablet.css' rel='stylesheet' type='text/css' />");
        builder.AppendLine("<body style='background-color:#424242;color:#fff;'><div style='direction:rtl'>");
        using (DbProxy proxy = new DbProxy())
        {
            decimal sumSale = Helper.GetDecimal(proxy.FillScalar("SELECT SUM(ISNULL(StoreDraft.TotalPrice, 0)) FROM StoreDraft WHERE StoreDraft.BuyerID=Personnel.ID AND WorkflowStatus=1 AND VisitorID=" + visitorID), 0);
            // Checks.Statu     0:New;1:Received;2:Refused;3:Sleep;4:Expend;5:SleepReceive;6:Visitor;7:Guarantee;9:GiveBack"/>
            decimal sumChecks = Helper.GetDecimal(proxy.FillScalar(" SELECT SUM(ISNULL(CONVERT(numeric(18,0), Checks.Amount), 0)) FROM Checks WHERE VisitorID=" + visitorID), 0);
            decimal sumPassed = Helper.GetDecimal(proxy.FillScalar(" SELECT SUM(ISNULL(CONVERT(numeric(18,0), Checks.Amount), 0)) FROM Checks WHERE Status IN (1, 4, 5) AND VisitorID=" + visitorID), 0);
            decimal sumReturened = Helper.GetDecimal(proxy.FillScalar(" SELECT SUM(ISNULL(CONVERT(numeric(18,0), Checks.Amount), 0)) FROM Checks WHERE Status IN (2, 9) AND VisitorID=" + visitorID), 0);
            decimal sumCurrent = Helper.GetDecimal(proxy.FillScalar(" SELECT SUM(ISNULL(CONVERT(numeric(18,0), Checks.Amount), 0)) FROM Checks WHERE Status IN (0, 3, 6, 7) AND VisitorID=" + visitorID), 0);

            builder.Append("<table cellspacing='0' cellpadding='4' border='1' dir='rtl'>");
            builder.AppendFormat("<tr><td>جمع کل فروش:</td><td>{0}</td></tr>", sumSale.ToString("N2", Helper.NumberCulture).Replace(".00", ""));
            builder.AppendFormat("<tr><td>جمع کل چک ها:</td><td>{0}</td></tr>", sumChecks.ToString("N2", Helper.NumberCulture).Replace(".00", ""));
            builder.AppendFormat("<tr><td>چک های پاس شده:</td><td>{0}</td></tr>", sumPassed.ToString("N2", Helper.NumberCulture).Replace(".00", ""));
            builder.AppendFormat("<tr><td>چک ها ی برگشتی:</td><td>{0}</td></tr>", sumReturened.ToString("N2", Helper.NumberCulture).Replace(".00", ""));
            builder.AppendFormat("<tr><td>چک های پاس نشده:</td><td>{0}</td></tr>", sumCurrent.ToString("N2", Helper.NumberCulture).Replace(".00", ""));
            builder.Append("</table>"); 
        }
        builder.AppendLine("</div></body>");

        return builder.ToString();
    }
}