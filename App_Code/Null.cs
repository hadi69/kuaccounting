﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Reflection;

/// <summary>
/// Class for dealing with the translation of database null values.
/// </summary>
public class Null
{
    // define application encoded null values 
    public static short NullShort
    {
        get
        {
            return -1;
        }
    }
    public static int NullInteger
    {
        get
        {
            return -1;
        }
    }
    public static Single NullSingle
    {
        get
        {
            return Single.MinValue;
        }
    }
    public static double NullDouble
    {
        get
        {
            return Double.MinValue;
        }
    }
    public static decimal NullDecimal
    {
        get
        {
            return 0;
        }
    }
    public static DateTime NullDate
    {
        get
        {
            return DateTime.MinValue;
        }
    }
    public static string NullString
    {
        get
        {
            return "";
        }
    }
    public static bool NullBoolean
    {
        get
        {
            return false;
        }
    }
    public static Guid NullGuid
    {
        get
        {
            return Guid.Empty;
        }
    }

    /// <summary>
    /// sets a field to an application encoded null value ( used in BLL layer )
    /// </summary>
    /// <param name="value"></param>
    /// <param name="field"></param>
    /// <returns></returns>
    public static object SetNull(object value, object field)
    {
        if (value is DBNull || value == null)
        {
            if (field is short)
                return NullShort;
            else if (field is int)
                return NullInteger;
            else if (field is Single)
                return NullSingle;
            else if (field is double)
                return NullDouble;
            else if (field is decimal)
                return NullDecimal;
            else if (field is DateTime)
                return NullDate;
            else if (field is string)
                return NullString;
            else if (field is bool)
                return NullBoolean;
            else if (field is Guid)
                return NullGuid;
            else // complex object
                return null;
        }
        else    // return value
            return value;
    }

    /// <summary>
    /// sets a field to an application encoded null value ( used in BLL layer )
    /// </summary>
    /// <param name="propertyInfo"></param>
    /// <returns></returns>
    public static object SetNull(PropertyInfo propertyInfo)
    {
        switch (propertyInfo.PropertyType.ToString())
        {
            case "System.Int16":
                return NullShort;
            case "System.Int32":
            case "System.Int64":
                return NullInteger;
            case "System.Single":
                return NullSingle;
            case "System.Double":
                return NullDouble;
            case "System.Decimal":
                return NullDecimal;
            case "System.DateTime":
                return NullDate;
            case "System.String":
            case "System.Char":
                return NullString;
            case "System.Boolean":
                return NullBoolean;
            case "System.Guid":
                return NullGuid;
            case "System.Object":
                return null;
            default:
                // Enumerations default to the first entry
                if (propertyInfo.PropertyType.BaseType.Equals(typeof(System.Enum)))
                {
                    Array enumValues = System.Enum.GetValues(propertyInfo.PropertyType);
                    Array.Sort(enumValues);
                    return System.Enum.ToObject(propertyInfo.PropertyType, enumValues.GetValue(0));
                }
                else // complex object
                    return null;
        }
    }

    /// <summary>
    /// convert an application encoded null value to a database null value ( used in DAL )
    /// </summary>
    /// <param name="field"></param>
    /// <param name="dbNull"></param>
    /// <returns></returns>
    public static object GetNull(object field, object dbNull)
    {
        if (field == null)
            return dbNull;
        else if (field is short)
        {
            if (Convert.ToInt16(field) == NullShort)
                return dbNull;
        }
        else if (field is int)
        {
            if (Convert.ToInt32(field) == NullInteger)
                return dbNull;
        }
        else if (field is Single)
        {
            if (Convert.ToSingle(field) == NullSingle)
                return dbNull;
        }
        else if (field is Double)
        {
            if (Convert.ToDouble(field) == NullDouble)
                return dbNull;
        }
        else if (field is Decimal)
        {
            if (Convert.ToDecimal(field) == NullDecimal)
                return dbNull;
        }
        else if (field is DateTime)
        {
            // compare the Date part of the DateTime with the DatePart of the NullDate ( this avoids subtle time differences )
            if (Convert.ToDateTime(field).Date == NullDate.Date)
                return DBNull.Value;
        }
        else if (field is string)
        {
            if (field == null || field.ToString() == NullString)
                return dbNull;
        }
        else if (field is bool)
        {
            if (Convert.ToBoolean(field) == NullBoolean)
                return dbNull;
        }
        else if (field is Guid)
        {
            if (((System.Guid)field).Equals(NullGuid))
                return dbNull;
        }
        return field;
    }

    // checks if a field contains an application encoded null value
    public static bool IsNull(object field)
    {
        if (field == null || field is DBNull)
            return true;
        if (field is int)
            return field.Equals(NullInteger);
        else if (field is Single)
            return field.Equals(NullSingle);
        else if (field is Double)
            return field.Equals(NullDouble);
        else if (field is Decimal)
            return field.Equals(NullDecimal);
        else if (field is DateTime)
            return ((DateTime)field).Date.Equals(NullDate.Date);
        else if (field is String)
            return field.Equals(NullString);
        else if (field is Boolean)
            return field.Equals(NullBoolean);
        else if (field is Guid)
            return field.Equals(NullGuid);
        else // complex object
            return false;
    }
}
