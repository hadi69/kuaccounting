﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using Stimulsoft.Report;

/// <summary>
/// Summary description for SiteSettings
/// </summary>
public class SiteSettings
{
    static string mBaseUrl = null;
    static bool? mDevelopeMode = null;
    static UserInfo mUser = null;
    public static int RowsPerPage = 25;
    public static bool RepeatHeaderRow = true;
    private static bool? mEnableSync = null;
    public static bool EnableSync
    {
        get
        {
            if (mEnableSync == null)
                mEnableSync = Helper.GetBool(System.Configuration.ConfigurationManager.AppSettings["EnableSync"], false);
            return mEnableSync.Value;
        }
    }
    public static int UserID
    {
        get
        {
            if (System.Web.HttpContext.Current.Session == null)
                return -1;
            return Helper.GetInt(System.Web.HttpContext.Current.Session["UserID"], -1);
        }
        set
        {
            if (System.Web.HttpContext.Current.Session == null)
                return;
            System.Web.HttpContext.Current.Session["UserID"] = value;
            mUser = null;

            //  fill other props
            Invalidate();
        }
    }
    public static UserInfo User
    {
        get
        {
            int id = UserID;
            if (id == -1)
                return null;
            if (mUser != null && mUser.ID == id)
                return mUser;
            using (DbProxy proxy = new DbProxy())
                mUser = proxy.GetUser(id);
            return mUser;
        }
    }
    public static string BaseUrl
    {
        get
        {
            if (mBaseUrl == null)
                mBaseUrl = System.Configuration.ConfigurationManager.AppSettings["BaseUrl"];
            return mBaseUrl;
        }
    }
    public static string ConnectionString
    {
        get
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        }
        set
        {
            System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString = value;
        }
    }
    //static string ConfigConnectionString
    //{
    //    get
    //    {
    //        return System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
    //    }
    //    set
    //    {
    //        System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString = value;
    //    }
    //}
    public static bool DevelopeMode
    {
        get
        {
            //return false;
            if (mDevelopeMode == null)
                mDevelopeMode = Helper.GetBool(System.Configuration.ConfigurationManager.AppSettings["DevelopeMode"], false);
            return mDevelopeMode.Value;
        }
    }
    public static DateType DateType
    {
        get
        {
            if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Application == null)
                return Helper.GetEnum(DbProxy.Instance.GetConfig("DateType", "fa"), DateType.fa);
            if (null == System.Web.HttpContext.Current.Application["DateType"])
                lock (System.Web.HttpContext.Current.Application)
                    SetApplication("DateType", Helper.GetEnum(DbProxy.Instance.GetConfig("DateType", "fa"), DateType.fa));
            return (DateType)GetApplication("DateType");
        }
    }
    public static string Lang
    {
        get
        {
            if (System.Web.HttpContext.Current.Session == null)
                return DbProxy.Instance.GetConfig("Language", "fa");
            string res = Helper.GetString(System.Web.HttpContext.Current.Session["Lang"], null);
            if (!string.IsNullOrEmpty(res))
                return res;
            return DbProxy.Instance.GetConfig("Language", "fa");
        }
        set
        {
            if (System.Web.HttpContext.Current.Session == null)
                return;
            System.Web.HttpContext.Current.Session["Lang"] = value;
        }
    }
    public static bool WorkflowInheritance
    {
         get
        {
            if (UserID == -1 || System.Web.HttpContext.Current.Session == null)
                return true;
            if (System.Web.HttpContext.Current.Session["WorkflowInheritance"] == null) // get it from options
            {
                System.Web.HttpContext.Current.Session["WorkflowInheritance"] = DbProxy.Instance.GetConfig("WorkflowInheritance", "1") == "1";
            }
            return (bool)System.Web.HttpContext.Current.Session["WorkflowInheritance"];
        }
        set
        {
            if (UserID == -1 || System.Web.HttpContext.Current.Session == null)
                return;
            //if (MonetaryUnit == value)
            //    return;
            System.Web.HttpContext.Current.Session["WorkflowInheritance"] = value;
        }
    }
    public static string POSUrl
    {
        get
        {
            if (System.Web.HttpContext.Current.Application == null)
                return "localhost:14560";
            lock (System.Web.HttpContext.Current.Application)
            {
                if (System.Web.HttpContext.Current.Application["POSUrl"] == null) // get it from options
                    System.Web.HttpContext.Current.Application["POSUrl"] = DbProxy.Instance.GetConfig("POSUrl", "localhost:14560");
                return (string)System.Web.HttpContext.Current.Application["POSUrl"];
            }
        }
        set
        {
            if (System.Web.HttpContext.Current.Application == null)
                return;
            lock (System.Web.HttpContext.Current.Application)
                System.Web.HttpContext.Current.Session["POSUrl"] = value;
        }
    }
    public static string POSPort
    {
        get
        {
            if (System.Web.HttpContext.Current.Application == null)
                return "COM2";
            lock (System.Web.HttpContext.Current.Application)
            {
                if (System.Web.HttpContext.Current.Application["POSPort"] == null) // get it from options
                    System.Web.HttpContext.Current.Application["POSPort"] = DbProxy.Instance.GetConfig("POSPort", "COM2");
                return (string)System.Web.HttpContext.Current.Application["POSPort"];
            }
        }
        set
        {
            if (System.Web.HttpContext.Current.Application == null)
                return;
            lock (System.Web.HttpContext.Current.Application)
                System.Web.HttpContext.Current.Session["POSPort"] = value;
        }
    }
    public static int DecimalPlaces
    {
        get
        {
            if (UserID == -1 || System.Web.HttpContext.Current.Session == null)
                return 2;
            if (System.Web.HttpContext.Current.Session["DecimalPlaces"] == null) // get it from options
            {
                System.Web.HttpContext.Current.Session["DecimalPlaces"] = Helper.GetInt( DbProxy.Instance.GetConfig("DecimalPlaces", "2"), 2);
            }
            return (int)System.Web.HttpContext.Current.Session["DecimalPlaces"];
        }
        set
        {
            if (UserID == -1 || System.Web.HttpContext.Current.Session == null)
                return;
            //if (MonetaryUnit == value)
            //    return;
            System.Web.HttpContext.Current.Session["DecimalPlaces"] = value;
        }
    }

    public static int UseAdditionalValue
    {
        get
        {
            if (UserID == -1 || System.Web.HttpContext.Current.Session == null)
                return 2;
            if (System.Web.HttpContext.Current.Session["UseAdditionalValue"] == null) // get it from options
            {
                System.Web.HttpContext.Current.Session["UseAdditionalValue"] = Helper.GetInt(DbProxy.Instance.GetConfig("UseAdditionalValue", "0"), 0);
            }
            return (int)System.Web.HttpContext.Current.Session["UseAdditionalValue"];
        }
        set
        {
            if (UserID == -1 || System.Web.HttpContext.Current.Session == null)
                return;
            //if (MonetaryUnit == value)
            //    return;
            System.Web.HttpContext.Current.Session["UseAdditionalValue"] = value;
        }
    }

    public static void ResetApplication()
    {
        lock (System.Web.HttpContext.Current.Application)
            System.Web.HttpContext.Current.Application.Clear();
    }
    public static void SetApplication(string key, object value)
    {
        lock (System.Web.HttpContext.Current.Application)
            System.Web.HttpContext.Current.Application[key] = value;
    }
    public static object GetApplication(string key)
    {
        if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Application == null)
            return null;
        lock (System.Web.HttpContext.Current.Application)
            return System.Web.HttpContext.Current.Application[key];
    }

    public static bool HasCardex()
    {
        int num = Helper.GetInt(DbProxy.Instance.FillScalar("SELECT COUNT(*) FROM CheckCardexView"), 0);
        return num > 0;
    }

    public static DateTime? FinanceYearStartDate
    {
        get
        {
            if (System.Web.HttpContext.Current.Session == null || System.Web.HttpContext.Current.Session["FinanceYearStartDate"] == null)
                return null;
            return Helper.GetDateTime(System.Web.HttpContext.Current.Session["FinanceYearStartDate"], DateTime.Now);
        }
        set
        {
            lock (System.Web.HttpContext.Current.Session)
                System.Web.HttpContext.Current.Session["FinanceYearStartDate"] = value;
        }
    }
    public static DateTime? FinanceYearEndDate
    {
        get
        {
            if (System.Web.HttpContext.Current.Session == null || System.Web.HttpContext.Current.Session["FinanceYearEndDate"] == null)
                return null;
            return Helper.GetDateTime(System.Web.HttpContext.Current.Session["FinanceYearEndDate"], DateTime.Now);
        }
        set
        {
            lock (System.Web.HttpContext.Current.Session)
                System.Web.HttpContext.Current.Session["FinanceYearEndDate"] = value;
        }
    }

    public static string GetFinanceYearTitle()
    {
        return DbProxy.Instance.FillScalarString("SELECT Title FROM FinanceYear WHERE Status=2");
    }

    /// <summary>
    /// Are we using the main DB or a DB from old financial years
    /// </summary>
    public static bool IsDbReadonly
    {
        get
        {
            // if (System.Web.HttpContext.Current.Session == null)
            // System.Web.HttpContext.Current.Session["OtherCatalogName"] = value;
            //SELECT * FROM FinanceYear WHERE Status=2
            if (string.IsNullOrEmpty(OtherCatalogName))
                return false;
            if (System.Web.HttpContext.Current.Session["IsDbReadonly"] == null)
            {
                object obj = DbProxy.Instance.FillScalar("SELECT TempClosed FROM FinanceYear WHERE Status=2");
                System.Web.HttpContext.Current.Session["IsDbReadonly"] = !Helper.GetBool(obj, false);
            }
            return Helper.GetBool(System.Web.HttpContext.Current.Session["IsDbReadonly"], true);
        }
    }
    /// <summary>
    /// The name of DB from old financial years
    /// </summary>
    public static string OtherCatalogName
    {
        get
        {
            //return "DB_1393";
            if (System.Web.HttpContext.Current.Session == null)
                return null;
            return Helper.GetString(System.Web.HttpContext.Current.Session["OtherCatalogName"], null);
        }
        set
        {
            if (System.Web.HttpContext.Current.Session == null)
                return;
            System.Web.HttpContext.Current.Session["OtherCatalogName"] = value;
            System.Web.HttpContext.Current.Session["IsDbReadonly"] = null;
            Invalidate();
        }
    }

    /// <summary>
    /// Current MonetaryUnit, chosen by current user
    /// </summary>
    public static int MonetaryUnit
    {
        get
        {
            if (UserID == -1 || System.Web.HttpContext.Current.Session == null)
                return -1;
            int res = Helper.GetInt(System.Web.HttpContext.Current.Session["MonetaryUnit"], -2);
            if (res == -2) // get it from options
            {
                res = Helper.GetInt(DbProxy.Instance.GetOption(UserID, "MonetaryUnit"), -1);
                if (res == -1) // get from config
                    res = Helper.GetInt(DbProxy.Instance.GetConfig("MonetaryUnit", -1), -1);
                System.Web.HttpContext.Current.Session["MonetaryUnit"] = res;
            }
            return res;
        }
        set
        {
            if (UserID == -1 || System.Web.HttpContext.Current.Session == null)
                return;
            //if (MonetaryUnit == value)
            //    return;
            System.Web.HttpContext.Current.Session["MonetaryUnit"] = value;
            DbProxy.Instance.SetOptions(UserID, "MonetaryUnit", value.ToString());
            System.Web.HttpContext.Current.Session["MonetaryUnitFactors"] = null;
        }
    }
    public static string GetTodayDate
    {
        get
        {
            string res = "";
            string qry = "SELECT [dbo].[GetPersionDayOfWeek](GETDATE()) + [dbo].GetShamsiDateSlashed(GETDATE())";
            if (DbProxy.Instance.GetConfig("DateType", "fa") != "fa")
            {
                qry = "SELECT DATENAME(WEEKDAY, GETDATE()) + ' ' + CONVERT(NVARCHAR, CONVERT(DATE, GETDATE()))";
            }
            DataTable dt = DbProxy.Instance.FillData(qry, "f");
            if (dt != null && dt.Rows.Count != 0)
            {
                res = dt.Rows[0][0].ToString();
            }
            return res;
        }
        set
        {
        }
    }
    public static string MonetaryUnitAbbreviation
    {
        get
        {
            string res = "";
            if (MonetaryUnit > 0)
            {
                DataTable dt = DbProxy.Instance.FillData("SELECT Abbreviation FROM MonetaryUnit WHERE ID = " + MonetaryUnit, "f");
                if (dt != null || dt.Rows.Count != 0)
                {
                    res = dt.Rows[0][0].ToString();
                }
            }
            return res;
        }
        set
        {
        }
    }
    public static string SecondMonetaryUnitAbbreviation
    {
        get
        {
            string res = "";
            if(System.Web.HttpContext.Current.Session["SecondMonetaryUnitAbbreviation"] != null)
            {
                res = System.Web.HttpContext.Current.Session["SecondMonetaryUnitAbbreviation"].ToString();
            }
            return res;
        }
    }
    public static List<Tuple<DateTime, DateTime, decimal>> MonetaryUnitFactors
    {
        get
        {
            if (UserID == -1 || System.Web.HttpContext.Current.Session == null)
                return null;

            System.Threading.Thread.CurrentThread.CurrentCulture = Helper.NumberCulture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = Helper.NumberCulture;
            List<Tuple<DateTime, DateTime, decimal>> res = System.Web.HttpContext.Current.Session["MonetaryUnitFactors"] as List<Tuple<DateTime, DateTime, decimal>>;
            if (res == null && MonetaryUnit > 0)
            {
                System.Web.HttpContext.Current.Session["MonetaryUnitFactors"] = res = new List<Tuple<DateTime, DateTime, decimal>>();
                int baseUnit = Helper.GetInt(DbProxy.Instance.GetConfig("MonetaryUnit", -1), -1);
                if (baseUnit <= 0 || baseUnit == MonetaryUnit || MonetaryUnit <= 0)
                    return res;
                DataTable dt = DbProxy.Instance.FillData("SELECT StartDate, EndDate, Factor FROM Conversion WHERE Enabled = 1 AND FromID = " + baseUnit + " AND ToID = " + MonetaryUnit, "f");
                bool viceVersa = false;
                if (dt == null || dt.Rows.Count == 0) // no conversion
                {
                    dt = DbProxy.Instance.FillData("SELECT StartDate, EndDate, Factor FROM Conversion WHERE Enabled = 1 AND FromID = " + MonetaryUnit + " AND ToID = " + baseUnit, "f");
                    if (dt == null || dt.Rows.Count == 0) // no conversion
                        return res;
                    viceVersa = true;
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    decimal factor = Helper.GetDecimal(dt.Rows[i][2], 1);
                    if (viceVersa)
                        factor = 1 / factor;
                    res.Add(new Tuple<DateTime, DateTime, decimal>((DateTime)dt.Rows[i][0], (DateTime)dt.Rows[i][1], factor));
                }
                System.Web.HttpContext.Current.Session["MonetaryUnitFactors"] = res;
            }
            return res;
        }
    }
    public static decimal SecondMonetaryUnitFactors
    {
        get
        {
            decimal res = 1;
            if (MonetaryUnit > 0)
            {
                DataTable dt = DbProxy.Instance.FillData(string.Format(@"SELECT TOP 1 (SELECT Abbreviation FROM MonetaryUnit WHERE ID = CASE WHEN FromID = {0} THEN ToID ELSE FromID END) AS Abbreviation, FromID, ToID, Factor FROM Conversion WHERE Enabled = 1 
                AND CONVERT(DATE, GETDATE()) BETWEEN CONVERT(DATE, StartDate) AND CONVERT(DATE, EndDate)
                AND(FromID = {0} OR ToID = {0}) ", MonetaryUnit), "f");

                if (dt != null && dt.Rows.Count != 0)
                {
                    if (dt.Rows[0][0] != null)
                        System.Web.HttpContext.Current.Session["SecondMonetaryUnitAbbreviation"] = dt.Rows[0][0].ToString();

                    if (Helper.GetInt(dt.Rows[0][1], 0) == MonetaryUnit) //fromid
                    {
                        res = Helper.GetDecimal(dt.Rows[0][3], 0);
                    }
                    else if (Helper.GetInt(dt.Rows[0][2], 0) == MonetaryUnit)//toid
                    {
                        res = 1/Helper.GetDecimal(dt.Rows[0][3], 0);
                    }
                }
            }
            return res;
        }
    }
    public static decimal ToMonetary(decimal val, DateTime date)
    {
        if (date == Helper.NullDate)
            return val;
        List<Tuple<DateTime, DateTime, decimal>> factors = MonetaryUnitFactors;
        if (factors == null || factors.Count == 0)
            return val;
        for (int i = 0; i < factors.Count; i++)
            if (factors[i].Item1 <= date && date <= factors[i].Item2)
                return val * factors[i].Item3;
        return val;
    }
    public static void Invalidate()
    {
        System.Web.HttpContext.Current.Session["MonetaryUnit"] = null;
        System.Web.HttpContext.Current.Session["MonetaryUnitName"] = null;
        System.Web.HttpContext.Current.Session["MonetaryUnitFactors"] = null;
        System.Web.HttpContext.Current.Session["SecondMonetaryUnitAbbreviation"] = null;
        System.Web.HttpContext.Current.Session["ProgramID"] = null;
        System.Web.HttpContext.Current.Session["ProgramClosed"] = null;
        System.Web.HttpContext.Current.Session["WorkflowInheritance"] = null;
        System.Web.HttpContext.Current.Session["DecimalPlaces"] = null;
        System.Web.HttpContext.Current.Session["UseAdditionalValue"] = null;
        lock (System.Web.HttpContext.Current.Application)
        {
            System.Web.HttpContext.Current.Application.Remove("DateType");
            System.Web.HttpContext.Current.Application.Remove("POSUrl");
            System.Web.HttpContext.Current.Application.Remove("POSPort");
        }
        DataRow row = DbProxy.Instance.FillRow("SELECT ID, StartDate, EndDate FROM FinanceYear WHERE Status=2");
        if (row == null)
        {
            FinanceYearStartDate = null;
            FinanceYearEndDate = null;
        }
        else
        {
            FinanceYearStartDate = Helper.GetDateTime(row, "StartDate", DateTime.Now);
            FinanceYearEndDate = Helper.GetDateTime(row, "EndDate", DateTime.Now);
        }
    }
    /// <summary>
    /// Current Program, chosen by current user
    /// -1: default Program = Null Program
    /// 0: All programs
    /// >0: specific program
    /// </summary>
    public static int ProgramID
    {
        get
        {
            if (UserID == -1 || System.Web.HttpContext.Current.Session == null)
                return -1;
            int res = Helper.GetInt(System.Web.HttpContext.Current.Session["ProgramID"], -2);
            if (res == -2) // get it from options
            {
                res = Helper.GetInt(DbProxy.Instance.GetOption(UserID, "ProgramID"), -1);
                System.Web.HttpContext.Current.Session["ProgramID"] = res;
            }
            return res;
        }
        set
        {
            if (UserID == -1 || System.Web.HttpContext.Current.Session == null)
                return;
            System.Web.HttpContext.Current.Session["ProgramID"] = value;
            System.Web.HttpContext.Current.Session["ProgramClosed"] = null;
            DbProxy.Instance.SetOptions(UserID, "ProgramID", value.ToString());
        }
    }

    public static bool ProgramClosed
    {
        get
        {
            if (UserID == -1 || System.Web.HttpContext.Current.Session == null)
                return true;
            if (System.Web.HttpContext.Current.Session["ProgramClosed"] == null)
            {
                if (ProgramID == 0)
                    System.Web.HttpContext.Current.Session["ProgramClosed"] = true;
                else
                {
                    DataRow financeYear = DbProxy.Instance.FillRow("SELECT * FROM FinanceYear WHERE Status=2");
                    if (financeYear == null) // "سال مالی فعال وجود ندارد.";
                        System.Web.HttpContext.Current.Session["ProgramClosed"] = true;
                    else
                    {
                        string closedsPrograms = "," + Helper.GetString(financeYear, "ClosedPrograms", "") + ",";
                        System.Web.HttpContext.Current.Session["ProgramClosed"] = closedsPrograms.Contains("," + ProgramID + ",");
                    }
                }
            }
            return Helper.GetBool(System.Web.HttpContext.Current.Session["ProgramClosed"], true);
        }
    }

    public static string ReplaceOptions(string query)
    {
        if (User != null)
            query = query.Replace("_Role_WardIDs_", SiteSettings.User.WardIDs);
        if (query.Contains("__PrOgrAmconD__"))
        {
            string prog = " (1 = 1) ";
            if (SiteSettings.ProgramID == -1) // default : NULL
                prog = " (ProgramID IS NULL) ";
            else if (SiteSettings.ProgramID == 0) // all
                prog = " (1 = 1) ";
            else
                prog = " (ProgramID = " + SiteSettings.ProgramID + ") ";
            query = query.Replace("__PrOgrAmconD__", prog);
        }
        if (query.Contains("__PrOgramiD__"))
        {
            string prog = " NULL ";
            if (SiteSettings.ProgramID == -1) // default : NULL
                prog = " NULL ";
            else if (SiteSettings.ProgramID == 0) // all --> no insert, update is allowed. make sure there is an error
                prog = " I am here to cause an error ";
            else
                prog = " " + SiteSettings.ProgramID + " ";
            query = query.Replace("__PrOgramiD__", prog);
        }
        return query.Replace("_PersonnelID_", SiteSettings.User == null ? "0" : SiteSettings.User.PersonnelID.ToString())
                        .Replace("_UserID_", SiteSettings.User == null ? "0" : SiteSettings.UserID.ToString())
                        .Replace("_IsAdmin_", SiteSettings.User == null ? "0" : SiteSettings.User.IsAdmin ? "1" : "0");
    }

    public const int MaxVouchersNoAct = 50, MaxVouchersAct1 = 100, MaxVouchersAct2 = 200;
    public static int MaxAllowedVouchers
    {
        get
        {
            // No Activation:   50
            // Activation 1:    100
            // Activation 2:    200
            // Dongle:          Max
            try
            {
                if (ValidDongle())
                    return int.MaxValue;
            }
            catch { }
            using (DbProxy proxy = new DbProxy())
            {
                string activation = proxy.GetConfig("Activation", -1);
                if (!string.IsNullOrEmpty(activation))
                {
                    FingerPrint fp = new FingerPrint();
                    if (fp.Validate2(activation))
                        return MaxVouchersAct2;
                    if (fp.Validate(activation))
                        return MaxVouchersAct1;
                }
                return MaxVouchersNoAct;
            }
        }
    }
    /// <summary>
    /// 3 digits. first digit = level, second digit = salary, third digit = visitor
    /// 100 = l1
    /// 110 = l1 + salary
    /// 111 = l1 + salary + visitor
    /// 301 = l3 + visitor
    /// ...
    /// </summary>
    public static int DemoLevel
    {
        get
        {
            //return "DB_1393";
            if (System.Web.HttpContext.Current.Session == null)
                return 100;
            return Helper.GetInt(System.Web.HttpContext.Current.Session["DemoLevel"], 100);
        }
        set
        {
            if (System.Web.HttpContext.Current.Session == null)
                return;
            System.Web.HttpContext.Current.Session["DemoLevel"] = value;
        }
    }
    public static bool DongleFound
    {
        get
        {
            return Helper.GetBool(GetApplication("DongleFound"), false);
        }
        set
        {
            SetApplication("DongleFound", value);
        }
    }
    public static int DongleLevel
    {
        get
        {
            int demo = DemoLevel / 100; // 301 --> 3
            return Math.Max(Helper.GetInt(GetApplication("DongleLevel"), 1), demo);
        }
        set
        {
            SetApplication("DongleLevel", value);
        }
    }
    public static bool DongleSalary
    {
        get
        {
            bool demo = (DemoLevel % 100) / 10 == 1; //310 --> 10 --> 1
            if (demo)
                return true;
            return Helper.GetBool(GetApplication("DongleSalary"), false);
        }
        set
        {
            SetApplication("DongleSalary", value);
        }
    }
    public static bool DongleVisitor
    {
        get
        {
            bool demo = (DemoLevel % 10) == 1; //301 --> 1
            if (demo)
                return true;
            return Helper.GetBool(GetApplication("DongleVisitor"), false);
        }
        set
        {
            SetApplication("DongleVisitor", value);
        }
    }
    private static bool ValidDongle()
    {
        //return true;
        TINYLib.TinyPlusCtrl tiny = new TINYLib.TinyPlusCtrl();
        // mine:
        //string userKey = "A3AA9F1576A4AA2EADAC4D4F54BED3C"; // read key
        //string strSafeKey1 = "5F5319C4E364CC1231DC2656D4E7F6A3CF516073C054FE9DB8F9539B46492858E6E994A3D3517C1B4230700520D1098F90BF";
        //string strSafeKey2 = "F9F33BD0FBB686140D348159D7DA35F7EF755ABF790DAF568198F64619287B73F9D4EBF7DB754E675559ABF8E18DFD537A25";
        //string[] ArrRequest = new string[20] { "88DC466574E53393A241", "0040CAE9940E52C8F792", "7F772DC8F72917F382A1", "E9338BB6B9695B27C2D7", "1E56CCEB96B79F65002F", "F656A4B746A4B0526500", "C537F38295DF49F185A0", "BDB76B16352527C7E281", "9CD44A6914E1539FBA4F", "DF57F19CAF3C54DEFD98", "474759681793C3416C0B", "BACA537E01A3D3456073", "A1C75F6E0D2151D1FC9B", "A3D3517C1B0F3321CCEB", "2A1EF4879691C74F5E61", "E64494B344C436F28796", "0B4339C8F78D27BB6605", "5CD40A29D4F24294B342", "CD47FB8A9DF26AA0B35E", "A0A842610C8B3FB9485B" };
        //string[] ArrResponse = new string[20] { "21D5775E55F94FF7D0B5", "9C64E2CB48734FBD0427", "7B7B593E132D7317F0D5", "A8D656697840569AFDFA", "A373E1C84B052547B48D", "CB7309247BCDE993A651", "195317F009E355D53116", "5C5A36C9F48C92FADD48", "20F0624BC8E640042764", "E373D53A13DD0D8FB661", "5D656B64AD27DF654AAF", "C018B79C75F9F1675469", "26D4447BA2756DF5DA3F", "F7EF755ABF938F85EACF", "6FABD9344BF6D4544B86", "BB61392079ED6733C8DF", "A9698B04CD4CEA6639E4", "60F0A28B08C75F392077", "8C0AA65D5CA9196B821D", "D1E183AA55297D0B8479" };

        // his:
        string userKey = "1DDED29FD749A81EA0FE4AC76A1137D"; // read key
        string strSafeKey1 = "1F13D984A35D3F27C2E1D65290BF5A6C6C36D5F08ED24877122517EF8AA9BB030520CFD0709AB964767230DFFA8BD355701F";
        string strSafeKey2 = "31357FF2D52F8F193EE35FBFC9EE93B75FFD288B20E4724FBCBBF181664FD1551F127531399B02E5E0F4B26D5C00E6E691BC";
        string[] ArrRequest = new string[20] { "2020EA89B444B40E2DC8", "DE5298476270303AD984", "B3337D18C72020EA89B4", "46B2002FCAD947A34E6D", "1907E38EAD97D3517C1B", "2A32F493BE40C00A29D4", "80C04A69147D3FC7E281", "BA020423CE3632F09FBA", "4AD21433DE8DCF577211", "0537CFEA8946B2002FCA", "3030FA994455D71F3AD9", "EE72A857722C2CF695B0", "A4946E0D283DFF87A241", "53F31D38E792F25C7B26", "0E52C8F792A1AF6B1635", "C63280AF4A5C3C26C5E0", "F333BD58070B53D5F09F", "A1AF6B1635C5378FAA49", "5A4224C3EE988062012C", "1753D1FC9BA9B7731E3D" };
        string[] ArrResponse = new string[20] { "A7AFED183B4D05A78EF9", "D064227F6C579F9DC82B", "B63E84674AA7AFED183B", "61F5AFA205CE74187B62", "8EB4D83B2245911B38E1", "1E1E6C8DB247CF8DB8DB", "07CF4D789B20E686B15C", "8E0E1C3D627175BF3215", "BE3E0C2D5230761641EC", "DBD1A1866F61F5AFA205", "979FDD086B2BB1F1D6BF", "C044126F5C7179DB4225", "ED2547AE99E0A646711C", "96FE64472A567E2405EA", "A064F2CF3CB2C08C775E", "E1752F228561898BF2D5", "F63EC4A78AAF0F99BE63", "B2C08C775E9BD161462F", "AECE3C5D82404026CFF4", "C5119BB861FE0448AB92" };
        try
        {
            tiny.FindFirstTPlus(userKey, strSafeKey1, strSafeKey2);
            short error = tiny.GetTPlusErrorCode();
            if (error == 0)
            {
                // lock found. check if this is realy ours
                Random rnd = new Random();
                int index = rnd.Next(ArrRequest.Length);
                //---------------------check query--------------------------------
                if (tiny.GetTPlusQuery(ArrRequest[index]) == ArrResponse[index])
                    return true;
                return false;
            }
            else
            {
                string errorStr = TinyErrorToString(error);
                if (error > 1)
                    Logger.Log(errorStr);
            }
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
        }
        finally
        {
            tiny.DisconnectFromTPlus();
        }
        return false;
    }
    public static void InitDongle()
    {
        try
        {
            InitDongleInternal();
        }
        catch (Exception ex)
        {
            ExceptionLog.Instance.Add(ex);
        }
    }
    private static void InitDongleInternal()
    {
        DongleFound = false;
        DongleLevel = 1;
        DongleSalary = false;
        DongleVisitor = false;

        TINYLib.TinyPlusCtrl tiny = new TINYLib.TinyPlusCtrl();
        // mine:
        //string userKey = "A3AA9F1576A4AA2EADAC4D4F54BED3C"; // read key
        //string strSafeKey1 = "5F5319C4E364CC1231DC2656D4E7F6A3CF516073C054FE9DB8F9539B46492858E6E994A3D3517C1B4230700520D1098F90BF";
        //string strSafeKey2 = "F9F33BD0FBB686140D348159D7DA35F7EF755ABF790DAF568198F64619287B73F9D4EBF7DB754E675559ABF8E18DFD537A25";
        //string[] ArrRequest = new string[20] { "88DC466574E53393A241", "0040CAE9940E52C8F792", "7F772DC8F72917F382A1", "E9338BB6B9695B27C2D7", "1E56CCEB96B79F65002F", "F656A4B746A4B0526500", "C537F38295DF49F185A0", "BDB76B16352527C7E281", "9CD44A6914E1539FBA4F", "DF57F19CAF3C54DEFD98", "474759681793C3416C0B", "BACA537E01A3D3456073", "A1C75F6E0D2151D1FC9B", "A3D3517C1B0F3321CCEB", "2A1EF4879691C74F5E61", "E64494B344C436F28796", "0B4339C8F78D27BB6605", "5CD40A29D4F24294B342", "CD47FB8A9DF26AA0B35E", "A0A842610C8B3FB9485B" };
        //string[] ArrResponse = new string[20] { "21D5775E55F94FF7D0B5", "9C64E2CB48734FBD0427", "7B7B593E132D7317F0D5", "A8D656697840569AFDFA", "A373E1C84B052547B48D", "CB7309247BCDE993A651", "195317F009E355D53116", "5C5A36C9F48C92FADD48", "20F0624BC8E640042764", "E373D53A13DD0D8FB661", "5D656B64AD27DF654AAF", "C018B79C75F9F1675469", "26D4447BA2756DF5DA3F", "F7EF755ABF938F85EACF", "6FABD9344BF6D4544B86", "BB61392079ED6733C8DF", "A9698B04CD4CEA6639E4", "60F0A28B08C75F392077", "8C0AA65D5CA9196B821D", "D1E183AA55297D0B8479" };

        // his:
        string userKey = "1DDED29FD749A81EA0FE4AC76A1137D"; // read key
        string strSafeKey1 = "1F13D984A35D3F27C2E1D65290BF5A6C6C36D5F08ED24877122517EF8AA9BB030520CFD0709AB964767230DFFA8BD355701F";
        string strSafeKey2 = "31357FF2D52F8F193EE35FBFC9EE93B75FFD288B20E4724FBCBBF181664FD1551F127531399B02E5E0F4B26D5C00E6E691BC";
        string[] ArrRequest = new string[20] { "2020EA89B444B40E2DC8", "DE5298476270303AD984", "B3337D18C72020EA89B4", "46B2002FCAD947A34E6D", "1907E38EAD97D3517C1B", "2A32F493BE40C00A29D4", "80C04A69147D3FC7E281", "BA020423CE3632F09FBA", "4AD21433DE8DCF577211", "0537CFEA8946B2002FCA", "3030FA994455D71F3AD9", "EE72A857722C2CF695B0", "A4946E0D283DFF87A241", "53F31D38E792F25C7B26", "0E52C8F792A1AF6B1635", "C63280AF4A5C3C26C5E0", "F333BD58070B53D5F09F", "A1AF6B1635C5378FAA49", "5A4224C3EE988062012C", "1753D1FC9BA9B7731E3D" };
        string[] ArrResponse = new string[20] { "A7AFED183B4D05A78EF9", "D064227F6C579F9DC82B", "B63E84674AA7AFED183B", "61F5AFA205CE74187B62", "8EB4D83B2245911B38E1", "1E1E6C8DB247CF8DB8DB", "07CF4D789B20E686B15C", "8E0E1C3D627175BF3215", "BE3E0C2D5230761641EC", "DBD1A1866F61F5AFA205", "979FDD086B2BB1F1D6BF", "C044126F5C7179DB4225", "ED2547AE99E0A646711C", "96FE64472A567E2405EA", "A064F2CF3CB2C08C775E", "E1752F228561898BF2D5", "F63EC4A78AAF0F99BE63", "B2C08C775E9BD161462F", "AECE3C5D82404026CFF4", "C5119BB861FE0448AB92" };
        try
        {
            tiny.FindFirstTPlus(userKey, strSafeKey1, strSafeKey2);
            short error = tiny.GetTPlusErrorCode();
            if (error == 0)
            {
                // lock found. check if this is realy ours
                Random rnd = new Random();
                int index = rnd.Next(ArrRequest.Length);
                //---------------------check query--------------------------------
                if (tiny.GetTPlusQuery(ArrRequest[index]) == ArrResponse[index])
                {
                    DongleFound = true;
                    string data = Helper.GetString(tiny.GetTPlusData(TINYLib.EnumTPlusData.TPLUS_DATAPARTITION), null);
                    if (string.IsNullOrEmpty(data))
                        return;
                    // dongle data: L2;L3;Visitor;Salary --> ex: nazdar;shepol;mixek;nime
                    // setting level: nazdar-nime  --> L2 + Salary
                    string level = DbProxy.Instance.GetConfig("MyLevelCode", "");
                    if (string.IsNullOrEmpty(level))
                        return;
                    string[] dataArr = data.Split(';');
                    if (dataArr.Length < 4)
                        return;
                    level = "-" + level + "-";
                    if (level.IndexOf("-" + dataArr[1] + "-") >= 0)
                        DongleLevel = 3;
                    else if (level.IndexOf("-" + dataArr[0] + "-") >= 0)
                        DongleLevel = 2;
                    if (level.IndexOf("-" + dataArr[2] + "-") >= 0)
                        DongleVisitor = true;
                    if (level.IndexOf("-" + dataArr[3] + "-") >= 0)
                        DongleSalary = true;
                }
            }
            else
            {
                string errorStr = TinyErrorToString(error);
                if (error > 1)
                    Logger.Log(errorStr);
            }
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
        }
        finally
        {
            tiny.DisconnectFromTPlus();
        }
    }
    private static string TinyErrorToString(short error)
    {
        //---------------if tiny+ error code is 0, mean TRUE else FALSE------------
        if (error == 0)
            return "Lock found(detect)";
        if (error == 1)
            return "Lock not found(not detect)";
        if (error == 2)
            return " error missing key user";
        if (error == 3)
            return error + " error lock change";
        if (error == 4)
            return error + " error writing data";
        if (error == 5)
            return error + " error network Init";
        if (error == 6)
            return error + " error send & receive";
        if (error == 7)
            return error + " error login in permission";
        if (error == 8)
            return error + " error activX  listening";
        if (error == 9)
            return error + " error invalid";
        if (error == 10)
            return error + " error invalid  lock";
        if (error == 11)
            return error + " error shared dongle";
        if (error == 12)
            return error + " error query";
        return error + " error unknown";
    }
}