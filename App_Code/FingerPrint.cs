﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Management;
using System.IO;

public class FingerPrint
{
    private string FPStr, mNumericKey = null, mStringKey = null;
    public FingerPrint()
    {
        string cpuID = "weregullm", diskID = "1356063013811023", biosID = "detgemsima", baseID = "kURd";
        try
        {
            cpuID = CpuId().Trim().Replace(" ", "");
        }
        catch
        {
        }
        try
        {
            diskID = DiskId().Trim().Replace(" ", "");
        }
        catch (Exception ex)
        {
        }
        try
        {
            biosID = BiosId().Trim().Replace(" ", "");
        }
        catch
        {
        }
        try
        {
            baseID = BaseId().Trim().Replace(" ", "");
        }
        catch
        {
        }
        if (string.IsNullOrEmpty(cpuID))
            cpuID = "weregullm";
        if (string.IsNullOrEmpty(diskID))
            diskID = "1356063013811023";
        if (string.IsNullOrEmpty(biosID))
            biosID = "detgemsima";
        if (string.IsNullOrEmpty(baseID))
            baseID = "kURd";
        if (cpuID.Length > 6)
            cpuID = cpuID.Substring(cpuID.Length - 6);
        if (diskID.Length > 10)
            diskID = diskID.Substring(diskID.Length - 10);
        if (baseID.Length > 4)
            baseID = baseID.Substring(baseID.Length - 4);
        //if ((cpuID.Length + diskID.Length) < 25)
        {
            if (biosID.Length > (25 - (cpuID.Length + diskID.Length + baseID.Length)))
                biosID = biosID.Substring(biosID.Length - (25 - (cpuID.Length + diskID.Length + baseID.Length)));
        }

        FPStr = string.Concat(cpuID, diskID, biosID, baseID);
    }
    public FingerPrint(string fpString)
    {
        FPStr = fpString;
        if (FPStr.Length < 25)
            FPStr += "bijikurdistanbmredujmnKurd".Substring(0, 25 - FPStr.Length);
    }
    public FingerPrint(string numericKey, string stringKey)
        : this()
    {
        mNumericKey = numericKey;
        mStringKey = stringKey;
    }

    #region private helpers
    private string Identifier(string wmiClass, string wmiProperty)
    {
        ManagementObjectCollection.ManagementObjectEnumerator en = null;
        string res = "";
        ManagementClass mClass = null;
        ManagementObjectCollection col = null;
        try
        {
            mClass = new ManagementClass(wmiClass);
            col = mClass.GetInstances();
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
            return res;
        }

        try
        {
            en = col.GetEnumerator();
            while (en.MoveNext())
            {
                ManagementObject obj = (ManagementObject)en.Current;
                if (res == "")
                {
                    try
                    {
                        res = obj[wmiProperty].ToString();
                    }
                    catch (Exception ex)
                    {
                        Logger.Log(ex);
                    }
                }
            }
        }
        finally
        {
            if (en != null)
            {
                ((IDisposable)en).Dispose();
            }
        }
        return res;
    }
    private string Identifier(string wmiClass, string wmiProperty, string wmiMustBeTrue)
    {
        ManagementObjectCollection.ManagementObjectEnumerator en = null;
        string res = "";
        ManagementClass mClass = null;
        ManagementObjectCollection col = null;
        try
        {
            mClass = new ManagementClass(wmiClass);
            col = mClass.GetInstances();
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
            return res;
        }

        try
        {
            en = col.GetEnumerator();
            while (en.MoveNext())
            {
                ManagementObject obj = (ManagementObject)en.Current;
                if (obj[wmiMustBeTrue].ToString().ToLower().Equals("true") && res.Equals(""))
                {
                    try
                    {
                        return obj[wmiProperty].ToString();
                    }
                    catch (Exception ex)
                    {
                        Logger.Log(ex);
                    }
                }
            }
        }
        finally
        {
            if (en != null)
            {
                ((IDisposable)en).Dispose();
            }
        }
        return res;
    }
    private string BaseId()
    {
        return string.Concat(Identifier("Win32_BaseBoard", "Model"),
            Identifier("Win32_BaseBoard", "Manufacturer"),
            Identifier("Win32_BaseBoard", "Name"),
            Identifier("Win32_BaseBoard", "SerialNumber"));
    }
    private string BiosId()
    {
        //return "bios";
        return string.Concat(this.Identifier("Win32_BIOS", "Manufacturer"),
            this.Identifier("Win32_BIOS", "SMBIOSBIOSVersion"),
            this.Identifier("Win32_BIOS", "IdentificationCode"),
            this.Identifier("Win32_BIOS", "SerialNumber"),
            this.Identifier("Win32_BIOS", "ReleaseDate")
            , this.Identifier("Win32_BIOS", "Version"));
    }
    private string CpuId()
    {
        string res;

        try
        {
            res = this.Identifier("Win32_Processor", "UniqueId");
            if (res == "")
            {
                res = this.Identifier("Win32_Processor", "ProcessorId");
                if (res == "")
                {
                    res = this.Identifier("Win32_Processor", "Name");
                    if (res == "")
                        res = this.Identifier("Win32_Processor", "Manufacturer");
                    res = res + this.Identifier("Win32_Processor", "MaxClockSpeed");
                }
            }
        }

        catch (Exception ex)
        {
            Logger.Log(ex);
            res = "9247474A38427127";
        }
        return res;
    }
    private string DiskId()
    {
        //try
        //{
        //    using (DriveListEx drvList = new DriveListEx())
        //    {
        //        drvList.Load();
        //        if (drvList.Count > 0)
        //        {
        //            string res = drvList[0].SerialNumber;
        //            if (res.Length > 3)
        //                return res;
        //        }
        //        else
        //            Logger.Log(new Exception("drvList.Count = 0"));
        //    }
        //}
        //catch (Exception ex)
        //{
        //    Logger.Log(ex);
        //}
        string diskIdText = null;

        try
        {
            diskIdText = Identifier("Win32_PhysicalMedia", "SerialNumber");
            if (string.IsNullOrEmpty(diskIdText))
            {
                diskIdText = this.Identifier("Win32_DiskDrive", "SerialNumber");
            }
            if (string.IsNullOrEmpty(diskIdText))
            {

            }
        }
        catch (Exception ex)
        {
            Logger.Log(ex);

        }
        if (string.IsNullOrEmpty(diskIdText))
        {
            DriveInfo[] drvInfo = System.IO.DriveInfo.GetDrives();
            diskIdText = string.Empty;
            for (int i = 0; i < drvInfo.Length; i++)
                try
                {
                    diskIdText = diskIdText + drvInfo[i].TotalSize.ToString();
                }
                catch (Exception ex1)
                {
                    Logger.Log(ex1, "Drive.Name=" + drvInfo[i].Name);
                }
        }
        if (!string.IsNullOrEmpty(diskIdText))
            return diskIdText;

        return "";
    }
    private string MacId()
    {
        return Identifier("Win32_NetworkAdapterConfiguration", "MACAddress", "IPEnabled");
    }

    #endregion
    public void DumpToFile(string filePath)
    {
        string cpuID = "weregullm", diskID = "1356063013811023", biosID = "detgemsima";
        try
        {
            cpuID = CpuId().Trim().Replace(" ", "");
        }
        catch
        {
        }
        try
        {
            diskID = DiskId().Trim().Replace(" ", "");
        }
        catch
        {
        }
        try
        {
            biosID = BiosId().Trim().Replace(" ", "");
        }
        catch
        {
        }
        if (string.IsNullOrEmpty(cpuID))
            cpuID = "weregullm";
        if (string.IsNullOrEmpty(diskID))
            diskID = "1356063013811023";
        if (string.IsNullOrEmpty(biosID))
            biosID = "detgemsima";

        System.IO.StreamWriter r = System.IO.File.CreateText(filePath);
        r.WriteLine("CpuID = " + cpuID);
        r.WriteLine("BiosID = " + biosID);
        r.WriteLine("DiskID (used) = " + diskID);
        r.WriteLine("BaseId = " + BaseId());
        r.WriteLine("MacID = " + MacId());
        r.WriteLine("FingerPrint = " + FPStr);
        r.WriteLine("PC Code = " + PCCode);
        r.WriteLine("Activation Code = " + ActivationCode);
        r.Close();
        r.Dispose();
    }

    public string PCCode
    {
        get
        {
            string res = CalcPCCode();
            return res;
        }
    }
    public string ActivationCode
    {
        get
        {
            string key = mStringKey == null ? "KURDA0PDMB6U45YH1BIJIRIX8O9QSFZT2NLEGCJ7" : mStringKey;
            try
            {
                System.Text.StringBuilder b = new System.Text.StringBuilder();
                string finger = PCCode;
                finger = finger.Replace("-", "").Replace(" ", "").Substring(0, 25);
                for (int i = 0; i < finger.Length; i++)
                {
                    int n = Convert.ToInt32(finger.Substring(i, 1));
                    n *= 3;
                    n += (int)key[i % key.Length];
                    b.Append(key[n % key.Length]);
                }
                finger = b.ToString();
                finger = string.Format("{0}-{1}-{2}-{3}-{4}",
                    finger.Substring(0, 5), finger.Substring(5, 5),
                    finger.Substring(10, 5), finger.Substring(15, 5),
                    finger.Substring(20, 5));
                return finger;
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }
            return "";
        }
    }
    public string ActivationCode2
    {
        get
        {
            string key = mStringKey == null ? "FUCKTURKNAZDAR1CHAWR3SHGY4N0H0HNATALIEJ7" : mStringKey;
            try
            {
                System.Text.StringBuilder b = new System.Text.StringBuilder();
                string finger = PCCode;
                finger = finger.Replace("-", "").Replace(" ", "").Substring(0, 25);
                for (int i = 0; i < finger.Length; i++)
                {
                    int n = Convert.ToInt32(finger.Substring(i, 1));
                    n *= 3;
                    n += (int)key[i % key.Length];
                    b.Append(key[n % key.Length]);
                }
                finger = b.ToString();
                finger = string.Format("{0}-{1}-{2}-{3}-{4}",
                    finger.Substring(0, 5), finger.Substring(5, 5),
                    finger.Substring(10, 5), finger.Substring(15, 5),
                    finger.Substring(20, 5));
                return finger;
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }
            return "";
        }
    }
    public bool Validate(string activationCode)
    {
        return ActivationCode == activationCode;
    }
    public bool Validate2(string activationCode)
    {
        return ActivationCode2 == activationCode;
    }
    string CalcCheckSum(string finger)
    {
        try
        {
            int sum = 0;
            for (int i = 0; i < finger.Length; i++)
                sum += Convert.ToInt32(finger.Substring(i, 1));
            sum %= 100;
            return sum > 9 ? sum.ToString() : "0" + sum.ToString();
        }
        catch
        { }
        return "00";
    }
    private string CalcPCCode()
    {
        if (this.FPStr == null || this.FPStr.Length < 10)
            return "0";
        try
        {
            string key = mNumericKey == null ? "2316735913817502934176481" : mNumericKey;
            System.Text.StringBuilder b = new System.Text.StringBuilder();
            for (int i = 0; i < key.Length; i++)
            {
                int n = i;// int.Parse(key[i].ToString());// Convert.ToInt32(key[i]);
                //n *= 3;
                n = (int)FPStr[n % FPStr.Length];
                b.Append(key[n % key.Length]);
            }
            string finger = b.ToString();
            finger = finger.Substring(0, 25);
            return string.Format("{0}-{1}-{2}-{3}-{4}-{5}",
                finger.Substring(0, 5), finger.Substring(5, 5),
                finger.Substring(10, 5), finger.Substring(15, 5),
                finger.Substring(20, 5),
                CalcCheckSum(finger));
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
            return "0";
        }
    }
}