﻿using System;
using System.Collections.Generic;

using System.Web;
using System.IO;

/// <summary>
/// All properties and methods of this class are Thread Safe.
/// Call <see cref="Init">Init</see> to initialize
/// </summary>
public static class Logger
{
    static StreamWriter log = null;
    static object mLock = new object();
    static string mFolder = null, mFileNamePrefix = null, mFileName = null;
    public static bool Pause = false;
    public static void Init(string folder, string fileNamePrefix)
    {
        if (log != null)
            return;
        lock (mLock)
        {
            mFolder = folder;
            mFileNamePrefix = fileNamePrefix;
        }
    }
    static void InsureLogFileExists()
    {
           }

    public static void StartLog()
    {
        //InsureLogFileExists();
        if (log == null)
            return;
        lock (mLock)
        {
            log.WriteLine("");
            log.WriteLine("=======================================");
            log.WriteLine("Log Time : " + Helper.FormatDateFull(DateTime.Now));
            log.Flush();
        }
    }
    public static void Log(Exception ex, System.Reflection.MethodBase method, string extraInf)
    {
        if (Pause)
            return;
        InsureLogFileExists();
        if (log == null)
            return;
        lock (mLock)
        {
            Log(ex, method);
            log.WriteLine("ExtraInf		: {0}", extraInf);
            log.Flush();
        }
    }
    public static void Log(Exception ex, System.Reflection.MethodBase method)
    {
        if (Pause)
            return;
        InsureLogFileExists();
        if (log == null)
            return;
        lock (mLock)
        {
            StartLog();
            log.WriteLine("Method		: {0}.{1}", method.DeclaringType.ToString(), method.Name);
            log.WriteLine("Error Message	: {0}", ex.Message);
            log.WriteLine("Error Source	: {0}", ex.Source);
            log.WriteLine("Stack Trace	: {0}", ex.StackTrace);
            log.Flush();
        }
    }
    public static void Log(Exception ex)
    {
        if (Pause)
            return;
        InsureLogFileExists();
        if (log == null)
            return;
        lock (mLock)
        {
            StartLog();
            log.WriteLine("Error Message	: {0}", ex.Message);
            log.WriteLine("Error Source		: {0}", ex.Source);
            log.WriteLine("Stack Trace		: {0}", ex.StackTrace);
            log.Flush();
        }
    }
    public static void Log(Exception ex, string extraInf)
    {
        if (Pause)
            return;
        InsureLogFileExists();
        if (log == null)
            return;
        lock (mLock)
        {
            StartLog();
            log.WriteLine("Error Message	: {0}", ex.Message);
            log.WriteLine("Error Source		: {0}", ex.Source);
            log.WriteLine("Stack Trace		: {0}", ex.StackTrace);
            log.WriteLine("ExtraInf		: {0}", extraInf);
            log.Flush();
        }
    }
    public static void Log(string message)
    {
        if (Pause)
            return;
        InsureLogFileExists();
        if (log == null)
            return;
        lock (mLock)
        {
            StartLog();
            log.WriteLine("Message	: {0}", message);
            log.Flush();
        }
    }
    public static void LogLine(string message)
    {
        if (Pause)
            return;
        InsureLogFileExists();
        if (log == null)
            return;
        lock (mLock)
        {
            log.WriteLine("Message	: {0}", message);
            log.Flush();
        }
    }
    public static string FileName
    {
        get
        {
            return mFileName;
        }
    }
}