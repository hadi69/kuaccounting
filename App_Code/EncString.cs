﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EncString
/// </summary>
public class EncString
{
	public EncString()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static string ConverToIranSys(string str)
    {
        string res = "";
        List<char> spaces = new List<char>(new char[] { ' ', ',', '.', ':', '-', '?', '/', '\\', '(', ')', '{', '}', '[', ']' });
        for (int i = 0; i < str.Length; i++)
        {
            bool hasPreChar = false, hasNextChar = false;
            bool alone = true;
            if (i > 0 && spaces.Contains(str[i - 1]))
                hasPreChar = true;
            if (i < str.Length - 2 && spaces.Contains(str[i + 1]))
                hasNextChar = true;
            if (hasPreChar || hasNextChar)
                alone = false;

            switch (str[i])
            {
                case 'آ':
                    res += (char) 141;
                    break;
                case 'ئ':
                    res += (char) 142;
                    break;
                case 'ا':
                    res += alone ? (char)144 : (char)145;
                    break;
                case 'ب':
                    res += alone ? (char)146 : (char)147;
                    break;
                case 'پ':
                    res += alone ? (char)148 : (char)149;
                    break;
                case 'ت':
                    res += alone ? (char)150 : (char)151;
                    break;
                case 'ث':
                    res += alone ? (char)152 : (char)153;
                    break;
                case 'ج':
                    res += alone ? (char)154 : (char)155;
                    break;
                case 'چ':
                    res += alone ? (char)156 : (char)157;
                    break;
                case 'ح':
                    res += alone ? (char)158 : (char)159;
                    break;
                case 'خ':
                    res += alone ? (char)160 : (char)161;
                    break;
                case 'د':
                    res += (char) 162;
                    break;
                case 'ذ':
                    res +=  (char) 163;
                    break;
                case 'ر':
                    res += (char) 164;
                    break;
                case 'ز':
                    res += (char)165;
                    break;
                case 'ژ':
                    res +=(char) 166;
                    break;
                case 'س':
                    res += alone ? (char)167 : (char)168;
                    break;
                case 'ش':
                    res += alone ? (char)169 : (char)170;
                    break;
                case 'ص':
                    res += alone ? (char)171 : (char)172;
                    break;
                case 'ض':
                    res += alone ? (char)173 : (char)174;
                    break;
                case 'ط':
                    res += (char) 175;
                    break;
                case 'ظ':
                    res +=(char)224;
                    break;
            }
        }
        return res;
    }
}