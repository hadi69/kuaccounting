﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Data;
using Newtonsoft.Json;

/// <summary>
/// Summary description for Toolbelt
/// </summary>
public static class Toolbelt
{
    public static string SerializeDataTable(DataTable table)
    {
        var stringWriter = new StringWriter();
        var jsonWriter = new JsonTextWriter(stringWriter);
        TableInfo info = null;
        info = TableController.Instance[table.TableName];

        jsonWriter.WriteStartArray();

        for (int i = 0; i < table.Rows.Count; i++)
        {
            jsonWriter.WriteStartObject();
            for (int j = 0; j < table.Columns.Count; j++)
            {
                if (info != null)
                {
                    FieldInfo f = info.GetField(table.Columns[j].ColumnName);
                    DataColumn column = table.Columns[j];
                    var serializer = new JsonSerializer();
                    if (column.ColumnName == "ID")
                    {
                        jsonWriter.WritePropertyName(column.ColumnName.ToLower());
                    }
                    else
                    {
                        jsonWriter.WritePropertyName(column.ColumnName);
                    }


                    if (f != null && f.Type.ToLower().StartsWith("date"))
                    {
                        serializer.Serialize(jsonWriter, Helper.FormatDate(Helper.GetDateTime(table.Rows[i][j], Helper.NullDate), null));
                        continue;
                    }
                    if (f != null && f.Encoded)
                    {
                        try
                        {
                            serializer.Serialize(jsonWriter, EncDec.Decrypt(Helper.GetString(table.Rows[i][j], null), GetEnDeP()));
                        }
                        catch
                        {
                            serializer.Serialize(jsonWriter, Helper.GetString(table.Rows[i][j], null));
                        }
                        continue;
                    }
                    serializer.Serialize(jsonWriter, (table.Rows[i][j]).ToString());
                }
            }
            jsonWriter.WriteEndObject();
        }

        jsonWriter.WriteEndArray();

        return stringWriter.ToString();
    }

    public static string ResponseError(string message)
    {
        return ResponseMessage("Error", message);
    }

    public static string ResponseSuccess(string message)
    {
        return ResponseMessage("Success", message);
    }


    private static string ResponseMessage(string type, string message)
    {
        
        var stringWriter = new StringWriter();
        JsonWriter jsonWriter = new JsonTextWriter(stringWriter);

        jsonWriter.WriteStartObject();

        jsonWriter.WritePropertyName(type);
        jsonWriter.WriteValue(message);

        jsonWriter.WriteEndObject();

        return stringWriter.ToString();
    }

    public static string GetEnDeP()
    {
        byte[] b = new byte[] { 116, 117, 114, 107, 107, 101, 114, 101, 64, 107, 110, 107, 109, 107, 122, 122, 113, 110, 97, 33, 36, 37, 49 };
        return System.Text.UTF8Encoding.UTF8.GetString(b);
    }
}