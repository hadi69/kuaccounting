﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using ZedGraph;

/// <summary>
/// Summary description for FormulaEvaluater
/// </summary>
public class FormulaEvaluater
{
    public static int ConstID { get; set; }
    public static int OrderID { get; set; }
    public static int FishID { get; set; }
    public static List<string[]> FormFields { get; set; }
    public static string Formula { get; set; }
    public static FieldsType FieldType { get; set; }
    public static int FieldID{ get; set; }

    public static List<FormField> FormFieldsInfo =new List<FormField>(); 

    public static List<OrderValueInfo> OrderValueInfos =new List<OrderValueInfo>();

    public static List<FishValueInfo> FishValueInfos = new List<FishValueInfo>();
    public static double CalcValue(string field)
    {
        double res = 0;
        if (!IsField(field))
            return -1;
        Formula = GetFormula(field);
        FieldType = Helper.GetFieldType(field);
        FieldID = Helper.GetFieldID(field);
        if (Formula == "")
            return 0;

        if (!string.IsNullOrEmpty(field))
        {
            if (Formula.StartsWith("IIF"))
            {
                res = Math.Round(GetIFFResult(Formula));
            }
            else
            {
                Formula = Formula.Replace("FieldID", FieldID.ToString());
                res = Math.Round(Evaluate(Formula));
            }
            if (res >= 0)
            {
                FormFieldsInfo.Find(x => x.FieldName.ToLower() == field.ToLower()).Value =  res.ToString();
                return res;
            }
                
        }
        return 0;
    }

    static string GetFormula(string field)
    {
        string formula = null;
        string query = string.Format("SELECT Formula FROM Fields WHERE Name=N'{0}'", field);
        formula = DbProxy.Instance.FillScalarString(query);
        return formula;
    }

    static bool IsField(string field)
    {
        int cnt = 0;
        string query = string.Format("SELECT Count(*) FROM Fields WHERE Name=N'{0}'", field);
        cnt = int.Parse(DbProxy.Instance.FillScalarString(query));
        if (cnt > 0)
            return true;
        return false;
    }
    private static double Evaluate(string formula)
    {
        string inner = FindInnerFormula(formula);
        if (formula.IndexOf(',') > 0)
        {
            formula = formula.Substring(formula.IndexOf('(') + 1, formula.LastIndexOf(')') - formula.IndexOf('(') - 1);
        }
        while (inner != null)
        {
            // inner = inner.Replace("-", "+#");
            formula = formula.Replace(string.Concat("(", inner, ")"), EvaluateInner(inner).ToString());
            inner = FindInnerFormula(formula);
        }
        return EvaluateInner(formula);
    }
    private static string FindInnerFormula(string formula)
    {
        int posL, posR = formula.IndexOf(')');
        if (posR > 0)
        {
            posL = formula.LastIndexOf('(', posR);
            return formula.Substring(posL + 1, posR - posL - 1);
        }
        return null;
    }
    private static double EvaluateInner(string inner)
    {
        if (inner.IndexOf('+') < 0)
            return EvaluateMinus(inner);
        string[] pluses = inner.Split(new char[] { '+' });
        double res = 0;
        for (int i = 0; i < pluses.Length; i++)
            res += EvaluateMinus(pluses[i]);
        return res;
    }
    private static bool EqualsToAnyOperator(char ch)
    {
        if (ch == '+')
            return true;
        if (ch == '*')
            return true;
        if (ch == '/')
            return true;
        if (ch == '~')
            return true;
        if (ch == '^')
            return true;
        return false;
    }
    private static double EvaluateMinus(string plus)
    {
        if (plus.IndexOf('-') < 0)
            return EvaluateMul(plus);
        if (plus.StartsWith("-"))
            plus = string.Concat("0", plus);
        plus = plus.Replace("--", "-0-");
        string[] minuses = plus.Split(new char[] { '-' });
        List<string> modified = new List<string>();
        modified.Add(minuses[0]);
        string last = modified[modified.Count - 1];
        for (int i = 1; i < minuses.Length; i++)
        {
            if (EqualsToAnyOperator(last[last.Length - 1]))
                modified[modified.Count - 1] = string.Format("{0}-{1}", last, minuses[i]);
            else
                modified.Add(minuses[i]);
            last = modified[modified.Count - 1];
        }
        if (minuses.Length != modified.Count)
        {
            minuses = new string[modified.Count];
            modified.CopyTo(minuses);
        }
        double res = EvaluateMul(minuses[0]);
        for (int i = 1; i < minuses.Length; i++)
            res -= EvaluateMul(minuses[i]);
        return res;
    }
    private static double EvaluateMul(string minus)
    {
        if (minus.IndexOf('*') < 0)
            return EvaluateDiv(minus);
        string[] muls = minus.Split(new char[] { '*' });
        double res = 1;
        for (int i = 0; i < muls.Length; i++)
            res *= EvaluateDiv(muls[i]);
        return res;
    }
    private static double EvaluateDiv(string mul)
    {
        if (mul.IndexOf('/') < 0)
            return EvaluateMax(mul);
        string[] divs = mul.Split(new char[] { '/' });
        double res = EvaluateMax(divs[0]);
        for (int i = 1; i < divs.Length; i++)
            res /= EvaluateMax(divs[i]);
        return res;
    }
    private static double EvaluateMax(string div)
    {
        if (div.IndexOf('^') < 0)
            return EvaluateMin(div);
        string[] maxs = div.Split(new char[] { '^' });
        double res = EvaluateMin(maxs[0]);
        for (int i = 1; i < maxs.Length; i++)
            res = Math.Max(res, EvaluateMin(maxs[i]));
        return res;
    }
    private static double EvaluateMin(string max)
    {
        if (max.IndexOf('~') < 0)
            return EvaluateComma(max);
        string[] mins = max.Split(new char[] { '~' });
        double res = EvaluateValue(mins[0]);
        for (int i = 1; i < mins.Length; i++)
            res = Math.Min(res, EvaluateValue(mins[i]));
        return res;
    }
    private static double EvaluateComma(string min)
    {
        if (min.IndexOf(',') < 0 && FieldType==FieldsType.Formula)
            return EvaluateValue(min);
        double res = 0;
        string formulainner = Formula.Substring(Formula.IndexOf('(') + 1, Formula.LastIndexOf(')') - Formula.IndexOf('(') - 1);
        string[] values = min.Split(',');
        string[] commas = formulainner.Split(',');
        string function = Formula.Replace(commas[0], EvaluateValue(values[0]).ToString());
        for (int i = 1; i < commas.Length; i++)
            if (values[i].IndexOf(';') < 0)
                function = function.Replace(commas[i], EvaluateValue(values[i]).ToString());
        res = Convert.ToDouble(DbProxy.Instance.FillScalar("SELECT " + function));
        return res;
    }
    private static double EvaluateValue(string val)
    {
        double res = 0;
        if (double.TryParse(val, out res))
            return res;
        else
        {
            res = GetFormFieldValue(val);
            if (res >= 0)
            {
                return res;
            }
            else if (ConstID != -1)
            {
                res = GetConstValue(ConstID, val);
                if (res >= 0)
                    return res;
            }
            else 
            {
                if (OrderID != -1)
                  res = GetOrderValue(OrderID, val);
                if (FishID != -1 && res<0)
                {
                    res = GetFishValue(FishID, val);
                }
            }
            
        }

        return res >= 0 ? res : 0;
    }
    private static double GetFormFieldValue(string field)
    {
        int i=0;
        if (FormFields.Count>0)
        {
            if (FormFieldsInfo.Count==0)
            {
                while (i < FormFields.Count)
                {
                    FormField ff=new FormField();
                    string[] f = FormFields[i];
                    ff.FieldName = f[0];
                    ff.Value = f[1];
                    FormFieldsInfo.Add(ff);
                    i++;
                }
            }
            FormField formField = FormFieldsInfo.Find(x => x.FieldName.ToLower() == field.ToLower());
            if (formField!=null)
            {
                return Helper.GetDouble(formField.Value, -1);
            }
        }

        
        return -1;
    }
    private static double GetConstValue(int constid, string constitem)
    {
        double val = -1;
        try
        {
            if (!string.IsNullOrEmpty(constitem))
            {
                string query = string.Format(@"SELECT Value FROM ConstValues 
                                     INNER JOIN ConstDefinitionItems ON ConstValues.ConstDefinitionItemID=ConstDefinitionItems.ID
                                     INNER JOIN ConstItems ON ConstDefinitionItems.ConstItemID=ConstItems.ID
                                     WHERE ConstID={0} AND ConstItems.Name=N'{1}'", constid, constitem);
                val = Convert.ToDouble(DbProxy.Instance.FillScalarString(query));
                if (val >= 0)
                    return val;
            }
        }
        catch(Exception ex)
        {
            ExceptionLog.Instance.Add(ex);
        }
        return val;
    }
    private static double GetOrderValue(int orderid, string fieldName)
    {
        double val = -1;
        OrderValueInfo orderValueInfo=new OrderValueInfo();
        try
        {
            if (!string.IsNullOrEmpty(fieldName))
            {
                if (OrderValueInfos.Count>0)
                {
                    orderValueInfo = OrderValueInfos.Find(x => x.OrderID == orderid && x.FieldName.ToLower()==fieldName.ToLower());
                    val = orderValueInfo.Value;
                }
                else
                {
                    string query = string.Format(@"SELECT  Orders.ID AS OrderID, Fields.Name AS FieldName, Value
                                               FROM dbo.OrderValues
                                               LEFT OUTER JOIN dbo.Orders ON dbo.Orders.ID=dbo.OrderValues.OrderID
                                               LEFT OUTER JOIN dbo.OrderDefFields ON dbo.OrderValues.OrderDefFieldID=dbo.OrderDefFields.ID
                                               LEFT OUTER JOIN dbo.Fields ON dbo.OrderDefFields.FieldID=dbo.Fields.ID
                                               WHERE OrderID={0}",orderid);
                    IDataReader dr = DbProxy.Instance.ExecuteReader(query);
                    OrderValueInfos = ObjectBinder.FillCollection<OrderValueInfo>(dr);
                    if (OrderValueInfos.Count>0)
                    {
                        orderValueInfo = OrderValueInfos.Find(x => x.OrderID == orderid && x.FieldName.ToLower() == fieldName.ToLower());
                        val = orderValueInfo.Value;
                    }
                }
                

                if (val >= 0)
                    return val;
            }
        }
        catch (Exception ex)
        {
            ExceptionLog.Instance.Add(ex);
        }
        return val;
    }
    private static double GetFishValue(int fishid, string fieldName)
    {
        double val = -1;
        FishValueInfo fishValueInfo = new FishValueInfo();
        try
        {
            if (!string.IsNullOrEmpty(fieldName))
            {

                if (FishValueInfos.Count > 0)
                {
                    fishValueInfo = FishValueInfos.Find(x => x.FishID == fishid && x.FieldName.ToLower() == fieldName.ToLower());
                    val = fishValueInfo.Value;
                }
                else
                {
                    string query = string.Format(@"SELECT  Fish.ID AS FishID, Fields.Name AS FieldName, Value 
                                               FROM dbo.FishValues
                                               LEFT OUTER JOIN dbo.Fish ON dbo.Fish.ID=dbo.FishValues.FishID
                                               LEFT OUTER JOIN dbo.FishDefFields ON dbo.FishValues.FishDefFieldID=dbo.FishDefFields.ID
                                               LEFT OUTER JOIN dbo.Fields ON dbo.FishDefFields.FieldID=dbo.Fields.ID
                                               WHERE FishID={0}", fishid);
                    IDataReader dr = DbProxy.Instance.ExecuteReader(query);
                    FishValueInfos = ObjectBinder.FillCollection<FishValueInfo>(dr);
                    if (FishValueInfos.Count > 0)
                    {
                        fishValueInfo = FishValueInfos.Find(x => x.FishID == fishid && x.FieldName.ToLower() == fieldName.ToLower());
                        val = fishValueInfo.Value;
                    }
                }

                if (val >= 0)
                    return val;
            }
        }
        catch (Exception ex)
        {
            ExceptionLog.Instance.Add(ex);
        }
        return val;
    }

    static double GetIFFResult(string formula)
    {
        formula = formula.Substring(4, formula.Length - 5);
        string[] f = formula.Split(',');
        string[] con = f[0].Split('>', '<');
        if (f[0].Contains('>'))
        {
            if (Evaluate(con[0])>Evaluate(con[1]))
            {
                return Evaluate(f[1]);
            }
            else
            {
                return Evaluate(f[2]);
            }
        }
        if (f[0].Contains('<'))
        {
            if (Evaluate(con[0])<Evaluate(con[1]))
            {
                return Evaluate(f[1]);
            }
            else
            {
                return Evaluate(f[2]);
            }
        }
        return 0;
    }
}

public class OrderValueInfo
{
    public int OrderID { get; set; }
    public string FieldName { get; set; }
    public double Value { get; set; }
}

public class FishValueInfo
{
    public int FishID { get; set; }
    public string FieldName { get; set; }
    public double Value { get; set; }
}

public class FormField
{
    public string FieldName { get; set; }
    public string Value { get; set; }
}