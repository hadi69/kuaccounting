﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Xml;
using Microsoft.ApplicationBlocks.Data;

public class DbProxy : IDisposable
{
    private SqlConnection mConnection = null;
    private string mConnectionString = "";
    private SqlTransaction mTransaction = null;

    public string ConnectionString
    {
        get
        {
            return mConnectionString;
        }
        set
        {
            if (value != mConnectionString)
            {
                mConnectionString = value;
                try
                {
                    if (mConnection != null)
                    {
                        mConnection.Close();
                        mConnection.Dispose();
                    }
                }
                catch { }
                mConnection = null;
            }
        }
    }
    private object mLock = new object();
    public static bool AutoDisconnect = false;
    public Exception LastException = null;
    //public static DbProxy Instance = new DbProxy();

    public DbProxy()
    {
        string other = SiteSettings.OtherCatalogName;
        if (string.IsNullOrEmpty(other))
            mConnectionString = SiteSettings.ConnectionString;
        else
        {
            // Server=(local)\SQLEXPRESS2008R2;Database=HIS;uid=pa;pwd=1234;
            mConnection = new SqlConnection(SiteSettings.ConnectionString);
            mConnectionString = mConnection.ConnectionString;
            mConnectionString = mConnection.ConnectionString.Replace(mConnection.Database, other);
            mConnection.Dispose();
            mConnection = null;
        }
    }
    public DbProxy(string dbName)
    {
        string other = dbName;
        if (string.IsNullOrEmpty(other))
            mConnectionString = SiteSettings.ConnectionString;
        else
        {
            // Server=(local)\SQLEXPRESS2008R2;Database=HIS;uid=pa;pwd=1234;
            mConnection = new SqlConnection(SiteSettings.ConnectionString);
            mConnectionString = mConnection.ConnectionString;
            mConnectionString = mConnection.ConnectionString.Replace(mConnection.Database, other);
            mConnection.Dispose();
            mConnection = null;
        }
    }
    public DbProxy(string connectionString, bool dummy)
    {
        mConnectionString = connectionString;
    }
    #region mConnection
    public bool Connected
    {
        get
        {
            return mConnection != null && mConnection.State == ConnectionState.Open;
        }
    }
    private bool EnsureConnection()
    {
        try
        {
            lock (mLock)
            {
                if (mConnection == null)
                    mConnection = new SqlConnection(mConnectionString);
                if (mConnection.State != ConnectionState.Open)
                    mConnection.Open();
            }
            return true;
        }
        catch (Exception ex)
        {
            LastException = ex;
            Logger.Log(ex, " ConnectionString=" + mConnectionString);
        }
        return false;
    }

    public void CloseConnection()
    {
        try
        {
            if (mConnection != null)
                if (mConnection.State != ConnectionState.Closed)
                {
                    mConnection.Close();
                    mConnection.Dispose();
                }
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
        }
        mConnection = null;
    }
    #endregion

    public string CatalogName
    {
        get
        {
            if (mConnection != null)
                return mConnection.Database;
            if (EnsureConnection())
            {
                string res = mConnection.Database;
                CloseConnection();
                return res;
            }
            return "HIS";
        }
    }

    public IDataReader ExecuteReader(string qry)
    {
        if (!EnsureConnection())
            return null;
        try
        {
            lock (mLock)
            {
                SqlCommand command = new SqlCommand(qry, mConnection);
                return command.ExecuteReader(CommandBehavior.CloseConnection);
            }
        }
        catch (Exception ex)
        {
            LastException = ex;
            ExceptionLog.Instance.Add(ex, qry);
            mConnection.Close();
            mConnection = null;
        }
        return null;
    }
    public IDataReader ExecuteReader(string spName, params object[] parameterValues)
    {
        if (!EnsureConnection())
            return null;
        try
        {
            SqlDataReader res = mTransaction == null ? SqlHelper.ExecuteReader(mConnection, spName, parameterValues)
                : SqlHelper.ExecuteReader(mTransaction, spName, parameterValues);
            return res;
            //using (SqlCommand command = mTransaction == null ? new SqlCommand(spName, mConnection) : new SqlCommand(spName, mConnection, mTransaction))
            ////using (SqlCommand command = new SqlCommand(qry, mConnection))
            //{
            //    command.CommandTimeout = 300;
            //    command.CommandType = CommandType.StoredProcedure;
            //    command.Parameters.AddRange(parameterValues);
            //    return command.ExecuteReader(CommandBehavior.CloseConnection);
            //}
        }
        catch (Exception ex)
        {
            ExceptionLog.Instance.Add(ex, spName);
        }
        finally
        {
            if (AutoDisconnect)
                CloseConnection();
        }
        return null;
    }
    public DataTable FillData(string qry, string tableName)
    {
        if (!EnsureConnection())
            return null;
        DataTable dt = new DataTable(tableName);
        try
        {
            lock (mLock)
            {
                using (SqlDataAdapter adpater = new SqlDataAdapter(qry, mConnection))
                {
                    if (mTransaction != null)
                        adpater.SelectCommand.Transaction = mTransaction;
                    adpater.Fill(dt);
                }
            }
        }
        catch (Exception ex)
        {
            LastException = ex;
            ExceptionLog.Instance.Add(ex, qry);
            dt.Dispose();
            dt = null;
            mConnection.Close();
            mConnection = null;
            return dt;
        }
        finally
        {
            if (AutoDisconnect)
                CloseConnection();
        }
        return dt;
    }
    public DataTable FillData(string qry, string tableName, int timeout)
    {
        if (!EnsureConnection())
            return null;
        DataTable dt = new DataTable(tableName);
        try
        {
            lock (mLock)
                using (SqlDataAdapter adpater = new SqlDataAdapter(qry, mConnection))
                {
                    adpater.SelectCommand.CommandTimeout = timeout;
                    adpater.Fill(dt);
                }
        }
        catch (Exception ex)
        {
            LastException = ex;
            ExceptionLog.Instance.Add(ex, qry);
            dt.Dispose();
            dt = null;
            mConnection.Close();
            mConnection = null;
            return dt;
        }
        finally
        {
            if (AutoDisconnect)
                CloseConnection();
        }
        return dt;
    }
    public DataSet FillDataSet(string qry)
    {
        if (!EnsureConnection())
            return null;
        DataSet ds = new DataSet();
        try
        {
            lock (mLock)
                using (SqlDataAdapter adpater = new SqlDataAdapter(qry, mConnection))
                {
                    if (mTransaction != null)
                        adpater.SelectCommand.Transaction = mTransaction;
                    adpater.Fill(ds);
                }
        }
        catch (Exception ex)
        {
            LastException = ex;
            ExceptionLog.Instance.Add(ex, qry);
            ds.Dispose();
            ds = null;
            mConnection.Close();
            mConnection = null;
            return ds;
        }
        finally
        {
            if (AutoDisconnect)
                CloseConnection();
        }
        return ds;
    }
    public DataRow FillRow(string qry)
    {
        if (!EnsureConnection())
            return null;
        DataTable dt = new DataTable();

        try
        {
            lock (mLock)
            {
                using (SqlDataAdapter adpater = new SqlDataAdapter(qry, mConnection))
                    adpater.Fill(dt);

                return (dt.Rows.Count > 0) ? dt.Rows[0] : null;
            }
        }
        catch (Exception ex)
        {
            ExceptionLog.Instance.Add(ex, qry);
            dt.Dispose();
            dt = null;
            mConnection.Close();
            mConnection = null;
        }
        finally
        {
            if (AutoDisconnect)
                CloseConnection();
        }
        return null;
    }
    public object FillScalar(string qry)
    {
        //DataSet ds = FillDataSet(qry);
        //if (ds == null)
        //    return null;
        //try
        //{
        //    if (ds.Tables.Count == 0)
        //        return null;
        //    if (ds.Tables[ds.Tables.Count - 1].Rows.Count == 0)
        //        return null;
        //    return ds.Tables[ds.Tables.Count - 1].Rows[0][0];
        //}
        //finally
        //{
        //    ds.Dispose();
        //    ds = null;
        //}

        if (!EnsureConnection())
            return null;
        DataSet ds = new DataSet();
        try
        {
            lock (mLock)
            {
                using (SqlCommand command = mTransaction == null ? new SqlCommand(qry, mConnection) : new SqlCommand(qry, mConnection, mTransaction))
                    return command.ExecuteScalar();
            }
        }
        catch (SqlException sex)
        {
            Exception e = null;
            if (sex.Errors != null && sex.Errors.Count > 0)
                for (int i = 0; i < sex.Errors.Count; i++)
                    if (sex.Errors[i].Message != null && sex.Errors[i].Message.StartsWith("_HIS_EX_"))
                    {
                        e = new Exception(sex.Errors[i].Message.Substring("_HIS_EX_".Length), sex);
                        break;
                    }
            if (e == null)
                e = sex;
            LastException = e;
            ExceptionLog.Instance.Add(e, qry);
        }
        catch (Exception ex)
        {
            if (ex.Message != null && ex.Message.StartsWith("_HIS_EX_"))
                ex = new Exception(ex.Message.Substring("_HIS_EX_".Length), ex);
            LastException = ex;
            ExceptionLog.Instance.Add(ex, qry);
            mConnection.Close();
            mConnection = null;
        }
        finally
        {
            if (AutoDisconnect)
                CloseConnection();
            if (ds != null)
                ds.Dispose();
        }
        return null;
    }
    public string FillScalarString(string qry)
    {
        if (!EnsureConnection())
            return null;
        try
        {
            string sc;
            using (SqlCommand command = mTransaction == null ? new SqlCommand(qry, mConnection) : new SqlCommand(qry, mConnection, mTransaction))
            //using (SqlCommand command = new SqlCommand(qry, mConnection))
            {
                sc = Helper.GetString(command.ExecuteScalar(), "");
                return sc;
            }
        }
        catch (Exception ex)
        {
            LastException = ex;
            ExceptionLog.Instance.Add(ex, qry);
            mConnection.Close();
            mConnection = null;
        }
        finally
        {
            if (AutoDisconnect)
                CloseConnection();
        }
        return "";
    }
    public object FillScalar(string spName, params object[] parameterValues)
    {
        if (!EnsureConnection())
            return null;
        try
        {
            lock (mLock)
                using (SqlCommand command = mTransaction == null ? new SqlCommand(spName, mConnection) : new SqlCommand(spName, mConnection, mTransaction))
                //using (SqlCommand command = new SqlCommand(qry, mConnection))
                {
                    command.CommandTimeout = 300;
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddRange(parameterValues);
                    return command.ExecuteScalar();
                }
        }
        catch (Exception ex)
        {
            ExceptionLog.Instance.Add(ex, spName);
        }
        finally
        {
            if (AutoDisconnect)
                CloseConnection();
        }
        return null;
    }

    #region Run Query
    /// <summary>
    /// Executes a NonQuery Command and Returns the Number of Rows Affected(ie. DELETE, UPDATE, INSERT)
    /// </summary>
    /// <param name="qry"></param>
    /// <returns>
    ///		more than 0 : Number of rows affected
    ///		0			: No error but no rows affected
    ///		-1			: Errors occured
    /// </returns>
    public bool ExecuteCommand(string qry)
    {
        return ExecuteCommand(qry, 30);
    }
    // Transaction Enabled
    public bool ExecuteCommand(string qry, int timeOut)
    {
        if (SiteSettings.IsDbReadonly)
            return false;
        if (!EnsureConnection())
            return false;
        try
        {
            lock (mLock)
                using (SqlCommand command = mTransaction == null ? new SqlCommand(qry, mConnection) : new SqlCommand(qry, mConnection, mTransaction))
                //using (SqlCommand command = new SqlCommand(qry, mConnection))
                {
                    command.CommandTimeout = timeOut;
                    command.ExecuteNonQuery();
                    command.Dispose();
                    return true;
                }
        }
        catch (Exception ex)
        {
            ExceptionLog.Instance.Add(ex, qry);
        }
        finally
        {
            if (AutoDisconnect)
                CloseConnection();
        }
        return false;
    }

    public int RunQuery(string qry)
    {
        return RunQuery(qry, false);
    }
    public int RunQuery(string qry, bool ignoreReadonlyDB)
    {
        if (!ignoreReadonlyDB)
            if (SiteSettings.IsDbReadonly)
                return -1;
        if (!EnsureConnection())
            return -1;
        lock (mLock)
            try
            {
                int Num;
                using (SqlCommand command = mTransaction == null ? new SqlCommand(qry, mConnection) : new SqlCommand(qry, mConnection, mTransaction))
                //using (SqlCommand command = new SqlCommand(qry, mConnection))
                {
                    command.CommandTimeout = 600;
                    Num = command.ExecuteNonQuery();
                    //command.Dispose();
                    //mConnection.Close();
                    //mConnection = null;
                    return Num;
                }
            }
            catch (SqlException sex)
            {
                Exception e = null;
                if (sex.Errors != null && sex.Errors.Count > 0)
                    for (int i = 0; i < sex.Errors.Count; i++)
                        if (sex.Errors[i].Message != null && sex.Errors[i].Message.StartsWith("_HIS_EX_"))
                        {
                            e = new Exception(sex.Errors[i].Message.Substring("_HIS_EX_".Length), sex);
                            break;
                        }
                if (e == null)
                    e = sex;
                LastException = e;
                ExceptionLog.Instance.Add(e, qry);
            }
            catch (Exception ex)
            {
                LastException = ex;
                ExceptionLog.Instance.Add(ex, qry);
            }
            finally
            {
                if (AutoDisconnect)
                    CloseConnection();
            }
        return -1;
    }
    public int RunQuery(string qry, int TimeOut)
    {
        return RunQuery(qry, TimeOut, false);
    }
    public int RunQuery(string qry, int TimeOut, bool ignoreReadonlyDB)
    {
        if (!ignoreReadonlyDB)
            if (SiteSettings.IsDbReadonly)
                return -1;
        if (!EnsureConnection())
            return -1;
        try
        {
            int Num;
            using (SqlCommand command = mTransaction == null ? new SqlCommand(qry, mConnection) : new SqlCommand(qry, mConnection, mTransaction))
            //using (SqlCommand command = new SqlCommand(qry, mConnection))
            {
                command.CommandTimeout = TimeOut;
                Num = command.ExecuteNonQuery();
                command.Dispose();
                return (1);
            }
        }
        catch (Exception ex)
        {
            LastException = ex;
            ExceptionLog.Instance.Add(ex, qry);
        }
        finally
        {
            if (AutoDisconnect)
                CloseConnection();
        }
        return -1;
    }
    public int RunSP(string spName, string paramName, string value, int TimeOut)
    {
        if (SiteSettings.IsDbReadonly)
            return -1;
        if (!EnsureConnection())
            return -1;
        try
        {
            int Num;
            using (SqlCommand command = mTransaction == null ? new SqlCommand(spName, mConnection) : new SqlCommand(spName, mConnection, mTransaction))
            //using (SqlCommand command = new SqlCommand(qry, mConnection))
            {
                command.CommandTimeout = TimeOut;
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue(paramName, value);
                Num = command.ExecuteNonQuery();
                command.Dispose();
                return (1);
            }
        }
        catch (Exception ex)
        {
            ExceptionLog.Instance.Add(ex, spName);
        }
        finally
        {
            if (AutoDisconnect)
                CloseConnection();
        }
        return -1;
    }
    public int RunSP(string spName, string[] paramNames, object[] values, int TimeOut)
    {
        if (SiteSettings.IsDbReadonly)
            return -1;
        if (!EnsureConnection())
            return -1;
        try
        {
            int Num;
            using (SqlCommand command = mTransaction == null ? new SqlCommand(spName, mConnection) : new SqlCommand(spName, mConnection, mTransaction))
            //using (SqlCommand command = new SqlCommand(qry, mConnection))
            {
                command.CommandTimeout = TimeOut;
                command.CommandType = CommandType.StoredProcedure;
                if (paramNames != null)
                    for (int i = 0; i < paramNames.Length; i++)
                        command.Parameters.AddWithValue(paramNames[i], values[i]);
                Num = command.ExecuteNonQuery();
                command.Dispose();
                return (1);
            }
        }
        catch (Exception ex)
        {
            LastException = ex;
            ExceptionLog.Instance.Add(ex, spName);
        }
        finally
        {
            if (AutoDisconnect)
                CloseConnection();
        }
        return -1;
    }
    #endregion

    public void BeginTransaction()
    {
        if (mTransaction != null)
            throw new Exception("Transaction is already started");
        EnsureConnection();
        mTransaction = mConnection.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);
    }
    /// <summary>
    /// Commits an open Transaction if any
    /// </summary>
    public void CommitTransaction()
    {
        if (mTransaction == null)
            return;
        mTransaction.Commit();
        mTransaction = null;
        mConnection.Close();
        mConnection = null;
    }
    /// <summary>
    /// Rollbacks an open Transaction if any
    /// </summary>
    public void RollbackTransaction()
    {
        try
        {
            try
            {
                if (mTransaction != null)
                    mTransaction.Rollback();
            }
            catch { }
            try
            {
                if (mConnection != null)
                    mConnection.Close();
            }
            catch { }
        }
        catch (Exception ex)
        {
            ExceptionLog.Instance.Add(ex);
        }
        mTransaction = null;
        mConnection = null;
    }
    #region Config
    // todo: Cache Config
    public string GetConfig(string name, int programID)
    {
        return GetConfig(name, "", programID);
    }
    public string GetConfig(string name, string defaultValue, int programID)
    {
        string qry = "SELECT [Value] FROM Config WHERE [Name]=" + Helper.QoutedString(name);
        if (programID <= 0)
            qry += " AND ProgramID IS NULL";
        else
            qry += " AND ProgramID=" + programID;
        object val = FillScalar(qry);
        string res = Helper.GetString(val, null);
        if (string.IsNullOrEmpty(res))
            return defaultValue;
        // it is encoded
        try
        {
            return EncDec.Decrypt(res, GetEnDeP());
        }
        catch
        {
            return res;
        }
        return "";
    }
    public string GetUserConfig(string name, string defaultValue, int userID)
    {
        string qry = "SELECT [Value] FROM UserConfig WHERE [Name]=" + Helper.QoutedString(name);
        qry += " AND UserID=" + userID;
        object val = FillScalar(qry);
        string res = Helper.GetString(val, null);
        if (string.IsNullOrEmpty(res))
            return defaultValue;
        // it is encoded
        try
        {
            return EncDec.Decrypt(res, GetEnDeP());
        }
        catch
        {
            return res;
        }
        return "";
    }
    public bool SetConfig(string name, string value, int programID, bool encrypt)
    {
        int res = 0;
        if (encrypt)
            value = EncDec.Encrypt(value, GetEnDeP());
        string progCond = "";
        if (programID <= 0)
            progCond = " AND ProgramID IS NULL";
        else
            progCond = " AND ProgramID=" + programID;
        object count = FillScalar("SELECT COUNT(*) FROM Config WHERE [Name]=" + Helper.QoutedString(name) + progCond);
        if (Helper.GetInt(count, 0) > 0)
            res = RunQuery(string.Format("UPDATE Config SET [Value]={0} WHERE [Name]={1} {2}", Helper.QoutedString(value), Helper.QoutedString(name), progCond));
        else
            res = RunQuery(string.Format("INSERT INTO Config ([Name], [Value], ProgramID) VALUES ({0}, {1}, {2})", Helper.QoutedString(name), Helper.QoutedString(value), programID <= 0 ? "NULL" : programID.ToString()));
        return res > 0;
    }
    #endregion

    #region Option
    // todo: Cache Config
    public string GetOption(int userID, string name)
    {
        return GetOption(userID, name, "");
    }
    public string GetOption(int userID, string name, string defaultValue)
    {
        object val = FillScalar("SELECT [Value] FROM Options WHERE [Name]=" + Helper.QoutedString(name) + " AND UserID = " + userID);
        string res = Helper.GetString(val, null);
        if (string.IsNullOrEmpty(res))
            return defaultValue;
        return res;
    }
    public bool SetOption(int userID, string name, string value)
    {
        int res = 0;
        object count = FillScalar("SELECT COUNT(*) FROM Options WHERE [Name]=" + Helper.QoutedString(name) + " AND UserID = " + userID);
        if (Helper.GetInt(count, 0) > 0)
            res = RunQuery(string.Format("UPDATE Options SET [Value]={0} WHERE UserID = {1} AND [Name]={2}", Helper.QoutedString(value), userID, Helper.QoutedString(name)));
        else
            res = RunQuery(string.Format("INSERT INTO Options (UserID, [Name], [Value]) VALUES ({0}, {1}, {2})", userID, Helper.QoutedString(name), Helper.QoutedString(value)));
        return res > 0;
    }
    #endregion
    #region Report
    public ReportFile GetReport(int id)
    {
        if (id < 1)
            return null;
        DataRow dr = FillRow("SELECT * FROM Reports WHERE ID=" + id);
        if (dr == null)
            return null;
        return new ReportFile(dr);
    }
    public ReportFile GetReport(string name)
    {
        if (string.IsNullOrEmpty(name))
            return null;
        DataRow dr = FillRow("SELECT * FROM Reports WHERE [Name]=" + Helper.QoutedString(name));
        if (dr == null)
            return null;
        return new ReportFile(dr);
    }
    public bool SaveReport(ReportFile report)
    {
        if (SiteSettings.IsDbReadonly)
            return false;
        string query = "";
        XmlDocument xmlDoc = report.SaveToXml();
        if (report.ID < 1)
        {
            query = string.Format("INSERT INTO Reports ([Name], [Query], XmlDoc) VALUES ({0}, {1}, {2});SELECT SCOPE_IDENTITY();", Helper.QoutedString(report.Name), Helper.QoutedString(report.Query), Helper.QoutedString(xmlDoc.OuterXml));
            object id = FillScalar(query);
            report.ID = Helper.GetInt(id, -1);
            return report.ID > 0;
        }
        query = string.Format("UPDATE Reports SET [Name]={0}, [Query]={1}, XmlDoc={2} WHERE ID={3}", Helper.QoutedString(report.Name), Helper.QoutedString(report.Query), Helper.QoutedString(xmlDoc.OuterXml), report.ID);
        return RunQuery(query) > 0;
    }
    #endregion
    #region User
    public UserInfo GetUser(string username)
    {
        DataRow row = FillRow(string.Format("SELECT Users.* FROM Users WHERE Username={0}"
            , Helper.QoutedString(username)));
        if (row == null)
            return null;
        return GetUser(row);
    }
    public UserInfo GetUserByMobile(string mobile)
    {
        DataRow row = FillRow(string.Format("SELECT Users.* FROM Users WHERE Mobile={0}"
            , Helper.QoutedString(mobile)));
        if (row == null)
            return null;
        return GetUser(row);
    }
    public UserInfo GetUser(string username, string password)
    {
        string password1 = EncDec.Encrypt(password, GetEnDeP());
        DataRow row = FillRow(string.Format("SELECT Users.* FROM Users WHERE Username={0} AND (Password={1} OR Password={2})"
            , Helper.QoutedString(username), Helper.QoutedString(password), Helper.QoutedString(password1)));
        if (row == null)
            return null;
        return GetUser(row);
    }
    public UserInfo GetUser(int id)
    {
        DataRow row = FillRow(string.Format("SELECT Users.*, (Personnel.Name+' '+Personnel.Surname) AS PersonnelName, Personnel.CellNumber1 AS PersonnelMobile From Users LEFT JOIN Personnel ON Users.PersonnelID=Personnel.ID WHERE Users.ID={0}"
            , id));
        if (row == null)
            row = FillRow(string.Format("SELECT *, null AS PersonnelName From Users WHERE Users.ID={0}", id));
        return GetUser(row);
    }
    public string GetUserFullName(int id)
    {
        UserInfo info = GetUser(id);
        return info == null ? "" : info.Username;
    }
    private string GetEnDeP()
    {
        //string res = "turkkere@knkmkzzqna!$%1";
        //byte[] b = System.Text.UTF8Encoding.UTF8.GetBytes(res);
        //string tt = "";
        //for (int i = 0; i < b.Length; i++)
        //{
        //    tt += b[i].ToString() + ", ";
        //}
        byte[] b = new byte[] { 116, 117, 114, 107, 107, 101, 114, 101, 64, 107, 110, 107, 109, 107, 122, 122, 113, 110, 97, 33, 36, 37, 49 };
        return System.Text.UTF8Encoding.UTF8.GetString(b);
    }
    UserInfo GetUser(DataRow row)
    {
        if (row == null)
            return null;
        UserInfo res = new UserInfo();
        res.ID = Helper.GetInt(row, "ID", -1);
        res.Username = Helper.GetString(row, "Username", "");
        res.Password = Helper.GetString(row, "Password", "");
        try
        {
            res.Password = EncDec.Decrypt(res.Password, GetEnDeP());
        }
        catch { }
        res.Fullname = Helper.GetString(row, "Fullname", "");
        res.IsAdmin = Helper.GetBool(row, "IsAdmin", true);
        res.IsStoreUser = Helper.GetBool(row, "IsStoreUser", false);
        res.OnVacation = Helper.GetBool(row, "OnVacation", false);
        res.Enabled = Helper.GetBool(row, "Enabled", true);
        res.PersonnelID = Helper.GetInt(row, "PersonnelID", -1);
        res.PersonnelName = Helper.GetString(row, "PersonnelName", "");
        res.Comments = Helper.GetString(row, "Comments", "");
        res.Mobile = Helper.GetString(row, "PersonnelMobile", Helper.GetString(row, "Mobile", ""));
        res.RegisterDate = Helper.GetDateTime(row, "RegisterDate", Null.NullDate);

        bool isAdmin;
        res.Access = GetAccess(res.ID, out isAdmin);
        if (isAdmin)
            res.IsAdmin = true;
        res.WardIDs = Helper.GetString(row, "WardIDs", "");
        res.StartTime = Helper.GetString(row, "StartTime", null);
        res.EndTime = Helper.GetString(row, "EndTime", null);
        return res;
    }
    public bool UpdateUserPass(int id, string password)
    {
        if (SiteSettings.IsDbReadonly)
            return false;
        return RunQuery(string.Format("UPDATE Users SET Password={0} WHERE ID={1}"
            , Helper.QoutedString(password), id)) > 0;
    }
    public bool UpdateUserOnVacation(int id, bool onVacation)
    {
        if (SiteSettings.IsDbReadonly)
            return false;
        return RunQuery(string.Format("UPDATE Users SET OnVacation={0} WHERE ID={1}"
            , onVacation ? 1 : 0, id)) > 0;
    }
    string GetAccess(int userID, out bool isAdmin)
    {
        string res = "";
        isAdmin = false;
        DataTable roles = FillData("SELECT Roles.Access, Roles.ID FROM UserRole INNER JOIN Roles ON UserRole.RoleID=Roles.ID WHERE UserID=" + userID, "roles");
        if (roles != null)
        {
            for (int i = 0; i < roles.Rows.Count; i++)
            {
                string access = Helper.GetString(roles.Rows[i], "Access", null);
                if (!string.IsNullOrEmpty(access))
                    res += access + ";";
            }
            roles.Dispose();
        }

        // check for vacations
        if (true)
        {
            DataTable vRoles = FillData(string.Format(@"SELECT Roles.Access, Roles.ID, Users.IsAdmin FROM UserRole 
                                    INNER JOIN Roles ON UserRole.RoleID = Roles.ID 
                                    INNER JOIN Users ON UserRole.UserID = Users.ID 
WHERE UserID <> {0}   -- it is not the current user
AND Users.OnVacation = 1 -- and it is on vacation
AND Users.SubstituteRoleID IN (SELECT RoleID FROM UserRole WHERE UserID = {0}) -- and the SubstituteRoleID is in current user's role
            ", userID), "roles");
            if (vRoles != null && vRoles.Rows.Count > 0)
            {
                for (int i = 0; i < vRoles.Rows.Count; i++)
                {
                    string access = Helper.GetString(vRoles.Rows[i], "Access", null);
                    if (Helper.GetBool(vRoles.Rows[i], "IsAdmin", false))
                        isAdmin = true;
                    if (!string.IsNullOrEmpty(access))
                        res += access + ";";
                }
            }
            if (vRoles != null)
                vRoles.Dispose();
        }

        List<string> distinct = new List<string>();
        string[] all = res.Split(';');
        for (int i = 0; i < all.Length; i++)
            if (!string.IsNullOrEmpty(all[i]) && !distinct.Contains(all[i]))
                distinct.Add(all[i]);
        res = string.Join(";", distinct) + ";";
        return res;
    }
    #endregion

    public string GetEnumTitle(string enumTable, int ID)
    {
        string qry = "SELECT Title FROM " + enumTable + " WHERE ID=" + ID;
        object val = FillScalar(qry);
        return Helper.GetString(val, null);
    }
    #region Forms
    public FormDeclare GetFormDeclare(int id)
    {
        DataRow dr = FillRow(string.Format("SELECT * FROM FormDeclare WHERE ID={0}", id));
        if (dr == null)
            return null;
        FormDeclare res = new FormDeclare(dr);
        dr.Table.Dispose();
        return res;
    }
    public FormDeclare GetFormDeclare(string code)
    {
        DataRow dr = FillRow(string.Format("SELECT * FROM FormDeclare WHERE Code={0}", Helper.QoutedString(code)));
        if (dr == null)
            return null;
        FormDeclare res = new FormDeclare(dr);
        dr.Table.Dispose();
        return res;
    }
    public FormDeclare GetFormDeclareByName(string code)
    {
        DataRow dr = FillRow(string.Format("SELECT * FROM FormDeclare WHERE Name={0}", Helper.QoutedString(code)));
        if (dr == null)
            return null;

        FormDeclare res = new FormDeclare(dr);
        dr.Table.Dispose();
        return res;
    }
    public List<FormDeclareItem> GetFormDeclareItems(int formDeclareID)
    {
        DataTable dt = FillData(string.Format("SELECT * FROM FormDeclareItem WHERE FormDeclareID={0} ORDER BY SortIndex", formDeclareID), "f");
        if (dt == null || dt.Rows.Count == 0)
            return null;
        List<FormDeclareItem> res = new List<FormDeclareItem>();
        for (int i = 0; i < dt.Rows.Count; i++)
            res.Add(new FormDeclareItem(dt.Rows[i]));
        dt.Dispose();
        return res;
    }
    public List<FormDeclareItem> GetFormDeclareItemsByCode(string formCode)
    {
        DataTable dt = FillData(string.Format("SELECT * FROM FormDeclareItem WHERE FormDeclareID IN (SELECT ID FROM FormDeclare WHERE Code={0}) ORDER BY SortIndex", Helper.QoutedString(formCode)), "f");
        if (dt == null || dt.Rows.Count == 0)
            return null;
        List<FormDeclareItem> res = new List<FormDeclareItem>();
        for (int i = 0; i < dt.Rows.Count; i++)
            res.Add(new FormDeclareItem(dt.Rows[i]));
        dt.Dispose();
        return res;
    }
    public List<FormDeclareItem> GetFormDeclareItemsByName(string formName)
    {
        DataTable dt = FillData(string.Format("SELECT * FROM FormDeclareItem WHERE FormDeclareID IN (SELECT ID FROM FormDeclare WHERE [Name]={0}) ORDER BY SortIndex", Helper.QoutedString(formName)), "f");
        if (dt == null || dt.Rows.Count == 0)
            return null;
        List<FormDeclareItem> res = new List<FormDeclareItem>();
        for (int i = 0; i < dt.Rows.Count; i++)
            res.Add(new FormDeclareItem(dt.Rows[i]));
        dt.Dispose();
        return res;
    }
    public FormDeclareItem GetFormDeclareItem(int id)
    {
        DataRow dr = FillRow(string.Format("SELECT * FROM FormDeclareItem WHERE ID={0}", id));
        if (dr == null)
            return null;
        FormDeclareItem res = new FormDeclareItem(dr);
        dr.Table.Dispose();
        return res;
    }
    public FormDeclareItem GetFormDeclareItem(int formDeclareID, string title)
    {
        DataRow dr = FillRow(string.Format("SELECT * FROM FormDeclareItem WHERE FormDeclareID={0} AND Title={1}", formDeclareID, Helper.QoutedString(title)));
        if (dr == null)
            return null;
        FormDeclareItem res = new FormDeclareItem(dr);
        dr.Table.Dispose();
        return res;
    }

    #endregion

    #region IDisposable Members
    public void Dispose()
    {
        RollbackTransaction();
        CloseConnection();
    }

    #endregion


    #region Instance
    public class Instance
    {
        public static IDataReader ExecuteReader(string qry)
        {
            try
            {
                SqlConnection cnt = new SqlConnection(SiteSettings.ConnectionString);
                string other = SiteSettings.OtherCatalogName;
                if (!string.IsNullOrEmpty(other))
                {
                    // Server=(local)\SQLEXPRESS2008R2;Database=HIS;uid=pa;pwd=1234;
                    string cntStr = cnt.ConnectionString;
                    cntStr = cntStr.Replace(cnt.Database, other);
                    cnt.Dispose();
                    cnt = new SqlConnection(cntStr);
                }
                cnt.Open();

                SqlCommand command = new SqlCommand(qry, cnt);
                return command.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                Logger.Log(ex, "Qry=" + qry);
            }
            return null;
        }
        public static IDataReader ExecuteReader(string qry, int timeOut)
        {
            try
            {
                SqlConnection cnt = new SqlConnection(SiteSettings.ConnectionString);
                string other = SiteSettings.OtherCatalogName;
                if (!string.IsNullOrEmpty(other))
                {
                    // Server=(local)\SQLEXPRESS2008R2;Database=HIS;uid=pa;pwd=1234;
                    string cntStr = cnt.ConnectionString;
                    cntStr = cntStr.Replace(cnt.Database, other);
                    cnt.Dispose();
                    cnt = new SqlConnection(cntStr);
                }
                cnt.Open();

                SqlCommand command = new SqlCommand(qry, cnt);
                command.CommandTimeout = timeOut;
                return command.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                Logger.Log(ex, "Qry=" + qry);
            }
            return null;
        }
        public static IDataReader ExecuteReader(string spName, params object[] parameterValues)
        {
            try
            {
                string cntStr = SiteSettings.ConnectionString;
                string other = SiteSettings.OtherCatalogName;
                if (!string.IsNullOrEmpty(other))
                {
                    // Server=(local)\SQLEXPRESS2008R2;Database=HIS;uid=pa;pwd=1234;
                    SqlConnection cnt = new SqlConnection(cntStr);
                    cntStr = cnt.ConnectionString.Replace(cnt.Database, other);
                    cnt.Dispose();
                }
                return SqlHelper.ExecuteReader(cntStr, spName, parameterValues);
            }
            catch (Exception ex)
            {
                Logger.Log(ex, "spName=" + spName);
            }
            return null;
        }
        public static DataTable FillData(string qry, string tableName)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.FillData(qry, tableName);
        }
        public static DataTable FillData(string qry, string tableName, List<string> dbs, string sort)
        {
            if (string.IsNullOrEmpty(qry))
                return null;
            if (dbs == null || dbs.Count == 0)
                return FillData(qry, tableName);

            DataTable res = null;
            using (DbProxy proxy = new DbProxy(dbs[0]))
                res = proxy.FillData(qry, tableName);
            if (dbs.Count > 1)
                for (int d = 1; d < dbs.Count; d++)
                {
                    DataTable dt = null;
                    using (DbProxy proxy = new DbProxy(dbs[d]))
                        dt = proxy.FillData(qry, tableName);
                    if (dt != null)
                    {
                        if (res == null)
                            res = dt;
                        else
                        {
                            // combine them
                            res.Merge(dt);
                            dt.Dispose();
                        }
                    }
                }
            try
            {
                if (!string.IsNullOrEmpty(sort))
                {
                    DataView dv = res.DefaultView;
                    dv.Sort = sort;
                    DataTable sorted = dv.ToTable();
                    res.Dispose();
                    res = sorted;
                }
            }
            catch (Exception ex)
            {
                // we have errors when the sort columns are not valid
                ExceptionLog.Instance.Add(ex);
            }
            return res;
        }
        public static DataTable FillData(string qry, string tableName, int timeout)
        {

            using (DbProxy proxy = new DbProxy())
                return proxy.FillData(qry, tableName, timeout);
        }
        public static DataSet FillDataSet(string qry)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.FillDataSet(qry);
        }
        public static DataRow FillRow(string qry)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.FillRow(qry);
        }
        public static object FillScalar(string qry)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.FillScalar(qry);
        }
        public static string FillScalarString(string qry)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.FillScalarString(qry);
        }
        public static object FillScalar(string spName, params object[] parameterValues)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.FillScalar(spName, parameterValues);
        }

        #region Run Query
        public static bool ExecuteCommand(string qry)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.ExecuteCommand(qry);
        }
        // Transaction Enabled
        public static bool ExecuteCommand(string qry, int timeOut)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.ExecuteCommand(qry, timeOut);
        }

        public static int RunQuery(string qry)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.RunQuery(qry);
        }
        public static int RunQuery(string qry, int TimeOut)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.RunQuery(qry, TimeOut);
        }
        public static int RunSP(string spName, string paramName, string value, int TimeOut)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.RunSP(spName, paramName, value, TimeOut);
        }
        public static int RunSP(string spName, string[] paramNames, object[] values, int TimeOut)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.RunSP(spName, paramNames, values, TimeOut);
        }
        #endregion

        #region Config
        // todo: Cache Config
        public static string GetConfig(string name)
        {
            return GetConfig(name, "", -1);
        }
        public static string GetConfig(string name, int programID)
        {
            return GetConfig(name, "", programID);
        }
        public static string GetConfig(string name, string defaultValue)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.GetConfig(name, defaultValue, -1);
        }
        public static string GetConfig(string name, string defaultValue, int programID)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.GetConfig(name, defaultValue, programID);
        }
        //public static bool SetConfig(string name, string value)
        //{
        //    using (DbProxy proxy = new DbProxy())
        //        return proxy.SetConfig(name, value);
        //}
        #endregion

        #region Options
        // todo: Cache Config
        public static string GetOption(int userID, string name)
        {
            return GetOption(userID, name, "");
        }
        public static string GetOption(int userID, string name, string defaultValue)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.GetOption(userID, name, defaultValue);
        }
        public static bool SetOptions(int userID, string name, string value)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.SetOption(userID, name, value);
        }
        #endregion

        #region Report
        public static ReportFile GetReport(int id)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.GetReport(id);
        }
        public static ReportFile GetReport(string name)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.GetReport(name);
        }
        public static bool SaveReport(ReportFile report)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.SaveReport(report);
        }
        #endregion

        #region Forms
        public static FormDeclare GetFormDeclare(int id)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.GetFormDeclare(id);
        }
        public static FormDeclare GetFormDeclare(string code)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.GetFormDeclare(code);
        }
        public static FormDeclare GetFormDeclareByName(string code)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.GetFormDeclareByName(code);
        }
        public static List<FormDeclareItem> GetFormDeclareItems(int formDeclareID)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.GetFormDeclareItems(formDeclareID);
        }
        public static List<FormDeclareItem> GetFormDeclareItemsByCode(string formCode)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.GetFormDeclareItemsByCode(formCode);
        }
        public static List<FormDeclareItem> GetFormDeclareItemsByName(string formName)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.GetFormDeclareItemsByName(formName);
        }
        public static FormDeclareItem GetFormDeclareItem(int id)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.GetFormDeclareItem(id);
        }
        public static FormDeclareItem GetFormDeclareItem(int formDeclareID, string title)
        {
            using (DbProxy proxy = new DbProxy())
                return proxy.GetFormDeclareItem(formDeclareID, title);
        }

        #endregion
        //public static Exception LastException
        //{
        //    get
        //    {
        //    }
        //    set
        //    {
        //    }
        //}
        #region ReportField

        public static int GetReportField(string Name, ReportFieldType Type)
        {
            return GetReportField(Name, -1, Type);
        }
        public static int GetReportField(string Name, int ProgramID, ReportFieldType Type)
        {
            int value = -1;
            try
            {
                string qry = string.Format("SELECT Value FROM ReportFields WHERE Name=N'{0}' AND Type={1}", Name, Helper.GetInt(Type, 1));
                if (ProgramID < 0)
                {
                    qry += " AND ProgramID IS NULL";
                }
                else
                {
                    qry += " AND ProgramID=" + ProgramID;
                }

                value = Helper.GetInt(FillScalar(qry), -1);
            }
            catch (Exception)
            {

                throw;
            }
            return value;
        }
        #endregion

    }
    #endregion

}