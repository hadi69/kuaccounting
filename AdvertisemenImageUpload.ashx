﻿<%@ WebHandler Language="C#" Class="AdvertisemenImageUpload" %>

using System;
using System.Web;
using System.IO;
using Newtonsoft.Json;

public class AdvertisemenImageUpload : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "application/json";
        context.Response.Expires = -1;
        try
        {
            int advertisementId = Helper.GetInt(context.Request.Params["advertisementId"], Null.NullInteger);
            if (advertisementId > 0)
            {
                // it is for Service
                String filename = HttpContext.Current.Request.Headers["X-File-Name"].Slugify();
                Stream inputStream = HttpContext.Current.Request.InputStream;
                string tempPath = "files/advertisement/" + advertisementId.ToString(); // System.Configuration.ConfigurationManager.AppSettings["FolderPath"];
                string savePath = context.Server.MapPath("~/" + tempPath);
                if (!Directory.Exists(savePath))
                {
                    Directory.CreateDirectory(savePath);
                }
                string actualFileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + "_" + filename;
                string actualFile = savePath + '\\' + actualFileName;
                string thumbnailFileName = "thumb_" + actualFileName;
                string thumbnail = savePath + '\\' + thumbnailFileName;
                FileStream fileStream = new FileStream(actualFile, FileMode.OpenOrCreate);
                inputStream.CopyTo(fileStream);
                fileStream.Close();

                ImageUtilities.ScaleImage(actualFile, thumbnail, 250, 200);

                var advertisementImage = new AdvertisementImage();
                advertisementImage.MainImage = tempPath + "/" + actualFileName;
                advertisementImage.ThumbImage = tempPath + "/" + thumbnailFileName;
                advertisementImage.Title = "";

                string query = "INSERT INTO AdvertisementImage (AdvertisementID, Title, MainImage, ThumbImage) Values({0}, '{1}', '{2}', '{3}'); Select SCOPE_IDENTITY();";
                query = String.Format(query, advertisementId, "", advertisementImage.MainImage, advertisementImage.ThumbImage);
                object id = DbProxy.Instance.FillScalar(query);

                advertisementImage.id = int.Parse(id.ToString());

                context.Response.Write("{success:true, model: " + JsonConvert.SerializeObject(advertisementImage) + "}");
            }
            else
            {
                HttpPostedFile postedFile = context.Request.Files["Filedata"];

                string tempPath = "~/files";// System.Configuration.ConfigurationManager.AppSettings["FolderPath"];
                string savepath = context.Server.MapPath(tempPath);
                if (!Directory.Exists(savepath))
                    Directory.CreateDirectory(savepath);
                string folder = context.Request["folder"];
                if (!string.IsNullOrEmpty(folder))
                {
                    if (folder.StartsWith("/"))
                        folder = folder.Substring(folder.LastIndexOf('/') + 1);
                    savepath = Path.Combine(savepath, folder);
                    if (!Directory.Exists(savepath))
                        Directory.CreateDirectory(savepath);
                }
                string filename = postedFile.FileName;
                postedFile.SaveAs(savepath + @"\" + filename);
                context.Response.Write(tempPath + "/" + filename);
                context.Response.StatusCode = 200;
            }

        }
        catch (Exception ex)
        {
            context.Response.Write("Error: " + ex.Message);
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}

class AdvertisementImage
{
    public string MainImage { get; set; }
    public string ThumbImage { get; set; }
    public string Title { get; set; }
    public int id { get; set; }
}