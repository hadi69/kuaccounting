﻿<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="CalculatePursant.aspx.cs" Inherits="CalculatePursant" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery-1.4.1-vsdoc.js" language="javascript" type="text/javascript"></script>
    <script src="js/plugins.js" language="javascript" type="text/javascript"></script>

    <script src="js/site.js" language="javascript" type="text/javascript"></script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>
    <br />
    <input type="hidden" runat="server" id="mMsg" class="mMsg" />
    <div id='tabPrice' class='tab_content'>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <label for='FromDate' class="fromdatelable left-aligned-label col-xs-12 col-sm-4"></label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <input type="text" id="FromDate" name="FromDate" runat="server" class='fromdate dateSelect form-control input-sm' />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <label for='ToDate' class="todatelable left-aligned-label col-xs-12 col-sm-4"></label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <input type="text" id="ToDate" name="ToDate" runat="server" class='todate dateSelect form-control input-sm' />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <label for='ServiceGroupName' class="servicegroupnamelable left-aligned-label col-xs-12 col-sm-4"></label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <input type="text" id="ServiceGroupName" name="ServiceGroupName" class='form-control input-sm' />
                        <input type="hidden" id="ServiceGroupID" name="ServiceGroupID" value="@ServiceGroupID" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <label for='PersonGroupName' class="persongroupnamelable left-aligned-label col-xs-12 col-sm-4"></label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <input type="text" id="PersonGroupName" name="PersonGroupName" class='form-control input-sm' />
                        <input type="hidden" id="PersonGroupID" name="PersonGroupID" value="@PersonGroupID" />
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container persondiv hidden">
                <div class="form-group">
                    <label for='PersonName' class="personnamelable left-aligned-label col-xs-12 col-sm-4"></label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <input type="text" id="PersonName" name="PersonName" class='form-control input-sm' />
                        <input type="hidden" id="PersonID" name="PersonID" value="@PersonID" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 col-lg-offset-5 form-group-container">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <input type="Button" id="Show" class="btn btn-primary calculatepursantshowbtn" />
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="oftershowclick hidden">
            <table id="tablePursant" class="awesome-table table-with-border zebra">
            </table>
        </div>
        <br />
        <div class="row oftershowclick hidden">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-offset-5 form-group-container">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-12 form-col-without-padding">
                        <asp:Button runat="server" ID="Button2" OnClick="doPursantDocument_Click" Text="Lang.DoPursantDocument" CssClass="btn btn-primary doPursantDocument" />
                        <input type="Button" id="Print" class="btn btn-primary print" />
                        <a href="#" onclick="$('#tablePursant').tableExport({type:'excel',escape:'false'});" class="btn btn-primary">XLS</a>
                    </div>
                </div>
            </div>
        </div>
        <br />
    </div>
    <script type="text/javascript" src="js/jsexport/tableExport.js"></script>
    <script type="text/javascript" src="js/jsexport/jquery.base64.js"></script>
    <script type="text/javascript" src="js/jsexport/html2canvas.js"></script>
    <script type="text/javascript" src="js/jsexport/sprintf.js"></script>
    <script type="text/javascript" src="js/jsexport/jspdf.js"></script>
    <script type="text/javascript" src="js/jsexport/base64.js"></script>
    <script>

        function handlePursant() {
            var head = "<thead><tr class='th'>";
            head += "<th class='width-20 align-center'><div>" + Lang.ColRowNr + "</div></th>";
            head += "<th class='width-150  align-center'><Label>" + Lang.PersonName + "<span class='copyT glyphicon glyphicon-comment' style='font-size: 15px;' ></span></label></th>";
            head += "<th class='width-90  align-center buyprice'><Label>" + Lang.CooperationType + "<spanL class='copyL glyphicon glyphicon-comment' style='font-size: 15px;' ></span></label></th>";
            if (toInt($('#ServiceGroupID').val(), 0) > 0) {
                head += "<th class='width-90  align-center buyprice'><Label>" + Lang.GoodsGroup + "<spanL class='copyL glyphicon glyphicon-comment' style='font-size: 15px;' ></span></label></th>";
            }
            head += "<th class='width-110  align-center'><Label>" + Lang.SaleTotalAmount + "<span class='copyN glyphicon glyphicon-comment' style='font-size: 15px;' ></span></Label></th>";
            head += "<th class='width-60  align-center'><Label>" + Lang.PursantPercent + "<input type='text' id='paste' style='width:15px' /><span class='copyN glyphicon glyphicon-comment' style='font-size: 15px;' ></span></label></th>";
            head += "<th class='width-110  align-center'><Label>" + Lang.PursantAmount + "<span class='copyN glyphicon glyphicon-comment' style='font-size: 15px;' ></span></label></th>";
            head += "<th class='width-50  align-center'><Label>" + Lang.Records + "</label></th>";
            head += '</tr></thead><tbody">';
            $('#tablePursant').append(head);

            var cnt = 1;
            function addPursant(node) {
                var row = '<tr>';
                row += '<td class="align-center width-40 row-number" data-th="' + Lang.ColRowNr + '">' + cnt + '</td>';
                row += '<td class="width-150 align-center" style="width: 150px">'
                    + '<input type="text" name="PersonName" id="PN_' + cnt + '" readonly="readonly" class="form-control align-center input-sm" value="' + getNodeVal(node, "PersonName") + '" /><input type="hidden" id="PI_' + cnt + '" name="PersonID" value="' + getNodeVal(node, "PersonID") + '"/></td>';
                row += '<td class="width-90 align-center">' + getNodeVal(node, "CooperationTypeName") + '</td>';
                if (toInt($('#ServiceGroupID').val(), 0) > 0) {
                    row += '<td class="width-90 align-center">' + getNodeVal(node, "GroupingName") + '</td>';
                }
                row += '<td class="width-110 align-center"><input type="text" name="SaleTotalAmount" id="STA_' + cnt + '" readonly="readonly" value="' + getNodeVal(node, "SaleTotalAmount", 0) + '" class="saletotalamount monetary form-control align-center input-sm" /></td>';
                row += '<td class="width-90 align-center"><div class="input-group input-group-sm">' +
                    '<input type="text" name="PursantPercent" id="PP_' + cnt + '" value="" class="pursantpercent form-control align-center input-sm" />' +
                    '<span class="input-group-addon">%</span></div></td>';
                row += '<td class="width-110 align-center"><input type="text" name="PursantAmount" id="PA_' + cnt + '" readonly="readonly" value="" class="pursantamount monetary form-control align-center input-sm" /></td>';
                row += '<td class="width-50 align-center"><a type="button" href="#" class="recordbtn" id="R_' + cnt + '"><span class="glyphicon glyphicon-eye-open" style="font-size: 22px;" ></span></a></td>';
                row += '</tr>';
                cnt += 1;
                return row;
            }
            function addSumRow() {
                var row = '<tr>';
                row += '<td class="align-center width-40 row-number"></td>';
                row += '<td class="width-150 align-center" style="width: 150px"></td>';
                row += '<td class="width-90 align-center"></td>';
                if (toInt($('#ServiceGroupID').val(), 0) > 0) {
                    row += '<td class="width-90 align-center"></td>';
                }
                row += '<td class="width-110 align-center"><input type="text" name="TotalSaleTotalAmount" readonly="readonly" class="totalsaletotalamount monetary form-control align-center input-sm" /></td>';
                row += '<td class="width-90 align-center"><div class="input-group input-group-sm">' +
                    '<input type="text" name="TotalPursantPercent" readonly="readonly" value="" class="totalpursantpercent form-control align-center input-sm" />' +
                    '<span class="input-group-addon">%</span></div></td>';
                row += '<td class="width-110 align-center"><input type="text" name="TotalPursantAmount" readonly="readonly" value="" class="totalpursantamount monetary form-control align-center input-sm" /></td>';
                row += '<td class="width-50 align-center"></td>';
                row += '</tr>';
                cnt += 1;
                return row;
            }
            fillTable("get.aspx?op=GetXml&qd=getallpersonforpursant&FromDate=" + $('.fromdate').val() + "&ToDate=" + $('.todate').val() + "&PersonGroupID=" + toInt($('#PersonGroupID').val(), 0) + "&PersonID=" + toInt($('#PersonID').val(), 0) + "&ServiceGroupID=" + toInt($('#ServiceGroupID').val(), 0), '#tablePursant', addPursant);
            $('#tablePursant').append('</tbody>');
            $('tr:last', '#tablePursant').after(addSumRow());
            $('.recordbtn').click(function () {
                var id = $(this).attr('id'); // R_1
                id = id.substring(2); // R_1  -->  1
                var personId = $('#PI_' + id).val();
                var personName = $('#PN_' + id).val();
                if (personId > 0) {
                    var _url = 'get.aspx?op=GetHtml&table=Personnel&view=pursanthistory&PersonID=' + personId + '&PersonName=' + personName + appendTime();
                    showPopup(_url, function (v, m, f) {
                        return true;
                    }, getCloseButtons());
                }
            });
            calcSum();
            cnt = 1;



            // copy number in textbox
            $('.copyN').click(function () {
                var th = $(this).parent().parent();
                var index = th.index() + 1;
                var col = ''; $('#tablePursant tr td:nth-child('+ index + ') :text').each(function () { col += $(this).val() + "\r\n"; });
                col = col.replace(/\,/g, '');
                copyTextToClipboard(col);
                Growl.Notice('Copied');
            });
            // copy text in textbox
            $('.copyT').click(function () {
                var th = $(this).parent().parent();
                var index = th.index() + 1;
                var col = ''; $('#tablePursant tr td:nth-child(' + index + ') :text').each(function () { col += $(this).val() + "\r\n"; });
                copyTextToClipboard(col);
                Growl.Notice('Copied');
            });
            // copy text
            $('.copyL').click(function () {
                var th = $(this).parent().parent();
                var index = th.index() + 1;
                var col = ''; $('#tablePursant tr td:nth-child(' + index + ')').each(function () { col += $(this).text() + "\r\n"; });
                copyTextToClipboard(col);
                Growl.Notice('Copied');
            });
            function handlePaste(e) {
                var clipboardData, pastedData;

                // Stop data actually being pasted into div
                e.stopPropagation();
                e.preventDefault();

                // Get pasted data via clipboard API
                clipboardData = e.clipboardData || window.clipboardData;
                pastedData = clipboardData.getData('Text');

                var lines = pastedData.split('\n');
                var table = $("#tablePursant")[0];
                for (var i = 0; i < lines.length; i++)
                    $(':text', table.rows[i + 1].cells[5]).val(lines[i])
            }

            document.getElementById('paste').addEventListener('paste', handlePaste);
        }
        function putComma() {
            $('.monetary').keyup(function () {
                $(this).val(numberWithCommasAllowDP(stripCommas($(this).val())));
            });
        }
        function takeComma() {
            $('.monetary').each(function () {
                $(this).val(fromMonetary($(this).val().replace(/\,/g, '')));
            });
        }

        function calcSum() {
            var totalSale = 0;
            var toralPP = 0;
            var totalp = 0;
            takeComma();
            $('.saletotalamount').each(function () {
                totalSale += toFloat($(this).val(), 0);
            });
            $('.pursantpercent').each(function () {
                toralPP += toFloat($(this).val(), 0);
            });
            $('.pursantamount').each(function () {
                totalp += toFloat($(this).val(), 0);
            });
            $('.totalsaletotalamount').val(totalSale);
            $('.totalpursantpercent').val(toralPP);
            $('.totalpursantamount').val(totalp);
            putComma()
        }
        $(function () {
            $('#Print').attr('value', Lang.Print);
            $('#Show').attr('value', Lang.Show);
            $('#Print').click(function () {
                var head = Lang.CalculatePursant;
                printTable('#tablePursant', '', head);
            });

            $('.doPursantDocument').click(function () {
                takeComma();
            });

            shortcut.add("F2", function () {
                $('#Show').click();
            });

            dateSelect('.dateSelect');
            $('.fromdatelable').text(Lang.FromDate);
            $('.todatelable').text(Lang.ToDate);
            $('.persongroupnamelable').text(Lang.PersonnelGroup);
            autoComplete('Grouping', 'grouppersonauto', '#PersonGroupName', '#PersonGroupID',
                function (e, data) {
                    $('#PersonGroupID').val(data[0]);
                    if (toInt($('#ServiceGroupID').val(), 0) > 0) {
                        $('.persondiv').removeClass('hidden');
                        $('.personnamelable').text(Lang.Person);
                        var _autourl = 'get.aspx?op=Auto&table=SaleType&qd=calcpursantpersonauto&GroupingID=' + data[0] + appendTime();
                        $('#PersonName').unautocomplete();
                        autoComplete('Grouping', 'calcpursantpersonauto', '#PersonName', '#PersonID', null, null, null, null, _autourl);
                    } else {
                        $('.persondiv').addClass('hidden');
                    }
                });
            $('.servicegroupnamelable').text(Lang.GoodsGroup);
            autoComplete('Grouping', 'groupserviceauto', '#ServiceGroupName', '#ServiceGroupID');
            $("#bar").prepend(breadcrumb({
                title: Lang.CalculatePursant,
                parent: {
                    title: Lang.MDistribution,
                    url: 'menu/distribution.aspx'
                }
            }));

            $('#Show').click(function () {
                if (toInt($('#ServiceGroupID').val(), 0) > 0) {
                    if (toInt($('#PersonGroupID').val(), 0) <= 0) {
                        Growl.Error(Lang.CalcPursantEMsg, 3000);
                        return false;
                    } else {
                        if (toInt($('#PersonID').val(), 0) <= 0) {
                            Growl.Error(Lang.CalcPursantEMsg1, 3000);
                            return false;
                        }
                    }
                }
                $("#tablePursant").empty();
                $('.oftershowclick').removeClass('hidden');
                handlePursant();
                insertDefaultComma();
                $('.pursantpercent').blur(function () {
                    takeComma();
                    var id = $(this).attr('id'); // PP_1
                    id = id.substring(3); // PP_1  -->  1
                    var persent = $(this).val();
                    if (persent > 0) {
                        var pursantAmount = (toFloat($(this).val(), 0) * toFloat($('#STA_' + id).val(), 0)) / 100;
                        $('#PA_' + id).val(pursantAmount);
                    }
                    insertDefaultComma();
                    calcSum();
                });
            });

            function insertDefaultComma() {
                $('.monetary').each(function () {
                    $(this).val(numberWithCommas(toMonetary($(this).val())));
                    if ($(this).val() == 0) {
                        $(this).val('');
                    }
                });
            }

            insertDefaultComma();
            putComma();
            var msg = $('.mMsg').val();
            if (msg)
                Growl.Notice(msg);
        });
    </script>
</asp:Content>
