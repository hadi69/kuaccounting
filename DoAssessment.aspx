﻿<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="DoAssessment.aspx.cs" Inherits="DoAssessment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery-1.4.1-vsdoc.js" language="javascript" type="text/javascript"></script>
    <script src="js/plugins.js" language="javascript" type="text/javascript"></script>

    <script src="js/site.js" language="javascript" type="text/javascript"></script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>
    <br/>
    <input type="hidden" runat="server" id="mMsg" class="mMsg" />
    <div id='tabPrice' class='tab_content'>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <label for='FromDate' class="fromdatelable left-aligned-label col-xs-12 col-sm-4"></label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <input type="text" id="FromDate" name="FromDate" runat="server" class='fromdate dateSelect form-control input-sm' />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <label for='ToDate' class="todatelable left-aligned-label col-xs-12 col-sm-4"></label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <input type="text" id="ToDate" name="ToDate" runat="server" class='todate dateSelect form-control input-sm' />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <label for='PersonGroupName' class="persongroupnamelable left-aligned-label col-xs-12 col-sm-4"></label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <input type="text" id="PersonGroupName" name="PersonGroupName" class='form-control input-sm' />
                        <input type="hidden" id="PersonGroupID" name="PersonGroupID" value="@PersonGroupID" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <input type="Button" id="Show" class="btn btn-primary calculatepursantshowbtn" />
                    </div>
                </div>
            </div>
        </div>
        <br/>
        <div class="oftershowclick hidden">
            <table id="tableScore" class="awesome-table table-with-border zebra">
            </table>
        </div>
        <br/>
        <div class="row oftershowclick hidden">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-offset-5 form-group-container">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-12 form-col-without-padding">
                        <asp:Button runat="server" ID="Button2" OnClick="doPursantDocument_Click" Text="Lang.DoPursantDocument" CssClass=" display-none btn btn-primary doPursantDocument" />
                        <input type="Button" id="Print" class="btn btn-primary print" />
                        <a href="#" onclick="$('#tableScore').tableExport({type:'excel',escape:'false'});" class="btn btn-primary">XLS</a>
                    </div>
                </div>
            </div>
        </div>
        <br />
    </div>
    <script type="text/javascript" src="js/jsexport/tableExport.js"></script>
    <script type="text/javascript" src="js/jsexport/jquery.base64.js"></script>
    <script type="text/javascript" src="js/jsexport/html2canvas.js"></script>
    <script type="text/javascript" src="js/jsexport/sprintf.js"></script>
    <script type="text/javascript" src="js/jsexport/jspdf.js"></script>
    <script type="text/javascript" src="js/jsexport/base64.js"></script>
    <script>

        function handleScore() {
            var head = "<thead><tr class='th'>";
            head += "<th class='width-150  align-center'><Label>" + Lang.PersonName + "</Label></th>";
            head += "<th class='width-90  align-center buyprice'><Label>" + Lang.CooperationType + "</Label></th>";
            head += "<th class='width-110  align-center'><Label>" + Lang.Score + "</Label></th>";
            head += '</tr></thead><tbody">';
            $('#tableScore').append(head);

            var cnt = 1;
            function addScore(node) {
                var row = '<tr>';
                row += '<td class="width-150 align-center" style="width: 150px">'
                    + getNodeVal(node, "PersonName") + '<input type="hidden" name="PersonID" value="' + getNodeVal(node, "PersonID") + '"/></td>';
                row += '<td class="width-90 align-center">' + getNodeVal(node, "CooperationTypeName") + '</td>';
                row += '<td class="width-110 align-center"><input type="text" name="Score" id="STA_' + cnt + '" readonly="readonly" value="' + getNodeVal(node, "Score") + '" class="score form-control align-center input-sm" /></td>';
                row += '</tr>';
                cnt += 1;
                return row;
            }
            fillTable("get.aspx?op=GetXml&qd=getpersonscore&FromDate=" + $('.fromdate').val() + "&ToDate=" + $('.todate').val() + "&PersonGroupID=" + $('#PersonGroupID').val(), '#tableScore', addScore);
            $('#tableScore').append('</tbody>');
            cnt = 1;
        }
        function putComma() {
            $('.monetary').keyup(function () {
                $(this).val(numberWithCommasAllowDP(stripCommas($(this).val())));
            });
        }
        function takeComma() {
            $('.monetary').each(function () {
                $(this).val(fromMonetary($(this).val().replace(/\,/g, '')));
            });
        }
        $(function () {
            $('#Print').attr('value', Lang.Print);
            $('#Show').attr('value', Lang.Show);
            $('#Print').click(function () {
                var head = Lang.DoAssessment;
                printTable('#tableScore', '', head);
            });

            $('.doPursantDocument').click(function () {
                takeComma();
            });

            shortcut.add("F2", function () {
                $('#Show').click();
            });

            dateSelect('.dateSelect');
            $('.fromdatelable').text(Lang.FromDate);
            $('.todatelable').text(Lang.ToDate);
            $('.persongroupnamelable').text(Lang.PersonnelGroup);
            autoComplete('Grouping', 'grouppersonauto', '#PersonGroupName', '#PersonGroupID');
            $("#bar").prepend(breadcrumb({
                title: Lang.DoAssessment,
                parent: {
                    title: Lang.MDistribution,
                    url: 'menu/distribution.aspx'
                }
            }));

            $('#Show').click(function () {
                $("#tableScore").empty();
                $('.oftershowclick').removeClass('hidden');
                handleScore();
                //insertDefaultComma();
                //$('.pursantpercent').blur(function () {
                //    takeComma();
                //    var id = $(this).attr('id'); // PP_1
                //    id = id.substring(3); // PP_1  -->  1
                //    var persent = $(this).val();
                //    if (persent > 0) {
                //        var pursantAmount = (toFloat($(this).val(), 0) * toFloat($('#STA_' + id).val(), 0)) / 100;
                //        $('#PA_' + id).val(pursantAmount);
                //    }
                //    insertDefaultComma();
                //});
            });

            function insertDefaultComma() {
                $('.monetary').each(function () {
                    $(this).val(numberWithCommas(toMonetary($(this).val())));
                    if ($(this).val() == 0) {
                        $(this).val('');
                    }
                });
            }
            
            //insertDefaultComma();
            //putComma();
            var msg = $('.mMsg').val();
            if (msg)
                Growl.Notice(msg);
        });
    </script>
</asp:Content>
