﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="start.aspx.cs" Inherits="start" EnableViewState="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery-1.4.1-vsdoc.js" language="javascript" type="text/javascript"></script>
    <script src="js/plugins.js" language="javascript" type="text/javascript"></script>
    <script src="js/site.js" language="javascript" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <input type="hidden" runat="server" id="mMsg" class="mMsg" />
    <input type="hidden" runat="server" id="mCurTab" class="mCurTab" />
     <asp:TextBox ID="UserID" runat="server" CssClass="userid form-control input-sm hidden"></asp:TextBox>
    <ul class="tabs">
        <li><a href="#tabService">
            <asp:Label runat="server">Lang.Service</asp:Label></a></li>
        <li><a href="#tabPerson">
            <asp:Label runat="server">Lang.Persons</asp:Label></a></li>
        <li><a href="#tabCheck">
            <asp:Label runat="server">Lang.Checks</asp:Label></a></li>
        <li><a href="#tabPayCheck">
            <asp:Label runat="server">Lang.PayChecks</asp:Label></a></li>
        <li><a href="#tabTreePath">
            <asp:Label runat="server">Lang.Accounts</asp:Label></a></li>
       <div class="col-sm-offset-10"><asp:Label runat="server">Lang.OpeningDocumentNo : <a id="OpeningDocNo"></a></asp:Label></div>
    </ul>
    <div class="tab_container">
        <div id='tabService' class='tab_content'>
            <table id="tableService" class="awesome-table table-with-border zebra">
                <tr class='th'>
                    <td class="row-cell width-120">
                        <asp:Label runat="server">Lang.Barcode</asp:Label></td>
                    <td>
                        <asp:Label runat="server">Lang.StoreService</asp:Label></td>
                    <td class="width-170  align-center">
                        <asp:Label runat="server">Lang.ServiceType</asp:Label></td>
                    <td class="width-100  align-center">
                        <asp:Label runat="server">Lang.Units</asp:Label></td>
                    <td class="width-100 align-center">
                        <asp:Label runat="server">Lang.Store</asp:Label></td>
                    <td class="width-100 align-center">
                        <asp:Label runat="server">Lang.Pricing</asp:Label></td>
                    <td class="width-100 align-center">
                        <asp:Label runat="server">Lang.StoreStock</asp:Label></td>
                    <td class="width-100 align-center">
                        <asp:Label runat="server">Lang.PurchasePriceOfTheUnit</asp:Label></td>
                    <td class="width-100 align-center">
                        <asp:Label runat="server">Lang.SaleWorth</asp:Label></td>
                    <td class="width-120 align-center">
                        <asp:Label runat="server">Lang.ExpireDate</asp:Label></td>
                    <td class="center width-20 align-center">
                        <img src="img/add.gif" class="addService" /></td>
                </tr>
            </table>
            <br/>
            <div class="align-center">
                <asp:CheckBox runat="server" ID="mServiceShowServiceType" CssClass="mServiceShowServiceType" Checked="True" Text="Lang.ServiceType" />
                <asp:CheckBox runat="server" ID="mServiceShowUnits" CssClass="mServiceShowUnits" Checked="True" Text="Lang.Units" />
                <asp:CheckBox runat="server" ID="mServiceShowStore" CssClass="mServiceShowStore" Checked="True" Text="Lang.Store" />
                <asp:Button runat="server" ID="doSaveServices" OnClick="doSaveServices_Click" Text="Lang.SaveService" CssClass="btn btn-primary doSaveServices" />
                <asp:Button runat="server" ID="doApplyServices1" OnClick="doApplyServices1_Click" Text="Lang.ApplyService1" CssClass="btn btn-primary doApplyServices" />
                <button type="button" class="btn btn-info"><span class="sumTtheLPBP"></span></button>
                <asp:CheckBox runat="server" ID="mHaveOpeningServiceDoc" CssClass="mHaveOpeningServiceDoc" Checked="false" Text="Lang.HaveOpeningDoc" />
           </div>
        </div>
        <div id='tabPerson' class='tab_content'>
            <table id="tablePersons" class="awesome-table table-with-border zebra">
                <tr class='th'>
                    <td class="row-cell width-80">
                        <asp:Label runat="server">Lang.Code</asp:Label></td>
                    <td class="width-100  align-center">
                        <asp:Label runat="server">Lang.Name</asp:Label></td>
                    <td class="width-100  align-center">
                        <asp:Label runat="server">Lang.Surname</asp:Label></td>
                    <td class="width-110  align-center">
                        <asp:Label runat="server">Lang.Owe</asp:Label></td>
                    <td class="width-110 align-center">
                        <asp:Label runat="server">Lang.Due</asp:Label></td>
                    <td class="width-150 align-center">
                        <asp:Label runat="server">Lang.PersonnelGroup</asp:Label></td>
                    <td class="width-100 align-center">
                        <asp:Label runat="server">Lang.HomeTelNum</asp:Label></td>
                    <td>
                        <asp:Label runat="server">Lang.Address</asp:Label></td>
                    <td class="center width-20 align-center">
                        <img src="img/add.gif" class="addPerson" /></td>
                </tr>
            </table>
            <br/>
            <div class="align-center">
                <asp:CheckBox runat="server" ID="mPersonShowPersonnelGroup" CssClass="mPersonShowPersonnelGroup" Checked="True" Text="Lang.PersonnelGroup" />
             <asp:Button runat="server" ID="doSavePersons" OnClick="doSavePersons_Click" Text="Lang.SavePersons" CssClass="btn btn-primary doSavePersons" />
             <asp:Button runat="server" ID="doApplyPersons1" OnClick="doApplyPersons1_Click" Text="Lang.ApplyPersons1" CssClass="btn btn-primary doApplyPersons" />
             <asp:CheckBox runat="server" ID="mHaveOpeningPersonDoc" CssClass="mHaveOpeningPersonDoc" Checked="False" Text="Lang.HaveOpeningDoc" />
            </div>
         </div>
        <div id='tabCheck' class='tab_content'>
            <table id="tablechecks" class="awesome-table table-with-border zebra">
                <tr class='th'>
                    <td class="row-cell width-50">
                        <asp:Label runat="server">Lang.Serial</asp:Label></td>
                    <td class="width-110 align-center">
                        <asp:Label runat="server">Lang.AssignorName</asp:Label></td>
                    <td class="width-80  align-center">
                        <asp:Label runat="server">Lang.OwnerCheck</asp:Label></td>
                    <td class="width-70  align-center">
                        <asp:Label runat="server">Lang.Bank</asp:Label></td>
                    <td class="width-30 align-center">
                        <asp:Label runat="server">Lang.Branch</asp:Label></td>
                    <td class="width-90 align-center">
                        <asp:Label runat="server">Lang.AccountNo</asp:Label></td>
                    <td class="width-60 align-center">
                        <asp:Label runat="server">Lang.IssueDate</asp:Label></td>
                    <td class="width-60 align-center">
                        <asp:Label runat="server">Lang.DueDate</asp:Label></td>
                    <td class="width-90 align-center">
                        <asp:Label runat="server">Lang.Amount</asp:Label></td>
                    <td class="width-60 align-center">
                        <asp:Label runat="server">Lang.Status</asp:Label></td>
<%--                    <td class="width-60 align-center">
                        <asp:Label runat="server">Lang.ReceiverBankName</asp:Label></td>
                    <td class="width-60 align-center">
                        <asp:Label runat="server">Lang.ReceiverPerson</asp:Label></td>--%>
                    <td class="center width-20 align-center">
                        <img src="img/add.gif" class="addcheck" /></td>
                </tr>
            </table>
            <br/>
            <div class="align-center">
             <asp:Button runat="server" ID="doSaveChecks" OnClick="doSaveChecks_Click" Text="Lang.doSaveChecks" CssClass="btn btn-primary doSaveChecks" />
             <asp:Button runat="server" ID="doApplyChecks1" OnClick="doApplyChecks1_Click" Text="Lang.doApplyChecks1" CssClass="btn btn-primary doApplyChecks" />
             <asp:CheckBox runat="server" ID="mHaveOpeningCheckDoc" CssClass="mHaveOpeningCheckDoc" Checked="false" Text="Lang.HaveOpeningDoc" />
           </div>
        </div>
        <div id='tabPayCheck' class='tab_content'>
            <table id="tablepaychecks" class="awesome-table table-with-border zebra full-width">
                <tr class='th'>
                    <td class="row-cell width-120">
                        <asp:Label runat="server">Lang.BankAccountCheckName</asp:Label></td>
                    <td class="width-70  align-center">
                        <asp:Label runat="server">Lang.Serial</asp:Label></td>
                    <td class="width-120  align-center">
                        <asp:Label runat="server">Lang.BankName</asp:Label></td>
                    <td class="width-100  align-center">
                        <asp:Label runat="server">Lang.ReceiverName</asp:Label></td>
                    <td class="width-90 align-center">
                        <asp:Label runat="server">Lang.PayDate</asp:Label></td>
                    <td class="width-90 align-center">
                        <asp:Label runat="server">Lang.DueDate</asp:Label></td>
                    <td class="width-100 align-center">
                        <asp:Label runat="server">Lang.Amount</asp:Label></td>
                    <td class="width-100 align-center">
                        <asp:Label runat="server">Lang.Status</asp:Label></td>
                    <td class="center width-20 align-center">
                        <img src="img/add.gif" class="addpaycheck" /></td>
                </tr>
            </table>
            <br/>
            <div class="align-center">
             <asp:Button runat="server" ID="doSavePayChecks" OnClick="doSavePayChecks_Click" Text="Lang.doSavePayChecks" CssClass="btn btn-primary doSavePayChecks" />
             <asp:Button runat="server" ID="doApplyPayChecks1" OnClick="doApplyPayChecks1_Click" Text="Lang.doApplyPayChecks1" CssClass="btn btn-primary doApplyPayChecks" />
             <asp:CheckBox runat="server" ID="mHaveOpeningPayCheckDoc" CssClass="mHaveOpeningPayCheckDoc" Checked="false" Text="Lang.HaveOpeningDoc" />
           </div>
          </div>
        <div id='tabTreePath' class='tab_content'>
            <table id="tableTreePath" class="awesome-table table-with-border zebra">
                <tr class='th'>
                    <td class="row-cell width-50">
                        <asp:Label runat="server">Lang.Code</asp:Label></td>
                    <td class="width-170  align-center">
                        <asp:Label runat="server">Lang.Title</asp:Label></td>
                    <td class="width-70  align-center">
                        <asp:Label runat="server">Lang.Nature</asp:Label></td>
                    <td class="width-100  align-center">
                        <asp:Label runat="server">Lang.Owe</asp:Label></td>
                    <td class="width-100 align-center">
                        <asp:Label runat="server">Lang.Due</asp:Label></td>
                </tr>
            </table>
            <br/>
            <div class="align-center">
             <asp:Button runat="server" ID="doSaveTreePath" OnClick="doSaveTreePath_Click" Text="Lang.SaveTreePath" CssClass="btn btn-primary doSaveTreePath" />
             <asp:Button runat="server" ID="doApplyTreePath" OnClick="doApplyTreePath_Click" Text="Lang.ApplyTreePath" CssClass="btn btn-primary doApplyTreePath" />
            </div>
        </div>
    </div>
    <script>
        function handleServices(node) {
            var cntService = 0;
            var repeatedBarCode = '';
            var repeatedTitle = '';
            function handleServiceCols() {
                //alert($('.mPersonShowPersonnelGroup').is(':checked'));
                if ($('.mServiceShowServiceType').find(':checkbox').is(':checked'))
                    $("#tableService td:nth-child(3),th:nth-child(3)").show();
                else
                    $("#tableService td:nth-child(3),th:nth-child(3)").hide();
                if ($('.mServiceShowUnits').find(':checkbox').is(':checked'))
                    $("#tableService td:nth-child(4),th:nth-child(4)").show();
                else
                    $("#tableService td:nth-child(4),th:nth-child(4)").hide();
                if ($('.mServiceShowStore').find(':checkbox').is(':checked'))
                    $("#tableService td:nth-child(5),th:nth-child(5)").show();
                else
                    $("#tableService td:nth-child(5),th:nth-child(5)").hide();
            }
            function sumTheLPBP() {
                var sum = 0;
                $('.thelpbp').each(function (index, value) {
                    sum += toFloat($(value).val(), 0) * toFloat($(this).parent().parent().find('.income').val(), 0);
                });
                $('.sumTtheLPBP').html(Lang.Sum + ': ' + numberWithCommas(sum));
                // 
            }
            function addService(node) {
                // get some data from last row
                var st = getNodeVal(node, "ServiceTypeName"), stid = getNodeVal(node, "ServiceTypeID")
                    , un = getNodeVal(node, "UnitName"), unid = getNodeVal(node, "UnitID")
                    , store = getNodeVal(node, "StoreName"), storeid = getNodeVal(node, "StoreID")
                    , pricing = getNodeVal(node, "Pricing");
                if (!node && cntService > 0) {
                    var lastID = cntService - 1;
                    stid = $('#stid_' + lastID).val();
                    st = $('#st_' + lastID).val();
                    unid = $('#unid_' + lastID).val();
                    un = $('#un_' + lastID).val();
                    storeid = $('#sid_' + lastID).val();
                    store = $('#s_' + lastID).val();
                    pricing = $('#pr_' + lastID).val();
                }

                if (node) {
                    if (getNodeVal(node, "RepeatedBarCode") == 'True')
                        repeatedBarCode = 'startfeildrepeated';
                    if (getNodeVal(node, "RepeatedTitle") == 'True')
                        repeatedTitle = 'startfeildrepeated';
                }
                var row = '<tr>';

                row += '<td><input type="hidden" name="ID" value="-1" /><input type="text" name="Code" id="cd_' + cntService + '"  class="form-control barcode input-sm ' + repeatedBarCode + ' " value="' + getNodeVal(node, "Code") + '" /></td>';
                row += '<td><input type="text" id="sn_' + cntService + '" name="Name" class="form-control input-sm servicename ' + repeatedTitle + ' " value="' + getNodeVal(node, "Name") + '" /></td>';

                row += '<td>'
                    + '<div class="input-group input-group-sm">'
                    + '<input type="text" id="st_' + cntService + '" name="ServiceTypeName" value="' + st + '" class="servicetypename form-control" />'
                    + '<input type="hidden" id="stid_' + cntService + '" name="ServiceTypeID" value="' + stid + '" class="servicetypeid" />'
                    + '<span class="input-group-btn">'
                    + '<input type="button" id="neuST_' + cntService + '" value="..." class="neuST btn btn-dark-grey"  title="' + Lang.NewGroup + '" />'
                    + '</span>'
                    + '</div>'
                    + '</td>';

                row += '<td>'
                   + '<div class="input-group input-group-sm">'
                   + '<input type="text" id="un_' + cntService + '" name="UnitName" value="' + un + '" class="unitname form-control" />'
                   + '<input type="hidden" id="unid_' + cntService + '" name="UnitID" value="' + unid + '" class="unitid" />'
                   + '<span class="input-group-btn">'
                   + '<input type="button" id="neuUN_' + cntService + '" value="..." class="neuUN btn btn-dark-grey"  title="' + Lang.NewUnit + '" />'
                   + '</span>'
                   + '</div>'
                   + '</td>';

                row += '<td>'
                   + '<div class="input-group input-group-sm">'
                   + '<input type="text" id="s_' + cntService + '" name="StoreName" value="' + store + '" class="storename form-control" />'
                   + '<input type="hidden" id="sid_' + cntService + '" name="StoreID" value="' + storeid + '" class="storeid" />'
                   + '<span class="input-group-btn">'
                   + '<input type="button" id="neuS_' + cntService + '" value="..." class="neuS btn btn-dark-grey" title="' + Lang.NewStore + '" />'
                   + '</span>'
                   + '</div>'
                   + '</td>';

                row += '<td class="align-center">'
                    + '<select id="pr_' + cntService + '"  class="Pricing" name="Pricing" v="' + pricing + '" >'
                    + '<option value="1">LIFO</option>'
                    + '<option value="0">FIFO</option>'
                    + '<option value="2">Mean</option>'
                    + '<option value="3">Manual(%)</option>'
                    + '<option value="4">Manual</option>'
                    + '</select>'
                    + '</td>';


                row += '<td class="align-center">'
                    + '<input type="text" id="in_' + cntService + '" name="Income" value="' + getNodeVal(node, "Income") + '" class="income validate[custom[integer]] form-control align-center input-sm number" />'
                    + '</td>';

                row += '<td class="align-center">'
                   + '<input type="text" id="bp_' + cntService + '" name="TheLPBP" value="' + getNodeVal(node, "TheLPBP") + '" class="thelpbp monetary validate[custom[integer]] form-control align-center input-sm number" />'
                   + '</td>';

                row += '<td class="align-center">'
                   + '<input type="text" id="sp_' + cntService + '" name="TheLPSP" value="' + getNodeVal(node, "TheLPSP") + '" class="thelpsp monetary validate[custom[integer]] form-control align-center input-sm number" />'
                   + '</td>';

                row += '<td class="align-center">'
                    + '<input type="text" id="ex_' + cntService + '" name="ExpireDate' + cntService + '" value="' + getNodeVal(node, "ExpireDate") + '" class=" dateSelect ExpireDate form-control input-sm" />'
                    + '</td>';

                row += '<td class="align-center action-remove">'
                + '<a id="r_' + cntService + '" class="btn btn-danger btn-xs btn-block btn-remove removeservice">'
                + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
                + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
                + '</a>'
                + '</td>';

                row += '</tr>';
                 repeatedBarCode = '';
                 repeatedTitle = '';
                 cntService++;
                return row;
            }

            function serviceRemove($who) {
                takeComma();
                var id = $who.attr('id'); // r_1
                id = id.substring(id.indexOf('_') + 1); // r_1  -->  1
                $('#cd_' + id).val(""); // Code 
                $('#stid_' + id).val(-1);// storeserviceID
                $who.parent().parent().hide();
                $who.parent().parent().find(':input').removeClass();
                putAllComma();
                Growl.Notice(Lang.DeletedSuccessfully);
            }

            function addServiceRow(notify) {
                var table = $('#tableService');
                $('tr:last', table).after(addService());
                setValue('#tableService tr:last .Pricing');
                var lastRow = $('tr:last', table);
                $('html, body').animate({
                    scrollTop: $('.doApplyServices').offset().top
                });
                if (notify)
                    Growl.Notice(Lang.RowAddedSuccessfully);

                dateSelect('#tableService tr:last .dateSelect');
                $('#tableService tr:last .ExpireDate').keydown(function (e) {
                    if (e.keyCode === 13 && !e.ctrlKey) {
                        e.preventDefault();
                        addServiceRow(true);
                        handleServiceCols();
                    }
                });

                autoComplete('Units', 'enumauto', "#tableService tr:last .unitname", "#justforfun", tableAutoPostPack);
                autoComplete('Stores', 'enumauto', "#tableService tr:last .storename", "#justforfun", tableAutoPostPack);
                autoComplete('ServiceType', 'servicetypeauto', "#tableService tr:last .servicetypename", "#f", tableAutoPostPack
                    , function (data, i, n, value) {
                        return '<div dir=rtl><b>' + data[1] + '</b><br />' + Lang.Code + ': ' + data[2] + '</div>';
                    });

                $('.neuST', lastRow).click(function () {
                    var id = $(this).attr('id');
                    id = id.substring(id.indexOf('_', 0) + 1); // neuST_1  -->  1
                    function _showcallback(v, m, f) {
                        if (v == 'OK') {
                            newShowcallback(v, m, f, 'servicetypeupdate', 'servicetypeinsert', "ServiceType", null, function () { });
                            if (!_Context.success)
                                return false;
                            $('#stid_' + id).val(_Context.resultID);
                            $('#st_' + id).val(f.Title);
                        }
                    }
                    _Context.id = -1;
                    var _url = 'get.aspx?op=GetHtml&table=servicetype&qd=servicetype&id=-1' + appendTime();
                    showPopup(_url, _showcallback, getButtons());
                });

                $('.neuUN', lastRow).click(function () {
                    var id = $(this).attr('id');
                    id = id.substring(id.indexOf('_', 0) + 1); // neuUN_1  -->  1
                    function _showcallback(v, m, f) {
                        if (v == 'OK') {
                            newShowcallback(v, m, f, 'enumupdate', 'enuminsert', "Units", null, function () { });
                            if (!_Context.success)
                                return false;
                            $('#unid_' + id).val(_Context.resultID);
                            $('#un_' + id).val(f.Title);
                        }
                    }
                    _Context.id = -1;
                    var _url = 'get.aspx?op=GetHtml&table=Units&qd=enum&id=-1' + appendTime();
                    showPopup(_url, _showcallback, getButtons());
                });

                $('.neuS', lastRow).click(function () {
                    var id = $(this).attr('id');
                    id = id.substring(id.indexOf('_', 0) + 1); // neuS_1  -->  1
                    function _showcallback(v, m, f) {
                        if (v == 'OK') {
                            newShowcallback(v, m, f, 'storesupdate', 'storesinsert', "Stores", null, function () { });
                            if (!_Context.success)
                                return false;
                            $('#sid_' + id).val(_Context.resultID);
                            $('#s_' + id).val(f.Title);
                        }
                    }
                    _Context.id = -1;
                    var _url = 'get.aspx?op=GetHtml&table=Stores&qd=store&id=-1' + appendTime();
                    showPopup(_url, _showcallback, getButtons());
                });

                $('.removeservice').click(function () {
                    var element = $(this);
                    doConfirm(function () {
                        serviceRemove(element);
                    }, Lang.ConfirmDelete);
                });
                $('.servicename', lastRow).focusout(repeatedDiagnosis);
                $('.number', lastRow).focusout(validateNum);
               // $('.servicetypename, .unitname, .storename', lastRow).focusout(checknesseceryfeildval);
                $('.thelpbp,.income').unbind('blur').blur(sumTheLPBP);
                putComma();
                //monetaryInputsSet();
            }

            $('.addService').click(function () {
                addServiceRow(true);
                handleServiceCols();
            });

            function repeatedDiagnosis() {
                var elem = $(this);
                var val = elem.val();
                if (val != '') {
                    $('#tableService .servicename')
                        .each(function() {
                            if ($(this).attr('id') != elem.attr('id')) {
                                if ($(this).val() == val) {
                                    elem.val('');
                                    Growl.Error(Lang.StartServiceRepeatedName, 3000);
                                    setTimeout(function() {
                                            elem.focus();
                                        },
                                        5);
                                }
                            }
                        });
                } else {
                    //Growl.Error(Lang.StartServiceName, 3000);
                    //setTimeout(function () {
                    //    elem.focus();
                    //},
                    //    5);
                }

            }

            // start with some empty rows
            //for (var i = 0; i < 10; i++)
            //    addServiceRow(); //.servicetypename, .unitname, .storename
            $('.doSaveServices').click(function () {
                var validaition = true;
                $('.servicename, .servicetypename, .unitname, .storename').each(function () {
                    var elm = $(this);
                    if (elm.val() === '') {
                        elm.css("background-color", "red");
                        validaition = false;
                    } else {
                        elm.css("background-color", "white");
                    }
                });
                if (!validaition) {
                    Growl.Error(Lang.FillRedField, 4000);
                    return false;
                }
                $('.ExpireDate').attr('value', 'ExpireDate');
                takeComma();
            });

            $('.doApplyServices').click(function () {
                var validaition = true;
                $('.servicename, .servicetypename, .unitname, .storename').each(function () {
                    var elm = $(this);
                    if (elm.val() === '') {
                        elm.css("background-color", "red");
                        validaition = false;
                    } else {
                        elm.css("background-color", "white");
                    }
                });
                if (!validaition) {
                    Growl.Error(Lang.FillRedField, 4000);
                    return false;
                }
                $('.ExpireDate').attr('name', 'ExpireDate');
                takeComma();
            });

            fillTable("get.aspx?op=GetXml&qd=startservice", '#tableService', addService);
            setValue('.Pricing');
            $('#tableService .servicename').focusout(repeatedDiagnosis);
           // $('.servicetypename, .unitname, .storename').focusout(checknesseceryfeildval);

            autoComplete('Units', 'enumauto', "#tableService .unitname", "#justforfun", tableAutoPostPack);
            autoComplete('Stores', 'enumauto', "#tableService .storename", "#justforfun", tableAutoPostPack);
            autoComplete('ServiceType', 'servicetypeauto', "#tableService .servicetypename", "#f", tableAutoPostPack
                , function (data, i, n, value) {
                    return '<div dir=rtl><b>' + data[1] + '</b><br />' + Lang.Code + ': ' + data[2] + '</div>';
                });

            $('#tableService .neuST').click(function () {
                var id = $(this).attr('id');
                id = id.substring(id.indexOf('_', 0) + 1); // neuST_1  -->  1
                function _showcallback(v, m, f) {
                    if (v == 'OK') {
                        newShowcallback(v, m, f, 'servicetypeupdate', 'servicetypeinsert', "ServiceType", null, function () { });
                        if (!_Context.success)
                            return false;
                        $('#stid_' + id).val(_Context.resultID);
                        $('#st_' + id).val(f.Title);
                    }
                }
                _Context.id = -1;
                var _url = 'get.aspx?op=GetHtml&table=servicetype&qd=servicetype&id=-1' + appendTime();
                showPopup(_url, _showcallback, getButtons());
            });

            $('#tableService .neuUN').click(function () {
                var id = $(this).attr('id');
                id = id.substring(id.indexOf('_', 0) + 1); // neuUN_1  -->  1
                function _showcallback(v, m, f) {
                    if (v == 'OK') {
                        newShowcallback(v, m, f, 'enumupdate', 'enuminsert', "Units", null, function () { });
                        if (!_Context.success)
                            return false;
                        $('#unid_' + id).val(_Context.resultID);
                        $('#un_' + id).val(f.Title);
                    }
                }
                _Context.id = -1;
                var _url = 'get.aspx?op=GetHtml&table=Units&qd=enum&id=-1' + appendTime();
                showPopup(_url, _showcallback, getButtons());
            });

            $('#tableService .neuS').click(function () {
                var id = $(this).attr('id');
                id = id.substring(id.indexOf('_', 0) + 1); // neuS_1  -->  1
                function _showcallback(v, m, f) {
                    if (v == 'OK') {
                        newShowcallback(v, m, f, 'storesupdate', 'storesinsert', "Stores", null, function () { });
                        if (!_Context.success)
                            return false;
                        $('#sid_' + id).val(_Context.resultID);
                        $('#s_' + id).val(f.Title);
                    }
                }
                _Context.id = -1;
                var _url = 'get.aspx?op=GetHtml&table=Stores&qd=store&id=-1' + appendTime();
                showPopup(_url, _showcallback, getButtons());
            });

            $('.removeservice').click(function () {
                var element = $(this);
                doConfirm(function () {
                    serviceRemove(element);
                }, Lang.ConfirmDelete);
            });

            if (cntService == 0) {
                addServiceRow(false);
                setDefault('Units', 'firstunit', "#tableService tr:last .unitname", "#tableService tr:last .unitid");
                setDefault('Stores', 'firststore', "#tableService tr:last .storename", "#tableService tr:last .storeid"); 
                setDefault('ServiceType', 'firstservicetype', "#tableService tr:last .servicetypename", "#tableService tr:last .servicetypeid");
            }
            sumTheLPBP();
            $('.thelpbp,.income').unbind('blur').blur(sumTheLPBP);
            handleServiceCols();
            $('.mServiceShowServiceType,.mServiceShowUnits,.mServiceShowStore').click(handleServiceCols);
        }
        function handlePersons() {
            var cntPerson = 0, repeatedCode = '';
            function handlePersonCols()
            {
                //alert($('.mPersonShowPersonnelGroup').is(':checked'));
                if ($('.mPersonShowPersonnelGroup').find(':checkbox').is(':checked'))
                    $("#tablePersons td:nth-child(6),th:nth-child(6)").show();
                else
                    $("#tablePersons td:nth-child(6),th:nth-child(6)").hide();
            }

            function personRemove($who) {
                takeComma();
                var id = $who.attr('id'); // r_1
                id = id.substring(id.indexOf('_') + 1); // r_1  -->  1
                $('#pcd_' + id).val(""); // Code pgid_
                $('#pgid_' + id).val(-1); // PersonnelGroupID
                $who.parent().parent().hide();
                $who.parent().parent().find(':input').removeClass();
                putAllComma();
                Growl.Notice(Lang.DeletedSuccessfully);
            };

            function addPerson(node) {
                // get some data from last row
                var pgid = getNodeVal(node, "PersonnelGroupID"), pg = getNodeVal(node, "PersonnelGroupName");
                if (!node && cntPerson > 0) {
                    var lastID = cntPerson - 1;
                    pgid = $('#pgid_' + lastID).val();
                    pg = $('#pg_' + lastID).val();
                }
                if (node) {
                    if (getNodeVal(node, "RepeatedCode") == 'True')
                        repeatedCode = 'startfeildrepeated';
                }
                var row = '<tr>';

                row += '<td><input type="text" name="PCode" id="pcd_' + cntPerson + '" value="' + getNodeVal(node, "Code") + '" class="form-control input-sm number ' + repeatedCode + ' " /></td>';
                row += '<td><input type="text" name="PName" value="' + getNodeVal(node, "Name") + '" class="form-control input-sm" /></td>';
                row += '<td><input type="text" name="PSurname" value="' + getNodeVal(node, "Surname") + '" class="form-control input-sm" /></td>';
                row += '<td class="align-center"><input type="text" name="POwe" value="' + getNodeVal(node, "Owe") + '" class="monetary powe validate[custom[integer]] form-control align-center input-sm number" /></td>';
                row += '<td class="align-center"><input type="text" name="PDue" value="' + getNodeVal(node, "Due") + '" class="monetary pdue validate[custom[integer]] form-control align-center input-sm number" /></td>';

                row += '<td>'
                    + '<div class="input-group input-group-sm">'
                    + '<input type="text" id="pg_' + cntPerson + '" name="PersonnelGroupName" value="' + pg + '" class="personnelgroupname form-control" />'
                    + '<input type="hidden" id="pgid_' + cntPerson + '" name="PersonnelGroupID" value="' + pgid + '" class="personnelgroupid" />'
                    + '<span class="input-group-btn">'
                    + '<input type="button" id="neuPG_' + cntPerson + '" value="..." class="neuPG btn btn-dark-grey"  title="' + Lang.NewGroup + '"/>'
                    + '</span>'
                    + '</div>'
                    + '</td>';


                row += '<td><input type="text" name="PHomeTelNum" value="' + getNodeVal(node, "HomeTelNum") + '" class="form-control input-sm" /></td>';
                row += '<td><input type="text" name="PHomeAddress" value="' + getNodeVal(node, "HomeAddress") + '" class="form-control input-sm phomeaddress" /></td>';

                row += '<td class="align-center action-remove">'
                + '<a id="pr_' + cntPerson + '" class="btn btn-danger btn-xs btn-block btn-remove removeperson">'
                + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
                + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
                + '</a>'
                + '</td>';

                row += '</tr>';
                cntPerson++;
                return row;
            }
            function addPersonRow(notify) {
                var table = $('#tablePersons');
                $('tr:last', table).after(addPerson());
                var lastRow = $('tr:last', table);
                $('html, body').animate({
                    scrollTop: $('.doApplyPersons').offset().top
                });
                if (notify)
                    Growl.Notice(Lang.RowAddedSuccessfully);

                $('#tablePersons tr:last .phomeaddress').keydown(function (e) {
                    if (e.keyCode === 13 && !e.ctrlKey) {
                        e.preventDefault();
                        addPersonRow(true);
                        handlePersonCols();
                    }
                });

                autoComplete('PersonnelGroup', 'personnelgroupauto', "#tablePersons tr:last .personnelgroupname", "#f", tableAutoPostPack);

                $('.neuPG', lastRow).click(function () {
                    var id = $(this).attr('id');
                    id = id.substring(id.indexOf('_', 0) + 1); // neuPG_1  -->  1
                    function _showcallback(v, m, f) {
                        if (v == 'OK') {
                            newShowcallback(v, m, f, 'personnelgroupupdate', 'personnelgroupinsert', "PersonnelGroup", null, function () { });
                            if (!_Context.success)
                                return false;
                            $('#pgid_' + id).val(_Context.resultID);
                            $('#pg_' + id).val(f.Title);
                        }
                    }
                    _Context.id = -1;
                    var _url = 'get.aspx?op=GetHtml&table=PersonnelGroup&qd=personnelgroup&id=-1' + appendTime();
                    showPopup(_url, _showcallback, getButtons());
                });

                $('.removeperson').click(function () {
                    var element = $(this);
                    doConfirm(function () {
                        personRemove(element);
                    }, Lang.ConfirmDelete);
                });
                $('.number', lastRow).focusout(validateNum);
                $('.personnelgroupname', lastRow).focusout(checknesseceryfeildval);
                putComma();
            }
            function checkOweDue() {
                // check Owe-Due
                var found = false;
                var badOwe = null;
                $('.powe').each(function () {
                    if (found)
                        return;
                    if (toFloat($(this).val(), 0) == 0)
                        return;
                    // check due
                    if (toFloat($(this).closest('tr').find('.pdue').val(), 0) > 0) {
                        found = true;
                        badOwe = $(this);
                    }
                });
                if (found)
                {
                    badOwe.focus();
                    Growl.Error(Lang.StartHasOweDue);
                    return false;
                }
                return true;
            }
            $('.addPerson').click(function () {
                addPersonRow(true);
                handlePersonCols();
            });

            $('.doSavePersons').click(function () {
                if (!checkOweDue())
                    return false;
                takeComma();
            });

            $('.doApplyPersons').click(function () {
                if (!checkOweDue())
                    return false;
                takeComma();
            });

            fillTable("get.aspx?op=GetXml&qd=startperson", '#tablePersons', addPerson);
            $('#tablePersons .personnelgroupname').focusout(checknesseceryfeildval);
            autoComplete('PersonnelGroup', 'personnelgroupauto', "#tablePersons .personnelgroupname", "#f", tableAutoPostPack);
            $('#tablePersons .neuPG').click(function () {
                var id = $(this).attr('id');
                id = id.substring(id.indexOf('_', 0) + 1); // neuPG_1  -->  1
                function _showcallback(v, m, f) {
                    if (v == 'OK') {
                        newShowcallback(v, m, f, 'personnelgroupupdate', 'personnelgroupinsert', "PersonnelGroup", null, function () { });
                        if (!_Context.success)
                            return false;
                        $('#pgid_' + id).val(_Context.resultID);
                        $('#pg_' + id).val(f.Title);
                    }
                }
                _Context.id = -1;
                var _url = 'get.aspx?op=GetHtml&table=PersonnelGroup&qd=personnelgroup&id=-1' + appendTime();
                showPopup(_url, _showcallback, getButtons());
            });


            $('.removeperson').click(function () {
                var element = $(this);
                doConfirm(function () {
                    personRemove(element);
                }, Lang.ConfirmDelete);
            });

            if (cntPerson == 0) {
                addPersonRow(false);
                setDefault('PersonnelGroup', 'firstpersonnelgroup', "#tablePersons tr:last .personnelgroupname", "#tablePersons tr:last .personnelgroupid");
            }
            handlePersonCols();
            $('.mPersonShowPersonnelGroup').click(handlePersonCols);
        }
        function handleTreePath() {
            var cntTreePath = 0;
            function addTreePath(node) {
                var row = '<tr>';

                row += '<td><input type="hidden" name="TPTreePath" value="' + getNodeVal(node, "TreePath") + '" />' + getNodeVal(node, "Code") + '</td>';
                row += '<td>' + getNodeVal(node, "Name") + '</td>';
                row += '<td>' + displayTopicNature(null, getNodeVal(node, "Nature")) + '</td>';
                row += '<td class="align-center"><input type="text" name="TPOwe" value="' + getNodeVal(node, "Owe") + '" class="monetary tpowe validate[custom[integer]] form-control align-center input-sm number" /></td>';
                row += '<td class="align-center"><input type="text" name="TPDue" value="' + getNodeVal(node, "Due") + '" class="monetary tpdue validate[custom[integer]] form-control align-center input-sm number" /></td>';

                row += '</tr>';

                return row;
            }
            fillTable("get.aspx?op=GetXml&qd=starttreepath", '#tableTreePath', addTreePath);
            function checkOweDue() {
                // check Owe-Due
                var found = false;
                var badOwe = null;
                $('.tpowe').each(function () {
                    if (found)
                        return;
                    if (toFloat($(this).val(), 0) == 0)
                        return;
                    // check due
                    if (toFloat($(this).closest('tr').find('.tpdue').val(), 0) > 0) {
                        found = true;
                        badOwe = $(this);
                    }
                });
                if (found) {
                    badOwe.focus();
                    Growl.Error(Lang.StartHasOweDue);
                    return false;
                }
                return true;
            }
            $('.doSaveTreePath').click(function () {
                if (!checkOweDue())
                    return false;
                takeComma();
            });
            $('.doApplyTreePath').click(function () {
                if (!checkOweDue())
                    return false;
                takeComma();
            });
        }
        function handleChecks(node) {
            var cntcheck = 0;
            function addCheck(node) {
                var row = '<tr>';

                row += '<td><input type="hidden" name="ID2" value="-1" />'
                    + '<input type="text" name="Serial" id="sr_' + cntcheck + '"  class="serial form-control input-sm" value="' + getNodeVal(node, "Serial") + '" />'
                    + '</td>';

                row += '<td>'
                    + '<div class="input-group input-group-sm">'
                    + '<input type="text" id="pn_' + cntcheck + '" name="PersonnelName" value="' + getNodeVal(node, "PersonnelName") + '" class="personnelname form-control" />'
                    + '<input type="hidden" id="pid_' + cntcheck + '" name="PersonnelID" value="' + getNodeVal(node, "PersonnelID") + '" class="personnelid" />' 
                    + '<span class="input-group-btn">'
                    + '<input type="button" id="neuP_' + cntcheck + '" value="..." class="neuP btn btn-dark-grey" title="'+ Lang.NewPerson +'" />'
                    + '</span>'
                    + '</div>'
                    + '</td>';

                row += '<td class="align-center">'
                    + '<input type="text" id="co_' + cntcheck + '" name="CheckOwnerName" value="' + getNodeVal(node, "CheckOwnerName") + '" class="checkownername form-control input-sm" />'
                    + '</td>';

                row += '<td>'
                    + '<div class="input-group input-group-sm">'
                    + '<input type="text" id="ba_' + cntcheck + '" name="BankAccountName" value="' + getNodeVal(node, "BankAccountName") + '" class="bankaccountname form-control" />'
                    + '<input type="hidden" id="baid_' + cntcheck + '" name="BankAccountID" value="' + getNodeVal(node, "BankAccountID") + '" class="bankaccountid" />'
                    + '<span class="input-group-btn">'
                    + '<input type="button" id="neuBA_' + cntcheck + '" value="..." class="neuBA btn btn-dark-grey" title="' + Lang.NewBank + '" />'
                    + '</span>'
                    + '</div>'
                    + '</td>';

                row += '<td class="align-center">'
                    + '<input type="text" id="br_' + cntcheck + '" name="Branch" value="' + getNodeVal(node, "Branch") + '" class="branch form-control align-center input-sm" />'
                    + '</td>';

                row += '<td class="align-center">'
                   + '<input type="text" id="an_' + cntcheck + '" name="AccountNo" value="' + getNodeVal(node, "AccountNo") + '" class="accountno validate[custom[integer]] form-control align-center input-sm" />'
                   + '</td>';

                row += '<td class="align-center">'
                    + '<input type="text" id="is_' + cntcheck + '" name="IssueDate' + cntcheck + '" value="' + getNodeVal(node, "IssueDate") + '" class=" dateSelect IssueDate form-control input-sm" />'
                    + '</td>';

                row += '<td class="align-center">'
                    + '<input type="text" id="du_' + cntcheck + '" name="DueDate' + cntcheck + '" value="' + getNodeVal(node, "DueDate") + '" class=" dateSelect DueDate form-control input-sm" />'
                    + '</td>';

                row += '<td class="align-center">'
                   + '<input type="text" id="am_' + cntcheck + '" name="Amount" value="' + getNodeVal(node, "Amount") + '" class="Amount monetary validate[custom[integer]] form-control align-center input-sm number" />'
                   + '</td>';

                row += '<td class="align-center">'
                    + '<select id="st_' + cntcheck + '"  class="Status" name="Status" v="' + getNodeVal(node, "Status") + '" >'
                    + '<option value="0">' + Lang.CheckStatusNew + '</option>'
                   // + '<option value="1">' + Lang.CheckStatusReceived + '</option>'
                  //  + '<option value="2">' + Lang.CheckStatusRefused + '</option>'
                  //  + '<option value="3">' + Lang.CheckStatusSleep + '</option>'
                  //  + '<option value="4">' + Lang.CheckStatusExpend + '</option>'
                  //  + '<!-- <option value="5">Lang.CheckStatusSleepReceive</option>-->'
                  //  + '<!--<option value="6">' + Lang.CheckStatusVisitor + '</option>-->'
                    + '</select>'
                    + '</td>';

                //row += '<td>'
                //    + '<div class="input-group input-group-sm">'
                //    + '<input type="text" id="rb_' + cntcheck + '" name="ReceiverBankName" value="' + getNodeVal(node, "ReceiverBankName") + '" class="receiverbankname form-control" />'
                //    + '<input type="hidden" id="rbt_' + cntcheck + '" name="ReceiverBankTreePath" value="' + getNodeVal(node, "ReceiverBankTreePath") + '" class="receiverbanktreepath" />' 
                //    + '<span class="input-group-btn">'
                //    + '<input type="button" id="neuRB_' + cntcheck + '" value="..." class="neuRB btn btn-dark-grey" title="' + Lang.NewPerson + '" />'
                //    + '</span>'
                //    + '</div>'
                //    + '</td>';

                //row += '<td>'
                //    + '<div class="input-group input-group-sm">'
                //    + '<input type="text" id="rpn_' + cntcheck + '" name="ReceiverPerson" value="' + getNodeVal(node, "ReceiverPerson") + '" class="receiverperson form-control" />'
                //    + '<input type="hidden" id="rpid_' + cntcheck + '" name="ReceiverPersonID" value="' + getNodeVal(node, "ReceiverPersonID") + '" class="receiverpersonid" />'
                //    + '<span class="input-group-btn">'
                //    + '<input type="button" id="neuR_' + cntcheck + '" value="..." class="neuR btn btn-dark-grey" title="' + Lang.NewPerson + '" />'
                //    + '</span>'
                //    + '</div>'
                //    + '</td>';

                row += '<td class="align-center action-remove">'
                + '<a id="r_' + cntcheck + '" class="btn btn-danger btn-xs btn-block btn-remove removecheck">'
                + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
                + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
                + '</a>'
                + '</td>';

                row += '</tr>';
                cntcheck++;
                setValue('.Status');
                return row;
            }

            function checkRemove($who) {
                takeComma();
               var id = $who.attr('id'); // r_1
                id = id.substring(id.indexOf('_') + 1); // r_1  -->  1
                $('#sr_' + id).val(""); // Serial 
                $('#baid_' + id).val(-1); // BankAccountID
                $who.parent().parent().hide();
                $who.parent().parent().find(':input').removeClass();
                putAllComma();
                Growl.Notice(Lang.DeletedSuccessfully);
            };

            function addCheckRow(notify) {
              var table = $('#tablechecks');
                $('tr:last', table).after(addCheck(null));
                var lastRow = $('tr:last', table);
                $('html, body').animate({
                    scrollTop: $('.doApplyChecks').offset().top
                });
                if (notify)
                    Growl.Notice(Lang.RowAddedSuccessfully);

                dateSelect('#tablechecks tr:last .dateSelect');
                $('#tablechecks tr:last .receiverperson').keydown(function (e) {
                    if (e.keyCode === 13 && !e.ctrlKey) {
                        e.preventDefault();
                        addCheckRow(true);
                    }
                });

                autoComplete('Personnel', 'personnelselectauto', '#tablechecks tr:last .personnelname', '#justforfun', tableAutoPostPack
                    , function (data, i, n, value) {
                        //$('#subDetailForm #PersonnelID').val('');
                        return formatPersonnelItem(data);
                    });
                autoComplete('Personnel', 'personnelselectauto', '#tablechecks tr:last .receiverperson', '#justforfun', tableAutoPostPack
                    , function (data, i, n, value) {
                        // $('#subDetailForm #ReceiverPersonID').val('');
                        return formatPersonnelItem(data);
                    });

                autoComplete('BankAccountCheck', 'bankaccountautoforchecks', '#tablechecks tr:last .bankaccountname', '#justforfun', tableAutoPostPack);

                autoComplete('Trees', 'treesbanksauto', '#tablechecks tr:last .receiverbankname', '#justforfun', tableAutoPostPack, formatTreePath);

                $('.neuP', lastRow).click(function () {
                    var id = $(this).attr('id');
                    id = id.substring(id.indexOf('_', 0) + 1); // neuP_1  -->  1
                    function _showcallback(v, m, f) {
                        if (v == 'OK') {
                            newShowcallback(v, m, f, 'personupdate', 'personinsert', "Personnel", null, function () { });
                            if (!_Context.success)
                                return false;
                            $('#pid_' + id).val(_Context.resultID);
                            $('#pn_' + id).val(f.Name + ' ' + f.Surname);
                        }
                    }
                    _Context.id = -1;
                    var _url = 'get.aspx?op=GetHtml&table=Personnel&view=person&qd=person&id=-1' + appendTime();
                    showPopup(_url, _showcallback, getButtons());
                });

                $('.neuR', lastRow).click(function () {
                    var id = $(this).attr('id');
                    id = id.substring(id.indexOf('_', 0) + 1); // neuP_1  -->  1
                    function _showcallback(v, m, f) {
                        if (v == 'OK') {
                            newShowcallback(v, m, f, 'personupdate', 'personinsert', "Personnel", null, function () { });
                            if (!_Context.success)
                                return false;
                            $('#rpid_' + id).val(_Context.resultID);
                            $('#rpn_' + id).val(f.Name + ' ' + f.Surname);
                        }
                    }
                    _Context.id = -1;
                    var _url = 'get.aspx?op=GetHtml&table=Personnel&view=person&qd=person&id=-1' + appendTime();
                    showPopup(_url, _showcallback, getButtons());
                });

                $('.neuBA', lastRow).click(function () {
                    var id = $(this).attr('id');
                    id = id.substring(id.indexOf('_', 0) + 1); // neuP_1  -->  1
                    function _showcallback(v, m, f) {
                        if (v == 'OK') {
                            newShowcallback(v, m, f, 'bankaccountupdate', 'bankaccountinsert', "BankAccount", null, function () { });
                            if (!_Context.success)
                                return false;
                            $('#baid_' + id).val(_Context.resultID);
                            $('#ba_' + id).val(f.BankName);
                        }
                    }
                    _Context.id = -1;
                    var _url = 'get.aspx?op=GetHtml&table=BankAccount&qd=bankaccount&BankAccountType=2&id=-1' + appendTime();
                    showPopup(_url, _showcallback, getButtons());
                });

                $('.neuRB', lastRow).click(function () {
                    var id = $(this).attr('id');
                    id = id.substring(id.indexOf('_', 0) + 1); // neuP_1  -->  1
                    function _showcallback(v, m, f) {
                        if (v == 'OK') {
                            newShowcallback(v, m, f, 'bankaccountupdate', 'bankaccountinsert', "BankAccount", null, function () { });
                            if (!_Context.success)
                                return false;
                            $('#rbt_' + id).val(f.TreePathName);
                            $('#rb_' + id).val(f.TreePath);
                        }
                    }
                    _Context.id = -1;
                    var _url = 'get.aspx?op=GetHtml&table=BankAccount&qd=bankaccount&id=-1' + appendTime();
                    showPopup(_url, _showcallback, getButtons());
                });

                $('.removecheck').click(function () {
                    var element = $(this);
                    doConfirm(function () {
                        checkRemove(element);
                    }, Lang.ConfirmDelete);
                });
                $('.number', lastRow).focusout(validateNum);
              //  $('.personnelname, .bankaccountname, .serial', lastRow).focusout(checknesseceryfeildval);
                putComma();
            }
            $('.addcheck').click(function () {
                addCheckRow(true);
            });

            // start with some empty rows
            //for (var i = 0; i < 10; i++)
            //    addServiceRow();
            $('.doSaveChecks').click(function () {
                var validaition = true;
                $('.personnelname, .bankaccountname, .serial').each(function () {
                    var elm = $(this);
                    if (elm.val() === '') {
                        elm.css("background-color", "red");
                        validaition = false;
                    } else {
                        elm.css("background-color", "white");
                    }
                });
                if (!validaition) {
                    Growl.Error(Lang.FillRedField, 4000);
                    return false;
                }
                $('.IssueDate').attr('name', 'IssueDate');
                $('.DueDate').attr('name', 'DueDate');
                takeComma();
            });
            $('.doApplyChecks').click(function () {
                var validaition = true;
                $('.personnelname, .bankaccountname, .serial').each(function () {
                    var elm = $(this);
                    if (elm.val() === '') {
                        elm.css("background-color", "red");
                        validaition = false;
                    } else {
                        elm.css("background-color", "white");
                    }
                });
                if (!validaition) {
                    Growl.Error(Lang.FillRedField, 4000);
                    return false;
                }
                $('.IssueDate').attr('name', 'IssueDate');
                $('.DueDate').attr('name', 'DueDate');
                takeComma();
            });
            fillTable("get.aspx?op=GetXml&qd=startcheck", '#tablechecks', addCheck);
            //$('.personname, .bankaccountname, .serial').focusout(checknesseceryfeildval);

            $('#tablechecks .neuP').click(function () {
                var id = $(this).attr('id');
                id = id.substring(id.indexOf('_', 0) + 1); // neuP_1  -->  1
                function _showcallback(v, m, f) {
                    if (v == 'OK') {
                        newShowcallback(v, m, f, 'personupdate', 'personinsert', "Personnel", null, function () { });
                        if (!_Context.success)
                            return false;
                        $('#pid_' + id).val(_Context.resultID);
                        $('#pn_' + id).val(f.Name + ' ' + f.Surname);
                    }
                }
                _Context.id = -1;
                var _url = 'get.aspx?op=GetHtml&table=Personnel&view=person&qd=person&id=-1' + appendTime();
                showPopup(_url, _showcallback, getButtons());
            });

            $('#tablechecks .neuR').click(function () {
                var id = $(this).attr('id');
                id = id.substring(id.indexOf('_', 0) + 1); // neuP_1  -->  1
                function _showcallback(v, m, f) {
                    if (v == 'OK') {
                        newShowcallback(v, m, f, 'personupdate', 'personinsert', "Personnel", null, function () { });
                        if (!_Context.success)
                            return false;
                        $('#rpid_' + id).val(_Context.resultID);
                        $('#rpn_' + id).val(f.Name + ' ' + f.Surname);
                    }
                }
                _Context.id = -1;
                var _url = 'get.aspx?op=GetHtml&table=Personnel&view=person&qd=person&id=-1' + appendTime();
                showPopup(_url, _showcallback, getButtons());
            });

            $('#tablechecks .neuBA').click(function () {
                var id = $(this).attr('id');
                id = id.substring(id.indexOf('_', 0) + 1); // neuP_1  -->  1
                function _showcallback(v, m, f) {
                    if (v == 'OK') {
                        newShowcallback(v, m, f, 'bankaccountupdate', 'bankaccountinsert', "BankAccount", null, function () { });
                        if (!_Context.success)
                            return false;
                        $('#baid_' + id).val(_Context.resultID);
                        $('#ba_' + id).val(f.BankName);
                    }
                }
                _Context.id = -1;
                var _url = 'get.aspx?op=GetHtml&table=BankAccount&qd=bankaccount&BankAccountType=2&id=-1' + appendTime();
                showPopup(_url, _showcallback, getButtons());
            });

            $('#tablechecks .neuRB').click(function () {
                var id = $(this).attr('id');
                id = id.substring(id.indexOf('_', 0) + 1); // neuP_1  -->  1
                function _showcallback(v, m, f) {
                    if (v == 'OK') {
                        newShowcallback(v, m, f, 'bankaccountupdate', 'bankaccountinsert', "BankAccount", null, function () { });
                        if (!_Context.success)
                            return false;
                        $('#rbt_' + id).val(f.TreePathName);
                        $('#rb_' + id).val(f.TreePath);
                    }
                }
                _Context.id = -1;
                var _url = 'get.aspx?op=GetHtml&table=BankAccount&qd=bankaccount&id=-1' + appendTime();
                showPopup(_url, _showcallback, getButtons());
            });

            autoComplete('Personnel', 'personnelselectauto', '#tablechecks .personnelname', '#justforfun', tableAutoPostPack
            , function (data, i, n, value) {
            //$('#subDetailForm #PersonnelID').val('');
            return formatPersonnelItem(data);
            });
            autoComplete('Personnel', 'personnelselectauto', '#tablechecks .receiverperson', '#justforfun', tableAutoPostPack
                , function (data, i, n, value) {
                    // $('#subDetailForm #ReceiverPersonID').val('');
                    return formatPersonnelItem(data);
                });

            autoComplete('Personnel', 'personnelselectauto', '#tablechecks .assignorname', '#justforfun', tableAutoPostPack
            , function (data, i, n, value) {
                // $('#subDetailForm #AssignorID').val('');
                return formatPersonnelItem(data);
            });
            autoComplete('BankAccountCheck', 'bankaccountautoforchecks', '#tablechecks .bankaccountname', '#justforfun', tableAutoPostPack);

            autoComplete('Trees', 'treesbanksauto', '#tablechecks .receiverbankname', '#justforfun', tableAutoPostPack, formatTreePath);

            $('.removecheck').click(function () {
                var element = $(this);
                doConfirm(function () {
                    checkRemove(element);
                }, Lang.ConfirmDelete);
            });
            if (cntcheck == 0)
                addCheckRow(false);
            setValue('.Status');
        }
        function handlePayChecks(node) {
            var cntpaycheck = 0;
            function addPayCheck(node) {
                var row = '<tr>';

                row += '<td>'
                    + '<div class="input-group input-group-sm">'
                    + '<input type="hidden" name="ID3" value="-1" />'
                    + '<input type="text" id="bac_' + cntpaycheck + '" name="BankAccountCheckName" value="' + getNodeVal(node, "BankAccountCheckName") + '" class="bankaccountcheckname form-control" />'
                    + '<input type="hidden" id="bacid_' + cntpaycheck + '" name="BankAccountCheckID" value="' + getNodeVal(node, "BankAccountCheckID") + '" class="bankaccountcheckid" />'
                    + '<span class="input-group-btn">'
                    + '<input type="button" id="neuBAC_' + cntpaycheck + '" value="..." class="neuBAC btn btn-dark-grey" title="' + Lang.NewBankAccountCheck + '" />'
                    + '</span>'
                    + '</div>'
                    + '</td>';

                row += '<td><input type="text" name="PSerial" id="psr_' + cntpaycheck + '"  class="pserial form-control input-sm" value="' + getNodeVal(node, "PSerial") + '" />' 
                    + '</td>';
                
                row += '<td>'
                    + '<div class="input-group input-group-sm">'
                    + '<input type="text" id="bn_' + cntpaycheck + '" name="BankName" value="' + getNodeVal(node, "BankName") + '" class="bankname form-control" />'
                    + '<input type="hidden" id="bnt_' + cntpaycheck + '" name="BankTreePath" value="' + getNodeVal(node, "BankTreePath") + '" class="banktreepath" />'
                    + '<span class="input-group-btn">'
                    + '<input type="button" id="neuB_' + cntpaycheck + '" value="..." class="neuB btn btn-dark-grey" title="' + Lang.NewBank + '" />'
                    + '</span>'
                    + '</div>'
                    + '</td>';

                row += '<td>'
                    + '<div class="input-group input-group-sm">'
                    + '<input type="text" id="rn_' + cntpaycheck + '" name="ReceiverName" value="' + getNodeVal(node, "ReceiverName") + '" class="receivername form-control" />'
                    + '<input type="hidden" id="rid_' + cntpaycheck + '" name="ReceiverID" value="' + getNodeVal(node, "ReceiverID") + '" class="receiverid" />'
                    + '<span class="input-group-btn">'
                    + '<input type="button" id="neuR_' + cntpaycheck + '" value="..." class="neuR btn btn-dark-grey" title="' + Lang.NewPerson + '" />'
                    + '</span>'
                    + '</div>'
                    + '</td>';

                row += '<td class="align-center">'
                    + '<input type="text" id="ppd_' + cntpaycheck + '" name="PPayDate' + cntpaycheck + '" value="' + getNodeVal(node, "PPayDate") + '" class="dateSelect PPayDate form-control input-sm" />'
                    + '</td>';

                row += '<td class="align-center">'
                    + '<input type="text" id="pdu_' + cntpaycheck + '" name="PDueDate' + cntpaycheck + '" value="' + getNodeVal(node, "PDueDate") + '" class="dateSelect PDueDate form-control input-sm" />'
                    + '</td>';

                row += '<td class="align-center">'
                   + '<input type="text" id="pam_' + cntpaycheck + '" name="PAmount" value="' + getNodeVal(node, "PAmount") + '" class="PAmount monetary validate[custom[integer]] form-control align-center input-sm number" />'
                   + '</td>';

                row += '<td class="align-center">'
                    + '<select id="st_' + cntpaycheck + '"  class="PStatus" name="PStatus" v="' + getNodeVal(node, "PStatus") + '" >'
                    + '<option value="0">' + Lang.CheckStatusNew + '</option>'
                  //  + '<option value="1">' + Lang.CheckStatusReceived + '</option>'
                  //  + '<option value="2">' + Lang.CheckStatusRefused + '</option>'
                    + '</select>'
                    + '</td>';

               row += '<td class="align-center action-remove">'
                + '<a id="r_' + cntpaycheck + '" class="btn btn-danger btn-xs btn-block btn-remove removepaycheck">'
                + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
                + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
                + '</a>'
                + '</td>';

                row += '</tr>';
                cntpaycheck++;
                setValue('.PStatus');
                return row;
            }

            function paycheckRemove($who) {
                takeComma();
                var id = $who.attr('id'); // r_1
                id = id.substring(id.indexOf('_') + 1); // r_1  -->  1
                $('#psr_' + id).val(""); // Serial 
                $('#bacid_' + id).val(-1); // BankAccountCheckID
                $who.parent().parent().hide();
                $who.parent().parent().find(':input').removeClass();
                putAllComma();
                Growl.Notice(Lang.DeletedSuccessfully);
            };

            function addPayCheckRow(notify) {
                var table = $('#tablepaychecks');
                $('tr:last', table).after(addPayCheck());
                var lastRow = $('tr:last', table);
                $('html, body').animate({
                    scrollTop: $('.doSavePayChecks').offset().top
                });
                if (notify)
                    Growl.Notice(Lang.RowAddedSuccessfully);

                dateSelect('#tablepaychecks tr:last .dateSelect');
                $('#tablepaychecks tr:last .PStatus').keydown(function (e) {
                    if (e.keyCode === 13 && !e.ctrlKey) {
                        e.preventDefault();
                        addPayCheckRow(true);
                    }
                });

                autoComplete('Personnel', 'personnelselectauto', '#tablepaychecks tr:last .receivername', '#justforfun', tableAutoPostPack
                    , function (data, i, n, value) {
                        //$('#subDetailForm #PersonnelID').val('');
                        return formatPersonnelItem(data);
                    });

                autoComplete('BankAccountCheck', 'bankaccountcheckauto', '#tablepaychecks tr:last .bankaccountcheckname', '#tablepaychecks tr:last .bankaccountcheckid'
                , function (event, data, formatted) {
                    $('#tablepaychecks tr:last .bankname').val(data[3]);
                    $('#tablepaychecks tr:last .banktreepath').val(data[2]);

                    // serial : get unused one
                    var serial = toInt(getServerValue("reternpaycheckminserial", "PayCheck", "&BankAccountCheckID=" + data[0]), 0);
                    if (!serial) {
                        alert(Lang.PayCheckJsM1);
                        runQuery('bankaccountcheckupdateformpaycheck', 'PayCheck', '&ID=' + data[0]);
                    }
                    else {
                        $('#tablepaychecks tr:last .pserial').val(serial);
                    }

                    $('#tablepaychecks tr:last .pserial').attr('readonly', false);
                    // #Res.Serial, #Res.Serial Serial2, Amount, DueDate, ReceiverName, No, documentNo, Status
                    autoComplete('BankAccountCheck', 'bankaccountcheckserialauto', '#tablepaychecks tr:last .pserial', '#justforfun'
                        , function (event, data, formatted) {
                            if (data[5] || data[6] || data[7] == 11) // has no or DocumentNo or disproof
                                $('#tablepaychecks tr:last .pserial').val('');
                            else
                                $('#tablepaychecks tr:last .pserial').val(data[0]);
                        }, function (data, i, n, value) {
                            // $('#Serial').val('');
                            if (data[5] && data[7] != 11) {
                                // used// data[7] != 11//disproof checks
                                return Lang.Serial + " : " + data[0] + "<br />" + Lang.PayCheck + " - " + Lang.No + " : " + data[5]
                                    + ", " + Lang.Reciever + " : " + data[4]
                                   + "<br />" + Lang.DueDate + " : " + data[3]
                                   + ", " + Lang.Amount + " : " + data[2];
                            }
                            if (data[6]) {
                                return Lang.Serial + " : " + data[0] + "<br />" + Lang.UsedInDocAccounting + " : " + data[6];
                            }
                            if (data[7] == 11) {
                                return Lang.Serial + " : " + data[0] + "<br />" + Lang.DisProof;
                            }
                            return Lang.Serial + " : " + data[0] + "<br />" + Lang.Unused;
                        }, null, null, null, 'TreePath=NULL&BankAccountCheckID=' + data[0]);
                });
                
                autoComplete('Trees', 'treesbanksauto', '#tablepaychecks tr:last .bankname', '#justforfun', tableAutoPostPack, formatTreePath);

                $('.neuB', lastRow).click(function () {
                    var id = $(this).attr('id');
                    id = id.substring(id.indexOf('_', 0) + 1); // neuP_1  -->  1
                    function _showcallback(v, m, f) {
                        if (v == 'OK') {
                            newShowcallback(v, m, f, 'bankaccountupdate', 'bankaccountinsert', "BankAccount", null, function () { });
                            if (!_Context.success)
                                return false;
                            $('#bn_' + id).val(f.TreePathName);
                            $('#bnt_' + id).val(f.TreePath);
                        }
                    }
                    _Context.id = -1;
                    var _url = 'get.aspx?op=GetHtml&table=BankAccount&qd=bankaccount&id=-1' + appendTime();
                    showPopup(_url, _showcallback, getButtons());
                });

                $('.neuR', lastRow).click(function () {
                    var id = $(this).attr('id');
                    id = id.substring(id.indexOf('_', 0) + 1); // neuP_1  -->  1
                    function _showcallback(v, m, f) {
                        if (v == 'OK') {
                            newShowcallback(v, m, f, 'personupdate', 'personinsert', "Personnel", null, function () { });
                            if (!_Context.success)
                                return false;
                            $('#rid_' + id).val(_Context.resultID);
                            $('#rn_' + id).val(f.Name + ' ' + f.Surname);
                        }
                    }
                    _Context.id = -1;
                    var _url = 'get.aspx?op=GetHtml&table=Personnel&view=person&qd=person&id=-1' + appendTime();
                    showPopup(_url, _showcallback, getButtons());
                });

                $('.neuBAC', lastRow).click(function () {
                    var id = $(this).attr('id');
                    id = id.substring(id.indexOf('_', 0) + 1); // neuP_1  -->  1
                    function _showcallback(v, m, f) {
                        if (v == 'OK') {
                            newShowcallback(v, m, f, 'bankaccountcheckupdate', 'bankaccountcheckinsert', "BankAccountCheck", null, function () { });
                            if (!_Context.success)
                                return false;
                            $('#bacid_' + id).val(_Context.resultID);
                            $('#bac_' + id).val(f.BankAccountName);
                        }
                    }
                    _Context.id = -1;
                    var _url = 'get.aspx?op=GetHtml&table=BankAccountCheck&qd=bankaccountcheck&id=-1' + appendTime();
                    showPopup(_url, _showcallback, getButtons());
                });

                $('.removepaycheck').click(function () {
                    var element = $(this);
                    doConfirm(function () {
                        paycheckRemove(element);
                    }, Lang.ConfirmDelete);
                });
                $('.number', lastRow).focusout(validateNum);
               // $('.bankname, .bankaccountcheckname, .pserial, .pPayDate, .pDueDate, .receivername', lastRow).focusout(checknesseceryfeildval);
                putComma();
            }
            $('.addpaycheck').click(function () {
                addPayCheckRow(true);
            });

            // start with some empty rows
            //for (var i = 0; i < 10; i++)
            //    addServiceRow();
            $('.doSavePayChecks').click(function () {
                var validaition = true;
                $('.bankname, .bankaccountcheckname, .pserial, .PPayDate, .PDueDate, .receivername').each(function () {
                    var elm = $(this);
                    if (elm.val() === '') {
                        elm.css("background-color", "red");
                        validaition = false;
                    } else {
                        elm.css("background-color", "white");
                    }
                });
                if (!validaition) {
                    Growl.Error(Lang.FillRedField, 4000);
                    return false;
                }
                $('.PPayDate').attr('name', 'PPayDate');
                $('.PDueDate').attr('name', 'PDueDate');
                takeComma();
            });
            $('.doApplyPayChecks').click(function () {
                var validaition = true;
                $('.bankname, .bankaccountcheckname, .pserial, .PPayDate, .PDueDate, .receivername').each(function () {
                    var elm = $(this);
                    if (elm.val() === '') {
                        elm.css("background-color", "red");
                        validaition = false;
                    } else {
                        elm.css("background-color", "white");
                    }
                });
                if (!validaition) {
                    Growl.Error(Lang.FillRedField, 4000);
                    return false;
                }
                $('.PPayDate').attr('name', 'PPayDate');
                $('.PDueDate').attr('name', 'PDueDate');
                takeComma();
            });

            fillTable("get.aspx?op=GetXml&qd=startpaycheck", '#tablepaychecks', addPayCheck);
            //$('.bankname, .bankaccountcheckname, .pserial, .pPayDate, .pDueDate, .receivername').focusout(checknesseceryfeildval);

            $('#tablepaychecks .neuB').click(function () {
                var id = $(this).attr('id');
                id = id.substring(id.indexOf('_', 0) + 1); // neuP_1  -->  1
                function _showcallback(v, m, f) {
                    if (v == 'OK') {
                        newShowcallback(v, m, f, 'bankaccountupdate', 'bankaccountinsert', "BankAccount", null, function () { });
                        if (!_Context.success)
                            return false;
                        $('#bn_' + id).val(f.TreePathName);
                        $('#bnt_' + id).val(f.TreePath);
                    }
                }
                _Context.id = -1;
                var _url = 'get.aspx?op=GetHtml&table=BankAccount&qd=bankaccount&id=-1' + appendTime();
                showPopup(_url, _showcallback, getButtons());
            });

            $('#tablepaychecks .neuR').click(function () {
                var id = $(this).attr('id');
                id = id.substring(id.indexOf('_', 0) + 1); // neuP_1  -->  1
                function _showcallback(v, m, f) {
                    if (v == 'OK') {
                        newShowcallback(v, m, f, 'personupdate', 'personinsert', "Personnel", null, function () { });
                        if (!_Context.success)
                            return false;
                        $('#rid_' + id).val(_Context.resultID);
                        $('#rn_' + id).val(f.Name + ' ' + f.Surname);
                    }
                }
                _Context.id = -1;
                var _url = 'get.aspx?op=GetHtml&table=Personnel&view=person&qd=person&id=-1' + appendTime();
                showPopup(_url, _showcallback, getButtons());
            });

            $('#tablepaychecks .neuBAC').click(function () {
                var id = $(this).attr('id');
                id = id.substring(id.indexOf('_', 0) + 1); // neuP_1  -->  1
                function _showcallback(v, m, f) {
                    if (v == 'OK') {
                        newShowcallback(v, m, f, 'bankaccountcheckupdate', 'bankaccountcheckinsert', "BankAccountCheck", null, function () { });
                        if (!_Context.success)
                            return false;
                        $('#bacid_' + id).val(_Context.resultID);
                        $('#bac_' + id).val(f.BankAccountName);
                    }
                }
                _Context.id = -1;
                var _url = 'get.aspx?op=GetHtml&table=BankAccountCheck&qd=bankaccountcheck&id=-1' + appendTime();
                showPopup(_url, _showcallback, getButtons());
            });

            autoComplete('Personnel', 'personnelselectauto', '#tablepaychecks .receivername', '#justforfun', tableAutoPostPack
                , function (data, i, n, value) {
                    //$('#subDetailForm #PersonnelID').val('');
                    return formatPersonnelItem(data);
                });

            autoComplete('BankAccountCheck', 'bankaccountcheckauto', '#tablepaychecks .bankaccountcheckname', '#tablepaychecks .bankaccountcheckid'
            , function (event, data, formatted) {
                $('#tablepaychecks tr:last .bankname').val(data[3]);
                $('#tablepaychecks tr:last .banktreepath').val(data[2]);

                // serial : get unused one
                var serial = toInt(getServerValue("reternpaycheckminserial", "PayCheck", "&BankAccountCheckID=" + data[0]), 0);
                if (!serial) {
                    alert(Lang.PayCheckJsM1);
                    runQuery('bankaccountcheckupdateformpaycheck', 'PayCheck', '&ID=' + data[0]);
                }
                else {
                    $('#tablepaychecks tr:last .pserial').val(serial);
                }

                $('#tablepaychecks tr:last .pserial').attr('readonly', false);
                // #Res.Serial, #Res.Serial Serial2, Amount, DueDate, ReceiverName, No, documentNo, Status
                autoComplete('BankAccountCheck', 'bankaccountcheckserialauto', '#tablepaychecks .pserial', '#justforfun'
                    , function (event, data, formatted) {
                        if (data[5] || data[6] || data[7] == 11) // has no or DocumentNo or disproof
                            $('#tablepaychecks tr:last .pserial').val('');
                        else
                            $('#tablepaychecks tr:last .pserial').val(data[0]);
                    }, function (data, i, n, value) {
                        // $('#Serial').val('');
                        if (data[5] && data[7] != 11) {
                            // used// data[7] != 11//disproof checks
                            return Lang.Serial + " : " + data[0] + "<br />" + Lang.PayCheck + " - " + Lang.No + " : " + data[5]
                                + ", " + Lang.Reciever + " : " + data[4]
                               + "<br />" + Lang.DueDate + " : " + data[3]
                               + ", " + Lang.Amount + " : " + data[2];
                        }
                        if (data[6]) {
                            return Lang.Serial + " : " + data[0] + "<br />" + Lang.UsedInDocAccounting + " : " + data[6];
                        }
                        if (data[7] == 11) {
                            return Lang.Serial + " : " + data[0] + "<br />" + Lang.DisProof;
                        }
                        return Lang.Serial + " : " + data[0] + "<br />" + Lang.Unused;
                    }, null, null, null, 'TreePath=NULL&BankAccountCheckID=' + data[0]);
            });

            autoComplete('Trees', 'treesbanksauto', '#tablepaychecks .bankname', '#justforfun', tableAutoPostPack, formatTreePath);

            $('.removepaycheck').click(function () {
                var element = $(this);
                doConfirm(function () {
                    paycheckRemove(element);
                }, Lang.ConfirmDelete);
            });
            if (cntpaycheck == 0)
                addPayCheckRow(false);
            setValue('.PStatus');
        }
        function putComma() {
            $('.monetary').keyup(function () {
                $(this).val(numberWithCommasAllowDP(stripCommas($(this).val())));
            });
            $('.monetary').keyup(handleMonetaryKeys);
        }
        function takeComma() {
            $('.monetary').each(function () {
                $(this).val(fromMonetary($(this).val().replace(/\,/g, '')));
            });
        }

        function takeZeros() {
            $('.income').each(function () {
                var parts = $(this).val().toString().replace(/,/g, "").split(".");
                if (parts.length > 1 && parseInt(parts[1], 10) == 0)
                    $(this).val(parts[0]);
                else if (parts[0])
                    $(this).val(toFloat($(this).val(), 0).toFixed(_Context.decimalPoints));
                
            });
        }

        function validateNum() {
            var elem = $(this);
            var fPattern = new RegExp(/^[\-\+]?(([0-9]+)([\.,]([0-9]+))?|([\.,]([0-9]+))?)$/);
            if (!fPattern.test(stripCommas(elem.val()))) {
                Growl.Error(Lang.EnterCorrectNumber, 3000);
                elem.val('');
                setTimeout(function () {
                    elem.focus();
                }, 5);
            }
        }

        function checknesseceryfeildval() {
            var elem = $(this);
            var val = elem.val();
            if (val === '')  {
                Growl.Error(Lang.StartNesseceryFeild, 3000);
                //setTimeout(function () {
                //    elem.focus();
                //},5);
            }
        }

        function putAllComma() {
            $('.monetary').each(function () {
                $(this).val(numberWithCommas(toMonetary($(this).val())));
            });
        }
        $(function () {
            $('.userid').val(_Context.userID);
            $('.sidebar').addClass('collapsed');
            tabify();
            var u = 'get.aspx?op=Auto&table=documentaccounting&qd=getopeningdoc' + appendTime();
            var row = getServerValues(u);
            if (row != null && row[0] != '' && row[1] != '') {
                $('#OpeningDocNo').attr('href', "list.aspx?table=DocumentAccounting&id=" + row[0]);
                $('#OpeningDocNo').html(row[1]);
            }
            handleServices();
            handlePersons();
            handleTreePath();
            handleChecks();
            handlePayChecks();

            putComma();
            putAllComma();
            takeZeros();

            $('.number').focusout(validateNum);

            var curTab = $('.mCurTab').val();
            if (curTab == 'person')
                $('a[href="#tabPerson"]').click();
            else if (curTab == 'check')
                $('a[href="#tabCheck"]').click();
            else if (curTab == 'paycheck')
                $('a[href="#tabPayCheck"]').click();
            else if (curTab == 'treepath')
                $('a[href="#tabTreePath"]').click();
            var msg = $('.mMsg').val();
            if (msg)
                Growl.Notice(msg, 4000);
        });
    </script>
</asp:Content>

