﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Transfer : System.Web.UI.Page
{
    System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
    DbProxy srcDB = null, destDB = null;
    StringBuilder mErrors = new StringBuilder();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (SiteSettings.User == null || !SiteSettings.User.HasAccess("admin_database"))
            Response.Redirect("~/default.aspx");

        msg.Visible = false;
        error.Visible = false;

        if (!IsPostBack)
        {
            Page.SetLang();
            int year = cal.GetYear(DateTime.Now);
            fFromDate.Text = string.Format("{0}/01/01", year);
            IDataReader financeYearStartDate = DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");//0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                fFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            fToDate.Text = Helper.FormatDate(DateTime.Now, null);

            BindFilterCombos();

            mDest.Text = SiteSettings.ConnectionString;
            string cntStr = DbProxy.Instance.GetConfig("VoucherDB", null, -1);
            if (!string.IsNullOrEmpty(cntStr))
                mDest.Text = cntStr;
        }
    }
    void BindFilterCombos()
    {
        using (DbProxy proxy = new DbProxy())
        {
            {
                IDataReader stores = proxy.ExecuteReader("SELECT ID, Title FROM Stores");
                fStores.DataSource = stores;
                fStores.DataTextField = "Title";
                fStores.DataValueField = "ID";
                fStores.DataBind();
                if (stores != null)
                    stores.Close();
            }
        }
    }
    protected void doShow_Click(object sender, EventArgs e)
    {
        DateTime start = Helper.ParseDate(fFromDate.Text, DateTime.Now.AddYears(-50));
        DateTime end = Helper.ParseDate(fToDate.Text, DateTime.Now.AddYears(50));
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);
        string query = null;
        int type = Helper.GetInt(fType.SelectedValue, 0);
        int storeID = Helper.GetInt(fStores.SelectedValue, 0);

        // SELECT FactorDate AS Date, NOSerial, Comments, BuyerName AS Person, TotalAmount FROM StoreDraftView
        // SELECT ReceiptDate AS Date, NOSerial, Comments, SellerName AS Person, TotalAmount FROM PurchaseView
        // SELECT RefuseDate AS Date, NOSerial, Comments, '' AS Person, TotalPrice AS TotalAmount FROM Refuse
        // SELECT ReturnDate AS Date, NOSerial, Comments, '' AS Person, TotalPrice AS TotalAmount FROM [Returns]
        if (type == 0)
            query = string.Format("SELECT ID, FactorDate AS Date, NOSerial, Comments, BuyerName AS Person, TotalPrice, TotalAmount FROM StoreDraftView WHERE WorkflowStatus = 1 AND {0} <= FactorDate AND FactorDate <= {1}"
                , Helper.QoutedDateTime(start), Helper.QoutedDateTime(end));
        else if (type == 1)
            query = string.Format("SELECT ID, ReceiptDate AS Date, NOSerial, Comments, SellerName AS Person, TotalPrice, TotalAmount FROM PurchaseView WHERE WorkflowStatus = 1 AND {0} <= ReceiptDate AND ReceiptDate <= {1}"
                , Helper.QoutedDateTime(start), Helper.QoutedDateTime(end));
        else if (type == 2)
            query = string.Format("SELECT ID, RefuseDate AS Date, NOSerial, Comments, '' AS Person, TotalPrice, 0 TotalAmount FROM Refuse WHERE WorkflowStatus = 1 AND {0} <= RefuseDate AND RefuseDate <= {1}"
                , Helper.QoutedDateTime(start), Helper.QoutedDateTime(end));
        else if (type == 3)
            query = string.Format("SELECT ID, ReturnDate AS Date, NOSerial, Comments, '' AS Person, TotalPrice, 0 TotalAmount FROM [Returns] WHERE WorkflowStatus = 1 AND {0} <= ReturnDate AND ReturnDate <= {1}"
                , Helper.QoutedDateTime(start), Helper.QoutedDateTime(end));
        if (storeID > 0)
            query += " AND StoreID = " + storeID;
        mGrid.DataSource = DbProxy.Instance.ExecuteReader(query);
        mGrid.DataBind();
    }
    protected void doExcel_Click(object sender, EventArgs e)
    {
        Exporter.ExportExcel(Response
            , new List<string>(new string[] { LangProvider.Instance["TransferFactors"] })
            , new List<GridView>(new GridView[] { mGrid }));

    }
    protected void doTransfer_Click(object sender, EventArgs e)
    {
        mErrors.Clear();
        if (mDest.Text.Trim().ToLower() == SiteSettings.ConnectionString.ToLower().Trim())
        {
            msg.Text = LangProvider.Instance["SameSrcDest"];
            msg.CssClass += " alert-danger ";
            msg.Visible = true;
            return;
        }
        List<int> ids = new List<int>();
        for (int i = 0; i < mGrid.Rows.Count; i++)
        {
            int id = Helper.GetInt(mGrid.Rows[i].Cells[2].Text, -1);
            if (!string.IsNullOrEmpty(Request["ch" + id]))
                ids.Add(id);
        }
        if (ids.Count == 0)
            return;

        int type = Helper.GetInt(fType.SelectedValue, 0);
        srcDB = new DbProxy();
        destDB = new DbProxy(mDest.Text, true);
        string done = "";
        try
        {
            done += TransferCommonTables();
            if (type == 0)
                done += TransferStoreDrafts(ids);
            else if (type == 1)
                done += TransferPurchases(ids);
            else if (type == 2)
                done += TransferRefuses(ids);
            else if (type == 3)
                done += TransferReturns(ids);
        }
        finally
        {
            srcDB.Dispose();
            destDB.Dispose();
        }
        msg.Text = done;
        msg.CssClass += " alert-success ";
        msg.Visible = true;
        if (mErrors.Length > 0)
        {
            error.Text = "Errors: <br />" + mErrors.ToString();
            error.CssClass += " alert-warning ";
            error.Visible = true;
        }
    }

    /// <summary>
    /// Transfer tables with one key column, for example Units with Code
    /// </summary>
    /// <param name="table"></param>
    /// <param name="qInfo"></param>
    /// <param name="tInfo"></param>
    /// <param name="keyField"></param>
    /// <returns></returns>
    int TransferTable(DataTable table, string insertQuery, TableInfo tInfo, string keyField)
    {
        int res = 0;
        if (table == null || table.Rows.Count == 0)
            return 0;
        for (int i = 0; i < table.Rows.Count; i++)
        {
            string key = Helper.GetString(table.Rows[i], keyField, null);
            if (string.IsNullOrEmpty(key))
                continue;
            string destKey = destDB.FillScalarString(string.Format("SELECT TOP 1 {0} FROM {1} WHERE {0} = {2}", keyField, tInfo.Name, Helper.QoutedString(key)));
            if (key == destKey)
                continue; // it is already in dest DB
            // transfer it
            string query = insertQuery.Replace("@Table", tInfo.Name);
            query = Helper.ParseQueryVars(query, tInfo, table.Rows[i]);
            destDB.LastException = null;
            if (destDB.RunQuery(query) <= 0)
                if (destDB.LastException != null)
                    mErrors.Append("[" + tInfo.Name + "] -- " + destDB.LastException.Message + "<br />");
            destKey = destDB.FillScalarString(string.Format("SELECT TOP 1 {0} FROM {1} WHERE {0} = {2}", keyField, tInfo.Name, Helper.QoutedString(key)));
            if (destKey == key || Helper.GetInt(destKey, Null.NullInteger) > 0)
                res++;
        }
        return res;
    }
    /// <summary>
    /// Transfer tables with two key columns, for example ServicePrice with ServiceID and Model
    /// </summary>
    /// <param name="table"></param>
    /// <param name="qInfo"></param>
    /// <param name="tInfo"></param>
    /// <param name="keyField1"></param>
    /// <param name="keyField2"></param>
    /// <returns></returns>
    int TransferTable(DataTable table, QueryInfo qInfo, TableInfo tInfo, string keyField1, string keyField2)
    {
        int res = 0;
        if (table == null || table.Rows.Count == 0)
            return 0;
        for (int i = 0; i < table.Rows.Count; i++)
        {
            string key1 = Helper.GetString(table.Rows[i], keyField1, null);
            string key2 = Helper.GetString(table.Rows[i], keyField2, null);
            if (string.IsNullOrEmpty(key1) || string.IsNullOrEmpty(key2))
                continue;
            DataRow destRow = destDB.FillRow(string.Format("SELECT TOP 1 {0}, {1} FROM {2} WHERE {0} = {3} AND {1} = {4}", keyField1, keyField2, tInfo.Name, Helper.QoutedString(key1), Helper.QoutedString(key2)));
            if (destRow != null)
                continue; // it is already in dest DB
            // transfer it
            string query = qInfo.Query;
            query = Helper.ParseQueryVars(query, tInfo, table.Rows[i]);
            destDB.LastException = null;
            if (destDB.RunQuery(query) <= 0)
                if (destDB.LastException != null)
                    mErrors.Append("[" + tInfo.Name + "] -- " + destDB.LastException.Message + "<br />");
            destRow = destDB.FillRow(string.Format("SELECT TOP 1 {0}, {1} FROM {2} WHERE {0} = {3} AND {1} = {4}", keyField1, keyField2, tInfo.Name, Helper.QoutedString(key1), Helper.QoutedString(key2)));
            if (destRow != null)
                res++;
        }
        return res;
    }
    string TransferCommonTables()
    {
        // Units
        // SaleType
        string res = "";
        int num = TransferTable(srcDB.FillData("SELECT * FROM Units", "Units"), QueryController.Instance["enuminsert"].Query, TableController.Instance["Units"], "Code");
        if (num > 0)
            res += string.Format(LangProvider.Instance["TransferTableMsg"], num, "Units", LangProvider.Instance["Units"]) + "<br />";

        num = TransferTable(srcDB.FillData("SELECT * FROM SaleType", "SaleType"), QueryController.Instance["saletypeinsert"].Query, TableController.Instance["SaleType"], "Title");
        if (num > 0)
            res += string.Format(LangProvider.Instance["TransferTableMsg"], num, "SaleType", LangProvider.Instance["SaleType"]) + "<br />";

        num = TransferTable(srcDB.FillData("SELECT * FROM Stores", "Stores"), QueryController.Instance["storesinsert"].Query, TableController.Instance["Stores"], "Code");
        if (num > 0)
            res += string.Format(LangProvider.Instance["TransferTableMsg"], num, "Stores", LangProvider.Instance["Stores"]) + "<br />";

        // ParentID is not important and is hard to transfer
        num = TransferTable(srcDB.FillData("SELECT * FROM ServiceType", "ServiceType")
            , "INSERT INTO ServiceType (Title, Code, Type, Comments, Enabled, MemberNumOfCharCode) VALUES (@Title, dbo.[NeuServiceTypeNo](@Code, NULL), @Type, @Comments, @Enabled, @MemberNumOfCharCode);SELECT SCOPE_IDENTITY();", TableController.Instance["ServiceType"], "Code");
        if (num > 0)
            res += string.Format(LangProvider.Instance["TransferTableMsg"], num, "ServiceType", LangProvider.Instance["ServiceType"]) + "<br />";
        
        num = TransferTable(srcDB.FillData("SELECT * FROM Supplier", "Supplier"), QueryController.Instance["supplierinsert"].Query, TableController.Instance["Supplier"], "CompanyName");
        if (num > 0)
            res += string.Format(LangProvider.Instance["TransferTableMsg"], num, "Supplier", LangProvider.Instance["Supplier"]) + "<br />";
        
        num = TransferTable(srcDB.FillData("SELECT * FROM Program", "Program"), QueryController.Instance["programinsert"].Query, TableController.Instance["Program"], "Code");
        if (num > 0)
            res += string.Format(LangProvider.Instance["TransferTableMsg"], num, "Program", LangProvider.Instance["Program"]) + "<br />";
        
        num = TransferTable(srcDB.FillData("SELECT * FROM FinanceYear", "FinanceYear"), QueryController.Instance["financeyearinsert"].Query, TableController.Instance["FinanceYear"], "Code");
        if (num > 0)
            res += string.Format(LangProvider.Instance["TransferTableMsg"], num, "FinanceYear", LangProvider.Instance["FinanceYear"]) + "<br />";
        
        num = TransferTable(srcDB.FillData("SELECT * FROM PayMethod", "PayMethod"), QueryController.Instance["enuminsert"].Query, TableController.Instance["PayMethod"], "Code");
        if (num > 0)
            res += string.Format(LangProvider.Instance["TransferTableMsg"], num, "PayMethod", LangProvider.Instance["PayMethod"]) + "<br />";
        
        num = TransferTable(srcDB.FillData("SELECT * FROM Service", "Service"), QueryController.Instance["serviceinsert"].Query, TableController.Instance["Service"], "Code");
        if (num > 0)
            res += string.Format(LangProvider.Instance["TransferTableMsg"], num, "Service", LangProvider.Instance["Service"]) + "<br />";

        return res;
    }

    string TransferStoreDrafts(List<int> ids)
    {
        // persons
        // storeDrafts
        // storeDraftItems
        //     Serviceprice + StoreServices + StoreServiceDef

        string res = "";
        string idsStr = string.Join(",", ids);

        // persons
        DataTable persons = srcDB.FillData(string.Format(@"SELECT * FROM Personnel WHERE ID IN (
	SELECT PersonnelID FROM StoreDraft WHERE ID IN ({0})
	UNION
	SELECT AccepterID FROM StoreDraft WHERE ID IN ({0})
	UNION
	SELECT BuyerID FROM StoreDraft WHERE ID IN ({0})
	UNION
	SELECT StoreKeeperID FROM StoreDraft WHERE ID IN ({0})
	UNION
	SELECT VisitorID FROM StoreDraft WHERE ID IN ({0})
)", idsStr), "PC");

        int num = TransferTable(persons, QueryController.Instance["personinsert"].Query, TableController.Instance["Personnel"], "Code");
        if (num > 0)
            res += string.Format(LangProvider.Instance["TransferTableMsg"], num, "Personnel", LangProvider.Instance["Personnel"]) + "<br />";
        if (persons != null)
            persons.Dispose();


        QueryInfo qInfo = QueryController.Instance["storedraftinsert"];
        TableInfo tInfo = TableController.Instance["StoreDraft"];
        QueryInfo iqInfo = QueryController.Instance["storedraftiteminsert"];
        TableInfo itInfo = TableController.Instance["StoreDraftItems"];
        DataTable factors = srcDB.FillData(string.Format("SELECT * FROM StoreDraft WHERE ID IN ({0})", idsStr), "StoreDrafts");
        int numSD = 0;
        for (int s = 0; s < factors.Rows.Count; s++)
        {
            DataRow sdRow = factors.Rows[s];
            int srcStoreID = Helper.GetInt(sdRow, "StoreID", Null.NullInteger);
            // assumed to be the same: WardID, UserID, OweTreePath, DueTreePath, CashTreePath, CheckTreePath, OwedTreePath, ShipmentTreePath, BankTreePath

            // map IDs
            sdRow["PersonnelID"] = GetDestID("Personnel", Helper.GetInt(sdRow, "PersonnelID", Null.NullInteger), "Code");
            sdRow["AccepterID"] = GetDestID("Personnel", Helper.GetInt(sdRow, "AccepterID", Null.NullInteger), "Code");
            sdRow["BuyerID"] = GetDestID("Personnel", Helper.GetInt(sdRow, "BuyerID", Null.NullInteger), "Code");
            sdRow["StoreKeeperID"] = GetDestID("Personnel", Helper.GetInt(sdRow, "StoreKeeperID", Null.NullInteger), "Code");
            sdRow["VisitorID"] = GetDestID("Personnel", Helper.GetInt(sdRow, "VisitorID", Null.NullInteger), "Code");
            sdRow["ProgramID"] = GetDestID("Program", Helper.GetInt(sdRow, "ProgramID", Null.NullInteger), "Code");
            sdRow["StoreID"] = GetDestID("Stores", Helper.GetInt(sdRow, "StoreID", Null.NullInteger), "Code");
            sdRow["FinanceYearID"] = GetDestID("FinanceYear", Helper.GetInt(sdRow, "FinanceYearID", Null.NullInteger), "Code");

            // not transfered: CheckID, PurchaseID
            sdRow["CheckID"] = DBNull.Value;
            sdRow["PurchaseID"] = DBNull.Value;
            sdRow["DesiredEntryTaskID"] = DBNull.Value;
            
            string query = qInfo.Query;
            query = query.Replace("__PrOgramiD__", Helper.GetString(sdRow["ProgramID"], "NULL"));
            query = Helper.ParseQueryVars(query, tInfo, sdRow);
            destDB.LastException = null;
            int destID = Helper.GetInt(destDB.FillScalarString(query), -1);
            if (destID > 0)
                numSD++;
            else
            {
                if (destDB.LastException != null)
                    mErrors.Append("[" + tInfo.Name + "] -- " + destDB.LastException.Message + "<br />");
                continue;
            }

            // transfer items
            DataTable sdItems = srcDB.FillData("SELECT * FROM StoreDraftItems WHERE StoreDraftID =" + sdRow["ID"], "StoreDraftItems");
            for (int i = 0; i < sdItems.Rows.Count; i++)
            {
                DataRow itemRow = sdItems.Rows[i];
                itemRow["UnitID"] = GetDestID("Units", Helper.GetInt(itemRow, "UnitID", Null.NullInteger), "Code");
                itemRow["SaleTypeID"] = GetDestID("SaleType", Helper.GetInt(itemRow, "SaleTypeID", Null.NullInteger), "Title");
                itemRow["StoreServiceID"] = GetDestStoreServiceID(itemRow["StoreServiceID"]);

                query = iqInfo.Query;
                query = query.Replace("@MasterID", destID.ToString());
                query = Helper.ParseQueryVars(query, itInfo, itemRow);

                destDB.LastException = null;
                if (destDB.RunQuery(query) <= 0)
                    if (destDB.LastException != null)
                        mErrors.Append("[" + itInfo.Name + "] -- " + destDB.LastException.Message + "<br />");
            }
        }

        res += string.Format(LangProvider.Instance["TransferTableMsg"], numSD, "StoreDrafts", LangProvider.Instance["StoreDrafts"]) + "<br />";
        return res;
    }
    string TransferPurchases(List<int> ids)
    {
        // persons
        // Purchase
        // PurchaseItems
        //     Serviceprice + StoreServices + StoreServiceDef

        string res = "";
        string idsStr = string.Join(",", ids);

        // persons
        DataTable persons = srcDB.FillData(string.Format(@"SELECT * FROM Personnel WHERE ID IN (
	SELECT StoreKeeperID FROM Purchase WHERE ID IN ({0})
	UNION
	SELECT AccountantID FROM Purchase WHERE ID IN ({0})
	UNION
	SELECT SellerID FROM Purchase WHERE ID IN ({0})
)", idsStr), "PC");
        int num = TransferTable(persons, QueryController.Instance["personinsert"].Query, TableController.Instance["Personnel"], "Code");
        if (num > 0)
            res += string.Format(LangProvider.Instance["TransferTableMsg"], num, "Personnel", LangProvider.Instance["Personnel"]) + "<br />";
        if (persons != null)
            persons.Dispose();


        QueryInfo qInfo = QueryController.Instance["purchaseinsert"];
        TableInfo tInfo = TableController.Instance["Purchase"];
        // QueryInfo iqInfo = QueryController.Instance["purchaseiteminsert"];
        string iqInfo = @"
         INSERT INTO PurchaseItems (PurchaseID, Amount, Price, Discount, Tax, SingleVAT, StoreServiceID, ServiceVariateID, ExpireDate, ProductionDate, UnitID, UseTime, BestUseTime, Realpercent, SalePrice, UnitCurrencyConverter, SaleWorth, Series, EquipmentLabelID, ProjectLabelID, PersonLabelID, Comment, ShipmentPriceAdded) 
          VALUES (@MasterID, @Amount, @Price + ISNULL(@ShipmentPriceAdded, 0), @Discount, @Tax, @SingleVAT, @StoreServiceID, @ServiceVariateID, @ExpireDate, @ProductionDate, @UnitID, @UseTime, @BestUseTime, @Realpercent, @SalePrice, @UnitCurrencyConverter, @SaleWorth + ISNULL(@ShipmentPriceAdded, 0), @Series, @EquipmentLabelID, @ProjectLabelID, @PersonLabelID, @Comment, @ShipmentPriceAdded);
         ";
        TableInfo itInfo = TableController.Instance["PurchaseItems"];
        DataTable factors = srcDB.FillData(string.Format("SELECT * FROM Purchase WHERE ID IN ({0})", idsStr), "Purchases");
        int numSD = 0;
        for (int s = 0; s < factors.Rows.Count; s++)
        {
            DataRow sdRow = factors.Rows[s];
            int srcStoreID = Helper.GetInt(sdRow, "StoreID", Null.NullInteger);
            // assumed to be the same: WardID, UserID, DesiredEntryTaskID, OweTreePath, DueTreePath, CashTreePath, CheckTreePath, OwedTreePath, ShipmentTreePath, BankTreePath

            // map IDs
            sdRow["StoreKeeperID"] = GetDestID("Personnel", Helper.GetInt(sdRow, "StoreKeeperID", Null.NullInteger), "Code");
            sdRow["AccountantID"] = GetDestID("Personnel", Helper.GetInt(sdRow, "AccountantID", Null.NullInteger), "Code");
            sdRow["SellerID"] = GetDestID("Personnel", Helper.GetInt(sdRow, "SellerID", Null.NullInteger), "Code");
            sdRow["ProgramID"] = GetDestID("Program", Helper.GetInt(sdRow, "ProgramID", Null.NullInteger), "Code");
            sdRow["StoreID"] = GetDestID("Stores", Helper.GetInt(sdRow, "StoreID", Null.NullInteger), "Code");
            sdRow["FinanceYearID"] = GetDestID("FinanceYear", Helper.GetInt(sdRow, "FinanceYearID", Null.NullInteger), "Code");
            sdRow["PayMethodID"] = GetDestID("PayMethod", Helper.GetInt(sdRow, "PayMethodID", Null.NullInteger), "Code");

            // not transfered: CheckID, PurchaseID
            sdRow["PayCheckID"] = DBNull.Value;
            sdRow["PurchaseRequestID"] = DBNull.Value;
            sdRow["BuildOrderID"] = DBNull.Value;
            sdRow["DesiredEntryTaskID"] = DBNull.Value;

            string query = qInfo.Query;
            query = query.Replace("__PrOgramiD__", Helper.GetString(sdRow["ProgramID"], "NULL"));
            query = Helper.ParseQueryVars(query, tInfo, sdRow);
            destDB.LastException = null;
            int destID = Helper.GetInt(destDB.FillScalarString(query), -1);
            if (destID > 0)
                numSD++;
            else
            {
                if (destDB.LastException != null)
                    mErrors.Append("[" + tInfo.Name + "] -- " + destDB.LastException.Message + "<br />");
                continue;
            }

            // transfer items
            DataTable sdItems = srcDB.FillData("SELECT * FROM PurchaseItems WHERE PurchaseID =" + sdRow["ID"], "PurchaseItems");
            for (int i = 0; i < sdItems.Rows.Count; i++)
            {
                DataRow itemRow = sdItems.Rows[i];
                itemRow["UnitID"] = GetDestID("Units", Helper.GetInt(itemRow, "UnitID", Null.NullInteger), "Code");
                itemRow["PersonLabelID"] = GetDestID("Personnel", Helper.GetInt(itemRow, "PersonLabelID", Null.NullInteger), "Title");
                itemRow["StoreServiceID"] = GetDestStoreServiceID(itemRow["StoreServiceID"]);

                query = iqInfo;
                query = query.Replace("@MasterID", destID.ToString());
                query = Helper.ParseQueryVars(query, itInfo, itemRow);

                destDB.LastException = null;
                if (destDB.RunQuery(query) <= 0)
                    if (destDB.LastException != null)
                        mErrors.Append("[" + itInfo.Name + "] -- " + destDB.LastException.Message + "<br />");
            }
        }

        res += string.Format(LangProvider.Instance["TransferTableMsg"], numSD, "Purchases", LangProvider.Instance["Purchases"]) + "<br />";
        return res;
    }
    private string TransferRefuses(List<int> ids)
    {
        // persons
        // Refuse
        // RefuseItem
        //     Serviceprice + StoreServices + StoreServiceDef

        string res = "";
        string idsStr = string.Join(",", ids);

        // persons
        DataTable persons = srcDB.FillData(string.Format(@"SELECT * FROM Personnel WHERE ID IN (
	SELECT StoreKeeperID FROM Refuse WHERE ID IN ({0})
	UNION
	SELECT AccountantID FROM Refuse WHERE ID IN ({0})
	UNION
	SELECT SellerID FROM Refuse WHERE ID IN ({0})
)", idsStr), "PC");
        int num = TransferTable(persons, QueryController.Instance["personinsert"].Query, TableController.Instance["Personnel"], "Code");
        if (num > 0)
            res += string.Format(LangProvider.Instance["TransferTableMsg"], num, "Personnel", LangProvider.Instance["Personnel"]) + "<br />";
        if (persons != null)
            persons.Dispose();


        QueryInfo qInfo = QueryController.Instance["refuseinsert"];
        TableInfo tInfo = TableController.Instance["Refuse"];
        QueryInfo iqInfo = QueryController.Instance["refuseiteminsert"];
        TableInfo itInfo = TableController.Instance["RefuseItem"];
        DataTable factors = srcDB.FillData(string.Format("SELECT * FROM Refuse WHERE ID IN ({0})", idsStr), "Refuses");
        int numSD = 0;
        for (int s = 0; s < factors.Rows.Count; s++)
        {
            DataRow sdRow = factors.Rows[s];
            int srcStoreID = Helper.GetInt(sdRow, "StoreID", Null.NullInteger);
            // assumed to be the same: WardID, UserID, DesiredEntryTaskID, OweTreePath, DueTreePath, CashTreePath, CheckTreePath, OwedTreePath, ShipmentTreePath, BankTreePath

            // map IDs
            sdRow["StoreKeeperID"] = GetDestID("Personnel", Helper.GetInt(sdRow, "StoreKeeperID", Null.NullInteger), "Code");
            sdRow["AccountantID"] = GetDestID("Personnel", Helper.GetInt(sdRow, "AccountantID", Null.NullInteger), "Code");
            sdRow["SellerID"] = GetDestID("Personnel", Helper.GetInt(sdRow, "SellerID", Null.NullInteger), "Code");
            sdRow["ProgramID"] = GetDestID("Program", Helper.GetInt(sdRow, "ProgramID", Null.NullInteger), "Code");
            sdRow["StoreID"] = GetDestID("Stores", Helper.GetInt(sdRow, "StoreID", Null.NullInteger), "Code");
            sdRow["FinanceYearID"] = GetDestID("FinanceYear", Helper.GetInt(sdRow, "FinanceYearID", Null.NullInteger), "Code");

            // not transfered: CheckID, PurchaseID
            sdRow["PurchaseID"] = DBNull.Value;
            sdRow["VoucherID"] = DBNull.Value;
            sdRow["DesiredEntryTaskID"] = DBNull.Value;

            string query = qInfo.Query;
            query = query.Replace("__PrOgramiD__", Helper.GetString(sdRow["ProgramID"], "NULL"));
            query = Helper.ParseQueryVars(query, tInfo, sdRow);
            destDB.LastException = null;
            int destID = Helper.GetInt(destDB.FillScalarString(query), -1);
            if (destID > 0)
                numSD++;
            else
            {
                if (destDB.LastException != null)
                    mErrors.Append("[" + tInfo.Name + "] -- " + destDB.LastException.Message + "<br />");
                continue;
            }

            // transfer items
            DataTable sdItems = srcDB.FillData("SELECT * FROM RefuseItem WHERE RefuseID =" + sdRow["ID"], "RefuseItem");
            for (int i = 0; i < sdItems.Rows.Count; i++)
            {
                DataRow itemRow = sdItems.Rows[i];
                itemRow["UnitID"] = GetDestID("Units", Helper.GetInt(itemRow, "UnitID", Null.NullInteger), "Code");
                itemRow["StoreServiceID"] = GetDestStoreServiceID(itemRow["StoreServiceID"]);

                query = iqInfo.Query;
                query = query.Replace("@MasterID", destID.ToString());
                query = Helper.ParseQueryVars(query, itInfo, itemRow);

                destDB.LastException = null;
                if (destDB.RunQuery(query) <= 0)
                    if (destDB.LastException != null)
                        mErrors.Append("[" + itInfo.Name + "] -- " + destDB.LastException.Message + "<br />");
            }
        }

        res += string.Format(LangProvider.Instance["TransferTableMsg"], numSD, "Refuse", LangProvider.Instance["Refuse"]) + "<br />";
        return res;
    }

    private string TransferReturns(List<int> ids)
    {
        // persons
        // Returns
        // RefuseItem
        //     Serviceprice + StoreServices + StoreServiceDef

        string res = "";
        string idsStr = string.Join(",", ids);

        // persons
        DataTable persons = srcDB.FillData(string.Format(@"SELECT * FROM Personnel WHERE ID IN (
	SELECT StoreKeeperID FROM Refuse WHERE ID IN ({0})
	UNION
	SELECT AccountantID FROM Refuse WHERE ID IN ({0})
	UNION
	SELECT BuyerID FROM Refuse WHERE ID IN ({0})
	UNION
	SELECT VisitorID FROM Refuse WHERE ID IN ({0})
)", idsStr), "PC");
        int num = TransferTable(persons, QueryController.Instance["personinsert"].Query, TableController.Instance["Personnel"], "Code");
        if (num > 0)
            res += string.Format(LangProvider.Instance["TransferTableMsg"], num, "Personnel", LangProvider.Instance["Personnel"]) + "<br />";
        if (persons != null)
            persons.Dispose();


        QueryInfo qInfo = QueryController.Instance["returninsert"];
        TableInfo tInfo = TableController.Instance["Returns"];
        QueryInfo iqInfo = QueryController.Instance["returniteminsert"];
        TableInfo itInfo = TableController.Instance["ReturnItem"];
        DataTable factors = srcDB.FillData(string.Format("SELECT * FROM [Returns] WHERE ID IN ({0})", idsStr), "Returns");
        int numSD = 0;
        for (int s = 0; s < factors.Rows.Count; s++)
        {
            DataRow sdRow = factors.Rows[s];
            int srcStoreID = Helper.GetInt(sdRow, "StoreID", Null.NullInteger);
            // assumed to be the same: WardID, UserID, DesiredEntryTaskID, OweTreePath, DueTreePath, CashTreePath, CheckTreePath, OwedTreePath, ShipmentTreePath, BankTreePath

            // map IDs
            sdRow["StoreKeeperID"] = GetDestID("Personnel", Helper.GetInt(sdRow, "StoreKeeperID", Null.NullInteger), "Code");
            sdRow["AccountantID"] = GetDestID("Personnel", Helper.GetInt(sdRow, "AccountantID", Null.NullInteger), "Code");
            sdRow["BuyerID"] = GetDestID("Personnel", Helper.GetInt(sdRow, "BuyerID", Null.NullInteger), "Code");
            sdRow["VisitorID"] = GetDestID("Personnel", Helper.GetInt(sdRow, "VisitorID", Null.NullInteger), "Code");
            sdRow["ProgramID"] = GetDestID("Program", Helper.GetInt(sdRow, "ProgramID", Null.NullInteger), "Code");
            sdRow["StoreID"] = GetDestID("Stores", Helper.GetInt(sdRow, "StoreID", Null.NullInteger), "Code");
            sdRow["FinanceYearID"] = GetDestID("FinanceYear", Helper.GetInt(sdRow, "FinanceYearID", Null.NullInteger), "Code");

            // not transfered: CheckID, PurchaseID
            sdRow["StoreDraftID"] = DBNull.Value;
            sdRow["VoucherID"] = DBNull.Value;
            sdRow["DesiredEntryTaskID"] = DBNull.Value;

            string query = qInfo.Query;
            query = query.Replace("__PrOgramiD__", Helper.GetString(sdRow["ProgramID"], "NULL"));
            query = Helper.ParseQueryVars(query, tInfo, sdRow);
            destDB.LastException = null;
            int destID = Helper.GetInt(destDB.FillScalarString(query), -1);
            if (destID > 0)
                numSD++;
            else
            {
                if (destDB.LastException != null)
                    mErrors.Append("[" + tInfo.Name + "] -- " + destDB.LastException.Message + "<br />");
                continue;
            }

            // transfer items
            DataTable sdItems = srcDB.FillData("SELECT * FROM ReturnItem WHERE ReturnID =" + sdRow["ID"], "ReturnItem");
            for (int i = 0; i < sdItems.Rows.Count; i++)
            {
                DataRow itemRow = sdItems.Rows[i];
                itemRow["UnitID"] = GetDestID("Units", Helper.GetInt(itemRow, "UnitID", Null.NullInteger), "Code");
                itemRow["StoreServiceID"] = GetDestStoreServiceID(itemRow["StoreServiceID"]);

                query = iqInfo.Query;
                query = query.Replace("@MasterID", destID.ToString());
                query = Helper.ParseQueryVars(query, itInfo, itemRow);

                destDB.LastException = null;
                if (destDB.RunQuery(query) <= 0)
                    if (destDB.LastException != null)
                        mErrors.Append("[" + itInfo.Name + "] -- " + destDB.LastException.Message + "<br />");
            }
        }
        res += string.Format(LangProvider.Instance["TransferTableMsg"], numSD, "Return", LangProvider.Instance["Return"]) + "<br />";
        return res;
    }
    object GetDestStoreServiceID(object srcStoreServiceID)
    {
        DataRow row = srcDB.FillRow(@"SELECT Supplier.CompanyName, Service.Code ServiceCode, Stores.Code StoresCode FROM ServicePrice 
	INNER JOIN Supplier ON ServicePrice.Model = Supplier.ID
	INNER JOIN Service ON ServicePrice.ServiceID = Service.ID 
	INNER JOIN StoreServiceDef ON StoreServiceDef.ServicePriceID = ServicePrice.ID
	INNER JOIN StoreServices ON StoreServices.StoreServiceDefID= StoreServiceDef.ID
	INNER JOIN Stores ON StoreServiceDef.StoreID = Stores.ID
	WHERE StoreServices.ID = " + srcStoreServiceID);
        string companyName = Helper.GetString(row, "CompanyName", null);
        string serviceCode = Helper.GetString(row, "ServiceCode", null);
        string storesCode = Helper.GetString(row, "StoresCode", null);

        int destStoreServiceID = Helper.GetInt(destDB.FillScalar(string.Format(@"SELECT StoreServices.ID FROM ServicePrice 
	INNER JOIN Supplier ON ServicePrice.Model = Supplier.ID
	INNER JOIN Service ON ServicePrice.ServiceID = Service.ID 
	INNER JOIN StoreServiceDef ON StoreServiceDef.ServicePriceID = ServicePrice.ID
	INNER JOIN StoreServices ON StoreServices.StoreServiceDefID= StoreServiceDef.ID
	INNER JOIN Stores ON StoreServiceDef.StoreID = Stores.ID
	WHERE Supplier.CompanyName = {0} AND Service.Code = {1} AND Stores.Code = {2}"
                 , Helper.QoutedString(companyName), Helper.QoutedString(serviceCode), Helper.QoutedString(storesCode))), -1);
        // ignored: ServiceVariateID, LabelID, StoreLayoutID
        if (destStoreServiceID < 0)
        {
            object supplierID = GetDestID("Supplier", companyName, "CompanyName");
            object serviceID = GetDestID("Service", serviceCode, "Code");
            object storeID = GetDestID("Stores", storesCode, "Code");

            // check serviceprice
            DataRow sPriceRow = destDB.FillRow(string.Format("SELECT * FROM ServicePrice WHERE Model={0} AND ServiceID={1}", supplierID, serviceID));
            if (sPriceRow == null)
            {
                DataRow srcRow = srcDB.FillRow(@"SELECT ServicePrice.* FROM ServicePrice 
                        INNER JOIN StoreServiceDef ON StoreServiceDef.ServicePriceID = ServicePrice.ID
                        INNER JOIN StoreServices ON StoreServices.StoreServiceDefID= StoreServiceDef.ID
                    WHERE StoreServices.ID = " + srcStoreServiceID);
                string qry = @"INSERT INTO ServicePrice (ServiceID, Model, Description, StartDatesp, FreeBase, ProfitPercent, SalesManCommission, TheLPBP, TheLPSP, Length, Width, Height, Volume, Weight, DefaultDiscount, AppAvailable) 
                    VALUES (@MasterID, @ModelID, @Description, @StartDatesp, @FreeBase, @ProfitPercent, @SalesManCommission, @TheLPBP, @TheLPSP, @Length, @Width, @Height, @Volume, @Weight, @DefaultDiscount, @AppAvailable)";
                qry = qry.Replace("@MasterID", serviceID.ToString());
                qry = qry.Replace("@ModelID", supplierID.ToString());
                qry = Helper.ParseQueryVars(qry, TableController.Instance["ServicePrice"], srcRow);
                destDB.LastException = null;
                if (destDB.RunQuery(qry) <= 0)
                    if (destDB.LastException != null)
                        mErrors.Append("[ServicePrice] -- " + destDB.LastException.Message + "<br />");
                sPriceRow = destDB.FillRow(string.Format("SELECT * FROM ServicePrice WHERE Model={0} AND ServiceID={1}", supplierID, serviceID));
            }

            // check StoreServiceDef
            int servicePriceID = Helper.GetInt(sPriceRow, "ID", -1);
            DataRow sDefRow = destDB.FillRow(string.Format("SELECT * FROM StoreServiceDef WHERE ServicePriceID={0} AND ServiceID={1}", servicePriceID, storeID));
            if (sDefRow == null)
            {
                DataRow srcRow = srcDB.FillRow(@"SELECT StoreServiceDef.* FROM StoreServiceDef
                        INNER JOIN StoreServices ON StoreServices.StoreServiceDefID= StoreServiceDef.ID
                    WHERE StoreServices.ID = " + srcStoreServiceID);
                string qry = @"INSERT INTO StoreServiceDef (StoreID, ServicePriceID, StoreLayoutID, MaxStock, MinStock, OrderPoint, AveragePrice, Income, Outcome) 
                    VALUES (@StoreID, @ServicePriceID, @StoreLayoutID, @MaxStock, @MinStock, @OrderPoint, @AveragePrice, @Income, @Outcome)";
                qry = qry.Replace("@StoreID", storeID.ToString());
                qry = qry.Replace("@ServicePriceID", servicePriceID.ToString());
                qry = Helper.ParseQueryVars(qry, TableController.Instance["StoreServiceDef"], srcRow);
                destDB.LastException = null;
                if (destDB.RunQuery(qry) <= 0)
                    if (destDB.LastException != null)
                        mErrors.Append("[StoreServiceDef] -- " + destDB.LastException.Message + "<br />");
                sDefRow = destDB.FillRow(string.Format("SELECT * FROM StoreServiceDef WHERE ServicePriceID={0} AND StoreID={1}", servicePriceID, storeID));
            }

            // check StoreServices (ignore ExpireDate)
            int storeServiceDefID = Helper.GetInt(sDefRow, "ID", -1);
            DataRow sServiceRow = destDB.FillRow(string.Format("SELECT * FROM StoreServices WHERE StoreServiceDefID={0}", storeServiceDefID));
            if (sServiceRow == null)
            {
                string qry = @"INSERT INTO StoreServices (StoreServiceDefID, ExpireDate, Income, Outcome) 
                    VALUES (@StoreServiceDefID, '2100-01-01', 0, 0)";
                qry = qry.Replace("@StoreServiceDefID", storeServiceDefID.ToString());
                destDB.LastException = null;
                if (destDB.RunQuery(qry) <= 0)
                    if (destDB.LastException != null)
                        mErrors.Append("[StoreServices] -- " + destDB.LastException.Message + "<br />");
                sServiceRow = destDB.FillRow(string.Format("SELECT * FROM StoreServices WHERE StoreServiceDefID={0}", storeServiceDefID));
            }
            destStoreServiceID = Helper.GetInt(sServiceRow, "ID", -1);
        }

        return destStoreServiceID;
    }
    object GetDestID(string tableName, int srcID, string keyField)
    {
        if (srcID <= 0)
            return DBNull.Value;
        string srcKey = srcDB.FillScalarString(string.Format("SELECT {0} FROM {1} WHERE ID = {2}", keyField, tableName, srcID));
        object destID = destDB.FillScalarString(string.Format("SELECT ID FROM {0} WHERE {1} = {2}", tableName, keyField, Helper.QoutedString(srcKey)));
        string res = Helper.GetString(destID, null);
        if (string.IsNullOrEmpty(res))
            return DBNull.Value;
        return res;
    }
    object GetDestID(string tableName, string srcKey, string keyField)
    {
        if (string.IsNullOrEmpty(srcKey))
            return DBNull.Value;
        object destID = destDB.FillScalarString(string.Format("SELECT ID FROM {0} WHERE {1} = {2}", tableName, keyField, Helper.QoutedString(srcKey)));
        string res = Helper.GetString(destID, null);
        if (string.IsNullOrEmpty(res))
            return DBNull.Value;
        return res;
    }

    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.Common.DbDataRecord record = e.Row.DataItem as System.Data.Common.DbDataRecord;
            // DataRowView record = e.Row.DataItem as System.Data.Common.DataRecordInternal;
            e.Row.Cells[2].Text = Helper.GetInt(record["ID"], 0).ToString();
            e.Row.Cells[4].Text = Helper.FormatDateFull(Helper.GetDateTime(e.Row.Cells[4].Text, Helper.NullDate));
            e.Row.Cells[7].Text = Helper.GetDecimal(e.Row.Cells[7].Text, 0).ToString("N2", Helper.NumberCulture).Replace(".00", "");

        }
    }
    protected void mGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        //if (e.SortExpression == SortCol)
        //{
        //    SortOrder = SortOrder == "asc" ? "desc" : "asc";
        //}
        //else
        //{
        //    SortCol = e.SortExpression;
        //    SortOrder = "asc";
        //}
        //BindGrid();
    }
}