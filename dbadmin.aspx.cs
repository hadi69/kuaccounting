﻿using System.Diagnostics;
using System.Linq;
using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LibGit2Sharp;

public partial class dbadmin : System.Web.UI.Page
{
    private string userName;
    private string passWord;
    private string repoUrl;
    string mWotkingDbBackup = null;

    DbProxy mProxy = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        mProxy = new DbProxy();
        mProxy.LastException = null;
        msg.Visible = false;
        lblUploadMessage.Visible = false;
        if (SiteSettings.User == null || !SiteSettings.User.HasAccess("admin_database"))
            Response.Redirect("~/default.aspx");

        if (!IsPostBack)
        {
            Page.SetLang();
            BindData();
        }
    }
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        try
        {
            if (mProxy != null)
                mProxy.Dispose();
        }
        catch { }
    }

    void BindData()
    {
        mWotkingDbBackup = mProxy.GetConfig("WorkingDbBackup", Null.NullInteger);

        //string[] files = System.IO.Directory.GetFiles(Server.MapPath("~/files/db"), "DB_*.bak");
        string dir = Server.MapPath("~/files/db");
        if (System.IO.Directory.Exists(dir))
        {
            string[] files = System.IO.Directory.GetFiles(dir, "*.bak");
            Array.Reverse(files);
            mBackups.DataSource = files;
            mBackups.DataBind();
            if (mBackups.Rows.Count == 0)
            {
                lblNoBackups.Visible = true;
            }
        }
    }
    protected void doUpdateDB_Click(object sender, EventArgs e)
    {
        DateTime startTime = DateTime.Now;
        QueryController.Instance.Reload();
        TableController.Instance.Reload();
        Dictionary<string, TableInfo> dict = TableController.Instance.Tables;
        if (dict == null || dict.Count == 0)
        {
            msg.Text = "no table in tables.xml";
            msg.CssClass += " alert-danger ";
            msg.Visible = true;
            return;
        }
        List<TableInfo> tables = new List<TableInfo>(dict.Values);

        // Update main DB
        using (DbValidator dbValidator = new DbValidator(null))
        {
            dbValidator.Messages.Clear();
            Logger.Pause = true;
            ExceptionLog.Pause = true;
            if (dbValidator.SynchronizeTableDesign(tables, "*"))
            {
                msg.Text = "Ewa debe <br />";
                msg.CssClass += " alert-success ";
                msg.Visible = true;
            }
            else
            {
                msg.Text = "Bedaxewe <br />";
                msg.CssClass += " alert-danger ";
                msg.Visible = true;
            }
            Logger.Pause = false;
            ExceptionLog.Pause = false;
            for (int i = 0; i < dbValidator.Messages.Count; i++)
                msg.Text += dbValidator.Messages[i] + "<br />";
            msg.Text = "It took " + (DateTime.Now - startTime).TotalSeconds.ToString() + " seconds to update the database! <br />" + msg.Text;
        }
        // Other FinanceYear DBs
        try
        {
            string path = Server.MapPath("~/files/fdb");
            if (System.IO.Directory.Exists(path))
            {
                string[] files = System.IO.Directory.GetFiles(path, "DB_*.mdf");
                for (int i = 0; i < files.Length; i++)
                {
                    string fileName = System.IO.Path.GetFileNameWithoutExtension(files[i]);
                    if (!fileName.ToLower().EndsWith("_log"))
                    {
                        msg.Text += "<hr /><h3>" + fileName + "</h3><br />";
                        using (DbValidator dbValidator = new DbValidator(fileName))
                        {
                            dbValidator.Messages.Clear();
                            Logger.Pause = true;
                            ExceptionLog.Pause = true;
                            if (dbValidator.SynchronizeTableDesign(tables, "*"))
                            {
                                msg.Text += "Ewa debe <br />";
                                msg.CssClass += " alert-success ";
                                msg.Visible = true;
                            }
                            else
                            {
                                msg.Text += "Bedaxewe <br />";
                                msg.CssClass += " alert-danger ";
                                msg.Visible = true;
                            }
                            Logger.Pause = false;
                            ExceptionLog.Pause = false;
                            for (int j = 0; j < dbValidator.Messages.Count; j++)
                                msg.Text += dbValidator.Messages[j] + "<br />";
                            msg.Text = "It took " + (DateTime.Now - startTime).TotalSeconds.ToString() + " seconds to update the database! <br />" + msg.Text;
                        }
                    }
                }
            }
        }
        catch { }
        finally
        {
            Logger.Pause = false;
            ExceptionLog.Pause = false;
        }
    }
    protected void doUpload_Click(object sender, EventArgs e)
    {
        if (!mFile.HasFile)
        {
            lblUploadMessage.Text = LangProvider.Instance["NoFileChosen"];
            lblUploadMessage.CssClass += " alert-danger ";
            lblUploadMessage.Visible = true;
            return;
        }
        string path = Server.MapPath("~/files/DB");
        //path = System.IO.Path.Combine(path, string.Format("DB_{0}.bak", DateTime.Now.ToString("yyyyMMdd_HHmm")));
        if (mFile.FileName.ToLower().EndsWith(".zip"))
        {
            path = Path.Combine(path, Path.GetFileName(mFile.FileName));
            mFile.SaveAs(path);
            UnzipFile(path);
        }
        else
        {
            path = Path.Combine(path, Path.GetFileNameWithoutExtension(mFile.FileName) + ".bak");
            mFile.SaveAs(path);
        }
        lblUploadMessage.Text = LangProvider.Instance["FileUploadedSuccessfully"];
        lblUploadMessage.CssClass += " alert-success ";
        lblUploadMessage.Visible = true;
        BindData();
    }

    private void UnzipFile(string zipFilePath)
    {
        string serverFolder = Server.MapPath("~/files/DB/");
        ZipInputStream s = new ZipInputStream(File.OpenRead(zipFilePath));
        ZipEntry entry;
        bool first = true;
        while ((entry = s.GetNextEntry()) != null)
        {

            string directoryName = Path.GetDirectoryName(entry.Name);
            string fileName = Path.GetFileName(entry.Name);

            // create directory
            Directory.CreateDirectory(Path.Combine(serverFolder, directoryName));

            if (fileName != String.Empty)
            {
                FileStream streamWriter = File.Create(Path.Combine(serverFolder, entry.Name));
                int size = 2048;
                byte[] data = new byte[2048];
                while (true)
                {
                    size = s.Read(data, 0, data.Length);
                    if (size > 0)
                    {
                        streamWriter.Write(data, 0, size);
                    }
                    else
                    {
                        break;
                    }
                }
                streamWriter.Close();
                if (first && entry.Name.ToLower().EndsWith(".bak"))
                {
                    first = false;
                    serverFolder = Path.Combine(serverFolder, entry.Name.Substring(0, entry.Name.Length - ".bak".Length));
                    Directory.CreateDirectory(serverFolder);
                }
            }
        }

        s.Close();

    }
    protected void doBackup_Click(object sender, EventArgs e)
    {
        doBack();
    }
    protected void doReloadStructure_Click(object sender, EventArgs e)
    {
        QueryController.Instance.Reload();
        TableController.Instance.Reload();
        msg.Text = "Reloaded!";
        msg.CssClass += " alert-success ";
        msg.Visible = true;
    }

    protected void mBackups_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string str = e.Row.DataItem.ToString();
            if (System.IO.Path.GetFileName(str) == mWotkingDbBackup)
                e.Row.CssClass += " hint";

            e.Row.Cells[0].Text = System.IO.Path.GetFileName(str);
            e.Row.Cells[2].Text = string.Format("<a href='files/DB/{0}' target=_blank>{1}</a>", System.IO.Path.GetFileName(str), LangProvider.Instance["Download"]);
            try
            {
                string date = System.IO.Path.GetFileNameWithoutExtension(str).Substring(3, 8);
                e.Row.Cells[1].Text = Helper.FormatDate(Helper.ParseReversEnDate(date, DateTime.Now.AddDays(-2)), "yyyy-MM-dd");
            }
            catch
            {
                e.Row.Cells[1].Text = "";
            }

            //JsTools.HandleDeleteButton((LinkButton)e.Row.Cells[e.Row.Cells.Count - 2].Controls[0], LangProvider.Instance["ConfirmRestore"]);
            JsTools.HandleDeleteButton(e, LangProvider.Instance["ConfirmDelete"]);
            string path = Server.MapPath(@"~\files\db\");
            path = Path.Combine(path, str.Replace(".bak", ""));
            path = Path.Combine(path, "dadocuments");
            path = Path.Combine(path, "c0mment.txt");
            if (File.Exists(path))
            {
                TextBox cmt = (TextBox)e.Row.FindControl("Comments");
                cmt.Text = File.ReadAllText(path);
            }

        }
    }
    protected void mBackups_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (mBackups.SelectedRow == null)
            return;
        // save users
        DataTable users = mProxy.FillData("SELECT * FROM [Users]", "users");
        string fileName = mBackups.SelectedRow.Cells[0].Text;
        string path = Server.MapPath("~/files/DB");

        // Backup refreshes the grid and the file address changes, so we backup must be after 
        doBack();

        path = System.IO.Path.Combine(path, fileName);
        try
        {
            SqlAdmin sqlAdmin = new SqlAdmin();
            string cnt = mProxy.ConnectionString;
            cnt = cnt.Replace(mProxy.CatalogName, "master");
            string catalog = mProxy.CatalogName;
            string cmd = "ALTER DATABASE [" + catalog + "] SET Single_User WITH Rollback Immediate; \r\n";
            cmd += "RESTORE DATABASE [" + catalog + "] FROM DISK='" + path + "' WITH REPLACE ; \r\n";
            cmd += "ALTER DATABASE [" + catalog + "] SET Multi_User;";
            string oldCont = mProxy.ConnectionString;
            mProxy.CloseConnection();
            mProxy.ConnectionString = oldCont.Replace(catalog, "master");
            mProxy.LastException = null;
            mProxy.RunQuery(cmd, true);
            if (mProxy.LastException == null)
            {
                #region restore files
                string src = path.Replace(".bak", "");
                string dest = Server.MapPath("~/files");
                Helper.DirectoryCopy(Path.Combine(src, "dadocuments"), Path.Combine(dest, "dadocuments"), true, null, true);
                #endregion

                BindData();
                msg.Text = LangProvider.Instance["RestoreDone"];
                msg.CssClass += " alert-success ";
                msg.Visible = true;
            }
            else
            {
                msg.Text = LangProvider.Instance["RestoreError"] + ": <br />";
                msg.CssClass += " alert-danger ";
                msg.Visible = true;
                Exception ee = mProxy.LastException;
                while (ee != null)
                {
                    msg.Text += ee.Message + "<br />";
                    ee = ee.InnerException;
                }
            }
            mProxy.CloseConnection();
            mProxy.ConnectionString = oldCont;

            // restore user passwords
            if (users != null)
                for (int i = 0; i < users.Rows.Count; i++)
                {
                    mProxy.RunQuery(string.Format("UPDATE  [Users] SET [Password]={0} WHERE [ID]={1}"
                        , Helper.QoutedString(Helper.GetString(users.Rows[i], "Password", ""))
                        , Helper.GetInt(users.Rows[i], "ID", 0)));
                }

            // Save current backup in setting, we need to bold it in list
            mProxy.SetConfig("WorkingDbBackup", fileName, Null.NullInteger, false);
            BindData();
        }
        catch (Exception ex)
        {
            msg.Text = LangProvider.Instance["RestoreError"] + ": <br />" + ex.Message;
            msg.CssClass += " alert-danger ";
            msg.Visible = true;
        }
    }
    protected void mBackups_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {

    }
    protected void mBackups_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string fileName = mBackups.Rows[e.RowIndex].Cells[0].Text;
        string path = Server.MapPath("~/files/DB");
        path = System.IO.Path.Combine(path, fileName);
        try
        {
            File.Delete(path);
            BindData();
        }
        catch (Exception ex)
        {
            msg.Text = LangProvider.Instance["DeleteError"] + ": <br />" + ex.Message;
            msg.CssClass += " alert-danger ";
            msg.Visible = true;
        }
    }
    protected void mBackups_RowEditing(object sender, GridViewEditEventArgs e)
    {

        mBackups.EditIndex = -1;
        string fileName = mBackups.Rows[e.NewEditIndex].Cells[0].Text;
        string path = Server.MapPath("~/files/DB");
        path = System.IO.Path.Combine(path, fileName);
        string src = Path.Combine(path.Replace(".bak", ""), "dadocuments");

        MemoryStream fs = new MemoryStream();

        byte[] buffer = new byte[4096];
        ZipOutputStream s = new ZipOutputStream(fs);
        {
            //s.SetLevel(9); // 0-9, 9 being the highest compression
            ZipEntry entry = new ZipEntry(Path.GetFileName(path));
            s.PutNextEntry(entry);
            using (FileStream srcFs = File.OpenRead(path))
            {
                int sourceBytes;
                do
                {
                    sourceBytes = srcFs.Read(buffer, 0, buffer.Length);

                    s.Write(buffer, 0, sourceBytes);

                } while (sourceBytes > 0);
            }

            foreach (string file in Directory.GetFiles(src))
            {
                entry = new ZipEntry("dadocuments/" + Path.GetFileName(file));
                entry.DateTime = DateTime.Now;
                entry.IsUnicodeText = true;
                s.PutNextEntry(entry);

                using (FileStream srcFs = File.OpenRead(file))
                {
                    int sourceBytes;
                    do
                    {
                        sourceBytes = srcFs.Read(buffer, 0, buffer.Length);

                        s.Write(buffer, 0, sourceBytes);

                    } while (sourceBytes > 0);
                }
            }
            s.Finish();
            fs.Flush();
            //s.Close();
        }

        //ICSharpCode.SharpZipLib.Zip.ZipFile zf = ICSharpCode.SharpZipLib.Zip.ZipFile.Create(fs);//Server.MapPath("~/files/ddd.zip"));//fs
        //zf.BeginUpdate();
        //zf.Add(path, Path.GetFileName(path));
        //zf.AddDirectory("dadocuments");
        //foreach (string file in Directory.GetFiles(src))
        //{
        //    string f = Path.GetFileName(file);
        //    //zf.Add("dadocuments/" + f, CompressionMethod.Deflated, true);
        //    zf.Add(file, CompressionMethod.Deflated, true);
        //    //ZipEntry entry = new ZipEntry("dadocuments/" + Path.GetFileName(file));

        //    //entry.DateTime = DateTime.Now;
        //    //entry.IsUnicodeText = true;
        //    //zf.Add(entry);
        //    //using (FileStream srcFs = File.OpenRead(file))
        //    //{
        //    //    int sourceBytes;
        //    //    do
        //    //    {
        //    //        sourceBytes = srcFs.Read(buffer, 0, buffer.Length);

        //    //        fs.Write(buffer, 0, sourceBytes);

        //    //    } while (sourceBytes > 0);
        //    //}
        //}
        //zf.CommitUpdate();
        //fs.Flush();

        Response.Clear();
        Response.ClearHeaders();
        Response.ClearContent();
        Response.AddHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(path) + ".zip");
        Response.AddHeader("Content-Length", fs.Length.ToString());
        Response.ContentType = "application/octet-stream";
        Response.Flush();
        Response.BinaryWrite(fs.ToArray());
        Response.End();

        //s.Close();
        fs.Close();
    }
    protected void doFixYK_Click(object sender, EventArgs e)
    {
        string query = @"
ALTER TABLE Loan DISABLE TRIGGER LoanInsertUpdate
ALTER TABLE Workflow DISABLE TRIGGER WorkflowInsertUpdate
ALTER TABLE FinanceYear DISABLE TRIGGER FinanceYearInsertUpdate
ALTER TABLE DocumentAccountingItems DISABLE TRIGGER DocumentAccountingItemsInsertUpdate
ALTER TABLE DocumentAccountingItems DISABLE TRIGGER DocumentAccountingItemsDelete
ALTER TABLE TaskImpl DISABLE TRIGGER TaskImplInsert
ALTER TABLE StoreServices DISABLE TRIGGER StoreServicesAll
ALTER TABLE Trees DISABLE TRIGGER TreesDelete
ALTER TABLE Checks DISABLE TRIGGER ChecksInsert
ALTER TABLE Checks DISABLE TRIGGER ChecksUpdate
ALTER TABLE PayCheck DISABLE TRIGGER PayCheckInsertUpdate
ALTER TABLE PayCheck DISABLE TRIGGER PayCheckUpdate
ALTER TABLE Purchase DISABLE TRIGGER PurchaseInsertUpdate
ALTER TABLE Purchase DISABLE TRIGGER PurchaseDelete
ALTER TABLE PurchaseItems DISABLE TRIGGER PurchaseItemsDelete
ALTER TABLE PurchaseItems DISABLE TRIGGER PurchaseItemsInsertUpdate
ALTER TABLE StoreDraft DISABLE TRIGGER StoreDraftInsertUpdate
ALTER TABLE StoreDraft DISABLE TRIGGER StoreDraftDelete
ALTER TABLE StoreDraftItems DISABLE TRIGGER StoreDraftItemsDelete
ALTER TABLE StoreDraftItems DISABLE TRIGGER StoreDraftItemsInsertUpdate
ALTER TABLE PurchaseRequest DISABLE TRIGGER PurchaseRequestInsertUpdate
ALTER TABLE PurchaseRequest DISABLE TRIGGER PurchaseRequestDelete
ALTER TABLE Request DISABLE TRIGGER RequestInsertUpdate
ALTER TABLE Request DISABLE TRIGGER RequestDelete
ALTER TABLE Returns DISABLE TRIGGER ReturnsInsertUpdate
ALTER TABLE Returns DISABLE TRIGGER ReturnsDelete
ALTER TABLE ReturnItem DISABLE TRIGGER ReturnItemDelete
ALTER TABLE ReturnItem DISABLE TRIGGER ReturnItemInsertUpdate
ALTER TABLE Refuse DISABLE TRIGGER RefuseInsertUpdate
ALTER TABLE Refuse DISABLE TRIGGER RefuseDelete
ALTER TABLE RefuseItem DISABLE TRIGGER RefuseItemDelete
ALTER TABLE RefuseItem DISABLE TRIGGER RefuseItemInsertUpdate
ALTER TABLE Transfer DISABLE TRIGGER TransferInsertUpdate
ALTER TABLE Transfer DISABLE TRIGGER TransferDelete
ALTER TABLE TransferItem DISABLE TRIGGER TransferItemDelete
ALTER TABLE TransferItem DISABLE TRIGGER TransferItemInsertUpdate
ALTER TABLE Cardex DISABLE TRIGGER CardexInsertUpdate
ALTER TABLE Service DISABLE TRIGGER ServiceUpdate
ALTER TABLE BuildOrder DISABLE TRIGGER BuildOrderInsertUpdate
ALTER TABLE BuildOrder DISABLE TRIGGER BuildOrderDelete
ALTER TABLE [Transfer] DISABLE TRIGGER TransferInsertUpdate
ALTER TABLE [Transfer] DISABLE TRIGGER TransferDelete
ALTER TABLE TransferItem DISABLE TRIGGER TransferItemDelete
ALTER TABLE TaskImpl DISABLE TRIGGER TaskImplInsert

-- solve problem of Y-K
DECLARE @Tbl NVARCHAR(MAX),  
@Col NVARCHAR(MAX)     
 DECLARE Table_Cursor CURSOR   
 FOR  
    SELECT a.name, --table  
           b.name --col  
    FROM   sysobjects a,  
           syscolumns b  
    WHERE  a.id = b.id  
           AND a.xtype = 'u' --User table  
           AND (  
                   b.xtype = 99 --ntext  
                   OR b.xtype = 35 -- text  
                   OR b.xtype = 231 --nvarchar  
                   OR b.xtype = 167 --varchar  
                   OR b.xtype = 175 --char  
                   OR b.xtype = 239 --nchar  
               )  
   
 OPEN Table_Cursor FETCH NEXT FROM  Table_Cursor INTO @Tbl, @Col  
 WHILE (@@FETCH_STATUS = 0)  
 BEGIN  
    EXEC (  
             'update [' + @Tbl + '] set [' + @Col +  
             ']= REPLACE(REPLACE(CAST([' + @Col +  
             '] as nvarchar(max)) , NCHAR(1610), NCHAR(1740)),NCHAR(1603),NCHAR(1705)) WHERE CHARINDEX(NCHAR(1603),[' + @Col +  '])>0 OR CHARINDEX(NCHAR(1610),[' + @Col +  '])>0'  
         )  
    PRINT 'Table: ' + @Tbl +' Col: '+ @Col;
    FETCH NEXT FROM Table_Cursor INTO @Tbl,@Col  
 END CLOSE Table_Cursor DEALLOCATE Table_Cursor
";
        try
        {
            mProxy.LastException = null;
            if (mProxy.RunQuery(query) < 0 || mProxy.LastException != null)
            {
                msg.Text = LangProvider.Instance["Failed"] + "<br />";
                if (mProxy.LastException != null)
                    msg.Text += mProxy.LastException.Message;
                msg.CssClass += " alert-danger ";
            }
            else
            {
                msg.Text = LangProvider.Instance["Success"];
                msg.CssClass += " alert-success ";
            }
            msg.Visible = true;
        }
        finally
        {
            // reenable triggers
            query = @"ALTER TABLE Loan ENABLE TRIGGER LoanInsertUpdate
ALTER TABLE Workflow ENABLE TRIGGER WorkflowInsertUpdate
ALTER TABLE FinanceYear ENABLE TRIGGER FinanceYearInsertUpdate
ALTER TABLE DocumentAccountingItems ENABLE TRIGGER DocumentAccountingItemsInsertUpdate
ALTER TABLE DocumentAccountingItems ENABLE TRIGGER DocumentAccountingItemsDelete
ALTER TABLE TaskImpl ENABLE TRIGGER TaskImplInsert
ALTER TABLE StoreServices ENABLE TRIGGER StoreServicesAll
ALTER TABLE Trees ENABLE TRIGGER TreesDelete
ALTER TABLE Checks ENABLE TRIGGER ChecksInsert
ALTER TABLE Checks ENABLE TRIGGER ChecksUpdate
ALTER TABLE PayCheck ENABLE TRIGGER PayCheckInsertUpdate
ALTER TABLE PayCheck ENABLE TRIGGER PayCheckUpdate
ALTER TABLE Purchase ENABLE TRIGGER PurchaseInsertUpdate
ALTER TABLE Purchase ENABLE TRIGGER PurchaseDelete
ALTER TABLE PurchaseItems ENABLE TRIGGER PurchaseItemsDelete
ALTER TABLE PurchaseItems ENABLE TRIGGER PurchaseItemsInsertUpdate
ALTER TABLE StoreDraft ENABLE TRIGGER StoreDraftInsertUpdate
ALTER TABLE StoreDraft ENABLE TRIGGER StoreDraftDelete
ALTER TABLE StoreDraftItems ENABLE TRIGGER StoreDraftItemsDelete
ALTER TABLE StoreDraftItems ENABLE TRIGGER StoreDraftItemsInsertUpdate
ALTER TABLE PurchaseRequest ENABLE TRIGGER PurchaseRequestInsertUpdate
ALTER TABLE PurchaseRequest ENABLE TRIGGER PurchaseRequestDelete
ALTER TABLE Request ENABLE TRIGGER RequestInsertUpdate
ALTER TABLE Request ENABLE TRIGGER RequestDelete
ALTER TABLE Returns ENABLE TRIGGER ReturnsInsertUpdate
ALTER TABLE Returns ENABLE TRIGGER ReturnsDelete
ALTER TABLE ReturnItem ENABLE TRIGGER ReturnItemDelete
ALTER TABLE ReturnItem ENABLE TRIGGER ReturnItemInsertUpdate
ALTER TABLE Refuse ENABLE TRIGGER RefuseInsertUpdate
ALTER TABLE Refuse ENABLE TRIGGER RefuseDelete
ALTER TABLE RefuseItem ENABLE TRIGGER RefuseItemDelete
ALTER TABLE RefuseItem ENABLE TRIGGER RefuseItemInsertUpdate
ALTER TABLE Transfer ENABLE TRIGGER TransferInsertUpdate
ALTER TABLE Transfer ENABLE TRIGGER TransferDelete
ALTER TABLE TransferItem ENABLE TRIGGER TransferItemDelete
ALTER TABLE TransferItem ENABLE TRIGGER TransferItemInsertUpdate
ALTER TABLE Cardex ENABLE TRIGGER CardexInsertUpdate
ALTER TABLE BuildOrder ENABLE TRIGGER BuildOrderInsertUpdate
ALTER TABLE BuildOrder ENABLE TRIGGER BuildOrderDelete
ALTER TABLE [Transfer] ENABLE TRIGGER TransferInsertUpdate
ALTER TABLE [Transfer] ENABLE TRIGGER TransferDelete
ALTER TABLE TransferItem ENABLE TRIGGER TransferItemDelete
ALTER TABLE Service ENABLE TRIGGER ServiceUpdate
ALTER TABLE TaskImpl ENABLE TRIGGER TaskImplInsert";
            mProxy.RunQuery(query);
        }
        
    }
    bool DownloadFile(string src, string dest)
    {
        try
        {
            if (File.Exists(dest))
                File.Delete(dest);
            using (WebClient Client = new WebClient())
            {
                Client.DownloadFile(src, dest);
            }
        }
        catch (Exception ex)
        {
            msg.Text = ex.Message;
            msg.CssClass += " alert-danger ";
            msg.Visible = true;
            return false;
        }
        return true;
    }
    protected void doUpdateSoftware_Click(object sender, EventArgs e)
    {
        try
        {
            UpdateSoftwareInternal();
        }
        catch (Exception ex)
        {
            msg.Text = LangProvider.Instance.ParseLang("Lang.Error") + ":<br />" + ex.Message;
            msg.CssClass += " alert-danger ";
            msg.Visible = true;
        }
    }
    void UpdateSoftwareInternal()
    {
        var repo = new Repository(Server.MapPath("~"));
        PullOptions pullOptions = new PullOptions
        {
            FetchOptions = new FetchOptions
            {
                CredentialsProvider = GetCredentials
            },
            MergeOptions = new MergeOptions
            {
                FileConflictStrategy = CheckoutFileConflictStrategy.Theirs
            }
        };

        MergeResult result = repo.Network.Pull(Helper.GetSignature(), pullOptions);

        switch (result.Status)
        {
            case MergeStatus.FastForward:
            case MergeStatus.NonFastForward:
                msg.Text = LangProvider.Instance.ParseLang("Lang.AppUpdatedSuccessfully");
                msg.CssClass += " alert-success ";
                msg.Visible = true;
                Response.Redirect("default.aspx?do=logout");
                break;
            case MergeStatus.UpToDate:
                msg.Text = LangProvider.Instance.ParseLang("Lang.AppIsAlreadyUpToDate");
                msg.CssClass += " alert-info ";
                msg.Visible = true;
                break;
            case MergeStatus.Conflicts:
                msg.Text = LangProvider.Instance.ParseLang("Lang.AppUpdateFailedContactSUpport");
                msg.CssClass += " alert-danger ";
                msg.Visible = true;
                break;
            default:
                msg.Text = LangProvider.Instance.ParseLang("Lang.UnknownErroWhileUpdatingContactSupport");
                msg.CssClass += " alert-danger ";
                msg.Visible = true;
                break;
        }
    }

    private static Credentials GetCredentials(string url, string usernameFromUrl, SupportedCredentialTypes types)
    {
        return new UsernamePasswordCredentials
        {
            Username = "jpcorelease",
            Password = "BAMumV7eeyxXsB5VgRYdnZFN"
        };
    }

    protected void doBack()
    {
        try
        {
            SqlAdmin sqlAdmin = new SqlAdmin();
            if (sqlAdmin.BackupHisDB())
            {
                BindData();
                msg.Text = LangProvider.Instance["BackupDone"];
                msg.CssClass += " alert-success ";
                msg.Visible = true;
            }
            else
            {
                msg.Text = LangProvider.Instance["BackupError"] + ": <br />";
                msg.CssClass += " alert-danger ";
                msg.Visible = true;
                Exception ee = sqlAdmin.LastException;
                while (ee != null)
                {
                    msg.Text += ee.Message + "<br />";
                    ee = ee.InnerException;
                }
            }
        }
        catch (Exception ex)
        {
            msg.Text = LangProvider.Instance["BackupError"] + ": <br />" + ex.Message;
            msg.CssClass += " alert-danger ";
            msg.Visible = true;
        }
    }

    protected void save_Comment(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "AddComments")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            string fileName = mBackups.Rows[index].Cells[0].Text;
            string path = Server.MapPath("~/files/DB");
            path = System.IO.Path.Combine(path, fileName);
            path = System.IO.Path.Combine(path, "dadocuments");
            path = path.Replace(".bak", "");
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            TextBox cmt = (TextBox)mBackups.Rows[index].FindControl("Comments");
            string comment = cmt.Text;
            File.WriteAllText(Path.Combine(path, "c0mment.txt"), comment);// for sepecific name we use 0 in c0mment
        }
    }
}
