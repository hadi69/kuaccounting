﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="Tax.aspx.cs" Inherits="Tax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .col-wide-label {
            width: 44.7% !important;
        }

        .col-wide-text {
            width: 21% !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
            <div class="form-group">
                <asp:Label ID="Label2" runat="server" Text="Lang.Year" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"></asp:Label>
                <div class="col-xs-12 col-sm-8 form-col-without-padding">
                    <asp:DropDownList ID="ddlYear" runat="server" PlaceHolder="YYYY" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" Text="Lang.Month" PlaceHolder="MM" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"></asp:Label>
                <div class="col-xs-12 col-sm-8 form-col-without-padding">
                    <asp:DropDownList ID="ddlMonth" runat="server" CssClass="chosen-select chosen-rtl">
                        <asp:ListItem Value="01">Lang.Farvardin</asp:ListItem>
                        <asp:ListItem Value="02">Lang.Ordibehesht</asp:ListItem>
                        <asp:ListItem Value="03">Lang.Khordad</asp:ListItem>
                        <asp:ListItem Value="04">Lang.Tir</asp:ListItem>
                        <asp:ListItem Value="05">Lang.Mordad</asp:ListItem>
                        <asp:ListItem Value="06">Lang.Shahrivar</asp:ListItem>
                        <asp:ListItem Value="07">Lang.Mehr</asp:ListItem>
                        <asp:ListItem Value="08">Lang.Aban</asp:ListItem>
                        <asp:ListItem Value="09">Lang.Azar</asp:ListItem>
                        <asp:ListItem Value="10">Lang.Dey</asp:ListItem>
                        <asp:ListItem Value="11">Lang.Bahman</asp:ListItem>
                        <asp:ListItem Value="12">Lang.Esfand</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" Text="Lang.IncludeTaxFreePeople" CssClass="left-aligned-label control-label col-xs-12 col-sm-8"></asp:Label>
                <div class="col-xs-12 col-sm-4 form-col-without-padding">
                    <asp:CheckBox ID="ShowUnTax" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <div class="row margin-10-top">
        <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" Text="Lang.Date" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"></asp:Label>
                <div class="col-xs-12 col-sm-8 form-col-without-padding">
                    <asp:TextBox ID="Date" runat="server" PlaceHolder="YYYY/MM/DD" CssClass="form-control input-sm dateSelect"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" Text="Lang.PaymentMethod" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"></asp:Label>
                <div class="col-xs-12 col-sm-8 form-col-without-padding">
                    <asp:DropDownList ID="PayType" runat="server" CssClass="chosen-select chosen-rtl">
                        <asp:ListItem Value="1">Lang.PersonalCheck</asp:ListItem>
                        <asp:ListItem Value="2">Lang.PaymentCard</asp:ListItem>
                        <asp:ListItem Value="3">Lang.BankTransfer</asp:ListItem>
                        <asp:ListItem Value="4">Lang.PromissoryNote</asp:ListItem>
                        <asp:ListItem Value="5">Lang.CertifiedCheck</asp:ListItem>
                        <asp:ListItem Value="6">Lang.Lang.CashPayment</asp:ListItem>
                        <asp:ListItem Value="7">Lang.Treasury</asp:ListItem>
                        <asp:ListItem Value="8">Lang.PresentListWithoutPayingTax</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="row margin-10-top">
        <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" Text="Lang.CheckDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"></asp:Label>
                <div class="col-xs-12 col-sm-8 form-col-without-padding">
                    <asp:TextBox ID="CheckDate" runat="server" PlaceHolder="YYYY/MM/DD" CssClass="form-control input-sm dateSelect"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" Text="Lang.CheckSerialNo" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"></asp:Label>
                <div class="col-xs-12 col-sm-8 form-col-without-padding">
                    <asp:TextBox ID="SerialNo" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" Text="Lang.Bank" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"></asp:Label>
                <div class="col-xs-12 col-sm-8 form-col-without-padding">
                    <asp:DropDownList ID="Bank" runat="server" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="row margin-10-top">
        <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" Text="Lang.Branch" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"></asp:Label>
                <div class="col-xs-12 col-sm-8 form-col-without-padding">
                    <asp:TextBox ID="BranchName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" Text="Lang.AccountNo" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"></asp:Label>
                <div class="col-xs-12 col-sm-8 form-col-without-padding">
                    <asp:TextBox ID="AccountNo" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" Text="Lang.CheckAmount" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"></asp:Label>
                <div class="col-xs-12 col-sm-8 form-col-without-padding">
                    <asp:TextBox ID="CheckPrice" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="row margin-10-top">
        <div class="col-xs-12 hidden">
            <asp:Label runat="server" Text="Lang.FineForNotPresenting" CssClass="left-aligned-label control-label col-xs-12 col-wide-label"></asp:Label>
            <div class="col-xs-12 col-wide-text form-col-without-padding">
                <asp:TextBox ID="CurFine1" runat="server" CssClass="form-control input-sm"></asp:TextBox>
            </div>
        </div>
        <div class="col-xs-12 hidden">
            &nbsp;
        </div>
        <div class="col-xs-12 hidden">
            <asp:Label runat="server" Text="Lang.FineForNotSubtracting" CssClass="left-aligned-label control-label col-xs-12 col-wide-label"></asp:Label>
            <div class="col-xs-12 col-wide-text form-col-without-padding">
                <asp:TextBox ID="CurFine2" CssClass="form-control input-sm" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="col-xs-12 hidden">
            &nbsp;
        </div>
        <div class="col-xs-12">
            <asp:Label runat="server" Text="Lang.ThisMonthTaxDebt" CssClass="left-aligned-label control-label col-xs-12 col-wide-label"></asp:Label>
            <div class="col-xs-12 col-wide-text form-col-without-padding">
                <asp:TextBox ID="CurFine3" CssClass="form-control input-sm" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="col-xs-12">
            &nbsp;
        </div>
        <div class="col-xs-12">
            <asp:Label runat="server" Text="Lang.SumOfWages" CssClass="left-aligned-label control-label col-xs-12 col-wide-label"></asp:Label>
            <div class="col-xs-12 col-wide-text form-col-without-padding">
                <asp:TextBox ID="PastTotalPrice" CssClass="form-control input-sm monetary" runat="server" BackColor="#FFFF99"></asp:TextBox>
            </div>
        </div>
        <div class="col-xs-12">
            &nbsp;
        </div>
        <div class="col-xs-12">
            <asp:Label runat="server" Text="Lang.SumOfWagesInCash" CssClass="left-aligned-label control-label col-xs-12 col-wide-label"></asp:Label>
            <div class="col-xs-12 col-wide-text form-col-without-padding">
                <asp:TextBox ID="PastTaxable" CssClass="form-control input-sm monetary" runat="server" BackColor="#FFFF99"></asp:TextBox>
            </div>
        </div>
        <div class="col-xs-12">
            &nbsp;
        </div>
        <div class="col-xs-12">
            <asp:Label runat="server" Text="Lang.SumOfTaxOfPreviousMonth" CssClass="left-aligned-label control-label col-xs-12 col-wide-label"></asp:Label>
            <div class="col-xs-12 col-wide-text form-col-without-padding">
                <asp:TextBox ID="PastTotalTax" CssClass="form-control input-sm monetary" runat="server" BackColor="#FFFF99"></asp:TextBox>
            </div>
        </div>
        <div class="col-xs-12">
            &nbsp;
        </div>
        <div class="col-xs-12 hidden">
            <asp:Label runat="server" Text="Lang.FineForNotPresentingOnTime" CssClass="left-aligned-label control-label col-xs-12 col-wide-label"></asp:Label>
            <div class="col-xs-12 col-wide-text form-col-without-padding">
                <asp:TextBox ID="PastFine1" CssClass="form-control input-sm" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="col-xs-12 hidden">
            &nbsp;
        </div>
        <div class="col-xs-12 hidden">
            <asp:Label runat="server" Text="Lang.FineForNotSubtractingLastMonth" CssClass="left-aligned-label control-label col-xs-12 col-wide-label"></asp:Label>
            <div class="col-xs-12 col-wide-text form-col-without-padding">
                <asp:TextBox ID="PastFine2" CssClass="form-control input-sm" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="col-xs-12 hidden">
            &nbsp;
        </div>
        <div class="col-xs-12">
            <asp:Label runat="server" Text="Lang.LastMonthTaxDebt" CssClass="left-aligned-label control-label col-xs-12 col-wide-label"></asp:Label>
            <div class="col-xs-12 col-wide-text form-col-without-padding">
                <asp:TextBox ID="PastFine3" CssClass="form-control input-sm" runat="server"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="row margin-10-top">
        <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" Text="Lang.CurTotalPrice" CssClass="left-aligned-label control-label col-xs-12 col-sm-5"></asp:Label>
                <div class="col-xs-12 col-sm-7 form-col-without-padding">
                    <asp:Label runat="server" ID="lblTotalPrice" CssClass="control-label" Text="0"></asp:Label>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" Text="Lang.CurTaxable" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"></asp:Label>
                <div class="col-xs-12 col-sm-8 form-col-without-padding">
                    <asp:Label runat="server" ID="lblTaxable" CssClass="control-label" Text="0"></asp:Label>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" Text="Lang.CurTax" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"></asp:Label>
                <div class="col-xs-12 col-sm-8 form-col-without-padding">
                    <asp:Label runat="server" ID="lblTax" CssClass="control-label" Text="0"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <div class="row margin-10-top">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
            <asp:Button ID="CalcPastWage" runat="server" Text="Lang.CalculateLastMonthWage" OnClick="CalcPastWage_Click1" CssClass="btn btn-sm btn-primary btn-block" />
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
            <asp:Button ID="CreateFile" runat="server" Text="Lang.CreateWageListFile" OnClick="CreateFile_Click" CssClass="btn btn-sm btn-primary btn-block" />
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
            <input type="button" id="PrintIntroduction" name="PrintIntroduction" value="PrintIntroduction" class="btn btn-sm btn-primary btn-block" />
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
            <input type="button" id="PrintSummary" name="PrintSummary" value="PrintListSummary" class="btn btn-sm btn-primary btn-block" />
        </div>
    </div>
    <div class="row margin-10-top">
        <div class="col-xs-12">
            <asp:Label ID="mFile" runat="server" Text="" Visible="false"></asp:Label>
        </div>
    </div>
    <script type="text/javascript">
        dateSelect('.dateSelect');
        $(document).ready(function () {
            $("#bar").prepend(breadcrumb({ title: Lang.TaxAffairs, grandParent: { title: Lang.MSalary, url: 'menu/salary.aspx' }, parent: { title: Lang.OutputFiles, url: 'menu/outputfiles.aspx' } }));
            makeChosen();
            setClassSalary();

            $("#PrintIntroduction").attr('value', Lang[$('#PrintIntroduction').val()]);
            $("#PrintSummary").attr('value', Lang[$('#PrintSummary').val()]);
        });

        $("#PrintIntroduction").click(function () {
            var year = $("#<%= ddlYear.ClientID %>").val();
            if (year == "") {
                alert(Lang.YearHasNotBeenEntered);
                return;
            }
            var month = $("#<%= ddlMonth.ClientID %>").val();
            _url = 'print/tax-introduction/' + year + '/' + month;
            popup(Router.Generate('print-tax-introduction', { params: { month: month, year: year }, query: { print: "true" } }));
            return false;
        });

        $("#PrintSummary").click(function () {
            var year = $("#<%= ddlYear.ClientID %>").val();
            if (year == "") {
                alert(Lang.YearHasNotBeenEntered);
                return;
            }
            var month = $("#<%= ddlMonth.ClientID %>").val();
            var date = $("#<%=Date.ClientID%>").val();
            var paytype = $("#<%=PayType.ClientID%>").val();
            var checkdate = $("#<%=CheckDate.ClientID%>").val();
            var serialno = $("#<%=SerialNo.ClientID%>").val();
            var bank = $("#<%=Bank.ClientID%>").val();
            var branch = $("#<%=BranchName.ClientID%>").val();
            var accno = $("#<%=AccountNo.ClientID%>").val();
            var curfine1 = $("#<%=CurFine1.ClientID%>").val();
            var curfine2 = $("#<%=CurFine2.ClientID%>").val();
            var curfine3 = $("#<%=CurFine3.ClientID%>").val();
            var pasttotalprice = $("#<%=PastTotalPrice.ClientID%>").val();
            var pasttaxable = $("#<%=PastTaxable.ClientID%>").val();
            var pasttotaltax = $("#<%=PastTotalTax.ClientID%>").val();
            var pastfine1 = $("#<%=PastFine1.ClientID%>").val();
            var pastfine2 = $("#<%=PastFine2.ClientID%>").val();
            var pastfine3 = $("#<%=PastFine3.ClientID%>").val();
            //_url = 'print/tax-summary/' + year + '/' + month + '?date=' + date + '&paytype=' + paytype + '&checkdate=' + checkdate + '&serialno=' + serialno + '&bank=' + bank + '&branch=' + branch + '&accno=' + accno + '&curfine1=' + curfine1 + '&curfine2=' + curfine2 + '&curfine3=' + curfine3;
            popup(Router.Generate("print-tax-summary", { params: { month: month, year: year }, query: { date: date, paytype: paytype, checkdate: checkdate, serialno: serialno, bank: bank, branch: branch, accno: accno, curfine1: curfine1, curfine2: curfine2, curfine3: curfine3, pasttotalprice: pasttotalprice, pasttaxable: pasttaxable, pasttotaltax: pasttotaltax, pastfine1: pastfine1, pastfine2: pastfine2, pastfine3: pastfine3, print: "true" } }));
            return false;
        });
    </script>
</asp:Content>

