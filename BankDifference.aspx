﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="BankDifference.aspx.cs" Inherits="BankDifference" %>

<%@ Register Src="controls/ReportSetting.ascx" TagName="ReportSetting" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>

    <asp:Label runat="server" ID="mMsg" CssClass="alert block" Visible="False" EnableViewState="False"></asp:Label><br />
    <asp:Label runat="server" Text="Lang.ChooseFile" CssClass="dashed-bottom padding-10-bottom margin-10-bottom yekan-family block"></asp:Label>
    <asp:FileUpload runat="server" ID="mFileUpload" CssClass="fileupload" /><asp:Label runat="server" ID="mFileName"></asp:Label><br />
    <asp:Button runat="server" ID="doUpload" Text="Lang.Upload" OnClick="doUpload_Click" CssClass="btn btn-sm btn-primary" />
    <br />
    <asp:Panel runat="server" ID="mDetail" Visible="false">
        <div class="SettingRow">
            <uc2:ReportSetting ID="mReportSetting" runat="server" />
        </div>
        <asp:Label runat="server" ID="mHeaders" Visible="false"></asp:Label>
        <asp:Label runat="server" Text="Lang.Preview"></asp:Label>:
        <asp:Label runat="server" ID="mPreview"></asp:Label><br />
        <asp:CheckBox runat="server" ID="mHeaderRow1" Text="Lang.HeaderRow1" Checked="true" /><br />
        <asp:CheckBox runat="server" ID="mReverseRows" Text="Lang.ReverseRows" Checked="true" /><br />
        <asp:Label runat="server" Text="Lang.BankTopic"></asp:Label>
        <asp:TextBox runat="server" ID="mBankTopicName" CssClass="BankTopicName" /><asp:HiddenField runat="server" ID="mBankTopicCode" /><br />
        <asp:Label runat="server" Text="Lang.RemainCol"></asp:Label><asp:TextBox runat="server" ID="mRemainIndex" /><br />
        <asp:Label runat="server" Text="Lang.DateCol"></asp:Label><asp:TextBox runat="server" ID="mDateIndex" /><br />
        <asp:Label runat="server" Text="Lang.StartDate"></asp:Label><asp:TextBox runat="server" ID="mStartDate" CssClass="dateSelect" />
        <asp:Label runat="server" Text="Lang.EndDate"></asp:Label><asp:TextBox runat="server" ID="mEndDate" CssClass="dateSelect" /><br />
        <asp:Button runat="server" ID="doCompare" Text="Lang.StartProcess" OnClick="doCompare_Click" />
        <asp:PlaceHolder runat="server" ID="mResult"></asp:PlaceHolder>
    </asp:Panel>
    <script>
        $(function () {
            checkLevel(2);
            setClassBank();
            dateSelect('.dateSelect');
            
            autoComplete('Trees', 'treesauto', '.BankTopicName', '.BankTopicCode', function (event, data, formatted, textInput, hiddenInput) {
                if (data) {
                    // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due, TopicTitle
                    var hasChild = data[5] != '';
                    if (hasChild) {
                        $(event.target).next().val('');
                        alert(Lang.HasChild);
                    }
                    else {
                        $(event.target).next().val(data[0]);
                    }
                }
            }, formatTreePath, function (data) { return data[8]; });

            $("#bar").prepend(breadcrumb({
                title: Lang.BankDifference,
                parent: {
                    title: Lang.BankAndCheck,
                    url: 'menu/bank.aspx'
                }
            }));
        });
    </script>
</asp:Content>



