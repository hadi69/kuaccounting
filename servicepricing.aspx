﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="servicepricing.aspx.cs" Inherits="servicepricing" EnableViewState="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery-1.4.1-vsdoc.js" language="javascript" type="text/javascript"></script>
    <script src="js/plugins.js" language="javascript" type="text/javascript"></script>

  <script src="js/site.js" language="javascript" type="text/javascript"></script>
  

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <input type="hidden" runat="server" id="mMsg" class="mMsg" />
    <div>
        <div id='tabPrice' class='tab_content'>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                    <div class="form-group">
                        <label for='Date' class="datelable left-aligned-label col-xs-12 col-sm-4"></label>
                        <div class="col-xs-12 col-sm-8 form-col-without-padding">
                            <input type="text" id="Date" name="Date" runat="server" class='date dateSelect form-control input-sm' />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                    <div class="form-group">
                        <label for='Show' class="col-xs-12 col-sm-4"></label>
                        <div class="col-xs-12 col-sm-8 form-col-without-padding">
                            <input type="Button" ID="Show" class="btn btn-primary show" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container">
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-8 form-col-without-padding">
                            <asp:Button runat="server" ID="doSavePrice1" OnClick="doSavePrice_Click" Text="Lang.SaveQuery (F2)" CssClass="btn btn-primary doSavePrice" />
                            <input type="Button" ID="Print" class="btn btn-primary print" />
                            <a href="#" onClick ="$('#tablePrice').tableExport({type:'excel',escape:'false'});" class="btn btn-primary">XLS</a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="HiddenInputs" class="display-none">
            </div>
            <div class="scrollTableContainer">
            <table id="tablePrice" class="table-with-border awesome-table" >
            </table>
            </div>
            <br />
            <div class="align-center display-none">
                <asp:Button runat="server" ID="doSavePrice" OnClick="doSavePrice_Click" Text="Lang.SaveQuery (F2)" CssClass="btn btn-primary doSavePrice" />
                <!--<asp:Button runat="server" ID="doUpdate" Text="Lang.Update (F3)" CssClass="btn btn-primary doUpdate" />-->
            </div>
        </div>
      </div>
      <script type="text/javascript" src="js/jsexport/tableExport.js"></script>
      <script type="text/javascript" src="js/jsexport/jquery.base64.js"></script>
      <script type="text/javascript" src="js/jsexport/html2canvas.js"></script>
      <script type="text/javascript" src="js/jsexport/sprintf.js"></script>
      <script type="text/javascript" src="js/jsexport/jspdf.js"></script>
      <script type="text/javascript" src="js/jsexport/base64.js"></script>
    <script>

        function handlePrice() {
            var _autourl = 'get.aspx?op=Auto&table=SaleType&qd=allsaletypes' + appendTime();
            var saleTypes = getServerRows(_autourl);
            if (saleTypes) {
                var head = "<thead><tr class='th'>";
                head += "<th class='width-150  align-center'><Label>" + Lang.Service + "</Label><input  type='text' id='sfilter' placeholder='filter' /></th>";
                head += "<th class='width-90  align-center buyprice'><Label>" + Lang.BuyPrice + "</Label></th>";
                head += "<th class='width-90  align-center'><Label>" + Lang.SalePrice + "</Label></th>";
                for (var i = 0; i < saleTypes.length - 1; i++) {
                    head += "<th class='width-90  align-center'>";
                    head += "<Label>" + saleTypes[i];
                    head += "</Label>";
                    head += "</th>";
                }
                head += '</tr></thead><tbody">';
                $('#tablePrice').append(head);
            }

            var cnt = 1;
            function addPrice(node) {
                var serviceName = getNodeVal(node, "ServiceName");
                var row = '<tr>';
                row += '<td class="align-center">' + serviceName + '</td>';
                row += '<td class="align-center buyprice"><input type="text" name="BuyPrice" readonly="readonly" value="' + getNodeVal(node, "BuyPrice") + '" class=" buyprice monetary form-control align-center input-sm" /></td>';
                row += '<td class="align-center"><input type="text" name="SalePrice" readonly="readonly" value="' + getNodeVal(node, "SalePrice") + '" class="monetary form-control align-center input-sm" /></td>';
                for (var j = 1; j <= saleTypes.length - 1; j++) {
                    $('#HiddenInputs').append('<input type="hidden" id="spid' + cnt + '"  name="ServicePriceID" title="' + serviceName + '" value="' + getNodeVal(node, "ServicePriceID" + j) + '" class="input-sm" />');
                    $('#HiddenInputs').append('<input type="hidden" id="stid' + cnt + '"  name="SaleTypeID" value="' + getNodeVal(node, "SaleTypeID" + j) + '" class="input-sm" /><br/>');
                    row += '<td class="align-center">'
                        + '<input type="text" name="Price" value="' + getNodeVal(node, "Price" + j) + '" class="monetary Price validate[custom[integer]] form-control align-center input-sm col-xs-11" /><Lable><a id="l' + cnt + '" class="link" href="#" title="' + Lang.Portfolios + '">::</a></Lable></td>';
                    cnt++;
                }
                row += '</tr>';
                return row; //.replace(/\.0000/g, '');
            }
            fillTable("get.aspx?op=GetXml&qd=allservicepricing&Date=" + $('.date').val(), '#tablePrice', addPrice);
            $('#tablePrice').append('</tbody>');
            cnt = 1;
            $('.doSavePrice, .doSavePrice1').click(function () {
                takeComma();
            });
            //$('.doUpdate, .doUpdate1').click(function () {
            //    takeComma();
            //});
            $('#sfilter').keyup(function () {
                var w = $('#sfilter').val();
                $('#tablePrice tr').each(function (index) {
                    if (index > 0)
                        if (w == '' || $(this).html().indexOf(w) > 0)
                            $(this).removeClass('hidden');
                        else
                            $(this).addClass('hidden');
                });
            });
        }
        function putComma() {
            $('.monetary').keyup(function () {
                $(this).val(numberWithCommasAllowDP(stripCommas($(this).val())));
            });
        }
        function takeComma() {
            $('.monetary').each(function () {
                $(this).val(fromMonetary($(this).val().replace(/\,/g, '')));
            });
        }
        $(function () {
           // $('#filter').attr('class', 'display-none');
            $('#Print').attr('value', Lang.Print);
            $('#Print').click(function () {
                $('#sfilter').hide();
                var head = Lang.ServicePricing;
                printTable('#tablePrice', '', head);
                $('#sfilter').show();
            });

            shortcut.add("F2", function () {
                $('.doSavePrice, .doSavePrice1').click();
            });

            //shortcut.add("F3", function () {
            //    $('.doUpdate, .doUpdate1').click();
            //});

            $('.sidebar').addClass('collapsed');
            tabify();
            dateSelect('.dateSelect');
            $('#Show').val(Lang.Display);
            $('.datelable').text(Lang.Date);

            handlePrice();

            $('.link').click(function() {
                var id = $(this).attr('id').substring(1, $(this).attr('id').length);
                var servicePriceID = $('#spid' + id).val();
                var saleTypeID = $('#stid' + id).val();
                var sName = $('#spid' + id).attr('title');
                var th = $('#tablePrice th').eq($(this).parent().parent().index());
                var saleTypeName = th.text();
                var _url = 'get.aspx?op=GetHtml&table=SaleTypeItem&view=servicepriceinformation&ServicePriceID=' + servicePriceID + '&STypeID=' + saleTypeID + '&ServiceName=' + sName + '&SaleTypeName=' + saleTypeName + appendTime();
                 showPopup(_url, function (v, m, f) {
                 return true;
                 }, getCloseButtons());
            });

            $('#Show').click(function () {
                $("#tablePrice").empty();
                $('#HiddenInputs').empty();
                handlePrice();
                insertDefaultComma();
                Growl.Notice(Lang.Success, 3000);
            });

            function insertDefaultComma() {
                $('.monetary').each(function () {
                    $(this).val(numberWithCommas(toMonetary($(this).val())));
                    if ($(this).val() == 0) {
                        $(this).val('');
                    }
                });
            }

            //alert(numberWithCommas(toMonetary(60)));

            insertDefaultComma();
            putComma();
            var msg = $('.mMsg').val();
            if (msg)
                Growl.Notice(msg);
        });
    </script>
</asp:Content>
