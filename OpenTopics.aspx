﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="OpenTopics.aspx.cs" Inherits="OpenTopics" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container margin-10-top">
            <div class="form-group">
                <label class="control-label col-xs-12 col-sm-4">&nbsp;</label>
                <div class="col-xs-12 col-sm-8">
                    <asp:Label runat="server" ID="msg" CssClass="alert block" Visible="False" EnableViewState="False" />
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="mNewFinanceYearName" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ChooseYear</asp:Label>
                <div class="col-xs-12 col-sm-8">
                    <asp:TextBox runat="server" ID="mNewFinanceYearName" CssClass="NewFinanceYearName form-control input-sm"></asp:TextBox>
                    <input type='hidden' runat="server" id="mNewFinanceYearID" class="NewFinanceYearID" />
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container display-none">
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="mIsTemp" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.OpenTopicsIsTemp</asp:Label>
                <div class="col-xs-12 col-sm-8">
                    <asp:CheckBox runat="server" ID="mIsTemp" CssClass="" AutoPostBack="true" OnCheckedChanged="mIsTemp_CheckedChanged"></asp:CheckBox>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container" id="mOpeningBalanceNameDiv" runat="server">
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="mOpeningBalanceName" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ChooseOpeningBalance</asp:Label>
                <div class="col-xs-12 col-sm-8">
                    <asp:TextBox runat="server" ID="mOpeningBalanceName" CssClass="OpeningBalanceName form-control input-sm" ReadOnly="True"></asp:TextBox>
                    <input type='hidden' runat="server" id="mOpeningBalanceID" class="OpeningBalanceID" />
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container" id="mClosingBalanceNameDiv" runat="server">
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="mClosingBalanceName" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ChooseClosingBalance</asp:Label>
                <div class="col-xs-12 col-sm-8">
                    <asp:TextBox runat="server" ID="mClosingBalanceName" CssClass="ClosingBalanceName form-control input-sm" ReadOnly="True"></asp:TextBox>
                    <input type='hidden' runat="server" id="mClosingBalanceID" class="ClosingBalanceID" />
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container margin-10-top">
            <div class="form-group">
                <label class="control-label col-xs-12 col-sm-4">&nbsp;</label>
                <div class="col-xs-12 col-sm-8">
                    <asp:Button runat="server" ID="doStart" Text="Lang.DoStart" OnClick="doStart_Click" CssClass="btn btn-primary btn-sm" />
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function () {
            setClassManager();
            autoComplete('FinanceYear', 'enumauto1', '.NewFinanceYearName', '.NewFinanceYearID', null, null, null, 'Status=0');
            //autoComplete('Trees', 'treesauto', '.OpeningBalanceName', '.OpeningBalanceID', null);
            //autoComplete('Trees', 'treesauto', '.ClosingBalanceName', '.ClosingBalanceID', null);

            setDefaultByTitle('Trees', 'settingtreesauto', '.OpeningBalanceName', '.OpeningBalanceID', 'OpeningBalanceTreePath');
            setDefaultByTitle('Trees', 'settingtreesauto', '.ClosingBalanceName', '.ClosingBalanceID', 'ClosingBalanceTreePath');

            $('#<%= doStart.ClientID %>').click(function () {
                return confirm(Lang.ConfirmYear);
            });

            // This caused an error, so I commented it out.
            // It was not defined!
            //enableMore();

            $("#bar").prepend(breadcrumb({
                title: Lang.ActivateYear,
                parent: {
                    title: Lang.Management,
                    url: "menu/admin.aspx"
                }
            }));
        });
    </script>
</asp:Content>

