﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

public partial class topics : System.Web.UI.Page
{
    decimal mRemain = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.SetLang();
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");
        if (!IsPostBack)
        {
            BindTopics();
            mTree_SelectedNodeChanged(null, null);
        }
        mTopicProxy.Update += mTopicProxy_Update;
        mTopicProxy.Insert += mTopicProxy_Insert;
        mTopicProxy.CodeChange += mTopicProxy_CodeChange;
        mTopicChooser.TopicSelected += new EventHandler(mTopicChooser_TopicSelected);
        mGrid.EmptyDataText = LangProvider.Instance["ListIsEmpty"];
    }
    protected void doUpdateOweDue_Click(object sender, EventArgs e)
    {
        AccController.UpdateDueOwes();
    }
    void mTopicChooser_TopicSelected(object sender, EventArgs e)
    {
        int id = mTopicChooser.SelectedTopicID;
        if (id == Null.NullInteger)
            return;
        if (AccController.ChangeTree(mTree.SelectedValue, id))
        {
            mTree.SelectedNode.ChildNodes.Clear();
            TopicInfo info = AccController.GetTopic(id);
            mTree.SelectedNode.Text = info.NameCode;
            mTree.SelectedNode.Value = mTree.SelectedValue.Substring(0, mTree.SelectedValue.LastIndexOf('-') + 1) + id;
            mSelectedTitle.Text = AccController.GetTreeFullPath(mTree.SelectedValue);
            List<TreeInfo> childs = AccController.GetTrees(AccController.GetTree(mTree.SelectedValue));
            childs.Sort();
            for (int i = 0; i < childs.Count; i++)
                mTree.SelectedNode.ChildNodes.Add(GetTreeNode(childs[i]));
            mTree.SelectedNode.ExpandAll();
        }
    }

    void DoCopy(string srcTreePath, string dstTreePath)
    {
        if (srcTreePath == dstTreePath)
        {
            (this.Master as IMessageBox).Show(WarningLevel.Error, LangProvider.Instance.ParseLang("Lang.DestinationIsSameAsOrigin"));
            return;
        }
        string error;
        if (AccController.CopyChilds(srcTreePath, dstTreePath, out error))
        {
            //BindAccounts();
            (this.Master as IMessageBox).Show(WarningLevel.Success, LangProvider.Instance.ParseLang("Lang.CopyFinished") + "<br>" + error);
        }
        else
        {
            (this.Master as IMessageBox).Show(WarningLevel.Error, LangProvider.Instance.ParseLang("Lang.CopyFailed"));
            return;
        }
    }
    void mTopicProxy_Update(object sender, EventArgs e)
    {
        if (mTree.Nodes.Count == 0)
            return;
        // edit mode : just update the account
        TopicInfo treeInfo = mTopicProxy.Info;
        Helper.UpdateNodeText(mTree.Nodes[0].ChildNodes, mTree.SelectedNode.Text, treeInfo.NameCode);
        TreeInfo info = AccController.GetTree(mTree.SelectedValue);
       // doAdd.Enabled = info == null || info.CanHaveSubTree;
    }

    void mTopicProxy_Insert(object sender, EventArgs e)
    {
        TopicInfo info = mTopicProxy.Info;
        TreeInfo child = mTopicProxy.Child;

        TreeNode node = GetSingleTreeNode(child, info);
        TreeNode parent = Helper.FindNode(mTree.Nodes[0].ChildNodes, mTopicProxy.ParentTreePath);
        bool hasItems = false;
        List<TreeInfo> childs = null;
        if (parent == null)
        {
            childs = AccController.GetTrees(0, SiteSettings.ProgramID);
            parent = mTree.Nodes[0];
        }
        else
        {
            TreeInfo tree = AccController.GetTree(parent.Value);
            childs = AccController.GetTrees(tree);
            hasItems = AccController.HasItems(tree.TreePath);
        }

        childs.Sort();
        int index = parent.ChildNodes.Count;
        for (int i = 0; i < childs.Count; i++)
        {
            if (childs[i].TreePath == child.TreePath)
            {
                index = i;
                break;
            }
        }
        try
        {
            parent.ChildNodes.AddAt(index, node);
        }
        catch
        {
            parent.ChildNodes.Add(node);
        }
        parent.Expand();
        if (hasItems)
            JsTools.ShowMsg(Page, LangProvider.Instance["RefineVoucherItems"]);
    }
    void mTopicProxy_CodeChange(object sender, EventArgs e)
    {
        BindTopics();
        mTree_SelectedNodeChanged(null, null);
    }

    #region Bindings

    private void BindTopics()
    {
        mTree.Nodes.Clear();
        TreeNode rootNode = new TreeNode(LangProvider.Instance.ParseLang("Lang.MasterAccounts"), "");
        List<TreeInfo> roots = AccController.GetTrees(0, SiteSettings.ProgramID);
        roots.Sort();
        for (int i = 0; i < roots.Count; i++)
        {
            TreeNode node = GetTreeNode(roots[i]);
            if (node != null)
                rootNode.ChildNodes.Add(node);
            else
                rootNode.ChildNodes.Add(new TreeNode("INVALID TOPIC : " + roots[i].TreePath));
        }

        mTree.Nodes.Add(rootNode);
        mTree.CollapseAll();
        rootNode.Expand();
    }
    private TreeNode GetTreeNode(TreeInfo info)
    {
        TopicInfo topic = AccController.GetTopic(info.TopicID);
        TreeNode node = GetSingleTreeNode(info, topic);
        if (node == null)
            return null;
        List<TreeInfo> childs = AccController.GetTrees(info);
        childs.Sort();
        if (childs.Count > 0)
            node.ImageUrl = "~/img/programIcon.gif";
        for (int i = 0; i < childs.Count; i++)
        {
            TreeNode nd = GetTreeNode(childs[i]);
            if (nd != null)
                node.ChildNodes.Add(nd);
            else
                node.ChildNodes.Add(new TreeNode("INVALID TOPIC : " + childs[i].TreePath));
        }
        node.ToolTip = LangProvider.Instance["TotalOwe"] + SiteSettings.ToMonetary(info.Owe, DateTime.Now).ToString("N0") + "\r\n"
            + LangProvider.Instance["TotalDue"] + SiteSettings.ToMonetary(info.Due, DateTime.Now).ToString("N0") + "\r\n"
            + LangProvider.Instance["Remained"] + SiteSettings.ToMonetary(info.Remaining, DateTime.Now).ToString("N0");
        return node;
    }
    private TreeNode GetSingleTreeNode(TreeInfo info, TopicInfo topic)
    {
        if (topic == null || info == null)
            return null;
        TreeNode node = new TreeNode(topic.NameCode, info.TreePath);
        //if (info.Depth == 0)
        node.ImageUrl = "~/img/leaf.gif";
        //else if (info.Depth == 1)
        //    node.ImageUrl = "~/img/totalIcon.gif";
        //else
        //    node.ImageUrl = "~/img/elaborativeIcon.gif";
        return node;
    }
    #endregion

    void ShowMessage(WarningLevel level, string msg, string title)
    {
        string script = string.Format("$(function(){{displayError('{0}', null, '{1}');}});", msg, title);
        ScriptManager.RegisterStartupScript(this, typeof(Page), "script1", script, true);
    }
    protected void mTree_SelectedNodeChanged(object sender, EventArgs e)
    {
        if (mTree.SelectedNode != null)
        {
            if (IsCopying)
            {
                // Copying Mode
                if (mTree.SelectedNode == mTree.Nodes[0])
                {
                    ShowMessage(WarningLevel.Error, LangProvider.Instance.ParseLang("Lang.YouMustChooseAnAccount"), LangProvider.Instance.ParseLang("Lang.CopyFailed"));
                    return;
                }
                TreeInfo old = AccController.GetTree(OldSelectedTreePath), neu = AccController.GetTree(mTree.SelectedValue);
                if (old == null || neu == null)
                {
                    ShowMessage(WarningLevel.Error, LangProvider.Instance.ParseLang("Lang.OriginOrDestinationAccountAreIncorrect"), LangProvider.Instance.ParseLang("Lang.CopyFailed"));
                    return;
                }
                #region Check Nature
                TopicNature oldNature = TopicNature.Both;
                if (old.Depth == 0)
                {
                }
                else
                {
                    TopicInfo total = AccController.GetTopic(old.TotalID);
                    oldNature = total.Nature;
                }
                TopicNature neuNature = TopicNature.Both;
                if (neu.Depth == 0)
                {
                }
                else
                {
                    TopicInfo total = AccController.GetTopic(neu.TotalID);
                    neuNature = total.Nature;
                }
                if (oldNature != neuNature)
                {
                    ShowMessage(WarningLevel.Error, LangProvider.Instance.ParseLang("Lang.OriginAndDestinationAreNotOfTheSameNature"), LangProvider.Instance.ParseLang("Lang.CopyFailed"));
                    return;
                }
                #endregion
                DoCopy(OldSelectedTreePath, mTree.SelectedValue);
                IsCopying = false;
                OldSelectedTreePath = mTree.SelectedValue;
                // Just Load the new Node
                mTree.SelectedNode.ChildNodes.Clear();
                List<TreeInfo> childs = AccController.GetTrees(AccController.GetTree(mTree.SelectedValue));
                childs.Sort();
                for (int i = 0; i < childs.Count; i++)
                    mTree.SelectedNode.ChildNodes.Add(GetTreeNode(childs[i]));
                mTree.SelectedNode.ExpandAll();
                return;
            }
            mSelectedTitle.Text = AccController.GetTreeFullPath(mTree.SelectedValue);
            doEdit.Enabled = mTree.SelectedNode != mTree.Nodes[0];
            TreeInfo info = AccController.GetTree(mTree.SelectedValue);
            // doAdd.Enabled = info == null || info.CanHaveSubTree;
            if (info != null)
            doDelete.Enabled = info.Removable;
        }
        else
        {
            IsCopying = false;
            mSelectedTitle.Text = LangProvider.Instance["TopicsM1"];
            doEdit.Enabled = doDelete.Enabled = doCopy.Enabled = false;
           // doAdd.Enabled = true;
        }
        OldSelectedTreePath = mTree.SelectedValue;

        // Apply role
        doChange.Enabled = doCopy.Enabled = doEdit.Enabled = doEdit.Enabled && SiteSettings.User.HasAccess("Topic", AccessType.Edit);
        doDelete.Enabled = doDelete.Enabled && SiteSettings.User.HasAccess("Topic", AccessType.Delete);
        BindVouchers();
    }
    void BindVouchers()
    {
        mRemain = 0;
        if (mTree.SelectedNode == null)
            mGrid.DataSource = null;
        else
            mGrid.DataSource = AccController.GetBook(mTree.SelectedValue, DateTime.Now.AddYears(-2), DateTime.Now.AddYears(2)
                , Null.NullInteger, -1, -1, SiteSettings.ProgramID, Null.NullInteger, Null.NullInteger, null, null, null, true, false, false, false, null);
        mGrid.DataBind();
        if (null != mGrid.DataSource)
            (mGrid.DataSource as System.Data.DataTable).Dispose();
    }
    protected void doEdit_Click(object sender, EventArgs e)
    {
        mTopicProxy.Visible = true;
        mTopicProxy.Edit(mTree.SelectedValue);
    }
    protected void doAdd_Click(object sender, EventArgs e)
    {
        mTopicProxy.Visible = true;
        mTopicProxy.Add(mTree.SelectedValue, AccController.GetTreePartialPath(mTree.SelectedValue));
    }
    protected void doDelete_Click(object sender, EventArgs e)
    {
        if (mTree.SelectedValue == "")
            return;
        if (AccController.DeleteTree(mTree.SelectedValue))
        {
            mTree.SelectedNode.Parent.ChildNodes.Remove(mTree.SelectedNode);
            IsCopying = false;
            mSelectedTitle.Text = "Lang.TopicsM1";
            Page.SetLang();
            doEdit.Enabled = doDelete.Enabled = doCopy.Enabled = false;
        }
        else
        {
            List<VoucherInfo> useds = AccController.GetVouchers(mTree.SelectedValue);
            if (useds == null || useds.Count == 0)
            {
                ShowMessage(WarningLevel.Error, LangProvider.Instance.ParseLang("Lang.ThisAccountHasChildrenOrSetting"), LangProvider.Instance.ParseLang("Lang.DeletionError"));
            }
            else
            {
                StringBuilder b = new StringBuilder();
                for (int i = 0; i < useds.Count - 1; i++)
                    b.Append(useds[i].ID + " - ");
                b.Append(useds[useds.Count - 1].ID);
                ShowMessage(WarningLevel.Error, LangProvider.Instance.ParseLang("Lang.ThisAccountHasSubProccess") + ":<br>" + b.ToString(), LangProvider.Instance.ParseLang("Lang.DeletionError"));
            }
        }
    }

    protected void doCopy_Click(object sender, EventArgs e)
    {
        if (mTree.SelectedNode == null || mTree.SelectedNode == mTree.Nodes[0])
            return;
        IsCopying = true;
    }
    protected void doChange_Click(object sender, EventArgs e)
    {
        mTopicChooser.Show();
        IsCopying = false;
    }
    private bool IsCopying
    {
        get
        {
            return Helper.GetBool(ViewState["IsCopying"], false);
        }
        set
        {
            ViewState["IsCopying"] = value;
            mCopyModeLbl.Visible = value;
        }
    }
    private string OldSelectedTreePath
    {
        get
        {
            return Helper.GetString(ViewState["OldSelectedTreePath"], null);
        }
        set
        {
            ViewState["OldSelectedTreePath"] = value;
        }
    }

    private decimal sumOwe = 0, sumDue = 0, _due = 0, _owe = 0, _remain = 0;
    string rowStyle = "GridRow";
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblDocumentDate = (Label)e.Row.FindControl("lblDocumentDate");
            System.Data.DataRowView record = e.Row.DataItem as System.Data.DataRowView;
            lblDocumentDate.Text = Helper.ToFaFormat(Helper.GetDateTime(record["Date"], Helper.NullDate));
            _due = SiteSettings.ToMonetary(Helper.GetDecimal(record["Due"], 0), DateTime.Now);
            _owe = SiteSettings.ToMonetary(Helper.GetDecimal(record["Owe"], 0), DateTime.Now);
            _remain = _due - _owe;

            sumDue += _due;
            sumOwe += _owe;

            Label owe = (Label)e.Row.FindControl("owe");
            owe.Text = _owe.ToString("N0").Replace(".00", "");

            Label due = (Label)e.Row.FindControl("due");
            due.Text = _due.ToString("N0").Replace(".00", "");
            mRemain += _remain;
            if (_remain < 0)
            {
                _remain = _remain * -1;
            }
            e.Row.Cells[9].Text = Math.Abs(_remain).ToString("N0");
            //if (mRemain < 0)
            //    e.Row.Cells[9].CssClass = "neg";
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblOwe = (Label)e.Row.FindControl("TotalOwe");
            lblOwe.Text = sumOwe.ToString("N0");

            Label lblDue = (Label)e.Row.FindControl("TotalDue");
            lblDue.Text = sumDue.ToString("N0");
            if (mRemain < 0)
            {
                mRemain = mRemain * -1;
            }
            Label lblTotal = (Label)e.Row.FindControl("remainingTotal");
            lblTotal.Text = (mRemain.ToString("N0").Replace(".00", ""));
        }
    }

}