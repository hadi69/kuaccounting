﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BankFile.aspx.cs" Inherits="BankFile" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>
    <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">
        <div class="form-group">
            <asp:Label ID="Label1" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.bank</asp:Label>
            <div class="col-xs-12 col-sm-8">
                <asp:DropDownList runat="server" ID="ddlBank" CssClass="form-control input-sm" />
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">
        <div class="form-group">
            <asp:Label runat="server" ID="Label2" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.SettlementType</asp:Label>
            <div class="col-xs-12 col-sm-8">
                <asp:DropDownList runat="server" ID="ddlSettlementType" CssClass="form-control input-sm" />
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">
        <div class="form-group">
            <asp:Label ID="Label3" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.PayDate</asp:Label>
            <div class="col-xs-12 col-sm-8">
                <asp:TextBox ID="txtPayDate" class="T2 dateselect form-control input-sm" PlaceHolder="YYYY/MM/DD" runat="server" />
                

            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">
        <div class="form-group">
            <asp:Label ID="Label5" runat="server" Text="Lang.Year" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"></asp:Label>
            <div class="col-xs-12 col-sm-8">
                <asp:DropDownList ID="ddlYear" runat="server" PlaceHolder="YYYY" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">
        <div class="form-group">
            <asp:Label ID="Label4" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.Month</asp:Label>
            <div class="col-xs-12 col-sm-8">
                <asp:DropDownList ID="ddlMonth" runat="server" CssClass="chosen-select chosen-rtl">
                    <asp:ListItem Value="01">Lang.Farvardin</asp:ListItem>
                    <asp:ListItem Value="02">Lang.Ordibehesht</asp:ListItem>
                    <asp:ListItem Value="03">Lang.Khordad</asp:ListItem>
                    <asp:ListItem Value="04">Lang.Tir</asp:ListItem>
                    <asp:ListItem Value="05">Lang.Mordad</asp:ListItem>
                    <asp:ListItem Value="06">Lang.Shahrivar</asp:ListItem>
                    <asp:ListItem Value="07">Lang.Mehr</asp:ListItem>
                    <asp:ListItem Value="08">Lang.Aban</asp:ListItem>
                    <asp:ListItem Value="09">Lang.Azar</asp:ListItem>
                    <asp:ListItem Value="10">Lang.Dey</asp:ListItem>
                    <asp:ListItem Value="11">Lang.Bahman</asp:ListItem>
                    <asp:ListItem Value="12">Lang.Esfand</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">
        <div class="form-group">
            <div class="col-xs-12 col-sm-4">
            </div>
            <div class="col-xs-12 col-sm-8">
                <asp:Button runat="server" ID="doCreateFile" Text="Lang.CreateFile" CssClass="btn btn-primary btn-sm col-xs-12 col-sm-12" OnClick="doCreateFile_Click" />
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">
        <div class="form-group">
            <div class="col-xs-12 col-sm-4">
            </div>
            <div class="col-xs-12 col-sm-8">
                <input type="button" id="PrintBank" value="PrintBank" class="btn btn-primary btn-sm col-xs-12 col-sm-12" />
            </div>
        </div>
    </div>
    <div class="row margin-10-top">
        <div class="col-xs-12">
            <asp:Label ID="mFile" runat="server" Text="" Visible="false"></asp:Label>
        </div>
    </div>
    <div class="row margin-10-top">
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    </div>
    <asp:GridView ID="mGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
        scrolling="Vertical" runat="server" BorderStyle="None" OnRowDataBound="mGrid_RowDataBound"
        AllowSorting="True" OnRowDeleting="mGrid_RowDeleting">
        <Columns>
            <asp:TemplateField HeaderText="Lang.ColRowNr">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ID" HeaderText="ID" Visible="False" />
            <asp:BoundField DataField="BankAccountID" HeaderText="Lang.BankAccount" ItemStyle-HorizontalAlign="Center">
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="No" HeaderText="Lang.No" ItemStyle-HorizontalAlign="Center" SortExpression="No">
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="Date" HeaderText="Lang.Date" ItemStyle-HorizontalAlign="Center" >
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="Year" HeaderText="Lang.Year" ItemStyle-HorizontalAlign="Center" >
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="Month" HeaderText="Lang.Month" ItemStyle-HorizontalAlign="Center" >
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:BoundField>
            <asp:CommandField DeleteText="Lang.ColDel" ShowDeleteButton="True" />
        </Columns>
        <HeaderStyle CssClass="GridHeader" HorizontalAlign="Center" VerticalAlign="Middle" />
        <RowStyle CssClass="GridRow" />
        <SelectedRowStyle CssClass="GridRowSelected" />
        <FooterStyle CssClass="GridHeader" HorizontalAlign="Center" />
    </asp:GridView>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#bar").prepend(breadcrumb({ title: Lang.BankFile, grandParent: { title: Lang.MSalary, url: 'menu/salary.aspx' }, parent: { title: Lang.OutputFiles, url: 'menu/outputfiles.aspx' } }));
        });
        dateSelect('.dateselect');
        $("#PrintBank").attr('value', Lang[$('#PrintBank').val()]);
        $("#PrintBank").click(function () {
            var id = $("#<%=ddlBank.ClientID%>").val();
            var year = $("#<%=ddlYear.ClientID%>").val();
            var month = $("#<%=ddlMonth.ClientID%>").val();
            var payDate = $("#<%=txtPayDate.ClientID%>").val();
            popup(Router.Generate("print-banksalary-melibank", { params: { id: id, year: year, month: month }, query: { paydate: payDate } }));
        });

    </script>
</asp:Content>

