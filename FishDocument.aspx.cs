﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.DynamicData;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class FishDocument : System.Web.UI.Page
{
    private int index = 0;
    private DataTable dt = new DataTable("dt");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Page.SetLang();
            BindSalaryDocumentAccounting();
            BindPersonnel();
            BindSection();
            BindYear();
            BindTree();
        }
    }

    #region Binding
    void BindSalaryDocumentAccounting()
    {
        DataTable salarydocument = DbProxy.Instance.FillData(@"SELECT * FROM DocumentAccounting WHERE Mode=19 ORDER BY No DESC",
            "salarydocument");
        mGrid.DataSource = salarydocument;
        mGrid.DataBind();
    }
    void BindPersonnel()
    {
        DataTable personnels = DbProxy.Instance.FillData("SELECT ID, [dbo].[DisplayPerson](ID, 0) AS PersonnelName  FROM Personnel WHERE PersonnelGroupID=" + DbProxy.Instance.GetConfig("PersonnelGroup"), "personnels") ;
        ddlPersonnel.DataTextField = "PersonnelName";
        ddlPersonnel.DataValueField = "ID";
        ddlPersonnel.DataSource = personnels;
        ddlPersonnel.DataBind();
    }
    void BindSection()
    {
        DataTable sections = DbProxy.Instance.FillData("SELECT ID, Name  FROM Ward WHERE SectionType=8 OR SectionType=9" , "personnels");
        ddlSection.DataTextField = "Name";
        ddlSection.DataValueField = "ID";
        ddlSection.DataSource = sections;
        ddlSection.Items.Insert(0,new ListItem("","NULL"));
        ddlSection.DataBind();
    }
    void BindYear()
    {
        PersianCalendar calendar = new PersianCalendar();
        int year = calendar.GetYear(DateTime.Now);
        ddlYear.Items.Add((year-1).ToString());
        ddlYear.Items.Add(year.ToString());
        ddlYear.Items.Add((year+1).ToString());
        ddlYear.SelectedIndex = 1;
    }
    void BindTree()
    {
        ddlBank.Items.Clear();
        ddlBank.Items.Add(new ListItem(LangProvider.Instance["Banks"], ""));
        QueryInfo qInfo = QueryController.Instance["childtreesauto"];
        string query = qInfo.Query.Replace("@ParentTreePath","'"+
            DbProxy.Instance.GetConfig("BankTreePath", SiteSettings.ProgramID)+"'").Replace("@Title",ddlBank.Text);
        DataTable banks = DbProxy.Instance.FillData(query, "banks");
        ddlBank.DataTextField = "TopicName";
        ddlBank.DataValueField = "TreePath";
        ddlBank.DataSource = banks;
        ddlBank.DataBind();
        ddlBank.SelectedIndex = 0;
    }
    #endregion

    protected void CreateFishDocument_Click(object sender, EventArgs e)
    { 
        try
        {
            int i=
                DbProxy.Instance.RunQuery(string.Format("EXEC dbo.CreateFishsDocument {0},{1},{2}" , ddlYear.Text ,ddlMonth.SelectedValue, SiteSettings.ProgramID));
                lblMessage.Text = LangProvider.Instance.ParseLang("DocumentCreated");
                lblMessage.Visible = true;
            BindSalaryDocumentAccounting();
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
        }
    }
    protected void SettlementDocument_Click(object sender, EventArgs e)
    {
        try
        {
            DataRow row =
                DbProxy.Instance.FillRow(string.Format("EXEC dbo.CreateSettlementDocument {0},{1},{2},'{3}'" , ddlYear.SelectedItem ,ddlMonth.SelectedValue ,SiteSettings.ProgramID , ddlBank.SelectedValue));
            lblMessage.Text = LangProvider.Instance.ParseLang("DocumentCreated");
                lblMessage.Visible = true;
            BindSalaryDocumentAccounting();
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
        }
    }
    protected void ShareDocument_Click(object sender, EventArgs e)
    {
        try
        {
            DataRow row=
                DbProxy.Instance.FillRow(string.Format("EXEC dbo.CreateFishSharingDocument {0},{1},{2}" , ddlYear.Text ,ddlMonth.SelectedValue , SiteSettings.ProgramID));
                lblMessage.Text = LangProvider.Instance.ParseLang("DocumentCreated");
                lblMessage.Visible = true;
            BindSalaryDocumentAccounting();
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
        }
    }
    protected void CreateTestDocument_Click(object sender, EventArgs e)
    {
        try
        {
            DataRow row =
                DbProxy.Instance.FillRow(string.Format("EXEC dbo.CreateTestDocument {0},{1},{2},{3}" ,ddlYear.Text ,ddlMonth.SelectedValue , ddlPersonnel.SelectedValue , SiteSettings.ProgramID));
                lblMessage.Text = LangProvider.Instance.ParseLang("DocumentCreated");
                lblMessage.Visible = true;
            BindSalaryDocumentAccounting();
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
        }
    }
    protected void InsuranceDocument_Click(object sender, EventArgs e)
    {
        try
        {
            DataRow row =
                DbProxy.Instance.FillRow(string.Format("EXEC dbo.CreateInsuranceDocument {0},{1},{2},'{3}'", ddlYear.Text , ddlMonth.SelectedValue , SiteSettings.ProgramID , ddlBank.SelectedValue));
                lblMessage.Text = LangProvider.Instance.ParseLang("DocumentCreated");
                Visible = true;
            BindSalaryDocumentAccounting();
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
        }
    }


    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView row = e.Row.DataItem as DataRowView;
            // url2
            e.Row.Cells[1].Text = row["ID"].ToString();
            try
            {
                e.Row.Cells[3].Text = Helper.ToFaFormat(Helper.GetDateTime(row["Date"],new DateTime()));
            }
            catch
            {
                e.Row.Cells[3].Text = "";
            }
            e.Row.Cells[4].Text = string.Format("<a href='list.aspx?table=DocumentAccounting&id={0}'>{1}</a>",
                row["ID"], row["No"]);
        }
    }
    protected void mGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        
        int id = Helper.GetInt(mGrid.Rows[e.RowIndex].Cells[1].Text,-1);
        QueryInfo qinfo = QueryController.Instance["documentaccountingdelete"];
        int num=DbProxy.Instance.RunQuery(qinfo.Query.Replace("@ID",id.ToString()));
        if (num>0)
        {
            lblMessage.Text = LangProvider.Instance["DocumentDeletedSuccess"];
            lblMessage.Visible = true;
        }
        BindSalaryDocumentAccounting();
    }
    protected void CreateFishProjectDocument_Click(object sender, EventArgs e)
    {
        try
        {
            int i =
                DbProxy.Instance.RunQuery(string.Format("EXEC dbo.CreateFishDocumentByProject {0},{1},{2}", ddlYear.Text, ddlMonth.SelectedValue, SiteSettings.ProgramID));
            lblMessage.Text = LangProvider.Instance.ParseLang("DocumentCreated");
            lblMessage.Visible = true;
            BindSalaryDocumentAccounting();
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
        }
    }
    protected void CreateTestDocumentByProject_Click(object sender, EventArgs e)
    {
        try
        {
            DataRow row =
                DbProxy.Instance.FillRow(string.Format("EXEC dbo.CreateTestDocumentByProject {0},{1},{2},{3},{4}", ddlYear.Text, ddlMonth.SelectedValue, ddlPersonnel.SelectedValue,ddlSection.SelectedValue, SiteSettings.ProgramID));
            lblMessage.Text = LangProvider.Instance.ParseLang("DocumentCreated");
            lblMessage.Visible = true;
            BindSalaryDocumentAccounting();
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
        }
    }
}