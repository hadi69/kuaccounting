﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class sendmessage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int visitorID = Helper.GetInt(Request.QueryString["vid"], Null.NullInteger);
        string msg = Helper.GetString(Request.QueryString["msg"], "");
        if (visitorID != Null.NullInteger && !string.IsNullOrEmpty(msg))
        {
            DbProxy.Instance.RunQuery(string.Format("INSERT INTO TabletMsg (VisitorID, Msg, AddDate, [Mode]) VALUES ({0}, {1}, GETDATE(), 1)"
            , visitorID, Helper.QoutedString(msg)));
        }
        Response.End();
    }
}