﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Text;

public partial class showreport : System.Web.UI.Page
{
    string view;
    protected void Page_Load(object sender, EventArgs e)
    {
        mHolder.Controls.Clear();
        view = Request.QueryString["view"];
        if (string.IsNullOrEmpty(view))
            return;

        // view     : files.htm
        // result   : filesresult.htm
        // master   : filesmaster
        // detail   : filesdetail
        string path = Server.MapPath("~/reports/" + view + "result.htm");
        if (!File.Exists(path))
            return;
        string content = File.ReadAllText(path);
        content = GetString(content, "<body>", "</body>");

        string masterQry = null, detailQry = null;
        #region find queries
        QueryInfo qInfo = QueryController.Instance[view + "master"];
        if (qInfo != null)
            if (!string.IsNullOrEmpty(qInfo.Query))
                masterQry = qInfo.Query;
        qInfo = QueryController.Instance[view + "detail"];
        if (qInfo != null)
            if (!string.IsNullOrEmpty(qInfo.Query))
                detailQry = qInfo.Query;
        #endregion

        string header = GetString(content, "<!-- HEADER START //-->", "<!-- HEADER END //-->");
        string pageHeader = GetString(content, "<!-- PAGE HEADER START //-->", "<!-- PAGE HEADER END //-->");
        string record = GetString(content, "<!-- RECORD START //-->", "<!-- RECORD END //-->");
        string pageFooter = GetString(content, "<!-- PAGE FOOTER START //-->", "<!-- PAGE FOOTER END //-->");
        string footer = GetString(content, "<!-- FOOTER START //-->", "<!-- FOOTER END //-->");

        // Header & Footer from Main
        DataRow row = masterQry == null ? null : DbProxy.Instance.FillRow(Helper.ParseQueryVars(masterQry, (TableInfo)null, Request));
        header = Helper.ParseVars(header, (List<TableInfo>)null, row, Request);
        footer = Helper.ParseVars(footer, (List<TableInfo>)null, row, Request);

        StringBuilder builder = new StringBuilder();
        builder.Append(pageHeader);
        DataTable dt = detailQry == null ? null : DbProxy.Instance.FillData(Helper.ParseQueryVars(detailQry,(TableInfo) null, Request), "xx");
        int count = 0;
        if (dt != null)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                builder.Append(Helper.ParseVars(record, (List<TableInfo>)null, dt.Rows[i], Request));
                count++;
                if (count == SiteSettings.RowsPerPage)
                {
                    builder.Append(pageFooter);
                    count = 0;
                }
            }
        }
        if (count > 0)
            builder.Append(pageFooter);

        mHolder.Controls.Add(new LiteralControl(header));
        mHolder.Controls.Add(new LiteralControl(builder.ToString()));
        mHolder.Controls.Add(new LiteralControl(footer));
    }
    static string GetString(string content, string startStr, string endStr)
    {
        if (!content.Contains(startStr))
            return "";
        int start = content.IndexOf(startStr);
        if (start >= 0)
            content = content.Substring(start + startStr.Length);
        start = content.IndexOf(endStr);
        if (start >= 0)
            content = content.Substring(0, start);
        return content;
    }
}