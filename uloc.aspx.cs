﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class uloc : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int visitorID = Convert.ToInt32(Request.QueryString["vid"]);
            double lat = Convert.ToDouble(Request.QueryString["lat"]);
            double lon = Convert.ToDouble(Request.QueryString["lon"]);
            //string title = DbProxy.Instance.FillScalarString(string.Format("SELECT [dbo].[DisplayPerson]({0}, 1)", aId));

            DataRow dr = DbProxy.Instance.FillRow("SELECT LastLatittude, LastLongitude FROM Personnel WHERE ID=" + visitorID);
            // Update last location anyway
             DbProxy.Instance.RunQuery(string.Format("UPDATE Personnel SET LastLocationDate={0}, LastLatittude={1}, LastLongitude={2} WHERE ID={3}"
                , Helper.QoutedDateTime(DateTime.Now), lat, lon, visitorID));

            // insert location
             if (dr != null)
                 if (lat != Helper.GetDouble(dr, "LastLatittude", 0) || lon != Helper.GetDouble(dr, "LastLongitude", 0))
                     DbProxy.Instance.RunQuery(string.Format("INSERT INTO VisitorLocation (PersonnelID, LocationDate, Latittude, Longitude) VALUES({0}, {1}, {2}, {3})"
                         , visitorID, Helper.QoutedDateTime(DateTime.Now), lat, lon));
        }

        Response.End();
    }
}