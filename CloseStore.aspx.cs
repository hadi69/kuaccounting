﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CloseStore : System.Web.UI.Page
{
    System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
    bool mViewMode = false;
    bool excelMode = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        mGrid.EmptyDataText = LangProvider.Instance["ServiceNotFound"];
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");

        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["CloseStore"];
            Page.SetLang();

            this.StoreHandlingID = Helper.GetInt(Request.QueryString["id"], Null.NullInteger);
            if (StoreHandlingID == Null.NullInteger)
                Response.Redirect("list.aspx?table=StoreHandling");

            InitData();
        }
        msg.Text = "";
    }

    void InitData()
    {
        DataRow row = DbProxy.Instance.FillRow("SELECT * FROM StoreHandlingView WHERE ID = " + this.StoreHandlingID);
        this.StoreID = Helper.GetInt(row, "StoreID", Null.NullInteger);
        if (StoreID == Null.NullInteger)
            Response.Redirect("list.aspx?table=StoreHandling");
        mStoreName.Text = Helper.GetString(row, "StoreName", "");
        RowTitle.Value = Helper.GetString(row, "Title", mStoreName.Text);

        IDataReader services = DbProxy.Instance.ExecuteReader("SELECT ServicePriceView.ID, ServiceFullName FROM ServicePriceView");
        mServiceName.Items.Clear();
        mServiceName.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
        if (services != null)
        {
            while (services.Read())
            {
                if (!services.IsDBNull(0) && !services.IsDBNull(1))
                    mServiceName.Items.Add(new ListItem(services.GetString(1), services.GetInt32(0).ToString()));
            }
            services.Close();
        }
        mServiceName.SelectedIndex = 0;

        //IDataReader storeLayouts = DbProxy.Instance.ExecuteReader("SELECT ID, Title FROM StoreLayout WHERE StoreID = "
        //    + this.StoreID);
        //mStoreLayouts.Items.Clear();
        //mStoreLayouts.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
        //if (storeLayouts != null)
        //{
        //    while (storeLayouts.Read())
        //        mStoreLayouts.Items.Add(new ListItem(storeLayouts.GetString(1), storeLayouts.GetInt32(0).ToString()));
        //    storeLayouts.Close();
        //}
        //mStoreLayouts.SelectedIndex = 0;


        IDataReader projects = DbProxy.Instance.ExecuteReader("SELECT ID, Title FROM ServiceType");
        mServiceTypes.Items.Clear();
        mServiceTypes.Items.Add(new ListItem(LangProvider.Instance["AllCommodityGroups"], ""));
        if (projects != null)
        {
            while (projects.Read())
                mServiceTypes.Items.Add(new ListItem(projects.GetString(1), projects.GetInt32(0).ToString()));
            projects.Close();
        }
        mServiceTypes.SelectedIndex = 0;

        IDataReader Grouping1 = DbProxy.Instance.ExecuteReader("SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM [Grouping] WHERE [Grouping].GroupingType=1");
        mSZGroup.Items.Clear();
        mSZGroup.Items.Add(new ListItem(LangProvider.Instance["AllGroup"], ""));
        if (Grouping1 != null)
        {
            while (Grouping1.Read())
                mSZGroup.Items.Add(new ListItem(Grouping1.IsDBNull(1) ? "" : Grouping1.GetString(1), Grouping1.GetInt32(0).ToString()));
            Grouping1.Close();
        }
        mSZGroup.SelectedIndex = 0;
    }

    protected void BindGrid()
    {
        using (DbProxy dbProxy = new DbProxy())
        {
            // detect view mode
            int storeDraftID = Helper.GetInt(dbProxy.FillScalar("SELECT StoreDraftID FROM StoreHandling WHERE ID = " + this.StoreHandlingID), Null.NullInteger);
            mViewMode = storeDraftID > 0;
            doSave.Enabled = doSave2.Enabled = doFinalize.Enabled = !mViewMode;
            mLinks.Visible = mViewMode;
            if (mViewMode)
            {
                string storeDraftNo = Helper.GetString(dbProxy.FillScalar("SELECT NOSerial FROM StoreDraft WHERE ID = " + storeDraftID), "");
                int purchaseID = Helper.GetInt(dbProxy.FillScalar("SELECT PurchaseID FROM StoreHandling WHERE ID = " + this.StoreHandlingID), Null.NullInteger);
                string purchaseNo = Helper.GetString(dbProxy.FillScalar("SELECT NOSerial FROM Purchase WHERE ID = " + purchaseID), "");

                int storeDraftVoucherID = Helper.GetInt(dbProxy.FillScalar("SELECT VoucherID FROM StoreDraft WHERE ID = " + storeDraftID), Null.NullInteger);
                string storeDraftVoucherNo = Helper.GetString(dbProxy.FillScalar("SELECT No FROM DocumentAccounting WHERE ID = " + storeDraftVoucherID), "");

                int purchaseVoucherID = Helper.GetInt(dbProxy.FillScalar("SELECT VoucherID FROM Purchase WHERE ID = " + purchaseID), Null.NullInteger);
                string purchaseVoucherNo = Helper.GetString(dbProxy.FillScalar("SELECT No FROM DocumentAccounting WHERE ID = " + purchaseVoucherID), "");

                mStoreDraftLink.Text = string.Format("<a href='list.aspx?table=StoreDraft&view=shopstoredrafts&id={0}'>{1}</a>", storeDraftID, storeDraftNo);
                mPurchaseLink.Text = string.Format("<a href='list.aspx?table=Purchase&view=shoppurchases&id={0}'>{1}</a>", purchaseID, purchaseNo);
                mStoreDraftVoucherLink.Text = string.Format("<a href='list.aspx?table=DocumentAccounting&id={0}'>{1}</a>", storeDraftVoucherID, storeDraftVoucherNo);
                mPurchaseVoucherLink.Text = string.Format("<a href='list.aspx?table=DocumentAccounting&id={0}'>{1}</a>", purchaseVoucherID, purchaseVoucherNo);
            }
            
            // fill StoreHandlingItems for current Store
            string query = string.Format(@"INSERT INTO StoreHandlingItems (StoreHandlingID, ServicePriceID, Count1, Count2, Count3) 
                SELECT {0}, ID, NULL, NULL, NULL FROM ServicePrice WHERE 
                    ServicePrice.ID IN (SELECT ServicePriceID FROM StoreServiceDef WHERE StoreID = {1})
                    AND ServicePrice.ServiceID IN (SELECT ID FROM [Service] WHERE [Service].[Enabled] = 1)
                    AND NOT ServicePrice.ID IN (SELECT ServicePriceID FROM StoreHandlingItems WHERE StoreHandlingID = {0})"
                , this.StoreHandlingID, this.StoreID);
            dbProxy.RunQuery(query);

            string where = " 1 = 1 ";
            int serviceTypeID = Helper.GetInt(mServiceTypes.SelectedValue, Null.NullInteger);
            if (serviceTypeID != Null.NullInteger)
                where += " AND Service.ServiceTypeID = " + serviceTypeID;

            int gID = Helper.GetInt(mSZGroup.SelectedValue, Null.NullInteger);
            if (gID != Null.NullInteger)
                where += "AND ServicePriceView.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (SELECT ID FROM dbo.GetAllGroupingChilds(" + gID + ")))";

            //int storeLayoutID = Helper.GetInt(mStoreLayouts.SelectedValue, Null.NullInteger);
            //if (storeLayoutID != Null.NullInteger)
            //    where += string.Format(" AND ServicePriceView.ID IN (SELECT ServicePriceID FROM StoreServiceDef WHERE StoreServiceDef.StoreID={0} AND StoreLayoutID={1})", this.StoreID, storeLayoutID);

            int servicePriceID = Helper.GetInt(mServiceName.SelectedValue, Null.NullInteger);
            if (servicePriceID != Null.NullInteger)
                where += string.Format(" AND ServicePriceView.ID = {0}", servicePriceID);

            query = string.Format(@"SELECT StoreHandlingItems.ID, SPW.Title, SPW.UnitName, SPW.Code, Count1, Count2, Count3
	, SPW.Supply
    , (ISNULL(Count1, 0) - ISNULL(Supply, 0)) Diff1, (ISNULL(Count2, 0) - ISNULL(Supply, 0)) Diff2, (ISNULL(Count3, 0) - ISNULL(Supply, 0)) Diff3
FROM StoreHandlingItems
	INNER JOIN (SELECT ServicePriceView.ID, ServicePriceView.ServiceFullName Title, Service.Code, Units.Title UnitName
	    , (SELECT SUM(ISNULL(Income, 0) - ISNULL(Outcome, 0)) FROM StoreServiceDef 
			WHERE StoreServiceDef.ServicePriceID=ServicePriceView.ID AND StoreServiceDef.StoreID = {2}) Supply
            FROM ServicePriceView INNER JOIN Service ON ServicePriceView.ServiceID = Service.ID
            INNER JOIN Units ON Service.UnitID = Units.ID
        WHERE {1}
    ) SPW ON StoreHandlingItems.ServicePriceID = SPW.ID
	INNER JOIN StoreHandling ON StoreHandlingItems.StoreHandlingID = StoreHandling.ID    
WHERE StoreHandling.ID = {0}", this.StoreHandlingID, where, this.StoreID);


            if (!string.IsNullOrEmpty(SortCol))
                query += " ORDER BY " + SortCol + " " + SortOrder;
            else
                query += " ORDER BY Title";
            IDataReader items = DbProxy.Instance.ExecuteReader(query);
            try
            {
                mGrid.DataSource = items;
                mGrid.DataBind();
            }
            finally
            {
                if (null != items)
                {
                    items.Close();
                    items.Dispose();
                }
            }
        }
    }
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.Common.DbDataRecord record = e.Row.DataItem as System.Data.Common.DbDataRecord;
            int id = Helper.GetInt(record["ID"], -1);
            e.Row.Cells[1].Text = id.ToString();

            //decimal income = Helper.GetDecimal(record["Income"], 0), outcome = Helper.GetDecimal(record["Outcome"], 0);
            decimal c1 = Helper.GetDecimal(record["Count1"], 0);
            decimal c2 = Helper.GetDecimal(record["Count2"], 0);
            decimal c3 = Helper.GetDecimal(record["Count3"], 0);
            decimal d1 = Helper.GetDecimal(record["Diff1"], 0);
            decimal d2 = Helper.GetDecimal(record["Diff2"], 0);
            decimal d3 = Helper.GetDecimal(record["Diff3"], 0);
            if (excelMode)
            {
                e.Row.Cells[6].Text = c1 == 0 ? "" : c1.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                e.Row.Cells[8].Text = c2 == 0 ? "" : c2.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                e.Row.Cells[10].Text = c3 == 0 ? "" : c3.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                e.Row.Cells[7].Text = d1.ToString("N4", Helper.NumberCulture);
                e.Row.Cells[9].Text = d2.ToString("N4", Helper.NumberCulture);
                e.Row.Cells[11].Text = d3.ToString("N4", Helper.NumberCulture);
            }
            else
            {
               if (mViewMode)
                {
                    e.Row.Cells[6].Text = string.Format("<span class='counted'>{1}</span>", id, c1 == 0 ? "" : c1.ToString("N4", Helper.NumberCulture).Replace(".0000", ""));
                    e.Row.Cells[8].Text = string.Format("<span class='counted'>{1}</span>", id, c2 == 0 ? "" : c2.ToString("N4", Helper.NumberCulture).Replace(".0000", ""));
                    e.Row.Cells[10].Text = string.Format("<span class='counted'>{1}</span>", id, c3 == 0 ? "" : c3.ToString("N4", Helper.NumberCulture).Replace(".0000", ""));
                }
                else
                {
                    e.Row.Cells[6].Text = string.Format("<input type='text' name='c1_{0}' value='{1}' />", id, c1 == 0 ? "" : c1.ToString("N4", Helper.NumberCulture).Replace(".0000", ""));
                    e.Row.Cells[8].Text = string.Format("<input type='text' name='c2_{0}' value='{1}' />", id, c2 == 0 ? "" : c2.ToString("N4", Helper.NumberCulture).Replace(".0000", ""));
                    e.Row.Cells[10].Text = string.Format("<input type='text' name='c3_{0}' value='{1}' />", id, c3 == 0 ? "" : c3.ToString("N4", Helper.NumberCulture).Replace(".0000", ""));
                }

                e.Row.Cells[7].Text = string.Format("<span class={0}>{1}</span>", d1 < 0 ? "neg" : "", d1);
                e.Row.Cells[9].Text = string.Format("<span class={0}>{1}</span>", d2 < 0 ? "neg" : "", d2);
                e.Row.Cells[11].Text = string.Format("<span class={0}>{1}</span>", d3 < 0 ? "neg" : "", d3);
            }
        }
    }
    protected void mGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        BindGrid();
    }

    protected string SortCol
    {
        get
        {
            return Helper.GetString(ViewState["SortCol"], null);
        }
        set
        {
            ViewState["SortCol"] = value;
        }
    }
    protected string SortOrder
    {
        get
        {
            return Helper.GetString(ViewState["SortOrder"], "asc");
        }
        set
        {
            ViewState["SortOrder"] = value;
        }
    }
    protected void doShow_Click(object sender, EventArgs e)
    {
        doExcel.Enabled = true;
        BindGrid();
        voucherDetailPanel.Visible = true;

        //mSumDue.Text = sumDue.ToString("N0");
        //mSumOwe.Text = sumOwe.ToString("N0");
        //mSumUnit.Text = sumUnit.ToString("N0");
    }
    protected void doExcel_Click(object sender, EventArgs e)
    {
            excelMode = true;
            BindGrid();
            Exporter.ExportExcel(Response, mGridTitle.Text, mGrid);
            excelMode = false;
            BindGrid();
    }
    protected void doSave_Click(object sender, EventArgs e)
    {
        using (DbProxy dbProxy = new DbProxy())
        {
            for (int i = 0; i < mGrid.Rows.Count; i++)
            {
                int id = Helper.GetInt(mGrid.Rows[i].Cells[1].Text, Null.NullInteger);
                if (id == Null.NullInteger)
                    continue;
                decimal count1 = Helper.GetDecimal(Request.Form["c1_" + id], 0);
                decimal count2 = Helper.GetDecimal(Request.Form["c2_" + id], 0);
                decimal count3 = Helper.GetDecimal(Request.Form["c3_" + id], 0);
                string query = string.Format("UPDATE StoreHandlingItems SET Count1={0}, Count2={1}, Count3={2} WHERE ID = {3}", count1, count2, count3, id);
                dbProxy.RunQuery(query);
            }
        }
        BindGrid();
    }
    protected void doFinalize_Click(object sender, EventArgs e)
    {
        doSave_Click(sender, e);
        int countMode = Helper.GetInt(mCountMode.SelectedValue, Null.NullInteger);
        if (countMode == Null.NullInteger)
            return;

        using (DbProxy dbProxy = new DbProxy())
        {
            dbProxy.BeginTransaction();
            try
            {
                string query = string.Format(@"DECLARE @PurchaseID INT, @StoreDraftID INT;
EXEC StoreHandling2StoreDraft {0}, {1}, {2}
EXEC StoreHandling2Purchase {0}, {2}
UPDATE StoreHandling SET EndDate = GETDATE() WHERE ID = {0}
SELECT @PurchaseID = PurchaseID, @StoreDraftID = StoreDraftID FROM StoreHandling WHERE ID = {0}
EXEC CreatePurchaseDocumentForStoreHandling @PurchaseID
EXEC CreateStoreDraftDocumentForStoreHandling @StoreDraftID

"
                    , this.StoreHandlingID, SiteSettings.User.PersonnelID, countMode);
                if (!mCreateDocs.Checked)
                    query = string.Format(@"DECLARE @PurchaseID INT, @StoreDraftID INT;
EXEC StoreHandling2StoreDraft {0}, {1}, {2}
EXEC StoreHandling2Purchase {0}, {2}
UPDATE StoreHandling SET EndDate = GETDATE() WHERE ID = {0}
SELECT @PurchaseID = PurchaseID, @StoreDraftID = StoreDraftID FROM StoreHandling WHERE ID = {0}
"
                    , this.StoreHandlingID, SiteSettings.User.PersonnelID, countMode);
                if (dbProxy.RunQuery(query, 300)
                    <= 0)
                    throw new Exception(LangProvider.Instance["Failed"] + " :" + dbProxy.LastException == null ? "" : dbProxy.LastException.Message);
                dbProxy.CommitTransaction();
                BindGrid();
                ShowMessage(LangProvider.Instance["Success"], "success");

            }
            catch (Exception ex)
            {
                dbProxy.RollbackTransaction();
                ExceptionLog.Instance.Add(ex);
                ShowMessage(ex.Message);
            }
        }
    }
    private void ShowMessage(string message, string type = "danger")
    {
        if (type != "success" && type != "danger")
        {
            type = "danger";
        }

        msg.Text += message;
        msg.Visible = true;
        msg.CssClass += " alert-" + type;
    }
    protected int StoreHandlingID
    {
        get
        {
            return Helper.GetInt(ViewState["StoreHandlingID"], Null.NullInteger);
        }
        set
        {
            ViewState["StoreHandlingID"] = value;
        }
    }
    protected int StoreID
    {
        get
        {
            return Helper.GetInt(ViewState["StoreID"], Null.NullInteger);
        }
        set
        {
            ViewState["StoreID"] = value;
        }
    }
}