﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="Insurance.aspx.cs" Inherits="Insurance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">
            <div class="form-group">
                <asp:Label ID="Label2" runat="server" Text="Lang.Year" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"></asp:Label>
                <div class="col-xs-12 col-sm-8">
                    <asp:DropDownList ID="ddlYear" runat="server" PlaceHolder="YYYY" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" Text="Lang.Month" PlaceHolder="MM" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"></asp:Label>
                <div class="col-xs-12 col-sm-8">
                    <asp:DropDownList ID="ddlMonth" runat="server" CssClass="chosen-select chosen-rtl">
                        <asp:ListItem Value="01">Lang.Farvardin</asp:ListItem>
                        <asp:ListItem Value="02">Lang.Ordibehesht</asp:ListItem>
                        <asp:ListItem Value="03">Lang.Khordad</asp:ListItem>
                        <asp:ListItem Value="04">Lang.Tir</asp:ListItem>
                        <asp:ListItem Value="05">Lang.Mordad</asp:ListItem>
                        <asp:ListItem Value="06">Lang.Shahrivar</asp:ListItem>
                        <asp:ListItem Value="07">Lang.Mehr</asp:ListItem>
                        <asp:ListItem Value="08">Lang.Aban</asp:ListItem>
                        <asp:ListItem Value="09">Lang.Azar</asp:ListItem>
                        <asp:ListItem Value="10">Lang.Dey</asp:ListItem>
                        <asp:ListItem Value="11">Lang.Bahman</asp:ListItem>
                        <asp:ListItem Value="12">Lang.Esfand</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">
            <div class="form-group">
                <asp:Label ID="Label1" runat="server" Text="Lang.Ward" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"></asp:Label>
                <div class="col-xs-12 col-sm-8">
                    <asp:DropDownList ID="ddlSection" runat="server" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" Text="Lang.ListNo" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"></asp:Label>
                <div class="col-xs-12 col-sm-8">
                    <asp:TextBox ID="ListNo" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" Text="Lang.ListComment" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"></asp:Label>
                <div class="col-xs-12 col-sm-8">
                    <asp:TextBox ID="ListComment" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" Text="Lang.TXTFormat" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"></asp:Label>
                <div class="col-xs-12 col-sm-8">
                    <asp:CheckBox runat="server" ID="cbTxt"/>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">
            <div class="form-group">
                <asp:Label runat="server" Text="Lang.DBFFormat" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"></asp:Label>
                <div class="col-xs-12 col-sm-8">
                    <asp:CheckBox runat="server" ID="cbDbf"/>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">
            <div class="form-group">
                <div class="col-xs-12 col-sm-4">&nbsp;</div>
                <div class="col-xs-12 col-sm-8">
                    <div class="col-xs-12 col-sm-6 padding-0-right">
                        <asp:Button ID="CreateFile" runat="server" Text="Lang.CreateWageListFile" OnClick="CreateFile_Click" CssClass="btn btn-primary btn-sm btn-block" />
                    </div>
                    <div class="col-xs-12 col-sm-6 col-without-padding">
                        <input type="button" id="PrintInsurance" name="PrintSummary" value="PrintWageSummary" class="btn btn-primary btn-sm btn-block" />
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">
            <asp:Label ID="mFile" runat="server" Text="" Visible="false"></asp:Label>
        </div>
    </div>
    <script type="text/javascript">
        $("#PrintInsurance").click(function () {
            var year = $("#<%= ddlYear.ClientID %>").val();
            if (year == "") {
                alert(Lang.YearHasNotBeenEntered);
                return;
            }
            var month = $("#<%= ddlMonth.ClientID %>").val();
            var section = $("#<%=ddlSection.ClientID%>").val();
            if (section=="") {
                section = -1;
            }

            popup(Router.Generate("print-insurance", { params: { month: month, year: year, section: section }, query: { print: "true" } }));
        return false;
        });
        $(document).ready(function() {
            $("#bar").prepend(breadcrumb({ title: Lang.Insurance, grandParent: { title: Lang.MSalary, url: 'menu/salary.aspx' }, parent: { title: Lang.OutputFiles, url: 'menu/outputfiles.aspx' } }));
            setClassSalary();
            $("#PrintInsurance").attr('value', Lang[$('#PrintInsurance').val()]);
        });
    </script>
</asp:Content>

