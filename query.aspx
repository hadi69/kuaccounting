﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true"
    CodeFile="query.aspx.cs" Inherits="query" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <table dir="ltr" border="0" cellspacing="0" cellpadding="2">
        <tr>
            <td>
                sql:
            </td>
            <td>
                <asp:TextBox ID="mQuery" runat="server" TextMode="MultiLine" Width="1053px" Height="300px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Button ID="doUpdate" runat="server" Text="Update" OnClick="doUpdate_Click" />&nbsp;
                <asp:Button ID="doQuery" runat="server" Text="Query" OnClick="doQuery_Click" />&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label runat="server" ID="mMsg" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:GridView runat="server" ID="mGrid"></asp:GridView>
    <asp:GridView runat="server" ID="GridView1"></asp:GridView>
    <asp:GridView runat="server" ID="GridView2"></asp:GridView>
    <asp:GridView runat="server" ID="GridView3"></asp:GridView>
    <asp:GridView runat="server" ID="GridView4"></asp:GridView>
    <asp:GridView runat="server" ID="GridView5"></asp:GridView>
    <asp:GridView runat="server" ID="GridView6"></asp:GridView>
    <asp:GridView runat="server" ID="GridView7"></asp:GridView>
    <asp:GridView runat="server" ID="GridView8"></asp:GridView>
    <asp:GridView runat="server" ID="GridView9"></asp:GridView>
    <script type="text/javascript">
        $(function () {
            setClassManager();
        });
    </script>
</asp:Content>
