﻿var getCSS = function (prop, fromClass) {

    var $inspector = $("<div>").css('display', 'none').addClass(fromClass);
    $("body").append($inspector); // add to DOM, in order to read the CSS property
    try {
        return $inspector.css(prop);
    } finally {
        $inspector.remove(); // and remove from DOM
    }
};

/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2006, 2014 Klaus Hartl
 * Released under the MIT license
 */
(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD (Register as an anonymous module)
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // Node/CommonJS
        module.exports = factory(require('jquery'));
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {

    var pluses = /\+/g;

    function encode(s) {
        return config.raw ? s : encodeURIComponent(s);
    }

    function decode(s) {
        return config.raw ? s : decodeURIComponent(s);
    }

    function stringifyCookieValue(value) {
        return encode(config.json ? JSON.stringify(value) : String(value));
    }

    function parseCookieValue(s) {
        if (s.indexOf('"') === 0) {
            // This is a quoted cookie as according to RFC2068, unescape...
            s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
        }

        try {
            // Replace server-side written pluses with spaces.
            // If we can't decode the cookie, ignore it, it's unusable.
            // If we can't parse the cookie, ignore it, it's unusable.
            s = decodeURIComponent(s.replace(pluses, ' '));
            return config.json ? JSON.parse(s) : s;
        } catch (e) { }
    }

    function read(s, converter) {
        var value = config.raw ? s : parseCookieValue(s);
        return $.isFunction(converter) ? converter(value) : value;
    }

    var config = $.cookie = function (key, value, options) {

        // Write

        if (arguments.length > 1 && !$.isFunction(value)) {
            options = $.extend({}, config.defaults, options);

            if (typeof options.expires === 'number') {
                var days = options.expires, t = options.expires = new Date();
                t.setMilliseconds(t.getMilliseconds() + days * 864e+5);
            }

            return (document.cookie = [
				encode(key), '=', stringifyCookieValue(value),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path ? '; path=' + options.path : '',
				options.domain ? '; domain=' + options.domain : '',
				options.secure ? '; secure' : ''
            ].join(''));
        }

        // Read

        var result = key ? undefined : {},
			// To prevent the for loop in the first place assign an empty array
			// in case there are no cookies at all. Also prevents odd result when
			// calling $.cookie().
			cookies = document.cookie ? document.cookie.split('; ') : [],
			i = 0,
			l = cookies.length;

        for (; i < l; i++) {
            var parts = cookies[i].split('='),
				name = decode(parts.shift()),
				cookie = parts.join('=');

            if (key === name) {
                // If second argument (value) is a function it's a converter...
                result = read(cookie, value);
                break;
            }

            // Prevent storing a cookie that we couldn't decode.
            if (!key && (cookie = read(cookie)) !== undefined) {
                result[name] = cookie;
            }
        }

        return result;
    };

    config.defaults = {};

    $.removeCookie = function (key, options) {
        // Must not alter options, thus extending a fresh object...
        $.cookie(key, '', $.extend({}, options, { expires: -1 }));
        return !$.cookie(key);
    };

}));

/*
* Flexigrid for jQuery -  v1.1
*
* Copyright (c) 2008 Paulo P. Marinas (code.google.com/p/flexigrid/)
* Dual licensed under the MIT or GPL Version 2 licenses.
* http://jquery.org/license
*
*/
(function ($) {
    $.addFlex = function (t, p) {
        if (t.grid) return false; //return if already exist
        p = $.extend({ //apply default properties
            height: gridHeight, //default height
            width: 'auto', //auto width
            striped: true, //apply odd even stripes
            novstripe: false,
            minwidth: 30, //min width of columns
            minheight: 80, //min height of columns
            resizable: true, //allow table resizing
            url: false, //URL if using data from AJAX
            method: 'POST', //data sending method
            dataType: 'xml', //type of data for AJAX, either xml or json
            errormsg: Lang.ErrorWhileConnecting,
            usepager: false,
            nowrap: true,
            page: 1, //current page
            total: 1, //total pages
            useRp: true, //use the results per page select box
            rp: 15, //results per page
            rpOptions: [10, 15, 20, 30, 50], //allowed per-page values 
            title: false,
            pagestat: Lang.Display + ' {from} ' + Lang.To + ' {to} ' + Lang.From + ' {total} ' + Lang.Record,
            pagetext: Lang.Page,
            outof: Lang.From,
            findtext: Lang.Search,
            procmsg: Lang.ProccessingPleaseWait,
            query: '',
            qtype: '',
            nomsg: Lang.Empty,
            minColToggle: 1, //minimum allowed column to be hidden
            showToggleBtn: true, //show or hide column toggle popup
            hideOnSubmit: true,
            autoload: true,
            blockOpacity: 0.5,
            preProcess: false,
            onDragCol: false,
            onToggleCol: false,
            onChangeSort: false,
            onDoubleClick: false, // umed: raised on double click on row
            onSuccess: false,
            onError: false,
            onSubmit: false, //using a custom populate function
            onSelect: false, // umed: raised when selection changed
            cookies: true, // umed: save oder, width, hidden in cookies
            tableId: null, // umed: save oder, width, hidden in cookies
            colMove: true 
        }, p);
        $(t).addClass("awesome-table");
        $(t).addClass("condensed");
        $(t).show() //show if hidden
			.attr({
			    cellPadding: 0,
			    cellSpacing: 0,
			    border: 0
			}) //remove padding and spacing
			.removeAttr('width'); //remove width properties
        //create grid class
        var g = {
            hset: {},
            rePosDrag: function () {
                var cdleft = 0 - this.hDiv.scrollLeft;
                if (this.hDiv.scrollLeft > 0) cdleft -= Math.floor(p.cgwidth / 2);
                $(g.cDrag).css({
                    top: g.hDiv.offsetTop + 1
                });
                var cdpad = this.cdpad;
                $('div', g.cDrag).hide();
                $('thead tr:first th:visible', this.hDiv).each(function () {
                    var n = $('thead tr:first th:visible', g.hDiv).index(this);
                    var cdpos = parseInt($('div', this).width());
                    if (cdleft == 0) cdleft -= Math.floor(p.cgwidth / 2);
                    cdpos = cdpos + cdleft + cdpad;
                    if (isNaN(cdpos)) {
                        cdpos = 0;
                    }
                    $('div:eq(' + n + ')', g.cDrag).css({
                        'left': cdpos + 'px'
                    }).show();
                    cdleft = cdpos;
                });
            },
            fixHeight: function (newH) {
                newH = false;
                if (!newH) newH = $(g.bDiv).height();
                var hdHeight = $(this.hDiv).height();
                $('div', this.cDrag).each(
					function () {
					    $(this).height(newH + hdHeight);
					}
				);
                var nd = parseInt($(g.nDiv).height());
                if (nd > newH) $(g.nDiv).height(newH).width(200);
                else $(g.nDiv).height('auto').width('auto');
                $(g.block).css({
                    height: newH,
                    marginBottom: (newH * -1)
                });
                var hrH = g.bDiv.offsetTop + newH;
                if (p.height != 'auto' && p.resizable) hrH = g.vDiv.offsetTop;
                $(g.rDiv).css({
                    height: hrH
                });
            },
            dragStart: function (dragtype, e, obj) { //default drag function start
                if (dragtype == 'colresize') {//column resize
                    $(g.nDiv).hide();
                    $(g.nBtn).hide();
                    var n = $('div', this.cDrag).index(obj);
                    var ow = $('th:visible div:eq(' + n + ')', this.hDiv).width();
                    $(obj).addClass('dragging').siblings().hide();
                    $(obj).prev().addClass('dragging').show();
                    this.colresize = {
                        startX: e.pageX,
                        ol: parseInt(obj.style.left),
                        ow: ow,
                        n: n
                    };
                    $('body').css('cursor', 'col-resize');
                } else if (dragtype == 'vresize') {//table resize
                    var hgo = false;
                    $('body').css('cursor', 'row-resize');
                    if (obj) {
                        hgo = true;
                        $('body').css('cursor', 'col-resize');
                    }
                    this.vresize = {
                        h: p.height,
                        sy: e.pageY,
                        w: p.width,
                        sx: e.pageX,
                        hgo: hgo
                    };
                } else if (dragtype == 'colMove') {//column header drag
                    $(g.nDiv).hide();
                    $(g.nBtn).hide();
                    this.hset = $(this.hDiv).offset();
                    this.hset.right = this.hset.left + $('table', this.hDiv).width();
                    this.hset.bottom = this.hset.top + $('table', this.hDiv).height();
                    this.dcol = obj;
                    this.dcoln = $('th', this.hDiv).index(obj);
                    this.colCopy = document.createElement("div");
                    this.colCopy.className = "colCopy";
                    this.colCopy.innerHTML = obj.innerHTML;
                    if ($.browser.msie) {
                        this.colCopy.className = "colCopy ie";
                    }
                    $(this.colCopy).css({
                        position: 'absolute',
                        float: 'left',
                        display: 'none',
                        textAlign: obj.align
                    });
                    $('body').append(this.colCopy);
                    $(this.cDrag).hide();
                }
                $('body').noSelect();
            },
            dragMove: function (e) {
                if (this.colresize) {//column resize
                    var n = this.colresize.n;
                    var diff = e.pageX - this.colresize.startX;
                    var nleft = this.colresize.ol + diff;
                    var nw = this.colresize.ow + diff;
                    if (nw > p.minwidth) {
                        $('div:eq(' + n + ')', this.cDrag).css('left', nleft);
                        this.colresize.nw = nw;
                    }
                } else if (this.vresize) {//table resize
                    var v = this.vresize;
                    var y = e.pageY;
                    var diff = y - v.sy;
                    if (!p.defwidth) p.defwidth = p.width;
                    if (p.width != 'auto' && !p.nohresize && v.hgo) {
                        var x = e.pageX;
                        var xdiff = x - v.sx;
                        var newW = v.w + xdiff;
                        if (newW > p.defwidth) {
                            this.gDiv.style.width = newW + 'px';
                            p.width = newW;
                        }
                    }
                    var newH = v.h + diff;
                    if ((newH > p.minheight || p.height < p.minheight) && !v.hgo) {
                        this.bDiv.style.height = newH + 'px';
                        p.height = newH;
                        this.fixHeight(newH);
                    }
                    v = null;
                } else if (this.colCopy) {
                    $(this.dcol).addClass('thMove').removeClass('thOver');
                    if (e.pageX > this.hset.right || e.pageX < this.hset.left || e.pageY > this.hset.bottom || e.pageY < this.hset.top) {
                        //this.dragEnd();
                        $('body').css('cursor', 'move');
                    } else {
                        $('body').css('cursor', 'pointer');
                    }
                    $(this.colCopy).css({
                        top: e.pageY + 10,
                        left: e.pageX + 20,
                        display: 'block'
                    });
                }
            },
            dragEnd: function () {
                if (this.colresize) {
                    var n = this.colresize.n;
                    var nw = this.colresize.nw;
                    $('th:visible div:eq(' + n + ')', this.hDiv).css('width', nw);
                    $('tr', this.bDiv).each(
						function () {
						    $('td:visible div:eq(' + n + ')', this).css('width', nw);
						}
					);
                    this.hDiv.scrollLeft = this.bDiv.scrollLeft;
                    $('div:eq(' + n + ')', this.cDrag).siblings().show();
                    $('.dragging', this.cDrag).removeClass('dragging');
                    this.rePosDrag();
                    this.fixHeight();
                    this.colresize = false;
                    if (p.cookies) {
                        var name = p.colModel[n].name;		// Store the widths in the cookies
                        $.cookie(p.tableId + '_colw' + name, nw);
                    }
                } else if (this.vresize) {
                    this.vresize = false;
                } else if (this.colCopy) {
                    $(this.colCopy).remove();
                    if (this.dcolt != null) {
                        if (this.dcoln > this.dcolt) $('th:eq(' + this.dcolt + ')', this.hDiv).before(this.dcol);
                        else $('th:eq(' + this.dcolt + ')', this.hDiv).after(this.dcol);
                        this.switchCol(this.dcoln, this.dcolt);
                        $(this.cdropleft).remove();
                        $(this.cdropright).remove();
                        this.rePosDrag();
                        if (p.onDragCol) {
                            p.onDragCol(this.dcoln, this.dcolt);
                        }
                    }
                    this.dcol = null;
                    this.hset = null;
                    this.dcoln = null;
                    this.dcolt = null;
                    this.colCopy = null;
                    $('.thMove', this.hDiv).removeClass('thMove');
                    $(this.cDrag).show();
                }
                $('body').css('cursor', 'default');
                $('body').noSelect(false);
            },
            toggleCol: function (cid, visible) {
                var ncol = $("th[axis='col" + cid + "']", this.hDiv)[0];
                var n = $('thead th', g.hDiv).index(ncol);
                var cb = $('input[value=' + cid + ']', g.nDiv)[0];
                if (visible == null) {
                    visible = ncol.hidden;
                }
                if ($('input:checked', g.nDiv).length < p.minColToggle && !visible) {
                    return false;
                }
                if (visible) {
                    ncol.hidden = false;
                    $(ncol).show();
                    cb.checked = true;
                } else {
                    ncol.hidden = true;
                    $(ncol).hide();
                    cb.checked = false;
                }
                $('tbody tr', t).each(
					function () {
					    if (visible) {
					        $('td:eq(' + n + ')', this).show();
					    } else {
					        $('td:eq(' + n + ')', this).hide();
					    }
					}
				);
                this.rePosDrag();
                if (p.onToggleCol) {
                    p.onToggleCol(cid, visible);
                }
                if (p.cookies) {

                    var cm = p.colModel[cid];
                    var cookie_hide = p.tableId + '_colh' + cm.name;
                    $.cookie(cookie_hide, visible ? 0 : 1);
                }
                return visible;
            },
            switchCol: function (cdrag, cdrop) { //switch columns
                $('tbody tr', t).each(
					function () {
					    if (cdrag > cdrop) $('td:eq(' + cdrop + ')', this).before($('td:eq(' + cdrag + ')', this));
					    else $('td:eq(' + cdrop + ')', this).after($('td:eq(' + cdrag + ')', this));
					}
				);
                //switch order in nDiv
                if (cdrag > cdrop) {
                    $('tr:eq(' + cdrop + ')', this.nDiv).before($('tr:eq(' + cdrag + ')', this.nDiv));
                } else {
                    $('tr:eq(' + cdrop + ')', this.nDiv).after($('tr:eq(' + cdrag + ')', this.nDiv));
                }
                if ($.browser.msie && $.browser.version < 7.0) {
                    $('tr:eq(' + cdrop + ') input', this.nDiv)[0].checked = true;
                }
                this.hDiv.scrollLeft = this.bDiv.scrollLeft;
            },
            scroll: function () {
                this.hDiv.scrollLeft = this.bDiv.scrollLeft;
                this.rePosDrag();
            },
            addData: function (data) { //parse data
                if (p.dataType == 'json') {
                    data = $.extend({ rows: [], page: 0, total: 0 }, data);
                }
                if (p.preProcess) {
                    data = p.preProcess(data);
                }
                $('.pReload', this.pDiv).removeClass('loading');
                this.loading = false;
                if (!data) {
                    $('.pPageStat', this.pDiv).html(p.errormsg);
                    return false;
                }
                if (p.dataType == 'xml') {
                    p.total = +$('rows total', data).text();
                } else {
                    p.total = data.total;
                }
                if (p.total == 0) {
                    $('tr, a, td, div', t).unbind();
                    $(t).empty();
                    p.pages = 1;
                    p.page = 1;
                    this.buildpager();
                    $('.pPageStat', this.pDiv).html(p.nomsg);
                    return false;
                }
                p.pages = Math.ceil(p.total / p.rp);
                if (p.dataType == 'xml') {
                    p.page = +$('rows page', data).text();
                } else {
                    p.page = data.page;
                }
                this.buildpager();
                //build new body
                var tbody = document.createElement('tbody');
                if (p.dataType == 'json') {
                    $.each(data.rows, function (i, row) {
                        var tr = document.createElement('tr');
                        if (i % 2 && p.striped) {
                            tr.className = 'erow';
                        }
                        if (row.id) {
                            tr.id = 'row' + row.id;
                        }
                        $('thead tr:first th', g.hDiv).each( //add cell
							function () {
							    var td = document.createElement('td');
							    var idx = $(this).attr('axis').substr(3);
							    td.align = this.align;
							    // If the json elements aren't named (which is typical), use numeric order
							    if (typeof row.cell[idx] != "undefined") {
							        td.innerHTML = (row.cell[idx] != null) ? row.cell[idx] : ''; //null-check for Opera-browser
							    } else {
							        td.innerHTML = row.cell[p.colModel[idx].name];
							    }
							    $(td).attr('abbr', $(this).attr('abbr'));
							    $(tr).append(td);
							    td = null;
							}
						);
                        if ($('thead', this.gDiv).length < 1) {//handle if grid has no headers
                            for (idx = 0; idx < cell.length; idx++) {
                                var td = document.createElement('td');
                                // If the json elements aren't named (which is typical), use numeric order
                                if (typeof row.cell[idx] != "undefined") {
                                    td.innerHTML = (row.cell[idx] != null) ? row.cell[idx] : ''; //null-check for Opera-browser
                                } else {
                                    td.innerHTML = row.cell[p.colModel[idx].name];
                                }
                                $(tr).append(td);
                                td = null;
                            }
                        }
                        $(tbody).append(tr);
                        tr = null;
                    });
                } else if (p.dataType == 'xml') {
                    var i = 1,
                        j = 0,
                        rows = data.getElementsByTagName('row'),
                        length = rows.length,
                        that, row,
                        headerCells,
                        headerCellsLength,
                        headerCounter,
                        headerCell;
                    for (; j < length; j++) {
                        i++;
                        row = rows[j];
                        that = $(row);
                        var tr = document.createElement('tr');
                        if (i % 2 && p.striped) {
                            tr.className = 'erow';
                        }
                        var nid = that.attr('id');
                        if (nid) {
                            tr.id = 'row' + nid;
                        }
                        nid = null;
                        var robj = this;
                        headerCells = g.hDiv.getElementsByTagName("th"), headerCellsLength = headerCells.length;
                        for (headerCounter = 0; headerCounter < headerCellsLength; headerCounter++) {
                            headerCell = headerCells[headerCounter];
                            var td = document.createElement('td'),
                                idx = headerCell.getAttribute('axis').substr(3),
                                css_class = headerCell.getAttribute('data-css-class'),
                                headerText = headerCell.getAttribute('data-th'),
                                innerHtml = $("cell:eq(" + idx + ")", row).text();
                            if (css_class !== (void 0) && css_class != null) {
                                td.className += td.className.length > 0 ? ' ' : '';
                                td.className += css_class;
                            }
                            if (headerText !== (void 0) && headerText != null) {
                                td.setAttribute('data-th', headerText);
                            }
                            if (headerCell.getAttribute('align')) {
                                td.align = headerCell.getAttribute('align');
                            }
                            if (innerHtml.length > AppOptions.maxTextLength) {
                                td.title = innerHtml;
                                innerHtml = innerHtml.substr(0, AppOptions.maxTextLength - 4) + ' ...';
                            }
                            td.innerHTML = innerHtml;
                            td.setAttribute('abbr', headerCell.getAttribute('abbr'));
                            tr.appendChild(td);
                            td = null;
                        }
                        if ($('thead', this.gDiv).length < 1) {//handle if grid has no headers
                            $('cell', this).each(function () {
                                var td = document.createElement('td');
                                td.innerHTML = $(this).text();
                                $(tr).append(td);
                                td = null;
                            });
                        }
                        $(tbody).append(tr);
                        tr = null;
                        robj = null;
                    }
                }
                $('tr', t).unbind();
                $(t).empty();
                $(t).append(tbody);

                this.addCellProp();
                this.addRowProp();
                this.rePosDrag();
                tbody = null;
                data = null;
                i = null;

                // --------------------------------
                // umed: footer                
                (function () {
                    function getFooterValues() {
                        // sum, td style, div style, td class, div class
                        var vals = [];

                        var sel = $("tr", t);
                        if (sel.length > 0) {
                            for (var x = 0; x < sel.length; x++) {
                                var tds = $(sel[x]).children('td');
                                for (var i = 0; i < tds.length; i++) {
                                    if (!vals[i]) vals[i] = [0, $(tds[i]).attr('style'), $(tds[i]).find('div').attr('style'), $(tds[i]).attr('class'), , $(tds[i]).find('div').attr('class')];
                                    if (p.colModel[i].footer && p.colModel[i].footer == 'sum')
                                        vals[i][0] += toFloat($(tds[i]).find('div').text(), 0);
                                }
                            }
                        }

                        return vals;
                    }
                    // check if has any sum
                    var hasAny = false;
                    for (var i = 0; i < p.colModel.length; i++)
                        if (p.colModel[i].footer)
                        {
                            hasAny = true;
                            break;
                        }
                    if (!hasAny)
                        return;
                    var summ = getFooterValues();
                    var footer = $('<tr id="trFooter"></tr>');
                    var tbody = $("tbody", t);
                    for (var i = 0; i < summ.length; i++) {
                        var div;
                        if (summ[i][0])
                            div = $('<div style="' + summ[i][2] + '" class="' + summ[i][4] + '">' + numberWithCommas(summ[i][0]) + '</div>');
                        else
                            div = $('<div style="' + summ[i][2] + '" class="' + summ[i][4] + '"></div>');

                        $(footer).append($('<td  style="' + summ[i][1] + '" class="' + summ[i][3] + '"></td>').append(div));
                    }

                    $(tbody).append(footer);
                })();
                // ----------------------------------
                $('td', t).bind('mouseenter', function () {
                    var $this = $(this);
                    if (this.offsetWidth < this.scrollWidth && !$this.attr('title')) {
                        $this.attr('title', $this.text());
                    }
                });
                if (p.onSuccess) {
                    p.onSuccess(this);
                }
                
                if (p.hideOnSubmit) {
                    $(g.block).remove();
                }
                this.hDiv.scrollLeft = this.bDiv.scrollLeft;
                if ($.browser.opera) {
                    $(t).css('visibility', 'visible');
                }
                // Trigger an event and notify anyone who cares, that the grid has been populated!!
                $(document).trigger('grid:loaded', $(t));
            },
            changeSort: function (th) { //change sortorder
                if (this.loading) {
                    return true;
                }
                $(g.nDiv).hide();
                $(g.nBtn).hide();
                if (p.sortname == $(th).attr('abbr')) {
                    if (p.sortorder == 'asc') {
                        p.sortorder = 'desc';
                    } else {
                        p.sortorder = 'asc';
                    }
                }
                $(th).addClass('sorted').siblings().removeClass('sorted');
                $('.sdesc', this.hDiv).removeClass('sdesc');
                $('.sasc', this.hDiv).removeClass('sasc');
                $('div', th).addClass('s' + p.sortorder);
                p.sortname = $(th).attr('abbr');
                if (p.onChangeSort) {
                    p.onChangeSort(p.sortname, p.sortorder);
                } else {
                    this.populate();
                }
            },
            buildpager: function () { //rebuild pager based on new properties
                $('.pcontrol input', this.pDiv).val(p.page);
                $('.pcontrol span', this.pDiv).html(p.pages);
                var r1 = (p.page - 1) * p.rp + 1;
                var r2 = r1 + p.rp - 1;
                if (p.total < r2) {
                    r2 = p.total;
                }
                var stat = p.pagestat;
                stat = stat.replace(/{from}/, r1);
                stat = stat.replace(/{to}/, r2);
                stat = stat.replace(/{total}/, p.total);
                $('.pPageStat', this.pDiv).html(stat);
            },
            populate: function () { //get latest data
                if (p.rp < p.rpOptions[0])
                    p.rp = p.rpOptions[0];
                if (this.loading) {
                    return true;
                }
                if (p.onSubmit) {
                    var gh = p.onSubmit();
                    if (!gh) {
                        return false;
                    }
                }
                this.loading = true;
                if (!p.url) {
                    return false;
                }
                $('.pPageStat', this.pDiv).html(p.procmsg);
                $('.pReload', this.pDiv).addClass('loading');
                $(g.block).css({
                    top: g.bDiv.offsetTop
                });
                if (p.hideOnSubmit) {
                    $(this.gDiv).prepend(g.block);
                }
                if ($.browser.opera) {
                    $(t).css('visibility', 'hidden');
                }
                if (!p.newp) {
                    p.newp = 1;
                }
                if (p.page > p.pages) {
                    p.page = p.pages;
                }
                var param = [{
                    name: 'page',
                    value: p.newp
                }, {
                    name: 'rp',
                    value: p.rp
                }, {
                    name: 'sortname',
                    value: p.sortname
                }, {
                    name: 'sortorder',
                    value: p.sortorder
                }, {
                    name: 'query',
                    value: p.query
                }, {
                    name: 'qtype',
                    value: p.qtype
                }];
                if (p.params) {
                    for (var pi = 0; pi < p.params.length; pi++) {
                        param[param.length] = p.params[pi];
                    }
                }
                $.ajax({
                    type: p.method,
                    url: p.url,
                    data: param,
                    dataType: p.dataType,
                    success: function (data) {
                        g.addData(data);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        try {
                            if (p.onError) p.onError(XMLHttpRequest, textStatus, errorThrown);
                        } catch (e) { }
                    }
                });
            },
            doSearch: function () {
                p.query = $('input[name=q]', g.sDiv).val();
                p.qtype = $('select[name=qtype]', g.sDiv).val();
                p.newp = 1;
                this.populate();
            },
            changePage: function (ctype) { //change page
                if (this.loading) {
                    return true;
                }
                switch (ctype) {
                    case 'first':
                        p.newp = 1;
                        break;
                    case 'prev':
                        if (p.page > 1) {
                            p.newp = parseInt(p.page) - 1;
                        }
                        break;
                    case 'next':
                        if (p.page < p.pages) {
                            p.newp = parseInt(p.page) + 1;
                        }
                        break;
                    case 'last':
                        p.newp = p.pages;
                        break;
                    case 'input':
                        var nv = parseInt($('.pcontrol input', this.pDiv).val());
                        if (isNaN(nv)) {
                            nv = 1;
                        }
                        if (nv < 1) {
                            nv = 1;
                        } else if (nv > p.pages) {
                            nv = p.pages;
                        }
                        $('.pcontrol input', this.pDiv).val(nv);
                        p.newp = nv;
                        break;
                }
                if (p.newp == p.page) {
                    return false;
                }
                if (p.onChangePage) {
                    p.onChangePage(p.newp);
                } else {
                    this.populate();
                }
            },
            addCellProp: function () {
                var cells = g.bDiv.getElementsByTagName("td"),
                    cellsLength = cells.length,
                    currentCell,
                    cellCounter = 0,
                    tdDiv,
                    n,
                    innerHtml,
                    children,
                    allTh = Array.prototype.slice.call(g.hDiv.getElementsByTagName("th")),
                    pth;
                //$('tbody tr td', g.bDiv).each(function ()
                for (; cellCounter < cellsLength; cellCounter++) {
                    currentCell = cells[cellCounter], innerHtml = currentCell.innerHTML, children = Array.prototype.slice.call(currentCell.parentNode.children);
                    tdDiv = document.createElement('div');
                    n = children.indexOf(currentCell);
                    pth = allTh[n];
                    if (pth != null) {
                        if (p.sortname == pth.getAttribute('abbr') && p.sortname) {
                            currentCell.className += currentCell.className.length > 0 ? ' ' : '';
                            currentCell.className += 'sorted';
                        }
                        tdDiv.style.textAlign = pth.align || '';
                        tdDiv.width = pth.getElementsByTagName("div")[0].style.offsetWidth;
                        if (pth.hidden) {
                            currentCell.style.display = 'none';
                        }
                    }
                    if (p.nowrap == false) {
                        tdDiv.style.whiteSpace = 'normal';
                    }
                    if (currentCell.innerHTML == '') {
                        currentCell.innerHTML = '&nbsp;';
                    }
                    tdDiv.innerHTML = currentCell.innerHTML;
                    var prnt = currentCell.parentNode;
                    var pid = false;
                    if (prnt.id) {
                        pid = prnt.id.substr(3);
                    }
                    if (pth != null) {
                        if (pth.process) {
                            pth.process(tdDiv, pid, innerHtml);
                        }
                    }
                    currentCell.innerHTML = '';
                    currentCell.appendChild(tdDiv);
                    currentCell.removeAttribute('width'); // wrap content
                }
            },
            getCellDim: function (obj) {// get cell prop for editable event
                var ht = parseInt($(obj).height());
                var pht = parseInt($(obj).parent().height());
                var wt = parseInt(obj.style.width);
                var pwt = parseInt($(obj).parent().width());
                var top = obj.offsetParent.offsetTop;
                var left = obj.offsetParent.offsetLeft;
                var pdl = parseInt($(obj).css('paddingLeft'));
                var pdt = parseInt($(obj).css('paddingTop'));
                return {
                    ht: ht,
                    wt: wt,
                    top: top,
                    left: left,
                    pdl: pdl,
                    pdt: pdt,
                    pht: pht,
                    pwt: pwt
                };
            },
            addRowProp: function () {
                $('tbody tr', g.bDiv).each(function () {
                    $(this).click(function (e) {
                        var obj = (e.target || e.srcElement);
                        if (obj.href || obj.type) return true;
                        $(this).toggleClass('trSelected');
                        if (p.singleSelect) $(this).siblings().removeClass('trSelected');
                        // umed
                        if (p.onSelect) {
                            p.onSelect();
                        }
                    }).mousedown(function (e) {
                        if (e.shiftKey) {
                            $(this).toggleClass('trSelected');
                            g.multisel = true;
                            this.focus();
                            $(g.gDiv).noSelect();
                        }
                    }).mouseup(function () {
                        if (g.multisel) {
                            g.multisel = false;
                            $(g.gDiv).noSelect(false);
                        }
                    }).dblclick(function () { //umed
                        if (p.onDoubleClick) {
                            p.onDoubleClick(this, g, p);
                        }
                    }).hover(function (e) {
                        if (g.multisel) {
                            $(this).toggleClass('trSelected');
                        }
                    }, function () { });
                    if ($.browser.msie && $.browser.version < 7.0) {
                        $(this).hover(function () {
                            $(this).addClass('trOver');
                        }, function () {
                            $(this).removeClass('trOver');
                        });
                    }
                });
            },
            pager: 0
        };
        if (p.colModel) { //create model if any
            thead = document.createElement('thead');
            var tr = document.createElement('tr');

            if (!p.tableId)
                p.tableId = _Context.table;
            if (!p.tableId)
                p.tableId = "flexi";
            // ----------------------------
            // UMED
            var nfs = 1 + (parseInt(getCSS('font-size', 'flexigrid'), 0) - 11.0) / 20;
            for (var i = 0; i < p.colModel.length; i++) {
                var cm = p.colModel[i];
                var th = document.createElement('th');
                th.innerHTML = cm.display;
                if (cm.name && cm.sortable) {
                    $(th).attr('abbr', cm.sortname ? cm.sortname : cm.name);
                }
                $(th).attr('axis', 'col' + i);
                if (cm.align) {
                    th.align = cm.align;
                }
                if (cm.display) {
                    $(th).attr('data-th', cm.display);
                }
                if (cm.css_class) {
                    $(th).addClass(cm.css_class);
                    $(th).attr('data-css-class', cm.css_class);
                }
                if (cm.width) {
                    // UMED
                    //$(th).attr('width', cm.width);
                    $(th).attr('width', Math.round(parseInt(cm.width) * nfs));
                }
                if ($.cookie(p.tableId + '_col' + i))
                    $(th).attr('width', $.cookie(p.tableId + '_col' + i));
                if ($.cookies) {
                    var cookie_width = p.tableId + '_colw' + cm.name;		// Re-Store the widths in the cookies
                    if ($.cookie(cookie_width) != undefined) {
                        $(th).attr('width', cookie_width);
                    }
                }
                if ($(cm).attr('hide')) {
                    th.hidden = true;
                }
                else if (p.cookies) {
                    var cookie_hide = p.tableId + '_colh' + cm.name;		// Re-Store the widths in the cookies
                    if ($.cookie(cookie_hide) == '1')
                        th.hidden = true;
                    else if ($.cookie(cookie_hide) == '0')
                        th.hidden = false;
                }
                if (cm.process) {
                    th.process = cm.process;
                }
                $(tr).append(th);
            }
            $(thead).append(tr);
            $(t).prepend(thead);
        } // end if p.colmodel
        //init divs
        g.gDiv = document.createElement('div'); //create global container
        g.mDiv = document.createElement('div'); //create title container
        g.hDiv = document.createElement('div'); //create header container
        g.bDiv = document.createElement('div'); //create body container
        g.vDiv = document.createElement('div'); //create grip
        g.rDiv = document.createElement('div'); //create horizontal resizer
        g.cDrag = document.createElement('div'); //create column drag
        g.block = document.createElement('div'); //creat blocker
        g.nDiv = document.createElement('div'); //create column show/hide popup
        g.nBtn = document.createElement('div'); //create column show/hide button
        g.iDiv = document.createElement('div'); //create editable layer
        g.tDiv = document.createElement('div'); //create toolbar
        g.sDiv = document.createElement('div');
        g.pDiv = document.createElement('div'); //create pager container
        if (!p.usepager) {
            g.pDiv.style.display = 'none';
        }
        g.hTable = document.createElement('table');
        g.gDiv.className = 'flexigrid';
        if (p.width != 'auto') {
            g.gDiv.style.width = p.width + 'px';
        }
        //add conditional classes
        if ($.browser.msie) {
            $(g.gDiv).addClass('ie');
        }
        if (p.novstripe) {
            $(g.gDiv).addClass('novstripe');
        }
        $(t).before(g.gDiv);
        $(g.gDiv).append(t);
        //set toolbar
        if (p.buttons) {
            g.tDiv.className = 'tDiv';
            var tDiv2 = document.createElement('div');
            tDiv2.className = 'tDiv2';
            for (var i = 0; i < p.buttons.length; i++) {
                var btn = p.buttons[i];
                if (!btn.separator) {
                    var btnDiv = document.createElement('div');
                    btnDiv.className = 'fbutton';
                    btnDiv.innerHTML = "<div><span>" + btn.name + "</span></div>";
                    if (btn.bclass) $('span', btnDiv).addClass(btn.bclass).css({
                        paddingLeft: 20
                    });
                    btnDiv.onpress = btn.onpress;
                    btnDiv.onaccess = btn.onaccess; // umed: onaccess added
                    btnDiv.name = btn.name;
                    if (btn.onpress) {
                        $(btnDiv).click(function () {
                            // umed
                            if (typeof this.onaccess != 'undefined')
                                if (!this.onaccess) {
                                    displayError(Lang.AccessToThisPageIsProhibited);
                                    return;
                                }
                            this.onpress(this.name, g.gDiv);
                        });
                    }
                    $(tDiv2).append(btnDiv);
                    if ($.browser.msie && $.browser.version < 7.0) {
                        $(btnDiv).hover(function () {
                            $(this).addClass('fbOver');
                        }, function () {
                            $(this).removeClass('fbOver');
                        });
                    }
                } else {
                    $(tDiv2).append("<div class='btnseparator'></div>");
                }
            }
            $(g.tDiv).append(tDiv2);
            $(g.tDiv).append("<div style='clear:both'></div>");
            $(g.gDiv).prepend(g.tDiv);
        }
        g.hDiv.className = 'hDiv';
        $(t).before(g.hDiv);
        g.hTable.cellPadding = 0;
        g.hTable.cellSpacing = 0;
        $(g.hDiv).append('<div class="hDivBox"></div>');
        $('div', g.hDiv).append(g.hTable);
        var thead = $("thead:first", t).get(0);
        if (thead) $(g.hTable).append(thead);
        thead = null;
        if (!p.colmodel) var ci = 0;
        $('thead tr:first th', g.hDiv).each(function () {
            var thdiv = document.createElement('div');
            if ($(this).attr('abbr')) {
                $(this).click(function (e) {
                    var obj = (e.target || e.srcElement);
                    if (obj.href || obj.type) return true;
                    g.changeSort(this);
                });
                if ($(this).attr('abbr') == p.sortname) {
                    $(this).addClass('sorted');
                    thdiv.className = 's' + p.sortorder;
                }
            }
            if (this.hidden) {
                $(this).hide();
            }
            if (!p.colmodel) {
                $(this).attr('axis', 'col' + ci++);
            }
            $(thdiv).css({
                textAlign: this.align,
                width: this.width + 'px'
            });
            thdiv.innerHTML = this.innerHTML;
            $(this).empty().append(thdiv).removeAttr('width').mousedown(function (e) {
                g.dragStart('colMove', e, this);
            }).hover(function () {
                if (!g.colresize && !$(this).hasClass('thMove') && !g.colCopy) {
                    $(this).addClass('thOver');
                }
                if ($(this).attr('abbr') != p.sortname && !g.colCopy && !g.colresize && $(this).attr('abbr')) {
                    $('div', this).addClass('s' + p.sortorder);
                } else if ($(this).attr('abbr') == p.sortname && !g.colCopy && !g.colresize && $(this).attr('abbr')) {
                    var no = (p.sortorder == 'asc') ? 'desc' : 'asc';
                    $('div', this).removeClass('s' + p.sortorder).addClass('s' + no);
                }
                if (g.colCopy) {
                    var n = $('th', g.hDiv).index(this);
                    if (n == g.dcoln) {
                        return false;
                    }
                    if (n < g.dcoln) {
                        $(this).append(g.cdropleft);
                    } else {
                        $(this).append(g.cdropright);
                    }
                    g.dcolt = n;
                } else if (!g.colresize) {
                    var nv = $('th:visible', g.hDiv).index(this);
                    var onl = parseInt($('div:eq(' + nv + ')', g.cDrag).css('left'));
                    var nw = jQuery(g.nBtn).outerWidth();
                    var nl = onl - nw + Math.floor(p.cgwidth / 2);
                    //$(g.nDiv).hide();
                    $(g.nBtn).hide();
                    $(g.nBtn).css({
                        'left': nl,
                        top: g.hDiv.offsetTop
                    }).show();
                    var ndw = parseInt($(g.nDiv).width());
                    $(g.nDiv).css({
                        top: g.bDiv.offsetTop
                    });
                    if ((nl + ndw) > $(g.gDiv).width()) {
                        $(g.nDiv).css('left', onl - ndw + 1);
                    } else {
                        $(g.nDiv).css('left', nl);
                    }
                    if ($(this).hasClass('sorted')) {
                        $(g.nBtn).addClass('srtd');
                    } else {
                        $(g.nBtn).removeClass('srtd');
                    }
                }
            }, function () {
                $(this).removeClass('thOver');
                if ($(this).attr('abbr') != p.sortname) {
                    $('div', this).removeClass('s' + p.sortorder);
                } else if ($(this).attr('abbr') == p.sortname) {
                    var no = (p.sortorder == 'asc') ? 'desc' : 'asc';
                    $('div', this).addClass('s' + p.sortorder).removeClass('s' + no);
                }
                if (g.colCopy) {
                    $(g.cdropleft).remove();
                    $(g.cdropright).remove();
                    g.dcolt = null;
                }
            }); //wrap content
        });
        //set bDiv
        g.bDiv.className = 'bDiv';
        $(t).before(g.bDiv);
        $(g.bDiv).css({
            height: (p.height == 'auto') ? 'auto' : p.height + "px"
        }).scroll(function (e) {
            g.scroll()
        }).append(t);
        if (p.height == 'auto') {
            $('table', g.bDiv).addClass('autoht');
        }
        //add td & row properties
        g.addCellProp();
        g.addRowProp();
        //set cDrag
        var cdcol = $('thead tr:first th:first', g.hDiv).get(0);
        if (cdcol != null) {
            g.cDrag.className = 'cDrag';
            g.cdpad = 0;
            g.cdpad += (isNaN(parseInt($('div', cdcol).css('borderLeftWidth'))) ? 0 : parseInt($('div', cdcol).css('borderLeftWidth')));
            g.cdpad += (isNaN(parseInt($('div', cdcol).css('borderRightWidth'))) ? 0 : parseInt($('div', cdcol).css('borderRightWidth')));
            g.cdpad += (isNaN(parseInt($('div', cdcol).css('paddingLeft'))) ? 0 : parseInt($('div', cdcol).css('paddingLeft')));
            g.cdpad += (isNaN(parseInt($('div', cdcol).css('paddingRight'))) ? 0 : parseInt($('div', cdcol).css('paddingRight')));
            g.cdpad += (isNaN(parseInt($(cdcol).css('borderLeftWidth'))) ? 0 : parseInt($(cdcol).css('borderLeftWidth')));
            g.cdpad += (isNaN(parseInt($(cdcol).css('borderRightWidth'))) ? 0 : parseInt($(cdcol).css('borderRightWidth')));
            g.cdpad += (isNaN(parseInt($(cdcol).css('paddingLeft'))) ? 0 : parseInt($(cdcol).css('paddingLeft')));
            g.cdpad += (isNaN(parseInt($(cdcol).css('paddingRight'))) ? 0 : parseInt($(cdcol).css('paddingRight')));
            $(g.bDiv).before(g.cDrag);
            var cdheight = $(g.bDiv).height();
            var hdheight = $(g.hDiv).height();
            $(g.cDrag).css({
                top: -hdheight + 'px'
            });
            $('thead tr:first th', g.hDiv).each(function () {
                var cgDiv = document.createElement('div');
                $(g.cDrag).append(cgDiv);
                if (!p.cgwidth) {
                    p.cgwidth = $(cgDiv).width();
                }
                $(cgDiv).css({
                    height: cdheight + hdheight
                }).mousedown(function (e) {
                    g.dragStart('colresize', e, this);
                });
                if ($.browser.msie && $.browser.version < 7.0) {
                    g.fixHeight($(g.gDiv).height());
                    $(cgDiv).hover(function () {
                        g.fixHeight();
                        $(this).addClass('dragging')
                    }, function () {
                        if (!g.colresize) $(this).removeClass('dragging')
                    });
                }
            });
        }
        //add strip
        if (p.striped) {
            $('tbody tr:odd', g.bDiv).addClass('erow');
        }
        if (p.resizable && p.height != 'auto') {
            g.vDiv.className = 'vGrip';
            $(g.vDiv).mousedown(function (e) {
                g.dragStart('vresize', e)
            }).html('<span></span>');
            $(g.bDiv).after(g.vDiv);
        }
        if (p.resizable && p.width != 'auto' && !p.nohresize) {
            g.rDiv.className = 'hGrip';
            $(g.rDiv).mousedown(function (e) {
                g.dragStart('vresize', e, true);
            }).html('<span></span>').css('height', $(g.gDiv).height());
            if ($.browser.msie && $.browser.version < 7.0) {
                $(g.rDiv).hover(function () {
                    $(this).addClass('hgOver');
                }, function () {
                    $(this).removeClass('hgOver');
                });
            }
            $(g.gDiv).append(g.rDiv);
        }
        // add pager
        if (p.usepager) {
            g.pDiv.className = 'pDiv';
            g.pDiv.innerHTML = '<div class="pDiv2"></div>';
            $(g.bDiv).after(g.pDiv);
            var html = ' <div class="pGroup"> <div class="pFirst pButton"><span></span></div><div class="pPrev pButton"><span></span></div> </div> <div class="btnseparator"></div> <div class="pGroup"><span class="pcontrol">' + p.pagetext + ' <input type="text" size="4" value="1" /> ' + p.outof + ' <span> 1 </span></span></div> <div class="btnseparator"></div> <div class="pGroup"> <div class="pNext pButton"><span></span></div><div class="pLast pButton"><span></span></div> </div> <div class="btnseparator"></div> <div class="pGroup"> <div class="pReload pButton"><span></span></div> </div> <div class="btnseparator"></div> <div class="pGroup"><span class="pPageStat"></span></div>';
            $('div', g.pDiv).html(html);
            $('.pReload', g.pDiv).click(function () {
                g.populate();
            });
            $('.pFirst', g.pDiv).click(function () {
                g.changePage('first');
            });
            $('.pPrev', g.pDiv).click(function () {
                g.changePage('prev');
            });
            $('.pNext', g.pDiv).click(function () {
                g.changePage('next');
            });
            $('.pLast', g.pDiv).click(function () {
                g.changePage('last');
            });
            $('.pcontrol input', g.pDiv).keydown(function (e) {
                if (e.keyCode == 13) g.changePage('input');
            });
            if ($.browser.msie && $.browser.version < 7) $('.pButton', g.pDiv).hover(function () {
                $(this).addClass('pBtnOver');
            }, function () {
                $(this).removeClass('pBtnOver');
            });
            if (p.useRp) {
                var opt = '',
					sel = '';
                for (var nx = 0; nx < p.rpOptions.length; nx++) {
                    if (p.rp == p.rpOptions[nx]) sel = 'selected="selected"';
                    else sel = '';
                    opt += "<option value='" + p.rpOptions[nx] + "' " + sel + " >" + p.rpOptions[nx] + "&nbsp;&nbsp;</option>";
                }
                $('.pDiv2', g.pDiv).prepend("<div class='pGroup page-size'><select name='rp'>" + opt + "</select></div> <div class='btnseparator'></div>");
                $('select', g.pDiv).change(function () {
                    if (p.onRpChange) {
                        p.onRpChange(+this.value);
                    } else {
                        p.newp = 1;
                        p.rp = +this.value;
                        g.populate();
                    }
                });
            }
            //add search button
            if (p.searchitems) {
                $('.pDiv2', g.pDiv).prepend("<div class='pGroup'> <div class='pSearch pButton'><span></span></div> </div>  <div class='btnseparator'></div>");
                $('.pSearch', g.pDiv).click(function () {
                    $(g.sDiv).slideToggle('fast', function () {
                        $('.sDiv:visible input:first', g.gDiv).trigger('focus');
                    });
                });
                //add search box
                g.sDiv.className = 'sDiv';
                var sitems = p.searchitems;
                var sopt = '', sel = '';
                for (var s = 0; s < sitems.length; s++) {
                    if (p.qtype == '' && sitems[s].isdefault == true) {
                        p.qtype = sitems[s].name;
                        sel = 'selected="selected"';
                    } else {
                        sel = '';
                    }
                    sopt += "<option value='" + sitems[s].name + "' " + sel + " >" + sitems[s].display + "&nbsp;&nbsp;</option>";
                }
                if (p.qtype == '') {
                    p.qtype = sitems[0].name;
                }
                $(g.sDiv).append("<div class='sDiv2'>" + p.findtext +
						" <input type='text' value='" + p.query + "' size='30' name='q' class='qsbox' /> " +
						" <select name='qtype'>" + sopt + "</select></div>");
                //Split into separate selectors because of bug in jQuery 1.3.2
                $('input[name=q]', g.sDiv).keydown(function (e) {
                    if (e.keyCode == 13) {
                        g.doSearch();
                    }
                });
                $('select[name=qtype]', g.sDiv).keydown(function (e) {
                    if (e.keyCode == 13) {
                        g.doSearch();
                    }
                });
                $('input[value=Clear]', g.sDiv).click(function () {
                    $('input[name=q]', g.sDiv).val('');
                    p.query = '';
                    g.doSearch();
                });
                $(g.bDiv).after(g.sDiv);
            }
        }
        $(g.pDiv, g.sDiv).append("<div style='clear:both'></div>");
        // add title
        if (p.title) {
            g.mDiv.className = 'mDiv';
            g.mDiv.innerHTML = '<div class="ftitle">' + p.title + '</div>';
            $(g.gDiv).prepend(g.mDiv);
            if (p.showTableToggleBtn) {
                $(g.mDiv).append('<div class="ptogtitle" title="Minimize/Maximize Table"><span></span></div>');
                $('div.ptogtitle', g.mDiv).click(function () {
                    $(g.gDiv).toggleClass('hideBody');
                    $(this).toggleClass('vsble');
                });
            }
        }
        //setup cdrops
        g.cdropleft = document.createElement('span');
        g.cdropleft.className = 'cdropleft';
        g.cdropright = document.createElement('span');
        g.cdropright.className = 'cdropright';
        //add block
        g.block.className = 'gBlock';
        var gh = $(g.bDiv).height();
        var gtop = g.bDiv.offsetTop;
        $(g.block).css({
            width: g.bDiv.style.width,
            height: gh,
            background: 'white',
            position: 'relative',
            marginBottom: (gh * -1),
            zIndex: 1,
            top: gtop,
            left: '0px'
        });
        $(g.block).fadeTo(0, p.blockOpacity);
        // add column control
        if ($('th', g.hDiv).length) {
            g.nDiv.className = 'nDiv';
            g.nDiv.innerHTML = "<table cellpadding='0' cellspacing='0'><tbody></tbody></table>";
            $(g.nDiv).css({
                marginBottom: (gh * -1),
                display: 'none',
                top: gtop
            }).noSelect();
            var cn = 0;
            $('th div', g.hDiv).each(function () {
                var kcol = $("th[axis='col" + cn + "']", g.hDiv)[0];
                var chk = 'checked="checked"';
                if (kcol.style.display == 'none') {
                    chk = '';
                }
                $('tbody', g.nDiv).append('<tr><td class="ndcol1"><input type="checkbox" ' + chk + ' class="togCol" value="' + cn + '" /></td><td class="ndcol2">' + this.innerHTML + '</td></tr>');
                cn++;
            });
            if ($.browser.msie && $.browser.version < 7.0) $('tr', g.nDiv).hover(function () {
                $(this).addClass('ndcolover');
            }, function () {
                $(this).removeClass('ndcolover');
            });
            $('td.ndcol2', g.nDiv).click(function () {
                if ($('input:checked', g.nDiv).length <= p.minColToggle && $(this).prev().find('input')[0].checked) return false;
                return g.toggleCol($(this).prev().find('input').val());
            });
            $('input.togCol', g.nDiv).click(function () {
                if ($('input:checked', g.nDiv).length < p.minColToggle && this.checked == false) return false;
                $(this).parent().next().trigger('click');
            });
            $(g.gDiv).prepend(g.nDiv);
            $(g.nBtn).addClass('nBtn')
				.html('<div></div>')
				.attr('title', 'Hide/Show Columns')
				.click(function () {
				    $(g.nDiv).toggle();
				    return true;
				}
			);
            if (p.showToggleBtn) {
                $(g.gDiv).prepend(g.nBtn);
            }
        }
        // add date edit layer
        $(g.iDiv).addClass('iDiv').css({
            display: 'none'
        });
        $(g.bDiv).append(g.iDiv);
        // add flexigrid events
        $(g.bDiv).hover(function () {
            //$(g.nDiv).hide();
            $(g.nBtn).hide();
        }, function () {
            if (g.multisel) {
                g.multisel = false;
            }
        });
        $(g.gDiv).hover(function () { }, function () {
            //$(g.nDiv).hide();
            $(g.nBtn).hide();
        });
        //add document events
        $(document).mousemove(function (e) {
            g.dragMove(e)
        }).mouseup(function (e) {
            g.dragEnd()
        }).hover(function () { }, function () {
            g.dragEnd()
        });
        //browser adjustments
        if ($.browser.msie && $.browser.version < 7.0) {
            $('.hDiv,.bDiv,.mDiv,.pDiv,.vGrip,.tDiv, .sDiv', g.gDiv).css({
                width: '100%'
            });
            $(g.gDiv).addClass('ie6');
            if (p.width != 'auto') {
                $(g.gDiv).addClass('ie6fullwidthbug');
            }
        }
        g.rePosDrag();
        g.fixHeight();
        //make grid functions accessible
        t.p = p;
        t.grid = g;
        // load data
        if (p.url && p.autoload) {
            g.populate();
        }
        return t;
    };
    var docloaded = false;
    $(document).ready(function () {
        docloaded = true
    });
    $.fn.flexigrid = function (p) {
        return this.each(function () {
            if (!docloaded) {
                $(this).hide();
                var t = this;
                $(document).ready(function () {
                    $.addFlex(t, p);
                });
            } else {
                $.addFlex(this, p);
            }
        });
    }; //end flexigrid
    $.fn.flexReload = function (p) { // function to reload grid
        return this.each(function () {
            if (this.grid && this.p.url) this.grid.populate();
        });
    }; //end flexReload
    $.fn.flexUrl = function (p) { // by UMED
        var res = '';
        this.each(function () {
            if (this.grid && this.p.url) res = this.p.url;
        });
        return res;
    }; //end flexUrl
    $.fn.flexSortname = function (p) { // by UMED
        var res = '';
        this.each(function () {
            if (this.grid && this.p.sortname) res = this.p.sortname;
        });
        return res;
    }; //end flexSortname
    $.fn.flexSortorder = function (p) { // by UMED
        var res = '';
        this.each(function () {
            if (this.grid && this.p.sortorder) res = this.p.sortorder;
        });
        return res;
    }; //end flexSortorder
    $.fn.flexParams = function (p) { // by UMED
        var res = '';
        this.each(function () {
            if (this.grid && this.p.params) res = this.p.params;
        });
        return res;
    }; //end flexParams
    $.fn.flexColModel = function (p) { // by UMED
        var res = '';
        this.each(function () {
            if (this.grid && this.p.colModel) res = this.p.colModel;
        });
        return res;
    }; //end flexParams
    $.fn.flexOptions = function (p) { //function to update general options
        return this.each(function () {
            if (this.grid) $.extend(this.p, p);
        });
    }; //end flexOptions
    $.fn.flexToggleCol = function (cid, visible) { // function to reload grid
        return this.each(function () {
            if (this.grid) this.grid.toggleCol(cid, visible);
        });
    }; //end flexToggleCol
    $.fn.flexAddData = function (data) { // function to add data to grid
        return this.each(function () {
            if (this.grid) this.grid.addData(data);
        });
    };
    $.fn.noSelect = function (p) { //no select plugin by me :-)
        var prevent = (p == null) ? true : p;
        if (prevent) {
            return this.each(function () {
                if ($.browser.msie || $.browser.safari) $(this).bind('selectstart', function () {
                    return false;
                });
                else if ($.browser.mozilla) {
                    $(this).css('MozUserSelect', 'none');
                    $('body').trigger('focus');
                } else if ($.browser.opera) $(this).bind('mousedown', function () {
                    return false;
                });
                else $(this).attr('unselectable', 'on');
            });
        } else {
            return this.each(function () {
                if ($.browser.msie || $.browser.safari) $(this).unbind('selectstart');
                else if ($.browser.mozilla) $(this).css('MozUserSelect', 'inherit');
                else if ($.browser.opera) $(this).unbind('mousedown');
                else $(this).removeAttr('unselectable', 'on');
            });
        }
    }; //end noSelect
})(jQuery);


// ----------------------------------------------------------------------------------------
/*
* jQuery Impromptu
* By: Trent Richardson [http://trentrichardson.com]
* Version 3.1
* Last Modified: 3/30/2010
* 
* Copyright 2010 Trent Richardson
* Dual licensed under the MIT and GPL licenses.
* http://trentrichardson.com/Impromptu/GPL-LICENSE.txt
* http://trentrichardson.com/Impromptu/MIT-LICENSE.txt
* 
*/
(function ($) {
    $.prompt = function (message, options) {
        options = $.extend({}, $.prompt.defaults, options);
        $.prompt.currentPrefix = options.prefix;
        var ie6 = ($.browser.msie && $.browser.version < 7); var $body = $(document.body); var $window = $(window);
        options.classes = $.trim(options.classes);
        if (options.classes != '') options.classes = ' ' + options.classes;
        var msgbox = '<div class="' + options.prefix + 'box' + options.classes + '" id="' + options.prefix + 'box">';
        if (options.useiframe && (($('object, applet').length > 0) || ie6)) {
            msgbox += '<iframe src="javascript:false;" style="display:block;position:absolute;z-index:-1;" class="' + options.prefix + 'fade" id="' + options.prefix + 'fade"></iframe>';
        } else {
            if (ie6) { $('select').css('visibility', 'hidden'); }
            msgbox += '<div class="' + options.prefix + 'fade" id="' + options.prefix + 'fade"></div>';
        }
        msgbox += '<div class="' + options.prefix + '" id="' + options.prefix + '"><div class="' + options.prefix + 'container"><div class="';
        msgbox += options.prefix + 'close">X</div><div id="' + options.prefix + 'states"></div>';
        msgbox += '</div></div></div>'; var $jqib = $(msgbox).appendTo($body);
        var $jqi = $jqib.children('#' + options.prefix);
        var $jqif = $jqib.children('#' + options.prefix + 'fade');
        if (message.constructor == String) {
            message = { state0: { html: message, buttons: options.buttons, focus: options.focus, submit: options.submit} };
        }
        var states = "";
        $.each(message, function (statename, stateobj) {
            stateobj = $.extend({}, $.prompt.defaults.state, stateobj); message[statename] = stateobj; states += '<div id="' + options.prefix + '_state_' + statename + '" class="' + options.prefix + '_state" style="display:none;"><div class="' + options.prefix + 'message">' + stateobj.html + '</div><div class="' + options.prefix + 'buttons"><div class="container"><div class="row"><div class="col-xs-12 col-md-3 col-lg-2">&nbsp;</div><div class="col-xs-12 col-md-6 col-lg-8" id="form-buttons-container">';
            $.each(stateobj.buttons, function (k, v) {
                if (typeof v == 'object') {
                    // UMED: backColor added
                    var backColor = '';
                    if (v.color && v.color != 'FFFFFF')
                        backColor = " style='background-color:#" + v.color + " !important;'";
                    states +=
                        '<button class="btn btn-primary btn-sm block-xs" name="' + options.prefix + '_' + statename + '_button' + v.title.replace(/[^a-z0-9]+/gi, '')
                        + '" id="' + options.prefix + '_' + statename + '_button' + v.title.replace(/[^a-z0-9]+/gi, '')
                        + '" value="' + v.value + '" ' + backColor + '>' + v.title + '</button>';
                } else {
                    states += '<button class="btn btn-primary btn-sm block-xs ' + v.toString().toLowerCase() + '" name="' + options.prefix + '_' + statename + '_button' + replaceWhiteSpace(k)
                        + '" id="' + options.prefix + '_' + statename + '_button' + replaceWhiteSpace(k)
                        + '" value="' + v + '">' + k + '</button>';
                }
            });
            states += '</div><!-- col-xs-12 col-lg-8 -->';   // col-xs-12 col-lg-8
            states += '<div class="col-xs-12 visible-xs-block visible-sm-block"><div class="gap-5"></div></div>';
            states += '<div class="col-xs-12 col-md-3 col-lg-2" id="program-box-container"></div>';   // col-xs-12 col-lg-2
            states += '</div><!-- row -->';         // row
            states += '</div><!-- container -->';   // container
            states += '</div><!-- buttons -->';     // jqibuttons
            states += '</div><!-- state -->';       // jqi_state
        });
        $jqi.find('#' + options.prefix + 'states').html(states).children('.' + options.prefix + '_state:first').css('display', 'block');
        $jqi.find('.' + options.prefix + 'buttons:empty').css('display', 'none');
        $.each(message, function (statename, stateobj) {
            var $state = $jqi.find('#' + options.prefix + '_state_' + statename);
            $state.children('.' + options.prefix + 'buttons').find('button').click(function() {
                var msg = $state.children('.' + options.prefix + 'message');
                var clicked = stateobj.buttons[$(this).text()];
                if (clicked == undefined) {
                    for (var i in stateobj.buttons) {
                        if (stateobj.buttons[i].title == $(this).text()) {
                            clicked = stateobj.buttons[i].value;
                        }
                    }
                }
                if (typeof clicked == 'object') {
                    clicked = clicked.value;
                }
                var forminputs = {};
                $.each($jqi.find('#' + options.prefix + 'states :input').serializeArray(), function(i, obj) {
                    if (forminputs[obj.name] === undefined) {
                        forminputs[obj.name] = obj.value;
                    } else if (typeof forminputs[obj.name] == Array || typeof forminputs[obj.name] == 'object') {
                        forminputs[obj.name].push(obj.value);
                    } else {
                        forminputs[obj.name] = [forminputs[obj.name], obj.value];
                    }
                });
                var close = stateobj.submit(clicked, msg, forminputs);
                if (close === undefined || close) {
                    removePrompt(true, clicked, msg, forminputs);
                }
            });
            $state.find('.' + options.prefix + 'buttons button:eq(' + stateobj.focus + ')').removeClass('btn-primary').addClass('btn-success');
        });
        var ie6scroll = function () { $jqib.css({ top: $window.scrollTop() }); };
        var fadeClicked = function () { if (options.persistent) { var i = 0; $jqib.addClass(options.prefix + 'warning'); var intervalid = setInterval(function () { $jqib.toggleClass(options.prefix + 'warning'); if (i++ > 1) { clearInterval(intervalid); $jqib.removeClass(options.prefix + 'warning'); } }, 100); } else { removePrompt(); } }; var keyPressEventHandler = function (e) { var key = (window.event) ? event.keyCode : e.keyCode; if (key == 27) { fadeClicked(); } if (key == 9) { var $inputels = $(':input:enabled:visible', $jqib); var fwd = !e.shiftKey && e.target == $inputels[$inputels.length - 1]; var back = e.shiftKey && e.target == $inputels[0]; if (fwd || back) { setTimeout(function () { if (!$inputels) return; var el = $inputels[back === true ? $inputels.length - 1 : 0]; if (el) el.focus(); }, 10); return false; } } }; var positionPrompt = function () { $jqib.css({ position: (ie6) ? "absolute" : "absolute", height: $window.height(), width: "100%", top: (ie6) ? $window.scrollTop() : 0, left: 0, right: 0, bottom: 0 }); $jqif.css({ position: "absolute", height: $window.height(), width: "100%", top: 0, left: 0, right: 0, bottom: 0 }); $jqi.css({ position: "absolute", top: options.top, left: "50%", marginLeft: (($jqi.outerWidth() / 2) * -1) }); }; var stylePrompt = function () { $jqif.css({ zIndex: options.zIndex, display: "none", opacity: options.opacity }); $jqi.css({ zIndex: options.zIndex + 1, display: "none" }); $jqib.css({ zIndex: options.zIndex }); };
        var removePrompt = function (callCallback, clicked, msg, formvals) {
            $jqi.remove();
            if (ie6) { $body.unbind('scroll', ie6scroll); } $window.unbind('resize', positionPrompt); $jqif.fadeOut(options.overlayspeed, function () { $jqif.unbind('click', fadeClicked); $jqif.remove(); if (callCallback) { options.callback(clicked, msg, formvals); } $jqib.unbind('keypress', keyPressEventHandler); $jqib.remove(); if (ie6 && !options.useiframe) { $('select').css('visibility', 'visible'); } });
        };
        positionPrompt();
        stylePrompt();
        if (ie6) { $window.scroll(ie6scroll); }
        $jqif.click(fadeClicked); $window.resize(positionPrompt);
        $jqib.bind("keydown keypress", keyPressEventHandler);
        $jqi.find('.' + options.prefix + 'close').click(removePrompt);
        $jqif.fadeIn(options.overlayspeed);
        $jqi[options.show](options.promptspeed, options.loaded);
        $jqi.find('#' + options.prefix + 'states .' + options.prefix + '_state:first .' + options.prefix + 'defaultbutton').focus();
        if (options.timeout > 0) setTimeout($.prompt.close, options.timeout);
        return $jqib;
    };
    $.prompt.defaults = { prefix: 'jqi', classes: '', buttons: { Ok: true }, loaded: function () { }, submit: function () { return true; }, callback: function () { }, opacity: 0.6, zIndex: 999, overlayspeed: 'slow', promptspeed: 'fast', show: 'fadeIn', focus: 0, useiframe: false, top: "10%", persistent: true, timeout: 0, state: { html: '', buttons: { Ok: true }, focus: 0, submit: function () { return true; } } }; $.prompt.currentPrefix = $.prompt.defaults.prefix; $.prompt.setDefaults = function (o) { $.prompt.defaults = $.extend({}, $.prompt.defaults, o); }; $.prompt.setStateDefaults = function (o) { $.prompt.defaults.state = $.extend({}, $.prompt.defaults.state, o); }; $.prompt.getStateContent = function (state) { return $('#' + $.prompt.currentPrefix + '_state_' + state); }; $.prompt.getCurrentState = function () { return $('.' + $.prompt.currentPrefix + '_state:visible'); }; $.prompt.getCurrentStateName = function () { var stateid = $.prompt.getCurrentState().attr('id'); return stateid.replace($.prompt.currentPrefix + '_state_', ''); }; $.prompt.goToState = function (state, callback) { $('.' + $.prompt.currentPrefix + '_state').slideUp('slow'); $('#' + $.prompt.currentPrefix + '_state_' + state).slideDown('slow', function () { $(this).find('.' + $.prompt.currentPrefix + 'defaultbutton').focus(); if (typeof callback == 'function') callback(); }); }; $.prompt.nextState = function (callback) { var $next = $('.' + $.prompt.currentPrefix + '_state:visible').next(); $('.' + $.prompt.currentPrefix + '_state').slideUp('slow'); $next.slideDown('slow', function () { $next.find('.' + $.prompt.currentPrefix + 'defaultbutton').focus(); if (typeof callback == 'function') callback(); }); }; $.prompt.prevState = function (callback) { var $next = $('.' + $.prompt.currentPrefix + '_state:visible').prev(); $('.' + $.prompt.currentPrefix + '_state').slideUp('slow'); $next.slideDown('slow', function () { $next.find('.' + $.prompt.currentPrefix + 'defaultbutton').focus(); if (typeof callback == 'function') callback(); }); }; $.prompt.close = function () { $('#' + $.prompt.currentPrefix + 'box').fadeOut('fast', function () { $(this).remove(); }); }; $.fn.prompt = function (options) {
        if (options == undefined) options = {}; if (options.withDataAndEvents == undefined) options.withDataAndEvents = false;
        $.prompt($(this).clone(options.withDataAndEvents).html(), options);
    }
})(jQuery);

// ----------------------------------------------------------------------------------------
// Formly v1.0 by Daniel Raftery
//
// http://thrivingkings.com/formly
// http://twitter.com/ThrivingKings


(function ($) {

    $.fn.formly = function (options, callback) {
        // Default settings
        var settings =
			{
			    'theme': 'Base',
			    'onBlur': true
			};

        if (options)
        { $.extend(settings, options); }

        // Form name, very important!
        var formName = this.attr('id');
        if (!formName) {
            // If no name, make a random one
            formName = Math.ceil(Math.random() * 5000);
            this.attr('id', formName);
        }
        this.append('<div style="clear:both;"></div><div class="formlyAlerts"></div>');
        this.addClass('formlyWrapper-' + settings['theme']);
        if (this.attr('width'))
        { this.css('width', this.attr('width')); }

        if (this.attr('subtitle') || this.attr('title'))
        { this.prepend('<hr/>'); }
        if (this.attr('subtitle'))
        { this.prepend('<h2>' + this.attr('subtitle') + '</h2>'); }
        if (this.attr('title'))
        { this.prepend('<h1>' + this.attr('title') + '</h1>'); }

        this.children().each(function (index, item) {
            // Placeholder text
            if ($(item).attr('place')) {
                if ($(item).attr('type') == 'password') {
                    // Password fields with placeholders
                    var hID = 'pwPlace-' + $(item).attr('name');
                    $(item).after('<input type="text" id="' + hID + '" value="' + $(item).attr('place') + '" class="formlyPWPlaces" />');
                    $('#' + hID).css('color', '#bbb');
                    $(item).hide();
                    $('#' + hID).show();

                    // Focus and blur must be handled independently for variables sake
                    $('#' + hID).focus(function () {
                        $('#' + hID).hide();
                        $(item).show();
                        $(item).focus();
                    });
                    $(item).blur(function () {
                        if (!$(item).val()) {
                            $('#' + hID).show();
                            $(item).hide();
                        }
                    });
                }
                else {
                    $(item).val($(item).attr('place'));
                    $(item).css('color', '#bbb');
                }
            }

            $(item).blur(function () {
                // Placeholder text
                if (!$(item).val() || $(item).val() == $(item).attr('pre-fix')) {
                    if ($(item).attr('type') != 'password') {
                        $(item).val($(item).attr('place'));
                        $(item).css('color', '#bbb');
                    }
                }
                if ($(item).attr('pre-fix')) {
                    var originalVal = $(item).val();
                    var thePrefix = $(item).attr('pre-fix');
                    if (thePrefix.length == 1) {
                        if (originalVal.charAt(0) != thePrefix && $(item).val() != $(item).attr('place'))
                        { $(item).val(thePrefix + originalVal); }
                    }
                    else {
                        if (originalVal.indexOf(thePrefix) == -1 && $(item).val() != $(item).attr('place'))
                        { $(item).val(thePrefix + originalVal); }
                    }
                }
                if (settings['onBlur']) {
                    // Validation
                    if ($(item).attr('validate'))
                    { functions.validate(item); }
                    // Required
                    if ($(item).attr('require'))
                    { functions.require(item); }
                    // Match
                    if ($(item).attr('match'))
                    { functions.match(item); }
                }
            });

            // Focus actions
            $(item).focus(function () {
                // Placeholder
                if ($(item).attr('place')) {
                    if ($(item).val() == $(item).attr('place')) {
                        $(item).val('');
                        $(item).css('color', '');
                    }
                }
                // Prefixes
                if ($(item).attr('pre-fix') && !$(item).val()) {
                    $(item).val('');
                    $(item).val($(item).attr('pre-fix'));
                }
            });


            // Reset button
            $('#' + formName).find('input:reset').click(function (item) {
                item.preventDefault();
                $('#' + formName).find('input:text, input:password, input:checkbox, input:radio').each(function () {
                    $(this).css('border-color', '');

                    if ($(this).is(':checked'))
                    { $(this).attr('checked', false); }

                    if ($(this).attr('place')) {
                        if ($(this).attr('type') != 'password') {
                            $(this).val($(this).attr('place'));
                            $(this).css('color', '#bbb');
                        }
                        else {
                            if ($(this).hasClass('formlyPWPlaces')) {
                                $(this).show();
                                $(this).prev('input').hide();
                            }
                            else
                            { $(this).val(''); }
                        }
                    }
                    else {
                        if ($(this).hasClass('formlyPWPlaces')) {
                            $(this).show();
                            $(this).prev('input').hide();
                        }
                        else
                        { $(this).val(''); }
                    }
                });
                $('#' + formName).find('.formlyAlert').each(function () {
                    $(this).fadeOut(function ()
                    { $(this).remove() });
                });
            });
        });

        // Submit button
        this.submit(function (item) {
            var canSubmit = true;
            $(this).find('input').each(function () {
                if ($(this).attr('require')) {
                    if (!functions.require(this))
                    { canSubmit = false; }
                }
                if ($(this).attr('validate')) {
                    if (!functions.validate(this))
                    { canSubmit = false; }
                }
                // Match
                if ($(this).attr('match')) {
                    if (!functions.match(this))
                    { canSubmit = false; }
                }
            });
            if (!canSubmit)
            { item.preventDefault(); }
            else {
                if (callback) {  // Change this to .serializeArray() for JSON
                    item.preventDefault();
                    callback($(this).serialize());
                }
            }
        });

        var functions =
			{
			    validateString: function (type, string) {
			        // Validate email regular expression
			        if (type == 'email') {
			            var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
			            if (filter.test(string))
			            { return true; }
			            else
			            { return false; }
			        }
			        // Validate a simple URL regular expression
			        else if (type == 'http') {
			            var filter = /http:\/\/[A-Za-z0-9\.-]{3,}\.[A-Za-z]{2,3}/i
			            if (filter.test(string))
			            { return true; }
			            else
			            { return false; }
			        }
			    },
			    validate: function (item) {
			        var alertName = formName + $(item).attr('name');
			        if ($(item).attr('validate') == 'email') {
			            var valid = functions.validateString('email', $(item).val());
			            var validType = 'email address';
			        }
			        else if ($(item).attr('validate') == 'http') {
			            var valid = functions.validateString('http', $(item).val());
			            var validType = 'web address';
			        }
			        if (!valid) {
			            if (!$('#' + alertName).is(':visible')) {
			                $('#' + formName).find('.formlyAlerts').append('<div class="formlyInvalid formlyAlert" id="' + alertName + '">Invalid ' + validType + '</div>')
			                $('#' + alertName).fadeIn();
			            }
			            var borderColor = $('#' + alertName).css('background-color');
			            $(item).css('border-color', borderColor);
			            if ($(item).attr('type') == 'password')
			            { $(item).next('.formlyPWPlaces').css('border-color', borderColor); }
			            return false;
			        }
			        else {
			            $('#' + alertName).fadeOut(function ()
			            { $(this).remove() });
			            $(item).css('border-color', '');
			            $('.formlyPWPlaces').css('border-color', '');
			            return true;
			        }

			    },
			    require: function (item) {
			        var alertName = formName + $(item).attr('name');
			        var label = $(item).attr('label') + ' ';
			        if (label == 'undefined ') { label = ''; }
			        if (!$(item).val() || $(item).val() == $(item).attr('place')) {
			            if (!$('#' + alertName).is(':visible')) {
			                $('#' + formName).find('.formlyAlerts').append('<div class="formlyRequired formlyAlert" id="' + alertName + '">' + label + 'Required</div>')
			                $('#' + alertName).fadeIn();
			            }
			            var borderColor = $('#' + alertName).css('background-color');
			            $(item).css('border-color', borderColor);
			            if ($(item).attr('type') == 'password')
			            { $(item).next('.formlyPWPlaces').css('border-color', borderColor); }
			            return false;
			        }
			        else if ($(item).attr('type') == 'checkbox' && !$(item).is(':checked')) {
			            if (!$('#' + alertName).is(':visible')) {
			                $('#' + formName).find('.formlyAlerts').append('<div class="formlyRequired formlyAlert" id="' + alertName + '">' + label + 'Required</div>')
			                $('#' + alertName).fadeIn();
			                $(item).focus();
			            }
			            var borderColor = $('#' + alertName).css('background-color');
			            $(item).css('border-color', borderColor);
			            return false;
			        }
			        else {
			            $('#' + alertName).fadeOut(function ()
			            { $(this).remove() });
			            $(item).css('border-color', '');
			            $('.formlyPWPlaces').css('border-color', '');
			            return true;
			        }
			    },
			    match: function (item) {
			        var alertName = formName + $(item).attr('name');
			        var label = $(item).attr('label') + ' ';
			        if (label == 'undefined ') { label = ''; }
			        var toMatch = $(item).attr('match');
			        if ($(item).val() != $('#' + formName).find('input[name=' + toMatch + ']').val() || !$(item).val()) {
			            if (!$('#' + alertName).is(':visible')) {
			                $('#' + formName).find('.formlyAlerts').append('<div class="formlyInvalid formlyAlert" id="' + alertName + '">' + label + 'Does not match</div>')
			                $('#' + alertName).fadeIn();
			            }
			            var borderColor = $('#' + alertName).css('background-color');
			            $(item).css('border-color', borderColor);
			            if ($(item).attr('type') == 'password')
			            { $(item).next('.formlyPWPlaces').css('border-color', borderColor); }
			            return false;
			        }
			        else {
			            $('#' + alertName).fadeOut(function ()
			            { $(this).remove() });
			            $(item).css('border-color', '');
			            $('.formlyPWPlaces').css('border-color', '');
			            return true;
			        }
			    }
			};
    };

})(jQuery);

// ----------------------------------------------------------------------------------------
/*
* Autocomplete - jQuery plugin 1.1pre
*
* Copyright (c) 2007 Dylan Verheul, Dan G. Switzer, Anjesh Tuladhar, Jörn Zaefferer
*
* Dual licensed under the MIT and GPL licenses:
*   http://www.opensource.org/licenses/mit-license.php
*   http://www.gnu.org/licenses/gpl.html
*
* Revision: $Id: jquery.autocomplete.js 5785 2008-07-12 10:37:33Z joern.zaefferer $
*
*/

; (function ($) {

    $.fn.extend({
        autocomplete: function (urlOrData, options) {
            var isUrl = typeof urlOrData == "string";
            options = $.extend({}, $.Autocompleter.defaults, {
                url: isUrl ? urlOrData : null,
                data: isUrl ? null : urlOrData,
                delay: isUrl ? $.Autocompleter.defaults.delay : 10,
                max: options && !options.scroll ? 30 : 50
            }, options);

            // if highlight is set to false, replace it with a do-nothing function
            options.highlight = options.highlight || function (value) { return value; };

            // if the formatMatch option is not specified, then use formatItem for backwards compatibility
            options.formatMatch = options.formatMatch || options.formatItem;

            return this.each(function () {
                new $.Autocompleter(this, options);
            });
        },
        result: function (handler) {
            return this.bind("result", handler);
        },
        search: function (handler) {
            return this.trigger("search", [handler]);
        },
        flushCache: function () {
            return this.trigger("flushCache");
        },
        setOptions: function (options) {
            return this.trigger("setOptions", [options]);
        },
        unautocomplete: function () {
            return this.trigger("unautocomplete");
        },
        // UMED
        visible: function (handler) {
            return this.trigger("visible", handler);
        }
    });

    $.Autocompleter = function (input, options) {

        var KEY = {
            UP: 38,
            DOWN: 40,
            DEL: 46,
            TAB: 9,
            RETURN: 13,
            ESC: 27,
            COMMA: 188,
            PAGEUP: 33,
            PAGEDOWN: 34,
            BACKSPACE: 8
        };

        // Create $ object for input element
        var $input = $(input).attr("autocomplete", "off").addClass(options.inputClass);

        var timeout;
        var previousValue = "";
        var cache = $.Autocompleter.Cache(options);
        var hasFocus = 0;
        var lastKeyPressCode;
        var config = {
            mouseDownOnSelect: false
        };
        var select = $.Autocompleter.Select(options, input, selectCurrent, config);

        var blockSubmit;

        // prevent form submit in opera when selecting with return key
        $.browser.opera && $(input.form).bind("submit.autocomplete", function () {
            if (blockSubmit) {
                blockSubmit = false;
                return false;
            }
        });

        // only opera doesn't trigger keydown multiple times while pressed, others don't work with keypress at all
        $input.bind(($.browser.opera ? "keypress" : "keydown") + ".autocomplete", function (event) {
            // track last key pressed
            lastKeyPressCode = event.keyCode;
            switch (event.keyCode) {

                case KEY.UP:
                    event.preventDefault();
                    if (select.visible()) {
                        select.prev();
                    } else {
                        onChange(0, true);
                    }
                    break;

                case KEY.DOWN:
                    event.preventDefault();
                    if (select.visible()) {
                        select.next();
                    } else {
                        onChange(0, true);
                    }
                    break;

                case KEY.PAGEUP:
                    event.preventDefault();
                    if (select.visible()) {
                        select.pageUp();
                    } else {
                        onChange(0, true);
                    }
                    break;

                case KEY.PAGEDOWN:
                    event.preventDefault();
                    if (select.visible()) {
                        select.pageDown();
                    } else {
                        onChange(0, true);
                    }
                    break;

                // matches also semicolon 
                case options.multiple && $.trim(options.multipleSeparator) == "," && KEY.COMMA:
                case KEY.TAB:
                case KEY.RETURN:
                    if (selectCurrent()) {
                        // stop default to prevent a form submit, Opera needs special handling
                        event.preventDefault();
                        blockSubmit = true;
                        return false;
                    }
                    break;

                case KEY.ESC:
                    if (select.visible()) {
                        event.stopPropagation();
                        event.preventDefault();
                    }
                    select.hide();
                    break;

                default:
                    clearTimeout(timeout);
                    timeout = setTimeout(onChange, options.delay);
                    break;
            }
        }).focus(function () {
            // track whether the field has focus, we shouldn't process any
            // results if the field no longer has focus
            hasFocus++;
        }).blur(function () {
            hasFocus = 0;
            if (!config.mouseDownOnSelect) {
                hideResults();
            }
        }).click(function () {
            // show select when clicking in a focused field
            if (hasFocus++ > 1 && !select.visible()) {
                onChange(0, true);
            }
        }).bind("search", function () {
            // TODO why not just specifying both arguments?
            var fn = (arguments.length > 1) ? arguments[1] : null;
            function findValueCallback(q, data) {
                var result;
                if (data && data.length) {
                    for (var i = 0; i < data.length; i++) {
                        var _a = trimWords(data[i].result);
                        var _b = trimWords(q);
                        if (_a.length == 1 && _b.length == 1)
                        if (_a[0].toLowerCase() == _b[0].toLowerCase()) {
                            result = data[i];
                            break;
                        }
                        if (trimWords(data[i].result) == q) {
                            result = data[i];
                            break;
                        }
                    }
                }
                //if (!result)
                //{
                //    alert(q + " === " + data);
                //    if (data)
                //        alert(data.length);
                //    if (data && data.length) 
                //        alert(trimWords(data[0].result) + " => " + trimWords(q) + " => " + q + " => " + options.multipleSeparator);
                //}
                if (typeof fn == "function") fn(result);
                else $input.trigger("result", result && [result.data, result.value]);
            }
            $.each(trimWords($input.val()), function (i, value) {
                request(value, findValueCallback, findValueCallback);
            });
        }).bind("flushCache", function () {
            cache.flush();
        }).bind("setOptions", function () {
            $.extend(options, arguments[1]);
            // if we've updated the data, repopulate
            if ("data" in arguments[1])
                cache.populate();
        }).bind("unautocomplete", function () {
            select.unbind();
            $input.unbind();
            $(input.form).unbind(".autocomplete");
            $input.removeClass(options.inputClass);
        }).bind("visible", function () { // UMED
            var fn = (arguments.length > 1) ? arguments[1] : null;
            if (fn)
                fn(select.visible());
        });


        function selectCurrent() {
            var selected = select.selected();
            if (!selected)
                return false;

            var v = selected.result;
            previousValue = v;

            if (options.multiple) {
                var words = trimWords($input.val());
                if (words.length > 1) {
                    v = words.slice(0, words.length - 1).join(options.multipleSeparator) + options.multipleSeparator + v;
                }
                v += options.multipleSeparator;
            }

            $input.val(v);
            hideResultsNow();
            $input.trigger("result", [selected.data, selected.value]);
            return true;
        }

        function onChange(crap, skipPrevCheck) {
            if (lastKeyPressCode == KEY.DEL) {
                select.hide();
                return;
            }

            var currentValue = $input.val();

            if (!skipPrevCheck && currentValue == previousValue)
                return;

            previousValue = currentValue;

            currentValue = lastWord(currentValue);
            if (currentValue.length >= options.minChars) {
                $input.addClass(options.loadingClass);
                if (!options.matchCase)
                    currentValue = currentValue.toLowerCase();
                request(currentValue, receiveData, hideResultsNow);
            } else {
                stopLoading();
                select.hide();
            }
        };

        function trimWords(value) {
            if (!value) {
                return [""];
            }
            var words = value.split(options.multipleSeparator);
            var result = [];
            $.each(words, function (i, value) {
                if ($.trim(value))
                    result[i] = $.trim(value);
            });
            return result;
        }

        function lastWord(value) {
            if (!options.multiple)
                return value;
            var words = trimWords(value);
            return words[words.length - 1];
        }

        // fills in the input box w/the first match (assumed to be the best match)
        // q: the term entered
        // sValue: the first matching result
        function autoFill(q, sValue) {
            // autofill in the complete box w/the first match as long as the user hasn't entered in more data
            // if the last user key pressed was backspace, don't autofill
            if (options.autoFill && (lastWord($input.val()).toLowerCase() == q.toLowerCase()) && lastKeyPressCode != KEY.BACKSPACE) {
                // fill in the value (keep the case the user has typed)
                $input.val($input.val() + sValue.substring(lastWord(previousValue).length));
                // select the portion of the value not typed by the user (so the next character will erase)
                $.Autocompleter.Selection(input, previousValue.length, previousValue.length + sValue.length);
            }
        };

        function hideResults() {
            clearTimeout(timeout);
            timeout = setTimeout(hideResultsNow, 200);
        };

        function hideResultsNow() {
            var wasVisible = select.visible();
            select.hide();
            clearTimeout(timeout);
            stopLoading();
            $input.removeClass("autoerror");
            if (options.mustMatch) {
                // call search and run callback
                $input.search(
				function (result) {
				    // if no value found, clear the input box
				    if (!result) {
				        if (options.multiple) {
				            var words = trimWords($input.val()).slice(0, -1);
				            $input.val(words.join(options.multipleSeparator) + (words.length ? options.multipleSeparator : ""));
				        }
				        else{
                            $input.val("");
                            $input.addClass("autoerror");
                        }
				    }
				}
			);
            }
            if (wasVisible)
            // position cursor at end of input field
                $.Autocompleter.Selection(input, input.value.length, input.value.length);
        };

        function receiveData(q, data) {
            if (data && data.length && hasFocus) {
                stopLoading();
                select.display(data, q);
                autoFill(q, data[0].value);
                select.show();
            } else {
                hideResultsNow();
            }
        };

        function request(term, success, failure) {
            if (!options.matchCase)
                term = term.toLowerCase();
            var data = cache.load(term);
            // recieve the cached data
            if (data && data.length) {
                success(term, data);
                // if an AJAX url has been supplied, try loading the data now
            } else if ((typeof options.url == "string") && (options.url.length > 0)) {

                var extraParams = {
                    timestamp: +new Date()
                };
                $.each(options.extraParams, function (key, param) {
                    extraParams[key] = typeof param == "function" ? param() : param;
                });

                $.ajax({
                    // try to leverage ajaxQueue plugin to abort previous requests
                    mode: "abort",
                    // limit abortion to this input
                    port: "autocomplete" + input.name,
                    dataType: options.dataType,
                    url: options.url,
                    data: $.extend({
                        q: lastWord(term),
                        limit: options.max
                    }, extraParams),
                    success: function (data) {
                        var parsed = options.parse && options.parse(data) || parse(data);
                        // umed: no cahce :D
                        // it has problems with Numeric values less than 11, because the cache length is 10 and it interpates numbers 
                        // as indices rather than charhters :(
                        //cache.add(term, parsed);
                        success(term, parsed);
                    }
                });
            } else {
                // if we have a failure, we need to empty the list -- this prevents the the [TAB] key from selecting the last successful match
                select.emptyList();
                failure(term);
            }
        };

        function parse(data) {
            var parsed = [];
            var rows = data.split("\n");
            for (var i = 0; i < rows.length; i++) {
                var row = $.trim(rows[i]);
                if (row) {
                    row = row.split("|");
                    parsed[parsed.length] = {
                        data: row,
                        value: row[0],
                        result: options.formatResult && options.formatResult(row, row[0]) || row[0]
                    };
                }
            }
            return parsed;
        };

        function stopLoading() {
            $input.removeClass(options.loadingClass);
        };

    };

    $.Autocompleter.defaults = {
        inputClass: "ac_input",
        resultsClass: "ac_results",
        loadingClass: "ac_loading",
        minChars: 1,
        delay: 400,
        matchCase: false,
        matchSubset: true,
        matchContains: false,
        cacheLength: 10,
        max: 100,
        mustMatch: false,
        extraParams: {},
        selectFirst: true,
        formatItem: function (row) { return row[0]; },
        formatMatch: null,
        autoFill: false,
        width: 0,
        multiple: false,
        multipleSeparator: "::_ ",
        highlight: function (value, term) {
            return value; //.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + term.replace(/([\^\$\(\)\[\]\{\}\*\.\+\?\|\\])/gi, "\\$1") + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
        },
        scroll: true,
        scrollHeight: 180
    };

    $.Autocompleter.Cache = function (options) {

        var data = {};
        var length = 0;

        function matchSubset(s, sub) {
            if (!options.matchCase)
                s = s.toLowerCase();
            var i = s.indexOf(sub);
            if (options.matchContains == "word") {
                i = s.toLowerCase().search("\\b" + sub.toLowerCase());
            }
            if (i == -1) return false;
            return i == 0 || options.matchContains;
        };

        function add(q, value) {
            if (length > options.cacheLength) {
                flush();
            }
            if (!data[q]) {
                length++;
            }
            data[q] = value;
        }

        function populate() {
            if (!options.data) return false;
            // track the matches
            var stMatchSets = {},
			nullData = 0;

            // no url was specified, we need to adjust the cache length to make sure it fits the local data store
            if (!options.url) options.cacheLength = 1;

            // track all options for minChars = 0
            stMatchSets[""] = [];

            // loop through the array and create a lookup structure
            for (var i = 0, ol = options.data.length; i < ol; i++) {
                var rawValue = options.data[i];
                // if rawValue is a string, make an array otherwise just reference the array
                rawValue = (typeof rawValue == "string") ? [rawValue] : rawValue;

                var value = options.formatMatch(rawValue, i + 1, options.data.length);
                if (value === false)
                    continue;

                var firstChar = value.charAt(0).toLowerCase();
                // if no lookup array for this character exists, look it up now
                if (!stMatchSets[firstChar])
                    stMatchSets[firstChar] = [];

                // if the match is a string
                var row = {
                    value: value,
                    data: rawValue,
                    result: options.formatResult && options.formatResult(rawValue) || value
                };

                // push the current match into the set list
                stMatchSets[firstChar].push(row);

                // keep track of minChars zero items
                if (nullData++ < options.max) {
                    stMatchSets[""].push(row);
                }
            };

            // add the data items to the cache
            $.each(stMatchSets, function (i, value) {
                // increase the cache size
                options.cacheLength++;
                // add to the cache
                add(i, value);
            });
        }

        // populate any existing data
        setTimeout(populate, 25);

        function flush() {
            data = {};
            length = 0;
        }

        return {
            flush: flush,
            add: add,
            populate: populate,
            load: function (q) {
                if (!options.cacheLength || !length)
                    return null;
                /* 
                * if dealing w/local data and matchContains than we must make sure
                * to loop through all the data collections looking for matches
                */
                if (!options.url && options.matchContains) {
                    // track all matches
                    var csub = [];
                    // loop through all the data grids for matches
                    for (var k in data) {
                        // don't search through the stMatchSets[""] (minChars: 0) cache
                        // this prevents duplicates
                        if (k.length > 0) {
                            var c = data[k];
                            $.each(c, function (i, x) {
                                // if we've got a match, add it to the array
                                if (matchSubset(x.value, q)) {
                                    csub.push(x);
                                }
                            });
                        }
                    }
                    return csub;
                } else
                // if the exact item exists, use it
                    if (data[q]) {
                        return data[q];
                    } else
                        if (options.matchSubset) {
                            for (var i = q.length - 1; i >= options.minChars; i--) {
                                var c = data[q.substr(0, i)];
                                if (c) {
                                    var csub = [];
                                    $.each(c, function (i, x) {
                                        if (matchSubset(x.value, q)) {
                                            csub[csub.length] = x;
                                        }
                                    });
                                    return csub;
                                }
                            }
                        }
                return null;
            }
        };
    };

    $.Autocompleter.Select = function (options, input, select, config) {
        var CLASSES = {
            ACTIVE: "ac_over"
        };

        var listItems,
		active = -1,
		data,
		term = "",
		needsInit = true,
		element,
		list;

        // Create results
        function init() {
            if (!needsInit)
                return;
            element = $("<div/>")
		.hide()
		.addClass(options.resultsClass)
		.css("position", "absolute")
		.appendTo(document.body);

            list = $("<ul/>").appendTo(element).mouseover(function (event) {
                if (target(event).nodeName && target(event).nodeName.toUpperCase() == 'LI') {
                    active = $("li", list).removeClass(CLASSES.ACTIVE).index(target(event));
                    $(target(event)).addClass(CLASSES.ACTIVE);
                }
            }).click(function (event) {
                $(target(event)).addClass(CLASSES.ACTIVE);
                select();
                // TODO provide option to avoid setting focus again after selection? useful for cleanup-on-focus
                input.focus();
                return false;
            }).mousedown(function () {
                config.mouseDownOnSelect = true;
            }).mouseup(function () {
                config.mouseDownOnSelect = false;
            });

            if (options.width > 0)
                element.css("width", options.width);

            needsInit = false;
        }

        function target(event) {
            var element = event.target;
            while (element && element.tagName != "LI")
                element = element.parentNode;
            // more fun with IE, sometimes event.target is empty, just ignore it then
            if (!element)
                return [];
            return element;
        }

        function moveSelect(step) {
            listItems.slice(active, active + 1).removeClass(CLASSES.ACTIVE);
            movePosition(step);
            var activeItem = listItems.slice(active, active + 1).addClass(CLASSES.ACTIVE);
            if (options.scroll) {
                var offset = 0;
                listItems.slice(0, active).each(function () {
                    offset += this.offsetHeight;
                });
                if ((offset + activeItem[0].offsetHeight - list.scrollTop()) > list[0].clientHeight) {
                    list.scrollTop(offset + activeItem[0].offsetHeight - list.innerHeight());
                } else if (offset < list.scrollTop()) {
                    list.scrollTop(offset);
                }
            }
        };

        function movePosition(step) {
            active += step;
            if (active < 0) {
                active = listItems.size() - 1;
            } else if (active >= listItems.size()) {
                active = 0;
            }
        }

        function limitNumberOfItems(available) {
            return options.max && options.max < available
			? options.max
			: available;
        }

        function fillList() {
            list.empty();
            var max = limitNumberOfItems(data.length);
            for (var i = 0; i < max; i++) {
                if (!data[i])
                    continue;
                var formatted = options.formatItem(data[i].data, i + 1, max, data[i].value, term);
                if (formatted === false)
                    continue;
                var li = $("<li/>").html(options.highlight(formatted, term)).addClass(i % 2 == 0 ? "ac_even" : "ac_odd").appendTo(list)[0];
                $.data(li, "ac_data", data[i]);
            }
            listItems = list.find("li");
            if (options.selectFirst) {
                listItems.slice(0, 1).addClass(CLASSES.ACTIVE);
                active = 0;
            }
            // apply bgiframe if available
            if ($.fn.bgiframe)
                list.bgiframe();
        }

        return {
            display: function (d, q) {
                init();
                data = d;
                term = q;
                fillList();
            },
            next: function () {
                moveSelect(1);
            },
            prev: function () {
                moveSelect(-1);
            },
            pageUp: function () {
                if (active != 0 && active - 8 < 0) {
                    moveSelect(-active);
                } else {
                    moveSelect(-8);
                }
            },
            pageDown: function () {
                if (active != listItems.size() - 1 && active + 8 > listItems.size()) {
                    moveSelect(listItems.size() - 1 - active);
                } else {
                    moveSelect(8);
                }
            },
            hide: function () {
                element && element.hide();
                listItems && listItems.removeClass(CLASSES.ACTIVE);
                active = -1;
            },
            visible: function () {
                return element && element.is(":visible");
            },
            current: function () {
                return this.visible() && (listItems.filter("." + CLASSES.ACTIVE)[0] || options.selectFirst && listItems[0]);
            },
            show: function () {
                // By default, the right side of the result div is aligned with the right side of the input
                // But! if the left side of the result div is out of view, then we should align the left sides
                // The result is that, if the input is on the right half of the page, it will align right sides
                // and if it is on the left half of the page, then left sides will be aligned
                var offset = $(input).offset(),
                    inputWidth = $(input).outerWidth(),
                    elementWidth = element.width(),
                    elementLeft = offset.left + inputWidth - elementWidth - 2; // Substract 2 pixels for better alignment

                if (!isHorizontallyInViewport(elementLeft)) {
                    elementLeft = offset.left;
                }
                if (options.above)
                    element.css({
                        bottom: $(window).height() - offset.top, // + input.offsetHeight,
                        left: elementLeft
                    }).show();
                else
                    element.css({
                        top: offset.top + input.offsetHeight,
                        left: elementLeft
                    }).show();
                if (options.scroll) {
                    list.scrollTop(0);
                    list.css({
                        maxHeight: options.scrollHeight,
                        overflow: 'auto'
                    });

                    if ($.browser.msie && typeof document.body.style.maxHeight === "undefined") {
                        var listHeight = 0;
                        listItems.each(function () {
                            listHeight += this.offsetHeight;
                        });
                        var scrollbarsVisible = listHeight > options.scrollHeight;
                        list.css('height', scrollbarsVisible ? options.scrollHeight : listHeight);
                        if (!scrollbarsVisible) {
                            // IE doesn't recalculate width when scrollbar disappears
                            listItems.width(list.width() - parseInt(listItems.css("padding-left")) - parseInt(listItems.css("padding-right")));
                        }
                    }

                }
            },
            selected: function () {
                var selected = listItems && listItems.filter("." + CLASSES.ACTIVE).removeClass(CLASSES.ACTIVE);
                return selected && selected.length && $.data(selected[0], "ac_data");
            },
            emptyList: function () {
                list && list.empty();
            },
            unbind: function () {
                element && element.remove();
            }
        };
    };

    $.Autocompleter.Selection = function (field, start, end) {
        if (field.createTextRange) {
            var selRange = field.createTextRange();
            selRange.collapse(true);
            selRange.moveStart("character", start);
            selRange.moveEnd("character", end);
            selRange.select();
        } else if (field.setSelectionRange) {
            field.setSelectionRange(start, end);
        } else {
            if (field.selectionStart) {
                field.selectionStart = start;
                field.selectionEnd = end;
            }
        }
        field.focus();
    };

})(jQuery);

//--------------------------------------------------------------
/*!
* jQuery Display Message Plugin
*
* Copyright 2010, Andrey Voev
* http://www.andreyvoev.com
*
* Date: Fri Dec 12 16:12 2010 -0800
*/

(function ($) {

    $.fn.displayMessage = function (options) {

        // Default configuration properties.
        var defaults = {
            message: 'Error message',
            background: '#111111',
            color: '#FFFFFF',
            speed: 'fast',
            skin: 'plain',
            position: 'relative', // relative, absolute, fixed
            autohide: false
        }

        var options = $.extend(defaults, options);
        $(this).slideUp('fast');
        $(this).removeClass().empty();
        return this.each(function () {

            var sticky = (options.sticky == false) ? 'relative' : 'absolute';
            $(this).addClass('messagebar-skin-' + options.skin + '_bar').css('position', options.position).css('background-color', options.background);
            $(this).append('<div class="messagebar-skin-' + options.skin + '_inner"><span class="messagebar-skin-' + options.skin + '_text"></span><a href="#" id="close" class="messagebar-skin-' + options.skin + '_close"></a></div>').css('color', options.color);
            $(this).find('span').html(options.message);

            $(this).slideDown(options.speed, function () {

                var parent = ($(this).attr('id')) ? "#" + $(this).attr('id') : "." + $(this).attr('class');
                var close_button = ($(this).find('a').attr('id')) ? "#" + $(this).find('a').attr('id') : "." + $(this).find('a').attr('class');

                if (options.autohide == true) {
                    $(this).delay(2400).slideUp('slow');
                }

                $(parent + ">div>" + close_button).bind("click", function (event) {
                    event.preventDefault();
                    $(parent + ">div>" + close_button).animate({ "opacity": "hide" }, function () {
                        $(parent + ">div>span").fadeOut("slow").html("");
                        $(parent + ">div>" + close_button).parent().parent().slideUp(options.speed);
                    });
                });

            });

        });

    };
})(jQuery);

//------------------------------------------------------------
/*
* Inline Form Validation Engine 2.1, jQuery plugin
*
* Copyright(c) 2010, Cedric Dugas
* http://www.position-absolute.com
*
* 2.0 Rewrite by Olivier Refalo
* http://www.crionics.com
*
* Form validation engine allowing custom regex rules to be added.
* Licensed under the MIT License
*/
(function ($) {

    var methods = {

        /**
        * Kind of the constructor, called before any action
        * @param {Map} user options
        */
        init: function (options) {
            var form = this;
            if (!form.data('jqv') || form.data('jqv') == null) {
                methods._saveOptions(form, options);

                // bind all formError elements to close on click
                $(".formError").live("click", function () {
                    $(this).fadeOut(150, function () {

                        // remove prompt once invisible
                        $(this).remove();
                    });
                });
            }
        },
        /**
        * Attachs jQuery.validationEngine to form.submit and field.blur events
        * Takes an optional params: a list of options
        * ie. jQuery("#formID1").validationEngine('attach', {promptPosition : "centerRight"});
        */
        attach: function (userOptions) {
            var form = this;
            var options;

            if (userOptions)
                options = methods._saveOptions(form, userOptions);
            else
                options = form.data('jqv');

            if (!options.binded) {
                if (options.bindMethod == "bind") {
                    // bind fields
                    form.find("[class*=validate]:not([type=checkbox])").bind(options.validationEventTrigger, methods._onFieldEvent);
                    form.find("[class*=validate][type=checkbox]").bind("click", methods._onFieldEvent);

                    // bind form.submit
                    form.bind("submit", methods._onSubmitEvent);
                } else if (options.bindMethod == "live") {
                    // bind fields with LIVE (for persistant state)
                    form.find("[class*=validate]:not([type=checkbox])").live(options.validationEventTrigger, methods._onFieldEvent);
                    form.find("[class*=validate][type=checkbox]").live("click", methods._onFieldEvent);

                    // bind form.submit
                    form.live("submit", methods._onSubmitEvent);
                }

                options.binded = true;
            }

        },
        /**
        * Unregisters any bindings that may point to jQuery.validaitonEngine
        */
        detach: function () {
            var form = this;
            var options = form.data('jqv');
            if (options.binded) {

                // unbind fields
                form.find("[class*=validate]").not("[type=checkbox]").unbind(options.validationEventTrigger, methods._onFieldEvent);
                form.find("[class*=validate][type=checkbox]").unbind("click", methods._onFieldEvent);
                // unbind form.submit
                form.unbind("submit", methods.onAjaxFormComplete);


                // unbind live fields (kill)
                form.find("[class*=validate]").not("[type=checkbox]").die(options.validationEventTrigger, methods._onFieldEvent);
                form.find("[class*=validate][type=checkbox]").die("click", methods._onFieldEvent);
                // unbind form.submit
                form.die("submit", methods.onAjaxFormComplete);

                form.removeData('jqv');
            }
        },
        /**
        * Validates the form fields, shows prompts accordingly.
        * Note: There is no ajax form validation with this method, only field ajax validation are evaluated
        *
        * @return true if the form validates, false if it fails
        */
        validate: function () {
            return methods._validateFields(this);
        },
        /**
        * Validates one field, shows prompt accordingly.
        * Note: There is no ajax form validation with this method, only field ajax validation are evaluated
        *
        * @return true if the form validates, false if it fails
        */
        validateField: function (el) {
            var options = $(this).data('jqv');
            return methods._validateField($(el), options);
        },
        /**
        * Validates the form fields, shows prompts accordingly.
        * Note: this methods performs fields and form ajax validations(if setup)
        *
        * @return true if the form validates, false if it fails, undefined if ajax is used for form validation
        */
        validateform: function () {
            return methods._onSubmitEvent.call(this);
        },
        /**
        * Displays a prompt on a element.
        * Note that the element needs an id!
        *
        * @param {String} promptText html text to display type
        * @param {String} type the type of bubble: 'pass' (green), 'load' (black) anything else (red)
        * @param {String} possible values topLeft, topRight, bottomLeft, centerRight, bottomRight
        */
        showPrompt: function (promptText, type, promptPosition, showArrow) {

            var form = this.closest('form');
            var options = form.data('jqv');
            // No option, take default one
            if (!options) options = methods._saveOptions(this, options);
            if (promptPosition)
                options.promptPosition = promptPosition;
            options.showArrow = showArrow == true;

            methods._showPrompt(this, promptText, type, false, options);
        },
        /**
        * Closes all error prompts on the page
        */
        hidePrompt: function () {
            var promptClass = "." + methods._getClassName($(this).attr("id")) + "formError"
            $(promptClass).fadeTo("fast", 0.3, function () {
                $(this).remove();
            });
        },
        /**
        * Closes form error prompts, CAN be invidual
        */
        hide: function () {
            if ($(this).is("form")) {
                var closingtag = "parentForm" + $(this).attr('id');
            } else {

                var closingtag = $(this).attr('id') + "formError"
            }
            $('.' + closingtag).fadeTo("fast", 0.3, function () {
                $(this).remove();
            });
        },
        /**
        * Closes all error prompts on the page
        */
        hideAll: function () {
            $('.formError').fadeTo("fast", 0.3, function () {
                $(this).remove();
            });
        },
        /**
        * Typically called when user exists a field using tab or a mouse click, triggers a field
        * validation
        */
        _onFieldEvent: function () {
            var field = $(this);
            var form = field.closest('form');
            var options = form.data('jqv');
            // validate the current field
            methods._validateField(field, options);
        },
        /**
        * Called when the form is submited, shows prompts accordingly
        *
        * @param {jqObject}
        *            form
        * @return false if form submission needs to be cancelled
        */
        _onSubmitEvent: function () {
            var form = $(this);
            var options = form.data('jqv');

            // validate each field (- skip field ajax validation, no necessary since we will perform an ajax form validation)
            var r = methods._validateFields(form, true);

            if (r && options.ajaxFormValidation) {
                methods._validateFormWithAjax(form, options);
                return false;
            }

            if (options.onValidationComplete) {
                options.onValidationComplete(form, r);
                return false;
            }
            return r;
        },

        /**
        * Return true if the ajax field validations passed so far
        * @param {Object} options
        * @return true, is all ajax validation passed so far (remember ajax is async)
        */
        _checkAjaxStatus: function (options) {
            var status = true;
            $.each(options.ajaxValidCache, function (key, value) {
                if (!value) {
                    status = false;
                    // break the each
                    return false;
                }
            });
            return status;
        },
        /**
        * Validates form fields, shows prompts accordingly
        *
        * @param {jqObject}
        *            form
        * @param {skipAjaxFieldValidation}
        *            boolean - when set to true, ajax field validation is skipped, typically used when the submit button is clicked
        *
        * @return true if form is valid, false if not, undefined if ajax form validation is done
        */
        _validateFields: function (form, skipAjaxValidation) {
            var options = form.data('jqv');

            // this variable is set to true if an error is found
            var errorFound = false;

            // Trigger hook, start validation
            form.trigger("jqv.form.validating")
            // first, evaluate status of non ajax fields
            form.find('[class*=validate]').not(':hidden').each(function () {
                var field = $(this);
                errorFound |= methods._validateField(field, options, skipAjaxValidation);
            });
            // second, check to see if all ajax calls completed ok
            // errorFound |= !methods._checkAjaxStatus(options);

            // thrird, check status and scroll the container accordingly
            form.trigger("jqv.form.result", [errorFound])

            if (errorFound) {

                if (options.scroll) {

                    // get the position of the first error, there should be at least one, no need to check this
                    //var destination = form.find(".formError:not('.greenPopup'):first").offset().top;

                    // look for the visually top prompt
                    var destination = Number.MAX_VALUE;

                    var lst = $(".formError:not('.greenPopup')");
                    for (var i = 0; i < lst.length; i++) {
                        var d = $(lst[i]).offset().top;
                        if (d < destination)
                            destination = d;
                    }

                    if (!options.isOverflown)
                        $("html:not(:animated),body:not(:animated)").animate({
                            scrollTop: destination
                        }, 1100);
                    else {
                        var overflowDIV = $(options.overflownDIV);
                        var scrollContainerScroll = overflowDIV.scrollTop();
                        var scrollContainerPos = -parseInt(overflowDIV.offset().top);

                        destination += scrollContainerScroll + scrollContainerPos - 5;
                        var scrollContainer = $(options.overflownDIV + ":not(:animated)");

                        scrollContainer.animate({
                            scrollTop: destination
                        }, 1100);
                    }
                }
                return false;
            }
            return true;
        },
        /**
        * This method is called to perform an ajax form validation.
        * During this process all the (field, value) pairs are sent to the server which returns a list of invalid fields or true
        *
        * @param {jqObject} form
        * @param {Map} options
        */
        _validateFormWithAjax: function (form, options) {

            var data = form.serialize();
            var url = (options.ajaxFormValidationURL) ? options.ajaxFormValidationURL : form.attr("action");
            $.ajax({
                type: "GET",
                url: url,
                cache: false,
                dataType: "json",
                data: data,
                form: form,
                methods: methods,
                options: options,
                beforeSend: function () {
                    return options.onBeforeAjaxFormValidation(form, options);
                },
                error: function (data, transport) {
                    methods._ajaxError(data, transport);
                },
                success: function (json) {

                    if (json !== true) {

                        // getting to this case doesn't necessary means that the form is invalid
                        // the server may return green or closing prompt actions
                        // this flag helps figuring it out
                        var errorInForm = false;
                        for (var i = 0; i < json.length; i++) {
                            var value = json[i];

                            var errorFieldId = value[0];
                            var errorField = $($("#" + errorFieldId)[0]);

                            // make sure we found the element
                            if (errorField.length == 1) {

                                // promptText or selector
                                var msg = value[2];
                                // if the field is valid
                                if (value[1] == true) {

                                    if (msg == "" || !msg) {
                                        // if for some reason, status==true and error="", just close the prompt
                                        methods._closePrompt(errorField);
                                    } else {
                                        // the field is valid, but we are displaying a green prompt
                                        if (options.allrules[msg]) {
                                            var txt = options.allrules[msg].alertTextOk;
                                            if (txt)
                                                msg = txt;
                                        }
                                        methods._showPrompt(errorField, msg, "pass", false, options, true);
                                    }

                                } else {
                                    // the field is invalid, show the red error prompt
                                    errorInForm |= true;
                                    if (options.allrules[msg]) {
                                        var txt = options.allrules[msg].alertText;
                                        if (txt)
                                            msg = txt;
                                    }
                                    methods._showPrompt(errorField, msg, "", false, options, true);
                                }
                            }
                        }
                        options.onAjaxFormComplete(!errorInForm, form, json, options);
                    } else
                        options.onAjaxFormComplete(true, form, "", options);
                }
            });

        },
        /**
        * Validates field, shows prompts accordingly
        *
        * @param {jqObject}
        *            field
        * @param {Array[String]}
        *            field's validation rules
        * @param {Map}
        *            user options
        * @return true if field is valid
        */
        _validateField: function (field, options, skipAjaxValidation) {
            if (!field.attr("id"))
                $.error("jQueryValidate: an ID attribute is required for this field: " + field.attr("name") + " class:" +
                field.attr("class"));

            var rulesParsing = field.attr('class');
            var getRules = /validate\[(.*)\]/.exec(rulesParsing);
            if (!getRules)
                return false;
            var str = getRules[1];
            var rules = str.split(/\[|,|\]/);

            // true if we ran the ajax validation, tells the logic to stop messing with prompts
            var isAjaxValidator = false;
            var fieldName = field.attr("name");
            var promptText = "";
            var required = false;
            options.isError = false;
            options.showArrow = true;
            optional = false;

            for (var i = 0; i < rules.length; i++) {

                var errorMsg = undefined;
                switch (rules[i]) {

                    case "optional":
                        optional = true;
                        break;
                    case "required":
                        required = true;
                        errorMsg = methods._required(field, rules, i, options);
                        break;
                    case "custom":
                        errorMsg = methods._customRegex(field, rules, i, options);
                        break;
                    case "ajax":
                        // ajax has its own prompts handling technique
                        if (!skipAjaxValidation) {
                            methods._ajax(field, rules, i, options);
                            isAjaxValidator = true;
                        }
                        break;
                    case "minSize":
                        errorMsg = methods._minSize(field, rules, i, options);
                        break;
                    case "maxSize":
                        errorMsg = methods._maxSize(field, rules, i, options);
                        break;
                    case "min":
                        errorMsg = methods._min(field, rules, i, options);
                        break;
                    case "max":
                        errorMsg = methods._max(field, rules, i, options);
                        break;
                    case "past":
                        errorMsg = methods._past(field, rules, i, options);
                        break;
                    case "future":
                        errorMsg = methods._future(field, rules, i, options);
                        break;
                    case "maxCheckbox":
                        errorMsg = methods._maxCheckbox(field, rules, i, options);
                        field = $($("input[name='" + fieldName + "']"));
                        break;
                    case "minCheckbox":
                        errorMsg = methods._minCheckbox(field, rules, i, options);
                        field = $($("input[name='" + fieldName + "']"));
                        break;
                    case "equals":
                        errorMsg = methods._equals(field, rules, i, options);
                        break;
                    case "funcCall":
                        errorMsg = methods._funcCall(field, rules, i, options);
                        break;

                    default:
                        //$.error("jQueryValidator rule not found"+rules[i]);
                }
                if (errorMsg !== undefined) {
                    promptText += errorMsg + "<br/>";
                    options.isError = true;

                }

            }
            // If the rules required is not added, an empty field is not validated
            if (!required) {
                if (field.val() == "") options.isError = false;
            }
            // Hack for radio/checkbox group button, the validation go into the
            // first radio/checkbox of the group
            var fieldType = field.attr("type");

            if ((fieldType == "radio" || fieldType == "checkbox") && $("input[name='" + fieldName + "']").size() > 1) {
                field = $($("input[name='" + fieldName + "'][type!=hidden]:first"));
                options.showArrow = false;
            }

            if (options.isError) {

                methods._showPrompt(field, promptText, "", false, options);
            } else {
                if (!isAjaxValidator) methods._closePrompt(field);
            }
            field.closest('form').trigger("jqv.field.error", [field, options.isError, promptText])
            return options.isError;
        },
        /**
        * Required validation
        *
        * @param {jqObject} field
        * @param {Array[String]} rules
        * @param {int} i rules index
        * @param {Map}
        *            user options
        * @return an error string if validation failed
        */
        _required: function (field, rules, i, options) {
            switch (field.attr("type")) {
                case "text":
                case "password":
                case "textarea":
                case "file":
                default:
                    if (!field.val())
                        return options.allrules[rules[i]].alertText;
                    break;
                case "radio":
                case "checkbox":
                    var name = field.attr("name");
                    if ($("input[name='" + name + "']:checked").size() == 0) {

                        if ($("input[name='" + name + "']").size() == 1)
                            return options.allrules[rules[i]].alertTextCheckboxe;
                        else
                            return options.allrules[rules[i]].alertTextCheckboxMultiple;
                    }
                    break;
                // required for <select>  
                case "select-one":
                    // added by paul@kinetek.net for select boxes, Thank you
                    if (!field.val())
                        return options.allrules[rules[i]].alertText;
                    break;
                case "select-multiple":
                    // added by paul@kinetek.net for select boxes, Thank you
                    if (!field.find("option:selected").val())
                        return options.allrules[rules[i]].alertText;
                    break;
            }
        },
        /**
        * Validate Regex rules
        *
        * @param {jqObject} field
        * @param {Array[String]} rules
        * @param {int} i rules index
        * @param {Map}
        *            user options
        * @return an error string if validation failed
        */
        _customRegex: function (field, rules, i, options) {
            var customRule = rules[i + 1];
            var rule = options.allrules[customRule];
            if (!rule) {
                alert("jqv:custom rule not found " + customRule);
                return;
            }

            var ex = rule.regex;
            if (!ex) {
                alert("jqv:custom regex not found " + customRule);
                return;
            }
            var pattern = new RegExp(ex);

            if (!pattern.test(field.attr('value')))
                return options.allrules[customRule].alertText;
        },
        /**
        * Validate custom function outside of the engine scope
        *
        * @param {jqObject} field
        * @param {Array[String]} rules
        * @param {int} i rules index
        * @param {Map}
        *            user options
        * @return an error string if validation failed
        */
        _funcCall: function (field, rules, i, options) {
            var functionName = rules[i + 1];
            var fn = window[functionName];
            if (typeof (fn) == 'function')
                return fn(field, rules, i, options);

        },
        /**
        * Field match
        *
        * @param {jqObject} field
        * @param {Array[String]} rules
        * @param {int} i rules index
        * @param {Map}
        *            user options
        * @return an error string if validation failed
        */
        _equals: function (field, rules, i, options) {
            var equalsField = rules[i + 1];

            if (field.attr('value') != $("#" + equalsField).attr('value'))
                return options.allrules.equals.alertText;
        },
        /**
        * Check the maximum size (in characters)
        *
        * @param {jqObject} field
        * @param {Array[String]} rules
        * @param {int} i rules index
        * @param {Map}
        *            user options
        * @return an error string if validation failed
        */
        _maxSize: function (field, rules, i, options) {
            var max = rules[i + 1];
            var len = field.attr('value').length;

            if (len > max) {
                var rule = options.allrules.maxSize;
                return rule.alertText + max + rule.alertText2;
            }
        },
        /**
        * Check the minimum size (in characters)
        *
        * @param {jqObject} field
        * @param {Array[String]} rules
        * @param {int} i rules index
        * @param {Map}
        *            user options
        * @return an error string if validation failed
        */
        _minSize: function (field, rules, i, options) {
            var min = rules[i + 1];
            var len = field.attr('value').length;

            if (len < min) {
                var rule = options.allrules.minSize;
                return rule.alertText + min + rule.alertText2;
            }
        },
        /**
        * Check number minimum value
        *
        * @param {jqObject} field
        * @param {Array[String]} rules
        * @param {int} i rules index
        * @param {Map}
        *            user options
        * @return an error string if validation failed
        */
        _min: function (field, rules, i, options) {
            var min = parseFloat(rules[i + 1]);
            var len = parseFloat(field.attr('value'));

            if (len < min) {
                var rule = options.allrules.min;
                if (rule.alertText2) return rule.alertText + min + rule.alertText2;
                return rule.alertText + min;
            }
        },
        /**
        * Check number maximum value
        *
        * @param {jqObject} field
        * @param {Array[String]} rules
        * @param {int} i rules index
        * @param {Map}
        *            user options
        * @return an error string if validation failed
        */
        _max: function (field, rules, i, options) {
            var max = parseFloat(rules[i + 1]);
            var len = parseFloat(field.attr('value'));

            if (len > max) {
                var rule = options.allrules.max;
                if (rule.alertText2) return rule.alertText + max + rule.alertText2;
                //orefalo: to review, also do the translations
                return rule.alertText + max;
            }
        },
        /**
        * Checks date is in the past
        *
        * @param {jqObject} field
        * @param {Array[String]} rules
        * @param {int} i rules index
        * @param {Map}
        *            user options
        * @return an error string if validation failed
        */
        _past: function (field, rules, i, options) {

            var p = rules[i + 1];
            var pdate = (p.toLowerCase() == "now") ? new Date() : methods._parseDate(p);
            var vdate = methods._parseDate(field.attr('value'));

            if (vdate > pdate) {
                var rule = options.allrules.past;
                if (rule.alertText2) return rule.alertText + methods._dateToString(pdate) + rule.alertText2;
                return rule.alertText + methods._dateToString(pdate);
            }
        },
        /**
        * Checks date is in the past
        *
        * @param {jqObject} field
        * @param {Array[String]} rules
        * @param {int} i rules index
        * @param {Map}
        *            user options
        * @return an error string if validation failed
        */
        _future: function (field, rules, i, options) {

            var p = rules[i + 1];
            var pdate = (p.toLowerCase() == "now") ? new Date() : methods._parseDate(p);
            var vdate = methods._parseDate(field.attr('value'));

            if (vdate < pdate) {
                var rule = options.allrules.future;
                if (rule.alertText2) return rule.alertText + methods._dateToString(pdate) + rule.alertText2;
                return rule.alertText + methods._dateToString(pdate);
            }
        },
        /**
        * Max number of checkbox selected
        *
        * @param {jqObject} field
        * @param {Array[String]} rules
        * @param {int} i rules index
        * @param {Map}
        *            user options
        * @return an error string if validation failed
        */
        _maxCheckbox: function (field, rules, i, options) {

            var nbCheck = rules[i + 1];
            var groupname = field.attr("name");
            var groupSize = $("input[name='" + groupname + "']:checked").size();
            if (groupSize > nbCheck) {
                options.showArrow = false;
                return options.allrules.maxCheckbox.alertText;
            }
        },
        /**
        * Min number of checkbox selected
        *
        * @param {jqObject} field
        * @param {Array[String]} rules
        * @param {int} i rules index
        * @param {Map}
        *            user options
        * @return an error string if validation failed
        */
        _minCheckbox: function (field, rules, i, options) {

            var nbCheck = rules[i + 1];
            var groupname = field.attr("name");
            var groupSize = $("input[name='" + groupname + "']:checked").size();
            if (groupSize < nbCheck) {
                options.showArrow = false;
                return options.allrules.minCheckbox.alertText + " " + nbCheck + " " +
                options.allrules.minCheckbox.alertText2;
            }
        },
        /**
        * Ajax field validation
        *
        * @param {jqObject} field
        * @param {Array[String]} rules
        * @param {int} i rules index
        * @param {Map}
        *            user options
        * @return nothing! the ajax validator handles the prompts itself
        */
        _ajax: function (field, rules, i, options) {


            var errorSelector = rules[i + 1];
            var rule = options.allrules[errorSelector];
            var extraData = rule.extraData;

            if (!extraData)
                extraData = "";

            if (!options.isError) {
                $.ajax({
                    type: "GET",
                    url: rule.url,
                    cache: false,
                    dataType: "json",
                    data: "fieldId=" + field.attr("id") + "&fieldValue=" + field.attr("value") + "&extraData=" + extraData,
                    field: field,
                    rule: rule,
                    methods: methods,
                    options: options,
                    beforeSend: function () {
                        // build the loading prompt
                        var loadingText = rule.alertTextLoad;
                        if (loadingText)
                            methods._showPrompt(field, loadingText, "load", true, options);
                    },
                    error: function (data, transport) {
                        methods._ajaxError(data, transport);
                    },
                    success: function (json) {

                        // asynchronously called on success, data is the json answer from the server
                        var errorFieldId = json[0];
                        var errorField = $($("#" + errorFieldId)[0]);
                        // make sure we found the element
                        if (errorField.length == 1) {
                            var status = json[1];
                            if (!status) {
                                // Houston we got a problem
                                options.ajaxValidCache[errorFieldId] = false;
                                options.isError = true;
                                var promptText = rule.alertText;
                                methods._showPrompt(errorField, promptText, "", true, options);
                            } else {
                                if (options.ajaxValidCache[errorFieldId] !== undefined)
                                    options.ajaxValidCache[errorFieldId] = true;

                                // see if we should display a green prompt
                                var alertTextOk = rule.alertTextOk;
                                if (alertTextOk)
                                    methods._showPrompt(errorField, alertTextOk, "pass", true, options);
                                else
                                    methods._closePrompt(errorField);
                            }
                        }
                    }
                });
            }
        },
        /**
        * Common method to handle ajax errors
        *
        * @param {Object} data
        * @param {Object} transport
        */
        _ajaxError: function (data, transport) {
            if (data.status == 0 && transport == null)
                alert("The page is not served from a server! ajax call failed");
            else if (typeof console != "undefined")
                console.log("Ajax error: " + data.status + " " + transport);
        },
        /**
        * date -> string
        *
        * @param {Object} date
        */
        _dateToString: function (date) {

            return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
        },
        /**
        * Parses an ISO date
        * @param {String} d
        */
        _parseDate: function (d) {

            var dateParts = d.split("-");
            if (dateParts !== d)
                dateParts = d.split("/");
            return new Date(dateParts[0], (dateParts[1] - 1), dateParts[2]);
        },
        /**
        * Builds or updates a prompt with the given information
        *
        * @param {jqObject} field
        * @param {String} promptText html text to display type
        * @param {String} type the type of bubble: 'pass' (green), 'load' (black) anything else (red)
        * @param {boolean} ajaxed - use to mark fields than being validated with ajax
        * @param {Map} options user options
        */
        _showPrompt: function (field, promptText, type, ajaxed, options, ajaxform) {
            var prompt = methods._getPrompt(field);
            // The ajax submit errors are not see has an error in the form,
            // When the form errors are returned, the engine see 2 bubbles, but those are ebing closed by the engine at the same time
            // Because no error was found befor submitting
            if (ajaxform) prompt = false;
            if (prompt)
                methods._updatePrompt(field, prompt, promptText, type, ajaxed, options);
            else
                methods._buildPrompt(field, promptText, type, ajaxed, options);
        },
        /**
        * Builds and shades a prompt for the given field.
        *
        * @param {jqObject} field
        * @param {String} promptText html text to display type
        * @param {String} type the type of bubble: 'pass' (green), 'load' (black) anything else (red)
        * @param {boolean} ajaxed - use to mark fields than being validated with ajax
        * @param {Map} options user options
        */
        _buildPrompt: function (field, promptText, type, ajaxed, options) {

            // create the prompt
            var prompt = $('<div>');
            prompt.addClass(methods._getClassName(field.attr("id")) + "formError");
            // add a class name to identify the parent form of the prompt
            if (field.is(":input")) prompt.addClass("parentForm" + methods._getClassName(field.parents('form').attr("id")));
            prompt.addClass("formError");

            switch (type) {
                case "pass":
                    prompt.addClass("greenPopup");
                    break;
                case "load":
                    prompt.addClass("blackPopup");
            }
            if (ajaxed)
                prompt.addClass("ajaxed");

            // create the prompt content
            var promptContent = $('<div>').addClass("formErrorContent").html(promptText).appendTo(prompt);
            // create the css arrow pointing at the field
            // note that there is no triangle on max-checkbox and radio
            if (options.showArrow) {
                var arrow = $('<div>').addClass("formErrorArrow");

                switch (options.promptPosition) {
                    case "bottomLeft":
                    case "bottomRight":
                        prompt.find(".formErrorContent").before(arrow);
                        arrow.addClass("formErrorArrowBottom").html('<div class="line1"><!-- --></div><div class="line2"><!-- --></div><div class="line3"><!-- --></div><div class="line4"><!-- --></div><div class="line5"><!-- --></div><div class="line6"><!-- --></div><div class="line7"><!-- --></div><div class="line8"><!-- --></div><div class="line9"><!-- --></div><div class="line10"><!-- --></div>');
                        break;
                    case "topLeft":
                    case "topRight":
                        arrow.html('<div class="line10"><!-- --></div><div class="line9"><!-- --></div><div class="line8"><!-- --></div><div class="line7"><!-- --></div><div class="line6"><!-- --></div><div class="line5"><!-- --></div><div class="line4"><!-- --></div><div class="line3"><!-- --></div><div class="line2"><!-- --></div><div class="line1"><!-- --></div>');
                        prompt.append(arrow);
                        break;
                }
            }

            //Cedric: Needed if a container is in position:relative
            // insert prompt in the form or in the overflown container?
            if (options.isOverflown)
                field.before(prompt);
            else
                $("body").append(prompt);

            var pos = methods._calculatePosition(field, prompt, options);
            prompt.css({
                "top": pos.callerTopPosition,
                "left": pos.callerleftPosition,
                "marginTop": pos.marginTopSize,
                "opacity": 0
            });

            return prompt.animate({
                "opacity": 0.87
            });

        },
        /**
        * Updates the prompt text field - the field for which the prompt
        * @param {jqObject} field
        * @param {String} promptText html text to display type
        * @param {String} type the type of bubble: 'pass' (green), 'load' (black) anything else (red)
        * @param {boolean} ajaxed - use to mark fields than being validated with ajax
        * @param {Map} options user options
        */
        _updatePrompt: function (field, prompt, promptText, type, ajaxed, options) {

            if (prompt) {
                if (type == "pass")
                    prompt.addClass("greenPopup");
                else
                    prompt.removeClass("greenPopup");

                if (type == "load")
                    prompt.addClass("blackPopup");
                else
                    prompt.removeClass("blackPopup");

                if (ajaxed)
                    prompt.addClass("ajaxed");
                else
                    prompt.removeClass("ajaxed");

                prompt.find(".formErrorContent").html(promptText);

                var pos = methods._calculatePosition(field, prompt, options);
                prompt.animate({
                    "top": pos.callerTopPosition,
                    "marginTop": pos.marginTopSize
                });
            }
        },
        /**
        * Closes the prompt associated with the given field
        *
        * @param {jqObject}
        *            field
        */
        _closePrompt: function (field) {

            var prompt = methods._getPrompt(field);
            if (prompt)
                prompt.fadeTo("fast", 0, function () {
                    prompt.remove();
                });
        },
        closePrompt: function (field) {
            return methods._closePrompt(field);
        },
        /**
        * Returns the error prompt matching the field if any
        *
        * @param {jqObject}
        *            field
        * @return undefined or the error prompt (jqObject)
        */
        _getPrompt: function (field) {

            var className = "." + methods._getClassName(field.attr("id")) + "formError";
            var match = $(className)[0];
            if (match)
                return $(match);
        },
        /**
        * Calculates prompt position
        *
        * @param {jqObject}
        *            field
        * @param {jqObject}
        *            the prompt
        * @param {Map}
        *            options
        * @return positions
        */
        _calculatePosition: function (field, promptElmt, options) {

            var promptTopPosition, promptleftPosition, marginTopSize;
            var fieldWidth = field.width();
            var promptHeight = promptElmt.height();

            var overflow = options.isOverflown;
            if (overflow) {
                // is the form contained in an overflown container?
                promptTopPosition = promptleftPosition = 0;
                // compensation for the arrow
                marginTopSize = -promptHeight;
            } else {
                var offset = field.offset();
                promptTopPosition = offset.top;
                promptleftPosition = offset.left;
                marginTopSize = 0;
            }

            switch (options.promptPosition) {

                default:
                case "topRight":
                    if (overflow)
                    // Is the form contained in an overflown container?
                        promptleftPosition += fieldWidth - 30;
                    else {
                        promptleftPosition += fieldWidth - 30;
                        promptTopPosition += -promptHeight;
                    }
                    break;
                case "topLeft":
                    promptTopPosition += -promptHeight - 10;
                    break;
                case "centerRight":
                    promptleftPosition += fieldWidth + 13;
                    break;
                case "bottomLeft":
                    promptTopPosition = promptTopPosition + field.height() + 15;
                    break;
                case "bottomRight":
                    promptleftPosition += fieldWidth - 30;
                    promptTopPosition += field.height() + 5;
            }

            return {
                "callerTopPosition": promptTopPosition + "px",
                "callerleftPosition": promptleftPosition + "px",
                "marginTopSize": marginTopSize + "px"
            };
        },
        /**
        * Saves the user options and variables in the form.data
        *
        * @param {jqObject}
        *            form - the form where the user option should be saved
        * @param {Map}
        *            options - the user options
        * @return the user options (extended from the defaults)
        */
        _saveOptions: function (form, options) {

            // is there a language localisation ?
            if ($.validationEngineLanguage)
                var allRules = $.validationEngineLanguage.allRules;
            else
                $.error("jQuery.validationEngine rules are not loaded, plz add localization files to the page");

            var userOptions = $.extend({

                // Name of the event triggering field validation
                validationEventTrigger: "blur",
                // Automatically scroll viewport to the first error
                scroll: true,
                // Opening box position, possible locations are: topLeft,
                // topRight, bottomLeft, centerRight, bottomRight
                promptPosition: "topRight",
                bindMethod: "bind",
                // internal, automatically set to true when it parse a _ajax rule
                inlineAjax: false,
                // if set to true, the form data is sent asynchronously via ajax to the form.action url (get)
                ajaxFormValidation: false,
                // Ajax form validation callback method: boolean onComplete(form, status, errors, options)
                // retuns false if the form.submit event needs to be canceled.
                ajaxFormValidationURL: false,
                // The url to send the submit ajax validation (default to action)
                onAjaxFormComplete: $.noop,
                // called right before the ajax call, may return false to cancel
                onBeforeAjaxFormValidation: $.noop,
                // Stops form from submitting and execute function assiciated with it
                onValidationComplete: false,

                // Used when the form is displayed within a scrolling DIV
                isOverflown: false,
                overflownDIV: "",

                // --- Internals DO NOT TOUCH or OVERLOAD ---
                // validation rules and i18
                allrules: allRules,
                // true when form and fields are binded
                binded: false,
                // set to true, when the prompt arrow needs to be displayed
                showArrow: true,
                // did one of the validation fail ? kept global to stop further ajax validations
                isError: false,
                // Caches field validation status, typically only bad status are created.
                // the array is used during ajax form validation to detect issues early and prevent an expensive submit
                ajaxValidCache: {}

            }, options);

            form.data('jqv', userOptions);
            return userOptions;
        },

        /**
        * Removes forbidden characters from class name
        * @param {String} className
        */
        _getClassName: function (className) {
            return className.replace(":", "_").replace(".", "_");
        }
    };

    /**
    * Plugin entry point.
    * You may pass an action as a parameter or a list of options.
    * if none, the init and attach methods are being called.
    * Remember: if you pass options, the attached method is NOT called automatically
    *
    * @param {String}
    *            method (optional) action
    */
    $.fn.validationEngine = function (method) {

        var form = $(this);
        if (!form[0]) return false;  // stop here if the form does not exist

        if (typeof (method) == 'string' && method.charAt(0) != '_' && methods[method]) {

            // make sure init is called once
            if (method != "showPrompt" && method != "hidePrompt" && method != "hide" && method != "hideAll")
                methods.init.apply(form);

            return methods[method].apply(form, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method == 'object' || !method) {
            // default constructor with or without arguments
            methods.init.apply(form, arguments);
            return methods.attach.apply(form);
        } else {
            $.error('Method ' + method + ' does not exist in jQuery.validationEngine');
        }
    };
})(jQuery);

//------------------------------------------------------------
// jquery.validationEngine-en.js
(function ($) {
    $.fn.validationEngineLanguage = function () {
    };
    $.validationEngineLanguage = {
        newLang: function () {
            $.validationEngineLanguage.allRules = {
                "required": { // Add your regex rules here, you can take telephone as an example
                    "regex": "none",
                    "alertText": "* " + Lang.ThisFieldIsRequired,
                    "alertTextCheckboxMultiple": "* " + Lang.PleaseChooseAnItem,
                    "alertTextCheckboxe": "* " + Lang.RequiredCheckbox
                },
                "minSize": {
                    "regex": "none",
                    "alertText": "* Minimum ",
                    "alertText2": " characters allowed"
                },
                "maxSize": {
                    "regex": "none",
                    "alertText": "* Maximum ",
                    "alertText2": " characters allowed"
                },
                "min": {
                    "regex": "none",
                    "alertText": "* Minimum value is "
                },
                "max": {
                    "regex": "none",
                    "alertText": "* Maximum value is "
                },
                "past": {
                    "regex": "none",
                    "alertText": "* Date prior to "
                },
                "future": {
                    "regex": "none",
                    "alertText": "* Date past "
                },
                "maxCheckbox": {
                    "regex": "none",
                    "alertText": "* Checks allowed Exceeded"
                },
                "minCheckbox": {
                    "regex": "none",
                    "alertText": "* Please select ",
                    "alertText2": " options"
                },
                "equals": {
                    "regex": "none",
                    "alertText": "* Fields do not match"
                },
                "phone": {
                    // credit: jquery.h5validate.js / orefalo
                    "regex": /^([\+][0-9]{1,3}[ \.\-])?([\(]{1}[0-9]{2,6}[\)])?([0-9 \.\-\/]{3,20})((x|ext|extension)[ ]?[0-9]{1,4})?$/,
                    "alertText": "* Invalid phone number"
                },
                "email": {
                    // Simplified, was not working in the Iphone browser
                    "regex": /^([A-Za-z0-9_\-\.\'])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,6})$/,
                    "alertText": "* Invalid email address"
                },
                "integer": {
                    "regex": /^[\-\+]?\d+$/,
                    "alertText": "* Not a valid integer"
                },
                "number": {
                    // Number, including positive, negative, and floating decimal. credit: orefalo
                    "regex": /^[\-\+]?(([0-9]+)([\.,]([0-9]+))?|([\.,]([0-9]+))?)$/,
                    "alertText": "* Invalid floating decimal number"
                },
                "date": {
                    "regex": /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/,
                    "alertText": "* Invalid date, must be in YYYY-MM-DD format"
                },
                "ipv4": {
                    "regex": /^((([01]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))[.]){3}(([0-1]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))$/,
                    "alertText": "* Invalid IP address"
                },
                "url": {
                    "regex": /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/,
                    "alertText": "* Invalid URL"
                },
                "onlyNumberSp": {
                    "regex": /^[0-9\ ]+$/,
                    "alertText": "* Numbers only"
                },
                "onlyLetterSp": {
                    "regex": /^[آ-ی\ \']+$/,
                    "alertText": "* Letters only"
                },
                "onlyLetterSp2": {
                    "regex": /^[a-zA-Z\ \']+$/,
                    "alertText": "* Letters only"
                },
                "onlyLetterNumber": {
                    "regex": /^[0-9a-zA-Z]+$/,
                    "alertText": "* No special characters allowed"
                },
                // --- CUSTOM RULES -- Those are specific to the demos, they can be removed or changed to your likings
                "ajaxUserCall": {
                    "url": "ajaxValidateFieldUser",
                    // you may want to pass extra data on the ajax call
                    "extraData": "name=eric",
                    "alertText": "* This user is already taken",
                    "alertTextLoad": "* Validating, please wait"
                },
                "ajaxUserCallPhp": {
                    "url": "phpajax/ajaxValidateFieldUser.php",
                    // you may want to pass extra data on the ajax call
                    "extraData": "name=eric",
                    // if you provide an "alertTextOk", it will show as a green prompt when the field validates
                    "alertTextOk": "* This username is available",
                    "alertText": "* This user is already taken",
                    "alertTextLoad": "* Validating, please wait"
                },
                "ajaxNameCall": {
                    // remote json service location
                    "url": "ajaxValidateFieldName",
                    // error
                    "alertText": "* This name is already taken",
                    // if you provide an "alertTextOk", it will show as a green prompt when the field validates
                    "alertTextOk": "* This name is available",
                    // speaks by itself
                    "alertTextLoad": "* Validating, please wait"
                },
                "ajaxNameCallPhp": {
                    // remote json service location
                    "url": "phpajax/ajaxValidateFieldName.php",
                    // error
                    "alertText": "* This name is already taken",
                    // speaks by itself
                    "alertTextLoad": "* Validating, please wait"
                },
                "validate2fields": {
                    "alertText": "* Please input HELLO"
                }
            };

        }
    };
    $.validationEngineLanguage.newLang();
})(jQuery);


//------------------------------------------------------------
/* http://keith-wood.name/timeEntry.html
Time entry for jQuery v1.4.9.
Written by Keith Wood (kbwood{at}iinet.com.au) June 2007.
Dual licensed under the GPL (http://dev.jquery.com/browser/trunk/jquery/GPL-LICENSE.txt) and 
MIT (http://dev.jquery.com/browser/trunk/jquery/MIT-LICENSE.txt) licenses. 
Please attribute the author if you use it. */
(function ($) { function TimeEntry() { this._disabledInputs = []; this.regional = []; this.regional[''] = { show24Hours: false, separator: ':', ampmPrefix: '', ampmNames: ['AM', 'PM'], spinnerTexts: ['Now', 'Previous field', 'Next field', 'Increment', 'Decrement'] }; this._defaults = { appendText: '', showSeconds: false, timeSteps: [1, 1, 1], initialField: 0, useMouseWheel: true, defaultTime: null, minTime: null, maxTime: null, spinnerImage: 'spinnerDefault.png', spinnerSize: [20, 20, 8], spinnerBigImage: '', spinnerBigSize: [40, 40, 16], spinnerIncDecOnly: false, spinnerRepeat: [500, 250], beforeShow: null, beforeSetTime: null }; $.extend(this._defaults, this.regional['']) } var m = 'timeEntry'; $.extend(TimeEntry.prototype, { markerClassName: 'hasTimeEntry', setDefaults: function (a) { extendRemove(this._defaults, a || {}); return this }, _connectTimeEntry: function (b, c) { var d = $(b); if (d.hasClass(this.markerClassName)) { return } var e = {}; e.options = $.extend({}, c); e._selectedHour = 0; e._selectedMinute = 0; e._selectedSecond = 0; e._field = 0; e.input = $(b); $.data(b, m, e); var f = this._get(e, 'spinnerImage'); var g = this._get(e, 'spinnerText'); var h = this._get(e, 'spinnerSize'); var i = this._get(e, 'appendText'); var j = (!f ? null : $('<span class="timeEntry_control" style="display: inline-block; ' + 'background: url(\'' + f + '\') 0 0 no-repeat; ' + 'width: ' + h[0] + 'px; height: ' + h[1] + 'px;' + ($.browser.mozilla && $.browser.version < '1.9' ? ' padding-left: ' + h[0] + 'px; padding-bottom: ' + (h[1] - 18) + 'px;' : '') + '"></span>')); d.wrap('<span class="timeEntry_wrap"></span>').after(i ? '<span class="timeEntry_append">' + i + '</span>' : '').after(j || ''); d.addClass(this.markerClassName).bind('focus.timeEntry', this._doFocus).bind('blur.timeEntry', this._doBlur).bind('click.timeEntry', this._doClick).bind('keydown.timeEntry', this._doKeyDown).bind('keypress.timeEntry', this._doKeyPress); if ($.browser.mozilla) { d.bind('input.timeEntry', function (a) { $.timeEntry._parseTime(e) }) } if ($.browser.msie) { d.bind('paste.timeEntry', function (a) { setTimeout(function () { $.timeEntry._parseTime(e) }, 1) }) } if (this._get(e, 'useMouseWheel') && $.fn.mousewheel) { d.mousewheel(this._doMouseWheel) } if (j) { j.mousedown(this._handleSpinner).mouseup(this._endSpinner).mouseover(this._expandSpinner).mouseout(this._endSpinner).mousemove(this._describeSpinner) } }, _enableTimeEntry: function (a) { this._enableDisable(a, false) }, _disableTimeEntry: function (a) { this._enableDisable(a, true) }, _enableDisable: function (b, c) { var d = $.data(b, m); if (!d) { return } b.disabled = c; if (b.nextSibling && b.nextSibling.nodeName.toLowerCase() == 'span') { $.timeEntry._changeSpinner(d, b.nextSibling, (c ? 5 : -1)) } $.timeEntry._disabledInputs = $.map($.timeEntry._disabledInputs, function (a) { return (a == b ? null : a) }); if (c) { $.timeEntry._disabledInputs.push(b) } }, _isDisabledTimeEntry: function (a) { return $.inArray(a, this._disabledInputs) > -1 }, _changeTimeEntry: function (a, b, c) { var d = $.data(a, m); if (d) { if (typeof b == 'string') { var e = b; b = {}; b[e] = c } var f = this._extractTime(d); extendRemove(d.options, b || {}); if (f) { this._setTime(d, new Date(0, 0, 0, f[0], f[1], f[2])) } } $.data(a, m, d) }, _destroyTimeEntry: function (b) { $input = $(b); if (!$input.hasClass(this.markerClassName)) { return } $input.removeClass(this.markerClassName).unbind('.timeEntry'); if ($.fn.mousewheel) { $input.unmousewheel() } this._disabledInputs = $.map(this._disabledInputs, function (a) { return (a == b ? null : a) }); $input.parent().replaceWith($input); $.removeData(b, m) }, _setTimeTimeEntry: function (a, b) { var c = $.data(a, m); if (c) { if (b === null || b === '') { c.input.val('') } else { this._setTime(c, b ? (typeof b == 'object' ? new Date(b.getTime()) : b) : null) } } }, _getTimeTimeEntry: function (a) { var b = $.data(a, m); var c = (b ? this._extractTime(b) : null); return (!c ? null : new Date(0, 0, 0, c[0], c[1], c[2])) }, _getOffsetTimeEntry: function (a) { var b = $.data(a, m); var c = (b ? this._extractTime(b) : null); return (!c ? 0 : (c[0] * 3600 + c[1] * 60 + c[2]) * 1000) }, _doFocus: function (a) { var b = (a.nodeName && a.nodeName.toLowerCase() == 'input' ? a : this); if ($.timeEntry._lastInput == b || $.timeEntry._isDisabledTimeEntry(b)) { $.timeEntry._focussed = false; return } var c = $.data(b, m); $.timeEntry._focussed = true; $.timeEntry._lastInput = b; $.timeEntry._blurredInput = null; var d = $.timeEntry._get(c, 'beforeShow'); extendRemove(c.options, (d ? d.apply(b, [b]) : {})); $.data(b, m, c); $.timeEntry._parseTime(c); setTimeout(function () { $.timeEntry._showField(c) }, 10) }, _doBlur: function (a) { $.timeEntry._blurredInput = $.timeEntry._lastInput; $.timeEntry._lastInput = null }, _doClick: function (b) { var c = b.target; var d = $.data(c, m); if (!$.timeEntry._focussed) { var e = $.timeEntry._get(d, 'separator').length + 2; d._field = 0; if (c.selectionStart != null) { for (var f = 0; f <= Math.max(1, d._secondField, d._ampmField); f++) { var g = (f != d._ampmField ? (f * e) + 2 : (d._ampmField * e) + $.timeEntry._get(d, 'ampmPrefix').length + $.timeEntry._get(d, 'ampmNames')[0].length); d._field = f; if (c.selectionStart < g) { break } } } else if (c.createTextRange) { var h = $(b.srcElement); var i = c.createTextRange(); var j = function (a) { return { thin: 2, medium: 4, thick: 6}[a] || a }; var k = b.clientX + document.documentElement.scrollLeft - (h.offset().left + parseInt(j(h.css('border-left-width')), 10)) - i.offsetLeft; for (var f = 0; f <= Math.max(1, d._secondField, d._ampmField); f++) { var g = (f != d._ampmField ? (f * e) + 2 : (d._ampmField * e) + $.timeEntry._get(d, 'ampmPrefix').length + $.timeEntry._get(d, 'ampmNames')[0].length); i.collapse(); i.moveEnd('character', g); d._field = f; if (k < i.boundingWidth) { break } } } } $.data(c, m, d); $.timeEntry._showField(d); $.timeEntry._focussed = false }, _doKeyDown: function (a) { if (a.keyCode >= 48) { return true } var b = $.data(a.target, m); switch (a.keyCode) { case 9: return (a.shiftKey ? $.timeEntry._changeField(b, -1, true) : $.timeEntry._changeField(b, +1, true)); case 35: if (a.ctrlKey) { $.timeEntry._setValue(b, '') } else { b._field = Math.max(1, b._secondField, b._ampmField); $.timeEntry._adjustField(b, 0) } break; case 36: if (a.ctrlKey) { $.timeEntry._setTime(b) } else { b._field = 0; $.timeEntry._adjustField(b, 0) } break; case 37: $.timeEntry._changeField(b, -1, false); break; case 38: $.timeEntry._adjustField(b, +1); break; case 39: $.timeEntry._changeField(b, +1, false); break; case 40: $.timeEntry._adjustField(b, -1); break; case 46: $.timeEntry._setValue(b, ''); break } return false }, _doKeyPress: function (a) { var b = String.fromCharCode(a.charCode == undefined ? a.keyCode : a.charCode); if (b < ' ') { return true } var c = $.data(a.target, m); $.timeEntry._handleKeyPress(c, b); return false }, _doMouseWheel: function (a, b) { if ($.timeEntry._isDisabledTimeEntry(a.target)) { return } b = ($.browser.opera ? -b / Math.abs(b) : ($.browser.safari ? b / Math.abs(b) : b)); var c = $.data(a.target, m); c.input.focus(); if (!c.input.val()) { $.timeEntry._parseTime(c) } $.timeEntry._adjustField(c, b); a.preventDefault() }, _expandSpinner: function (b) { var c = $.timeEntry._getSpinnerTarget(b); var d = $.data($.timeEntry._getInput(c), m); if ($.timeEntry._isDisabledTimeEntry(d.input[0])) { return } var e = $.timeEntry._get(d, 'spinnerBigImage'); if (e) { d._expanded = true; var f = $(c).offset(); var g = null; $(c).parents().each(function () { var a = $(this); if (a.css('position') == 'relative' || a.css('position') == 'absolute') { g = a.offset() } return !g }); var h = $.timeEntry._get(d, 'spinnerSize'); var i = $.timeEntry._get(d, 'spinnerBigSize'); $('<div class="timeEntry_expand" style="position: absolute; left: ' + (f.left - (i[0] - h[0]) / 2 - (g ? g.left : 0)) + 'px; top: ' + (f.top - (i[1] - h[1]) / 2 - (g ? g.top : 0)) + 'px; width: ' + i[0] + 'px; height: ' + i[1] + 'px; background: transparent url(' + e + ') no-repeat 0px 0px; z-index: 10;"></div>').mousedown($.timeEntry._handleSpinner).mouseup($.timeEntry._endSpinner).mouseout($.timeEntry._endExpand).mousemove($.timeEntry._describeSpinner).insertAfter(c) } }, _getInput: function (a) { return $(a).siblings('.' + $.timeEntry.markerClassName)[0] }, _describeSpinner: function (a) { var b = $.timeEntry._getSpinnerTarget(a); var c = $.data($.timeEntry._getInput(b), m); b.title = $.timeEntry._get(c, 'spinnerTexts')[$.timeEntry._getSpinnerRegion(c, a)] }, _handleSpinner: function (a) { var b = $.timeEntry._getSpinnerTarget(a); var c = $.timeEntry._getInput(b); if ($.timeEntry._isDisabledTimeEntry(c)) { return } if (c == $.timeEntry._blurredInput) { $.timeEntry._lastInput = c; $.timeEntry._blurredInput = null } var d = $.data(c, m); $.timeEntry._doFocus(c); var e = $.timeEntry._getSpinnerRegion(d, a); $.timeEntry._changeSpinner(d, b, e); $.timeEntry._actionSpinner(d, e); $.timeEntry._timer = null; $.timeEntry._handlingSpinner = true; var f = $.timeEntry._get(d, 'spinnerRepeat'); if (e >= 3 && f[0]) { $.timeEntry._timer = setTimeout(function () { $.timeEntry._repeatSpinner(d, e) }, f[0]); $(b).one('mouseout', $.timeEntry._releaseSpinner).one('mouseup', $.timeEntry._releaseSpinner) } }, _actionSpinner: function (a, b) { if (!a.input.val()) { $.timeEntry._parseTime(a) } switch (b) { case 0: this._setTime(a); break; case 1: this._changeField(a, -1, false); break; case 2: this._changeField(a, +1, false); break; case 3: this._adjustField(a, +1); break; case 4: this._adjustField(a, -1); break } }, _repeatSpinner: function (a, b) { if (!$.timeEntry._timer) { return } $.timeEntry._lastInput = $.timeEntry._blurredInput; this._actionSpinner(a, b); this._timer = setTimeout(function () { $.timeEntry._repeatSpinner(a, b) }, this._get(a, 'spinnerRepeat')[1]) }, _releaseSpinner: function (a) { clearTimeout($.timeEntry._timer); $.timeEntry._timer = null }, _endExpand: function (a) { $.timeEntry._timer = null; var b = $.timeEntry._getSpinnerTarget(a); var c = $.timeEntry._getInput(b); var d = $.data(c, m); $(b).remove(); d._expanded = false }, _endSpinner: function (a) { $.timeEntry._timer = null; var b = $.timeEntry._getSpinnerTarget(a); var c = $.timeEntry._getInput(b); var d = $.data(c, m); if (!$.timeEntry._isDisabledTimeEntry(c)) { $.timeEntry._changeSpinner(d, b, -1) } if ($.timeEntry._handlingSpinner) { $.timeEntry._lastInput = $.timeEntry._blurredInput } if ($.timeEntry._lastInput && $.timeEntry._handlingSpinner) { $.timeEntry._showField(d) } $.timeEntry._handlingSpinner = false }, _getSpinnerTarget: function (a) { return a.target || a.srcElement }, _getSpinnerRegion: function (a, b) { var c = this._getSpinnerTarget(b); var d = ($.browser.opera || $.browser.safari ? $.timeEntry._findPos(c) : $(c).offset()); var e = ($.browser.safari ? $.timeEntry._findScroll(c) : [document.documentElement.scrollLeft || document.body.scrollLeft, document.documentElement.scrollTop || document.body.scrollTop]); var f = this._get(a, 'spinnerIncDecOnly'); var g = (f ? 99 : b.clientX + e[0] - d.left - ($.browser.msie ? 2 : 0)); var h = b.clientY + e[1] - d.top - ($.browser.msie ? 2 : 0); var i = this._get(a, (a._expanded ? 'spinnerBigSize' : 'spinnerSize')); var j = (f ? 99 : i[0] - 1 - g); var k = i[1] - 1 - h; if (i[2] > 0 && Math.abs(g - j) <= i[2] && Math.abs(h - k) <= i[2]) { return 0 } var l = Math.min(g, h, j, k); return (l == g ? 1 : (l == j ? 2 : (l == h ? 3 : 4))) }, _changeSpinner: function (a, b, c) { $(b).css('background-position', '-' + ((c + 1) * this._get(a, (a._expanded ? 'spinnerBigSize' : 'spinnerSize'))[0]) + 'px 0px') }, _findPos: function (a) { var b = curTop = 0; if (a.offsetParent) { b = a.offsetLeft; curTop = a.offsetTop; while (a = a.offsetParent) { var c = b; b += a.offsetLeft; if (b < 0) { b = c } curTop += a.offsetTop } } return { left: b, top: curTop} }, _findScroll: function (a) { var b = false; $(a).parents().each(function () { b |= $(this).css('position') == 'fixed' }); if (b) { return [0, 0] } var c = a.scrollLeft; var d = a.scrollTop; while (a = a.parentNode) { c += a.scrollLeft || 0; d += a.scrollTop || 0 } return [c, d] }, _get: function (a, b) { return (a.options[b] != null ? a.options[b] : $.timeEntry._defaults[b]) }, _parseTime: function (a) { var b = this._extractTime(a); var c = this._get(a, 'showSeconds'); if (b) { a._selectedHour = b[0]; a._selectedMinute = b[1]; a._selectedSecond = b[2] } else { var d = this._constrainTime(a); a._selectedHour = d[0]; a._selectedMinute = d[1]; a._selectedSecond = (c ? d[2] : 0) } a._secondField = (c ? 2 : -1); a._ampmField = (this._get(a, 'show24Hours') ? -1 : (c ? 3 : 2)); a._lastChr = ''; a._field = Math.max(0, Math.min(Math.max(1, a._secondField, a._ampmField), this._get(a, 'initialField'))); if (a.input.val() != '') { this._showTime(a) } }, _extractTime: function (a, b) { b = b || a.input.val(); var c = this._get(a, 'separator'); var d = b.split(c); if (c == '' && b != '') { d[0] = b.substring(0, 2); d[1] = b.substring(2, 4); d[2] = b.substring(4, 6) } var e = this._get(a, 'ampmNames'); var f = this._get(a, 'show24Hours'); if (d.length >= 2) { var g = !f && (b.indexOf(e[0]) > -1); var h = !f && (b.indexOf(e[1]) > -1); var i = parseInt(d[0], 10); i = (isNaN(i) ? 0 : i); i = ((g || h) && i == 12 ? 0 : i) + (h ? 12 : 0); var j = parseInt(d[1], 10); j = (isNaN(j) ? 0 : j); var k = (d.length >= 3 ? parseInt(d[2], 10) : 0); k = (isNaN(k) || !this._get(a, 'showSeconds') ? 0 : k); return this._constrainTime(a, [i, j, k]) } return null }, _constrainTime: function (a, b) { var c = (b != null); if (!c) { var d = this._determineTime(a, this._get(a, 'defaultTime')) || new Date(); b = [d.getHours(), d.getMinutes(), d.getSeconds()] } var e = false; var f = this._get(a, 'timeSteps'); for (var i = 0; i < f.length; i++) { if (e) { b[i] = 0 } else if (f[i] > 1) { b[i] = Math.round(b[i] / f[i]) * f[i]; e = true } } return b }, _showTime: function (a) { var b = this._get(a, 'show24Hours'); var c = this._get(a, 'separator'); var d = (this._formatNumber(b ? a._selectedHour : ((a._selectedHour + 11) % 12) + 1) + c + this._formatNumber(a._selectedMinute) + (this._get(a, 'showSeconds') ? c + this._formatNumber(a._selectedSecond) : '') + (b ? '' : this._get(a, 'ampmPrefix') + this._get(a, 'ampmNames')[(a._selectedHour < 12 ? 0 : 1)])); this._setValue(a, d); this._showField(a) }, _showField: function (a) { var b = a.input[0]; if (a.input.is(':hidden') || $.timeEntry._lastInput != b) { return } var c = this._get(a, 'separator'); var d = c.length + 2; var e = (a._field != a._ampmField ? (a._field * d) : (a._ampmField * d) - c.length + this._get(a, 'ampmPrefix').length); var f = e + (a._field != a._ampmField ? 2 : this._get(a, 'ampmNames')[0].length); if (b.setSelectionRange) { b.setSelectionRange(e, f) } else if (b.createTextRange) { var g = b.createTextRange(); g.moveStart('character', e); g.moveEnd('character', f - a.input.val().length); g.select() } if (!b.disabled) { b.focus() } }, _formatNumber: function (a) { return (a < 10 ? '0' : '') + a }, _setValue: function (a, b) { if (b != a.input.val()) { a.input.val(b).trigger('change') } }, _changeField: function (a, b, c) { var d = (a.input.val() == '' || a._field == (b == -1 ? 0 : Math.max(1, a._secondField, a._ampmField))); if (!d) { a._field += b } this._showField(a); a._lastChr = ''; $.data(a.input[0], m, a); return (d && c) }, _adjustField: function (a, b) { if (a.input.val() == '') { b = 0 } var c = this._get(a, 'timeSteps'); this._setTime(a, new Date(0, 0, 0, a._selectedHour + (a._field == 0 ? b * c[0] : 0) + (a._field == a._ampmField ? b * 12 : 0), a._selectedMinute + (a._field == 1 ? b * c[1] : 0), a._selectedSecond + (a._field == a._secondField ? b * c[2] : 0))) }, _setTime: function (a, b) { b = this._determineTime(a, b); var c = this._constrainTime(a, b ? [b.getHours(), b.getMinutes(), b.getSeconds()] : null); b = new Date(0, 0, 0, c[0], c[1], c[2]); var b = this._normaliseTime(b); var d = this._normaliseTime(this._determineTime(a, this._get(a, 'minTime'))); var e = this._normaliseTime(this._determineTime(a, this._get(a, 'maxTime'))); b = (d && b < d ? d : (e && b > e ? e : b)); var f = this._get(a, 'beforeSetTime'); if (f) { b = f.apply(a.input[0], [this._getTimeTimeEntry(a.input[0]), b, d, e]) } a._selectedHour = b.getHours(); a._selectedMinute = b.getMinutes(); a._selectedSecond = b.getSeconds(); this._showTime(a); $.data(a.input[0], m, a) }, _normaliseTime: function (a) { if (!a) { return null } a.setFullYear(1900); a.setMonth(0); a.setDate(0); return a }, _determineTime: function (i, j) { var k = function (a) { var b = new Date(); b.setTime(b.getTime() + a * 1000); return b }; var l = function (a) { var b = $.timeEntry._extractTime(i, a); var c = new Date(); var d = (b ? b[0] : c.getHours()); var e = (b ? b[1] : c.getMinutes()); var f = (b ? b[2] : c.getSeconds()); if (!b) { var g = /([+-]?[0-9]+)\s*(s|S|m|M|h|H)?/g; var h = g.exec(a); while (h) { switch (h[2] || 's') { case 's': case 'S': f += parseInt(h[1], 10); break; case 'm': case 'M': e += parseInt(h[1], 10); break; case 'h': case 'H': d += parseInt(h[1], 10); break } h = g.exec(a) } } c = new Date(0, 0, 10, d, e, f, 0); if (/^!/.test(a)) { if (c.getDate() > 10) { c = new Date(0, 0, 10, 23, 59, 59) } else if (c.getDate() < 10) { c = new Date(0, 0, 10, 0, 0, 0) } } return c }; return (j ? (typeof j == 'string' ? l(j) : (typeof j == 'number' ? k(j) : j)) : null) }, _handleKeyPress: function (a, b) { if (b == this._get(a, 'separator')) { this._changeField(a, +1, false) } else if (b >= '0' && b <= '9') { var c = parseInt(b, 10); var d = parseInt(a._lastChr + b, 10); var e = this._get(a, 'show24Hours'); var f = (a._field != 0 ? a._selectedHour : (e ? (d < 24 ? d : c) : (d >= 1 && d <= 12 ? d : (c > 0 ? c : a._selectedHour)) % 12 + (a._selectedHour >= 12 ? 12 : 0))); var g = (a._field != 1 ? a._selectedMinute : (d < 60 ? d : c)); var h = (a._field != a._secondField ? a._selectedSecond : (d < 60 ? d : c)); var i = this._constrainTime(a, [f, g, h]); this._setTime(a, new Date(0, 0, 0, i[0], i[1], i[2])); a._lastChr = b } else if (!this._get(a, 'show24Hours')) { b = b.toLowerCase(); var j = this._get(a, 'ampmNames'); if ((b == j[0].substring(0, 1).toLowerCase() && a._selectedHour >= 12) || (b == j[1].substring(0, 1).toLowerCase() && a._selectedHour < 12)) { var k = a._field; a._field = a._ampmField; this._adjustField(a, +1); a._field = k; this._showField(a) } } } }); function extendRemove(a, b) { $.extend(a, b); for (var c in b) { if (b[c] == null) { a[c] = null } } return a } var n = ['getOffset', 'getTime', 'isDisabled']; $.fn.timeEntry = function (c) { var d = Array.prototype.slice.call(arguments, 1); if (typeof c == 'string' && $.inArray(c, n) > -1) { return $.timeEntry['_' + c + 'TimeEntry'].apply($.timeEntry, [this[0]].concat(d)) } return this.each(function () { var a = this.nodeName.toLowerCase(); if (a == 'input') { if (typeof c == 'string') { $.timeEntry['_' + c + 'TimeEntry'].apply($.timeEntry, [this].concat(d)) } else { var b = ($.fn.metadata ? $(this).metadata() : {}); $.timeEntry._connectTimeEntry(this, $.extend(b, c)) } } }) }; $.timeEntry = new TimeEntry() })(jQuery);

/* http://keith-wood.name/timeEntry.html
Persian initialisation for the jQuery time entry extension
translation by benyblack */
(function ($) {
    $.timeEntry.regional['fa'] = { show24Hours: true, separator: ':',
        ampmPrefix: '', ampmNames: [Lang.A_M, Lang.P_M],
        spinnerTexts: [Lang.Now, Lang.Prev, Lang.Next, Lang.Increment, Lang.Decrement]
    };
    $.timeEntry.setDefaults($.timeEntry.regional['fa']);
})(jQuery);


// -----------------------------------------
// fullscreen. not a jQuery plugin
// http://johndyer.name/native-fullscreen-javascript-api-plus-jquery-plugin/
function setFullScreen() {
    var fullScreenApi = {
        supportsFullScreen: false,
        isFullScreen: function () { return false; },
        requestFullScreen: function () { },
        cancelFullScreen: function () { },
        fullScreenEventName: '',
        prefix: ''
    },
        browserPrefixes = 'webkit moz o ms khtml'.split(' ');

    // check for native support
    if (typeof document.cancelFullScreen != 'undefined') {
        fullScreenApi.supportsFullScreen = true;
    } else {
        // check for fullscreen support by vendor prefix
        for (var i = 0, il = browserPrefixes.length; i < il; i++) {
            fullScreenApi.prefix = browserPrefixes[i];

            if (typeof document[fullScreenApi.prefix + 'CancelFullScreen'] != 'undefined') {
                fullScreenApi.supportsFullScreen = true;

                break;
            }
        }
    }

    // update methods to do something useful
    if (fullScreenApi.supportsFullScreen) {
        fullScreenApi.fullScreenEventName = fullScreenApi.prefix + 'fullscreenchange';

        fullScreenApi.isFullScreen = function () {
            switch (this.prefix) {
                case '':
                    return document.fullScreen;
                case 'webkit':
                    return document.webkitIsFullScreen;
                default:
                    return document[this.prefix + 'FullScreen'];
            }
        }
        fullScreenApi.requestFullScreen = function (el) {
            return (this.prefix === '') ? el.requestFullScreen() : el[this.prefix + 'RequestFullScreen']();
        }
        fullScreenApi.cancelFullScreen = function (el) {
            return (this.prefix === '') ? document.cancelFullScreen() : document[this.prefix + 'CancelFullScreen']();
        }
    }

    // jQuery plugin
    if (typeof jQuery != 'undefined') {
        jQuery.fn.requestFullScreen = function () {
            return this.each(function () {
                if (fullScreenApi.supportsFullScreen) {
                    fullScreenApi.requestFullScreen(this);
                }
            });
        };
    }

    // export api
    window.fullScreenApi = fullScreenApi;
}
setFullScreen();


/*
SWFObject v2.2 <http://code.google.com/p/swfobject/> 
is released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
*/
; var swfobject = function () {
    var D = "undefined", r = "object", S = "Shockwave Flash", W = "ShockwaveFlash.ShockwaveFlash", q = "application/x-shockwave-flash", R = "SWFObjectExprInst", x = "onreadystatechange", O = window, j = document, t = navigator, T = false, U = [h], o = [], N = [], I = [], l, Q, E, B, J = false, a = false, n, G, m = true, M = function () {
        var aa = typeof j.getElementById != D && typeof j.getElementsByTagName != D && typeof j.createElement != D, ah = t.userAgent.toLowerCase(), Y = t.platform.toLowerCase(), ae = Y ? /win/.test(Y) : /win/.test(ah), ac = Y ? /mac/.test(Y) : /mac/.test(ah), af = /webkit/.test(ah) ? parseFloat(ah.replace(/^.*webkit\/(\d+(\.\d+)?).*$/, "$1")) : false, X = ! +"\v1", ag = [0, 0, 0], ab = null;
        if (typeof t.plugins != D && typeof t.plugins[S] == r) {
            ab = t.plugins[S].description; if (ab && !(typeof t.mimeTypes != D && t.mimeTypes[q] && !t.mimeTypes[q].enabledPlugin)) {
                T = true;
                X = false; ab = ab.replace(/^.*\s+(\S+\s+\S+$)/, "$1"); ag[0] = parseInt(ab.replace(/^(.*)\..*$/, "$1"), 10); ag[1] = parseInt(ab.replace(/^.*\.(.*)\s.*$/, "$1"), 10);
                ag[2] = /[a-zA-Z]/.test(ab) ? parseInt(ab.replace(/^.*[a-zA-Z]+(.*)$/, "$1"), 10) : 0;
            } 
        } else {
            if (typeof O.ActiveXObject != D) {
                try {
                    var ad = new ActiveXObject(W); if (ad) {
                        ab = ad.GetVariable("$version");
                        if (ab) { X = true; ab = ab.split(" ")[1].split(","); ag = [parseInt(ab[0], 10), parseInt(ab[1], 10), parseInt(ab[2], 10)]; } 
                    } 
                } catch (Z) { } 
            } 
        } return { w3: aa, pv: ag, wk: af, ie: X, win: ae, mac: ac };
    } (), k = function () {
        if (!M.w3) { return; } if ((typeof j.readyState != D && j.readyState == "complete") || (typeof j.readyState == D && (j.getElementsByTagName("body")[0] || j.body))) {
            f();
        } if (!J) {
            if (typeof j.addEventListener != D) { j.addEventListener("DOMContentLoaded", f, false); } if (M.ie && M.win) {
                j.attachEvent(x, function () {
                    if (j.readyState == "complete") {
                        j.detachEvent(x, arguments.callee);
                        f();
                    } 
                }); if (O == top) { (function () { if (J) { return; } try { j.documentElement.doScroll("left"); } catch (X) { setTimeout(arguments.callee, 0); return; } f(); })(); } 
            } if (M.wk) {
                (function () {
                    if (J) {
                        return;
                    } if (!/loaded|complete/.test(j.readyState)) { setTimeout(arguments.callee, 0); return; } f();
                })();
            } s(f);
        } 
    } (); function f() {
        if (J) { return; } try {
            var Z = j.getElementsByTagName("body")[0].appendChild(C("span"));
            Z.parentNode.removeChild(Z);
        } catch (aa) { return; } J = true; var X = U.length; for (var Y = 0; Y < X; Y++) { U[Y](); } 
    } function K(X) { if (J) { X(); } else { U[U.length] = X; } } function s(Y) {
        if (typeof O.addEventListener != D) {
            O.addEventListener("load", Y, false);
        } else {
            if (typeof j.addEventListener != D) { j.addEventListener("load", Y, false); } else {
                if (typeof O.attachEvent != D) { i(O, "onload", Y); } else {
                    if (typeof O.onload == "function") {
                        var X = O.onload;
                        O.onload = function () { X(); Y(); };
                    } else { O.onload = Y; } 
                } 
            } 
        } 
    } function h() { if (T) { V(); } else { H(); } } function V() {
        var X = j.getElementsByTagName("body")[0]; var aa = C(r);
        aa.setAttribute("type", q); var Z = X.appendChild(aa); if (Z) {
            var Y = 0; (function () {
                if (typeof Z.GetVariable != D) {
                    var ab = Z.GetVariable("$version"); if (ab) {
                        ab = ab.split(" ")[1].split(",");
                        M.pv = [parseInt(ab[0], 10), parseInt(ab[1], 10), parseInt(ab[2], 10)];
                    } 
                } else { if (Y < 10) { Y++; setTimeout(arguments.callee, 10); return; } } X.removeChild(aa); Z = null; H();
            })();
        } else { H(); } 
    } function H() {
        var ag = o.length; if (ag > 0) {
            for (var af = 0; af < ag; af++) {
                var Y = o[af].id; var ab = o[af].callbackFn; var aa = { success: false, id: Y }; if (M.pv[0] > 0) {
                    var ae = c(Y);
                    if (ae) {
                        if (F(o[af].swfVersion) && !(M.wk && M.wk < 312)) { w(Y, true); if (ab) { aa.success = true; aa.ref = z(Y); ab(aa); } } else {
                            if (o[af].expressInstall && A()) {
                                var ai = {}; ai.data = o[af].expressInstall;
                                ai.width = ae.getAttribute("width") || "0"; ai.height = ae.getAttribute("height") || "0"; if (ae.getAttribute("class")) { ai.styleclass = ae.getAttribute("class"); } if (ae.getAttribute("align")) {
                                    ai.align = ae.getAttribute("align");
                                } var ah = {}; var X = ae.getElementsByTagName("param"); var ac = X.length; for (var ad = 0; ad < ac; ad++) {
                                    if (X[ad].getAttribute("name").toLowerCase() != "movie") {
                                        ah[X[ad].getAttribute("name")] = X[ad].getAttribute("value");
                                    } 
                                } P(ai, ah, Y, ab);
                            } else { p(ae); if (ab) { ab(aa); } } 
                        } 
                    } 
                } else { w(Y, true); if (ab) { var Z = z(Y); if (Z && typeof Z.SetVariable != D) { aa.success = true; aa.ref = Z; } ab(aa); } } 
            } 
        } 
    } function z(aa) {
        var X = null;
        var Y = c(aa); if (Y && Y.nodeName == "OBJECT") { if (typeof Y.SetVariable != D) { X = Y; } else { var Z = Y.getElementsByTagName(r)[0]; if (Z) { X = Z; } } } return X;
    } function A() {
        return !a && F("6.0.65") && (M.win || M.mac) && !(M.wk && M.wk < 312);
    } function P(aa, ab, X, Z) {
        a = true; E = Z || null; B = { success: false, id: X }; var ae = c(X); if (ae) {
            if (ae.nodeName == "OBJECT") { l = g(ae); Q = null; } else { l = ae; Q = X; } aa.id = R; if (typeof aa.width == D || (!/%$/.test(aa.width) && parseInt(aa.width, 10) < 310)) {
                aa.width = "310";
            } if (typeof aa.height == D || (!/%$/.test(aa.height) && parseInt(aa.height, 10) < 137)) { aa.height = "137"; } j.title = j.title.slice(0, 47) + " - Flash Player Installation";
            var ad = M.ie && M.win ? "ActiveX" : "PlugIn", ac = "MMredirectURL=" + O.location.toString().replace(/&/g, "%26") + "&MMplayerType=" + ad + "&MMdoctitle=" + j.title; if (typeof ab.flashvars != D) {
                ab.flashvars += "&" + ac;
            } else { ab.flashvars = ac; } if (M.ie && M.win && ae.readyState != 4) {
                var Y = C("div"); X += "SWFObjectNew"; Y.setAttribute("id", X); ae.parentNode.insertBefore(Y, ae); ae.style.display = "none";
                (function () { if (ae.readyState == 4) { ae.parentNode.removeChild(ae); } else { setTimeout(arguments.callee, 10); } })();
            } u(aa, ab, X);
        } 
    } function p(Y) {
        if (M.ie && M.win && Y.readyState != 4) {
            var X = C("div");
            Y.parentNode.insertBefore(X, Y); X.parentNode.replaceChild(g(Y), X); Y.style.display = "none"; (function () {
                if (Y.readyState == 4) { Y.parentNode.removeChild(Y); } else {
                    setTimeout(arguments.callee, 10);
                } 
            })();
        } else { Y.parentNode.replaceChild(g(Y), Y); } 
    } function g(ab) {
        var aa = C("div"); if (M.win && M.ie) { aa.innerHTML = ab.innerHTML; } else {
            var Y = ab.getElementsByTagName(r)[0];
            if (Y) {
                var ad = Y.childNodes; if (ad) {
                    var X = ad.length; for (var Z = 0; Z < X; Z++) {
                        if (!(ad[Z].nodeType == 1 && ad[Z].nodeName == "PARAM") && !(ad[Z].nodeType == 8)) {
                            aa.appendChild(ad[Z].cloneNode(true));
                        } 
                    } 
                } 
            } 
        } return aa;
    } function u(ai, ag, Y) {
        var X, aa = c(Y); if (M.wk && M.wk < 312) { return X; } if (aa) {
            if (typeof ai.id == D) { ai.id = Y; } if (M.ie && M.win) {
                var ah = ""; for (var ae in ai) {
                    if (ai[ae] != Object.prototype[ae]) {
                        if (ae.toLowerCase() == "data") {
                            ag.movie = ai[ae];
                        } else { if (ae.toLowerCase() == "styleclass") { ah += ' class="' + ai[ae] + '"'; } else { if (ae.toLowerCase() != "classid") { ah += " " + ae + '="' + ai[ae] + '"'; } } } 
                    } 
                } var af = ""; for (var ad in ag) {
                    if (ag[ad] != Object.prototype[ad]) {
                        af += '<param name="' + ad + '" value="' + ag[ad] + '" />';
                    } 
                } aa.outerHTML = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"' + ah + ">" + af + "</object>"; N[N.length] = ai.id; X = c(ai.id);
            } else {
                var Z = C(r); Z.setAttribute("type", q);
                for (var ac in ai) {
                    if (ai[ac] != Object.prototype[ac]) {
                        if (ac.toLowerCase() == "styleclass") { Z.setAttribute("class", ai[ac]); } else {
                            if (ac.toLowerCase() != "classid") {
                                Z.setAttribute(ac, ai[ac]);
                            } 
                        } 
                    } 
                } for (var ab in ag) { if (ag[ab] != Object.prototype[ab] && ab.toLowerCase() != "movie") { e(Z, ab, ag[ab]); } } aa.parentNode.replaceChild(Z, aa); X = Z;
            } 
        } return X;
    } function e(Z, X, Y) {
        var aa = C("param");
        aa.setAttribute("name", X); aa.setAttribute("value", Y); Z.appendChild(aa);
    } function y(Y) {
        var X = c(Y); if (X && X.nodeName == "OBJECT") {
            if (M.ie && M.win) {
                X.style.display = "none";
                (function () { if (X.readyState == 4) { b(Y); } else { setTimeout(arguments.callee, 10); } })();
            } else { X.parentNode.removeChild(X); } 
        } 
    } function b(Z) {
        var Y = c(Z); if (Y) {
            for (var X in Y) {
                if (typeof Y[X] == "function") {
                    Y[X] = null;
                } 
            } Y.parentNode.removeChild(Y);
        } 
    } function c(Z) { var X = null; try { X = j.getElementById(Z); } catch (Y) { } return X; } function C(X) { return j.createElement(X); } function i(Z, X, Y) {
        Z.attachEvent(X, Y);
        I[I.length] = [Z, X, Y];
    } function F(Z) {
        var Y = M.pv, X = Z.split("."); X[0] = parseInt(X[0], 10); X[1] = parseInt(X[1], 10) || 0; X[2] = parseInt(X[2], 10) || 0; return (Y[0] > X[0] || (Y[0] == X[0] && Y[1] > X[1]) || (Y[0] == X[0] && Y[1] == X[1] && Y[2] >= X[2])) ? true : false;
    } function v(ac, Y, ad, ab) {
        if (M.ie && M.mac) { return; } var aa = j.getElementsByTagName("head")[0]; if (!aa) { return; } var X = (ad && typeof ad == "string") ? ad : "screen"; if (ab) {
            n = null;
            G = null;
        } if (!n || G != X) {
            var Z = C("style"); Z.setAttribute("type", "text/css"); Z.setAttribute("media", X); n = aa.appendChild(Z); if (M.ie && M.win && typeof j.styleSheets != D && j.styleSheets.length > 0) {
                n = j.styleSheets[j.styleSheets.length - 1];
            } G = X;
        } if (M.ie && M.win) { if (n && typeof n.addRule == r) { n.addRule(ac, Y); } } else {
            if (n && typeof j.createTextNode != D) {
                n.appendChild(j.createTextNode(ac + " {" + Y + "}"));
            } 
        } 
    } function w(Z, X) { if (!m) { return; } var Y = X ? "visible" : "hidden"; if (J && c(Z)) { c(Z).style.visibility = Y; } else { v("#" + Z, "visibility:" + Y); } } function L(Y) {
        var Z = /[\\\"<>\.;]/;
        var X = Z.exec(Y) != null; return X && typeof encodeURIComponent != D ? encodeURIComponent(Y) : Y;
    } var d = function () {
        if (M.ie && M.win) {
            window.attachEvent("onunload", function () {
                var ac = I.length;
                for (var ab = 0; ab < ac; ab++) { I[ab][0].detachEvent(I[ab][1], I[ab][2]); } var Z = N.length; for (var aa = 0; aa < Z; aa++) { y(N[aa]); } for (var Y in M) { M[Y] = null; } M = null; for (var X in swfobject) {
                    swfobject[X] = null;
                } swfobject = null;
            });
        } 
    } (); return { registerObject: function (ab, X, aa, Z) {
        if (M.w3 && ab && X) {
            var Y = {}; Y.id = ab; Y.swfVersion = X; Y.expressInstall = aa; Y.callbackFn = Z; o[o.length] = Y;
            w(ab, false);
        } else { if (Z) { Z({ success: false, id: ab }); } } 
    }, getObjectById: function (X) { if (M.w3) { return z(X); } }, embedSWF: function (ab, ah, ae, ag, Y, aa, Z, ad, af, ac) {
        var X = { success: false, id: ah };
        if (M.w3 && !(M.wk && M.wk < 312) && ab && ah && ae && ag && Y) {
            w(ah, false); K(function () {
                ae += ""; ag += ""; var aj = {}; if (af && typeof af === r) { for (var al in af) { aj[al] = af[al]; } } aj.data = ab;
                aj.width = ae; aj.height = ag; var am = {}; if (ad && typeof ad === r) { for (var ak in ad) { am[ak] = ad[ak]; } } if (Z && typeof Z === r) {
                    for (var ai in Z) {
                        if (typeof am.flashvars != D) {
                            am.flashvars += "&" + ai + "=" + Z[ai];
                        } else { am.flashvars = ai + "=" + Z[ai]; } 
                    } 
                } if (F(Y)) { var an = u(aj, am, ah); if (aj.id == ah) { w(ah, true); } X.success = true; X.ref = an; } else {
                    if (aa && A()) {
                        aj.data = aa; P(aj, am, ah, ac);
                        return;
                    } else { w(ah, true); } 
                } if (ac) { ac(X); } 
            });
        } else { if (ac) { ac(X); } } 
    }, switchOffAutoHideShow: function () { m = false; }, ua: M, getFlashPlayerVersion: function () {
        return { major: M.pv[0], minor: M.pv[1], release: M.pv[2] };
    }, hasFlashPlayerVersion: F, createSWF: function (Z, Y, X) { if (M.w3) { return u(Z, Y, X); } else { return undefined; } }, showExpressInstall: function (Z, aa, X, Y) {
        if (M.w3 && A()) {
            P(Z, aa, X, Y);
        } 
    }, removeSWF: function (X) { if (M.w3) { y(X); } }, createCSS: function (aa, Z, Y, X) { if (M.w3) { v(aa, Z, Y, X); } }, addDomLoadEvent: K, addLoadEvent: s, getQueryParamValue: function (aa) {
        var Z = j.location.search || j.location.hash;
        if (Z) {
            if (/\?/.test(Z)) { Z = Z.split("?")[1]; } if (aa == null) { return L(Z); } var Y = Z.split("&"); for (var X = 0; X < Y.length; X++) {
                if (Y[X].substring(0, Y[X].indexOf("=")) == aa) {
                    return L(Y[X].substring((Y[X].indexOf("=") + 1)));
                } 
            } 
        } return "";
    }, expressInstallCallback: function () {
        if (a) {
            var X = c(R); if (X && l) {
                X.parentNode.replaceChild(l, X); if (Q) {
                    w(Q, true); if (M.ie && M.win) {
                        l.style.display = "block";
                    } 
                } if (E) { E(B); } 
            } a = false;
        } 
    } 
    };
} ();


var SWFUpload; if (SWFUpload == undefined) { SWFUpload = function (a) { this.initSWFUpload(a) } } SWFUpload.prototype.initSWFUpload = function (b) { try { this.customSettings = {}; this.settings = b; this.eventQueue = []; this.movieName = "SWFUpload_" + SWFUpload.movieCount++; this.movieElement = null; SWFUpload.instances[this.movieName] = this; this.initSettings(); this.loadFlash(); this.displayDebugInfo() } catch (a) { delete SWFUpload.instances[this.movieName]; throw a } }; SWFUpload.instances = {}; SWFUpload.movieCount = 0; SWFUpload.version = "2.2.0 2009-03-25"; SWFUpload.QUEUE_ERROR = { QUEUE_LIMIT_EXCEEDED: -100, FILE_EXCEEDS_SIZE_LIMIT: -110, ZERO_BYTE_FILE: -120, INVALID_FILETYPE: -130 }; SWFUpload.UPLOAD_ERROR = { HTTP_ERROR: -200, MISSING_UPLOAD_URL: -210, IO_ERROR: -220, SECURITY_ERROR: -230, UPLOAD_LIMIT_EXCEEDED: -240, UPLOAD_FAILED: -250, SPECIFIED_FILE_ID_NOT_FOUND: -260, FILE_VALIDATION_FAILED: -270, FILE_CANCELLED: -280, UPLOAD_STOPPED: -290 }; SWFUpload.FILE_STATUS = { QUEUED: -1, IN_PROGRESS: -2, ERROR: -3, COMPLETE: -4, CANCELLED: -5 }; SWFUpload.BUTTON_ACTION = { SELECT_FILE: -100, SELECT_FILES: -110, START_UPLOAD: -120 }; SWFUpload.CURSOR = { ARROW: -1, HAND: -2 }; SWFUpload.WINDOW_MODE = { WINDOW: "window", TRANSPARENT: "transparent", OPAQUE: "opaque" }; SWFUpload.completeURL = function (a) { if (typeof (a) !== "string" || a.match(/^https?:\/\//i) || a.match(/^\//)) { return a } var c = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ":" + window.location.port : ""); var b = window.location.pathname.lastIndexOf("/"); if (b <= 0) { path = "/" } else { path = window.location.pathname.substr(0, b) + "/" } return path + a }; SWFUpload.prototype.initSettings = function () { this.ensureDefault = function (b, a) { this.settings[b] = (this.settings[b] == undefined) ? a : this.settings[b] }; this.ensureDefault("upload_url", ""); this.ensureDefault("preserve_relative_urls", false); this.ensureDefault("file_post_name", "Filedata"); this.ensureDefault("post_params", {}); this.ensureDefault("use_query_string", false); this.ensureDefault("requeue_on_error", false); this.ensureDefault("http_success", []); this.ensureDefault("assume_success_timeout", 0); this.ensureDefault("file_types", "*.*"); this.ensureDefault("file_types_description", "All Files"); this.ensureDefault("file_size_limit", 0); this.ensureDefault("file_upload_limit", 0); this.ensureDefault("file_queue_limit", 0); this.ensureDefault("flash_url", "swfupload.swf"); this.ensureDefault("prevent_swf_caching", true); this.ensureDefault("button_image_url", ""); this.ensureDefault("button_width", 1); this.ensureDefault("button_height", 1); this.ensureDefault("button_text", ""); this.ensureDefault("button_text_style", "color: #000000; font-size: 16pt;"); this.ensureDefault("button_text_top_padding", 0); this.ensureDefault("button_text_left_padding", 0); this.ensureDefault("button_action", SWFUpload.BUTTON_ACTION.SELECT_FILES); this.ensureDefault("button_disabled", false); this.ensureDefault("button_placeholder_id", ""); this.ensureDefault("button_placeholder", null); this.ensureDefault("button_cursor", SWFUpload.CURSOR.ARROW); this.ensureDefault("button_window_mode", SWFUpload.WINDOW_MODE.WINDOW); this.ensureDefault("debug", false); this.settings.debug_enabled = this.settings.debug; this.settings.return_upload_start_handler = this.returnUploadStart; this.ensureDefault("swfupload_loaded_handler", null); this.ensureDefault("file_dialog_start_handler", null); this.ensureDefault("file_queued_handler", null); this.ensureDefault("file_queue_error_handler", null); this.ensureDefault("file_dialog_complete_handler", null); this.ensureDefault("upload_start_handler", null); this.ensureDefault("upload_progress_handler", null); this.ensureDefault("upload_error_handler", null); this.ensureDefault("upload_success_handler", null); this.ensureDefault("upload_complete_handler", null); this.ensureDefault("debug_handler", this.debugMessage); this.ensureDefault("custom_settings", {}); this.customSettings = this.settings.custom_settings; if (!!this.settings.prevent_swf_caching) { this.settings.flash_url = this.settings.flash_url + (this.settings.flash_url.indexOf("?") < 0 ? "?" : "&") + "preventswfcaching=" + new Date().getTime() } if (!this.settings.preserve_relative_urls) { this.settings.upload_url = SWFUpload.completeURL(this.settings.upload_url); this.settings.button_image_url = SWFUpload.completeURL(this.settings.button_image_url) } delete this.ensureDefault }; SWFUpload.prototype.loadFlash = function () { var a, b; if (document.getElementById(this.movieName) !== null) { throw "ID " + this.movieName + " is already in use. The Flash Object could not be added" } a = document.getElementById(this.settings.button_placeholder_id) || this.settings.button_placeholder; if (a == undefined) { throw "Could not find the placeholder element: " + this.settings.button_placeholder_id } b = document.createElement("div"); b.innerHTML = this.getFlashHTML(); a.parentNode.replaceChild(b.firstChild, a); if (window[this.movieName] == undefined) { window[this.movieName] = this.getMovieElement() } }; SWFUpload.prototype.getFlashHTML = function () { return ['<object id="', this.movieName, '" type="application/x-shockwave-flash" data="', this.settings.flash_url, '" width="', this.settings.button_width, '" height="', this.settings.button_height, '" class="swfupload">', '<param name="wmode" value="', this.settings.button_window_mode, '" />', '<param name="movie" value="', this.settings.flash_url, '" />', '<param name="quality" value="high" />', '<param name="menu" value="false" />', '<param name="allowScriptAccess" value="always" />', '<param name="flashvars" value="' + this.getFlashVars() + '" />', "</object>"].join("") }; SWFUpload.prototype.getFlashVars = function () { var b = this.buildParamString(); var a = this.settings.http_success.join(","); return ["movieName=", encodeURIComponent(this.movieName), "&amp;uploadURL=", encodeURIComponent(this.settings.upload_url), "&amp;useQueryString=", encodeURIComponent(this.settings.use_query_string), "&amp;requeueOnError=", encodeURIComponent(this.settings.requeue_on_error), "&amp;httpSuccess=", encodeURIComponent(a), "&amp;assumeSuccessTimeout=", encodeURIComponent(this.settings.assume_success_timeout), "&amp;params=", encodeURIComponent(b), "&amp;filePostName=", encodeURIComponent(this.settings.file_post_name), "&amp;fileTypes=", encodeURIComponent(this.settings.file_types), "&amp;fileTypesDescription=", encodeURIComponent(this.settings.file_types_description), "&amp;fileSizeLimit=", encodeURIComponent(this.settings.file_size_limit), "&amp;fileUploadLimit=", encodeURIComponent(this.settings.file_upload_limit), "&amp;fileQueueLimit=", encodeURIComponent(this.settings.file_queue_limit), "&amp;debugEnabled=", encodeURIComponent(this.settings.debug_enabled), "&amp;buttonImageURL=", encodeURIComponent(this.settings.button_image_url), "&amp;buttonWidth=", encodeURIComponent(this.settings.button_width), "&amp;buttonHeight=", encodeURIComponent(this.settings.button_height), "&amp;buttonText=", encodeURIComponent(this.settings.button_text), "&amp;buttonTextTopPadding=", encodeURIComponent(this.settings.button_text_top_padding), "&amp;buttonTextLeftPadding=", encodeURIComponent(this.settings.button_text_left_padding), "&amp;buttonTextStyle=", encodeURIComponent(this.settings.button_text_style), "&amp;buttonAction=", encodeURIComponent(this.settings.button_action), "&amp;buttonDisabled=", encodeURIComponent(this.settings.button_disabled), "&amp;buttonCursor=", encodeURIComponent(this.settings.button_cursor)].join("") }; SWFUpload.prototype.getMovieElement = function () { if (this.movieElement == undefined) { this.movieElement = document.getElementById(this.movieName) } if (this.movieElement === null) { throw "Could not find Flash element" } return this.movieElement }; SWFUpload.prototype.buildParamString = function () { var c = this.settings.post_params; var b = []; if (typeof (c) === "object") { for (var a in c) { if (c.hasOwnProperty(a)) { b.push(encodeURIComponent(a.toString()) + "=" + encodeURIComponent(c[a].toString())) } } } return b.join("&amp;") }; SWFUpload.prototype.destroy = function () { try { this.cancelUpload(null, false); var a = null; a = this.getMovieElement(); if (a && typeof (a.CallFunction) === "unknown") { for (var c in a) { try { if (typeof (a[c]) === "function") { a[c] = null } } catch (e) { } } try { a.parentNode.removeChild(a) } catch (b) { } } window[this.movieName] = null; SWFUpload.instances[this.movieName] = null; delete SWFUpload.instances[this.movieName]; this.movieElement = null; this.settings = null; this.customSettings = null; this.eventQueue = null; this.movieName = null; return true } catch (d) { return false } }; SWFUpload.prototype.displayDebugInfo = function () { this.debug(["---SWFUpload Instance Info---\n", "Version: ", SWFUpload.version, "\n", "Movie Name: ", this.movieName, "\n", "Settings:\n", "\t", "upload_url:               ", this.settings.upload_url, "\n", "\t", "flash_url:                ", this.settings.flash_url, "\n", "\t", "use_query_string:         ", this.settings.use_query_string.toString(), "\n", "\t", "requeue_on_error:         ", this.settings.requeue_on_error.toString(), "\n", "\t", "http_success:             ", this.settings.http_success.join(", "), "\n", "\t", "assume_success_timeout:   ", this.settings.assume_success_timeout, "\n", "\t", "file_post_name:           ", this.settings.file_post_name, "\n", "\t", "post_params:              ", this.settings.post_params.toString(), "\n", "\t", "file_types:               ", this.settings.file_types, "\n", "\t", "file_types_description:   ", this.settings.file_types_description, "\n", "\t", "file_size_limit:          ", this.settings.file_size_limit, "\n", "\t", "file_upload_limit:        ", this.settings.file_upload_limit, "\n", "\t", "file_queue_limit:         ", this.settings.file_queue_limit, "\n", "\t", "debug:                    ", this.settings.debug.toString(), "\n", "\t", "prevent_swf_caching:      ", this.settings.prevent_swf_caching.toString(), "\n", "\t", "button_placeholder_id:    ", this.settings.button_placeholder_id.toString(), "\n", "\t", "button_placeholder:       ", (this.settings.button_placeholder ? "Set" : "Not Set"), "\n", "\t", "button_image_url:         ", this.settings.button_image_url.toString(), "\n", "\t", "button_width:             ", this.settings.button_width.toString(), "\n", "\t", "button_height:            ", this.settings.button_height.toString(), "\n", "\t", "button_text:              ", this.settings.button_text.toString(), "\n", "\t", "button_text_style:        ", this.settings.button_text_style.toString(), "\n", "\t", "button_text_top_padding:  ", this.settings.button_text_top_padding.toString(), "\n", "\t", "button_text_left_padding: ", this.settings.button_text_left_padding.toString(), "\n", "\t", "button_action:            ", this.settings.button_action.toString(), "\n", "\t", "button_disabled:          ", this.settings.button_disabled.toString(), "\n", "\t", "custom_settings:          ", this.settings.custom_settings.toString(), "\n", "Event Handlers:\n", "\t", "swfupload_loaded_handler assigned:  ", (typeof this.settings.swfupload_loaded_handler === "function").toString(), "\n", "\t", "file_dialog_start_handler assigned: ", (typeof this.settings.file_dialog_start_handler === "function").toString(), "\n", "\t", "file_queued_handler assigned:       ", (typeof this.settings.file_queued_handler === "function").toString(), "\n", "\t", "file_queue_error_handler assigned:  ", (typeof this.settings.file_queue_error_handler === "function").toString(), "\n", "\t", "upload_start_handler assigned:      ", (typeof this.settings.upload_start_handler === "function").toString(), "\n", "\t", "upload_progress_handler assigned:   ", (typeof this.settings.upload_progress_handler === "function").toString(), "\n", "\t", "upload_error_handler assigned:      ", (typeof this.settings.upload_error_handler === "function").toString(), "\n", "\t", "upload_success_handler assigned:    ", (typeof this.settings.upload_success_handler === "function").toString(), "\n", "\t", "upload_complete_handler assigned:   ", (typeof this.settings.upload_complete_handler === "function").toString(), "\n", "\t", "debug_handler assigned:             ", (typeof this.settings.debug_handler === "function").toString(), "\n"].join("")) }; SWFUpload.prototype.addSetting = function (b, c, a) { if (c == undefined) { return (this.settings[b] = a) } else { return (this.settings[b] = c) } }; SWFUpload.prototype.getSetting = function (a) { if (this.settings[a] != undefined) { return this.settings[a] } return "" }; SWFUpload.prototype.callFlash = function (functionName, argumentArray) { argumentArray = argumentArray || []; var movieElement = this.getMovieElement(); var returnValue, returnString; try { returnString = movieElement.CallFunction('<invoke name="' + functionName + '" returntype="javascript">' + __flash__argumentsToXML(argumentArray, 0) + "</invoke>"); returnValue = eval(returnString) } catch (ex) { throw "Call to " + functionName + " failed" } if (returnValue != undefined && typeof returnValue.post === "object") { returnValue = this.unescapeFilePostParams(returnValue) } return returnValue }; SWFUpload.prototype.selectFile = function () { this.callFlash("SelectFile") }; SWFUpload.prototype.selectFiles = function () { this.callFlash("SelectFiles") }; SWFUpload.prototype.startUpload = function (a) { this.callFlash("StartUpload", [a]) }; SWFUpload.prototype.cancelUpload = function (a, b) { if (b !== false) { b = true } this.callFlash("CancelUpload", [a, b]) }; SWFUpload.prototype.stopUpload = function () { this.callFlash("StopUpload") }; SWFUpload.prototype.getStats = function () { return this.callFlash("GetStats") }; SWFUpload.prototype.setStats = function (a) { this.callFlash("SetStats", [a]) }; SWFUpload.prototype.getFile = function (a) { if (typeof (a) === "number") { return this.callFlash("GetFileByIndex", [a]) } else { return this.callFlash("GetFile", [a]) } }; SWFUpload.prototype.addFileParam = function (a, b, c) { return this.callFlash("AddFileParam", [a, b, c]) }; SWFUpload.prototype.removeFileParam = function (a, b) { this.callFlash("RemoveFileParam", [a, b]) }; SWFUpload.prototype.setUploadURL = function (a) { this.settings.upload_url = a.toString(); this.callFlash("SetUploadURL", [a]) }; SWFUpload.prototype.setPostParams = function (a) { this.settings.post_params = a; this.callFlash("SetPostParams", [a]) }; SWFUpload.prototype.addPostParam = function (a, b) { this.settings.post_params[a] = b; this.callFlash("SetPostParams", [this.settings.post_params]) }; SWFUpload.prototype.removePostParam = function (a) { delete this.settings.post_params[a]; this.callFlash("SetPostParams", [this.settings.post_params]) }; SWFUpload.prototype.setFileTypes = function (a, b) { this.settings.file_types = a; this.settings.file_types_description = b; this.callFlash("SetFileTypes", [a, b]) }; SWFUpload.prototype.setFileSizeLimit = function (a) { this.settings.file_size_limit = a; this.callFlash("SetFileSizeLimit", [a]) }; SWFUpload.prototype.setFileUploadLimit = function (a) { this.settings.file_upload_limit = a; this.callFlash("SetFileUploadLimit", [a]) }; SWFUpload.prototype.setFileQueueLimit = function (a) { this.settings.file_queue_limit = a; this.callFlash("SetFileQueueLimit", [a]) }; SWFUpload.prototype.setFilePostName = function (a) { this.settings.file_post_name = a; this.callFlash("SetFilePostName", [a]) }; SWFUpload.prototype.setUseQueryString = function (a) { this.settings.use_query_string = a; this.callFlash("SetUseQueryString", [a]) }; SWFUpload.prototype.setRequeueOnError = function (a) { this.settings.requeue_on_error = a; this.callFlash("SetRequeueOnError", [a]) }; SWFUpload.prototype.setHTTPSuccess = function (a) { if (typeof a === "string") { a = a.replace(" ", "").split(",") } this.settings.http_success = a; this.callFlash("SetHTTPSuccess", [a]) }; SWFUpload.prototype.setAssumeSuccessTimeout = function (a) { this.settings.assume_success_timeout = a; this.callFlash("SetAssumeSuccessTimeout", [a]) }; SWFUpload.prototype.setDebugEnabled = function (a) { this.settings.debug_enabled = a; this.callFlash("SetDebugEnabled", [a]) }; SWFUpload.prototype.setButtonImageURL = function (a) { if (a == undefined) { a = "" } this.settings.button_image_url = a; this.callFlash("SetButtonImageURL", [a]) }; SWFUpload.prototype.setButtonDimensions = function (c, a) { this.settings.button_width = c; this.settings.button_height = a; var b = this.getMovieElement(); if (b != undefined) { b.style.width = c + "px"; b.style.height = a + "px" } this.callFlash("SetButtonDimensions", [c, a]) }; SWFUpload.prototype.setButtonText = function (a) { this.settings.button_text = a; this.callFlash("SetButtonText", [a]) }; SWFUpload.prototype.setButtonTextPadding = function (b, a) { this.settings.button_text_top_padding = a; this.settings.button_text_left_padding = b; this.callFlash("SetButtonTextPadding", [b, a]) }; SWFUpload.prototype.setButtonTextStyle = function (a) { this.settings.button_text_style = a; this.callFlash("SetButtonTextStyle", [a]) }; SWFUpload.prototype.setButtonDisabled = function (a) { this.settings.button_disabled = a; this.callFlash("SetButtonDisabled", [a]) }; SWFUpload.prototype.setButtonAction = function (a) { this.settings.button_action = a; this.callFlash("SetButtonAction", [a]) }; SWFUpload.prototype.setButtonCursor = function (a) { this.settings.button_cursor = a; this.callFlash("SetButtonCursor", [a]) }; SWFUpload.prototype.queueEvent = function (b, c) { if (c == undefined) { c = [] } else { if (!(c instanceof Array)) { c = [c] } } var a = this; if (typeof this.settings[b] === "function") { this.eventQueue.push(function () { this.settings[b].apply(this, c) }); setTimeout(function () { a.executeNextEvent() }, 0) } else { if (this.settings[b] !== null) { throw "Event handler " + b + " is unknown or is not a function" } } }; SWFUpload.prototype.executeNextEvent = function () { var a = this.eventQueue ? this.eventQueue.shift() : null; if (typeof (a) === "function") { a.apply(this) } }; SWFUpload.prototype.unescapeFilePostParams = function (c) { var e = /[$]([0-9a-f]{4})/i; var f = {}; var d; if (c != undefined) { for (var a in c.post) { if (c.post.hasOwnProperty(a)) { d = a; var b; while ((b = e.exec(d)) !== null) { d = d.replace(b[0], String.fromCharCode(parseInt("0x" + b[1], 16))) } f[d] = c.post[a] } } c.post = f } return c }; SWFUpload.prototype.testExternalInterface = function () { try { return this.callFlash("TestExternalInterface") } catch (a) { return false } }; SWFUpload.prototype.flashReady = function () { var a = this.getMovieElement(); if (!a) { this.debug("Flash called back ready but the flash movie can't be found."); return } this.cleanUp(a); this.queueEvent("swfupload_loaded_handler") }; SWFUpload.prototype.cleanUp = function (a) { try { if (this.movieElement && typeof (a.CallFunction) === "unknown") { this.debug("Removing Flash functions hooks (this should only run in IE and should prevent memory leaks)"); for (var c in a) { try { if (typeof (a[c]) === "function") { a[c] = null } } catch (b) { } } } } catch (d) { } window.__flash__removeCallback = function (e, f) { try { if (e) { e[f] = null } } catch (g) { } } }; SWFUpload.prototype.fileDialogStart = function () { this.queueEvent("file_dialog_start_handler") }; SWFUpload.prototype.fileQueued = function (a) { a = this.unescapeFilePostParams(a); this.queueEvent("file_queued_handler", a) }; SWFUpload.prototype.fileQueueError = function (a, c, b) { a = this.unescapeFilePostParams(a); this.queueEvent("file_queue_error_handler", [a, c, b]) }; SWFUpload.prototype.fileDialogComplete = function (b, c, a) { this.queueEvent("file_dialog_complete_handler", [b, c, a]) }; SWFUpload.prototype.uploadStart = function (a) { a = this.unescapeFilePostParams(a); this.queueEvent("return_upload_start_handler", a) }; SWFUpload.prototype.returnUploadStart = function (a) { var b; if (typeof this.settings.upload_start_handler === "function") { a = this.unescapeFilePostParams(a); b = this.settings.upload_start_handler.call(this, a) } else { if (this.settings.upload_start_handler != undefined) { throw "upload_start_handler must be a function" } } if (b === undefined) { b = true } b = !!b; this.callFlash("ReturnUploadStart", [b]) }; SWFUpload.prototype.uploadProgress = function (a, c, b) { a = this.unescapeFilePostParams(a); this.queueEvent("upload_progress_handler", [a, c, b]) }; SWFUpload.prototype.uploadError = function (a, c, b) { a = this.unescapeFilePostParams(a); this.queueEvent("upload_error_handler", [a, c, b]) }; SWFUpload.prototype.uploadSuccess = function (b, a, c) { b = this.unescapeFilePostParams(b); this.queueEvent("upload_success_handler", [b, a, c]) }; SWFUpload.prototype.uploadComplete = function (a) { a = this.unescapeFilePostParams(a); this.queueEvent("upload_complete_handler", a) }; SWFUpload.prototype.debug = function (a) { this.queueEvent("debug_handler", a) }; SWFUpload.prototype.debugMessage = function (c) { if (this.settings.debug) { var a, d = []; if (typeof c === "object" && typeof c.name === "string" && typeof c.message === "string") { for (var b in c) { if (c.hasOwnProperty(b)) { d.push(b + ": " + c[b]) } } a = d.join("\n") || ""; d = a.split("\n"); a = "EXCEPTION: " + d.join("\nEXCEPTION: "); SWFUpload.Console.writeLine(a) } else { SWFUpload.Console.writeLine(c) } } }; SWFUpload.Console = {}; SWFUpload.Console.writeLine = function (d) { var b, a; try { b = document.getElementById("SWFUpload_Console"); if (!b) { a = document.createElement("form"); document.getElementsByTagName("body")[0].appendChild(a); b = document.createElement("textarea"); b.id = "SWFUpload_Console"; b.style.fontFamily = "monospace"; b.setAttribute("wrap", "off"); b.wrap = "off"; b.style.overflow = "auto"; b.style.width = "700px"; b.style.height = "350px"; b.style.margin = "5px"; a.appendChild(b) } b.value += d + "\n"; b.scrollTop = b.scrollHeight - b.clientHeight } catch (c) { alert("Exception: " + c.name + " Message: " + c.message) } };

/*
Uploadify v3.2
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
*/

(function ($) {

    // These methods can be called by adding them as the first argument in the uploadify plugin call
    var methods = {

        init: function (options, swfUploadOptions) {

            return this.each(function () {

                // Create a reference to the jQuery DOM object
                var $this = $(this);

                // Clone the original DOM object
                var $clone = $this.clone();

                // Setup the default options
                var settings = $.extend({
                    // Required Settings
                    id: $this.attr('id'), // The ID of the DOM object
                    swf: 'uploadify.swf',  // The path to the uploadify SWF file
                    uploader: 'uploadify.php',  // The path to the server-side upload script

                    // Options
                    auto: true,               // Automatically upload files when added to the queue
                    buttonClass: '',                 // A class name to add to the browse button DOM object
                    buttonCursor: 'hand',             // The cursor to use with the browse button
                    buttonImage: null,               // (String or null) The path to an image to use for the Flash browse button if not using CSS to style the button
                    buttonText: 'SELECT FILES',     // The text to use for the browse button
                    checkExisting: false,              // The path to a server-side script that checks for existing files on the server
                    debug: false,              // Turn on swfUpload debugging mode
                    fileObjName: 'Filedata',         // The name of the file object to use in your server-side script
                    fileSizeLimit: 0,                  // The maximum size of an uploadable file in KB (Accepts units B KB MB GB if string, 0 for no limit)
                    fileTypeDesc: 'All Files',        // The description for file types in the browse dialog
                    fileTypeExts: '*.*',              // Allowed extensions in the browse dialog (server-side validation should also be used)
                    height: 30,                 // The height of the browse button
                    itemTemplate: false,              // The template for the file item in the queue
                    method: 'post',             // The method to use when sending files to the server-side upload script
                    multi: true,               // Allow multiple file selection in the browse dialog
                    formData: {},                 // An object with additional data to send to the server-side upload script with every file upload
                    preventCaching: true,               // Adds a random value to the Flash URL to prevent caching of it (conflicts with existing parameters)
                    progressData: 'percentage',       // ('percentage' or 'speed') Data to show in the queue item during a file upload
                    queueID: false,              // The ID of the DOM object to use as a file queue (without the #)
                    queueSizeLimit: 999,                // The maximum number of files that can be in the queue at one time
                    removeCompleted: true,               // Remove queue items from the queue when they are done uploading
                    removeTimeout: 3,                  // The delay in seconds before removing a queue item if removeCompleted is set to true
                    requeueErrors: false,              // Keep errored files in the queue and keep trying to upload them
                    successTimeout: 30,                 // The number of seconds to wait for Flash to detect the server's response after the file has finished uploading
                    uploadLimit: 0,                  // The maximum number of files you can upload
                    width: 120,                // The width of the browse button

                    // Events
                    overrideEvents: []             // (Array) A list of default event handlers to skip
                    /*
                    onCancel         // Triggered when a file is cancelled from the queue
                    onClearQueue     // Triggered during the 'clear queue' method
                    onDestroy        // Triggered when the uploadify object is destroyed
                    onDialogClose    // Triggered when the browse dialog is closed
                    onDialogOpen     // Triggered when the browse dialog is opened
                    onDisable        // Triggered when the browse button gets disabled
                    onEnable         // Triggered when the browse button gets enabled
                    onFallback       // Triggered is Flash is not detected    
                    onInit           // Triggered when Uploadify is initialized
                    onQueueComplete  // Triggered when all files in the queue have been uploaded
                    onSelectError    // Triggered when an error occurs while selecting a file (file size, queue size limit, etc.)
                    onSelect         // Triggered for each file that is selected
                    onSWFReady       // Triggered when the SWF button is loaded
                    onUploadComplete // Triggered when a file upload completes (success or error)
                    onUploadError    // Triggered when a file upload returns an error
                    onUploadSuccess  // Triggered when a file is uploaded successfully
                    onUploadProgress // Triggered every time a file progress is updated
                    onUploadStart    // Triggered immediately before a file upload starts
                    */
                }, options);

                // Prepare settings for SWFUpload
                var swfUploadSettings = {
                    assume_success_timeout: settings.successTimeout,
                    button_placeholder_id: settings.id,
                    button_width: settings.width,
                    button_height: settings.height,
                    button_text: null,
                    button_text_style: null,
                    button_text_top_padding: 0,
                    button_text_left_padding: 0,
                    button_action: (settings.multi ? SWFUpload.BUTTON_ACTION.SELECT_FILES : SWFUpload.BUTTON_ACTION.SELECT_FILE),
                    button_disabled: false,
                    button_cursor: (settings.buttonCursor == 'arrow' ? SWFUpload.CURSOR.ARROW : SWFUpload.CURSOR.HAND),
                    button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
                    debug: settings.debug,
                    requeue_on_error: settings.requeueErrors,
                    file_post_name: settings.fileObjName,
                    file_size_limit: settings.fileSizeLimit,
                    file_types: settings.fileTypeExts,
                    file_types_description: settings.fileTypeDesc,
                    file_queue_limit: settings.queueSizeLimit,
                    file_upload_limit: settings.uploadLimit,
                    flash_url: settings.swf,
                    prevent_swf_caching: settings.preventCaching,
                    post_params: settings.formData,
                    upload_url: settings.uploader,
                    use_query_string: (settings.method == 'get'),

                    // Event Handlers 
                    file_dialog_complete_handler: handlers.onDialogClose,
                    file_dialog_start_handler: handlers.onDialogOpen,
                    file_queued_handler: handlers.onSelect,
                    file_queue_error_handler: handlers.onSelectError,
                    swfupload_loaded_handler: settings.onSWFReady,
                    upload_complete_handler: handlers.onUploadComplete,
                    upload_error_handler: handlers.onUploadError,
                    upload_progress_handler: handlers.onUploadProgress,
                    upload_start_handler: handlers.onUploadStart,
                    upload_success_handler: handlers.onUploadSuccess
                }

                // Merge the user-defined options with the defaults
                if (swfUploadOptions) {
                    swfUploadSettings = $.extend(swfUploadSettings, swfUploadOptions);
                }
                // Add the user-defined settings to the swfupload object
                swfUploadSettings = $.extend(swfUploadSettings, settings);

                // Detect if Flash is available
                var playerVersion = swfobject.getFlashPlayerVersion();
                var flashInstalled = (playerVersion.major >= 9);

                if (flashInstalled) {
                    // Create the swfUpload instance
                    window['uploadify_' + settings.id] = new SWFUpload(swfUploadSettings);
                    var swfuploadify = window['uploadify_' + settings.id];

                    // Add the SWFUpload object to the elements data object
                    $this.data('uploadify', swfuploadify);

                    // Wrap the instance
                    var $wrapper = $('<div />', {
                        'id': settings.id,
                        'class': 'uploadify',
                        'css': {
                            'height': settings.height + 'px',
                            'width': settings.width + 'px'
                        }
                    });
                    $('#' + swfuploadify.movieName).wrap($wrapper);
                    // Recreate the reference to wrapper
                    $wrapper = $('#' + settings.id);
                    // Add the data object to the wrapper 
                    $wrapper.data('uploadify', swfuploadify);

                    // Create the button
                    var $button = $('<div />', {
                        'id': settings.id + '-button',
                        'class': 'uploadify-button ' + settings.buttonClass
                    });
                    if (settings.buttonImage) {
                        $button.css({
                            'background-image': "url('" + settings.buttonImage + "')",
                            'text-indent': '-9999px'
                        });
                    }
                    $button.html('<span class="uploadify-button-text">' + settings.buttonText + '</span>')
					.css({
					    'height': settings.height + 'px',
					    'line-height': settings.height + 'px',
					    'width': settings.width + 'px'
					});
                    // Append the button to the wrapper
                    $wrapper.append($button);

                    // Adjust the styles of the movie
                    $('#' + swfuploadify.movieName).css({
                        'position': 'absolute',
                        'z-index': 1
                    });

                    // Create the file queue
                    if (!settings.queueID) {
                        var $queue = $('<div />', {
                            'id': settings.id + '-queue',
                            'class': 'uploadify-queue'
                        });
                        $wrapper.after($queue);
                        swfuploadify.settings.queueID = settings.id + '-queue';
                        swfuploadify.settings.defaultQueue = true;
                    }

                    // Create some queue related objects and variables
                    swfuploadify.queueData = {
                        files: {}, // The files in the queue
                        filesSelected: 0, // The number of files selected in the last select operation
                        filesQueued: 0, // The number of files added to the queue in the last select operation
                        filesReplaced: 0, // The number of files replaced in the last select operation
                        filesCancelled: 0, // The number of files that were cancelled instead of replaced
                        filesErrored: 0, // The number of files that caused error in the last select operation
                        uploadsSuccessful: 0, // The number of files that were successfully uploaded
                        uploadsErrored: 0, // The number of files that returned errors during upload
                        averageSpeed: 0, // The average speed of the uploads in KB
                        queueLength: 0, // The number of files in the queue
                        queueSize: 0, // The size in bytes of the entire queue
                        uploadSize: 0, // The size in bytes of the upload queue
                        queueBytesUploaded: 0, // The size in bytes that have been uploaded for the current upload queue
                        uploadQueue: [], // The files currently to be uploaded
                        errorMsg: 'Some files were not added to the queue:'
                    };

                    // Save references to all the objects
                    swfuploadify.original = $clone;
                    swfuploadify.wrapper = $wrapper;
                    swfuploadify.button = $button;
                    swfuploadify.queue = $queue;

                    // Call the user-defined init event handler
                    if (settings.onInit) settings.onInit.call($this, swfuploadify);

                } else {

                    // Call the fallback function
                    if (settings.onFallback) settings.onFallback.call($this);

                }
            });

        },

        // Stop a file upload and remove it from the queue 
        cancel: function (fileID, supressEvent) {

            var args = arguments;

            this.each(function () {
                // Create a reference to the jQuery DOM object
                var $this = $(this),
					swfuploadify = $this.data('uploadify'),
					settings = swfuploadify.settings,
					delay = -1;

                if (args[0]) {
                    // Clear the queue
                    if (args[0] == '*') {
                        var queueItemCount = swfuploadify.queueData.queueLength;
                        $('#' + settings.queueID).find('.uploadify-queue-item').each(function () {
                            delay++;
                            if (args[1] === true) {
                                swfuploadify.cancelUpload($(this).attr('id'), false);
                            } else {
                                swfuploadify.cancelUpload($(this).attr('id'));
                            }
                            $(this).find('.data').removeClass('data').html(' - Cancelled');
                            $(this).find('.uploadify-progress-bar').remove();
                            $(this).delay(1000 + 100 * delay).fadeOut(500, function () {
                                $(this).remove();
                            });
                        });
                        swfuploadify.queueData.queueSize = 0;
                        swfuploadify.queueData.queueLength = 0;
                        // Trigger the onClearQueue event
                        if (settings.onClearQueue) settings.onClearQueue.call($this, queueItemCount);
                    } else {
                        for (var n = 0; n < args.length; n++) {
                            swfuploadify.cancelUpload(args[n]);
                            $('#' + args[n]).find('.data').removeClass('data').html(' - Cancelled');
                            $('#' + args[n]).find('.uploadify-progress-bar').remove();
                            $('#' + args[n]).delay(1000 + 100 * n).fadeOut(500, function () {
                                $(this).remove();
                            });
                        }
                    }
                } else {
                    var item = $('#' + settings.queueID).find('.uploadify-queue-item').get(0);
                    $item = $(item);
                    swfuploadify.cancelUpload($item.attr('id'));
                    $item.find('.data').removeClass('data').html(' - Cancelled');
                    $item.find('.uploadify-progress-bar').remove();
                    $item.delay(1000).fadeOut(500, function () {
                        $(this).remove();
                    });
                }
            });

        },

        // Revert the DOM object back to its original state
        destroy: function () {

            this.each(function () {
                // Create a reference to the jQuery DOM object
                var $this = $(this),
					swfuploadify = $this.data('uploadify'),
					settings = swfuploadify.settings;

                // Destroy the SWF object and 
                swfuploadify.destroy();

                // Destroy the queue
                if (settings.defaultQueue) {
                    $('#' + settings.queueID).remove();
                }

                // Reload the original DOM element
                $('#' + settings.id).replaceWith(swfuploadify.original);

                // Call the user-defined event handler
                if (settings.onDestroy) settings.onDestroy.call(this);

                delete swfuploadify;
            });

        },

        // Disable the select button
        disable: function (isDisabled) {

            this.each(function () {
                // Create a reference to the jQuery DOM object
                var $this = $(this),
					swfuploadify = $this.data('uploadify'),
					settings = swfuploadify.settings;

                // Call the user-defined event handlers
                if (isDisabled) {
                    swfuploadify.button.addClass('disabled');
                    if (settings.onDisable) settings.onDisable.call(this);
                } else {
                    swfuploadify.button.removeClass('disabled');
                    if (settings.onEnable) settings.onEnable.call(this);
                }

                // Enable/disable the browse button
                swfuploadify.setButtonDisabled(isDisabled);
            });

        },

        // Get or set the settings data
        settings: function (name, value, resetObjects) {

            var args = arguments;
            var returnValue = value;

            this.each(function () {
                // Create a reference to the jQuery DOM object
                var $this = $(this),
					swfuploadify = $this.data('uploadify'),
					settings = swfuploadify.settings;

                if (typeof (args[0]) == 'object') {
                    for (var n in value) {
                        setData(n, value[n]);
                    }
                }
                if (args.length === 1) {
                    returnValue = settings[name];
                } else {
                    switch (name) {
                        case 'uploader':
                            swfuploadify.setUploadURL(value);
                            break;
                        case 'formData':
                            if (!resetObjects) {
                                value = $.extend(settings.formData, value);
                            }
                            swfuploadify.setPostParams(settings.formData);
                            break;
                        case 'method':
                            if (value == 'get') {
                                swfuploadify.setUseQueryString(true);
                            } else {
                                swfuploadify.setUseQueryString(false);
                            }
                            break;
                        case 'fileObjName':
                            swfuploadify.setFilePostName(value);
                            break;
                        case 'fileTypeExts':
                            swfuploadify.setFileTypes(value, settings.fileTypeDesc);
                            break;
                        case 'fileTypeDesc':
                            swfuploadify.setFileTypes(settings.fileTypeExts, value);
                            break;
                        case 'fileSizeLimit':
                            swfuploadify.setFileSizeLimit(value);
                            break;
                        case 'uploadLimit':
                            swfuploadify.setFileUploadLimit(value);
                            break;
                        case 'queueSizeLimit':
                            swfuploadify.setFileQueueLimit(value);
                            break;
                        case 'buttonImage':
                            swfuploadify.button.css('background-image', settingValue);
                            break;
                        case 'buttonCursor':
                            if (value == 'arrow') {
                                swfuploadify.setButtonCursor(SWFUpload.CURSOR.ARROW);
                            } else {
                                swfuploadify.setButtonCursor(SWFUpload.CURSOR.HAND);
                            }
                            break;
                        case 'buttonText':
                            $('#' + settings.id + '-button').find('.uploadify-button-text').html(value);
                            break;
                        case 'width':
                            swfuploadify.setButtonDimensions(value, settings.height);
                            break;
                        case 'height':
                            swfuploadify.setButtonDimensions(settings.width, value);
                            break;
                        case 'multi':
                            if (value) {
                                swfuploadify.setButtonAction(SWFUpload.BUTTON_ACTION.SELECT_FILES);
                            } else {
                                swfuploadify.setButtonAction(SWFUpload.BUTTON_ACTION.SELECT_FILE);
                            }
                            break;
                    }
                    settings[name] = value;
                }
            });

            if (args.length === 1) {
                return returnValue;
            }

        },

        // Stop the current uploads and requeue what is in progress
        stop: function () {

            this.each(function () {
                // Create a reference to the jQuery DOM object
                var $this = $(this),
					swfuploadify = $this.data('uploadify');

                // Reset the queue information
                swfuploadify.queueData.averageSpeed = 0;
                swfuploadify.queueData.uploadSize = 0;
                swfuploadify.queueData.bytesUploaded = 0;
                swfuploadify.queueData.uploadQueue = [];

                swfuploadify.stopUpload();
            });

        },

        // Start uploading files in the queue
        upload: function () {

            var args = arguments;

            this.each(function () {
                // Create a reference to the jQuery DOM object
                var $this = $(this),
					swfuploadify = $this.data('uploadify');

                // Reset the queue information
                swfuploadify.queueData.averageSpeed = 0;
                swfuploadify.queueData.uploadSize = 0;
                swfuploadify.queueData.bytesUploaded = 0;
                swfuploadify.queueData.uploadQueue = [];

                // Upload the files
                if (args[0]) {
                    if (args[0] == '*') {
                        swfuploadify.queueData.uploadSize = swfuploadify.queueData.queueSize;
                        swfuploadify.queueData.uploadQueue.push('*');
                        swfuploadify.startUpload();
                    } else {
                        for (var n = 0; n < args.length; n++) {
                            swfuploadify.queueData.uploadSize += swfuploadify.queueData.files[args[n]].size;
                            swfuploadify.queueData.uploadQueue.push(args[n]);
                        }
                        swfuploadify.startUpload(swfuploadify.queueData.uploadQueue.shift());
                    }
                } else {
                    swfuploadify.startUpload();
                }

            });

        }

    }

    // These functions handle all the events that occur with the file uploader
    var handlers = {

        // Triggered when the file dialog is opened
        onDialogOpen: function () {
            // Load the swfupload settings
            var settings = this.settings;

            // Reset some queue info
            this.queueData.errorMsg = 'Some files were not added to the queue:';
            this.queueData.filesReplaced = 0;
            this.queueData.filesCancelled = 0;

            // Call the user-defined event handler
            if (settings.onDialogOpen) settings.onDialogOpen.call(this);
        },

        // Triggered when the browse dialog is closed
        onDialogClose: function (filesSelected, filesQueued, queueLength) {
            // Load the swfupload settings
            var settings = this.settings;

            // Update the queue information
            this.queueData.filesErrored = filesSelected - filesQueued;
            this.queueData.filesSelected = filesSelected;
            this.queueData.filesQueued = filesQueued - this.queueData.filesCancelled;
            this.queueData.queueLength = queueLength;

            // Run the default event handler
            if ($.inArray('onDialogClose', settings.overrideEvents) < 0) {
                if (this.queueData.filesErrored > 0) {
                    alert(this.queueData.errorMsg);
                }
            }

            // Call the user-defined event handler
            if (settings.onDialogClose) settings.onDialogClose.call(this, this.queueData);

            // Upload the files if auto is true
            if (settings.auto) $('#' + settings.id).uploadify('upload', '*');
        },

        // Triggered once for each file added to the queue
        onSelect: function (file) {
            // Load the swfupload settings
            var settings = this.settings;

            // Check if a file with the same name exists in the queue
            var queuedFile = {};
            for (var n in this.queueData.files) {
                queuedFile = this.queueData.files[n];
                if (queuedFile.uploaded != true && queuedFile.name == file.name) {
                    var replaceQueueItem = confirm('The file named "' + file.name + '" is already in the queue.\nDo you want to replace the existing item in the queue?');
                    if (!replaceQueueItem) {
                        this.cancelUpload(file.id);
                        this.queueData.filesCancelled++;
                        return false;
                    } else {
                        $('#' + queuedFile.id).remove();
                        this.cancelUpload(queuedFile.id);
                        this.queueData.filesReplaced++;
                    }
                }
            }

            // Get the size of the file
            var fileSize = Math.round(file.size / 1024);
            var suffix = 'KB';
            if (fileSize > 1000) {
                fileSize = Math.round(fileSize / 1000);
                suffix = 'MB';
            }
            var fileSizeParts = fileSize.toString().split('.');
            fileSize = fileSizeParts[0];
            if (fileSizeParts.length > 1) {
                fileSize += '.' + fileSizeParts[1].substr(0, 2);
            }
            fileSize += suffix;

            // Truncate the filename if it's too long
            var fileName = file.name;
            if (fileName.length > 25) {
                fileName = fileName.substr(0, 25) + '...';
            }

            // Create the file data object
            itemData = {
                'fileID': file.id,
                'instanceID': settings.id,
                'fileName': fileName,
                'fileSize': fileSize
            }

            // Create the file item template
            if (settings.itemTemplate == false) {
                settings.itemTemplate = '<div id="${fileID}" class="uploadify-queue-item">\
					<div class="cancel">\
						<a href="javascript:$(\'#${instanceID}\').uploadify(\'cancel\', \'${fileID}\')">X</a>\
					</div>\
					<span class="fileName">${fileName} (${fileSize})</span><span class="data"></span>\
					<div class="uploadify-progress">\
						<div class="uploadify-progress-bar"><!--Progress Bar--></div>\
					</div>\
				</div>';
            }

            // Run the default event handler
            if ($.inArray('onSelect', settings.overrideEvents) < 0) {

                // Replace the item data in the template
                itemHTML = settings.itemTemplate;
                for (var d in itemData) {
                    itemHTML = itemHTML.replace(new RegExp('\\$\\{' + d + '\\}', 'g'), itemData[d]);
                }

                // Add the file item to the queue
                $('#' + settings.queueID).append(itemHTML);
            }

            this.queueData.queueSize += file.size;
            this.queueData.files[file.id] = file;

            // Call the user-defined event handler
            if (settings.onSelect) settings.onSelect.apply(this, arguments);
        },

        // Triggered when a file is not added to the queue
        onSelectError: function (file, errorCode, errorMsg) {
            // Load the swfupload settings
            var settings = this.settings;

            // Run the default event handler
            if ($.inArray('onSelectError', settings.overrideEvents) < 0) {
                switch (errorCode) {
                    case SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED:
                        if (settings.queueSizeLimit > errorMsg) {
                            this.queueData.errorMsg += '\nThe number of files selected exceeds the remaining upload limit (' + errorMsg + ').';
                        } else {
                            this.queueData.errorMsg += '\nThe number of files selected exceeds the queue size limit (' + settings.queueSizeLimit + ').';
                        }
                        break;
                    case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:
                        this.queueData.errorMsg += '\nThe file "' + file.name + '" exceeds the size limit (' + settings.fileSizeLimit + ').';
                        break;
                    case SWFUpload.QUEUE_ERROR.ZERO_BYTE_FILE:
                        this.queueData.errorMsg += '\nThe file "' + file.name + '" is empty.';
                        break;
                    case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:
                        this.queueData.errorMsg += '\nThe file "' + file.name + '" is not an accepted file type (' + settings.fileTypeDesc + ').';
                        break;
                }
            }
            if (errorCode != SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED) {
                delete this.queueData.files[file.id];
            }

            // Call the user-defined event handler
            if (settings.onSelectError) settings.onSelectError.apply(this, arguments);
        },

        // Triggered when all the files in the queue have been processed
        onQueueComplete: function () {
            if (this.settings.onQueueComplete) this.settings.onQueueComplete.call(this, this.settings.queueData);
        },

        // Triggered when a file upload successfully completes
        onUploadComplete: function (file) {
            // Load the swfupload settings
            var settings = this.settings,
				swfuploadify = this;

            // Check if all the files have completed uploading
            var stats = this.getStats();
            this.queueData.queueLength = stats.files_queued;
            if (this.queueData.uploadQueue[0] == '*') {
                if (this.queueData.queueLength > 0) {
                    this.startUpload();
                } else {
                    this.queueData.uploadQueue = [];

                    // Call the user-defined event handler for queue complete
                    if (settings.onQueueComplete) settings.onQueueComplete.call(this, this.queueData);
                }
            } else {
                if (this.queueData.uploadQueue.length > 0) {
                    this.startUpload(this.queueData.uploadQueue.shift());
                } else {
                    this.queueData.uploadQueue = [];

                    // Call the user-defined event handler for queue complete
                    if (settings.onQueueComplete) settings.onQueueComplete.call(this, this.queueData);
                }
            }

            // Call the default event handler
            if ($.inArray('onUploadComplete', settings.overrideEvents) < 0) {
                if (settings.removeCompleted) {
                    switch (file.filestatus) {
                        case SWFUpload.FILE_STATUS.COMPLETE:
                            setTimeout(function () {
                                if ($('#' + file.id)) {
                                    swfuploadify.queueData.queueSize -= file.size;
                                    swfuploadify.queueData.queueLength -= 1;
                                    delete swfuploadify.queueData.files[file.id]
                                    $('#' + file.id).fadeOut(500, function () {
                                        $(this).remove();
                                    });
                                }
                            }, settings.removeTimeout * 1000);
                            break;
                        case SWFUpload.FILE_STATUS.ERROR:
                            if (!settings.requeueErrors) {
                                setTimeout(function () {
                                    if ($('#' + file.id)) {
                                        swfuploadify.queueData.queueSize -= file.size;
                                        swfuploadify.queueData.queueLength -= 1;
                                        delete swfuploadify.queueData.files[file.id];
                                        $('#' + file.id).fadeOut(500, function () {
                                            $(this).remove();
                                        });
                                    }
                                }, settings.removeTimeout * 1000);
                            }
                            break;
                    }
                } else {
                    file.uploaded = true;
                }
            }

            // Call the user-defined event handler
            if (settings.onUploadComplete) settings.onUploadComplete.call(this, file);
        },

        // Triggered when a file upload returns an error
        onUploadError: function (file, errorCode, errorMsg) {
            // Load the swfupload settings
            var settings = this.settings;

            // Set the error string
            var errorString = 'Error';
            switch (errorCode) {
                case SWFUpload.UPLOAD_ERROR.HTTP_ERROR:
                    errorString = 'HTTP Error (' + errorMsg + ')';
                    break;
                case SWFUpload.UPLOAD_ERROR.MISSING_UPLOAD_URL:
                    errorString = 'Missing Upload URL';
                    break;
                case SWFUpload.UPLOAD_ERROR.IO_ERROR:
                    errorString = 'IO Error';
                    break;
                case SWFUpload.UPLOAD_ERROR.SECURITY_ERROR:
                    errorString = 'Security Error';
                    break;
                case SWFUpload.UPLOAD_ERROR.UPLOAD_LIMIT_EXCEEDED:
                    alert('The upload limit has been reached (' + errorMsg + ').');
                    errorString = 'Exceeds Upload Limit';
                    break;
                case SWFUpload.UPLOAD_ERROR.UPLOAD_FAILED:
                    errorString = 'Failed';
                    break;
                case SWFUpload.UPLOAD_ERROR.SPECIFIED_FILE_ID_NOT_FOUND:
                    break;
                case SWFUpload.UPLOAD_ERROR.FILE_VALIDATION_FAILED:
                    errorString = 'Validation Error';
                    break;
                case SWFUpload.UPLOAD_ERROR.FILE_CANCELLED:
                    errorString = 'Cancelled';
                    this.queueData.queueSize -= file.size;
                    this.queueData.queueLength -= 1;
                    if (file.status == SWFUpload.FILE_STATUS.IN_PROGRESS || $.inArray(file.id, this.queueData.uploadQueue) >= 0) {
                        this.queueData.uploadSize -= file.size;
                    }
                    // Trigger the onCancel event
                    if (settings.onCancel) settings.onCancel.call(this, file);
                    delete this.queueData.files[file.id];
                    break;
                case SWFUpload.UPLOAD_ERROR.UPLOAD_STOPPED:
                    errorString = 'Stopped';
                    break;
            }

            // Call the default event handler
            if ($.inArray('onUploadError', settings.overrideEvents) < 0) {

                if (errorCode != SWFUpload.UPLOAD_ERROR.FILE_CANCELLED && errorCode != SWFUpload.UPLOAD_ERROR.UPLOAD_STOPPED) {
                    $('#' + file.id).addClass('uploadify-error');
                }

                // Reset the progress bar
                $('#' + file.id).find('.uploadify-progress-bar').css('width', '1px');

                // Add the error message to the queue item
                if (errorCode != SWFUpload.UPLOAD_ERROR.SPECIFIED_FILE_ID_NOT_FOUND && file.status != SWFUpload.FILE_STATUS.COMPLETE) {
                    $('#' + file.id).find('.data').html(' - ' + errorString);
                }
            }

            var stats = this.getStats();
            this.queueData.uploadsErrored = stats.upload_errors;

            // Call the user-defined event handler
            if (settings.onUploadError) settings.onUploadError.call(this, file, errorCode, errorMsg, errorString);
        },

        // Triggered periodically during a file upload
        onUploadProgress: function (file, fileBytesLoaded, fileTotalBytes) {
            // Load the swfupload settings
            var settings = this.settings;

            // Setup all the variables
            var timer = new Date();
            var newTime = timer.getTime();
            var lapsedTime = newTime - this.timer;
            if (lapsedTime > 500) {
                this.timer = newTime;
            }
            var lapsedBytes = fileBytesLoaded - this.bytesLoaded;
            this.bytesLoaded = fileBytesLoaded;
            var queueBytesLoaded = this.queueData.queueBytesUploaded + fileBytesLoaded;
            var percentage = Math.round(fileBytesLoaded / fileTotalBytes * 100);

            // Calculate the average speed
            var suffix = 'KB/s';
            var mbs = 0;
            var kbs = (lapsedBytes / 1024) / (lapsedTime / 1000);
            kbs = Math.floor(kbs * 10) / 10;
            if (this.queueData.averageSpeed > 0) {
                this.queueData.averageSpeed = Math.floor((this.queueData.averageSpeed + kbs) / 2);
            } else {
                this.queueData.averageSpeed = Math.floor(kbs);
            }
            if (kbs > 1000) {
                mbs = (kbs * .001);
                this.queueData.averageSpeed = Math.floor(mbs);
                suffix = 'MB/s';
            }

            // Call the default event handler
            if ($.inArray('onUploadProgress', settings.overrideEvents) < 0) {
                if (settings.progressData == 'percentage') {
                    $('#' + file.id).find('.data').html(' - ' + percentage + '%');
                } else if (settings.progressData == 'speed' && lapsedTime > 500) {
                    $('#' + file.id).find('.data').html(' - ' + this.queueData.averageSpeed + suffix);
                }
                $('#' + file.id).find('.uploadify-progress-bar').css('width', percentage + '%');
            }

            // Call the user-defined event handler
            if (settings.onUploadProgress) settings.onUploadProgress.call(this, file, fileBytesLoaded, fileTotalBytes, queueBytesLoaded, this.queueData.uploadSize);
        },

        // Triggered right before a file is uploaded
        onUploadStart: function (file) {
            // Load the swfupload settings
            var settings = this.settings;

            var timer = new Date();
            this.timer = timer.getTime();
            this.bytesLoaded = 0;
            if (this.queueData.uploadQueue.length == 0) {
                this.queueData.uploadSize = file.size;
            }
            if (settings.checkExisting) {
                $.ajax({
                    type: 'POST',
                    async: false,
                    url: settings.checkExisting,
                    data: { filename: file.name },
                    success: function (data) {
                        if (data == 1) {
                            var overwrite = confirm('A file with the name "' + file.name + '" already exists on the server.\nWould you like to replace the existing file?');
                            if (!overwrite) {
                                this.cancelUpload(file.id);
                                $('#' + file.id).remove();
                                if (this.queueData.uploadQueue.length > 0 && this.queueData.queueLength > 0) {
                                    if (this.queueData.uploadQueue[0] == '*') {
                                        this.startUpload();
                                    } else {
                                        this.startUpload(this.queueData.uploadQueue.shift());
                                    }
                                }
                            }
                        }
                    }
                });
            }

            // Call the user-defined event handler
            if (settings.onUploadStart) settings.onUploadStart.call(this, file);
        },

        // Triggered when a file upload returns a successful code
        onUploadSuccess: function (file, data, response) {
            // Load the swfupload settings
            var settings = this.settings;
            var stats = this.getStats();
            this.queueData.uploadsSuccessful = stats.successful_uploads;
            this.queueData.queueBytesUploaded += file.size;

            // Call the default event handler
            if ($.inArray('onUploadSuccess', settings.overrideEvents) < 0) {
                $('#' + file.id).find('.data').html(' - Complete');
            }

            // Call the user-defined event handler
            if (settings.onUploadSuccess) settings.onUploadSuccess.call(this, file, data, response);
        }

    }

    $.fn.uploadify = function (method) {

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('The method ' + method + ' does not exist in $.uploadify');
        }

    }

})($);

/*
* outerHtml
* https://github.com/JamesMGreene/jquery.outerHtml
*
* Copyright (c) 2013 James M. Greene
* Licensed under the MIT license.
*/

(function ($) {
    'use strict';


    var hasNativeOuterHTML = !!('outerHTML' in $('<div></div>').get(0));

    // Prefer the native `outerHTML` property when possible
    var getterFn = function () {
        var target = this.get(0);

        // If the browser supports the `outerHTML` property on elements AND if `target` is an element node
        if (hasNativeOuterHTML && target.nodeType === 1) {
            return target.outerHTML;
        }
        else {
            return $('<div></div>').append(this.eq(0).clone()).html();
        }
    };

    var setterFn = function (value) {
        // Do not attempt to replace anything using the native `outerHTML` property setter
        // even if it exists: it is riddled with bugs!
        return $('<div id="jquery-outerHtml-transformer"></div>').append(value).contents().replaceAll(this);
    };

    // Detect jQuery 1.8.x bug (for which the value here is `false`)
    var doesNotLeaveTempParentOnDetachedDomElement = true;

    $.fn.outerHtml = function (value) {
        if (arguments.length) {
            if (doesNotLeaveTempParentOnDetachedDomElement) {
                return setterFn.call(this, value);
            }
            else {
                // Fix for jQuery 1.8.x bug: https://github.com/JamesMGreene/jquery.outerHtml/issues/1
                var parentsOfThis = (function () {
                    var parents = new Array(this.length);
                    this.each(function (i) {
                        parents[i] = this.parentNode || null;
                    });
                    return parents;
                }).call(this);

                return setterFn.call(this, value).map(function (i) {
                    if (!parentsOfThis[i]) {
                        if (this.parentNode) {
                            return this.parentNode.removeChild(this);
                        }
                    }
                    else if (parentsOfThis[i] !== this.parentNode) {
                        // Appending to the end: this doesn't seem right but it should cover the detached DOM scenarios
                        return parentsOfThis[i].appendChild(this);
                    }
                    return this;
                });
            }
        }
        else {
            return getterFn.call(this);
        }
    };

    // Detect jQuery 1.8.x bug (for which the value here is `false`)
    doesNotLeaveTempParentOnDetachedDomElement = (function () {
        var parent = $('<s>bad</s>').outerHtml('<div>good</div>').get(0).parentNode;
        return (parent.nodeName === '#document-fragment' && parent.nodeType === 11);
    })();

} (jQuery));

/*
 * jQuery treegrid Plugin 0.3.0
 * https://github.com/maxazan/jquery-treegrid
 *
 * Copyright 2013, Pomazan Max
 * Licensed under the MIT licenses.
 */
(function ($) {

    var methods = {
        /**
         * Initialize tree
         *
         * @param {Object} options
         * @returns {Object[]}
         */
        initTree: function (options) {
            var settings = $.extend({}, this.treegrid.defaults, options);
            return this.each(function () {
                var $this = $(this);
                $this.treegrid('setTreeContainer', $(this));
                $this.treegrid('setSettings', settings);
                settings.getRootNodes.apply(this, [$(this)]).treegrid('initNode', settings);
                $this.treegrid('getRootNodes').treegrid('render');
            });
        },
        /**
         * Initialize node
         *
         * @param {Object} settings
         * @returns {Object[]}
         */
        initNode: function (settings) {
            return this.each(function () {
                var $this = $(this);
                $this.treegrid('setTreeContainer', settings.getTreeGridContainer.apply(this));
                $this.treegrid('getChildNodes').treegrid('initNode', settings);
                $this.treegrid('initExpander').treegrid('initIndent').treegrid('initEvents').treegrid('initState').treegrid('initChangeEvent').treegrid("initSettingsEvents");
            });
        },
        initChangeEvent: function () {
            var $this = $(this);
            //Save state on change
            $this.on("change", function () {
                var $this = $(this);
                $this.treegrid('render');
                if ($this.treegrid('getSetting', 'saveState')) {
                    $this.treegrid('saveState');
                }
            });
            return $this;
        },
        /**
         * Initialize node events
         *
         * @returns {Node}
         */
        initEvents: function () {
            var $this = $(this);
            //Default behavior on collapse
            $this.on("collapse", function () {
                var $this = $(this);
                $this.removeClass('treegrid-expanded');
                $this.addClass('treegrid-collapsed');
            });
            //Default behavior on expand
            $this.on("expand", function () {
                var $this = $(this);
                $this.removeClass('treegrid-collapsed');
                $this.addClass('treegrid-expanded');
            });

            return $this;
        },
        /**
         * Initialize events from settings
         *
         * @returns {Node}
         */
        initSettingsEvents: function () {
            var $this = $(this);
            //Save state on change
            $this.on("change", function () {
                var $this = $(this);
                if (typeof ($this.treegrid('getSetting', 'onChange')) === "function") {
                    $this.treegrid('getSetting', 'onChange').apply($this);
                }
            });
            //Default behavior on collapse
            $this.on("collapse", function () {
                var $this = $(this);
                if (typeof ($this.treegrid('getSetting', 'onCollapse')) === "function") {
                    $this.treegrid('getSetting', 'onCollapse').apply($this);
                }
            });
            //Default behavior on expand
            $this.on("expand", function () {
                var $this = $(this);
                if (typeof ($this.treegrid('getSetting', 'onExpand')) === "function") {
                    $this.treegrid('getSetting', 'onExpand').apply($this);
                }

            });

            return $this;
        },
        /**
         * Initialize expander for node
         *
         * @returns {Node}
         */
        initExpander: function () {
            var $this = $(this);
            var cell = $this.find('td').get($this.treegrid('getSetting', 'treeColumn'));
            var tpl = $this.treegrid('getSetting', 'expanderTemplate');
            var expander = $this.treegrid('getSetting', 'getExpander').apply(this);
            if (expander) {
                expander.remove();
            }
            $(tpl).prependTo(cell).click(function () {
                $($(this).closest('tr')).treegrid('toggle');
            });
            return $this;
        },
        /**
         * Initialize indent for node
         *
         * @returns {Node}
         */
        initIndent: function () {
            var $this = $(this);
            $this.find('.treegrid-indent').remove();
            var tpl = $this.treegrid('getSetting', 'indentTemplate');
            var expander = $this.find('.treegrid-expander');
            var depth = $this.treegrid('getDepth');
            for (var i = 0; i < depth; i++) {
                $(tpl).insertBefore(expander);
            }
            return $this;
        },
        /**
         * Initialise state of node
         *
         * @returns {Node}
         */
        initState: function () {
            var $this = $(this);
            if ($this.treegrid('getSetting', 'saveState') && !$this.treegrid('isFirstInit')) {
                $this.treegrid('restoreState');
            } else {
                if ($this.treegrid('getSetting', 'initialState') === "expanded") {
                    $this.treegrid('expand');
                } else {
                    $this.treegrid('collapse');
                }
            }
            return $this;
        },
        /**
         * Return true if this tree was never been initialised
         *
         * @returns {Boolean}
         */
        isFirstInit: function () {
            var tree = $(this).treegrid('getTreeContainer');
            if (tree.data('first_init') === undefined) {
                tree.data('first_init', $.cookie(tree.treegrid('getSetting', 'saveStateName')) === undefined);
            }
            return tree.data('first_init');
        },
        /**
         * Save state of current node
         *
         * @returns {Node}
         */
        saveState: function () {
            var $this = $(this);
            if ($this.treegrid('getSetting', 'saveStateMethod') === 'cookie') {

                var stateArrayString = $.cookie($this.treegrid('getSetting', 'saveStateName')) || '';
                var stateArray = (stateArrayString === '' ? [] : stateArrayString.split(','));
                var nodeId = $this.treegrid('getNodeId');

                if ($this.treegrid('isExpanded')) {
                    if ($.inArray(nodeId, stateArray) === -1) {
                        stateArray.push(nodeId);
                    }
                } else if ($this.treegrid('isCollapsed')) {
                    if ($.inArray(nodeId, stateArray) !== -1) {
                        stateArray.splice($.inArray(nodeId, stateArray), 1);
                    }
                }
                $.cookie($this.treegrid('getSetting', 'saveStateName'), stateArray.join(','));
            }
            return $this;
        },
        /**
         * Restore state of current node.
         *
         * @returns {Node}
         */
        restoreState: function () {
            var $this = $(this);
            if ($this.treegrid('getSetting', 'saveStateMethod') === 'cookie') {
                var stateArray = $.cookie($this.treegrid('getSetting', 'saveStateName')).split(',');
                if ($.inArray($this.treegrid('getNodeId'), stateArray) !== -1) {
                    $this.treegrid('expand');
                } else {
                    $this.treegrid('collapse');
                }

            }
            return $this;
        },
        /**
         * Method return setting by name
         *
         * @param {type} name
         * @returns {unresolved}
         */
        getSetting: function (name) {
            if (!$(this).treegrid('getTreeContainer')) {
                return null;
            }
            return $(this).treegrid('getTreeContainer').data('settings')[name];
        },
        /**
         * Add new settings
         *
         * @param {Object} settings
         */
        setSettings: function (settings) {
            $(this).treegrid('getTreeContainer').data('settings', settings);
        },
        /**
         * Return tree container
         *
         * @returns {HtmlElement}
         */
        getTreeContainer: function () {
            return $(this).data('treegrid');
        },
        /**
         * Set tree container
         *
         * @param {HtmlE;ement} container
         */
        setTreeContainer: function (container) {
            return $(this).data('treegrid', container);
        },
        /**
         * Method return all root nodes of tree.
         *
         * Start init all child nodes from it.
         *
         * @returns {Array}
         */
        getRootNodes: function () {
            return $(this).treegrid('getSetting', 'getRootNodes').apply(this, [$(this).treegrid('getTreeContainer')]);
        },
        /**
         * Method return all nodes of tree.
         *
         * @returns {Array}
         */
        getAllNodes: function () {
            return $(this).treegrid('getSetting', 'getAllNodes').apply(this, [$(this).treegrid('getTreeContainer')]);
        },
        /**
         * Mthod return true if element is Node
         *
         * @returns {String}
         */
        isNode: function () {
            return $(this).treegrid('getNodeId') !== null;
        },
        /**
         * Mthod return id of node
         *
         * @returns {String}
         */
        getNodeId: function () {
            if ($(this).treegrid('getSetting', 'getNodeId') === null) {
                return null;
            } else {
                return $(this).treegrid('getSetting', 'getNodeId').apply(this);
            }
        },
        /**
         * Method return parent id of node or null if root node
         *
         * @returns {String}
         */
        getParentNodeId: function () {
            return $(this).treegrid('getSetting', 'getParentNodeId').apply(this);
        },
        /**
         * Method return parent node or null if root node
         *
         * @returns {Object[]}
         */
        getParentNode: function () {
            if ($(this).treegrid('getParentNodeId') === null) {
                return null;
            } else {
                return $(this).treegrid('getSetting', 'getNodeById').apply(this, [$(this).treegrid('getParentNodeId'), $(this).treegrid('getTreeContainer')]);
            }
        },
        /**
         * Method return array of child nodes or null if node is leaf
         *
         * @returns {Object[]}
         */
        getChildNodes: function () {
            return $(this).treegrid('getSetting', 'getChildNodes').apply(this, [$(this).treegrid('getNodeId'), $(this).treegrid('getTreeContainer')]);
        },
        /**
         * Method return depth of tree.
         *
         * This method is needs for calculate indent
         *
         * @returns {Number}
         */
        getDepth: function () {
            if ($(this).treegrid('getParentNode') === null) {
                return 0;
            }
            return $(this).treegrid('getParentNode').treegrid('getDepth') + 1;
        },
        /**
         * Method return true if node is root
         *
         * @returns {Boolean}
         */
        isRoot: function () {
            return $(this).treegrid('getDepth') === 0;
        },
        /**
         * Method return true if node has no child nodes
         *
         * @returns {Boolean}
         */
        isLeaf: function () {
            return $(this).treegrid('getChildNodes').length === 0;
        },
        /**
         * Method return true if node last in branch
         *
         * @returns {Boolean}
         */
        isLast: function () {
            if ($(this).treegrid('isNode')) {
                var parentNode = $(this).treegrid('getParentNode');
                if (parentNode === null) {
                    if ($(this).treegrid('getNodeId') === $(this).treegrid('getRootNodes').last().treegrid('getNodeId')) {
                        return true;
                    }
                } else {
                    if ($(this).treegrid('getNodeId') === parentNode.treegrid('getChildNodes').last().treegrid('getNodeId')) {
                        return true;
                    }
                }
            }
            return false;
        },
        /**
         * Method return true if node first in branch
         *
         * @returns {Boolean}
         */
        isFirst: function () {
            if ($(this).treegrid('isNode')) {
                var parentNode = $(this).treegrid('getParentNode');
                if (parentNode === null) {
                    if ($(this).treegrid('getNodeId') === $(this).treegrid('getRootNodes').first().treegrid('getNodeId')) {
                        return true;
                    }
                } else {
                    if ($(this).treegrid('getNodeId') === parentNode.treegrid('getChildNodes').first().treegrid('getNodeId')) {
                        return true;
                    }
                }
            }
            return false;
        },
        /**
         * Return true if node expanded
         *
         * @returns {Boolean}
         */
        isExpanded: function () {
            return $(this).hasClass('treegrid-expanded');
        },
        /**
         * Return true if node collapsed
         *
         * @returns {Boolean}
         */
        isCollapsed: function () {
            return $(this).hasClass('treegrid-collapsed');
        },
        /**
         * Return true if at least one of parent node is collapsed
         *
         * @returns {Boolean}
         */
        isOneOfParentsCollapsed: function () {
            var $this = $(this);
            if ($this.treegrid('isRoot')) {
                return false;
            } else {
                if ($this.treegrid('getParentNode').treegrid('isCollapsed')) {
                    return true;
                } else {
                    return $this.treegrid('getParentNode').treegrid('isOneOfParentsCollapsed');
                }
            }
        },
        /**
         * Expand node
         *
         * @returns {Node}
         */
        expand: function () {
            if (!this.treegrid('isLeaf') && !this.treegrid("isExpanded")) {
                this.trigger("expand");
                this.trigger("change");
                return this;
            }
            return this;
        },
        /**
         * Expand all nodes
         *
         * @returns {Node}
         */
        expandAll: function () {
            var $this = $(this);
            $this.treegrid('getRootNodes').treegrid('expandRecursive');
            return $this;
        },
        /**
         * Expand current node and all child nodes begin from current
         *
         * @returns {Node}
         */
        expandRecursive: function () {
            return $(this).each(function () {
                var $this = $(this);
                $this.treegrid('expand');
                if (!$this.treegrid('isLeaf')) {
                    $this.treegrid('getChildNodes').treegrid('expandRecursive');
                }
            });
        },
        /**
         * Collapse node
         *
         * @returns {Node}
         */
        collapse: function () {
            return $(this).each(function () {
                var $this = $(this);
                if (!$this.treegrid('isLeaf') && !$this.treegrid("isCollapsed")) {
                    $this.trigger("collapse");
                    $this.trigger("change");
                }
            });
        },
        /**
         * Collapse all nodes
         *
         * @returns {Node}
         */
        collapseAll: function () {
            var $this = $(this);
            $this.treegrid('getRootNodes').treegrid('collapseRecursive');
            return $this;
        },
        /**
         * Collapse current node and all child nodes begin from current
         *
         * @returns {Node}
         */
        collapseRecursive: function () {
            return $(this).each(function () {
                var $this = $(this);
                $this.treegrid('collapse');
                if (!$this.treegrid('isLeaf')) {
                    $this.treegrid('getChildNodes').treegrid('collapseRecursive');
                }
            });
        },
        /**
         * Expand if collapsed, Collapse if expanded
         *
         * @returns {Node}
         */
        toggle: function () {
            var $this = $(this);
            if ($this.treegrid('isExpanded')) {
                $this.treegrid('collapse');
            } else {
                $this.treegrid('expand');
            }
            return $this;
        },
        /**
         * Rendering node
         *
         * @returns {Node}
         */
        render: function () {
            return $(this).each(function () {
                var $this = $(this);
                //if parent colapsed we hidden
                if ($this.treegrid('isOneOfParentsCollapsed')) {
                    $this.hide();
                } else {
                    $this.show();
                }
                if (!$this.treegrid('isLeaf')) {
                    $this.treegrid('renderExpander');
                    $this.treegrid('getChildNodes').treegrid('render');
                }
            });
        },
        /**
         * Rendering expander depends on node state
         *
         * @returns {Node}
         */
        renderExpander: function () {
            return $(this).each(function () {
                var $this = $(this);
                var expander = $this.treegrid('getSetting', 'getExpander').apply(this);
                if (expander) {

                    if (!$this.treegrid('isCollapsed')) {
                        expander.removeClass($this.treegrid('getSetting', 'expanderCollapsedClass'));
                        expander.addClass($this.treegrid('getSetting', 'expanderExpandedClass'));
                    } else {
                        expander.removeClass($this.treegrid('getSetting', 'expanderExpandedClass'));
                        expander.addClass($this.treegrid('getSetting', 'expanderCollapsedClass'));
                    }
                } else {
                    $this.treegrid('initExpander');
                    $this.treegrid('renderExpander');
                }
            });
        }
    };
    $.fn.treegrid = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.initTree.apply(this, arguments);
        } else {
            $.error('Method with name ' + method + ' does not exists for jQuery.treegrid');
        }
    };
    /**
     *  Plugin's default options
     */
    $.fn.treegrid.defaults = {
        initialState: 'expanded',
        saveState: false,
        saveStateMethod: 'cookie',
        saveStateName: 'tree-grid-state',
        expanderTemplate: '<span class="treegrid-expander"></span>',
        indentTemplate: '<span class="treegrid-indent"></span>',
        expanderExpandedClass: 'treegrid-expander-expanded',
        expanderCollapsedClass: 'treegrid-expander-collapsed',
        treeColumn: 0,
        getExpander: function () {
            return $(this).find('.treegrid-expander');
        },
        getNodeId: function () {
            var template = /treegrid-([A-Za-z0-9_-]+)/;
            if (template.test($(this).attr('class'))) {
                return template.exec($(this).attr('class'))[1];
            }
            return null;
        },
        getParentNodeId: function () {
            var template = /treegrid-parent-([A-Za-z0-9_-]+)/;
            if (template.test($(this).attr('class'))) {
                return template.exec($(this).attr('class'))[1];
            }
            return null;
        },
        getNodeById: function (id, treegridContainer) {
            var templateClass = "treegrid-" + id;
            return treegridContainer.find('tr.' + templateClass);
        },
        getChildNodes: function (id, treegridContainer) {
            var templateClass = "treegrid-parent-" + id;
            return treegridContainer.find('tr.' + templateClass);
        },
        getTreeGridContainer: function () {
            return $(this).closest('table');
        },
        getRootNodes: function (treegridContainer) {
            var result = $.grep(treegridContainer.find('tr'), function (element) {
                var classNames = $(element).attr('class');
                var templateClass = /treegrid-([A-Za-z0-9_-]+)/;
                var templateParentClass = /treegrid-parent-([A-Za-z0-9_-]+)/;
                return templateClass.test(classNames) && !templateParentClass.test(classNames);
            });
            return $(result);
        },
        getAllNodes: function (treegridContainer) {
            var result = $.grep(treegridContainer.find('tr'), function (element) {
                var classNames = $(element).attr('class');
                var templateClass = /treegrid-([A-Za-z0-9_-]+)/;
                return templateClass.test(classNames);
            });
            return $(result);
        },
        //Events
        onCollapse: null,
        onExpand: null,
        onChange: null

    };
})($jQuery1_9);
// Ion.Sound
// version 1.3.0 Build: 20
// © 2013 Denis Ineshin | IonDen.com
//
// Project page:    http://ionden.com/a/plugins/ion.sound/en.html
// GitHub page:     https://github.com/IonDen/ion.sound
//
// Released under MIT licence:
// http://ionden.com/a/plugins/licence-en.html
// =====================================================================================================================

(function ($) {

    if ($.ionSound) {
        return;
    }

    var settings = {},
        soundsNum,
        canMp3,
        url,
        i,

        sounds = {},
        playing = false,

        VERSION = "1.3.0";


    var createSound = function (soundInfo) {
        var name,
            volume;

        if (soundInfo.indexOf(":") !== -1) {
            name = soundInfo.split(":")[0];
            volume = soundInfo.split(":")[1];
        } else {
            name = soundInfo;
        }

        sounds[name] = new Audio();
        canMp3 = sounds[name].canPlayType("audio/mp3");
        if (canMp3 === "probably" || canMp3 === "maybe") {
            url = settings.path + name + ".mp3";
        } else {
            url = settings.path + name + ".ogg";
        }

        $(sounds[name]).attr("src", url);
        sounds[name].load();
        sounds[name].preload = "auto";
        sounds[name].volume = volume || settings.volume;
    };


    var playSound = function (info) {
        var $sound,
            name,
            volume,
            playing_int;

        if (info.indexOf(":") !== -1) {
            name = info.split(":")[0];
            volume = info.split(":")[1];
        } else {
            name = info;
        }

        $sound = sounds[name];

        if (typeof $sound !== "object" || $sound === null) {
            return;
        }


        if (volume) {
            $sound.volume = volume;
        }

        if (!settings.multiPlay && !playing) {

            $sound.play();
            playing = true;

            playing_int = setInterval(function () {
                if ($sound.ended) {
                    clearInterval(playing_int);
                    playing = false;
                }
            }, 250);

        } else if (settings.multiPlay) {

            if ($sound.ended) {
                $sound.play();
            } else {
                try {
                    $sound.currentTime = 0;
                } catch (e) { }
                $sound.play();
            }

        }
    };


    var stopSound = function (name) {
        var $sound = sounds[name];

        if (typeof $sound !== "object" || $sound === null) {
            return;
        }

        $sound.pause();
        try {
            $sound.currentTime = 0;
        } catch (e) { }
    };


    var killSound = function (name) {
        var $sound = sounds[name];

        if (typeof $sound !== "object" || $sound === null) {
            return;
        }

        try {
            sounds[name].src = "";
        } catch (e) { }
        sounds[name] = null;
    };


    // Plugin methods
    $.ionSound = function (options) {

        settings = $.extend({
            sounds: [
                "water_droplet"
            ],
            path: "static/sounds/",
            multiPlay: true,
            volume: "0.5"
        }, options);

        soundsNum = settings.sounds.length;

        if (typeof Audio === "function" || typeof Audio === "object") {
            for (i = 0; i < soundsNum; i += 1) {
                createSound(settings.sounds[i]);
            }
        }
        $.ionSound.play = function (name) {
            playSound(name);
        };
        $.ionSound.stop = function (name) {
            stopSound(name);
        };
        $.ionSound.kill = function (name) {
            killSound(name);
        };
    };
    $.ionSound.play = function (name) {
        playSound(name);
    };

    $.ionSound.destroy = function () {
        for (i = 0; i < soundsNum; i += 1) {
            sounds[settings.sounds[i]] = null;
        }
        soundsNum = 0;
        $.ionSound.play = function () { };
        $.ionSound.stop = function () { };
        $.ionSound.kill = function () { };
    };

}(jQuery));

/* JQuery Searchable DropDown Plugin | Copyright (c) 2012 xhaggi
http://sourceforge.net/projects/jsearchdropdown/
http://jsearchdropdown.sourceforge.net/
*/
(function ($) { var B = register("searchable"); B.defaults = { maxListSize: 100, maxMultiMatch: 50, exactMatch: false, wildcards: true, ignoreCase: true, warnMultiMatch: "top {0} matches ...", warnNoMatch: "no matches ...", latency: 200, zIndex: "auto" }; B.execute = function (g, h) { var j = null; var k = null; var l = null; if ($.browser.msie && parseInt(jQuery.browser.version) < 7) return this; if (this.nodeName != "SELECT" || this.size > 1) return this; var m = $(this); var n = { index: -1, options: null }; var o = "lang"; var p = false; $.browser.chrome = /chrome/.test(navigator.userAgent.toLowerCase()); if ($.browser.chrome) $.browser.safari = false; if ($.meta) { g = $.extend({}, options, m.data()) } var q = $("<div/>"); var r = $("<div/>"); var t = $("<input/>"); var u = $("<select/>"); var x = $("<option>" + g.warnMultiMatch.replace(/\{0\}/g, g.maxMultiMatch) + "</option>").attr("disabled", "true"); var y = $("<option>" + g.warnNoMatch + "</option>").attr("disabled", "true"); var z = { option: function (a) { return $(u.get(0).options[a]) }, selected: function () { return u.find(":selected") }, selectedIndex: function (a) { if (a > -1) u.get(0).selectedIndex = a; return u.get(0).selectedIndex }, size: function (a) { u.attr("size", Math.max(2, Math.min(a, 20))) }, reset: function () { if ((m.get(0).selectedIndex - 1) == m.data("index")) return; var a = m.get(0).selectedIndex; var b = m.get(0).length; var c = Math.floor(g.maxMultiMatch / 2); var d = Math.max(1, (a - c)); var e = Math.min(b, Math.max(g.maxMultiMatch, (a + c))); var f = a - d; u.empty(); this.size(e - d); for (var i = d; i < e; i++) u.append($(m.get(0).options[i]).clone().attr(o, i - 1)); if (e > g.maxMultiMatch) u.append(x); u.get(0).selectedIndex = f } }; draw(); var A = false; r.mouseover(function () { A = true }); r.mouseout(function () { A = false }); u.mouseover(function () { A = true }); u.mouseout(function () { A = false }); t.click(function (e) { if (!p) enable(e, true); else disable(e, true) }); t.blur(function (e) { if (!A && p) disable(e, true) }); m.keydown(function (e) { if (e.keyCode != 9 && !e.shiftKey && !e.ctrlKey && !e.altKey) t.click() }); m.click(function (e) { u.focus() }); u.click(function (e) { if (z.selectedIndex() < 0) return; disable(e) }); u.focus(function (e) { t.focus() }); u.blur(function (e) { if (!A) disable(e, true) }); u.mousemove(function (e) { if ($.browser.opera && parseFloat(jQuery.browser.version) >= 9.8) return true; var a = Math.floor(parseFloat(/([0-9\.]+)px/.exec(z.option(0).css("font-size")))); var b = 4; if ($.browser.opera) b = 2.5; if ($.browser.safari || $.browser.chrome) b = 3; a += Math.round(a / b); z.selectedIndex(Math.floor((e.pageY - u.offset().top + this.scrollTop) / a)) }); r.click(function (e) { t.click() }); t.keyup(function (e) { if (jQuery.inArray(e.keyCode, new Array(9, 13, 16, 33, 34, 35, 36, 38, 40)) > -1) return true; l = $.trim(t.val().toLowerCase()); clearSearchTimer(); j = setTimeout(searching, g.latency) }); t.keydown(function (e) { if (e.keyCode == 9) { disable(e) } if (e.shiftKey || e.ctrlKey || e.altKey) return; switch (e.keyCode) { case 13: disable(e); m.focus(); break; case 27: disable(e, true); m.focus(); break; case 33: if (z.selectedIndex() - u.attr("size") > 0) { z.selectedIndex(z.selectedIndex() - u.attr("size")) } else { z.selectedIndex(0) } synchronize(); break; case 34: if (z.selectedIndex() + u.attr("size") < u.get(0).options.length - 1) { z.selectedIndex(z.selectedIndex() + u.attr("size")) } else { z.selectedIndex(u.get(0).options.length - 1) } synchronize(); break; case 38: if (z.selectedIndex() > 0) { z.selectedIndex(z.selectedIndex() - 1); synchronize() } break; case 40: if (z.selectedIndex() < u.get(0).options.length - 1) { z.selectedIndex(z.selectedIndex() + 1); synchronize() } break; default: return true } return false }); function draw() { m.css("text-decoration", "none"); m.width(m.outerWidth()); m.height(m.outerHeight()); q.css("position", "relative"); q.css("width", m.outerWidth()); if ($.browser.msie) q.css("z-index", h); r.css({ "position": "absolute", "top": 0, "left": 0, "width": m.outerWidth(), "height": m.outerHeight(), "background-color": "#FFFFFF", "opacity": "0.01" }); t.attr("type", "text"); t.hide(); t.height(m.outerHeight()); t.css({ "position": "absolute", "top": 0, "left": 0, "margin": "0px", "padding": "0px", "outline-style": "none", "border-style": "solid", "border-bottom-style": "none", "border-color": "transparent", "background-color": "transparent" }); var a = new Array(); a.push("border-left-width"); a.push("border-top-width"); a.push("font-size"); a.push("font-stretch"); a.push("font-variant"); a.push("font-weight"); a.push("color"); a.push("text-align"); a.push("text-indent"); a.push("text-shadow"); a.push("text-transform"); a.push("padding-left"); a.push("padding-top"); for (var i = 0; i < a.length; i++) t.css(a[i], m.css(a[i])); if ($.browser.msie && parseInt(jQuery.browser.version) < 8) { t.css("padding", "0px"); t.css("padding-left", "3px"); t.css("border-left-width", "2px"); t.css("border-top-width", "3px") } else if ($.browser.chrome) { t.height(m.innerHeight()); t.css("text-transform", "none"); t.css("padding-left", parseFloatPx(t.css("padding-left")) + 3); t.css("padding-top", 2) } else if ($.browser.safari) { t.height(m.innerHeight()); t.css("padding-top", 2); t.css("padding-left", 3); t.css("text-transform", "none") } else if ($.browser.opera) { t.height(m.innerHeight()); var b = parseFloatPx(m.css("padding-left")); t.css("padding-left", b == 1 ? b + 1 : b); t.css("padding-top", 0) } else if ($.browser.mozilla) { t.css("padding-top", "0px"); t.css("border-top", "0px"); t.css("padding-left", parseFloatPx(m.css("padding-left")) + 3) } else { t.css("padding-left", parseFloatPx(m.css("padding-left")) + 3); t.css("padding-top", parseFloatPx(m.css("padding-top")) + 1) } var c = parseFloatPx(m.css("padding-left")) + parseFloatPx(m.css("padding-right")) + parseFloatPx(m.css("border-left-width")) + parseFloatPx(m.css("border-left-width")) + 23; t.width(m.outerWidth() - c); var w = m.css("width"); var d = m.outerWidth(); m.css("width", "auto"); d = d > m.outerWidth() ? d : m.outerWidth(); m.css("width", w); u.hide(); z.size(m.get(0).length); u.css({ "position": "absolute", "top": m.outerHeight(), "left": 0, "width": d, "border": "1px solid #333", "font-weight": "normal", "padding": 0, "background-color": m.css("background-color"), "text-transform": m.css("text-transform") }); var e = /^\d+$/.test(m.css("z-index")) ? m.css("z-index") : 1; if (g.zIndex && /^\d+$/.test(g.zIndex)) e = g.zIndex; r.css("z-index", (e).toString(10)); t.css("z-index", (e + 1).toString(10)); u.css("z-index", (e + 2).toString(10)); m.wrap(q); m.after(r); m.after(t); m.after(u) }; function enable(e, s, v) { if (m.attr("disabled")) return false; m.prepend("<option />"); if (typeof v == "undefined") p = !p; z.reset(); synchronize(); store(); if (s) u.show(); t.show(); t.focus(); t.select(); m.get(0).selectedIndex = 0; if (typeof e != "undefined") e.stopPropagation() }; function disable(e, a) { p = false; m.find(":first").remove(); clearSearchTimer(); t.hide(); u.hide(); if (typeof a != "undefined") restore(); populate(); if (typeof e != "undefined") e.stopPropagation() }; function clearSearchTimer() { if (j != null) clearTimeout(j) }; function populate() { if (z.selectedIndex() < 0 || z.selected().get(0).disabled) return; m.get(0).selectedIndex = parseInt(u.find(":selected").attr(o)); m.change(); m.data("index", new Number(m.get(0).selectedIndex)) }; function synchronize() { if (z.selectedIndex() > -1 && !z.selected().get(0).disabled) t.val(u.find(":selected").text()); else t.val(m.find(":selected").text()) }; function store() { n.index = z.selectedIndex(); n.options = new Array(); for (var i = 0; i < u.get(0).options.length; i++) n.options.push(u.get(0).options[i]) }; function restore() { u.empty(); for (var i = 0; i < n.options.length; i++) u.append(n.options[i]); z.selectedIndex(n.index); z.size(n.options.length) }; function escapeRegExp(a) { var b = ["/", ".", "*", "+", "?", "|", "(", ")", "[", "]", "{", "}", "\\", "^", "$"]; var c = new RegExp("(\\" + b.join("|\\") + ")", "g"); return a.replace(c, "\\$1") }; function searching() { if (k == l) { j = null; return } var a = 0; k = l; u.hide(); u.empty(); var b = escapeRegExp(l); if (g.exactMatch) b = "^" + b; if (g.wildcards) { b = b.replace(/\\\*/g, ".*"); b = b.replace(/\\\?/g, ".") } var c = null; if (g.ignoreCase) c = "i"; l = new RegExp(b, c); for (var i = 1; i < m.get(0).length && a < g.maxMultiMatch; i++) { if (l.length == 0 || l.test(m.get(0).options[i].text)) { var d = $(m.get(0).options[i]).clone().attr(o, i - 1); if (m.data("index") == i) d.text(m.data("text")); u.append(d); a++ } } if (a >= 1) { z.selectedIndex(0) } else if (a == 0) { u.append(y) } if (a >= g.maxMultiMatch) { u.append(x) } z.size(a); u.show(); j = null }; function parseFloatPx(a) { try { a = parseFloat(a.replace(/[\s]*px/, "")); if (!isNaN(a)) return a } catch (e) { } return 0 }; return }; function register(d) { var e = $[d] = {}; $.fn[d] = function (b) { b = $.extend(e.defaults, b); var c = this.size(); return this.each(function (a) { e.execute.call(this, b, c - a) }) }; return e } })(jQuery);



/*
 * Raphael 1.3.1 - JavaScript Vector Library
 *
 * Copyright (c) 2008 - 2009 Dmitry Baranovskiy (http://raphaeljs.com)
 * Licensed under the MIT (http://www.opensource.org/licenses/mit-license.php) license.
 */
Raphael = (function () { var a = /[, ]+/, aO = /^(circle|rect|path|ellipse|text|image)$/, L = document, au = window, l = { was: "Raphael" in au, is: au.Raphael }, an = function () { if (an.is(arguments[0], "array")) { var d = arguments[0], e = w[aW](an, d.splice(0, 3 + an.is(d[0], al))), S = e.set(); for (var R = 0, a0 = d[m]; R < a0; R++) { var E = d[R] || {}; aO.test(E.type) && S[f](e[E.type]().attr(E)); } return S; } return w[aW](an, arguments); }, aT = function () { }, aL = "appendChild", aW = "apply", aS = "concat", at = "", am = " ", z = "split", F = "click dblclick mousedown mousemove mouseout mouseover mouseup"[z](am), Q = "hasOwnProperty", az = "join", m = "length", aY = "prototype", aZ = String[aY].toLowerCase, ab = Math, g = ab.max, aI = ab.min, al = "number", aA = "toString", aw = Object[aY][aA], aQ = {}, aM = ab.pow, f = "push", aU = /^(?=[\da-f]$)/, c = /^url\(['"]?([^\)]+)['"]?\)$/i, x = /^\s*((#[a-f\d]{6})|(#[a-f\d]{3})|rgb\(\s*([\d\.]+\s*,\s*[\d\.]+\s*,\s*[\d\.]+)\s*\)|rgb\(\s*([\d\.]+%\s*,\s*[\d\.]+%\s*,\s*[\d\.]+%)\s*\)|hs[bl]\(\s*([\d\.]+\s*,\s*[\d\.]+\s*,\s*[\d\.]+)\s*\)|hs[bl]\(\s*([\d\.]+%\s*,\s*[\d\.]+%\s*,\s*[\d\.]+%)\s*\))\s*$/i, O = ab.round, v = "setAttribute", W = parseFloat, G = parseInt, aN = String[aY].toUpperCase, j = { "clip-rect": "0 0 1e9 1e9", cursor: "default", cx: 0, cy: 0, fill: "#fff", "fill-opacity": 1, font: '10px "Arial"', "font-family": '"Arial"', "font-size": "10", "font-style": "normal", "font-weight": 400, gradient: 0, height: 0, href: "http://raphaeljs.com/", opacity: 1, path: "M0,0", r: 0, rotation: 0, rx: 0, ry: 0, scale: "1 1", src: "", stroke: "#000", "stroke-dasharray": "", "stroke-linecap": "butt", "stroke-linejoin": "butt", "stroke-miterlimit": 0, "stroke-opacity": 1, "stroke-width": 1, target: "_blank", "text-anchor": "middle", title: "Raphael", translation: "0 0", width: 0, x: 0, y: 0 }, Z = { along: "along", "clip-rect": "csv", cx: al, cy: al, fill: "colour", "fill-opacity": al, "font-size": al, height: al, opacity: al, path: "path", r: al, rotation: "csv", rx: al, ry: al, scale: "csv", stroke: "colour", "stroke-opacity": al, "stroke-width": al, translation: "csv", width: al, x: al, y: al }, aP = "replace"; an.version = "1.3.1"; an.type = (au.SVGAngle || L.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#BasicStructure", "1.1") ? "SVG" : "VML"); if (an.type == "VML") { var ag = document.createElement("div"); ag.innerHTML = "<!--[if vml]><br><br><![endif]-->"; if (ag.childNodes[m] != 2) { return null; } } an.svg = !(an.vml = an.type == "VML"); aT[aY] = an[aY]; an._id = 0; an._oid = 0; an.fn = {}; an.is = function (e, d) { d = aZ.call(d); return ((d == "object" || d == "undefined") && typeof e == d) || (e == null && d == "null") || aZ.call(aw.call(e).slice(8, -1)) == d; }; an.setWindow = function (d) { au = d; L = au.document; }; var aD = function (e) { if (an.vml) { var d = /^\s+|\s+$/g; aD = aj(function (R) { var S; R = (R + at)[aP](d, at); try { var a0 = new ActiveXObject("htmlfile"); a0.write("<body>"); a0.close(); S = a0.body; } catch (a2) { S = createPopup().document.body; } var i = S.createTextRange(); try { S.style.color = R; var a1 = i.queryCommandValue("ForeColor"); a1 = ((a1 & 255) << 16) | (a1 & 65280) | ((a1 & 16711680) >>> 16); return "#" + ("000000" + a1[aA](16)).slice(-6); } catch (a2) { return "none"; } }); } else { var E = L.createElement("i"); E.title = "Rapha\xebl Colour Picker"; E.style.display = "none"; L.body[aL](E); aD = aj(function (i) { E.style.color = i; return L.defaultView.getComputedStyle(E, at).getPropertyValue("color"); }); } return aD(e); }; an.hsb2rgb = aj(function (a3, a1, a7) { if (an.is(a3, "object") && "h" in a3 && "s" in a3 && "b" in a3) { a7 = a3.b; a1 = a3.s; a3 = a3.h; } var R, S, a8; if (a7 == 0) { return { r: 0, g: 0, b: 0, hex: "#000" }; } if (a3 > 1 || a1 > 1 || a7 > 1) { a3 /= 255; a1 /= 255; a7 /= 255; } var a0 = ~~(a3 * 6), a4 = (a3 * 6) - a0, E = a7 * (1 - a1), e = a7 * (1 - (a1 * a4)), a9 = a7 * (1 - (a1 * (1 - a4))); R = [a7, e, E, E, a9, a7, a7][a0]; S = [a9, a7, a7, e, E, E, a9][a0]; a8 = [E, E, a9, a7, a7, e, E][a0]; R *= 255; S *= 255; a8 *= 255; var a5 = { r: R, g: S, b: a8 }, d = (~~R)[aA](16), a2 = (~~S)[aA](16), a6 = (~~a8)[aA](16); d = d[aP](aU, "0"); a2 = a2[aP](aU, "0"); a6 = a6[aP](aU, "0"); a5.hex = "#" + d + a2 + a6; return a5; }, an); an.rgb2hsb = aj(function (d, e, a1) { if (an.is(d, "object") && "r" in d && "g" in d && "b" in d) { a1 = d.b; e = d.g; d = d.r; } if (an.is(d, "string")) { var a3 = an.getRGB(d); d = a3.r; e = a3.g; a1 = a3.b; } if (d > 1 || e > 1 || a1 > 1) { d /= 255; e /= 255; a1 /= 255; } var a0 = g(d, e, a1), i = aI(d, e, a1), R, E, S = a0; if (i == a0) { return { h: 0, s: 0, b: a0 }; } else { var a2 = (a0 - i); E = a2 / a0; if (d == a0) { R = (e - a1) / a2; } else { if (e == a0) { R = 2 + ((a1 - d) / a2); } else { R = 4 + ((d - e) / a2); } } R /= 6; R < 0 && R++; R > 1 && R--; } return { h: R, s: E, b: S }; }, an); var aE = /,?([achlmqrstvxz]),?/gi; an._path2string = function () { return this.join(",")[aP](aE, "$1"); }; function aj(E, e, d) { function i() { var R = Array[aY].slice.call(arguments, 0), a0 = R[az]("\u25ba"), S = i.cache = i.cache || {}, a1 = i.count = i.count || []; if (S[Q](a0)) { return d ? d(S[a0]) : S[a0]; } a1[m] >= 1000 && delete S[a1.shift()]; a1[f](a0); S[a0] = E[aW](e, R); return d ? d(S[a0]) : S[a0]; } return i; } an.getRGB = aj(function (d) { if (!d || !!((d = d + at).indexOf("-") + 1)) { return { r: -1, g: -1, b: -1, hex: "none", error: 1 }; } if (d == "none") { return { r: -1, g: -1, b: -1, hex: "none" }; } !(({ hs: 1, rg: 1 })[Q](d.substring(0, 2)) || d.charAt() == "#") && (d = aD(d)); var S, i, E, a2, a3, a0 = d.match(x); if (a0) { if (a0[2]) { a2 = G(a0[2].substring(5), 16); E = G(a0[2].substring(3, 5), 16); i = G(a0[2].substring(1, 3), 16); } if (a0[3]) { a2 = G((a3 = a0[3].charAt(3)) + a3, 16); E = G((a3 = a0[3].charAt(2)) + a3, 16); i = G((a3 = a0[3].charAt(1)) + a3, 16); } if (a0[4]) { a0 = a0[4][z](/\s*,\s*/); i = W(a0[0]); E = W(a0[1]); a2 = W(a0[2]); } if (a0[5]) { a0 = a0[5][z](/\s*,\s*/); i = W(a0[0]) * 2.55; E = W(a0[1]) * 2.55; a2 = W(a0[2]) * 2.55; } if (a0[6]) { a0 = a0[6][z](/\s*,\s*/); i = W(a0[0]); E = W(a0[1]); a2 = W(a0[2]); return an.hsb2rgb(i, E, a2); } if (a0[7]) { a0 = a0[7][z](/\s*,\s*/); i = W(a0[0]) * 2.55; E = W(a0[1]) * 2.55; a2 = W(a0[2]) * 2.55; return an.hsb2rgb(i, E, a2); } a0 = { r: i, g: E, b: a2 }; var e = (~~i)[aA](16), R = (~~E)[aA](16), a1 = (~~a2)[aA](16); e = e[aP](aU, "0"); R = R[aP](aU, "0"); a1 = a1[aP](aU, "0"); a0.hex = "#" + e + R + a1; return a0; } return { r: -1, g: -1, b: -1, hex: "none", error: 1 }; }, an); an.getColor = function (e) { var i = this.getColor.start = this.getColor.start || { h: 0, s: 1, b: e || 0.75 }, d = this.hsb2rgb(i.h, i.s, i.b); i.h += 0.075; if (i.h > 1) { i.h = 0; i.s -= 0.2; i.s <= 0 && (this.getColor.start = { h: 0, s: 1, b: i.b }); } return d.hex; }; an.getColor.reset = function () { delete this.start; }; an.parsePathString = aj(function (d) { if (!d) { return null; } var i = { a: 7, c: 6, h: 1, l: 2, m: 2, q: 4, s: 4, t: 2, v: 1, z: 0 }, e = []; if (an.is(d, "array") && an.is(d[0], "array")) { e = av(d); } if (!e[m]) { (d + at)[aP](/([achlmqstvz])[\s,]*((-?\d*\.?\d*(?:e[-+]?\d+)?\s*,?\s*)+)/ig, function (R, E, a1) { var a0 = [], S = aZ.call(E); a1[aP](/(-?\d*\.?\d*(?:e[-+]?\d+)?)\s*,?\s*/ig, function (a3, a2) { a2 && a0[f](+a2); }); while (a0[m] >= i[S]) { e[f]([E][aS](a0.splice(0, i[S]))); if (!i[S]) { break; } } }); } e[aA] = an._path2string; return e; }); an.findDotsAtSegment = function (e, d, be, bc, a0, R, a2, a1, a8) { var a6 = 1 - a8, a5 = aM(a6, 3) * e + aM(a6, 2) * 3 * a8 * be + a6 * 3 * a8 * a8 * a0 + aM(a8, 3) * a2, a3 = aM(a6, 3) * d + aM(a6, 2) * 3 * a8 * bc + a6 * 3 * a8 * a8 * R + aM(a8, 3) * a1, ba = e + 2 * a8 * (be - e) + a8 * a8 * (a0 - 2 * be + e), a9 = d + 2 * a8 * (bc - d) + a8 * a8 * (R - 2 * bc + d), bd = be + 2 * a8 * (a0 - be) + a8 * a8 * (a2 - 2 * a0 + be), bb = bc + 2 * a8 * (R - bc) + a8 * a8 * (a1 - 2 * R + bc), a7 = (1 - a8) * e + a8 * be, a4 = (1 - a8) * d + a8 * bc, E = (1 - a8) * a0 + a8 * a2, i = (1 - a8) * R + a8 * a1, S = (90 - ab.atan((ba - bd) / (a9 - bb)) * 180 / ab.PI); (ba > bd || a9 < bb) && (S += 180); return { x: a5, y: a3, m: { x: ba, y: a9 }, n: { x: bd, y: bb }, start: { x: a7, y: a4 }, end: { x: E, y: i }, alpha: S }; }; var U = aj(function (a5) { if (!a5) { return { x: 0, y: 0, width: 0, height: 0 }; } a5 = H(a5); var a2 = 0, a1 = 0, R = [], e = [], E; for (var S = 0, a4 = a5[m]; S < a4; S++) { E = a5[S]; if (E[0] == "M") { a2 = E[1]; a1 = E[2]; R[f](a2); e[f](a1); } else { var a0 = aC(a2, a1, E[1], E[2], E[3], E[4], E[5], E[6]); R = R[aS](a0.min.x, a0.max.x); e = e[aS](a0.min.y, a0.max.y); a2 = E[5]; a1 = E[6]; } } var d = aI[aW](0, R), a3 = aI[aW](0, e); return { x: d, y: a3, width: g[aW](0, R) - d, height: g[aW](0, e) - a3 }; }), av = function (a0) { var E = []; if (!an.is(a0, "array") || !an.is(a0 && a0[0], "array")) { a0 = an.parsePathString(a0); } for (var e = 0, R = a0[m]; e < R; e++) { E[e] = []; for (var d = 0, S = a0[e][m]; d < S; d++) { E[e][d] = a0[e][d]; } } E[aA] = an._path2string; return E; }, ad = aj(function (R) { if (!an.is(R, "array") || !an.is(R && R[0], "array")) { R = an.parsePathString(R); } var a4 = [], a6 = 0, a5 = 0, a9 = 0, a8 = 0, E = 0; if (R[0][0] == "M") { a6 = R[0][1]; a5 = R[0][2]; a9 = a6; a8 = a5; E++; a4[f](["M", a6, a5]); } for (var a1 = E, ba = R[m]; a1 < ba; a1++) { var d = a4[a1] = [], a7 = R[a1]; if (a7[0] != aZ.call(a7[0])) { d[0] = aZ.call(a7[0]); switch (d[0]) { case "a": d[1] = a7[1]; d[2] = a7[2]; d[3] = a7[3]; d[4] = a7[4]; d[5] = a7[5]; d[6] = +(a7[6] - a6).toFixed(3); d[7] = +(a7[7] - a5).toFixed(3); break; case "v": d[1] = +(a7[1] - a5).toFixed(3); break; case "m": a9 = a7[1]; a8 = a7[2]; default: for (var a0 = 1, a2 = a7[m]; a0 < a2; a0++) { d[a0] = +(a7[a0] - ((a0 % 2) ? a6 : a5)).toFixed(3); } } } else { d = a4[a1] = []; if (a7[0] == "m") { a9 = a7[1] + a6; a8 = a7[2] + a5; } for (var S = 0, e = a7[m]; S < e; S++) { a4[a1][S] = a7[S]; } } var a3 = a4[a1][m]; switch (a4[a1][0]) { case "z": a6 = a9; a5 = a8; break; case "h": a6 += +a4[a1][a3 - 1]; break; case "v": a5 += +a4[a1][a3 - 1]; break; default: a6 += +a4[a1][a3 - 2]; a5 += +a4[a1][a3 - 1]; } } a4[aA] = an._path2string; return a4; }, 0, av), r = aj(function (R) { if (!an.is(R, "array") || !an.is(R && R[0], "array")) { R = an.parsePathString(R); } var a3 = [], a5 = 0, a4 = 0, a8 = 0, a7 = 0, E = 0; if (R[0][0] == "M") { a5 = +R[0][1]; a4 = +R[0][2]; a8 = a5; a7 = a4; E++; a3[0] = ["M", a5, a4]; } for (var a1 = E, a9 = R[m]; a1 < a9; a1++) { var d = a3[a1] = [], a6 = R[a1]; if (a6[0] != aN.call(a6[0])) { d[0] = aN.call(a6[0]); switch (d[0]) { case "A": d[1] = a6[1]; d[2] = a6[2]; d[3] = a6[3]; d[4] = a6[4]; d[5] = a6[5]; d[6] = +(a6[6] + a5); d[7] = +(a6[7] + a4); break; case "V": d[1] = +a6[1] + a4; break; case "H": d[1] = +a6[1] + a5; break; case "M": a8 = +a6[1] + a5; a7 = +a6[2] + a4; default: for (var a0 = 1, a2 = a6[m]; a0 < a2; a0++) { d[a0] = +a6[a0] + ((a0 % 2) ? a5 : a4); } } } else { for (var S = 0, e = a6[m]; S < e; S++) { a3[a1][S] = a6[S]; } } switch (d[0]) { case "Z": a5 = a8; a4 = a7; break; case "H": a5 = d[1]; break; case "V": a4 = d[1]; break; default: a5 = a3[a1][a3[a1][m] - 2]; a4 = a3[a1][a3[a1][m] - 1]; } } a3[aA] = an._path2string; return a3; }, null, av), aX = function (e, E, d, i) { return [e, E, d, i, d, i]; }, aK = function (e, E, a0, R, d, i) { var S = 1 / 3, a1 = 2 / 3; return [S * e + a1 * a0, S * E + a1 * R, S * d + a1 * a0, S * i + a1 * R, d, i]; }, K = function (a9, bE, bi, bg, ba, a4, S, a8, bD, bb) { var R = ab.PI, bf = R * 120 / 180, d = R / 180 * (+ba || 0), bm = [], bj, bA = aj(function (bF, bI, i) { var bH = bF * ab.cos(i) - bI * ab.sin(i), bG = bF * ab.sin(i) + bI * ab.cos(i); return { x: bH, y: bG }; }); if (!bb) { bj = bA(a9, bE, -d); a9 = bj.x; bE = bj.y; bj = bA(a8, bD, -d); a8 = bj.x; bD = bj.y; var e = ab.cos(R / 180 * ba), a6 = ab.sin(R / 180 * ba), bo = (a9 - a8) / 2, bn = (bE - bD) / 2; bi = g(bi, ab.abs(bo)); bg = g(bg, ab.abs(bn)); var by = (bo * bo) / (bi * bi) + (bn * bn) / (bg * bg); if (by > 1) { bi = ab.sqrt(by) * bi; bg = ab.sqrt(by) * bg; } var E = bi * bi, br = bg * bg, bt = (a4 == S ? -1 : 1) * ab.sqrt(ab.abs((E * br - E * bn * bn - br * bo * bo) / (E * bn * bn + br * bo * bo))), bd = bt * bi * bn / bg + (a9 + a8) / 2, bc = bt * -bg * bo / bi + (bE + bD) / 2, a3 = ab.asin(((bE - bc) / bg).toFixed(7)), a2 = ab.asin(((bD - bc) / bg).toFixed(7)); a3 = a9 < bd ? R - a3 : a3; a2 = a8 < bd ? R - a2 : a2; a3 < 0 && (a3 = R * 2 + a3); a2 < 0 && (a2 = R * 2 + a2); if (S && a3 > a2) { a3 = a3 - R * 2; } if (!S && a2 > a3) { a2 = a2 - R * 2; } } else { a3 = bb[0]; a2 = bb[1]; bd = bb[2]; bc = bb[3]; } var a7 = a2 - a3; if (ab.abs(a7) > bf) { var be = a2, bh = a8, a5 = bD; a2 = a3 + bf * (S && a2 > a3 ? 1 : -1); a8 = bd + bi * ab.cos(a2); bD = bc + bg * ab.sin(a2); bm = K(a8, bD, bi, bg, ba, 0, S, bh, a5, [a2, be, bd, bc]); } a7 = a2 - a3; var a1 = ab.cos(a3), bC = ab.sin(a3), a0 = ab.cos(a2), bB = ab.sin(a2), bp = ab.tan(a7 / 4), bs = 4 / 3 * bi * bp, bq = 4 / 3 * bg * bp, bz = [a9, bE], bx = [a9 + bs * bC, bE - bq * a1], bw = [a8 + bs * bB, bD - bq * a0], bu = [a8, bD]; bx[0] = 2 * bz[0] - bx[0]; bx[1] = 2 * bz[1] - bx[1]; if (bb) { return [bx, bw, bu][aS](bm); } else { bm = [bx, bw, bu][aS](bm)[az]()[z](","); var bk = []; for (var bv = 0, bl = bm[m]; bv < bl; bv++) { bk[bv] = bv % 2 ? bA(bm[bv - 1], bm[bv], d).y : bA(bm[bv], bm[bv + 1], d).x; } return bk; } }, M = function (e, d, E, i, a2, a1, a0, S, a3) { var R = 1 - a3; return { x: aM(R, 3) * e + aM(R, 2) * 3 * a3 * E + R * 3 * a3 * a3 * a2 + aM(a3, 3) * a0, y: aM(R, 3) * d + aM(R, 2) * 3 * a3 * i + R * 3 * a3 * a3 * a1 + aM(a3, 3) * S }; }, aC = aj(function (i, d, R, E, a9, a8, a5, a2) { var a7 = (a9 - 2 * R + i) - (a5 - 2 * a9 + R), a4 = 2 * (R - i) - 2 * (a9 - R), a1 = i - R, a0 = (-a4 + ab.sqrt(a4 * a4 - 4 * a7 * a1)) / 2 / a7, S = (-a4 - ab.sqrt(a4 * a4 - 4 * a7 * a1)) / 2 / a7, a3 = [d, a2], a6 = [i, a5], e; ab.abs(a0) > 1000000000000 && (a0 = 0.5); ab.abs(S) > 1000000000000 && (S = 0.5); if (a0 > 0 && a0 < 1) { e = M(i, d, R, E, a9, a8, a5, a2, a0); a6[f](e.x); a3[f](e.y); } if (S > 0 && S < 1) { e = M(i, d, R, E, a9, a8, a5, a2, S); a6[f](e.x); a3[f](e.y); } a7 = (a8 - 2 * E + d) - (a2 - 2 * a8 + E); a4 = 2 * (E - d) - 2 * (a8 - E); a1 = d - E; a0 = (-a4 + ab.sqrt(a4 * a4 - 4 * a7 * a1)) / 2 / a7; S = (-a4 - ab.sqrt(a4 * a4 - 4 * a7 * a1)) / 2 / a7; ab.abs(a0) > 1000000000000 && (a0 = 0.5); ab.abs(S) > 1000000000000 && (S = 0.5); if (a0 > 0 && a0 < 1) { e = M(i, d, R, E, a9, a8, a5, a2, a0); a6[f](e.x); a3[f](e.y); } if (S > 0 && S < 1) { e = M(i, d, R, E, a9, a8, a5, a2, S); a6[f](e.x); a3[f](e.y); } return { min: { x: aI[aW](0, a6), y: aI[aW](0, a3) }, max: { x: g[aW](0, a6), y: g[aW](0, a3) } }; }), H = aj(function (a9, a4) { var R = r(a9), a5 = a4 && r(a4), a6 = { x: 0, y: 0, bx: 0, by: 0, X: 0, Y: 0, qx: null, qy: null }, d = { x: 0, y: 0, bx: 0, by: 0, X: 0, Y: 0, qx: null, qy: null }, a0 = function (ba, bb) { var i, bc; if (!ba) { return ["C", bb.x, bb.y, bb.x, bb.y, bb.x, bb.y]; } !(ba[0] in { T: 1, Q: 1 }) && (bb.qx = bb.qy = null); switch (ba[0]) { case "M": bb.X = ba[1]; bb.Y = ba[2]; break; case "A": ba = ["C"][aS](K[aW](0, [bb.x, bb.y][aS](ba.slice(1)))); break; case "S": i = bb.x + (bb.x - (bb.bx || bb.x)); bc = bb.y + (bb.y - (bb.by || bb.y)); ba = ["C", i, bc][aS](ba.slice(1)); break; case "T": bb.qx = bb.x + (bb.x - (bb.qx || bb.x)); bb.qy = bb.y + (bb.y - (bb.qy || bb.y)); ba = ["C"][aS](aK(bb.x, bb.y, bb.qx, bb.qy, ba[1], ba[2])); break; case "Q": bb.qx = ba[1]; bb.qy = ba[2]; ba = ["C"][aS](aK(bb.x, bb.y, ba[1], ba[2], ba[3], ba[4])); break; case "L": ba = ["C"][aS](aX(bb.x, bb.y, ba[1], ba[2])); break; case "H": ba = ["C"][aS](aX(bb.x, bb.y, ba[1], bb.y)); break; case "V": ba = ["C"][aS](aX(bb.x, bb.y, bb.x, ba[1])); break; case "Z": ba = ["C"][aS](aX(bb.x, bb.y, bb.X, bb.Y)); break; } return ba; }, e = function (ba, bb) { if (ba[bb][m] > 7) { ba[bb].shift(); var bc = ba[bb]; while (bc[m]) { ba.splice(bb++, 0, ["C"][aS](bc.splice(0, 6))); } ba.splice(bb, 1); a7 = g(R[m], a5 && a5[m] || 0); } }, E = function (be, bd, bb, ba, bc) { if (be && bd && be[bc][0] == "M" && bd[bc][0] != "M") { bd.splice(bc, 0, ["M", ba.x, ba.y]); bb.bx = 0; bb.by = 0; bb.x = be[bc][1]; bb.y = be[bc][2]; a7 = g(R[m], a5 && a5[m] || 0); } }; for (var a2 = 0, a7 = g(R[m], a5 && a5[m] || 0) ; a2 < a7; a2++) { R[a2] = a0(R[a2], a6); e(R, a2); a5 && (a5[a2] = a0(a5[a2], d)); a5 && e(a5, a2); E(R, a5, a6, d, a2); E(a5, R, d, a6, a2); var a1 = R[a2], a8 = a5 && a5[a2], S = a1[m], a3 = a5 && a8[m]; a6.x = a1[S - 2]; a6.y = a1[S - 1]; a6.bx = W(a1[S - 4]) || a6.x; a6.by = W(a1[S - 3]) || a6.y; d.bx = a5 && (W(a8[a3 - 4]) || d.x); d.by = a5 && (W(a8[a3 - 3]) || d.y); d.x = a5 && a8[a3 - 2]; d.y = a5 && a8[a3 - 1]; } return a5 ? [R, a5] : R; }, null, av), p = aj(function (a4) { var a3 = []; for (var a0 = 0, a5 = a4[m]; a0 < a5; a0++) { var e = {}, a2 = a4[a0].match(/^([^:]*):?([\d\.]*)/); e.color = an.getRGB(a2[1]); if (e.color.error) { return null; } e.color = e.color.hex; a2[2] && (e.offset = a2[2] + "%"); a3[f](e); } for (var a0 = 1, a5 = a3[m] - 1; a0 < a5; a0++) { if (!a3[a0].offset) { var E = W(a3[a0 - 1].offset || 0), R = 0; for (var S = a0 + 1; S < a5; S++) { if (a3[S].offset) { R = a3[S].offset; break; } } if (!R) { R = 100; S = a5; } R = W(R); var a1 = (R - E) / (S - a0 + 1); for (; a0 < S; a0++) { E += a1; a3[a0].offset = E + "%"; } } } return a3; }), ao = function () { var i, e, R, E, d; if (an.is(arguments[0], "string") || an.is(arguments[0], "object")) { if (an.is(arguments[0], "string")) { i = L.getElementById(arguments[0]); } else { i = arguments[0]; } if (i.tagName) { if (arguments[1] == null) { return { container: i, width: i.style.pixelWidth || i.offsetWidth, height: i.style.pixelHeight || i.offsetHeight }; } else { return { container: i, width: arguments[1], height: arguments[2] }; } } } else { if (an.is(arguments[0], al) && arguments[m] > 3) { return { container: 1, x: arguments[0], y: arguments[1], width: arguments[2], height: arguments[3] }; } } }, aG = function (d, i) { var e = this; for (var E in i) { if (i[Q](E) && !(E in d)) { switch (typeof i[E]) { case "function": (function (R) { d[E] = d === e ? R : function () { return R[aW](e, arguments); }; })(i[E]); break; case "object": d[E] = d[E] || {}; aG.call(this, d[E], i[E]); break; default: d[E] = i[E]; break; } } } }, ak = function (d, e) { d == e.top && (e.top = d.prev); d == e.bottom && (e.bottom = d.next); d.next && (d.next.prev = d.prev); d.prev && (d.prev.next = d.next); }, Y = function (d, e) { if (e.top === d) { return; } ak(d, e); d.next = null; d.prev = e.top; e.top.next = d; e.top = d; }, k = function (d, e) { if (e.bottom === d) { return; } ak(d, e); d.next = e.bottom; d.prev = null; e.bottom.prev = d; e.bottom = d; }, A = function (e, d, i) { ak(e, i); d == i.top && (i.top = e); d.next && (d.next.prev = e); e.next = d.next; e.prev = d; d.next = e; }, aq = function (e, d, i) { ak(e, i); d == i.bottom && (i.bottom = e); d.prev && (d.prev.next = e); e.prev = d.prev; d.prev = e; e.next = d; }, s = function (d) { return function () { throw new Error("Rapha\xebl: you are calling to method \u201c" + d + "\u201d of removed object"); }; }, ar = /^r(?:\(([^,]+?)\s*,\s*([^\)]+?)\))?/; if (an.svg) { aT[aY].svgns = "http://www.w3.org/2000/svg"; aT[aY].xlink = "http://www.w3.org/1999/xlink"; var O = function (d) { return +d + (~~d === d) * 0.5; }, V = function (S) { for (var e = 0, E = S[m]; e < E; e++) { if (aZ.call(S[e][0]) != "a") { for (var d = 1, R = S[e][m]; d < R; d++) { S[e][d] = O(S[e][d]); } } else { S[e][6] = O(S[e][6]); S[e][7] = O(S[e][7]); } } return S; }, aJ = function (i, d) { if (d) { for (var e in d) { if (d[Q](e)) { i[v](e, d[e]); } } } else { return L.createElementNS(aT[aY].svgns, i); } }; an[aA] = function () { return "Your browser supports SVG.\nYou are running Rapha\xebl " + this.version; }; var q = function (d, E) { var e = aJ("path"); E.canvas && E.canvas[aL](e); var i = new ax(e, E); i.type = "path"; aa(i, { fill: "none", stroke: "#000", path: d }); return i; }; var b = function (E, a7, d) { var a4 = "linear", a1 = 0.5, S = 0.5, a9 = E.style; a7 = (a7 + at)[aP](ar, function (bb, i, bc) { a4 = "radial"; if (i && bc) { a1 = W(i); S = W(bc); var ba = ((S > 0.5) * 2 - 1); aM(a1 - 0.5, 2) + aM(S - 0.5, 2) > 0.25 && (S = ab.sqrt(0.25 - aM(a1 - 0.5, 2)) * ba + 0.5) && S != 0.5 && (S = S.toFixed(5) - 0.00001 * ba); } return at; }); a7 = a7[z](/\s*\-\s*/); if (a4 == "linear") { var a0 = a7.shift(); a0 = -W(a0); if (isNaN(a0)) { return null; } var R = [0, 0, ab.cos(a0 * ab.PI / 180), ab.sin(a0 * ab.PI / 180)], a6 = 1 / (g(ab.abs(R[2]), ab.abs(R[3])) || 1); R[2] *= a6; R[3] *= a6; if (R[2] < 0) { R[0] = -R[2]; R[2] = 0; } if (R[3] < 0) { R[1] = -R[3]; R[3] = 0; } } var a3 = p(a7); if (!a3) { return null; } var e = aJ(a4 + "Gradient"); e.id = "r" + (an._id++)[aA](36); aJ(e, a4 == "radial" ? { fx: a1, fy: S } : { x1: R[0], y1: R[1], x2: R[2], y2: R[3] }); d.defs[aL](e); for (var a2 = 0, a8 = a3[m]; a2 < a8; a2++) { var a5 = aJ("stop"); aJ(a5, { offset: a3[a2].offset ? a3[a2].offset : !a2 ? "0%" : "100%", "stop-color": a3[a2].color || "#fff" }); e[aL](a5); } aJ(E, { fill: "url(#" + e.id + ")", opacity: 1, "fill-opacity": 1 }); a9.fill = at; a9.opacity = 1; a9.fillOpacity = 1; return 1; }; var N = function (e) { var d = e.getBBox(); aJ(e.pattern, { patternTransform: an.format("translate({0},{1})", d.x, d.y) }); }; var aa = function (a6, bf) { var a9 = { "": [0], none: [0], "-": [3, 1], ".": [1, 1], "-.": [3, 1, 1, 1], "-..": [3, 1, 1, 1, 1, 1], ". ": [1, 3], "- ": [4, 3], "--": [8, 3], "- .": [4, 3, 1, 3], "--.": [8, 3, 1, 3], "--..": [8, 3, 1, 3, 1, 3] }, bb = a6.node, a7 = a6.attrs, a3 = a6.rotate(), S = function (bm, bl) { bl = a9[aZ.call(bl)]; if (bl) { var bj = bm.attrs["stroke-width"] || "1", bh = { round: bj, square: bj, butt: 0 }[bm.attrs["stroke-linecap"] || bf["stroke-linecap"]] || 0, bk = []; var bi = bl[m]; while (bi--) { bk[bi] = bl[bi] * bj + ((bi % 2) ? 1 : -1) * bh; } aJ(bb, { "stroke-dasharray": bk[az](",") }); } }; bf[Q]("rotation") && (a3 = bf.rotation); var a2 = (a3 + at)[z](a); if (!(a2.length - 1)) { a2 = null; } else { a2[1] = +a2[1]; a2[2] = +a2[2]; } W(a3) && a6.rotate(0, true); for (var ba in bf) { if (bf[Q](ba)) { if (!j[Q](ba)) { continue; } var a8 = bf[ba]; a7[ba] = a8; switch (ba) { case "rotation": a6.rotate(a8, true); break; case "href": case "title": case "target": var bd = bb.parentNode; if (aZ.call(bd.tagName) != "a") { var E = aJ("a"); bd.insertBefore(E, bb); E[aL](bb); bd = E; } bd.setAttributeNS(a6.paper.xlink, ba, a8); break; case "cursor": bb.style.cursor = a8; break; case "clip-rect": var e = (a8 + at)[z](a); if (e[m] == 4) { a6.clip && a6.clip.parentNode.parentNode.removeChild(a6.clip.parentNode); var i = aJ("clipPath"), bc = aJ("rect"); i.id = "r" + (an._id++)[aA](36); aJ(bc, { x: e[0], y: e[1], width: e[2], height: e[3] }); i[aL](bc); a6.paper.defs[aL](i); aJ(bb, { "clip-path": "url(#" + i.id + ")" }); a6.clip = bc; } if (!a8) { var be = L.getElementById(bb.getAttribute("clip-path")[aP](/(^url\(#|\)$)/g, at)); be && be.parentNode.removeChild(be); aJ(bb, { "clip-path": at }); delete a6.clip; } break; case "path": if (a8 && a6.type == "path") { a7.path = V(r(a8)); aJ(bb, { d: a7.path }); } break; case "width": bb[v](ba, a8); if (a7.fx) { ba = "x"; a8 = a7.x; } else { break; } case "x": if (a7.fx) { a8 = -a7.x - (a7.width || 0); } case "rx": if (ba == "rx" && a6.type == "rect") { break; } case "cx": a2 && (ba == "x" || ba == "cx") && (a2[1] += a8 - a7[ba]); bb[v](ba, O(a8)); a6.pattern && N(a6); break; case "height": bb[v](ba, a8); if (a7.fy) { ba = "y"; a8 = a7.y; } else { break; } case "y": if (a7.fy) { a8 = -a7.y - (a7.height || 0); } case "ry": if (ba == "ry" && a6.type == "rect") { break; } case "cy": a2 && (ba == "y" || ba == "cy") && (a2[2] += a8 - a7[ba]); bb[v](ba, O(a8)); a6.pattern && N(a6); break; case "r": if (a6.type == "rect") { aJ(bb, { rx: a8, ry: a8 }); } else { bb[v](ba, a8); } break; case "src": if (a6.type == "image") { bb.setAttributeNS(a6.paper.xlink, "href", a8); } break; case "stroke-width": bb.style.strokeWidth = a8; bb[v](ba, a8); if (a7["stroke-dasharray"]) { S(a6, a7["stroke-dasharray"]); } break; case "stroke-dasharray": S(a6, a8); break; case "translation": var a0 = (a8 + at)[z](a); a0[0] = +a0[0] || 0; a0[1] = +a0[1] || 0; if (a2) { a2[1] += a0[0]; a2[2] += a0[1]; } t.call(a6, a0[0], a0[1]); break; case "scale": var a0 = (a8 + at)[z](a); a6.scale(+a0[0] || 1, +a0[1] || +a0[0] || 1, +a0[2] || null, +a0[3] || null); break; case "fill": var R = (a8 + at).match(c); if (R) { var i = aJ("pattern"), a5 = aJ("image"); i.id = "r" + (an._id++)[aA](36); aJ(i, { x: 0, y: 0, patternUnits: "userSpaceOnUse", height: 1, width: 1 }); aJ(a5, { x: 0, y: 0 }); a5.setAttributeNS(a6.paper.xlink, "href", R[1]); i[aL](a5); var bg = L.createElement("img"); bg.style.cssText = "position:absolute;left:-9999em;top-9999em"; bg.onload = function () { aJ(i, { width: this.offsetWidth, height: this.offsetHeight }); aJ(a5, { width: this.offsetWidth, height: this.offsetHeight }); L.body.removeChild(this); a6.paper.safari(); }; L.body[aL](bg); bg.src = R[1]; a6.paper.defs[aL](i); bb.style.fill = "url(#" + i.id + ")"; aJ(bb, { fill: "url(#" + i.id + ")" }); a6.pattern = i; a6.pattern && N(a6); break; } if (!an.getRGB(a8).error) { delete bf.gradient; delete a7.gradient; !an.is(a7.opacity, "undefined") && an.is(bf.opacity, "undefined") && aJ(bb, { opacity: a7.opacity }); !an.is(a7["fill-opacity"], "undefined") && an.is(bf["fill-opacity"], "undefined") && aJ(bb, { "fill-opacity": a7["fill-opacity"] }); } else { if ((({ circle: 1, ellipse: 1 })[Q](a6.type) || (a8 + at).charAt() != "r") && b(bb, a8, a6.paper)) { a7.gradient = a8; a7.fill = "none"; break; } } case "stroke": bb[v](ba, an.getRGB(a8).hex); break; case "gradient": (({ circle: 1, ellipse: 1 })[Q](a6.type) || (a8 + at).charAt() != "r") && b(bb, a8, a6.paper); break; case "opacity": case "fill-opacity": if (a7.gradient) { var d = L.getElementById(bb.getAttribute("fill")[aP](/^url\(#|\)$/g, at)); if (d) { var a1 = d.getElementsByTagName("stop"); a1[a1[m] - 1][v]("stop-opacity", a8); } break; } default: ba == "font-size" && (a8 = G(a8, 10) + "px"); var a4 = ba[aP](/(\-.)/g, function (bh) { return aN.call(bh.substring(1)); }); bb.style[a4] = a8; bb[v](ba, a8); break; } } } D(a6, bf); if (a2) { a6.rotate(a2.join(am)); } else { W(a3) && a6.rotate(a3, true); } }; var h = 1.2; var D = function (d, R) { if (d.type != "text" || !(R[Q]("text") || R[Q]("font") || R[Q]("font-size") || R[Q]("x") || R[Q]("y"))) { return; } var a3 = d.attrs, e = d.node, a5 = e.firstChild ? G(L.defaultView.getComputedStyle(e.firstChild, at).getPropertyValue("font-size"), 10) : 10; if (R[Q]("text")) { a3.text = R.text; while (e.firstChild) { e.removeChild(e.firstChild); } var E = (R.text + at)[z]("\n"); for (var S = 0, a4 = E[m]; S < a4; S++) { if (E[S]) { var a1 = aJ("tspan"); S && aJ(a1, { dy: a5 * h, x: a3.x }); a1[aL](L.createTextNode(E[S])); e[aL](a1); } } } else { var E = e.getElementsByTagName("tspan"); for (var S = 0, a4 = E[m]; S < a4; S++) { S && aJ(E[S], { dy: a5 * h, x: a3.x }); } } aJ(e, { y: a3.y }); var a0 = d.getBBox(), a2 = a3.y - (a0.y + a0.height / 2); a2 && isFinite(a2) && aJ(e, { y: a3.y + a2 }); }; var ax = function (e, d) { var E = 0, i = 0; this[0] = e; this.id = an._oid++; this.node = e; e.raphael = this; this.paper = d; this.attrs = this.attrs || {}; this.transformations = []; this._ = { tx: 0, ty: 0, rt: { deg: 0, cx: 0, cy: 0 }, sx: 1, sy: 1 }; !d.bottom && (d.bottom = this); this.prev = d.top; d.top && (d.top.next = this); d.top = this; this.next = null; }; ax[aY].rotate = function (e, d, E) { if (this.removed) { return this; } if (e == null) { if (this._.rt.cx) { return [this._.rt.deg, this._.rt.cx, this._.rt.cy][az](am); } return this._.rt.deg; } var i = this.getBBox(); e = (e + at)[z](a); if (e[m] - 1) { d = W(e[1]); E = W(e[2]); } e = W(e[0]); if (d != null) { this._.rt.deg = e; } else { this._.rt.deg += e; } (E == null) && (d = null); this._.rt.cx = d; this._.rt.cy = E; d = d == null ? i.x + i.width / 2 : d; E = E == null ? i.y + i.height / 2 : E; if (this._.rt.deg) { this.transformations[0] = an.format("rotate({0} {1} {2})", this._.rt.deg, d, E); this.clip && aJ(this.clip, { transform: an.format("rotate({0} {1} {2})", -this._.rt.deg, d, E) }); } else { this.transformations[0] = at; this.clip && aJ(this.clip, { transform: at }); } aJ(this.node, { transform: this.transformations[az](am) }); return this; }; ax[aY].hide = function () { !this.removed && (this.node.style.display = "none"); return this; }; ax[aY].show = function () { !this.removed && (this.node.style.display = ""); return this; }; ax[aY].remove = function () { if (this.removed) { return; } ak(this, this.paper); this.node.parentNode.removeChild(this.node); for (var d in this) { delete this[d]; } this.removed = true; }; ax[aY].getBBox = function () { if (this.removed) { return this; } if (this.type == "path") { return U(this.attrs.path); } if (this.node.style.display == "none") { this.show(); var E = true; } var a1 = {}; try { a1 = this.node.getBBox(); } catch (S) { } finally { a1 = a1 || {}; } if (this.type == "text") { a1 = { x: a1.x, y: Infinity, width: 0, height: 0 }; for (var d = 0, R = this.node.getNumberOfChars() ; d < R; d++) { var a0 = this.node.getExtentOfChar(d); (a0.y < a1.y) && (a1.y = a0.y); (a0.y + a0.height - a1.y > a1.height) && (a1.height = a0.y + a0.height - a1.y); (a0.x + a0.width - a1.x > a1.width) && (a1.width = a0.x + a0.width - a1.x); } } E && this.hide(); return a1; }; ax[aY].attr = function () { if (this.removed) { return this; } if (arguments[m] == 0) { var R = {}; for (var E in this.attrs) { if (this.attrs[Q](E)) { R[E] = this.attrs[E]; } } this._.rt.deg && (R.rotation = this.rotate()); (this._.sx != 1 || this._.sy != 1) && (R.scale = this.scale()); R.gradient && R.fill == "none" && (R.fill = R.gradient) && delete R.gradient; return R; } if (arguments[m] == 1 && an.is(arguments[0], "string")) { if (arguments[0] == "translation") { return t.call(this); } if (arguments[0] == "rotation") { return this.rotate(); } if (arguments[0] == "scale") { return this.scale(); } if (arguments[0] == "fill" && this.attrs.fill == "none" && this.attrs.gradient) { return this.attrs.gradient; } return this.attrs[arguments[0]]; } if (arguments[m] == 1 && an.is(arguments[0], "array")) { var d = {}; for (var e in arguments[0]) { if (arguments[0][Q](e)) { d[arguments[0][e]] = this.attrs[arguments[0][e]]; } } return d; } if (arguments[m] == 2) { var S = {}; S[arguments[0]] = arguments[1]; aa(this, S); } else { if (arguments[m] == 1 && an.is(arguments[0], "object")) { aa(this, arguments[0]); } } return this; }; ax[aY].toFront = function () { if (this.removed) { return this; } this.node.parentNode[aL](this.node); var d = this.paper; d.top != this && Y(this, d); return this; }; ax[aY].toBack = function () { if (this.removed) { return this; } if (this.node.parentNode.firstChild != this.node) { this.node.parentNode.insertBefore(this.node, this.node.parentNode.firstChild); k(this, this.paper); var d = this.paper; } return this; }; ax[aY].insertAfter = function (d) { if (this.removed) { return this; } var e = d.node; if (e.nextSibling) { e.parentNode.insertBefore(this.node, e.nextSibling); } else { e.parentNode[aL](this.node); } A(this, d, this.paper); return this; }; ax[aY].insertBefore = function (d) { if (this.removed) { return this; } var e = d.node; e.parentNode.insertBefore(this.node, e); aq(this, d, this.paper); return this; }; var P = function (e, d, S, R) { d = O(d); S = O(S); var E = aJ("circle"); e.canvas && e.canvas[aL](E); var i = new ax(E, e); i.attrs = { cx: d, cy: S, r: R, fill: "none", stroke: "#000" }; i.type = "circle"; aJ(E, i.attrs); return i; }; var aF = function (i, d, a1, e, S, a0) { d = O(d); a1 = O(a1); var R = aJ("rect"); i.canvas && i.canvas[aL](R); var E = new ax(R, i); E.attrs = { x: d, y: a1, width: e, height: S, r: a0 || 0, rx: a0 || 0, ry: a0 || 0, fill: "none", stroke: "#000" }; E.type = "rect"; aJ(R, E.attrs); return E; }; var ai = function (e, d, a0, S, R) { d = O(d); a0 = O(a0); var E = aJ("ellipse"); e.canvas && e.canvas[aL](E); var i = new ax(E, e); i.attrs = { cx: d, cy: a0, rx: S, ry: R, fill: "none", stroke: "#000" }; i.type = "ellipse"; aJ(E, i.attrs); return i; }; var o = function (i, a0, d, a1, e, S) { var R = aJ("image"); aJ(R, { x: d, y: a1, width: e, height: S, preserveAspectRatio: "none" }); R.setAttributeNS(i.xlink, "href", a0); i.canvas && i.canvas[aL](R); var E = new ax(R, i); E.attrs = { x: d, y: a1, width: e, height: S, src: a0 }; E.type = "image"; return E; }; var X = function (e, d, S, R) { var E = aJ("text"); aJ(E, { x: d, y: S, "text-anchor": "middle" }); e.canvas && e.canvas[aL](E); var i = new ax(E, e); i.attrs = { x: d, y: S, "text-anchor": "middle", text: R, font: j.font, stroke: "none", fill: "#000" }; i.type = "text"; aa(i, i.attrs); return i; }; var aV = function (e, d) { this.width = e || this.width; this.height = d || this.height; this.canvas[v]("width", this.width); this.canvas[v]("height", this.height); return this; }; var w = function () { var E = ao[aW](null, arguments), i = E && E.container, e = E.x, a0 = E.y, R = E.width, d = E.height; if (!i) { throw new Error("SVG container not found."); } var S = aJ("svg"); R = R || 512; d = d || 342; aJ(S, { xmlns: "http://www.w3.org/2000/svg", version: 1.1, width: R, height: d }); if (i == 1) { S.style.cssText = "position:absolute;left:" + e + "px;top:" + a0 + "px"; L.body[aL](S); } else { if (i.firstChild) { i.insertBefore(S, i.firstChild); } else { i[aL](S); } } i = new aT; i.width = R; i.height = d; i.canvas = S; aG.call(i, i, an.fn); i.clear(); return i; }; aT[aY].clear = function () { var d = this.canvas; while (d.firstChild) { d.removeChild(d.firstChild); } this.bottom = this.top = null; (this.desc = aJ("desc"))[aL](L.createTextNode("Created with Rapha\xebl")); d[aL](this.desc); d[aL](this.defs = aJ("defs")); }; aT[aY].remove = function () { this.canvas.parentNode && this.canvas.parentNode.removeChild(this.canvas); for (var d in this) { this[d] = s(d); } }; } if (an.vml) { var aH = function (a8) { var a5 = /[ahqstv]/ig, a0 = r; (a8 + at).match(a5) && (a0 = H); a5 = /[clmz]/g; if (a0 == r && !(a8 + at).match(a5)) { var e = { M: "m", L: "l", C: "c", Z: "x", m: "t", l: "r", c: "v", z: "x" }, R = /([clmz]),?([^clmz]*)/gi, S = /-?[^,\s-]+/g; var a4 = (a8 + at)[aP](R, function (a9, bb, i) { var ba = []; i[aP](S, function (bc) { ba[f](O(bc)); }); return e[bb] + ba; }); return a4; } var a6 = a0(a8), E, a4 = [], d; for (var a2 = 0, a7 = a6[m]; a2 < a7; a2++) { E = a6[a2]; d = aZ.call(a6[a2][0]); d == "z" && (d = "x"); for (var a1 = 1, a3 = E[m]; a1 < a3; a1++) { d += O(E[a1]) + (a1 != a3 - 1 ? "," : at); } a4[f](d); } return a4[az](am); }; an[aA] = function () { return "Your browser doesn\u2019t support SVG. Falling down to VML.\nYou are running Rapha\xebl " + this.version; }; var q = function (d, S) { var E = ah("group"); E.style.cssText = "position:absolute;left:0;top:0;width:" + S.width + "px;height:" + S.height + "px"; E.coordsize = S.coordsize; E.coordorigin = S.coordorigin; var i = ah("shape"), e = i.style; e.width = S.width + "px"; e.height = S.height + "px"; i.coordsize = this.coordsize; i.coordorigin = this.coordorigin; E[aL](i); var R = new ax(i, E, S); R.isAbsolute = true; R.type = "path"; R.path = []; R.Path = at; d && aa(R, { fill: "none", stroke: "#000", path: d }); S.canvas[aL](E); return R; }; var aa = function (a3, a8) { a3.attrs = a3.attrs || {}; var a6 = a3.node, a9 = a3.attrs, a0 = a6.style, E, bd = a3; for (var a1 in a8) { if (a8[Q](a1)) { a9[a1] = a8[a1]; } } a8.href && (a6.href = a8.href); a8.title && (a6.title = a8.title); a8.target && (a6.target = a8.target); a8.cursor && (a0.cursor = a8.cursor); if (a8.path && a3.type == "path") { a9.path = a8.path; a6.path = aH(a9.path); } if (a8.rotation != null) { a3.rotate(a8.rotation, true); } if (a8.translation) { E = (a8.translation + at)[z](a); t.call(a3, E[0], E[1]); if (a3._.rt.cx != null) { a3._.rt.cx += +E[0]; a3._.rt.cy += +E[1]; a3.setBox(a3.attrs, E[0], E[1]); } } if (a8.scale) { E = (a8.scale + at)[z](a); a3.scale(+E[0] || 1, +E[1] || +E[0] || 1, +E[2] || null, +E[3] || null); } if ("clip-rect" in a8) { var d = (a8["clip-rect"] + at)[z](a); if (d[m] == 4) { d[2] = +d[2] + (+d[0]); d[3] = +d[3] + (+d[1]); var a2 = a6.clipRect || L.createElement("div"), bc = a2.style, S = a6.parentNode; bc.clip = an.format("rect({1}px {2}px {3}px {0}px)", d); if (!a6.clipRect) { bc.position = "absolute"; bc.top = 0; bc.left = 0; bc.width = a3.paper.width + "px"; bc.height = a3.paper.height + "px"; S.parentNode.insertBefore(a2, S); a2[aL](S); a6.clipRect = a2; } } if (!a8["clip-rect"]) { a6.clipRect && (a6.clipRect.style.clip = at); } } if (a3.type == "image" && a8.src) { a6.src = a8.src; } if (a3.type == "image" && a8.opacity) { a6.filterOpacity = " progid:DXImageTransform.Microsoft.Alpha(opacity=" + (a8.opacity * 100) + ")"; a0.filter = (a6.filterMatrix || at) + (a6.filterOpacity || at); } a8.font && (a0.font = a8.font); a8["font-family"] && (a0.fontFamily = '"' + a8["font-family"][z](",")[0][aP](/^['"]+|['"]+$/g, at) + '"'); a8["font-size"] && (a0.fontSize = a8["font-size"]); a8["font-weight"] && (a0.fontWeight = a8["font-weight"]); a8["font-style"] && (a0.fontStyle = a8["font-style"]); if (a8.opacity != null || a8["stroke-width"] != null || a8.fill != null || a8.stroke != null || a8["stroke-width"] != null || a8["stroke-opacity"] != null || a8["fill-opacity"] != null || a8["stroke-dasharray"] != null || a8["stroke-miterlimit"] != null || a8["stroke-linejoin"] != null || a8["stroke-linecap"] != null) { a6 = a3.shape || a6; var a7 = (a6.getElementsByTagName("fill") && a6.getElementsByTagName("fill")[0]), ba = false; !a7 && (ba = a7 = ah("fill")); if ("fill-opacity" in a8 || "opacity" in a8) { var e = ((+a9["fill-opacity"] + 1 || 2) - 1) * ((+a9.opacity + 1 || 2) - 1); e < 0 && (e = 0); e > 1 && (e = 1); a7.opacity = e; } a8.fill && (a7.on = true); if (a7.on == null || a8.fill == "none") { a7.on = false; } if (a7.on && a8.fill) { var i = a8.fill.match(c); if (i) { a7.src = i[1]; a7.type = "tile"; } else { a7.color = an.getRGB(a8.fill).hex; a7.src = at; a7.type = "solid"; if (an.getRGB(a8.fill).error && (bd.type in { circle: 1, ellipse: 1 } || (a8.fill + at).charAt() != "r") && b(bd, a8.fill)) { a9.fill = "none"; a9.gradient = a8.fill; } } } ba && a6[aL](a7); var R = (a6.getElementsByTagName("stroke") && a6.getElementsByTagName("stroke")[0]), bb = false; !R && (bb = R = ah("stroke")); if ((a8.stroke && a8.stroke != "none") || a8["stroke-width"] || a8["stroke-opacity"] != null || a8["stroke-dasharray"] || a8["stroke-miterlimit"] || a8["stroke-linejoin"] || a8["stroke-linecap"]) { R.on = true; } (a8.stroke == "none" || R.on == null || a8.stroke == 0 || a8["stroke-width"] == 0) && (R.on = false); R.on && a8.stroke && (R.color = an.getRGB(a8.stroke).hex); var e = ((+a9["stroke-opacity"] + 1 || 2) - 1) * ((+a9.opacity + 1 || 2) - 1), a4 = (W(a8["stroke-width"]) || 1) * 0.75; e < 0 && (e = 0); e > 1 && (e = 1); a8["stroke-width"] == null && (a4 = a9["stroke-width"]); a8["stroke-width"] && (R.weight = a4); a4 && a4 < 1 && (e *= a4) && (R.weight = 1); R.opacity = e; a8["stroke-linejoin"] && (R.joinstyle = a8["stroke-linejoin"] || "miter"); R.miterlimit = a8["stroke-miterlimit"] || 8; a8["stroke-linecap"] && (R.endcap = a8["stroke-linecap"] == "butt" ? "flat" : a8["stroke-linecap"] == "square" ? "square" : "round"); if (a8["stroke-dasharray"]) { var a5 = { "-": "shortdash", ".": "shortdot", "-.": "shortdashdot", "-..": "shortdashdotdot", ". ": "dot", "- ": "dash", "--": "longdash", "- .": "dashdot", "--.": "longdashdot", "--..": "longdashdotdot" }; R.dashstyle = a5[Q](a8["stroke-dasharray"]) ? a5[a8["stroke-dasharray"]] : at; } bb && a6[aL](R); } if (bd.type == "text") { var a0 = bd.paper.span.style; a9.font && (a0.font = a9.font); a9["font-family"] && (a0.fontFamily = a9["font-family"]); a9["font-size"] && (a0.fontSize = a9["font-size"]); a9["font-weight"] && (a0.fontWeight = a9["font-weight"]); a9["font-style"] && (a0.fontStyle = a9["font-style"]); bd.node.string && (bd.paper.span.innerHTML = (bd.node.string + at)[aP](/</g, "&#60;")[aP](/&/g, "&#38;")[aP](/\n/g, "<br>")); bd.W = a9.w = bd.paper.span.offsetWidth; bd.H = a9.h = bd.paper.span.offsetHeight; bd.X = a9.x; bd.Y = a9.y + O(bd.H / 2); switch (a9["text-anchor"]) { case "start": bd.node.style["v-text-align"] = "left"; bd.bbx = O(bd.W / 2); break; case "end": bd.node.style["v-text-align"] = "right"; bd.bbx = -O(bd.W / 2); break; default: bd.node.style["v-text-align"] = "center"; break; } } }; var b = function (d, a1) { d.attrs = d.attrs || {}; var a2 = d.attrs, a4 = d.node.getElementsByTagName("fill"), S = "linear", a0 = ".5 .5"; d.attrs.gradient = a1; a1 = (a1 + at)[aP](ar, function (a6, a7, i) { S = "radial"; if (a7 && i) { a7 = W(a7); i = W(i); aM(a7 - 0.5, 2) + aM(i - 0.5, 2) > 0.25 && (i = ab.sqrt(0.25 - aM(a7 - 0.5, 2)) * ((i > 0.5) * 2 - 1) + 0.5); a0 = a7 + am + i; } return at; }); a1 = a1[z](/\s*\-\s*/); if (S == "linear") { var e = a1.shift(); e = -W(e); if (isNaN(e)) { return null; } } var R = p(a1); if (!R) { return null; } d = d.shape || d.node; a4 = a4[0] || ah("fill"); if (R[m]) { a4.on = true; a4.method = "none"; a4.type = (S == "radial") ? "gradientradial" : "gradient"; a4.color = R[0].color; a4.color2 = R[R[m] - 1].color; var a5 = []; for (var E = 0, a3 = R[m]; E < a3; E++) { R[E].offset && a5[f](R[E].offset + am + R[E].color); } a4.colors && (a4.colors.value = a5[m] ? a5[az](",") : "0% " + a4.color); if (S == "radial") { a4.focus = "100%"; a4.focussize = a0; a4.focusposition = a0; } else { a4.angle = (270 - e) % 360; } } return 1; }; var ax = function (R, a0, d) { var S = 0, i = 0, e = 0, E = 1; this[0] = R; this.id = an._oid++; this.node = R; R.raphael = this; this.X = 0; this.Y = 0; this.attrs = {}; this.Group = a0; this.paper = d; this._ = { tx: 0, ty: 0, rt: { deg: 0 }, sx: 1, sy: 1 }; !d.bottom && (d.bottom = this); this.prev = d.top; d.top && (d.top.next = this); d.top = this; this.next = null; }; ax[aY].rotate = function (e, d, i) { if (this.removed) { return this; } if (e == null) { if (this._.rt.cx) { return [this._.rt.deg, this._.rt.cx, this._.rt.cy][az](am); } return this._.rt.deg; } e = (e + at)[z](a); if (e[m] - 1) { d = W(e[1]); i = W(e[2]); } e = W(e[0]); if (d != null) { this._.rt.deg = e; } else { this._.rt.deg += e; } i == null && (d = null); this._.rt.cx = d; this._.rt.cy = i; this.setBox(this.attrs, d, i); this.Group.style.rotation = this._.rt.deg; return this; }; ax[aY].setBox = function (bb, e, d) { if (this.removed) { return this; } var a5 = this.Group.style, R = (this.shape && this.shape.style) || this.node.style; bb = bb || {}; for (var a9 in bb) { if (bb[Q](a9)) { this.attrs[a9] = bb[a9]; } } e = e || this._.rt.cx; d = d || this._.rt.cy; var a7 = this.attrs, a1, a0, a2, ba; switch (this.type) { case "circle": a1 = a7.cx - a7.r; a0 = a7.cy - a7.r; a2 = ba = a7.r * 2; break; case "ellipse": a1 = a7.cx - a7.rx; a0 = a7.cy - a7.ry; a2 = a7.rx * 2; ba = a7.ry * 2; break; case "rect": case "image": a1 = +a7.x; a0 = +a7.y; a2 = a7.width || 0; ba = a7.height || 0; break; case "text": this.textpath.v = ["m", O(a7.x), ", ", O(a7.y - 2), "l", O(a7.x) + 1, ", ", O(a7.y - 2)][az](at); a1 = a7.x - O(this.W / 2); a0 = a7.y - this.H / 2; a2 = this.W; ba = this.H; break; case "path": if (!this.attrs.path) { a1 = 0; a0 = 0; a2 = this.paper.width; ba = this.paper.height; } else { var a8 = U(this.attrs.path); a1 = a8.x; a0 = a8.y; a2 = a8.width; ba = a8.height; } break; default: a1 = 0; a0 = 0; a2 = this.paper.width; ba = this.paper.height; break; } e = (e == null) ? a1 + a2 / 2 : e; d = (d == null) ? a0 + ba / 2 : d; var E = e - this.paper.width / 2, a4 = d - this.paper.height / 2; if (this.type == "path" || this.type == "text") { (a5.left != E + "px") && (a5.left = E + "px"); (a5.top != a4 + "px") && (a5.top = a4 + "px"); this.X = this.type == "text" ? a1 : -E; this.Y = this.type == "text" ? a0 : -a4; this.W = a2; this.H = ba; (R.left != -E + "px") && (R.left = -E + "px"); (R.top != -a4 + "px") && (R.top = -a4 + "px"); } else { (a5.left != E + "px") && (a5.left = E + "px"); (a5.top != a4 + "px") && (a5.top = a4 + "px"); this.X = a1; this.Y = a0; this.W = a2; this.H = ba; (a5.width != this.paper.width + "px") && (a5.width = this.paper.width + "px"); (a5.height != this.paper.height + "px") && (a5.height = this.paper.height + "px"); (R.left != a1 - E + "px") && (R.left = a1 - E + "px"); (R.top != a0 - a4 + "px") && (R.top = a0 - a4 + "px"); (R.width != a2 + "px") && (R.width = a2 + "px"); (R.height != ba + "px") && (R.height = ba + "px"); var S = (+bb.r || 0) / aI(a2, ba); if (this.type == "rect" && this.arcsize.toFixed(4) != S.toFixed(4) && (S || this.arcsize)) { var a6 = ah("roundrect"), bc = {}, a9 = 0, a3 = this.events && this.events[m]; a6.arcsize = S; a6.raphael = this; this.Group[aL](a6); this.Group.removeChild(this.node); this[0] = this.node = a6; this.arcsize = S; for (var a9 in a7) { bc[a9] = a7[a9]; } delete bc.scale; this.attr(bc); if (this.events) { for (; a9 < a3; a9++) { this.events[a9].unbind = ae(this.node, this.events[a9].name, this.events[a9].f, this); } } } } }; ax[aY].hide = function () { !this.removed && (this.Group.style.display = "none"); return this; }; ax[aY].show = function () { !this.removed && (this.Group.style.display = "block"); return this; }; ax[aY].getBBox = function () { if (this.removed) { return this; } if (this.type == "path") { return U(this.attrs.path); } return { x: this.X + (this.bbx || 0), y: this.Y, width: this.W, height: this.H }; }; ax[aY].remove = function () { if (this.removed) { return; } ak(this, this.paper); this.node.parentNode.removeChild(this.node); this.Group.parentNode.removeChild(this.Group); this.shape && this.shape.parentNode.removeChild(this.shape); for (var d in this) { delete this[d]; } this.removed = true; }; ax[aY].attr = function () { if (this.removed) { return this; } if (arguments[m] == 0) { var E = {}; for (var e in this.attrs) { if (this.attrs[Q](e)) { E[e] = this.attrs[e]; } } this._.rt.deg && (E.rotation = this.rotate()); (this._.sx != 1 || this._.sy != 1) && (E.scale = this.scale()); E.gradient && E.fill == "none" && (E.fill = E.gradient) && delete E.gradient; return E; } if (arguments[m] == 1 && an.is(arguments[0], "string")) { if (arguments[0] == "translation") { return t.call(this); } if (arguments[0] == "rotation") { return this.rotate(); } if (arguments[0] == "scale") { return this.scale(); } if (arguments[0] == "fill" && this.attrs.fill == "none" && this.attrs.gradient) { return this.attrs.gradient; } return this.attrs[arguments[0]]; } if (this.attrs && arguments[m] == 1 && an.is(arguments[0], "array")) { var d = {}; for (var e = 0, R = arguments[0][m]; e < R; e++) { d[arguments[0][e]] = this.attrs[arguments[0][e]]; } return d; } var S; if (arguments[m] == 2) { S = {}; S[arguments[0]] = arguments[1]; } arguments[m] == 1 && an.is(arguments[0], "object") && (S = arguments[0]); if (S) { if (S.text && this.type == "text") { this.node.string = S.text; } aa(this, S); if (S.gradient && (({ circle: 1, ellipse: 1 })[Q](this.type) || (S.gradient + at).charAt() != "r")) { b(this, S.gradient); } (this.type != "path" || this._.rt.deg) && this.setBox(this.attrs); } return this; }; ax[aY].toFront = function () { !this.removed && this.Group.parentNode[aL](this.Group); this.paper.top != this && Y(this, this.paper); return this; }; ax[aY].toBack = function () { if (this.removed) { return this; } if (this.Group.parentNode.firstChild != this.Group) { this.Group.parentNode.insertBefore(this.Group, this.Group.parentNode.firstChild); k(this, this.paper); } return this; }; ax[aY].insertAfter = function (d) { if (this.removed) { return this; } if (d.Group.nextSibling) { d.Group.parentNode.insertBefore(this.Group, d.Group.nextSibling); } else { d.Group.parentNode[aL](this.Group); } A(this, d, this.paper); return this; }; ax[aY].insertBefore = function (d) { if (this.removed) { return this; } d.Group.parentNode.insertBefore(this.Group, d.Group); aq(this, d, this.paper); return this; }; var P = function (e, d, a1, S) { var R = ah("group"), a0 = ah("oval"), i = a0.style; R.style.cssText = "position:absolute;left:0;top:0;width:" + e.width + "px;height:" + e.height + "px"; R.coordsize = e.coordsize; R.coordorigin = e.coordorigin; R[aL](a0); var E = new ax(a0, R, e); E.type = "circle"; aa(E, { stroke: "#000", fill: "none" }); E.attrs.cx = d; E.attrs.cy = a1; E.attrs.r = S; E.setBox({ x: d - S, y: a1 - S, width: S * 2, height: S * 2 }); e.canvas[aL](R); return E; }, aF = function (e, a1, a0, a2, E, d) { var R = ah("group"), i = ah("roundrect"), a3 = (+d || 0) / (aI(a2, E)); R.style.cssText = "position:absolute;left:0;top:0;width:" + e.width + "px;height:" + e.height + "px"; R.coordsize = e.coordsize; R.coordorigin = e.coordorigin; R[aL](i); i.arcsize = a3; var S = new ax(i, R, e); S.type = "rect"; aa(S, { stroke: "#000" }); S.arcsize = a3; S.setBox({ x: a1, y: a0, width: a2, height: E, r: d }); e.canvas[aL](R); return S; }, ai = function (d, a2, a1, i, e) { var R = ah("group"), E = ah("oval"), a0 = E.style; R.style.cssText = "position:absolute;left:0;top:0;width:" + d.width + "px;height:" + d.height + "px"; R.coordsize = d.coordsize; R.coordorigin = d.coordorigin; R[aL](E); var S = new ax(E, R, d); S.type = "ellipse"; aa(S, { stroke: "#000" }); S.attrs.cx = a2; S.attrs.cy = a1; S.attrs.rx = i; S.attrs.ry = e; S.setBox({ x: a2 - i, y: a1 - e, width: i * 2, height: e * 2 }); d.canvas[aL](R); return S; }, o = function (e, d, a2, a1, a3, E) { var R = ah("group"), i = ah("image"), a0 = i.style; R.style.cssText = "position:absolute;left:0;top:0;width:" + e.width + "px;height:" + e.height + "px"; R.coordsize = e.coordsize; R.coordorigin = e.coordorigin; i.src = d; R[aL](i); var S = new ax(i, R, e); S.type = "image"; S.attrs.src = d; S.attrs.x = a2; S.attrs.y = a1; S.attrs.w = a3; S.attrs.h = E; S.setBox({ x: a2, y: a1, width: a3, height: E }); e.canvas[aL](R); return S; }, X = function (e, a2, a1, a3) { var R = ah("group"), E = ah("shape"), a0 = E.style, a4 = ah("path"), d = a4.style, i = ah("textpath"); R.style.cssText = "position:absolute;left:0;top:0;width:" + e.width + "px;height:" + e.height + "px"; R.coordsize = e.coordsize; R.coordorigin = e.coordorigin; a4.v = an.format("m{0},{1}l{2},{1}", O(a2), O(a1), O(a2) + 1); a4.textpathok = true; a0.width = e.width; a0.height = e.height; i.string = a3 + at; i.on = true; E[aL](i); E[aL](a4); R[aL](E); var S = new ax(i, R, e); S.shape = E; S.textpath = a4; S.type = "text"; S.attrs.text = a3; S.attrs.x = a2; S.attrs.y = a1; S.attrs.w = 1; S.attrs.h = 1; aa(S, { font: j.font, stroke: "none", fill: "#000" }); S.setBox(); e.canvas[aL](R); return S; }, aV = function (i, d) { var e = this.canvas.style; i == +i && (i += "px"); d == +d && (d += "px"); e.width = i; e.height = d; e.clip = "rect(0 " + i + " " + d + " 0)"; return this; }, ah; L.createStyleSheet().addRule(".rvml", "behavior:url(#default#VML)"); try { !L.namespaces.rvml && L.namespaces.add("rvml", "urn:schemas-microsoft-com:vml"); ah = function (d) { return L.createElement("<rvml:" + d + ' class="rvml">'); }; } catch (af) { ah = function (d) { return L.createElement("<" + d + ' xmlns="urn:schemas-microsoft.com:vml" class="rvml">'); }; } var w = function () { var i = ao[aW](null, arguments), d = i.container, a2 = i.height, a3, e = i.width, a1 = i.x, a0 = i.y; if (!d) { throw new Error("VML container not found."); } var R = new aT, S = R.canvas = L.createElement("div"), E = S.style; e = e || 512; a2 = a2 || 342; e == +e && (e += "px"); a2 == +a2 && (a2 += "px"); R.width = 1000; R.height = 1000; R.coordsize = "1000 1000"; R.coordorigin = "0 0"; R.span = L.createElement("span"); R.span.style.cssText = "position:absolute;left:-9999em;top:-9999em;padding:0;margin:0;line-height:1;display:inline;"; S[aL](R.span); E.cssText = an.format("width:{0};height:{1};position:absolute;clip:rect(0 {0} {1} 0);overflow:hidden", e, a2); if (d == 1) { L.body[aL](S); E.left = a1 + "px"; E.top = a0 + "px"; } else { d.style.width = e; d.style.height = a2; if (d.firstChild) { d.insertBefore(S, d.firstChild); } else { d[aL](S); } } aG.call(R, R, an.fn); return R; }; aT[aY].clear = function () { this.canvas.innerHTML = at; this.span = L.createElement("span"); this.span.style.cssText = "position:absolute;left:-9999em;top:-9999em;padding:0;margin:0;line-height:1;display:inline;"; this.canvas[aL](this.span); this.bottom = this.top = null; }; aT[aY].remove = function () { this.canvas.parentNode.removeChild(this.canvas); for (var d in this) { this[d] = s(d); } }; } if ((/^Apple|^Google/).test(navigator.vendor) && !(navigator.userAgent.indexOf("Version/4.0") + 1)) { aT[aY].safari = function () { var d = this.rect(-99, -99, this.width + 99, this.height + 99); setTimeout(function () { d.remove(); }); }; } else { aT[aY].safari = function () { }; } var ae = (function () { if (L.addEventListener) { return function (R, i, e, d) { var E = function (S) { return e.call(d, S); }; R.addEventListener(i, E, false); return function () { R.removeEventListener(i, E, false); return true; }; }; } else { if (L.attachEvent) { return function (S, E, i, e) { var R = function (a0) { return i.call(e, a0 || au.event); }; S.attachEvent("on" + E, R); var d = function () { S.detachEvent("on" + E, R); return true; }; return d; }; } } })(); for (var ac = F[m]; ac--;) { (function (d) { ax[aY][d] = function (e) { if (an.is(e, "function")) { this.events = this.events || []; this.events.push({ name: d, f: e, unbind: ae(this.shape || this.node, d, e, this) }); } return this; }; ax[aY]["un" + d] = function (E) { var i = this.events, e = i[m]; while (e--) { if (i[e].name == d && i[e].f == E) { i[e].unbind(); i.splice(e, 1); !i.length && delete this.events; return this; } } return this; }; })(F[ac]); } ax[aY].hover = function (e, d) { return this.mouseover(e).mouseout(d); }; ax[aY].unhover = function (e, d) { return this.unmouseover(e).unmouseout(d); }; aT[aY].circle = function (d, i, e) { return P(this, d || 0, i || 0, e || 0); }; aT[aY].rect = function (d, R, e, i, E) { return aF(this, d || 0, R || 0, e || 0, i || 0, E || 0); }; aT[aY].ellipse = function (d, E, i, e) { return ai(this, d || 0, E || 0, i || 0, e || 0); }; aT[aY].path = function (d) { d && !an.is(d, "string") && !an.is(d[0], "array") && (d += at); return q(an.format[aW](an, arguments), this); }; aT[aY].image = function (E, d, R, e, i) { return o(this, E || "about:blank", d || 0, R || 0, e || 0, i || 0); }; aT[aY].text = function (d, i, e) { return X(this, d || 0, i || 0, e || at); }; aT[aY].set = function (d) { arguments[m] > 1 && (d = Array[aY].splice.call(arguments, 0, arguments[m])); return new T(d); }; aT[aY].setSize = aV; aT[aY].top = aT[aY].bottom = null; aT[aY].raphael = an; function u() { return this.x + am + this.y; } ax[aY].scale = function (a6, a5, E, e) { if (a6 == null && a5 == null) { return { x: this._.sx, y: this._.sy, toString: u }; } a5 = a5 || a6; !+a5 && (a5 = a6); var ba, a8, a9, a7, bm = this.attrs; if (a6 != 0) { var a4 = this.getBBox(), a1 = a4.x + a4.width / 2, R = a4.y + a4.height / 2, bl = a6 / this._.sx, bk = a5 / this._.sy; E = (+E || E == 0) ? E : a1; e = (+e || e == 0) ? e : R; var a3 = ~~(a6 / ab.abs(a6)), a0 = ~~(a5 / ab.abs(a5)), be = this.node.style, bo = E + (a1 - E) * bl, bn = e + (R - e) * bk; switch (this.type) { case "rect": case "image": var a2 = bm.width * a3 * bl, bd = bm.height * a0 * bk; this.attr({ height: bd, r: bm.r * aI(a3 * bl, a0 * bk), width: a2, x: bo - a2 / 2, y: bn - bd / 2 }); break; case "circle": case "ellipse": this.attr({ rx: bm.rx * a3 * bl, ry: bm.ry * a0 * bk, r: bm.r * aI(a3 * bl, a0 * bk), cx: bo, cy: bn }); break; case "path": var bg = ad(bm.path), bh = true; for (var bj = 0, bc = bg[m]; bj < bc; bj++) { var bf = bg[bj], bi, S = aN.call(bf[0]); if (S == "M" && bh) { continue; } else { bh = false; } if (S == "A") { bf[bg[bj][m] - 2] *= bl; bf[bg[bj][m] - 1] *= bk; bf[1] *= a3 * bl; bf[2] *= a0 * bk; bf[5] = +(a3 + a0 ? !!+bf[5] : !+bf[5]); } else { if (S == "H") { for (bi = 1, jj = bf[m]; bi < jj; bi++) { bf[bi] *= bl; } } else { if (S == "V") { for (bi = 1, jj = bf[m]; bi < jj; bi++) { bf[bi] *= bk; } } else { for (bi = 1, jj = bf[m]; bi < jj; bi++) { bf[bi] *= (bi % 2) ? bl : bk; } } } } } var d = U(bg), ba = bo - d.x - d.width / 2, a8 = bn - d.y - d.height / 2; bg[0][1] += ba; bg[0][2] += a8; this.attr({ path: bg }); break; } if (this.type in { text: 1, image: 1 } && (a3 != 1 || a0 != 1)) { if (this.transformations) { this.transformations[2] = "scale("[aS](a3, ",", a0, ")"); this.node[v]("transform", this.transformations[az](am)); ba = (a3 == -1) ? -bm.x - (a2 || 0) : bm.x; a8 = (a0 == -1) ? -bm.y - (bd || 0) : bm.y; this.attr({ x: ba, y: a8 }); bm.fx = a3 - 1; bm.fy = a0 - 1; } else { this.node.filterMatrix = " progid:DXImageTransform.Microsoft.Matrix(M11="[aS](a3, ", M12=0, M21=0, M22=", a0, ", Dx=0, Dy=0, sizingmethod='auto expand', filtertype='bilinear')"); be.filter = (this.node.filterMatrix || at) + (this.node.filterOpacity || at); } } else { if (this.transformations) { this.transformations[2] = at; this.node[v]("transform", this.transformations[az](am)); bm.fx = 0; bm.fy = 0; } else { this.node.filterMatrix = at; be.filter = (this.node.filterMatrix || at) + (this.node.filterOpacity || at); } } bm.scale = [a6, a5, E, e][az](am); this._.sx = a6; this._.sy = a5; } return this; }; ax[aY].clone = function () { var d = this.attr(); delete d.scale; delete d.translation; return this.paper[this.type]().attr(d); }; var aB = function (d, e) { return function (a9, S, a0) { a9 = H(a9); var a5, a4, E, a1, R = "", a8 = {}, a6, a3 = 0; for (var a2 = 0, a7 = a9.length; a2 < a7; a2++) { E = a9[a2]; if (E[0] == "M") { a5 = +E[1]; a4 = +E[2]; } else { a1 = n(a5, a4, E[1], E[2], E[3], E[4], E[5], E[6]); if (a3 + a1 > S) { if (e && !a8.start) { a6 = an.findDotsAtSegment(a5, a4, E[1], E[2], E[3], E[4], E[5], E[6], (S - a3) / a1); R += ["C", a6.start.x, a6.start.y, a6.m.x, a6.m.y, a6.x, a6.y]; if (a0) { return R; } a8.start = R; R = ["M", a6.x, a6.y + "C", a6.n.x, a6.n.y, a6.end.x, a6.end.y, E[5], E[6]][az](); a3 += a1; a5 = +E[5]; a4 = +E[6]; continue; } if (!d && !e) { a6 = an.findDotsAtSegment(a5, a4, E[1], E[2], E[3], E[4], E[5], E[6], (S - a3) / a1); return { x: a6.x, y: a6.y, alpha: a6.alpha }; } } a3 += a1; a5 = +E[5]; a4 = +E[6]; } R += E; } a8.end = R; a6 = d ? a3 : e ? a8 : an.findDotsAtSegment(a5, a4, E[1], E[2], E[3], E[4], E[5], E[6], 1); a6.alpha && (a6 = { x: a6.x, y: a6.y, alpha: a6.alpha }); return a6; }; }, n = aj(function (E, d, a0, S, a6, a5, a4, a3) { var R = { x: 0, y: 0 }, a2 = 0; for (var a1 = 0; a1 < 1.01; a1 += 0.01) { var e = M(E, d, a0, S, a6, a5, a4, a3, a1); a1 && (a2 += ab.sqrt(aM(R.x - e.x, 2) + aM(R.y - e.y, 2))); R = e; } return a2; }); var ap = aB(1), C = aB(), J = aB(0, 1); ax[aY].getTotalLength = function () { if (this.type != "path") { return; } return ap(this.attrs.path); }; ax[aY].getPointAtLength = function (d) { if (this.type != "path") { return; } return C(this.attrs.path, d); }; ax[aY].getSubpath = function (i, e) { if (this.type != "path") { return; } if (ab.abs(this.getTotalLength() - e) < 0.000001) { return J(this.attrs.path, i).end; } var d = J(this.attrs.path, e, 1); return i ? J(d, i).end : d; }; an.easing_formulas = { linear: function (d) { return d; }, "<": function (d) { return aM(d, 3); }, ">": function (d) { return aM(d - 1, 3) + 1; }, "<>": function (d) { d = d * 2; if (d < 1) { return aM(d, 3) / 2; } d -= 2; return (aM(d, 3) + 2) / 2; }, backIn: function (e) { var d = 1.70158; return e * e * ((d + 1) * e - d); }, backOut: function (e) { e = e - 1; var d = 1.70158; return e * e * ((d + 1) * e + d) + 1; }, elastic: function (i) { if (i == 0 || i == 1) { return i; } var e = 0.3, d = e / 4; return aM(2, -10 * i) * ab.sin((i - d) * (2 * ab.PI) / e) + 1; }, bounce: function (E) { var e = 7.5625, i = 2.75, d; if (E < (1 / i)) { d = e * E * E; } else { if (E < (2 / i)) { E -= (1.5 / i); d = e * E * E + 0.75; } else { if (E < (2.5 / i)) { E -= (2.25 / i); d = e * E * E + 0.9375; } else { E -= (2.625 / i); d = e * E * E + 0.984375; } } } return d; } }; var I = { length: 0 }, aR = function () { var a2 = +new Date; for (var be in I) { if (be != "length" && I[Q](be)) { var bj = I[be]; if (bj.stop) { delete I[be]; I[m]--; continue; } var a0 = a2 - bj.start, bb = bj.ms, ba = bj.easing, bf = bj.from, a7 = bj.diff, E = bj.to, a6 = bj.t, a9 = bj.prev || 0, a1 = bj.el, R = bj.callback, a8 = {}, d; if (a0 < bb) { var S = an.easing_formulas[ba] ? an.easing_formulas[ba](a0 / bb) : a0 / bb; for (var bc in bf) { if (bf[Q](bc)) { switch (Z[bc]) { case "along": d = S * bb * a7[bc]; E.back && (d = E.len - d); var bd = C(E[bc], d); a1.translate(a7.sx - a7.x || 0, a7.sy - a7.y || 0); a7.x = bd.x; a7.y = bd.y; a1.translate(bd.x - a7.sx, bd.y - a7.sy); E.rot && a1.rotate(a7.r + bd.alpha, bd.x, bd.y); break; case "number": d = +bf[bc] + S * bb * a7[bc]; break; case "colour": d = "rgb(" + [B(O(bf[bc].r + S * bb * a7[bc].r)), B(O(bf[bc].g + S * bb * a7[bc].g)), B(O(bf[bc].b + S * bb * a7[bc].b))][az](",") + ")"; break; case "path": d = []; for (var bh = 0, a5 = bf[bc][m]; bh < a5; bh++) { d[bh] = [bf[bc][bh][0]]; for (var bg = 1, bi = bf[bc][bh][m]; bg < bi; bg++) { d[bh][bg] = +bf[bc][bh][bg] + S * bb * a7[bc][bh][bg]; } d[bh] = d[bh][az](am); } d = d[az](am); break; case "csv": switch (bc) { case "translation": var a4 = a7[bc][0] * (a0 - a9), a3 = a7[bc][1] * (a0 - a9); a6.x += a4; a6.y += a3; d = a4 + am + a3; break; case "rotation": d = +bf[bc][0] + S * bb * a7[bc][0]; bf[bc][1] && (d += "," + bf[bc][1] + "," + bf[bc][2]); break; case "scale": d = [+bf[bc][0] + S * bb * a7[bc][0], +bf[bc][1] + S * bb * a7[bc][1], (2 in E[bc] ? E[bc][2] : at), (3 in E[bc] ? E[bc][3] : at)][az](am); break; case "clip-rect": d = []; var bh = 4; while (bh--) { d[bh] = +bf[bc][bh] + S * bb * a7[bc][bh]; } break; } break; } a8[bc] = d; } } a1.attr(a8); a1._run && a1._run.call(a1); } else { if (E.along) { var bd = C(E.along, E.len * !E.back); a1.translate(a7.sx - (a7.x || 0) + bd.x - a7.sx, a7.sy - (a7.y || 0) + bd.y - a7.sy); E.rot && a1.rotate(a7.r + bd.alpha, bd.x, bd.y); } (a6.x || a6.y) && a1.translate(-a6.x, -a6.y); E.scale && (E.scale = E.scale + at); a1.attr(E); delete I[be]; I[m]--; a1.in_animation = null; an.is(R, "function") && R.call(a1); } bj.prev = a0; } } an.svg && a1 && a1.paper.safari(); I[m] && setTimeout(aR); }, B = function (d) { return d > 255 ? 255 : (d < 0 ? 0 : d); }, t = function (d, i) { if (d == null) { return { x: this._.tx, y: this._.ty, toString: u }; } this._.tx += +d; this._.ty += +i; switch (this.type) { case "circle": case "ellipse": this.attr({ cx: +d + this.attrs.cx, cy: +i + this.attrs.cy }); break; case "rect": case "image": case "text": this.attr({ x: +d + this.attrs.x, y: +i + this.attrs.y }); break; case "path": var e = ad(this.attrs.path); e[0][1] += +d; e[0][2] += +i; this.attr({ path: e }); break; } return this; }; ax[aY].animateWith = function (e, i, d, R, E) { I[e.id] && (i.start = I[e.id].start); return this.animate(i, d, R, E); }; ax[aY].animateAlong = ay(); ax[aY].animateAlongBack = ay(1); function ay(d) { return function (E, i, e, S) { var R = { back: d }; an.is(e, "function") ? (S = e) : (R.rot = e); E && E.constructor == ax && (E = E.attrs.path); E && (R.along = E); return this.animate(R, i, S); }; } ax[aY].onAnimation = function (d) { this._run = d || 0; return this; }; ax[aY].animate = function (be, a5, a4, E) { if (an.is(a4, "function") || !a4) { E = a4 || null; } var a9 = {}, e = {}, a2 = {}; for (var a6 in be) { if (be[Q](a6)) { if (Z[Q](a6)) { a9[a6] = this.attr(a6); (a9[a6] == null) && (a9[a6] = j[a6]); e[a6] = be[a6]; switch (Z[a6]) { case "along": var bc = ap(be[a6]), a7 = C(be[a6], bc * !!be.back), R = this.getBBox(); a2[a6] = bc / a5; a2.tx = R.x; a2.ty = R.y; a2.sx = a7.x; a2.sy = a7.y; e.rot = be.rot; e.back = be.back; e.len = bc; be.rot && (a2.r = W(this.rotate()) || 0); break; case "number": a2[a6] = (e[a6] - a9[a6]) / a5; break; case "colour": a9[a6] = an.getRGB(a9[a6]); var a8 = an.getRGB(e[a6]); a2[a6] = { r: (a8.r - a9[a6].r) / a5, g: (a8.g - a9[a6].g) / a5, b: (a8.b - a9[a6].b) / a5 }; break; case "path": var S = H(a9[a6], e[a6]); a9[a6] = S[0]; var a3 = S[1]; a2[a6] = []; for (var bb = 0, a1 = a9[a6][m]; bb < a1; bb++) { a2[a6][bb] = [0]; for (var ba = 1, bd = a9[a6][bb][m]; ba < bd; ba++) { a2[a6][bb][ba] = (a3[bb][ba] - a9[a6][bb][ba]) / a5; } } break; case "csv": var d = (be[a6] + at)[z](a), a0 = (a9[a6] + at)[z](a); switch (a6) { case "translation": a9[a6] = [0, 0]; a2[a6] = [d[0] / a5, d[1] / a5]; break; case "rotation": a9[a6] = (a0[1] == d[1] && a0[2] == d[2]) ? a0 : [0, d[1], d[2]]; a2[a6] = [(d[0] - a9[a6][0]) / a5, 0, 0]; break; case "scale": be[a6] = d; a9[a6] = (a9[a6] + at)[z](a); a2[a6] = [(d[0] - a9[a6][0]) / a5, (d[1] - a9[a6][1]) / a5, 0, 0]; break; case "clip-rect": a9[a6] = (a9[a6] + at)[z](a); a2[a6] = []; var bb = 4; while (bb--) { a2[a6][bb] = (d[bb] - a9[a6][bb]) / a5; } break; } e[a6] = d; } } } } this.stop(); this.in_animation = 1; I[this.id] = { start: be.start || +new Date, ms: a5, easing: a4, from: a9, diff: a2, to: e, el: this, callback: E, t: { x: 0, y: 0 } }; ++I[m] == 1 && aR(); return this; }; ax[aY].stop = function () { I[this.id] && I[m]--; delete I[this.id]; return this; }; ax[aY].translate = function (d, e) { return this.attr({ translation: d + " " + e }); }; ax[aY][aA] = function () { return "Rapha\xebl\u2019s object"; }; an.ae = I; var T = function (d) { this.items = []; this[m] = 0; if (d) { for (var e = 0, E = d[m]; e < E; e++) { if (d[e] && (d[e].constructor == ax || d[e].constructor == T)) { this[this.items[m]] = this.items[this.items[m]] = d[e]; this[m]++; } } } }; T[aY][f] = function () { var R, d; for (var e = 0, E = arguments[m]; e < E; e++) { R = arguments[e]; if (R && (R.constructor == ax || R.constructor == T)) { d = this.items[m]; this[d] = this.items[d] = R; this[m]++; } } return this; }; T[aY].pop = function () { delete this[this[m]--]; return this.items.pop(); }; for (var y in ax[aY]) { if (ax[aY][Q](y)) { T[aY][y] = (function (d) { return function () { for (var e = 0, E = this.items[m]; e < E; e++) { this.items[e][d][aW](this.items[e], arguments); } return this; }; })(y); } } T[aY].attr = function (e, a0) { if (e && an.is(e, "array") && an.is(e[0], "object")) { for (var d = 0, S = e[m]; d < S; d++) { this.items[d].attr(e[d]); } } else { for (var E = 0, R = this.items[m]; E < R; E++) { this.items[E].attr[aW](this.items[E], arguments); } } return this; }; T[aY].animate = function (S, e, a2, a1) { (an.is(a2, "function") || !a2) && (a1 = a2 || null); var d = this.items[m], E = d, a0 = this, R; a1 && (R = function () { !--d && a1.call(a0); }); this.items[--E].animate(S, e, a2 || R, R); while (E--) { this.items[E].animateWith(this.items[d - 1], S, e, a2 || R, R); } return this; }; T[aY].insertAfter = function (e) { var d = this.items[m]; while (d--) { this.items[d].insertAfter(e); } return this; }; T[aY].getBBox = function () { var d = [], a0 = [], e = [], R = []; for (var E = this.items[m]; E--;) { var S = this.items[E].getBBox(); d[f](S.x); a0[f](S.y); e[f](S.x + S.width); R[f](S.y + S.height); } d = aI[aW](0, d); a0 = aI[aW](0, a0); return { x: d, y: a0, width: g[aW](0, e) - d, height: g[aW](0, R) - a0 }; }; an.registerFont = function (e) { if (!e.face) { return e; } this.fonts = this.fonts || {}; var E = { w: e.w, face: {}, glyphs: {} }, i = e.face["font-family"]; for (var a0 in e.face) { if (e.face[Q](a0)) { E.face[a0] = e.face[a0]; } } if (this.fonts[i]) { this.fonts[i][f](E); } else { this.fonts[i] = [E]; } if (!e.svg) { E.face["units-per-em"] = G(e.face["units-per-em"], 10); for (var R in e.glyphs) { if (e.glyphs[Q](R)) { var S = e.glyphs[R]; E.glyphs[R] = { w: S.w, k: {}, d: S.d && "M" + S.d[aP](/[mlcxtrv]/g, function (a1) { return { l: "L", c: "C", x: "z", t: "m", r: "l", v: "c" }[a1] || "M"; }) + "z" }; if (S.k) { for (var d in S.k) { if (S[Q](d)) { E.glyphs[R].k[d] = S.k[d]; } } } } } } return e; }; aT[aY].getFont = function (a2, a3, e, R) { R = R || "normal"; e = e || "normal"; a3 = +a3 || { normal: 400, bold: 700, lighter: 300, bolder: 800 }[a3] || 400; var S = an.fonts[a2]; if (!S) { var E = new RegExp("(^|\\s)" + a2[aP](/[^\w\d\s+!~.:_-]/g, at) + "(\\s|$)", "i"); for (var d in an.fonts) { if (an.fonts[Q](d)) { if (E.test(d)) { S = an.fonts[d]; break; } } } } var a0; if (S) { for (var a1 = 0, a4 = S[m]; a1 < a4; a1++) { a0 = S[a1]; if (a0.face["font-weight"] == a3 && (a0.face["font-style"] == e || !a0.face["font-style"]) && a0.face["font-stretch"] == R) { break; } } } return a0; }; aT[aY].print = function (R, E, d, a1, a2, bb) { bb = bb || "middle"; var a7 = this.set(), ba = (d + at)[z](at), a8 = 0, a4 = at, bc; an.is(a1, "string") && (a1 = this.getFont(a1)); if (a1) { bc = (a2 || 16) / a1.face["units-per-em"]; var e = a1.face.bbox.split(a), a0 = +e[0], a3 = +e[1] + (bb == "baseline" ? e[3] - e[1] + (+a1.face.descent) : (e[3] - e[1]) / 2); for (var a6 = 0, S = ba[m]; a6 < S; a6++) { var a5 = a6 && a1.glyphs[ba[a6 - 1]] || {}, a9 = a1.glyphs[ba[a6]]; a8 += a6 ? (a5.w || a1.w) + (a5.k && a5.k[ba[a6]] || 0) : 0; a9 && a9.d && a7[f](this.path(a9.d).attr({ fill: "#000", stroke: "none", translation: [a8, 0] })); } a7.scale(bc, bc, a0, a3).translate(R - a0, E - a3); } return a7; }; an.format = function (i) { var e = an.is(arguments[1], "array") ? [0][aS](arguments[1]) : arguments, d = /\{(\d+)\}/g; i && an.is(i, "string") && e[m] - 1 && (i = i[aP](d, function (R, E) { return e[++E] == null ? at : e[E]; })); return i || at; }; an.ninja = function () { var d = Raphael; if (l.was) { Raphael = l.is; } else { delete Raphael; } return d; }; an.el = ax[aY]; return an; })();

/**
 * Originally grabbed from the official RaphaelJS Documentation
 * http://raphaeljs.com/graffle.html
 * Adopted (arrows) and commented by Philipp Strathausen http://blog.ameisenbar.de
 * Licenced under the MIT licence.
 */

/**
 * Usage:
 * connect two shapes
 * parameters: 
 *      source shape [or connection for redrawing], 
 *      target shape,
 *      style with { fg : linecolor, bg : background color, directed: boolean }
 * returns:
 *      connection { draw = function() }
 */
Raphael.fn.connection = function (obj1, obj2, style) {
    var selfRef = this;
    /* create and return new connection */
    var edge = {/*
        from : obj1,
        to : obj2,
        style : style,*/
        draw: function () {
            /* get bounding boxes of target and source */
            var bb1 = obj1.getBBox();
            var bb2 = obj2.getBBox();
            var off1 = 0;
            var off2 = 0;
            /* coordinates for potential connection coordinates from/to the objects */
            var p = [
                { x: bb1.x + bb1.width / 2, y: bb1.y - off1 },              /* NORTH 1 */
                { x: bb1.x + bb1.width / 2, y: bb1.y + bb1.height + off1 }, /* SOUTH 1 */
                { x: bb1.x - off1, y: bb1.y + bb1.height / 2 },             /* WEST  1 */
                { x: bb1.x + bb1.width + off1, y: bb1.y + bb1.height / 2 }, /* EAST  1 */
                { x: bb2.x + bb2.width / 2, y: bb2.y - off2 },              /* NORTH 2 */
                { x: bb2.x + bb2.width / 2, y: bb2.y + bb2.height + off2 }, /* SOUTH 2 */
                { x: bb2.x - off2, y: bb2.y + bb2.height / 2 },             /* WEST  2 */
                { x: bb2.x + bb2.width + off2, y: bb2.y + bb2.height / 2 }  /* EAST  2 */
            ];

            /* distances between objects and according coordinates connection */
            var d = {}, dis = [];

            /*
             * find out the best connection coordinates by trying all possible ways
             */
            /* loop the first object's connection coordinates */
            for (var i = 0; i < 4; i++) {
                /* loop the seond object's connection coordinates */
                for (var j = 4; j < 8; j++) {
                    var dx = Math.abs(p[i].x - p[j].x),
                        dy = Math.abs(p[i].y - p[j].y);
                    if ((i == j - 4) || (((i != 3 && j != 6) || p[i].x < p[j].x) && ((i != 2 && j != 7) || p[i].x > p[j].x) && ((i != 0 && j != 5) || p[i].y > p[j].y) && ((i != 1 && j != 4) || p[i].y < p[j].y))) {
                        dis.push(dx + dy);
                        d[dis[dis.length - 1].toFixed(3)] = [i, j];
                    }
                }
            }
            if (style && style.hasReturn) {
                dis.push(0);
                if (bb1.y < bb2.y)
                    d[dis[dis.length - 1].toFixed(3)] = [0, 4];
                else
                    d[dis[dis.length - 1].toFixed(3)] = [1, 5];
            }
            var res = dis.length == 0 ? [0, 4] : d[Math.min.apply(Math, dis).toFixed(3)];
            /* bezier path */
            var x1 = p[res[0]].x,
                y1 = p[res[0]].y,
                x4 = p[res[1]].x,
                y4 = p[res[1]].y,
                dx = Math.max(Math.abs(x1 - x4) / 2, 10),
                dy = Math.max(Math.abs(y1 - y4) / 2, 10),
                x2 = [x1, x1, x1 - dx, x1 + dx][res[0]].toFixed(3),
                y2 = [y1 - dy, y1 + dy, y1, y1][res[0]].toFixed(3),
                x3 = [0, 0, 0, 0, x4, x4, x4 - dx, x4 + dx][res[1]].toFixed(3),
                y3 = [0, 0, 0, 0, y1 + dy, y1 - dy, y4, y4][res[1]].toFixed(3);
            /* assemble path and arrow */
            var path = ["M", x1.toFixed(3), y1.toFixed(3), "C", x2, y2, x3, y3, x4.toFixed(3), y4.toFixed(3)].join(",");
            /* arrow */
            if (style && style.directed) {
                /* magnitude, length of the last path vector */
                var mag = Math.sqrt((y4 - y3) * (y4 - y3) + (x4 - x3) * (x4 - x3));
                /* vector normalisation to specified length  */
                var norm = function (x, l) { return (-x * (l || 5) / mag); };
                /* calculate array coordinates (two lines orthogonal to the path vector) */
                var arr = [
                    { x: (norm(x4 - x3) + norm(y4 - y3) + x4).toFixed(3), y: (norm(y4 - y3) + norm(x4 - x3) + y4).toFixed(3) },
                    { x: (norm(x4 - x3) - norm(y4 - y3) + x4).toFixed(3), y: (norm(y4 - y3) - norm(x4 - x3) + y4).toFixed(3) }
                ];
                path = path + ",M" + arr[0].x + "," + arr[0].y + ",L" + x4 + "," + y4 + ",L" + arr[1].x + "," + arr[1].y;
            }
            /* function to be used for moving existent path(s), e.g. animate() or attr() */
            var move = "attr";
            /* applying path(s) */
            edge.fg && edge.fg[move]({ path: path })
                || (edge.fg = selfRef.path(path).attr({ stroke: style && style.stroke || "#000", fill: "none" }).toBack());
            edge.bg && edge.bg[move]({ path: path })
                || style && style.fill && (edge.bg = style.fill.split && selfRef.path(path).attr({ stroke: style.fill.split("|")[0], fill: "none", "stroke-width": style.fill.split("|")[1] || 3 }).toBack());
            /* setting label */
            style && style.label
                && (edge.label && edge.label.attr({ x: (x1 + x4) / 2, y: (y1 + y4) / 2 })
                    || (edge.label = selfRef.text((x1 + x4) / 2, (y1 + y4) / 2, style.label).attr({ fill: "#000", "font-size": style["font-size"] || "12px" })));
            style && style.label && style["label-style"] && edge.label && edge.label.attr(style["label-style"]);
            style && style.callback && style.callback(edge);
        }
    }
    edge.draw();
    return edge;
};
//Raphael.prototype.set.prototype.dodo=function(){console.log("works");};

/*
 *  Dracula Graph Layout and Drawing Framework 0.0.3alpha
 *  (c) 2010 Philipp Strathausen <strathausen@gmail.com>, http://strathausen.eu
 *  Contributions by Jake Stothard <stothardj@gmail.com>.
 *
 *  based on the Graph JavaScript framework, version 0.0.1
 *  (c) 2006 Aslak Hellesoy <aslak.hellesoy@gmail.com>
 *  (c) 2006 Dave Hoover <dave.hoover@gmail.com>
 *
 *  Ported from Graph::Layouter::Spring in
 *    http://search.cpan.org/~pasky/Graph-Layderer-0.02/
 *  The algorithm is based on a spring-style layouter of a Java-based social
 *  network tracker PieSpy written by Paul Mutton <paul@jibble.org>.
 *
 *  This code is freely distributable under the MIT license. Commercial use is
 *  hereby granted without any cost or restriction.
 *
 *  Links:
 *
 *  Graph Dracula JavaScript Framework:
 *      http://graphdracula.net
 *
 /*--------------------------------------------------------------------------*/

/*
 * Edge Factory
 */
var AbstractEdge = function () {
}
AbstractEdge.prototype = {
    hide: function () {
        this.connection.fg.hide();
        this.connection.bg && this.bg.connection.hide();
    }
};
var EdgeFactory = function () {
    this.template = new AbstractEdge();
    this.template.style = new Object();
    this.template.style.directed = false;
    this.template.style.hasReturn = false;
    this.template.weight = 1;
};
EdgeFactory.prototype = {
    build: function (source, target) {
        var e = jQuery.extend(true, {}, this.template);
        e.source = source;
        e.target = target;
        return e;
    }
};

/*
 * Graph
 */
var Graph = function () {
    this.nodes = {};
    this.edges = [];
    this.snapshots = []; // previous graph states TODO to be implemented
    this.edgeFactory = new EdgeFactory();
};
Graph.prototype = {
    /* 
     * add a node
     * @id          the node's ID (string or number)
     * @content     (optional, dictionary) can contain any information that is
     *              being interpreted by the layout algorithm or the graph
     *              representation
     */
    addNode: function (id, content) {
        /* testing if node is already existing in the graph */
        if (this.nodes[id] == undefined) {
            this.nodes[id] = new Graph.Node(id, content);
        }
        return this.nodes[id];
    },

    addEdge: function (source, target, style) {
        var s = this.addNode(source);
        var t = this.addNode(target);
        var edge = this.edgeFactory.build(s, t);
        jQuery.extend(edge.style, style);
        s.edges.push(edge);
        this.edges.push(edge);
        // NOTE: Even directed edges are added to both nodes.
        t.edges.push(edge);
    },

    /* TODO to be implemented
     * Preserve a copy of the graph state (nodes, positions, ...)
     * @comment     a comment describing the state
     */
    snapShot: function (comment) {
        /* FIXME
        var graph = new Graph();
        graph.nodes = jQuery.extend(true, {}, this.nodes);
        graph.edges = jQuery.extend(true, {}, this.edges);
        this.snapshots.push({comment: comment, graph: graph});
        */
    },
    removeNode: function (id) {
        delete this.nodes[id];
        for (var i = 0; i < this.edges.length; i++) {
            if (this.edges[i].source.id == id || this.edges[i].target.id == id) {
                this.edges.splice(i, 1);
                i--;
            }
        }
    }
};

/*
 * Node
 */
Graph.Node = function (id, node) {
    node = node || {};
    node.id = id;
    node.edges = [];
    node.hide = function () {
        this.hidden = true;
        this.shape && this.shape.hide(); /* FIXME this is representation specific code and should be elsewhere */
        for (i in this.edges)
            (this.edges[i].source.id == id || this.edges[i].target == id) && this.edges[i].hide && this.edges[i].hide();
    };
    node.show = function () {
        this.hidden = false;
        this.shape && this.shape.show();
        for (i in this.edges)
            (this.edges[i].source.id == id || this.edges[i].target == id) && this.edges[i].show && this.edges[i].show();
    };
    return node;
};
Graph.Node.prototype = {
};

/*
 * Renderer base class
 */
Graph.Renderer = {};

/*
 * Renderer implementation using RaphaelJS
 */
Graph.Renderer.Raphael = function (element, graph, width, height) {
    this.width = width || 400;
    this.height = height || 400;
    var selfRef = this;
    this.r = Raphael(element, this.width, this.height);
    this.radius = 40; /* max dimension of a node */
    this.graph = graph;
    this.mouse_in = false;

    /* TODO default node rendering function */
    if (!this.graph.render) {
        this.graph.render = function () {
            return;
        }
    }

    /*
     * Dragging
     */
    this.isDrag = false;
    this.dragger = function (e) {
        this.dx = e.clientX;
        this.dy = e.clientY;
        selfRef.isDrag = this;
        this.set && this.set.animate({ "fill-opacity": .1 }, 200) && this.set.toFront();
        e.preventDefault && e.preventDefault();
    };

    var d = document.getElementById(element);
    d.onmousemove = function (e) {
        e = e || window.event;
        if (selfRef.isDrag) {
            var bBox = selfRef.isDrag.set.getBBox();
            // TODO round the coordinates here (eg. for proper image representation)
            var newX = e.clientX - selfRef.isDrag.dx + (bBox.x + bBox.width / 2);
            var newY = e.clientY - selfRef.isDrag.dy + (bBox.y + bBox.height / 2);
            /* prevent shapes from being dragged out of the canvas */
            var clientX = e.clientX - (newX < 20 ? newX - 20 : newX > selfRef.width - 20 ? newX - selfRef.width + 20 : 0);
            var clientY = e.clientY - (newY < 20 ? newY - 20 : newY > selfRef.height - 20 ? newY - selfRef.height + 20 : 0);
            selfRef.isDrag.set.translate(clientX - Math.round(selfRef.isDrag.dx), clientY - Math.round(selfRef.isDrag.dy));
            //            console.log(clientX - Math.round(selfRef.isDrag.dx), clientY - Math.round(selfRef.isDrag.dy));
            for (var i in selfRef.graph.edges) {
                selfRef.graph.edges[i].connection && selfRef.graph.edges[i].connection.draw();
            }
            //selfRef.r.safari();
            selfRef.isDrag.dx = clientX;
            selfRef.isDrag.dy = clientY;
        }
    };
    d.onmouseup = function () {
        selfRef.isDrag && selfRef.isDrag.set.animate({ "fill-opacity": .6 }, 500);
        selfRef.isDrag = false;
    };
    this.draw();
};
Graph.Renderer.Raphael.prototype = {
    translate: function (point) {
        return [
            (point[0] - this.graph.layoutMinX) * this.factorX + this.radius,
            (point[1] - this.graph.layoutMinY) * this.factorY + this.radius
        ];
    },

    rotate: function (point, length, angle) {
        var dx = length * Math.cos(angle);
        var dy = length * Math.sin(angle);
        return [point[0] + dx, point[1] + dy];
    },

    draw: function () {
        this.factorX = (this.width - 2 * this.radius) / (this.graph.layoutMaxX - this.graph.layoutMinX);
        this.factorY = (this.height - 2 * this.radius) / (this.graph.layoutMaxY - this.graph.layoutMinY);
        for (i in this.graph.nodes) {
            this.drawNode(this.graph.nodes[i]);
        }
        for (var i = 0; i < this.graph.edges.length; i++) {
            this.drawEdge(this.graph.edges[i]);
        }
    },

    drawNode: function (node) {
        var point = this.translate([node.layoutPosX, node.layoutPosY]);
        node.point = point;

        /* if node has already been drawn, move the nodes */
        if (node.shape) {
            var oBBox = node.shape.getBBox();
            var opoint = { x: oBBox.x + oBBox.width / 2, y: oBBox.y + oBBox.height / 2 };
            node.shape.translate(Math.round(point[0] - opoint.x), Math.round(point[1] - opoint.y));
            this.r.safari();
            return node;
        }/* else, draw new nodes */

        var shape;

        /* if a node renderer function is provided by the user, then use it 
           or the default render function instead */
        if (!node.render) {
            node.render = function (r, node) {
                /* the default node drawing */
                var color = Raphael.getColor();
                var ellipse = r.ellipse(0, 0, 30, 20).attr({ fill: color, stroke: color, "stroke-width": 2 });
                /* set DOM node ID */
                ellipse.node.id = node.label || node.id;
                shape = r.set().
                    push(ellipse).
                    push(r.text(0, 30, node.label || node.id));
                return shape;
            }
        }
        /* or check for an ajax representation of the nodes */
        if (node.shapes) {
            // TODO ajax representation evaluation
        }

        shape = node.render(this.r, node).hide();

        shape.attr({ "fill-opacity": .6 });
        /* re-reference to the node an element belongs to, needed for dragging all elements of a node */
        shape.items.forEach(function (item) { item.set = shape; item.node.style.cursor = "move"; });
        shape.mousedown(this.dragger);

        var box = shape.getBBox();
        shape.translate(Math.round(point[0] - (box.x + box.width / 2)), Math.round(point[1] - (box.y + box.height / 2)))
        //console.log(box,point);
        node.hidden || shape.show();
        node.shape = shape;
    },
    drawEdge: function (edge) {
        /* if this edge already exists the other way around and is undirected */
        if (edge.backedge)
            return;
        if (edge.source.hidden || edge.target.hidden) {
            edge.connection && edge.connection.fg.hide() | edge.connection.bg && edge.connection.bg.hide();
            return;
        }
        /* if edge already has been drawn, only refresh the edge */
        if (!edge.connection) {
            edge.style && edge.style.callback && edge.style.callback(edge); // TODO move this somewhere else
            edge.connection = this.r.connection(edge.source.shape, edge.target.shape, edge.style);
            return;
        }
        //FIXME showing doesn't work well
        edge.connection.fg.show();
        edge.connection.bg && edge.connection.bg.show();
        edge.connection.draw();
    }
};
Graph.Layout = {};
Graph.Layout.Spring = function (graph) {
    this.graph = graph;
    this.iterations = 500;
    this.maxRepulsiveForceDistance = 6;
    this.k = 2;
    this.c = 0.01;
    this.maxVertexMovement = 0.5;
    this.layout();
};
Graph.Layout.Spring.prototype = {
    layout: function () {
        this.layoutPrepare();
        for (var i = 0; i < this.iterations; i++) {
            this.layoutIteration();
        }
        this.layoutCalcBounds();
    },

    layoutPrepare: function () {
        for (i in this.graph.nodes) {
            var node = this.graph.nodes[i];
            node.layoutPosX = 0;
            node.layoutPosY = 0;
            node.layoutForceX = 0;
            node.layoutForceY = 0;
        }

    },

    layoutCalcBounds: function () {
        var minx = Infinity, maxx = -Infinity, miny = Infinity, maxy = -Infinity;

        for (i in this.graph.nodes) {
            var x = this.graph.nodes[i].layoutPosX;
            var y = this.graph.nodes[i].layoutPosY;

            if (x > maxx) maxx = x;
            if (x < minx) minx = x;
            if (y > maxy) maxy = y;
            if (y < miny) miny = y;
        }

        this.graph.layoutMinX = minx;
        this.graph.layoutMaxX = maxx;
        this.graph.layoutMinY = miny;
        this.graph.layoutMaxY = maxy;
    },

    layoutIteration: function () {
        // Forces on nodes due to node-node repulsions

        var prev = new Array();
        for (var c in this.graph.nodes) {
            var node1 = this.graph.nodes[c];
            for (var d in prev) {
                var node2 = this.graph.nodes[prev[d]];
                this.layoutRepulsive(node1, node2);

            }
            prev.push(c);
        }

        // Forces on nodes due to edge attractions
        for (var i = 0; i < this.graph.edges.length; i++) {
            var edge = this.graph.edges[i];
            this.layoutAttractive(edge);
        }

        // Move by the given force
        for (i in this.graph.nodes) {
            var node = this.graph.nodes[i];
            var xmove = this.c * node.layoutForceX;
            var ymove = this.c * node.layoutForceY;

            var max = this.maxVertexMovement;
            if (xmove > max) xmove = max;
            if (xmove < -max) xmove = -max;
            if (ymove > max) ymove = max;
            if (ymove < -max) ymove = -max;

            node.layoutPosX += xmove;
            node.layoutPosY += ymove;
            node.layoutForceX = 0;
            node.layoutForceY = 0;
        }
    },

    layoutRepulsive: function (node1, node2) {
        if (typeof node1 == 'undefined' || typeof node2 == 'undefined')
            return;
        var dx = node2.layoutPosX - node1.layoutPosX;
        var dy = node2.layoutPosY - node1.layoutPosY;
        var d2 = dx * dx + dy * dy;
        if (d2 < 0.01) {
            dx = 0.1 * Math.random() + 0.1;
            dy = 0.1 * Math.random() + 0.1;
            var d2 = dx * dx + dy * dy;
        }
        var d = Math.sqrt(d2);
        if (d < this.maxRepulsiveForceDistance) {
            var repulsiveForce = this.k * this.k / d;
            node2.layoutForceX += repulsiveForce * dx / d;
            node2.layoutForceY += repulsiveForce * dy / d;
            node1.layoutForceX -= repulsiveForce * dx / d;
            node1.layoutForceY -= repulsiveForce * dy / d;
        }
    },

    layoutAttractive: function (edge) {
        var node1 = edge.source;
        var node2 = edge.target;

        var dx = node2.layoutPosX - node1.layoutPosX;
        var dy = node2.layoutPosY - node1.layoutPosY;
        var d2 = dx * dx + dy * dy;
        if (d2 < 0.01) {
            dx = 0.1 * Math.random() + 0.1;
            dy = 0.1 * Math.random() + 0.1;
            var d2 = dx * dx + dy * dy;
        }
        var d = Math.sqrt(d2);
        if (d > this.maxRepulsiveForceDistance) {
            d = this.maxRepulsiveForceDistance;
            d2 = d * d;
        }
        var attractiveForce = (d2 - this.k * this.k) / this.k;
        if (edge.attraction == undefined) edge.attraction = 1;
        attractiveForce *= Math.log(edge.attraction) * 0.5 + 1;

        node2.layoutForceX -= attractiveForce * dx / d;
        node2.layoutForceY -= attractiveForce * dy / d;
        node1.layoutForceX += attractiveForce * dx / d;
        node1.layoutForceY += attractiveForce * dy / d;
    }
};

Graph.Layout.Ordered = function (graph, order) {
    this.graph = graph;
    this.order = order;
    this.layout();
};
Graph.Layout.Ordered.prototype = {
    layout: function () {
        this.layoutPrepare();
        this.layoutCalcBounds();
    },

    layoutPrepare: function (order) {
        for (i in this.graph.nodes) {
            var node = this.graph.nodes[i];
            node.layoutPosX = 0;
            node.layoutPosY = 0;
        }
        var counter = 0;
        for (i in this.order) {
            var node = this.order[i];
            node.layoutPosX = counter;
            node.layoutPosY = Math.random();
            counter++;
        }
    },

    layoutCalcBounds: function () {
        var minx = Infinity, maxx = -Infinity, miny = Infinity, maxy = -Infinity;

        for (i in this.graph.nodes) {
            var x = this.graph.nodes[i].layoutPosX;
            var y = this.graph.nodes[i].layoutPosY;

            if (x > maxx) maxx = x;
            if (x < minx) minx = x;
            if (y > maxy) maxy = y;
            if (y < miny) miny = y;
        }

        this.graph.layoutMinX = minx;
        this.graph.layoutMaxX = maxx;

        this.graph.layoutMinY = miny;
        this.graph.layoutMaxY = maxy;
    }
};

/*
 * usefull JavaScript extensions, 
 */

function log(a) { console.log && console.log(a); }

/*
 * Raphael Tooltip Plugin
 * - attaches an element as a tooltip to another element
 *
 * Usage example, adding a rectangle as a tooltip to a circle:
 *
 *      paper.circle(100,100,10).tooltip(paper.rect(0,0,20,30));
 *
 * If you want to use more shapes, you'll have to put them into a set.
 *
 */
Raphael.el.tooltip = function (tp) {
    this.tp = tp;
    this.tp.o = { x: 0, y: 0 };
    this.tp.hide();
    this.hover(
        function (event) {
            this.mousemove(function (event) {
                this.tp.translate(event.clientX -
                                  this.tp.o.x, event.clientY - this.tp.o.y);
                this.tp.o = { x: event.clientX, y: event.clientY };
            });
            this.tp.show().toFront();
        },
        function (event) {
            this.tp.hide();
            this.unmousemove();
        });
    return this;
};

/* For IE */
if (!Array.prototype.forEach) {
    Array.prototype.forEach = function (fun /*, thisp*/) {
        var len = this.length;
        if (typeof fun != "function")
            throw new TypeError();

        var thisp = arguments[1];
        for (var i = 0; i < len; i++) {
            if (i in this)
                fun.call(thisp, this[i], i, this);
        }
    };
}


/*!
 * ASP.NET SignalR JavaScript Library v2.2.1
 * http://signalr.net/
 *
 * Copyright (c) .NET Foundation. All rights reserved.
 * Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
 *
 */
(function (n, t, i) { function w(t, i) { var u, f; if (n.isArray(t)) { for (u = t.length - 1; u >= 0; u--) f = t[u], n.type(f) === "string" && r.transports[f] || (i.log("Invalid transport: " + f + ", removing it from the transports list."), t.splice(u, 1)); t.length === 0 && (i.log("No transports remain within the specified transport array."), t = null) } else if (r.transports[t] || t === "auto") { if (t === "auto" && r._.ieVersion <= 8) return ["longPolling"] } else i.log("Invalid transport: " + t.toString() + "."), t = null; return t } function b(n) { return n === "http:" ? 80 : n === "https:" ? 443 : void 0 } function a(n, t) { return t.match(/:\d+$/) ? t : t + ":" + b(n) } function k(t, i) { var u = this, r = []; u.tryBuffer = function (i) { return t.state === n.signalR.connectionState.connecting ? (r.push(i), !0) : !1 }; u.drain = function () { if (t.state === n.signalR.connectionState.connected) while (r.length > 0) i(r.shift()) }; u.clear = function () { r = [] } } var f = { nojQuery: "jQuery was not found. Please ensure jQuery is referenced before the SignalR client JavaScript file.", noTransportOnInit: "No transport could be initialized successfully. Try specifying a different transport or none at all for auto initialization.", errorOnNegotiate: "Error during negotiation request.", stoppedWhileLoading: "The connection was stopped during page load.", stoppedWhileNegotiating: "The connection was stopped during the negotiate request.", errorParsingNegotiateResponse: "Error parsing negotiate response.", errorDuringStartRequest: "Error during start request. Stopping the connection.", stoppedDuringStartRequest: "The connection was stopped during the start request.", errorParsingStartResponse: "Error parsing start response: '{0}'. Stopping the connection.", invalidStartResponse: "Invalid start response: '{0}'. Stopping the connection.", protocolIncompatible: "You are using a version of the client that isn't compatible with the server. Client version {0}, server version {1}.", sendFailed: "Send failed.", parseFailed: "Failed at parsing response: {0}", longPollFailed: "Long polling request failed.", eventSourceFailedToConnect: "EventSource failed to connect.", eventSourceError: "Error raised by EventSource", webSocketClosed: "WebSocket closed.", pingServerFailedInvalidResponse: "Invalid ping response when pinging server: '{0}'.", pingServerFailed: "Failed to ping server.", pingServerFailedStatusCode: "Failed to ping server.  Server responded with status code {0}, stopping the connection.", pingServerFailedParse: "Failed to parse ping server response, stopping the connection.", noConnectionTransport: "Connection is in an invalid state, there is no transport active.", webSocketsInvalidState: "The Web Socket transport is in an invalid state, transitioning into reconnecting.", reconnectTimeout: "Couldn't reconnect within the configured timeout of {0} ms, disconnecting.", reconnectWindowTimeout: "The client has been inactive since {0} and it has exceeded the inactivity timeout of {1} ms. Stopping the connection." }; if (typeof n != "function") throw new Error(f.nojQuery); var r, h, o = t.document.readyState === "complete", e = n(t), c = "__Negotiate Aborted__", u = { onStart: "onStart", onStarting: "onStarting", onReceived: "onReceived", onError: "onError", onConnectionSlow: "onConnectionSlow", onReconnecting: "onReconnecting", onReconnect: "onReconnect", onStateChanged: "onStateChanged", onDisconnect: "onDisconnect" }, v = function (n, i) { if (i !== !1) { var r; typeof t.console != "undefined" && (r = "[" + (new Date).toTimeString() + "] SignalR: " + n, t.console.debug ? t.console.debug(r) : t.console.log && t.console.log(r)) } }, s = function (t, i, r) { return i === t.state ? (t.state = r, n(t).triggerHandler(u.onStateChanged, [{ oldState: i, newState: r }]), !0) : !1 }, y = function (n) { return n.state === r.connectionState.disconnected }, l = function (n) { return n._.keepAliveData.activated && n.transport.supportsKeepAlive(n) }, p = function (i) { var f, e; i._.configuredStopReconnectingTimeout || (e = function (t) { var i = r._.format(r.resources.reconnectTimeout, t.disconnectTimeout); t.log(i); n(t).triggerHandler(u.onError, [r._.error(i, "TimeoutException")]); t.stop(!1, !1) }, i.reconnecting(function () { var n = this; n.state === r.connectionState.reconnecting && (f = t.setTimeout(function () { e(n) }, n.disconnectTimeout)) }), i.stateChanged(function (n) { n.oldState === r.connectionState.reconnecting && t.clearTimeout(f) }), i._.configuredStopReconnectingTimeout = !0) }; if (r = function (n, t, i) { return new r.fn.init(n, t, i) }, r._ = { defaultContentType: "application/x-www-form-urlencoded; charset=UTF-8", ieVersion: function () { var i, n; return t.navigator.appName === "Microsoft Internet Explorer" && (n = /MSIE ([0-9]+\.[0-9]+)/.exec(t.navigator.userAgent), n && (i = t.parseFloat(n[1]))), i }(), error: function (n, t, i) { var r = new Error(n); return r.source = t, typeof i != "undefined" && (r.context = i), r }, transportError: function (n, t, r, u) { var f = this.error(n, r, u); return f.transport = t ? t.name : i, f }, format: function () { for (var t = arguments[0], n = 0; n < arguments.length - 1; n++) t = t.replace("{" + n + "}", arguments[n + 1]); return t }, firefoxMajorVersion: function (n) { var t = n.match(/Firefox\/(\d+)/); return !t || !t.length || t.length < 2 ? 0 : parseInt(t[1], 10) }, configurePingInterval: function (i) { var f = i._.config, e = function (t) { n(i).triggerHandler(u.onError, [t]) }; f && !i._.pingIntervalId && f.pingInterval && (i._.pingIntervalId = t.setInterval(function () { r.transports._logic.pingServer(i).fail(e) }, f.pingInterval)) } }, r.events = u, r.resources = f, r.ajaxDefaults = { processData: !0, timeout: null, async: !0, global: !1, cache: !1 }, r.changeState = s, r.isDisconnecting = y, r.connectionState = { connecting: 0, connected: 1, reconnecting: 2, disconnected: 4 }, r.hub = { start: function () { throw new Error("SignalR: Error loading hubs. Ensure your hubs reference is correct, e.g. <script src='/signalr/js'><\/script>."); } }, typeof e.on == "function") e.on("load", function () { o = !0 }); else e.load(function () { o = !0 }); r.fn = r.prototype = { init: function (t, i, r) { var f = n(this); this.url = t; this.qs = i; this.lastError = null; this._ = { keepAliveData: {}, connectingMessageBuffer: new k(this, function (n) { f.triggerHandler(u.onReceived, [n]) }), lastMessageAt: (new Date).getTime(), lastActiveAt: (new Date).getTime(), beatInterval: 5e3, beatHandle: null, totalTransportConnectTimeout: 0 }; typeof r == "boolean" && (this.logging = r) }, _parseResponse: function (n) { var t = this; return n ? typeof n == "string" ? t.json.parse(n) : n : n }, _originalJson: t.JSON, json: t.JSON, isCrossDomain: function (i, r) { var u; return (i = n.trim(i), r = r || t.location, i.indexOf("http") !== 0) ? !1 : (u = t.document.createElement("a"), u.href = i, u.protocol + a(u.protocol, u.host) !== r.protocol + a(r.protocol, r.host)) }, ajaxDataType: "text", contentType: "application/json; charset=UTF-8", logging: !1, state: r.connectionState.disconnected, clientProtocol: "1.5", reconnectDelay: 2e3, transportConnectTimeout: 0, disconnectTimeout: 3e4, reconnectWindow: 3e4, keepAliveWarnAt: 2 / 3, start: function (i, h) { var a = this, v = { pingInterval: 3e5, waitForPageLoad: !0, transport: "auto", jsonp: !1 }, d, y = a._deferral || n.Deferred(), b = t.document.createElement("a"), k, g; if (a.lastError = null, a._deferral = y, !a.json) throw new Error("SignalR: No JSON parser found. Please ensure json2.js is referenced before the SignalR.js file if you need to support clients without native JSON parsing support, e.g. IE<8."); if (n.type(i) === "function" ? h = i : n.type(i) === "object" && (n.extend(v, i), n.type(v.callback) === "function" && (h = v.callback)), v.transport = w(v.transport, a), !v.transport) throw new Error("SignalR: Invalid transport(s) specified, aborting start."); return (a._.config = v, !o && v.waitForPageLoad === !0) ? (a._.deferredStartHandler = function () { a.start(i, h) }, e.bind("load", a._.deferredStartHandler), y.promise()) : a.state === r.connectionState.connecting ? y.promise() : s(a, r.connectionState.disconnected, r.connectionState.connecting) === !1 ? (y.resolve(a), y.promise()) : (p(a), b.href = a.url, b.protocol && b.protocol !== ":" ? (a.protocol = b.protocol, a.host = b.host) : (a.protocol = t.document.location.protocol, a.host = b.host || t.document.location.host), a.baseUrl = a.protocol + "//" + a.host, a.wsProtocol = a.protocol === "https:" ? "wss://" : "ws://", v.transport === "auto" && v.jsonp === !0 && (v.transport = "longPolling"), a.url.indexOf("//") === 0 && (a.url = t.location.protocol + a.url, a.log("Protocol relative URL detected, normalizing it to '" + a.url + "'.")), this.isCrossDomain(a.url) && (a.log("Auto detected cross domain url."), v.transport === "auto" && (v.transport = ["webSockets", "serverSentEvents", "longPolling"]), typeof v.withCredentials == "undefined" && (v.withCredentials = !0), v.jsonp || (v.jsonp = !n.support.cors, v.jsonp && a.log("Using jsonp because this browser doesn't support CORS.")), a.contentType = r._.defaultContentType), a.withCredentials = v.withCredentials, a.ajaxDataType = v.jsonp ? "jsonp" : "text", n(a).bind(u.onStart, function () { n.type(h) === "function" && h.call(a); y.resolve(a) }), a._.initHandler = r.transports._logic.initHandler(a), d = function (i, o) { var c = r._.error(f.noTransportOnInit); if (o = o || 0, o >= i.length) { o === 0 ? a.log("No transports supported by the server were selected.") : o === 1 ? a.log("No fallback transports were selected.") : a.log("Fallback transports exhausted."); n(a).triggerHandler(u.onError, [c]); y.reject(c); a.stop(); return } if (a.state !== r.connectionState.disconnected) { var p = i[o], h = r.transports[p], v = function () { d(i, o + 1) }; a.transport = h; try { a._.initHandler.start(h, function () { var i = r._.firefoxMajorVersion(t.navigator.userAgent) >= 11, f = !!a.withCredentials && i; a.log("The start request succeeded. Transitioning to the connected state."); l(a) && r.transports._logic.monitorKeepAlive(a); r.transports._logic.startHeartbeat(a); r._.configurePingInterval(a); s(a, r.connectionState.connecting, r.connectionState.connected) || a.log("WARNING! The connection was not in the connecting state."); a._.connectingMessageBuffer.drain(); n(a).triggerHandler(u.onStart); e.bind("unload", function () { a.log("Window unloading, stopping the connection."); a.stop(f) }); i && e.bind("beforeunload", function () { t.setTimeout(function () { a.stop(f) }, 0) }) }, v) } catch (w) { a.log(h.name + " transport threw '" + w.message + "' when attempting to start."); v() } } }, k = a.url + "/negotiate", g = function (t, i) { var e = r._.error(f.errorOnNegotiate, t, i._.negotiateRequest); n(i).triggerHandler(u.onError, e); y.reject(e); i.stop() }, n(a).triggerHandler(u.onStarting), k = r.transports._logic.prepareQueryString(a, k), a.log("Negotiating with '" + k + "'."), a._.negotiateRequest = r.transports._logic.ajax(a, { url: k, error: function (n, t) { t !== c ? g(n, a) : y.reject(r._.error(f.stoppedWhileNegotiating, null, a._.negotiateRequest)) }, success: function (t) { var i, e, h, o = [], s = []; try { i = a._parseResponse(t) } catch (c) { g(r._.error(f.errorParsingNegotiateResponse, c), a); return } if (e = a._.keepAliveData, a.appRelativeUrl = i.Url, a.id = i.ConnectionId, a.token = i.ConnectionToken, a.webSocketServerUrl = i.WebSocketServerUrl, a._.pollTimeout = i.ConnectionTimeout * 1e3 + 1e4, a.disconnectTimeout = i.DisconnectTimeout * 1e3, a._.totalTransportConnectTimeout = a.transportConnectTimeout + i.TransportConnectTimeout * 1e3, i.KeepAliveTimeout ? (e.activated = !0, e.timeout = i.KeepAliveTimeout * 1e3, e.timeoutWarning = e.timeout * a.keepAliveWarnAt, a._.beatInterval = (e.timeout - e.timeoutWarning) / 3) : e.activated = !1, a.reconnectWindow = a.disconnectTimeout + (e.timeout || 0), !i.ProtocolVersion || i.ProtocolVersion !== a.clientProtocol) { h = r._.error(r._.format(f.protocolIncompatible, a.clientProtocol, i.ProtocolVersion)); n(a).triggerHandler(u.onError, [h]); y.reject(h); return } n.each(r.transports, function (n) { if (n.indexOf("_") === 0 || n === "webSockets" && !i.TryWebSockets) return !0; s.push(n) }); n.isArray(v.transport) ? n.each(v.transport, function (t, i) { n.inArray(i, s) >= 0 && o.push(i) }) : v.transport === "auto" ? o = s : n.inArray(v.transport, s) >= 0 && o.push(v.transport); d(o) } }), y.promise()) }, starting: function (t) { var i = this; return n(i).bind(u.onStarting, function () { t.call(i) }), i }, send: function (n) { var t = this; if (t.state === r.connectionState.disconnected) throw new Error("SignalR: Connection must be started before data can be sent. Call .start() before .send()"); if (t.state === r.connectionState.connecting) throw new Error("SignalR: Connection has not been fully initialized. Use .start().done() or .start().fail() to run logic after the connection has started."); return t.transport.send(t, n), t }, received: function (t) { var i = this; return n(i).bind(u.onReceived, function (n, r) { t.call(i, r) }), i }, stateChanged: function (t) { var i = this; return n(i).bind(u.onStateChanged, function (n, r) { t.call(i, r) }), i }, error: function (t) { var i = this; return n(i).bind(u.onError, function (n, r, u) { i.lastError = r; t.call(i, r, u) }), i }, disconnected: function (t) { var i = this; return n(i).bind(u.onDisconnect, function () { t.call(i) }), i }, connectionSlow: function (t) { var i = this; return n(i).bind(u.onConnectionSlow, function () { t.call(i) }), i }, reconnecting: function (t) { var i = this; return n(i).bind(u.onReconnecting, function () { t.call(i) }), i }, reconnected: function (t) { var i = this; return n(i).bind(u.onReconnect, function () { t.call(i) }), i }, stop: function (i, h) { var a = this, v = a._deferral; if (a._.deferredStartHandler && e.unbind("load", a._.deferredStartHandler), delete a._.config, delete a._.deferredStartHandler, !o && (!a._.config || a._.config.waitForPageLoad === !0)) { a.log("Stopping connection prior to negotiate."); v && v.reject(r._.error(f.stoppedWhileLoading)); return } if (a.state !== r.connectionState.disconnected) return a.log("Stopping connection."), t.clearTimeout(a._.beatHandle), t.clearInterval(a._.pingIntervalId), a.transport && (a.transport.stop(a), h !== !1 && a.transport.abort(a, i), l(a) && r.transports._logic.stopMonitoringKeepAlive(a), a.transport = null), a._.negotiateRequest && (a._.negotiateRequest.abort(c), delete a._.negotiateRequest), a._.initHandler && a._.initHandler.stop(), delete a._deferral, delete a.messageId, delete a.groupsToken, delete a.id, delete a._.pingIntervalId, delete a._.lastMessageAt, delete a._.lastActiveAt, a._.connectingMessageBuffer.clear(), s(a, a.state, r.connectionState.disconnected), n(a).triggerHandler(u.onDisconnect), a }, log: function (n) { v(n, this.logging) } }; r.fn.init.prototype = r.fn; r.noConflict = function () { return n.connection === r && (n.connection = h), r }; n.connection && (h = n.connection); n.connection = n.signalR = r })($jQuery1_9, window), function (n, t, i) { function s(n) { n._.keepAliveData.monitoring && l(n); u.markActive(n) && (n._.beatHandle = t.setTimeout(function () { s(n) }, n._.beatInterval)) } function l(t) { var i = t._.keepAliveData, u; t.state === r.connectionState.connected && (u = (new Date).getTime() - t._.lastMessageAt, u >= i.timeout ? (t.log("Keep alive timed out.  Notifying transport that connection has been lost."), t.transport.lostConnection(t)) : u >= i.timeoutWarning ? i.userNotified || (t.log("Keep alive has been missed, connection may be dead/slow."), n(t).triggerHandler(f.onConnectionSlow), i.userNotified = !0) : i.userNotified = !1) } function e(n, t) { var i = n.url + t; return n.transport && (i += "?transport=" + n.transport.name), u.prepareQueryString(n, i) } function h(n) { this.connection = n; this.startRequested = !1; this.startCompleted = !1; this.connectionStopped = !1 } var r = n.signalR, f = n.signalR.events, c = n.signalR.changeState, o = "__Start Aborted__", u; r.transports = {}; h.prototype = { start: function (n, r, u) { var f = this, e = f.connection, o = !1; if (f.startRequested || f.connectionStopped) { e.log("WARNING! " + n.name + " transport cannot be started. Initialization ongoing or completed."); return } e.log(n.name + " transport starting."); n.start(e, function () { o || f.initReceived(n, r) }, function (t) { return o || (o = !0, f.transportFailed(n, t, u)), !f.startCompleted || f.connectionStopped }); f.transportTimeoutHandle = t.setTimeout(function () { o || (o = !0, e.log(n.name + " transport timed out when trying to connect."), f.transportFailed(n, i, u)) }, e._.totalTransportConnectTimeout) }, stop: function () { this.connectionStopped = !0; t.clearTimeout(this.transportTimeoutHandle); r.transports._logic.tryAbortStartRequest(this.connection) }, initReceived: function (n, i) { var u = this, f = u.connection; if (u.startRequested) { f.log("WARNING! The client received multiple init messages."); return } u.connectionStopped || (u.startRequested = !0, t.clearTimeout(u.transportTimeoutHandle), f.log(n.name + " transport connected. Initiating start request."), r.transports._logic.ajaxStart(f, function () { u.startCompleted = !0; i() })) }, transportFailed: function (i, u, e) { var o = this.connection, h = o._deferral, s; this.connectionStopped || (t.clearTimeout(this.transportTimeoutHandle), this.startRequested ? this.startCompleted || (s = r._.error(r.resources.errorDuringStartRequest, u), o.log(i.name + " transport failed during the start request. Stopping the connection."), n(o).triggerHandler(f.onError, [s]), h && h.reject(s), o.stop()) : (i.stop(o), o.log(i.name + " transport failed to connect. Attempting to fall back."), e())) } }; u = r.transports._logic = { ajax: function (t, i) { return n.ajax(n.extend(!0, {}, n.signalR.ajaxDefaults, { type: "GET", data: {}, xhrFields: { withCredentials: t.withCredentials }, contentType: t.contentType, dataType: t.ajaxDataType }, i)) }, pingServer: function (t) { var e, f, i = n.Deferred(); return t.transport ? (e = t.url + "/ping", e = u.addQs(e, t.qs), f = u.ajax(t, { url: e, success: function (n) { var u; try { u = t._parseResponse(n) } catch (e) { i.reject(r._.transportError(r.resources.pingServerFailedParse, t.transport, e, f)); t.stop(); return } u.Response === "pong" ? i.resolve() : i.reject(r._.transportError(r._.format(r.resources.pingServerFailedInvalidResponse, n), t.transport, null, f)) }, error: function (n) { n.status === 401 || n.status === 403 ? (i.reject(r._.transportError(r._.format(r.resources.pingServerFailedStatusCode, n.status), t.transport, n, f)), t.stop()) : i.reject(r._.transportError(r.resources.pingServerFailed, t.transport, n, f)) } })) : i.reject(r._.transportError(r.resources.noConnectionTransport, t.transport)), i.promise() }, prepareQueryString: function (n, i) { var r; return r = u.addQs(i, "clientProtocol=" + n.clientProtocol), r = u.addQs(r, n.qs), n.token && (r += "&connectionToken=" + t.encodeURIComponent(n.token)), n.data && (r += "&connectionData=" + t.encodeURIComponent(n.data)), r }, addQs: function (t, i) { var r = t.indexOf("?") !== -1 ? "&" : "?", u; if (!i) return t; if (typeof i == "object") return t + r + n.param(i); if (typeof i == "string") return u = i.charAt(0), (u === "?" || u === "&") && (r = ""), t + r + i; throw new Error("Query string property must be either a string or object."); }, getUrl: function (n, i, r, f, e) { var h = i === "webSockets" ? "" : n.baseUrl, o = h + n.appRelativeUrl, s = "transport=" + i; return !e && n.groupsToken && (s += "&groupsToken=" + t.encodeURIComponent(n.groupsToken)), r ? (o += f ? "/poll" : "/reconnect", !e && n.messageId && (s += "&messageId=" + t.encodeURIComponent(n.messageId))) : o += "/connect", o += "?" + s, o = u.prepareQueryString(n, o), e || (o += "&tid=" + Math.floor(Math.random() * 11)), o }, maximizePersistentResponse: function (n) { return { MessageId: n.C, Messages: n.M, Initialized: typeof n.S != "undefined" ? !0 : !1, ShouldReconnect: typeof n.T != "undefined" ? !0 : !1, LongPollDelay: n.L, GroupsToken: n.G } }, updateGroups: function (n, t) { t && (n.groupsToken = t) }, stringifySend: function (n, t) { return typeof t == "string" || typeof t == "undefined" || t === null ? t : n.json.stringify(t) }, ajaxSend: function (t, i) { var h = u.stringifySend(t, i), c = e(t, "/send"), o, s = function (t, u) { n(u).triggerHandler(f.onError, [r._.transportError(r.resources.sendFailed, u.transport, t, o), i]) }; return o = u.ajax(t, { url: c, type: t.ajaxDataType === "jsonp" ? "GET" : "POST", contentType: r._.defaultContentType, data: { data: h }, success: function (n) { var i; if (n) { try { i = t._parseResponse(n) } catch (r) { s(r, t); t.stop(); return } u.triggerReceived(t, i) } }, error: function (n, i) { i !== "abort" && i !== "parsererror" && s(n, t) } }) }, ajaxAbort: function (n, t) { if (typeof n.transport != "undefined") { t = typeof t == "undefined" ? !0 : t; var i = e(n, "/abort"); u.ajax(n, { url: i, async: t, timeout: 1e3, type: "POST" }); n.log("Fired ajax abort async = " + t + ".") } }, ajaxStart: function (t, i) { var h = function (n) { var i = t._deferral; i && i.reject(n) }, s = function (i) { t.log("The start request failed. Stopping the connection."); n(t).triggerHandler(f.onError, [i]); h(i); t.stop() }; t._.startRequest = u.ajax(t, { url: e(t, "/start"), success: function (n, u, f) { var e; try { e = t._parseResponse(n) } catch (o) { s(r._.error(r._.format(r.resources.errorParsingStartResponse, n), o, f)); return } e.Response === "started" ? i() : s(r._.error(r._.format(r.resources.invalidStartResponse, n), null, f)) }, error: function (n, i, u) { i !== o ? s(r._.error(r.resources.errorDuringStartRequest, u, n)) : (t.log("The start request aborted because connection.stop() was called."), h(r._.error(r.resources.stoppedDuringStartRequest, null, n))) } }) }, tryAbortStartRequest: function (n) { n._.startRequest && (n._.startRequest.abort(o), delete n._.startRequest) }, tryInitialize: function (n, t, i) { t.Initialized && i ? i() : t.Initialized && n.log("WARNING! The client received an init message after reconnecting.") }, triggerReceived: function (t, i) { t._.connectingMessageBuffer.tryBuffer(i) || n(t).triggerHandler(f.onReceived, [i]) }, processMessages: function (t, i, r) { var f; u.markLastMessage(t); i && (f = u.maximizePersistentResponse(i), u.updateGroups(t, f.GroupsToken), f.MessageId && (t.messageId = f.MessageId), f.Messages && (n.each(f.Messages, function (n, i) { u.triggerReceived(t, i) }), u.tryInitialize(t, f, r))) }, monitorKeepAlive: function (t) { var i = t._.keepAliveData; i.monitoring ? t.log("Tried to monitor keep alive but it's already being monitored.") : (i.monitoring = !0, u.markLastMessage(t), t._.keepAliveData.reconnectKeepAliveUpdate = function () { u.markLastMessage(t) }, n(t).bind(f.onReconnect, t._.keepAliveData.reconnectKeepAliveUpdate), t.log("Now monitoring keep alive with a warning timeout of " + i.timeoutWarning + ", keep alive timeout of " + i.timeout + " and disconnecting timeout of " + t.disconnectTimeout)) }, stopMonitoringKeepAlive: function (t) { var i = t._.keepAliveData; i.monitoring && (i.monitoring = !1, n(t).unbind(f.onReconnect, t._.keepAliveData.reconnectKeepAliveUpdate), t._.keepAliveData = {}, t.log("Stopping the monitoring of the keep alive.")) }, startHeartbeat: function (n) { n._.lastActiveAt = (new Date).getTime(); s(n) }, markLastMessage: function (n) { n._.lastMessageAt = (new Date).getTime() }, markActive: function (n) { return u.verifyLastActive(n) ? (n._.lastActiveAt = (new Date).getTime(), !0) : !1 }, isConnectedOrReconnecting: function (n) { return n.state === r.connectionState.connected || n.state === r.connectionState.reconnecting }, ensureReconnectingState: function (t) { return c(t, r.connectionState.connected, r.connectionState.reconnecting) === !0 && n(t).triggerHandler(f.onReconnecting), t.state === r.connectionState.reconnecting }, clearReconnectTimeout: function (n) { n && n._.reconnectTimeout && (t.clearTimeout(n._.reconnectTimeout), delete n._.reconnectTimeout) }, verifyLastActive: function (t) { if ((new Date).getTime() - t._.lastActiveAt >= t.reconnectWindow) { var i = r._.format(r.resources.reconnectWindowTimeout, new Date(t._.lastActiveAt), t.reconnectWindow); return t.log(i), n(t).triggerHandler(f.onError, [r._.error(i, "TimeoutException")]), t.stop(!1, !1), !1 } return !0 }, reconnect: function (n, i) { var f = r.transports[i]; if (u.isConnectedOrReconnecting(n) && !n._.reconnectTimeout) { if (!u.verifyLastActive(n)) return; n._.reconnectTimeout = t.setTimeout(function () { u.verifyLastActive(n) && (f.stop(n), u.ensureReconnectingState(n) && (n.log(i + " reconnecting."), f.start(n))) }, n.reconnectDelay) } }, handleParseFailure: function (t, i, u, e, o) { var s = r._.transportError(r._.format(r.resources.parseFailed, i), t.transport, u, o); e && e(s) ? t.log("Failed to parse server response while attempting to connect.") : (n(t).triggerHandler(f.onError, [s]), t.stop()) }, initHandler: function (n) { return new h(n) }, foreverFrame: { count: 0, connections: {} } } }($jQuery1_9, window), function (n, t) { var r = n.signalR, u = n.signalR.events, f = n.signalR.changeState, i = r.transports._logic; r.transports.webSockets = { name: "webSockets", supportsKeepAlive: function () { return !0 }, send: function (t, f) { var e = i.stringifySend(t, f); try { t.socket.send(e) } catch (o) { n(t).triggerHandler(u.onError, [r._.transportError(r.resources.webSocketsInvalidState, t.transport, o, t.socket), f]) } }, start: function (e, o, s) { var h, c = !1, l = this, a = !o, v = n(e); if (!t.WebSocket) { s(); return } e.socket || (h = e.webSocketServerUrl ? e.webSocketServerUrl : e.wsProtocol + e.host, h += i.getUrl(e, this.name, a), e.log("Connecting to websocket endpoint '" + h + "'."), e.socket = new t.WebSocket(h), e.socket.onopen = function () { c = !0; e.log("Websocket opened."); i.clearReconnectTimeout(e); f(e, r.connectionState.reconnecting, r.connectionState.connected) === !0 && v.triggerHandler(u.onReconnect) }, e.socket.onclose = function (t) { var i; this === e.socket && (c && typeof t.wasClean != "undefined" && t.wasClean === !1 ? (i = r._.transportError(r.resources.webSocketClosed, e.transport, t), e.log("Unclean disconnect from websocket: " + (t.reason || "[no reason given]."))) : e.log("Websocket closed."), s && s(i) || (i && n(e).triggerHandler(u.onError, [i]), l.reconnect(e))) }, e.socket.onmessage = function (t) { var r; try { r = e._parseResponse(t.data) } catch (u) { i.handleParseFailure(e, t.data, u, s, t); return } r && (n.isEmptyObject(r) || r.M ? i.processMessages(e, r, o) : i.triggerReceived(e, r)) }) }, reconnect: function (n) { i.reconnect(n, this.name) }, lostConnection: function (n) { this.reconnect(n) }, stop: function (n) { i.clearReconnectTimeout(n); n.socket && (n.log("Closing the Websocket."), n.socket.close(), n.socket = null) }, abort: function (n, t) { i.ajaxAbort(n, t) } } }($jQuery1_9, window), function (n, t) { var i = n.signalR, u = n.signalR.events, e = n.signalR.changeState, r = i.transports._logic, f = function (n) { t.clearTimeout(n._.reconnectAttemptTimeoutHandle); delete n._.reconnectAttemptTimeoutHandle }; i.transports.serverSentEvents = { name: "serverSentEvents", supportsKeepAlive: function () { return !0 }, timeOut: 3e3, start: function (o, s, h) { var c = this, l = !1, a = n(o), v = !s, y; if (o.eventSource && (o.log("The connection already has an event source. Stopping it."), o.stop()), !t.EventSource) { h && (o.log("This browser doesn't support SSE."), h()); return } y = r.getUrl(o, this.name, v); try { o.log("Attempting to connect to SSE endpoint '" + y + "'."); o.eventSource = new t.EventSource(y, { withCredentials: o.withCredentials }) } catch (p) { o.log("EventSource failed trying to connect with error " + p.Message + "."); h ? h() : (a.triggerHandler(u.onError, [i._.transportError(i.resources.eventSourceFailedToConnect, o.transport, p)]), v && c.reconnect(o)); return } v && (o._.reconnectAttemptTimeoutHandle = t.setTimeout(function () { l === !1 && o.eventSource.readyState !== t.EventSource.OPEN && c.reconnect(o) }, c.timeOut)); o.eventSource.addEventListener("open", function () { o.log("EventSource connected."); f(o); r.clearReconnectTimeout(o); l === !1 && (l = !0, e(o, i.connectionState.reconnecting, i.connectionState.connected) === !0 && a.triggerHandler(u.onReconnect)) }, !1); o.eventSource.addEventListener("message", function (n) { var t; if (n.data !== "initialized") { try { t = o._parseResponse(n.data) } catch (i) { r.handleParseFailure(o, n.data, i, h, n); return } r.processMessages(o, t, s) } }, !1); o.eventSource.addEventListener("error", function (n) { var r = i._.transportError(i.resources.eventSourceError, o.transport, n); this === o.eventSource && (h && h(r) || (o.log("EventSource readyState: " + o.eventSource.readyState + "."), n.eventPhase === t.EventSource.CLOSED ? (o.log("EventSource reconnecting due to the server connection ending."), c.reconnect(o)) : (o.log("EventSource error."), a.triggerHandler(u.onError, [r])))) }, !1) }, reconnect: function (n) { r.reconnect(n, this.name) }, lostConnection: function (n) { this.reconnect(n) }, send: function (n, t) { r.ajaxSend(n, t) }, stop: function (n) { f(n); r.clearReconnectTimeout(n); n && n.eventSource && (n.log("EventSource calling close()."), n.eventSource.close(), n.eventSource = null, delete n.eventSource) }, abort: function (n, t) { r.ajaxAbort(n, t) } } }($jQuery1_9, window), function (n, t) { var r = n.signalR, e = n.signalR.events, o = n.signalR.changeState, i = r.transports._logic, u = function () { var n = t.document.createElement("iframe"); return n.setAttribute("style", "position:absolute;top:0;left:0;width:0;height:0;visibility:hidden;"), n }, f = function () { var i = null, f = 1e3, n = 0; return { prevent: function () { r._.ieVersion <= 8 && (n === 0 && (i = t.setInterval(function () { var n = u(); t.document.body.appendChild(n); t.document.body.removeChild(n); n = null }, f)), n++) }, cancel: function () { n === 1 && t.clearInterval(i); n > 0 && n-- } } }(); r.transports.foreverFrame = { name: "foreverFrame", supportsKeepAlive: function () { return !0 }, iframeClearThreshold: 50, start: function (n, r, e) { var l = this, s = i.foreverFrame.count += 1, h, o = u(), c = function () { n.log("Forever frame iframe finished loading and is no longer receiving messages."); e && e() || l.reconnect(n) }; if (t.EventSource) { e && (n.log("Forever Frame is not supported by SignalR on browsers with SSE support."), e()); return } o.setAttribute("data-signalr-connection-id", n.id); f.prevent(); h = i.getUrl(n, this.name); h += "&frameId=" + s; t.document.documentElement.appendChild(o); n.log("Binding to iframe's load event."); o.addEventListener ? o.addEventListener("load", c, !1) : o.attachEvent && o.attachEvent("onload", c); o.src = h; i.foreverFrame.connections[s] = n; n.frame = o; n.frameId = s; r && (n.onSuccess = function () { n.log("Iframe transport started."); r() }) }, reconnect: function (n) { var r = this; i.isConnectedOrReconnecting(n) && i.verifyLastActive(n) && t.setTimeout(function () { if (i.verifyLastActive(n) && n.frame && i.ensureReconnectingState(n)) { var u = n.frame, t = i.getUrl(n, r.name, !0) + "&frameId=" + n.frameId; n.log("Updating iframe src to '" + t + "'."); u.src = t } }, n.reconnectDelay) }, lostConnection: function (n) { this.reconnect(n) }, send: function (n, t) { i.ajaxSend(n, t) }, receive: function (t, u) { var f, e, o; if (t.json !== t._originalJson && (u = t._originalJson.stringify(u)), o = t._parseResponse(u), i.processMessages(t, o, t.onSuccess), t.state === n.signalR.connectionState.connected && (t.frameMessageCount = (t.frameMessageCount || 0) + 1, t.frameMessageCount > r.transports.foreverFrame.iframeClearThreshold && (t.frameMessageCount = 0, f = t.frame.contentWindow || t.frame.contentDocument, f && f.document && f.document.body))) for (e = f.document.body; e.firstChild;) e.removeChild(e.firstChild) }, stop: function (n) { var r = null; if (f.cancel(), n.frame) { if (n.frame.stop) n.frame.stop(); else try { r = n.frame.contentWindow || n.frame.contentDocument; r.document && r.document.execCommand && r.document.execCommand("Stop") } catch (u) { n.log("Error occurred when stopping foreverFrame transport. Message = " + u.message + ".") } n.frame.parentNode === t.document.body && t.document.body.removeChild(n.frame); delete i.foreverFrame.connections[n.frameId]; n.frame = null; n.frameId = null; delete n.frame; delete n.frameId; delete n.onSuccess; delete n.frameMessageCount; n.log("Stopping forever frame.") } }, abort: function (n, t) { i.ajaxAbort(n, t) }, getConnection: function (n) { return i.foreverFrame.connections[n] }, started: function (t) { o(t, r.connectionState.reconnecting, r.connectionState.connected) === !0 && n(t).triggerHandler(e.onReconnect) } } }($jQuery1_9, window), function (n, t) { var r = n.signalR, u = n.signalR.events, e = n.signalR.changeState, f = n.signalR.isDisconnecting, i = r.transports._logic; r.transports.longPolling = { name: "longPolling", supportsKeepAlive: function () { return !1 }, reconnectDelay: 3e3, start: function (o, s, h) { var a = this, v = function () { v = n.noop; o.log("LongPolling connected."); s ? s() : o.log("WARNING! The client received an init message after reconnecting.") }, y = function (n) { return h(n) ? (o.log("LongPolling failed to connect."), !0) : !1 }, c = o._, l = 0, p = function (i) { t.clearTimeout(c.reconnectTimeoutId); c.reconnectTimeoutId = null; e(i, r.connectionState.reconnecting, r.connectionState.connected) === !0 && (i.log("Raising the reconnect event"), n(i).triggerHandler(u.onReconnect)) }, w = 36e5; o.pollXhr && (o.log("Polling xhr requests already exists, aborting."), o.stop()); o.messageId = null; c.reconnectTimeoutId = null; c.pollTimeoutId = t.setTimeout(function () { (function e(s, h) { var g = s.messageId, nt = g === null, k = !nt, tt = !h, d = i.getUrl(s, a.name, k, tt, !0), b = {}; (s.messageId && (b.messageId = s.messageId), s.groupsToken && (b.groupsToken = s.groupsToken), f(s) !== !0) && (o.log("Opening long polling request to '" + d + "'."), s.pollXhr = i.ajax(o, { xhrFields: { onprogress: function () { i.markLastMessage(o) } }, url: d, type: "POST", contentType: r._.defaultContentType, data: b, timeout: o._.pollTimeout, success: function (r) { var h, w = 0, u, a; o.log("Long poll complete."); l = 0; try { h = o._parseResponse(r) } catch (b) { i.handleParseFailure(s, r, b, y, s.pollXhr); return } (c.reconnectTimeoutId !== null && p(s), h && (u = i.maximizePersistentResponse(h)), i.processMessages(s, h, v), u && n.type(u.LongPollDelay) === "number" && (w = u.LongPollDelay), f(s) !== !0) && (a = u && u.ShouldReconnect, !a || i.ensureReconnectingState(s)) && (w > 0 ? c.pollTimeoutId = t.setTimeout(function () { e(s, a) }, w) : e(s, a)) }, error: function (f, h) { var v = r._.transportError(r.resources.longPollFailed, o.transport, f, s.pollXhr); if (t.clearTimeout(c.reconnectTimeoutId), c.reconnectTimeoutId = null, h === "abort") { o.log("Aborted xhr request."); return } if (!y(v)) { if (l++, o.state !== r.connectionState.reconnecting && (o.log("An error occurred using longPolling. Status = " + h + ".  Response = " + f.responseText + "."), n(s).triggerHandler(u.onError, [v])), (o.state === r.connectionState.connected || o.state === r.connectionState.reconnecting) && !i.verifyLastActive(o)) return; if (!i.ensureReconnectingState(s)) return; c.pollTimeoutId = t.setTimeout(function () { e(s, !0) }, a.reconnectDelay) } } }), k && h === !0 && (c.reconnectTimeoutId = t.setTimeout(function () { p(s) }, Math.min(1e3 * (Math.pow(2, l) - 1), w)))) })(o) }, 250) }, lostConnection: function (n) { n.pollXhr && n.pollXhr.abort("lostConnection") }, send: function (n, t) { i.ajaxSend(n, t) }, stop: function (n) { t.clearTimeout(n._.pollTimeoutId); t.clearTimeout(n._.reconnectTimeoutId); delete n._.pollTimeoutId; delete n._.reconnectTimeoutId; n.pollXhr && (n.pollXhr.abort(), n.pollXhr = null, delete n.pollXhr) }, abort: function (n, t) { i.ajaxAbort(n, t) } } }($jQuery1_9, window), function (n) { function r(n) { return n + e } function s(n, t, i) { for (var f = n.length, u = [], r = 0; r < f; r += 1) n.hasOwnProperty(r) && (u[r] = t.call(i, n[r], r, n)); return u } function h(t) { return n.isFunction(t) ? null : n.type(t) === "undefined" ? null : t } function u(n) { for (var t in n) if (n.hasOwnProperty(t)) return !0; return !1 } function f(n, t) { var i = n._.invocationCallbacks, r, f; u(i) && n.log("Clearing hub invocation callbacks with error: " + t + "."); n._.invocationCallbackId = 0; delete n._.invocationCallbacks; n._.invocationCallbacks = {}; for (f in i) r = i[f], r.method.call(r.scope, { E: t }) } function i(n, t) { return new i.fn.init(n, t) } function t(i, r) { var u = { qs: null, logging: !1, useDefaultPath: !0 }; return n.extend(u, r), (!i || u.useDefaultPath) && (i = (i || "") + "/signalr"), new t.fn.init(i, u) } var e = ".hubProxy", o = n.signalR; i.fn = i.prototype = { init: function (n, t) { this.state = {}; this.connection = n; this.hubName = t; this._ = { callbackMap: {} } }, constructor: i, hasSubscriptions: function () { return u(this._.callbackMap) }, on: function (t, i) { var u = this, f = u._.callbackMap; return t = t.toLowerCase(), f[t] || (f[t] = {}), f[t][i] = function (n, t) { i.apply(u, t) }, n(u).bind(r(t), f[t][i]), u }, off: function (t, i) { var e = this, o = e._.callbackMap, f; return t = t.toLowerCase(), f = o[t], f && (f[i] ? (n(e).unbind(r(t), f[i]), delete f[i], u(f) || delete o[t]) : i || (n(e).unbind(r(t)), delete o[t])), e }, invoke: function (t) { var i = this, r = i.connection, e = n.makeArray(arguments).slice(1), c = s(e, h), f = { H: i.hubName, M: t, A: c, I: r._.invocationCallbackId }, u = n.Deferred(), l = function (f) { var e = i._maximizeHubResponse(f), h, s; n.extend(i.state, e.State); e.Progress ? u.notifyWith ? u.notifyWith(i, [e.Progress.Data]) : r._.progressjQueryVersionLogged || (r.log("A hub method invocation progress update was received but the version of jQuery in use (" + n.prototype.jquery + ") does not support progress updates. Upgrade to jQuery 1.7+ to receive progress notifications."), r._.progressjQueryVersionLogged = !0) : e.Error ? (e.StackTrace && r.log(e.Error + "\n" + e.StackTrace + "."), h = e.IsHubException ? "HubException" : "Exception", s = o._.error(e.Error, h), s.data = e.ErrorData, r.log(i.hubName + "." + t + " failed to execute. Error: " + s.message), u.rejectWith(i, [s])) : (r.log("Invoked " + i.hubName + "." + t), u.resolveWith(i, [e.Result])) }; return r._.invocationCallbacks[r._.invocationCallbackId.toString()] = { scope: i, method: l }, r._.invocationCallbackId += 1, n.isEmptyObject(i.state) || (f.S = i.state), r.log("Invoking " + i.hubName + "." + t), r.send(f), u.promise() }, _maximizeHubResponse: function (n) { return { State: n.S, Result: n.R, Progress: n.P ? { Id: n.P.I, Data: n.P.D } : null, Id: n.I, IsHubException: n.H, Error: n.E, StackTrace: n.T, ErrorData: n.D } } }; i.fn.init.prototype = i.fn; t.fn = t.prototype = n.connection(); t.fn.init = function (t, i) { var e = { qs: null, logging: !1, useDefaultPath: !0 }, u = this; n.extend(e, i); n.signalR.fn.init.call(u, t, e.qs, e.logging); u.proxies = {}; u._.invocationCallbackId = 0; u._.invocationCallbacks = {}; u.received(function (t) { var f, o, e, i, s, h; t && (typeof t.P != "undefined" ? (e = t.P.I.toString(), i = u._.invocationCallbacks[e], i && i.method.call(i.scope, t)) : typeof t.I != "undefined" ? (e = t.I.toString(), i = u._.invocationCallbacks[e], i && (u._.invocationCallbacks[e] = null, delete u._.invocationCallbacks[e], i.method.call(i.scope, t))) : (f = this._maximizeClientHubInvocation(t), u.log("Triggering client hub event '" + f.Method + "' on hub '" + f.Hub + "'."), s = f.Hub.toLowerCase(), h = f.Method.toLowerCase(), o = this.proxies[s], n.extend(o.state, f.State), n(o).triggerHandler(r(h), [f.Args]))) }); u.error(function (n, t) { var i, r; t && (i = t.I, r = u._.invocationCallbacks[i], r && (u._.invocationCallbacks[i] = null, delete u._.invocationCallbacks[i], r.method.call(r.scope, { E: n }))) }); u.reconnecting(function () { u.transport && u.transport.name === "webSockets" && f(u, "Connection started reconnecting before invocation result was received.") }); u.disconnected(function () { f(u, "Connection was disconnected before invocation result was received.") }) }; t.fn._maximizeClientHubInvocation = function (n) { return { Hub: n.H, Method: n.M, Args: n.A, State: n.S } }; t.fn._registerSubscribedHubs = function () { var t = this; t._subscribedToHubs || (t._subscribedToHubs = !0, t.starting(function () { var i = []; n.each(t.proxies, function (n) { this.hasSubscriptions() && (i.push({ name: n }), t.log("Client subscribed to hub '" + n + "'.")) }); i.length === 0 && t.log("No hubs have been subscribed to.  The client will not receive data from hubs.  To fix, declare at least one client side function prior to connection start for each hub you wish to subscribe to."); t.data = t.json.stringify(i) })) }; t.fn.createHubProxy = function (n) { n = n.toLowerCase(); var t = this.proxies[n]; return t || (t = i(this, n), this.proxies[n] = t), this._registerSubscribedHubs(), t }; t.fn.init.prototype = t.fn; n.hubConnection = t }($jQuery1_9, window), function (n) { n.signalR.version = "2.2.1" }($jQuery1_9);



// http://localhost:8080/signalr/hubs
/*!
 * ASP.NET SignalR JavaScript Library v2.2.1
 * http://signalr.net/
 *
 * Copyright (c) .NET Foundation. All rights reserved.
 * Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
 *
 */

/// <reference path="..\..\SignalR.Client.JS\Scripts\jquery-1.6.4.js" />
/// <reference path="jquery.signalR.js" />
(function ($, window, undefined) {
    /// <param name="$" type="jQuery" />
    "use strict";

    if (typeof ($.signalR) !== "function") {
        throw new Error("SignalR: SignalR is not loaded. Please ensure jquery.signalR-x.js is referenced before ~/signalr/js.");
    }

    var signalR = $.signalR;

    function makeProxyCallback(hub, callback) {
        return function () {
            // Call the client hub method
            callback.apply(hub, $.makeArray(arguments));
        };
    }

    function registerHubProxies(instance, shouldSubscribe) {
        var key, hub, memberKey, memberValue, subscriptionMethod;

        for (key in instance) {
            if (instance.hasOwnProperty(key)) {
                hub = instance[key];

                if (!(hub.hubName)) {
                    // Not a client hub
                    continue;
                }

                if (shouldSubscribe) {
                    // We want to subscribe to the hub events
                    subscriptionMethod = hub.on;
                } else {
                    // We want to unsubscribe from the hub events
                    subscriptionMethod = hub.off;
                }

                // Loop through all members on the hub and find client hub functions to subscribe/unsubscribe
                for (memberKey in hub.client) {
                    if (hub.client.hasOwnProperty(memberKey)) {
                        memberValue = hub.client[memberKey];

                        if (!$.isFunction(memberValue)) {
                            // Not a client hub function
                            continue;
                        }

                        subscriptionMethod.call(hub, memberKey, makeProxyCallback(hub, memberValue));
                    }
                }
            }
        }
    }

    $.hubConnection.prototype.createHubProxies = function () {
        var proxies = {};
        this.starting(function () {
            // Register the hub proxies as subscribed
            // (instance, shouldSubscribe)
            registerHubProxies(proxies, true);

            this._registerSubscribedHubs();
        }).disconnected(function () {
            // Unsubscribe all hub proxies when we "disconnect".  This is to ensure that we do not re-add functional call backs.
            // (instance, shouldSubscribe)
            registerHubProxies(proxies, false);
        });

        proxies['SepPcPosHub'] = this.createHubProxy('SepPcPosHub');
        proxies['SepPcPosHub'].client = {};
        proxies['SepPcPosHub'].server = {
            _posClientBase_CardSwiped: function (sender, e) {
                return proxies['SepPcPosHub'].invoke.apply(proxies['SepPcPosHub'], $.merge(["_posClientBase_CardSwiped"], $.makeArray(arguments)));
            },

            _posClientBase_ErrorReceived: function (sender, e) {
                return proxies['SepPcPosHub'].invoke.apply(proxies['SepPcPosHub'], $.merge(["_posClientBase_ErrorReceived"], $.makeArray(arguments)));
            },

            _posClientBase_PosResultReceived: function (sender, e) {
                return proxies['SepPcPosHub'].invoke.apply(proxies['SepPcPosHub'], $.merge(["_posClientBase_PosResultReceived"], $.makeArray(arguments)));
            },

            PCPOSInitial: function (IP, Port) {
                return proxies['SepPcPosHub'].invoke.apply(proxies['SepPcPosHub'], $.merge(["PCPOSInitial"], $.makeArray(arguments)));
            },

            PCPOSSendAmount: function (Amount, AuthorizationId) {
                return proxies['SepPcPosHub'].invoke.apply(proxies['SepPcPosHub'], $.merge(["PCPOSSendAmount"], $.makeArray(arguments)));
            },

            PCPOSWaitForPos: function (TimeOut) {
                return proxies['SepPcPosHub'].invoke.apply(proxies['SepPcPosHub'], $.merge(["PCPOSWaitForPos"], $.makeArray(arguments)));
            },

            send: function (name, message) {
                return proxies['SepPcPosHub'].invoke.apply(proxies['SepPcPosHub'], $.merge(["Send"], $.makeArray(arguments)));
            }
        };

        return proxies;
    };

    signalR.hub = $.hubConnection("/signalr", { useDefaultPath: false });
    $.extend(signalR, signalR.hub.createHubProxies());

}($jQuery1_9, window));