﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/enums.js" />
$.ajaxSetup({
    cache: false
});
// General variables used in forms
var _Context = {
    table: '',
    title: '',
    editForm: '',
    id: -1,
    resultID: -1,
    ready: false,
    userID: -1,
    userName: '',
    userFullName: '',
    isAdmin: false,
    isStoreUser: false,
    // csv hash values. ex: ,12345,946123,23312,123123123,21312,
    access: '',
    personnelID: -1,
    personnelName: '',
    success: false,
    viewMode: false,
    today: '',
    // finance year start date: in language format
    fyStart: '',
    // finance year end date: in language format
    fyEnd: '',
    programStatus: 0, // 0: all, 1: ok, 2: closed
    isAccountingVer: true,
    priceDate: null,
    priceFactor: [], // monetary unit converter: [['1392/01/01', '1392/06/31', 10], ['1392/07/01', '1392/12/29', 20]]
    baseUrl: './',
    editFinalized: false,
    //bViewMode: ViewEnum.Doctor, // bedriddenViewMode
    isMobile: window.matchMedia("only screen and (max-width: 760px)").matches || /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
    displayMenu: false,
    // to be used with select popup-forms like purchaseselect
    selectResult: null,
    filterPostBack: null,
    level: 1,
    distribution: 0,
    salary: 0,
    isDemo: false,
    workflowInheritance: true,
    decimalPoints: 2,
    POSUrl: 'localhost:14560',
    POSPort: 'COM2',
    monetaryUnitAbbreviation: '',
    monetaryUnitAbbreviationSecond: '',
    SecondFactorPrice: '',
    useAdditionalValue: 0,
    todayDate: ''
};
var gridWidth = '';
var popupGridWidth = 900;
var gridHeight = 450;
var gridToggle = false;
var gridRp = 15;
var gridResizable = false;
var clicksDisabled = false;
var showWaiting = false; // used to prevent showing forms twice on doubleClick
var rowsPerPage = 40;
var generalSubSystem = 'general';
var generalManageForms = 'manageforms';
var generalSettings = 'generalsettings';

var _options = {
    hideColumnButtons: false,
    hideColumnDelete: false,
    hideColumnEdit: false,
    gridWidth: '',
    gridHeight: 450,
    gridToggle: false,
    gridResizable: false,

    enableAdmissionDate: false,
    dateType: 'fa', // fa, en, ar
    lang: 'fa' // ku, fa
}

// --------------------------------
// Enums : Started
// --------------------------------

// --------------------------------
// Enums : Ended
// --------------------------------
function hasValueNumber(val) {
    return val && (val != '') && (val != -1);
}
function hasValuePositive(val) {
    return val && (val != '') && (parseFloat(val) > 0);
}
function stringToBytes(str) {
    var bytes = [];

    for (var i = 0; i < str.length; ++i) {
        bytes.push(str.charCodeAt(i));
    }
    return bytes;
}
function getStableHash(s, hashlength) {
    var hash = 0;
    var bytes = stringToBytes(s);   // this function just grabs a byte array for the given input string
    for (var i = 0; i < bytes.length; i++) {
        hash += bytes[i];
        hash += (hash << 10);
        hash ^= (hash >>> 6);
    }
    // final avalanche
    hash += (hash << 3);
    hash ^= (hash >>> 11);
    hash += (hash << 15);
    return (hash >>> 0) % hashlength;
    //return Math.round(hash % hashlength);
}
var t_0 = 0;
function hasAccess(subsystem, form, button) {
    if (_Context.isAdmin)
        return true;
    // subsystem is currently ignored.
    // sample access : ;Gender_new;Gender_print;Gender_delete;Gender_edit;city_new;city_print;city_delete;city_edit;country_new;country_print;country_delete;country_edit;
    var key = form + "_" + button;
    var hash = "," + getStableHash(key, 10000000) + ",";
    return _Context.access.indexOf(hash) >= 0;
}
function hasGeneralAccess(button, form) {
    form = form || generalManageForms;
    return hasAccess(generalSubSystem, form, button);
}
function hasLinkAccess(subsystem, link) {
    if (_Context.isAdmin)
        return true;
    // subsystem is currently ignored.
    // sample access : ;Gender_new;Gender_print;Gender_delete;Gender_edit;city_new;city_print;city_delete;city_edit;country_new;country_print;country_delete;country_edit;
    return (_Context.access.indexOf(link) >= 0);
}
function canSeeAllPersonnel(subsystem, form) {
    if (_Context.personnelID <= 0)
        return true;
    return hasAccess(subsystem, form, 'SeeAllPersonnel');
}
function hideColumn(subsystem, form, button) {
    if (button == 'delete')
        return _options.hideColumnDelete || !hasAccess(subsystem, form, button) || !hasGeneralAccess(button);
    if (button == 'edit')
        return _options.hideColumnEdit || !hasAccess(subsystem, form, button) || !hasGeneralAccess(button);
    return _options.hideColumnButtons || !hasAccess(subsystem, form, button)
}
function disableClicks() {
    clicksDisabled = true;
    setTimeout(function () { clicksDisabled = false }, 2000);
}
function notImpemented() {
    $.prompt(Lang.NotImplemented);
}
function appendTime() {
    return "&time=" + (new Date().valueOf());
}
$.urlParam = function (name) {
    var results = new RegExp('[\\?&]' + name + '=([^&]*)').exec(window.location.href);
    return results ? (results[1] || 0) : null;
}
$.strParam = function (name, href) {
    var results = new RegExp('[\\?&]' + name + '=([^&]*)').exec(href);
    return results ? (results[1] || 0) : null;
}

if (!String.prototype.trim) {
    String.prototype.trim = function () { return this.replace(/^\s+|\s+$/g, ''); };
    String.prototype.ltrim = function () { return this.replace(/^\s+/, ''); };
    String.prototype.rtrim = function () { return this.replace(/\s+$/, ''); };
    String.prototype.fulltrim = function () { return this.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g, '').replace(/\s+/g, ' '); };
}
function message(html) {
    if (html.indexOf("Error:") >= 0)
        Growl.Error(html);
    else
        Growl.Notice(html);
    //var skinMode = html.indexOf("Error:") >= 0 ? 'red' : 'modern';
    //$('#message_bar').displayMessage({
    //    message: html,
    //    position: 'fixed',
    //    skin: skinMode,
    //    autohide: true
    //});
}
function displayError(html, form, title) {
    if (html.indexOf('Error:') == 0)
        html = html.substring('Error:'.length).trim();
    if (html.indexOf('خطا:') == 0) // This should not be translated !!!
        html = html.substring('خطا:'.length).trim(); // Neither should this
    //Error: Violation of UNIQUE KEY constraint 'IX_Service_Code'. Cannot insert duplicate key in object 'dbo.Service'. 
    if (html.indexOf('UNIQUE KEY constraint') >= 0) {
        html = html.substring(html.indexOf("'") + 1);
        html = html.substring(0, html.indexOf("'"));
        if (html.indexOf('IX_') == 0)
            html = html.substring('IX_'.length);
        if (html.indexOf('_') > 0)
            html = html.substring(html.indexOf("_") + 1);
        // try to retrive label
        if (form) {
            var lbl = $('label[for="' + html + '"]', form).text();
            if (lbl)
                html += " - " + lbl;
        }
        html = Lang.ThisFieldsValueIsDuplicate + ":<br />" + html;
    }
    // delete conflict errors: we must list them all  here
    if (html.indexOf('DELETE') >= 0) {
        for (var p in ForeignKeys)
            if (html.indexOf(p) >= 0) {
                var fk = ForeignKeys[p];
                if (fk == "")
                    fk = p;
                html = Lang.ThisRecordHasBeenUsedIn + ":<br />" + fk;
            }
    }

    title = title || Lang.Error;
    html = "<div class='popupTitle'>" + title + "</div><div style='max-height:400px;overflow:auto' class='msg'>" + html + "</div>";
    $.prompt(html);
}
function ajaxError(xhr, ajaxOptions, thrownError) {
    showWaiting = false;
    alert('Status=' + xhr.status + ' StatusText=' + xhr.statusText + ' thrownError=' + thrownError);
}
function displayPrescriptionStatus(celDiv, id) {
    //0:New,1:Paid
    var status = parseInt($(celDiv).html(), 0);
    if (status == 0) {
        $(celDiv).html(Lang.SNew);
        $(celDiv).addClass("cfs0");
    }
    else if (status == 1) {
        $(celDiv).html(Lang.MSConfrimed);
        $(celDiv).addClass("cfs1");
    }
    else {
        $(celDiv).html(Lang.UnknownError);
        $(celDiv).addClass("unknown");
    }
}
function doConfirm(func, msg, noFunc, showCancel, scroll) {
    msg = msg || Lang.AreYouSureYouWantToDeleteThisItem;
    msg = '<span class="msg">' + msg + '</span><script>shortcut.remove("Y");shortcut.remove("N");shortcut.add("Y", function () {$(".jqibuttons").find(":button[value=' + "Yes" + ']").click()});shortcut.add("N", function () {$(".jqibuttons").find(":button[value=' + "No" + ']").click()});</script>';
    var btns = { بلهY: 'Yes', خیرN: 'No' };
    if (showCancel)
        btns[Lang.Cancel] = 'Cancel';
    $.prompt(msg, {
        submit: function(v, m, f) {
            if (v == 'Yes') {
                func(v, m, f);
            } else if (v == 'No') {
                if (noFunc) noFunc();
            }
            shortcut.remove('Y');
            shortcut.remove('N');
        },
        buttons: btns,
        focus: 0
    });
    if (scroll)
        window.scrollTo(0, 0);
}
function doConfirmDel(func, msg) {
    msg = msg || Lang.AreYouSureYouWantToDeleteThisItem2;
    msg = '<div class="tc"><span class="msg">' + msg + '</span><br /><textarea type="text" id="delReason" name="delReason" class="wide"></textarea></div><script>shortcut.remove("Y");shortcut.remove("N");shortcut.add("Y", function () {$(".jqibuttons").find(":button[value=' + "Yes" + ']").click()});shortcut.add("N", function () {$(".jqibuttons").find(":button[value=' + "No" + ']").click()});</script>';
    var btns = { بلهY: 'Yes', خیرN: 'No' };
    $.prompt(msg, {
        submit: function (v, m, f) {
            if (v == 'Yes') {
                func(v, m, f);
            } else if (v == 'No') {
                
            }
            shortcut.remove('Y');
            shortcut.remove('N');
        },
        buttons: btns,
        focus: 0
    });
}

// UMED : 92-01-25 : onSelect added to support Tab select events
function tabify(parent, onSelect) {
    if (parent) {
        $(".tab_content", parent).hide(); //Hide all content
        $("ul.tabs li:first", parent).addClass("active").show(); //Activate first tab
        $(".tab_content:first", parent).show(); //Show first tab content

        //On Click Event
        $("ul.tabs li", parent).click(function () {

            $("ul.tabs li", parent).removeClass("active"); //Remove any "active" class
            $(this).addClass("active"); //Add "active" class to selected tab
            $(".tab_content", parent).hide(); //Hide all tab content

            var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
            if (activeTab) {
                $(activeTab).fadeIn(); //Fade in the active ID content
                if (onSelect)
                    onSelect(activeTab);
            }
            return false;
        });
    }
    else {
        $(".tab_content").hide(); //Hide all content
        $("ul.tabs li:first").addClass("active").show(); //Activate first tab
        $(".tab_content:first").show(); //Show first tab content

        //On Click Event
        $("ul.tabs li").click(function () {

            $("ul.tabs li").removeClass("active"); //Remove any "active" class
            $(this).addClass("active"); //Add "active" class to selected tab
            $(".tab_content").hide(); //Hide all tab content

            var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
            if (activeTab) {
                $(activeTab).fadeIn(); //Fade in the active ID content
                if (onSelect)
                    onSelect(activeTab);
            }
            return false;
        });
    }
}
function showLink(celDiv, id, funcShow) {
    funcShow = funcShow || show;
    $(celDiv).html('<img src="img/edit.gif" />');
    $(celDiv).click(function (event) {
        event.preventDefault();
        funcShow(id, this);
    });
}
// newquery--click edit
function show2Link(celDiv, id) {
    $(celDiv).html('<img src="img/edit.gif" />');
    $(celDiv).click(function (event) {
        alert("show2Link");
        event.preventDefault();
        return id;
    });
}
function showLink2(celDiv, id, table, qd, more, op) {
    op = op || 'GetHtml';
    $(celDiv).html('<img src="img/edit.gif" />');
    $(celDiv).click(function (event) {
        event.preventDefault();
        var _url = 'get.aspx?op=' + op + '&table=' + table + '&qd=' + qd + '&id=' + id + more + appendTime();
        showPopup(_url, function () { }, { بستن: 'Cancel' });
    });
}

function editLink(celDiv, id, funcShow) {
    funcShow = funcShow || show;
    $(celDiv).html('<img src="img/edit.gif" />');
    $(celDiv).click(function (event) {
        event.preventDefault();
        if (clicksDisabled)
            return;
        disableClicks();
        funcShow(id, this);
    });
}
function checkMark(celDiv, id, funcShow) {
    funcShow = funcShow || show;
    $(celDiv).html('<input type="checkbox" class="checkmark" />');
}
function editLink2(celDiv, id, table, qd, more, callback, op) {
    op = op || 'GetHtml';
    $(celDiv).html('<img src="img/edit.gif" />');
    $(celDiv).click(function (event) {
        event.preventDefault();
        if (clicksDisabled)
            return;
        disableClicks();
        var _url = 'get.aspx?op=' + op + '&table=' + table + '&qd=' + qd + '&id=' + id + more + appendTime();
        $.ajax({
            url: _url,
            type: 'GET',
            success: function (html) {
                $.prompt(html, {
                    submit: callback,
                    buttons: getButtons(),
                    focus: 1
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert('Status=' + xhr.status + ' StatusText=' + xhr.statusText + ' thrownError=' + thrownError);
            }
        });
    });
}
function delLink(celDiv, id, table, flexi) {
    table = table || _Context.table;
    flexi = flexi || "#flexi";
    $(celDiv).html('<img src="img/delete.gif" />');
    $(celDiv).click(function (event) {
        if (clicksDisabled)
            return;
        disableClicks();
        event.preventDefault();
        doConfirmDel(function (v, m, f) {
            var _url = 'set.aspx?op=Delete&table=' + table + '&id=' + id + '&comment=' + encodeURIComponent(f.delReason) + appendTime();
            //alert(_url);
            $.ajax({
                url: _url,
                type: 'GET',
                success: function (html) {
                    message(html);
                    if (html.indexOf("Error:") >= 0 || html.indexOf('خطا:') >= 0) {
                        // show errors:
                        displayError(html);
                    }
                    else
                        $(flexi).flexReload();
                },
                error: ajaxError
            });
        });
    });
}
function showBool(celDiv, id) {
    if (celDiv.innerText == 'True')
        $(celDiv).html('<span style="color:#0AD30A;"><i class="fa fa-check"></i></span>');
    else
        $(celDiv).html('<span style="color: red;"><i class="fa fa-times"></i></span>');
}
function showInt(celDiv, id) {
    //todo: format integr
    $(celDiv).html(numberWithCommas(celDiv.innerText));
}
function showMonetary(celDiv, id, text) {
    $(celDiv).html(numberWithCommas(toMonetary(text)));
}
function capturePriceDate(celDiv, id, text) {
    _Context.priceDate = text;
}
function showDate(celDiv, id) {
    //todo: display date
    $(celDiv).html(celDiv.innerText);
}
function showTime(celDiv, id) {
    //todo: display Time
    $(celDiv).html(celDiv.innerText);
}

function showTaskDuration(celDiv, id) {
    var minutes = toInt(celDiv.innerText);
    var hours = Math.round(minutes / 60) + '';
    minutes = (minutes % 60) + '';
    while (hours.length < 2)
        hours = '0' + hours;
    while (minutes.length < 2)
        minutes = '0' + minutes;
    //todo: display Time
    $(celDiv).html(hours + ':' + minutes);
}
// not tested
function getRow(id) {
    var td = document.getElementById('flexi').rows[id].cells;
    str = '';
    for (j = 0; j < td.length; j++) {  //serialize as you like
        str += "'" + $(td[j]).text() + "',";
    }
    return str;
}


function popup(url) {
    var width = '1050';
    var height = 500;
    var left = (screen.width - width) / 2;
    var top = (screen.height - height) / 2;
    var params = 'width=' + width + ', height=' + height;
    params += ', top=' + top + ', left=' + left;
    params += ', directories=no';
    params += ', location=no';
    params += ', menubar=no';
    params += ', resizable=yes';
    params += ', scrollbars=yes';
    params += ', status=no';
    params += ', toolbar=no';
    newwin = window.open(url, 'windowname5', params);
    if (window.focus) { newwin.focus() }
}
function choose(formTitle, table, autoQuery, postback) {
    var _url = 'get.aspx?op=GetHtml&view=chooser&Table=' + table + '&Auto=' + autoQuery + '&FormTitle=' + formTitle + appendTime();
    $.ajax({
        url: _url,
        type: 'GET',
        success: function (html) {
            $.prompt(html, {
                submit: function showcallback(v, m, f) {
                    if (v == 'OK' && f.TheID > 0)
                        postback(f.TheID, f.TheName);
                    else
                        postback(-1, '');
                },
                buttons: { OK: 'OK' },
                focus: 0,
                top: '1%'
            });
            $('#Name').focus();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert('Status=' + xhr.status + ' StatusText=' + xhr.statusText + ' thrownError=' + thrownError);
        }
    });
}
function chooseFile(formTitle, postback, autoQuery) {
    autoQuery = autoQuery || '';
    var _url = 'get.aspx?op=GetHtml&table=Files&view=filechooser&qd=' + autoQuery + '&FormTitle=' + formTitle + appendTime();
    $.ajax({
        url: _url,
        type: 'GET',
        success: function (html) {
            $.prompt(html, {
                submit: function showcallback(v, m, f) {
                    if (v == 'OK' && f.TheID > 0)
                        //_id, _fullname, _nationalNo, _no, _repeat, _status
                        postback(f.TheID, f.TheFullname, f.TheNationalCode, f.TheNo, f.TheRepeat, f.TheStatus);
                    else
                        postback(-1, '', null, null, null);
                },
                buttons: { تأیید: 'OK', انصراف: 'Cancel' },
                focus: 0,
                top: '1%'
            });
            $('#Name').focus();
        },
        error: ajaxError
    });
}

function getRowID(grid) {
    var res = -1;
    if ($('.trSelected', grid).length != 1) {
        return res;
    }
    else {
        $('.trSelected', grid).each(function () {
            res = $(this).attr('id');
            res = res.substring(res.lastIndexOf('row') + 3);
        });
    }
    return res;
}


// return an array of selected IDs
function getRowIDs(grid) {
    var res = [];
    $('.trSelected', grid).each(function () {
        var id = $(this).attr('id');
        res[res.length] = id.substring(id.lastIndexOf('row') + 3);
    });
    return res;
}
//get row table
function getRowtable(url) {
    var res = new Array();
    $.ajax({
        url: url,
        type: 'GET',
        async: false,
        success: function (data) {
            var rows = data.split("\n");
            if (data.trim().length == 0)
                return;
            for (var i = 0; i < rows.length; i++) {
                res[i] = new Array(2);
                var row = $.trim(rows[i]);
                if (row) {
                    row = row.split("|");
                    res[i] = row;
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            ajaxError(xhr, ajaxOptions, thrownError);
        }
    });

    return res;
}
function getRowstable(url) {
    var res = new Array();
    $.ajax({
        url: url,
        type: 'GET',
        async: false,
        success: function (data) {
            var rows = data.split("\n");
            for (var i = 0; i < rows.length; i++) {
                res[i] = new Array(4);
                var row = $.trim(rows[i]);
                if (row) {
                    row = row.split("|");
                    res[i][0] = row[0];
                    res[i][1] = row[1];
                    res[i][2] = row[2];
                    if ((row[3] == 'True') || (row[3] == 1) || (row[3] == null)) {
                        res[i][3] = 0;
                    }
                    else if ((row[3] == 'False') || (row[3] == 0)) {
                        res[i][3] = row[4];
                    }
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert('Status=' + xhr.status + ' StatusText=' + xhr.statusText + ' thrownError=' + thrownError);
        }
    });

    return res;
}
function getGridRowContent(grid, colIndex) {
    var res = '';
    if ($('.trSelected', grid).length != 1) {
        return '';
    }
    $('.trSelected', grid).each(function () {
        var td = $('td:nth-child(' + colIndex + ')', this);
        var div = $('div', td);
        res = $('div', td).html();
    });
    return res;
}
function getGridRowContents(grid, colIndex) {
    var res = [];
    $('.trSelected', grid).each(function () {
        var td = $('td:nth-child(' + colIndex + ')', this);
        var div = $('div', td);
        res[res.length] = $('div', td).html();
    });
    return res;
}
function getGridRowClasses(grid, colIndex) {
    var res = [];
    $('.trSelected', grid).each(function () {
        var td = $('td:nth-child(' + colIndex + ')', this);
        var div = $('div', td);
        res[res.length] = $('div', td).attr('class');
    });
    return res;
}
function getGridRowClass(grid, colIndex) {
    var res = '';
    $('.trSelected', grid).each(function () {
        var td = $('td:nth-child(' + colIndex + ')', this);
        var div = $('div', td);
        res = $('div', td).attr('class');
    });
    return res;
}
function getColClass(tr, colIndex) {
    var td = $('td:nth-child(' + colIndex + ')', tr);
    var div = $('div', td);
    var res = $('div', td).attr('class');
    return res;
}
function getColContent(tr, colIndex) {
    var td = $('td:nth-child(' + colIndex + ')', tr);
    var div = $('div', td);
    var res = $('div', td).html();
    return res;
}
/* ----------------------*/
/*   Keep Grid Selection */
var _selectedRowIDs;
function saveSelectedRows(grid, curID) {
    if (curID) {
        _selectedRowIDs = [curID];
        //_selectedRowIDs[_selectedRowIDs.length] = curID;
    }
    else
        _selectedRowIDs = getRowIDs(grid);
}
function restoreSelectedRows(grid) {
    //alert(_selectedRowIDs);
    if (typeof _selectedRowIDs === 'undefined')
        return;
    if (_selectedRowIDs.length == 0)
        return;
    for (var i = 0; i < _selectedRowIDs.length; i++)
        $('#row' + _selectedRowIDs[i]).addClass('trSelected');
}
/* --------------------*/

/* ----------------------*/
/*   Keep Grid checked checkboxes */
var _checkedRowIDs = [];
function saveCheckedRows(curId) {
    if (curId) {
        _checkedRowIDs[_checkedRowIDs.length] = [curId];
    }
}
function restoreCheckedRows(grid) {
    if (typeof _checkedRowIDs === 'undefined')
        return;
    if (_checkedRowIDs.length == 0)
        return;
    for (var i = 0; i < _checkedRowIDs.length; i++) {
        var row = $('#row' + _checkedRowIDs[i]);
        row.children().children().children('.checkmark').attr('checked', 'checked');
    }
}

function saveDisCheckedRows(curId) {
    _checkedRowIDs = jQuery.grep(_checkedRowIDs,
        function(value) {
            return value != curId;
        });
}

/* --------------------*/

// exportTitle, exportHide, exportStatusMapping: for Excel or Html or Word or ....
function generalCommand(com, grid, funcShow, exportTitle, exportHide, exportEnums, multiPrintRoot, secondgrid) {
    if (clicksDisabled)
        return;
    var ids = '', id = '';
    disableClicks();
    funcShow = funcShow || show;
    if (com == Lang.GBEdit) {
        id = getRowID(grid);
        if (id != -1) {
            funcShow(id);
        }
    }
    else if (com == Lang.GBDel) {
        ids = '';
        $('.trSelected', grid).each(function () {
            id = $(this).attr('id');
            id = id.substring(id.lastIndexOf('row') + 3);
            ids += id + ',';
        });
        if (ids.length > 0) {
            ids = ids.substring(0, ids.length - 1); // remove last comma
            doConfirmDel(function (v, m, f) {
                var _url = 'set.aspx?op=MultiDelete&table=' + _Context.table + '&ids=' + ids + '&comment=' + encodeURIComponent(f.delReason) + appendTime();
                $.ajax({
                    url: _url,
                    type: 'GET',
                    success: function (html) {
                        message(html);
                        if (html.indexOf("Error:") >= 0 || html.indexOf('خطا:') >= 0) {
                            // show errors:
                            displayError(html);
                        }
                        else
                            $("#flexi").flexReload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert('Status=' + xhr.status + ' StatusText=' + xhr.statusText + ' thrownError=' + thrownError);
                    }
                });
            });
        }
    }
    else if (com == Lang.GBNew) {
        funcShow(-1);
    }
    else if (com == Lang.GBPrint) {
           var totalIds = '', indexOfPipe = 0;
            $('.trSelected', grid).each(function () {
                id = $(this).attr('id');
                id = id.substring(id.lastIndexOf('row') + 3);
                ids += ',' + id;
            });
            if (ids && multiPrintRoot) {
                ids = ids.replace(',', '');
                totalIds = ids.replace(/\,/g, '|');
                while (indexOfPipe >= 0) {
                    indexOfPipe = totalIds.indexOf('|', 190);
                    if (indexOfPipe >= 0) {
                        window.open(eval(multiPrintRoot.replace('totalIds', 'totalIds.substr(0, indexOfPipe)')));
                    }

                    totalIds = totalIds.substr(indexOfPipe + 1);
                }
                window.open(eval(multiPrintRoot));
            } else {
                if (secondgrid) {
                    printCommandSecond();
                } else {
                    printCommand();
                }
            }
    }
    else if (com == Lang.GBExcel) {
        if (secondgrid) {
            excelCommandSecond(com, grid, funcShow, null, exportTitle, exportHide, exportEnums);
        } else {
            excelCommand(com, grid, funcShow, null, exportTitle, exportHide, exportEnums);
        }
    }
    else
        notImpemented();
}

function printCommand() {
    var _url = $("#flexi").flexUrl();
    var fParams = "";
    $(':input', '#fValues').each(function () {
        // 'q' is for fields, 'w' is more Where (w in queries.xml)
        var fName = '_q_' + $(this).attr('q');
        if ($(this).attr('w'))
            fName = $(this).attr('w');
        var fValue = $(this).val();
        if (fValue && fValue.length > 0 && fValue != -1)
            fParams += "&" + fName + "=" + fValue;
    });
    if (fParams.length > 0)
        fParams += '&comb=' + $('input:checked', '#fComb').val() + '&_qMode=q';
    fParams += '&orderby=' + $('#flexi').flexSortname() + '&sortorder=' + $('#flexi').flexSortorder();
    _url += fParams;

    // _url: service.asmx/GetList?table=PaybackReason&qd=enum&time=1366654656236
    var _colModel = $("#flexi").flexColModel();

    //var list = '<link href="styles/print.css" rel="stylesheet" type="text/css" />';
    // table header
    var colSelection = '', thIndex = 0;
    for (var i = 0; i < _colModel.length; i++) {
        if (_colModel[i].hide || _colModel[i].name == 'Edit' || _colModel[i].name == 'Enabled' || _colModel[i].name == 'Del')
            continue;
        var cssClass = '';

        if (_colModel[i].css_class) {
            cssClass = "class='" + _colModel[i].css_class + "'";
        }
        colSelection += "<div class='col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container check'>"
                    + "<div class='form-group'>"
                    + "    <label for='colTh" + thIndex + "' class='left-aligned-label col-xs-12 col-sm-4'>" + _colModel[i].display + "</label>"
                    + "    <div class='col-xs-12 col-sm-8 form-col-without-padding'>"
                    + "        <input type='checkbox' value='1' id='colTh" + thIndex + "' name='colTh" + thIndex + "' class='form-control input-sm ac_input' checked='checked' />"
                    + "    </div></div></div>";
        // colSelection += "<label>" + _colModel[i].display + "<input type='checkbox' value='1' id='colTh" + thIndex + "' name='colTh" + thIndex + "' checked='checked' /></label>";
        thIndex++;
    }    

    colSelection = "<div class='container'><div class='row'><div class='colSelection'>" + Lang.ChoosePrintColumns + '<br />' + colSelection + "</div></div></div>"
    doConfirm(function (v, m, f) {
        thIndex = 0;
        for (var i = 0; i < _colModel.length; i++) {
            if (_colModel[i].hide || _colModel[i].name == 'Edit' || _colModel[i].name == 'Enabled' || _colModel[i].name == 'Del')
                continue;
            _colModel[i].doPrint = f['colTh' + thIndex] == 1;
            thIndex++;
        }

        var header = '<tr>';
        for (var i = 0; i < _colModel.length; i++) {
            if (_colModel[i].hide || _colModel[i].name == 'Edit' || _colModel[i].name == 'Enabled' || _colModel[i].name == 'Del')
                continue;
            if (_colModel[i].doPrint) {
                var cssClass = '';
                if (_colModel[i].css_class) {
                    cssClass = "class='" + _colModel[i].css_class + "'";
                }
                header += '<th ' + cssClass + '>' + _colModel[i].display + '</th>';
            }
        }
        header += '</tr>';
        var list = "<table class='_printTablePopup'>";
        list += header;
        var printDummy = $('#printDummy');
        //alert(_url);
        $.ajax({
            url: _url,
            type: 'GET',
            dataType: 'xml',
            async: false,
            success: function (xmlDoc) {
                var xNode = (xmlDoc.firstChild.nextSibling) ? xmlDoc.firstChild.nextSibling : xmlDoc.firstChild;
                var items = xNode; //.childNodes[0];
                var lineNo = 0;
                $(items).find("row").each(function () {
                    if (lineNo > 0 && (lineNo % rowsPerPage == 0)) {
                        //start new page
                        /// list += "</table><table style='page-break-after: always'  align='center' cellspacing=0 border=0>" + header;
                    }
                    //nodeName, nodeType
                    list += '<tr>';
                    lineNo++;
                    var counter = 0;
                    for (var i = 0; i < this.childNodes.length; i++) {
                        if (this.childNodes[i].nodeType != 1)
                            continue;
                        if (!(_colModel[counter].hide || _colModel[counter].name == 'Edit' || _colModel[counter].name == 'Enabled' || _colModel[counter].name == 'Del')) {
                            if (_colModel[counter].doPrint) {
                                var val = this.childNodes[i].childNodes[0].nodeValue;
                                if (_colModel[counter].process) {
                                    printDummy.innerText = val;
                                    printDummy.html(val);
                                    _colModel[counter].process(printDummy, null, val);
                                    val = printDummy.html();
                                }
                                var cssClass = '';
                                if (_colModel[counter].css_class) {
                                    cssClass = "class='" + _colModel[counter].css_class + "'";
                                }
                                list += "<td " + cssClass + ">" + val + "</td>";
                            }
                        }
                        counter++;
                    }
                    list += '</tr>';
                    //var row = addRow($(this));
                    //$(table + ' tr:last').after(row);
                });
            },
            error: ajaxError
        });
        list += '</table>';

        var newWin = window.open(this.href);
        newWin.document.write('<html lang="fa" dir="rtl">');
        newWin.document.write('<head>');
        newWin.document.write('<link href="styles/beauty.css" rel="stylesheet" type="text/css">');
        newWin.document.write('<link href="styles/utility.css" rel="stylesheet" type="text/css">');
        newWin.document.write('</head>');
        newWin.document.write('<body>');
        newWin.document.write(list);
        newWin.document.write('</body>');
        newWin.document.write('</html>');
        //newWin.print();
    }, colSelection);
}

function printCommandSecond() {
    var _url = $("#scondFlexi").flexUrl();
    var fParams = "";
    $(':input', '#fValues').each(function () {
        // 'q' is for fields, 'w' is more Where (w in queries.xml)
        var fName = '_q_' + $(this).attr('q');
        if ($(this).attr('w'))
            fName = $(this).attr('w');
        var fValue = $(this).val();
        if (fValue && fValue.length > 0 && fValue != -1)
            fParams += "&" + fName + "=" + fValue;
    });
    if (fParams.length > 0)
        fParams += '&comb=' + $('input:checked', '#fComb').val() + '&_qMode=q';
    fParams += '&orderby=' + $('#scondFlexi').flexSortname() + '&sortorder=' + $('#scondFlexi').flexSortorder();
    _url += fParams;

    // _url: service.asmx/GetList?table=PaybackReason&qd=enum&time=1366654656236
    var _colModel = $("#scondFlexi").flexColModel();

    //var list = '<link href="styles/print.css" rel="stylesheet" type="text/css" />';
    // table header
    var colSelection = '', thIndex = 0;
    for (var i = 0; i < _colModel.length; i++) {
        if (_colModel[i].hide || _colModel[i].name == 'Edit' || _colModel[i].name == 'Enabled' || _colModel[i].name == 'Del')
            continue;
        var cssClass = '';

        if (_colModel[i].css_class) {
            cssClass = "class='" + _colModel[i].css_class + "'";
        }
        colSelection += "<div class='col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container check'>"
                    + "<div class='form-group'>"
                    + "    <label for='colTh" + thIndex + "' class='left-aligned-label col-xs-12 col-sm-4'>" + _colModel[i].display + "</label>"
                    + "    <div class='col-xs-12 col-sm-8 form-col-without-padding'>"
                    + "        <input type='checkbox' value='1' id='colTh" + thIndex + "' name='colTh" + thIndex + "' class='form-control input-sm ac_input' checked='checked' />"
                    + "    </div></div></div>";
        // colSelection += "<label>" + _colModel[i].display + "<input type='checkbox' value='1' id='colTh" + thIndex + "' name='colTh" + thIndex + "' checked='checked' /></label>";
        thIndex++;
    }

    colSelection = "<div class='container'><div class='row'><div class='colSelection'>" + Lang.ChoosePrintColumns + '<br />' + colSelection + "</div></div></div>"
    doConfirm(function (v, m, f) {
        thIndex = 0;
        for (var i = 0; i < _colModel.length; i++) {
            if (_colModel[i].hide || _colModel[i].name == 'Edit' || _colModel[i].name == 'Enabled' || _colModel[i].name == 'Del')
                continue;
            _colModel[i].doPrint = f['colTh' + thIndex] == 1;
            thIndex++;
        }

        var header = '<tr>';
        for (var i = 0; i < _colModel.length; i++) {
            if (_colModel[i].hide || _colModel[i].name == 'Edit' || _colModel[i].name == 'Enabled' || _colModel[i].name == 'Del')
                continue;
            if (_colModel[i].doPrint) {
                var cssClass = '';
                if (_colModel[i].css_class) {
                    cssClass = "class='" + _colModel[i].css_class + "'";
                }
                header += '<th ' + cssClass + '>' + _colModel[i].display + '</th>';
            }
        }
        header += '</tr>';
        var list = "<table class='_printTablePopup'>";
        list += header;
        var printDummy = $('#printDummy');
        //alert(_url);
        $.ajax({
            url: _url,
            type: 'GET',
            dataType: 'xml',
            async: false,
            success: function (xmlDoc) {
                var xNode = (xmlDoc.firstChild.nextSibling) ? xmlDoc.firstChild.nextSibling : xmlDoc.firstChild;
                var items = xNode; //.childNodes[0];
                var lineNo = 0;
                $(items).find("row").each(function () {
                    if (lineNo > 0 && (lineNo % rowsPerPage == 0)) {
                        //start new page
                        /// list += "</table><table style='page-break-after: always'  align='center' cellspacing=0 border=0>" + header;
                    }
                    //nodeName, nodeType
                    list += '<tr>';
                    lineNo++;
                    var counter = 0;
                    for (var i = 0; i < this.childNodes.length; i++) {
                        if (this.childNodes[i].nodeType != 1)
                            continue;
                        if (!(_colModel[counter].hide || _colModel[counter].name == 'Edit' || _colModel[counter].name == 'Enabled' || _colModel[counter].name == 'Del')) {
                            if (_colModel[counter].doPrint) {
                                var val = this.childNodes[i].childNodes[0].nodeValue;
                                if (_colModel[counter].process) {
                                    printDummy.innerText = val;
                                    printDummy.html(val);
                                    _colModel[counter].process(printDummy, null, val);
                                    val = printDummy.html();
                                }
                                var cssClass = '';
                                if (_colModel[counter].css_class) {
                                    cssClass = "class='" + _colModel[counter].css_class + "'";
                                }
                                list += "<td " + cssClass + ">" + val + "</td>";
                            }
                        }
                        counter++;
                    }
                    list += '</tr>';
                    //var row = addRow($(this));
                    //$(table + ' tr:last').after(row);
                });
            },
            error: ajaxError
        });
        list += '</table>';

        var newWin = window.open(this.href);
        newWin.document.write('<html lang="fa" dir="rtl">');
        newWin.document.write('<head>');
        newWin.document.write('<link href="styles/beauty.css" rel="stylesheet" type="text/css">');
        newWin.document.write('<link href="styles/utility.css" rel="stylesheet" type="text/css">');
        newWin.document.write('</head>');
        newWin.document.write('<body>');
        newWin.document.write(list);
        newWin.document.write('</body>');
        newWin.document.write('</html>');
        //newWin.print();
    }, colSelection);
}
// examples:
// hide: ID,InvoiceID
// enums:   Status=0^New^1^OK&State=0^Bad^2^Good
function excelCommand(com, grid, funcShow, extra, title, hide, enums) {
    var _url = $("#flexi").flexUrl();
    // _url: service.asmx/GetList?table=PaybackReason&qd=enum&time=1366654656236
    var fParams = "";
    $(':input', '#fValues').each(function () {
        // 'q' is for fields, 'w' is more Where (w in queries.xml)
        var fName = '_q_' + $(this).attr('q');
        if ($(this).attr('w'))
            fName = $(this).attr('w');
        var fValue = $(this).val();
        if (fValue && fValue.length > 0 && fValue != -1)
            fParams += "&" + fName + "=" + fValue;
        //fParams[fParams.length] = { name: fName, value: fValue };

        //var fName = '_q_' + $(this).attr('q');
        //var fValue = $(this).val();
        //if (fValue && fValue.length > 0 && fValue != -1)
        //    fParams += "&" + fName + "=" + fValue;
    });
    //alert($('input:checked', '#fComb').val());
    if (fParams.length > 0)
        fParams += '&comb=' + $('input:checked', '#fComb').val();
    fParams += '&orderby=' + $('#flexi').flexSortname() + '&sortorder=' + $('#flexi').flexSortorder();
    _url = _url.replace('service.asmx/GetList?', 'get.aspx?op=excel&');
    _url += fParams;
    if (extra)
        _url += "&" + extra;
    if (title)
        _url += "&t=" + title;
    if (hide)
        _url += "&hide=" + hide;
    if (enums)
        _url += "&" + enums;
    //$.prompt(_url);
    document.location = _url;
}

function excelCommandSecond(com, grid, funcShow, extra, title, hide, enums) {
    var _url = $("#scondFlexi").flexUrl();
    // _url: service.asmx/GetList?table=PaybackReason&qd=enum&time=1366654656236
    var fParams = "";
    $(':input', '#fValues').each(function () {
        // 'q' is for fields, 'w' is more Where (w in queries.xml)
        var fName = '_q_' + $(this).attr('q');
        if ($(this).attr('w'))
            fName = $(this).attr('w');
        var fValue = $(this).val();
        if (fValue && fValue.length > 0 && fValue != -1)
            fParams += "&" + fName + "=" + fValue;
        //fParams[fParams.length] = { name: fName, value: fValue };

        //var fName = '_q_' + $(this).attr('q');
        //var fValue = $(this).val();
        //if (fValue && fValue.length > 0 && fValue != -1)
        //    fParams += "&" + fName + "=" + fValue;
    });
    //alert($('input:checked', '#fComb').val());
    if (fParams.length > 0)
        fParams += '&comb=' + $('input:checked', '#fComb').val();
    fParams += '&orderby=' + $('#scondFlexi').flexSortname() + '&sortorder=' + $('#scondFlexi').flexSortorder();
    if (_url.includes('service.asmx/GetList?')) {
        _url = _url.replace('service.asmx/GetList?', 'get.aspx?op=excel&');
    } else if (_url.includes('service.asmx/GetQueryList?')) {
        _url = _url.replace('service.asmx/GetQueryList?', 'get.aspx?op=excel&');
    }

    _url += fParams;
    if (extra)
        _url += "&" + extra;
    if (title)
        _url += "&t=" + title;
    if (hide)
        _url += "&hide=" + hide;
    if (enums)
        _url += "&" + enums;
    //$.prompt(_url);
    document.location = _url;
}

function insertValidation(form) {
    form = form || '#detailForm';
    //$("#detailForm").validationEngine();
    $('input', form).each(function () {
        var me = $(this);
        if (_Context.isAccountingVer && me.hasClass('noacc')) // it is permanantly hidden
            return;
        var cls = me.attr('class');
        if (!cls)
            return;
        var rules = cls.split(/\[|,|\]/);
        for (var i = 0; i < rules.length; i++)
            if (rules[i] == 'required') {
                var n = me.next();
                //$(this).val(n.html());
                if (n.html() != '*' && n.html() != '<span class="red">*</span>')
                    me.after('<span class="red">*</span>');
                me.keyup(function () {
                    if (me.val() != '')
                        me.removeClass('req');
                });
                break;
            }
    });
    checkInputAccess(form);
}

function addRequired(selector) {
    var me = $(selector);
    if (!me.hasClass('validate[required]')) {
        me.addClass('validate[required]');
        var n = me.next();
        if (n.html() != '*' && n.html() != '<span class="red">*</span>')
            me.after('<span class="red">*</span>');
        me.unbind("keyup", function () {
            if (me.val() != '')
                me.removeClass('req');
        });
        me.keyup(function () {
            if (me.val() != '')
                me.removeClass('req');
        });
    }
}
function removeRequired(selector) {
    var me = $(selector);
    me.removeClass('req');
    if (me.hasClass('validate[required]')) {
        me.removeClass('validate[required]');
        var n = me.next();
        if (n.html() == '*' || n.html() == '<span class="red">*</span>')
            n.detach();
    }
}
function checkInputAccess(form) {
    if (_Context.isAdmin)
        return;
    form = form || '#detailForm';
    var UID = _Context.userID;
    // ex: access[bedridden_referringdoctor]
    $('input', form).each(function () {
        var me = $(this);
        var cls = me.attr('class');
        if (!cls)
            return;
        var rules = cls.split(/\[|,|\]| /);
        for (var i = 0; i < rules.length; i++) {
            if (rules[i] == 'access') {
                if (_Context.access.indexOf(rules[i + 1].toLowerCase()) < 0) {
                    me.attr("disabled", "disabled");
                    return;
                }
            }
        }
    });
}
function findTitleById(id, form) {
    // try to retrive label
    if (form) {
        var lbl = $('label[for="' + id + '"]', form).text();
        if (lbl)
            return lbl;
        lbl = $('#' + id).attr('placeholder');
        if (lbl)
            return lbl;
    }
    return id;
}
function validateInputs(form, f) {
    // old way
    //return $('#detailForm').validationEngine('validate');
    form = form || '#detailForm';

    // Check custom formValidation
    if (f.formIsValid && f.formIsValid == '0') {
        return false;
    }

    var res = true;
    var intPattern = new RegExp(/^[\-\+]?\d+$/);
    var fPattern = new RegExp(/^[\-\+]?(([0-9]+)([\.,]([0-9]+))?|([\.,]([0-9]+))?)$/);
    var ePattern = new RegExp(/^([\w!.%+\-])+@([\w\-])+(?:\.[\w\-]+)+$/);
    var datePattern = new RegExp(/^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/);
    if (_options.dateType != 'fa') {
        datePattern = new RegExp(/^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/);
    }
    var msg = '';
    $('input', form).each(function () {
        var me = $(this);
        me.removeClass('req').removeClass('reqi');
        if ((me.attr('disabled') == '' || !me.attr('disabled')) && me.is(":visible")) {
            var _val = me.val();
            if (me.hasClass('percent'))
                _val = _val.replace('%', '');
            var cls = me.attr('class');
            if (!cls)
                return;
            var rules = cls.split(/\[|,|\]/);
            for (var i = 0; i < rules.length; i++) {
                if (rules[i] == 'required' && _val == '') {
                    me.addClass('req');
                    msg += findTitleById(me.attr("ID"), form) + ' : ' + Lang.IsRequired + '\r\n';
                    res = false;
                    break;
                }
                if (_val == '')
                    continue;
                if (rules[i].indexOf('dateSelect') >= 0 && !datePattern.test(_val)) {
                    me.addClass('req');
                    msg += findTitleById(me.attr("ID"), form) + ' : ' + Lang.MustBeDate + '\r\n';
                    res = false;
                    break;
                }
                if (rules[i] == 'integer' && !intPattern.test(stripCommas(_val))) {
                    if (!me.hasClass('monetary')) {
                        me.addClass('reqi');
                        msg += findTitleById(me.attr("ID"), form) + ' : ' + Lang.MustBeAnInteger + '\r\n';
                        res = false;
                        break;
                    } 
                }
                if (rules[i] == 'integer' && !fPattern.test(stripCommas(_val))) {
                    if (me.hasClass('monetary')) {
                        me.addClass('reqi');
                        msg += findTitleById(me.attr("ID"), form) + ' : ' + Lang.MustBeANumber + '\r\n';
                        res = false;
                        break;
                    }
                }
                if (rules[i] == 'nocommainteger' && !intPattern.test(_val)) {
                    if (!me.hasClass('monetary')) {
                        me.addClass('reqi');
                        msg += findTitleById(me.attr("ID"), form) + ' : ' + Lang.MustBeAnIntegerNoComma + '\r\n';
                        res = false;
                        break;
                    }
                }
                if (rules[i] == 'number' && !fPattern.test(stripCommas(_val))) {
                    me.addClass('reqi');
                    msg += findTitleById(me.attr("ID"), form) + ' : ' + Lang.MustBeANumber + '\r\n';
                    res = false;
                    break;
                }
                if (rules[i] == 'email' && !ePattern.test(_val)) {
                    me.addClass('reqi');
                    msg += findTitleById(me.attr("ID"), form) + ' : ' + Lang.MustBeAValidEmail + '\r\n';
                    res = false;
                    break;
                }
                if (rules[i] == 'ncode' && !isValidNationalCode(_val)) {
                    me.addClass('reqi');
                    msg += findTitleById(me.attr("ID"), form) + ' : ' + Lang.SSNIsNotValid + '\r\n';
                    res = false;
                    break;
                }
                if (rules[i] == 'minSize' && i < (rules.length - 1)) {
                    // me.addClass('reqi');
                    var _min = parseInt(rules[i + 1], 10);
                    if (_val.length < _min) {
                        me.addClass('reqi');
                        msg += findTitleById(me.attr("ID"), form) + ' : ' + Lang.MinimumLenght + ' = ' + _min + '\r\n';
                        res = false;
                        break;
                    }
                }
                if (rules[i] == 'maxSize' && i < (rules.length - 1)) {
                    var _max = parseInt(rules[i + 1], 10);
                    if (_val.length > _max) {
                        me.addClass('reqi');
                        msg += findTitleById(me.attr("ID"), form) + ' : '+ Lang.MaximumLength + ' = ' + _max + '\r\n';
                        res = false;
                        break;
                    }
                }
                if (rules[i] == 'min' && i < (rules.length - 1)) {
                    var _min = parseFloat(rules[i + 1]);
                    if (parseFloat(_val) < _min) {
                        me.addClass('reqi');
                        msg += findTitleById(me.attr("ID"), form) + ' : ' + Lang.MinimumValue + ' = ' + _min + '\r\n';
                        res = false;
                        break;
                    }
                }
                // const string BracketOpen = "brAckoP", BracketClose = "brAckcL";
                if (rules[i] == 'regex' && i < (rules.length - 2)) {
                    // Sample: regex[ddd,عدد 3 رقمی]
                    var pattern = new RegExp(rules[i + 1].replace('brAckoP', '[').replace('brAckcL', ']'));
                    if (!pattern.test(_val)) {
                        alert(pattern + " -- " + _val);
                        me.addClass('reqi');
                        msg += findTitleById(me.attr("ID"), form) + " : " + rules[i + 2] + ' \r\n';
                        res = false;
                        break;
                    }
                }
            }
        }
    });
    if (!res)
        $('.popupTitle').css('background-color', '#ee0000');
    if (msg != '')
        alert(msg)
    return res;
}
function generalPrintTemplate(form, template) {
    form = form || '#detailForm';
    var list = '<link href="styles/print.css" rel="stylesheet" type="text/css" />';
    var chunks = template.split(/\{|\}/);  // split by { }
    for (var i = 0; i < chunks.length; i++) {
        if (chunks[i] == '')
            continue;
        if (chunks[i][0] == '#' || chunks[i][0] == '.' || chunks[i].indexOf('label') == 0) // it is a selector
        {
            var val = chunks[i];
            if ($(chunks[i]).length > 0) {
                val = $(chunks[i]).val();
                if (!val || val == '')
                    val += $(chunks[i]).outerHtml();
            }
            list += val;
        }
        else
            list += chunks[i];
    }
    //list += $(form).parent().html();
    newWin = window.open(this.href);
    newWin.document.write(list);
    //alert($('.popupTitle', newWin.document).length);
    $('.popupTitle', newWin.document).css("width", $(form).attr("width") + "px");

    // enlarge small divs
    $('div', newWin.document).css('overflow', 'auto').css('height', 'auto');

    // remove add column
    // <img src="img/add.gif" class="addSpecimen">
    $("img[src='img/add.gif']", newWin.document).each(function () {
        //var example1ColIndex = cell.parent("tr").children().index(cell);
        var img = $(this);
        var index = img.parent('td').parent().children().index(img.parent('td'));
        var table = img.closest('table');
        $('tr', table).find('td:eq(' + index + '),th:eq(' + index + ')').remove();
    });
    return false;
}
function generalPrint(v, m, f, form) {
    form = form || '#detailForm';
    var list = '<link href="styles/print.css" rel="stylesheet" type="text/css" />';
    list += $(form).parent().html();

    newWin = window.open(this.href);
    newWin.document.write(list);
    //alert($('.popupTitle', newWin.document).length);
    $('.popupTitle', newWin.document).css("width", $(form).attr("width") + "px");

    // enlarge small divs
    $('div', newWin.document).css('overflow', 'auto').css('height', 'auto');

    // untabify
    $('.tabs li a', newWin.document).each(function () {
        //<ul class="tabs">
        //  <li class=" "><a href="#tabGeneral" id="aGeneral" style="display: none;">اطلاعات عمومی</a></li>
        var display = $(this).css('display');
        if (display != 'none') {
            var href = $(this).attr('href');
            $(href, newWin.document).show();
            $(href, newWin.document).before('<b>' + $(this).html() + '</b><br />');
        }
        $(this).parent().remove();
    });

    // remove add column
    // <img src="img/add.gif" class="addSpecimen">
    $("img[src='img/add.gif']", newWin.document).each(function () {
        //var example1ColIndex = cell.parent("tr").children().index(cell);
        var img = $(this);
        var index = img.parent('td').parent().children().index(img.parent('td'));
        var table = img.closest('table');
        $('tr', table).find('td:eq(' + index + '),th:eq(' + index + ')').remove();
    });
    return false;
}
function generalOK(v, m, f, updateQry, insertQuery, table, form) {
    form = form || '#detailForm';
    _Context.success = false;
    var valid = validateInputs(form, f);
    if (!valid)
        return false;
    if (_Context.isDemo) {
        alert(Lang.DemoVersionNoSave);
        return false;
    }
    table = table || _Context.table;
    _Context.resultID = _Context.id;
    var op = "RunQuery";
    if (v == 'OKPrint')
        op = "RunScalar";
    var _url = "set.aspx?op=" + op + "&qd=" + updateQry + "&table=" + table + appendTime();
    if (!_Context.id || _Context.id == -1)
        _url = "set.aspx?op=" + op + "&qd=" + insertQuery + "&table=" + table + appendTime();
    commaInputsRemove(form);
    monetaryInputsRemove(form);
    percentInputsRemove(form);
    textInputsCommaRemove(form);
    dataString = $(form).serialize();
    textInputsCommaSet(form);
    monetaryInputsSet(form);
    commaInputsSet(form);
    percentInputsSet(form);
    //alert(_url + " --> " + dataString);
    $.ajax({
        type: "POST",
        url: _url,
        data: dataString,
        async: false,
        success: function (html) {
            if (html.indexOf('Error:') != 0 && html.indexOf('خطا:') != 0) {
                message(html);
                if (v == 'OKPrint')
                    _Context.resultID = parseInt(html, 10);
                _Context.success = true;
                $("#flexi").flexReload();
            }
            else {
                displayError(html);
            }
        },
        error: function () {
            _Context.success = false;
        }
    });
    if (v == 'OKPrint')
        popup('print.aspx?id=' + _Context.resultID + '&table=' + table + appendTime());
    if (_Context.success) {
        var title = $('.popupTitle').html();
        $('.popupTitle').html(Lang.Saved).css('background-color', '#5cb85c');
        // todo: this line has bugs in new jquery :(
        //$('.popupTitle').animate({ 'background-color': '#f00' }, 1000, 'linear', function () { $('.popupTitle').animate({ 'background-color': '#00496F' }, 'slow'); $('.popupTitle').html(title); });
    }
    else
        $('.popupTitle').css('background-color', '#e00');
    return false; // postSuccess;
}
function generalOKScalar(v, m, f, updateQry, insertQuery, table, form) {
    form = form || '#detailForm';
    _Context.success = false;
    var valid = validateInputs(form, f);
    if (!valid)
        return false;
    table = table || _Context.table;
    var _res = '';
    var _url = "set.aspx?op=RunScalar&qd=" + updateQry + "&table=" + table + appendTime();
    if (_Context.id == -1)
        _url = "set.aspx?op=RunScalar&qd=" + insertQuery + "&table=" + table + appendTime();
    commaInputsRemove(form);
    monetaryInputsRemove(form);
    percentInputsRemove(form);
    textInputsCommaRemove(form);
    dataString = $(form).serialize();
    textInputsCommaSet(form);
    monetaryInputsSet(form);
    commaInputsSet(form);
    percentInputsSet(form);
    $.ajax({
        type: "POST",
        url: _url,
        data: dataString,
        async: false,
        success: function (html) {
            if (html.indexOf('Error:') != 0 && html.indexOf('خطا:') != 0) {
                message(html);
                _Context.resultID = parseInt(html, 10);
                _res = html;
                _Context.success = true;
                $("#flexi").flexReload();
            }
            else {
                displayError(html);
            }
        },
        error: function () {
            _Context.success = false;
        }
    });
    return _res; // postSuccess;
}
function generalShowcallback(v, m, f, updateQry, insertQuery, table, form, funcShow) {
    form = form || '#detailForm';
    funcShow = funcShow || show;

    if (v == 'OK' || v == 'OKPrint') {
        return generalOK(v, m, f, updateQry, insertQuery, table, form);
    }
    if (v == "Print")
        return generalPrint(v, m, f, form);
    $(form).validationEngine('hide');
    if (v == 'New') {
        funcShow(-1);
    }
    return true;
}

// reshow: close and show the form anyway, even in edit mode. it is usefull for cases 
//  that there are some formula or triggers on the update query, like Orders
function newShowcallback(v, m, f, updateQry, insertQuery, table, form, funcShow, reshow, extraQuery) {
    funcShow = funcShow || show;
    var res = newShowcallbackInternal(v, m, f, updateQry, insertQuery, table, form, funcShow, extraQuery);
    if (res == 0)
        return false;
    if (res < 0)
        return true;
    if (_Context.id == -1 || reshow) {
        funcShow(res, null, function () {
            setTimeout(function () {
                var title = $('.popupTitle').html();
                $('.popupTitle').html(Lang.Saved).css('background-color', '#5cb85c');
            }, 200);
        });
    }
    else
        return false;
}
// 0 = false
// -1 = true
// >0 = ID
function newShowcallbackInternal(v, m, f, updateQry, insertQuery, table, form, funcShow, extraQuery) {
    _Context.success = false;
    funcShow = funcShow || show;
    if (v == 'New') {
        funcShow(-1);
        return -1;
    }
    form = form || '#detailForm';
    if (v == 'OK' || v == 'OKPrint' || v == 'Calc') {
        var valid = validateInputs(form, f);
        if (!valid)
            return 0;
        if (_Context.isDemo) {
            alert(Lang.DemoVersionNoSave);
            return 0;
        }
        table = table || _Context.table;
        _Context.resultID = _Context.id;
        var _url = "set.aspx?op=RunScalar&qd=" + updateQry + "&table=" + table + appendTime();
        if (!_Context.id || _Context.id == -1)
            _url = "set.aspx?op=RunScalar&qd=" + insertQuery + "&table=" + table + appendTime();
        if (extraQuery)
            _url += "&extra=" + extraQuery;
        commaInputsRemove(form);
        monetaryInputsRemove(form);
        percentInputsRemove(form);
        textInputsCommaRemove(form);
        dataString = $(form).serialize();
        textInputsCommaSet(form);
        monetaryInputsSet(form);
        commaInputsSet(form);
        percentInputsSet(form);
        $.ajax({
            type: "POST",
            url: _url,
            data: dataString,
            async: false,
            success: function (html) {
                if (html.indexOf('Error:') != 0 && html.indexOf('خطا:') != 0) {
                    message(html);
                    _Context.resultID = parseInt(html, 10);
                    _Context.success = true;
                    //$("#flexi").flexReload();
                    setTimeout(function () { $("#flexi").flexReload(); }, 100);
                }
                else {
                    displayError(html);
                }
            },
            error: function () {
                _Context.success = false;
            }
        });
        if (!_Context.success)
            return 0;
        if (v == 'OKPrint')
            popup('print.aspx?id=' + _Context.resultID + '&table=' + table + appendTime());
        if (_Context.success) {
            var title = $('.popupTitle').html();
            $('.popupTitle').html(Lang.Saved).css('background-color', '#5cb85c');
            // todo: this line has bugs in new jquery :(
            //$('.popupTitle').animate({ 'background-color': '#f00' }, 1000, 'linear', function () { $('.popupTitle').animate({ 'background-color': '#00496F' }, 'slow'); $('.popupTitle').html(title); });
        }
        else
            $('.popupTitle').css('background-color', '#e00');
        return _Context.resultID;
    }
    if (v == "Print")
        return generalPrint(v, m, f, form);
    $(form).validationEngine('hide');
    return -1;
}
function generalMulticallback(v, m, f, updateQry, insertQuery, table, funcShow) {
    funcShow = funcShow || show;
    if (v == 'New') {
        funcShow(-1);
    }
    if (v == 'OK') {
        var valid = validateInputs(null, f);
        if (!valid)
            return false;
        if (_Context.isDemo) {
            alert(Lang.DemoVersionNoSave);
            return false;
        }
        table = table || _Context.table;
        var _url = "set.aspx?op=multi&uqd=" + updateQry + "&iqd=" + insertQuery + "&table=" + table + appendTime();
        commaInputsRemove(form);
        monetaryInputsRemove(form);
        percentInputsRemove(form);
        textInputsCommaRemove(form);
        dataString = $(form).serialize();
        textInputsCommaSet(form);
        monetaryInputsSet(form);
        commaInputsSet(form);
        percentInputsSet(form);
        //alert(_url + " --> " + dataString);
        var postSuccess = false;
        $.ajax({
            type: "POST",
            url: _url,
            data: dataString,
            async: false,
            success: function (html) {
                if (html.indexOf('Error:') != 0 && html.indexOf('خطا:') != 0) {
                    message(html);
                    postSuccess = true;
                    $("#flexi").flexReload();
                }
                else
                    displayError(html);
            },
            error: function () {
                postSuccess = false;
            }
        });
        return postSuccess;
    }
    $('#detailForm').validationEngine('hide');
    return true;
}
// Master + One Detail
function generalMastercallback(v, m, f, masterTable, masterQry, table, updateQry, insertQry, deleteQry, requiredField, form, funcShow, noReload, extraQuery) {
    _Context.success = false;
    funcShow = funcShow || show;
    if (v == 'New') {
        funcShow(-1);
    }
    form = form || '#detailForm';
    if (v == 'OK' || v == 'OKPrint') {
        var valid = validateInputs(form, f);
        if (!valid)
            return false;
        if (_Context.isDemo) {
            alert(Lang.DemoVersionNoSave);
            return false;
        }
        table = table || _Context.table;
        var _id = _Context.id;
        var _url = "set.aspx?op=master&mtable=" + masterTable + "&mq=" + masterQry + "&table=" + table + "&uqd=" + updateQry + "&iqd=" + insertQry + "&dqd=" + deleteQry + '&req=' + requiredField + appendTime();
        if (extraQuery)
            _url += "&extra=" + extraQuery;
        commaInputsRemove(form);
        monetaryInputsRemove(form);
        percentInputsRemove(form);
        textInputsCommaRemove(form);
        dataString = $(form).serialize();
        textInputsCommaSet(form);
        monetaryInputsSet(form);
        commaInputsSet(form);
        percentInputsSet(form);
        // alert(dataString);
        $.ajax({
            type: "POST",
            url: _url,
            data: dataString,
            async: false,
            success: function (html) {
                if (html.indexOf('Error:') < 0 && html.indexOf('خطا:') < 0) {
                    message(html);
                    if (v == 'OKPrint')
                        _id = parseInt(html, 10);
                    _Context.success = true;
                    if (!noReload) {
                        $("#flexi").flexReload();
                        //alert(noReload);
                    }
                }
                else
                    displayError(html);
            },
            error: function () {
                _Context.success = false;
            }
        });
        if (v == 'OKPrint')
            popup('print.aspx?id=' + _id + '&table=' + masterTable + appendTime());
        if (_Context.success) {
            var title = $('.popupTitle').html();
            $('.popupTitle').html(Lang.Saved).css('background-color', '#5cb85c');
            // todo: this line has bugs in new jquery :(
            //$('.popupTitle').animate({ 'background-color': '#f00' }, 1000, 'linear', function () { $('.popupTitle').animate({ 'background-color': '#00496F' }, 1000, 'linear'); $('.popupTitle').html(title); });
        }
        else
            $('.popupTitle').css('background-color', '#e00');
        return false;
    }
    $('#detailForm').validationEngine('hide');
    return true;
}
// 0 = false
// -1 = true
// >0 = ID
function newMastercallback(v, m, f, masterTable, masterQry, table, updateQry, insertQry, deleteQry, requiredField, form, funcShow, dontReshow, extraQuery) {
    funcShow = funcShow || show;
    var res = newMastercallbackInternal(v, m, f, masterTable, masterQry, table, updateQry, insertQry, deleteQry, requiredField, form, funcShow, extraQuery);
    if (res == 0)
        return false;
    if (res < 0)
        return true;
    if (true || _Context.id == -1) {
        if (!dontReshow)
            funcShow(res, null, function () {
                setTimeout(function () {
                    var title = $('.popupTitle').html();
                    $('.popupTitle').html(Lang.Saved).css('background-color', '#5cb85c');
                    // todo: this line has bugs in new jquery :(
                    //$('.popupTitle').animate({ 'background-color': '#f00' }, 1000, 'linear', function () { $('.popupTitle').animate({ 'background-color': '#00496F' }, 1000, 'linear'); $('.popupTitle').html(title); });
                }, 200);
            });
        return true;
    }
    else
        return false;
}
// 0 = false
// -1 = true
// >0 = ID
function newMastercallbackInternal(v, m, f, masterTable, masterQry, table, updateQry, insertQry, deleteQry, requiredField, form, funcShow, extraQuery) {
    _Context.success = false;
    funcShow = funcShow || show;
    if (v == 'New') {
        funcShow(-1);
        return -1;
    }
    form = form || '#detailForm';
    if (v == 'OK' || v == 'OKPrint') {
        var valid = validateInputs(form, f);
        if (!valid)
            return 0;
        if (_Context.isDemo) {
            alert(Lang.DemoVersionNoSave);
            return 0;
        }
        table = table || _Context.table;
        _Context.resultID = _Context.id;
        var _url = "set.aspx?op=master&mtable=" + masterTable + "&mq=" + masterQry + "&table=" + table + "&uqd=" + updateQry + "&iqd=" + insertQry + "&dqd=" + deleteQry + '&req=' + requiredField;
        if (extraQuery)
            _url += "&extra=" + extraQuery;
        _url += appendTime();

        commaInputsRemove(form);
        monetaryInputsRemove(form);
        percentInputsRemove(form);
        textInputsCommaRemove(form);
        dataString = $(form).serialize();
        textInputsCommaSet(form);
        monetaryInputsSet(form);
        commaInputsSet(form);
        percentInputsSet(form);
        // alert(dataString);
        $.ajax({
            type: "POST",
            url: _url,
            data: dataString,
            async: false,
            success: function (html) {
                if (html.indexOf('Error:') < 0 && html.indexOf('خطا:') < 0) {
                    //message(html);
                    _Context.resultID = parseInt(html, 10);
                    _Context.success = true;
                    //$("#flexi").flexReload();
                    setTimeout(function () { $("#flexi").flexReload(); }, 100);
                }
                else
                    displayError(html);
            },
            error: function () {
                _Context.success = false;
            }
        });
        if (!_Context.success)
            return 0;
        if (v == 'OKPrint')
            popup('print.aspx?id=' + _Context.resultID + '&table=' + masterTable + appendTime());
        if (_Context.success) {
            var title = $('.popupTitle').html();
            $('.popupTitle').html(Lang.Saved).css('background-color', '#5cb85c');
            // todo: this line has bugs in new jquery :(
            //$('.popupTitle').animate({ 'background-color': '#f00' }, 1000, 'linear', function () { $('.popupTitle').animate({ 'background-color': '#00496F' }, 1000, 'linear'); $('.popupTitle').html(title); });
        }
        else
            $('.popupTitle').css('background-color', '#e00');
        return _Context.resultID;
    }
    $(form).validationEngine('hide');
    return -1;
}
// Master + 2 Details
function generalMaster2callback(v, m, f, masterTable, masterQry, table, updateQry, insertQry, deleteQry, requiredField, tables, updateQries, insertQries, deleteQries, requiredFields) {
    if (v == 'New') {
        show(-1);
    }
    if (v == 'OK' || v == 'OKPrint') {
        var valid = validateInputs(null, f);
        //alert(valid);
        if (!valid)
            return false;
        if (_Context.isDemo) {
            alert(Lang.DemoVersionNoSave);
            return false;
        }
        table = table || _Context.table;
        tables = tables || _Context.table;
        var _id = _Context.id;
        var _url = "set.aspx?op=master2&mtable=" + masterTable + "&mq=" + masterQry + "&table1=" + table + "&uqd1=" + updateQry + "&iqd1=" + insertQry + "&dqd1=" + deleteQry + '&req1=' + requiredField + "&table2=" + tables + "&uqd2=" + updateQries + "&iqd2=" + insertQries + "&dqd2=" + deleteQries + '&req2=' + requiredFields + appendTime();
        commaInputsRemove(form);
        monetaryInputsRemove(form);
        percentInputsRemove(form);
        textInputsCommaRemove(form);
        dataString = $(form).serialize();
        textInputsCommaSet(form);
        monetaryInputsSet(form);
        commaInputsSet(form);
        percentInputsSet(form)
        //alert(_url + " --> " + dataString);
        _Context.success = false;
        $.ajax({
            type: "POST",
            url: _url,
            data: dataString,
            async: false,
            success: function (html) {
                if (html.indexOf('Error:') != 0 && html.indexOf('خطا:') != 0) {
                    message(html);
                    if (v == 'OKPrint')
                        _id = parseInt(html, 10);
                    _Context.success = true;
                    $("#flexi").flexReload();
                }
                else
                    displayError(html);
            },
            error: function () {
                _Context.success = false;
            }
        });
        if (v == 'OKPrint')
            popup('print.aspx?id=' + _id + '&table=' + masterTable + appendTime());
        return _Context.success;
    }
    $('#detailForm').validationEngine('hide');
    return true;
}
// Master + n Details
function generalMasterscallback(v, m, f, masterTable, masterQry, tables, updateQueries, insertQuries, deleteQuries, requiredFields, extraQuery, form, funcShow) {
    funcShow = funcShow || show;
    form = form || '#detailForm';
    if (v == 'New') {
        funcShow(-1);
    }
    if (v == 'OK' || v == 'OKPrint') {
        var valid = validateInputs(null, f);
        if (!valid)
            return false;
        if (_Context.isDemo) {
            alert(Lang.DemoVersionNoSave);
            return false;
        }
        var _id = _Context.id;
        var _url = "set.aspx?op=masters&mtable=" + masterTable + "&mq=" + masterQry + "&tables=" + tables + "&uqds=" + updateQueries
            + "&iqds=" + insertQuries + "&dqds=" + deleteQuries + '&reqs=' + requiredFields;
        if (extraQuery)
            _url += "&extra=" + extraQuery;
        _url += appendTime();
        commaInputsRemove(form);
        monetaryInputsRemove(form);
        percentInputsRemove(form);
        textInputsCommaRemove(form);
        dataString = $(form).serialize();
        textInputsCommaSet(form);
        monetaryInputsSet(form);
        commaInputsSet(form);
        percentInputsSet(form);
        //alert(_url + " --> " + dataString);
        _Context.success = false;
        $.ajax({
            type: "POST",
            url: _url,
            data: dataString,
            async: false,
            success: function (html) {
                if (html.indexOf('Error:') != 0 && html.indexOf('خطا:') != 0) {
                    message(html);
                    if (v == 'OKPrint')
                        _id = parseInt(html, 10);
                    _Context.success = true;
                    $("#flexi").flexReload();
                }
                else
                    displayError(html);
            },
            error: function () {
                _Context.success = false;
            }
        });
        if (v == 'OKPrint')
            popup('print.aspx?id=' + _id + '&table=' + masterTable + appendTime());
        return _Context.success;
    }
    $('#detailForm').validationEngine('hide');
    return true;
}

function newMasterscallback(v, m, f, masterTable, masterQry, tables, updateQueries, insertQuries, deleteQuries, requiredFields, extraQuery, form, funcShow) {
    funcShow = funcShow || show;
    var res = newMasterscallbackInternal(v, m, f, masterTable, masterQry, tables, updateQueries, insertQuries, deleteQuries, requiredFields, extraQuery, form, funcShow);
    if (res == 0)
        return false;
    if (res < 0)
        return true;
    if (true || _Context.id == -1) {
        funcShow(res, null, function () {
            setTimeout(function () {
                var title = $('.popupTitle').html();
                $('.popupTitle').html(Lang.Saved).css('background-color', '#5cb85c');
                // todo: this line has bugs in new jquery :(
                //$('.popupTitle').animate({ 'background-color': '#f00' }, 1000, 'linear', function () { $('.popupTitle').animate({ 'background-color': '#00496F' }, 1000, 'linear'); $('.popupTitle').html(title); });
            }, 100);
        });
    }
    else
        return false;
}
// 0 = false
// -1 = true
// >0 = ID
function newMasterscallbackInternal(v, m, f, masterTable, masterQry, tables, updateQueries, insertQuries, deleteQuries, requiredFields, extraQuery, form, funcShow) {
    _Context.success = false;
    funcShow = funcShow || show;
    if (v == 'New') {
        funcShow(-1);
        return -1;
    }
    form = form || '#detailForm';
    if (v == 'OK' || v == 'OKPrint') {
        var valid = validateInputs(form, f);
        if (!valid)
            return 0;
        if (_Context.isDemo) {
            alert(Lang.DemoVersionNoSave);
            return 0;
        }
        _Context.resultID = _Context.id;
        var _url = "set.aspx?op=masters&mtable=" + masterTable + "&mq=" + masterQry + "&tables=" + tables + "&uqds=" + updateQueries
            + "&iqds=" + insertQuries + "&dqds=" + deleteQuries + '&reqs=' + requiredFields;
        if (extraQuery)
            _url += "&extra=" + extraQuery;
        _url += appendTime();
        commaInputsRemove(form);
        monetaryInputsRemove(form);
        percentInputsRemove(form);
        textInputsCommaRemove(form);
        dataString = $(form).serialize();
        textInputsCommaSet(form);
        monetaryInputsSet(form);
        commaInputsSet(form);
        percentInputsSet(form);
        $.ajax({
            type: "POST",
            url: _url,
            data: dataString,
            async: false,
            success: function (html) {
                if (html.indexOf('Error:') != 0 && html.indexOf('خطا:') != 0) {
                    message(html);
                    _Context.resultID = parseInt(html, 10);
                    _Context.success = true;
                    //$("#flexi").flexReload();
                    setTimeout(function () { $("#flexi").flexReload(); }, 100);
                }
                else
                    displayError(html, form);
            },
            error: function () {
                _Context.success = false;
            }
        });
        if (!_Context.success)
            return 0;
        if (v == 'OKPrint')
            popup('print.aspx?id=' + _Context.resultID + '&table=' + masterTable + appendTime());
        if (_Context.success) {
            var title = $('.popupTitle').html();
            $('.popupTitle').html(Lang.Saved).css('background-color', '#5cb85c');
            // todo: this line has bugs in new jquery :(
            //$('.popupTitle').animate({ 'background-color': '#f00' }, 1000, 'linear', function () { $('.popupTitle').animate({ 'background-color': '#00496F' }, 1000, 'linear'); $('.popupTitle').html(title); });
        }
        else
            $('.popupTitle').css('background-color', '#e00');
        return _Context.resultID;
    }
    $('#detailForm').validationEngine('hide');
    return -1;
}
function masterscallbackScalar(v, m, f, masterTable, masterQry, tables, updateQueries, insertQuries, deleteQuries, requiredFields, extraQuery, form) {
    _Context.success = false;
    form = form || '#detailForm';
    var valid = validateInputs(form, f);
    if (!valid)
        return 0;
    _Context.resultID = _Context.id;
    var _url = "set.aspx?op=masters&mtable=" + masterTable + "&mq=" + masterQry + "&tables=" + tables + "&uqds=" + updateQueries
        + "&iqds=" + insertQuries + "&dqds=" + deleteQuries + '&reqs=' + requiredFields;
    if (extraQuery)
        _url += "&extra=" + extraQuery;
    _url += appendTime();
    commaInputsRemove(form);
    monetaryInputsRemove(form);
    percentInputsRemove(form);
    textInputsCommaRemove(form);
    dataString = $(form).serialize();
    textInputsCommaSet(form);
    monetaryInputsSet(form);
    commaInputsSet(form);
    percentInputsSet(form);
    $.ajax({
        type: "POST",
        url: _url,
        data: dataString,
        async: false,
        success: function (html) {
            if (html.indexOf('Error:') != 0 && html.indexOf('خطا:') != 0) {
                message(html);
                _Context.resultID = parseInt(html, 10);
                _Context.success = true;
                $("#flexi").flexReload();
            }
            else
                displayError(html);
        },
        error: function () {
            _Context.success = false;
        }
    });
    if (!_Context.success)
        return 0;
    return _Context.resultID;
}

// n Details
function generalDetailscallback(v, m, f, tables, updateQueries, insertQuries, deleteQuries, requiredFields, extraQuery, form, funcShow) {
    funcShow = funcShow || show;
    form = form || '#detailForm';
    if (v == 'New') {
        funcShow(-1);
    }
    if (v == 'OK' || v == 'OKPrint') {
        var valid = validateInputs(null, f);
        if (!valid)
            return false;
        if (_Context.isDemo) {
            alert(Lang.DemoVersionNoSave);
            return 0;
        }
        var _id = _Context.id;
        var _url = "set.aspx?op=details&tables=" + tables + "&uqds=" + updateQueries + "&iqds=" + insertQuries + "&dqds=" + deleteQuries + '&reqs=' + requiredFields;
        if (extraQuery)
            _url += "&extra=" + extraQuery;
        _url += appendTime();
        commaInputsRemove(form);
        monetaryInputsRemove(form);
        percentInputsRemove(form);
        textInputsCommaRemove(form);
        dataString = $(form).serialize();
        textInputsCommaSet(form);
        monetaryInputsSet(form);
        commaInputsSet(form);
        percentInputsSet(form);
        //alert(_url + " --> " + dataString);
        _Context.success = false;
        $.ajax({
            type: "POST",
            url: _url,
            data: dataString,
            async: false,
            success: function (html) {
                if (html.indexOf('Error:') != 0 && html.indexOf('خطا:') != 0) {
                    message(html);
                    if (v == 'OKPrint')
                        _id = parseInt(html, 10);
                    _Context.success = true;
                    $("#flexi").flexReload();
                }
                else
                    displayError(html);
            },
            error: function () {
                _Context.success = false;
            }
        });
        return _Context.success;
    }
    $('#detailForm').validationEngine('hide');
    return true;
}

function newDetailscallback(v, m, f, tables, updateQueries, insertQuries, deleteQuries, requiredFields, extraQuery, form, funcShow) {
    funcShow = funcShow || show;
    var res = newDetailscallbackInternal(v, m, f, tables, updateQueries, insertQuries, deleteQuries, requiredFields, extraQuery, form, funcShow);
    if (res == 0)
        return false;
    if (res < 0)
        return true;
    if (true || _Context.id == -1) {
        funcShow(res, null, function () {
            setTimeout(function () {
                var title = $('.popupTitle').html();
                $('.popupTitle').html(Lang.Saved).css('background-color', '#5cb85c');
                // todo: this line has bugs in new jquery :(
                //$('.popupTitle').animate({ 'background-color': '#f00' }, 1000, 'linear', function () { $('.popupTitle').animate({ 'background-color': '#00496F' }, 1000, 'linear'); $('.popupTitle').html(title); });
            }, 200);
        });
    }
    else
        return false;
}
// 0 = false
// -1 = true
// >0 = ID
function newDetailscallbackInternal(v, m, f, tables, updateQueries, insertQuries, deleteQuries, requiredFields, extraQuery, form, funcShow) {
    _Context.success = false;
    funcShow = funcShow || show;
    if (v == 'New') {
        funcShow(-1);
        return -1;
    }
    form = form || '#detailForm';
    if (v == 'OK' || v == 'OKPrint') {
        var valid = validateInputs(form, f);
        if (!valid)
            return 0;
        if (_Context.isDemo) {
            alert(Lang.DemoVersionNoSave);
            return 0;
        }
        _Context.resultID = _Context.id;
        var _url = "set.aspx?op=details&tables=" + tables + "&uqds=" + updateQueries+ "&iqds=" + insertQuries + "&dqds=" + deleteQuries + '&reqs=' + requiredFields;
        if (extraQuery)
            _url += "&extra=" + extraQuery;
        _url += appendTime();
        commaInputsRemove(form);
        monetaryInputsRemove(form);
        percentInputsRemove(form);
        textInputsCommaRemove(form);
        dataString = $(form).serialize();
        textInputsCommaSet(form);
        monetaryInputsSet(form);
        commaInputsSet(form);
        percentInputsSet(form);
        $.ajax({
            type: "POST",
            url: _url,
            data: dataString,
            async: false,
            success: function (html) {
                if (html.indexOf('Error:') != 0 && html.indexOf('خطا:') != 0) {
                    message(html);
                    _Context.resultID = parseInt(html, 10);
                    _Context.success = true;
                    //$("#flexi").flexReload();
                    setTimeout(function () { $("#flexi").flexReload(); }, 100);
                }
                else
                    displayError(html, form);
            },
            error: function () {
                _Context.success = false;
            }
        });
        if (!_Context.success)
            return 0;
        if (_Context.success) {
            var title = $('.popupTitle').html();
            $('.popupTitle').html(Lang.Saved).css('background-color', '#5cb85c');
            // todo: this line has bugs in new jquery :(
            //$('.popupTitle').animate({ 'background-color': '#f00' }, 1000, 'linear', function () { $('.popupTitle').animate({ 'background-color': '#00496F' }, 1000, 'linear'); $('.popupTitle').html(title); });
        }
        else
            $('.popupTitle').css('background-color', '#e00');
        return _Context.resultID;
    }
    $('#detailForm').validationEngine('hide');
    return -1;
}

function DetailscallbackScalar(v, m, f, tables, updateQueries, insertQuries, deleteQuries, requiredFields, extraQuery, form) {
    _Context.success = false;
    form = form || '#detailForm';
    var valid = validateInputs(form, f);
    if (!valid)
        return 0;
    _Context.resultID = _Context.id;
    var _url = "set.aspx?op=details&tables=" + tables + "&uqds=" + updateQueries + "&iqds=" + insertQuries + "&dqds=" + deleteQuries + '&reqs=' + requiredFields;
    if (extraQuery)
        _url += "&extra=" + extraQuery;
    _url += appendTime();
    commaInputsRemove(form);
    monetaryInputsRemove(form);
    percentInputsRemove(form);
    textInputsCommaRemove(form);
    dataString = $(form).serialize();
    textInputsCommaSet(form);
    monetaryInputsSet(form);
    commaInputsSet(form);
    percentInputsSet(form);
    $.ajax({
        type: "POST",
        url: _url,
        data: dataString,
        async: false,
        success: function (html) {
            if (html.indexOf('Error:') != 0 && html.indexOf('خطا:') != 0) {
                message(html);
                _Context.resultID = parseInt(html, 10);
                _Context.success = true;
                $("#flexi").flexReload();
            }
            else
                displayError(html);
        },
        error: function () {
            _Context.success = false;
        }
    });
    if (!_Context.success)
        return 0;
    return _Context.resultID;
}

function runQuery(qry, table, more, scalar) {
    table = table || _Context.table;
    if (more && (more.indexOf('&') != 0))
        more = '&' + more;
    var _url = "set.aspx?op=RunQuery&qd=" + qry + "&table=" + table + more + appendTime();
    if (scalar)
        _url = "set.aspx?op=RunScalar&qd=" + qry + "&table=" + table + more + appendTime();
    var postSuccess = false;
    $.ajax({
        type: "POST",
        url: _url,
        async: false,
        success: function (html) {
            if (html.indexOf('Error:') != 0 && html.indexOf('خطا:') != 0) {
                //message(html);
                postSuccess = true;
            }
            else {
                displayError(html);
            }
        },
        error: function () {
            postSuccess = false;
        }
    });
    return postSuccess;
}
function getServerValueFiltered(qry, table, more) {
    table = table || _Context.table;
    var _url = _Context.baseUrl + "get.aspx?op=GetValueFiltered&qd=" + qry + "&table=" + table;
    if (more)
        _url += "&more=" + more;

    var fParams = "";
    $(':input', '#fValues').each(function () {
        // 'q' is for fields, 'w' is more Where (w in queries.xml)
        var fName = '_q_' + $(this).attr('q');
        if ($(this).attr('w'))
            fName = $(this).attr('w');
        var fValue = $(this).val();
        if (fValue && fValue.length > 0 && fValue != -1)
            fParams += "&" + fName + "=" + fValue;
    });
    //alert(fParams);
    if (fParams.length > 0)
        fParams += '&comb=' + $('input:checked', '#fComb').val();
    _url += fParams;
    _url += appendTime();
    //alert(_url);

    var returnValue = null;
    $.ajax({
        type: "POST",
        url: _url,
        async: false,
        success: function (html) {
            //  alert("html-->" + html);
            if (html.indexOf('Error:') != 0 && html.indexOf('خطا:') != 0) {
                //    alert("returnValue" + returnValue);
                returnValue = html;
            }
            else {
                //    alert("returnValue-->" + returnValue);
                //displayError(html);
            }
        },
        error: function () {
            //postSuccess = false;
        }
    });
    return returnValue;
}
function getServerValue(qry, table, more) {
    table = table || _Context.table;
    if (more && (more.indexOf('&') != 0))
        more = '&' + more;
    var _url = _Context.baseUrl + "get.aspx?op=GetValue&qd=" + qry + "&table=" + table + more + appendTime();
    // alert("_url-->" + _url);
    var returnValue = null;
    $.ajax({
        type: "POST",
        url: _url,
        async: false,
        success: function (html) {
            //  alert("html-->" + html);
            if (html.indexOf('Error:') != 0 && html.indexOf('خطا:') != 0) {
                //    alert("returnValue" + returnValue);
                returnValue = html;
            }
            else {
                //    alert("returnValue-->" + returnValue);
                //displayError(html);
            }
        },
        error: function () {
            //postSuccess = false;
        }
    });
    return returnValue;
}
function getFieldValue(table, fieldName, cond) {
    return getServerValue('getfieldval', table, '&FieldName=' + fieldName + '&' + cond);
}
// gets the same Url as autocomplete and returns first row in an array
function getServerValues(_url) {
    var returnValue = null;
    $.ajax({
        // try to leverage ajaxQueue plugin to abort previous requests
        mode: "abort",
        url: _url,
        async: false,
        success: function (data) {
            var rows = data.split("\n");
            for (var i = 0; i < rows.length; i++) {
                var row = $.trim(rows[i]);
                if (row) {
                    returnValue = row.split("|");
                    break;
                }
            }
        }
    });
    return returnValue;
}
// gets the same Url as autocomplete and returns all rows
function getServerRows(_url) {
    var returnValue = null;
    $.ajax({
        // try to leverage ajaxQueue plugin to abort previous requests
        mode: "abort",
        url: _url,
        async: false,
        success: function (data) {
            var rows = data.split("\n");
            for (var i = 0; i < rows.length; i++)
                rows[i] = $.trim(rows[i]);
            returnValue = rows;
        }
    });
    return returnValue;
}
function runCmd(cmd, more) {
    var _url = 'set.aspx?op=' + cmd + appendTime();
    if (more)
        _url += more;
    var postSuccess = false;
    $.ajax({
        type: "GET",
        url: _url,
        async: false,
        success: function (res) {
            if (res.indexOf('Error:') != 0 && html.indexOf('خطا:') != 0) {
                message(html);
                postSuccess = true;
            }
            else {
                displayError(html);
            }
        },
        error: function () {
            postSuccess = false;
        }
    });
    return postSuccess;
}
function fillCombo(combo, url) {
    combo.empty();
    $.ajax({
        url: url,
        type: 'GET',
        async: false,
        success: function (data) {
            var rows = data.split("\n");
            for (var i = 0; i < rows.length; i++) {
                var row = $.trim(rows[i]);
                if (row) {
                    row = row.split("|");
                    if (row.length > 2)
                        combo.append($('<option></option>').val(row[0]).html(row[1] + ' ' + row[2]));
                    else
                        combo.append($('<option></option>').val(row[0]).html(row[1]));
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert('Status=' + xhr.status + ' StatusText=' + xhr.statusText + ' thrownError=' + thrownError);
        }
    });
}

// ------------------------------
// Detail Tables helpers  : started
// ------------------------------
// ID, Name --> ID goes to the next input. it is used with the autocompletes in sub tables
function tableAutoPostPack(event, data, formatted) {
    $(event.target).next().val(data[0]); // ID
}
function formatItemNameCode(data, i, n, value) {
    return data[1] + ' (' + data[2] + ')';
}
function formatTreePath(data, i, n, value) {
    // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due, Nature
    // Nature   0=Owe
    //          1 = Due
    //          2 = Both
    var hasChild = (data[5] != ''),
        balance = (data[6] - data[7]),
        nature = +data[8],
        balanceClass = 'success',
        name = data[1],
        code = data[4];

    if ((nature === 0 && balance < 0) || (nature === 1 && balance > 0)) {
        balanceClass = 'danger';
    }

    if (balance < 0) {
        balance = numberWithCommas(toMonetary(balance) * -1) + ' ' + Lang.Cr;
    } else if (balance > 0) {
        balance = numberWithCommas(toMonetary(balance)) + ' ' + Lang.Li;
    } else {
        balance = numberWithCommas(toMonetary(balance));
    }

    return CompileHB('topic-autocomplete', {
        balance: balance,
        balanceClass: balanceClass,
        name: name,
        code: code
    });
}
function formatTreePath2(data, i, n, value) {
    // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due, TopicTitle, ProgramCode, Nature
    // Nature   0=Owe
    //          1 = Due
    //          2 = Both
    var hasChild = (data[5] != ''),
        balance = (data[6] - data[7]),
        nature = +data[10],
        balanceClass = 'success',
        name = data[1],
        code = data[4];

    if ((nature === 0 && balance < 0) || (nature === 1 && balance > 0)) {
        balanceClass = 'danger';
    }

    if (balance < 0) {
        balance = numberWithCommas(toMonetary(balance) * -1) + ' ' + Lang.Cr;
    } else if (balance > 0) {
        balance = numberWithCommas(toMonetary(balance)) + ' ' + Lang.Li;
    } else {
        balance = numberWithCommas(toMonetary(balance));
    }

    return CompileHB('topic-autocomplete', {
        balance: balance,
        balanceClass: balanceClass,
        name: name,
        code: code
    });
}
function postBackTreePath(event, data, formatted, textInput, hiddenInput) {
    if (data) {
        // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due, TopicTitle, ProgramCode, Nature, EquipmentNeed, CanHaveSubTree
        var hasChild = toBool(data[5]), canHaveSubTree = toBool(data[12]);
        if (hasChild || canHaveSubTree) {
            $(textInput).val('');
            $(hiddenInput).val('');
            if (hasChild)
                alert(Lang.HasChild);
            else
                alert(Lang.HasSubTreeBut);
        }
    }
}
function tableRemoveRow(img, prefix) {
    var id = img.attr('id'); // r_1
    id = id.substring(2); // r_1  -->  1
    $(prefix + id).val(""); // Name
    $(prefix + id).next().val(-1); // ID
    img.parent().parent().hide(); // img --> td --> tr --> hide
}

function autoCompleteMatch(table, qd, textInput, hiddenInput, postback, formatItem, formatResult, more, _autourl, moreUrl, _above) {
    var $input = $(textInput);
    _autourl = _autourl || 'get.aspx?op=Auto&table=' + table + '&qd=' + qd + appendTime();
    if (more)
        _autourl += '&more=' + encodeURIComponent(more);
    if (moreUrl)
        _autourl += "&" + moreUrl;

    $input.autocomplete(_autourl, {
        width: 300,
        minChars: 0,
        matchContains: true,
        mustMatch: true /*true*/,
        above: _above,
        formatItem: formatItem || function (data, i, n, value) {
            $(hiddenInput).val('');
            return data[1];
        },
        formatResult: formatResult || function (data, value) {
            return data[1];
        }
    });
    $input.result(function (event, data, formatted) {
        var hidden = $(hiddenInput);
        hidden.val(data[0]);
        if (postback)
            postback(event, data, formatted, textInput, hiddenInput);
    });

    function autoCompleteBlur() {
        var me = $(this);
        me.visible(function (v) {
            // v means the dropdown is still visible
            if (!v) {
                if (me.val() == '')
                    $(hiddenInput).val('');
            }
        });
    }
    $input.unbind('blur', autoCompleteBlur).blur(autoCompleteBlur);
}
function autoComplete(table, qd, textInput, hiddenInput, postback, formatItem, formatResult, more, _autourl, moreUrl, _above, allowNull) {
    var $input = $(textInput);
    _autourl = _autourl || 'get.aspx?op=Auto&table=' + table + '&qd=' + qd + appendTime();
    if (more)
        _autourl += '&more=' + encodeURIComponent(more);
    if (moreUrl)
        _autourl += "&" + moreUrl;
    if (allowNull)
        _autourl += '&null=true';
    $input.autocomplete(_autourl, {
        minChars: 0,
        matchContains: true,
        mustMatch: false /*true*/,
        above: _above,
        multipleSeparator: ',',
        formatItem: formatItem || function (data, i, n, value) {
            $(hiddenInput).val('');
            return data[1];
        },
        formatResult: formatResult || function (data, value) {
            return data[1];
        }
    });
    $input.result(function (event, data, formatted) {
        var hidden = $(hiddenInput);
        //alert(hidden);
        hidden.val(data[0]);
        //alert(postback);
        if (postback)
            postback(event, data, formatted, textInput, hiddenInput);
    });
    function autoCompleteBlur() {
        var me = $(this);
        me.visible(function (v) {
            // v means the dropdown is still visible
            if (!v) {
                me.search(function (res) { if (!res) me.val(''); });
                if (me.val() == '')
                    $(hiddenInput).val('');
            }
        });
        //me.search(function (res) { if (!res) me.val(''); });
        //if (me.val() == '')
        //    $(hiddenInput).val('');
    }
    $input.unbind('blur', autoCompleteBlur).blur(autoCompleteBlur);
}
function autoCompleteNew(options) {
    var $input = $(options.textInput);
    _autourl = options.autourl || 'get.aspx?op=Auto2&table=' + options.table + '&qd=' + options.qd + appendTime();
    _minChars = options.minChars || 0;
    if (options.more)
        _autourl += '&more=' + encodeURIComponent(options.more);
    if (options.moreUrl)
        _autourl += "&" + options.moreUrl;
    if (options.allowNull)
        _autourl += '&null=true';
    var multi = options.multiple && options.multiple == true;
    $input.autocomplete(_autourl, {
        minChars: _minChars,
        matchContains: true,
        mustMatch: false /*true*/,
        above: options.above,
        multiple: multi,
        multipleSeparator: ',',
        formatItem: options.formatItem || function (data, i, n, value) {
            if (options.hiddenInput)
                $(options.hiddenInput).val('');
            return data[1];
        },
        formatResult: options.formatResult || function (data, value) {
            return data[1];
        }
    });
    $input.result(function (event, data, formatted) {
        if (options.hiddenInput) {
            var hidden = $(options.hiddenInput);
            //alert(hidden);
            hidden.val(data[0]);
        }
        //alert(postback);
        if (options.postback)
            options.postback(event, data, formatted, options.textInput, options.hiddenInput);
    });
    function autoCompleteBlur() {
        var me = $(this);
        //if (typeof options.allowNew === 'undefined')
        //    me.search(function (res) { if (!res) me.val(''); });
        //if (options.hiddenInput && me.val() == '')
        //    $(options.hiddenInput).val('');
        me.visible(function (v) {
            // v means the dropdown is still visible
            if (!v) {
                if (typeof options.allowNew === 'undefined')
                    me.search(function (res) { if (!res) me.val(''); });
                if (options.hiddenInput && me.val() == '')
                    $(options.hiddenInput).val('');
            }
        });
    }
    if(!multi)
    $input.unbind('blur', autoCompleteBlur).blur(autoCompleteBlur);
}
// mustMatch = false
function autoCompleteFree(table, qd, textInput, hiddenInput, postback, formatItem, formatResult, more, _autourl, _width, moreQS, _above) {
    var $input = $(textInput);
    _autourl = _autourl || 'get.aspx?op=Auto&table=' + table + '&qd=' + qd + appendTime();
    if (more)
        _autourl += '&more=' + encodeURIComponent(more);
    if (moreQS)
        _autourl += '&' + moreQS;
    //_width = _width || 300;
    $input.autocomplete(_autourl, {
        width: _width,
        minChars: 0,
        matchContains: true,
        mustMatch: false,
        cache: false,
        above: _above,
        multipleSeparator: ',',
        formatItem: formatItem || function (data, i, n, value) {
            if (hiddenInput)
                $(hiddenInput).val('');
            return data[1];
        },
        formatResult: formatResult || function (data, value) {
            return data[1];
        }
    });
    $input.result(function (event, data, formatted) {
        if (hiddenInput) {
            var hidden = $(hiddenInput);
            hidden.val(data[0]);
        }
        if (postback)
            postback(event, data, formatted, textInput, hiddenInput, $input);
    });

    function autoCompleteBlur() {
        var me = $(this);
        me.visible(function (v) {
            // v means the dropdown is still visible
            if (!v) {
                me.search(function (res) { if (!res) me.val(''); });
                if (me.val() == '')
                    $(hiddenInput).val('');
            }
        });
    }
    $jQuery1_9(textInput).unbind('blur.autoCompleteBlur', autoCompleteBlur);
    $jQuery1_9(textInput).bind('blur.autoCompleteBlur', autoCompleteBlur);
}
function autoCompleteForm(formDeclareItemID, textInput, hiddenInput, postback) {
    _autourl = 'get.aspx?op=AutoForm&id=' + formDeclareItemID + appendTime();

    $(textInput).autocomplete(_autourl, {
        width: 300,
        minChars: 0,
        matchContains: true,
        mustMatch: false,
        formatItem: function (data, i, n, value) {
            return data[1];
        },
        formatResult: function (data, value) {
            return data[1];
        }
    });
    $(textInput).result(function (event, data, formatted) {
        var hidden = $(hiddenInput);
        //alert(hidden);
        hidden.val(data[0]);
        //alert(postback);
        if (postback)
            postback(event, data, formatted, textInput, hiddenInput);
    });
}
function setDefault(table, qd, textInput, hiddenInput, code) {
    var _autourl = _autourl || 'get.aspx?op=Auto&table=' + table + '&qd=' + qd + appendTime();
    if (code)
        _autourl += '&more=' + encodeURIComponent("Code=N'" + code + "'");

    res = getRowtable(_autourl);
    if (res && res.length > 0) {
        $(hiddenInput).val(res[0][0]);
        $(textInput).val(res[0][1]);
    }
}
function setDefaultByTitle(table, qd, textInput, hiddenInput, title) {
    var _autourl = _autourl || 'get.aspx?op=Auto&table=' + table + '&qd=' + qd + appendTime();
    if (title)
        _autourl += '&q=' + encodeURIComponent(title);
    res = getRowtable(_autourl);
    if (res && res.length > 0) {
        $(hiddenInput).val(res[0][0]);
        $(textInput).val(res[0][1]);
        //   alert($(textInput).val());
        //    alert($(hiddenInput).val());
    }
}
function setDefaultByTitleTree(table, qd, codeInput, titleInput, hiddenInput, title, more) {
    var _autourl = _autourl || 'get.aspx?op=Auto&table=' + table + '&qd=' + qd;
    if (title)
        _autourl += '&q=' + encodeURIComponent(title);
    if (more)
        _autourl += more;
    _autourl += appendTime();
    res = getRowtable(_autourl);
    if (res && res.length > 0) {
        $(hiddenInput).val(res[0][0]);
        $(codeInput).val(res[0][2]);
        $(titleInput).val(res[0][3]);
        $(titleInput).attr('title', res[0][1]);
        //   alert($(textInput).val());
        //    alert($(hiddenInput).val());
    }
}

function dateSelect(selector) {
    $(selector).unbind('keydown');
    $(selector).unbind('dblclick');
    $(selector).attr("autocomplete", "off");
    if (_options.dateType == 'fa') {
        $(selector).keydown(function (e) {
            if (e.keyCode == 40) {
                displayDatePicker2(this);
                $('html').unbind('click');
                $('html').click(function (ee) {
                    if ($(ee.target).closest('.dpTable').length > 0)
                        return;
                    $('.dpDiv').hide();
                    $('html').unbind('click');
                });
            }
        });
        $jQuery1_9(selector).on('taphold', function() {
            displayDatePicker2(this);
            $('html').unbind('click');
            $('html').click(function (ee) {
                if ($(ee.target).closest('.dpTable').length > 0)
                    return;
                $('.dpDiv').hide();
                $('html').unbind('click');
            });
            return false;
        });
        $(selector).dblclick(function (e) {
            displayDatePicker2(this);
            $('html').unbind('click');
            $('html').click(function (ee) {
                if ($(ee.target).closest('.dpTable').length > 0)
                    return;
                $('.dpDiv').hide();
                $('html').unbind('click');
            });
            return false;
        });
    }
    else {
        //$(selector).datepicker({});
        //$(selector).datepicker("option", "dateFormat", "dd/mm/yy");
        $.datepicker.regional['ku'] = { // Default regional settings
            closeText: 'ئەنجام درا', // Display text for close link
            prevText: 'Prev', // Display text for previous month link
            nextText: 'Next', // Display text for next month link
            currentText: 'ئەمڕۆ', // Display text for current month link
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June',
                'July', 'August', 'September', 'October', 'November', 'December'], // Names of months for drop-down and formatting
            monthNamesShort: ['جانوییەر', 'فێبرییەر', 'مارس', 'ئاپریل', 'مەی', 'جوون', 'جولای', 'ئۆگست', 'سێپتەمبەر', 'ئۆکتۆبەر', 'نێڤەمبەر', 'دیسەمبەر'], // For formatting
            dayNames: ['یەکشەنبە', 'دووشەنبە', 'سێشەنبە', 'چوارشەنبە', 'پێنجشەنبە', 'هەینی', 'شەنبە'], // For formatting
            dayNamesShort: ['١ش', '٢ش', '٣ش', '٤ش', '٥ش', 'هە', 'ش'], // For formatting
            dayNamesMin: ['١ش', '٢ش', '٣ش', '٤ش', '٥ش', 'هە', 'ش'], // Column headings for days starting at Sunday
            weekHeader: 'Wk', // Column header for week of the year
            dateFormat: 'dd/mm/yy', // See format options on parseDate
            firstDay: 0, // The first day of the week, Sun = 0, Mon = 1, ...
            isRTL: false, // True if right-to-left language, false if left-to-right
            showMonthAfterYear: false, // True if the year select precedes month, false for month then year
            yearSuffix: '' // Additional text to append to the year in the month headers
        };
        $.datepicker.setDefaults($.datepicker.regional['ku']);
        $(selector).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            showButtonPanel: true,
            //buttonImage: buttonCalendar,
            buttonImageOnly: true,
            showOn: "dblclick"
        });
        $(selector).dblclick(function (e) {
            //$(this).next().click();
            $(this).datepicker('show');
        });
    }
}

function displayRadiologyStatus(celDiv, id) {
    //0:New,1:Paid,2:Canceled
    var status = parseInt($(celDiv).html(), 0);
    if (status == 0) {
        $(celDiv).html(Lang.SNew);
        $(celDiv).addClass("cfs0");
    }
    else if (status == 1) {
        $(celDiv).html(Lang.Paid);
        $(celDiv).addClass("cfs1");
    }
    else {
        $(celDiv).html(Lang.UnknownError);
        $(celDiv).addClass("unknown");
    }
}
function displayServiceStatus(celDiv, id) {
    //0:New,1:Paid
    var status = parseInt($(celDiv).html(), 0);
    if (status == 0) {
        $(celDiv).html(Lang.SNew);
        $(celDiv).addClass("cfs0");
    }
    else if (status == 1) {
        $(celDiv).html(Lang.Confirmed);
        $(celDiv).addClass("cfs1");
    }
    else {
        $(celDiv).html(Lang.UnknownError);
        $(celDiv).addClass("unknown");
    }
}
function displayTestStatus(celDiv, id) {
    //0:New;1:Performing;2:Canceled;3:Confirmed
    var _status = parseInt($(celDiv).html(), 0);
    if (_status == 0) {
        $(celDiv).html(Lang.NotVerified);
        $(celDiv).addClass("cfs0");
    }
    else if (_status == 1) {
        $(celDiv).html(Lang.WFStatusInProgress);
        $(celDiv).addClass("cfs1");
    }
    else if (_status == 2) {
        $(celDiv).html(Lang.FSTCanceled);
        $(celDiv).addClass("cfs2");
    }
    else if (_status == 3) {
        $(celDiv).html(Lang.Confirmed);
        $(celDiv).addClass("cfs3");
    }
    else {
        $(celDiv).html(Lang.UnknownError);
        $(celDiv).addClass("unknown");
    }
}
function displayTestTestStatus(celDiv, id) {
    //0:New;1:Uncomplete;2:Complete
    var status = parseInt($(celDiv).html(), 0);
    if (status == 0) {
        $(celDiv).html("");
        $(celDiv).addClass("cfs0");
    }
    else if (status == 1) {
        $(celDiv).html(Lang.PSSomePaid);
        $(celDiv).addClass("cfs1");
    }
    else if (status == 2) {
        $(celDiv).html(Lang.FSTComplete);
        $(celDiv).addClass("cfs2");
    }
    else {
        $(celDiv).html(Lang.UnknownError);
        $(celDiv).addClass("unknown");
    }
}
//function displayTopicNature(celDiv, id) {
//    //0=Income;1=Expense;2=Both
//    var _nature = parseInt($(celDiv).html(), 0);
//    if (_nature == 0) {
//        $(celDiv).html(Lang.NatureDue);
//        $(celDiv).addClass("cfs0");
//    }
//    else if (_nature == 1) {
//        $(celDiv).html(Lang.NatureOwe);
//        $(celDiv).addClass("cfs1");
//    }
//    else if (_nature == 2) {
//        $(celDiv).html(Lang.NatureBoth);
//        $(celDiv).addClass("cfs2");
//    }
//    else {
//        $(celDiv).html(Lang.UnknownError);
//        $(celDiv).addClass("unknown");
//    }
//}
function displayServiceTypeType(type) {
    //0:Other;1:Drug;2:Equipment;3:Test;4:RadiologyService
    if (type == '0')
        return Lang.SOther;
    else if (type == '1')
        return Lang.Drug;
    else if (type == '2')
        return Lang.ConsumedMaterial;
    else if (type == '3')
        return Lang.Test;
    else if (type == '4')
        return Lang.RadiologyService;
    else if (type == '5')
        return Lang.Surgery;
    else if (type == '6')
        return Lang.Panel;
    else if (type == '7')
        return Lang.ServiceWithForm;
    else if (type == '8')
        return Lang.MClinic;
    return Lang.StatusUnknown + type;
}
function displayBedStatus(status) {
    //1:Occupied,2:Ready,3:OutOfService,4:Reserved,5:Moving,6:Releasing
    if (status == 1)
        return Lang.BedOccupied;
    if (status == 2)
        return Lang.BedAvailable;
    if (status == 3)
        return Lang.BedBad;
    if (status == 4)
        return Lang.BedReserved;
    if (status == 5)
        return Lang.InTransition;
    if (status == 6)
        return Lang.TheDischarge;

    return "";
}
function displayLang(celDiv, id) {
    var word = $(celDiv).html();
    if (Lang[word])
        $(celDiv).html(Lang[word]);
}

function getStatus(_class, startIndex) {
    if (!_class)
        return -1;
    if (!startIndex) startIndex = 2;
    var status = parseInt(_class.substring(startIndex), 10);
    return status;
}
function getStatuses(_classes, startIndex) {
    var res = [];
    if (!_classes || _classes.length == 0)
        return res;
    if (!startIndex) startIndex = 2;
    for (var i = 0; i < _classes.length; i++)
        res[res.length] = parseInt(_classes[i].substring(startIndex), 10);
    //alert(_classes[res.length - 1] + " --> " + res[res.length - 1]);
    return res;
}
//-----checkNationalcode--------------
function isValidNationalCode(nc) {
    if (!nc || nc.length != 10)
        return false;
    if (nc == '1111111111' || nc == '0000000000'
        || nc == '2222222222' || nc == '3333333333'
        || nc == '4444444444' || nc == '5555555555'
        || nc == '6666666666' || nc == '7777777777'
        || nc == '8888888888' || nc == '9999999999')
        return false;
    var c = parseInt(nc.charAt(9));
    var n = parseInt(nc.charAt(0)) * 10
        + parseInt(nc.charAt(1)) * 9
        + parseInt(nc.charAt(2)) * 8
        + parseInt(nc.charAt(3)) * 7
        + parseInt(nc.charAt(4)) * 6
        + parseInt(nc.charAt(5)) * 5
        + parseInt(nc.charAt(6)) * 4
        + parseInt(nc.charAt(7)) * 3
        + parseInt(nc.charAt(8)) * 2;
    var r1 = parseInt(n % 11);
    //    alert(11 - r1);
    //    alert(c);
    if (r1 < 2) {
        if (c == r1)
            return true;
    }
    else if ((11 - r1) == c)
        return true;
    return false;
}

function showColor(celDiv, id) {
    $(celDiv).css('background-color', '#' + celDiv.innerText);
}

function getEnumTitle(enumTable, id) {
    var _url = "get.aspx?op=GetEnumTitle&enumTable=" + enumTable + "&id=" + id;
    var _res = "";
    $.ajax({
        url: _url,
        type: 'GET',
        async: false,
        success: function (html) {
            _res = html;
        },
        error: ajaxError
    });
    return _res;
}

$(function () {
    shortcut.add("Insert", function () {
        if ($(".jqibuttons").find(":button[value='Cancel']").length)
            return; // schon offen
        if (show) {
            show(-1);
        }
    });
    shortcut.add("F2", function () {
        var focused = $(':focus');
        focused.blur();
        if ($(".jqibuttons").find(":button[value='OK']").length == 1)
            $(".jqibuttons").find(":button[value='OK']").click();
        else
            $(".jqibuttons:last").find(":button[value='OK']").click();
    });
    shortcut.add("ESC", function () {
        // close Datepicker
        if ($('.dpTodayButton').last().is(':visible'))
            $('.dpTodayButton').last().click();
            // second _CANCEL_
        else if ($(".jqibuttons").find(":button[value='_CANCEL_']").length == 1) {
            $(".jqibuttons").find(":button[value='_CANCEL_']").click();
        }
            // support second form: use .last() to click the top one
        else if ($(".jqibuttons").find(":button[value='Cancel']").length > 0) {
            $(".jqibuttons").find(":button[value='Cancel']").last().click();
        }
        $('#filterDiv').hide();
    });

    shortcut.add("ctrl+down", function () {
        $("img.add").click();
    });
    shortcut.add("ctrl+p", function () {
        if ($(".jqibuttons").find(":button[value='Print']").length > 0)
            $(".jqibuttons").find(":button[value='Print']").last().click();
        else
            $('span.print').click()
    });
    shortcut.add("alt+n", function () {
        if ($(".jqibuttons").find(":button[value='New']").length > 0)
            $(".jqibuttons").find(":button[value='New']").last().click();
        else
            $('span.new').click()
    });
    shortcut.add("ctrl+delete", function () {
        if (document.activeElement) {
            var tr = $(document.activeElement).closest('tr');
            var remove = tr.find('.remove');
            if (remove.length > 0) {
                var all = tr.parent().children();
                var index = all.index(tr);
                if (index < all.length - 1)
                    tr.next().find(':text').first().focus();
                else if (index > 0)
                    tr.prev().find(':text').first().focus();
                remove.click();
            }
        }
        //if ($(".jqibuttons").find(":button[value='New']").length > 0)
        //    $(".jqibuttons").find(":button[value='New']").last().click();
        //else
        //    $('span.new').click()
    });
    $(document).keydown(function (e) {
        //alert(e.keyCode);
        var elem = e.target.nodeName.toLowerCase();
        if (e.keyCode === 13 && elem !== "textarea" &&
            !($(e.target).parent().hasClass("chosen-search") && $(e.target).parent().parent().parent().hasClass('chosen-with-drop'))) {
            if (elem == 'input' || elem == 'textarea' || elem == 'select' || elem == "button") {
                if ($(e.target).is(':button')) {
                    if (e.altKey) {
                        $(e.target).click();
                        return;
                    }
                }
                e.preventDefault();
                var $inp = $(':input:visible:not([readonly])');
                var len = $inp.length;
                var nxtIdx = $inp.index(e.target) + 1;
                if ($(e.target).hasClass("no-keydown") && !e.ctrlKey) {
                    $(e.target).blur();
                    var table = $(e.target).parent().parent().parent().parent();
                    $('img.add', table).click();
                    return;
                }
                while (nxtIdx < len) {
                    var next = $(":input:visible:not([readonly]):eq(" + nxtIdx + ")")[0];
                    if (next.type !== "hidden" && $(next).is(':visible') && !$(next).parent().hasClass('hidden') && !$(next).is(':disabled')) {
                        $(e.target).trigger("blur");
                        $(":input:visible:not([readonly]):eq(" + nxtIdx + ")").focus().select();
                        return;
                    }
                    nxtIdx++;
                }
            }
        }
    });
});

// -----------------------------------
// support filter
// 91-06-27
// -----------------------------------
$(function () {
    var preventFilterHide = false;
    $('#filterDiv').click(function () {
        event.stopPropagation();
    });

    $("#filter,.popup-options-icon-filters").click(function () {
        $('html').unbind('click');
        var offset = $("#filter").offset();
        $('#filterDiv').show();
        if ($('.fButtons a').length < 3)
            $('.fButtons').append("<a id='fClose' href='#'>" + Lang.FClose + "</a>");
        $('#fClose').click(function () {
            $('#filterDiv').hide();
            return false;
        });
        $('#filterDiv').offset({ top: offset.top + 25, left: offset.left - 85 }); //
        $('html').click(function () {
            if (!preventFilterHide)
            $('#filterDiv').hide();
        });
        $("#filterDiv :input:visible:enabled:first").focus();
        return false;
    });
    //$("#filterDiv").keydown(function (e) {
    //    $('#fRefresh').click();
    //    if (e.keyCode === 13) {
    //        //$('#fRefresh').click();
    //        event.stopPropagation();
    //    }
    //});
    $("#filterDiv").keyup(function (e) {
        $('#fRefresh').click();
        if (e.keyCode === 13) {
            //$('#fRefresh').click();
            event.stopPropagation();
        }
    });
    $('#fRefresh').click(function () {
        // ex: [{ name: 'Title', value: 'ن' }, { name: 'comb', value: 'and'}]
        var fParams = [];
        var vonVal = "";
        var bisVal = "";
        var bisId = "";
        $(':input', '#fValues').each(function () {
            // key1 ---> for search a single item in von-bis
            var fValue = "";
            if ($(this).attr('q')) {
                if ($(this).attr('q').search('_von') > 0) {
                    vonVal = $(this).val();
                    bisId = ($(this).attr('id')).replace('von', 'bis').replace('Von', 'Bis');
                }
                if ($(this).attr('q').search('_bis') > 0 && $(this).attr('id') == bisId) {
                    bisVal = $(this).val();
                    if (!bisVal && vonVal) {
                        fValue = vonVal;
                    }
                }
            }
            if (!fValue) {
                    fValue = $(this).val();
                }
            // end key1
            // 'q' is for fields, 'w' is more Where (w in queries.xml)
            var fName = '_q_' + $(this).attr('q');
            if ($(this).attr('w'))
                fName = $(this).attr('w');
            if ($(this).is(':radio')) {
                if (!$(this).is(':checked')) {
                    fValue = '';
                }
            }
            if (fValue && fValue.length > 0 && fValue != -1)
                fParams[fParams.length] = { name: fName, value: fValue };
        });
        //alert($('input:checked', '#fComb').val());
        if (fParams.length > 0) {
            $('#filter').html(Lang.FFilter + ' (' + fParams.length + ')');
            fParams[fParams.length] = { name: 'comb', value: $('input:checked', '#fComb').val() };
            $(".popup-options-icon-filters").addClass('filterchecked');
        }
        else {
            $('#filter').html(Lang.FFilter);
            $(".popup-options-icon-filters").removeClass('filterchecked');
        }
        //alert(fParams);
        $("#flexi").flexOptions({ params: fParams });
        preventFilterHide = true;
        $(".flexigrid .pFirst").click();
        preventFilterHide = false;
        $("#flexi").flexReload();
        if (_Context.filterPostBack)
            _Context.filterPostBack();
        return false;
    });
    $('#fClear').click(function () {
        $(':input', '#fValues').val('');
        $("#flexi").flexOptions({ params: null });
        $("#flexi").flexReload();
        if (_Context.filterPostBack)
            _Context.filterPostBack();
        $('#filterDiv').hide();
        $('#filter').html(Lang.FFilter);
        $(".popup-options-icon-filters").removeClass('filterchecked');
        return false;
    });
});

function uploadify(src, dest, send, folder, postback, width, selectPostBack, namePrefix) {
    width = width || "";
    namePrefix = namePrefix || "";
    $(send).hide();
    $(src).uploadify({
        'swf': 'js/uploadify.swf',
        'buttonText': Lang.ChooseFile,
        'uploader': 'Upload.ashx',
        'formData': { 'folder': folder, 'width': width, 'namePrefix': namePrefix },
        'fileTypeDesc': Lang.ChooseYourFile,
        'fileTypeExts': '*.jpg;*.jpeg;*.gif;*.png;*.doc;*.docx;*.pdf;*.zip;*.rar;*.xlsx;*.xls;*.avi;*.ppt;*.pptx',
        'multi': false,
        'auto': false,
        onUploadSuccess: function (file, data, response) {
            message(Lang.SFile + '[' + file.name + ']  ' + Lang.SavedSuccessfully);
            $(dest).val(namePrefix + file.name);
            $(send).hide();
        },
        onUploadComplete: function (file) {
            //alert('The file ' + file.name + ' was successfully uploaded with a response of ' + response + ':' + data);
            //$(dest).val(file.name);
            $(send).hide();
            if (postback)
                postback(namePrefix + file.name);
        },
        onUploadError: function (file, errorCode, errorMsg, errorString) {
            displayError(Lang.ErrorWhileSaving + ' [' + file.name + ']: ' + errorString);
        },
        onSelect: function (file) {
            $(send).show();
            if (selectPostBack)
                selectPostBack(file);
            //alert('The file ' + file.name + ' was added to the queue.');
        }
    });
    $(send).click(function () {
        $(src).uploadify('upload', '*');
    });
}
function uploadifyMulti(src, dest, send, folder, postback, width, selectPostBack) {
    width = width || "";
    $(send).hide();
    $(dest).val('');
    $(src).uploadify({
        'swf': 'js/uploadify.swf',
        'buttonText': Lang.ChooseFile,
        'uploader': 'Upload.ashx',
        'formData': { 'folder': folder, 'width': width },
        'fileTypeDesc': Lang.ChooseYourFile,
        'fileTypeExts': '*.jpg;*.jpeg;*.gif;*.png;*.doc;*.docx;*.pdf;*.zip;*.rar;*.xlsx;*.xls;*.txt;*.ppt;*.pptx',
        'multi': true,
        'auto': true,
        onUploadSuccess: function (file, data, response) {
            message(Lang.SFile + '[' + file.name + ']  ' + Lang.SavedSuccessfully);
            $(dest).val(file.name + ',' + $(dest).val());
            //$(send).hide();
        },
        onUploadComplete: function (file) {
            //alert('The file ' + file.name + ' was successfully uploaded with a response of ' + response + ':' + data);
            //$(dest).val(file.name);
            $(send).hide();
            if (postback)
                postback(file.name);
        },
        onUploadError: function (file, errorCode, errorMsg, errorString) {
            displayError(Lang.ErrorWhileSaving + ' [' + file.name + ']: ' + errorString);
        },
        onSelect: function (file) {
            $(send).show();
            if (selectPostBack)
                selectPostBack(file);
            //alert('The file ' + file.name + ' was added to the queue.');
        }
    });
    $(send).click(function () {
        $(src).uploadify('upload', '*');
    });
}
function getAccessID(link, field) {
    var mores = "";
    var userID = _Context.userID;
    var more = "&UserID=" + userID;
    var access = getServerValue("selectuserrole", "Roles", more);
    if (access != null) {
        var values = access.split(";");
        if (values.length > 3) {
            var IDs = new Array();
            var k = 0;
            for (var i = 0; i < values.length; i++) {
                var v = values[i].split("_");
                for (var j = 0; j < v.length - 1; j++) {

                    if (v[j] == link) {
                        IDs[k] = v[j + 1];
                        k++;
                    }
                }
            }
            if (IDs.length == 0)
                return mores;

            var ID = "";
            for (var i = 0; i < IDs.length - 1; i++) {
                ID += IDs[i] + ",";
            }
            ID += IDs[IDs.length - 1];
            mores = encodeURIComponent(field + " in(" + ID + ")");
        }
    }
    return mores;
}
function taskDefinition(celDiv, id) {
    $(celDiv).html('<a href="list.aspx?table=Task&id=' + id + '"><img src="img/task_icon.jpg" /></a>');
}
function transitionDefinition(celDiv, id) {
    $(celDiv).html('<a href="list.aspx?table=Transition&id=' + id + '"><img src="img/transition_icon.jpg" /></a>');
}


/* ------------------------------------- */
function appendToCombo(combo, val, text) {
    $(combo).append($('<option></option>').val(val).html(text));
}

function showPopup(_url, _callback, _buttons, _top, _successCallback, showHints) {

    if (showWaiting)
        return;
    showWaiting = true;
    _buttons = _buttons || getDefaultButtons();
    if (_callback == null)
        _callback = function () { return true; };
    _top = _top || '1%';
    $.ajax({
        url: _url,
        type: 'GET',
        success: function (html) {
            showWaiting = false;
            $.prompt(html, {
                submit: _callback,
                buttons: _buttons,
                focus: 0,
                top: _top
            });

            if (_Context.monetaryUnitAbbreviation != '' &&
                _Context.monetaryUnitAbbreviation != undefined &&
                _Context.monetaryUnitAbbreviation != null) {
                if ($('.popupTitle')[0]) {
                    if ($('.popupTitle')[0].innerHTML.toString().indexOf(_Context.monetaryUnitAbbreviation) < 0)
                        $('.popupTitle')[0].append(' ( ' + _Context.monetaryUnitAbbreviation + ' )');
                }
                if ($('.popupTitle')[1]) {
                    if ($('.popupTitle')[1].innerHTML.toString().indexOf(_Context.monetaryUnitAbbreviation) < 0)
                        $('.popupTitle')[1].append(' ( ' + _Context.monetaryUnitAbbreviation + ' )');
                }
            }
            //dateSelect
            if (_options.dateType != "fa" && ($("#detailForm :input:not([readonly]):visible:enabled:first").hasClass('dateSelect')
                || $("#subDetailForm :input:not([readonly]):visible:enabled:first").hasClass('dateSelect')))
            {
                $("#subDetailForm :input:not([readonly]):visible:enabled:nth-child(2)").focus();
                $("#detailForm :input:not([readonly]):visible:enabled:nth-child(2)").focus();
            }
            else
            {
                $("#subDetailForm :input:not([readonly]):visible:enabled:first").focus();
                $("#detailForm :input:not([readonly]):visible:enabled:first").focus();
            }
            if (_successCallback)
                _successCallback();
            if (showHints) {
                var programID = toInt($('.Program').val(), -1);
                if (programID != -1)
                    $("#program-box-container").append('<div class="l program-box">' + Lang.IndependentAccount + ': ' + $('.Program: option:selected').text() + "</div>");
            }
        },
        error: ajaxError
    });
}
function fillExcel(table, qd, more, topRows) {
    var _url = "get.aspx?op=GetExcel&table=" + table + "&qd=" + qd;
    if (more)
    {
        if (more.charAt(0) != '&')
            _url += '&';
        _url += more;
    }
    if (topRows)
        _url += '&toprows=' + encodeURIComponent(topRows);
    document.location = _url;
}
function fillTable(_url, table, addRow) {
    var items = null;
    $.ajax({
        url: _url,
        type: 'GET',
        dataType: 'xml',
        async: false,
        success: function(xmlDoc) {
            var xNode = (xmlDoc.firstChild.nextSibling) ? xmlDoc.firstChild.nextSibling : xmlDoc.firstChild;
            items = xNode.childNodes[0];
            if (addRow)
                $(items).find("R").each(function () {
                    var row = addRow($(this));
                    $(table).append(row);
                });
        },
        error: ajaxError
    });
    return items;
}
function iterateRows(_url, callback) {
    var items = null;
    $.ajax({
        url: _url,
        type: 'GET',
        dataType: 'xml',
        async: false,
        success: function (xmlDoc) {
            var xNode = (xmlDoc.firstChild.nextSibling) ? xmlDoc.firstChild.nextSibling : xmlDoc.firstChild;
            items = xNode.childNodes[0];
            if (callback)
                $(items).find("R").each(function () {
                    callback($(this));
                });
        },
        error: ajaxError
    });
    return items;
}

function killEvent() {
    event.returnValue = false;
    if (window.event) window.event.cancelBubble = true;
    if (event.preventDefault) event.preventDefault();
    if (event.stopPropagation) event.stopPropagation();
}
function SMS(v, m, f, recipient, message, fileID, form) {

    form = form || '#detailForm';
    var _url = "set.aspx?op=sms&recipient=" + recipient + "&message=" + message + "&fileID=" + fileID + appendTime();
    //alert(_url + " --> " + dataString);
    var postSuccess = false;
    $.ajax({
        type: "POST",
        url: _url,
        async: false
    });

    alert("SMS " + Lang.WasSent);
}

function getDefaultButtons(displayPrint, subsystem, form) {
    var btns = {};
    if (typeof displayPrint === 'undefined')
        displayPrint = _Context.id > 0;
    if (typeof subsystem != 'undefined') {
        if (hasAccess(subsystem, form, "edit") && hasGeneralAccess("edit"))
            btns[Lang.OK] = 'OK';
        if (hasAccess(subsystem, form, "print") && hasGeneralAccess("print"))
            if (displayPrint)
                btns[Lang.Print] = 'Print';
        if (hasAccess(subsystem, form, "new") && hasGeneralAccess("new"))
            btns[Lang.GBNew] = 'New';
    }
    else {
        btns[Lang.OK] = 'OK';
        if (displayPrint)
            btns[Lang.Print] = 'Print';
        btns[Lang.GBNew] = 'New';
    }
    btns[Lang.Close] = 'Cancel';
    return btns;
}
function getButtons(hideOK, subsystem, form) {
    var btns = {};
    if (typeof hideOK === 'undefined')
        hideOK = false;
    if (typeof subsystem != 'undefined')
        if (!hasAccess(subsystem, form, "edit") || !hasGeneralAccess("edit"))
            hideOK = true;
    if (hideOK == false)
        btns[Lang.OK] = 'OK';
    btns[Lang.Close] = 'Cancel';
    return btns;
}
function getFactorButtons() {
    var btns = {};
    btns[Lang.Close] = 'Cancel';
    btns[Lang.Payment] = 'pay';
    btns[Lang.Factor] = 'Factor';
}

function getCloseButtons() {
    var btns = {};
    btns[Lang.Close] = 'Cancel';
    return btns;
}
function getCloseAndPrintButtons() {
    var btns = {};
    btns[Lang.Close] = 'Cancel';
    btns[Lang.Print] = 'Print';
    return btns;
}
function getTaskButtons(type, relatedID, hideOK, subsystem, form) {
    var btns = {};
    if (typeof hideOK === 'undefined')
        hideOK = false;
    var items = getItems("gettransitions", 'RelatedID=' + relatedID + "&Type=" + type + '&UserID=' + _Context.userID);
    var numTransitions = 0;
    if (items) {
        $(items).find("R").each(function () {
            var node = $(this);
            var NextTaskID = node.attr("NextTaskID");
            var Name = node.attr("Name");
            btns[Name] = { title: Name, value: NextTaskID, color: node.attr("TransColor") };
            //btns[Name].color = node.attr("TransColor");
            numTransitions++;
        });
    }
    if (typeof subsystem != 'undefined')
        if (!hasAccess(subsystem, form, "edit") || !hasGeneralAccess("edit"))
            hideOK = true;
    if (hideOK == false && (numTransitions > 0 || hasGeneralAccess("alloweditfinalized", generalSettings))) // numTransitions > 0 means is not finalized
        btns[Lang.OK] = 'OK';
    if (typeof subsystem != 'undefined')
        if (hasAccess(subsystem, form, "print") && hasAccess("print") || hasGeneralAccess("print")) {
            var allowPrint = getServerValue("gettaskallowprint", "Task", 'RelatedID=' + relatedID + "&Type=" + type);
            //alert(allowPrint);
            if (allowPrint && allowPrint != 'False')
                btns[Lang.Print] = 'Print';
        }
    //btns[Lang.TaskImpls] = 'TaskImpls';
    if (typeof subsystem != 'undefined'){
        if (hasAccess(subsystem, form, "changes") || hasGeneralAccess("changes"))
            btns[Lang.History] = 'ChangeLog';
    if (hasAccess(subsystem, form, "new") && hasGeneralAccess("new"))
        btns[Lang.GBNew] = 'New';
}
    btns[Lang.Close] = 'Cancel';
    return btns;
}
function getTaskGridButtons(type, doCommand, subsystem, form) {
    var btns = [];
        //[
        //    { name: Lang.GBNew, bclass: 'new', onpress: doCommand, onaccess: hasAccess(subsystem, form, "new") && hasGeneralAccess("new") }
        //];
    var items = getItems("getuserstarttasks", "Type=" + type + '&UserID=' + _Context.userID);
    var numTasks = 0;
    if (items) {
        $(items).find("R").each(function () {
            var node = $(this);
            var id = node.attr("ID");
            var title = node.attr("Title");
            var btnTitle = node.attr("BtnTitle");
            if (!btnTitle)
                btnTitle = title;
            var showIt = true; //getServerValue('hasaccesstoworkflowstarterbutton', 'Task', '&TaskID=' + id + '&UserID=' + _Context.userID + '&Type=' + type);
            if (showIt) {
                btns[numTasks] = { name: btnTitle, bclass: 'new ' + id, onpress: doCommand, onaccess: hasAccess(subsystem, form, "new") && hasGeneralAccess("new") };
                numTasks++;
            }
        });
    }
    if (numTasks == 0) {
        // do not add a 'New' button if it is not allowed by the workflow
        //btns[numTasks] = { name: Lang.GBNew, bclass: 'new', onpress: doCommand, onaccess: hasAccess(subsystem, form, "new") && hasGeneralAccess("new") };
        //numTasks++;
    }
    btns[numTasks + 0] = { name: Lang.GBDel, bclass: 'delete', onpress: doCommand, onaccess: hasAccess(subsystem, form, "delete") && hasGeneralAccess("delete") };
    //btns[numTasks + 1] = { separator: true };
    btns[numTasks + 1] = { name: Lang.GBPrint, bclass: 'print', onpress: doCommand, onaccess: hasAccess(subsystem, form, "print") && hasGeneralAccess("print") };
    btns[numTasks + 2] = { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand, onaccess: hasAccess(subsystem, form, "excel") && hasGeneralAccess("excel") };
    return btns;
}
function showTaskImpls(relatedID, type)
{
    var _url = 'get.aspx?op=GetHtml&table=TaskImpl&view=taskimpls&RelatedID=' + relatedID + "&Type=" + type + appendTime();
    var btns = {};
    btns[Lang.Close] = 'Cancel';
    showPopup(_url, function () { }, btns, null, null, false);
    return true;
}
function showWorkflowTasks(relatedID, type) {
    var _url = "get.aspx?op=GetXml&qd=taskimpls&RelatedID=" + relatedID + "&Type=" + type;
    var states = '';
    var count = 0;
    iterateRows(_url, function (node) {
        var exitDate = getNodeVal(node, 'ExitDate', '');
        var exitUser = getNodeVal(node, 'ExitUserName', '');
        var msg = getNodeVal(node, 'Msg', '');
        var cls = "alert-danger";
        if (exitDate)
            cls = "alert-success";
        var a = "<span title='" + msg + " @ " + exitDate + "' class='" + cls + "'>" + getNodeVal(node, 'TaskName', '') + "</span>";
        states += a;
        if (exitDate)
            states += " -> ";
        count++;
    });
    if (count <= 1)
        return "";
    return states;
}
function showLogChange(tableName, tableID, type) {
    var _url = 'get.aspx?op=GetHtml&table=LogChange&view=logchangestable&TableID=' + tableID + "&TableName=" + tableName + "&Type=" + type + appendTime();
    var btns = {};
    btns[Lang.Close] = 'Cancel';
    showPopup(_url, function () { }, btns, null, null, false);
    return true;
}
// -------------------------------------
// Menus - Navigation  : Started
// -------------------------------------
function selectCurMenu(menu) {
    $(menu).parent().addClass('active');
}
function chooseFloatingMenus(file) {
    var html = "<div class='popupTitle'>" + Lang.ChooseBed + "</div><div class='chooseFM'>" + $('#floatingMenu').find('.body').html() + "</div>";
    $.prompt(html, {
        submit: function (v, m, f) {
            if (v == 'OK') {
                var lis = ",";
                $(':checkbox', '.chooseFM').each(function (index) {
                    if ($(this).attr('checked'))
                        lis += index + ",";
                })
                var _url = "set.aspx?op=RunQuery&qd=configinsertupdate&table=Config" + appendTime();
                $.ajax({
                    type: "POST",
                    url: _url,
                    data: { Name: 'FM' + file + _Context.userID, Value: lis },
                    async: false,
                    success: function (html) {
                        if (html.indexOf('Error:') != 0 && html.indexOf('خطا:') != 0) {
                            message(html);
                        }
                        else {
                            displayError(html);
                        }
                    },
                    error: function () {
                        _Context.success = false;
                    }
                });
            }
        },
        buttons: getButtons(),
        focus: 1
    });
    $('ul', '.chooseFM').removeClass('hidden');
    $('li', '.chooseFM').each(function (index, value) {
        var cid = "lic" + index;
        var li = $(this).html().replace("<a ", "<label for='" + cid + "'").replace("</a>", "</label>");
        $(this).html(li);
        if ($(this).hasClass('hidden'))
            $(this).removeClass('hidden').prepend("<input type='checkbox' id='" + cid + "' />");
        else
            $(this).prepend("<input type='checkbox' id='" + cid + "' checked='checked' />");
    });
    return false;
}
function setClassPatient() {
    selectCurMenu('#patienta');
}
function setClassHospital() {
    selectCurMenu('#hospitala');
}
function setClassClinic() {
    selectCurMenu('#clinica');
}
function setClassParaclinic() {
    selectCurMenu('#paraclinica');
}
function setClassDrug() {
    selectCurMenu('#druga');
}
function setClassIns() {
    selectCurMenu('#insa');
}
function setClassBasic() {
    selectCurMenu('#basicsa');
}
function setClassBank() {
    selectCurMenu('#banka');
}
function setClassReports(path) {
    selectCurMenu('#reportsa');
}
function setClassDistribution(path) {
    selectCurMenu('#dista');

    path = path || '';
    if (!_Context.distribution)
        document.location = path + "welcome.aspx";
}
function setClassAccounting(path) {
    selectCurMenu('#accountinga');
}
function setClassAsset(path) {
    selectCurMenu('#asseta');
}
function setClassPersonnel() {
    selectCurMenu('#personnela');
}
function setClassFood() {
    selectCurMenu('#fooda');
}
function setClassManager() {
    selectCurMenu('#managera');
}
function setClassSys() {
    selectCurMenu('#sysa');
}
function setClassSales() {
    selectCurMenu('#salesa');
}
function setClassBlood() {
    selectCurMenu('#blooda');
}
function setClassService() {
    selectCurMenu('#servicea');
}
function setClassStore() {
    selectCurMenu('#storea');
}
function setClassStoreForms() {
    selectCurMenu('#storeformsa');
}
function setClassReport() {
    selectCurMenu('#reporta');
}
function setClassSalary(path) {
    selectCurMenu('#salarysa');

    path = path || '';
    if (!_Context.salary)
        document.location = path + "welcome.aspx";
}
function setClassProgram() {
    selectCurMenu('#programa');
}
function setClassVisitor(path) {
    selectCurMenu('#visitora');
}
function enableMenu() {
    var access = $(this).attr("access");
    if (access && access.length > 0) {
        var formButton = access.split(",");
        if (formButton.length == 2) {
            //simple version: hospital,release
            if (!hasAccess("link", formButton[0], formButton[1])) {
                //alert(false);
                $(this).attr("href", "#");
                $(this).addClass('disabled');
            }
        }
        else if (formButton.length == 4) {
            //2 access version: hospital,dr,ward,17
            if (!hasAccess("link", formButton[0], formButton[1]) || !hasAccess("link", formButton[2], formButton[3])) {
                //alert(formButton + " --> " + _Context.access);
                $(this).attr("href", "#");
                $(this).addClass('disabled');
            }
        }
    }

    // check level
    var level = $(this).attr("level");
    if (!level)
        level = 1;
    if (level > _Context.level)
        $(this).parent().hide();
}
function checkAccess(accessArr, path) {
    var has = false;
    path = path || '';
    for (var i = 0; i < accessArr.length; i++) {
        var access = accessArr[i];
        var formButton = access.split(",");
        if (formButton.length == 2) {
            //simple version: hospital,release
            has = has || hasAccess("link", formButton[0], formButton[1]);
        }
        else if (formButton.length == 4) {
            //2 access version: hospital,dr,ward,17
            has = has || (hasAccess("link", formButton[0], formButton[1]) && hasAccess("link", formButton[2], formButton[3]));
        }
    }
    if (!has) {
        document.location = path + "welcome.aspx";
        return;
    }
}
function checkLevel(level, path) {
    path = path || '';
    if (level > _Context.level) {
        document.location = path + "welcome.aspx";
        return;
    }
}
// -------------------------------------
// Menus - Navigation  : Ended
// -------------------------------------

function printSubform(form) {
    form = form || '#detailForm';
    var printContent = "<head><title>" + $('.popupTitle').html() + "</title>";
    printContent += "<link href='styles/jquery-ui.css' rel='stylesheet' type='text/css' />";
    printContent += '<link href="styles/bootstrap.css" rel="stylesheet" type="text/css">';
    printContent += '<link href="styles/bootstrap-rtl.css" rel="stylesheet" type="text/css">';
    
    printContent += "<link href='styles/flexigrid.pack.css' rel='stylesheet' type='text/css' />";
    printContent += "<link href='styles/tabs.css' rel='stylesheet' type='text/css' />";
    printContent += "<link href='styles/print5.css' rel='stylesheet' type='text/css' />";
    printContent += "</head><body dir=rtl class=printBody>";
    printContent += $(form).parent().html();
    printContent += "</body>";
    //alert(printContent);
    var windowUrl = 'NewWindow';
    var uniqueName = new Date();
    var windowName = 'Print' + uniqueName.getTime();
    var printWindow = window.open(windowUrl, 'print', 'left=200,top=200');
    printWindow.document.write(printContent);
    // ---------------------------------
    // simplify:
    $(form, printWindow.document).attr('width', '100%');
    $('span.red', printWindow.document).remove(); // remove * : <span class="red">*</span>
    // remove overflow
    $('div', printWindow.document).css('overflow', 'visible').css('overflow-y', 'visible');

    // remove add column
    // <img src="img/add.gif" class="addSpecimen">
    $("img[src='img/add.gif']", printWindow.document).each(function () {
        //var example1ColIndex = cell.parent("tr").children().index(cell);
        var img = $(this);
        var index = img.parent('td').parent().children().index(img.parent('td'));
        var table = img.closest('table');
        $('tr', table).find('td:eq(' + index + '),th:eq(' + index + ')').remove();
    });
    $('input', printWindow.document).unautocomplete();
    $('td', printWindow.document).removeAttr('width');
    $('div', printWindow.document).css('height', 'auto');
    $('select', printWindow.document).each(function () {
        if ($(this).hasClass('noprint')) {
            $(this).outerHtml("<span></span>");
        }
        else {
            if ($(this).attr('id')) {
                var val = $('#' + $(this).attr('id') + " option:selected").text();
                $(this).outerHtml("<span class='val'>" + val + '</span>');
            }
        }
    });
    $(':button', printWindow.document).hide();
    $(':text', printWindow.document).each(function () {
        if ($(this).hasClass('noprint')) {
            $(this).outerHtml("<span></span>");
        }
        else {
            var cls = $(this).attr('class');
            if (cls)
                cls = " " + cls;
            else
                cls = "";
            var val = $(this).val(); // from printWindow
            // the user may have changed the value
            if ($(this).attr('id')) {
                var newVal = $('#' + $(this).attr('id')).val(); // from main window
                if (newVal != '')
                    val = newVal;
            }
            //if ($(this).hasClass('percent'))
            //    alert(val);
            if (($(this).hasClass('percent') || val == '') && $(this).attr('id'))
                val = $('#' + $(this).attr('id')).val(); // from main window
            else if ($(this).hasClass('comma'))
                val = numberWithCommas(val);
            $(this).outerHtml("<span class='val" + cls + "'>" + val + '</span>');
        }
    });
    $('label', printWindow.document).each(function () {
        if ($(this).hasClass('noprint') || $(this).attr('class') == 'noprint')
            $(this).outerHtml("<span class='noprint'></span>");
        else {
            var val = $(this).text();
            if (val == '' || val.indexOf(':') < 0)
                $(this).text(val + ':');
        }
    });
    $(':checkbox', printWindow.document).each(function () {
        if ($(this).attr('id'))
            if ($('#' + $(this).attr('id')).attr('checked')) // from main window
                $(this).attr('checked', true);
    });
    $('.th td', printWindow.document).each(function (index, value) {
        if ($(this).attr('type') == 'hidden')
            return;
        if ($(this).hasClass('noprint') || $(this).attr('class') == 'noprint') {
            hideTr($(this));
        }
    });
    $('.noprint', printWindow.document).hide();
    $('.print', printWindow.document).show();
    printWindow.focus();
    //printWindow.print();
    //printWindow.setTimeout("window.close()", 2000);
}

// -------------------------------------------------------
// used in many forms to insert/update Forms (FormDeclare)
// -------------------------------------------------------
function showFormDeclareCallback(v, m, f, form) {
    if (v != 'OK')
        return true;
    form = form || "#declareForm";
    var valid = validateInputs(form, f);
    if (!valid)
        return false;
    var _url = "set.aspx?op=setform" + appendTime();
    dataString = $(form).serialize();
    //alert(_url + " --> " + dataString);
    var postSuccess = false;
    $.ajax({
        type: "POST",
        url: _url,
        data: dataString,
        async: false,
        success: function (html) {
            if (html.indexOf('Error:') != 0 && html.indexOf('خطا:') != 0) {
                message(html);
                if (v == 'OKPrint')
                    _id = parseInt(html, 10);
                postSuccess = true;
                $("#flexi").flexReload();
            }
            else
                displayError(html);
        },
        error: function () {
            postSuccess = false;
        }
    });
    return postSuccess;
}
function getFlexiOptions(_url, _colModel) {
    return {
        url: _url,
        dataType: 'xml',
        colModel: _colModel,
        sortname: "ID",
        sortorder: "asc",
        usepager: false,
        useRp: false,
        rp: gridRp,
        showTableToggleBtn: false,
        width: gridWidth,
        height: 200,
        singleSelect: true
    }
}
function padLeft(str, len, ch) {
    ch = ch || '0';
    str = str.toString();
    while (str.length < len)
        str = ch + str;
    return str;
}

function printTable(table, path, title, pre, post, headerClass, multiRowHeadr) {
    path = path || '';
    title = '<div class="Title" style="text-align: center">' + (title || '') + '</div>';
    var list = '',
        count,
        header;
    if (pre) {
        list += pre;
    }

    list += title;
    function printTableInternal(f, num) {
        $(table).each(function () {
            count = $('tr', this).length;

            if (count === 1) {
                return;
            }
            header = $('tr', this).first().outerHtml();

            if (headerClass) {
                header = "";
                $(' tr.' + headerClass, this).each(function () {
                    header += $(this).outerHtml();
                });
            }

            list += '<table style="font-size: 0.50em;">' + header;

            var lineNo = 0, first = true;
            $('tr', this).each(function () {
                if (!$(this).hasClass('hidden')) {
             if (headerClass && $(this).hasClass(headerClass))
                    return;
                if (first && !headerClass) {
                    first = false;
                    return;
                }
                if (lineNo > 0 && (lineNo % rowsPerPage == 0)) {
                    //start new page
                    list += '</table><table style="font-size: 0.50em;">' + header;
                }
                list += $(this).outerHtml();
                lineNo++;
                }
            });
            list += '</table>';
        });

        if (post) {
            list += post;
        }

        var newWin = window.open(this.href);
        newWin.document.write('<html lang="fa" dir="rtl">');
        newWin.document.write('<head>');
        newWin.document.write('<link href="' + path + 'styles/beauty.css" rel="stylesheet" type="text/css">');
        newWin.document.write('<link href="' + path + 'styles/utility.css" rel="stylesheet" type="text/css">');
        newWin.document.write('<script src="' + path + 'js/jquery-1.4.1-vsdoc.js" language="javascript" type="text/javascript" ></script>');
        newWin.document.write('<script src="' + path + 'js/plugins.js" language="javascript" type="text/javascript"></script>');
        newWin.document.write('<script src="' + path + 'js/JsFarsiCalendar.js" type="text/javascript" language="JavaScript"></script>');
        newWin.document.write('<script src="' + path + 'js/site.js" language="javascript" type="text/javascript" ></script>');
        newWin.document.write('</head>');
        newWin.document.write('<body>');
        newWin.document.write(list);
        newWin.document.write('</body>');
        newWin.document.write('</html>');
        newWin.document.write("<script language='javascript' type='text/javascript'> var inputs = document.getElementsByTagName('input');"
        + "for (var i = 0; i < inputs.length; ++i) { inputs[i].value = numberWithCommas(stripCommas(inputs[i].value));  inputs[i].setAttribute('style','font-family: peyvand,tahoma;')"
        + "};</script>");
        if (f) {
            setTimeout(
                function () {
                    for (var i = 0; i < num; i++) {
                        if (f['colTh' + i] != 1)
                        {
                            $('table tbody tr', newWin.document).each(function () {
                                $(this).find("td").eq(i).hide();
                                $(this).find("th").eq(i).hide();                               
                                
                            });
                            $('.GridHeader', newWin.document).each(function () {
                                $(this).find("th").eq(i).hide();

                            });
                        }
                        //alert(f['colTh' + i] != 1);
                    }
                }, 1000);
        }
    }
    if ($(table).length == 1 && !multiRowHeadr) {

        var colSelection = '', thIndex = 0, num = 0;
        $('th', table).each(function () {
            var text = $(this).html();
            if ($('a', this).length == 1)
                text = $('a', this).html();

            colSelection += "<div class='col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container check'>"
                        + "<div class='form-group'>"
                        + "    <label for='colTh" + thIndex + "' class='left-aligned-label col-xs-12 col-sm-4'>" + text + "</label>"
                        + "    <div class='col-xs-12 col-sm-8 form-col-without-padding'>"
                        + "        <input type='checkbox' value='1' id='colTh" + thIndex + "' name='colTh" + thIndex + "' class='form-control input-sm ac_input' checked='checked' />"
                        + "    </div></div></div>";
            // colSelection += "<label>" + _colModel[i].display + "<input type='checkbox' value='1' id='colTh" + thIndex + "' name='colTh" + thIndex + "' checked='checked' /></label>";
            thIndex++;
            num++;
        });

        colSelection = "<div class='container'><div class='row'><div class='colSelection'>" + Lang.ChoosePrintColumns + '<br />' + colSelection + "</div></div></div>";

        doConfirm(function (v, m, f) {
            printTableInternal(f, num);
        }, colSelection, null, null, true);
    }
    else
        printTableInternal();
    
    //newWin.print();
    return;
}
//14-09-92

function getItems(qd, more) {
    var _url = "get.aspx?op=GetXml&qd=" + qd;
    if (more)
        _url += "&" + more;
    var items = null;
    $.ajax({
        url: _url,
        type: 'GET',
        dataType: 'xml',
        async: false,
        success: function (xmlDoc) {
            var xNode = (xmlDoc.firstChild.nextSibling) ? xmlDoc.firstChild.nextSibling : xmlDoc.firstChild;
            items = xNode.childNodes[0];
        },
        error: ajaxError
    });
    return items;
}


function getTerminology(table) {
    table = table.toLowerCase();
    var all = ["Country", "ISO_3166-1"
, "Province", "countryDivisions"
, "City", "countryDivisions"
, "InsuranceOrganization", "thritaEHR.organizationType"
, "Insurance", "thritaEHR.insuranceBox"
, "Ward", "thritaEHR.ward"
, "WardType", "thritaEHR.ward"
, "KType", "thritaEHR.kType"
, "Specialty", "thritaEHR.specialty"
, "Marital", "thritaEHR.maritalStatus"
, "Gender", "thritaEHR.gender"
, "AdmissionType", "thritaEHR.admissionType"
, "ConditionOnDischarge", "thritaEHR.conditionOnDischarge"
, "ServiceType", "thritaEHR.serviceType"
, "MedicalRecordType", "thritaEHR.medicalRecordType"
, "LocationType", "thritaEHR.accidentLocation"
, "InjuredStatus", "thritaEHR.injuredStatus"
, "InjuryType", "thritaEHR.injuryType"
, "EducationLevel", "thritaEHR.educationLevel"
, "DeathLocation", "thritaEHR.deathLocation"
, "VehicleType", "thritaEHR.vehicleType"
, "_____", "thritaEHR.healthcareProvider.role"
, "DiagnosisStatus", "thritaEHR.daignosis.status"
, "DateAccuracy", "thritaEHR.birthDateAccuracy"
, "Job", "thritaEHR.job"
, "Diagnosis", "ICD10, DSM-IV"
, "AccidentType", "ICD"
, "Cause", "ICD10, DSM-IV"
, "clarity", "SNOMEDCT"
, "Bloodgroup", "SNOMEDCT"
, "BloodRH", "SNOMEDCT"
, "SpecimenAdequacy", "thritaEHR.specimenQuality"
, "CollectionProcedure", "SNOMEDCT"
, "SpecimenTissueType", "SNOMEDCT"
, "Morphology", "ICDO"
, "Topography", "ICDO"
, "BSTest", "SNOMEDCT"
, "BSTiming", "SNOMEDCT"];
    for (var i = 0; i < all.length; i += 2) {
        if (all[i].toLowerCase() == table)
            return all[i + 1];
    }
    return null;
}
function getNodeVal(node, attName, defaultVal) {
    if (node && node != null && node.attr(attName))
        return node.attr(attName);
    if (typeof defaultVal === 'undefined')
        return '';
    return defaultVal;
}
//  reads the state from attrib 'v'
function setChecked(checkboxSelector) {
    $(checkboxSelector).each(function () {
        var v = $(this).attr('v');
        if (v == '1' || v == 'checked' || v == 'true')
            $(this).attr("checked", 'checked');
        else
            $(this).attr("checked", null);
    });
}
//  reads the state from attrib 'v'
function setValue(comboSelector) {
    $(comboSelector).each(function () {
        var v = $(this).attr('v');
        if (v && v != '')
            $(this).val(v);
    });
    $(comboSelector).trigger("chosen:updated");
}

// options:parent, cmds, callback
// cmds: [['do it', 0], ['see it', 1], ['title'], ['why', 2]]
// callback(cmd) //cmd = 0, 1, ....
function showMenu(options) {
    killEvent();
    $('#menuDiv').html('');
    var offset = $(options.parent).offset();
    for (var i = 0; i < options.cmds.length; i++)
        if (options.cmds[i].length == 1 || options.cmds[i][1] == '') {
            $('#menuDiv').append("<span>" + options.cmds[i][0] + "</span>");
        }
        else {
            $('#menuDiv').append("<a href='" + options.cmds[i][1] + "' class='cmd'>" + options.cmds[i][0] + "</a>");
        }
    if (options.width)
        $('#menuDiv').css("width", options.width);
    $('#menuDiv').offset({ top: offset.top + 10, left: offset.left - $('#menuDiv').width() }); //$('#usermenu').width()
    $('#menuDiv').show();
    $('#menuDiv').offset({ top: offset.top + 10, left: offset.left - $('#menuDiv').width() }); //
    //alert($('#menuDiv').html());
    function hideMenu() {
        $('#menuDiv').hide();
        $('html').unbind('click', hideMenu);
    }
    $('html').click(hideMenu);
    //alert($('#menuDiv .cmd').length);
    if (options.callback)
        $('#menuDiv .cmd').click(function () {
            hideMenu();
            mCmd = $(this).attr('href');
            options.callback(mCmd);
            return false;
        });
}
function toInt(val, defaultVal) {
    defaultVal = defaultVal | 0;
    if (!val || val.length == 0)
        return defaultVal;
    try{
        var res = parseInt(val.replace(/,/g, ""), 10);
        if (isNaN(res))
            return defaultVal;
        return res;
    }
    catch(e){
        //alert(val);
        return val;
    }
}
function toFloat(val, defaultVal) {
    if (!val)
        return defaultVal;
    val = '' + val;
    if (val.length == 0)
        return defaultVal;
    var res = parseFloat(val.replace(/,/g, ""));
    if (isNaN(res))
        return defaultVal;
    return res;
}
function toBool(val, defaultVal) {
    defaultVal = defaultVal | 0;
    if (!val || val.length == 0)
        return defaultVal;
    if (val == '0' || val.toLowerCase() == 'false')
        return false;
    return true;
}
// compares two dates
// return: 0 --> date1 == date2,  1 --> date1 > date2,  -1 ---> date1 < date2, -2 ---> invalid dates
function compareDate(date1, date2) {
    // format: yyyy/MM/dd
    if (date1 == date2)
        return 0;
    if (!date1 && !date2) // both are null
        return 0;
    if (date1 == '')
        return -2;
    if (date2 == '')
        return 1;
    var ymd1 = date1.split("/"), ymd2 = date2.split("/");
    if (ymd1.length != 3 || ymd2.length != 3)
        return -2;
    var d1 = 0, d2 = 0;
    if (_options.dateType == "fa") {
        // yyyy/mm/dd
        d1 = toInt(ymd1[0], 0) * 10000 + toInt(ymd1[1], 0) * 100 + toInt(ymd1[2], 0);
        d2 = toInt(ymd2[0], 0) * 10000 + toInt(ymd2[1], 0) * 100 + toInt(ymd2[2], 0);
    }
    else
    {
        // dd/mm/yyyy
        d1 = toInt(ymd1[2], 0) * 10000 + toInt(ymd1[1], 0) * 100 + toInt(ymd1[0], 0);
        d2 = toInt(ymd2[2], 0) * 10000 + toInt(ymd2[1], 0) * 100 + toInt(ymd2[0], 0);
    }
    if (d1 < d2)
        return -1;
    return 1;
}

function validateDateIntoFinanceYear(date) {
    if (_Context.fyStart == '')
        return false;
    return compareDate(date, _Context.fyStart) >= 0 && compareDate(date, _Context.fyEnd) <= 0;
}
function checkWorkflowStatus(f) {
    if (f.WorkflowStatus == WorkflowStatusEnum.Final) {
        alert(Lang.WFStatusFinalError);
        return false;
    }
    return true;
}
function noDecimalPlace(x) {
    if (x)
        return toInt(x.toString().replace(/,/g, "").split(".")[0], 0);
    return x;
}
function numberWithCommasAllowDP(x) {
    var parts = x.toString().replace(/,/g, "").split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}
function numberWithCommas(x) {
    var parts = x.toString().replace(/,/g, "").split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    if (parts.length > 1 && parseInt(parts[1], 10) == 0)
        return parts[0];
    if (parts[1]) {
        parts[1] = parts[1].replace(/0*$/, '');
        if (parts[1] == '')
            return parts[0];
    }
    return parts.join(".");
}
function stripCommas(x) {
    return x.toString().replace(/,/g, "");
}
function commaInputs(form) {
    form = form || '#detailForm';
    $('input', form).each(function () {
        var me = $(this);
        if (me.hasClass('comma'))
            return;
        if (me.attr('disabled') == '' || !me.attr('disabled') /*&& me.is(":visible")*/) {
            var cls = me.attr('class');
            if (!cls)
                return;
            var rules = cls.split(/\[|,|\]/);
            var isNumeric = false;
            for (var i = 0; i < rules.length; i++) {
                if (rules[i] == 'integer') {
                    isNumeric = true;
                    break;
                }
            }
            if (isNumeric) {
                me.addClass('comma');
                me.keyup(function (e) {
                    if (!me.attr('readonly') && e.which != 13)
                        if (me.val() != '' && me.val().indexOf('%') < 0)
                            me.val(numberWithCommasAllowDP(stripCommas(me.val()))); //numberWithCommasAllowDP
                });
                //if (toFloat(stripCommas(me.val()), 0) < 0) {
                //    alert(me.val() + " -->   " + stripCommas(me.val()) + " ===> " + numberWithCommas(stripCommas(me.val())));
                //}
                if (me.val() != '' && me.val().indexOf('%') < 0)
                    me.val(numberWithCommas(stripCommas(me.val())));
            }
        }
    });
}
function commaInputsRemove(form) {
    form = form || '#detailForm';

    $('input', form).each(function () {
        var me = $(this);
        if (me.hasClass('comma'))
            me.val(stripCommas(me.val()));
    });
}
function commaInputsSet(form) {
    form = form || '#detailForm';

    $('input', form).each(function () {
        var me = $(this);
        if (me.hasClass('comma'))
            if (me.val() != '' && me.val().indexOf('%') < 0)
                me.val(numberWithCommas(stripCommas(me.val())));
    });
}
// 20% --> 0.20
function percentInputsRemove(form) {
    form = form || '#detailForm';
    $('input', form).each(function () {
        var me = $(this);
        if (me.hasClass('percent')) {
            var val = me.val();
            if (val.indexOf('%') >= 0) {
                val = toFloat(val.replace('%', ''), 0);
                val *= -1;
                me.val(val);
            }
        }
    });
}
function percentInputsSet(form) {
    form = form || '#detailForm';
    $('input', form).each(function () {
        var me = $(this);
        if (me.hasClass('percent')) {
            var val = toFloat(me.val(), 0);
            if (val < 0)
                me.val(numberWithCommas((val * -1.0).toFixed(_Context.decimalPoints)) + '%');
            else
                me.val(numberWithCommas((val * 1.0).toFixed(_Context.decimalPoints)));
        }
    });
}

function autoPercentSign(form) {
    form = form || '#detailForm';
    $('.autopercentsign', form).blur(function () {
        var me = $(this);
        var val = me.val();
        if (val.indexOf('%') < 0 && toFloat(val.replace('%', ''), 0)> 0 ) {
            me.val(val + '%');
            me.blur();
        }
    });
}

function monetaryInputsRemove(form) {
    var pf = getPriceFactor();
    if (pf == 1 || pf == 0)
        return;
    form = form || '#detailForm';
    $('input', form).each(function () {
        var me = $(this);
        if (me.hasClass('monetary')) {
            var val = me.val();
            // check for percent mode
            if (val != '' && val.indexOf('%') < 0) {
                val = toFloat(val, 0);
                val /= pf;
                me.val(val);
            }
        }
    });
}
function monetaryInputsSet(form, dateSelector) {
    if (dateSelector) {
        var dt = $(dateSelector).val();
        if (dt && dt != '')
            _Context.priceDate = dt;
    }
    var pf = getPriceFactor();
    if (pf == 1 || pf == 0)
        pf = 1;
    form = form || '#detailForm';
    $('input', form).each(function () {
        var me = $(this);
        if (me.hasClass('monetary')) {
            me.unbind('keyup', handleMonetaryKeys);
            me.keyup(handleMonetaryKeys);
            var val = toFloat(me.val(), 0);
            //alert(me.val() + " --> " + val);
            // check for percent mode
            if (val > 0) {
                val = (val * pf).toFixed(_Context.decimalPoints);
                me.val(numberWithCommas(stripCommas(val)));
            }
        }
    });
}
function monetaryKeys(parent, cls) {
    cls = cls || 'monetary';
    $('input', parent).each(function () {
        var me = $(this);
        if (me.hasClass(cls)) {
            me.unbind('keyup', handleMonetaryKeys);
            me.keyup(handleMonetaryKeys);
        }
    });
}
function handleMonetaryKeys(e) {
    if ($(this).val().indexOf('%') >= 0)
        return;
    var val = toFloat($(this).val(), -1);
    if (val == -1)
        return;
    if (e.keyCode === 37) // left
    {
        if (val > 1000) {
            var parts = $(this).val().toString().replace(/,/g, "").split(".");
            parts[0] = parseInt(parseInt(parts[0]) / 1000);
            $(this).val(parts.join("."));
        }
    }
    else if (e.keyCode === 38) // up
    {
        $(this).val(val + 1);
    }
    else if (e.keyCode === 39) // right
    {
        var parts = $(this).val().toString().replace(/,/g, "").split(".");
        parts[0] = parseInt(parts[0]) * 1000;
        $(this).val(parts.join("."));
    }
    else if (e.keyCode === 40) // up
    {
        if (val > 1)
            $(this).val(val - 1);
    }
}
// umed 93-03-14: use `today` when _Context.priceDate is empty
function getPriceFactor() {
    // [['1392/01/01', '1392/06/31', 10], ['1392/07/01', '1392/12/29', 20]]
    if (!_Context.priceFactor)
        return 1;
    var dt = _Context.priceDate;
    if (!dt || dt == '')
        dt = _Context.today;
    for (var i = 0; i < _Context.priceFactor.length; i++) {
        if (compareDate(dt, _Context.priceFactor[i][0]) >= 0
            && compareDate(dt, _Context.priceFactor[i][1]) <= 0)
            return _Context.priceFactor[i][2];
    }
    return 1;
}
function toMonetary(val, date) {
    if (!val || val == '')
        return val;
    if (date && (date != ''))
        _Context.priceDate = date;
    var pf = getPriceFactor();
    if (pf == 1 || pf == 0)
        return toFloat(val, 0).toFixed(_Context.decimalPoints);
    else {
        val = (toFloat(val, 0) * pf).toFixed(_Context.decimalPoints);
        return val;
    }
}
function fromMonetary(val, date) {
    if (!val || val == '')
        return val;
    if (date && (date != ''))
        _Context.priceDate = date;
    var pf = getPriceFactor();
    if (pf == 1 || pf == 0)
        return val;
    else {
        val = toFloat(val, 0) / pf;
        return val;
    }
}

function checkProgramStatus() {
    // programStatus= 0: all, 1: ok, 2: closed
    if (_Context.programStatus == 0) {
        alert(Lang.PleaseChooseOneOfTheIndependentAccount);
        return false;
    }
    if (_Context.programStatus == 2) {
        alert(Lang.ThisIndependentAccountHasBeenClosed);
        return false;
    }
    return true;
}
var createCookie = function (name, value, days) {
    var expires;
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    }
    else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = document.cookie.length;
            }
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return "";
}

function autoCompleteTreePath(textInput, hiddenInput, button) {
    autoComplete('Trees', 'treesauto', textInput, hiddenInput, postBackTreePath, formatTreePath);
    if (button)
        $(button).click(function selectTreePath() {
            var _url = 'get.aspx?op=GetHtml&table=Trees&view=treeselect' + appendTime();
            var btns = {};
            btns[Lang.Cancel] = "_CANCEL_";
            showPopup(_url, function (v, m, f) {
                if (f.STreePathName != '') {
                    $(textInput).val(f.STreePathName);
                    $(hiddenInput).val(f.STreePath);
                    //$('#EPeNeed').val(f.SPeNeed);
                    //$('#EPrNeed').val(f.SPrNeed);
                }
            }, btns);
        });
}
function autoCompleteTreePath2(codeInput, titleInput, hiddenInput, button, programInput, _above) {
    $(codeInput).unautocomplete();
    $(titleInput).unautocomplete();
    autoComplete('Trees', 'treesauto', codeInput, hiddenInput
        , function (event, data, formatted, textInput, hiddenInput) {
            if (data) {
                // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due, TopicTitle, ProgramCode, Nature, EquipmentNeed, CanHaveSubTree
                var hasChild = toBool(data[5]), canHaveSubTree = toBool(data[12]);
                if (hasChild || canHaveSubTree) {
                    $(codeInput).val('');
                    $(titleInput).val('');
                    $(hiddenInput).val('');
                    if (programInput)
                        $(programInput).val('');
                    if (hasChild)
                        alert(Lang.HasChild);
                    else
                        alert(Lang.HasSubTreeBut);
                }
                else {
                    $(codeInput).val(data[4] + '(' + data[13] + ')');
                    $(titleInput).val(data[8]);
                    $(titleInput).attr('title', data[1]);
                    $(hiddenInput).val(data[0]);
                    if (programInput)
                        $(programInput).val(data[9]);

                    $('#EPeNeed').val(data[2]);
                    if (!toBool(data[2])) {
                        $('#PersonnelName,#PersonnelID').val('');
                    }
                    $('#EPrNeed').val(data[3]);
                    if (!toBool(data[3])) {
                        $('#WardName,#WardID').val('');
                    }
                    $('#EqNeed').val(data[11]);
                    if (!toBool(data[11])) {
                        $('#EquipmentName,#EquipmentID').val('');
                    }
                }
            }
        }
        , formatTreePath2, function (data) { return data[4] + '(' + data[13] + ')';}, null, null, null, _above);
    autoComplete('Trees', 'treesauto', titleInput, hiddenInput
        , function (event, data, formatted, textInput, hiddenInput) {
            if (data) {
                // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due, TopicTitle, ProgramCode, Nature, EquipmentNeed, CanHaveSubTree
                var hasChild = toBool(data[5]), canHaveSubTree = toBool(data[12]);
                if (hasChild || canHaveSubTree) {
                    $(codeInput).val('');
                    $(titleInput).val('');
                    $(hiddenInput).val('');
                    if (programInput)
                        $(programInput).val('');
                    if (hasChild)
                        alert(Lang.HasChild);
                    else
                        alert(Lang.HasSubTreeBut);
                }
                else {
                    $(codeInput).val(data[4] + '(' + data[13] + ')');
                    $(titleInput).val(data[8]);
                    $(titleInput).attr('title', data[1]);
                    $(hiddenInput).val(data[0]);
                    if (programInput)
                        $(programInput).val(data[9]);

                    $('#EPeNeed').val(data[2]);
                    if (!toBool(data[2])) {
                        $('#PersonnelName,#PersonnelID').val('');
                    }
                    $('#EPrNeed').val(data[3]);
                    if (!toBool(data[3])) {
                        $('#WardName,#WardID').val('');
                    }
                    $('#EqNeed').val(data[11]);
                    if (!toBool(data[11])) {
                        $('#EquipmentName,#EquipmentID').val('');
                    }
                }
            }
    }, formatTreePath2, function (data) { return data[8]; }, null, null, null, _above);
    if (button)
        $(button).click(function selectTreePath() {
            var _url = 'get.aspx?op=GetHtml&table=Trees&view=treeselect' + appendTime();
            var btns = {};
            btns[Lang.Cancel] = "_CANCEL_";
            showPopup(_url, function (v, m, f) {
                if (f.STreePathName != '') {
                    $(codeInput).val(f.STopicCode + '(' + f.SMainParentName + ')');
                    $(titleInput).val(f.STopicTitle);
                    $(titleInput).attr('title', f.STreePathName);
                    $(hiddenInput).val(f.STreePath);
                    if (programInput)
                        $(programInput).val(f.SProgramCode);
                    $('#EPeNeed').val(f.SPeNeed);
                    $('#EPrNeed').val(f.SPrNeed);
                    $('#EqNeed').val(f.SEqNeed);
                }
            }, btns);
        });
    $(codeInput).unbind('keyup');
    $(codeInput).keyup(function (e) {
        if (e.keyCode === 37) {
            $('.ac_results').hide();
            var _url = 'get.aspx?op=GetHtml&table=Trees&view=treeselect' + appendTime();
            var baseCode = toInt($(codeInput).val(), -1);
            if (baseCode != -1)
                _url += '&bcode=' + baseCode;
            _url += appendTime();
            var btns = {};
            btns[Lang.Cancel] = "_CANCEL_";
            showPopup(_url, function (v, m, f) {
                if (f.STreePathName != '') {
                    $(codeInput).val(f.STopicCode + '(' + f.SMainParentName + ')');
                    $(titleInput).val(f.STopicTitle);
                    $(titleInput).attr('title', f.STreePathName);
                    $(hiddenInput).val(f.STreePath);
                    if (programInput)
                        $(programInput).val(f.SProgramCode);
                    //$('#EPeNeed').val(f.SPeNeed);
                    //$('#EPrNeed').val(f.SPrNeed);
                }
                $(codeInput).focus();
            }, btns);
        }
    });
}
function autoCompleteTreePathTyped(codeInput, titleInput, hiddenInput, button, programInput, type) {
    autoComplete('Trees', 'treestypedauto', codeInput, hiddenInput
        , function (event, data, formatted, textInput, hiddenInput) {
            if (data) {
                // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due, TopicTitle
                var hasChild = data[5] != '';
                if (hasChild) {
                    $(codeInput).val('');
                    $(titleInput).val('');
                    $(hiddenInput).val('');
                    if (programInput)
                        $(programInput).val('');
                    alert(Lang.HasChild);
                }
                else {
                    $(codeInput).val(data[4]);
                    $(titleInput).val(data[9]);
                    $(titleInput).attr('title', data[1]);
                    $(hiddenInput).val(data[0]);
                    if (programInput)
                        $(programInput).val(data[10]);
                }
            }
        }
        , formatTreePath2, function (data) { return data[4]; }, null
        , 'get.aspx?op=Auto&table=Trees&qd=treestypedauto&Type=' + type + appendTime());
    autoComplete('Trees', 'treestypedauto', titleInput, hiddenInput, function (event, data, formatted, textInput, hiddenInput) {
        if (data) {
            // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due, TopicTitle
            var hasChild = data[5] != '';
            if (hasChild) {
                $(codeInput).val('');
                $(titleInput).val('');
                $(hiddenInput).val('');
                alert(Lang.HasChild);
                if (programInput)
                    $(programInput).val('');
            }
            else {
                $(codeInput).val(data[4]);
                $(titleInput).val(data[9]);
                $(titleInput).attr('title', data[1]);
                $(hiddenInput).val(data[0]);
                if (programInput)
                    $(programInput).val(data[10]);
            }
        }
    }, formatTreePath2, function (data) { return data[9]; }, null
    , 'get.aspx?op=Auto&table=Trees&qd=treestypedauto&Type=' + type + appendTime());
    if (button)
        $(button).click(function selectTreePath() {
            var _url = 'get.aspx?op=GetHtml&table=Trees&view=treeselect' + appendTime();
            var btns = {};
            btns[Lang.Cancel] = "_CANCEL_";
            showPopup(_url, function (v, m, f) {
                if (f.STreePathName != '') {
                    $(codeInput).val(f.STopicCode);
                    $(titleInput).val(f.STopicTitle);
                    $(titleInput).attr('title', f.STreePathName);
                    $(hiddenInput).val(f.STreePath);
                    if (programInput)
                        $(programInput).val(f.SProgramCode);
                    //$('#EPeNeed').val(f.SPeNeed);
                    //$('#EPrNeed').val(f.SPrNeed);
                }
            }, btns);
        });
}
var fn1 = ['', 'یک', 'دو', 'سه', 'چهار', 'پنج', 'شش', 'هفت', 'هشت', 'نه', 'ده', 'یازده', 'دوازده', 'سیزده', 'چهارده', 'پانزده', 'شانزده', 'هفده', 'هیجده', 'نوزده', 'بیست'];
var fn2 = ['', '', 'بیست', 'سی', 'چهل', 'پنجاه', 'شصت', 'هفتاد', 'هشتاد', 'نود', 'صد'];
var fn3 = ['', 'صد', 'دویست', 'سیصد', 'چهارصد', 'پانصد', 'ششصد', 'هفتصد', 'هشتصد', 'نهصد', 'هزار'];
var dec = ['دهم', 'صدم', 'هزارم', 'ده هزارم', 'صدهزارم', 'میلیونم'];
function numToString(n) {
    if (toFloat(n, 0) == 0)
        return "صفر";
    var parts = n.toString().replace(/,/g, "").split(".");
    if (parts.length == 1 || parseInt(parts[1], 10) == 0) {
        return numToStringSingle(parts[0]);
    }
    if (parseInt(parts[0], 10) == 0)
        return numToStringSingle(parts[1]) + " " + dec[parts[1].length - 1];
    return numToStringSingle(parts[0]) + " و " + numToStringSingle(parts[1]) + " " + dec[parts[1].length - 1];
}
function numToStringSingle(n) {
    if (toFloat(n, 0) == 0)
        return "صفر";
    var res = numToStringInternal(stripCommas(n));
    res = res.replace(/و و/g, 'و').trim();
    if (res.lastIndexOf('دو') != res.length - 2)
        if (res.lastIndexOf('و') == res.length - 1) {
            // alert(res + " ==== " + res.lastIndexOf('و'));
            res = res.substr(0, res.length - 1);
        }
    return res;
}
function numToStringInternal(n) {
    //return Adad(Number(n));
    n = n + "";
    var num = toInt(n, 0);
    n = num + "";
    if (num < 21)
        return fn1[num];
    if (num < 100) {
        if (n.charAt(1) == '0')
            return fn2[n.charAt(0)];
        return fn2[n.charAt(0)] + ' و ' + numToStringInternal(n.charAt(1));
    }
    if (num == 100)
        return 'صد';
    if (num < 1000) {
        if (toInt(n.substr(1), 0) == 0)
            return fn3[n.charAt(0)];
        return fn3[n.charAt(0)] + ' و ' + numToStringInternal(n.substr(1));
    }
    if (num == 1000)
        return 'هزار';
    if (num < 1000000) {
        if (toInt(n.substr(n.length - 3), 0) == 0)
            return numToStringInternal(n.substr(0, n.length - 3)) + ' هزار';
        return numToStringInternal(n.substr(0, n.length - 3)) + ' هزار و ' + numToStringInternal(n.substr(n.length - 3));
    }
    //if (num == 1000000)
    //    return 'هزار';
    if (num < 1000000000) {
        if (toInt(n.substr(n.length - 6), 0) == 0)
            return numToStringInternal(n.substr(0, n.length - 6)) + ' میلیون ';
        return numToStringInternal(n.substr(0, n.length - 6)) + ' میلیون و ' + numToStringInternal(n.substr(n.length - 6));
    }
    if (num < 1000000000000) {
        if (toInt(n.substr(n.length - 9), 0) == 0)
            return numToStringInternal(n.substr(0, n.length - 9)) + ' میلیارد';
        return numToStringInternal(n.substr(0, n.length - 9)) + ' میلیارد و ' + numToStringInternal(n.substr(n.length - 9));
    }
    return 'بەڕاستی گەورەیە';
}

var monthes = ['', 'فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند'];
function dateToString(dt) {
    if (!dt || dt == '')
        return "";
    var ymd = dt.split("/");
    if (ymd.length != 3)
        return "";
    return numToString(ymd[2]) + " " + monthes[toInt(ymd[1], 0)] + " " + numToString(ymd[0]);
}
function getPersonCredit(personID, newOwe) {
    // OK = 1, NOK = -1
    newOwe = newOwe || 0;
    var status = getServerValue("personcheckmaxcredit", "Personnel", "&PersonnelID=" + personID + "&NewOwe=" + newOwe);
    return status;
}

function getVisitorCredit(storedraftId, newOwe) {
    // OK = 1, NOK = -1
    newOwe = newOwe || 0;
    var status = getServerValue("visitorcheckmaxcredit", "Personnel", "&StoredraftID=" + storedraftId + "&NewOwe=" + newOwe);
    return status;
}
function checkPersonCredit(personID, newOwe) {
    // OK = 1, NOK = -1
    newOwe = newOwe || 0;
    var status = getPersonCredit(personID, newOwe);
    if (status == -1)
        alert(Lang.LowCredit);
}


function getSettings(title) {
    return getServerValue('getsetting', 'Config', 'Name=' + title);
}
function getSettingTax() {
    var settingTax = getSettings('DefTaxPercent');
    if (toFloat(settingTax, 0) > 0)
        settingTax += '%';
    else
        settingTax = "";
    return settingTax;
}
function getSettingVat() {
    var settingVat = getSettings('DefVATPercent');
    if (toFloat(settingVat, 0) > 0)
        settingVat += '%';
    else
        settingVat = "";
    return settingVat;
}

function prepareMobileTable(table) {
    if (_Context.isMobile) {
        $(table).html("<tr class='th'><td width='20'>" + Lang.ColRowNr + "</td><td width='400'>" + Lang.StoreService + "</td><td width='29' class='center'><img src='img/add.gif' class='add' /></td></tr>");
        $(table).closest('div').css('width', '500px');
    }
}
function tdStart(label, first, cls) {
    if (_Context.isMobile) {
        if (first)
            return "<td><label>" + label + "</label>";
        return "<label>" + label + "</label>";
    }
    if (cls)
        return "<td class='" + cls + "'>";
    return "<td>";
}
function tdEnd() {
    if (_Context.isMobile)
        return "";
    return "</td>";
}

function offerstepsDefinition(celDiv, id) {
    $(celDiv).html('<a href="list.aspx?table=OfferSteps&id=' + id + '"><img src="img/task_icon.jpg" /></a>');
}
function offerconditionsDefinition(celDiv, id) {
    $(celDiv).html('<a href="list.aspx?table=OfferConditions&id=' + id + '"><img src="img/task_icon.jpg" /></a>');
}


function formatPersonnelItem(data, index) {
    // ID, Title, Remain, Code, PCoding, PIdentify, Nature
    // Nature:
    // 0 = Owe
    // 1 = Due
    // 2 = Both
    index = index || 2;
    var personIdentify = '',
        name = data[1],
        remained = data[index],
        code = data[3],
        coding = data[4],
        balanceClass = 'success',
        nature = +data[6];

    //if (data[5] && data[5] != '') {
    //    personIdentify =  data[5];
    //}

    if ((nature === 0 && remained > 0) || (nature === 1 && remained < 0)) {
        balanceClass = 'danger';
    }

    if (remained < 0) {
        remained = numberWithCommas(toMonetary(remained) * -1) + ' ' + Lang.Li;
    } else if (remained > 0) {
        remained = numberWithCommas(toMonetary(remained)) + ' ' + Lang.Cr;
    } else {
        remained = numberWithCommas(toMonetary(remained));
    }

    return CompileHB('personnel-autocomplete', {
        name: name,
        balanceClass: balanceClass,
        remained: remained,
        code: code,
        coding: coding
    });
}
function hasFocus(elem) {
    return elem === $(document.activeElement);
};

function showPersonBook(selector, showAsDropDown, showDetail) {
    var id = $(selector).attr('id') + 'pb';
    if (showAsDropDown) {
        $(selector).after("<a class='fake-button' id='" + id + "' href='#'><i class='fa fa-bank'></i>" + Lang.PersonBookButton + "</a>");
    } else {
        $(selector).after("<input class='btn btn-dark-grey input-sm small-group-button' type='button' id='" + id + "' value='" + Lang.PersonBookButton + "' />");
    }
    $('#' + id).click(function () {
        var pid = $(selector).val();
        if (hasValuePositive(pid))
            popup("reports/accounting/PersonBook.aspx?pid=" + pid);
    });
    if (showDetail)
    {
        id = $(selector).attr('id') + 'pd';
        $(selector).parent().after("<li><a class='fake-button' id='" + id + "' href='#'><i class='fa fa-user'></i> " + Lang.BuyerInformation + "</a></li>");
        $('#' + id).click(function () {
            var pid = $(selector).val();
            if (hasValuePositive(pid))
            {
                //_Context.id = id;
                var _url = 'get.aspx?op=GetHtml&table=Personnel&view=person&qd=person&id=' + pid + appendTime();
                showPopup(_url, function () { return true; }, getButtons(true), null, null);
            }
        });
    }
}
function choosePrintFields(table, view) {
    var _url = 'get.aspx?op=GetHtml&table=' + table + '&disablejs=1';
    if (view)
        _url += '&view=' + view;
    function _callback(v, m, f) {
        if (v == 'OK') {
            var lis = ",";
            $(':checkbox', '.choosePrint').each(function (index) {
                if (!$(this).attr('checked'))
                    lis += $(this).attr('id') + ",";
            })
            var _url = "set.aspx?op=RunQuery&qd=configinsertupdate&table=Config" + appendTime();
            $.ajax({
                type: "POST",
                url: _url,
                data: { Name: 'ChoosePrint' + table, Value: lis },
                async: false,
                success: function (html) {
                    if (html.indexOf('Error:') != 0 && html.indexOf('خطا:') != 0) {
                        message(html);
                    }
                    else {
                        displayError(html);
                    }
                },
                error: function () {
                    _Context.success = false;
                }
            });
        }
    }
    $.ajax({
        url: _url,
        type: 'GET',
        async: false,
        success: function (html) {
            html = "<div class='choosePrint'>" + html + "</div>";
            $.prompt(html, {
                submit: _callback,
                buttons: getButtons(false),
                focus: 0,
                top: '1%'
            });
        },
        error: ajaxError
    });
    var ids = getSettings('ChoosePrint' + table);
    $('label', '.choosePrint').each(function (index, label) {
        var elementId = $(label).attr("for");
        if (elementId === "") {
            return;
        }
        var element = $("#" + elementId);
        // Check if we have found any elements?
        if (!element) {
            return;
        }

        // If an element is marked as noprint, skip it!
        if ($(element).hasClass("noprint")) {
            $(element).remove();
            $(label).remove();
            return;
        }
        var checkbox = '<input type="checkbox" class="choose" id="' + elementId + '" checked="checked" />';
        if (ids && ids.indexOf("," + elementId + ",") >= 0) {
            checkbox = '<input type="checkbox" class="choose" id="' + elementId + '" />';
        }
        var parent = $(element).parent();
        $(element).remove();
        parent.append(checkbox);
    });
    // Remove remaining inputs
    $("input:not(.choose)", ".choosePrint").remove();

    // Remove buttons
    $("a.btn").remove();

    // table columns
    $('.th td', '.choosePrint').each(function (index, value) {
        if ($(this).attr('type') == 'hidden')
            return;
        var cid = '_td_' + index;
        var li = "<input type='checkbox' id='" + cid + "' checked='checked' />";
        if (ids && ids.indexOf("," + cid + ",") >= 0)
            li = "<input type='checkbox' id='" + cid + "' />";
        $(this).append(li);
    });
    // resize tables: no height
    $('table', '.choosePrint').closest('div').css('height', 'auto');
    // remove tabs
    $('.tabs', '.choosePrint').remove();
    return false;
}
function hideTr(td) {
    var p = td.parent();
    var index = p.children().index(td);
    $('tr', p.closest('table')).each(function (indexTr) {
        //if (indexTr > 1)
        $('td', $(this)).each(function (indexTd) {
            if (indexTd == index)
                $(this).hide();
        });
    });
    //$('td:nth-child(' + index + ')', p.closest('table')).addClass('noprint');
    //$('th:nth-child(' + index + ')').hide();
    //$('td:nth-child(' + index + ')').hide();
    //tags.hide();
}
function applyPrintFields(table) {
    var ids = getSettings('ChoosePrint' + table);
    if (ids) {
        $('input,select', '#detailForm').each(function (index, value) {
            if ($(this).attr('type') == 'hidden')
                return;
            var cid = $(this).attr('id');
            if (ids.indexOf("," + cid + ",") >= 0) {
                $(this).addClass('noprint');
                $('label[for="' + cid + '"]').addClass('noprint');
            }
        });
        // table columns
        $('.th td', '#detailForm').each(function (index, value) {
            if ($(this).attr('type') == 'hidden')
                return;
            var cid = '_td_' + index;
            if (ids.indexOf("," + cid + ",") >= 0) {
                $(this).addClass('noprint');
            }
        });
    }
}


// I know this is dirty code, but I have to do it!
window.AppOptions = window.AppOptions || {
    maxTextLength: 400
};
function replaceWhiteSpace(str, replacement) {
    // Default replacement character
    replacement = replacement || '_';

    return str.replace(/\s+/gi, replacement);
}

// Check if current user has permission to access the given access list
// Returns true if she is permitted and false otherwise
function isPermitted(access) {
    var permitted = false;
    if (access && access.length > 0) {
        var formButton = access.split(",");
        if (formButton.length == 2) {
            //simple version: hospital,release
            if (hasAccess("link", formButton[0], formButton[1])) {
                permitted = true;
            }
        } else if (formButton.length == 4) {
            //2 access version: hospital,dr,ward,17
            if (hasAccess("link", formButton[0], formButton[1]) || !hasAccess("link", formButton[2], formButton[3])) {
                permitted = true;
            }
        }
    }

    return permitted;
}

function makeChosen() {
    $(".chosen-select").chosen({
        width: '100%',
        no_results_text: Lang.NoResultsText,
        placeholder_text_single: Lang.PlaceholderTextSingle,
        search_contains: true
    });
    $('.chosen-container').click(function () { $('.ac_results').hide();});
}

function jpDebug() {
    var i = 0, length = arguments.length;
    console.log("\n====================");
    for (i = 0; i < length; i++) {
        console.log(arguments[i]);
    }
    console.log("====================\n");
}

function triggerChosenUpdate(selector) {
    (typeof (selector) === "string" ? $(selector) : selector).trigger("chosen:updated");
}
function removeZeroInDateString(dateString) {
    dateString = dateString.replace('01', '1').replace('02', '2').replace('03', '3').replace('04', '4').replace('05', '5')
                .replace('06', '6').replace('07', '7').replace('08', '8').replace('09', '9');
    return dateString;
}
// This function is called upon displaying any form in the browser.
// You can use it to make UI tweaks.
// Be careful not to mess up things though!
function TweakUI() {
    $("input[readonly]").attr("tabindex", "-1");
}

function fixFormButtonContainer() {
    $("#form-buttons-container")
        .removeClass("col-md-6")
        .removeClass("col-lg-8")
        .prev()
        .remove();
}

function disableKeydownOnLastInputByName(name) {
    $('input[name=' + name + ']').removeClass('no-keydown');
    $('input[name=' + name + ']').last().addClass('no-keydown');
}

// Compile the template with the supplied data and replace the default print button with the newly generated text
function replacePrintButton(data) {
    // Remove the the default filthy print button, and add our shiny one!
    var oldPrintButton = $("button[value=Print]"), parentContainer = oldPrintButton.parent(), sibling = oldPrintButton.prev();
    oldPrintButton.remove();

    var dropDownPrintButton = CompileHB("shiny-print-button", data);
    if (sibling.length > 0) {
        sibling.after(dropDownPrintButton);
    } else {
        parentContainer.prepend(dropDownPrintButton);
    }

    // printlog' m="{{ mode }}" t="{{ type }}" i
    $('.printlog').parent().click(function () {
        var b = $(this).find('.printlog');
        var mode = b.attr('m');
        var type = b.attr('t');
        var id = b.attr('i');
        runQuery('printloginsert', 'PrintLog', 'Mode=' + mode + '&Type=' + type + '&OtherID=' + id);
        //alert(mode + ' - ' + type + ' - ' + id);
        return true;
    });
}
function processPrintButton(mode, type, id) {
    var printButton = $("button[value=Print]");
    printButton.attr('m', mode).attr('t', type).attr('i', id);
   // printlog' m="{{ mode }}" t="{{ type }}" i
    printButton.click(function () {
        runQuery('printloginsert', 'PrintLog', 'Mode=' + mode + '&Type=' + type + '&OtherID=' + id);
        return true;
    });
}

function fixScrollbarBug() {
    $(".hDivBox table").first().css("width", $(".bDiv table").first().css("width"));
}

function formatService(data) {
    var id = data[0],
        serviceName = data[1],
        expireDate = data[3],
        income = data[4],
        outcome = data[5],
        availability = (income - outcome).toFixed(4).replace('.0000', ''),
        profitPercent = data[11],
        saleWorth = data[12],
        coding = data[13],
        price = 0, //قیمت فروش
        freebase = 0, // قیمت خرید
        notConfirmOutcome = data[15], //خروجی تایید نشده
        showAvailability,
        showFreebase,
        showPrice;

    showAvailability = hasGeneralAccess("seeavailability", generalSettings);
    showFreebase = hasGeneralAccess("seebuyprice", generalSettings);
    showPrice = hasGeneralAccess("seesellprice", generalSettings);
    if (expireDate) {
        var y = expireDate.split('/')[0];
        if (y > 1450)
            expireDate = null;
    }
    // Price priority:
    // 1- GroupingPrice: col10 | col14 = BuyPrice, col16 = SalePrice
    // 2- ProfitPercent: col10 = BasePrice, col2 = SalePrice
    // 3- Last Prices: col12 = LastSalePrice (TheLPSP), col14 = LastBuyPrice (TheLPBP)
    //price = data[2];
    if (profitPercent) {
        // ProfitPercent
        if (data[2])
            price = data[2];
        if (data[10])
            freebase = data[10];
    } else {
        // Last Prices
        if (saleWorth)
            price = saleWorth;
        if (data[14])
            freebase = data[14];
    }
    if (data.length > 15 && data[16]) {
        // GroupingPrice
        price = data[16];
    }
    if(data[19])
        serviceName = data[1] + "-" + data[19];
    price = numberWithCommas(toMonetary(price));
    freebase = numberWithCommas(toMonetary(freebase));

    return CompileHB('service-autocomplete', {
        name: serviceName,
        coding: coding,
        freebase: freebase,
        availability: availability,
        availabilityClass: (availability <= 0 ? 'danger' : 'success'),
        price: price,
        notConfirmOutcome: notConfirmOutcome,
        expireDate: expireDate,
        showAvailability: showAvailability,
        showFreebase: showFreebase,
        showPrice: showPrice
    });
}

function formatServicePrice(data) {
    var id = data[0],
        serviceName = data[1],
        income = data[9],
        outcome = data[10],
        availability = (income - outcome).toFixed(4).replace('.0000', ''),
        profitPercent = data[14],
        coding = data[12],
        saleWorth = data[13],
        price = 0, //قیمت فروش
        freebase = 0, //قیمت خرید
        acronym = '',
        showAvailability,
        showFreebase,
        showPrice;

    showAvailability = hasGeneralAccess("seeavailability", generalSettings);
    showFreebase = hasGeneralAccess("seebuyprice", generalSettings);
    showPrice = hasGeneralAccess("seesellprice", generalSettings);

    if (profitPercent) {
        if (data[8]) {
            price = data[8];
        }
        if (data[11]) {
            freebase = data[11];
        }
    } else {
        if (saleWorth) {
            price = saleWorth;
        }
        if (data[4]) {
            freebase = data[4];
        }
    }
    price = numberWithCommas(toMonetary(price));
    freebase = numberWithCommas(toMonetary(freebase));
    if (data.length > 15)
        acronym = data[15];

    return CompileHB('servicePrice-autocomplete', {
        name: serviceName,
        coding: coding,
        freebase: freebase,
        availability: availability,
        availabilityClass: (availability <= 0 ? 'danger' : 'success'),
        price: price,
        acronym: acronym,
        showAvailability: showAvailability,
        showFreebase: showFreebase,
        showPrice: showPrice
    });
}

function formatDocumentAccounting(data, i, n, value) {
    // No, DoComments
    var number = data[0],
        doComments = data[1] || '<' + Lang.NoComments + '>';

    return CompileHB('documentaccounting-autocomplete', {
        Number: number,
        Comments: doComments
    });
}
function formatCheck(data, i, n, value) {
    var no = data[1],
        amount = numberWithCommas(toMonetary(noDecimalPlace(data[2]))),
        dueDate = data[3];

    return CompileHB('check-autocomplete', {
        No: no,
        Amount: amount,
        DueDate: dueDate
    });
}

function colorTasks() {
    $('#flexi tr').each(function () {
        var color = $('.WorkflowColor div', this).html();
        if (color && color != '')
            $('.WorkflowColor', this).prev().prev().css('background-color', '#' + color);
    });
}

//insert order
function insertOrder(v, m, f, updateQry, insertQuery, table, form, funcShow, reshow) {
    funcShow = funcShow || show;
    var res = -1;
    if (v == 'New') {
        funcShow(-1);
        res = -1;
    }
    form = form || '#detailForm';
    if (v == 'OK' || v == 'OKPrint'||v=='Lock'||v=='Unlock') {
        var valid = validateInputs(form, f);
        if (!valid)
            res = 0;
        if (_Context.isDemo) {
            alert(Lang.DemoVersionNoSave);
            return false;
        }
        table = table || _Context.table;
        _Context.resultID = f.ID;
        var _url = "set.aspx?op=InsertValues&qd=" + updateQry + "&table=" + table + "&id=" + f.ID + appendTime();
        if (!_Context.id || _Context.id == -1)
            _url = "set.aspx?op=InsertValues&qd=" + insertQuery + "&table=" + table + appendTime();
        commaInputsRemove(form);
        monetaryInputsRemove(form);
        percentInputsRemove(form);
        textInputsCommaRemove(form);
        dataString = $(form).serialize();
        textInputsCommaSet(form);
        monetaryInputsSet(form);
        commaInputsSet(form);
        percentInputsSet(form);
        $.ajax({
            type: "POST",
            url: _url,
            data: dataString,
            async: false,
            success: function (html) {
                if (html.indexOf('Error:') != 0 && html.indexOf('خطا:') != 0) {
                    message(html);
                    _Context.resultID = parseInt(html, 10);
                    _Context.success = true;
                    $("#flexi").flexReload();
                }
                else {
                    displayError(html);
                }
            },
            error: function () {
                _Context.success = false;
            }
        });
        if (!_Context.success)
            res = 0;
        if (v == 'OKPrint')
            popup('print.aspx?id=' + _Context.resultID + '&table=' + table + appendTime());
        if (_Context.success) {
            var title = $('.popupTitle').html();
            $('.popupTitle').html(Lang.Saved);
            // todo: this line has bugs in new jquery :(
            //$('.popupTitle').animate({ 'background-color': '#f00' }, 1000, 'linear', function () { $('.popupTitle').animate({ 'background-color': '#00496F' }, 'slow'); $('.popupTitle').html(title); });
        }
        res = _Context.resultID;
    }
    if (v == "Print")
        res = generalPrint(v, m, f, form);
    $(form).validationEngine('hide');
    if (res == 0)
        return false;
    if (res < 0)
        return true;
    if (_Context.id == -1 || reshow) {
        funcShow(res, null, function () {
            setTimeout(function () {
                var title = $('.popupTitle').html();
                $('.popupTitle').html(Lang.Saved);
                // todo: this line has bugs in new jquery :(
                //$('.popupTitle').animate({ 'background-color': '#f00' }, 1000, 'linear', function () { $('.popupTitle').animate({ 'background-color': '#00496F' }, 1000, 'linear'); $('.popupTitle').html(title); });
            }, 200);
        });
    }
    else
        return false;
}

function calcOrder(v, m, f, constid, orderid, fishdefid, fields, form, fishid) {
    form = form || '#detailForm';
    _Context.success = false;
    var valid = validateInputs(form, f);
    if (!valid)
        return false;
    var _res = '';
    var _url = "set.aspx?op=CalcOrder&orderid=" + orderid + "&defid=" + fishdefid + "&fields=" + fields + "&cid=" + constid ;
    if (fishid) {
        _url += "&fishid=" + fishid;
    }
    _url += appendTime();
    commaInputsRemove(form);
    monetaryInputsRemove(form);
    percentInputsRemove(form);
    textInputsCommaRemove(form);
    dataString = $(form).serialize();
    textInputsCommaSet(form);
    monetaryInputsSet(form);
    commaInputsSet(form);
    percentInputsSet(form);
    $.ajax({
        type: "POST",
        url: _url,
        data: dataString,
        async: false,
        success: function (html) {
            if (html.indexOf('Error:') != 0 && html.indexOf('خطا:') != 0) {
                message(html);
            }
            var fields = "";
            $(html).find('R').each(function () {
                var name = getNodeVal($(this), "Name");
                var value = getNodeVal($(this), "Value");
                $("#" + name).val(value);
                fields += name +"-"+value+ "|";
            });
            $("#Fields").val(fields);
            _res = fields.substr(0, fields.length - 1);
        },
        error: function () {
            _Context.success = false;
        }
    });
    return _res; // postSuccess;
}



// ----------------------------------------
// show and showcallback for factors
// we moved them here, so we can access them from cartable.js
// ----------------------------------------
function purchaserequestshowcallback(v, m, f) {
    if (v == 'TaskImpls') {
        showTaskImpls(_Context.id, WorkflowTypeEnum.PurchaseRequest);
        return false;
    }
    if (v == 'ChangeLog') {
        showLogChange('PurchaseRequest', _Context.id, WorkflowTypeEnum.PurchaseRequest);
        return false;
    }
    if (v == 'Print') {
        printSubform();
        return false;
    }
    if (v == 'Cancel')
        return true;
    _Context.priceDate = f.RequestDate;
    var qry = (_Context.id == -1) ? 'purchaserequestinsert' : 'purchaserequestupdate';
    if (v != 'OK' && v != 'New') {
        var taskMsg = f.ConfirmTaskMsg;
        //var taskMsg = prompt(Lang.ConfirmTaskMsg, '');
        //if (taskMsg == null)
        //    return false;
        _Context.success = false;
        generalMastercallback('OK', m, f, 'PurchaseRequest', qry, 'PurchaseRequestItems'
            , 'purchaserequestitemupdate', 'purchaserequestiteminsert', 'purchaserequestitemdelete', 'Amount', null, purchaserequestshow, true, 'purchaserequest2purchase');
        if (!_Context.success)
            return;

        var more = '&RelatedID=' + f.MasterID + '&NextTaskID=' + v + '&UserID=' + _Context.userID + '&Type=' + WorkflowTypeEnum.PurchaseRequest
            + '&Msg=' + encodeURIComponent(taskMsg) + '&FileName=' + encodeURIComponent(f.FileName);
        if (runQuery('dotransition', 'TaskImpl', more)) {
            runQuery('purchaserequest2purchase', 'PurchaseRequest', '&MasterID=' + f.MasterID, true);
            $("#flexi").flexReload();
            return true;
        }
        return false;
    }

    if (!checkWorkflowStatus(f))
        return false;

    return newMastercallback(v, m, f, 'PurchaseRequest', qry, 'PurchaseRequestItems', 'purchaserequestitemupdate', 'purchaserequestiteminsert', 'purchaserequestitemdelete'
        , 'Amount', null, purchaserequestshow, false, 'purchaserequest2purchase');
}
function purchaserequestshow(id, celDiv, successCallback, desiredEntryTaskID) {
    var btns;
    if (id == -1) {
        btns = getDefaultButtons();
    }
    else {
        btns = getTaskButtons(WorkflowTypeEnum.PurchaseRequest, id, false, "storeforms", "purchaserequest");
    }

    _Context.id = id;
    var _url = 'get.aspx?op=GetHtml&table=PurchaseRequest&qd=purchaserequest&id=' + id;
    if (desiredEntryTaskID)
        _url += '&DesiredEntryTaskID=' + desiredEntryTaskID;
    _url += appendTime();
    showPopup(_url, purchaserequestshowcallback, btns, null, successCallback);
}

function requestshowcallback(v, m, f) {
    function checkNums() {
        var res = true;
        $('.numconfirmed').each(function (index, value) {
            if (res) {
                var id = $(this).attr('id'); // a_0
                id = '#r_' + id.substring(2); // a_0 --> r_0
                var confirmed = parseInt($(this).val(), 10);
                var requested = parseInt($(id).val(), 10);
                if (confirmed > requested)
                    res = false;
            }
        });
        if (!res)
            alert(Lang.RequestsJsM1);
        return res;
    }
    if (v == 'TaskImpls') {
        showTaskImpls(_Context.id, WorkflowTypeEnum.Request);
        return false;
    }
    if (v == 'ChangeLog') {
        showLogChange('Request', _Context.id, WorkflowTypeEnum.Request);
        return false;
    }
    if (v == 'Print') {
        printSubform();
        return false;
    }
    if (v == 'Cancel')
        return true;
    if (v == 'Excel') {
        topRows = Lang.Request;
        topRows += '\n' + Lang.Store + '\t' + f.StoreName + '\t' + Lang.RequestDate + '\t' + f.RequestDate;
        fillExcel('Request', 'requestitemexcel', 'RequestID=' + _Context.id, topRows);
        return false;
    }
    var qry = (_Context.id == -1) ? 'requestinsert' : 'requestupdate';
    if (v != 'OK' && v != 'New') {
        var taskMsg = f.ConfirmTaskMsg;
        //var taskMsg = prompt(Lang.ConfirmTaskMsg, '');
        //if (taskMsg == null)
        //    return false;

        _Context.success = false;
        generalMastercallback('OK', m, f, 'Request', qry, 'RequestItems'
            , 'requestitemupdate', 'requestiteminsert', 'requestitemdelete', 'StoreServiceID', null, requestshow, true);
        if (!_Context.success)
            return;

        var more = '&RelatedID=' + f.MasterID + '&NextTaskID=' + v + '&UserID=' + _Context.userID + '&Type=' + WorkflowTypeEnum.Request
            + '&Msg=' + encodeURIComponent(taskMsg) + '&FileName=' + encodeURIComponent(f.FileName);
        if (runQuery('dotransition', 'TaskImpl', more)) {
            $("#flexi").flexReload();
            return true;
        }
        return false;
    }

    if (!checkWorkflowStatus(f))
        return false;

    if (!checkNums(f))
        return false;

    return newMastercallback(v, m, f, 'Request', qry, 'RequestItems', 'requestitemupdate', 'requestiteminsert', 'requestitemdelete', 'StoreServiceID'
        , null, requestshow, null, 'requestextra');
}
function requestshow(id, celDiv, successCallback, desiredEntryTaskID) {
    var btns;
    if (id == -1) {
        btns = getDefaultButtons();
    }
    else {
        btns = getTaskButtons(WorkflowTypeEnum.Request, id, false, "storeforms", "request");
        btns[Lang.Excel] = 'Excel';
    }

    _Context.id = id;
    var _url = 'get.aspx?op=GetHtml&table=Request&qd=request&id=' + id;
    if (desiredEntryTaskID)
        _url += '&DesiredEntryTaskID=' + desiredEntryTaskID;
    _url += appendTime();
    showPopup(_url, requestshowcallback, btns, null, successCallback, true);
}

function purchaseshowcallback(v, m, f) {
    function checkDate(f) {
        //if (f.ReceiptDate != '' && f.Date != '' && compareDate(f.Date, f.ReceiptDate) == -1) {
        //    alert(Lang.PurchasesJsM1);
        //    return false;
        //}
        return true;
    }
    function checkDateIntoFinanceYear(f) {
        if (_Context.fyStart == '') {
            alert(Lang.NoCurYear);
            return false;
        }
        if ((f.ReceiptDate == '' || validateDateIntoFinanceYear(f.ReceiptDate))
            && (f.Date == '' || validateDateIntoFinanceYear(f.Date)))
            return true;
        if (!_Context.isAccountingVer)
            if (validateDateIntoFinanceYear(f.PayDate))
                return true;
        alert(Lang.PurchasesJsM2 + _Context.fyStart + " - " + _Context.fyEnd + Lang.PurchasesJsM3);
        return false;
    }
    function checkExpireDate(f) {
        var res = true;
        $('.ExpireDate').each(function (index, value) {
            var id = $(this).attr('id'); // a_0
            id = '#s_' + id.substring(2); // a_0 --> r_0
            var expiredate = $(this).val();
            if (expiredate == '') {
                if (_options.dateType != 'fa') {
                    $(this).val('21/03/2111');
                } else {
                    $(this).val('1490/01/01');
                }
            }
            else
                res = res && (compareDate(f.Date, expiredate) < 0);
        });
        if (!res)
            alert(Lang.PurchasesJsM4);

        return res;
    }
    function checkProductDate(f) {
        var res = true;
        $('.ProductionDate').each(function (index, value) {
            var id = $(this).attr('id'); // a_0
            var productdate = $(this).val();
            res = res && compareDate(productdate, f.Date) <= 0
                && compareDate(productdate, f.ReceiptDate) <= 0
                && compareDate(productdate, f.PayDate) <= 0;
        });
        if (!res)
            alert(Lang.PurchasesJsM5);
        return res;
    }
    if (v == 'TaskImpls') {
        showTaskImpls(_Context.id, WorkflowTypeEnum.Purchase);
        return false;
    }
    if (v == 'ChangeLog') {
        showLogChange('Purchase', _Context.id, WorkflowTypeEnum.Purchase);
        return false;
    }
    if (v == 'Print') {
        applyPrintFields('Purchase');
        printSubform();
        return false;
    }
    if (v == 'Cancel')
        return true;
    if (v == 'Documents') {
        if (_Context.id > 0) {
            var _url = 'list.aspx?table=PurchaseDocument&pid=' + _Context.id;
            popup(_url);
        }
        return false;
    }
    if (!checkProgramStatus())
        return false;

    if (v != 'New') {
        if (!checkDate(f))
            return false;
        if (!checkDateIntoFinanceYear(f))
            return false;
        if (!checkExpireDate(f))
            return false;
        //if (!checkProductDate(f))
        //    return false;
    }
    _Context.priceDate = f.ReceiptDate;
    if (v != 'OK' && v != 'New') {
        var valid = validateInputs('#detailForm', f);
        if (!valid)
            return false;
        var taskMsg = f.ConfirmTaskMsg;
        //if (!)
        //var taskMsg = prompt(Lang.ConfirmTaskMsg, '');
        //if (taskMsg == null)
        //    return false;
        // we need to save it before sending to next task, because the user makes changes and dosen't hit save
        $('.ProductionDate').attr('name', 'ProductionDate');
        $('.ExpireDate').attr('name', 'ExpireDate');
        // here is a bug: all changes must be done in a single request|transaction. fix it later
        generalMastercallback('OK', m, f, 'Purchase', 'purchaseupdate', 'PurchaseItems'
            , 'purchaseitemupdate', 'purchaseiteminsert', 'purchaseitemdelete', 'StoreServiceID', null, purchaseshow, true);
        if (!_Context.success)
            return false;
        var more = '&RelatedID=' + f.MasterID + '&NextTaskID=' + v + '&UserID=' + _Context.userID + '&Type=' + WorkflowTypeEnum.Purchase
            + '&Msg=' + encodeURIComponent(taskMsg) + '&FileName=' + encodeURIComponent(f.FileName);
        if (runQuery('dotransition', 'TaskImpl', more)) {
            setTimeout(function () { $("#flexi").flexReload(); }, 200);
            return true;
        }
        return false;
    }

    if (!checkWorkflowStatus(f))
        return false;

    var qry = (_Context.id == -1) ? 'purchaseinsert' : 'purchaseupdate';
    var extraQ = (_Context.id == -1) ? 'purchasenewextra' : 'purchaseextra';
    $('.ProductionDate').attr('name', 'ProductionDate');
    $('.ExpireDate').attr('name', 'ExpireDate');
    return newMastercallback(v, m, f, 'Purchase', qry, 'PurchaseItems'
        , 'purchaseitemupdate', 'purchaseiteminsert', 'purchaseitemdelete', 'StoreServiceName', null, purchaseshow, null, extraQ);
}
function purchaseshow(id, celDiv, successCallback, desiredEntryTaskID) {
    var btns;
    if (id == -1) {
        btns = getDefaultButtons(false);
    }
    else {
        //if (storeMode)
        btns = getTaskButtons(WorkflowTypeEnum.Purchase, id, false, "sales", "purchase");
        var numDocs = toInt(getServerValue("getnumpds", "PurchaseDocument", "ID=" + id), 0);
        if (numDocs > 0)
            btns[Lang.Docs + ' ' + numDocs] = 'Documents';
        else
            btns[Lang.Docs] = 'Documents';
        //else
        //{
        //    var cls = celDiv ? getColClass($(celDiv).closest('tr'), 11) : getGridRowClass("#flexi", 8);
        //    var wfStatus = getStatus(cls, 3);
        //    btns = wfStatus == 1 ? getCloseButtons() : getDefaultButtons(id != -1);
        //}
    }
    var fID = getServerValue('getfinanceyear', 'FinanceYear', null);
    if (fID == -1) {
        alert(Lang.NoActiveFinancialYear);
        return false;
    }
    _Context.id = id;
    var _url = 'get.aspx?op=GetHtml&table=Purchase&qd=purchase&id=' + id + '&FinanceYearID=' + fID;
    if (desiredEntryTaskID)
        _url += '&DesiredEntryTaskID=' + desiredEntryTaskID;
    _url += appendTime();
    showPopup(_url, purchaseshowcallback, btns, null, successCallback, true);
}

function storedraftshowcallback(v, m, f) {
    function checkDateIntoFinanceYear(f) {
        if (_Context.fyStart == '') {
            alert(Lang.NoCurYear);
            return false;
        }
        if (f.Date == '' || validateDateIntoFinanceYear(f.Date))
            return true;
        alert(Lang.PurchasesJsM2 + _Context.fyStart + " - " + _Context.fyEnd + Lang.PurchasesJsM3);
        return false;
    }
    function checkCredit(f) {
        var neuremain = 0;
        if (toInt(f.PayMethod, -1) == 2)
            neuremain = toFloat(fromMonetary(f.Total[f.Total.length - 1], f.FactorDate), 0);
        else if (toInt(f.PayMethod, -1) == 3)
            neuremain = toFloat(fromMonetary(f.OwedPrice, f.FactorDate), 0);
        var status = getPersonCredit(f.BuyerID, neuremain);
        if (status == -1) {
            Growl.Error(Lang.LowCredit, 3000);
            return false;
        }
        return true;
    }
    function checkNums(f) {
        var res = true;
        $('.delivered').each(function (index, value) {
            if (res) {
                var id = $(this).attr('id');
                id = id.substring(id.indexOf('_') + 1);
                var delivered = toInt($(this).val(), 0);
                var requested = toInt($("#r_" + id).val(), 0);
                if (delivered > requested)
                    res = false;
            }
        });
        if (!res)
            alert(Lang.StoreDraftsJsM1);
        return res;
    }
    if (v == 'TaskImpls') {
        showTaskImpls(_Context.id, WorkflowTypeEnum.StoreDraft);
        return false;
    }
    if (v == 'ChangeLog') {
        showLogChange("StoreDraft", _Context.id, WorkflowTypeEnum.StoreDraft);
        return false;
    }
    var _url;
    if (v == 'Print') {
        _url = 'print/storedraft/' + _Context.id;
        popup(_url);
        return false;
    }
    if (v == 'Cancel')
        return true;
    if (v == 'Documents') {
        if (_Context.id > 0) {
            _url = 'list.aspx?table=StoreDraftDocument&sid=' + _Context.id;
            popup(_url);
        }
        return false;
    }
    if (v != 'New') {
        if (!checkNums(f))
            return false;
        if (!checkDateIntoFinanceYear(f))
            return false;
        var insertStoreDraftWithBuyerOutBalance = toInt(getSettings('InsertStoreDraftWithBuyerOutBalance'), 0);
        if (insertStoreDraftWithBuyerOutBalance != 1)
            if (!checkCredit(f))
                return false;
    }
    if (!checkProgramStatus())
        return false;

    // expand all collapsed regions to enable validation
    $('.collapser').each(function () {
        $($(this).attr('target')).show();
    });    
    _Context.priceDate = f.FactorDate;
    if (v != 'OK' && v != 'New') {
        var valid = validateInputs('#detailForm', f);
        if (!valid)
            return false;
        var taskMsg = f.ConfirmTaskMsg;
        //var taskMsg = prompt(Lang.ConfirmTaskMsg, '');
        //if (taskMsg == null)
        //    return false;
        generalMasterscallback('OK', m, f, 'StoreDraft', 'storedraftupdate', 'StoreDraftItems,StoreDraftPerson'
            , 'storedraftitemupdate,storedraftpersonupdate ', 'storedraftiteminsert,storedraftpersoninsert', 'storedraftitemdelete,storedraftpersondelete', 'StoreServiceID,PartnerID', null, null, storedraftshow);
        if (!_Context.success)
            return false;
        var more = '&RelatedID=' + f.MasterID + '&NextTaskID=' + v + '&UserID=' + _Context.userID + '&Type=' + WorkflowTypeEnum.StoreDraft
            + '&Msg=' + encodeURIComponent(taskMsg) + '&FileName=' + encodeURIComponent(f.FileName);
        //alert(more);
        if (runQuery('dotransition', 'TaskImpl', more)) {
            setTimeout(function () { $("#flexi").flexReload(); }, 200);
            return true;
        }
        return false;
    }
    if (!checkWorkflowStatus(f))
        return false;

    function checkLabel() {
        var ok = true;
            $('.labelid').each(function () {
                var val = toInt($(this).val(), 0);
                if (val <= 0) {
                    ok = false;
                }
            });
            return ok;
    }
    if (f.LabelCheckBox) {
        if (!checkLabel()) {
        Growl.Error(Lang.StoreDraftLabelMsg, 4000);
        return false;
    }
    }
        var qry = (_Context.id == -1) ? 'storedraftinsert' : 'storedraftupdate';
        return newMasterscallback(v, m, f, 'StoreDraft', qry, 'StoreDraftItems, StoreDraftPerson'
                , 'storedraftitemupdate,storedraftpersonupdate', 'storedraftiteminsert,storedraftpersoninsert', 'storedraftitemdelete,storedraftpersondelete', 'StoreServiceID,PartnerID', "storedraftextra", null, storedraftshow);
}
function storedraftshow(id, celDiv, successCallback, desiredEntryTaskID, isTemp) {
    var btns;
    if (id == -1)
        btns = getDefaultButtons(false);
    else {
        btns = getTaskButtons(WorkflowTypeEnum.StoreDraft, id, false, "sales", "storedraft");
        var numDocs = toInt(getServerValue("getnumsds", "StoreDraftDocument", "ID=" + id), 0);
        if (numDocs > 0)
            btns[Lang.Docs + ' ' + numDocs] = 'Documents';
        else
            btns[Lang.Docs] = 'Documents';
    }
    _Context.id = id;
    var fID = getServerValue('getfinanceyear', 'FinanceYear', null);
    if (fID == -1) {
        alert(Lang.NoCurYear);
        return false;
    }
    var _url = 'get.aspx?op=GetHtml&table=StoreDraft&qd=storedraft&id=' + id + '&FinanceYearID=' + fID;
    if (desiredEntryTaskID)
        _url += '&DesiredEntryTaskID=' + desiredEntryTaskID;
    if (isTemp) {
        _url += '&IsTemp=' + 1;
    }
    _url += appendTime();
    showPopup(_url, storedraftshowcallback, btns, null, successCallback, true);
}

function refuseshowcallback(v, m, f) {
    function checkDateIntoFinanceYear(f) {
        if (_Context.fyStart == '') {
            alert(Lang.NoCurYear);
            return false;
        }
        if (validateDateIntoFinanceYear(f.RefuseDate))
            return true;
        alert(Lang.PurchaseJsM2 + _Context.fyStart + " - " + _Context.fyEnd + Lang.PurchaseJsM3);
        return false;
    }
    if (v == 'TaskImpls') {
        showTaskImpls(_Context.id, WorkflowTypeEnum.Refuse);
        return false;
    }
    if (v == 'ChangeLog') {
        showLogChange('Refuse', _Context.id, WorkflowTypeEnum.Refuse);
        return false;
    }
    if (v == 'Print') {
        applyPrintFields('Refuse');
        printSubform();
        return false;
    }
    if (v == 'Cancel')
        return true;

    if (!checkDateIntoFinanceYear(f))
        return false;

    if (!checkProgramStatus())
        return false;

    _Context.priceDate = f.RefuseDate;
    var qry = (_Context.id == -1) ? 'refuseinsert' : 'refuseupdate';
    if (v != 'OK' && v != 'New') {
        var valid = validateInputs('#detailForm', f);
        if (!valid)
            return false;
        var taskMsg = f.ConfirmTaskMsg;
        //var taskMsg = prompt(Lang.ConfirmTaskMsg, '');
        //if (taskMsg == null)
        //    return false;
        _Context.success = false;
        generalMastercallback('OK', m, f, 'Refuse', qry, 'RefuseItem'
            , 'refuseitemupdate'
            , 'refuseiteminsert'
            , 'refuseitemdelete'
            , 'StoreServiceID', null, refuseshow, true, 'refuseextra');
        if (!_Context.success)
            return false;
        var more = '&RelatedID=' + f.MasterID + '&NextTaskID=' + v + '&UserID=' + _Context.userID + '&Type=' + WorkflowTypeEnum.Refuse
            + '&Msg=' + encodeURIComponent(taskMsg) + '&FileName=' + encodeURIComponent(f.FileName);
        if (runQuery('dotransition', 'TaskImpl', more)) {
            setTimeout(function () { $("#flexi").flexReload(); }, 200);
            return true;
        }
        return false;
    }

    return newMastercallback(v, m, f, 'Refuse', qry, 'RefuseItem'
            , 'refuseitemupdate'
            , 'refuseiteminsert'
            , 'refuseitemdelete'
            , 'StoreServiceID', null, refuseshow, null, 'refuseextra');
}
function refuseshow(id, celDiv, successCallback, desiredEntryTaskID) {
    var btns;
    if (id == -1)
        btns = getDefaultButtons();
    else
        btns = getTaskButtons(WorkflowTypeEnum.Refuse, id, false, "sales", "refuse");
    _Context.id = id;
    var fID = getServerValue('getfinanceyear', 'FinanceYear', null);
    if (fID == -1) {
        alert(Lang.NoCurYear);
        return false;
    }
    var _url = 'get.aspx?op=GetHtml&table=Refuse&qd=refuse&id=' + id + '&FinanceYearID=' + fID;
    if (desiredEntryTaskID)
        _url += '&DesiredEntryTaskID=' + desiredEntryTaskID;
    _url += appendTime();
    showPopup(_url, refuseshowcallback, btns, null, successCallback, true);
}

function returnshowcallback(v, m, f) {
    if (v == 'TaskImpls') {
        showTaskImpls(_Context.id, WorkflowTypeEnum.Return);
        return false;
    }
    if (v == 'ChangeLog') {
        showLogChange('Returns', _Context.id, WorkflowTypeEnum.Return);
        return false;
    }
    var _url;
    if (v == 'Print') {
        _url = 'print/returns/' + _Context.id;
        popup(_url);
        return false;
    }
    if (v == 'Cancel')
        return true;

    _Context.priceDate = f.ReturnDate;
    var qry = (_Context.id == -1) ? 'returninsert' : 'returnupdate';
    if (v != 'OK' && v != 'New') {
        var taskMsg = f.ConfirmTaskMsg;
        _Context.success = false;
        generalMastercallback('OK', m, f, 'Returns', qry, 'ReturnItem'
            , 'returnitemupdate', 'returniteminsert', 'returnitemdelete', 'StoreServiceID', null, returnshow, true);
        if (!_Context.success)
            return false;
        var more = '&RelatedID=' + f.MasterID + '&NextTaskID=' + v + '&UserID=' + _Context.userID + '&Type=' + WorkflowTypeEnum.Return
            + '&Msg=' + encodeURIComponent(taskMsg) + '&FileName=' + encodeURIComponent(f.FileName);
        if (runQuery('dotransition', 'TaskImpl', more)) {
            setTimeout(function () { $("#flexi").flexReload(); }, 200);
            return true;
        }
        return false;
    }

    if (!checkWorkflowStatus(f))
        return false;

    if (!checkProgramStatus())
        return false;

    return newMastercallback(v, m, f, 'Returns', qry, 'ReturnItem'
            , 'returnitemupdate', 'returniteminsert', 'returnitemdelete', 'StoreServiceID', null, returnshow, null, 'returnextra');
}
function returnshow(id, celDiv, successCallback, desiredEntryTaskID) {
    var btns;
    if (id == -1) {
        btns = getDefaultButtons();
    }
    else {
        btns = getTaskButtons(WorkflowTypeEnum.Return, id, false, "sales", "return");
    }
    var fID = getServerValue('getfinanceyear', 'FinanceYear', null);
    if (fID == -1) {
        alert(Lang.NoCurYear);
        return false;
    }
    _Context.id = id;
    var _url = 'get.aspx?op=GetHtml&table=Returns&qd=return&id=' + id + '&FinanceYearID=' + fID;
    if (desiredEntryTaskID)
        _url += '&DesiredEntryTaskID=' + desiredEntryTaskID;
    _url += appendTime();
    showPopup(_url, returnshowcallback, btns, null, successCallback, true);
}

function transfershowcallback(v, m, f) {
    function checkDateIntoFinanceYear(f) {
        if (_Context.fyStart == '') {
            alert(Lang.NoCurYear);
            return false;
        }
        if (validateDateIntoFinanceYear(f.Date))
            return true;
        aler(Lang.PurchasesJsM2 + _Context.fyStart + " - " + _Context.fyEnd + Lang.PurchasesJsM3);
        return false;
    }
    function checkStores(f) {
        if (f.SourceStoreID != f.DestStoreID)
            return true;
        alert(Lang.TransfersJsM1);
        return false;
    }
    if (v == 'TaskImpls') {
        showTaskImpls(_Context.id, WorkflowTypeEnum.Transfer);
        return false;
    }
    if (v == 'ChangeLog') {
        showLogChange('Transfer', _Context.id, WorkflowTypeEnum.Transfer);
        return false;
    }
    if (v == 'Print') {
        printSubform();
        return false;
    }
    if (v == 'Cancel')
        return true;

    if (!checkDateIntoFinanceYear(f))
        return false;
    if (!checkStores(f))
        return false;

    if (v != 'OK' && v != 'New') {
        var taskMsg = Lang.ConfirmTaskMsg;
        //var taskMsg = prompt(Lang.ConfirmTaskMsg, '');
        //if (taskMsg == null)
        //    return false;

        var more = '&RelatedID=' + f.MasterID + '&NextTaskID=' + v + '&UserID=' + _Context.userID + '&Type=' + WorkflowTypeEnum.Transfer
            + '&Msg=' + encodeURIComponent(taskMsg);
        if (runQuery('dotransition', 'TaskImpl', more)) {
            $("#flexi").flexReload();
            return true;
        }
        return false;
    }

    var qry = (_Context.id == -1) ? 'transferinsert' : 'transferupdate';
    return newMastercallback(v, m, f, 'Transfer', qry, 'TransferItem', 'transferitemupdate', 'transferiteminsert', 'transferitemdelete'
        , 'StoreServiceID', null, transfershow);
}
function transfershow(id, celDiv, successCallback, desiredEntryTaskID) {
    var btns;
    if (id == -1)
        btns = getDefaultButtons();
    else
        btns = getTaskButtons(WorkflowTypeEnum.Transfer, id, false, "store", "transfer");
    _Context.id = id;
    var fID = getServerValue('getfinanceyear', 'FinanceYear', null);
    if (fID == -1) {
        alert(Lang.NoCurYear);
        return false;
    }
    var _url = 'get.aspx?op=GetHtml&table=Transfer&qd=transfer&id=' + id + '&FinanceYearID=' + fID;
    if (desiredEntryTaskID)
        _url += '&DesiredEntryTaskID=' + desiredEntryTaskID;
    _url += appendTime();
    showPopup(_url, transfershowcallback, btns, null, successCallback, true);
}

function buildordershowcallback(v, m, f) {
    if (v == 'TaskImpls') {
        showTaskImpls(_Context.id, WorkflowTypeEnum.BuildOrder);
        return false;
    }
    if (v == 'ChangeLog') {
        showLogChange('BuildOrder', _Context.id, WorkflowTypeEnum.BuildOrder);
        return false;
    }
    if (v == 'Print') {
        printSubform();
        return false;
    }
    if (v == 'Cancel')
        return true;
    var qry = (_Context.id == -1) ? 'buildorderinsert' : 'buildorderupdate';
    if (v != 'OK' && v != 'New') {
        var taskMsg = f.ConfirmTaskMsg;
        //var taskMsg = prompt(Lang.ConfirmTaskMsg, '');
        //if (taskMsg == null)
        //    return false;

        _Context.success = false;
        generalMastercallback('OK', m, f, 'BuildOrder', qry, 'BuildOrderItem'
            , 'buildorderitemupdate', 'buildorderiteminsert', 'buildorderitemdelete', 'StoreServiceID', null, buildordershow, true);
        if (!_Context.success)
            return;

        var more = '&RelatedID=' + f.MasterID + '&NextTaskID=' + v + '&UserID=' + _Context.userID + '&Type=' + WorkflowTypeEnum.BuildOrder
            + '&Msg=' + encodeURIComponent(taskMsg) + '&FileName=' + encodeURIComponent(f.FileName);
        if (runQuery('dotransition', 'TaskImpl', more)) {
            $("#flexi").flexReload();
            return true;
        }
        return false;
    }

    if (!checkWorkflowStatus(f))
        return false;


    return newMastercallback(v, m, f, 'BuildOrder', qry, 'BuildOrderItem', 'buildorderitemupdate', 'buildorderiteminsert', 'buildorderitemdelete', 'StoreServiceID'
        , null, buildordershow);
}
function buildordershow(id, celDiv, successCallback, desiredEntryTaskID) {
    var btns;
    if (id == -1) {
        btns = getDefaultButtons();
    }
    else {
        btns = getTaskButtons(WorkflowTypeEnum.BuildOrder, id, false, "storeforms", "buildorder");
    }

    _Context.id = id;
    var _url = 'get.aspx?op=GetHtml&table=BuildOrder&qd=buildorder&id=' + id;
    if (desiredEntryTaskID)
        _url += '&DesiredEntryTaskID=' + desiredEntryTaskID;
    _url += appendTime();
    showPopup(_url, buildordershowcallback, btns, null, successCallback, true);
}

/*
$(function () {
    window.onbeforeunload = function (event) {
        var message = 'Important: Please click on \'Save\' button to leave this page.';
        if (typeof event == 'undefined') {
            event = window.event;
        }
        if (event) {
            event.returnValue = message;
        }
        return message;
    };
});*/

// -----------------------------------
// -----------------------------------
// Banking Started
// -----------------------------------
// -----------------------------------
var PosTimerTick;
var PosTimerIsOn = 0;
function PosFormat(data)
{
    if (data.indexOf("<ResponseCode>0</ResponseCode>") > 0)
        return "تراکنش با موفقیت انجام پذیرفت";
    if (data.indexOf("<ResponseCode>1</ResponseCode>") > 0)
        return "عدم دریافت پاسخ در زمان مناسب";
    if (data.indexOf("<ResponseCode>2</ResponseCode>") > 0)
        return "داده نامعتبر";
    if (data.indexOf("<ResponseCode>3</ResponseCode>") > 0)
        return "کنسل شدن عملیات توسط مشتری";
    if (data.indexOf("<ResponseCode>5</ResponseCode>") > 0)
        return "عدم ارتباط با مرکز";
    if (data.indexOf("<ResponseCode>19</ResponseCode>") > 0)
        return "تراکنش را مجددا تکرار کنید";
    if (data.indexOf("<ResponseCode>51</ResponseCode>") > 0)
        return "موجودی کافی نیست";
    if (data.indexOf("<ResponseCode>55</ResponseCode>") > 0)
        return "رمز کارت اشتباه است";
    if (data.indexOf("<ResponseCode>57</ResponseCode>") > 0)
        return "دارنده کارت مجوز انجام چنين تراکنشي را ندارد";
    if (data.indexOf("<ResponseCode>61</ResponseCode>") > 0)
        return "سقف مبلغ تراکنش برداشت وجه رعايت نشده است";
    if (data.indexOf("<ResponseCode>63</ResponseCode>") > 0)
        return "خطای امنیتی";
    if (data.indexOf("<ResponseCode>68</ResponseCode>") > 0)
        return "پاسخ در زمان مناسب از مرکز دریافت نشد";
    if (data.indexOf("<ResponseCode>75</ResponseCode>") > 0)
        return "دفعات ورود رمز اشتباه بیشتر از حد مجاز";
    if (data.indexOf("<ResponseCode>78</ResponseCode>") > 0)
        return "کارت غير فعال شده است";
    if (data.indexOf("<ResponseCode>84</ResponseCode>") > 0)
        return "خطای صادر کننده";
    if (data.indexOf("<ResponseCode>90</ResponseCode>") > 0)
        return "در حال تغيير دوره مالي";
    if (data.indexOf("<ResponseCode>94</ResponseCode>") > 0)
        return "ارسال تکراري تراکنش بوجود آمده است";
    return data;
}
function PosTick() {
    var jj = $.getJSON("http://" + _Context.POSUrl + "/GetXmlRecieveState?callback=?",
    function (data) {

        if (data.toString() != "") {
            if (data.toString() == "XMLRecive") {
                var jj = $.getJSON("http://" + _Context.POSUrl + "/GetXmlRecieve?callback=?",
                function (data) {
                    if (data.toString().indexOf("<ResponseCode>0</ResponseCode>") > 0)
                        Growl.Notice(PosFormat(data.toString()), 3000);
                    else
                        Growl.Error(Lang.POSError + " : " + PosFormat(data.toString()), 3000);
                    //document.getElementById('txtRecieve').value = data.toString();
                });
            }
            else {
                var jj = $.getJSON("http://" + _Context.POSUrl + "/GetXmlError?callback=?",
                function (data) {
                    Growl.Error(Lang.POSError + " : " + PosFormat(data.toString()), 4000);
                });
            }
            PosStopTick();
        }
        else {
            PosTimerTick = setTimeout("PosTick()", 500);
        }
    });
}

function PosStartTimer() {
    if (!PosTimerIsOn) {
        PosTimerIsOn = 1;
        PosTick();
    }
}

function PosStopTick() {
    clearTimeout(PosTimerTick);
    PosTimerIsOn = 0;
}
function PosOpenCOM() {
    //Init();
    var jj = $.getJSON("http://" + _Context.POSUrl + "/ClosePort?callback=?",
    function (data) {
        if (data)
            Growl.Notice(data.toString(), 2000);
    });

    var jj = $.getJSON("http://" + _Context.POSUrl + "/SetPort?PortName=" + encodeURIComponent(_Context.POSPort) + "&callback=?",
    function (data) {
        if (data)
            Growl.Notice(data.toString(), 2000);
    });

    var jj = $.getJSON("http://" + _Context.POSUrl + "/OpenPort?callback=?",
    function (data) {
        if (data)
            Growl.Notice(data.toString(), 2000);
    });
}
function PosCloseCOM() {
    var jj = $.getJSON("http://" + _Context.POSUrl + "/ClosePort?callback=?",
    function (data) {
        if (data)
            Growl.Notice(data.toString(), 2000);
    });
}

function PosSendToCOM(val) {
    var msg = "<Amount1>" + val + "</Amount1><TotalFee>" + val + "</TotalFee><PrgVer>v2.0.50727</PrgVer><SegmentPurchase>false</SegmentPurchase>";
    var ss = encodeURIComponent(msg);
    var jj = $.getJSON("http://" + _Context.POSUrl + "/SendToCOM?xml=" + ss + "&callback=?",
    function (data) {
        if (data) {
            PosStartTimer();
        }
        else {
            var jj = $.getJSON("http://" + _Context.POSUrl + "/GetXmlError?callback=?",
                function (data) {
                    if (data)
                        Growl.Error(Lang.POSError + " : " + data.toString(), 4000);
                });
        }
    });
}
// -----------------------------------
// -----------------------------------
// Banking Ended 
// -----------------------------------
// -----------------------------------

function copyTextToClipboard(text) {
    var textArea = document.createElement("textarea");

    //
    // *** This styling is an extra step which is likely not required. ***
    //
    // Why is it here? To ensure:
    // 1. the element is able to have focus and selection.
    // 2. if element was to flash render it has minimal visual impact.
    // 3. less flakyness with selection and copying which **might** occur if
    //    the textarea element is not visible.
    //
    // The likelihood is the element won't even render, not even a flash,
    // so some of these are just precautions. However in IE the element
    // is visible whilst the popup box asking the user for permission for
    // the web page to copy to the clipboard.
    //

    // Place in top-left corner of screen regardless of scroll position.
    textArea.style.position = 'fixed';
    textArea.style.top = 0;
    textArea.style.left = 0;

    // Ensure it has a small width and height. Setting to 1px / 1em
    // doesn't work as this gives a negative w/h on some browsers.
    textArea.style.width = '2em';
    textArea.style.height = '2em';

    // We don't need padding, reducing the size if it does flash render.
    textArea.style.padding = 0;

    // Clean up any borders.
    textArea.style.border = 'none';
    textArea.style.outline = 'none';
    textArea.style.boxShadow = 'none';

    // Avoid flash of white box if rendered for any reason.
    textArea.style.background = 'transparent';


    textArea.value = text;

    document.body.appendChild(textArea);

    textArea.select();

    try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Copying text command was ' + msg);
    } catch (err) {
        console.log('Oops, unable to copy');
    }

    document.body.removeChild(textArea);
}

function fa2EnNumber(str) {
    var fa = "٠١٢٣٤٥٦٧٨٩";
    var en = "0123456789";
    if (!str)
        return str;
    str = str.toString();
    var res = "";
    for (var i = 0; i < str.length; i++) {
        var index = fa.indexOf(str.charAt(i));
        if (index >= 0)
            res += en.charAt(index);
        else
            res += str.charAt(i);
    }
    return res;
}

function convertToScondMonetary(elm) {
    var val = $(elm).val();
    if (val.toString().indexOf('%') < 0) {
        val = toFloat($(elm).val(), 0);
        $('#ConvertedPrice').val(numberWithCommas((val * _Context.SecondFactorPrice).toFixed(_Context.decimalPoints)));
    } else {
        $('#ConvertedPrice').val(0);
    }
}
$(function () {
    $jQuery1_9(document).on('blur', '.amount,[class*="number"],[class*="integer"]', function () {
        $(this).val(fa2EnNumber($(this).val()));
    });
});

// strings including commas are treated as many values on the server
// ex: comments=abc,def  --> server thinks it has two fields. 
// the problem mainly occures when a Master and its details has the same field, like StoreDraft.Comments and StoreDraftItems.Comments
// or when a field in the detail has commas
function textInputsCommaRemove(form) {
    form = form || '#detailForm';

    $('input', form).each(function () {
        var me = $(this);
        var val = me.val();
        if (val && val.indexOf(',') > -1)
        {
            val = val.toString().replace(/,/g, "iAmcOMMA");
            me.val(val);
        }
    });
}
function textInputsCommaSet(form) {
    form = form || '#detailForm';

    $('input', form).each(function () {
        var me = $(this);
        var val = me.val();
        if (val && val.indexOf('iAmcOMMA') > -1) {
            val = val.toString().replace(/iAmcOMMA/g, ",");
            me.val(val);
        }
    });
}