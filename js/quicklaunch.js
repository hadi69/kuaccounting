﻿var allQuickLaunchUrls = [
    {
        access: "accounting,topic",
        iconClass: "accounting-topicaccount",
        label: Lang.TopicAccount,
        value: Lang.TopicAccount,
        href:"topics.aspx"
    },
    {
        access: "accounting,topicgroup",
        iconClass: "accounting-topicgroup",
        label: Lang.TopicGroup,
        value: Lang.TopicGroup,
        href:"list.aspx?table=TopicGroup&menu=accountinga"
    },
    {
        access: "accounting,voucher",
        iconClass: "accounting-voucher",
        label: Lang.DocumentAccounting,
        value: Lang.DocumentAccounting,
        href:"list.aspx?table=DocumentAccounting"
    },
    {
        access: "accounting,chart",
        iconClass: "accounting-personnelgroup",
        label: Lang.PersonnelGroup,
        value: Lang.PersonnelGroup,
        href:"list.aspx?table=PersonnelGroup&menu=accountinga"
    },
    {
        access: "accounting,person",
        iconClass: "accounting-person",
        label: Lang.Persons,
        value: Lang.Persons,
        href:"list.aspx?table=Personnel&view=persons"
    },
    {
        access: "accounting,project",
        iconClass: "system-ward",
        label: Lang.Project,
        value: Lang.Project,
        href: "list.aspx?table=Ward&view=projects&menu=accountinga"
    },
    {
        access: "accounting,receivefromcustomer",
        iconClass: "admin-start",
        label: Lang.ReceiveFromCustomer,
        value: Lang.ReceiveFromCustomer,
        href: "ReceiveFromCustomer.aspx"
    },
    {
        access: "admin,open",
        iconClass: "admin-opentopic",
        label: Lang.ActivateYear,
        value: Lang.ActivateYear,
        href:"OpenTopics.aspx"
    },
    {
        access: "admin,close",
        iconClass: "admin-closetopic",
        label: Lang.CloseTopics,
        value: Lang.CloseTopics,
        href:"CloseTopics.aspx"
    },
    {
        access: "admin,storehandling",
        iconClass: "admin-start",
        label: Lang.CloseStore,
        value: Lang.CloseStore,
        href: "list.aspx?table=StoreHandling"
    },
    {
        access: "admin,financeyear",
        iconClass: "admin-financeyear",
        label: Lang.FinanceYear,
        value: Lang.FinanceYear,
        href:"list.aspx?table=FinanceYear"
    },
    {
        access: "admin,database",
        iconClass: "admin-dbadmin",
        label: Lang.DataBaseAdmin,
        value: Lang.DataBaseAdmin,
        href:"dbadmin.aspx"
    },
    {
        access: "sales,grouping",
        iconClass: "admin-grouping",
        label: Lang.Grouping,
        value: Lang.Grouping,
        href:"list.aspx?table=Grouping&menu=basicsa"
    },
    {
        access: "admin,roles",
        iconClass: "admin-roles",
        label: Lang.Permission,
        value: Lang.Permission,
        href:"list.aspx?table=roles"
    },
    {
        access: "admin,users",
        iconClass: "admin-users",
        label: Lang.Users,
        value: Lang.Users,
        href:"list.aspx?table=users"
    },
    {
        access: "admin,config",
        iconClass: "system-config",
        label: Lang.Settings,
        value: Lang.Settings,
        href: "list.aspx?table=config"
    },
    {
        access: "admin,smsqueue",
        iconClass: "admin-contactcenter",
        label: Lang.SmsQueue,
        value: Lang.SmsQueue,
        href: "list.aspx?table=items"
    },
    {
        access: "admin,start",
        iconClass: "admin-start",
        label: Lang.Start,
        value: Lang.Start,
        href: "start.aspx"
    },
    {
        access: "distribution,visitorplan",
        iconClass: "admin-visitorplan",
        label: Lang.VisitorPlan,
        value: Lang.VisitorPlan,
        href: "list.aspx?table=VisitorPlan"
    },
    {
        access: "distribution,map",
        iconClass: "distribution-map",
        label: Lang.OnlineMap,
        value: Lang.OnlineMap,
        href: "map.aspx"
    },
    {
        access: "distribution,tabletmsg",
        iconClass: "distribution-contactcenter",
        label: Lang.TabletMsg,
        value: Lang.TabletMsg,
        href: "list.aspx?table=TabletMsg"
    },
    {
        access: "distribution,tabletworkflow",
        iconClass: "sprites-workflow-32",
        label: Lang.TabletWorkFlow,
        value: Lang.TabletWorkFlow,
        href: "list.aspx?table=TabletWorkFlow"
    },
    {
        access: "distribution,assessment",
        iconClass: "sprites-workflow-32",
        label: Lang.Assessment,
        value: Lang.Assessment,
        href: "list.aspx?table=Assessment"
    },
    {
        access: "distribution,doassessment",
        iconClass: "sprites-workflow-32",
        label: Lang.DoAssessment,
        value: Lang.DoAssessment,
        href: "DoAssessment.aspx"
    },
    {
        access: "distribution,calculatepursant",
        iconClass: "sprites-workflow-32",
        label: Lang.CalculatePursant,
        value: Lang.CalculatePursant,
        href: "CalculatePursant.aspx"
    },
    {
        access: "distribution,loaddistributecheckout",
        iconClass: "sprites-accountcheckout-32",
        label: Lang.LoadDistributeCheckOut,
        value: Lang.LoadDistributeCheckOut,
        href: "list.aspx?table=StoreDraft&view=loaddistributecheckout"
    },
    {
        access: "distribution,loaddistribute",
        iconClass: "sprites-performancereport-32",
        label: Lang.LoadDistribute,
        value: Lang.LoadDistribute,
        href: "LoadDistribut.aspx"
    },
    {
        access: "distribution,hotsalecheckout",
        iconClass: "sprites-accountcheckout-32",
        label: Lang.HotSaleCheckout,
        value: Lang.HotSaleCheckout,
        href: "list.aspx?table=VisitorPlanDayService&view=hotsalecheckout"
    },
    {
        access: "bank,bankaccount",
        iconClass: "bank-bankaccount",
        label: Lang.BankAccount,
        value: Lang.BankAccount,
        href:"list.aspx?table=BankAccount"
    },
    {
        access: "bank,bankaccountcheck",
        iconClass: "bank-bankaccountcheck",
        label: Lang.BankAccountCheck,
        value: Lang.BankAccountCheck,
        href: "list.aspx?table=BankAccountCheck"
    },
    {
        access: "bank,loan",
        iconClass: "bank-loans",
        label: Lang.Loans,
        value: Lang.Loans,
        href:"list.aspx?table=Loan"
    },
    {
        access: "bank,checksshops",
        iconClass: "bank-check",
        label: Lang.AutoChecksRegister,
        value: Lang.AutoChecksRegister,
        href:"list.aspx?table=Checks&view=checksshops&qd=checksshop"
    },
    {
        access: "bank,paychecksshops",
        iconClass: "bank-paycheck",
        label: Lang.AutoPayChecksRegister,
        value: Lang.AutoPayChecksRegister,
        href: "list.aspx?table=PayCheck&view=checksshops&qd=checksshop"
    },
    {
        access: "bank,paycheck",
        iconClass: "bank-paycheck",
        label: Lang.PayCheck,
        value: Lang.PayCheck,
        href:"list.aspx?table=PayCheck"
    },
    {
        access: "bank,checks",
        iconClass: "bank-checks",
        label: Lang.ReceivedChecks,
        value: Lang.ReceivedChecks,
        href: "list.aspx?table=Checks"
    },
    {
        access: "bank,guaranteechecks",
        iconClass: "bank-checks",
        label: Lang.GuaranteeChecks,
        value: Lang.GuaranteeChecks,
        href: "list.aspx?table=Checks&view=guaranteecheckss&qd=checksshop"
    },
    {
        access: "bank,bankprint",
        iconClass: "bank-bankprint",
        label: Lang.PrintTemplate,
        value: Lang.PrintTemplate,
        href:"list.aspx?table=BankPrint"
    },
    {
        access: "distribution,offers",
        iconClass: "distribution-offers",
        label: Lang.Offers,
        value: Lang.Offers,
        href:"list.aspx?table=Offers&menu=basicsa"
    },
    {
        access: "distribution,groupsearch",
        iconClass: "distribution-groupsearch",
        label: Lang.GroupSearch,
        value: Lang.GroupSearch,
        href:"list.aspx?table=Grouping&view=groupsearch&menu=basicsa"
    },
    {
        access: "distribution,awards",
        iconClass: "distribution-awards",
        label: Lang.Awards,
        value: Lang.Awards,
        href:"list.aspx?table=Awards&view=awards&menu=basicsa"
    },
    {
        access: "distribution,visitorsperformancereport",
        iconClass: "distribution-performancereport",
        label: Lang.VisitorsPerformanceReport,
        value: Lang.VisitorsPerformanceReport,
        href:"list.aspx?table=accreport"
    },
    {
        access: "distribution,visitorfixedroutes",
        iconClass: "distribution-performancereport",
        label: Lang.VisitorFixedRoutes,
        value: Lang.VisitorFixedRoutes,
        href: "list.aspx?table=VisitorFixedRoutes"
    },
    {
        access: "distribution,cooperationtype",
        iconClass: "system-cooperationtype",
        label: Lang.CooperationType,
        value: Lang.CooperationType,
        href: "list.aspx?table=CooperationType"
    },
    {
        access: "distribution,holidays",
        iconClass: "system-holidays",
        label: Lang.Holidays,
        value: Lang.Holidays,
        href: "list.aspx?table=holidays&view=holidays"
    },
    {
        access: "distribution,dailyplan",
        iconClass: "visitor-dailyplan",
        label: Lang.DailyPlan,
        value: Lang.DailyPlan,
        href: "list.aspx?table=VisitorCustomersStatus&view=visitorcustomersstatuss"
    },
    {
        access: "distribution,performancereport",
        iconClass: "visitor-performancereport",
        label: Lang.PerformanceReport,
        value: Lang.PerformanceReport,
        href: "help/notfound.htm"
    },
    {
        access: "distribution,visitoroffers",
        iconClass: "visitor-visitoroffers",
        label: Lang.VisitorOffers,
        value: Lang.VisitorOffers,
        href: "help/notfound.htm"
    },
    {
        access: "distribution,contactcenter",
        iconClass: "visitor-contactcenter",
        label: Lang.ContactCenter,
        value: Lang.ContactCenter,
        href: "help/notfound.htm"
    },
    {
        access: "accounting,equipment",
        iconClass: "system-producttype",
        label: Lang.Equipment,
        value: Lang.Equipment,
        href: "list.aspx?table=Equipment&menu=accountinga"
    },
    {
        access: "reports,report",
        iconClass: "report-accreport",
        label: Lang.CreatAccountingReport,
        value: Lang.CreatAccountingReport,
        href: "list.aspx?table=accreport"
    },
    {
        access: "reports,mybook",
        iconClass: "report-mybook",
        label: Lang.MyBook,
        value: Lang.MyBook,
        href:"reports/accounting/MyBook.aspx"
    },
    {
        access: "reports,personbook",
        iconClass: "report-personbook",
        label: Lang.PersonBook,
        value: Lang.PersonBook,
        href:"reports/accounting/PersonBook.aspx"
    },
    {
        access: "reports,fundreport",
        iconClass: "report-fundreport",
        label: Lang.FundReport,
        value: Lang.FundReport,
        href: "reports/accounting/FundReport.aspx"
    },
    {
        access: "reports,cardex",
        iconClass: "report-cardex",
        label: Lang.Cardex,
        value: Lang.Cardex,
        href:"reports/accounting/Cardex.aspx"
    },
    {
        access: "reports,report1",
        iconClass: "report-report1",
        label: Lang.Report1,
        value: Lang.Report1,
        href:"reports/accounting/Report1.aspx"
    },
    {
        access: "reports,totalbook",
        iconClass: "report-totalbook",
        label: Lang.TotalBook,
        value: Lang.TotalBook,
        href:"reports/accounting/TotalBook.aspx"
    },
    {
        access: "reports,paperbook",
        iconClass: "report-paperbook",
        label: Lang.PaperBook,
        value: Lang.PaperBook,
        href:"reports/accounting/PaperBook.aspx"
    },
    {
        access: "reports,functionalbalance",
        iconClass: "report-functionalbalance",
        label: Lang.MonthlyOperationsBalance,
        value: Lang.MonthlyOperationsBalance,
        href:"reports/accounting/FunctionalBalance.aspx?level=0"
    },
    {
        access: "reports,expbalance",
        iconClass: "report-expbalance",
        label: Lang.ExpBalance,
        value: Lang.ExpBalance,
        href:"reports/accounting/ExpBalance.aspx"
    },
    {
        access: "reports,expbalance8",
        iconClass: "report-expbalance8",
        label: Lang.ExpBalance8,
        value: Lang.ExpBalance8,
        href:"reports/accounting/ExpBalance8.aspx"
    },
    {
        access: "reports,expbalancesimple",
        iconClass: "report-expbalance",
        label: Lang.ExpbalanceSimple,
        value: Lang.ExpbalanceSimple,
        href: "reports/accounting/ExpbalanceSimple.aspx"
    },
    {
        access: "reports,expbalancebaseon",
        iconClass: "report-expbalance",
        label: Lang.ExpbalanceBaseOn,
        value: Lang.ExpbalanceBaseOn,
        href: "reports/accounting/ExpbalanceBaseOn.aspx"
    },
    {
        access: "reports,salereports",
        iconClass: "report-report1",
        label: Lang.SaleReports,
        value: Lang.SaleReports,
        href: "reports/accounting/SaleReports.aspx"
    },
    {
        access: "reports,missedvoucher",
        iconClass: "report-report1",
        label: Lang.MissedVouchers,
        value: Lang.MissedVouchers,
        href: "reports/accounting/vouchersmissed.aspx"
    },
    {
        access: "reports,servicereport",
        iconClass: "report-report1",
        label: Lang.ServiceReport,
        value: Lang.ServiceReport,
        href: "reports/accounting/services.aspx"
    },
    {
        access: "reports,shopprofit",
        iconClass: "report-report1",
        label: Lang.ShopProfit,
        value: Lang.ShopProfit,
        href: "reports/accounting/ShopProfit.aspx"
    },
    {
        access: "reports,distreport",
        iconClass: "report-salereports",
        label: Lang.DistReport,
        value: Lang.DistReport,
        href: "reports/accounting/DistReport.aspx"
    },
    {
        access: "reports,trustandinternalreport",
        iconClass: "report-salereports",
        label: Lang.TrustAndInternalReport,
        value: Lang.TrustAndInternalReport,
        href: "reports/accounting/TrustAndInternalReport.aspx"
    },
    {
        access: "reports,commoditysalereport",
        iconClass: "report-salereports",
        label: Lang.CommoditySaleReport,
        value: Lang.CommoditySaleReport,
        href: "reports/accounting/CommoditySaleReport.aspx"
    },
    {
        access: "reports,commoditysalereport2",
        iconClass: "report-salereports",
        label: Lang.CommoditySaleReport2,
        value: Lang.CommoditySaleReport2,
        href: "reports/accounting/CommoditySaleReport2.aspx"
    },
    {
        access: "reports,commoditybuyreport",
        iconClass: "report-salereports",
        label: Lang.CommodityBuyReport,
        value: Lang.CommodityBuyReport,
        href: "reports/accounting/CommodityBuyReport.aspx"
    },
    {
        access: "reports,supportreport",
        iconClass: "report-salereports",
        label: Lang.SupportReport,
        value: Lang.SupportReport,
        href: "reports/accounting/SupportReport.aspx"
    },
    {
        access: "reports,saleadminreport",
        iconClass: "sprites-topics-32",
        label: Lang.SaleAdminReport,
        value: Lang.SaleAdminReport,
        href: "reports/accounting/SaleAdminReport.aspx"
    },
    {
        access: "reports,financesheet",
        iconClass: "sprites-topics-32",
        label: Lang.FinanceSheet,
        value: Lang.FinanceSheet,
        href: "reports/accounting/FinanceSheet.aspx"
    },
    {
        access: "reports,topic",
        iconClass: "sprites-topics-32",
        label: Lang.TopicAccountReport,
        value: Lang.TopicAccountReport,
        href: "reports/accounting/treeremain.aspx"
    },
    {
        access: "reports,topicbalance",
        iconClass: "sprites-topics-32",
        label: Lang.TopicBalance,
        value: Lang.TopicBalance,
        href: "reports/accounting/TopicBalance.aspx"
    },
    {
        access: "accounting,reportpersons",
        iconClass: "sprites-person-32",
        label: Lang.ReportPersons,
        value: Lang.ReportPersons,
        href: "reports/accounting/persons.aspx"
    },
    {
        access: "sales,purchaseinvoice",
        iconClass: "sales-purchase",
        label: Lang.SalesPurchase,
        value: Lang.SalesPurchase,
        href:"list.aspx?table=Purchase&menu=accountinga"
    },
    {
        access: "sales,salesinvoice",
        iconClass: "sales-salesinvoice",
        label: Lang.SalesInvoice,
        value: Lang.SalesInvoice,
        href:"list.aspx?table=StoreDraft&menu=accountinga"
    },
    {
        access: "sales,refuseinvoice",
        iconClass: "sales-refuseinvoice",
        label: Lang.RefuseInvoice,
        value: Lang.RefuseInvoice,
        href:"list.aspx?table=Refuse&menu=accountinga"
    },
    {
        access: "sales,returninvoice",
        iconClass: "sales-returninvoice",
        label: Lang.ReturnInvoice,
        value: Lang.ReturnInvoice,
        href:"list.aspx?table=Returns&menu=accountinga"
    },
    {
        access: "sales,browse",
        iconClass: "sales-browse",
        label: Lang.Browse,
        value: Lang.Browse,
        href:"list.aspx?table=Grouping&view=browse&menu=basicsa"
    },
    {
        access: "sales,shoppurchaseinvoice",
        iconClass: "sales-purchase",
        label: Lang.ShopSalesPurchase,
        value: Lang.ShopSalesPurchase,
        href: "list.aspx?table=Purchase&menu=accountinga&view=shoppurchases"
    },
    {
        access: "sales,shopsalesinvoice",
        iconClass: "sales-salesinvoice",
        label: Lang.ShopSalesInvoice,
        value: Lang.ShopSalesInvoice,
        href: "list.aspx?table=StoreDraft&menu=accountinga&view=shopstoredrafts"
    },
    {
        access: "sales,refuseinvoice",
        iconClass: "sales-refuseinvoice",
        label: Lang.ShopRefuseInvoice,
        value: Lang.ShopRefuseInvoice,
        href: "list.aspx?table=Refuse&menu=accountinga&view=shoprefuses"
    },
    {
        access: "sales,returninvoice",
        iconClass: "sales-returninvoice",
        label: Lang.ShopReturnInvoice,
        value: Lang.ShopReturnInvoice,
        href: "list.aspx?table=Returns&menu=accountinga&view=shopreturns"
    },
    {
        access: "sales,accountcheckout",
        iconClass: "sales-accountcheckout",
        label: Lang.AccountCheckout,
        value: Lang.AccountCheckout,
        href: "list.aspx?table=StoreDraft&view=accountcheckout&menu=accountinga"
    },
    {
        access: "sales,coding",
        iconClass: "admin-grouping",
        label: Lang.Coding,
        value: Lang.Coding,
        href: "list.aspx?table=Coding"
    },
    {
        access: "sales,servicepricing",
        iconClass: "admin-start",
        label: Lang.ServicePricing,
        value: Lang.ServicePricing,
        href: "servicepricing.aspx"
    },
    {
        href:"list.aspx?table=Gender&menu=salarysa",
        access: "salary,gender",
        label: Lang.Gender,
        value: Lang.Gender
    },
    {
        href:"list.aspx?table=Country&menu=salarysa",
        access: "salary,country",
        label: Lang.Country,
        value: Lang.Country
    },
    {
        href:"list.aspx?table=Province&menu=salarysa",
        access: "salary,province",
        label: Lang.ProvinceName,
        value: Lang.ProvinceName
    },
    {
        href:"list.aspx?table=City&menu=salarysa",
        access: "salary,city",
        label: Lang.City,
        value: Lang.City
    },
    {
        href:"list.aspx?table=SalaryResource&menu=salarysa",
        access: "salary,salaryresource",
        label: Lang.CashSource,
        value: Lang.CashSource
    },
    {
        href:"list.aspx?table=PaymentSystem&menu=salarysa",
        access: "salary,paymentsystem",
        label: Lang.PaymentSystem,
        value: Lang.PaymentSystem
    },
    {
        href:"list.aspx?table=EducationLevel&menu=salarysa",
        access: "salary,educationlevel",
        label: Lang.EducationDocument,
        value: Lang.EducationDocument
    },
    {
        href:"list.aspx?table=FieldOFStudy&menu=salarysa",
        access: "salary,fieldofstudy",
        label: Lang.DegreeField,
        value: Lang.DegreeField
    },
    {
        href:"list.aspx?table=Category&menu=salarysa",
        access: "salary,category",
        label: Lang.Category,
        value: Lang.Category
    },
    {
        href:"list.aspx?table=Field&menu=salarysa",
        access: "salary,field",
        label: Lang.DefineFields,
        value: Lang.DefineFields
    },
    {
        href:"list.aspx?table=Post&menu=salarysa",
        access: "salary,post",
        label: Lang.OrganizationalPosition,
        value: Lang.OrganizationalPosition
    },
    {
        href:"list.aspx?table=OrganizationUnit&menu=salarysa",
        access: "salary,organizationunit",
        label: Lang.OrganizationUnit,
        value: Lang.OrganizationUnit
    },
    {
        href:"list.aspx?table=OrderType&menu=salarysa",
        access: "salary,ordertype",
        label: Lang.OrderTypes,
        value: Lang.OrderTypes
    },
    {
        href:"list.aspx?table=ZYearAdd&menu=salarysa",
        access: "salary,zyearadd",
        label: Lang.ZYearAdd,
        value: Lang.ZYearAdd
    },
    {
        href:"list.aspx?table=ZBYearAdd&menu=salarysa",
        access: "salary,zbyearadd",
        label: Lang.ZBYearAdd,
        value: Lang.ZBYearAdd
    },
    {
        href:"list.aspx?table=SFVijeh&menu=salarysa",
        access: "salary,sfvijeh",
        label: Lang.SFVijeh,
        value: Lang.SFVijeh
    },
    {
        href:"list.aspx?table=TakmiliInsuranceType&menu=salarysa",
        access: "salary,takmiliinsurancetype",
        label: Lang.TakmiliInsuranceType,
        value: Lang.TakmiliInsuranceType
    },
    {
        href:"list.aspx?table=BankType&menu=salarysa",
        access: "salary,banktype",
        label: Lang.BankType,
        value: Lang.BankType
    },
    {
        href:"list.aspx?table=Bank&menu=salarysa",
        access: "salary,bank",
        label: Lang.Bank,
        value: Lang.Bank
    },
    {
        href:"list.aspx?table=FormType&menu=salarysa",
        access: "salary,formtype",
        label: Lang.FormType,
        value: Lang.FormType
    },
    {
        href:"list.aspx?table=Department&menu=salarysa",
        access: "salary,department",
        label: Lang.Department,
        value: Lang.Department
    },
    {
        href:"list.aspx?table=Workhouse&menu=salarysa",
        access: "salary,workhouse",
        label: "Workhouse",
        value: "Workhouse"
    },
    {
        href:"list.aspx?table=SkillLevels&menu=salarysa",
        access: "salary,skilllevels",
        label: Lang.SkillLevels,
        value: Lang.SkillLevels
    },
    {
        href:"list.aspx?table=WorkRank&menu=salarysa",
        access: "salary,wrkrank",
        label: Lang.WorkRank,
        value: Lang.WorkRank
    },
    {
        href:"list.aspx?table=FShoglRank&menu=salarysa",
        access: "salary,fshoglrank",
        label: Lang.FShoglRank,
        value: Lang.FShoglRank
    },
    {
        href:"list.aspx?table=DarsadJanbazi&menu=salarysa",
        access: "salary,darsadjanbazi",
        label: Lang.DarsadJanbazi,
        value: Lang.DarsadJanbazi
    },
    {
        href:"list.aspx?table=ModateEsarat&menu=salarysa",
        access: "salary,modateesarat",
        label: Lang.ModateEsarat,
        value: Lang.ModateEsarat
    },
    {
        href:"list.aspx?table=ModateKhedmat&menu=salarysa",
        access: "salary,modateesarat",
        label: Lang.ModateKhedmat,
        value: Lang.ModateKhedmat
    },
    {
        href:"list.aspx?table=HozeMKhdmat&menu=salarysa",
        access: "salary,hozemkhdmat",
        label: Lang.HozeJMKhdmat,
        value: Lang.HozeJMKhdmat
    },
    {
        href:"list.aspx?table=SathModiryat&menu=salarysa",
        access: "salary,sathmodiryat",
        label: Lang.SathModiryat,
        value: Lang.SathModiryat
    },
    {
        href:"list.aspx?table=IsarType&menu=salarysa",
        access: "salary,isartype",
        label: Lang.IsarType,
        value: Lang.IsarType
    },
    {
        href:"list.aspx?table=ContractType&menu=salarysa",
        access: "salary,contracttype",
        label: Lang.ContractType,
        value: Lang.ContractType
    },
    {
        href:"list.aspx?table=PostType&menu=salarysa",
        access: "salary,posttype",
        label: Lang.PostType,
        value: Lang.PostType
    },
    {
        href:"list.aspx?table=ContractDuration&menu=salarysa",
        access: "salary,contractduratione",
        label: Lang.ContractDuration,
        value: Lang.ContractDuration
    },
    {
        href:"list.aspx?table=MilitaryStatus&menu=salarysa",
        access: "salary,militarystatus",
        label: Lang.MilitaryStatus,
        value: Lang.MilitaryStatus
    },
    {
        href:"list.aspx?table=EmpType&menu=salarysa",
        access: "salary,emptype",
        label: Lang.EmpStatus,
        value: Lang.EmpStatus
    },
    {
        href:"list.aspx?table=ManageRank&menu=salarysa",
        access: "salary,managerank",
        label: Lang.ManageRank,
        value: Lang.ManageRank
    },
    {
        href:"list.aspx?table=ManageLevel&menu=salarysa",
        access: "salary,managelevel",
        label: Lang.ManagementLevel,
        value: Lang.ManagementLevel
    },
    {
        href:"list.aspx?table=ManageType&menu=salarysa",
        access: "salary,managetype",
        label: Lang.ManageType,
        value: Lang.ManageType
    },
    {
        href:"list.aspx?table=Nation&menu=salarysa",
        access: "salary,nation",
        label: Lang.Nation,
        value: Lang.Nation
    },
    {
        href:"list.aspx?table=FundType&menu=salarysa",
        access: "salary,fundtype",
        label: Lang.FundType,
        value: Lang.FundType
    },
    {
        href:"list.aspx?table=Course&menu=salarysa",
        access: "salary,course",
        label: Lang.Course,
        value: Lang.Course
    },
    {
        href:"list.aspx?table=ConstItems&menu=salarysa",
        access: "salary,constitems",
        label: Lang.ConstItems,
        value: Lang.ConstItems
    },
    {
        href:"list.aspx?table=ConstDefinitions&menu=salarysa",
        access: "salary,constdefination",
        label: Lang.ConstDefinitions,
        value: Lang.ConstDefinitions
    },
    {
        href:"list.aspx?table=Personnel&menu=salarysa&view=staffs",
        access: "salary,staff",
        label: Lang.Personnel,
        value: Lang.Personnel
    },
    {
        href:"list.aspx?table=Orders&menu=salarysa",
        access: "salary,orders",
        label: Lang.SalaryOrder,
        value: Lang.SalaryOrder
    },
    {
        href:"list.aspx?table=Funds&menu=salarysa",
        access: "salary,funds",
        label: Lang.Funds,
        value: Lang.Funds
    },
    {
        access: "stores,units",
        iconClass: "store-units",
        label: Lang.MeasurementUnits,
        value: Lang.MeasurementUnits,
        href:"list.aspx?table=Units&menu=storea"
    },
    {
        access: "stores,servicetype",
        iconClass: "store-servicetype",
        label: Lang.ServiceType,
        value: Lang.ServiceType,
        href:"list.aspx?table=ServiceType&menu=storea"
    },
    {
        access: "stores,service",
        iconClass: "store-service",
        label: Lang.Commodity,
        value: Lang.Commodity,
        href:"list.aspx?table=Service&menu=storea"
    },
    {
        access: "stores,storecategory",
        iconClass: "store-storecategory",
        label: Lang.StoreNature,
        value: Lang.StoreNature,
        href:"list.aspx?table=StoreCategory&menu=storea"
    },
    {
        access: "stores,store",
        iconClass: "store-store",
        label: Lang.Store,
        value: Lang.Store,
        href:"list.aspx?table=Stores&menu=storea"
    },
    {
        access: "stores,supplier",
        iconClass: "store-supplier",
        label: Lang.Suppliers,
        value: Lang.Suppliers,
        href:"list.aspx?table=Supplier&menu=storea"
    },
    {
        access: "stores,transfer",
        iconClass: "storeforms-transfer",
        label: Lang.WFTTransfer,
        value: Lang.WFTTransfer,
        href:"list.aspx?table=Transfer&menu=storea"
    },
    {
        access: "stores,purchaserequest",
        iconClass: "storeforms-purchaserequest",
        label: Lang.WFTPurchaseRequest,
        value: Lang.WFTPurchaseRequest,
        href:"list.aspx?table=PurchaseRequest&menu=storea"
    },
    {
        access: "stores,request",
        iconClass: "storeforms-request",
        label: Lang.WFTRequest,
        value: Lang.WFTRequest,
        href:"list.aspx?table=Request&menu=storea"
    },
    {
        access: "stores,purchase",
        iconClass: "storeforms-purchase",
        label: Lang.WFTPurchase,
        value: Lang.WFTPurchase,
        href:"list.aspx?table=Purchase&menu=storea"
    },
    {
        access: "stores,storedraft",
        iconClass: "storeforms-storedraft",
        label: Lang.WFTStoreDraft,
        value: Lang.WFTStoreDraft,
        href:"list.aspx?table=StoreDraft&menu=storea"
    },
    {
        access: "stores,refuse",
        iconClass: "storeforms-refuse",
        label: Lang.WFTRefuse,
        value: Lang.WFTRefuse,
        href:"list.aspx?table=Refuse&menu=storea"
    },
    {
        access: "stores,returns",
        iconClass: "storeforms-returns",
        label: Lang.WFTReturn,
        value: Lang.WFTReturn,
        href:"list.aspx?table=Returns&menu=storea"
    },
    {
        access: "stores,buildorder",
        iconClass: "storeforms-returns",
        label: Lang.WFTBuildOrder,
        value: Lang.WFTBuildOrder,
        href: "list.aspx?table=BuildOrder&menu=storea"
    },
    {
        access: "sys,monetaryunit",
        iconClass: "system-monetaryunit",
        label: Lang.MonetaryUnit,
        value: Lang.MonetaryUnit,
        href:"list.aspx?table=MonetaryUnit&menu=sysa"
    },
    {
        access: "sys,conversion",
        iconClass: "system-conversion",
        label: Lang.Conversion,
        value: Lang.Conversion,
        href:"list.aspx?table=Conversion&menu=sysa"
    },
    {
        access: "sys,subsystem",
        iconClass: "system-system",
        label: Lang.Subsystem,
        value: Lang.Subsystem,
        href:"list.aspx?table=subsystem"
    },
    {
        access: "sys,form",
        iconClass: "system-form",
        label: Lang.SystemForms,
        value: Lang.SystemForms,
        href:"list.aspx?table=form"
    },
    {
        access: "sys,workflow",
        iconClass: "system-workflow",
        label: Lang.Workflow,
        value: Lang.Workflow,
        href:"list.aspx?table=Workflow"
    },
    //{
    //    access: "sys,reports",
    //    iconClass: "system-reports",
    //    label: Lang.DynamicReport,
    //    value: Lang.DynamicReport,
    //    href:"list.aspx?table=reports"
    //},
    {
        access: "sys,langs",
        iconClass: "system-lang",
        label: Lang.LangEdit,
        value: Lang.LangEdit,
        href:"list.aspx?view=langs"
    },
    {
        access: "sys,logs",
        iconClass: "system-logs",
        label: Lang.ErrorReport,
        value: Lang.ErrorReport,
        href:"list.aspx?table=logs"
    },
    {
        access: "sys,program",
        iconClass: "system-program",
        label: Lang.IndependentAccounts,
        value: Lang.IndependentAccounts,
        href:"list.aspx?table=Program&menu=sysa"
    },
    {
        access: "sys,formbuild",
        iconClass: "system-form",
        label: Lang.FormDeclare,
        value: Lang.FormDeclare,
        href: "list.aspx?table=Program&menu=sysa"
    },
    {
        access: "sys,theneighbourhood",
        iconClass: "sprites-form-32",
        label: Lang.TheNeighbourhood,
        value: Lang.TheNeighbourhood,
        href: "list.aspx?table=TheNeighbourhood&menu=sysa"
    },
    {
        access: "salary,rate",
        iconClass: "salary-rate",
        label: Lang.Rate,
        value: Lang.Rate,
        href: "list.aspx?table=Rate&menu=salarysa"
    },
    {
        access: "salary,salaryfish",
        iconClass: "salary-salaryfish",
        label: Lang.SalaryFish,
        value: Lang.SalaryFish,
        href: "list.aspx?table=SalaryFish&menu=salarysa"
    },
    {
        access: "salary,enums",
        iconClass: "salary-enums",
        label: Lang.EnumDef,
        value: Lang.EnumDef,
        href: "list.aspx?table=Enums&menu=salarysa"
    },
    {
        access: "salary,orderdefs",
        iconClass: "salary-orderdefs",
        label: Lang.OrderDefs,
        value: Lang.OrderDefs,
        href: "list.aspx?table=OrderDefs&menu=salarysa"
    }
];

function filterItemsByAccess(items) {
    return $.map(items, function(item) {
        var access = item.access;
        if (access && access.length > 0) {
            var formButton = access.split(",");
            if (formButton.length == 2) {
                //simple version: hospital,release
                if (hasAccess("link", formButton[0], formButton[1])) {
                    return item;
                }
            }
            else if (formButton.length == 4) {
                //2 access version: hospital,dr,ward,17
                if (hasAccess("link", formButton[0], formButton[1]) || !hasAccess("link", formButton[2], formButton[3])) {
                    return item;
                }
            }
        }
    });
}

function setQuickLaunchItems() {
    window.quickLaunchUrls = filterItemsByAccess(allQuickLaunchUrls);
}