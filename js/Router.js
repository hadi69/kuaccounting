﻿var Router = (function() {
    var routes = {};

    var add = function(name, path) {
        routes[name] = path;
    };

    var generate = function (name, options) {
        var url = routes[name];
        if (url == null) {
            console.log("Ooops! I can't find a route named " + name);
            return "";
        }

        if (options.params) {
            for (param in options.params) {
                url = url.replace("{" + param + "}", options.params[param]);
            }
        }

        if (options.query) {
            url += "?";
            for (qry in options.query) {
                url += qry + "=" + options.query[qry] + "&";
            }
        }

        if (url[url.length - 1] == "&") {
            url = url.substring(0, url.length - 1);
        }

        return url;
    };

    return {
        Add: add,
        Generate: generate
    };
})(this);