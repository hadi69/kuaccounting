﻿$(function() {
    window.CompileHB = function(templateId, args) {
        var source = $('#' + templateId).html();
        var template = Handlebars.compile(source);
        return template(args);
    };

    window.breadcrumb = function(args) {
        // We used to use a top level object named pageInfo, but from now on, we are removing this top level object
        if (args.pageInfo) {
            args = args.pageInfo;
        }
        // use the args syntax, but check backward incompatibality
        if (!args.parent && args.parentTitle && args.parentUrl) {
            args.parent = {
                title: args.parentTitle,
                url: args.parentUrl
            };
            delete args.parentTitle;
            delete args.parentUrl;
        }
        // Use title instead of pageTitle
        if (args.pageTitle) {
            args.title = args.pageTitle;
            delete args.pageTitle;
        }

        return CompileHB('breadcrumb-template', args);
    };

    window.NodeToJson = function(node) {
        var obj = {};
        $.each(node.attributes, function() {
            if (this.specified) {
                obj[this.name] = this.value;
            }
        });
        return obj;
    };


    // Check if "x" in (x, y) of an element is inside the viewport
    window.isHorizontallyInViewport = function(x) {
        return x >= 0 && x <= (window.innerWidth || document.documentElement.clientWidth);
    }

    // Check if "y" in (x, y) of an element is inside the viewport
    window.isVerticallyInViewport = function(y) {
        return y >= 0 && y <= (window.innerHeight || document.documentElement.clientHeight);
    }

    // Handlebar helpers
    Handlebars.registerHelper('number_with_commas', function(arg) {
        return numberWithCommas(arg);
    });

    Handlebars.registerHelper('to_monetary', function(arg) {
        return toMonetary(arg);
    });

    // Handlebar helper for debugging
    Handlebars.registerHelper("debug", function(optionalValue) {
      console.log("Current Context");
      console.log("====================");
      console.log(this);
     
      if (optionalValue) {
        console.log("Value");
        console.log("====================");
        console.log(optionalValue);
      }
    });

    // Handlebars helper for translating a string
    // It is used like this:
    // {{ t 'AddToFactor' }}
    Handlebars.registerHelper("t", function(str) {
        return window.Lang[str];
    });

    // Handlebars helper which returns a global object
    Handlebars.registerHelper("var", function(key) {
        return window[key];
    });

    // Handlebars helper for truncating large chunks of text
    Handlebars.registerHelper("truncate", function(str) {
        var maxLenght = 30;
        if (str.length > maxLenght) {
            return str.substr(0, maxLenght) + " ...";
        } else {
            return str;
        }
    });
}());