﻿var Growl = (function () {
    // Main growl function
    function show(args) {
        _.defaults(args, {
            type: 'notice',
            duration: 2500
        });

        var element = $("<div>");
        element.addClass("growl alert");
        element.addClass("growl-" + args.type);
        element.html(args.message);

        // Remove the growl after 'args.duration' miliseconds
        setTimeout(function() {
            $(element).remove();
        }, args.duration);

        // If this growl is clicked, remove it!
        element.click(function() {
            $(this).remove();
        });

        $("#growl-container").append(element);
    };

    // Notice growl
    var notice = function(args, time) {
        if (typeof args === "string") {
            show({
                message: args,
                duration: time
            });
        }

        show(args);
    };

    var error = function (args, time) {
        if (typeof args === "string") {
            show({
                message: args,
                type: 'error',
                duration: time
            });
        }

        args = _.extend(args, {
            type: 'error'
        });
        show(args);
    };

    var warn = function(args, time) {
        if (typeof args === "string") {
            show({
                message: args,
                type: 'warning',
                duration: time
            });
        }

        args = _.extend(args, {
            type: 'warning'
        });
        show(args);
    };

    return {
        Notice: notice,
        Error: error,
        Warn: warn
    };
})(this);