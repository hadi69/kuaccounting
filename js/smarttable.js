﻿(function($) {
    $.fn.smartTable = function() {
        var beingObserved = $.fn.smartTable.prototype.beginObserved || ($.fn.smartTable.prototype.beginObserved = []);

        var arrayContains = function(array, object) {
            for (var i = 0; i < array.length; i++) {
                if (array[i] === object) {
                    return i;
                }
            }
            return -1;
        };

        // Check if we have MutationObserver, and if not, do nothing
        if (!window.MutationObserver) {
            return this;
        }

        // Target maybe "tbody" or a table
        var addEmptyRow = function(target) {
            if (typeof(target) === "string") {
                target = $(target);
            }
            if (!(target instanceof(jQuery))) {
                target = $(target);
            }
            var headerRows = target.find("tr.th");
            var numberOfHeaderRows = headerRows.length;
            var numberOfColumns = (numberOfHeaderRows > 0 ? headerRows.first().find("td").length : 0);
            var emptyRow = $('<tr class="empty-row">');
            var emptyCell = $("<td>");
            if (numberOfColumns > 0) {
                emptyCell.attr("colspan", numberOfColumns);
            }
            emptyCell.text(Lang.EmptyData);
            emptyRow.append(emptyCell);
            
            target.append(emptyRow);
        };

        var handleTableMutation = function(mutationRecord) {
            var target = $(mutationRecord.target);

            if (!target.is("tr") && !target.is("tbody")) {
                return;
            }

            if (target.is("tbody") && mutationRecord.type !== "childList") {
                return;
            }

            if (target.is("tr") && mutationRecord.type === "attributes" && mutationRecord.attributeName !== "style") {
                return;
            }

            if (target.is("tr")) {
                target = target.parent();
            }

            var totalNumberOfRows = target.find("tr:visible").length;
            var headerRows = target.find("tr.th:visible");
            var numberOfHeaderRows = headerRows.length;
            var numberOfEmptyRows = target.find("tr.empty-row").length;

            var numberOfDataRows = totalNumberOfRows - numberOfHeaderRows - numberOfEmptyRows;

            // If we have any data rows, remove any empty-data rows
            if (numberOfDataRows > 0 && numberOfEmptyRows > 0) {
                target.find("tr.empty-row").remove();

                return;
            }
            // If we have no data rows and no empty-data rows, add an empty-data row
            if (numberOfDataRows === 0 && numberOfEmptyRows === 0) {
                addEmptyRow(target);
            }
        };

        var observer = new MutationObserver(function(mutations) {
            $(mutations).each(function() {
                handleTableMutation(this);
            });
        });

        var config = {childList: true, subtree: true, attributes: true};

        this.each(function() {
            if ($(this).is("table")) {
                if (arrayContains(beingObserved, this) === -1) {
                    addEmptyRow(this);
                    observer.observe(this, config);
                    beingObserved.push(this);
                }
            }
        });

        return this;
    };
})(jQuery);