﻿var gridKeyboardShortcuts = (function () {
    var gridSelector = '.flexigrid';

    var clearAllSelectedRows = function() {
        return $(gridSelector).find('.trSelected').removeClass('trSelected');
    };

    var selectRow = function(rows) {
        clearAllSelectedRows();

        return rows.addClass('trSelected');
    };

    var selectLastRow = function () {
        clearAllSelectedRows();
        return selectRow($('tr:regex(id, ^row)', gridSelector).last());
    };

    var selectFirstRow = function() {
        clearAllSelectedRows();
        return selectRow($('tr:regex(id, ^row)', gridSelector).first());
    };

    var moveUp = function() {
        var grid = $(gridSelector);
        // If there are no flexigrids on the page, do nothing!
        if (grid.length == 0) {
            return;
        }

        // If the grid has no rows, do nothing!
        if ($('tr:regex(id, ^row)', grid).length == 0) {
            return;
        }

        // If there is only one row, select it and return
        if ($('tr:regex(id, ^row)', grid).length == 1) {
            $('tr:regex(id, ^row)', grid).first().addClass('trSelected');
            return;
        }

        var currentSelectedRow = grid.find('.trSelected').first();
        if (currentSelectedRow.length == 0) {
            // Currently we have no selected rows, so we should select the last row
            selectLastRow();
        } else {
            // We have a selected row, so we should select previous row

            // Check if the row which is currently selected is the first row of the table, and if so, select the last row, otherwise select the previous row
            var firstRow = $('tr:regex(id, ^row)', grid).first();

            if (currentSelectedRow.attr('id') == firstRow.attr('id')) {
                selectLastRow();
            } else {
                selectRow(currentSelectedRow.prev('tr:regex(id, ^row)'));
            }
        }
    };

    var moveDown = function() {
        var grid = $(gridSelector);
        // If there are no flexigrids on the page, do nothing!
        if (grid.length == 0) {
            return;
        }

        // If the grid has no rows, do nothing!
        if ($('tr:regex(id, ^row)', grid).length == 0) {
            return;
        }

        // If there is only one row, select it and return
        if ($('tr:regex(id, ^row)', grid).length == 1) {
            $('tr:regex(id, ^row)', grid).first().addClass('trSelected');
            return;
        }

        var currentSelectedRow = grid.find('.trSelected').first();
        if (currentSelectedRow.length == 0) {
            // Currently we have no selected rows, so we should select the last row
            selectFirstRow();
        } else {
            // We have a selected row, so we should select previous row

            // Check if the row which is currently selected is the first row of the table, and if so, select the last row, otherwise select the previous row
            var lastRow = $('tr:regex(id, ^row)', grid).last();

            if (currentSelectedRow.attr('id') == lastRow.attr('id')) {
                selectFirstRow();
            } else {
                selectRow(currentSelectedRow.next('tr:regex(id, ^row)'));
            }
        }
    };

    return {
        moveUp: moveUp,
        moveDown: moveDown
    };
})(this);