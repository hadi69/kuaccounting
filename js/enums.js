﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/lang/fa.js" />

/*
92-11-01: 
    WorkflowStatusEnum, TaskStateEnum added
*/
var FileStatusEnum = {
    None: -1,
    New: 0,
    Bedridden: 10,
    Operation: 11,
    ChangeSection: 12,
    Release: 20,
    HealthCare: 21,
    Insurance: 22,
    Helper: 23,
    Fund: 24,
    PayOff: 25,
    Final: 30,
    NoPay: 31,
    Clinic: 50 // سرپایی
}
var MoveStatusEnum = {
    None: -1,
    Waiting: 0,
    Confirmed: 1,
    Canceled: 2
}
var FileServiceStatusEnum = {
    None: -1,
    New: 0,
    NewBed: 1,
    Performing: 2,
    Complete: 3,
    Confirmed: 4,
    Canceled: 5,
    Temp: 100
}
var ItemTypeEnum = {
    None: -1,
    Numeric: 0,
    Text: 1,
    Date: 2,
    Bool: 3,
    Radio: 4,
    Enum: 5,
    Table: 6,
    Report: 7,
    XML: 8,
    TextFile: 9,
    Subform: 10,
    Form: 11,
    File: 12
}
var FormTypeEnum = {
    Sales: 0,
    Accounting: 1,
    Personnel: 2,
    Ward: 3,
    Service: 4,
    Free: 5,
    Admin: 6,
    Bank: 7,
    Store: 8,
    StoreForm: 9,
    Distribution: 10,
    Salary: 11,
    Visitor: 12
}
var SectionTypeEnum = {
    Lab: 0,
    Radiology: 1,
    Pharmacy: 2,
    Clinic: 3,
    Hospital: 4,
    Surgery: 5,
    Helper: 6,
    Other: 7
}
var PayStatusEnum = {
    NotPaid: 0,
    SomePaid: 1,
    Paid: 2
}
var CmdEnum = {
    None: -1,
    Drug: 1,
    DrugReturn: 2,
    Test: 3,
    Radiology: 4,
    Surgery: 5,
    BloodRequest: 6,
    FileDiagnosis: 7,
    FileCommand: 8,
    Food: 9,
    FileOrder: 10,
    VitalSigns: 11,
    FileDocument: 12,
    FileVitalSign: 13,
    FileServiceItemDo: 14,
    Installment: 20,
    Discount: 21,
    FileDeduction: 22,
    Bill: 23,
    UncompleteFileService: 24
}
var DAModeEnum = {
    Normal: 0,
    Bedridden: 1,
    Clinic: 2,
    Check: 3,
    Purchase: 4,
    Refuse: 5,
    Return: 6,
    StoreDraft: 7,
    PayCheck: 8,
    PurchaseInvoice: 9,
    RefuseInvoice: 10,
    Closing: 11,
    Final: 12,
    Opening: 13,
    CheckOwe: 14,
    PayCheckOwe: 15,
    CheckExpendAndPass: 16,
    Loan: 17,
    Installment: 18,
    Fish: 19,
    Sale: 20,
    PayBack: 21,
    FinalSale: 22,
    AccountCheckout: 23,
    Deduction: 24
}
var DAStatusEnum = {
    Closed: 0,
    Locked: 1,
    Determined: 2
}
var TopicNatureEnum = {
    Debtor: 0,
    Creditor: 1,
    Both: 2
}
var CardexTypeEnum = {
    Transfer: 0,
    Purchase: 2,
    Return: 4,
    Refuse: 5,
    StoreDraft: 6,
    Opening: 13
}

// for Personnel
var PCmdEnum = {
    None: -1,
    Info: 1,
    Service: 2,
    Relative: 3,
    Position: 4,
    Employment: 5,
    Job: 6,
    Education: 7,
    Rating: 8,
    Score: 9,
    Military: 10
}
// used in bedridden(s).html
var ViewEnum = {
    Doctor: 0,
    Nurse: 1
}

// used in bedridden(s).html
var CheckStatusEnum = {
    New: 0,
    Received: 1,
    Refused: 2,
    Sleep: 3,
    Expend: 4,
    SleepReceive: 5,
    Visitor: 6,
    Guarantee: 7,
    TakeBack: 8,
    GiveBack: 9,
    UseInDocumentAccounting: 10,
    DisProof: 11,
}

var FinanceYearEnum = {
    Temp: 0,
    Closed: 1,
    Active: 2,
    Closing: 3
}
var WorkflowTypeEnum =
{
    Transfer: 0, Request: 1, Purchase: 2, PurchaseRequest: 3, Return: 4, Refuse: 5, StoreDraft: 6, BuildOrder: 7, Voucher: 8
}
var WorkflowStatusEnum = {
    InProgress: 0,
    Final: 1
}
var TaskStateEnum = {
    Start: 0,
    Middle: 1,
    Final: 2,
    Single: 3 //means both start & final
}
var FactorRoleEnum = {
    None: 0,
    StoreDraft: 1,
    Purchase: 2,
    PurchaseRequest: 3
}

var SalesInvoicePayMethodEnum = {
    Cash: 0,
    Check: 1,
    Other: 2,
    Complex: 3,
    Bank: 4,
    Check2: 5,
}

var FishStatusEnum = {
    Temp: 0,
    Final: 1
};
var GroupingTypeEnum = {
    Person: 0,
    Service: 1,
    Visitor: 2,
    Supplier: 3,
    Advertisement: 4
};
var WeekdayEnum = {
    Sunday: 0,
    Monday: 1,
    Tuesday: 2,
    Wednesday: 3,
    Thursday: 4,
    Friday: 5,
    Saturday: 6
};

var BankAccountTypeEnum = {
    Rial: 0,
    Arz: 1,
    Customer: 2
};

var StoredraftPayStatusEnum = {
    NotPaied: 0,
    Paied: 1
};

var LoanStatusEnum = {
    Disable: 0,
    Enable: 1,
    Settled: 2
}
var LoanTypeEnum = {
    Geted: 0,
    Paied: 1
}
var StoreDraftPayMethodEnum = {
    PayMethodCash: 0,
    PayMethodCheck: 1,
    PayMethodOwe: 2,
    PayMethodComplex: 3,
    PayMethodBank: 4
}
var OfferStepSaleTypeEnum = {
    Numerical: 0,
    Amount: 1,
    Cash: 2,
    Check1: 3,
    bank: 4,
    Check2: 5
}
var OfferStepRewardTypeEnum = {
    Commodity: 0,
    Discount: 1,
    Deadline: 2,
    Pursant: 3
}

var MembershipStatusEnum = {
    Disable: 0,
    Enable: 1
}

var OfferTypeEnum = {
    Commodity: 0,
    GoodsGroup: 1,
    Factors: 2,
}
var SmsStatusEnum = {
    Temp: 0,
    New: 1,
    Sent: 2,
    Failed: 2,
}
var TabletMsgModeEnum = {
    Send: 0,
    Receive: 1
}
var TabletWorkflowTypeEnum =
{
    NewF: 0,
    OldF: 1
}
var BasketStatusEnum =
{
    New: 1,
    Confirmed: 2,
    Canceled: 3
}
var BasketPayStatusEnum =
{
    NotPaid: 1,
    InPayment: 2,
    Paid: 3
}
var BasketPayMethodEnum =
{
    Online: 1,
    Coupon: 2,
    Cash: 3,
    InPlace: 4
}
var AdvertisementTypeEnum =
{
    Estate10: 10,
    Estate11: 11,
    Estate12: 12,
    Estate13: 13,
    Estate14: 14,
    Car20: 20,
    Car21: 21,
    Car22: 22,
    Electric30: 30,
    Electric31: 31,
    Electric32: 32,
    Electric33: 33,
    Electric34: 34,
    Furniture: 40,
    Personal50: 50,
    Personal51: 51,
    Personal52: 52,
    Games: 60,
    Services: 70,
    Employement: 80,
    Other: 100
}
var AssessmentTypeEnum =
{
    Manually: 1,
    ManuallySelected: 2,
    SumOfFactorRows: 3,
    SumOfFactorServices: 4,
    SumOfFactorsPrice: 5,
    NumOfNotPassedChecks: 6,
    SumOFPassedCheckPrice: 7,
    NumOFPassedCheck: 8,
    OweAmount: 9,
    CashPay: 10,
    BankPay: 11,
    NumOfServiceInServiceGroup: 12,
    CustomerCaverage: 13
}
var AdvertisementGeneralTypeEnum =
{
    Supply: 1,
    Demand: 2
}
var AdvertisementStatusEnum =
{
    New: 1,
    Rejected: 2,
    Published: 3,
    Sold: 4,
    Expired: 5
}
var ClaimStatusEnum =
{
    New: 1,
    Done: 2
}
var ClaimTypeEnum =
{
    Claim: 1,
    Suggest: 2
}
var CalcMethodEnum =
{
    Direct: 1,
    Descending: 2,
    None: 3
}
// -------------------------------------
// Display Enums : Start
// -------------------------------------
function displayFileStatus(selector, status) {
    //BedriddenStatus:  0:New,10:Bedridden;11:Operation;20:Release;21:HealthCare;22:Insurance;23:Helper;24:Fund;25:PayOff;30:Final;31:NoPay;
    if (status == FileStatusEnum.New) {
        if (!selector)
            return Lang.FSNew;
        $(selector).html(Lang.FSNew);
        $(selector).addClass("fs0"); // a class should always be of type XX0 = Two letters + status
    }
    else if (status == FileStatusEnum.Bedridden) {
        if (!selector)
            return Lang.FSBedridden;
        $(selector).html(Lang.FSBedridden);
        $(selector).addClass("fs10");
    }
    else if (status == FileStatusEnum.Operation) {
        if (!selector)
            return Lang.FSOperation;
        $(selector).html(Lang.FSOperation);
        $(selector).addClass("fs11");
    }

    else if (status == FileStatusEnum.ChangeSection) {
        if (!selector)
            return Lang.FSChangeSection;
        $(selector).html(Lang.FSChangeSection);
        $(selector).addClass("fs12");
    }
    else if (status == FileStatusEnum.Release) {
        if (!selector)
            return Lang.FSRelease;
        $(selector).html(Lang.FSRelease);
        $(selector).addClass("fs20");
    }
    else if (status == FileStatusEnum.HealthCare) {
        if (!selector)
            return Lang.KarshenasaneBimarestani;
        $(selector).html(Lang.KarshenasaneBimarestani);
        $(selector).addClass("fs21");
    }
    else if (status == FileStatusEnum.Insurance) {
        if (!selector)
            return Lang.NazeraneBimeh;
        $(selector).html(Lang.NazeraneBimeh);
        $(selector).addClass("fs22");
    }
    else if (status == FileStatusEnum.Helper) {
        if (!selector)
            return Lang.FSHelper;
        $(selector).html(Lang.FSHelper);
        $(selector).addClass("fs23");
    }
    else if (status == FileStatusEnum.Fund) {
        if (!selector)
            return Lang.FSFund;
        $(selector).html(Lang.FSFund);
        $(selector).addClass("fs24");
    }
    else if (status == FileStatusEnum.PayOff) {
        if (!selector)
            return Lang.FSPayOff;
        $(selector).html(Lang.FSPayOff);
        $(selector).addClass("fs25");
    }
    else if (status == FileStatusEnum.Final) {
        if (!selector)
            return Lang.FSFinal;
        $(selector).html(Lang.FSFinal);
        $(selector).addClass("fs30");
    }
    else if (status == FileStatusEnum.NoPay) {
        if (!selector)
            return Lang.FSNoPay;
        $(selector).html(Lang.FSNoPay);
        $(selector).addClass("fs30");
    }
    else {
        if (!selector)
            return Lang.NotSpecified;
        $(selector).html(Lang.NotSpecified + status);
        $(selector).addClass("unknown");
    }
}
function displayMoveStatus(selector, status) {
    if (status == MoveStatusEnum.Waiting) {
        if (!selector)
            return Lang.MSWaiting;
        $(selector).html(Lang.MSWaiting);
        $(selector).addClass("fsm0"); // a class should always be of type XX0 = Two letters + status
    }
    else if (status == MoveStatusEnum.Confirmed) {
        if (!selector)
            return Lang.MSConfrimed;
        $(selector).html(Lang.MSConfrimed);
        $(selector).addClass("fsm1");
    }
    else if (status == MoveStatusEnum.Canceled) {
        if (!selector)
            return Lang.MSCancel;
        $(selector).html(Lang.MSCancel);
        $(selector).addClass("fsm2");
    }
    else {
        if (!selector)
            return Lang.NotSpecified;
        $(selector).html(Lang.NotSpecified + status);
        $(selector).addClass("unknown");
    }
}
function displayFileServiceStatus(selector, status) {
    var cls = "fss" + status;
    if (status == FileServiceStatusEnum.New) {
        if (!selector)
            return Lang.FSTNew;
        $(selector).html(Lang.FSTNew);
    }
    else if (status == FileServiceStatusEnum.NewBed) {
        if (!selector)
            return Lang.FSTNewBed;
        $(selector).html(Lang.FSTNewBed);
    }
    else if (status == FileServiceStatusEnum.Performing) {
        if (!selector)
            return Lang.FSTPerforming;
        $(selector).html(Lang.FSTPerforming);
    }
    else if (status == FileServiceStatusEnum.Complete) {
        if (!selector)
            return Lang.FSTComplete;
        $(selector).html(Lang.FSTComplete);
    }
    else if (status == FileServiceStatusEnum.Confirmed) {
        if (!selector)
            return Lang.FSTConfirmed;
        $(selector).html(Lang.FSTConfirmed);
    }
    else if (status == FileServiceStatusEnum.Canceled) {
        if (!selector)
            return Lang.FSTCanceled;
        $(selector).html(Lang.FSTCanceled);
    }
    else if (status == FileServiceStatusEnum.Temp) {
        if (!selector)
            return Lang.FSTTemp;
        $(selector).html(Lang.FSTTemp);
    }
    else {
        if (!selector)
            return Lang.StatusUnknown;
        $(selector).html(Lang.StatusUnknown + status);
    }
    $(selector).addClass(cls);
}
function displayPayStatus(selector, status) {
    if (status == PayStatusEnum.NotPaid) {
        if (!selector)
            return Lang.PSNotPaid;
        $(selector).html(Lang.PSNotPaid);
        $(selector).addClass("fsm1");
    }
    else if (status == PayStatusEnum.SomePaid) {
        if (!selector)
            return Lang.PSSomePaid;
        $(selector).html(Lang.PSSomePaid);
        $(selector).addClass("fsm1");
    }
    else if (status == PayStatusEnum.Paid) {
        if (!selector)
            return Lang.PSPaid;
        $(selector).html(Lang.PSPaid);
        $(selector).addClass("fsm2");
    }
    else {
        if (!selector)
            return Lang.StatusUnknown;
        $(selector).html(Lang.StatusUnknown + status);
        $(selector).addClass("unknown");
    }
}

function displaySectionType(selector, type) {
    if (type == SectionTypeEnum.Lab) {
        if (!selector)
            return Lang.STLab;
        $(selector).html(Lang.STLab);
        $(selector).addClass("fs_1"); // a class should always be of type XX0 = Two letters + type
    }
    else if (type == SectionTypeEnum.Radiology) {
        if (!selector)
            return Lang.STRadiology;
        $(selector).html(Lang.STRadiology);
        $(selector).addClass("fs0");
    }
    else if (type == SectionTypeEnum.Pharmacy) {
        if (!selector)
            return Lang.STPharmacy;
        $(selector).html(Lang.STPharmacy);
        $(selector).addClass("fs1");
    }
    else if (type == SectionTypeEnum.Clinic) {
        if (!selector)
            return Lang.STClinic;
        $(selector).html(Lang.STClinic);
        $(selector).addClass("fs20");
    }
    else if (type == SectionTypeEnum.Hospital) {
        if (!selector)
            return Lang.STHospital;
        $(selector).html(Lang.STHospital);
        $(selector).addClass("fs21");
    }
    else if (type == SectionTypeEnum.Surgery) {
        if (!selector)
            return Lang.STSurgery;
        $(selector).html(Lang.STSurgery);
        $(selector).addClass("fs21");
    }
    else if (type == SectionTypeEnum.Helper) {
        if (!selector)
            return Lang.STHelper;
        $(selector).html(Lang.STHelper);
        $(selector).addClass("fs21");
    }
    else if (type == SectionTypeEnum.Other) {
        if (!selector)
            return Lang.STOther;
        $(selector).html(Lang.STOther);
        $(selector).addClass("fs21");
    }
    else {
        if (!selector)
            return Lang.StatusUnknown;
        $(selector).html(Lang.StatusUnknown + type);
        $(selector).addClass("unknown");
    }
}
function displayFormType(selector, type) {
    if (type == FormTypeEnum.Sales) {
        if (!selector)
            return Lang.FormTypeSales;
        $(selector).html(Lang.FormTypeSales);
        $(selector).addClass("fs_1"); // a class should always be of type XX0 = Two letters + type
    }
    else if (type == FormTypeEnum.Accounting) {
        if (!selector)
            return Lang.FormTypeAccounting;
        $(selector).html(Lang.FormTypeAccounting);
        $(selector).addClass("fs0");
    }
    else if (type == FormTypeEnum.Personnel) {
        if (!selector)
            return Lang.FormTypePersonnel;
        $(selector).html(Lang.FormTypePersonnel);
        $(selector).addClass("fs1");
    }
    else if (type == FormTypeEnum.Ward) {
        if (!selector)
            return Lang.FormTypeWard;
        $(selector).html(Lang.FormTypeWard);
        $(selector).addClass("fs20");
    }
    else if (type == FormTypeEnum.Service) {
        if (!selector)
            return Lang.FormTypeService;
        $(selector).html(Lang.FormTypeService);
        $(selector).addClass("fs21");
    }
    else if (type == FormTypeEnum.Free) {
        if (!selector)
            return Lang.FormTypeFree;
        $(selector).html(Lang.FormTypeFree);
        $(selector).addClass("fs5");
    }
    else if (type == FormTypeEnum.Admin) {
        if (!selector)
            return Lang.FormTypeAdmin;
        $(selector).html(Lang.FormTypeAdmin);
        $(selector).addClass("fs6");
    }
    else if (type == FormTypeEnum.Bank) {
        if (!selector)
            return Lang.FormTypeBank;
        $(selector).html(Lang.FormTypeBank);
        $(selector).addClass("fs7");
    }
    else if (type == FormTypeEnum.Store) {
        if (!selector)
            return Lang.FormTypeStore;
        $(selector).html(Lang.FormTypeStore);
        $(selector).addClass("fs8");
    }
    else if (type == FormTypeEnum.StoreForm) {
        if (!selector)
            return Lang.FormTypeStoreForms;
        $(selector).html(Lang.FormTypeStoreForms);
        $(selector).addClass("fs9");
    }
    else if (type == FormTypeEnum.Distribution) {
        if (!selector)
            return Lang.FormTypeDistribution;
        $(selector).html(Lang.FormTypeDistribution);
        $(selector).addClass("fs10");
    }
    else if (type == FormTypeEnum.Salary) {
        if (!selector)
            return Lang.FormTypeSalary;
        $(selector).html(Lang.FormTypeSalary);
        $(selector).addClass("fs11");
    }
    else if (type == FormTypeEnum.Visitor) {
        if (!selector)
            return Lang.FormTypeVisitor;
        $(selector).html(Lang.FormTypeVisitor);
        $(selector).addClass("fs12");
    }
    else {
        if (!selector)
            return Lang.StatusUnknown;
        $(selector).html(Lang.StatusUnknown + type);
        $(selector).addClass("unknown");
    }
}
function displayFormDeclareItemType(selector, type) {
    //-1:None;0:Numeric;1:Text;2:Date;3:Bool;4:Radio;5:Enum;6:Table;7:Report;8:XML;9:TextFile;10:Subform;11:Form;12:File
    if (type == ItemTypeEnum.None) {
        if (!selector)
            return Lang.ItemTypeNone;
        $(selector).html(Lang.ItemTypeNone);
        $(selector).addClass("fs_1"); // a class should always be of type XX0 = Two letters + type
    }
    else if (type == ItemTypeEnum.Numeric) {
        if (!selector)
            return Lang.ItemTypeNumeric;
        $(selector).html(Lang.ItemTypeNumeric);
        $(selector).addClass("fs0");
    }
    else if (type == ItemTypeEnum.Text) {
        if (!selector)
            return Lang.ItemTypeText;
        $(selector).html(Lang.ItemTypeText);
        $(selector).addClass("fs1");
    }
    else if (type == ItemTypeEnum.Date) {
        if (!selector)
            return Lang.ItemTypeDate;
        $(selector).html(Lang.ItemTypeDate);
        $(selector).addClass("fs20");
    }
    else if (type == ItemTypeEnum.Bool) {
        if (!selector)
            return Lang.ItemTypeBool;
        $(selector).html(Lang.ItemTypeBool);
        $(selector).addClass("fs21");
    }
    else if (type == ItemTypeEnum.Radio) {
        if (!selector)
            return Lang.ItemTypeRadio;
        $(selector).html(Lang.ItemTypeRadio);
        $(selector).addClass("fs22");
    }
    else if (type == ItemTypeEnum.Enum) {
        if (!selector)
            return Lang.ItemTypeEnum;
        $(selector).html(Lang.ItemTypeEnum);
        $(selector).addClass("fs23");
    }
    else if (type == ItemTypeEnum.Table) {
        if (!selector)
            return Lang.ItemTypeTable;
        $(selector).html(Lang.ItemTypeTable);
        $(selector).addClass("fs24");
    }
    else if (type == ItemTypeEnum.Report) {
        if (!selector)
            return Lang.ItemTypeReport;
        $(selector).html(Lang.ItemTypeReport);
        $(selector).addClass("fs25");
    }
    else if (type == ItemTypeEnum.XML) {
        if (!selector)
            return Lang.ItemTypeXML;
        $(selector).html(Lang.ItemTypeXML);
        $(selector).addClass("fs30");
    }
    else if (type == ItemTypeEnum.TextFile) {
        if (!selector)
            return Lang.ItemTypeTextFile;
        $(selector).html(Lang.ItemTypeTextFile);
        $(selector).addClass("fs30");
    }
    else if (type == ItemTypeEnum.Subform) {
        if (!selector)
            return Lang.ItemTypeSubform;
        $(selector).html(Lang.ItemTypeSubform);
        $(selector).addClass("fs30");
    }
    else if (type == ItemTypeEnum.Form) {
        if (!selector)
            return Lang.ItemTypeForm;
        $(selector).html(Lang.ItemTypeForm);
        $(selector).addClass("fs30");
    }
    else if (type == ItemTypeEnum.File) {
        if (!selector)
            return Lang.ItemTypeFile;
        $(selector).html(Lang.ItemTypeFile);
        $(selector).addClass("fs30");
    }
    else {
        if (!selector)
            return Lang.StatusUnknown;
        $(selector).html(Lang.StatusUnknown + type);
        $(selector).addClass("unknown");
    }
}

function displayWorkflowType(selector, type) {
    if (type == WorkflowTypeEnum.Transfer) {
        if (!selector)
            return Lang.WFTTransfer;
        $(selector).html(Lang.WFTTransfer);
        $(selector).addClass("wt0");
    }
    else if (type == WorkflowTypeEnum.Request) {
        if (!selector)
            return Lang.WFTRequest;
        $(selector).html(Lang.WFTRequest);
        $(selector).addClass("wt1");
    }
    else if (type == WorkflowTypeEnum.Purchase) {
        if (!selector)
            return Lang.WFTPurchase;
        $(selector).html(Lang.WFTPurchase);
        $(selector).addClass("wt2");
    }
    else if (type == WorkflowTypeEnum.PurchaseRequest) {
        if (!selector)
            return Lang.WFTPurchaseRequest;
        $(selector).html(Lang.WFTPurchaseRequest);
        $(selector).addClass("wt3");
    }
    else if (type == WorkflowTypeEnum.Return) {
        if (!selector)
            return Lang.WFTReturn;
        $(selector).html(Lang.WFTReturn);
        $(selector).addClass("wt4");
    }
    else if (type == WorkflowTypeEnum.Refuse) {
        if (!selector)
            return Lang.WFTRefuse;
        $(selector).html(Lang.WFTRefuse);
        $(selector).addClass("wt5");
    }
    else if (type == WorkflowTypeEnum.StoreDraft) {
        if (!selector)
            return Lang.WFTStoreDraft;
        $(selector).html(Lang.WFTStoreDraft);
        $(selector).addClass("wt6");
    }
    else if (type == WorkflowTypeEnum.BuildOrder) {
        if (!selector)
            return Lang.WFTBuildOrder;
        $(selector).html(Lang.WFTBuildOrder);
        $(selector).addClass("wt7");
    }
    else if (type == WorkflowTypeEnum.Voucher) {
        if (!selector)
            return Lang.WFTVoucher;
        $(selector).html(Lang.WFTVoucher);
        $(selector).addClass("wt8");
    }
    else {
        if (!selector)
            return Lang.StatusUnknown;
        $(selector).html(Lang.StatusUnknown + type);
        $(selector).addClass("unknown");
    }
}

function displayWorkflowStatus(selector, type) {
    if (type == WorkflowStatusEnum.InProgress) {
        if (!selector)
            return Lang.WFStatusInProgress;
        $(selector).html(Lang.WFStatusInProgress);
        $(selector).addClass("wfs0");
    }
    else if (type == WorkflowStatusEnum.Final) {
        if (!selector)
            return Lang.WFStatusFinal;
        $(selector).html(Lang.WFStatusFinal);
        $(selector).addClass("wfs1");
    }
    else {
        if (!selector)
            return Lang.StatusUnknown;
        $(selector).html(Lang.StatusUnknown + type);
        $(selector).addClass("unknown");
    }
}
function displayTaskStateEnum(selector, type) {
    if (type == TaskStateEnum.Start) {
        if (!selector)
            return Lang.TaskStateStart;
        $(selector).html(Lang.TaskStateStart);
        $(selector).addClass("ts0");
    }
    else if (type == TaskStateEnum.Middle) {
        if (!selector)
            return Lang.TaskStateMiddle;
        $(selector).html(Lang.TaskStateMiddle);
        $(selector).addClass("ts1");
    }
    else if (type == TaskStateEnum.Final) {
        if (!selector)
            return Lang.TaskStateFinal;
        $(selector).html(Lang.TaskStateFinal);
        $(selector).addClass("ts2");
    }
    else if (type == TaskStateEnum.Single) {
        if (!selector)
            return Lang.TaskStateSingle;
        $(selector).html(Lang.TaskStateSingle);
        $(selector).addClass("ts3");
    }
    else {
        if (!selector)
            return Lang.StatusUnknown;
        $(selector).html(Lang.StatusUnknown + type);
        $(selector).addClass("unknown");
    }
}
function displayTaskFactorRole(selector, type) {
    if (type == FactorRoleEnum.None) {
        if (!selector)
            return Lang.TaskStateStart;
        $(selector).html(Lang.TaskStateStart);
        $(selector).addClass("ts0");
    }
    else if (type == FactorRoleEnum.StoreDraft) {
        if (!selector)
            return Lang.TaskStateMiddle;
        $(selector).html(Lang.TaskStateMiddle);
        $(selector).addClass("ts1");
    }
    else if (type == FactorRoleEnum.Purchase) {
        if (!selector)
            return Lang.TaskStateFinal;
        $(selector).html(Lang.TaskStateFinal);
        $(selector).addClass("ts2");
    }
    else if (type == FactorRoleEnum.PurchaseRequest) {
        if (!selector)
            return Lang.TaskStateSingle;
        $(selector).html(Lang.TaskStateSingle);
        $(selector).addClass("ts3");
    }
    else {
        if (!selector)
            return Lang.StatusUnknown;
        $(selector).html(Lang.StatusUnknown + type);
        $(selector).addClass("unknown");
    }
}
function displayDAMode(selector, type) {
    //-1:None;0:Numeric;1:Text;2:Date;3:Bool;4:Radio;5:Enum;6:Table;7:Report;8:XML;9:TextFile;10:Subform;11:Form;12:File
    if (type == DAModeEnum.Normal) {
        if (!selector)
            return Lang.DAModeNormal;
        $(selector).html(Lang.DAModeNormal);
        $(selector).addClass("da0"); // a class should always be of type XX0 = Two letters + type
    }
    else if (type == DAModeEnum.Bedridden) {
        if (!selector)
            return Lang.DAModeFile;
        $(selector).html(Lang.DAModeFile);
        $(selector).addClass("da1");
    }
    else if (type == DAModeEnum.Clinic) {
        if (!selector)
            return Lang.DAModeClinic;
        $(selector).html(Lang.DAModeClinic);
        $(selector).addClass("da2");
    }
    else if (type == DAModeEnum.Check) {
        if (!selector)
            return Lang.DAModeCheck;
        $(selector).html(Lang.DAModeCheck);
        $(selector).addClass("da3");
    }
    else if (type == DAModeEnum.Purchase) {
        if (!selector)
            return Lang.DAModePurchase;
        $(selector).html(Lang.DAModePurchase);
        $(selector).addClass("da4");
    }
    else if (type == DAModeEnum.Refuse) {
        if (!selector)
            return Lang.DAModeRefuse;
        $(selector).html(Lang.DAModeRefuse);
        $(selector).addClass("da5");
    }
    else if (type == DAModeEnum.Return) {
        if (!selector)
            return Lang.DAModeReturn;
        $(selector).html(Lang.DAModeReturn);
        $(selector).addClass("da6");
    }
    else if (type == DAModeEnum.StoreDraft) {
        if (!selector)
            return Lang.DAModeStoreDraft;
        $(selector).html(Lang.DAModeStoreDraft);
        $(selector).addClass("da7");
    }
    else if (type == DAModeEnum.PayCheck) {
        if (!selector)
            return Lang.DAModePayCheck;
        $(selector).html(Lang.DAModePayCheck);
        $(selector).addClass("da8");
    }
    else if (type == DAModeEnum.PurchaseInvoice) {
        if (!selector)
            return Lang.DAModePurchaseInvoice;
        $(selector).html(Lang.DAModePurchaseInvoice);
        $(selector).addClass("da9");
    }
    else if (type == DAModeEnum.RefuseInvoice) {
        if (!selector)
            return Lang.DAModeRefuseInvoice;
        $(selector).html(Lang.DAModeRefuseInvoice);
        $(selector).addClass("da10");
    }
    else if (type == DAModeEnum.Closing) {
        if (!selector)
            return Lang.DAModeClosing;
        $(selector).html(Lang.DAModeClosing);
        $(selector).addClass("da11");
    }
    else if (type == DAModeEnum.Final) {
        if (!selector)
            return Lang.DAModeFinal;
        $(selector).html(Lang.DAModeFinal);
        $(selector).addClass("da12");
    }
    else if (type == DAModeEnum.Opening) {
        if (!selector)
            return Lang.DAModeOpening;
        $(selector).html(Lang.DAModeOpening);
        $(selector).addClass("da13");
    }
    else if (type == DAModeEnum.CheckOwe) {
        if (!selector)
            return Lang.DAModeCheckOwe;
        $(selector).html(Lang.DAModeCheckOwe);
        $(selector).addClass("da14");
    }
    else if (type == DAModeEnum.PayCheckOwe) {
        if (!selector)
            return Lang.DAModePayCheckOwe;
        $(selector).html(Lang.DAModePayCheckOwe);
        $(selector).addClass("da15");
    }
    else if (type == DAModeEnum.CheckExpendAndPass) {
        if (!selector)
            return Lang.DAModeCheckExpendAndPass;
        $(selector).html(Lang.DAModeCheckExpendAndPass);
        $(selector).addClass("da16");
    }
    else if (type == DAModeEnum.Loan) {
        if (!selector)
            return Lang.DAModeLoan;
        $(selector).html(Lang.DAModeLoan);
        $(selector).addClass("da17");
    }
    else if (type == DAModeEnum.Installment) {
        if (!selector)
            return Lang.DAModeInstallment;
        $(selector).html(Lang.DAModeInstallment);
        $(selector).addClass("da18");
    }
    else if (type == DAModeEnum.Fish) {
        if (!selector)
            return Lang.DAModeFish;
        $(selector).html(Lang.DAModeFish);
        $(selector).addClass("da18");
    }
    else if (type == DAModeEnum.Sale) {
        if (!selector)
            return Lang.DAModeSale;
        $(selector).html(Lang.DAModeSale);
        $(selector).addClass("da18");
    }
    else if (type == DAModeEnum.PayBack) {
        if (!selector)
            return Lang.DAModePayBack;
        $(selector).html(Lang.DAModePayBack);
        $(selector).addClass("da18");
    }
    else if (type == DAModeEnum.FinalSale) {
        if (!selector)
            return Lang.DAModeFinalSale;
        $(selector).html(Lang.DAModeFinalSale);
        $(selector).addClass("da18");
    }
    else if (type == DAModeEnum.AccountCheckout) {
        if (!selector)
            return Lang.AccountCheckout;
        $(selector).html(Lang.AccountCheckout);
        $(selector).addClass("da23");
    }
    else if (type == DAModeEnum.Deduction) {
        if (!selector)
            return Lang.DAModeDeduction;
        $(selector).html(Lang.DAModeDeduction);
        $(selector).addClass("da24");
    }
    else {
        if (!selector)
            return Lang.StatusUnknown;
        $(selector).html(Lang.StatusUnknown + type);
        $(selector).addClass("unknown");
    }
}

function displayDAStatus(selector, type) {
    //-1:None;0:Numeric;1:Text;2:Date;3:Bool;4:Radio;5:Enum;6:Table;7:Report;8:XML;9:TextFile;10:Subform;11:Form;12:File
    if (type == DAStatusEnum.Closed) {
        if (!selector)
            return Lang.DAStatusClosed;
        $(selector).html(Lang.DAStatusClosed);
        $(selector).addClass("da0"); // a class should always be of type XX0 = Two letters + type
    }
    else if (type == DAStatusEnum.Locked) {
        if (!selector)
            return Lang.DAStatusLocked;
        $(selector).html(Lang.DAStatusLocked);
        $(selector).addClass("da1");
    }
    else if (type == DAStatusEnum.Determined) {
        if (!selector)
            return Lang.DAStatusDetermined;
        $(selector).html(Lang.DAStatusDetermined);
        $(selector).addClass("da2");
    }
    else {
        if (!selector)
            return Lang.StatusUnknown;
        $(selector).html(Lang.StatusUnknown + type);
        $(selector).addClass("unknown");
    }
}
function displayTopicNature(selector, type) {
    if (type == TopicNatureEnum.Debtor) {
        if (!selector)
            return Lang.Debtor;
        $(selector).html(Lang.Debtor);
        $(selector).addClass("nat0"); // a class should always be of type XX0 = Two letters + type
    }
    else if (type == TopicNatureEnum.Creditor) {
        if (!selector)
            return Lang.Creditor;
        $(selector).html(Lang.Creditor);
        $(selector).addClass("nat1");
    }
    else if (type == TopicNatureEnum.Both) {
        if (!selector)
            return Lang.NatureBoth;
        $(selector).html(Lang.NatureBoth);
        $(selector).addClass("nat2");
    }
    else {
        if (!selector)
            return Lang.StatusUnknown;
        $(selector).html(Lang.StatusUnknown + type);
        $(selector).addClass("unknown");
    }
}

function displayPayCheckStatus(selector, type, cStatus) {
    //-1:None;0:Numeric;1:Text;2:Date;3:Bool;4:Radio;5:Enum;6:Table;7:Report;8:XML;9:TextFile;10:Subform;11:Form;12:File
    if (type == CheckStatusEnum.New) {
        if (!selector) {
            if (cStatus == 'True') {
                return Lang.ColToCustomer;

            } else {
                return Lang.CheckStatusFund;
            }

        }
        if (cStatus == 'True') {
            $(selector).html(Lang.ColToCustomer);
        } else {
            $(selector).html(Lang.CheckStatusFund);
        }
        $(selector).addClass("cs0"); // a class should always be of type XX0 = Two letters + type
    } else if (type == CheckStatusEnum.Received) {
        if (!selector)
            return Lang.CheckStatusReceived;
        $(selector).html(Lang.CheckStatusReceived);
        $(selector).addClass("cs1");
    } else if (type == CheckStatusEnum.Refused) {
        if (!selector)
            return Lang.CheckStatusRefused;
        $(selector).html(Lang.CheckStatusRefused);
        $(selector).addClass("cs2");
    }
}

function displayCheckStatus(selector, type) {
    //-1:None;0:Numeric;1:Text;2:Date;3:Bool;4:Radio;5:Enum;6:Table;7:Report;8:XML;9:TextFile;10:Subform;11:Form;12:File
    if (type == CheckStatusEnum.New) {
        if (!selector)
                return Lang.CheckStatusNew;
        $(selector).html(Lang.CheckStatusNew);
        $(selector).addClass("cs0"); // a class should always be of type XX0 = Two letters + type
    }
    else if (type == CheckStatusEnum.Received) {
        if (!selector)
            return Lang.CheckStatusReceived;
        $(selector).html(Lang.CheckStatusReceived);
        $(selector).addClass("cs1");
    }
    else if (type == CheckStatusEnum.Refused) {
        if (!selector)
            return Lang.CheckStatusRefused;
        $(selector).html(Lang.CheckStatusRefused);
        $(selector).addClass("cs2");
    }
    else if (type == CheckStatusEnum.Sleep) {
        if (!selector)
            return Lang.CheckStatusSleep;
        $(selector).html(Lang.CheckStatusSleep);
        $(selector).addClass("cs3");
    }
    else if (type == CheckStatusEnum.Expend) {
        if (!selector)
            return Lang.CheckStatusExpend;
        $(selector).html(Lang.CheckStatusExpend);
        $(selector).addClass("cs4");
    }
    else if (type == CheckStatusEnum.SleepReceive) {
        if (!selector)
            return Lang.CheckStatusSleepReceive;
        $(selector).html(Lang.CheckStatusSleepReceive);
        $(selector).addClass("cs5");
    }

    else if (type == CheckStatusEnum.Visitor) {
        if (!selector)
            return Lang.CheckStatusVisitor;
        $(selector).html(Lang.CheckStatusVisitor);
        $(selector).addClass("cs6");
    }

    else if (type == CheckStatusEnum.Guarantee) {
        if (!selector)
            return Lang.CheckStatusGuarantee;
        $(selector).html(Lang.CheckStatusGuarantee);
        $(selector).addClass("cs7");
    }
    else if (type == CheckStatusEnum.TakeBack) {
        if (!selector)
            return Lang.CheckStatusTakeBack;
        $(selector).html(Lang.CheckStatusTakeBack);
        $(selector).addClass("cs8");
    }
    else if (type == CheckStatusEnum.GiveBack) {
        if (!selector)
            return Lang.CheckStatusGiveBack;
        $(selector).html(Lang.CheckStatusGiveBack);
        $(selector).addClass("cs9");
    }
    else if (type == CheckStatusEnum.UseInDocumentAccounting) {
        if (!selector)
            return Lang.UseInDocumentAccounting;
        $(selector).html(Lang.UseInDocumentAccounting);
        $(selector).addClass("cs10");
    }
    else if (type == CheckStatusEnum.DisProof) {
        if (!selector)
            return Lang.DisProof;
        $(selector).html(Lang.DisProof);
        $(selector).addClass("cs11");
    }
    else {
        if (!selector)
            return Lang.StatusUnknown;
        $(selector).html(Lang.StatusUnknown + type);
        $(selector).addClass("unknown");
    }
}

function displayFinanceYearStatus(selector, type) {
    //0:Temp;1:Closed;2:Active
    if (type == FinanceYearEnum.Temp) {
        if (!selector)
            return Lang.Temp;
        $(selector).html(Lang.Temp);
        $(selector).addClass("fy0");
    }
    else if (type == FinanceYearEnum.Closed) {
        if (!selector)
            return Lang.Closed;
        $(selector).html(Lang.Closed);
        $(selector).addClass("fy1");
    }
    else if (type == FinanceYearEnum.Active) {
        if (!selector)
            return Lang.Active;
        $(selector).html(Lang.Active);
        $(selector).addClass("fy2");
    }
    else if (type == FinanceYearEnum.Closing) {
        if (!selector)
            return Lang.Closing;
        $(selector).html(Lang.Closing);
        $(selector).addClass("fy3");
    }
    else {
        $(selector).html(Lang.Unknown);
        $(selector).addClass("unknown");
    }
}

function displaySalesInvoicePayMethodEnum(selector, type) {
    if (type == SalesInvoicePayMethodEnum.Cash) {
        if (!selector)
            return Lang.PayMethodCash;
        $(selector).html(Lang.PayMethodCash);
        $(selector).addClass("fy0");
    }
    else if (type == SalesInvoicePayMethodEnum.Check) {
        if (!selector)
            return Lang.PayMethodCheck;
        $(selector).html(Lang.PayMethodCheck);
        $(selector).addClass("cfs1");
    }
    else if (type == SalesInvoicePayMethodEnum.Other) {
        if (!selector)
            return Lang.PayMethodOther;
        $(selector).html(Lang.PayMethodOther);
        $(selector).addClass("cfs2");
    }
    else if (type == SalesInvoicePayMethodEnum.Complex) {
        if (!selector)
            return Lang.PayMethodComplex;
        $(selector).html(Lang.PayMethodComplex);
        $(selector).addClass("cfs3");
    }
    else if (type == SalesInvoicePayMethodEnum.Bank) {
        if (!selector)
            return Lang.PayMethodComplex;
        $(selector).html(Lang.PayMethodBank);
        $(selector).addClass("cfs4");
    }
    else {
        $(selector).html(Lang.Unknown);
        $(selector).addClass("unknown");
    }
}

function displayFishStatusEnum(selector, type) {
    if (type == FishStatusEnum.Temp) {
        if (!selector)
            return Lang.FishTemp;
        $(selector).html(Lang.FishTemp);
        $(selector).addClass("fs0");
    }
    else if (type == FishStatusEnum.Final) {
        if (!selector)
            return Lang.FishFinal;
        $(selector).html(Lang.FishFinal);
        $(selector).addClass("fs1");
    }
    else {
        $(selector).html(Lang.Unknown);
        $(selector).addClass("unknown");
    }
}

function displayGroupingTypeEnum(selector, type) {
    if (type == GroupingTypeEnum.Person) {
        if (!selector)
            return Lang.Persons;
        $(selector).html(Lang.Persons);
        $(selector).addClass("g0");
    }
    else if (type == GroupingTypeEnum.Service) {
        if (!selector)
            return Lang.Product;
        $(selector).html(Lang.Product);
        $(selector).addClass("g1");
    }
    else if (type == GroupingTypeEnum.Visitor) {
        if (!selector)
            return Lang.Visitor;
        $(selector).html(Lang.Visitor);
        $(selector).addClass("g2");
    }
    else if (type == GroupingTypeEnum.Supplier) {
        if (!selector)
            return Lang.Suppliers;
        $(selector).html(Lang.Suppliers);
        $(selector).addClass("g3");
    } else if (type == GroupingTypeEnum.Advertisement) {
        if (!selector)
            return Lang.Advertisement;
        $(selector).html(Lang.Advertisement);
        $(selector).addClass("g4");
    }
    else {
        $(selector).html(Lang.Unknown);
        $(selector).addClass("unknown");
    }
}

function showCardexType(celDiv, id) {
    var type = parseInt($(celDiv).html(), 0);
    if (type == CardexTypeEnum.Purchase) {
        $(celDiv).addClass('_pType');
        $(celDiv).html(Lang.Purchase);
    }
    else if (type == CardexTypeEnum.StoreDraft) {
        $(celDiv).addClass('_sType');
        $(celDiv).html(Lang.StoreDraft);
    }
    else if (type == CardexTypeEnum.Transfer) {
        $(celDiv).addClass('_tType');
        $(celDiv).html(Lang.Transfer);
    }
    else if (type == CardexTypeEnum.Return) {
        $(celDiv).addClass('_rnType');
        $(celDiv).html(Lang.Return);
    }
    else if (type == CardexTypeEnum.Refuse) {
        $(celDiv).addClass('_reType');
        $(celDiv).html(Lang.Refuse);
    }
    else if (type == CardexTypeEnum.Opening) {
        $(celDiv).addClass('_oType');
        $(celDiv).html(Lang.DAModeOpening);
    }
    else
        $(celDiv).html(Lang.Refuse);
}

function displayWeekdayNameEnum(selector, num) {
    if (num == WeekdayEnum.Sunday) {
        if (!selector)
            return Lang.Sunday;
        $(selector).html(Lang.Sunday);
    }
    else if (num == WeekdayEnum.Monday) {
        if (!selector)
            return Lang.Monday;
        $(selector).html(Lang.Monday);
    }
    else if (num == WeekdayEnum.Tuesday) {
        if (!selector)
            return Lang.Tuesday;
        $(selector).html(Lang.Tuesday);
    }
    else if (num == WeekdayEnum.Wednesday) {
        if (!selector)
            return Lang.Wednesday;
        $(selector).html(Lang.Wednesday);
    }
    else if (num == WeekdayEnum.Thursday) {
        if (!selector)
            return Lang.Thursday;
        $(selector).html(Lang.Thursday);
    }
    else if (num == WeekdayEnum.Friday) {
        if (!selector)
            return Lang.Friday;
        $(selector).html(Lang.Friday);
    }
    else if (num == WeekdayEnum.Saturday) {
        if (!selector)
            return Lang.Saturday;
        $(selector).html(Lang.Saturday);
    }
    else {
        $(selector).html(Lang.Unknown);
        $(selector).addClass("unknown");
    }
}

function displayBankAccountTypeEnum(selector, num) {
    if (num == BankAccountTypeEnum.Rial) {
        if (!selector)
            return Lang.BankAccountTypeRial;
        $(selector).html(Lang.BankAccountTypeRial);
    }
    else if (num == BankAccountTypeEnum.Arz) {
        if (!selector)
            return Lang.BankAccountTypeArz;
        $(selector).html(Lang.BankAccountTypeArz);
    }
    else if (num == BankAccountTypeEnum.Customer) {
        if (!selector)
            return Lang.BankAccountTypeCustomer;
        $(selector).html(Lang.BankAccountTypeCustomer);
    }
    else {
        $(selector).html(Lang.Unknown);
        $(selector).addClass("unknown");
    }
}

function displayStoredraftPayStatus(selector, num) {
    if (num == StoredraftPayStatusEnum.NotPaied) {
        if (!selector)
            return Lang.NotPaid;
        $(selector).html(Lang.NotPaid);
    }
    else if (num == StoredraftPayStatusEnum.Paied) {
        if (!selector)
            return Lang.Paid;
        $(selector).html(Lang.Paid);
    }
    else {
        $(selector).html(Lang.Unknown);
        $(selector).addClass("unknown");
    }
}

function displayLoanStatusEnum(selector, type) {
    if (type == LoanStatusEnum.Disable) {
        if (!selector)
            return Lang.Disabled;
        $(selector).html(Lang.Disabled);
        $(selector).addClass("fs0");
    }
    else if (type == LoanStatusEnum.Enable) {
        if (!selector)
            return Lang.Enabled;
        $(selector).html(Lang.Enabled);
        $(selector).addClass("fs2");
    }
    else if (type == LoanStatusEnum.Settled) {
        if (!selector)
            return Lang.Settled;
        $(selector).html(Lang.Settled);
        $(selector).addClass("fs1");
    }
    else {
        $(selector).html(Lang.Unknown);
        $(selector).addClass("unknown");
    }
}

function displayMembershipStatusEnum(selector, type) {
    if (type == MembershipStatusEnum.Disable) {
        if (!selector)
            return Lang.Disabled;
        $(selector).html(Lang.Disabled);
        $(selector).addClass("fs0");
    }
    else if (type == MembershipStatusEnum.Enable) {
        if (!selector)
            return Lang.Enabled;
        $(selector).html(Lang.Enabled);
        $(selector).addClass("fs1");
    }
    else {
        $(selector).html(Lang.Unknown);
        $(selector).addClass("unknown");
    }
}

function displayLoanType(selector, type) {
    if (type == LoanTypeEnum.Geted) {
        if (!selector)
            return Lang.Received;
        $(selector).html(Lang.Received);
        $(selector).addClass("fs0");
    }
    else if (type == LoanTypeEnum.Paied) {
        if (!selector)
            return Lang.Payable1;
        $(selector).html(Lang.Payable1);
        $(selector).addClass("fs1");
    }
    else {
        $(selector).html(Lang.Unknown);
        $(selector).addClass("unknown");
    }
}
function displayStoreDraftPayMethod(selector, type) {
    if (type == StoreDraftPayMethodEnum.PayMethodCash) {
        if (!selector)
            return Lang.PayMethodCash;
        $(selector).html(Lang.PayMethodCash);
    }
    else if (type == StoreDraftPayMethodEnum.PayMethodCheck) {
        if (!selector)
            return Lang.PayMethodCheck;
        $(selector).html(Lang.PayMethodCheck);
    }
    else if (type == StoreDraftPayMethodEnum.PayMethodOwe) {
        if (!selector)
            return Lang.PayMethodOwe;
        $(selector).html(Lang.PayMethodOwe);
    }
    else if (type == StoreDraftPayMethodEnum.PayMethodComplex) {
        if (!selector)
            return Lang.PayMethodComplex;
        $(selector).html(Lang.PayMethodComplex);
    }
    else if (type == StoreDraftPayMethodEnum.PayMethodBank) {
        if (!selector)
            return Lang.PayMethodBank;
        $(selector).html(Lang.PayMethodBank);
    }
    else {
        $(selector).html(Lang.Unknown);
        $(selector).addClass("unknown");
    }
}
function displayOfferStepSaleType(selector, type) {
    if (type == OfferStepSaleTypeEnum.Numerical) {
        if (!selector)
            return Lang.Numerical;
        $(selector).html(Lang.Numerical);
    }
    else if (type == OfferStepSaleTypeEnum.Amount) {
        if (!selector)
            return Lang.Amount1;
        $(selector).html(Lang.Amount1);
    }
    else if (type == OfferStepSaleTypeEnum.Cash) {
        if (!selector)
            return Lang.PayMethodCash;
        $(selector).html(Lang.PayMethodCash);
    }
    else if (type == OfferStepSaleTypeEnum.Check1) {
        if (!selector)
            return Lang.PayMethodCheck1;
        $(selector).html(Lang.PayMethodCheck1);
    }
    else if (type == OfferStepSaleTypeEnum.bank) {
        if (!selector)
            return Lang.PayMethodBank;
        $(selector).html(Lang.PayMethodBank);
    }
    else if (type == OfferStepSaleTypeEnum.Check2) {
        if (!selector)
            return Lang.PayMethodCheck2;
        $(selector).html(Lang.PayMethodCheck2);
    }
}
function displayOfferStepRewardType(selector, type) {
    if (type == OfferStepRewardTypeEnum.Commodity) {
        if (!selector)
            return Lang.Commodity;
        $(selector).html(Lang.Commodity);
    }
    else if (type == OfferStepRewardTypeEnum.Deadline) {
        if (!selector)
            return Lang.Deadline;
        $(selector).html(Lang.Deadline);
    }
    if (type == OfferStepRewardTypeEnum.Discount) {
        if (!selector)
            return Lang.Discount;
        $(selector).html(Lang.Discount);
    }
    else if (type == OfferStepRewardTypeEnum.Pursant) {
        if (!selector)
            return Lang.Pursant;
        $(selector).html(Lang.Pursant);
    }
}
function displayOfferType(selector, type) {
    if (type == OfferTypeEnum.Commodity) {
        if (!selector)
            return Lang.Commodity;
        $(selector).html(Lang.Commodity);
    }
    else if (type == OfferTypeEnum.GoodsGroup) {
        if (!selector)
            return Lang.GoodsGroup;
        $(selector).html(Lang.GoodsGroup);
    }
    if (type == OfferTypeEnum.Factors) {
        if (!selector)
            return Lang.Factors;
        $(selector).html(Lang.Factors);
    }
}
function displaySmsStatus(selector, type) {
    if (type == SmsStatusEnum.Temp) {
        if (!selector)
            return Lang.SmsStatusTemp;
        $(selector).html(Lang.SmsStatusTemp);
    }
    else if (type == SmsStatusEnum.New) {
        if (!selector)
            return Lang.SmsStatusNew;
        $(selector).html(Lang.SmsStatusNew);
    }
    else if (type == SmsStatusEnum.Sent) {
        if (!selector)
            return Lang.SmsStatusSent;
        $(selector).html(Lang.SmsStatusSent);
    }
    else if (type == SmsStatusEnum.Failed) {
        if (!selector)
            return Lang.SmsStatusFailed;
        $(selector).html(Lang.SmsStatusFailed);
    }
    else {
        if (!selector)
            return Lang.NotSpecified;
        $(selector).html(Lang.NotSpecified + status);
        $(selector).addClass("unknown");
    }
}
function displayTabletMsgMode(selector, type) {
    if (type == TabletMsgModeEnum.Send) {
        if (!selector)
            return Lang.TabletMsgModeSend;
        $(selector).html(Lang.TabletMsgModeSend);
    }
    else if (type == TabletMsgModeEnum.Receive) {
        if (!selector)
            return Lang.TabletMsgModeReceive;
        $(selector).html(Lang.TabletMsgModeReceive);
    }
    else {
        if (!selector)
            return Lang.NotSpecified;
        $(selector).html(Lang.NotSpecified + status);
        $(selector).addClass("unknown");
    }
}
function displayTabletWorkFlowType(selector, type) {
    if (type == TabletWorkflowTypeEnum.NewF) {
        if (!selector)
            return Lang.NewF;
        $(selector).html(Lang.NewF);
    }
    else if (type == WorkflowTypeEnum.OldF) {
        if (!selector)
            return Lang.OldF;
        $(selector).html(Lang.OldF);
    }
    else {
        if (!selector)
            return Lang.StatusUnknown;
        $(selector).html(Lang.StatusUnknown + type);
        $(selector).addClass("unknown");
    }
}

function displayBasketStatus(selector, type) {
    if (type == BasketStatusEnum.New) {
        if (!selector)
            return Lang.BasketStatusNew;
        $(selector).html(Lang.BasketStatusNew);
    }
    else if (type == BasketStatusEnum.Confirmed) {
        if (!selector)
            return Lang.BasketStatusConfirmed;
        $(selector).html(Lang.BasketStatusConfirmed);
    }
    else if (type == BasketStatusEnum.Canceled) {
        if (!selector)
            return Lang.BasketStatusCanceled;
        $(selector).html(Lang.BasketStatusCanceled);
    }
    else {
        if (!selector)
            return Lang.StatusUnknown;
        $(selector).html(Lang.StatusUnknown + type);
        $(selector).addClass("unknown");
    }
}

function displayBasketPayStatus(selector, type) {
    if (type == BasketPayStatusEnum.NotPaid) {
        if (!selector)
            return Lang.BasketPayStatusNotPaid;
        $(selector).html(Lang.BasketPayStatusNotPaid);
    }
    else if (type == BasketPayStatusEnum.InPayment) {
        if (!selector)
            return Lang.BasketPayStatusInPayment;
        $(selector).html(Lang.BasketPayStatusInPayment);
    }
    else if (type == BasketPayStatusEnum.Paid) {
        if (!selector)
            return Lang.BasketPayStatusPaid;
        $(selector).html(Lang.BasketPayStatusPaid);
    }
    else {
        if (!selector)
            return Lang.StatusUnknown;
        $(selector).html(Lang.StatusUnknown + type);
        $(selector).addClass("unknown");
    }
}


function displayBasketPayMethod(selector, type) {
    if (type == BasketPayMethodEnum.Online) {
        if (!selector)
            return Lang.BasketPayMethodOnline;
        $(selector).html(Lang.BasketPayMethodOnline);
    }
    else if (type == BasketPayMethodEnum.Coupon) {
        if (!selector)
            return Lang.BasketPayMethodCoupon;
        $(selector).html(Lang.BasketPayMethodCoupon);
    }
    else if (type == BasketPayMethodEnum.Cash) {
        if (!selector)
            return Lang.BasketPayMethodCash;
        $(selector).html(Lang.BasketPayMethodCash);
    }
    else if (type == BasketPayMethodEnum.InPlace) {
        if (!selector)
            return Lang.BasketPayMethodInPlace;
        $(selector).html(Lang.BasketPayMethodInPlace);
    }
    else {
        if (!selector)
            return Lang.StatusUnknown;
        $(selector).html(Lang.StatusUnknown + type);
        $(selector).addClass("unknown");
    }
}
function _ShowAdvertisementTypeInternal(type) {
    switch (type)
    {
        case AdvertisementTypeEnum.Estate10: return Lang.AdvEstate10;
        case AdvertisementTypeEnum.Estate11: return Lang.AdvEstate11;
        case AdvertisementTypeEnum.Estate12: return Lang.AdvEstate12;
        case AdvertisementTypeEnum.Estate13: return Lang.AdvEstate13;
        case AdvertisementTypeEnum.Estate14: return Lang.AdvEstate14;
        case AdvertisementTypeEnum.Car20: return Lang.AdvCar20;
        case AdvertisementTypeEnum.Car21: return Lang.AdvCar21;
        case AdvertisementTypeEnum.Car22: return Lang.AdvCar22;
        case AdvertisementTypeEnum.Electric30: return Lang.AdvElectric30;
        case AdvertisementTypeEnum.Electric31: return Lang.AdvElectric31;
        case AdvertisementTypeEnum.Electric32: return Lang.AdvElectric32;
        case AdvertisementTypeEnum.Electric33: return Lang.AdvElectric33;
        case AdvertisementTypeEnum.Electric34: return Lang.AdvElectric34;
        case AdvertisementTypeEnum.Furniture: return Lang.AdvFurniture;
        case AdvertisementTypeEnum.Personal50: return Lang.AdvPersonal50;
        case AdvertisementTypeEnum.Personal51: return Lang.AdvPersonal51;
        case AdvertisementTypeEnum.Personal52: return Lang.AdvPersonal52;
        case AdvertisementTypeEnum.Games: return Lang.AdvGames;
        case AdvertisementTypeEnum.Services: return Lang.AdvServices;
        case AdvertisementTypeEnum.Employement: return Lang.AdvEmployement;
        case AdvertisementTypeEnum.Other: return Lang.AdvOther;
    }
    return "unknown";
}
function ShowAdvertisementType(selector, type) {
    var res = _ShowAdvertisementTypeInternal(type);
    if (res == "unknown")
    {
        if (!selector)
            return Lang.StatusUnknown;
        $(selector).html(Lang.StatusUnknown + type);
        $(selector).addClass("unknown");
    }
    if (!selector)
        return res;
    $(selector).html(res);
}

function _ShowAssessmentTypeInternal(type) {
    switch (type) {
        case AssessmentTypeEnum.Manually: return Lang.Manually;
        case AssessmentTypeEnum.ManuallySelected: return Lang.ManuallySelected;
        case AssessmentTypeEnum.SumOfFactorRows: return Lang.SumOfFactorRows;
        case AssessmentTypeEnum.SumOfFactorServices: return Lang.SumOfFactorServices;
        case AssessmentTypeEnum.SumOfFactorsPrice: return Lang.SumOfFactorsPrice;
        case AssessmentTypeEnum.NumOfNotPassedChecks: return Lang.NumOfNotPassedChecks;
        case AssessmentTypeEnum.SumOFPassedCheckPrice: return Lang.SumOFPassedCheckPrice;
        case AssessmentTypeEnum.NumOFPassedCheck: return Lang.NumOFPassedCheck;
        case AssessmentTypeEnum.OweAmount: return Lang.OweAmount;
        case AssessmentTypeEnum.CashPay: return Lang.CashPay;
        case AssessmentTypeEnum.BankPay: return Lang.BankPay;
        case AssessmentTypeEnum.NumOfServiceInServiceGroup: return Lang.NumOfServiceInServiceGroup;
        case AssessmentTypeEnum.CustomerCaverage: return Lang.CustomerCaverage;
    }
    return "unknown";
}
function ShowAssessmentType(selector, type) {
    var res = _ShowAssessmentTypeInternal(type);
    if (res == "unknown") {
        if (!selector)
            return Lang.StatusUnknown;
        $(selector).html(Lang.StatusUnknown + type);
        $(selector).addClass("unknown");
    }
    if (!selector)
        return res;
    $(selector).html(res);
}

function ShowAdvertisementGeneralType(selector, type) {
    if (type == AdvertisementGeneralTypeEnum.Supply) {
        if (!selector)
            return Lang.AdvGTSupply;
        $(selector).html(Lang.AdvGTSupply);
    }
    else if (type == AdvertisementGeneralTypeEnum.Demand) {
        if (!selector)
            return Lang.AdvGTDemand;
        $(selector).html(Lang.AdvGTDemand);
    }
    else {
        if (!selector)
            return Lang.StatusUnknown;
        $(selector).html(Lang.StatusUnknown + type);
        $(selector).addClass("unknown");
    }
}

function ShowAdvertisementStatus(selector, type) {
    if (type == AdvertisementStatusEnum.New) {
        if (!selector)
            return Lang.AdvNew;
        $(selector).html(Lang.AdvNew);
    }
    else if (type == AdvertisementStatusEnum.Rejected) {
        if (!selector)
            return Lang.AdvRejected;
        $(selector).html(Lang.AdvRejected);
    }
    else if (type == AdvertisementStatusEnum.Published) {
        if (!selector)
            return Lang.AdvPublished;
        $(selector).html(Lang.AdvPublished);
    }
    else if (type == AdvertisementStatusEnum.Sold) {
        if (!selector)
            return Lang.AdvSold;
        $(selector).html(Lang.AdvSold);
    }
    else if (type == AdvertisementStatusEnum.Expired) {
        if (!selector)
            return Lang.AdvExpired;
        $(selector).html(Lang.AdvExpired);
    }
    else {
        if (!selector)
            return Lang.StatusUnknown;
        $(selector).html(Lang.StatusUnknown + type);
        $(selector).addClass("unknown");
    }
}
function ShowClaimStatus(selector, type) {
    if (type == ClaimStatusEnum.New) {
        if (!selector)
            return Lang.ClaimStatusNew;
        $(selector).html(Lang.ClaimStatusNew);
    }
    else if (type == ClaimStatusEnum.Done) {
        if (!selector)
            return Lang.ClaimStatusDone;
        $(selector).html(Lang.ClaimStatusDone);
    }
    else {
        if (!selector)
            return Lang.StatusUnknown;
        $(selector).html(Lang.StatusUnknown + type);
        $(selector).addClass("unknown");
    }
}
function ShowClaimType(selector, type) {
    if (type == ClaimTypeEnum.Claim) {
        if (!selector)
            return Lang.ClaimTypeClaim;
        $(selector).html(Lang.ClaimTypeClaim);
    }
    else if (type == ClaimTypeEnum.Suggest) {
        if (!selector)
            return Lang.ClaimTypeSuggest;
        $(selector).html(Lang.ClaimTypeSuggest);
    }
    else {
        if (!selector)
            return Lang.StatusUnknown;
        $(selector).html(Lang.StatusUnknown + type);
        $(selector).addClass("unknown");
    }
}
function ShowCalcMethod(selector, type) {
    if (type == CalcMethodEnum.Direct) {
        if (!selector)
            return Lang.CalcMethodDirect;
        $(selector).html(Lang.CalcMethodDirect);
    }
    else if (type == CalcMethodEnum.Descending) {
        if (!selector)
            return Lang.CalcMethodDescending;
        $(selector).html(Lang.CalcMethodDescending);
    }
    else if (type == CalcMethodEnum.None) {
        if (!selector)
            return Lang.CalcMethodNone;
        $(selector).html(Lang.CalcMethodNone);
    }
    else {
        if (!selector)
            return Lang.StatusUnknown;
        $(selector).html(Lang.StatusUnknown + type);
        $(selector).addClass("unknown");
    }
}
// -------------------------------------
// Display Enums : Ended
// -------------------------------------