﻿/*!
 * jQuery QueryBuilder 2.0.1
 * Reference language file
 * Licensed under MIT (http://opensource.org/licenses/MIT)
 */

jQuery.fn.queryBuilder.defaults({ lang: {
  "add_rule": "افزودن فیلتر",
  "add_group": "افزودن گروه",
  "delete_rule": "حذف",
  "delete_group": "حذف",
  "conditions": {
    "AND": "AND (و)",
    "OR": "OR (یا)"
  },
  "operators": {
    "equal": "برابر باشد با",
    "not_equal": "برابر نباشد با",
    "in": "موجود در",
    "not_in": "موجود نباشد در",
    "less": "کوچکتر از",
    "less_or_equal": "کوچکتر یا مساوی",
    "greater": "بزرگتر از",
    "greater_or_equal": "بزرگتر یا مساوی",
    "between": "بین",
    "begins_with": "شروع می‌شود با",
    "not_begins_with": "شروع نمی‌شود با",
    "contains": "شامل می‌شود",
    "not_contains": "شامل نمی‌شود",
    "ends_with": "خاتمه می‌یابد با",
    "not_ends_with": "خاتمه نمی‌یابد با",
    "is_empty": "تهی است",
    "is_not_empty": "تهی نیست",
    "is_null": "پوچ است",
    "is_not_null": "پوچ نیست"
  },
  "errors": {
    "no_filter": "هیچ فیلتری انتخاب نشده است",
    "empty_group": "گروه تهی است",
    "radio_empty": "هیچ مقداری انتخاب نشده است",
    "checkbox_empty": "هیچ مقداری انتخاب نشده است",
    "select_empty": "هیچ مقداری انتخاب نشده است",
    "string_empty": "مقدار تهی است",
    "string_exceed_min_length": "باید حداقل شامل {0} کاراکتر باشد",
    "string_exceed_max_length": "نباید بیشتر از {0} کاراکتر باشد",
    "string_invalid_format": "فرمت صحیح نیست ({0})",
    "number_nan": "لطفا عدد را وارد نمایید",
    "number_not_integer": "لطفا یک عدد صحیح وارد نمایید",
    "number_not_double": "لطفا یک عدد حقیقی وارد نمایید",
    "number_exceed_min": "باید از {0} بزرگتر باشد",
    "number_exceed_max": "باید از {0} کوچکتر باشد",
    "number_wrong_step": "باید مضربی از {0} باشد",
    "datetime_empty": "مقدار وارد شده تهی است",
    "datetime_invalid": "فرمت تاریخ معتبر نیست ({0})",
    "datetime_exceed_min": "باید بعد از {0} باشد",
    "datetime_exceed_max": "باید قبل از{0} باشد",
    "boolean_not_valid": "مقدار وارد شده باید true یا false باشد",
    "operator_not_multiple": "اپراتور {0} نمی‌تواند چند مقدار بگیرد"
  }
}});