﻿window.app = window.app || {};
Backbone.emulateHTTP = true;

app.Models = app.Models || {};
app.Views = app.Models || {};
app.Collections = app.Collections || {};

// Models
app.Models.ServiceImage = Backbone.Model.extend({
    defaults: {
        Title: "",
        MainImage: "",
        ThumbImage: ""
    }
});

app.Models.AdvertisementImage = Backbone.Model.extend({
    defaults: {
        Title: "",
        MainImage: "",
        ThumbImage: ""
    }
});

app.Models.Service = Backbone.Model.extend({
    defaults: {
        Coding: '--',
        OrderPoint: 0,
        MinStock: 0,
        Income: 0,
        Outcome: 0,
        ToMonetary: "",
        ToMonetaryWithCommas: "",
        Availability: ""
    },

    constructor: function() {
        // Listen to model changes and update computed properties
        this.on("change:Price", this.setToMonetary, this);
        this.on("change:ToMonetary", this.setToMonetaryWithCommas, this);
        this.on("change:Income change:Outcome change:OrderPoint change:MinStock", this.setAvailability, this);
        this.on("change:HasOffers", this.setShouldShow, this);


        // Call super
        Backbone.Model.apply(this, arguments);
    },

    initialize: function() {
        this.set("images", new app.Collections.ServiceImageList([], {id: this.get("ServiceID")}));
        this.get("images").fetch({reset: true});
    },

    setToMonetary: function() {
        this.set("ToMonetary", toMonetary(this.get("Price")));
    },

    setToMonetaryWithCommas: function () {
        this.set("ToMonetaryWithCommas", numberWithCommas(this.get("ToMonetary")));
    },

    setAvailability: function () {
        var availability;
        if (this.get("Income") - this.get("Outcome") > this.get("OrderPoint")) {
            availability = 'full';
        } else if (this.get("Income") - this.get("Outcome") <= this.get("MinStock")) {
            availability = 'medium';
        } else {
            availability = 'empty';
        }

        this.set("Availability", availability);
    },

    setShouldShow: function () {
        this.set("ShouldShow", this.get("HasOffers") == -1 ? false : true);
    },
});

// Collections
app.Collections.ServiceImageList = Backbone.Collection.extend({
    initialize: function(models, options) {
        this.ServiceId = options.id;
    },

    model: app.Models.ServiceImage,

    url: function() {
        return "Api/ServiceImage/" + this.ServiceId;
    }
});

app.Collections.AdvertisementImageList = Backbone.Collection.extend({
    initialize: function (models, options) {
        this.AdvertisementID = options.id;
    },

    model: app.Models.AdvertisementImage,

    url: function () {
        return "Api/AdvertisementImage/" + this.AdvertisementID;
    }
});

app.Collections.Services = Backbone.Collection.extend({
    model: app.Models.Service,

    // Parse the XML comming from the server and store each item as a Backbone model.
    // We accept a moneyUnit, which will be set on the newly created model
    parseXml: function(xml, moneyUnit) {
        var collection = this;
        $(xml).find("R").each(function() {
            var service = new app.Models.Service(NodeToJson(this));
            service.set("moneyUnit", moneyUnit);
            collection.add(service);
        });
    }
});

// Views
app.Views.ServiceImageView = Backbone.View.extend({
    initialize: function() {
        this.listenTo(this.model, "destroy", this.destroySelf);

        this.render();
    },

    destroySelf: function() {
        Growl.Notice(Lang.DeletedSuccessfully);
        this.remove();
    },

    events: {
        "click .remove-image": "destroy"
    },

    destroy: function() {
        this.model.destroy({wait: true});
    },

    render: function() {
        this.$el.html(CompileHB("service-image-template", this.model.attributes));
        return this;
    }
});

app.Views.AdvertisementImageView = Backbone.View.extend({
    initialize: function () {
        this.listenTo(this.model, "destroy", this.destroySelf);

        this.render();
    },

    destroySelf: function () {
        Growl.Notice(Lang.DeletedSuccessfully);
        this.remove();
    },

    events: {
        "click .remove-image": "destroy"
    },

    destroy: function () {
        this.model.destroy({ wait: true });
    },

    render: function () {
        this.$el.html(CompileHB("service-image-template", this.model.attributes));
        return this;
    }
});

app.Views.ServiceImageListView = Backbone.View.extend({
    initialize: function() {
        this.listenTo(this.collection, "add", this.addOne);
        this.listenTo(this.collection, "reset", this.render);
        this.listenTo(this.collection, "remove", this.checkEmpty);

        this.render();
    },

    checkEmpty: function() {
        if (this.collection.length == 0) {
            this.render();
        }
    },

    render: function() {
        this.$el.html("");

        if (this.collection.length == 0) {
            this.$el.html(CompileHB("empty-data", { message: Lang.NoImageHasBeenUploadedYet.replace("{0}", Lang.UploadFile) }));
        } else {
            this.collection.each(this.addOne, this);
        }
    },
    
    addOne: function(serviceImage) {
        if (this.collection.length > 0) {
            this.$(".empty-data-container").remove();
        }

        var serviceImageView = new app.Views.ServiceImageView({model: serviceImage});
        this.$el.append(serviceImageView.el);
    }
});

app.Views.AdvertisementImageListView = Backbone.View.extend({
    initialize: function () {
        this.listenTo(this.collection, "add", this.addOne);
        this.listenTo(this.collection, "reset", this.render);
        this.listenTo(this.collection, "remove", this.checkEmpty);

        this.render();
    },

    checkEmpty: function () {
        if (this.collection.length == 0) {
            this.render();
        }
    },

    render: function () {
        this.$el.html("");

        if (this.collection.length == 0) {
            this.$el.html(CompileHB("empty-data", { message: Lang.NoImageHasBeenUploadedYet.replace("{0}", Lang.UploadFile) }));
        } else {
            this.collection.each(this.addOne, this);
        }
    },

    addOne: function (advertisementImage) {
        if (this.collection.length > 0) {
            this.$(".empty-data-container").remove();
        }

        var advertisementImageView = new app.Views.AdvertisementImageView({ model: advertisementImage });
        this.$el.append(advertisementImageView.el);
    }
});

app.Views.ServiceImageViewer = Backbone.View.extend({
    initialize: function () {
        this.listenTo(this.collection, "add", this.render);
        this.listenTo(this.collection, "reset", this.render);
    },

    render: function () {
        this.$el.html("");
        this.collection.each(this.addOne, this);
        return this;
    },

    addOne: function (serviceImage) {
        var template = CompileHB("carousel-image-template", serviceImage.attributes);
        this.$el.append(template);
    }
});

app.Views.ServiceCatalog = Backbone.View.extend({
    className: "col-xs-12 col-sm-6 col-lg-4",
    initialize: function() {
        this.listenTo(this.model.get("images"), "reset", this.showImages);

        this.render();
    },

    showImages: function() {
        var carouselInner = this.$(".carousel-inner");
        carouselInner.html("");

        this.model.get("images").each(function(image) {
            var imageTemplate = CompileHB("carousel-image-template", image.attributes);
            carouselInner.append(imageTemplate);
        });

        carouselInner.find(".item").first().addClass("active");
    },

    render: function() {
        var itemTemplate = CompileHB("catalog-template", this.model.attributes);
        this.$el.html(itemTemplate);
    }
});

app.Views.Services = Backbone.View.extend({
    initialize: function(options) {
        this.pageSize = options.pageSize || 6;
        this.pageIndex = options.pageIndex || 1;
        this.totalPages = Math.ceil(this.collection.length / this.pageSize);

        this.render();
        this.updatePager();
    },

    events: {
        'click .next': "nextPage",
        'click .prev': "prevPage"
    },

    nextPage: function() {
        if (this.pageIndex < this.totalPages) {
            this.pageIndex += 1;
            this.render();
        }

        this.updatePager();
    },

    prevPage: function() {
        if (this.pageIndex > 1) {
            this.pageIndex -= 1;
            this.render();
        }

        this.updatePager();
    },

    updatePager: function() {
        // If we reach the first page, we should disable the 'prev' button
        if (this.pageIndex === 1) {
            this.$('.prev').addClass('disabled');
        } else {
            this.$('.prev').removeClass('disabled');
        }

        // If we reach the first page, we should disable the 'prev' button
        if (this.pageIndex === this.totalPages) {
            this.$('.next').addClass('disabled');
        } else {
            this.$('.next').removeClass('disabled');
        }
    },

    render: function () {
        var html = CompileHB("paged-services", {
                TotalPages: this.totalPages,
                PageIndex: this.pageIndex
            }),
            start = this.pageIndex - 1,
            end = start + this.pageSize;
        this.$el.html(html);

        _.each(this.collection.models.slice(start, end), this.addOne, this);
        if (!(typeof bindMemberEvents === 'undefined'))
            bindMemberEvents();
    },

    addOne: function(service) {
        var serviceView = new app.Views.ServiceCatalog({ model: service });
        this.$("#services-container").append(serviceView.el);
    }
});
