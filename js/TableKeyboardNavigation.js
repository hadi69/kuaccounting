﻿var TKN = (function () {
    function TKN (table) {
        var that = this;
        this.table = $(table);

        shortcut.remove('Up');
        shortcut.remove('Down');
        shortcut.remove('Left');
        shortcut.remove('Right');
        this.selectFirstRow();
        shortcut.add('Up', function () {
            that.selectPrevRow();
        });
        shortcut.add('Down', function () {
            that.selectNextRow();
        });
        shortcut.add('Left', function () {
            that.expandRow();
        });
        shortcut.add('Right', function () {
            that.collapseRow();
        });
        shortcut.add('Enter', function () {
            that.submitPage();
        });
    }

    TKN.prototype.getCurrentRow = function () {
        return this.table.find('tr.selected');
    };
    // Expand a parent row
    TKN.prototype.expandRow = function () {
        var currentRow = this.getCurrentRow();
        if (!currentRow) {
            return;
        }
        // The current row should be a parent row and should not be expanded!
        if (currentRow.hasClass("hasChild") && !currentRow.hasClass("treegrid-expanded")) {
            $(currentRow).click();
        }
    };

    // Collapse a parent row
    TKN.prototype.collapseRow = function () {
        var currentRow = this.getCurrentRow();
        if (!currentRow) {
            return;
        }
        // If the current row is an expanded parent row, collapse it
        if (currentRow.hasClass("hasChild") && currentRow.hasClass("treegrid-expanded")) {
            currentRow.click();
            return;
        }
        // If the current row is a child row, or a collapsed parent row, try to get it's parent
        // The parent should have the class "treegrid-expanded"
        var parentRow = currentRow.prevAll("tr.hasChild.treegrid-expanded").first();
        // If the current row is a child of an expanded parent row, it should have the class "treegrid-parent-N" where N is the id of it's parent
        // If the current row is missing the class "treegrid-parent-N" then, we should do nothing
        if (parentRow && parentRow.length > 0 && currentRow.attr("class").indexOf("treegrid-parent") > 0) {
            parentRow.click();
            this.selectRow(parentRow);
        }
    };

    // This is obvious! It clears all row selections!
    TKN.prototype.clearAllSelections = function () {
        this.table.find("tr.selected").removeClass("selected");
    };

    // Select a row
    TKN.prototype.selectRow = function (row) {
        this.clearAllSelections();
        $(row).addClass("selected");
        // Most of the time the selected row is not visible and we should scroll it into view.
        // We use the "scrollintoview" library to eliminate the bad UX with the builtin scrollIntoView function, which is terrible
        $(row).scrollintoview({
            duration: 10 // Be very quick, please!
        });
    };

    // Select first visible row, except the header row. It should be visible, because we don't want to select rows which are hidden under their parents
    TKN.prototype.selectFirstRow = function () {
        var rows = this.table.find("tr:visible:not(:regex(class, ^th))");
        if (rows.length > 0) {
            this.selectRow(rows[0]);
        }
    };

    // Select last visible row, except the header row. It should be visible, because we don't want to select rows which are hidden under their parents
    TKN.prototype.selectLastRow = function () {
        var rows = this.table.find("tr:visible:not(:regex(class, ^th))");
        if (rows.length > 0) {
            this.selectRow(rows[rows.length - 1]);
        }
    };

    // Select next row
    TKN.prototype.selectNextRow = function () {
        var currentRow = this.getCurrentRow(), nextRow;
        this.clearAllSelections();
        // If no rows are selected, we select the first row
        if (!currentRow) {
            this.selectFirstRow();
            return;
        }

        // Get next visible row and select it, and if there is no next row, select the first row
        nextRow = currentRow.nextAll("tr:not(.th):visible").first();
        if (nextRow && nextRow.length > 0) {
            this.selectRow(nextRow);
            return;
        } else {
            this.selectFirstRow();
        }
    };

    // Select the currently highlighted row and close the window
    TKN.prototype.submitPage = function () {
        var selectedRow = this.getCurrentRow();
        if (selectedRow && selectedRow.length > 0) {
            selectedRow.click();
        }
    };

    // Select previous row
    TKN.prototype.selectPrevRow = function () {
        var currentRow = this.getCurrentRow(), prevRow;
        this.clearAllSelections();
        // If there are no selected rows, we select the last row
        if (!currentRow) {
            this.selectLastRow();
            return;
        }
        // Find the previous row and select it and if there is no previous row select the last row
        prevRow = currentRow.prevAll("tr:not(.th):visible").first();
        if (prevRow && prevRow.length > 0) {
            this.selectRow(prevRow);
            return;
        } else {
            this.selectLastRow();
        }
    };

    return TKN;

})();