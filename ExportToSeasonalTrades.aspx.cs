﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class ExportToSeasonalTrades : System.Web.UI.Page
{
    System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
    string mFilePath = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["do"] == "download")
        {
            Response.ContentType = "application/octet-stream";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + Request.QueryString["name"]);
            Response.TransmitFile(Path.Combine(Server.MapPath("~/files/season-trade"), Request.QueryString["name"]));
            Response.End();
        }
        if (SiteSettings.User == null || !SiteSettings.User.HasAccess("admin_database"))
            Response.Redirect("~/default.aspx");

        msg.Visible = false;
        error.Visible = false;

        if (!IsPostBack)
        {
            Page.SetLang();
            int year = cal.GetYear(DateTime.Now);
            fFromDate.Text = string.Format("{0}/01/01", year);
            IDataReader financeYearStartDate = DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");//0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                fFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            fToDate.Text = Helper.FormatDate(DateTime.Now, null);
            InitData();
        }
    }
    void InitData()
    {
        using (DbProxy proxy = new DbProxy())
        {
            int companyID = Helper.GetInt(proxy.GetConfig("CompanyID", -1), Null.NullInteger);
            if (companyID > 0)
            {
                DataRow row = proxy.FillRow("SELECT * FROM Personnel WHERE ID = " + companyID);
                if (row != null)
                {
                    companyNameField.Text = Helper.GetString(row, "Name", "");
                    //oldEconomicNoField.Text = Helper.GetString(row, "", "");
                    newEconomicNoField.Text = Helper.GetString(row, "Code", "");
                    nationalCodeField.Text = Helper.GetString(row, "NatinoalCode", "");
                    //taxPayerTypeField.Text = Helper.GetString(row, "", "");
                    //postalCodeField.Text = Helper.GetString(row, "", "");
                    //countryField.Text = Helper.GetString(row, "", "");
                    //telCodeField.Text = Helper.GetString(row, "", "");
                    telNoField.Text = Helper.GetString(row, "HomeTelNum", "");
                    //faxNoField.Text = Helper.GetString(row, "", "");
                    //registrationNoField.Text = Helper.GetString(row, "", "");
                    //hozehField.Text = Helper.GetString(row, "", "");
                    Address.Text = Helper.GetString(row, "HomeAddress", "");
                    ShahrCode.Text = Helper.GetString(row, "CityCode", "");
                    EdareKolCode.Text = Helper.GetString(row, "StateCode", "");
                    //UserName.Text = Helper.GetString(row, "", "");
                    //Band.Text = Helper.GetString(row, "", "");
                    //SabteNam.Text = Helper.GetString(row, "", "");
                    //MoadyName.Text = Helper.GetString(row, "", "");
                }
            }
        }
    }
    protected void doExport_Click(object sender, EventArgs e)
    {
        error.Text = "";
        try
        {
            ExportInternal();
        }
        catch (Exception ex)
        {
            error.Text += ex.Message;
        }
        if (error.Text.Length > 0)
            error.Visible = true;
        if (msg.Text.Length > 0)
            msg.Visible = true;
    }
    void ExportInternal()
    {
        string path = UploadFile();
        if (string.IsNullOrEmpty(path))
            return;

        using (var conn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Persist Security Info=False;"))
        {
            conn.Open();
            DateTime start = Helper.ParseDate(fFromDate.Text, DateTime.Now.AddYears(-50));
            DateTime end = Helper.ParseDate(fToDate.Text, DateTime.Now.AddYears(50));
            start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
            end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);

            ExportCompanyInfo(conn);
            ExportStoreDrafts(conn, start, end);
            ExportReturns(conn, start, end);
            ExportPurchases(conn, start, end);
            ExportRefuses(conn, start, end);

            conn.Close();
        }
        msg.Text += "<br /><b>" + LangProvider.Instance["AutoDownloadIn5Second"] + "</b>";
        HtmlMeta meta = new HtmlMeta();
        meta.HttpEquiv = "Refresh";
        meta.Content = "5;url=ExportToSeasonalTrades.aspx?do=download&name=" + mFilePath;
        this.Page.Controls.Add(meta);
        //msg.Text += string.Format("<a href='{0}'>{1} {0}</a>", mFilePath, LangProvider.Instance["DownloadFile);
    }
    string UploadFile()
    {
        string srcPath = Server.MapPath("~/files/TTMS.mdb");
        if (!File.Exists(srcPath))
        {
            error.Visible = true;
            error.Text = LangProvider.Instance["FileNotExists"] + ":files/TTMS.mdb";
            return null;
        }

        string path = Server.MapPath("~/files/season-trade");
        if (!Directory.Exists(path))
            Directory.CreateDirectory(path);
        string fileName = "TTMS-" + DateTime.Now.ToString("yyyyMMdd-HHmmss") + ".mdb";
        path = Path.Combine(path, fileName);
        File.Copy(srcPath, path);
        mFilePath = fileName;
        return path;
    }

    void ExportCompanyInfo(OleDbConnection con)
    {
        string insert = string.Format(@"INSERT INTO Moshakhasat (companyNameField, oldEconomicNoField, newEconomicNoField, nationalCodeField, taxPayerTypeField, postalCodeField, countryField, telCodeField, telNoField, faxNoField, registrationNoField, hozehField, [Address], ShahrCode, EdareKolCode, [UserName], [Band], SabteNam, MoadyName)
            VALUES({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18})"
                , Helper.QoutedString(companyNameField.Text)
                , Helper.QoutedString(oldEconomicNoField.Text)
                , Helper.QoutedString(newEconomicNoField.Text)
                , Helper.QoutedString(nationalCodeField.Text)
                , taxPayerTypeField.SelectedValue
                , Helper.QoutedString(postalCodeField.Text)
                , Helper.QoutedString(countryField.Text)
                , Helper.QoutedString(telCodeField.Text)
                , Helper.QoutedString(telNoField.Text)
                , Helper.QoutedString(faxNoField.Text)
                , Helper.QoutedString(registrationNoField.Text)
                , Helper.QoutedString(hozehField.Text)
                , Helper.QoutedString(TrimStr(Address.Text, 70))
                , Helper.QoutedString(ShahrCode.Text)
                , Helper.QoutedString(EdareKolCode.Text)
                , Helper.QoutedString(UserName.Text)
                , Band.SelectedValue
                , SabteNam.SelectedValue
                , Helper.QoutedString(MoadyName.Text));
        insert = insert.Replace("N'", "'");
        int done = 0;
        using (var cmd = con.CreateCommand())
        {
            cmd.CommandText = insert;
            cmd.CommandType = CommandType.Text;
            try
            {
                done += cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                error.Text += insert + "<br />" + ex.Message + "<br />";
            }
        }

        msg.Text += LangProvider.Instance["CompanyInfo"] + " : " + string.Format(LangProvider.Instance["ExportedMsgFormat"], done, 1) + "<br />";
    }

    void ExportStoreDrafts(OleDbConnection con, DateTime start, DateTime end)
    {
        string query = @"SELECT StoreDraftItemsView.StoreServiceName, -- [KalaKhadamatName], 
StoreDraftItemsView.Code, -- [KalaCode], 
0, --[BargashtType], 
StoreDraftItemsView.Price * StoreDraftItemsView.DeliveredAmount * ISNULL(StoreDraftItemsView.UnitCurrencyConverter, 1), -- [Price], 
StoreDraftItemsView.Tax, -- [MaliatArzeshAfzoodeh], 
StoreDraftItemsView.SingleVAT, -- [AvarezArzeshAfzoodeh], 
1, --[HCKharidarTypeCode], 
'', --[KharidarPostCode], 
'', --[KharidarPerCityCode], 
Personnel.CellNumber1, -- [KharidarTell], 
Personnel.HomeAddress, -- [KharidarAddress], 
Personnel.Name, -- [KharidarName], 
Personnel.Surname, -- [KharidarLastNameSherkatName], 
Personnel.Code, -- [KharidarEconomicNO], 
Personnel.NatinoalCode, -- [KharidarNationalCode], 
2, --[HCKharidarType1Code], 
NULL, --[KharidarHozeh], 
ISNULL(Personnel.StateCode, 0), -- [StateCode]
ISNULL(Personnel.CityCode, 0), -- [CityCode]
NULL, --[IsSent], 
NULL, --[TempKey], 
0 --[Sarjam]
FROM StoreDraftItemsView 
INNER JOIN StoreDraft ON StoreDraftItemsView.StoreDraftID = StoreDraft.ID
INNER JOIN Personnel ON StoreDraft.BuyerID = Personnel.ID
";
        query += string.Format("WHERE WorkflowStatus = 1 AND {0} <= FactorDate AND FactorDate <= {1}", Helper.QoutedDateTime(start), Helper.QoutedDateTime(end));
        DataTable dt = DbProxy.Instance.FillData(query, "shemam");
        int done = 0;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string insert = string.Format(@"INSERT INTO Foroush_Detail ([KalaKhadamatName], [KalaCode], [BargashtType], [Price], [MaliatArzeshAfzoodeh], [AvarezArzeshAfzoodeh], [HCKharidarTypeCode], [KharidarPostCode], [KharidarPerCityCode], [KharidarTell], [KharidarAddress], [KharidarName], [KharidarLastNameSherkatName], [KharidarEconomicNO], [KharidarNationalCode], [HCKharidarType1Code], [KharidarHozeh], [StateCode], [CityCode], [IsSent], [TempKey], [Sarjam])
            VALUES({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18}, {19}, {20}, {21})"
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][0]))
                , Helper.QoutedString(FixCode(Helper.GetString(dt.Rows[i][1])), true) // KalaCode
                , 0 // BargashtType
                , Helper.GetDecimal(dt.Rows[i][3], 0) // Price
                , Helper.GetDecimal(dt.Rows[i][4], 0)
                , Helper.GetDecimal(dt.Rows[i][5], 0)
                , 1 // HCKharidarTypeCode
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][7]))
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][8]))
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][9])) // CellNumber1
                , Helper.QoutedString(TrimStr(Helper.GetString(dt.Rows[i][10]), 70)) // HomeAddress, -- [KharidarAddress],
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][11]))
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][12]))
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][13]))
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][14])) // NatinoalCode
                , 2
                , "NULL" // KharidarHozeh
                , 0 //[StateCode], BAD
                , 0
                , "NULL"
                , "NULL"
                , 0);
            insert = insert.Replace("N'", "'");
            using (var cmd = con.CreateCommand())
            {
                cmd.CommandText = insert;
                cmd.CommandType = CommandType.Text;
                try
                {
                    done += cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    error.Text += insert + "<br />" + ex.Message + "<br />";
                }
            }
        }

        msg.Text += LangProvider.Instance["StoreDraft"] + " : " + string.Format(LangProvider.Instance["ExportedMsgFormat"], done, dt.Rows.Count) + "<br />";
        dt.Dispose();
    }

    private string FixCode(string code)
    {
        int val = Helper.GetInt(code, int.MinValue);
        if (val == int.MinValue)
            return null;
        if (val > 1000000000)
            return null;
        return code;
    }

    private string TrimStr(string str, int maxLen)
    {
        if (string.IsNullOrEmpty(str) || str.Length <= maxLen)
            return str;
        return str.Substring(0, maxLen);
    }

    void ExportReturns(OleDbConnection con, DateTime start, DateTime end)
    {
        string query = @"SELECT ReturnItemView.StoreServiceName, -- [KalaKhadamatName], 
ReturnItemView.Code, -- [KalaCode], 
1, --[BargashtType], 
ReturnItemView.Price * ReturnItemView.Num * ISNULL(ReturnItemView.UnitCurrencyConverter, 1), -- [Price], 
ReturnItemView.Tax, -- [MaliatArzeshAfzoodeh], 
ReturnItemView.SingleVAT, -- [AvarezArzeshAfzoodeh], 
1, --[HCKharidarTypeCode], 
'', --[KharidarPostCode], 
'', --[KharidarPerCityCode], 
Personnel.CellNumber1, -- [KharidarTell], 
Personnel.HomeAddress, -- [KharidarAddress], 
Personnel.Name, -- [KharidarName], 
Personnel.Surname, -- [KharidarLastNameSherkatName], 
Personnel.Code, -- [KharidarEconomicNO], 
Personnel.NatinoalCode, -- [KharidarNationalCode], 
2, --[HCKharidarType1Code], 
NULL, --[KharidarHozeh], 
ISNULL(Personnel.StateCode, 0), -- [StateCode]
ISNULL(Personnel.CityCode, 0), -- [CityCode]
NULL, --[IsSent], 
NULL, --1[TempKey], 
0 --[Sarjam]
FROM ReturnItemView 
INNER JOIN [Returns] ON ReturnItemView.ReturnID = [Returns].ID
INNER JOIN Personnel ON [Returns].BuyerID = Personnel.ID
";
        query += string.Format("WHERE WorkflowStatus = 1 AND {0} <= ReturnDate AND ReturnDate <= {1}", Helper.QoutedDateTime(start), Helper.QoutedDateTime(end));
        DataTable dt = DbProxy.Instance.FillData(query, "shemam");
        int done = 0;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string insert = string.Format(@"INSERT INTO Foroush_Detail ([KalaKhadamatName], [KalaCode], [BargashtType], [Price], [MaliatArzeshAfzoodeh], [AvarezArzeshAfzoodeh], [HCKharidarTypeCode], [KharidarPostCode], [KharidarPerCityCode], [KharidarTell], [KharidarAddress], [KharidarName], [KharidarLastNameSherkatName], [KharidarEconomicNO], [KharidarNationalCode], [HCKharidarType1Code], [KharidarHozeh], [StateCode], [CityCode], [IsSent], [TempKey], [Sarjam])
            VALUES({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18}, {19}, {20}, {21})"
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][0]))
                , Helper.QoutedString(FixCode(Helper.GetString(dt.Rows[i][1])), true) // KalaCode
                , 1 // BargashtType
                , Helper.GetDecimal(dt.Rows[i][3], 0) // Price
                , Helper.GetDecimal(dt.Rows[i][4], 0)
                , Helper.GetDecimal(dt.Rows[i][5], 0)
                , 1 // HCKharidarTypeCode
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][7]))
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][8]))
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][9])) // CellNumber1
                , Helper.QoutedString(TrimStr(Helper.GetString(dt.Rows[i][10]), 70))
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][11]))
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][12]))
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][13]))
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][14])) // NatinoalCode
                , 2
                , "NULL" // KharidarHozeh
                , 0 //[StateCode], BAD
                , 0
                , "NULL"
                , "NULL"
                , 0);
            insert = insert.Replace("N'", "'");
            using (var cmd = con.CreateCommand())
            {
                cmd.CommandText = insert;
                cmd.CommandType = CommandType.Text;
                try
                {
                    done += cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    error.Text += insert + "<br />" + ex.Message + "<br />";
                }
            }
        }

        msg.Text += LangProvider.Instance["Return"] + " : " + string.Format(LangProvider.Instance["ExportedMsgFormat"], done, dt.Rows.Count) + "<br />";
        dt.Dispose();
    }

    void ExportPurchases(OleDbConnection con, DateTime start, DateTime end)
    {
        string query = @"SELECT PurchaseItemsView.StoreServiceName, -- [KalaKhadamatName], 
StoreServiceDefView.Code, -- [KalaCode], 
0, --[BargashtType], 
PurchaseItemsView.Price * PurchaseItemsView.Amount * ISNULL(PurchaseItemsView.UnitCurrencyConverter, 1), --[Price], 
PurchaseItemsView.Tax, -- [MaliatArzeshAfzoodeh], 
PurchaseItemsView.SingleVAT, -- [AvarezArzeshAfzoodeh], 
'', -- [ForoushandePostCode], 
'', -- [ForoushandePerCityCode], 
Personnel.CellNumber1, -- [ForoushandeTell], 
Personnel.HomeAddress, -- [ForoushandeAddress], 
Personnel.Name, -- [ForoushandeName], 
Personnel.Surname, -- [ForoushandeLastNameSherkatName], 
Personnel.Code, -- [ForoushandeEconomicNO], 
Personnel.NatinoalCode, -- [ForoushandeNationalCode], 
1, -- [HCForoushandeTypeCode], 
NULL, --[ForoushandeHozeh], 
ISNULL(Personnel.StateCode, 0), -- [StateCode],  BAD
ISNULL(Personnel.CityCode, 0), -- [CityCode], BAD
NULL, --[IsSent], 
NULL, --[TempKey], 
1, --[HCForoushandeType1Code], 
0 --[Sarjam]
FROM PurchaseItemsView 
INNER JOIN Purchase ON PurchaseItemsView.PurchaseID = Purchase.ID
INNER JOIN Personnel ON Purchase.SellerID = Personnel.ID
INNER JOIN StoreServices ON PurchaseItemsView.StoreServiceID = StoreServices.ID
INNER JOIN StoreServiceDefView ON StoreServices.StoreServiceDefID = StoreServiceDefView.ID
";
        query += string.Format("WHERE WorkflowStatus = 1 AND {0} <= ReceiptDate AND ReceiptDate <= {1}", Helper.QoutedDateTime(start), Helper.QoutedDateTime(end));
        DataTable dt = DbProxy.Instance.FillData(query, "shemam");
        int done = 0;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string insert = string.Format(@"INSERT INTO Kharid_Detail ([KalaKhadamatName], [KalaCode], [BargashtType], [Price], [MaliatArzeshAfzoodeh], [AvarezArzeshAfzoodeh], [ForoushandePostCode], [ForoushandePerCityCode], [ForoushandeTell], [ForoushandeAddress], [ForoushandeName], [ForoushandeLastNameSherkatName], [ForoushandeEconomicNO], [ForoushandeNationalCode], [HCForoushandeTypeCode], [ForoushandeHozeh], [StateCode], [CityCode], [IsSent], [TempKey], [HCForoushandeType1Code], [Sarjam])
            VALUES({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18}, {19}, {20}, {21})"
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][0]))
                , Helper.QoutedString(FixCode(Helper.GetString(dt.Rows[i][1])), true) // KalaCode
                , 0 // BargashtType
                , Helper.GetDecimal(dt.Rows[i][3], 0) // Price
                , Helper.GetDecimal(dt.Rows[i][4], 0)
                , Helper.GetDecimal(dt.Rows[i][5], 0)
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][6]))
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][7]))
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][8])) // CellNumber1
                , Helper.QoutedString(TrimStr(Helper.GetString(dt.Rows[i][9]), 70))
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][10]))
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][11]))
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][12]))
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][13])) // NatinoalCode
                , 1 // HCForoushandeTypeCode
                , "NULL" // ForoushandeHozeh
                , 0 //[StateCode], BAD
                , 0
                , "NULL"
                , "NULL"
                , 1 // HCForoushandeType1Code
                , 0);
            insert = insert.Replace("N'", "'");
            using (var cmd = con.CreateCommand())
            {
                cmd.CommandText = insert;
                cmd.CommandType = CommandType.Text;
                try
                {
                    done += cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    error.Text += insert + "<br />" + ex.Message + "<br />";
                }
            }
        }

        msg.Text += LangProvider.Instance["Purchase"] + " : " + string.Format(LangProvider.Instance["ExportedMsgFormat"], done, dt.Rows.Count) + "<br />";
        dt.Dispose();
    }
    void ExportRefuses(OleDbConnection con, DateTime start, DateTime end)
    {
        string query = @"SELECT RefuseItemView.StoreServiceName, -- [KalaKhadamatName], 
StoreServiceDefView.Code, -- [KalaCode], 
1, --[BargashtType], 
RefuseItemView.Price * RefuseItemView.Num * ISNULL(RefuseItemView.UnitCurrencyConverter, 1), --[Price], 
RefuseItemView.Tax, -- [MaliatArzeshAfzoodeh], 
RefuseItemView.SingleVAT, -- [AvarezArzeshAfzoodeh], 
'', -- [ForoushandePostCode], 
'', -- [ForoushandePerCityCode], 
Personnel.CellNumber1, -- [ForoushandeTell], 
Personnel.HomeAddress, -- [ForoushandeAddress], 
Personnel.Name, -- [ForoushandeName], 
Personnel.Surname, -- [ForoushandeLastNameSherkatName], 
Personnel.Code, -- [ForoushandeEconomicNO], 
Personnel.NatinoalCode, -- [ForoushandeNationalCode], 
1, -- [HCForoushandeTypeCode], 
NULL, --[ForoushandeHozeh], 
ISNULL(Personnel.StateCode, 0), -- [StateCode]
ISNULL(Personnel.CityCode, 0), -- [CityCode]
NULL, --[IsSent], 
NULL, --[TempKey], 
1, --[HCForoushandeType1Code], 
0 --[Sarjam]
FROM RefuseItemView 
INNER JOIN Refuse ON RefuseItemView.RefuseID = Refuse.ID
INNER JOIN Personnel ON Refuse.SellerID = Personnel.ID
INNER JOIN StoreServices ON RefuseItemView.StoreServiceID = StoreServices.ID
INNER JOIN StoreServiceDefView ON StoreServices.StoreServiceDefID = StoreServiceDefView.ID
";
        query += string.Format("WHERE WorkflowStatus = 1 AND {0} <= RefuseDate AND RefuseDate <= {1}", Helper.QoutedDateTime(start), Helper.QoutedDateTime(end));
        DataTable dt = DbProxy.Instance.FillData(query, "shemam");
        int done = 0;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string insert = string.Format(@"INSERT INTO Kharid_Detail ([KalaKhadamatName], [KalaCode], [BargashtType], [Price], [MaliatArzeshAfzoodeh], [AvarezArzeshAfzoodeh], [ForoushandePostCode], [ForoushandePerCityCode], [ForoushandeTell], [ForoushandeAddress], [ForoushandeName], [ForoushandeLastNameSherkatName], [ForoushandeEconomicNO], [ForoushandeNationalCode], [HCForoushandeTypeCode], [ForoushandeHozeh], [StateCode], [CityCode], [IsSent], [TempKey], [HCForoushandeType1Code], [Sarjam])
            VALUES({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18}, {19}, {20}, {21})"
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][0]))
                , Helper.QoutedString(FixCode(Helper.GetString(dt.Rows[i][1])), true) // KalaCode
                , 1 // BargashtType
                , Helper.GetDecimal(dt.Rows[i][3], 0) // Price
                , Helper.GetDecimal(dt.Rows[i][4], 0)
                , Helper.GetDecimal(dt.Rows[i][5], 0)
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][6]))
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][7]))
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][8])) // CellNumber1
                , Helper.QoutedString(TrimStr(Helper.GetString(dt.Rows[i][9]), 70))
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][10]))
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][11]))
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][12]))
                , Helper.QoutedString(Helper.GetString(dt.Rows[i][13])) // NatinoalCode
                , 1 // HCForoushandeTypeCode
                , "NULL" // ForoushandeHozeh
                , 0 //[StateCode], BAD
                , 0
                , "NULL"
                , "NULL"
                , 1 // HCForoushandeType1Code
                , 0);
            insert = insert.Replace("N'", "'");
            using (var cmd = con.CreateCommand())
            {
                cmd.CommandText = insert;
                cmd.CommandType = CommandType.Text;
                try
                {
                    done += cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    error.Text += insert + "<br />" + ex.Message + "<br />";
                }
            }
        }

        msg.Text += LangProvider.Instance["Refuse"] + " : " + string.Format(LangProvider.Instance["ExportedMsgFormat"], done, dt.Rows.Count) + "<br />";
        dt.Dispose();
    }
    protected void fRangeType_SelectedIndexChanged(object sender, EventArgs e)
    {
        int year = cal.GetYear(DateTime.Now);
        if (fRangeType.SelectedIndex == 0)
        {
            fFromDate.Text = string.Format("{0}/01/01", year);
            IDataReader financeYearStartDate = DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");//0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                fFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            fToDate.Text = Helper.FormatDate(DateTime.Now, null);
        }
        else if (fRangeType.SelectedIndex == 1)
        {
            fFromDate.Text = string.Format("{0}/01/01", year);
            fToDate.Text = string.Format("{0}/03/31", year);
        }
        else if (fRangeType.SelectedIndex == 2)
        {
            fFromDate.Text = string.Format("{0}/04/01", year);
            fToDate.Text = string.Format("{0}/06/31", year);
        }
        else if (fRangeType.SelectedIndex == 3)
        {
            fFromDate.Text = string.Format("{0}/07/01", year);
            fToDate.Text = string.Format("{0}/09/30", year);
        }
        else if (fRangeType.SelectedIndex == 4)
        {
            fFromDate.Text = string.Format("{0}/10/01", year);
            fToDate.Text = string.Format("{0}/12/{1}", year, cal.GetDaysInMonth(year, 12));
        }
    }
}