﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class vmap : System.Web.UI.Page
{
    bool centerSet = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");
        if (!IsPostBack)
        {
            mFromDate.Text = Helper.FormatDate(DateTime.Now, null);
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);
            PersonID = Helper.GetInt(Request.QueryString["ID"], SiteSettings.User.PersonnelID);

            // show persons only when the user is admin
            //panelPersons.Visible = false;
            //if (SiteSettings.User.IsAdmin)
            InitData();

            gMap.GoogleMapObject.APIKey = "AIzaSyC8oiObACdmi3uuAxkvp-OTtP_2_p3exNs";
            gMap.GoogleMapObject.APIVersion = "2";
            gMap.GoogleMapObject.Width = "1024px";
            gMap.GoogleMapObject.Height = "768px";
            gMap.GoogleMapObject.ZoomLevel = 14;

            gMap.GoogleMapObject.CenterPoint = new GooglePoint("CenterPoint", 35.311417, 46.995981);

            //Timer1_Tick(this, null);
            ShowPath();
        }

    }
    void InitData()
    {
        {
            panelPersons.Visible = SiteSettings.User.IsAdmin;
            IDataReader persons = DbProxy.Instance.ExecuteReader(@"DECLARE @VisitorGroupig int;
SET @VisitorGroupig = CONVERT(int, dbo.GetSetting('VisitorGrouping'), 0);
SELECT ID, [dbo].[DisplayPerson](ID, 1), ISNULL(Code, '') FROM Personnel 
WHERE Personnel.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID = @VisitorGroupig) -- Type=101");
            mPersons.Items.Clear();
            if (persons != null)
            {
                while (persons.Read())
                    mPersons.Items.Add(new ListItem((persons.IsDBNull(1) ? "" : persons.GetString(1)) + " - کد: " + (persons.IsDBNull(2) ? "" : persons.GetString(2)), persons.GetInt32(0).ToString()));
                persons.Close();
            }
            mPersons.SelectedValue = PersonID.ToString();// SiteSettings.User.PersonnelID.ToString();
        }

        {
            IDataReader Grouping = DbProxy.Instance.ExecuteReader("SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping  WHERE GroupingType = 0");
            mPersonsDummy.Items.Clear();
            if (Grouping != null)
            {
                while (Grouping.Read())
                    mPersonsDummy.Items.Add(new ListItem(Grouping.GetString(1), Grouping.GetInt32(0).ToString()));
                Grouping.Close();
            }
        }

        {
            IDataReader Grouping = DbProxy.Instance.ExecuteReader("SELECT ID, Title FROM PersonnelGroup");
            mCatsDummy.Items.Clear();
            if (Grouping != null)
            {
                while (Grouping.Read())
                    mCatsDummy.Items.Add(new ListItem(Grouping.GetString(1), Grouping.GetInt32(0).ToString()));
                Grouping.Close();
            }
        }
    }
    protected int PersonID
    {
        get
        {
            return Helper.GetInt(ViewState["PersonID"], Null.NullInteger);
        }
        set
        {
            ViewState["PersonID"] = value;
        }
    }
    protected void doShow_Click(object sender, EventArgs e)
    {
        gMap.GoogleMapObject.CenterPoint = new GooglePoint("CenterPoint", 35.311417, 46.995981);
        gMap.GoogleMapObject.Points.Clear();
        mLegend.Controls.Clear();

        PersonID = Helper.GetInt(mPersons.SelectedValue, PersonID);
        if (mShowActualPath.Checked)
            ShowPath();
        if (mShowPlanPath.Checked)
            ShowPlanPath();
        ShowPersonsLocation();
        gMap.GoogleMapObject.RecenterMap = true;
    }

    protected void ShowPath()
    {
        // PersonnelID, LocationDate, Latittude, Longitude
        DateTime start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        DateTime end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);
        DataTable dt = DbProxy.Instance.FillData(string.Format("SELECT * FROM VisitorLocation WHERE PersonnelID = {0} AND {1} <= LocationDate AND LocationDate <= {2} ORDER BY LocationDate"
            , PersonID, Helper.QoutedDateTime(start), Helper.QoutedDateTime(end)), "vis");

        DataRow last = DbProxy.Instance.FillRow("SELECT [dbo].[DisplayPerson](ID, 1) Title, LastLatittude, LastLongitude FROM Personnel WHERE ID =" + PersonID);
        string personName = Helper.GetString(last, "Title", "");

        if (dt != null)
        {
            GooglePolyline polygon = new GooglePolyline();
            polygon.ID = "PG1";
            //Give Hex code for line color
            //polygon.FillColor = "#0000FF";
            //polygon.FillOpacity = 0;
            //Stroke is outer border of polygon.
            polygon.ColorCode = "#0000FF";
            //polygon.StrokeOpacity = 1;
            polygon.Width = 2;
            //Add points to polygon
            int count = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow row = dt.Rows[i];
                string title = personName + "\r\n" + Helper.FormatDate(Helper.GetDateTime(row, "LocationDate", DateTime.Now), "yyyy-MM-dd HH:mm");
                int id = Helper.GetInt(row, "ID", -1);
                double lat = Helper.GetDouble(row, "Latittude", -1);
                double lon = Helper.GetDouble(row, "Longitude", -1);
                if (lat != -1 && lon != -1)
                {
                    //gMap.GoogleMapObject.Points.Add(new GooglePoint(id.ToString(), lat, lon, "img/map-pin-th.png", title, title, false));
                    GooglePoint point = new GooglePoint(id.ToString(), lat, lon, "", title, title, false);
                    
                    polygon.Points.Add(point);
                    gMap.GoogleMapObject.Points.Add(point);
                    if (!centerSet)
                    {
                        gMap.GoogleMapObject.CenterPoint = new GooglePoint("CenterPoint", lat, lon);
                        centerSet = true;
                    }
                }
            }
            if (polygon.Points.Count > 0)
                polygon.Points[0].IconImage = "img/mapstart.png";
            if (polygon.Points.Count > 1)
                polygon.Points[polygon.Points.Count - 1].IconImage = "img/mapstop.png";
            //mLegend.Controls.Add(new LiteralControl(string.Format("<img src='img/mapstart.png' />{0}", LangProvider.Instance["VisitorCustomersStatusVisiting"])));
            //mLegend.Controls.Add(new LiteralControl(string.Format("<img src='img/mapstop.png' />{0}", LangProvider.Instance["VisitorCustomersStatusVisiting"])));

            gMap.GoogleMapObject.Polylines.Add(polygon);
        }
    }
    protected void ShowPlanPath()
    {
        // PersonnelID, LocationDate, Latittude, Longitude
        DateTime start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        DateTime end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);
        DataTable dt = DbProxy.Instance.FillData(string.Format(@"SELECT Personnel.ID, [dbo].[DisplayPerson](Personnel.ID, 1) Title, Personnel.Latittude, Personnel.Longitude, VisitorCustomersStatus.Status FROM Personnel
            INNER JOIN VisitorCustomersStatus ON VisitorCustomersStatus.CustomerID = Personnel.ID
            INNER JOIN DailyCustomers ON DailyCustomers.ID = VisitorCustomersStatus.DailyCustomersID
            INNER JOIN VisitorPlanDays ON VisitorPlanDays.ID = DailyCustomers.VisitorPlanDaysID
            INNER JOIN VisitorPlan ON VisitorPlan.ID = VisitorPlanDays.VisitorPlanID
            WHERE VisitorPlan.VisitorID = {0} AND VisitorPlanDays.VisitDate > {1} AND VisitorPlanDays.VisitDate < {2}"
            , PersonID, Helper.QoutedDateTime(start), Helper.QoutedDateTime(end)), "vis");

        if (dt != null)
        {
            mLegend.Controls.Add(new LiteralControl(string.Format("<img src='img/map_visiting.png' />{0}", LangProvider.Instance["VisitorCustomersStatusVisiting"])));
            mLegend.Controls.Add(new LiteralControl(string.Format("<img src='img/map_visited.png' />{0}", LangProvider.Instance["VisitorCustomersStatusVisited"])));
            mLegend.Controls.Add(new LiteralControl(string.Format("<img src='img/map_novisit.png' />{0}", LangProvider.Instance["VisitorCustomersStatusNoVisit"])));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow row = dt.Rows[i];
                double lat = Helper.GetDouble(row, "Latittude", -1);
                double lon = Helper.GetDouble(row, "Longitude", -1);
                if (lat != -1 && lon != -1)
                {
                    int status = Helper.GetInt(row, "Status", 0); // 0:Visiting;1:Visited;2:NoVisit
                    string title = Helper.GetString(row, "Title", "") + "\r\n"
                        + (status == 0 ? LangProvider.Instance["VisitorCustomersStatusVisiting"] : status == 1
                        ? LangProvider.Instance["VisitorCustomersStatusVisited"] : LangProvider.Instance["VisitorCustomersStatusNoVisit"]);
                    int id = Helper.GetInt(row, "ID", -1);

                    //gMap.GoogleMapObject.Points.Add(new GooglePoint(id.ToString(), lat, lon, "img/map-pin-th.png", title, title, false));
                    string icon = status == 0 ? "img/map_visiting.png" : status == 1 ? "img/map_visited.png" : "img/map_novisit.png";
                    GooglePoint point = new GooglePoint("p" + id, lat, lon, icon, title, title, false);
                    gMap.GoogleMapObject.Points.Add(point);
                    if (centerSet)
                    {
                        gMap.GoogleMapObject.CenterPoint = new GooglePoint("CenterPoint", lat, lon);
                        centerSet = true;
                    }
                }
            }
        }
    }
    void ShowPersonsLocation()
    {
        string pgIDs, mgIDs, pgWhere;
        GetPersonsGroupWhere(out mgIDs, out pgIDs, out pgWhere);
        if (string.IsNullOrEmpty(mgIDs) && string.IsNullOrEmpty(pgIDs))
            return;

        string query = @"SELECT Personnel.ID, [dbo].[DisplayPerson](Personnel.ID, 1) PersonName, Personnel.Latittude, Personnel.Longitude, GroupingMember.GroupingID, dbo.[CodeToString]([Grouping].Code, [Grouping].GroupingType) GroupingTitle FROM Personnel
LEFT JOIN GroupingMember ON GroupingMember.OtherID = Personnel.ID
LEFT JOIN [Grouping] ON GroupingMember.GroupingID = [Grouping].ID
";
        string where = @"WHERE GroupingMember.GroupingID IN (SELECT ID FROM [Grouping] WHERE GroupingType = 0)
AND Personnel.Latittude IS NOT NULL AND Personnel.Longitude IS NOT NULL";
        #region bind grouping where
        if (!(string.IsNullOrEmpty(pgIDs) && string.IsNullOrEmpty(mgIDs)) && !string.IsNullOrEmpty(pgWhere))
        {
            // ------------------------
            // ids: 1,2,5
            // where: '&1%' AND ('&2%' OR '&5%')
            // it could be better, but i dont know regex well :(
            // ------------------------

            if (!string.IsNullOrEmpty(mgIDs))
            {
                string pgWhere2 = pgWhere;
                where += "\r\nAND (\r\n";
                // grouping members : '&1%', '!&1%'
                string[] ids = mgIDs.Split(',');
                for (int i = 0; i < ids.Length; i++)
                {
                    StringBuilder allIds = new StringBuilder();
                    int j = 0;
                    IDataReader childIds = DbProxy.Instance.ExecuteReader("SELECT ID FROM [GetAllGroupingChilds](" + ids[i] + ")");
                    while (childIds.Read())
                    {
                        if (j != 0)
                        {
                            allIds.Append(",");
                        }
                        allIds.Append(childIds.GetValue(0));
                        j++;
                    }
                    pgWhere2 = pgWhere2.Replace("'&" + ids[i] + "%'", " GroupingMember.GroupingID IN (" + allIds + ") \r\n");
                    pgWhere2 = pgWhere2.Replace("'!&" + ids[i] + "%'", " NOT GroupingMember.GroupingID IN (" + allIds + ") \r\n");
                }
                where += pgWhere2;
                where += "\r\n)\r\n";
            }

            where += "\r\nAND (\r\n";
            if (!string.IsNullOrEmpty(mgIDs))
            {
                // grouping members : '&1%', '!&1%'
                string[] ids = mgIDs.Split(',');
                for (int i = 0; i < ids.Length; i++)
                {
                    StringBuilder allIds = new StringBuilder();
                    int j = 0;
                    IDataReader childIds = DbProxy.Instance.ExecuteReader("SELECT ID FROM [GetAllGroupingChilds](" + ids[i] + ")");
                    while (childIds.Read())
                    {
                        if (j != 0)
                        {
                            allIds.Append(",");
                        }
                        allIds.Append(childIds.GetValue(0));
                        j++;
                    }
                    pgWhere = pgWhere.Replace("'&" + ids[i] + "%'", " Personnel.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + ")) \r\n");
                    pgWhere = pgWhere.Replace("'!&" + ids[i] + "%'", " NOT Personnel.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + ")) \r\n");
                    //where1 += "AND Personnel.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + "))";
                }
            }

            if (!string.IsNullOrEmpty(pgIDs))
            {
                // personnel groups: '^1%', '!^1%'
                string[] ids = pgIDs.Split(',');
                for (int i = 0; i < ids.Length; i++)
                {
                    pgWhere = pgWhere.Replace("'^" + ids[i] + "%'", " Personnel.ID IN (SELECT ID FROM Personnel WHERE PersonnelGroupID=" + ids[i] + ") \r\n");
                    pgWhere = pgWhere.Replace("'!^" + ids[i] + "%'", " NOT Personnel.ID IN (SELECT ID FROM Personnel WHERE PersonnelGroupID=" + ids[i] + ") \r\n");
                }
            }

            where += pgWhere;
            where += "\r\n)\r\n";
        }
        #endregion

        mLegend.Controls.Add(new LiteralControl(string.Format("<img src='img/mapmany.png' />{0}", LangProvider.Instance["Shared"])));
        DataTable dt = DbProxy.Instance.FillData(query + " " + where + " ORDER BY Personnel.ID", "locs");
        if (dt != null)
        {
            List<int> groupIDs = new List<int>();
            List<string> groupTitles = new List<string>();
            List<int> hasMany = new List<int>();
            List<int> personIDs = new List<int>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow row = dt.Rows[i];
                int pid = Helper.GetInt(row, "ID", -1);
                int gid = Helper.GetInt(row, "GroupingID", -1);
                string title = Helper.GetString(row, "GroupingTitle", "");
                //int pid = Helper.GetInt(row, "ID", -1);
                if (!groupIDs.Contains(gid))
                {
                    groupIDs.Add(gid);
                    groupTitles.Add(title);
                    mLegend.Controls.Add(new LiteralControl(string.Format("<img src='img/map{0}.png' />{1}", groupIDs.Count % 7, title)));
                }
                if (!personIDs.Contains(pid))
                    personIDs.Add(pid);
                else
                    hasMany.Add(pid);
            }
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow row = dt.Rows[i];
                string title = Helper.GetString(row, "PersonName", "");
                int id = Helper.GetInt(row, "ID", -1);
                int gid = Helper.GetInt(row, "GroupingID", -1);
                double lat = Helper.GetDouble(row, "Latittude", -1);
                double lon = Helper.GetDouble(row, "Longitude", -1);
                if (lat != -1 && lon != -1)
                {
                    string fileName;
                    if (hasMany.Contains(id))
                        fileName = "img/mapmany.png";
                    else
                    {
                        int index = groupIDs.IndexOf(gid);
                        fileName = "img/map" + (index % 7) + ".png"; // we have map0 to map6
                    }
                    //gMap.GoogleMapObject.Points.Add(new GooglePoint(id.ToString(), lat, lon, "img/map-pin-th.png", title, title, false));
                    gMap.GoogleMapObject.Points.Add(new GooglePoint("p" + id, lat, lon, fileName, title, title, false));
                    if (gMap.GoogleMapObject.Points.Count == 1)
                        gMap.GoogleMapObject.CenterPoint = new GooglePoint("CenterPoint", lat, lon);
                }
            }
        }
    }
    // ------------------------
    // ids: 1,2,5
    // where: '&1%' AND ('&2%' OR '&5%')
    // ------------------------
    private void GetPersonsGroupWhere(out string gmIDs, out string pgIDs, out string where)
    {
        gmIDs = pgIDs = "";
        where = "";

        // grouping members : '&1%', '!&1%'
        // category = 1375 AND ( category != 1076 OR category = 1075 ) 
        // --> '&1375%' AND ('!&1076%' OR '&1075%')
        where = mQueryBuilderSql.Value.Trim();
        where += " ";// we need this space at end to find IDs
        while (where.Contains("category = "))
        {
            int index = where.IndexOf("category = ") + "category = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            gmIDs += id + ",";
            where = where.Replace("category = " + id + " ", "'&" + id + "%' ");
        }
        while (where.Contains("category != "))
        {
            int index = where.IndexOf("category != ") + "category != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            gmIDs += id + ",";
            where = where.Replace("category != " + id + " ", "'!&" + id + "%' ");
        }
        if (gmIDs.EndsWith(","))
            gmIDs = gmIDs.Substring(0, gmIDs.Length - ",".Length);


        // personnel groups: '^1%', '!^1%'
        while (where.Contains("pcat = "))
        {
            int index = where.IndexOf("pcat = ") + "pcat = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pgIDs += id + ",";
            where = where.Replace("pcat = " + id + " ", "'^" + id + "%' ");
        }
        while (where.Contains("pcat != "))
        {
            int index = where.IndexOf("pcat != ") + "pcat != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pgIDs += id + ",";
            where = where.Replace("pcat != " + id + " ", "'!^" + id + "%' ");
        }
        if (pgIDs.EndsWith(","))
            pgIDs = pgIDs.Substring(0, pgIDs.Length - ",".Length);
    }
    protected void doExcel_Click(object sender, EventArgs e)
    {
        DateTime start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        DateTime end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);
        IDataReader dr = DbProxy.Instance.ExecuteReader(string.Format("SELECT * FROM VisitorLocation WHERE PersonnelID = {0} AND {1} <= LocationDate AND LocationDate <= {2}"
            , PersonID, Helper.QoutedDateTime(start), Helper.QoutedDateTime(end)));
        Exporter.ExportExcel(Response, dr);
        dr.Close();
    }
}