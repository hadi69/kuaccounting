﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="vmap.aspx.cs" Inherits="vmap" %>


<%@ Register Src="controls/GoogleMapForASPNet.ascx" TagName="GoogleMapForASPNet" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div id="bar">
    </div>
    <div class="well callout padding-5">
        <input type="hidden" runat="server" id="mQueryBuilder" class="mQueryBuilder" />
        <input type="hidden" runat="server" id="mQueryBuilderSql" class="mQueryBuilderSql" />
        <div class="hidden">
            <asp:DropDownList runat="server" ID="mPersonsDummy" CssClass="mPersonsDummy"></asp:DropDownList>
            <asp:DropDownList runat="server" ID="mCatsDummy" CssClass="mCatsDummy"></asp:DropDownList>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <h5 class="dashed-bottom padding-5-bottom yekan-family">
                    <asp:Label runat="server">Lang.PersonsGroupLable</asp:Label>
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="ltr builder">
                    <div id="builder"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="well padding-5">
        <div class="row">
            <asp:Panel ID="panelPersons" CssClass="col-xs-12 col-sm-3 form-group-container" runat="server">
                <div class="form-group">
                    <asp:Label ID="Label26" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Person</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mPersons" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </asp:Panel>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label1" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mFromDate" runat="server" CssClass="T2 dateSelect form-control input-sm"></asp:TextBox>
                        <cc1:maskededitextender id="mex" runat="server" targetcontrolid="mFromDate" mask="9999/99/99"
                            messagevalidatortip="true" onfocuscssclass="MaskedEditFocus" oninvalidcssclass="MaskedEditError"
                            masktype="None" clearmaskonlostfocus="false" displaymoney="None" acceptnegative="None" errortooltipenabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="mToDate" runat="server" CssClass="T2 dateSelect form-control input-sm"></asp:TextBox>
                        <cc1:maskededitextender id="mee1" runat="server" targetcontrolid="mToDate" mask="9999/99/99"
                            messagevalidatortip="true" onfocuscssclass="MaskedEditFocus" oninvalidcssclass="MaskedEditError"
                            masktype="None" clearmaskonlostfocus="false" displaymoney="None" acceptnegative="None" errortooltipenabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-2 form-group-container" >
                <div class="form-group">
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:CheckBox ID="mShowActualPath" runat="server" Text="Lang.ShowActualPath" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-2 form-group-container" >
                <div class="form-group">
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:CheckBox ID="mShowPlanPath" runat="server" Text="Lang.ShowPlanPath" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 form-group-container">
                <div class="form-group">
                    <div class="col-xs-12 form-col-without-padding">
                        <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-sm btn-primary padding-15-right padding-15-left doShow" OnClick="doShow_Click" />
                        <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-sm btn-primary padding-15-right padding-15-left" OnClick="doExcel_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <uc1:GoogleMapForASPNet ID="gMap" runat="server" ShowControls="False" />
    <asp:PlaceHolder runat="server" ID="mLegend" />
<script>
    $(function () {
        dateSelect('.dateSelect');
        $("#bar").prepend(breadcrumb({
            title: Lang.ShowPath
        }));
        setClassReports();

        var cats = {};
        $('.mPersonsDummy option').each(function () {
            cats[$(this).attr('value')] = $(this).text();
        });
        var pcats = {};
        $('.mCatsDummy option').each(function () {
            pcats[$(this).attr('value')] = $(this).text();
        });
        $jQuery1_9('#builder').queryBuilder({
            filters: [
                {
                    id: 'category',
                    label: Lang.PersonsGroupLable,
                    type: 'integer',
                    input: 'select',
                    values: cats,
                    operators: ['equal', 'not_equal']
                },
                {
                    id: 'pcat',
                    label: Lang.PersonnelGroup,
                    type: 'integer',
                    input: 'select',
                    values: pcats,
                    operators: ['equal', 'not_equal']
                }
            ]
        });

        // save rules before post back
        $('.doShow').click(function () {
            var rules = $jQuery1_9('#builder').queryBuilder('getRules');
            var rulesStr = JSON.stringify(rules);
            // alert(rulesStr);
            $('.mQueryBuilder').val(rulesStr);
            $('.mQueryBuilderSql').val($jQuery1_9('#builder').queryBuilder('getSQL', false, false).sql);
        });
        //reload saved rules
        $jQuery1_9('#builder').queryBuilder('reset');
        var ruleStr = $('.mQueryBuilder').val();
        if (ruleStr != '') {
            var rule = eval("(" + ruleStr + ")");
            try {
                $jQuery1_9('#builder').queryBuilder('setRules', rule);
            } catch (e) {
            }
        }
        $jQuery1_9('#builder').on('afterCreateRuleInput.queryBuilder', function (rule) {
            makeChosen();
        });
    });
    </script>
</asp:Content>

