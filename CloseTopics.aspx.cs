﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CloseTopics : System.Web.UI.Page
{
    DbProxy mDbProxy = new DbProxy();
    protected void Page_Load(object sender, EventArgs e)
    {
        mDbProxy.LastException = null;
        if (!IsPostBack)
        {
            Page.SetLang();
            AccController.UpdateDueOwes();
        }
    }
    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);
        try
        {
            if (mDbProxy != null)
                mDbProxy.Dispose();
        }
        catch { }
    }

    protected void doStart_Click(object sender, EventArgs e)
    {
        DataRow financeYear = mDbProxy.FillRow("SELECT * FROM FinanceYear WHERE Status=2");
        if (financeYear == null)
        {
            ShowMessage(LangProvider.Instance["NoCurYear"]);
            return;
        }

        // Check for program
        if (SiteSettings.ProgramID == 0)
        {
            ShowMessage(LangProvider.Instance["ChooseProgram"]);
            return;
        }
        string closedsPrograms = "," + Helper.GetString(financeYear, "ClosedPrograms", "") + ",";
        if (closedsPrograms.Contains("," + SiteSettings.ProgramID + ","))
        {
            ShowMessage(LangProvider.Instance["ClosedProgram"]);
            return;
        }

        // validate
        int curfinanceYearID = (int)financeYear["ID"];
        string profitTreePath = mProfitID.Value.Trim();
        string capitalTreePath = mCapitalID.Value.Trim();
        string closingBalanceTreePath = mClosingBalanceID.Value.Trim();
        string theTotalValueOfStock = TheTotalValueOfStock.Text;

        #region validate
        if (string.IsNullOrEmpty(profitTreePath) || string.IsNullOrEmpty(capitalTreePath))
        {
            ShowMessage(LangProvider.Instance["ChooseTopics"]);
            return;
        }
        #endregion

        try
        {
            mDbProxy.BeginTransaction();
            mDbProxy.RunSP("CloseTopics", new string[] { "FinanceYearID", "ProfitTreePath", "CapitalTreePath", "ProgramID", "TheTotalValueOfStock" }
                , new object[] { curfinanceYearID, profitTreePath, capitalTreePath, SiteSettings.ProgramID, theTotalValueOfStock }, 60000);
            ShowMessage(LangProvider.Instance["TempTopicsClosed"] + "<br />");
            mDbProxy.RunSP("MakeFinalVoucher", new string[] { "FinanceYearID", "ProgramID", "ClosingBalanceTreePath"}
                , new object[] { curfinanceYearID.ToString(), SiteSettings.ProgramID, closingBalanceTreePath }, 60000);
            ShowMessage(LangProvider.Instance["ClosingVoucherDone"] + "<br />");
            closedsPrograms = (closedsPrograms.Trim(',') + "," + SiteSettings.ProgramID).TrimStart(',');
            mDbProxy.RunQuery("UPDATE FinanceYear SET ClosedPrograms=" + Helper.QoutedString(closedsPrograms) + " WHERE ID = " + curfinanceYearID);

            // close the year if all programs are closed
            bool allClosed = false;
            closedsPrograms = "," + closedsPrograms + ",";
            if (closedsPrograms.Contains(",-1,")) // default program
            {
                allClosed = true;
                DataTable programs = DbProxy.Instance.FillData("SELECT ID FROM Program", "programs");
                if (programs != null && programs.Rows.Count > 0)
                    for (int i = 0; i < programs.Rows.Count; i++)
                        if (!closedsPrograms.Contains("," + programs.Rows[i][0].ToString() + ","))
                        {
                            allClosed = false;
                            break;
                        }
            }

            if (allClosed)
            {
                mDbProxy.RunQuery("UPDATE FinanceYear SET Status=3 WHERE ID = " + curfinanceYearID);
                ShowMessage(LangProvider.Instance["YearReadyToClose"] + "<br />");
            }
            else
                ShowMessage(LangProvider.Instance["ProgramClosed"] + "<br />");

            mDbProxy.CommitTransaction();
            SiteSettings.Invalidate();
        }
        catch (Exception ex)
        {
            ShowMessage(LangProvider.Instance["CloseError"] + " : <br />" + ex.Message);
            mDbProxy.RollbackTransaction();
        }
    }
    private void ShowMessage(string message, string type="danger")
    {
        if (type != "success" && type != "danger")
        {
            type = "danger";
        }

        msg.Text += message;
        msg.Visible = true;
        msg.CssClass += " alert-" + type;
    }
}