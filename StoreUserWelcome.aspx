﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Master.master" CodeFile="StoreUserWelcome.aspx.cs" Inherits="StoreUserWelcome" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div align="center">
        <JP:PageHeader ID="pageHeader" runat="server" Text="Lang.WelcomeMessage" />
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-6 col-lg-4 pill-container">
            <a href="list.aspx?table=userconfig">
                <div class="pill">
                    <div class="icon">
                        <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-config-32" />
                    </div>
                    <div class="text">
                        <asp:Label ID="Label1" runat="server" Text="Label">Lang.Management</asp:Label>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4 pill-container">
            <a href="list.aspx?table=Service&menu=storea">
                <div class="pill">
                    <div class="icon">
                        <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-product-32" />
                    </div>
                    <div class="text">
                        <asp:Label ID="Label11" runat="server" Text="Label">Lang.Commodity</asp:Label>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4 pill-container">
            <a href="storeuserserviceprices.aspx">
                <div class="pill">
                    <div class="icon">
                        <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-monetaryunit-32" />
                    </div>
                    <div class="text">
                        <asp:Label ID="Label6" runat="server" Text="Label">Lang.ServicePricing</asp:Label>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4 pill-container">
            <a href="list.aspx?table=Grouping&menu=salesa">
                <div class="pill">
                    <div class="icon">
                        <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-invoice-32" />
                    </div>
                    <div class="text">
                        <asp:Label ID="Label10" runat="server" Text="Label">Lang.Grouping</asp:Label>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4 pill-container">
            <a href="list.aspx?table=Basket">
                <div class="pill">
                    <div class="icon">
                        <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-storedraft-32" />
                    </div>
                    <div class="text">
                        <asp:Label ID="Label3" runat="server" Text="Label">Lang.BuyBags</asp:Label>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4 pill-container">
            <a href="list.aspx?table=DiscountCard">
                <div class="pill">
                    <div class="icon">
                        <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-storedraft-32" />
                    </div>
                    <div class="text">
                        <asp:Label ID="Label2" runat="server" Text="Label">Lang.DiscountCard</asp:Label>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4 pill-container">
            <a href="reports/app/persons.aspx">
                <div class="pill">
                    <div class="icon">
                        <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-person-32" />
                    </div>
                    <div class="text">
                        <asp:Label ID="Label4" runat="server" Text="Label">Lang.AppReportPersons</asp:Label>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4 pill-container">
            <a href="reports/app/services.aspx">
                <div class="pill">
                    <div class="icon">
                        <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-bill-32" />
                    </div>
                    <div class="text">
                        <asp:Label ID="Label5" runat="server" Text="Label">Lang.AppReportServices</asp:Label>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <script type="text/javascript">
    </script>
</asp:Content>

