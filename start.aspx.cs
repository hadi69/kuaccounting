﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class start : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.SetLang();
        mMsg.Value = "";
        if (System.Web.HttpContext.Current.Session["CurTab"] != null)
        {
            mCurTab.Value = System.Web.HttpContext.Current.Session["CurTab"].ToString();
        }
        else
        {
            mCurTab.Value = "";
        }
    }

    protected int getUserId()
    {
        int userId = Helper.GetInt(UserID.Text, 0);
        if (userId > 0)
        {
            return userId;
        }
        return -1;
    }

    protected void doSaveServices_Click(object sender, EventArgs e)
    {
        int num = SaveServices();
        mMsg.Value = string.Format(LangProvider.Instance["StartServicesSaved"], num);
        System.Web.HttpContext.Current.Session["CurTab"] = "";
        Response.Redirect(Request.Path);
    }
    protected void doApplyServices1_Click(object sender, EventArgs e)
    {
        int numSave = SaveServices();
        DbProxy proxy = new DbProxy();
        decimal sum = Helper.GetDecimal(proxy.FillScalar("SELECT SUM(ISNULL(TheLPBP, 0) * ISNULL(Income, 0)) FROM StartService WHERE Name IS NOT NULL AND Name <> '' AND ISNULL(RepeatedBarCode, 0) = 0 AND ISNULL(RepeatedTitle, 0) = 0 AND StartService.UserID =" + getUserId()), 0);

        int numAplay = ApplyServices();
        if (numAplay >= 0)
        {
            if (!mHaveOpeningServiceDoc.Checked)
            {
                if (numAplay > 0)
                doApplyServices2_Click(sum);
            }
            mMsg.Value = string.Format(LangProvider.Instance["StartServicesAdded"], numAplay , numSave - numAplay);
        }
        System.Web.HttpContext.Current.Session["CurTab"] = "";
        Response.Redirect(Request.Path);
    }
    protected void doApplyServices2_Click(decimal sum)
    {
       // int num = SaveServices();

        // has voucher : find sum 
        DbProxy proxy = new DbProxy();
        int fincanceYearID = Helper.GetInt(proxy.FillScalar("SELECT ID FROM FinanceYear WHERE Status=2"), -1);
        if (fincanceYearID == -1)
        {
            mMsg.Value = LangProvider.Instance["NoFinanceYear"];
            return;
        }
        int voucherID = Helper.GetInt(proxy.FillScalar(@"
DECLARE @VoucherID int;
SET @VoucherID = NULL;
SELECT TOP 1 @VoucherID = ID FROM DocumentAccounting WHERE Mode = 13
IF @VoucherID IS NULL
BEGIN
	INSERT INTO DocumentAccounting(FinanceYearID, [No], Mode, OtherID, Date, UserID, Status, DoComments, ProgramID) 
			VALUES (@FinanceYearID, dbo.[NeuVoucherNo] (NULL, @FinanceYearID, NULL, NULL), 13, NULL, GETDATE(), @UserID, 0, N'سند افتتاحیه', NULL)
	SET @VoucherID = SCOPE_IDENTITY()
END
SELECT @VoucherID;".Replace("@FinanceYearID", fincanceYearID.ToString()).Replace("@UserID", getUserId().ToString())), -1);
      //  num = ApplyServices();
        //if (num >= 0)
        //{
           // mMsg.Value = string.Format(LangProvider.Instance["StartServicesAdded"], num);

            // add voucher
            string script = @"DECLARE @OweTreePath NVARCHAR(250), @DueTreePath NVARCHAR(250);
        DECLARE @TotalOwe numeric(18, 2), @TotalDue numeric(18, 2);
	        SET @OweTreePath = dbo.GetSetting2('ServiceTreePath', NULL)
	        SET @DueTreePath = dbo.GetSetting2('WealthTreePath', NULL)	
        INSERT INTO DocumentAccountingItems(DocumentAccountingID, TreePath, Owe, Due)
	        VALUES({0}, @OweTreePath, {1}, 0);
        INSERT INTO DocumentAccountingItems(DocumentAccountingID, TreePath, Owe, Due)
	        VALUES({0}, @DueTreePath, 0, {1});
        -- Update Totals
        SELECT @TotalOwe = SUM(ISNULL(Owe, 0)), @TotalDue = SUM(ISNULL(Due, 0)) FROM DocumentAccountingItems WHERE DocumentAccountingID = {0}; 
        UPDATE DocumentAccounting SET TotalDue = @TotalDue, TotalOwe = @TotalOwe WHERE ID = {0};
        ";
            script = string.Format(script, voucherID, sum);
            proxy.RunQuery(script);
        // }

        System.Web.HttpContext.Current.Session["CurTab"] = "";
        Response.Redirect(Request.Path);
    }

    private int SaveServices()
    {
        string[] codes = Helper.GetString(Request["Code"], "").Split(',');
        string[] names = Helper.GetString(Request["Name"], "").Split(',');
        string[] serviceTypeIDs = Helper.GetString(Request["ServiceTypeID"], "").Split(',');
        string[] unitIDs = Helper.GetString(Request["UnitID"], "").Split(',');
        string[] storeIDs = Helper.GetString(Request["StoreID"], "").Split(',');
        string[] incomes = Helper.GetString(Request["Income"], "").Split(',');
        string[] theLPBPs = Helper.GetString(Request["TheLPBP"], "").Split(',');
        string[] theLPSPs = Helper.GetString(Request["TheLPSP"], "").Split(',');
        string[] expireDates = Helper.GetString(Request["ExpireDate"], "").Split(',');
        string[] pricings = Helper.GetString(Request["Pricing"], "").Split(',');
        DbProxy proxy = new DbProxy();
        proxy.RunQuery("UPDATE StartService SET Deleted = 1 WHERE StartService.UserID =" + getUserId());
        int num = 0;
        for (int i = 0; i < codes.Length; i++)
        {
            string code = codes[i];
            string name = names[Math.Min(i, names.Length - 1)];
            if (string.IsNullOrEmpty(name))
                continue;
            int serviceTypeID = Helper.GetInt(serviceTypeIDs[Math.Min(i, serviceTypeIDs.Length - 1)], -1);
            int unitID = Helper.GetInt(unitIDs[Math.Min(i, unitIDs.Length - 1)], -1);
            int storeID = Helper.GetInt(storeIDs[Math.Min(i, storeIDs.Length - 1)], -1);
            decimal income = Helper.GetDecimal(incomes[Math.Min(i, incomes.Length - 1)], 0);
            decimal theLPBP = Helper.GetDecimal(theLPBPs[Math.Min(i, theLPBPs.Length - 1)], 0);
            decimal theLPSP = Helper.GetDecimal(theLPSPs[Math.Min(i, theLPSPs.Length - 1)], 0);
            DateTime expireDate = Helper.ParseDate(expireDates[Math.Min(i, expireDates.Length - 1)], new DateTime(2111, 3, 21));
            int pricing = Helper.GetInt(pricings[Math.Min(i, pricings.Length - 1)], -1);
            if (unitID > 0 && storeID > 0 && serviceTypeID > 0)
            {
                string script = @"
INSERT INTO StartService (Code, Name, ServiceTypeID, UnitID, StoreID, Income, TheLPBP, TheLPSP, ExpireDate, Deleted, UserID, Pricing) 
    VALUES({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, 0, {9}, {10});SELECT SCOPE_IDENTITY()";
                script = string.Format(script, Helper.QoutedString(code), Helper.QoutedString(name), serviceTypeID, unitID, storeID, income
                    , theLPBP.ToString(Helper.NumberCulture), theLPSP.ToString(Helper.NumberCulture)
                    , Helper.QoutedDate(expireDate), getUserId(), pricing);
                int serviceID = Helper.GetInt(proxy.FillScalar(script), -1);
                if (serviceID > 0)
                    num++;
            }
        }
        proxy.RunQuery("DELETE FROM StartService WHERE Deleted = 1 AND UserID =" + getUserId());
        proxy.Dispose();
        return num;
    }

    private int ApplyServices()
    {
        DbProxy proxy = new DbProxy();
        int supplierID = Helper.GetInt(proxy.FillScalar(@"
DECLARE @SupplierID int;
SET @SupplierID = NULL;
SELECT TOP 1 @SupplierID = ID FROM Supplier WHERE IsDefault = 1
IF @SupplierID IS NULL
BEGIN
	INSERT INTO Supplier (CompanyName, IsDefault) VALUES(N'تک', 1)
	SELECT @SupplierID = SCOPE_IDENTITY();
END
SELECT @SupplierID;"), -1);
        int fincanceYearID = Helper.GetInt(proxy.FillScalar("SELECT ID FROM FinanceYear WHERE Status=2"), -1);
        if (fincanceYearID == -1)
        {
            mMsg.Value = LangProvider.Instance["NoFinanceYear"];
            return -1;
        }
        DataTable dt = proxy.FillData("SELECT * FROM StartService WHERE StartService.UserID =" + getUserId(), "m");
        int num = 0;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string barCode = Helper.GetString(dt.Rows[i], "Code", "");
            string name = Helper.GetString(dt.Rows[i], "Name", "");
            if (string.IsNullOrEmpty(name))
                continue;
            int serviceTypeID = Helper.GetInt(dt.Rows[i], "ServiceTypeID", -1);
            int unitID = Helper.GetInt(dt.Rows[i], "UnitID", -1);
            int storeID = Helper.GetInt(dt.Rows[i], "StoreID", -1);
            decimal income = Helper.GetDecimal(dt.Rows[i], "Income", 0);
            decimal theLPBP = Helper.GetDecimal(dt.Rows[i], "TheLPBP", 0);
            decimal theLPSP = Helper.GetDecimal(dt.Rows[i], "TheLPSP", 0);
            DateTime expireDate = Helper.GetDateTime(dt.Rows[i], "ExpireDate", Null.NullDate);
            int StartServiceID = Helper.GetInt(dt.Rows[i], "ID", -1);
            int pricing = Helper.GetInt(dt.Rows[i], "Pricing", -1);
            if (unitID > 0 && storeID > 0 && serviceTypeID > 0)
            {
                string script =
                    @"DECLARE @BarCode nvarchar(100), @Name nvarchar(500), @ServiceTypeID int, @UnitID int, @StoreID int, @Income int, @TheLPBP int, @TheLPSP int, @ExpireDate date;
DECLARE @ServiceID int, @StoreServiceDefID int, @StoreServiceID int, @ServicePriceID int, @SupplierID int, @FinanceYearID int, @Pricing int

SET @BarCode = {0};
SET @Name = {1};
SET @ServiceTypeID = {2};
SET @UnitID = {3};
SET @StoreID = {4};
SET @Income = {5};
SET @TheLPBP = {6};
SET @TheLPSP = {7};
SET @ExpireDate = {8};
SET @SupplierID = {9}
SET @FinanceYearID = {10}
SET @Pricing = {12}
DECLARE @NewCode NVARCHAR(300);
SET @NewCode = dbo.NeuServiceNo(NULL, @ServiceTypeID, NULL);
IF NOT EXISTS( SELECT ID FROM Service WHERE Title = @Name)
BEGIN
UPDATE StartService SET RepeatedTitle = 0 WHERE StartService.ID = {11};
IF NOT EXISTS( SELECT ID FROM Service WHERE BarCode = @BarCode)
 BEGIN
  INSERT INTO [Service] (Code, Title, BarCode, ServiceTypeID, UnitID, Enabled, Pricing) VALUES(@NewCode, @Name, CASE WHEN @BarCode = '' THEN @NewCode + @NewCode ELSE @BarCode END, CASE WHEN  @ServiceTypeID = -1 THEN NULL ELSE  @ServiceTypeID END, CASE WHEN  @UnitID = -1 THEN NULL ELSE  @UnitID END, 1, @Pricing)
  SELECT @ServiceID = SCOPE_IDENTITY();
  UPDATE StartService SET RepeatedBarCode = 0 WHERE StartService.ID = {11};
 END
 ELSE
 BEGIN
  UPDATE StartService SET RepeatedBarCode = 1 WHERE StartService.ID = {11};
 END
END
ELSE
BEGIN
 UPDATE StartService SET RepeatedTitle = 1 WHERE StartService.ID = {11};
END
IF(@ServiceID > 0)
INSERT INTO ServicePrice(ServiceID, Model, FreeBase, TheLPBP, TheLPSP) VALUES (@ServiceID, @SupplierID, @TheLPBP, @TheLPBP, @TheLPSP)
SELECT @ServicePriceID = SCOPE_IDENTITY();

IF(@ServicePriceID > 0)
INSERT INTO StoreServiceDef (StoreID, ServicePriceID, Income, Outcome) VALUES (CASE WHEN  @StoreID = -1 THEN NULL ELSE  @StoreID END, @ServicePriceID, @Income, 0)
SELECT @StoreServiceDefID = SCOPE_IDENTITY();

IF(@StoreServiceDefID > 0)	
INSERT INTO StoreServices (StoreServiceDefID, [ExpireDate], Income, Outcome) VALUES(@StoreServiceDefID, @ExpireDate, @Income, 0)
SELECT @StoreServiceID = SCOPE_IDENTITY();
	
IF(@StoreServiceID > 0)	
INSERT INTO Cardex (StoreServiceID, FinanceYearID, [Date], OtherDate, Income, Price, [ExpireDate], [Type])
	VALUES (@StoreServiceID, @FinanceYearID, GETDATE(), GETDATE(), @Income, @TheLPBP, @ExpireDate, 13)
SELECT @ServiceID;";
                script = string.Format(script, Helper.QoutedString(barCode), Helper.QoutedString(name), serviceTypeID,
                    unitID, storeID, income.ToString(Helper.NumberCulture), theLPBP.ToString(Helper.NumberCulture), theLPSP.ToString(Helper.NumberCulture)
                    , Helper.QoutedDate(expireDate), supplierID, fincanceYearID, StartServiceID, pricing);
                int serviceID = Helper.GetInt(proxy.FillScalar(script), -1);
                if (serviceID > 0)
                {
                    num++;
                }
            }
        }
        if (num > 0)
        {
            proxy.RunQuery("DELETE FROM StartService WHERE (ISNULL(RepeatedBarCode, 0) <> 1 AND ISNULL(RepeatedTitle, 0) <> 1) AND StartService.UserID =" + getUserId());
        }
        proxy.Dispose();
        return num;
    }
    protected void doSavePersons_Click(object sender, EventArgs e)
    {
        int num = SavePersons();
        mMsg.Value = string.Format(LangProvider.Instance["StartPersonsSaved"], num);
        System.Web.HttpContext.Current.Session["CurTab"] = "person";
        Response.Redirect(Request.Path);
    }
    protected void doApplyPersons1_Click(object sender, EventArgs e)
    {
        if (mHaveOpeningPersonDoc.Checked)
        {
            doApplyPersons2_Click(sender, e);
        }
        else
        {
            int numSave = SavePersons();
            int numAplay = ApplyPersons(true);
            if (numAplay >= 0)
                mMsg.Value = string.Format(LangProvider.Instance["StartPersonsAdded"], numAplay, numSave - numAplay);
            System.Web.HttpContext.Current.Session["CurTab"] = "person";
            Response.Redirect(Request.Path);
        }
    }
    protected void doApplyPersons2_Click(object sender, EventArgs e)
    {
        int numSave = SavePersons();
        int numAplay = ApplyPersons(false);
        if (numAplay >= 0)
            mMsg.Value = string.Format(LangProvider.Instance["StartPersonsAdded"], numAplay, numSave - numAplay);
        System.Web.HttpContext.Current.Session["CurTab"] = "person";
        Response.Redirect(Request.Path);
    }
    private int SavePersons()
    {
        string[] codes = Helper.GetString(Request["PCode"], "").Split(',');
        string[] names = Helper.GetString(Request["PName"], "").Split(',');
        string[] surnames = Helper.GetString(Request["PSurname"], "").Split(',');
        string[] owes = Helper.GetString(Request["POwe"], "").Split(',');
        string[] dues = Helper.GetString(Request["PDue"], "").Split(',');
        string[] personnelGroupIDs = Helper.GetString(Request["PersonnelGroupID"], "").Split(',');
        string[] homeTelNums = Helper.GetString(Request["PHomeTelNum"], "").Split(',');
        string[] homeAddresses = Helper.GetString(Request["PHomeAddress"], "").Split(',');
        DbProxy proxy = new DbProxy();
        proxy.RunQuery("UPDATE StartPerson SET Deleted = 1 WHERE StartPerson.UserID =" + getUserId());

        int num = 0;
        for (int i = 0; i < codes.Length; i++)
        {
            string code = codes[i];
            string name = names[Math.Min(i, names.Length - 1)];
            string surname = surnames[Math.Min(i, names.Length - 1)];
            if (string.IsNullOrEmpty(code) && string.IsNullOrEmpty(name) && string.IsNullOrEmpty(surname))
                continue;
            decimal owe = Helper.GetDecimal(owes[Math.Min(i, owes.Length - 1)], 0);
            decimal due = Helper.GetDecimal(dues[Math.Min(i, dues.Length - 1)], 0);
            int personnelGroupID = Helper.GetInt(personnelGroupIDs[Math.Min(i, personnelGroupIDs.Length - 1)], -1);
            string homeTelNum = homeTelNums[Math.Min(i, homeTelNums.Length - 1)];
            string homeAddress = homeAddresses[Math.Min(i, homeAddresses.Length - 1)];
            if (personnelGroupID > 0)
            {
                string script = @"INSERT INTO StartPerson (Code, Name, Surname, PersonnelGroupID, HomeTelNum, HomeAddress, Owe, Due, Deleted, UserID)
    VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, 0, {8});
SELECT SCOPE_IDENTITY();";
                script = string.Format(script, Helper.QoutedString(code), Helper.QoutedString(name), Helper.QoutedString(surname)
                    , personnelGroupID, Helper.QoutedString(homeTelNum), Helper.QoutedString(homeAddress), owe.ToString(Helper.NumberCulture), due.ToString(Helper.NumberCulture), getUserId());
                int personID = Helper.GetInt(proxy.FillScalar(script), -1);
                if (personID > 0)
                    num++;
            }
        }
        proxy.RunQuery("DELETE FROM StartPerson WHERE Deleted = 1 AND StartPerson.UserID =" + getUserId());
        proxy.Dispose();
        return num;
    }
    private int ApplyPersons(bool hasVoucher)
    {
        DbProxy proxy = new DbProxy();
        int voucherID = -1;
        if (true /*hasVoucher*/)
        {
            int fincanceYearID = Helper.GetInt(proxy.FillScalar("SELECT ID FROM FinanceYear WHERE Status=2"), -1);
            if (fincanceYearID == -1)
            {
                mMsg.Value = LangProvider.Instance["NoFinanceYear"];
                return -1;
            }
            voucherID = Helper.GetInt(proxy.FillScalar(@"
DECLARE @VoucherID int;
SET @VoucherID = NULL;
SELECT TOP 1 @VoucherID = ID FROM DocumentAccounting WHERE Mode = 13
IF @VoucherID IS NULL
BEGIN
	INSERT INTO DocumentAccounting(FinanceYearID, [No], Mode, OtherID, Date, UserID, Status, DoComments, ProgramID) 
			VALUES (@FinanceYearID, dbo.[NeuVoucherNo] (NULL, @FinanceYearID, NULL, NULL), 13, NULL, GETDATE(), @UserID, 0, N'سند افتتاحیه', NULL)
	SET @VoucherID = SCOPE_IDENTITY()
END
SELECT @VoucherID;".Replace("@FinanceYearID", fincanceYearID.ToString()).Replace("@UserID", getUserId().ToString())), -1);
        }
        int num = 0;
        DataTable dt = proxy.FillData("SELECT * FROM StartPerson WHERE StartPerson.UserID =" + getUserId(), "m");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string code = Helper.GetString(dt.Rows[i], "Code", "");
            string name = Helper.GetString(dt.Rows[i], "Name", "");
            string surname = Helper.GetString(dt.Rows[i], "Surname", "");
            if (string.IsNullOrEmpty(code) && string.IsNullOrEmpty(name) && string.IsNullOrEmpty(surname))
                continue;
            decimal owe = Helper.GetDecimal(dt.Rows[i], "Owe", 0);
            decimal due = Helper.GetDecimal(dt.Rows[i], "Due", 0);
            int personnelGroupID = Helper.GetInt(dt.Rows[i], "PersonnelGroupID", -1);
            string homeTelNum = Helper.GetString(dt.Rows[i], "HomeTelNum", "");
            string homeAddress = Helper.GetString(dt.Rows[i], "HomeAddress", "");
            bool additionalInformation = !string.IsNullOrEmpty(homeTelNum) || !string.IsNullOrEmpty(homeAddress);
            int StartPersonID = Helper.GetInt(dt.Rows[i], "ID", -1);
            if (personnelGroupID > 0)
            {
                string script = @"DECLARE @Code nvarchar(500), @Name nvarchar(500), @Surname nvarchar(500), @PersonnelGroupID int, @HomeTelNum nvarchar(500), @HomeAddress nvarchar(500)
	, @Owe numeric(18, 2), @Due numeric(18, 2), @PersonnelID INT, @OweTreePath NVARCHAR(250), @DueTreePath NVARCHAR(250), @HasVoucher BIT;
SET @Code = {0}
SET @Name = {1}
SET @Surname = {2}
SET @PersonnelGroupID = {3}
SET @HomeTelNum = {4}
SET @HomeAddress = {5}
SET @Owe = {6}
SET @Due = {7}
SET @HasVoucher = _HasVoucher
IF NOT EXISTS( SELECT ID FROM Personnel WHERE Code = @Code)
BEGIN
UPDATE StartPerson SET RepeatedCode = 0 WHERE StartPerson.ID = {9};
INSERT INTO Personnel (Code, Name, Surname, PersonnelGroupID, HomeTelNum, HomeAddress, Type, Enabled, AdditionalInformation)
	VALUES (dbo.NeuPersonnelCode(@Code), @Name, @Surname, CASE WHEN @PersonnelGroupID = -1 THEN NULL ELSE @PersonnelGroupID END, @HomeTelNum, @HomeAddress, 101, 1, _ADDITIONALINFORMATION);
SELECT @PersonnelID = SCOPE_IDENTITY();
END
ELSE
BEGIN
UPDATE StartPerson SET RepeatedCode = 1 WHERE StartPerson.ID = {9};
END
".Replace("_ADDITIONALINFORMATION", additionalInformation ? "1" : "0")
                                        .Replace("_HasVoucher", hasVoucher ? "1" : "0");
                    script += @"
IF @PersonnelID > 0
IF (@Owe > 0)
BEGIN
    SET @Due = 0;
	SET @OweTreePath = dbo.GetSetting2('OweTreePath', NULL);
    SET @DueTreePath = dbo.GetSetting2('WealthTreePath', NULL);
    INSERT INTO DocumentAccountingItems(DocumentAccountingID, TreePath, PersonnelID, Owe, Due)
	    VALUES({8}, @OweTreePath, @PersonnelID, @Owe + @Due, 0);
    IF @HasVoucher = 1
        INSERT INTO DocumentAccountingItems(DocumentAccountingID, TreePath, PersonnelID, Owe, Due)
	        VALUES({8}, @DueTreePath, NULL, 0, @Owe + @Due);
END
ELSE
BEGIN
    SET @Owe = 0;
	SET @DueTreePath = dbo.GetSetting2('DueTreePath', NULL)	;
    SET @OweTreePath = dbo.GetSetting2('WealthTreePath', NULL);
    IF @HasVoucher = 1
        INSERT INTO DocumentAccountingItems(DocumentAccountingID, TreePath, PersonnelID, Owe, Due)
	        VALUES({8}, @OweTreePath, NULL, @Owe + @Due, 0);
    INSERT INTO DocumentAccountingItems(DocumentAccountingID, TreePath, PersonnelID, Owe, Due)
	    VALUES({8}, @DueTreePath, @PersonnelID, 0, @Owe + @Due);
END
";
                script += " SELECT @PersonnelID";
                script = string.Format(script, Helper.QoutedString(code, true), Helper.QoutedString(name), Helper.QoutedString(surname)
                    , personnelGroupID, Helper.QoutedString(homeTelNum), Helper.QoutedString(homeAddress), owe.ToString(Helper.NumberCulture), due.ToString(Helper.NumberCulture)
                    , voucherID, StartPersonID);
                int personID = Helper.GetInt(proxy.FillScalar(script), -1);
                if (personID > 0)
                    num++;
            }
            if (num > 0)
                proxy.RunQuery(@"	-- Update Totals
	DECLARE @TotalOwe numeric(18, 2), @TotalDue numeric(18, 2);
	SELECT @TotalOwe = SUM(ISNULL(Owe, 0)), @TotalDue = SUM(ISNULL(Due, 0)) FROM DocumentAccountingItems WHERE DocumentAccountingID = @VoucherID; 
    UPDATE DocumentAccounting SET TotalDue = @TotalDue, TotalOwe = @TotalOwe WHERE ID = @VoucherID".Replace("@VoucherID", voucherID.ToString()));
        }

        if (num > 0)
            proxy.RunQuery("DELETE FROM StartPerson WHERE ISNULL(RepeatedCode, 0) <> 1 AND StartPerson.UserID =" + getUserId());
        proxy.Dispose();
        return num;
    }

    protected void doSaveTreePath_Click(object sender, EventArgs e)
    {
        string[] paths = Helper.GetString(Request["TPTreePath"], "").Split(',');
        string[] owes = Helper.GetString(Request["TPOwe"], "").Split(',');
        string[] dues = Helper.GetString(Request["TPDue"], "").Split(',');
        DbProxy proxy = new DbProxy();
        proxy.RunQuery("UPDATE StartTreePath SET Deleted = 1");

        int num = 0;
        for (int i = 0; i < paths.Length; i++)
        {
            if (string.IsNullOrEmpty(paths[i]))
                continue;
            string treePath = paths[i];
            decimal owe = Helper.GetDecimal(owes[Math.Min(i, owes.Length - 1)], 0);
            decimal due = Helper.GetDecimal(dues[Math.Min(i, dues.Length - 1)], 0);
            {
                string script = @"INSERT INTO StartTreePath (TreePath, Owe, Due, Deleted)
    VALUES ({0}, {1}, {2}, 0);
SELECT SCOPE_IDENTITY();";
                script = string.Format(script, Helper.QoutedString(treePath), owe.ToString(Helper.NumberCulture), due.ToString(Helper.NumberCulture));
                int id = Helper.GetInt(proxy.FillScalar(script), -1);
                if (id > 0)
                    num++;
            }
        }
        proxy.RunQuery("DELETE FROM StartTreePath WHERE Deleted = 1");
        proxy.Dispose();
        mMsg.Value = string.Format(LangProvider.Instance["StartTreePathSaved"], num);
        System.Web.HttpContext.Current.Session["CurTab"] = "treepath";
        Response.Redirect(Request.Path);
    }
    protected void doApplyTreePath_Click(object sender, EventArgs e)
    {
        DbProxy proxy = new DbProxy();
        int fincanceYearID = Helper.GetInt(proxy.FillScalar("SELECT ID FROM FinanceYear WHERE Status=2"), -1);
        if (fincanceYearID == -1)
        {
            mMsg.Value = LangProvider.Instance["NoFinanceYear"];
            return;
        }
        int voucherID = Helper.GetInt(proxy.FillScalar(@"
DECLARE @VoucherID int;
SET @VoucherID = NULL;
SELECT TOP 1 @VoucherID = ID FROM DocumentAccounting WHERE Mode = 13
IF @VoucherID IS NULL
BEGIN
	INSERT INTO DocumentAccounting(FinanceYearID, [No], Mode, OtherID, Date, UserID, Status, DoComments, ProgramID) 
			VALUES (@FinanceYearID, dbo.[NeuVoucherNo] (NULL, @FinanceYearID, NULL, NULL), 13, NULL, GETDATE(), @UserID, 0, N'سند افتتاحیه', NULL)
	SET @VoucherID = SCOPE_IDENTITY()
END
SELECT @VoucherID;".Replace("@FinanceYearID", fincanceYearID.ToString()).Replace("@UserID", getUserId().ToString())), -1);
        int num = 0;
        DataTable dt = proxy.FillData("SELECT * FROM StartTreePath", "m");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string treePath = Helper.GetString(dt.Rows[i], "TreePath", "");
            if (string.IsNullOrEmpty(treePath))
                continue;
            decimal owe = Helper.GetDecimal(dt.Rows[i], "Owe", 0);
            decimal due = Helper.GetDecimal(dt.Rows[i], "Due", 0);
            if (owe == 0 && due == 0)
                continue;
            string script = @"DECLARE @Owe numeric(18, 2), @Due numeric(18, 2), @TreePath NVARCHAR(250);
SET @TreePath = {0}
SET @Owe = {1}
SET @Due = {2}
IF (@Owe > 0)
BEGIN
    SET @Due = 0;
END
ELSE
BEGIN
    SET @Owe = 0;
END
INSERT INTO DocumentAccountingItems(DocumentAccountingID, TreePath, PersonnelID, Owe, Due)
	VALUES({3}, @TreePath, NULL, @Owe, @Due);
SELECT SCOPE_IDENTITY();";
            script = string.Format(script, Helper.QoutedString(treePath), owe.ToString(Helper.NumberCulture), due.ToString(Helper.NumberCulture), voucherID);
            int id = Helper.GetInt(proxy.FillScalar(script), -1);
            if (id > 0)
                num++;
        }
        if (num > 0)
            proxy.RunQuery(@"	-- Update Totals
	DECLARE @TotalOwe numeric(18, 2), @TotalDue numeric(18, 2);
	SELECT @TotalOwe = SUM(ISNULL(Owe, 0)), @TotalDue = SUM(ISNULL(Due, 0)) FROM DocumentAccountingItems WHERE DocumentAccountingID = @VoucherID; 
    UPDATE DocumentAccounting SET TotalDue = @TotalDue, TotalOwe = @TotalOwe WHERE ID = @VoucherID".Replace("@VoucherID", voucherID.ToString()));

        if (num > 0)
            proxy.RunQuery("DELETE FROM StartTreePath");
        proxy.Dispose();
        mMsg.Value = string.Format(LangProvider.Instance["StartTreePathAdded"], num);
        System.Web.HttpContext.Current.Session["treepath"] = "";
        Response.Redirect(Request.Path);
    }
    protected void doSaveChecks_Click(object sender, EventArgs e)
    {
        int num = SaveChecks();
        mMsg.Value = string.Format(LangProvider.Instance["StartChecksSaved"], num);
        mCurTab.Value = "check";
    }
    private int SaveChecks()
    {
        string[] Serials = Helper.GetString(Request["Serial"], "").Split(',');
        string[] PersonnelIDs = Helper.GetString(Request["PersonnelID"], "").Split(',');
        string[] BankAccountIDs = Helper.GetString(Request["BankAccountID"], "").Split(',');
        string[] Branchs = Helper.GetString(Request["Branch"], "").Split(',');
        string[] AccountNos = Helper.GetString(Request["AccountNo"], "").Split(',');
        string[] IssueDates = Helper.GetString(Request["IssueDate"], "").Split(',');
        string[] DueDates = Helper.GetString(Request["DueDate"], "").Split(',');
        string[] Amounts = Helper.GetString(Request["Amount"], "").Split(',');
        string[] Statuss = Helper.GetString(Request["Status"], "").Split(',');
        string[] ReceiverBankTreePaths = Helper.GetString(Request["ReceiverBankTreePath"], "").Split(',');
        string[] ReceiverPersonIDs = Helper.GetString(Request["ReceiverPersonID"], "").Split(',');
        string[] CheckOwnerNames = Helper.GetString(Request["CheckOwnerName"], "").Split(',');
        DbProxy proxy = new DbProxy();
        proxy.RunQuery("UPDATE StartChecks SET Deleted = 1 WHERE StartChecks.UserID =" + getUserId());
        int num = 0;
        for (int i = 0; i < Serials.Length; i++)
        {
            string serial = Serials[i];
            if (string.IsNullOrEmpty(serial))
                continue;
            int personnelID = Helper.GetInt(PersonnelIDs[Math.Min(i, PersonnelIDs.Length - 1)], -1);
            string checkOwnerName = CheckOwnerNames[i];
            int bankAccountID = Helper.GetInt(BankAccountIDs[Math.Min(i, BankAccountIDs.Length - 1)], -1);
            decimal amount = Helper.GetDecimal(Amounts[Math.Min(i, Amounts.Length - 1)], 0);
            int status = Helper.GetInt(Statuss[Math.Min(i, Statuss.Length - 1)], -1);
            int receiverPersonID = Helper.GetInt(ReceiverPersonIDs[Math.Min(i, ReceiverPersonIDs.Length - 1)], -1);
            DateTime issueDate = Helper.ParseDate(IssueDates[Math.Min(i, IssueDates.Length - 1)], new DateTime());
            DateTime dueDate = Helper.ParseDate(DueDates[Math.Min(i, DueDates.Length - 1)], new DateTime());
            string branch = Branchs[i];
            string accountNo = AccountNos[i];
            string receiverBankTreePath = ReceiverBankTreePaths[i];
            if (personnelID > 0 && bankAccountID > 0)
            {
                string script = @"
                INSERT INTO StartChecks (Serial, PersonnelID, CheckOwnerName, Branch, AccountNo, BankAccountID, IssueDate, DueDate, Amount, Status, ReceiverBankTreePath, ReceiverPersonID, Deleted, UserID) 
                VALUES({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, 0, {12});SELECT SCOPE_IDENTITY()";
                script = string.Format(script, Helper.QoutedString(serial), personnelID, Helper.QoutedString(checkOwnerName), Helper.QoutedString(branch), Helper.QoutedString(accountNo), bankAccountID,
                    Helper.QoutedDate(issueDate), Helper.QoutedDate(dueDate)
                    , amount.ToString(Helper.NumberCulture), status, Helper.QoutedString(receiverBankTreePath), receiverPersonID, getUserId());
                int checkID = Helper.GetInt(proxy.FillScalar(script), -1);
                if (checkID > 0)
                    num++;
            }
        }
        proxy.RunQuery("DELETE FROM StartChecks WHERE Deleted =  1 AND StartChecks.UserID =" + getUserId());
        proxy.Dispose();
        return num;
    }
    private int ApplyChecks(bool hasVoucher)
    {
        DbProxy proxy = new DbProxy();
        int voucherID = -1;
        int fincanceYearID = Helper.GetInt(proxy.FillScalar("SELECT ID FROM FinanceYear WHERE Status=2"), -1);
        if (hasVoucher)
        {
            if (fincanceYearID == -1)
            {
                mMsg.Value = LangProvider.Instance["NoFinanceYear"];
                return -1;
            }
            voucherID = Helper.GetInt(proxy.FillScalar(@"
                DECLARE @VoucherID int;
                SET @VoucherID = NULL;
                SELECT TOP 1 @VoucherID = ID FROM DocumentAccounting WHERE Mode = 13
                IF @VoucherID IS NULL
                BEGIN
	                INSERT INTO DocumentAccounting(FinanceYearID, [No], Mode, OtherID, Date, UserID, Status, DoComments, ProgramID) 
			                VALUES (@FinanceYearID, dbo.[NeuVoucherNo] (NULL, @FinanceYearID, NULL, NULL), 13, @UserID, GETDATE(), NULL, 0, N'سند افتتاحیه', NULL)
	                SET @VoucherID = SCOPE_IDENTITY()
                END
                SELECT @VoucherID;".Replace("@FinanceYearID", fincanceYearID.ToString()).Replace("@UserID", getUserId().ToString())), -1);
        }
        DataTable dt = proxy.FillData(@" ALTER TABLE Checks DISABLE TRIGGER ChecksInsert
                                         ALTER TABLE Checks DISABLE TRIGGER ChecksUpdate;  SELECT * FROM StartChecks WHERE StartChecks.UserID =" + getUserId(), "m");
        int num = 0;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string serial = Helper.GetString(dt.Rows[i], "Serial", "");
            if (string.IsNullOrEmpty(serial))
                continue;
            int personnelID = Helper.GetInt(dt.Rows[i], "PersonnelID", -1);
            string checkOwnerName = Helper.GetString(dt.Rows[i], "CheckOwnerName", "");
            int bankAccountID = Helper.GetInt(dt.Rows[i], "BankAccountID", -1);
            decimal amount = Helper.GetDecimal(dt.Rows[i], "Amount", 0);
            int status = Helper.GetInt(dt.Rows[i], "Status", -1);
            int receiverPersonID = Helper.GetInt(dt.Rows[i], "ReceiverPersonID", -1);
            DateTime issueDate = Helper.GetDateTime(dt.Rows[i], "IssueDate", Null.NullDate);
            DateTime dueDate = Helper.GetDateTime(dt.Rows[i], "DueDate", Null.NullDate);
            string branch = Helper.GetString(dt.Rows[i], "Branch", "");
            string accountNo = Helper.GetString(dt.Rows[i], "AccountNo", "");
            string receiverBankTreePath = Helper.GetString(dt.Rows[i], "ReceiverBankTreePath", "");

            if (personnelID > 0 && bankAccountID > 0)
            {
                string script = @"DECLARE @Serial nvarchar(100), @PersonnelID int, @CheckOwnerName nvarchar(400), @BankAccountID int
                              , @Amount numeric(18, 2), @Status int, @ReceiverPersonID int, @IssueDate date, @DueDate date, @CheckID int
                              , @Branch nvarchar(50), @AccountNo nvarchar(70), @ReceiverBankTreePath nvarchar(100), @OweTreePath nvarchar(100), @DueTreePath nvarchar(100), @Nature int;

                                SET @Serial = {0};
                                SET @PersonnelID = {1};
                                SET @CheckOwnerName = N'{2}';
                                SET @BankAccountID = {3};
                                SET @Amount = {4};
                                SET @Status = {5};
                                SET @ReceiverPersonID = {6};
                                SET @IssueDate = {7};
                                SET @DueDate = {8};
                                SET @Branch = {9}
                                SET @AccountNo = {10}
                                SET @ReceiverBankTreePath = {11}

                                INSERT INTO Checks (No, Serial, PersonnelID, CheckOwnerName, BankAccountID, Amount, Status, ReceiverPersonID
                                                   , IssueDate, DueDate, Branch, AccountNo, ReceiverBankTreePath, FinanceYearID, IsShop)
                                                   VALUES([dbo].[NeuCheckNo](NULL , {13}, NULL), @Serial, CASE WHEN @PersonnelID = -1 THEN NULL ELSE @PersonnelID END
                                                   , @CheckOwnerName, CASE WHEN @BankAccountID = -1 THEN NULL ELSE @BankAccountID END, @Amount, @Status, CASE WHEN @ReceiverPersonID = -1 THEN NULL ELSE @ReceiverPersonID END, @IssueDate, @DueDate
                                                   , @Branch, @AccountNo, @ReceiverBankTreePath, {13}, 1); SELECT @CheckID = SCOPE_IDENTITY();
                                ";
                if (hasVoucher)
                    script += @"
                            IF (@Amount > 0)
                            BEGIN 
                                 SET @OweTreePath =  dbo.GetSetting2('CheckTreePath', NULL)                           
                                -- Nature: 0=Owe;1=Due;2=Both
                                SELECT @Nature = Nature FROM Personnel WHERE ID = @PersonnelID
                                IF @Nature = 0 OR @Nature = 2
                                SET @DueTreePath = dbo.GetSetting2('OweTreePath', NULL)
                                ELSE
                                SET @DueTreePath = dbo.GetSetting2('DueTreePath', NULL)

                                INSERT INTO DocumentAccountingItems(DocumentAccountingID, TreePath, PersonnelID, Owe, Due, CheckID, Types)
	                             VALUES({12}, @OweTreePath, NULL, @Amount, 0, @CheckID, , @Status);
                                INSERT INTO DocumentAccountingItems(DocumentAccountingID, TreePath, PersonnelID, Owe, Due, CheckID, Types)
	                             VALUES({12}, @DueTreePath, CASE WHEN @PersonnelID = -1 THEN NULL ELSE @PersonnelID END, 0, @Amount, @CheckID, @Status)
                             END;";
                script = string.Format(script, Helper.QoutedString(serial), personnelID, checkOwnerName, bankAccountID
                    , amount.ToString(Helper.NumberCulture), status, receiverPersonID, Helper.QoutedDate(issueDate), Helper.QoutedDate(dueDate)
                    , Helper.QoutedString(branch), Helper.QoutedString(accountNo), Helper.QoutedString(receiverBankTreePath), voucherID, fincanceYearID);
                int checkID = Helper.GetInt(proxy.FillScalar(script), -1);
                if (checkID > 0)
                    num++;
            }
        }
        if (hasVoucher && num > 0)
            proxy.RunQuery(@"	-- Update Totals
	                        DECLARE @TotalOwe numeric(18, 2), @TotalDue numeric(18, 2);
	                        SELECT @TotalOwe = SUM(ISNULL(Owe, 0)), @TotalDue = SUM(ISNULL(Due, 0)) FROM DocumentAccountingItems WHERE DocumentAccountingID = @VoucherID; 
                            UPDATE DocumentAccounting SET TotalDue = @TotalDue, TotalOwe = @TotalOwe WHERE ID = @VoucherID".Replace("@VoucherID", voucherID.ToString()));
        if (num > 0)
            proxy.RunQuery(@" ALTER TABLE Checks ENABLE TRIGGER ChecksInsert
                               ALTER TABLE Checks ENABLE TRIGGER ChecksUpdate; DELETE FROM StartChecks WHERE StartChecks.UserID =" + getUserId());
        proxy.Dispose();
        return num;
    }
    protected void doSavePayChecks_Click(object sender, EventArgs e)
    {
        int num = SavePayChecks();
        mMsg.Value = string.Format(LangProvider.Instance["StartPayChecksAdded"], num);
        mCurTab.Value = "paycheck";
    }
    private int SavePayChecks()
    {
        string[] BankAccountCheckIDs = Helper.GetString(Request["BankAccountCheckID"], "").Split(',');
        string[] Serials = Helper.GetString(Request["PSerial"], "").Split(',');
        string[] BankTreePaths = Helper.GetString(Request["BankTreePath"], "").Split(',');
        string[] ReceiverIDs = Helper.GetString(Request["ReceiverID"], "").Split(',');
        string[] PayDates = Helper.GetString(Request["PPayDate"], "").Split(',');
        string[] DueDates = Helper.GetString(Request["PDueDate"], "").Split(',');
        string[] Amounts = Helper.GetString(Request["PAmount"], "").Split(',');
        string[] Statuss = Helper.GetString(Request["PStatus"], "").Split(',');

        DbProxy proxy = new DbProxy();
        proxy.RunQuery("UPDATE StartPayChecks SET Deleted = 1 WHERE StartPayChecks.UserID =" + getUserId());
        int num = 0;
        for (int i = 0; i < Serials.Length; i++)
        {
            string serial = Serials[i];
            if (string.IsNullOrEmpty(serial))
                continue;
            int bankAccountCheckID = Helper.GetInt(BankAccountCheckIDs[Math.Min(i, BankAccountCheckIDs.Length - 1)], -1);
            string bankTreePath = BankTreePaths[i];
            int receiverID = Helper.GetInt(ReceiverIDs[Math.Min(i, ReceiverIDs.Length - 1)], -1);
            DateTime payDate = Helper.ParseDate(PayDates[Math.Min(i, PayDates.Length - 1)], new DateTime());
            DateTime dueDate = Helper.ParseDate(DueDates[Math.Min(i, DueDates.Length - 1)], new DateTime());
            decimal amount = Helper.GetDecimal(Amounts[Math.Min(i, Amounts.Length - 1)], 0);
            int status = Helper.GetInt(Statuss[Math.Min(i, Statuss.Length - 1)], -1);

            if (bankAccountCheckID > 0 && receiverID > 0)
            {
                string script = @"
                INSERT INTO StartPayChecks (PSerial, bankAccountCheckID, bankTreePath, receiverID, PPayDate, PDueDate, PAmount, PStatus, Deleted, UserID) 
                VALUES({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, 0, {8});SELECT SCOPE_IDENTITY()";
                script = string.Format(script, Helper.QoutedString(serial), bankAccountCheckID, Helper.QoutedString(bankTreePath), receiverID,
                    Helper.QoutedDate(payDate), Helper.QoutedDate(dueDate)
                    , amount.ToString(Helper.NumberCulture), status, getUserId());
                int paycheckID = Helper.GetInt(proxy.FillScalar(script), -1);
                if (paycheckID > 0)
                    num++;
            }
        }
        proxy.RunQuery("DELETE FROM StartPayChecks WHERE Deleted = 1 AND StartPayChecks.UserID =" + getUserId());
        proxy.Dispose();
        return num;
    }
    private int ApplyPayChecks(bool hasVoucher)
    {
        DbProxy proxy = new DbProxy();

        int voucherID = -1;
        int fincanceYearID = Helper.GetInt(proxy.FillScalar("SELECT ID FROM FinanceYear WHERE Status=2"), -1);
        if (hasVoucher)
        {
            if (fincanceYearID == -1)
            {
                mMsg.Value = LangProvider.Instance["NoFinanceYear"];
                return -1;
            }
            voucherID = Helper.GetInt(proxy.FillScalar(@"
                DECLARE @VoucherID int;
                SET @VoucherID = NULL;
                SELECT TOP 1 @VoucherID = ID FROM DocumentAccounting WHERE Mode = 13
                IF @VoucherID IS NULL
                BEGIN
	                INSERT INTO DocumentAccounting(FinanceYearID, [No], Mode, OtherID, Date, UserID, Status, DoComments, ProgramID) 
			                VALUES (@FinanceYearID, dbo.[NeuVoucherNo] (NULL, @FinanceYearID, NULL, NULL), 13, NULL, GETDATE(), @UserID, 0, N'سند افتتاحیه', NULL)
	                SET @VoucherID = SCOPE_IDENTITY()
                END
                SELECT @VoucherID;".Replace("@FinanceYearID", fincanceYearID.ToString()).Replace("@UserID", getUserId().ToString())), -1);
        }
        DataTable dt = proxy.FillData(@" ALTER TABLE PayCheck DISABLE TRIGGER PayCheckInsertUpdate;
                                         SELECT * FROM StartPayChecks WHERE StartPayChecks.UserID =" + getUserId(), "m");
        int num = 0;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string serial = Helper.GetString(dt.Rows[i], "PSerial", "");
            if (string.IsNullOrEmpty(serial))
                continue;
            int bankAccountCheckID = Helper.GetInt(dt.Rows[i], "BankAccountCheckID", -1);
            string bankTreePath = Helper.GetString(dt.Rows[i], "BankTreePath", "");
            int receiverID = Helper.GetInt(dt.Rows[i], "ReceiverID", -1);
            DateTime payDate = Helper.GetDateTime(dt.Rows[i], "PPayDate", Null.NullDate);
            DateTime dueDate = Helper.GetDateTime(dt.Rows[i], "PDueDate", Null.NullDate);
            decimal amount = Helper.GetDecimal(dt.Rows[i], "PAmount", 0);
            int status = Helper.GetInt(dt.Rows[i], "PStatus", -1);


            if (bankAccountCheckID > 0 && receiverID > 0)
            {
                string script = @"DECLARE @Serial nvarchar(100), @BankAccountCheckID int, @BankTreePath nvarchar(100), @ReceiverID int, @PayDate date, @DueDate date
                                , @Amount numeric(18, 2), @Status int, @OweTreePath nvarchar(100), @DueTreePath nvarchar(100), @Nature int, @PayCheckID int;

                                SET @Serial = {0};
                                SET @BankAccountCheckID = {1};
                                SET @BankTreePath = {2};
                                SET @ReceiverID = {3}
                                SET @PayDate = {4};
                                SET @DueDate = {5};
                                SET @Amount = {6};
                                SET @Status = {7};


                                INSERT INTO PayCheck (No, Serial, BankAccountCheckID, BankTreePath, ReceiverID, PayDate, DueDate, Amount
                                                   , Status, FinanceYearID, IsShop)
                                                    VALUES([dbo].[NeuPaycheckNo](NULL, {9}, NULL ), @Serial, CASE WHEN  @BankAccountCheckID = -1 THEN NULL ELSE  @BankAccountCheckID END, @BankTreePath, CASE WHEN  @ReceiverID = -1 THEN NULL ELSE @ReceiverID END
                                                   , @PayDate, @DueDate
                                                   , @Amount, @Status, {9}, 1); SELECT @PayCheckID = SCOPE_IDENTITY();
                                ";
                if (hasVoucher)
                    script += @"
                            IF (@Amount > 0)
                            BEGIN  
                                 SET @DueTreePath =  dbo.GetSetting2('PayCheckTreePath', NULL)                           
                                -- Nature: 0=Owe;1=Due;2=Both
                                SELECT @Nature = Nature FROM Personnel WHERE ID = @ReceiverID
                                IF @Nature = 0 OR @Nature = 2
                                SET @OweTreePath = dbo.GetSetting2('OweTreePath', NULL)
                                ELSE
                                SET @OweTreePath = dbo.GetSetting2('DueTreePath', NULL)

                                INSERT INTO DocumentAccountingItems(DocumentAccountingID, TreePath, PersonnelID, Owe, Due, PayCheckID, [Types])
	                             VALUES({8}, @OweTreePath, CASE WHEN  @ReceiverID = -1 THEN NULL ELSE @ReceiverID END, @Amount, 0, @PayCheckID, @Status);
                                INSERT INTO DocumentAccountingItems(DocumentAccountingID, TreePath, PersonnelID, Owe, Due, PayCheckID, [Types])
	                             VALUES({8}, @DueTreePath, NULL, 0, @Amount, @PayCheckID, @Status)
                             END;";
                script = string.Format(script, Helper.QoutedString(serial), bankAccountCheckID, Helper.QoutedString(bankTreePath), receiverID, Helper.QoutedDate(payDate)
                    , Helper.QoutedDate(dueDate), amount.ToString(Helper.NumberCulture), status, voucherID, fincanceYearID);
                int paycheckID = Helper.GetInt(proxy.FillScalar(script), -1);
                if (paycheckID > 0)
                    num++;
            }
        }
        if (hasVoucher && num > 0)
            proxy.RunQuery(@"	-- Update Totals
	                        DECLARE @TotalOwe numeric(18, 2), @TotalDue numeric(18, 2);
	                        SELECT @TotalOwe = SUM(ISNULL(Owe, 0)), @TotalDue = SUM(ISNULL(Due, 0)) FROM DocumentAccountingItems WHERE DocumentAccountingID = @VoucherID; 
                            UPDATE DocumentAccounting SET TotalDue = @TotalDue, TotalOwe = @TotalOwe WHERE ID = @VoucherID".Replace("@VoucherID", voucherID.ToString()));

        if (num > 0)
            proxy.RunQuery(@" ALTER TABLE PayCheck ENABLE TRIGGER PayCheckInsertUpdate;
                                          DELETE FROM StartPayChecks WHERE StartPayChecks.UserID =" + getUserId());
        proxy.Dispose();
        return num;
    }
    protected void doApplyChecks1_Click(object sender, EventArgs e)
    {
        if (mHaveOpeningCheckDoc.Checked)
        {
            doApplyChecks2_Click(sender, e);
        }
        else
        {
            int numSaved = SaveChecks();
            int numApplyed = ApplyChecks(true);
            if (numApplyed >= 0)
                mMsg.Value = string.Format(LangProvider.Instance["StartChecksSaved"], numApplyed, numSaved - numApplyed);
            System.Web.HttpContext.Current.Session["check"] = "";
            Response.Redirect(Request.Path);
        }
    }
    protected void doApplyChecks2_Click(object sender, EventArgs e)
    {
        int numSaved = SaveChecks();
        int numApplyed = ApplyChecks(false);
        if (numApplyed >= 0)
            mMsg.Value = string.Format(LangProvider.Instance["StartChecksSaved"], numApplyed, numSaved - numApplyed);
        System.Web.HttpContext.Current.Session["check"] = "";
        Response.Redirect(Request.Path);
    }
    protected void doApplyPayChecks1_Click(object sender, EventArgs e)
    {
        if (mHaveOpeningPayCheckDoc.Checked)
        {
            doApplyPayChecks2_Click(sender, e);
        }
        else
        {
            int numSaved = SavePayChecks();
            int numApplyed = ApplyPayChecks(true);
            if (numApplyed >= 0)
                mMsg.Value = string.Format(LangProvider.Instance["StartPayChecksSaved"], numApplyed, numSaved - numApplyed);
            System.Web.HttpContext.Current.Session["paycheck"] = "";
            Response.Redirect(Request.Path);
        }
    }
    protected void doApplyPayChecks2_Click(object sender, EventArgs e)
    {
        int numSaved = SavePayChecks();
        int numApplyed = ApplyPayChecks(false);
        if (numApplyed >= 0)
            mMsg.Value = string.Format(LangProvider.Instance["StartPayChecksSaved"], numApplyed, numSaved - numApplyed);
        System.Web.HttpContext.Current.Session["paycheck"] = "";
        Response.Redirect(Request.Path);
    }
}