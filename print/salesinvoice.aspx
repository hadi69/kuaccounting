﻿<%@ Page Title="" Language="C#" MasterPageFile="~/print/PrintMaster.master" AutoEventWireup="true" CodeFile="salesinvoice.aspx.cs" Inherits="print_salesinvoice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 743px;
        }

        .auto-style2 {
            width: 88px;
        }

        .auto-style3 {
            width: 75px;
        }

        .auto-style4 {
            width: 111px;
        }

        .auto-style5 {
            width: 27px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CP1" runat="Server">
    <table style="width: 1000px" cellpadding="0" cellspacing="0" align='center' border="0">
        <tr>
            <td>
                <table width="100%" cellpadding='0' cellspacing='0'>
                    <tr>
                        <td class="auto-style1 tc"><asp:Label ID="Label3" runat="server"><%= "InvoicedSalesOfCommoditiesAndServices".Translate() %></asp:Label></td>
                        <td class="tl">
                            <table>
                                <tr>
                                    <td><asp:Label ID="Label1" runat="server"><%= "SerialNo".Translate() %></asp:Label></td>
                                    <td>
                                        <asp:Label ID="No" runat="server" Text="No"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td><asp:Label ID="Label2" runat="server"><%= "Date".Translate() %></asp:Label></td>
                                    <td>
                                        <asp:Label ID="SaleDate" runat="server" Text="SaleDate"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0" cellpadding='0' cellspacing='0' class="border">
                    <tr>
                        <td colspan="8" class="tc borderbottom"><asp:Label ID="Label4" runat="server"><%= "SellerInformation".Translate() %></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="auto-style4"><asp:Label ID="Label5" runat="server"><%= "NaturalOrLegalPerson".Translate() %></asp:Label></td>
                        <td colspan="3">
                            <asp:Label ID="OrgName" runat="server" Text="OrgName"></asp:Label>
                        </td>
                        <td class="auto-style3"><asp:Label ID="Label12" runat="server"><%= "EconomicNo".Translate() %></asp:Label></td>
                        <td>
                            <asp:Label ID="OrgCode" runat="server" Text="OrgCode"></asp:Label>
                        </td>
                        <td class="auto-style2"><asp:Label ID="Label13" runat="server"><%= "NationalCode".Translate() %></asp:Label></td>
                        <td>
                            <asp:Label ID="OrgNo" runat="server" Text="OrgNo"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style4"><asp:Label ID="Label14" runat="server"><%= "CompleteAdress".Translate() %></asp:Label></td>
                        <td>
                            <label runat="server" id="No6"></label>
                            <asp:Label ID="Label9" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td class="auto-style5"><asp:Label ID="Label15" runat="server"><%= "Town".Translate() %></asp:Label></td>
                        <td>
                            <label runat="server" id="No7"></label>
                            <asp:Label ID="Label10" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td class="auto-style3"><asp:Label ID="Label16" runat="server"><%= "ZipCode".Translate() %></asp:Label></td>
                        <td>
                            <label runat="server" id="No5"></label>
                            <asp:Label ID="Label8" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td class="auto-style2"><asp:Label ID="Label17" runat="server"><%= "City".Translate() %></asp:Label></td>
                        <td>
                            <label runat="server" id="No3"></label>
                            <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style4"><asp:Label ID="Label19" runat="server"><%= "Adress".Translate() %></asp:Label></td>
                        <td colspan="5">
                            <label runat="server" id="No8"></label>
                            <asp:Label ID="Label11" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td class="auto-style2"><asp:Label ID="Label21" runat="server"><%= "TelephoneNumber".Translate() %></asp:Label></td>
                        <td>
                            <label runat="server" id="No4"></label>
                            <asp:Label ID="Label7" runat="server" Text="Label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8" class="tc borderbottom bordertop"><%= "BuyerInfo".Translate() %></td>
                    </tr>
                    <tr>
                        <td class="auto-style4"><%= "PersonOrLegalPersonName".Translate() %>:</td>
                        <td colspan="3">
                            <asp:Label ID="BuyerName" runat="server" Text="BuyerName"></asp:Label>
                        </td>
                        <td class="auto-style3"><%= "EconomicalNumber".Translate() %>:</td>
                        <td>
                            <asp:Label ID="BuyerCode" runat="server" Text="BuyerCode"></asp:Label>
                        </td>
                        <td class="auto-style2"><%= "BuyerNC".Translate() %>:</td>
                        <td>
                            <asp:Label ID="BuyerNC" runat="server" Text="BuyerNC"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style4"><%= "FullAddressProvicne".Translate() %>:</td>
                        <td>
                            <asp:Label ID="Label18" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td class="auto-style5"><%= "Town".Translate() %>:</td>
                        <td>
                            <asp:Label ID="Label20" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td class="auto-style3"><%= "TenDigitPostalCode".Translate() %>:</td>
                        <td>
                            <asp:Label ID="Label22" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td class="auto-style2"><%= "City".Translate() %>:</td>
                        <td>
                            <asp:Label ID="Label24" runat="server" Text="Label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style4"><%= "Address2".Translate() %>:</td>
                        <td colspan="5">
                            <asp:Label ID="BuyerHomeAddress" runat="server" Text="BuyerHomeAddress"></asp:Label>
                        </td>
                        <td class="auto-style2"><%= "FaxTelNumber".Translate() %>:</td>
                        <td>
                            <asp:Label ID="BuyerHomeTelNum" runat="server" Text="BuyerHomeTelNum"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr><td><asp:PlaceHolder runat="server" ID="mPlaceHolder"></asp:PlaceHolder></td></tr>
    </table>
    
</asp:Content>

