﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class print_salesinvoice : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            BindData();
        }
        catch (Exception ex)
        {
            mPlaceHolder.Controls.Add(new LiteralControl("خطا: " + ex.Message));
        }
    }
    private string GetHeader()
    {
        StringBuilder b = new StringBuilder();
        b.Append("<tr>");
        b.Append("<td>" + "ColRowNr".Translate() + "</td>");
        b.Append("<td>" + "GoodCode".Translate() + "</td>");
        b.Append("<td>" + "GoodDescription".Translate() + "</td>");
        b.Append("<td>" + "NumOrValue".Translate() + "</td>");
        b.Append("<td>" + "MeasurementValue".Translate() + "</td>");
        b.Append("<td>" + "UnitPrice".Translate() + "</td>");
        b.Append("<td>" + "TotalPrice2".Translate() + "</td>");
        b.Append("<td>" + "DiscountAmount".Translate() + "</td>");
        b.Append("<td>" + "TotalPriceAfterDiscount".Translate() + "(</td>");
        b.Append("<td>" + "TotalTaxAndVAT".Translate() + "</td>");
        b.Append("<td>" + "TotalSumPlusTaxAndVAT".Translate() + "</td>");
        b.Append("</tr>");
        return b.ToString();
    }
    int rowsPerPage = 25;
    private void BindData()
    {
        int id = Helper.GetInt(Request.QueryString["id"], 0);
        DataRow master = DbProxy.Instance.FillRow("SELECT * FROM SalesInvoice WHERE ID=" + id);
        if (master == null)
        {
            mPlaceHolder.Controls.Add(new LiteralControl("خطا"));
            return;
        }
        DataRow buyer = DbProxy.Instance.FillRow("SELECT * FROM PersonnelView WHERE ID=" + Helper.GetInt(master, "BuyerID", -1));
        
        DataTable items = DbProxy.Instance.FillData("SELECT * FROM SalesInvoiceItemView WHERE SalesInvoiceID=" + id, "items");
        if (items == null || items.Rows.Count == 0)
        {
            mPlaceHolder.Controls.Add(new LiteralControl("FactorIsEmpty".Translate()));
            return;
        }

        No.Text = Helper.GetInt(master, "No", 0).ToString();
        SaleDate.Text = Helper.FormatDate(Helper.GetDateTime(master, "SaleDate", DateTime.Now), null);

        OrgName.Text = DbProxy.Instance.GetConfig("OrganizationID", -1);

        BuyerName.Text = Helper.GetString(buyer, "Name", "") + " " + Helper.GetString(buyer, "Surname", "");
        BuyerCode.Text = Helper.GetString(buyer, "Code", "");
        BuyerNC.Text = Helper.GetString(buyer, "NationalCode", "");
        BuyerHomeTelNum.Text = Helper.GetString(buyer, "HomeTelNum", "");
        BuyerHomeAddress.Text = Helper.GetString(buyer, "HomeAddress", "");
        StringBuilder builder = new StringBuilder();
        // -------------------------------------
        // Header
        string header = GetHeader();
        try
        {

            int i = 0, page = 1;
            bool closed = true;
            for (; i < items.Rows.Count; i++)
            {
                DataRow item = items.Rows[i];
                if (i % rowsPerPage == 0)
                {
                    if (builder.Length > 0)
                    {
                        // Close Table
                        builder.AppendLine("</tbody></table>");
                        closed = true;
                    }
                    // Start New
                    closed = false;
                    if (items.Rows.Count - i <= rowsPerPage)
                        builder.AppendLine("<table width='100%' border=1 cellpadding='0' cellspacing='0' dir='rtl'><tbody>");
                    else
                        builder.AppendLine("<table style='page-break-after: always' width='100%' align='center' cellpadding='0' cellspacing='0' border=1  dir='rtl'><tbody>");
                    builder.AppendLine("<tr><td colspan='11'>" + "TransactionGoodInformation".Translate() + "</td></tr>");
                    builder.AppendFormat("<tr><td dir='rtl' align='middle' colspan='11'>" + "PageDocumentAccounting".Translate() + " {0}</td></tr>", page++);
                    builder.Append(header);
                }

                string code = DbProxy.Instance.FillScalarString(@"Select Service.Code From Service 
INNER JOIN ServicePrice ON ServicePrice.ServiceID=Service.ID 
WHERE ServicePrice.ID IN (SELECT ServicePriceID FROM StoreServiceDef WHERE ID IN 
(SELECT StoreServiceDefID FROM StoreServices WHERE ID=_ID_))".Replace("_ID_", Helper.GetInt(item, "StoreServiceID", -1).ToString()));
                builder.AppendFormat("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td>{6}</td><td>{7}</td><td>{8}</td><td>{9}</td><td>{10}</td>"
                    , i + 1
                    , code
                    , Helper.GetString(item, "StoreServiceName", "")
                    , Helper.GetInt(item, "Amount", 0)
                    , Helper.GetString(item, "UnitName", "")
                    , Helper.GetDecimal(item, "Price", 0)
                    , Helper.GetDecimal(item, "Amount", 0) * Helper.GetInt(item, "Price", 0)
                    , Helper.GetDecimal(item, "Discount", 0)
                    , Helper.GetDecimal(item, "Amount", 0) * Helper.GetInt(item, "Price", 0) - Helper.GetDecimal(item, "Discount", 0)
                    , Helper.GetDecimal(item, "Tax", 0)
                    , Helper.GetDecimal(item, "Amount", 0) * Helper.GetInt(item, "Price", 0) - Helper.GetDecimal(item, "Discount", 0) + Helper.GetDecimal(item, "Tax", 0));
            }
            if (!closed)
            {
                builder.AppendLine("</tbody></table><table border=0 cellspacing=0>");
                while (i++ % rowsPerPage != 0)
                    builder.Append(" <tr dir=rtl><td class=rowNoLine align=middle colSpan=11>&nbsp;</td></tr>");
                // Close Table
                builder.AppendLine("</tbody></table>");
            }
            mPlaceHolder.Controls.Clear();
            mPlaceHolder.Controls.Add(new LiteralControl(builder.ToString()));
        }
        catch (Exception ex)
        {
            mPlaceHolder.Controls.Add(new LiteralControl("خطا: " + ex.Message + "<br />StackTrace: " + ex.StackTrace));
        }
    }
}