﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class print_voucher : System.Web.UI.Page
{
    const int rowsPerPage = 15;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            BindData();
        }
        catch (Exception ex)
        {
            mPlaceHolder.Controls.Add(new LiteralControl(LangProvider.Instance["Error"] + ex.Message));
        }
    }
    private string GetHeader()
    {
        StringBuilder b = new StringBuilder();
        b.Append("<tr>");
        b.Append(string.Format("<td>{0}</td>", LangProvider.Instance["ColRowNr"]));
        //b.Append("<td>شخص</td>");
        b.Append(string.Format("<td>{0}</td>", LangProvider.Instance["Code"]));
        b.Append(string.Format("<td>{0}</td>", LangProvider.Instance["headline"]));
        //b.Append("<td>کل</td>");
        //b.Append("<td>معین</td>");
        //b.Append("<td>تفضیلی</td>");

        //// add elabs      
        //int max = AccController.GetTreeDepth();
        //max -= 1; // program=0, total=1
        //if (max > 0)
        //    for (int i = 1; i < max; i++)
        //        b.Append(string.Format("<td>جزء{0}</td>", i));
        //b.Append("<td>ش مدرک</td>");
        //b.Append("<td>ت مدرک</td>");
        b.Append(string.Format("<td>{0}</td>", LangProvider.Instance["Portion"]));
        b.Append(string.Format("<td>{0}</td>", LangProvider.Instance["Liability"]));
        b.Append(string.Format("<td>{0}</td>", LangProvider.Instance["Credit"]));
        b.Append(string.Format("<td>{0}</td>", LangProvider.Instance["Comments"]));
        b.Append("</tr>");
        return b.ToString();

    }
    decimal sumUnit = 0, sumOwe = 0, sumDue = 0;
    private void BindData()
    {
        //"fromDate={0}&toDate={1}&serviceID={2}&driverID={3}&carID={4}&series={5}&no={6}",
        int id = Helper.GetInt(Request.QueryString["id"], 0);
        VoucherInfo info = AccController.GetVoucher(id);
        Boolean withNo = Helper.GetBool(Request.QueryString["withNo"], true);
        if (!withNo)
            info.No = null;

        DataTable dt = DbProxy.Instance.FillData(@"SELECT DocumentAccountingItems.Owe, DocumentAccountingItems.Due, DocumentAccountingItems.Comments
, Topic.Code TotalCode, Topic.Title TotalTitle
, [dbo].[DisplayPerson](Personnel.ID, 0) AS PersonnelNameCode
, dbo.TreePathToString(DocumentAccountingItems.TreePath, 0) AS TopicName 
, dbo.TreePathToTopicCode(DocumentAccountingItems.TreePath) TopicCode
FROM DocumentAccountingItems  
INNER JOIN Topic ON [dbo].[TreePathToProgramID](DocumentAccountingItems.TreePath)=Topic.ID
LEFT JOIN Personnel ON DocumentAccountingItems.PersonnelID=Personnel.ID
WHERE DocumentAccountingID=" + id + " ORDER BY SortIndex, DocumentAccountingID", "fck");
        // List<ItemInfo> items = AccController.GetItems(id);
        if (dt == null /*&& DbProxy.Instance.LastException != null*/)
        {
            //mPlaceHolder.Controls.Add(new LiteralControl("خطا: " + DbProxy.Instance.LastException.Message));
            mPlaceHolder.Controls.Add(new LiteralControl(LangProvider.Instance["Error"]));
            return;
        }
        if (dt.Rows.Count == 0)
        {
            mPlaceHolder.Controls.Add(new LiteralControl(LangProvider.Instance["DocumentIsEmpty"]));
            return;
        }

        StringBuilder builder = new StringBuilder();
        // -------------------------------------
        // Header
        string header = GetHeader();
        //int max = AccController.GetTreeDepth();
        string separator = DbProxy.Instance.GetConfig("TopicSeparator", "");
        string voucherSign1 = DbProxy.Instance.GetConfig("VoucherSign1", "");
        string voucherSign2 = DbProxy.Instance.GetConfig("VoucherSign2", "");
        string voucherSign3 = DbProxy.Instance.GetConfig("VoucherSign3", "");
        string voucherSign4 = DbProxy.Instance.GetConfig("VoucherSign4", "");
        string voucherSign5 = DbProxy.Instance.GetConfig("VoucherSign5", "");
        string numDocs = DbProxy.Instance.FillScalarString("SELECT COUNT(ID) FROM DocumentAccountingDocument WHERE DocumentAccountingID=" + id);
        try
        {

            int i = 0, page = 1;
            for (; i < dt.Rows.Count; i++)
            {
                DataRow item = dt.Rows[i];
                if (i % rowsPerPage == 0)
                {
                    if (builder.Length > 0)
                    {
                        // Close Table
                        builder.AppendLine("</tbody></table>");
                        builder.AppendLine(string.Format("<table style='page-break-after: always'  width='580' align='center' border=0 cellspacing=0  dir='rtl'><tr><td>{0}</td><td></td><td>{1}</td><td></td></tr>",LangProvider.Instance["Accountant"],LangProvider.Instance["AccountantSign"]));
                    }
                    // Start New
                    if (dt.Rows.Count - i <= rowsPerPage)
                        builder.AppendLine("<table width='880' align='center' border=1 cellspacing=0  dir='rtl'><tbody>");
                    else
                        builder.AppendLine("<table width='880' align='center' cellspacing=0 border=1  dir='rtl'><tbody>");
                    builder.AppendFormat("<tr><td dir='rtl' align='middle' colspan='11'>{0}{1}</td></tr>",LangProvider.Instance["DocumentAccountingPage"], page++);
                    builder.AppendFormat(@"<tr>
                        <td dir='rtl' align='middle' colspan='11'>{2} : {3}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{4} : {5}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{6} : {7}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{8} : {9}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{0} : {1}</td></tr><tr>"
                        , LangProvider.Instance["NumberOfAttachments"], DbProxy.Instance.GetConfig("OrganizationName", "")
                                   ,LangProvider.Instance["Date"]
                                   , Helper.FormatDate(info.Date, null), LangProvider.Instance["No"], info.No, LangProvider.Instance["Program"], AccController.GetProgramName(info.ProgramID)
                                   , LangProvider.Instance["FinanceYear"], SiteSettings.GetFinanceYearTitle(), numDocs);
                    //builder.AppendLine("<tr><td colspan='11' height='10'>&nbsp;</td></tr>");
                    builder.Append(header);
                }

                builder.AppendFormat("<tr><td>{0}</td><td>{1}</td><td{2}>{3}</td><td>{4}</td><td>{5}</td><td>{6}</td><td>{7}</td></tr>"
                   , i + 1
                   , Helper.GetString(item, "TotalCode", "")
                   , Helper.GetDecimal(item, "Due", 0) > 0 ? " class='tl'" : ""
                   , Helper.GetString(item, "TotalTitle", "")
                   , ""
                   , Helper.GetDecimal(item, "Owe", 0) == 0 ? "" : Helper.GetDecimal(item, "Owe", 0).ToString("N4", Helper.NumberCulture).Replace(".0000", "")
                   , Helper.GetDecimal(item, "Due", 0) == 0 ? "" : Helper.GetDecimal(item, "Due", 0).ToString("N4", Helper.NumberCulture).Replace(".0000", "")
                   , "");
                decimal val = Helper.GetDecimal(item, "Owe", 0) == 0 ? Helper.GetDecimal(item, "Due", 0) : Helper.GetDecimal(item, "Owe", 0);
                sumUnit += val;
                sumOwe += Helper.GetDecimal(item, "Owe", 0);
                sumDue += Helper.GetDecimal(item, "Due", 0);
                string topicCode = Helper.GetString(item, "TopicCode", "");
                if (topicCode.StartsWith(Helper.GetString(item, "TotalCode", "") + separator))
                    topicCode = topicCode.Substring((Helper.GetString(item, "TotalCode", "") + separator).Length);
                builder.AppendFormat("<tr><td>{0}</td><td>{1}</td><td{2}>{3}</td><td>{4}</td><td>{5}</td><td>{6}</td><td>{7}</td></tr>"
                   , ""
                   , topicCode
                   , Helper.GetDecimal(item, "Due", 0) > 0 ? " class='tl'" : ""
                   , Helper.GetString(item, "TopicName", "")
                   , val == 0 ? "" : val.ToString("N4", Helper.NumberCulture).Replace(".0000", "")
                   , "", "", Helper.GetString(item, "Comments", ""));

                //builder.AppendFormat("<td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>"
                //   , Helper.GetString(item, "DocNo", "")
                //   , Helper.FormatDate(Helper.GetDateTime(item, "DocDate", Null.NullDate), "yyyy-MM-dd")
                //   , Helper.GetDecimal(item, "Owe", 0) == 0 ? "" : Helper.GetDecimal(item, "Owe", 0).ToString("N4", Helper.NumberCulture).Replace(".0000", "")
                //   , Helper.GetDecimal(item, "Due", 0) == 0 ? "" : Helper.GetDecimal(item, "Due", 0).ToString("N4", Helper.NumberCulture).Replace(".0000", "")
                //   , Helper.GetString(item, "Comments", ""));

                //builder.AppendFormat("<tr><td>{0}</td><td>{1}</td><td{2}>{3}</td>"
                //    , i + 1
                //    , Helper.GetString(item, "PersonnelNameCode", "")
                //    , Helper.GetDecimal(item, "Due", 0) > 0 ? " class='tl'" : ""
                //    , Helper.GetString(item, "TreePathName", ""));
                //#region S
                ////if (max >= 2)
                ////{
                ////    string[] ids = item.TreePath.Split('-');
                ////    for (int j = 2; j <= max; j++)
                ////    {
                ////        if (ids.Length > 2 + j)
                ////            builder.AppendFormat("<td>{0}</td>", AccController.GetTopicTitle(Helper.GetInt(ids[2 + j], Null.NullInteger)));
                ////        else
                ////            builder.Append("<td> </td>");
                ////    }
                ////}
                //#endregion
                //builder.AppendFormat("<td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>"
                //   , Helper.GetString(item, "DocNo", "")
                //   , Helper.FormatDate(Helper.GetDateTime(item, "DocDate", Null.NullDate), "yyyy-MM-dd")
                //   , Helper.GetDecimal(item, "Owe", 0) == 0 ? "" : Helper.GetDecimal(item, "Owe", 0).ToString("N4", Helper.NumberCulture).Replace(".0000", "")
                //   , Helper.GetDecimal(item, "Due", 0) == 0 ? "" : Helper.GetDecimal(item, "Due", 0).ToString("N4", Helper.NumberCulture).Replace(".0000", "")
                //   , Helper.GetString(item, "Comments", ""));
            }
            //if (!closed)
            {
                builder.AppendLine(string.Format("<tr><td class=tl colspan=3>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>&nbsp;</td></tr>"
                   ,LangProvider.Instance["Sum"] , sumUnit.ToString("N4", Helper.NumberCulture).Replace(".0000", ""), sumOwe.ToString("N4", Helper.NumberCulture).Replace(".0000", ""), sumDue.ToString("N4", Helper.NumberCulture).Replace(".0000", "")));
                builder.AppendLine("</tbody></table>");
                builder.AppendLine(string.Format("<br /><table width='580' align='center' border=0 cellspacing=0  dir='rtl'><tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>"
                    , voucherSign1, voucherSign2, voucherSign3, voucherSign4, voucherSign5));
                //builder.AppendLine("<table border=0 cellspacing=0>");
                //while (i++ % rowsPerPage != 0)
                //    builder.Append(" <tr dir=rtl><td class=rowNoLine colSpan=10>&nbsp;</td></tr>");
                //// Close Table
                //builder.AppendLine("</tbody></table>");
            }
            mPlaceHolder.Controls.Clear();
            mPlaceHolder.Controls.Add(new LiteralControl(builder.ToString()));
        }
        catch (Exception ex)
        {
            mPlaceHolder.Controls.Add(new LiteralControl(LangProvider.Instance["Error"] + ex.Message + "<br />StackTrace: " + ex.StackTrace));
        }
    }
}