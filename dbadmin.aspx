﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true"
    CodeFile="dbadmin.aspx.cs" Inherits="dbadmin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div class="row">
        <div class="col-xs-12">
            <JP:PageHeader runat="server" Text="Lang.BackupPage" />
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h5 class="dashed-bottom yekan-family padding-10-bottom">
                <asp:Label runat="server">Lang.DBAdministration</asp:Label>
            </h5>
        </div>
        <div class="col-xs-12 col-sm-3">
            <asp:LinkButton runat="server" ID="doBackup" Text='Lang.DoBackup' OnClick="doBackup_Click" CssClass="btn btn-sm btn-primary btn-block">
                <i class="fa fa-floppy-o"></i>
                <asp:Label runat="server">Lang.DoBackup</asp:Label>
            </asp:LinkButton>
        </div>
        <div class="col-xs-12 col-sm-3">
            <asp:LinkButton runat="server" ID="doReloadStructure" OnClick="doReloadStructure_Click" CssClass="btn btn-sm btn-primary btn-block">
                <i class="fa fa-bullhorn"></i>
                <asp:Label runat="server">Lang.DoReloadStructure</asp:Label>
            </asp:LinkButton>
        </div>
        <div class="col-xs-12 col-sm-3">
            <asp:LinkButton runat="server" ID="doUpdateDB" OnClick="doUpdateDB_Click" CssClass="btn btn-sm btn-primary btn-block">
                <i class="fa fa-database"></i>
                <asp:Label runat="server">Lang.DoUpdateDB</asp:Label>
            </asp:LinkButton>
        </div>
        <div class="col-xs-12 col-sm-3">
            <asp:LinkButton runat="server" ID="doFixYK" OnClick="doFixYK_Click" CssClass="btn btn-sm btn-primary btn-block">
                <i class="fa fa-database"></i>
                <asp:Label runat="server">Lang.FixYK</asp:Label>
            </asp:LinkButton>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h5 class="dashed-bottom yekan-family padding-10-bottom">
                <asp:Label runat="server">Lang.AppAdministration</asp:Label>
            </h5>
        </div>
        
        <div class="col-xs-12 col-sm-3">
            <asp:LinkButton runat="server" ID="doUpdateSoftware" Text='Lang.DoUpdateSoftware' OnClick="doUpdateSoftware_Click" CssClass="btn btn-sm btn-success btn-block">
                <i class="fa fa-refresh"></i>
                <asp:Label runat="server">Lang.DoUpdateSoftware</asp:Label>
            </asp:LinkButton>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h5 class="dashed-bottom yekan-family padding-10-bottom">
                <asp:Label runat="server">Lang.UploadBackup</asp:Label>
            </h5>
        </div>
        <asp:Label runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-2 label-30 padding-0-left">
            Lang.ChooseBackupFile
        </asp:Label>
        <div class="col-xs-12 col-lg-4 padding-0-left">
            <asp:FileUpload runat="server" ID="mFile" CssClass="fileupload" />
        </div>
        <div class="col-xs-12 col-lg-2">
            <asp:LinkButton runat="server" ID="doUpload" OnClick="doUpload_Click" CssClass="btn btn-sm btn-primary btn-block">
                <i class="fa fa-upload"></i>
                <asp:Label runat="server">Lang.DoUpload</asp:Label>
            </asp:LinkButton>
        </div>
        <div class="col-xs-12 col-lg-4">
            <asp:Label runat="server" ID="lblUploadMessage" CssClass="label-30 alert block padding-0 padding-5-right" EnableViewState="False">
            </asp:Label>
        </div>
    </div>
    <div class="col-xs-12">
        &nbsp;
    </div>
    <div class="col-xs-12">
        <asp:Label runat="server" ID="msg" CssClass="label-30 alert block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
    </div>
    <div class="col-xs-12">
        &nbsp;
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h5 class="dashed-bottom yekan-family padding-10-bottom margin-20-top">
                <asp:label CssClass="yekan-family" runat="server">Lang.AvBackupFiles</asp:label>
            </h5>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <asp:GridView runat="server" ID="mBackups" AutoGenerateColumns="False" 
                OnRowDataBound="mBackups_RowDataBound"  CssClass="table table-bordered table-condensed zebra"
                onselectedindexchanged="mBackups_SelectedIndexChanged" OnRowCommand="save_Comment"
                OnRowDeleted="mBackups_RowDeleted" OnRowDeleting="mBackups_RowDeleting" OnRowEditing="mBackups_RowEditing">
                <Columns>
                    <asp:BoundField HeaderText="Lang.FileName" ItemStyle-Width="180px"/>
                    <asp:BoundField HeaderText="Lang.BackupDate" ItemStyle-Width="100px"/>
                    <asp:BoundField HeaderText="Lang.Download" Visible="false" />
                    <asp:templatefield headertext="Lang.Comments">
                    <ItemTemplate>
                        <asp:TextBox id="Comments" runat="server"  height="30px" width="90%"></asp:TextBox>
                        <asp:Button id="TextBox1" runat="server" Text="ذخیره" BorderStyle="None"  CommandName="AddComments" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"></asp:Button>
                    </ItemTemplate>
                    </asp:templatefield>
                    <asp:CommandField EditText="Lang.Download" HeaderText="Lang.Download" ShowEditButton="True" ItemStyle-Width="80px"/>
                    <asp:CommandField SelectText="Lang.Restore" HeaderText="Lang.Restore" ShowSelectButton="True" ItemStyle-Width="80px" />
                    <asp:CommandField DeleteText="Lang.ColDel" HeaderText="Lang.ColDel" ShowDeleteButton="true" ItemStyle-Width="80px"/>
                </Columns>
            </asp:GridView>
            <asp:Label runat="server" ID="lblNoBackups" Visible="False" EnableViewState="False" CssClass="alert alert-info block">
                Lang.NoBackup
            </asp:Label>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            setClassManager();
        });
    </script>
</asp:Content>
