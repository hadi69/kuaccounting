﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="Transfer.aspx.cs" Inherits="Transfer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="row">
        <div class="col-xs-12">
            <JP:PageHeader runat="server" Text="Lang.TransferFactors" />
        </div>
    </div>
    <div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label4" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.Based</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:DropDownList runat="server" ID="fType" CssClass="chosen-select chosen-rtl">
                            <asp:ListItem Text="Lang.StoreDraft" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Lang.Purchase" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Lang.Refuse" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Lang.Return" Value="3"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" AssociatedControlID="fFromDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="fFromDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="fFromDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label3" runat="server" AssociatedControlID="fToDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="fToDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="fToDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-lg-3 form-group-container" runat="server">
                <div class="form-group">
                    <asp:Label ID="Label26" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.Store</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:DropDownList ID="fStores" runat="server" CssClass="chosen-select chosen-rtl" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-9 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label1" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Dest</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:TextBox ID="mDest" runat="server" CssClass="form-control" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin-5-top">
            <div class="col-md-8 col-lg-6 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container form-col-without-padding">
                <div class="col-xs-12 col-sm-3">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doShow_Click" />
                </div>
                <div class="col-xs-12 col-sm-3">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExcel_Click" />
                </div>
                <div class="col-xs-12 col-sm-3">
                    <asp:Button ID="doTransfer" runat="server" Text="Lang.Transfer" CssClass="btn btn-xs btn-primary btn-block doShow" OnClick="doTransfer_Click" />
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12" style="direction: rtl;">
        <asp:Label runat="server" ID="msg" CssClass="label-30 alert block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
    </div>
    <div class="col-xs-12" style="direction: ltr;">
        <asp:Label runat="server" ID="error" CssClass="label-30 alert block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
    </div>
    <div class="row">
        <div class="col-xs-12">
            <asp:GridView ID="mGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed"
                Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                AllowSorting="true" OnSorting="mGrid_Sorting">
                <Columns>
                    <asp:TemplateField HeaderText="Lang.ColRowNr">
                        <ItemTemplate>
                            <%# Container.DataItemIndex + 1 %>
                        </ItemTemplate>
                        <ItemStyle CssClass="row-cell"></ItemStyle>
                        <HeaderStyle CssClass="row-cell"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Lang.ColRowNr">
                        <ItemTemplate>
                            <input class="ch" type="checkbox" name="ch<%# Eval("ID") %>" />
                        </ItemTemplate>
                        <ItemStyle CssClass="row-cell"></ItemStyle>
                        <HeaderStyle CssClass="row-cell"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ID" ItemStyle-HorizontalAlign="Center" Visible="false" />
                    <asp:BoundField DataField="NOSerial" HeaderText="Lang.DocumentNumber" ItemStyle-HorizontalAlign="Center" SortExpression="NOSerial" />
                    <asp:BoundField DataField="Date" HeaderText="Lang.Date" ItemStyle-CssClass="tl" SortExpression="Date" />
                    <asp:BoundField DataField="Person" HeaderText="Lang.Person" ItemStyle-HorizontalAlign="Center" SortExpression="Person" />
                    <asp:BoundField DataField="Comments" HeaderText="Lang.Comments" ItemStyle-HorizontalAlign="Center" SortExpression="Comments" />
                    <asp:BoundField DataField="TotalPrice" HeaderText="Lang.TotalPrice" ItemStyle-HorizontalAlign="Center" SortExpression="TotalPrice" />
                    <asp:BoundField DataField="TotalAmount" HeaderText="Lang.TotalAmount" ItemStyle-HorizontalAlign="Center" SortExpression="TotalAmount" />
                </Columns>
                <HeaderStyle CssClass="GridHeader" />
                <SelectedRowStyle CssClass="GridRowSelected" />
            </asp:GridView>
        </div>
    </div>
    <script>
        $(function () {
            dateSelect('.dateselect');
            $('.ch').click(function () {
                if ($(this).attr('checked'))
                    $(this).closest('tr').addClass('selected');
                else
                    $(this).closest('tr').removeClass('selected');
            });
        });
    </script>
</asp:Content>

