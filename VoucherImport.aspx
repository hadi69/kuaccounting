﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="VoucherImport.aspx.cs" Inherits="VoucherImport" %>

<%@ Register Src="controls/ReportSetting.ascx" TagName="ReportSetting" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>
    <asp:Label runat="server" ID="mMsg" CssClass="alert block" EnableViewState="False" Visible="False"></asp:Label>
    <asp:Label runat="server" Text="Lang.ChooseFile" CssClass="dashed-bottom yekan-family padding-10-bottom margin-10-bottom block"></asp:Label>
    <asp:FileUpload runat="server" ID="mFileUpload" CssClass="fileupload" /><asp:Label runat="server" ID="mFileName"></asp:Label><br />
    <asp:Button runat="server" ID="doUpload" Text="Lang.Upload" OnClick="doUpload_Click" CssClass="btn btn-sm btn-primary" />
    <br />
    <asp:Panel runat="server" ID="mDetail" Visible="false">
        <div class="SettingRow">
            <uc2:ReportSetting ID="mReportSetting" runat="server" />
        </div>
        <asp:Label runat="server" ID="mHeaders" Visible="false"></asp:Label>
        <asp:Label runat="server" Text="Lang.Preview"></asp:Label>:
        <asp:Label runat="server" ID="mPreview"></asp:Label><br />
        <asp:PlaceHolder runat="server" ID="mColumns"></asp:PlaceHolder>
        <asp:CheckBox runat="server" ID="mHeaderRow1" Text="Lang.HeaderRow1" Checked="true" /><br />
        <asp:CheckBox runat="server" ID="mOneVoucher" Text="Lang.OneVoucher" /><br />
        <asp:Label runat="server" Text="Lang.Type"></asp:Label>:<asp:DropDownList runat="server" ID="mType">
            <asp:ListItem Text="Lang.VoucherTypeDetail" Value="Detail"></asp:ListItem>
            <asp:ListItem Text="Lang.VoucherTypeMaster" Value="Master"></asp:ListItem>
        </asp:DropDownList><br />
        <asp:Button runat="server" ID="doImport" Text="Lang.StartProcess" OnClick="doImport_Click" CssClass="btn btn-sm btn-primary" />
    </asp:Panel>
    <script>
        $(function () {
            checkLevel(2);
            setClassAccounting();
            function handleType(who) {
                if ($(who).val() == 'empty') {
                    $(who).closest('tr').find('.fld').hide();
                    $(who).closest('tr').find('.t').hide();
                }
                else if ($(who).val() == 'field') {
                    $(who).closest('tr').find('.fld').show();
                    $(who).closest('tr').find('.t').hide();
                }
                else {
                    $(who).closest('tr').find('.fld').hide();
                    $(who).closest('tr').find('.t').show();
                }
            }
            $('.dp').each(function () {
                handleType(this);
            });
            $('.dp').change(function () { handleType(this); });

            autoComplete('Trees', 'treesauto', '.t', 'fuckTurkey', function (event, data, formatted, textInput, hiddenInput) {
                if (data) {
                    // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due, TopicTitle
                    var hasChild = data[5];
                    if (hasChild == "True") {
                        $(event.target).next().val('');
                        alert(Lang.HasChild);
                    }
                    else {
                        $(event.target).next().val(data[0]);
                    }
                }
            }, formatTreePath, function (data) { return data[8]; });
            $("#bar").prepend(breadcrumb({
                title: Lang.ImportVoucher,
                parent: {
                    title: Lang.MAccounting,
                    url: 'menu/accounting.aspx'
                }
            }));
        });
    </script>
</asp:Content>

