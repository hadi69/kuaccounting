﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using fyiReporting.RDL;
using SocialExplorer.IO.FastDBF;
using System.Security.AccessControl;
using Stimulsoft.Report;
using System.Globalization;
using System.Data.OleDb;

public partial class Insurance : System.Web.UI.Page
{
    int NumOfPersonnel = 0;
    private string folderName = "";
    private string WorkShopCode = "";
    private DataRow Section = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindYear();
            BindSection();
        }
    }
    void BindYear()
    {
        PersianCalendar calendar = new PersianCalendar();
        int year = calendar.GetYear(DateTime.Now);
        ddlYear.Items.Add((year - 1).ToString());
        ddlYear.Items.Add(year.ToString());
        ddlYear.Items.Add((year + 1).ToString());
        ddlYear.SelectedIndex = 1;
    }

    void BindSection()
    {
        DataTable sections = DbProxy.Instance.FillData("SELECT ID, Name FROM Ward WHERE SectionType=9 AND Enabled=1", "sections");

        ddlSection.DataTextField = "Name";
        ddlSection.DataValueField = "ID";
        ddlSection.DataSource = sections;
        ddlSection.DataBind();
        ddlSection.Items.Insert(0, "");
        ddlSection.SelectedIndex = 0;
    }
    string GetFolder()
    {
        string folder = Server.MapPath("~/temp/Insurance");
        folderName = Helper.FormatDate(DateTime.Now, "yyyy-MM-dd-fff");
        folder = Path.Combine(folder, folderName);

        if (!Directory.Exists(folder))
        {
            Directory.CreateDirectory(folder);
        }
        return folder;
    }
    protected void CreateFile_Click(object sender, EventArgs e)
    {
        string folder = GetFolder();
        if (string.IsNullOrEmpty(ListNo.Text))
        {
            mFile.Text = LangProvider.Instance["ListNoNecessary"];
            mFile.Visible = true;
        }
        else
        {
            if (string.IsNullOrEmpty(ddlSection.SelectedValue))
            {
                WorkShopCode = DbProxy.Instance.GetConfig("CompanyCode");
            }
            else
            {
                Section = DbProxy.Instance.FillRow("SELECT * FROM Ward WHERE SectionType=9 AND ID=" + ddlSection.SelectedValue);
                WorkShopCode = Helper.GetString(Section, "Code", "0");
            }
            mFile.Text = "";
            mFile.Visible = false;
            if (cbDbf.Checked)
            {
                //Create DBF Files
                CreateDBFCompanyFile(folder);
                CreateDBFInsuranceFile(folder);
            }
            if (cbTxt.Checked)
            {
                //Create TXT Files
                CreateTXTCompanyFile(folder);
                CreateTXTInsuranceFile(folder);
            }
            mFile.Visible = true;
        }

    }
    void CreateDBFCompanyFile(string folder)
    {

        string fileName = "DSKKAR00.DBF";
        string file = Path.Combine(folder, fileName);
        if (!file.ToLower().EndsWith(".dbf"))
            file += ".dbf";
        DbfFile dbf = new DbfFile(System.Text.UnicodeEncoding.UTF8);
        dbf.Open(file, FileMode.Create);

        int mul = 1, mulInt = 1;

        #region add columns
        dbf.Header.AddColumn(new DbfColumn("DSK_ID", DbfColumn.DbfColumnType.Character, 10 * mul, 0)); //0
        dbf.Header.AddColumn(new DbfColumn("DSK_NAME", DbfColumn.DbfColumnType.Character, 100 * mul, 0));
        dbf.Header.AddColumn(new DbfColumn("DSK_FARM", DbfColumn.DbfColumnType.Character, 100 * mul, 0));
        dbf.Header.AddColumn(new DbfColumn("DSK_ADRS", DbfColumn.DbfColumnType.Character, 100 * mul, 0));
        dbf.Header.AddColumn(new DbfColumn("DSK_KIND", DbfColumn.DbfColumnType.Number, 1 * mulInt, 0));
        dbf.Header.AddColumn(new DbfColumn("DSK_YY", DbfColumn.DbfColumnType.Number, 2 * mulInt, 0)); //5
        dbf.Header.AddColumn(new DbfColumn("DSK_MM", DbfColumn.DbfColumnType.Number, 2 * mulInt, 0));
        dbf.Header.AddColumn(new DbfColumn("DSK_LISTNO", DbfColumn.DbfColumnType.Character, 12 * mul, 0));
        dbf.Header.AddColumn(new DbfColumn("DSK_DISC", DbfColumn.DbfColumnType.Character, 100 * mul, 0));
        dbf.Header.AddColumn(new DbfColumn("DSK_NUM", DbfColumn.DbfColumnType.Number, 5 * mulInt, 0));
        dbf.Header.AddColumn(new DbfColumn("DSK_TDD", DbfColumn.DbfColumnType.Number, 6 * mulInt, 0)); //10
        dbf.Header.AddColumn(new DbfColumn("DSK_TROOZ", DbfColumn.DbfColumnType.Number, 12 * mulInt, 0));
        dbf.Header.AddColumn(new DbfColumn("DSK_TMAH", DbfColumn.DbfColumnType.Number, 12 * mulInt, 0));
        dbf.Header.AddColumn(new DbfColumn("DSK_TMAZ", DbfColumn.DbfColumnType.Number, 12 * mulInt, 0));
        dbf.Header.AddColumn(new DbfColumn("DSK_TMASH", DbfColumn.DbfColumnType.Number, 12 * mulInt, 0));
        dbf.Header.AddColumn(new DbfColumn("DSK_TTOTL", DbfColumn.DbfColumnType.Number, 12 * mulInt, 0));//15
        dbf.Header.AddColumn(new DbfColumn("DSK_TBIME", DbfColumn.DbfColumnType.Number, 12 * mulInt, 0));
        dbf.Header.AddColumn(new DbfColumn("DSK_TKOSO", DbfColumn.DbfColumnType.Number, 12 * mulInt, 0));
        dbf.Header.AddColumn(new DbfColumn("DSK_BIC", DbfColumn.DbfColumnType.Number, 12 * mulInt, 0));
        dbf.Header.AddColumn(new DbfColumn("DSK_RATE", DbfColumn.DbfColumnType.Number, 5 * mulInt, 0));
        dbf.Header.AddColumn(new DbfColumn("DSK_PRATE", DbfColumn.DbfColumnType.Number, 2 * mulInt, 0)); //20
        dbf.Header.AddColumn(new DbfColumn("DSK_BIMEH", DbfColumn.DbfColumnType.Number, 12 * mulInt, 0));
        dbf.Header.AddColumn(new DbfColumn("MON_PYM", DbfColumn.DbfColumnType.Character, 3 * mul, 0));
        #endregion

        try
        {
            string EmployerName = "", CompanyName="";
            string qry = "SELECT Fish.ID, PersonnelID, HasInsurance FROM Fish INNER JOIN Personnel ON Fish.PersonnelID=Personnel.ID WHERE FishYear=" + ddlYear.Text + " AND FishMonth=" +
                         ddlMonth.SelectedValue;
            if (Helper.GetInt(ddlSection.SelectedValue, -1) > 0)
            {
                qry += " AND Personnel.WardID=" + ddlSection.SelectedValue;
                EmployerName =
                    Helper.GetString(
                        DbProxy.Instance.FillScalar(
                            @"SELECT [dbo].[DisplayPerson](ID, 0) AS EmployerName FROM PersonnelView WHERE ID=" +
                            Helper.GetInt(Section, "EmployerID", -1)));
                CompanyName = Helper.GetString(Section, "Name", "");
            }
            else
            {
                 EmployerName = Helper.GetString(DbProxy.Instance.FillScalar(@"SELECT [dbo].[DisplayPerson](ID, 0) AS EmployerName FROM PersonnelView WHERE ID=" + DbProxy.Instance.GetConfig("Signature1")), "");
                CompanyName = DbProxy.Instance.GetConfig("CompanyName", "");
            }
            DataTable fishs = DbProxy.Instance.FillData(qry, "fishs");
            NumOfPersonnel = fishs.Rows.Count;
            decimal TotalInsuranceWork = 0, TotalDayWages = 0, TotalMonthWage = 0, TotalInsuranceWage = 0, TotalPrice = 0, TotalInsuranced = 0, TotalEmployer = 0, TotalBikari = 0, TotalBenefits=0;
            for (int i = 0; i < fishs.Rows.Count; i++)
            {
                string fid = Helper.GetString(fishs.Rows[i], "ID", "");
                bool hasInsurance = Helper.GetBool(fishs.Rows[i], "HasInsurance", false);
                FormulaEvaluater.FishID = int.Parse(fid);
                DataTable fishValues = DbProxy.Instance.FillData("SELECT * FROM FishValuesView WHERE FishID=" + fid, "fishValue");
                DataTable personnelInfo =
                   DbProxy.Instance.FillData(
                       "SELECT * FROM Personnel WHERE ID=" + Helper.GetString(fishs.Rows[i], "PersonnelID", "")
                       , "personnelInfo");
                string InsuranceTypeCode = Helper.GetEnumValuesCode(Helper.GetString(personnelInfo.Rows[0], "InsuranceTypeID", ""));
                //if (Helper.GetInt(InsuranceTypeCode, 0) != 1)
                //    NumOfPersonnel--;
                if (!hasInsurance)
                    NumOfPersonnel--;
                else
                {
                    TotalInsuranceWork = TotalInsuranceWork + Helper.GetFishValue(fishValues, Helper.GetField("WorkDays", ReportFieldType.Insurance), 0);
                    TotalDayWages = TotalDayWages + Helper.GetFishValue(fishValues, Helper.GetField("DayWage", ReportFieldType.Insurance), 0);
                    TotalMonthWage = TotalMonthWage + Helper.GetFishValue(fishValues, Helper.GetField("MonthWage", ReportFieldType.Insurance), 0);
                    TotalBenefits = TotalBenefits + Helper.GetFishValue(fishValues, Helper.GetField("BenefitWage", ReportFieldType.Insurance), 0);
                    TotalInsuranceWage = TotalInsuranceWage + Helper.GetFishValue(fishValues, Helper.GetField("InsuranceWage", ReportFieldType.Insurance), 0);
                    TotalPrice = TotalPrice + Helper.GetFishValue(fishValues, Helper.GetField("TotalPrice", ReportFieldType.Insurance), 0);
                    TotalInsuranced = TotalInsuranced + Helper.GetFishValue(fishValues, Helper.GetField("Insurance7", ReportFieldType.Insurance), 0);
                    TotalEmployer = TotalEmployer + Helper.GetFishValue(fishValues, Helper.GetField("Insurance20", ReportFieldType.Insurance), 0) + Helper.GetFishValue(fishValues, Helper.GetField("Insurance10", ReportFieldType.Insurance), 0);
                    TotalBikari = TotalBikari + Helper.GetFishValue(fishValues, Helper.GetField("Insurance3", ReportFieldType.Insurance), 0);
                }
            }

            //add company info
            DbfRecord dbfRecord = new DbfRecord(dbf.Header);
            dbfRecord[0] = WorkShopCode;
            dbfRecord.SetTextValue(1, UnicodeToIranSys.Convert(CompanyName));
            dbfRecord.SetTextValue(2, UnicodeToIranSys.Convert(EmployerName));
            dbfRecord.SetTextValue(3,UnicodeToIranSys.Convert(DbProxy.Instance.GetConfig("Address")));
            dbfRecord[4] = "0";
            dbfRecord[5] = ddlYear.Text.Substring(2, 2);
            dbfRecord[6] = ddlMonth.SelectedValue;
            dbfRecord[7] = ListNo.Text;
            dbfRecord.SetTextValue(8, UnicodeToIranSys.Convert(ListComment.Text));
            dbfRecord[9] = NumOfPersonnel.ToString();
            dbfRecord[10] = TotalInsuranceWork.ToString();
            dbfRecord[11] = TotalDayWages.ToString();
            dbfRecord[12] = Convert.ToInt64(TotalMonthWage).ToString();
            dbfRecord[13] = Convert.ToInt64(TotalBenefits).ToString(); ;
            dbfRecord[14] = Convert.ToInt64(TotalInsuranceWage).ToString();
            dbfRecord[15] = Convert.ToInt64(TotalPrice).ToString();
            dbfRecord[16] = Convert.ToInt64(TotalInsuranced).ToString();
            dbfRecord[17] = Convert.ToInt64(TotalEmployer).ToString();
            dbfRecord[18] = Convert.ToInt64(TotalBikari).ToString();
            dbfRecord[19] = "23";
            dbfRecord[20] = "0";
            dbfRecord[21] = "0";
            dbfRecord[22] = "0";
            dbf.Write(dbfRecord);
            dbf.WriteHeader();

            mFile.Text += LangProvider.Instance.ParseLang("Lang.CompanyFileCreated") + ": <a href='temp/Insurance/" + folderName + "/" + fileName + "' download='" + fileName + "'>" + LangProvider.Instance.ParseLang("Lang.Download") + "</a><br />";
        }
        catch (Exception ex)
        {
            mFile.Text = ex.Message;
            Logger.Log(ex);
        }
        finally
        {
            dbf.Close();
        }
    }
    void CreateDBFInsuranceFile(string folder)
    {
        string fileName = "DSKWOR00.DBF";
        string file = Path.Combine(folder, fileName);
        if (!file.ToLower().EndsWith(".dbf"))
            file += ".DBF";

        DbfFile dbf = new DbfFile(System.Text.UnicodeEncoding.UTF8);
        //DbfFile dbf = new DbfFile(Encoding.GetEncoding(1256)); //
        dbf.Open(file, FileMode.Create, FileAccess.ReadWrite);

        int mul = 1;
        #region define header
        dbf.Header.AddColumn(new DbfColumn("DSW_ID", DbfColumn.DbfColumnType.Character, 10 * mul, 0));
        dbf.Header.AddColumn(new DbfColumn("DSW_YY", DbfColumn.DbfColumnType.Number, 2, 0));
        dbf.Header.AddColumn(new DbfColumn("DSW_MM", DbfColumn.DbfColumnType.Number, 2, 0));
        dbf.Header.AddColumn(new DbfColumn("DSW_LISTNO", DbfColumn.DbfColumnType.Character, 12 * mul, 0));
        dbf.Header.AddColumn(new DbfColumn("DSW_ID1", DbfColumn.DbfColumnType.Character, 10 * mul, 0));
        dbf.Header.AddColumn(new DbfColumn("DSW_FNAME", DbfColumn.DbfColumnType.Character, 100 * mul, 0));
        dbf.Header.AddColumn(new DbfColumn("DSW_LNAME", DbfColumn.DbfColumnType.Character, 100 * mul, 0));
        dbf.Header.AddColumn(new DbfColumn("DSW_DNAME", DbfColumn.DbfColumnType.Character, 100 * mul, 0));
        dbf.Header.AddColumn(new DbfColumn("DSW_IDNO", DbfColumn.DbfColumnType.Character, 15 * mul, 0));
        dbf.Header.AddColumn(new DbfColumn("DSW_IDPLC", DbfColumn.DbfColumnType.Character, 100 * mul, 0));
        dbf.Header.AddColumn(new DbfColumn("DSW_IDATE", DbfColumn.DbfColumnType.Character, 8 * mul, 0));
        dbf.Header.AddColumn(new DbfColumn("DSW_BDATE", DbfColumn.DbfColumnType.Character, 8 * mul, 0));
        dbf.Header.AddColumn(new DbfColumn("DSW_SEX", DbfColumn.DbfColumnType.Character, 3 * mul, 0));
        dbf.Header.AddColumn(new DbfColumn("DSW_NAT", DbfColumn.DbfColumnType.Character, 10 * mul, 0));
        dbf.Header.AddColumn(new DbfColumn("DSW_OCP", DbfColumn.DbfColumnType.Character, 100 * mul, 0));
        dbf.Header.AddColumn(new DbfColumn("DSW_SDATE", DbfColumn.DbfColumnType.Character, 8 * mul, 0));
        dbf.Header.AddColumn(new DbfColumn("DSW_EDATE", DbfColumn.DbfColumnType.Character, 8 * mul, 0));
        dbf.Header.AddColumn(new DbfColumn("DSW_DD", DbfColumn.DbfColumnType.Number, 2, 0));
        dbf.Header.AddColumn(new DbfColumn("DSW_ROOZ", DbfColumn.DbfColumnType.Number, 12, 0));
        dbf.Header.AddColumn(new DbfColumn("DSW_MAH", DbfColumn.DbfColumnType.Number, 12, 0));
        dbf.Header.AddColumn(new DbfColumn("DSW_MAZ", DbfColumn.DbfColumnType.Number, 12, 0));
        dbf.Header.AddColumn(new DbfColumn("DSW_MASH", DbfColumn.DbfColumnType.Number, 12, 0));
        dbf.Header.AddColumn(new DbfColumn("DSW_TOTL", DbfColumn.DbfColumnType.Number, 12, 0));
        dbf.Header.AddColumn(new DbfColumn("DSW_BIME", DbfColumn.DbfColumnType.Number, 12, 0));
        dbf.Header.AddColumn(new DbfColumn("DSW_PRATE", DbfColumn.DbfColumnType.Number, 2, 0));
        dbf.Header.AddColumn(new DbfColumn("DSW_JOB", DbfColumn.DbfColumnType.Character, 6 * mul, 0));
        dbf.Header.AddColumn(new DbfColumn("PER_NATCOD", DbfColumn.DbfColumnType.Character, 10 * mul, 0));
        #endregion

        string errors = "";
        try
        {
            string qry = "SELECT Fish.ID, PersonnelID, HasInsurance FROM Fish INNER JOIN Personnel ON Fish.PersonnelID=Personnel.ID WHERE FishYear=" + ddlYear.Text + " AND FishMonth=" +
                         ddlMonth.SelectedValue;
            if (Helper.GetInt(ddlSection.SelectedValue, -1) > 0)
            {
                qry += " AND Personnel.WardID=" + ddlSection.SelectedValue;
            }
            DataTable fishs = DbProxy.Instance.FillData(qry, "fishs");
            NumOfPersonnel = fishs.Rows.Count;
            for (int i = 0; i < fishs.Rows.Count; i++)
            {
                DbfRecord dbfRecord = new DbfRecord(dbf.Header);
                string fid = Helper.GetString(fishs.Rows[i], "ID", "");
                bool hasInsurance = Helper.GetBool(fishs.Rows[i], "HasInsurance", false);
                try
                {
                    FormulaEvaluater.FishID = int.Parse(fid);
                    DataTable fishValues = DbProxy.Instance.FillData("SELECT * FROM FishValuesView WHERE FishID=" + fid, "fishValue");
                    DataTable personnelInfo =
                       DbProxy.Instance.FillData(
                           "SELECT * FROM Personnel WHERE ID=" + Helper.GetString(fishs.Rows[i], "PersonnelID", "")
                           , "personnelInfo");
                    if (personnelInfo == null || personnelInfo.Rows.Count == 0)
                        continue;
                    string InsuranceTypeCode = Helper.GetEnumValuesCode(Helper.GetString(personnelInfo.Rows[0], "InsuranceTypeID", ""));
                    //if (Helper.GetInt(InsuranceTypeCode, 0) != 1)
                    //    NumOfPersonnel--;
                    if (!hasInsurance)
                        NumOfPersonnel--;
                    else
                    {
                        string IssuePlace = DbProxy.Instance.FillScalarString("SELECT Title FROM City WHERE ID=" + Helper.GetString(personnelInfo.Rows[0], "IssuePlaceID", ""));
                        string Gender = DbProxy.Instance.FillScalarString("SELECT Title FROM Gender WHERE ID=" + Helper.GetString(personnelInfo.Rows[0], "GenderID", ""));
                        string JobCode = DbProxy.Instance.FillScalarString("SELECT Code FROM Job WHERE ID=" + Helper.GetString(personnelInfo.Rows[0], "JobID", ""));
                        string IssueDate = Helper.FormatDate(Convert.ToDateTime(Helper.GetString(personnelInfo.Rows[0], "IssueDate", null)), "yyyyMMdd");
                        string BirthDate = Helper.FormatDate(Convert.ToDateTime(Helper.GetString(personnelInfo.Rows[0], "BirthDate", null)), "yyyyMMdd");
                        string StartDate = Helper.FormatDate(Convert.ToDateTime(Helper.GetString(personnelInfo.Rows[0], "StartDate", null)), "yyyyMMdd");
                        string EndDate = Helper.FormatDate(Convert.ToDateTime(Helper.GetString(personnelInfo.Rows[0], "EndDate", null)), "yyyyMMdd");
                        string Nationality = Helper.GetEnumValuesTitle(Helper.GetString(personnelInfo.Rows[0], "NationalityID", ""));

                        #region fill record
                        //dbfRecord.SetTextValue(0, UnicodeToIranSys.Convert(WorkShopCode));
                        dbfRecord[0] = WorkShopCode;
                        dbfRecord[1] = ddlYear.Text.Substring(2, 2);
                        dbfRecord[2] = ddlMonth.SelectedValue;
                        dbfRecord[3] = ListNo.Text;
                        dbfRecord[4] = Helper.GetString(personnelInfo.Rows[0], "InsuranceNo", "");
                        dbfRecord.SetTextValue(5, UnicodeToIranSys.Convert(Helper.GetString(personnelInfo.Rows[0], "Name", "")));
                        dbfRecord.SetTextValue(6, UnicodeToIranSys.Convert(Helper.GetString(personnelInfo.Rows[0], "Surname", "")));
                        dbfRecord.SetTextValue(7, UnicodeToIranSys.Convert(Helper.GetString(personnelInfo.Rows[0], "Father", "")));
                        dbfRecord[8] = Helper.GetString(personnelInfo.Rows[0], "IDCardNo", "");
                        dbfRecord.SetTextValue(9, UnicodeToIranSys.Convert(IssuePlace));
                        dbfRecord[10] = IssueDate;
                        dbfRecord[11] = BirthDate;
                        dbfRecord.SetTextValue(12, UnicodeToIranSys.Convert(Gender));
                        dbfRecord.SetTextValue(13, UnicodeToIranSys.Convert(Nationality));
                        dbfRecord[14] = "";
                        dbfRecord[15]= StartDate;
                        dbfRecord[16]= EndDate;
                        dbfRecord[17] = Helper.GetFishValue(fishValues, Helper.GetField("WorkDays", ReportFieldType.Insurance), 0).ToString();
                        dbfRecord[18] = Helper.GetFishValue(fishValues, Helper.GetField("DayWage", ReportFieldType.Insurance), 0).ToString();
                        dbfRecord[19] = Helper.GetFishValue(fishValues, Helper.GetField("MonthWage", ReportFieldType.Insurance), 0).ToString();
                        dbfRecord[20] = Helper.GetFishValue(fishValues, Helper.GetField("BenefitWage", ReportFieldType.Insurance), 0).ToString();
                        dbfRecord[21] = Helper.GetFishValue(fishValues, Helper.GetField("InsuranceWage", ReportFieldType.Insurance), 0).ToString();
                        dbfRecord[22] = Helper.GetFishValue(fishValues, Helper.GetField("TotalPrice", ReportFieldType.Insurance), 0).ToString();
                        dbfRecord[23] = Helper.GetFishValue(fishValues, Helper.GetField("Insurance7", ReportFieldType.Insurance), 0).ToString();
                        dbfRecord[24] = "0";
                        dbfRecord[25] = JobCode;
                        dbfRecord[26] = Helper.GetString(personnelInfo.Rows[0], "NatinoalCode", "");
                        dbf.Write(dbfRecord);
                        #endregion
                    }


                }
                catch (Exception ex1)
                {
                    errors += "Error in record " + i + " : " + ex1.Message + "<br />";
                }
            }
            mFile.Text += LangProvider.Instance.ParseLang("Lang.InsuranceFileCreated") + ": <a href='temp/Insurance/" + folderName + "/" + fileName + "' download='" + fileName + "'>" + LangProvider.Instance.ParseLang("Lang.Download") + "</a><br />" + errors;
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
        }
        finally
        {
            dbf.WriteHeader();
            dbf.Close();
        }
    }

    void CreateTXTCompanyFile(string folder)
    {
        string fileName = "DSKKAR00.txt";
        string file = Path.Combine(folder, fileName);
        if (!file.ToLower().EndsWith(".txt"))
            file += ".txt";

        //StreamWriter sw = new StreamWriter(file, true, Encoding.UTF8);
        FileStream sw = File.Create(file);
        try
        {
            string EmployerName = "", CompanyName = "";
            string qry = "SELECT Fish.ID, PersonnelID, HasInsurance FROM Fish INNER JOIN Personnel ON Fish.PersonnelID=Personnel.ID WHERE FishYear=" + ddlYear.Text + " AND FishMonth=" +
                         ddlMonth.SelectedValue;
            if (Helper.GetInt(ddlSection.SelectedValue, -1) > 0)
            {
                qry += " AND Personnel.WardID=" + ddlSection.SelectedValue;
                EmployerName =
                    Helper.GetString(
                        DbProxy.Instance.FillScalar(
                            @"SELECT [dbo].[DisplayPerson](ID, 0) AS EmployerName FROM PersonnelView WHERE ID=" +
                            Helper.GetInt(Section, "EmployerID", -1)));
                CompanyName = Helper.GetString(Section, "Name", "");
            }
            else
            {
                EmployerName =
                Helper.GetString(
                    DbProxy.Instance.FillScalar(
                        @"SELECT [dbo].[DisplayPerson](ID, 0) AS EmployerName FROM PersonnelView WHERE ID=" +
                        DbProxy.Instance.GetConfig("Signature1")), "");
                CompanyName = Helper.GetString(Section, "Name", "");
            }
            DataTable fishs = DbProxy.Instance.FillData(qry, "fishs");
            NumOfPersonnel = fishs.Rows.Count;
            decimal TotalInsuranceWork = 0,
                TotalDayWages = 0,
                TotalMonthWage = 0,
                TotalBenefits = 0,
                TotalInsuranceWage = 0,
                TotalPrice = 0,
                TotalInsuranced = 0,
                TotalEmployer = 0,
                TotalBikari = 0;
            for (int i = 0; i < fishs.Rows.Count; i++)
            {
                string fid = Helper.GetString(fishs.Rows[i], "ID", "");
                bool hasInsurance = Helper.GetBool(fishs.Rows[i], "HasInsurance", false);
                FormulaEvaluater.FishID = int.Parse(fid);
                DataTable fishValues = DbProxy.Instance.FillData("SELECT * FROM FishValuesView WHERE FishID=" + fid,
                    "fishValue");
                DataTable personnelInfo =
                   DbProxy.Instance.FillData(
                       "SELECT * FROM Personnel WHERE ID=" + Helper.GetString(fishs.Rows[i], "PersonnelID", "")
                       , "personnelInfo");
                string InsuranceTypeCode = Helper.GetEnumValuesCode(Helper.GetString(personnelInfo.Rows[0], "InsuranceTypeID", ""));
                //if (Helper.GetInt(InsuranceTypeCode, 0) != 1)
                //    NumOfPersonnel--;
                if (!hasInsurance)
                    NumOfPersonnel--;
                else
                {
                    TotalInsuranceWork = TotalInsuranceWork + Helper.GetFishValue(fishValues, Helper.GetField("WorkDays", ReportFieldType.Insurance), 0);
                    TotalDayWages = TotalDayWages + Helper.GetFishValue(fishValues, Helper.GetField("DayWage", ReportFieldType.Insurance), 0);
                    TotalMonthWage = TotalMonthWage + Helper.GetFishValue(fishValues, Helper.GetField("MonthWage", ReportFieldType.Insurance), 0);
                    TotalBenefits = TotalBenefits + Helper.GetFishValue(fishValues, Helper.GetField("BenefitWage", ReportFieldType.Insurance), 0);
                    TotalInsuranceWage = TotalInsuranceWage + Helper.GetFishValue(fishValues, Helper.GetField("InsuranceWage", ReportFieldType.Insurance), 0);
                    TotalPrice = TotalPrice + Helper.GetFishValue(fishValues, Helper.GetField("TotalPrice", ReportFieldType.Insurance), 0);
                    TotalInsuranced = TotalInsuranced + Helper.GetFishValue(fishValues, Helper.GetField("Insurance7", ReportFieldType.Insurance), 0);
                    TotalEmployer = TotalEmployer + Helper.GetFishValue(fishValues, Helper.GetField("Insurance20", ReportFieldType.Insurance), 0) + Helper.GetFishValue(fishValues, Helper.GetField("Insurance10", ReportFieldType.Insurance), 0);
                    TotalBikari = TotalBikari + Helper.GetFishValue(fishValues, Helper.GetField("Insurance3", ReportFieldType.Insurance), 0);
                }
            }
            //string CompanyInfo = DbProxy.Instance.GetConfig("CompanyCode") + "," +
            //                     DbProxy.Instance.GetConfig("CompanyName") + "," + EmployerName + "," +
            //                     DbProxy.Instance.GetConfig("Address") + ",0," + ddlYear.Text.Substring(2, 2) + "," +
            //                     ddlMonth.SelectedValue + "," + ListNo.Text + ","
            //                     + ListComment.Text + "," + NumOfPersonnel + "," + TotalInsuranceWork + "," +
            //                     TotalDayWages + "," + TotalMonthWage + ",0," + TotalInsuranceWage + "," + TotalPrice +
            //                     "," + TotalInsuranced + "," + TotalEmployer + "," + TotalBikari + ",0,0,0,0";

            List<byte> line = new List<byte>();

            line.AddRange(Encoding.ASCII.GetBytes(WorkShopCode + ","));

            line.AddRange(UnicodeToIranSys.Convert(CompanyName));
            line.AddRange(Encoding.ASCII.GetBytes(","));

            line.AddRange(UnicodeToIranSys.Convert(EmployerName));
            line.AddRange(Encoding.ASCII.GetBytes(","));


            line.AddRange(UnicodeToIranSys.Convert(DbProxy.Instance.GetConfig("Address")));
            line.AddRange(Encoding.ASCII.GetBytes(","));

            line.AddRange(Encoding.ASCII.GetBytes("0,")); // list type

            line.AddRange(Encoding.ASCII.GetBytes(ddlYear.Text.Substring(2, 2) + ","));

            line.AddRange(Encoding.ASCII.GetBytes(ddlMonth.SelectedValue + ","));

            line.AddRange(Encoding.ASCII.GetBytes(ListNo.Text + ","));

            line.AddRange(UnicodeToIranSys.Convert(ListComment.Text));
            line.AddRange(Encoding.ASCII.GetBytes(","));

            line.AddRange(Encoding.ASCII.GetBytes(NumOfPersonnel + ",")); //NUM

            line.AddRange(Encoding.ASCII.GetBytes(TotalInsuranceWork + ",")); // TDD

            line.AddRange(Encoding.ASCII.GetBytes(TotalDayWages + ",")); // TROOZ

            line.AddRange(Encoding.ASCII.GetBytes(TotalMonthWage + ",")); // TMAH

            line.AddRange(Encoding.ASCII.GetBytes(TotalBenefits + ",")); // TMAZ

            line.AddRange(Encoding.ASCII.GetBytes(TotalInsuranceWage + ",")); // TMASH

            line.AddRange(Encoding.ASCII.GetBytes(TotalPrice + ",")); // TTOTL

            line.AddRange(Encoding.ASCII.GetBytes(TotalInsuranced + ",")); // TBIME

            line.AddRange(Encoding.ASCII.GetBytes(TotalEmployer + ",")); // TKOSO

            line.AddRange(Encoding.ASCII.GetBytes(TotalBikari + ",")); // BIC

            line.AddRange(Encoding.ASCII.GetBytes("23,0,0,000")); //RATE, PRATE, BIMH, PYM

            //line.Add((byte)'\r');
            //line.Add((byte)'\n');

            sw.Write(line.ToArray(), 0, line.Count);
            //sw.WriteLine(CompanyInfo);
            mFile.Text += LangProvider.Instance.ParseLang("Lang.CompanyFileCreated") + ": <a href='temp/Insurance/" + folderName + "/" + fileName + "' download='" + fileName + "'>" + LangProvider.Instance.ParseLang("Lang.Download") + "</a><br />";
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
        }
        finally
        {
            if (sw != null)
            {
                sw.Flush();
                sw.Close();
                sw.Dispose();
            }
        }
    }

    private void CreateTXTInsuranceFile(string folder)
    {
        string fileName = "DSKWOR00.txt";
        string file = Path.Combine(folder, fileName);
        if (!file.ToLower().EndsWith(".txt"))
            file += ".txt";
        //StreamWriter sw = new StreamWriter(file, true, Encoding.Unicode);
        //StreamWriter sw = new StreamWriter(file, true, Encoding.ASCII);
        FileStream sw = File.Create(file);
        try
        {
            string qry = "SELECT Fish.ID, PersonnelID, HasInsurance FROM Fish INNER JOIN Personnel ON Fish.PersonnelID=Personnel.ID WHERE FishYear=" + ddlYear.Text + " AND FishMonth=" +
                         ddlMonth.SelectedValue;
            if (Helper.GetInt(ddlSection.SelectedValue, -1) > 0)
            {
                qry += " AND Personnel.WardID=" + ddlSection.SelectedValue;
            }
            DataTable fishs = DbProxy.Instance.FillData(qry, "fishs");
            NumOfPersonnel = fishs.Rows.Count;
            for (int i = 0; i < fishs.Rows.Count; i++)
            {
                string fid = Helper.GetString(fishs.Rows[i], "ID", "");
                bool hasInsurance = Helper.GetBool(fishs.Rows[i], "HasInsurance", false);
                FormulaEvaluater.FishID = int.Parse(fid);
                DataTable fishValues = DbProxy.Instance.FillData("SELECT * FROM FishValuesView WHERE FishID=" + fid
                    , "fishValue");
                DataTable personnelInfo =
                    DbProxy.Instance.FillData(
                        "SELECT * FROM Personnel WHERE ID=" + Helper.GetString(fishs.Rows[i], "PersonnelID", "")
                        , "personnelInfo");
                string InsuranceTypeCode = Helper.GetEnumValuesCode(Helper.GetString(personnelInfo.Rows[0], "InsuranceTypeID", ""));
                //if (Helper.GetInt(InsuranceTypeCode, 0) != 1)
                //    NumOfPersonnel--;
                if (!hasInsurance)
                    NumOfPersonnel--;
                else
                {

                    string IssuePlace = Helper.GetString(DbProxy.Instance.FillScalar("SELECT Title FROM City WHERE ID=" +
                                                    Helper.GetString(personnelInfo.Rows[0], "IssuePlaceID", "-1")), "");
                    string Gender = Helper.GetString(DbProxy.Instance.FillScalar("SELECT Title FROM Gender WHERE ID=" +
                                                    Helper.GetString(personnelInfo.Rows[0], "GenderID", "-1")), "");
                    string JobCode = Helper.GetString(DbProxy.Instance.FillScalar("SELECT Code FROM Job WHERE ID=" +
                                                    Helper.GetString(personnelInfo.Rows[0], "JobID", "-1")), "");
                    string IssueDate =
                        Helper.FormatDate(
                            Convert.ToDateTime(Helper.GetString(personnelInfo.Rows[0], "IssueDate", null)), "yyyyMMdd");
                    string BirthDate =
                        Helper.FormatDate(
                            Convert.ToDateTime(Helper.GetString(personnelInfo.Rows[0], "BirthDate", null)), "yyyyMMdd");
                    string StartDate =
                        Helper.FormatDate(
                            Convert.ToDateTime(Helper.GetString(personnelInfo.Rows[0], "StartDate", null)), "yyyyMMdd");
                    string EndDate =
                        Helper.FormatDate(Convert.ToDateTime(Helper.GetString(personnelInfo.Rows[0], "EndDate", null)),
                            "yyyyMMdd");
                    string Nationality =
                        Helper.GetEnumValuesTitle(Helper.GetString(personnelInfo.Rows[0], "NationalityID", ""));


                    List<byte> line = new List<byte>();

                    line.AddRange(Encoding.ASCII.GetBytes(WorkShopCode + ","));
                    line.AddRange(Encoding.ASCII.GetBytes(ddlYear.Text.Substring(2, 2) + ","));
                    line.AddRange(Encoding.ASCII.GetBytes(ddlMonth.SelectedValue + ","));
                    line.AddRange(Encoding.ASCII.GetBytes(ListNo.Text + ","));
                    line.AddRange(Encoding.ASCII.GetBytes(Helper.GetString(personnelInfo.Rows[0], "InsuranceNo", "") + ","));

                    line.AddRange(UnicodeToIranSys.Convert(Helper.GetString(personnelInfo.Rows[0], "Name", "")));
                    line.AddRange(Encoding.ASCII.GetBytes(","));

                    line.AddRange(UnicodeToIranSys.Convert(Helper.GetString(personnelInfo.Rows[0], "Surname", "")));
                    line.AddRange(Encoding.ASCII.GetBytes(","));

                    line.AddRange(UnicodeToIranSys.Convert(Helper.GetString(personnelInfo.Rows[0], "Father", "")));
                    line.AddRange(Encoding.ASCII.GetBytes(","));

                    line.AddRange(Encoding.ASCII.GetBytes(Helper.GetString(personnelInfo.Rows[0], "IDCardNo", "") + ","));


                    line.AddRange(UnicodeToIranSys.Convert(IssuePlace));
                    line.AddRange(Encoding.ASCII.GetBytes(","));

                    line.AddRange(Encoding.ASCII.GetBytes(IssueDate + ","));



                    line.AddRange(UnicodeToIranSys.Convert(Gender));
                    line.AddRange(Encoding.ASCII.GetBytes(","));

                    line.AddRange(UnicodeToIranSys.Convert(Nationality));
                    line.AddRange(Encoding.ASCII.GetBytes(","));

                    line.AddRange(Encoding.ASCII.GetBytes(",")); //OCP

                    line.AddRange(Encoding.ASCII.GetBytes(StartDate + ","));
                    line.AddRange(Encoding.ASCII.GetBytes(EndDate + ","));
                    line.AddRange(Encoding.ASCII.GetBytes(Helper.GetFishValue(fishValues, Helper.GetField("WorkDays", ReportFieldType.Insurance), 0) + ","));//DD
                    line.AddRange(Encoding.ASCII.GetBytes(Helper.GetFishValue(fishValues, Helper.GetField("DayWage", ReportFieldType.Insurance), 0) + ","));//ROOZ
                    line.AddRange(Encoding.ASCII.GetBytes(Helper.GetFishValue(fishValues, Helper.GetField("MonthWage", ReportFieldType.Insurance), 0) + ",")); //MAH
                    line.AddRange(Encoding.ASCII.GetBytes(Helper.GetFishValue(fishValues, Helper.GetField("BenefitWage", ReportFieldType.Insurance), 0) + ",")); // MAZ
                    line.AddRange(Encoding.ASCII.GetBytes(Helper.GetFishValue(fishValues, Helper.GetField("InsuranceWage", ReportFieldType.Insurance), 0) + ",")); //MASH
                    line.AddRange(Encoding.ASCII.GetBytes(Helper.GetFishValue(fishValues, Helper.GetField("TotalPrice", ReportFieldType.Insurance), 0) + ",")); // TOTL
                    line.AddRange(Encoding.ASCII.GetBytes(Helper.GetFishValue(fishValues, Helper.GetField("Insurance7", ReportFieldType.Insurance), 0) + ",")); //BIME
                    line.AddRange(Encoding.ASCII.GetBytes("0,")); //PRATE
                    line.AddRange(Encoding.ASCII.GetBytes(JobCode + ",")); //JOB
                    line.AddRange(Encoding.ASCII.GetBytes(BirthDate + ","));
                    line.AddRange(Encoding.ASCII.GetBytes(Helper.GetString(personnelInfo.Rows[0], "NatinoalCode", ""))); //NATCOD

                    if (i < fishs.Rows.Count - 1)
                    {
                        line.Add((byte)'\r');
                        line.Add((byte)'\n');
                    }

                    sw.Write(line.ToArray(), 0, line.Count);
                    //sw.WriteLine(WorkerInfo);
                }
            }
            mFile.Text += LangProvider.Instance.ParseLang("Lang.InsuranceFileCreated") + ": <a href='temp/Insurance/" + folderName + "/" + fileName + "' download='" + fileName + "'>" + LangProvider.Instance.ParseLang("Lang.Download") + "</a><br />";
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
        }
        finally
        {
            if (sw != null)
            {
                sw.Flush();
                sw.Close();
                sw.Dispose();
            }
        }
    }
}