﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class storeuserserviceprices : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (SiteSettings.User == null)
            Response.Redirect("default.aspx?do=logout");
        BindGrid();
    }

    void BindGrid()
    {
        mH.Controls.Clear();
        string query = string.Format(@"SELECT ServicePriceView.ID, ServiceFullName, TheLPSP, Service.Enabled FROM ServicePriceView 
    INNER JOIN Service ON ServicePriceView.ServiceID=Service.ID 
    WHERE ServicePriceView.ID
         IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (SELECT * FROM [dbo].[GetAllGroupingChilds] (ISNULL((SELECT RelatedGroupID FROM Users WHERE ID = {0}), -1))))"
           , SiteSettings.UserID);
        DataTable dt = DbProxy.Instance.FillData(query, "oh");
        if (dt == null)
            return;
        StringBuilder builder = new StringBuilder();
        builder.AppendLine("<table class='table-with-border awesome-table' >");
        builder.AppendLine("<thead><tr class='th'>");
        builder.AppendFormat("<th class='width-10  align-center'>#</th><th class='width-150  align-center'>{0}</th><th class='width-150  align-center'>{1}</th><th class='width-150  align-center'>{2}</th><th class='width-50  align-center'>{3}</th>"
            , LangProvider.Instance["Service"], LangProvider.Instance["CurPrice"], LangProvider.Instance["NewPrice"], LangProvider.Instance["Enabled"]);
        builder.AppendLine("</tr></thead>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            int id = Helper.GetInt(dt.Rows[i], "ID", 0);
            builder.AppendLine("<tr>");
            builder.AppendFormat("<td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td>"
                , i + 1
                , Helper.GetString(dt.Rows[i], "ServiceFullName", "")
                , Helper.GetDecimal(dt.Rows[i], "TheLPSP", 0).ToString("N2", Helper.NumberCulture).Replace(".00", "")
                , string.Format("<input type='text' name='p{0}' />", id)
                , string.Format("<input type='checkbox' name='e{0}' {1} value='1' />", id, Helper.GetBool(dt.Rows[i], "Enabled", true) ? "checked='checked'" : "")
                );
            builder.AppendLine("</tr>");
        }
        builder.AppendLine("</table>");
        dt.Dispose();

        mH.Controls.Add(new LiteralControl(builder.ToString()));
    }
    protected void doSavePrice_Click(object sender, EventArgs e)
    {
        string query = string.Format(@"SELECT ServicePriceView.ID, ServiceFullName, TheLPSP, Service.Enabled FROM ServicePriceView 
    INNER JOIN Service ON ServicePriceView.ServiceID=Service.ID 
    WHERE ServicePriceView.ID
         IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (SELECT * FROM [dbo].[GetAllGroupingChilds] (ISNULL((SELECT RelatedGroupID FROM Users WHERE ID = {0}), -1))))"
           , SiteSettings.UserID);
        DataTable dt = DbProxy.Instance.FillData(query, "oh");
        if (dt == null)
            return;
        int done = 0;
        using (DbProxy proxy = new DbProxy())
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int id = Helper.GetInt(dt.Rows[i], "ID", 0);
                double newPrice = Helper.GetDouble(Request["p" + id], double.NaN);
                bool newEnabled = Request["e" + id] == "1";
                bool enabled = Helper.GetBool(dt.Rows[i], "Enabled", true);
                if (double.IsNaN(newPrice) && newEnabled == enabled)
                    continue;
                string update = "";
                if (!double.IsNaN(newPrice))
                    update += string.Format("UPDATE ServicePrice SET TheLPSP={0} WHERE ID={1};", newPrice, id);
                if (newEnabled != enabled)
                    update += string.Format("UPDATE Service SET Enabled={0} WHERE ID IN (SELECT ServiceID FROM ServicePrice WHERE ServicePrice.ID={1});", newEnabled ? 1 : 0, id);
                proxy.ExecuteCommand(update);
                done++;
            }
        }
        BindGrid();
        mMsg.Value = string.Format(LangProvider.Instance["Saved"] + " " + done + " " + LangProvider.Instance["Service"]);
    }
}