﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class webapi_AdvertisementImage_AdvertisementImageDelete : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // The response type is json
        Response.ContentType = "application/json";

        int id = -1;
        
        // If we don't have an integer ID, throw an error
        if (!int.TryParse(Page.RouteData.Values["ID"].ToString(), out id))
        {
            Response.StatusCode = (int) HttpStatusCode.BadRequest;
            Response.Write(Toolbelt.ResponseError("Parameter ID must be an integer!"));
            Response.End();
        }

        int advertisementId = -1;

        // If we don't have an integer ID, throw an error
        if (!int.TryParse(Page.RouteData.Values["AdvertisementID"].ToString(), out advertisementId))
        {
            Response.StatusCode = (int) HttpStatusCode.BadRequest;
            Response.Write(Toolbelt.ResponseError("Parameter AdvertisementID must be an integer!"));
            Response.End();
        }

        string query = "Delete from AdvertisementImage where ID = {0} and AdvertisementID = {1}";
        query = String.Format(query, id, advertisementId);

        if (DbProxy.Instance.RunQuery(query) != -1)
        {
            Response.Write(Toolbelt.ResponseSuccess("Deleted successfully"));
        }
        else
        {
            Response.Write(Toolbelt.ResponseError("AdvertisementImage was not deleted"));
        }
        Response.End();
    }
}