﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

public partial class webapi_AdvertisementImage_AdvertisementImageList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // The response type is json
        Response.ContentType = "application/json";

        int id = -1;
        
        // If we don't have an integer ID, throw an error
        if (!int.TryParse(Page.RouteData.Values["ID"].ToString(), out id))
        {
            Response.StatusCode = (int) HttpStatusCode.BadRequest;
            Response.Write(Toolbelt.ResponseError("Parameter ID must be an integer!"));
            Response.End();
        }

        Response.Write(GetJson(id));
        Response.End();
    }

    private string GetJson(int id)
    {
        string table = "AdvertisementImage";
        string q = "advertisementimage";
        TableInfo info = null;
        if (null != table)
        {
            info = TableController.Instance[table];
        }
        if (info == null)
        {
            return null;
        }
        int page = Helper.GetInt(HttpContext.Current.Request.Form["page"], 1);
        int rp = Helper.GetInt(HttpContext.Current.Request.Form["rp"], 10000);
        string sortname = Helper.GetString(HttpContext.Current.Request.Form["sortname"], "ID");
        //key1 , for do order by on query which returns data for grids print & excel
        string orderby = Helper.GetString(HttpContext.Current.Request.QueryString["orderby"], "");
        if (orderby != "")
        {
            sortname = orderby;
        }

        string select = "*", from = table, w = "";
        if (!string.IsNullOrEmpty(q))
        {
            QueryInfo qInfo = QueryController.Instance[q];
            if (qInfo != null)
            {
                if (!string.IsNullOrEmpty(qInfo.From))
                    from = qInfo.From.Replace("@Table", table);
                if (!string.IsNullOrEmpty(qInfo.Select))
                    select = qInfo.Select;
                if (!string.IsNullOrEmpty(qInfo.Where))
                    w = qInfo.Where.Replace("_PersonnelID_", SiteSettings.User.PersonnelID.ToString())
                        .Replace("_UserID_", SiteSettings.UserID.ToString())
                        .Replace("_IsAdmin_", SiteSettings.User.IsAdmin ? "1" : "0");
            }
        }

        string whereCondition = "";
        whereCondition = "Where AdvertisementID = " + id;

        string more = HttpContext.Current.Request.QueryString["more"];
        if (!string.IsNullOrEmpty(more))
        {
            whereCondition = (string.IsNullOrEmpty(whereCondition) ? " WHERE " : (whereCondition + " AND ")) + SiteSettings.ReplaceOptions(more);
        }
        if (!string.IsNullOrEmpty(w))
        {
            whereCondition = (string.IsNullOrEmpty(whereCondition) ? " WHERE " : (whereCondition + " AND ")) + w;
        }
        string sortorder = Helper.GetString(HttpContext.Current.Request.Form["sortorder"], "asc");
        string sortExp = (sortname + " " + sortorder).Trim();
        if (sortname != null && sortname.EndsWith("_cast2int"))
        {
            sortExp = ("CAST(" + sortname.Substring(0, sortname.Length - "_cast2int".Length) + " AS INT) " + sortorder).Trim();
        }
        int start = ((page - 1) * rp);

       
        string totalQry = string.Format("SELECT COUNT(*) FROM {0} {1}", from, whereCondition);
        int total = Helper.GetInt(DbProxy.Instance.FillScalar(totalQry), 0);
        string qry;
        if (select.ToLower().Contains("zeilenummer"))
        {
            string select2 = select.Replace("ZeileNummer,", "");
            qry = string.Format(@"
WITH TableRows AS 
(
	SELECT ROW_NUMBER() OVER (ORDER BY {0}) AS ZeileNummer,
		{7} FROM {2}
	{3}
)
SELECT {1} FROM TableRows WHERE ZeileNummer between {4} And {5} ORDER BY {6}"
   , sortExp, select, from, whereCondition, start + 1, start + rp, string.IsNullOrEmpty(sortExp) ? "ID" : sortExp, select2);
        }
        else
            qry = string.Format(@"
WITH TableRows AS 
(
	SELECT ROW_NUMBER() OVER (ORDER BY {0}) AS ZeileNummer,
		{1} FROM {2}
	{3}
)
SELECT {1} FROM TableRows WHERE ZeileNummer between {4} And {5} ORDER BY {6}"
            , sortExp, select, from, whereCondition, start + 1, start + rp, string.IsNullOrEmpty(sortExp) ? "ID" : sortExp);
        DataTable dt = DbProxy.Instance.FillData(qry, "AdvertisementImage");

        return Toolbelt.SerializeDataTable(dt);
    }
}