﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Demo mode
        panelDemo.Visible = false; // make it false

        if (Request.QueryString["do"] != "logout" && Request.QueryString["do"] != "vacation" && SiteSettings.UserID > 0 && !SiteSettings.User.IsStoreUser)
        {
            Response.Redirect("welcome.aspx");
        }
        else if (Request.QueryString["do"] != "logout" && Request.QueryString["do"] != "vacation" && SiteSettings.UserID > 0 && SiteSettings.User.IsStoreUser)
        {
            Response.Redirect("StoreUserWelcome.aspx");
        }
        //Response.Redirect("list.aspx");
        if (Request.QueryString["do"] == "logout")
        {
            if (Request.QueryString["backup"] == "true")
            {
                try
                {
                    SqlAdmin sqlAdmin = new SqlAdmin();
                    if (sqlAdmin.BackupHisDB())
                        ShowMessage(LangProvider.Instance["BackupDone"]);
                    else
                    {
                        ShowMessage(LangProvider.Instance["BackupError"] + ": <br />", "danger");
                        Exception ee = sqlAdmin.LastException;
                        while (ee != null)
                        {
                            ShowMessage(ee.Message + "<br />", "danger");
                            ee = ee.InnerException;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ShowMessage(LangProvider.Instance["BackupError"] + ": <br />" + ex.Message, "danger");
                }
            }
            DbProxy.Instance.RunQuery(string.Format("INSERT INTO LogChange (UserID, LogDate, Type, ShortData) VALUES ({0}, GETDATE(), 1, '')", SiteSettings.UserID));
            DbProxy.Instance.RunQuery("UPDATE DocumentAccounting SET LockerID=NULL ,Status=0 WHERE Status=1 AND LockerID=" + SiteSettings.UserID);

            SiteSettings.UserID = -1;
            SiteSettings.Lang = null;
        }
        else if (Request.QueryString["do"] == "vacation")
        {
            using (DbProxy proxy = new DbProxy())
            {
                proxy.RunQuery(string.Format("INSERT INTO LogChange (UserID, LogDate, Type, ShortData) VALUES ({0}, GETDATE(), 1, '')", SiteSettings.UserID));
                proxy.RunQuery("UPDATE DocumentAccounting SET LockerID=NULL ,Status=0 WHERE Status=1 AND LockerID=" + SiteSettings.UserID);
                proxy.UpdateUserOnVacation(SiteSettings.UserID, true);
            }
            SiteSettings.UserID = -1;
            SiteSettings.Lang = null;
        }
        if (!IsPostBack)
        {
            BindLangs();
            BindDBs();
            mUsernamelbl.Text = LangProvider.Instance["Username"];
            mPasswordlbl.Text = LangProvider.Instance["Password"];
            mDBslbl.Text = LangProvider.Instance["DataBase"];
            mLangslbl.Text = LangProvider.Instance["Language"];
            doLogin.Text = LangProvider.Instance["Login"];
        }
    }
    void BindLangs()
    {
        mLangs.DataSource = LangProvider.GetLangs();
        mLangs.DataBind();
        try
        {
            mLangs.SelectedValue = DbProxy.Instance.GetConfig("Language", "fa");
        }
        catch { }
    }
    void BindDBs()
    {
        mDBs.Items.Clear();
        mDBs.Items.Add("");
        try
        {
            string path = Server.MapPath("~/files/fdb");
            if (System.IO.Directory.Exists(path))
            {
                string[] files = System.IO.Directory.GetFiles(path, "DB_*.mdf");
                for (int i = 0; i < files.Length; i++)
                {
                    string fileName = System.IO.Path.GetFileNameWithoutExtension(files[i]);
                    if (!fileName.ToLower().EndsWith("_log"))
                        mDBs.Items.Add(fileName);
                }
            }
        }
        catch { }
    }
    protected void doLogin_Click(object sender, EventArgs e)
    {
        if (TempUserID == Null.NullInteger)
            PerformLogin();
        else
            PerformActivation();
    }
    void PerformLogin()
    {
        UserInfo info = null;
        SiteSettings.OtherCatalogName = mDBs.SelectedValue;
        if (panelDemo.Visible)
            SiteSettings.DemoLevel = Helper.GetInt(mDemoLevel.SelectedValue, SiteSettings.DemoLevel);
        else
            SiteSettings.DemoLevel = 100;
        using (DbProxy proxy = new DbProxy())
        {
            info = proxy.GetUser(mUsername.Text, mPassword.Text);
            if (info == null)
            {
                if (!proxy.Connected)
                {
                    ShowMessage("ErrorConnectingToDataBase".Translate(), "danger");
                    if (proxy.LastException != null)
                        ShowMessage("<br />" + proxy.LastException.Message, "danger");
                }
                else
                    ShowMessage("UsernameOrPasswordIsIncorrect".Translate(), "danger");
            }
            else
            {
                DateTime startTime = Convert.ToDateTime(info.StartTime);
                DateTime endTime = Convert.ToDateTime(info.EndTime);
                DateTime timeNow = DateTime.Now;
                if (info.Enabled == false)
                    ShowMessage("ThisUserHasBeenDisabledContactTheAdmin".Translate(), "danger");
                else if (!string.IsNullOrEmpty(info.EndTime) && !string.IsNullOrEmpty(info.StartTime) && (timeNow < startTime || timeNow > endTime))
                    ShowMessage("OutOfTimeLogin".Translate(), "danger");
                else
                {
                    // Vacation is canceled on first login
                    if (info.OnVacation)
                        proxy.UpdateUserOnVacation(info.ID, false);
                    // Check Activation
                    CheckActivation(info.ID);
                }
            }
        }
    }
    void CheckActivation(int userID)
    {
        // Check Activation
        int maxVouchers = SiteSettings.MaxAllowedVouchers;
        if (maxVouchers == int.MaxValue)
            FinalizeLogin(userID);
        else
        {
            int numVouchers = Helper.GetInt(DbProxy.Instance.FillScalar("SELECT COUNT(ID) FROM DocumentAccounting"), 0);
            if (numVouchers <= maxVouchers)
                FinalizeLogin(userID);
            else
            {
                mUsernamelbl.Text = LangProvider.Instance["PcCode"];
                mPasswordlbl.Text = LangProvider.Instance["Activation"];
                doLogin.Text = LangProvider.Instance["Login"];

                panelDBs.Visible = false;
                panelLanguage.Visible = false;

                FingerPrint fp = new FingerPrint();
                mUsername.Text = fp.PCCode;
                mUsername.ReadOnly = true;
                mPassword.Text = "";
                mPassword.TextMode = TextBoxMode.SingleLine;
                mPassword.MaxLength = 29;
                TempUserID = userID;
                if (numVouchers > SiteSettings.MaxVouchersAct2)
                {
                    // needs lock
                    ShowMessage(string.Format(LangProvider.Instance["VouchersExceeded3"], maxVouchers), "danger");
                    panelMainBox.Visible = false;
                    panelMainBox.Visible = false;
                }
                else if (numVouchers > SiteSettings.MaxVouchersAct1)
                    // needs activation 2
                    ShowMessage(string.Format(LangProvider.Instance["VouchersExceeded2"], maxVouchers), "danger");
                else
                    // needs activation 1
                    ShowMessage(string.Format(LangProvider.Instance["VouchersExceeded1"], maxVouchers), "danger");
            }
        }
    }
    void FinalizeLogin(int userID)
    {
        SiteSettings.InitDongle();
        SiteSettings.UserID = userID;
        SiteSettings.Lang = mLangs.SelectedValue;
        Response.Cookies.Add(new HttpCookie("numcartable", "0"));
        Response.Cookies.Add(new HttpCookie("numLoans", "0"));
        Response.Cookies.Add(new HttpCookie("numChecks", "0"));
        Response.Cookies.Add(new HttpCookie("numStoreDraft", "0"));
        DbProxy.Instance.RunQuery(string.Format("INSERT INTO LogChange (UserID, LogDate, Type, ShortData) VALUES ({0}, GETDATE(), 0, '')", userID));
        DbProxy.Instance.RunQuery("UPDATE DocumentAccounting SET LockerID=NULL ,Status=0 WHERE Status=1 AND LockerID=" + SiteSettings.UserID);
        if (SiteSettings.User.IsStoreUser)
        {
            Response.Redirect("StoreUserWelcome.aspx");
        }
        else
        {
            if (SiteSettings.HasCardex())
                Response.Redirect("list.aspx?table=checks&view=kardex");
            else
                Response.Redirect("welcome.aspx");
        }
    }
    void PerformActivation()
    {
        string activation = mPassword.Text;
        if (string.IsNullOrEmpty(activation))
            return;
        FingerPrint fp = new FingerPrint();
        if (fp.Validate(activation) || fp.Validate2(activation))
            new DbProxy().SetConfig("Activation", activation, -1, false);
        CheckActivation(TempUserID);
    }
    protected int TempUserID
    {
        get
        {
            return Helper.GetInt(ViewState["TempUserID"], Null.NullInteger);
        }
        set
        {
            ViewState["TempUserID"] = value;
        }
    }

    protected void ShowMessage(string msg, string type = "success")
    {
        mMsg.Text += msg;
        panelMessage.CssClass = "alert alert-" + type;
        panelMessage.Visible = true;
    }
}