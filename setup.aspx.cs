﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class setup : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FingerPrint f = new FingerPrint();
        string s = f.PCCode;
        string d = f.ActivationCode;
        //Response.Redirect("list.aspx");
        mMsg.Text = "";
        if (!IsPostBack)
        {
            BindDBs();
        }
    }
    void BindDBs()
    {

        mDBs.Items.Clear();
        mDBs.Items.Add("");
        try
        {
            //ServerName, InstanceName, IsClustered, Version
            DataTable dt = System.Data.Sql.SqlDataSourceEnumerator.Instance.GetDataSources();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string instance = Helper.GetString(dt.Rows[i], "InstanceName", "");
                if (string.IsNullOrEmpty(instance))
                    mDBs.Items.Add(dt.Rows[i]["ServerName"].ToString());
                else
                    mDBs.Items.Add(dt.Rows[i]["ServerName"] + "\\" + dt.Rows[i]["InstanceName"]);
            }
        }
        catch { }
    }
    protected void doTest_Click(object sender, EventArgs e)
    {
        // Driver={SQL Native Client};Server=.\SQLExpress;AttachDbFilename=c:\asd\qwe\mydbfile.mdf; Database=dbname;Trusted_Connection=Yes;
        string server = mServer.Text;
        if (mDBs.SelectedValue != "")
            server = mDBs.SelectedValue;
        if (server.Trim().Length == 0)
        {
            mMsg.Text = "ServerHasNotBeenChosen".Translate();
            return;
        }
        string cntStr = string.Format("Server={0};AttachDbFilename={1}; Database=his;Integrated Security=SSPI;User Instance=true"//uid=pa;pwd=1234;
            , server, Server.MapPath("~/app_data/his.mdf"));
        if (mUsername.Text.Trim().Length > 0)
            cntStr = string.Format("Server={0};AttachDbFilename={1}; Database=his;uid={2};pwd={3}"//uid=pa;pwd=1234;
            , server, Server.MapPath("~/app_data/his.mdf"), mUsername.Text, mPass.Text);
        try
        {
            SqlConnection cnt = new SqlConnection(cntStr);
            cnt.Open();
            int numUsers = 0;
            using (SqlCommand command = new SqlCommand("SELECT COUNT(*) FROM Users", cnt))
                numUsers = Helper.GetInt(command.ExecuteScalar(), 0);
            if (numUsers > 0)
            {
                mMsg.Text = "ServerFound".Translate();
            }
        }
        catch (Exception ex)
        {
            mMsg.Text = "خطا: " + ex.Message;
        }
        //UserInfo info = null;
        //SiteSettings.OtherCatalogName = mDBs.SelectedValue;
        //using (DbProxy proxy = new DbProxy())
        //{
        //    info = proxy.GetUser(mUsername.Text, mPassword.Text);
        //    if (info == null)
        //    {
        //        if (!proxy.Connected)
        //        {
        //            mMsg.Text = "خطا در ارتباط با دیتابیس";
        //            if (proxy.LastException != null)
        //                mMsg.Text += "<br />" + proxy.LastException.Message;
        //        }
        //        else
        //            mMsg.Text = "نام کاربری یا رمز عبور اشتباه می باشد";
        //    }
        //    else if (info.Enabled == false)
        //        mMsg.Text = "این کاربر غیرفعال شده است. با مدیر برنامه تماس حاصل فرمائید";
        //    else
        //    {
        //        SiteSettings.UserID = info.ID;
        //        SiteSettings.Lang = mLangs.SelectedValue;
        //        proxy.RunQuery(string.Format("INSERT INTO LogChange (UserID, LogDate, Type, ShortData) VALUES ({0}, GETDATE(), 0, '')", info.ID));
        //        if (SiteSettings.HasCardex())
        //            Response.Redirect("list.aspx?table=checks&view=kardex");
        //        else
        //            Response.Redirect("welcome.aspx");
        //    }
        //}
    }
}