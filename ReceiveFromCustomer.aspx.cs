﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ReceiveFromCustomer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.SetLang();
        mMsg.Value = "";
        mCurTab.Value = "";
        mDate.Text = Helper.FormatDate(DateTime.Now, null);
    }
    protected void doSave_Click(object sender, EventArgs e)
    {
        int userID = Helper.GetInt(UserID.Text, 0);
        if (userID > 0)
        {
            int num = SaveReceiveBank(userID);
            num += SaveReceiveChecks(userID);
            num += SaveReceiveCash(userID);
            mMsg.Value = string.Format(LangProvider.Instance["StartChecksSaved"], num);
            mCurTab.Value = "check";
        }
    }
    private int SaveReceiveChecks(int userID)
    {
        
        string[] Serials = Helper.GetString(Request["Serial"], "").Split(',');
        string[] OwnerCheckNames = Helper.GetString(Request["OwnerCheckName"], "").Split(',');
        string[] BankAccountIDs = Helper.GetString(Request["BankAccountID"], "").Split(',');
        string[] Branchs = Helper.GetString(Request["Branch"], "").Split(',');
        string[] AccountNos = Helper.GetString(Request["AccountNo"], "").Split(',');
        string[] IssueDates = Helper.GetString(Request["IssueDate"], "").Split(',');
        string[] DueDates = Helper.GetString(Request["DueDate"], "").Split(',');
        string[] Amounts = Helper.GetString(Request["Amount"], "").Split(',');
        string[] CheckComments = Helper.GetString(Request["CheckComment"], "").Split(',');
        DbProxy proxy = new DbProxy();
        proxy.RunQuery("UPDATE ReceiveFromCustomerChecks SET Deleted = 1 WHERE UserID =" + userID);
        int num = 0;
        for (int i = 0; i < Serials.Length; i++)
        {
            string serial = Serials[i];
            if (string.IsNullOrEmpty(serial))
                continue;
            int bankAccountID = Helper.GetInt(BankAccountIDs[Math.Min(i, BankAccountIDs.Length - 1)], -1);
            decimal amount = Helper.GetDecimal(Amounts[Math.Min(i, Amounts.Length - 1)], 0);
            DateTime issueDate = Helper.ParseDate(IssueDates[Math.Min(i, IssueDates.Length - 1)], new DateTime());
            DateTime dueDate = Helper.ParseDate(DueDates[Math.Min(i, DueDates.Length - 1)], new DateTime());
            string checkcomment = CheckComments[i];
            string branch = Branchs[i];
            string ownerCheckName = OwnerCheckNames[i];
            string accountNo = AccountNos[i];
            if (amount > 0 && bankAccountID > 0)
            {
                string script = @"
                INSERT INTO ReceiveFromCustomerChecks (Serial, OwnerCheckName, Branch, AccountNo, BankAccountID, IssueDate, DueDate, Amount, Deleted, UserID, CheckComment) 
                VALUES({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10});SELECT SCOPE_IDENTITY()";
                script = string.Format(script, Helper.QoutedString(serial), Helper.QoutedString(ownerCheckName), Helper.QoutedString(branch), Helper.QoutedString(accountNo), bankAccountID,
                    Helper.QoutedDate(issueDate), Helper.QoutedDate(dueDate)
                    , amount, 0, userID, Helper.QoutedString(checkcomment));
                int checkID = Helper.GetInt(proxy.FillScalar(script), -1);
                if (checkID > 0)
                    num++;
            }
        }
        proxy.RunQuery("DELETE FROM ReceiveFromCustomerChecks WHERE Deleted = 1 AND UserID =" + userID);
        proxy.Dispose();
        return num;
    }
    private int SaveReceiveCash(int userID)
    {
        string[] CashComments = Helper.GetString(Request["CashComment"], "").Split(',');
        string[] CashTreePaths = Helper.GetString(Request["CashTreePath"], "").Split(',');
        string[] CashAmounts = Helper.GetString(Request["CashAmount"], "").Split(',');
        DbProxy proxy = new DbProxy();
        proxy.RunQuery("UPDATE ReceiveFromCustomerCash SET Deleted = 1 WHERE UserID =" + userID);
        int num = 0;
        for (int i = 0; i < CashTreePaths.Length; i++)
        {
            string cashtreepath = CashTreePaths[i];
            if (string.IsNullOrEmpty(cashtreepath))
                continue;
            decimal cashamount = Helper.GetDecimal(CashAmounts[Math.Min(i, CashAmounts.Length - 1)], 0);
            string cashcomment = CashComments[i];
            if (cashamount > 0)
            {
                string script = @"
                INSERT INTO ReceiveFromCustomerCash (CashAmount, CashTreePath, CashComment, Deleted, UserID) 
                VALUES({0}, {1}, {2}, {3}, {4});SELECT SCOPE_IDENTITY()";
                script = string.Format(script, cashamount, Helper.QoutedString(cashtreepath), Helper.QoutedString(cashcomment), 0, userID);
                int cashID = Helper.GetInt(proxy.FillScalar(script), -1);
                if (cashID > 0)
                    num++;
            }
        }
        proxy.RunQuery("DELETE FROM ReceiveFromCustomerCash WHERE Deleted = 1 AND UserID =" + userID);
        proxy.Dispose();
        return num;
    }
    private int SaveReceiveBank(int userID)
    {
        string[] BankComments = Helper.GetString(Request["BankComment"], "").Split(',');
        string[] BankTreePaths = Helper.GetString(Request["BankTreePath"], "").Split(',');
        string[] BankAmounts = Helper.GetString(Request["BankAmount"], "").Split(',');
        DbProxy proxy = new DbProxy();
        proxy.RunQuery("UPDATE ReceiveFromCustomerBank SET Deleted = 1 WHERE UserID =" + userID);
        int num = 0;
        for (int i = 0; i < BankTreePaths.Length; i++)
        {
            string banktreepath = BankTreePaths[i];
            if (string.IsNullOrEmpty(banktreepath))
                continue;
            decimal bankamount = Helper.GetDecimal(BankAmounts[Math.Min(i, BankAmounts.Length - 1)], 0);
            string bankcomment = BankComments[i];
            if (bankamount > 0)
            {
                string script = @"
                INSERT INTO ReceiveFromCustomerBank (BankAmount, BankTreePath, BankComment, Deleted, UserID) 
                VALUES({0}, {1}, {2}, {3}, {4});SELECT SCOPE_IDENTITY()";
                script = string.Format(script, bankamount, Helper.QoutedString(banktreepath), Helper.QoutedString(bankcomment), 0, userID);
                int bankID = Helper.GetInt(proxy.FillScalar(script), -1);
                if (bankID > 0)
                    num++;
            }
        }
        proxy.RunQuery("DELETE FROM ReceiveFromCustomerBank WHERE Deleted = 1 AND UserID =" + userID);
        proxy.Dispose();
        return num;
    }
    protected void doApply_Click(object sender, EventArgs e)
    {
        doSave_Click(sender, e);
        int voucherId = -1;
        int customerId = -1;
        int userId = Helper.GetInt(UserID.Text, 0);
        decimal discountAmount = Helper.GetDecimal(Discount.Value, 0);
          DbProxy proxy = new DbProxy();
          customerId = Helper.GetInt(CustomerID.Value, -1);
          voucherId = Helper.GetInt(DocumentAccountingID.Value, -1);
        DateTime date = Helper.ParseDate(mDate.Text, new DateTime());
        if (customerId > 0 && userId > 0)
        {
            int fincanceYearId = Helper.GetInt(proxy.FillScalar("SELECT ID FROM FinanceYear WHERE Status=2"), -1);
            if (fincanceYearId == -1)
            {
                mMsg.Value = LangProvider.Instance["NoFinanceYear"];
            }
            else
            {
                if (voucherId > 0)
                {
                    proxy.RunQuery(String.Format("EXEC ReceiveFromCustomerDocument {0}, {1}, {2}, {3}, {4}", voucherId, customerId, date, userId, discountAmount));
                }
                else
                {
                    voucherId = Helper.GetInt(proxy.FillScalar(@"
                    DECLARE @VoucherID int;
	                    INSERT INTO DocumentAccounting(FinanceYearID, [No], Mode, OtherID, Date, UserID, Status, DoComments, ProgramID) 
			                    VALUES (@FinanceYearID, dbo.[NeuVoucherNo] (NULL, @FinanceYearID, NULL, NULL), 0, NULL, GETDATE(), NULL, 0, N'سند دریافت', NULL)
	                    SET @VoucherID = SCOPE_IDENTITY()
                    SELECT @VoucherID;".Replace("@FinanceYearID", fincanceYearId.ToString())), -1);

                    if (voucherId > 0)
                        proxy.RunQuery(String.Format("EXEC ReceiveFromCustomerDocument {0}, {1}, {2}, {3}, {4}", voucherId, customerId, Helper.QoutedDate(date), userId, discountAmount));
                }
               string script = @"DELETE FROM ReceiveFromCustomerChecks WHERE UserID = {1}
                                DELETE FROM ReceiveFromCustomerBank WHERE UserID = {1}
                                DELETE FROM ReceiveFromCustomerCash WHERE UserID = {1}
                                -- Update Totals
                                DECLARE @TotalOwe NUMERIC(18,4), @TotalDue NUMERIC(18,4)
                                SELECT @TotalOwe = SUM(ISNULL(Owe, 0)), @TotalDue = SUM(ISNULL(Due, 0))
                                FROM DocumentAccountingItems WHERE DocumentAccountingID = {0}; 
                                UPDATE DocumentAccounting SET TotalDue = @TotalDue, TotalOwe = @TotalOwe WHERE ID = {0};";
                script = string.Format(script, voucherId, userId);
                proxy.RunQuery(script);
                NewDocumentAccountingID.Value = voucherId.ToString();
            }

        }
        if (customerId <= 0)
        {
            CustomerName.Attributes["class"] = "form-control input-sm customername red1";
            CustomerName.Attributes["placeholder"] = "لطفا نام مشتری را انتخاب کنید";
        }
    }
}