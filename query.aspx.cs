﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class query : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (SiteSettings.User == null || !SiteSettings.User.HasAccess("query"))
            Response.Redirect("~/default.aspx");
        mMsg.Text = "";
    }
    protected void doQuery_Click(object sender, EventArgs e)
    {
        try
        {
            using (DbProxy proxy = new DbProxy())
            {
                DataSet ds = proxy.FillDataSet(mQuery.Text);
                if (ds == null || ds.Tables.Count == 0)
                    mMsg.Text += proxy.LastException.Message;
                else
                {
                    mGrid.DataSource = ds.Tables[0];
                    mGrid.DataBind();
                    GridView1.DataSource = GridView2.DataSource = GridView3.DataSource = GridView4.DataSource = null;
                    if (ds.Tables.Count > 1)
                        GridView1.DataSource = ds.Tables[1];
                    if (ds.Tables.Count > 2)
                        GridView2.DataSource = ds.Tables[2];
                    if (ds.Tables.Count > 3)
                        GridView3.DataSource = ds.Tables[3];
                    if (ds.Tables.Count > 4)
                        GridView4.DataSource = ds.Tables[4];
                    GridView1.DataBind();
                    GridView2.DataBind();
                    GridView3.DataBind();
                    GridView4.DataBind();
                    ds.Dispose();
                }
            }
        }
        catch (Exception ex)
        {
            //mMsg.Text += DbProxy.LastException + "<br>";
            mMsg.Text += ex.Message + "<br>" + ex.StackTrace;
        }
    }
    protected void doUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            int affected = DbProxy.Instance.RunQuery(mQuery.Text);
            mMsg.Text += "Num affected : " + affected.ToString() + "<br>";
        }
        catch (Exception ex)
        {
            mMsg.Text += ex.Message + "<br>" + ex.StackTrace;
        }
    }
}