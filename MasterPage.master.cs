﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class reports_accounting_MasterPage : System.Web.UI.MasterPage, IMessageBox
{
    public event EventHandler OkClick;
    protected void Page_Load(object sender, EventArgs e)
    {
        mMessageBox.OkClick += new EventHandler(mMessageBox_OkClick);
    }

    void mMessageBox_OkClick(object sender, EventArgs e)
    {
        if (OkClick != null)
            OkClick(this, e);
    }
    #region IMessageBox Members

    public void Show(WarningLevel warningLevel, string message)
    {
        mMessageBox.OkCancelMode = false;
        mMessageBox.Show(warningLevel, message);
    }

    public void Show(WarningLevel warningLevel, string message, string title)
    {
        mMessageBox.OkCancelMode = false;
        mMessageBox.Show(warningLevel, message, title);
    }
    public void ShowOkCancel(WarningLevel warningLevel, string message, string title)
    {
        mMessageBox.OkCancelMode = true;
        mMessageBox.Show(warningLevel, message, title);
    }
    #endregion
}
