﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="CloseStore.aspx.cs" Inherits="CloseStore" %>

<asp:Content ID="C1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="C2" ContentPlaceHolderID="cnt" runat="Server">
    <div id="bar">
    </div>
    <input name="RowTitle" id="RowTitle" class="RowTitle display-none" runat="server" />
    <div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 form-group-container hidden">
                <div class="form-group">
                    <asp:Label ID="Label1" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.Store</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:Label ID="mStoreName" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label11" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.CommodityName</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mServiceName" runat="server" CssClass="mServiceName chosen-select chosen-rtl"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label4" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.ServiceType</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mServiceTypes" runat="server" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 form-group-container display-none">
                <div class="form-group">
                    <asp:Label ID="Label6" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-one-sixth-aligned">Lang.ServiceZoneGroup</asp:Label>
                    <div class="col-xs-12 col-five-sixth-aligned form-col-without-padding">
                        <asp:DropDownList ID="mSZGroup" runat="server" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-lg-9 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doShow" runat="server" Text="Lang.Show" CssClass="btn btn-xs btn-primary btn-block doShow reportbutton" OnClick="doShow_Click" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doExcel" runat="server" Text="Lang.GBExcel" CssClass="btn btn-xs btn-primary btn-block reportbutton" OnClick="doExcel_Click" Enabled="false" />
                </div>
                <div class="col-xs-12 col-sm-4">
                    <input type="button" id="print" class="btn btn-xs btn-dark-blue btn-block reportbutton" value="" />
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="voucherDetailPanel" runat="server" Visible="False">
        <JP:MiniHeader runat="server" Text="Lang.CloseStore" ID="mGridTitle" />
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-9 form-group-container">
                <div class="row">
                    <div class="col-sm-12 col-md-7">
                        <div class="form-group">
                            <asp:Label ID="Label5" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-three-sixth-aligned">Lang.FinalizeCloseStore</asp:Label>
                            <div class="col-xs-12 col-three-sixth-aligned form-col-without-padding">
                                <asp:DropDownList ID="mCountMode" runat="server" CssClass="chosen-select chosen-rtl">
                                    <asp:ListItem Value="1">Lang.Count1</asp:ListItem>
                                    <asp:ListItem Value="2">Lang.Count2</asp:ListItem>
                                    <asp:ListItem Value="3">Lang.Count3</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-5">
                        <div class="form-group">
                            <asp:Label ID="Label9" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-three-sixth-aligned">Lang.CreateDocs</asp:Label>
                            <div class="col-xs-12 col-three-sixth-aligned form-col-without-padding">
                                <asp:CheckBox ID="mCreateDocs" runat="server" CssClass="" Checked="true" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 form-group-container">
                <div class="col-xs-12 col-sm-6">
                    <asp:Button ID="doSave" runat="server" Text="Lang.SaveF3" CssClass="btn btn-xs btn-primary btn-block doSave" OnClick="doSave_Click" />
                </div>
                <div class="col-xs-12 col-sm-6">
                    <asp:Button ID="doFinalize" runat="server" Text="Lang.DoFinalizeCloseStore" CssClass="btn btn-xs btn-primary btn-block doFinalize" OnClick="doFinalize_Click" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <asp:Label runat="server" ID="msg" CssClass="alert block" Visible="False" EnableViewState="False" />
            </div>
        </div>
        <div class="row" runat="server" id="mLinks">
            <div class="col-sm-3">
                <asp:Label runat="server" ID="Label8" Text="Lang.StoreDraft" />
                : 
                <asp:Label runat="server" ID="mStoreDraftLink" />
            </div>
            <div class="col-sm-3">
                <asp:Label runat="server" ID="Label10" Text="Lang.DocumentNumber" />
                <asp:Label runat="server" ID="mStoreDraftVoucherLink" />
            </div>
            <div class="col-sm-3">
                <asp:Label runat="server" ID="Label7" Text="Lang.Purchase" />
                : 
                <asp:Label runat="server" ID="mPurchaseLink" />
            </div>
            <div class="col-sm-3">
                <asp:Label runat="server" ID="Label12" Text="Lang.DocumentNumber" />
                <asp:Label runat="server" ID="mPurchaseVoucherLink" />
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <asp:GridView ID="mGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
                    Scrolling="Vertical" runat="server" ShowFooter="true" BorderStyle="None" OnRowDataBound="mGrid_RowDataBound" GridLines="Both" EmptyDataText=""
                    AllowSorting="true" OnSorting="mGrid_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="Lang.ColRowNr">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ID" Visible="false" />
                        <asp:BoundField DataField="Code" HeaderText="Lang.Code" ItemStyle-HorizontalAlign="Center" SortExpression="Code" />
                        <asp:BoundField DataField="Title" HeaderText="Lang.Commodity" ItemStyle-HorizontalAlign="Center" SortExpression="Title" />
                        <asp:BoundField DataField="UnitName" HeaderText="Lang.Units" ItemStyle-HorizontalAlign="Center" SortExpression="UnitName" />
                        <asp:BoundField DataField="Supply" HeaderText="Lang.Supply" ItemStyle-HorizontalAlign="Center" SortExpression="Supply" />
                        <asp:BoundField DataField="Count1" HeaderText="Lang.Count1" ItemStyle-HorizontalAlign="Center" SortExpression="Count1" />
                        <asp:BoundField DataField="Diff1" HeaderText="Lang.Diff" ItemStyle-HorizontalAlign="Center" SortExpression="Diff1" />
                        <asp:BoundField DataField="Count2" HeaderText="Lang.Count2" ItemStyle-HorizontalAlign="Center" SortExpression="Count2" />
                        <asp:BoundField DataField="Diff2" HeaderText="Lang.Diff" ItemStyle-HorizontalAlign="Center" SortExpression="Diff2" />
                        <asp:BoundField DataField="Count3" HeaderText="Lang.Count3" ItemStyle-HorizontalAlign="Center" SortExpression="Count3" />
                        <asp:BoundField DataField="Diff3" HeaderText="Lang.Diff" ItemStyle-HorizontalAlign="Center" SortExpression="Diff3" />
                    </Columns>
                    <HeaderStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridRow" />
                    <SelectedRowStyle CssClass="GridRowSelected" />
                </asp:GridView>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-6 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container">
                <div class="col-xs-12 col-sm-4">
                    <asp:Button ID="doSave2" runat="server" Text="Lang.SaveF3" CssClass="btn btn-xs btn-primary btn-block doSave2" OnClick="doSave_Click" />
                </div>
            </div>
        </div>
    </asp:Panel>
    <script>
        $(function () {
            checkAccess(['admin,storehandling']);

            setClassManager();

            $("#bar").prepend(breadcrumb({
                title: $('#<%= mStoreName.ClientID %>').text() + ' - ' + $('.RowTitle').val(),
                parent: {
                    title: Lang.CloseStore,
                    url: "list.aspx?table=StoreHandling"
                },
                grandParent: {
                    title: Lang.MManagement,
                    url: "menu/admin.aspx"
                }
            }));

            $('#print').attr('value', Lang.Print);
            $('#print').click(function () {
                printTable('.ScrollPanelCartable', '../../', Lang.CloseStore);
            });
            $('.GridRow,.GridAltRow').click(function () {
                $(this).toggleClass('selected');
            })
            dateSelect('.dateselect');
            $('.doFinalize').click(function () {
                return confirm(Lang.FinalizeCloseStoreWarning);
            });

            shortcut.remove("F2");
            shortcut.add("F3", function () {
                $(".doSave").click();
            });
            shortcut.add("F2", function () {
                $(".doShow").click();
            });
        });
    </script>
</asp:Content>

