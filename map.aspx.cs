﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class map : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            gMap.GoogleMapObject.APIKey = "AIzaSyC8oiObACdmi3uuAxkvp-OTtP_2_p3exNs";
            gMap.GoogleMapObject.APIVersion = "2";
            gMap.GoogleMapObject.Width = "1024px";
            gMap.GoogleMapObject.Height = "768px";
            gMap.GoogleMapObject.ZoomLevel = 14;

            gMap.GoogleMapObject.CenterPoint = new GooglePoint("CenterPoint", 35.311417, 46.995981);

            //Timer1_Tick(this, null);
            ShowAgents();
        }

    }
    protected void Timer1_Tick(object sender, EventArgs e)
    {
        ShowAgents();
    }

    protected void ShowAgents()
    {
        DataTable dt = DbProxy.Instance.FillData("SELECT ID, [dbo].[DisplayPerson](ID, 1) Title, LastLocationDate, LastLatittude, LastLongitude FROM Personnel WHERE LastLatittude IS NOT NULL", "locations");

        if (dt != null)
        {

            gMap.GoogleMapObject.Points.Clear();
            //gMap.GoogleMapObject.Points.Add(new GooglePoint(10.ToString(), 35.311427, 46.998981));
            //gMap.GoogleMapObject.Points.Add(new GooglePoint("xzxzxzxz", 35.321427, 46.997981, "", "nazdar", "xirxal", false));
            //return;
            foreach (DataRow row in dt.Rows)
            {
                string title = Helper.GetString(row, "Title", "") + Helper.FormatDate(Helper.GetDateTime(row, "LastLocationDate", DateTime.Now), "yyyy-MM-dd HH:mm");
                gMap.GoogleMapObject.Points.Add(new GooglePoint(Helper.GetInt(row, "ID", 0).ToString()
                    , Helper.GetDouble(row, "LastLatittude", 0), Helper.GetDouble(row, "LastLongitude", 0), "", title, title, false));
            }
        }
    }
}