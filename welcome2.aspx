﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true"
    CodeFile="welcome2.aspx.cs" Inherits="welcome2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .boxgrid
        {
            width: 130px;
            height: 120px;
            margin: 10px 30px 10px 30px;
            float: right;
            background: #fff;
            border: solid 2px #aaa;
            overflow: hidden;
            position: relative;
            -moz-border-radius: 2em 2em 0.5em 0.5em;
            border-radius: 2em 2em 0.5em 0.5em;
        }
        .boxgrid img
        {
            position: absolute;
            top: 0;
            left: 0;
            border: 0;
            width: 130px;
        }
        .boxgrid p
        {
            padding: 0 10px;
            color: #000;
            font-weight: bold;
            font: 10pt "Lucida Grande" , Arial, sans-serif;
        }
        
        .boxcaption
        {
            float: left;
            position: absolute;
            background: #aaa;
            height: 300px;
            width: 100%;
            opacity: .5; /* For IE 5-7 */
            filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=50); /* For IE 8 */
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";
        }
        .trans
        {
            background: #aaa;
            opacity: .9; /* For IE 5-7 */
            filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=90); /* For IE 8 */
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=90)";
        }
        h3
        {
            margin: 10px 3px;
        }
        .caption .boxcaption
        {
            top: 70;
            left: 0;
        }
        .caption a, .caption a:link, .caption a:visited, .caption a:link:visited
        {
            color: #fff;
            font-family: Tahoma;
            font-size: 11px;
        }
        .caption a:hover, .caption a:link:hover, .caption a:visited:hover, .caption a:link:visited:hover
        {
            color: #ff0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div style='border: solid 0px red; margin-top: 10px'>
        <a href='list.aspx?table=Patient&menu=patienta'>
            <div class="boxgrid caption">
                <img src="img/user.png">
                <div class="cover boxcaption" style="top: 90px;">
                    <h3>
                        <%= "PatientFiles".Translate() %>
                    </h3>
                </div>
        </a>
    </div>
    <div class="boxgrid caption">
        <a href='list.aspx?table=Files&menu=hospitala'>
            <img src="img/Hospitalbak.gif">
            <div class="cover boxcaption" style="top: 90px;">
                <h3>
                    <%= "MHospital".Translate() %>
                </h3>
            </div>
        </a>
    </div>
    <div class="boxgrid caption">
        <a href='list.aspx?table=Files&view=fileclinics&wardid=<%=ClinicID.ToString() %>&menu=clinica'>
            <img src="img/clinicbak.gif">
            <div class="cover boxcaption" style="top: 90px;">
                <h3>
                    <%= "STClinic".Translate() %>
                </h3>
            </div>
        </a>
    </div>
    <div class="boxgrid caption">
        <a href='list.aspx?table=FileService&view=tests&wardid=<%=LabID.ToString() %>&menu=paraclinica'>
            <img src="img/laboratorybak.gif">
            <div class="cover boxcaption" style="top: 90px;">
                <h3>
                    <%= "Clinical".Translate() %>
                </h3>
            </div>
        </a>
    </div>
    <div class="boxgrid caption">
        <a href='list.aspx?table=FileService&view=Prescriptions&wardid=<%=PharmacyID.ToString() %>&menu=druga'>
            <img src="img/pharmacybak.gif">
            <div class="cover boxcaption" style="top: 90px;">
                <h3>
                    <%= "Pharmacy".Translate() %>
                </h3>
            </div>
        </a>
    </div>
    <div class="boxgrid caption">
        <a href="list.aspx?table=Topic&menu=accountinga">
            <img src="img/accountingbak.gif">
            <div class="cover boxcaption" style="top: 90px;">
                <h3>
                    <%= "Accounting".Translate() %>
                </h3>
            </div>
        </a>
    </div>
    <div class="boxgrid caption">
        <a href="list.aspx?table=Personnel&menu=personnela">
            <img src="img/personnel.png">
            <div class="cover boxcaption" style="top: 90px;">
                <h3>
                    <%= "Staff".Translate() %>
                </h3>
            </div>
        </a>
    </div>
    <div class="boxgrid caption">
        <a href="list.aspx?table=Food&menu=fooda">
            <img src="img/food.png">
            <div class="cover boxcaption" style="top: 90px;">
                <h3>
                    <%= "nurture".Translate() %>
                </h3>
            </div>
        </a>
    </div>
    <div class="boxgrid caption">
        <a href="list.aspx?table=BloodProduct&menu=blooda">
            <img src="img/blood-icon.png">
            <div class="cover boxcaption" style="top: 90px;">
                <h3>
                    <%= "BloodBank".Translate() %>
                </h3>
            </div>
        </a>
    </div>
    <div class="boxgrid caption">
        <a href="list.aspx?table=Ward&menu=basicsa">
            <img src="img/basicsbak.gif">
            <div class="cover boxcaption" style="top: 90px;">
                <h3>
                    <%= "MBasics".Translate() %>
                </h3>
            </div>
        </a>
    </div>
    <div class="boxgrid caption">
        <a href="list.aspx?table=Service&menu=servicea">
            <img src="img/basicsbak.gif">
            <div class="cover boxcaption" style="top: 90px;">
                <h3>
                    <%= "TheService".Translate() %>
                </h3>
            </div>
        </a>
    </div>
    <div class="boxgrid caption">
        <a href="list.aspx?table=Stores&menu=storea">
            <img src="img/report.png">
            <div class="cover boxcaption" style="top: 90px;">
                <h3>
                    <%= "Store".Translate() %>
                </h3>
            </div>
        </a>
    </div>
    <div class="boxgrid caption">
        <a href="list.aspx?table=config&menu=managera">
            <img src="img/report.png">
            <div class="cover boxcaption" style="top: 90px;">
                <h3>
                    <%= "Management".Translate() %>
                </h3>
            </div>
        </a>
    </div>
    <div class="boxgrid caption">
        <a href="list.aspx?table=InsuranceOrganization&menu=insa">
            <img src="img/report.png">
            <div class="cover boxcaption" style="top: 90px;">
                <h3>
                    <%= "Insurance".Translate() %>
                </h3>
            </div>
        </a>
    </div>
    <div class="boxgrid caption">
        <a href="reports/default.aspx">
            <img src="img/report.png">
            <div class="cover boxcaption" style="top: 90px;">
                <h3>
                    <%= "Report".Translate() %>
                </h3>
            </div>
        </a>
    </div>
    <div class="clear">
    </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            //To switch directions up/down and left/right just place a "-" in front of the top/left attribute
            //Vertical Sliding
            $('.boxgrid.slidedown').hover(function () {
                $(".cover", this).stop().animate({ top: '-260px' }, { queue: false, duration: 300 });
            }, function () {
                $(".cover", this).stop().animate({ top: '0px' }, { queue: false, duration: 300 });
            });
            //Horizontal Sliding
            $('.boxgrid.slideright').hover(function () {
                $(".cover", this).stop().animate({ left: '325px' }, { queue: false, duration: 300 });
            }, function () {
                $(".cover", this).stop().animate({ left: '0px' }, { queue: false, duration: 300 });
            });
            //Diagnal Sliding
            $('.boxgrid.thecombo').hover(function () {
                $(".cover", this).stop().animate({ top: '260px', left: '325px' }, { queue: false, duration: 300 });
            }, function () {
                $(".cover", this).stop().animate({ top: '0px', left: '0px' }, { queue: false, duration: 300 });
            });
            //Partial Sliding (Only show some of background)
            $('.boxgrid.peek').hover(function () {
                $(".cover", this).stop().animate({ top: '90px' }, { queue: false, duration: 160 });
            }, function () {
                $(".cover", this).stop().animate({ top: '0px' }, { queue: false, duration: 160 });
            });
            //Full Caption Sliding (Hidden to Visible)
            $('.boxgrid.captionfull').hover(function () {
                $(".cover", this).stop().animate({ top: '160px' }, { queue: false, duration: 160 });
            }, function () {
                $(".cover", this).stop().animate({ top: '260px' }, { queue: false, duration: 160 });
            });
            //Caption Sliding (Partially Hidden to Visible)
            $('.boxgrid.caption').hover(function () {
                $(".cover", this).addClass('trans');
                //$(".cover", this).stop().animate({ top: '50px' }, { queue: false, duration: 160 });
            }, function () {
                $(".cover", this).removeClass('trans');
                //$(".cover", this).stop().animate({ top: '170px' }, { queue: false, duration: 160 });
            });
        });
    </script>
</asp:Content>
