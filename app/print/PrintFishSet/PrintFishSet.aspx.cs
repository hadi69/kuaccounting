﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class app_print_PrintFishSet_PrintFishSet : BaseReport
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetPageTitle("Lang.PrintFishSet");
        SetReportPath();

        int year = -1;
        int monthvalue = -1;
        string fromNum = "";
        string toNum = "";

        // If we don't have an integer year, throw an error
        if (!int.TryParse(Page.RouteData.Values["year"].ToString(), out year))
        {
            ShowError("Year must be an integer :-(");
            return;
        }

        // If we don't have an integer month, throw an error
        if (!int.TryParse(Page.RouteData.Values["month"].ToString(), out monthvalue))
        {
            ShowError("Month must be an integer :-(");
            return;
        }

        fromNum = Page.RouteData.Values["fromnum"].ToString();
        toNum = Page.RouteData.Values["tonum"].ToString();

        DataTable fishViews = BaseMap.QueryToTable("fishviewrange", new Dictionary<string, string> { { "FishYear", year.ToString() }, { "FishMonth", monthvalue.ToString() }, { "FromNum", fromNum }, { "ToNum", toNum } });

        if (fishViews.Rows.Count > 0)
        {
            string IDs = "";

            for (int i = 0; i < fishViews.Rows.Count; i++)
            {
                IDs += Helper.GetString(fishViews.Rows[i], "ID", "") + ",";
            }
            IDs = IDs.Substring(0, IDs.Length - 1);

            DataTable loansViews = BaseMap.QueryToTable("loansviewrange", new Dictionary<string, string> { { "IDs", IDs } });

            DataTable fishValueFunctions = BaseMap.QueryToTable("fishvalueviewrange", new Dictionary<string, string> { { "FieldShowType", ((int)FieldShowType.Function).ToString() }, { "IDs", IDs } });

            DataTable fishValueBenefits = BaseMap.QueryToTable("fishvalueviewrange", new Dictionary<string, string> { { "FieldShowType", ((int)FieldShowType.Benefit).ToString() }, { "IDs", IDs } });

            DataTable fishValueDeductions = BaseMap.QueryToTable("fishvalueviewrange", new Dictionary<string, string> { { "FieldShowType", ((int)FieldShowType.Deduction).ToString() }, { "IDs", IDs } });

            DataTable sumBenefits = BaseMap.QueryToTable("sumshowtyperang", new Dictionary<string, string> { { "FieldShowType", ((int)FieldShowType.Benefit).ToString() }, { "IDs", IDs } });

            DataTable sumDeductions = BaseMap.QueryToTable("sumshowtyperang", new Dictionary<string, string> { { "FieldShowType", ((int)FieldShowType.Deduction).ToString() }, { "IDs", IDs } });


            AddTable("Fishs", fishViews);
            AddTable("Loans", loansViews);
            AddTable("FunctionValues", fishValueFunctions);
            AddTable("BenefitValues", fishValueBenefits);
            AddTable("DeductionValues", fishValueDeductions);
            AddTable("SumBenefits", sumBenefits);
            AddTable("SumDeductions", sumDeductions);

        }
        ShowReport();
    }

    public override void SetReportPath()
    {
        reportPath = "app\\print\\PrintFishSet\\fa\\Report.mrt";
    }
}