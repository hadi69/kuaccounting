﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class app_print_Transfer : BaseReport
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetPageTitle("Lang.SalesPurchase");
        SetReportPath();

        int id = -1;
        
        // If we don't have an integer ID, throw an error
        if (!int.TryParse(Page.RouteData.Values["id"].ToString(), out id))
        {
            ShowError("ID must be an integer :-(");
            return;
        }

        TransferView transferView = TransferView.Find(id);

        if (transferView != null)
        {
            AddData("TransferView", transferView);
            AddData("Items", transferView.Items);

            //PersonnelView seller = PersonnelView.Find(UsersView.Find(storeDraftView.UserID).PersonnelID);
            //if (seller != null)
            //{
            //    AddData("Seller", seller);
            //}

            //PersonnelView buyer = PersonnelView.Find(storeDraftView.BuyerID);
            //if (buyer != null)
            //{
            //    AddData("Buyer", buyer);
            //}

            ShowReport();
        }

        Page.SetLang();
    }
    public override void SetReportPath()
    {
        AddReport(defaultReportSlug, @"app\print\Transfer\fa\Report.mrt");

        reportPath = GetReportPathBasedOnType();
    }
}