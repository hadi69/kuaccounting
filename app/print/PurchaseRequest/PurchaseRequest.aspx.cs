﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;

public partial class app_print_PurchaseRequest : BaseReport
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetPageTitle("Lang." + RouteData.Values["title"]);
        SetReportPath();

        string ids = RouteData.Values["ids"].ToString();

        if (String.IsNullOrEmpty(ids))
        {
            ShowError("I need at least one ID! :(");
            return;
        }

        ids = ids.Replace('|', ',');

        DataTable purchaseRequestViews = BaseMap.QueryToTable("purchaserequestviewrange", new Dictionary<string, string> { { "IDs", ids } });

        if (purchaseRequestViews != null && purchaseRequestViews.Rows.Count > 0)
        {

            DataTable purchaseRequestItemsViews = BaseMap.QueryToTable("purchaserequestitemsviewrange", new Dictionary<string, string> { { "IDs", ids } });


            DataTable companyinformation = BaseMap.QueryToTable("companyinformation", null);


            //DataTable buyers = BaseMap.QueryToTable("storedraftbuyerrange",
            //    new Dictionary<string, string> {{"IDs", ids}});

            CalculateAdditionalPurchaseRequestData(purchaseRequestViews);
            CalculateAdditionalPurchaseRequestItemData(purchaseRequestItemsViews);
           // CalculateAdditionalBuyerData(buyers);

            AddTable("purchaseRequestViews", purchaseRequestViews);
            AddTable("purchaseRequestItemsViews", purchaseRequestItemsViews);
            //AddTable("Buyers", buyers);
            AddTable("Companyformation", companyinformation);

            // Add logo and comment
            string logoImage = DbProxy.Instance.GetConfig("StoreDraftLogo", "");
            string logoPath = Helper.GetDomain() + "/files/config/" + logoImage;
            //string comment = DbProxy.Instance.GetConfig("StoreDraftComment", "");

            //AddVariable("Logo", logoPath);
           // AddVariable("Comment", comment);

            ShowReport();
        }

        Page.SetLang();
    }

    void CalculateAdditionalPurchaseRequestData(DataTable PurchaseRequest)
    {
        PurchaseRequest.Columns.Add("WorkFlowStatusText", typeof(string));
        PurchaseRequest.Columns.Add("SumAmount", typeof(double));
        PurchaseRequest.Columns.Add("SumTotalPrice", typeof(decimal));
        PurchaseRequest.Columns.Add("GrandTotal", typeof(decimal));
        PurchaseRequest.Columns.Add("RequestDateJalali", typeof(string));
        PurchaseRequest.Columns.Add("ConfirmDateJalali", typeof(string));

        foreach (DataRow purchaseRequestRow in PurchaseRequest.Rows)
        {
            PurchaseRequestView rv = PurchaseRequestView.Find(int.Parse(purchaseRequestRow["ID"].ToString()));

            purchaseRequestRow["WorkFlowStatusText"] = rv.WorkFlowStatusText;
            purchaseRequestRow["SumAmount"] = rv.SumAmount;
            purchaseRequestRow["SumTotalPrice"] = rv.SumTotalPrice;
            purchaseRequestRow["GrandTotal"] = rv.GrandTotal;
            purchaseRequestRow["RequestDateJalali"] = rv.RequestDateJalali;
            purchaseRequestRow["ConfirmDateJalali"] = rv.ConfirmDateJalali;
        }
    }

    void CalculateAdditionalPurchaseRequestItemData(DataTable purchaseRequestItems)
    {
        purchaseRequestItems.Columns.Add("TotalPrice", typeof(decimal));

        foreach (DataRow riRow in purchaseRequestItems.Rows)
        {
            PurchaseRequestItemsView riv = new PurchaseRequestItemsView(riRow);
            riRow["TotalPrice"] = riv.TotalPrice;
        }
    }

    public override void SetReportPath()
    {
        AddReport(defaultReportSlug, @"app\print\PurchaseRequest\fa\Report.mrt");
       // AddReport("returnstoremode", @"app\print\Returns\fa\ReturnStoreMode.mrt");
        
        reportPath = GetReportPathBasedOnType();
    }
}