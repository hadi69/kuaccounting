﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;

public partial class app_print_ServiceBarCode : BaseReport
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //SetPageTitle("Lang." + RouteData.Values["title"]);
        SetReportPath();

        string ids = RouteData.Values["ids"].ToString();
        string numOfRow = RouteData.Values["numOfRow"].ToString();
        string hasServiceVariat = RouteData.Values["hasServiceVariat"].ToString();

        if (String.IsNullOrEmpty(ids))
        {
            ShowError("I need at least one ID! :(");
            return;
        }

        ids = ids.Replace('|', ',');

        DataTable servicebarcodes = BaseMap.QueryToTable("servicebarcodesrange", new Dictionary<string, string>{{"IDs", ids}, {"numOfRow", numOfRow}, { "hasServiceVariat", hasServiceVariat}});

        if (servicebarcodes != null && servicebarcodes.Rows.Count > 0)
        {

            AddTable("ServiceBarCodes", servicebarcodes);


            ShowReport();
        }

        Page.SetLang();
    }

    public override void SetReportPath()
    {
        AddReport(defaultReportSlug, @"app\print\ServiceBarCode\fa\Report.mrt");

        reportPath = GetReportPathBasedOnType();
    }
}