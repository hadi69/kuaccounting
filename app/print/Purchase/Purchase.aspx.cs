﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class app_print_Purchase_Purchase : BaseReport
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetPageTitle("Lang.SalesPurchase");
        SetReportPath();

        int id = -1;
        
        // If we don't have an integer ID, throw an error
        if (!int.TryParse(Page.RouteData.Values["id"].ToString(), out id))
        {
            ShowError("ID must be an integer :-(");
            return;
        }

        PurchaseView purchaseView = PurchaseView.Find(id);

        if (purchaseView != null)
        {
            AddData("PurchaseView", purchaseView);
            AddData("Items", purchaseView.Items);

            //PersonnelView seller = PersonnelView.Find(UsersView.Find(storeDraftView.UserID).PersonnelID);
            //if (seller != null)
            //{
            //    AddData("Seller", seller);
            //}

            //PersonnelView buyer = PersonnelView.Find(storeDraftView.BuyerID);
            //if (buyer != null)
            //{
            //    AddData("Buyer", buyer);
            //}

            ShowReport();
        }

        Page.SetLang();
    }
    public override void SetReportPath()
    {
        AddReport(defaultReportSlug, @"app\print\Purchase\fa\Report.mrt");
        AddReport("simple", @"app\print\Purchase\fa\Simple.mrt");
        AddReport("receipt", @"app\print\Purchase\fa\Receipt.mrt");

        reportPath = GetReportPathBasedOnType();
    }
}