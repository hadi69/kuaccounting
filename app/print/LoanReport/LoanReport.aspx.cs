﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class app_print_LoanReport_LoanReport : BaseReport
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetPageTitle("");
        SetReportPath();

        int year = -1;
        int monthvalue = -1;
        int personnel = -1;

        // If we don't have an integer year, throw an error
        if (!int.TryParse(Page.RouteData.Values["year"].ToString(), out year))
        {
            ShowError("Year must be an integer :-(");
            return;
        }

        // If we don't have an integer month, throw an error
        if (!int.TryParse(Page.RouteData.Values["month"].ToString(), out monthvalue))
        {
            ShowError("Month must be an integer :-(");
            return;
        }
        personnel = Helper.GetInt(Request.QueryString["personnel"], -1);
        string month = Helper.ToPersianMonthName(monthvalue);

        string query = string.Format(@"SELECT  PersonnelName, FundName, dbo.MembershipItems.PayAmount, PersonneliNum
FROM dbo.MembershipItems 
INNER JOIN dbo.MembershipsView ON dbo.MembershipItems.MembershipID = dbo.MembershipsView.ID
INNER JOIN dbo.Funds ON dbo.MembershipsView.FundID = dbo.Funds.ID
INNER JOIN dbo.Personnel ON dbo.MembershipsView.PersonnelID = dbo.Personnel.ID
WHERE PayYear={0} AND PayMonth={1} {2}
UNION
SELECT  PersonnelName, FundName, dbo.LoanItems.PayAmount, PersonneliNum
FROM dbo.LoanItems 
INNER JOIN dbo.LoansView ON dbo.LoanItems.LoansID = dbo.LoansView.ID
INNER JOIN dbo.Funds ON dbo.LoansView.FundID = dbo.Funds.ID
INNER JOIN dbo.Personnel ON dbo.LoansView.PersonnelID = dbo.Personnel.ID
WHERE dbo.GetShamsiDateYear(dbo.LoanItems.PayDate)={0} AND dbo.GetShamsiMonth(dbo.LoanItems.PayDate)={1} {2}",
            year, monthvalue, personnel > 0 ? "AND PersonnelID=" + personnel : "");

        DataTable loans = DbProxy.Instance.FillData(query, "loans");
        if (loans.Rows.Count > 0)
        {
            AddTable("Loans", loans);
            AddVariable("Year", year);
            AddVariable("Month", month);
        }

        ShowReport();
    }

    public override void SetReportPath()
    {
        reportPath = "app\\print\\LoanReport\\fa\\Report.mrt";
    }
}