﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class app_print_WFTStoreDraft_WFTStoreDraft : BaseReport
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetPageTitle("Lang.WFTStoreDraft");
        SetReportPath();

        int id = -1;

        // If we don't have an integer ID, throw an error
        if (!int.TryParse(Page.RouteData.Values["id"].ToString(), out id))
        {
            ShowError("ID must be an integer :-(");
            return;
        }

        StoreDraftView storeDraftView = StoreDraftView.Find(id);

        if (storeDraftView != null)
        {
            AddData("StoreDraftView", storeDraftView);
            AddData("Items", storeDraftView.Items);

            PersonnelView seller = PersonnelView.Find(UsersView.Find(storeDraftView.UserID).PersonnelID);
            if (seller != null)
            {
                AddData("Seller", seller);
            }

            PersonnelView buyer = PersonnelView.Find(storeDraftView.BuyerID);
            if (buyer != null)
            {
                AddData("Buyer", buyer);

                decimal actualRemain = buyer.Remain;

                // PayMethod
                // 0 => Naqdi
                // 1 => Check
                // 2 => Bedehkari
                // 3 => Tarkibi

                if (storeDraftView.PayMethod == 2) // Bedehkari
                {
                    actualRemain -= storeDraftView.GrandTotal;
                }
                else if (storeDraftView.PayMethod == 3)
                {
                    actualRemain -= storeDraftView.OwedPrice;
                }

                AddVariable("ActualRemain", actualRemain);

                // Add logo and comment
                string logoImage = DbProxy.Instance.GetConfig("StoreDraftLogo", "");
                string logoPath = Helper.GetDomain() + "/files/config/" + logoImage;
                string comment = DbProxy.Instance.GetConfig("StoreDraftComment", "");

                AddVariable("Logo", logoPath);
                AddVariable("Comment", comment);
            }

            ShowReport();
        }

        Page.SetLang();
    }
    public override void SetReportPath()
    {
        AddReport(defaultReportSlug, @"app\print\WFTStoreDraft\fa\Report.mrt");
        AddReport("simple2", @"app\print\WFTStoreDraft\fa\Simple2.mrt");
        reportPath = GetReportPathBasedOnType();
    }
}
