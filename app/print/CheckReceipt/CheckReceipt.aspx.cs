﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class app_print_CheckReceipt_CheckReceipt : BaseReport
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetPageTitle("Lang.CheckReceipt");
        SetReportPath();

        int id = -1;

        // If we don't have an integer ID, throw an error
        if (!int.TryParse(Page.RouteData.Values["id"].ToString(), out id))
        {
            ShowError("ID must be an integer :-(");
            return;
        }

        CheckView CheckView = CheckView.Find(id);

        if (CheckView != null)
        {
            AddData("CheckView", CheckView);

            PersonnelView company = PersonnelView.Find(UsersView.Find(CheckView.UserID).PersonnelID);
            if (company != null)
            {
                AddData("Company", company);
            }

            ShowReport();
        }


        Page.SetLang();
    }
    public override void SetReportPath()
    {
        AddReport(defaultReportSlug, @"app\print\CheckReceipt\fa\Report.mrt");

        reportPath = GetReportPathBasedOnType();
    }
}