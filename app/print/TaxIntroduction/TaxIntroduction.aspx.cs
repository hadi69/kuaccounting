﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class app_print_TaxIntroduction_TaxIntroduction : BaseReport
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetPageTitle("");
        SetReportPath();

        int year = -1;
        int monthvalue = -1;

        // If we don't have an integer year, throw an error
        if (!int.TryParse(Page.RouteData.Values["year"].ToString(), out year))
        {
            ShowError("Year must be an integer :-(");
            return;
        }
        // If we don't have an integer month, throw an error
        if (!int.TryParse(Page.RouteData.Values["month"].ToString(), out monthvalue))
        {
            ShowError("Month must be an integer :-(");
            return;
        }
       
        string month = Helper.ToPersianMonthName(monthvalue);
        string date = Helper.ToFaFormat(DateTime.Now);

        AddData("EconomicNo", ConfigView.FindByName("EconomicNo"));
        AddData("TaxNo", ConfigView.FindByName("TaxNo"));
        AddData("PostCode", ConfigView.FindByName("PostCode"));
        AddData("Signature1", ConfigView.FindByName("Signature1"));
        AddData("CompanyName", ConfigView.FindByName("CompanyName"));
        AddVariable("Year", year);
        AddVariable("Month", month);
        AddVariable("Date", date);

        ShowReport();
    }

    public override void SetReportPath()
    {
        reportPath = "app\\print\\TaxIntroduction\\fa\\Report.mrt";
    }
}