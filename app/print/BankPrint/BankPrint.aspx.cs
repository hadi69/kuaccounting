﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class app_print_BankPrint_BankPrint : BaseReport
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetPageTitle("Lang.BankPrint");
        SetReportPath();

        int id = -1;

        // If we don't have an integer ID, throw an error
        if (!int.TryParse(Page.RouteData.Values["id"].ToString(), out id))
        {
            ShowError("ID must be an integer :-(");
            return;
        }

        PayCheckView payCheckView = PayCheckView.Find(id);

        if (payCheckView != null)
        {
            AddData("PayCheckView", payCheckView);
            ShowReport();
        }

        Page.SetLang();
    }
    public override void SetReportPath()
    {
        int id = -1;

        // If we don't have an integer ID, throw an error
        if (!int.TryParse(Page.RouteData.Values["id"].ToString(), out id))
        {
            ShowError("ID must be an integer :-(");
            return;
        }

        PayCheckView payCheckView = PayCheckView.Find(id);

        if (payCheckView == null || payCheckView.BankPrintID == 0)
        {
            throw new Exception("Print Template not defined for Cheque!");
        }

        AddReport(defaultReportSlug, @"app\print\BankPrint\fa\" + payCheckView.BankPrintID + ".mrt");

        reportPath = GetReportPathBasedOnType();
    }
}
