﻿using System;

public partial class app_print_BankPrint_EditBankPrint : BaseReport
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetPageTitle("Lang.BankPrint");
        SetReportPath();

        int id = -1;

        // If we don't have an integer ID, throw an error
        if (!int.TryParse(Page.RouteData.Values["id"].ToString(), out id))
        {
            ShowError("ID must be an integer :-(");
            return;
        }

        PayCheckView payCheckView = new PayCheckView();
        AddData("PayCheckView", payCheckView);
        ShowReport();

        Page.SetLang();
    }
    public override void SetReportPath()
    {
        int id = -1;

        // If we don't have an integer ID, throw an error
        if (!int.TryParse(Page.RouteData.Values["id"].ToString(), out id))
        {
            ShowError("ID must be an integer :-(");
            return;
        }

        AddReport(defaultReportSlug, @"app\print\BankPrint\fa\" + id + ".mrt");

        reportPath = GetReportPathBasedOnType();
    }

    protected new bool ShowDesigner
    {
        get { return true; }
    }
}
