﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class app_print_PaidWage_PaidWage : BaseReport
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetPageTitle("");
        SetReportPath();

        int year = -1;
        int monthvalue = -1;

        if (!int.TryParse(Page.RouteData.Values["year"].ToString(), out year))
        {
            ShowError("Year most be an integer :-(");
            return;
        }

        if (!int.TryParse(Page.RouteData.Values["month"].ToString(), out monthvalue))
        {
            ShowError("Month most be an integer :-(");
        }

        string month = Helper.ToPersianMonthName(monthvalue);

        List<FishView> fishview = FishView.FindTotalPaidByMonth(year, monthvalue);
        for (int i = 0; i < fishview.Count; i++)
        {
            if (fishview[i].Status == 0)
            {
                fishview.Remove(fishview[i]);
                i--;
            }
        }

        if (fishview.Count > 0)
        {
            AddData("FishView",fishview);
            AddVariable("Year", year);
            AddVariable("Month", month);
            AddData("CompanyName", ConfigView.FindByName("CompanyName"));
           
            ShowReport();
        }
        
        Page.SetLang();
    }
    public override void SetReportPath()
    {
        reportPath = "app\\print\\PaidWage\\fa\\Report.mrt";
    }
}