﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;

public partial class app_print_DocReceipt : BaseReport
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //SetPageTitle("Lang." + RouteData.Values["title"]);
        int type = Helper.GetInt(RouteData.Values["Type"], 0);
        SetReportPath(type);
        string title = "", term1 = "", term2 ="";
        if (type == 4) // // Fund2Person
        {
            title = LangProvider.Instance["ReceiveDocReceiptTitle"];
            term1 = LangProvider.Instance["ReceiveDocReceiptTerm1"];
            term2 = LangProvider.Instance["ReceiveDocReceiptTerm2"];
        }
        else if (type == 5) // Person2Fund
        {
            title = LangProvider.Instance["PayDocReceiptTitle"];
            term1 = LangProvider.Instance["PayDocReceiptTerm1"];
            term2 = LangProvider.Instance["PayDocReceiptTerm2"];
        }
        else if (type == 6) //Fund2Topic
        {
            title = LangProvider.Instance["PayCostReceiptTitle"];
            term1 = LangProvider.Instance["PayDocReceiptTerm1"];
            term2 = LangProvider.Instance["PayDocReceiptTerm2"];
        }
        string topicName = RouteData.Values["TopicName"].ToString();
        string no = RouteData.Values["No"].ToString();
        string date = Helper.FormatDate(DateTime.Now, null);
        decimal amount = Helper.GetDecimal(RouteData.Values["Amount"], 0);
        decimal discount = Helper.GetDecimal(RouteData.Values["Discount"], 0);
        decimal checkamount = Helper.GetDecimal(RouteData.Values["CheckAmount"], 0);
        string personName = RouteData.Values["Person"].ToString();
        string checkSerial = "";
        if (RouteData.Values["CheckSerial"].ToString() != "null")
            checkSerial = RouteData.Values["CheckSerial"].ToString();
        string monetaryUnit = DbProxy.Instance.GetConfig("MonetaryUnitForPrint", "");
        // Add logo and comment
        string logoImage = DbProxy.Instance.GetConfig("StoreDraftLogo", "");
        string logoPath = Helper.GetDomain() + "/files/config/" + logoImage;

        int personId = Helper.GetInt(RouteData.Values["PersonId"], 0);
        PersonnelView buyer = PersonnelView.Find(personId);
        if (buyer.Remain < 0)
        {
            AddVariable("Remain", (buyer.Remain * -1));
            AddVariable("RemainNature", "Cr".Translate());
        }
        else if (buyer.Remain > 0)
        {
            AddVariable("Remain", buyer.Remain);
            AddVariable("RemainNature", "Li".Translate());
        }
        else
        {
            AddVariable("Remain", buyer.Remain);
        }
        AddVariable("NotPassedChecks", buyer.NotPassedChecks);

        AddVariable("ReportTitle", title);
        AddVariable("Term1", term1);
        AddVariable("Term2", term2);
        AddVariable("No", no);
        AddVariable("Date", date);
        AddVariable("Amount", amount);
        AddVariable("Discount", discount);
        AddVariable("CheckAmount", checkamount);
        AddVariable("Logo", logoPath);
        AddVariable("PersonName", personName);
        AddVariable("CheckSerial", checkSerial);
        AddVariable("TopicName", topicName);
        AddVariable("MonetaryUnit", monetaryUnit);
        ShowReport();
        Page.SetLang();
    }
    
    public void SetReportPath( int type)
    {
        if (type == 6)
        {
            AddReport(defaultReportSlug, @"app\print\DocReceipt\fa\FundToTopic.mrt");
        }
        else
        {
            AddReport(defaultReportSlug, @"app\print\DocReceipt\fa\Report.mrt");
        }
        reportPath = GetReportPathBasedOnType();
    }
}