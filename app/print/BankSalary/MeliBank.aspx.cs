﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class app_print_BankSalary_MeliBank : BaseReport
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetPageTitle("");
        SetReportPath();

        int id = -1;
        int year = -1;
        int monthvalue = -1;
        string paydate = "";

        // If we don't have an integer id, throw an error
        if (!int.TryParse(Page.RouteData.Values["id"].ToString(), out id))
        {
            ShowError("Bank must be an integer :-(");
            return;
        }

        // If we don't have an integer year, throw an error
        if (!int.TryParse(Page.RouteData.Values["year"].ToString(), out year))
        {
            ShowError("Year must be an integer :-(");
            return;
        }

        // If we don't have an integer month, throw an error
        if (!int.TryParse(Page.RouteData.Values["month"].ToString(), out monthvalue))
        {
            ShowError("Month must be an integer :-(");
            return;
        }

        paydate = Request.QueryString["paydate"];
        //paydate = string.Format("{0}/{1}/{2}",paydate.Substring(0,2),paydate.Substring(2,2),paydate.Substring(4,2));

        BankAccountView bankAccount = BankAccountView.FindByID(id);
        DataTable SumPayValue = FishValuesView.FindPersonnelSumValue(year, monthvalue,
            Helper.GetField("FTotalPayed", ReportFieldType.Account));
        string SumPayText = Helper.NumberToText(Convert.ToInt64(Helper.GetDecimal(SumPayValue.Rows[0], "SumValue", 0)));
        string No = DbProxy.Instance.FillScalarString(string.Format(@"SELECT No FROM BankFileHistory Where BankAccountID={0} AND Month={1} AND Year={2}",id,monthvalue,year));
        if (bankAccount != null)
        {
            AddData("BankAccount",bankAccount);
            AddData("CompanyName",ConfigView.FindByName("CompanyName"));
            AddVariable("Date",Helper.ToFaFormat(DateTime.Now));
            AddVariable("PayDate",paydate);
            AddData("SumPayValue", SumPayValue);
            AddVariable("SumPayText",SumPayText);
            AddVariable("Month",Page.RouteData.Values["month"]);
            AddVariable("No",No);
        }
        ShowReport();
    }

    public override void SetReportPath()
    {
        reportPath = "app\\print\\BankSalary\\fa\\Report.mrt";
    }
}