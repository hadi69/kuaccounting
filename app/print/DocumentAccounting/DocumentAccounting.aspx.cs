﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class app_print_DocumentAccounting_DocumentAccounting : BaseReport
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetPageTitle("Lang.DocumentAccounting");
        SetReportPath();

        string  fromNo = Page.RouteData.Values["from"].ToString(),
                toNo = Page.RouteData.Values["to"].ToString();

        // If we don't have 'from' and 'to', throw an error
        if (String.IsNullOrEmpty(fromNo) || String.IsNullOrEmpty(toNo))
        {
            ShowError("Please specify 'from' and 'to' :-(");
            return;
        }

        DataTable documentAccountings = BaseMap.QueryToTable("documentaccountingrange",
            new Dictionary<string, string>
            {
                {"FromNo", fromNo},
                {"ToNo", toNo}
            });

        DataTable documentAccountingItems = BaseMap.QueryToTable("documentaccountingitemsrange",
            new Dictionary<string, string>
            {
                { "FromNo", fromNo },
                { "ToNo", toNo }
            });

        DataTable companyinformation = BaseMap.QueryToTable("companyinformation", null);

        foreach (DataRow documentAccounting in documentAccountings.Rows)
        {
            documentAccounting["TotalOwe"] = SiteSettings.ToMonetary(Helper.GetDecimal(documentAccounting["TotalOwe"], 0),
                (DateTime)documentAccounting["Date"]);
            documentAccounting["TotalDue"] = SiteSettings.ToMonetary(Helper.GetDecimal(documentAccounting["TotalDue"], 0),
                (DateTime)documentAccounting["Date"]);
        }

        foreach (DataRow documentAccountingItem in documentAccountingItems.Rows)
        {
            documentAccountingItem["Owe"] = SiteSettings.ToMonetary(Helper.GetDecimal(documentAccountingItem["Owe"], 0),
                (DateTime) documentAccountingItem["DDate"]);
            documentAccountingItem["Due"] = SiteSettings.ToMonetary(Helper.GetDecimal(documentAccountingItem["Due"], 0),
                (DateTime)documentAccountingItem["DDate"]);
        }

        AddTable("DocumentAccountings", documentAccountings);
        AddTable("DocumentAccountingItems", documentAccountingItems);
        AddTable("CompanyInformation", companyinformation);

        AddVariable("FromNo", fromNo);
        AddVariable("ToNo", toNo);

        // Add logo
        string logoImage = DbProxy.Instance.GetConfig("StoreDraftLogo", "");
        string logoPath = Helper.GetDomain() + "/files/config/" + logoImage;

        // Voucher signs
        string voucherSign1 = DbProxy.Instance.GetConfig("VoucherSign1", "");
        string voucherSign2 = DbProxy.Instance.GetConfig("VoucherSign2", "");
        string voucherSign3 = DbProxy.Instance.GetConfig("VoucherSign3", "");
        string voucherSign4 = DbProxy.Instance.GetConfig("VoucherSign4", "");
        string voucherSign5 = DbProxy.Instance.GetConfig("VoucherSign5", "");

        AddVariable("Logo", logoPath);

        // Voucher signs
        AddVariable("VoucherSign1", voucherSign1);
        AddVariable("VoucherSign2", voucherSign2);
        AddVariable("VoucherSign3", voucherSign3);
        AddVariable("VoucherSign4", voucherSign4);
        AddVariable("VoucherSign5", voucherSign5);


        
        ShowReport();
        Page.SetLang();
    }
    public override void SetReportPath()
    {
        AddReport(defaultReportSlug, @"app\print\DocumentAccounting\fa\Report.mrt");
        AddReport("simple", @"app\print\DocumentAccounting\fa\Simple.mrt");
        AddReport("secondsimple", @"app\print\DocumentAccounting\fa\SecondSimple.mrt");
        reportPath = GetReportPathBasedOnType();
    }
}