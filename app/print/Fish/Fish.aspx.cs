﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class app_print_Fish_Fish : BaseReport
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetPageTitle("Lang.Fish");
        SetReportPath();

        int id = -1;

        // If we don't have an integer ID, throw an error
        if (!int.TryParse(Page.RouteData.Values["id"].ToString(), out id))
        {
            ShowError("ID must be an integer :-(");
            return;
        }

        FishView fishView = FishView.Find(id);
        List<LoansView> loansViews = LoansView.FindLoansByFishId(id);
        List<FishValuesView> fishValueFunctions = FishValuesView.FindFishValuesByFieldShowType(id, FieldShowType.Function);
        List<FishValuesView> fishValueBenefits = FishValuesView.FindFishValuesByFieldShowType(id, FieldShowType.Benefit);
        List<FishValuesView> fishValueDeductions = FishValuesView.FindFishValuesByFieldShowType(id, FieldShowType.Deduction);
        DataTable TotalLoans =
            DbProxy.Instance.FillData(
                string.Format(
                    @"SELECT SUM(Amount) AS Amount, SUM(TotalPaied) AS TotalPaid, SUM(Amount-TotalPaied) AS Remaining FROM dbo.Loans INNER JOIN dbo.FishLoans ON dbo.Loans.ID=dbo.FishLoans.LoanID WHERE FishID={0}",
                    id), "TotalLoans");

        if (fishView != null)
        {
            AddData("FishView", fishView);
            PersonnelView personnel = PersonnelView.Find(fishView.PersonnelID);
            if (personnel!= null)
            {
                AddData("Personnel", personnel);
            }
            
            AddTable("TotalLoans",TotalLoans);
            AddData("fishValueFunctions",fishValueFunctions);
            AddData("fishValueBenefits", fishValueBenefits);
            AddData("fishValueDeductions", fishValueDeductions);
            AddData("CompanyName", ConfigView.FindByName("CompanyName"));
            AddData("SumBenefits",FishValuesView.FindSumValuesByFieldShowType(id,FieldShowType.Benefit));
            AddData("SumDeductions", FishValuesView.FindSumValuesByFieldShowType(id, FieldShowType.Deduction));
            AddData("Loans",loansViews);

            ShowReport();
        }

        Page.SetLang();
    }
    public override void SetReportPath()
    {
        reportPath = "app\\print\\Fish\\fa\\Report.mrt";
    }
}