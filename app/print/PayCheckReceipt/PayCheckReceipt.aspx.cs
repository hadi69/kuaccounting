﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class app_print_PayCheckReceipt_PayCheckReceipt : BaseReport
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetPageTitle("Lang.PayCheckReceipt");
        SetReportPath();

        int id = -1;

        // If we don't have an integer ID, throw an error
        if (!int.TryParse(Page.RouteData.Values["id"].ToString(), out id))
        {
            ShowError("ID must be an integer :-(");
            return;
        }

        PayCheckView payCheckView = PayCheckView.Find(id);

        if (payCheckView != null)
        {
            AddData("PayCheckView", payCheckView);

            PersonnelView company = PersonnelView.Find(UsersView.Find(payCheckView.UserID).PersonnelID);
            if (company != null)
            {
                AddData("Company", company);
            }

            PayCheckDocNo payCheckDocNo = PayCheckDocNo.Find(id);
            if (payCheckDocNo != null)
            {
                AddData("PayCheckDocNo", payCheckDocNo);
            }

            ShowReport();
        }


        Page.SetLang();
    }
    public override void SetReportPath()
    {
        AddReport(defaultReportSlug, @"app\print\PayCheckReceipt\fa\Report.mrt");

        reportPath = GetReportPathBasedOnType();
    }
}