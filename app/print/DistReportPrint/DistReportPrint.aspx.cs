﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;

public partial class app_print_DistReportPrint : BaseReport
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetPageTitle("Lang." + RouteData.Values["title"]);
        SetReportPath();

        DataTable distReportPrint = BaseMap.QueryToTable("distreportprint", null);

        if (distReportPrint != null && distReportPrint.Rows.Count > 0)
        {
            CalculateAdditionalDistReportPrint(distReportPrint);
            AddTable("DistReportPrint", distReportPrint);
            ShowReport();
        }

        Page.SetLang();
    }
    void CalculateAdditionalDistReportPrint(DataTable distReport)
    {
        distReport.Columns.Add("FactorDateJalali", typeof(string));
        distReport.Columns.Add("ReportDateJalali", typeof(string));
        distReport.Columns.Add("BuyerRemainNature", typeof(string));
        foreach (DataRow drr in distReport.Rows)
        {
            DistReportPrintView drpv = DistReportPrintView.Find(int.Parse(drr["ID"].ToString()));
            decimal buyerRemain = drpv.BuyerRemain;
            if (buyerRemain < 0)
            {
                drr["BuyerRemain"] = (buyerRemain*-1);
                drr["BuyerRemainNature"] = "Li".Translate();
            }
            else if (buyerRemain > 0)
            {
                drr["BuyerRemain"] = buyerRemain;
                drr["BuyerRemainNature"] = "Cr".Translate();
            }
            else
            {
                 drr["BuyerRemain"] = buyerRemain;
            }
            drr["FactorDateJalali"] = drpv.FactorDateJalali;
            drr["ReportDateJalali"] = drpv.ReportDateJalali;
        }
    }
    public override void SetReportPath()
    {
        AddReport(defaultReportSlug, @"app\print\DistReportPrint\fa\Report.mrt");

        reportPath = GetReportPathBasedOnType();
    }
}