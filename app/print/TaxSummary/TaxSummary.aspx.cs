﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class app_print_TaxSummary_TaxSummary : BaseReport
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetPageTitle("");
        SetReportPath();

        int year = -1;
        int monthvalue = -1;
        string date="";
        int paytypeid =-1;
        string checkdate ="";
        string serialno ="";
        int bankid =-1;
        string branch ="";
        string accno ="";
        decimal curfine1 =-1;
        decimal curfine2 =-1;
        decimal curfine3 =-1;
        decimal pasttotalprice = -1;
        decimal pasttaxable = -1;
        decimal pasttotaltax = -1;
        decimal pastfine1 = -1;
        decimal pastfine2 = -1;
        decimal pastfine3 = -1;

        // If we don't have an integer year, throw an error
        if (!int.TryParse(Page.RouteData.Values["year"].ToString(), out year))
        {
            ShowError("Year must be an integer :-(");
            return;
        }
        
        // If we don't have an integer month, throw an error
        if (!int.TryParse(Page.RouteData.Values["month"].ToString(), out monthvalue))
        {
            ShowError("Month must be an integer :-(");
            return;
        }


        // If we don't have an integer paytypeid, throw an error
        if (!int.TryParse(Request.QueryString["paytype"].ToString(), out paytypeid))
        {
            ShowError("PayType must be an integer :-(");
            return;
        }


        // If we don't have an integer curfine1, throw an error
        if (!decimal.TryParse(Request.QueryString["curfine1"].ToString(), out curfine1))
        {
            ShowError("Curfine1 must be an integer :-(");
            return;
        }

        // If we don't have an integer curfine2, throw an error
        if (!decimal.TryParse(Request.QueryString["curfine2"].ToString(), out curfine2))
        {
            ShowError("Curfine2 must be an integer :-(");
            return;
        }

        // If we don't have an curfine3 bank, throw an error
        if (!decimal.TryParse(Request.QueryString["curfine3"].ToString(), out curfine3))
        {
            ShowError("Curfine3 must be an integer :-(");
            return;
        }

        // If we don't have an pasttotalprice bank, throw an error
        if (!decimal.TryParse(Request.QueryString["pasttotalprice"].ToString(), out pasttotalprice))
        {
            ShowError("PastTotalPrice must be an integer :-(");
            return;
        }

        // If we don't have an pasttaxable bank, throw an error
        if (!decimal.TryParse(Request.QueryString["pasttaxable"].ToString(), out pasttaxable))
        {
            ShowError("PastTaxable must be an integer :-(");
            return;
        }

        // If we don't have an pasttotaltax bank, throw an error
        if (!decimal.TryParse(Request.QueryString["pasttotaltax"].ToString(), out pasttotaltax))
        {
            ShowError("PastTotalTax must be an integer :-(");
            return;
        }

        // If we don't have an pastfine1 bank, throw an error
        if (!decimal.TryParse(Request.QueryString["pastfine1"].ToString(), out pastfine1))
        {
            ShowError("pastfine1 must be an integer :-(");
            return;
        }

        // If we don't have an pastfine2 bank, throw an error
        if (!decimal.TryParse(Request.QueryString["pastfine2"].ToString(), out pastfine2))
        {
            ShowError("Pastfine2 must be an integer :-(");
            return;
        }

        // If we don't have an pastfine3 bank, throw an error
        if (!decimal.TryParse(Request.QueryString["pastfine3"].ToString(), out pastfine3))
        {
            ShowError("Pastfine3 must be an integer :-(");
            return;
        } 
       
        string paytype = GetPayType(paytypeid);
        string month = Helper.ToPersianMonthName(monthvalue);
        date = ToFaFormat(Request.QueryString["date"].ToString());
        checkdate = ToFaFormat(Request.QueryString["checkdate"].ToString());
        serialno = Request.QueryString["serialno"].ToString();
        string bank = "";
        if (bankid > 0)
            bank = DbProxy.Instance.FillScalar("SELECT Title FROM EnumValues WHERE ID=" + bankid).ToString();
        branch = Request.QueryString["branch"].ToString();
        accno = Request.QueryString["accno"].ToString();
        

        AddData("Address", ConfigView.FindByName("Address"));
        AddData("TaxNo", ConfigView.FindByName("TaxNo"));
        AddData("Phone", ConfigView.FindByName("PostCode"));
        AddData("Signature1", ConfigView.FindByName("Signature1"));
        AddData("CompanyName", ConfigView.FindByName("CompanyName"));
        AddData("PostCode", ConfigView.FindByName("PostCode"));
        AddData("NumOfPersonnel", FishView.FindFishViewByMonth(year, monthvalue,-1).Count);
        AddVariable("Year", year);
        AddVariable("Month", month);
        AddVariable("Date",date);
        AddVariable("PayType", paytype);
        AddVariable("CheckDate", checkdate);
        AddVariable("CheckSerial", serialno);
        AddVariable("Bank", bank);
        AddVariable("Branch", branch);
        AddVariable("AccountNo", accno);
        AddVariable("CurrentFine1", curfine1);
        AddVariable("CurrentFine2", curfine2);
        AddVariable("CurrentFine3", curfine3);
        AddVariable("PastTotalPrice", pasttotalprice);
        AddVariable("PastTaxable", pasttaxable);
        AddVariable("PastTotalTax", pasttotaltax);
        AddVariable("PastFine1", pastfine1);
        AddVariable("PastFine2", pastfine2);
        AddVariable("PastFine3", pastfine3);
        AddVariable("TotalPrice", FishValuesView.FindSumValue(year, monthvalue, Helper.GetField("TotalWagePrice", ReportFieldType.Tax)));
        AddVariable("TotalTaxable", FishValuesView.FindSumValue(year, monthvalue, Helper.GetField("TotalTaxable", ReportFieldType.Tax)));
        AddVariable("TotalTax", FishValuesView.FindSumValue(year, monthvalue, Helper.GetField("TotalTax", ReportFieldType.Tax)));
        ShowReport();
    }
    public override void SetReportPath()
    {
        reportPath = "app\\print\\TaxSummary\\fa\\Report.mrt";
    }
    public string ToFaFormat(string str)
    {
        string date;
        if (string.IsNullOrEmpty(str) || str.Length != 8)
            date = "";
        else
        {
            try
            {
                string yyyy = str.Substring(0, 4);
                string mm = str.Substring(4, 2);
                string dd = str.Substring(6, 2);
                date = yyyy + "/" + mm + "/" + dd;
                return date;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        return date;
    }
    string GetPayType(int paytypevalue)
    {
        string paytype = "";
        switch (paytypevalue)
        {
            case 1:
                paytype = "PayMethodCash".Translate();
                break;
            case 2:
                paytype = "NotCashPayment".Translate();
                break;
            case 3:
                paytype = "PresentListWithoutPayingTax".Translate();
                break;
        }
        return paytype;
    }
}