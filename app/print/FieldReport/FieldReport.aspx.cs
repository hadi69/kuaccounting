﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class app_print_FieldReport_FieldReport : BaseReport
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetPageTitle("");
        SetReportPath();

        int year = -1;
        int monthvalue = -1;
        int personnel = -1;
        string WorkShopCode = "";

        // If we don't have an integer year, throw an error
        if (!int.TryParse(Page.RouteData.Values["year"].ToString(), out year))
        {
            ShowError("Year must be an integer :-(");
            return;
        }

        // If we don't have an integer month, throw an error
        if (!int.TryParse(Page.RouteData.Values["month"].ToString(), out monthvalue))
        {
            ShowError("Month must be an integer :-(");
            return;
        }

        if (!string.IsNullOrEmpty(Request.QueryString["fields"]))
        {
            personnel = Helper.GetInt(Request.QueryString["personnel"], -1);
            string fields = "'" + Request.QueryString["fields"].Replace("|", "','") + "'";
            string month = Helper.ToPersianMonthName(monthvalue);

            string query = string.Format(@"SELECT PersonnelName, dbo.FishValuesView.*, PersonneliNum
                                     FROM dbo.FishValuesView 
                                     INNER JOIN dbo.FishView ON dbo.FishView.ID=dbo.FishValuesView.FishID
                                     INNER JOIN dbo.Personnel ON dbo.FishView.PersonnelID=dbo.Personnel.ID
                                     WHERE dbo.FishValuesView.FishMonth={0} AND dbo.FishValuesView.FishYear={1} AND FieldName IN ({2})",
                monthvalue, year, fields);
            if (personnel > 0)
            {
                query += " AND PersonnelID=" + personnel;
            }

            query += " Order By CONVERT(INT,PersonneliNum), FieldShowType, Priority";

            DataTable fishvalues = DbProxy.Instance.FillData(query, "fishvalues");
            if (fishvalues.Rows.Count > 0)
            {
                AddTable("FishValues",fishvalues);
                AddVariable("Year", year);
                AddVariable("Month",month);
            }

            ShowReport();
        }
    }

    public override void SetReportPath()
    {
        reportPath = "app\\print\\FieldReport\\fa\\Report.mrt";
    }
}