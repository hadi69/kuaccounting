﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;

public partial class app_print_Refuse : BaseReport
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetPageTitle("Lang." + RouteData.Values["title"]);
        SetReportPath();

        string ids = RouteData.Values["ids"].ToString();

        if (String.IsNullOrEmpty(ids))
        {
            ShowError("I need at least one ID! :(");
            return;
        }

        ids = ids.Replace('|', ',');

        DataTable refuseViews = BaseMap.QueryToTable("refuseviewrange", new Dictionary<string, string> { { "IDs", ids } });

        if (refuseViews != null && refuseViews.Rows.Count > 0)
        {

            DataTable refuseItemViews = BaseMap.QueryToTable("Refuseitemsviewrange", new Dictionary<string, string> { { "IDs", ids } });


            DataTable companyinformation = BaseMap.QueryToTable("companyinformation", null);


            DataTable sellers = BaseMap.QueryToTable("refusebuyerrange",
                new Dictionary<string, string> {{"IDs", ids}});

            CalculateAdditionalRefuseData(refuseViews);
            CalculateAdditionalRefuseItemData(refuseItemViews);
           // CalculateAdditionalBuyerData(buyers);

            AddTable("RefuseViews", refuseViews);
            AddTable("RefuseItemViews", refuseItemViews);
            AddTable("Buyers", sellers);
            AddTable("Companyformation", companyinformation);

            // Add logo and comment
            string logoImage = DbProxy.Instance.GetConfig("StoreDraftLogo", "");
            string logoPath = Helper.GetDomain() + "/files/config/" + logoImage;
            //string comment = DbProxy.Instance.GetConfig("StoreDraftComment", "");

            //AddVariable("Logo", logoPath);
           // AddVariable("Comment", comment);

            ShowReport();
        }

        Page.SetLang();
    }

    void CalculateAdditionalRefuseData(DataTable refuse)
    {
        refuse.Columns.Add("WorkFlowStatusText", typeof(string));
        refuse.Columns.Add("IsTrusText", typeof(string));
        refuse.Columns.Add("SumAmount", typeof(double));
        refuse.Columns.Add("SumTotalPrice", typeof(decimal));
        refuse.Columns.Add("SumDiscount", typeof(decimal));
        refuse.Columns.Add("SumActualDiscount", typeof(decimal));
        refuse.Columns.Add("SumTax", typeof(decimal));
        refuse.Columns.Add("SumActualTax", typeof(decimal));
        refuse.Columns.Add("SumSingleVAT", typeof(decimal));
        refuse.Columns.Add("SumActualSingleVAT", typeof(decimal));
        refuse.Columns.Add("GrandTotal", typeof(decimal));
        refuse.Columns.Add("RefuseDateJalali", typeof(string));
        refuse.Columns.Add("ConvertedShipmentPrice", typeof(string));

        foreach (DataRow refuseRow in refuse.Rows)
        {
            RefuseView rv = RefuseView.Find(int.Parse(refuseRow["ID"].ToString()));
            PersonnelView seller = PersonnelView.Find(rv.SellerID);

            refuseRow["WorkFlowStatusText"] = rv.WorkFlowStatusText;
            refuseRow["IsTrusText"] = rv.IsTrusText;
            refuseRow["SumAmount"] = rv.SumAmount;
            refuseRow["SumTotalPrice"] = rv.SumTotalPrice;
            refuseRow["SumDiscount"] = rv.SumDiscount;
            refuseRow["SumActualDiscount"] = rv.SumActualDiscount;
            refuseRow["SumTax"] = rv.SumTax;
            refuseRow["SumActualTax"] = rv.SumActualTax;
            refuseRow["SumSingleVAT"] = rv.SumSingleVAT;
            refuseRow["SumActualSingleVAT"] = rv.SumActualSingleVAT;
            refuseRow["GrandTotal"] = rv.GrandTotal;
           // refuseRow["DateJalali"] = rv.DateJalali;
            refuseRow["RefuseDateJalali"] = rv.RefuseDateJalali;
            refuseRow["ConvertedShipmentPrice"] = rv.ConvertedShipmentPrice;
        }
    }

    void CalculateAdditionalRefuseItemData(DataTable RefuseItems)
    {
        RefuseItems.Columns.Add("ActualDiscount", typeof(decimal));
        RefuseItems.Columns.Add("ActualTax", typeof(decimal));
        RefuseItems.Columns.Add("ActualSingleVAT", typeof(decimal));
        RefuseItems.Columns.Add("TotalPrice", typeof(decimal));
        RefuseItems.Columns.Add("ConvertedPrice", typeof(decimal));

        foreach (DataRow riRow in RefuseItems.Rows)
        {
            RefuseItemsView riv = new RefuseItemsView(riRow);
            riRow["ActualDiscount"] = riv.ActualDiscount;
            riRow["ActualTax"] = riv.ActualTax;
            riRow["ActualSingleVAT"] = riv.ActualSingleVAT;
            riRow["TotalPrice"] = riv.TotalPrice;
            riRow["ConvertedPrice"] = riv.ConvertedPrice;
        }
    }

    public override void SetReportPath()
    {
        AddReport(defaultReportSlug, @"app\print\refuse\fa\Report.mrt");
        AddReport("refusestoremode", @"app\print\refuse\fa\refusestoreMode.mrt");
        
        reportPath = GetReportPathBasedOnType();
    }
}