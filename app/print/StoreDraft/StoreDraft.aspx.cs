﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;

public partial class app_print_StoreDraft : BaseReport
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetPageTitle("Lang." + RouteData.Values["title"]);
        SetReportPath();

        string ids = RouteData.Values["ids"].ToString();

        if (String.IsNullOrEmpty(ids))
        {
            ShowError("I need at least one ID! :(");
            return;
        }

        ids = ids.Replace('|', ',');

        DataTable storeDraftViews = BaseMap.QueryToTable("storedraftviewrange", new Dictionary<string, string>{{"IDs", ids}});

        if (storeDraftViews != null && storeDraftViews.Rows.Count > 0)
        {

            DataTable storeDraftItemViews = BaseMap.QueryToTable("storedraftitemsviewrange", new Dictionary<string, string>{{"IDs", ids}});


            DataTable companyinformation = BaseMap.QueryToTable("companyinformation", null);


            DataTable buyers = BaseMap.QueryToTable("storedraftbuyerrange",
                new Dictionary<string, string> {{"IDs", ids}});

            UsersView user = UsersView.Find(int.Parse(SiteSettings.UserID.ToString()));
            string monetaryUnit = DbProxy.Instance.GetConfig("MonetaryUnitForPrint", "");

            CalculateAdditionalStoreDraftData(storeDraftViews);
            CalculateAdditionalStoreDraftItemData(storeDraftItemViews);
            CalculateAdditionalBuyerData(buyers);
            //CalculateAdditionalUserData(user);

            AddTable("StoreDraftViews", storeDraftViews);
            AddTable("StoreDraftItemViews", storeDraftItemViews);
            AddTable("Buyers", buyers);

            // Add logo and comment
            string logoImage = DbProxy.Instance.GetConfig("StoreDraftLogo", "");
            string logoPath = Helper.GetDomain() + "/files/config/" + logoImage;
            string comment = DbProxy.Instance.GetConfig("StoreDraftComment", "");

            if (!string.IsNullOrEmpty(user.StoreDraftFooterComment) &&
                !string.IsNullOrWhiteSpace(user.StoreDraftFooterComment))
            {
                comment = user.StoreDraftFooterComment;
            }

            if (!string.IsNullOrEmpty(user.StoreDraftHeaderComment) &&
                !string.IsNullOrWhiteSpace(user.StoreDraftHeaderComment))
            {
                foreach (DataRow company in companyinformation.Rows)
                {
                    company["Name"] = user.StoreDraftHeaderComment;
                    company["SurName"] = "";
                }
            }

            AddTable("Companyformation", companyinformation);
            AddVariable("Logo", logoPath);
            AddVariable("Comment", comment);
            AddVariable("MonetaryUnit",monetaryUnit);
            ShowReport();
        }

        Page.SetLang();
    }

    void CalculateAdditionalStoreDraftData(DataTable storeDrafts)
    {
        storeDrafts.Columns.Add("PayMethodText", typeof(string));
        storeDrafts.Columns.Add("WorkFlowStatusText", typeof(string));
        storeDrafts.Columns.Add("IsTrusText", typeof(string));
        storeDrafts.Columns.Add("IsInternalText", typeof(string));
        storeDrafts.Columns.Add("SumDeliveredAmount", typeof(double));
        storeDrafts.Columns.Add("SumTotalPrice", typeof(decimal));
        storeDrafts.Columns.Add("SumDiscount", typeof(decimal));
        storeDrafts.Columns.Add("SumActualDiscount", typeof(decimal));
        storeDrafts.Columns.Add("SumTax", typeof(decimal));
        storeDrafts.Columns.Add("SumActualTax", typeof(decimal));
        storeDrafts.Columns.Add("SumSingleVAT", typeof(decimal));
        storeDrafts.Columns.Add("SumActualSingleVAT", typeof(decimal));
        storeDrafts.Columns.Add("GrandTotal", typeof(decimal));
        storeDrafts.Columns.Add("GrandTotalText", typeof(string));
        storeDrafts.Columns.Add("ShellerShipmentText", typeof(string));
        storeDrafts.Columns.Add("DateJalali", typeof(string));
        storeDrafts.Columns.Add("FactorDateJalali", typeof(string));
        storeDrafts.Columns.Add("FactorDateTime", typeof(string));
        storeDrafts.Columns.Add("CashDiscount", typeof(decimal));
        storeDrafts.Columns.Add("BankDiscount", typeof(decimal));
        storeDrafts.Columns.Add("Check1Discount", typeof(decimal));
        storeDrafts.Columns.Add("Check2Discount", typeof(decimal));
        storeDrafts.Columns.Add("ActualRemain", typeof(decimal));
        storeDrafts.Columns.Add("ActualRemainNature", typeof(string));
        storeDrafts.Columns.Add("BuyerByCashRemain", typeof(decimal));
        storeDrafts.Columns.Add("BuyerByCashRemainNature", typeof(string));
        storeDrafts.Columns.Add("BuyerByBankRemain", typeof(decimal));
        storeDrafts.Columns.Add("BuyerByBankRemainNature", typeof(string));
        storeDrafts.Columns.Add("BuyerByCheck1Remain", typeof(decimal));
        storeDrafts.Columns.Add("BuyerByCheck1RemainNature", typeof(string));
        storeDrafts.Columns.Add("BuyerByCheck2Remain", typeof(decimal));
        storeDrafts.Columns.Add("BuyerByCheck2RemainNature", typeof(string));
        storeDrafts.Columns.Add("OfferCashPriceTotal", typeof(decimal));
        storeDrafts.Columns.Add("OfferBankPriceTotal", typeof(decimal));
        storeDrafts.Columns.Add("OfferCheck1PriceTotal", typeof(decimal));
        storeDrafts.Columns.Add("OfferCheck2PriceTotal", typeof(decimal));
        storeDrafts.Columns.Add("PayDateJalali", typeof(string));
        storeDrafts.Columns.Add("LabelsName", typeof(string));
        storeDrafts.Columns.Add("ConvertedTotalPrice", typeof(decimal));
        storeDrafts.Columns.Add("MonetaryUnitAbbreviation", typeof(string));

        foreach (DataRow storeDraftRow in storeDrafts.Rows)
        {
            StoreDraftView sdv = StoreDraftView.Find(int.Parse(storeDraftRow["ID"].ToString()));
            PersonnelView buyer = PersonnelView.FindBaseOnDate(sdv.BuyerID, sdv.FactorDate);
            decimal actualRemain = SiteSettings.ToMonetary(buyer.Remain, sdv.FactorDate);

            // PayMethod
            // 0 => Naqdi
            // 1 => Check
            // 2 => Bedehkari
            // 3 => Tarkibi

            if (sdv.PayMethod == 2 && actualRemain != 0) // Bedehkari
            {
                actualRemain -= sdv.GrandTotal;
            }
            else if (sdv.PayMethod == 3 && actualRemain != 0)
            {
                actualRemain -= sdv.OwedPrice;
            }

            decimal buyerByCashRemain = actualRemain;
            decimal buyerByBankRemain = actualRemain;
            decimal buyerByCheck1Remain = actualRemain;
            decimal buyerByCheck2Remain = actualRemain;

            if (sdv.OfferCashPriceTotal != 0) 
            {
                buyerByCashRemain += sdv.OfferCashPriceTotal;
            }
            if (sdv.OfferBankPriceTotal != 0)
            {
                buyerByBankRemain += sdv.OfferBankPriceTotal;
            }
            if (sdv.OfferCheck1PriceTotal != 0)
            {
                buyerByCheck1Remain += sdv.OfferCheck1PriceTotal;
            }
            if (sdv.OfferCheck2PriceTotal != 0)
            {
                buyerByCheck2Remain += sdv.OfferCheck2PriceTotal;
            }

            storeDraftRow["PayMethodText"] = sdv.PayMethodText;
            storeDraftRow["WorkFlowStatusText"] = sdv.WorkFlowStatusText;
            storeDraftRow["IsTrusText"] = sdv.IsTrusText;
            storeDraftRow["IsInternalText"] = sdv.IsInternalText;
            storeDraftRow["SumDeliveredAmount"] = sdv.SumDeliveredAmount;
            storeDraftRow["SumTotalPrice"] = sdv.SumTotalPrice;
            storeDraftRow["SumDiscount"] = sdv.SumDiscount;
            storeDraftRow["SumActualDiscount"] = sdv.SumActualDiscount;
            storeDraftRow["SumTax"] = sdv.SumTax;
            storeDraftRow["SumActualTax"] = sdv.SumActualTax;
            storeDraftRow["SumSingleVAT"] = sdv.SumSingleVAT;
            storeDraftRow["SumActualSingleVAT"] = sdv.SumActualSingleVAT;
            storeDraftRow["GrandTotal"] = sdv.GrandTotal;
            storeDraftRow["GrandTotalText"] = sdv.GrandTotalText;
            storeDraftRow["ShellerShipmentText"] = sdv.ShellerShipmentText;
            storeDraftRow["CashDiscount"] = sdv.CashDiscount;
            storeDraftRow["BankDiscount"] = sdv.BankDiscount;
            storeDraftRow["Check1Discount"] = sdv.Check1Discount;
            storeDraftRow["Check2Discount"] = sdv.Check2Discount;
            storeDraftRow["OfferCashPriceTotal"] = sdv.OfferCashPriceTotal;
            storeDraftRow["OfferBankPriceTotal"] = sdv.OfferBankPriceTotal;
            storeDraftRow["OfferCheck1PriceTotal"] = sdv.OfferCheck1PriceTotal;
            storeDraftRow["OfferCheck2PriceTotal"] = sdv.OfferCheck2PriceTotal;
            storeDraftRow["LabelsName"] = sdv.LabelsName;
            storeDraftRow["ConvertedTotalPrice"] = SiteSettings.ToMonetary(sdv.TotalPrice, sdv.FactorDate);
            storeDraftRow["MonetaryUnitAbbreviation"] = SiteSettings.MonetaryUnitAbbreviation;
            if (actualRemain < 0)
            {
                storeDraftRow["ActualRemain"] = (actualRemain * -1);
                storeDraftRow["ActualRemainNature"] = "Cr".Translate();
            }
            else if (actualRemain > 0)
            {
                storeDraftRow["ActualRemain"] = actualRemain;
                storeDraftRow["ActualRemainNature"] = "Li".Translate();
            }
            else
            {
                storeDraftRow["ActualRemain"] = actualRemain;
            }
            if (buyerByCashRemain < 0)
            {
                storeDraftRow["BuyerByCashRemain"] = (buyerByCashRemain * -1);
                storeDraftRow["BuyerByCashRemainNature"] = "Cr".Translate();
            }
            else if (buyerByCashRemain > 0)
            {
                storeDraftRow["BuyerByCashRemain"] = buyerByCashRemain;
                storeDraftRow["BuyerByCashRemainNature"] = "Li".Translate();
            }
            else
            {
                storeDraftRow["BuyerByCashRemain"] = buyerByCashRemain;
            }
            if (buyerByBankRemain < 0)
            {
                storeDraftRow["BuyerByBankRemain"] = (buyerByBankRemain * -1);
                storeDraftRow["BuyerByBankRemainNature"] = "Cr".Translate();
            }
            else if (buyerByBankRemain > 0)
            {
                storeDraftRow["BuyerByBankRemain"] = buyerByBankRemain;
                storeDraftRow["BuyerByBankRemainNature"] = "Li".Translate();
            }
            else
            {
                storeDraftRow["BuyerByBankRemain"] = buyerByBankRemain;
            }
            if (buyerByCheck1Remain < 0)
            {
                storeDraftRow["BuyerByCheck1Remain"] = (buyerByCheck1Remain * -1);
                storeDraftRow["BuyerByCheck1RemainNature"] = "Cr".Translate();
            }
            else if (buyerByCheck1Remain > 0)
            {
                storeDraftRow["BuyerByCheck1Remain"] = buyerByCheck1Remain;
                storeDraftRow["BuyerByCheck1RemainNature"] = "Li".Translate();
            }
            else
            {
                storeDraftRow["BuyerByCheck1Remain"] = buyerByCheck1Remain;
            }
            if (buyerByCheck2Remain < 0)
            {
                storeDraftRow["BuyerByCheck2Remain"] = (buyerByCheck2Remain * -1);
                storeDraftRow["BuyerByCheck2RemainNature"] = "Cr".Translate();
            }
            else if (buyerByCheck2Remain > 0)
            {
                storeDraftRow["BuyerByCheck2Remain"] = buyerByCheck2Remain;
                storeDraftRow["BuyerByCheck2RemainNature"] = "Li".Translate();
            }
            else
            {
                storeDraftRow["BuyerByCheck2Remain"] = buyerByCheck2Remain;
            }
            storeDraftRow["DateJalali"] = sdv.DateJalali;
            storeDraftRow["FactorDateJalali"] = sdv.FactorDateJalali;
            if (sdv.NoPrintFactorDate)
            {
                storeDraftRow["FactorDateJalali"] = "";
            }
            storeDraftRow["FactorDateTime"] = sdv.FactorDateTime;
            storeDraftRow["PayDateJalali"] = sdv.PayDateJalali;
        }
    }

    void CalculateAdditionalStoreDraftItemData(DataTable storeDraftItems)
    {
        storeDraftItems.Columns.Add("ActualDiscount", typeof(decimal));
        storeDraftItems.Columns.Add("ActualTax", typeof(decimal));
        storeDraftItems.Columns.Add("ActualSingleVAT", typeof(decimal));
        storeDraftItems.Columns.Add("TotalPrice", typeof(decimal));
        storeDraftItems.Columns.Add("DistGrateDeliverAmount", typeof(double));
        storeDraftItems.Columns.Add("DistDeliverAmount", typeof(double));

        storeDraftItems.Columns.Add("ItemBankDiscount", typeof(double));
        storeDraftItems.Columns.Add("ItemCashDiscount", typeof(double));
        storeDraftItems.Columns.Add("ItemCheck1Discount", typeof(double));
        storeDraftItems.Columns.Add("ItemCheck2Discount", typeof(double));
        storeDraftItems.Columns.Add("ConvertedPrice", typeof(decimal));
        storeDraftItems.Columns.Add("UnitNameWithDifferentUnits", typeof(string));

        foreach (DataRow sdiRow in storeDraftItems.Rows)
        {
            StoreDraftItemsView sdiv = new StoreDraftItemsView(sdiRow);
            StoreDraftView sdv = StoreDraftView.Find(sdiv.StoreDraftID);
            sdiRow["ActualDiscount"] = SiteSettings.ToMonetary(sdiv.ActualDiscount, sdv.FactorDate);
            sdiRow["ActualTax"] = SiteSettings.ToMonetary(sdiv.ActualTax, sdv.FactorDate);
            sdiRow["ActualSingleVAT"] = SiteSettings.ToMonetary(sdiv.ActualSingleVAT, sdv.FactorDate);
            sdiRow["TotalPrice"] = SiteSettings.ToMonetary(sdiv.TotalPrice, sdv.FactorDate);
            sdiRow["DistGrateDeliverAmount"] = sdiv.DistGrateDeliverAmount;
            sdiRow["DistDeliverAmount"] = sdiv.DistDeliverAmount;

            sdiRow["ItemBankDiscount"] = SiteSettings.ToMonetary(sdiv.ItemBankDiscount, sdv.FactorDate);
            sdiRow["ItemCashDiscount"] = SiteSettings.ToMonetary(sdiv.ItemCashDiscount, sdv.FactorDate);
            sdiRow["ItemCheck1Discount"] = SiteSettings.ToMonetary(sdiv.ItemCheck1Discount, sdv.FactorDate);
            sdiRow["ItemCheck2Discount"] = SiteSettings.ToMonetary(sdiv.ItemCheck2Discount, sdv.FactorDate);
            sdiRow["ConvertedPrice"] = SiteSettings.ToMonetary(sdiv.Price, sdv.FactorDate);
            string unitNameWithDifferentUnits = sdiv.UnitName;
            if (Helper.GetFloat(sdiv.UnitCurrencyConverter, 1) > 1)
            {
                unitNameWithDifferentUnits += " - " + sdiv.UnitCurrencyConverter.ToString() + " " + sdiv.MainUnitName;
            }
            sdiRow["UnitNameWithDifferentUnits"] = unitNameWithDifferentUnits;
        }
    }

    void CalculateAdditionalBuyerData(DataTable buyers)
    {
        buyers.Columns.Add("Remain", typeof(decimal));
        buyers.Columns.Add("NotPassedChecks", typeof(decimal));
        buyers.Columns.Add("RemainNature", typeof(string));

        foreach (DataRow buyerRow in buyers.Rows)
        {
            PersonnelView buyer = PersonnelView.FindBaseOnDate(int.Parse(buyerRow["ID"].ToString()), DateTime.Parse(buyerRow["FactorDate"].ToString()));
            if (buyer.Remain < 0)
            {
                buyerRow["Remain"] = SiteSettings.ToMonetary((buyer.Remain * -1), DateTime.Parse(buyerRow["FactorDate"].ToString()));
                buyerRow["RemainNature"] = "Cr".Translate();

            }
            else if (buyer.Remain > 0)
            {
                buyerRow["Remain"] = SiteSettings.ToMonetary(buyer.Remain, DateTime.Parse(buyerRow["FactorDate"].ToString()));
                buyerRow["RemainNature"] = "Li".Translate();
            }
            else
            {
                buyerRow["Remain"] = SiteSettings.ToMonetary(buyer.Remain, DateTime.Parse(buyerRow["FactorDate"].ToString()));
            }
            buyerRow["NotPassedChecks"] = buyer.NotPassedChecks;
        }
    }

    //void CalculateAdditionalUserData(DataTable users)
    //{
    //    foreach (DataRow userRow in users.Rows)
    //    {
    //        UsersView user = UsersView.Find(int.Parse(userRow["ID"].ToString()));
    //        if(string.IsNullOrEmpty(user.StoreDraftFooterComment) || string.IsNullOrWhiteSpace(user.StoreDraftFooterComment))
    //    }
    //}
    public override void SetReportPath()
    {
        AddReport(defaultReportSlug, @"app\print\StoreDraft\fa\Report.mrt");
        AddReport("simple", @"app\print\StoreDraft\fa\Simple.mrt");
        AddReport("simple2", @"app\print\StoreDraft\fa\Simple2.mrt");
        AddReport("simple3", @"app\print\StoreDraft\fa\Simple3.mrt");
        AddReport("catering", @"app\print\StoreDraft\fa\Catering.mrt");
        AddReport("distribute", @"app\print\StoreDraft\fa\Distribute.mrt");
        AddReport("simple2WithTax", @"app\print\StoreDraft\fa\simple2WithTax.mrt");

        reportPath = GetReportPathBasedOnType();
    }
}