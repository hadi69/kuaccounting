﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;

public partial class app_print_Returns : BaseReport
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetPageTitle("Lang." + RouteData.Values["title"]);
        SetReportPath();

        string ids = RouteData.Values["ids"].ToString();

        if (String.IsNullOrEmpty(ids))
        {
            ShowError("I need at least one ID! :(");
            return;
        }

        ids = ids.Replace('|', ',');

        DataTable returnsViews = BaseMap.QueryToTable("retursviewrange", new Dictionary<string, string> { { "IDs", ids } });

        if (returnsViews != null && returnsViews.Rows.Count > 0)
        {

            DataTable returnsItemViews = BaseMap.QueryToTable("returnitemsviewrange", new Dictionary<string, string> { { "IDs", ids } });


            DataTable companyinformation = BaseMap.QueryToTable("companyinformation", null);


            DataTable buyers = BaseMap.QueryToTable("storedraftbuyerrange",
                new Dictionary<string, string> {{"IDs", ids}});

            CalculateAdditionalReturnData(returnsViews);
            CalculateAdditionalReturnItemData(returnsItemViews);
           // CalculateAdditionalBuyerData(buyers);

            AddTable("ReturnViews", returnsViews);
            AddTable("ReturnItemViews", returnsItemViews);
            AddTable("Buyers", buyers);
            AddTable("Companyformation", companyinformation);

            // Add logo and comment
            string logoImage = DbProxy.Instance.GetConfig("StoreDraftLogo", "");
            string logoPath = Helper.GetDomain() + "/files/config/" + logoImage;
            //string comment = DbProxy.Instance.GetConfig("StoreDraftComment", "");

            //AddVariable("Logo", logoPath);
           // AddVariable("Comment", comment);

            ShowReport();
        }

        Page.SetLang();
    }

    void CalculateAdditionalReturnData(DataTable returns)
    {
        returns.Columns.Add("WorkFlowStatusText", typeof(string));
        returns.Columns.Add("IsTrusText", typeof(string));
        returns.Columns.Add("SumAmount", typeof(double));
        returns.Columns.Add("SumTotalPrice", typeof(decimal));
        returns.Columns.Add("SumDiscount", typeof(decimal));
        returns.Columns.Add("SumActualDiscount", typeof(decimal));
        returns.Columns.Add("SumTax", typeof(decimal));
        returns.Columns.Add("SumActualTax", typeof(decimal));
        returns.Columns.Add("SumSingleVAT", typeof(decimal));
        returns.Columns.Add("SumActualSingleVAT", typeof(decimal));
        returns.Columns.Add("GrandTotal", typeof(decimal));
        returns.Columns.Add("ReturnDateJalali", typeof(string));
        returns.Columns.Add("ConvertedShipmentPrice", typeof(decimal));

        foreach (DataRow returnsRow in returns.Rows)
        {
            ReturnView rv = ReturnView.Find(int.Parse(returnsRow["ID"].ToString()));
            PersonnelView buyer = PersonnelView.Find(rv.BuyerID);

            returnsRow["WorkFlowStatusText"] = rv.WorkFlowStatusText;
            returnsRow["IsTrusText"] = rv.IsTrusText;
            returnsRow["SumAmount"] = rv.SumAmount;
            returnsRow["SumTotalPrice"] = rv.SumTotalPrice;
            returnsRow["SumDiscount"] = rv.SumDiscount;
            returnsRow["SumActualDiscount"] = rv.SumActualDiscount;
            returnsRow["SumTax"] = rv.SumTax;
            returnsRow["SumActualTax"] = rv.SumActualTax;
            returnsRow["SumSingleVAT"] = rv.SumSingleVAT;
            returnsRow["SumActualSingleVAT"] = rv.SumActualSingleVAT;
            returnsRow["GrandTotal"] = rv.GrandTotal;
           // returnsRow["DateJalali"] = rv.DateJalali;
            returnsRow["ReturnDateJalali"] = rv.ReturnDateJalali;
            returnsRow["ConvertedShipmentPrice"] = rv.ConvertedShipmentPrice;
        }
    }

    void CalculateAdditionalReturnItemData(DataTable returnItems)
    {
        returnItems.Columns.Add("ActualDiscount", typeof(decimal));
        returnItems.Columns.Add("ActualTax", typeof(decimal));
        returnItems.Columns.Add("ActualSingleVAT", typeof(decimal));
        returnItems.Columns.Add("TotalPrice", typeof(decimal));
        returnItems.Columns.Add("ConvertedPrice", typeof(decimal));

        foreach (DataRow riRow in returnItems.Rows)
        {
            ReturnItemsView riv = new ReturnItemsView(riRow);
            riRow["ActualDiscount"] = riv.ActualDiscount;
            riRow["ActualTax"] = riv.ActualTax;
            riRow["ActualSingleVAT"] = riv.ActualSingleVAT;
            riRow["TotalPrice"] = riv.TotalPrice;
            riRow["ConvertedPrice"] = riv.ConvertedPrice;
        }
    }

    public override void SetReportPath()
    {
        AddReport(defaultReportSlug, @"app\print\Returns\fa\Report.mrt");
        AddReport("returnstoremode", @"app\print\Returns\fa\ReturnStoreMode.mrt");
        
        reportPath = GetReportPathBasedOnType();
    }
}