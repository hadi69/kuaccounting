﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class app_print_Insurance_Insurance : BaseReport
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetPageTitle("ListHoghughWaMazayayeBimeh".Translate());
        SetReportPath();

        int year = -1;
        int monthvalue = -1;
        int secId = -1;
        string WorkShopCode = "";

        // If we don't have an integer year, throw an error
        if (!int.TryParse(Page.RouteData.Values["year"].ToString(), out year))
        {
            ShowError("Year must be an integer :-(");
            return;
        }

        // If we don't have an integer month, throw an error
        if (!int.TryParse(Page.RouteData.Values["month"].ToString(), out monthvalue))
        {
            ShowError("Month must be an integer :-(");
            return;
        }

        string month = Helper.ToPersianMonthName(monthvalue);
        secId = Helper.GetInt(Page.RouteData.Values["section"], -1);
        DataRow Section = null;
        string EmployerName = "",CompanyName="";
        if (secId > 0)
        {
            Section = DbProxy.Instance.FillRow("SELECT * FROM Ward WHERE SectionType=9 AND ID=" + secId);
            WorkShopCode = Helper.GetString(Section, "Code", "");
            EmployerName = Helper.GetString(DbProxy.Instance.FillScalar(@"SELECT [dbo].[DisplayPerson](ID, 0) AS EmployerName FROM PersonnelView WHERE ID=" + Helper.GetInt(Section, "EmployerID", -1)));
            CompanyName = Helper.GetString(Section, "Name", "");
        }
        else
        {
            WorkShopCode = ConfigView.FindByName("CompanyCode").Value;
            EmployerName = ConfigView.FindByName("Signature1").DisplayValue;
            CompanyName = ConfigView.FindByName("CompanyName").Value;
        }


        List<FishView> fishView = FishView.FindFishViewByMonth(year, monthvalue, secId);

        if (fishView.Count>0)
        {
            AddData("FishView", fishView);
            AddVariable("CompanyCode",WorkShopCode);
            AddVariable("CompanyName", CompanyName);
            AddVariable("Address", ConfigView.FindByName("Address").Value);
            AddVariable("Signature1", EmployerName);
            AddData("SumInsuranceKarfarma", FishValuesView.FindSumValue(year, monthvalue, Helper.GetField("Insurance20",ReportFieldType.Insurance))+FishValuesView.FindSumValue(year, monthvalue, Helper.GetField("Insurance10", ReportFieldType.Insurance)));
            AddData("SumInsuranceBikari",FishValuesView.FindSumValue(year,monthvalue,Helper.GetField("Insurance3",ReportFieldType.Insurance)));
            AddData("SumInsurance30",FishValuesView.FindSumValue(year,monthvalue,Helper.GetField("Insurance3",ReportFieldType.Insurance))
                                    +FishValuesView.FindSumValue(year,monthvalue,Helper.GetField("Insurance7",ReportFieldType.Insurance))
                                    +FishValuesView.FindSumValue(year,monthvalue,Helper.GetField("Insurance20",ReportFieldType.Insurance))
                                    +FishValuesView.FindSumValue(year,monthvalue,Helper.GetField("Insurance10",ReportFieldType.Insurance)));
            AddVariable("Year", year);
            AddVariable("Month", month);

            ShowReport();
        }
        Page.SetLang();
    }
    public override void SetReportPath()
    {
        reportPath = "app\\print\\Insurance\\fa\\Report.mrt";
    }
}