﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Login</title>
    <link href="styles/bootstrap.css" rel="stylesheet" />
    <link href="styles/bootstrap-rtl.css" rel="stylesheet" />
    <style type="text/css">
        body {
            font-family: "peyvand",Tahoma;
            background-color: #f5f5f5;
        }
        @font-face {
            font-family: "peyvand";
            src: url('fonts/IRANSans.woff') format('woff'), url('fonts/IRANSans.ttf') format('truetype');
        }
        #main-container {
            margin-top: 20px;
        }
        @media only screen and (min-width: 1200px) {
            #main-container {
                margin-top: 60px;
            }
        }

        @media only screen and (min-width: 1200px) {
            #main-container {
                margin-top: 100px;
            }
        }

        .main-box {
            padding: 0;
            border: 1px solid #007BC6;
        }

        #logo-container {
            padding-top: 20px;
            padding-bottom: 20px;
            text-align: center;
            background: #03a9f4; /* Old browsers */
            background: -moz-linear-gradient(top,  #03a9f4 0, #1ac0ff 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0,#03a9f4), color-stop(100%,#1ac0ff)); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top,  #03a9f4 0,#1ac0ff 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top,  #03a9f4 0,#1ac0ff 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top,  #03a9f4 0,#1ac0ff 100%); /* IE10+ */
            background: linear-gradient(to bottom,  #03a9f4 0,#1ac0ff 100%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#03a9f4', endColorstr='#1ac0ff',GradientType=0 ); /* IE6-9 */
        }

        #inner-box {
            background-color: #ffffff;
            min-height: 200px;
            padding: 35px 40px 10px 40px;
            border-top: 1px solid #007BC6;
            border-bottom: 1px solid #007BC6;
        }

        @media only screen and (max-width: 450px) {
            #inner-box {
                padding: 35px 0 10px 0;
            }
        }

        @media only screen and (min-width: 451px) and (max-width: 768px) {
            #inner-box {
                padding: 35px 10% 10px 10%;
            }
        }

        #copyright {
            font-size: 11px;
            padding-top: 5px;
            padding-bottom: 5px;
            text-align: center;
            color: #fff;
            background-color: #03A9F4;
        }

        #copyright a {
            color: #333;
        }

        .control-label {
            font-size: 13px;
        }

        .input-group {
            width: 100%;
        }

        .input-group .input-group-addon {
            width: 90px !important;
            text-align: right;
        }
        @media only screen and (max-width: 768px) {
            .input-group .input-group-addon {
                width: 90px !important;
                text-align: right;
            }
        }

        .padding-0 {
            padding: 0;
        }

        input[type="text"], input[type="password"] {
            text-align: left;
            direction: ltr;
        }

        input:-webkit-autofill:focus {
            -webkit-box-shadow: 0 0 0 1000px white inset;
        }

        input:-webkit-autofill {
            -webkit-box-shadow: 0 0 0 1000px white inset;
        }
    </style>
</head>
<body>
    <div class="container">
        <form id="form1" runat="server">
            <div id="#padding">&nbsp;</div>
                <div class="row" id="main-container">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                        <asp:Panel class="well col-xs-12 padding-0 main-box" ID="panelMainBox" runat="server" >
                            <div id="logo-container" class="col-xs-12 padding-0">
                                <%--<img src='img/logoright.png' alt="logo" />--%>
                            </div>
                            <div id="inner-box" class="col-xs-12 padding-0">
                                <div class="col-xs-12 form-group-container">
                                    <div class="col-xs-12 padding-0">
                                        <div class="form-group">
                                            <div class="input-group input-group-sm">
                                                <span class="input-group-addon">
                                                    <asp:Label runat="server" ID="mUsernamelbl">
                                                    Lang.Username
                                                    </asp:Label>
                                                </span>
                                                <asp:TextBox runat="server" ID="mUsername" CssClass="form-control" MaxLength="20" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 form-group-container">
                                    <div class="col-xs-12 padding-0">
                                        <div class="form-group">
                                            <div class="input-group input-group-sm">
                                                <span class="input-group-addon">
                                                    <asp:Label runat="server" ID='mPasswordlbl'>
                                                        Lang.Password
                                                    </asp:Label>
                                                </span>
                                                <asp:TextBox runat="server" ID="mPassword" TextMode="Password" CssClass="form-control" MaxLength="15" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:Panel ID="panelDBs" runat="server" class="col-xs-12 col-md-6 form-group-container">
                                    <div class="col-xs-12 padding-0">
                                        <div class="form-group">
                                            <div class="input-group input-group-sm">
                                                <span class="input-group-addon">
                                                    <asp:Label runat="server" ID='mDBslbl'>
                                                        Lang.DataBase
                                                    </asp:Label>
                                                </span>
                                                <asp:DropDownList runat="server" ID="mDBs" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="panelLanguage" runat="server" class="col-xs-12 col-md-6 form-group-container">
                                    <div class="col-xs-12 padding-0">
                                        <div class="form-group">
                                            <div class="input-group input-group-sm">
                                                <span class="input-group-addon">
                                                    <asp:Label runat="server" ID='mLangslbl'>
                                                        Lang.Language
                                                    </asp:Label>
                                                </span>
                                                <asp:DropDownList runat="server" ID="mLangs" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel runat="server" id="panelDemo" CssClass="col-xs-12 form-group-container">
                                    <div class="col-xs-12 padding-0">
                                        <div class="form-group">
                                            <div class="input-group input-group-sm">
                                                <span class="input-group-addon">
                                                    <asp:Label runat="server" ID='Label1'>
                                                        Lang.DemoLevel
                                                    </asp:Label>
                                                </span>
                                                <asp:RequiredFieldValidator InitialValue="-1" ID="Req_ID" Display="Dynamic" runat="server" 
                                                    ControlToValidate="mDemoLevel" Text="*لطفا سطح دمو را انتخاب کنید " ErrorMessage="ErrorMessage"></asp:RequiredFieldValidator>
                                                <asp:DropDownList runat="server" ID="mDemoLevel" CssClass="form-control">
                                                    <asp:ListItem Value="-1" Text="" />
                                                    <asp:ListItem Value="100" Text="Lang.DL100" />
                                                    <asp:ListItem Value="110" Text="Lang.DL110" />
                                                    <asp:ListItem Value="101" Text="Lang.DL101" />
                                                    <asp:ListItem Value="111" Text="Lang.DL111" />
                                                    <asp:ListItem Value="200" Text="Lang.DL200" />
                                                    <asp:ListItem Value="210" Text="Lang.DL210" />
                                                    <asp:ListItem Value="201" Text="Lang.DL201" />
                                                    <asp:ListItem Value="211" Text="Lang.DL211" />
                                                    <asp:ListItem Value="300" Text="Lang.DL300" />
                                                    <asp:ListItem Value="310" Text="Lang.DL310" />
                                                    <asp:ListItem Value="301" Text="Lang.DL301" />
                                                    <asp:ListItem Value="311" Text="Lang.DL311" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <div class="col-xs-12 form-group-container">
                                    <div class="col-xs-12 padding-0">
                                        <div class="form-group">
                                            <asp:Button runat="server" ID="doLogin" OnClick="doLogin_Click" Text='Lang.Login' CssClass="btn btn-success btn-block"></asp:Button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="copyright" class="col-xs-12 padding-0">
                                <JP:Copyright runat="server" />
                            </div>
                        </asp:Panel>
                        <div class="col-xs-12 padding-0">
                            <asp:Panel runat="server" ID="panelMessage" Visible="False" EnableViewState="False">
                                <asp:Label runat="server" ID="mMsg"></asp:Label>
                            </asp:Panel>
                        </div>
                    </div>
                </div>

            <script src="js/jquery-1.9.1.js" type="text/javascript"></script>
            <script src="js/lang/fa.js" type="text/javascript"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#<%= mUsername.ClientID %>').focus().select();
                });
            </script>
        </form>
    </div>
</body>
</html>