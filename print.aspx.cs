﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;

public partial class print : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.p.Controls.Add(new LiteralControl(GetHtml()));
    }
    public string GetHtml()
    {
        string view = Request.QueryString["view"];
        string id = Request.QueryString["id"];
        string table = Request.QueryString["table"];
        string q = Request.QueryString["qd"];
        TableInfo tInfo = TableController.Instance[table];
        #region Check variables
        if (string.IsNullOrEmpty(view))
            if (tInfo != null)
                view = tInfo.Print;
        if (string.IsNullOrEmpty(view))
            return "";

        if (string.IsNullOrEmpty(q))
            if (tInfo != null)
                q = tInfo.DetailQuery;
        #endregion

        string path = Server.MapPath("~/print/" + view + ".htm");
        if (!File.Exists(path))
            return "";
        string content = File.ReadAllText(path);

        #region purge bounding html
        int start = content.IndexOf("<body>");
        if (start >= 0)
            content = content.Substring(start + "<body>".Length);
        start = content.IndexOf("</body>");
        if (start >= 0)
            content = content.Substring(0, start);
        #endregion

        content = content.Replace("_TODAY_", Helper.FormatDate(DateTime.Now, null));
        content = content.Replace("_USERNAME_", SiteSettings.User != null ? SiteSettings.User.Username : "");
        if (!content.Contains("@"))
            return content;

        string qry = string.IsNullOrEmpty(table) ? null : "SELECT * FROM @Table WHERE ID=@ID";
        if (!string.IsNullOrEmpty(q))
        {
            QueryInfo qInfo = QueryController.Instance[q];
            if (qInfo != null)
                if (!string.IsNullOrEmpty(qInfo.Query))
                    qry = qInfo.Query;
        }
        if (qry != null)
            qry = qry.Replace("@Table", table).Replace("@ID", id);

        DataRow row = qry == null ? null : DbProxy.Instance.FillRow(qry);
        content = Helper.ParseVars(content, tInfo, row, Request);
        return content;
    }
}