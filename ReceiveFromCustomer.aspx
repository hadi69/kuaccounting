﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReceiveFromCustomer.aspx.cs" Inherits="ReceiveFromCustomer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cnt" runat="Server">
    <input type="hidden" runat="server" id="mMsg" class="mMsg" />
    <input type="hidden" runat="server" id="mCurTab" class="mCurTab" />
    <br />
    <div class="well callout padding-5">
        <div class="row">
            <asp:TextBox ID="UserID" runat="server" CssClass="userid form-control input-sm hidden"></asp:TextBox>
            <div class="col-md-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" AssociatedControlID="mDate" CssClass="left-aligned-label col-md-4">Lang.Date</asp:Label>
                    <div class="col-md-8 form-col-without-padding">
                        <asp:TextBox ID="mDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="mDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <asp:Panel ID="panelPersons" CssClass="col-md-6 form-group-container" runat="server">
                <div class="form-group">
                    <asp:Label runat="server" CssClass="left-aligned-label col-xs-12 col-sm-4">Lang.CustomerName</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <input type="text" runat="server" id="CustomerName" name="CustomerName" value="" class="form-control input-sm customername" />
                        <input type="hidden" id="CustomerID" name="CustomerID" runat="server" class="customerid" value="-1" />
                    </div>
                </div>
            </asp:Panel>
            <div class="col-md-3 form-group-container">
                <div class="form-group">
                    <asp:Label runat="server" CssClass="left-aligned-label col-md-4">Lang.DocumentAccountingNum</asp:Label>
                    <div class="col-md-7 form-col-without-padding">
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon choosedocspan"><a href="#" class="choosedoc"></a></span>
                            <input type="text" id="DocumentAccountingNo" runat="server" class="documentAccountingno form-control text-center hidden" />
                            <input type="hidden" id="DocumentAccountingID" runat="server" class="documentAccountingid form-control text-center" />
                            <input type="hidden" id="NewDocumentAccountingID" name="NewDocumentAccountingID" runat="server" class="newdocumentAccountingid" value="-1" />
                            <span class="input-group-addon newdocspan">
                                <a href="#" class="newdoc hidden">
                                    <img src="img/newdoc.jpg" style="height: 16px; width: 16px;" /></a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div id='tabCashRecieve' class="col-md-6">
            <div class="col-md-12 align-center">
                <asp:Label runat="server">Lang.CashReceive</asp:Label>
            </div>
            <table id="tableCashReceive" class="awesome-table table-with-border zebra">
                <tr class='th'>
                    <td class="width-100 align-center">
                        <asp:Label runat="server">Lang.Amount</asp:Label></td>
                    <td class="width-100 align-center">
                        <asp:Label runat="server">Lang.FSFund</asp:Label></td>
                    <td class="width-120 align-center">
                        <asp:Label runat="server">Lang.Comments</asp:Label></td>
                    <td class="action-cell width-20">
                        <img src="img/add.gif" class="addcashreceive" /></td>
                </tr>
            </table>
            <div class="form-group">
                <div class="col-md-12 form-col-without-padding">
                    <div class="input-group input-group-sm">
                        <span class="input-group-addon numofcash"></span>
                        <asp:TextBox ID="CashTotalAmount" runat="server" ReadOnly="True" CssClass="monetary cashtotalamount text-center form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div id='tabBankReceive' class="col-md-6">
            <div class="col-md-12 align-center">
                <asp:Label runat="server">Lang.BankReceive</asp:Label>
            </div>
            <table id="tableBankReceive" class="awesome-table table-with-border zebra">
                <tr class='th'>
                    <td class="width-100 align-center">
                        <asp:Label runat="server">Lang.Amount</asp:Label></td>
                    <td class="width-100  align-center">
                        <asp:Label runat="server">Lang.Bank</asp:Label></td>
                    <td class="width-120 align-center">
                        <asp:Label runat="server">Lang.Comments</asp:Label></td>
                    <td class="width-20 action-cell">
                        <img src="img/add.gif" class="addbankreceive" /></td>
                </tr>
            </table>
            <div class="form-group">
                <div class="col-md-12 form-col-without-padding">
                    <div class="input-group input-group-sm">
                        <span class="input-group-addon numofbank"></span>
                        <asp:TextBox ID="BankTotalAmount" runat="server" ReadOnly="True" CssClass="monetary banktotalamount form-control input-sm text-center"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <div class="row">
        <div id='tabCheckReceive' class='col-md-12'>
            <div class="col-md-12 align-center">
                <asp:Label runat="server">Lang.CheckReceive</asp:Label>
            </div>
            <table id="tableCheckReceive" class="awesome-table table-with-border zebra">
                <tr class='th'>
                    <td class="row-cell width-50">
                        <asp:Label runat="server">Lang.Serial</asp:Label></td>
                    <td class="width-80  align-center">
                        <asp:Label runat="server">Lang.OwnerCheck</asp:Label></td>
                    <td class="width-60  align-center">
                        <asp:Label runat="server">Lang.Bank</asp:Label></td>
                    <td class="width-30 align-center">
                        <asp:Label runat="server">Lang.Branch</asp:Label></td>
                    <td class="width-90 align-center">
                        <asp:Label runat="server">Lang.AccountNo</asp:Label></td>
                    <td class="width-80 align-center">
                        <asp:Label runat="server">Lang.IssueDate</asp:Label></td>
                    <td class="width-80 align-center">
                        <asp:Label runat="server">Lang.DueDate</asp:Label></td>
                    <td class="width-100 align-center">
                        <asp:Label runat="server">Lang.Amount</asp:Label></td>
                    <td class="width-120 align-center">
                        <asp:Label runat="server">Lang.Comments</asp:Label></td>
                    <td class="width-20 action-cell">
                        <img src="img/add.gif" class="addcheckreceive" /></td>
                </tr>
            </table>
            <div class="form-group">
                <div class="col-md-6 form-col-without-padding col-md-offset-3">
                    <div class="input-group input-group-sm">
                        <span class="input-group-addon numofcheck"></span>
                        <input type="text" id="CheckTotalAmount" runat="server" readonly="readonly" class="monetary checktotalamount form-control text-center" />
                    </div>
                </div>
            </div>
        </div>
        <br />
    </div>
    <br />
    <div class="row">
        <div class="form-group">
            <div class="col-md-4 form-col-without-padding col-md-offset-4">
                <div class="input-group input-group-sm">
                    <span class="input-group-addon numoftotallabel width-110"></span>
                    <input type="text" id="TotalTotalAmount" runat="server" readonly="readonly" class="monetary totaltotalamount form-control text-center" />
                    <span class="input-group-addon numoftotal"></span>
                </div>
            </div>
            <div class="col-md-4 form-col-without-padding col-md-offset-4">
                <div class="input-group input-group-sm">
                    <span class="input-group-addon customerremainlable width-110"></span>
                    <input type="text" id="CustomerRemain" runat="server" readonly="readonly" class="monetary customerremain form-control text-center" />
                    <span class="input-group-addon customerremaindiagnosed"></span>
                </div>
            </div>
            <div class="col-md-4 form-col-without-padding col-md-offset-4">
                <div class="input-group input-group-sm">
                    <span class="input-group-addon discountlabel width-110"></span>
                    <input type="text" id="Discount" runat="server" class="monetary discount form-control text-center" />
                    <span class="input-group-addon discountbtn"></span>
                </div>
            </div>
            <div class="col-md-4 form-col-without-padding col-md-offset-4">
                <div class="input-group input-group-sm">
                    <span class="input-group-addon customerremainnewlable width-110"></span>
                    <input type="text" id="CustomerRemainNew" runat="server" readonly="readonly" class="monetary customerremainnew form-control text-center" />
                    <span class="input-group-addon customerremainnewdiagnosed"></span>
                </div>
            </div>
        </div>
    </div>
    <br />
    <div class="align-center">
        <asp:Button runat="server" ID="doSave" OnClick="doSave_Click" Text="Lang.Save" CssClass="btn btn-primary doSave" />
        <asp:Button runat="server" ID="doApply" OnClick="doApply_Click" Text="Lang.DoFinalizeCloseStore" CssClass="btn btn-primary doApply" />
    </div>
    <script>
        function handlCashReceive(node) {
            var cntCashReceive = 0;
            //function sumTheLPBP() {
            //    var sum = 0;
            //    $('.thelpbp').each(function (index, value) {
            //        sum += toFloat($(value).val(), 0) * toFloat($(this).parent().parent().find('.income').val(), 0);
            //    });
            //    $('.sumTtheLPBP').html(Lang.Sum + ': ' + numberWithCommas(sum));
            //    // 
            //}
            function addCashReceive(node) {
                var row = '<tr>';

                row += '<td class="align-center">'
                    + '<input type="text" id="ca_' + cntCashReceive + '" name="CashAmount" value="' + getNodeVal(node, "CashAmount") + '" class="cashamount monetary validate[custom[integer]] form-control align-center input-sm" />'
                    + '</td>';

                row += '<td>'
                    + '<input type="text" id="cn_' + cntCashReceive + '" name="CashName" value="' + getNodeVal(node, "CashName") + '" class="cashname form-control input-sm" />'
                    + '<input type="hidden" id="ct_' + cntCashReceive + '" name="CashTreePath" value="' + getNodeVal(node, "CashTreePath") + '" class="cashtreepath" />'
                    + '</td>';

                row += '<td class="align-center">'
                    + '<input type="text" id="cc_' + cntCashReceive + '" name="CashComment" value="' + getNodeVal(node, "CashComment") + '" class="cashcomment form-control input-sm" />'
                    + '</td>';

                row += '<td class="align-center action-remove">'
                    + '<a id="r_' + cntCashReceive + '" class="btn btn-danger btn-xs btn-block btn-remove removecashreceive">'
                    + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
                    + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
                    + '</a>'
                    + '</td>';

                row += '</tr>';
                cntCashReceive++;
                return row;
            }

            function cashReceiveRemove($who) {
                takeComma();
                var id = $who.attr('id'); // r_1
                id = id.substring(id.indexOf('_') + 1); // r_1  -->  1
                $('#ca_' + id).val(""); // Amount
                $who.parent().parent().hide();
                $who.parent().parent().find(':input').removeClass();
                putAllComma();
                sumCashAmount();
                Growl.Notice(Lang.DeletedSuccessfully);
            }

            function addCashReceiveRow(notify) {
                var table = $('#tableCashReceive');
                $('tr:last', table).after(addCashReceive());
                var lastRow = $('tr:last', table);
                //$('html, body').animate({
                //    scrollTop: $('.doApply').offset().top
                //});
                if (notify)
                    Growl.Notice(Lang.RowAddedSuccessfully);
                $('#tableCashReceive tr:last .cashcomment').keydown(function (e) {
                    if (e.keyCode === 13 && !e.ctrlKey) {
                        e.preventDefault();
                        addCashReceiveRow(true);
                    }
                });
                var url = 'get.aspx?op=Auto&table=Trees&qd=treestypedauto&Type=1' + appendTime();
                autoComplete('Trees', 'treestypedauto', '#tableCashReceive tr:last .cashname', '#justforfun', tableAutoPostPack, formatTreePath, null, null, url);
                $('.removecashreceive', lastRow).click(function () {
                    var element = $(this);
                    doConfirm(function () {
                        cashReceiveRemove(element);
                    }, Lang.ConfirmDelete);
                });
                $('#tableCashReceive tr:last .cashamount').blur(function () {
                    sumCashAmount();
                });
                putComma();
                //monetaryInputsSet();
            }
            $('.addcashreceive').click(function () {
                addCashReceiveRow(true);
            });

            // start with some empty rows
            //for (var i = 0; i < 10; i++)
            //    addServiceRow();
            //$('.doSave').click(function () {
            //    takeComma();
            //});
            //$('.doApply').click(function () {
            //    takeComma();
            //});

            fillTable("get.aspx?op=GetXml&qd=receivefromcustomercash", '#tableCashReceive', addCashReceive);

            if (cntCashReceive == 0) {
                addCashReceiveRow(false);
            }
            var url = 'get.aspx?op=Auto&table=Trees&qd=treestypedauto&Type=1' + appendTime();
            sumCashAmount();
            autoComplete('Trees', 'treestypedauto', '#tableCashReceive .cashname', '#justforfun', tableAutoPostPack, formatTreePath, null, null, url);

            function sumCashAmount() {
                var sumCashAmount = 0;
                var numOfCash = 0;
                $('.cashamount').each(function () {
                    var val = toFloat($(this).val().replace(/\,/g, ''), 0);
                    sumCashAmount += val;
                    numOfCash++;
                });
                $('.cashtotalamount').val(numberWithCommas(sumCashAmount));
                $('.numofcash').html(numOfCash);
                calcTotaltotal();
            }

            $('#tableCashReceive .cashamount').blur(function () {
                sumCashAmount();
            });
        }
        function handleBankReceive() {
            var cntBankReceive = 0;
            function bankReceiveRemove($who) {
                takeComma();
                var id = $who.attr('id'); // r_1
                id = id.substring(id.indexOf('_') + 1); // r_1  -->  1
                $('#ba_' + id).val(""); // bankamount
                $who.parent().parent().hide();
                $who.parent().parent().find(':input').removeClass();
                putAllComma();
                sumBankAmount();
                Growl.Notice(Lang.DeletedSuccessfully);
            };

            function addBankReceive(node) {
                var row = '<tr>';

                row += '<td class="align-center">'
                    + '<input type="text" id="ba_' + cntBankReceive + '" name="BankAmount" value="' + getNodeVal(node, "BankAmount") + '" class="bankamount monetary validate[custom[integer]] form-control align-center input-sm" />'
                    + '</td>';

                row += '<td>'
                    + '<input type="text" id="bn_' + cntBankReceive + '" name="BankName" value="' + getNodeVal(node, "BankName") + '" class="bankname form-control input-sm" />'
                    + '<input type="hidden" id="bt_' + cntBankReceive + '" name="BankTreePath" value="' + getNodeVal(node, "BankTreePath") + '" class="banktreepath" />'
                    + '</td>';

                row += '<td class="align-center">'
                    + '<input type="text" id="bc_' + cntBankReceive + '" name="BankComment" value="' + getNodeVal(node, "BankComment") + '" class="bankcomment form-control input-sm" />'
                    + '</td>';

                row += '<td class="align-center action-remove">'
                    + '<a id="r_' + cntBankReceive + '" class="btn btn-danger btn-xs btn-block btn-remove removebankreceive">'
                    + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
                    + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
                    + '</a>'
                    + '</td>';

                row += '</tr>';
                cntBankReceive++;
                return row;
            }
            function addBankReceiveRow(notify) {
                var table = $('#tableBankReceive');
                $('tr:last', table).after(addBankReceive());
                var lastRow = $('tr:last', table);
                if (notify)
                    Growl.Notice(Lang.RowAddedSuccessfully);

                $('#tableBankReceive tr:last .bankcomment').keydown(function (e) {
                    if (e.keyCode === 13 && !e.ctrlKey) {
                        e.preventDefault();
                        addBankReceiveRow(true);
                    }
                });
                autoComplete('Trees', 'treesbanksauto', '#tableBankReceive tr:last .bankname', '#justforfun', tableAutoPostPack, formatTreePath);
                $('.removebankreceive', lastRow).click(function () {
                    var element = $(this);
                    doConfirm(function () {
                        bankReceiveRemove(element);
                    }, Lang.ConfirmDelete);
                });
                $('#tableBankReceive tr:last .bankamount').blur(function () {
                    sumBankAmount();
                });
                putComma();
            }
            $('.addbankreceive').click(function () {
                addBankReceiveRow(true);
            });

            //$('.doSave').click(function () {
            //    takeComma();
            //});

            //$('.doApply').click(function () {
            //    takeComma();
            //});

            fillTable("get.aspx?op=GetXml&qd=receivefromcustomerbank", '#tableBankReceive', addBankReceive);
            sumBankAmount();
            $('.removebankreceive').click(function () {
                var element = $(this);
                doConfirm(function () {
                    bankReceiveRemove(element);
                }, Lang.ConfirmDelete);
            });

            if (cntBankReceive == 0) {
                addBankReceiveRow(false);
            }

            autoComplete('Trees', 'treesbanksauto', '#tableBankReceive .bankname', '#justforfun', tableAutoPostPack, formatTreePath);
            function sumBankAmount() {
                var sumBankAmount = 0;
                var numOfBank = 0;
                $('.bankamount').each(function () {
                    var val = toFloat($(this).val().replace(/\,/g, ''), 0);
                    sumBankAmount += val;
                    numOfBank++;
                });
                $('.banktotalamount').val(numberWithCommas(sumBankAmount));
                $('.numofbank').html(numOfBank);
                calcTotaltotal();
            }

            $('#tableBankReceive .bankamount').blur(function () {
                sumBankAmount();
            });
        }
        function handleCheckReceive(node) {
            var cntCheckReceive = 0;
            function addCheckReceive(node) {
                var row = '<tr>';

                row += '<td><input type="hidden" name="ID2" value="-1" />'
                    + '<input type="text" name="Serial" id="sr_' + cntCheckReceive + '"  class="form-control input-sm" value="' + getNodeVal(node, "Serial") + '" />'
                    + '</td>';

                row += '<td>'
                    + '<input type="text" id="oc_' + cntCheckReceive + '" name="OwnerCheckName" value="' + getNodeVal(node, "OwnerCheckName") + '" class="ownercheckname form-control input-sm" />'
                    + '</td>';

                row += '<td>'
                    + '<input type="text" id="ba_' + cntCheckReceive + '" name="BankAccountName" value="' + getNodeVal(node, "BankAccountName") + '" class="bankaccountname form-control input-sm" />'
                    + '<input type="hidden" id="baid_' + cntCheckReceive + '" name="BankAccountID" value="' + getNodeVal(node, "BankAccountID") + '" class="bankaccountid" />'
                    + '</td>';

                row += '<td class="align-center">'
                    + '<input type="text" id="br_' + cntCheckReceive + '" name="Branch" value="' + getNodeVal(node, "Branch") + '" class="branch form-control align-center input-sm" />'
                    + '</td>';

                row += '<td class="align-center">'
                   + '<input type="text" id="an_' + cntCheckReceive + '" name="AccountNo" value="' + getNodeVal(node, "AccountNo") + '" class="accountno validate[custom[integer]] form-control align-center input-sm" />'
                   + '</td>';

                row += '<td class="align-center">'
                    + '<input type="text" id="is_' + cntCheckReceive + '" name="IssueDate' + cntCheckReceive + '" value="' + getNodeVal(node, "IssueDate") + '" class=" dateSelect IssueDate form-control input-sm" />'
                    + '</td>';

                row += '<td class="align-center">'
                    + '<input type="text" id="du_' + cntCheckReceive + '" name="DueDate' + cntCheckReceive + '" value="' + getNodeVal(node, "DueDate") + '" class=" dateSelect DueDate form-control input-sm" />'
                    + '</td>';

                row += '<td class="align-center">'
                   + '<input type="text" id="am_' + cntCheckReceive + '" name="Amount" value="' + getNodeVal(node, "Amount") + '" class="checkamount monetary validate[custom[integer]] form-control align-center input-sm" />'
                   + '</td>';

                row += '<td class="align-center">'
                    + '<input type="text" id="chc_' + cntCheckReceive + '" name="CheckComment" value="' + getNodeVal(node, "CheckComment") + '" class="checkcomment form-control input-sm" />'
                    + '</td>';

                row += '<td class="align-center action-remove">'
                + '<a id="r_' + cntCheckReceive + '" class="btn btn-danger btn-xs btn-block btn-remove removecheckreceive">'
                + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
                + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
                + '</a>'
                + '</td>';

                row += '</tr>';
                cntCheckReceive++;
                return row;
            }

            function checkReceiveRemove($who) {
                takeComma();
                var id = $who.attr('id'); // r_1
                id = id.substring(id.indexOf('_') + 1); // r_1  -->  1
                $('#sr_' + id).val(""); // Serial 
                $('#baid_' + id).val(-1); // BankAccountID
                $who.parent().parent().hide();
                $who.parent().parent().find(':input').removeClass();
                putAllComma();
                sumCheckAmount();
                Growl.Notice(Lang.DeletedSuccessfully);
            };

            function addCheckReceiveRow(notify) {
                var table = $('#tableCheckReceive');
                $('tr:last', table).after(addCheckReceive(null));
                var lastRow = $('tr:last', table);
                if (notify)
                    Growl.Notice(Lang.RowAddedSuccessfully);
                dateSelect('#tableCheckReceive tr:last .dateSelect');
                $('#tableCheckReceive tr:last .checkcomment').keydown(function (e) {
                    if (e.keyCode === 13 && !e.ctrlKey) {
                        e.preventDefault();
                        addCheckReceiveRow(true);
                    }
                });

                //autoComplete('Personnel', 'personnelselectauto', '#tableCheckReceive tr:last .personnelname', '#justforfun', tableAutoPostPack
                //    , function (data, i, n, value) {
                //        //$('#subDetailForm #PersonnelID').val('');
                //        return formatPersonnelItem(data);
                //    });

                autoComplete('BankAccountCheck', 'bankaccountautoforchecks', '#tableCheckReceive tr:last .bankaccountname', '#justforfun', tableAutoPostPack);

                $('.removecheckreceive', lastRow).click(function () {
                    var element = $(this);
                    doConfirm(function () {
                        checkReceiveRemove(element);
                    }, Lang.ConfirmDelete);
                });
                $('#tableCheckReceive tr:last .checkamount').blur(function () {
                    sumCheckAmount();
                });
                putComma();
            }
            $('.addcheckreceive').click(function () {
                addCheckReceiveRow(true);
            });

            // start with some empty rows
            //for (var i = 0; i < 10; i++)
            //    addServiceRow();
            fillTable("get.aspx?op=GetXml&qd=receivefromcustomercheck", '#tableCheckReceive', addCheckReceive);
            autoComplete('BankAccountCheck', 'bankaccountautoforchecks', '#tableCheckReceive .bankaccountname', '#justforfun', tableAutoPostPack);
            sumCheckAmount();
            $('.removecheckreceive').click(function () {
                var element = $(this);
                doConfirm(function () {
                    checkReceiveRemove(element);
                }, Lang.ConfirmDelete);
            });
            if (cntCheckReceive == 0)
                addCheckReceiveRow(false);

            function sumCheckAmount() {
                var sumCheckAmount = 0;
                var numOfCheck = 0;
                $('.checkamount').each(function () {
                    var val = toFloat($(this).val().replace(/\,/g, ''), 0);
                    sumCheckAmount += val;
                    numOfCheck++;
                });
                $('.checktotalamount').val(numberWithCommas(sumCheckAmount));
                $('.numofcheck').html(numOfCheck);
                calcTotaltotal();
            }

            $('#tableCheckReceive .checkamount').blur(function () {
                sumCheckAmount();
            });
        }
        function putComma() {
            $('.monetary').keyup(function () {
                $(this).val(numberWithCommasAllowDP(stripCommas($(this).val())));
            });
            $('.monetary').keyup(handleMonetaryKeys);
        }
        function takeComma() {
            $('.monetary').each(function () {
                $(this).val(fromMonetary($(this).val().replace(/\,/g, '')));
            });
        }

        function putAllComma() {
            $('.monetary').each(function () {
                $(this).val(numberWithCommas(toMonetary($(this).val())));
            });
        }
        function calcTotaltotal() {
            var numoftotal = toInt($('.numofcash').html(), 0) + toInt($('.numofbank').html(), 0) + toInt($('.numofcheck').html(), 0);
            $('.numoftotal').html(numoftotal);
            var totalTotalAmount = toFloat($('.checktotalamount').val().replace(/\,/g, ''), 0) + toFloat($('.banktotalamount').val().replace(/\,/g, ''), 0) + toFloat($('.cashtotalamount').val().replace(/\,/g, ''), 0);
            $('.totaltotalamount').val(numberWithCommas(totalTotalAmount));
            calcCustomerNewRemain();
        }

        function calcCustomerNewRemain() {
            var pay = toFloat($('.totaltotalamount').val().replace(/\,/g, ''), 0) + toFloat($('.discount').val().replace(/\,/g, ''), 0);
            var oldRemain = toFloat($('.customerremain').val().replace(/\,/g, ''), 0);
            var diagnosed = $('.customerremaindiagnosed').html();
            var newRemain = 0;
            var newDiagnosed = Lang.Li;
            if (diagnosed == Lang.Li) {
                newRemain = oldRemain - pay;
                if (newRemain < 0) {
                    newRemain = newRemain * -1;
                    newDiagnosed = Lang.Cr;
                }
            } else {
                newRemain = oldRemain + pay;
                newDiagnosed = Lang.Cr;
            }
            $('.customerremainnewdiagnosed').html(newDiagnosed);
            $('.customerremainnew').val(numberWithCommas(newRemain));
        }
        function setLang() {
            $('.numoftotallabel').html(Lang.TotalS);
            $('.customerremainlable').html(Lang.CustomerRemain);
            $('.customerremainnewlable').html(Lang.CustomerRemainNew);
            $('.discountlabel').html(Lang.Discount);
            $('.choosedoc').html(Lang.ItemTypeBool);
            $('.discountbtn').html('<a class="accountcheckout" href="#" >' + Lang.AccountCheckout1 + '</a>');
            $('.accountcheckout').click(function () {
                var pay = toFloat($('.totaltotalamount').val().replace(/\,/g, ''), 0);
                var oldRemain = toFloat($('.customerremain').val().replace(/\,/g, ''), 0);
                var diagnosed = $('.customerremaindiagnosed').html();
                if (oldRemain > 0 && diagnosed == Lang.Li && pay < oldRemain)
                    $('.discount').val(numberWithCommas(oldRemain - pay));
                calcCustomerNewRemain();
            });
        }
        $(function () {
            $('.sidebar').addClass('collapsed');
            $('.userid').val(_Context.userID);
            //tabify();
            autoCompleteFree('Personnel', 'personnelselectauto', '.customername', '.customerid',
                function (e, data) {
                    $('.customerid').val(data[0]);
                    var remained = data[2];
                    if (remained < 0) {
                        $('.customerremain').val(numberWithCommas(remained * -1));
                        $('.customerremaindiagnosed').html(Lang.Li);
                    } else if (remained > 0) {
                        $('.customerremain').val(numberWithCommas(remained));
                        $('.customerremaindiagnosed').html(Lang.Cr);
                    } else {
                        $('.customerremain').val(0);
                        $('.customerremaindiagnosed').html(Lang.Li);
                    }
                    calcCustomerNewRemain();
                }, function (data, i, n, value) {
                    //$('#BuyerID').val('');
                    return formatPersonnelItem(data);
                });
            handlCashReceive();
            handleBankReceive();
            handleCheckReceive();
            dateSelect('.dateselect');
            setLang();
            autoComplete('DocumentAccounting', 'documentaccountingsearch', '.documentAccountingno', '.documentAccountingno', null, formatDocumentAccounting
                        , function (data) {
                            return data[0];
                        });
            $('.discount').blur(function () {
                calcCustomerNewRemain();
            });
            $('.choosedoc').click(function () {
                $('.choosedocspan').addClass('hidden');
                $('.newdoc').removeClass('hidden');
                $('.documentAccountingno').removeClass('hidden');
            });
            $('.newdoc').click(function () {
                $('.choosedocspan').removeClass('hidden');
                $('.newdoc').addClass('hidden');
                $('.documentAccountingno').addClass('hidden');
                $('.documentAccountingno').val('');
            });
            $('.doSave').click(function () {
                $('.IssueDate').attr('name', 'IssueDate');
                $('.DueDate').attr('name', 'DueDate');
                takeComma();
            });
            $('.doApply').click(function () {
                $('.IssueDate').attr('name', 'IssueDate');
                $('.DueDate').attr('name', 'DueDate');
                takeComma();
            });
            putComma();
            putAllComma();
            var newDocId = toInt($('.newdocumentAccountingid').val(), 0);
            if (newDocId > 0) {
                $('.customerremain, .customerremainnew, .discount').val(0);
                var newDocNo = $('.documentAccountingno').val();
                doConfirm(function () {
                    location.href = 'list.aspx?table=DocumentAccounting&id=' + newDocId;
                }, Lang.ReceiveFromCustomerDocShow);
            }
        });
    </script>
</asp:Content>

