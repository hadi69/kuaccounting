﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class profile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        rq1.ErrorMessage = "(" + LangProvider.Instance["Mandatory"] + ")";
        rq2.ErrorMessage = "(" + LangProvider.Instance["Mandatory"] + ")";
        Re.ErrorMessage = "(" + LangProvider.Instance["Mandatory"] + ")";
        cmp.ErrorMessage = LangProvider.Instance["ProfileAspxM1"];
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");
        Username.Text = SiteSettings.User.Username;
        Fullname.Text = SiteSettings.User.Fullname;
    }
    protected void doUpdate_Click(object sender, EventArgs e)
    {
        if (SiteSettings.User.Password != OldPassword.Text)
        {
            ShowMessage(LangProvider.Instance["ProfileAspxM2"]);
            return;
        }
        if (Password.Text != Password2.Text)
        {
            ShowMessage(LangProvider.Instance["ProfileAspxM1"]); 
            return;
        }
        if (Password.Text.Trim().Length == 0)
        {
            ShowMessage(LangProvider.Instance["ProfileAspxM3"]);
            return;
        }
        using (DbProxy proxy = new DbProxy())
        {
            if (proxy.UpdateUserPass(SiteSettings.UserID, Password.Text))
            {
                ShowMessage(LangProvider.Instance["ProfileAspxM4"], "success");
                return;
            }
            else
            {
                ShowMessage(LangProvider.Instance["ProfileAspxM5"] +proxy.LastException.Message);
                return;
            }
        }
    }
    private void ShowMessage(string msg, string type="danger")
    {
        if (type != "success" && type != "danger")
        {
            type = "danger";
        }

        mMsg.Text = msg;
        mMsg.Visible = true;
        mMsg.CssClass += " alert-" + type;
    }
}