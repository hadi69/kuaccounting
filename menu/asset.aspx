﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="asset.aspx.cs" Inherits="menu_asset" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div class="row">
        <JP:PageHeader ID="pageHeader" runat="server" Text="Lang.MAsset" />
    </div>
    
    <div id="pillsContainer" runat="server" class="row sub-system system-asset">
        <JP:Pill ID="pillOrgUnit" runat="server" Access="asset,orgunit" IconClass="sprites-topics-32" Text="Lang.OrgUnit" Href="list.aspx?table=OrgUnit" />
        <JP:Pill ID="pillRuleGroup" runat="server" Access="asset,rulegroup" IconClass="sprites-topicgroup-32" Text="Lang.RuleGroup" Href="list.aspx?table=RuleGroup" />
        <JP:Pill ID="pillSeating" runat="server" Access="asset,seating" IconClass="sprites-ward-32" Text="Lang.Seating" Href="list.aspx?table=Seating" />
        <JP:Pill ID="pillAssetLevel" runat="server" Access="asset,assetlevel" IconClass="sprites-workflow-32" Text="Lang.AssetLevel" Href="list.aspx?table=AssetLevel" />
        <JP:Pill ID="pillDecayRule" runat="server" Access="asset,decayrule" IconClass="sprites-workflow-32" Text="Lang.DecayRule" Href="list.aspx?table=DecayRule" />
        <JP:Pill ID="pillAssetGroup" runat="server" Access="asset,assetgroup" IconClass="sprites-workflow-32" Text="Lang.AssetGroup" Href="list.aspx?table=AssetGroup" />
        <JP:Pill ID="pillAsset" runat="server" Access="asset,asset" IconClass="sprites-workflow-32" Text="Lang.Asset" Href="list.aspx?table=Asset" />
        <JP:Pill ID="pillAssetDecay" runat="server" Access="asset,assetdecay" IconClass="sprites-workflow-32" Text="Lang.AssetDecay" Href="list.aspx?table=AssetDecay" />
  </div>

    <input type="hidden" runat="server" id="isadmin" class="isadmin" />
    <script type="text/javascript">
        $(function () {
            checkAccess(['mainmenu,asset'], '../');
            var isadmin = $('.isadmin').val();
            if (isadmin != '1') {
                $(".system-asset a").each(enableMenu);
            }
            setClassAccounting();
            if (_Context.isAccountingVer)
                $('.noacc').hide();
        });
    </script>
</asp:Content>
