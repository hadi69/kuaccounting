﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="salaryreports.aspx.cs" Inherits="menu_salaryreports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" Runat="Server">
    <div class="row">
        <JP:PageHeader ID="pageHeader" runat="server" Text="Lang.SalaryReports" />
    </div>
    
    <div id="pillsContainer" runat="server" class="row sub-system system-salary">
        <JP:Pill ID="pillWageReports" runat="server"  Href="reports/Recruitment/Wage.aspx" Access="salary,wagereports" Text="Lang.Wage" />
        <JP:Pill ID="pillFieldReports" runat="server" Href="reports/Recruitment/FieldReport.aspx" Access="salary,fieldreport" Text="Lang.FieldReport" />
        <JP:Pill ID="pillFishSet" runat="server" Href="reports/Recruitment/FishSet.aspx" Access="salary,fishset" Text="Lang.PrintFishSet" />
        <JP:Pill ID="pillLoanReport" runat="server" Href="reports/Recruitment/LoanReport.aspx" Access="salary,loanreport" Text="Lang.LoanReport" />
    </div>
    <script type="text/javascript">
        $(function () {
            checkAccess(['mainmenu,salary'], '../');
            $(".system-salary a").each(enableMenu);
            //setClassSalary();
        });
    </script>
</asp:Content>

