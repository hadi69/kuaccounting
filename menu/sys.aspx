﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="sys.aspx.cs" Inherits="menu_sys" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">

    <div class="row">
        <JP:PageHeader ID="pageHeader1" runat="server" Text="Lang.MManagement" />
    </div>
    
    <div id="pillsContainer" runat="server" class="row sub-system system-sys">
        <JP:Pill ID="pillWorkflow" runat="server" Level="3" Access="sys,workflow" IconClass="sprites-workflow-32" Text="Lang.Workflow" Href="list.aspx?table=Workflow" />
        <JP:Pill ID="pillLang" runat="server" Level="3" Access="sys,langs" IconClass="sprites-lang-32" Text="Lang.LangEdit" Href="list.aspx?view=langs" />
        <JP:Pill ID="pillProgram" runat="server" Level="3" Access="sys,program" IconClass="sprites-program-32" Text="Lang.IndependentAccounts" Href="list.aspx?table=Program&menu=sysa" />
        <JP:Pill ID="pillMonetaryunit" runat="server" Level="3" Access="sys,monetaryunit" IconClass="sprites-monetaryunit-32" Text="Lang.MonetaryUnit" Href="list.aspx?table=MonetaryUnit&menu=sysa" />
        <JP:Pill ID="pillConversion" runat="server" Level="3" Access="sys,conversion" IconClass="sprites-conversion-32" Text="Lang.Conversion" Href="list.aspx?table=Conversion&menu=sysa" />
        <JP:Pill ID="pillFormDeclare" runat="server" Level="3" Access="sys,formbuild" IconClass="sprites-form-32" Text="Lang.FormDeclare" Href="list.aspx?table=FormDeclare" />
        <JP:Pill ID="pillSystem" runat="server" Level="3" Access="sys,subsystem" IconClass="sprites-system-32" Text="Lang.Subsystem" Href="list.aspx?table=subsystem" />
        <JP:Pill ID="pillForm" runat="server" Level="3" Access="sys,form" IconClass="sprites-form-32" Text="Lang.SystemForms" Href="list.aspx?table=form" />
        <JP:Pill ID="pillReports" runat="server" Level="3" Access="sys,reports" IconClass="sprites-report-32" Text="Lang.DynamicReport" Href="list.aspx?table=reports" Visible="False" />
        <JP:Pill ID="pillLogs" runat="server" Level="3" Access="sys,logs" IconClass="sprites-logs-32" Text="Lang.ErrorReport" Href="list.aspx?table=logs" />
        <JP:Pill ID="pillProvince" runat="server"  Level="3" Access="sys,province" IconClass="sprites-form-32" Text="Lang.ProvinceName" Href="list.aspx?table=Province&menu=sys" />
        <JP:Pill ID="pillCity" runat="server"  Level="3" Access="sys,city" IconClass="sprites-form-32" Text="Lang.City" Href="list.aspx?table=City&menu=sys" />
        <JP:Pill ID="pillTheNeighbourhood" runat="server"  Level="3" Access="sys,theneighbourhood" IconClass="sprites-form-32" Text="Lang.TheNeighbourhood" Href="list.aspx?table=TheNeighbourhood&menu=sys" />
       <asp:PlaceHolder runat="server" ID="mFreeForms"></asp:PlaceHolder>
    </div>

    <script type="text/javascript">
        $(function () {
            checkAccess(['mainmenu,sys'], '../');
            checkLevel(3, '../');
            var isadmin = $('.isadmin').val();
            if (isadmin != '1') {
                $(".system-sys a").each(enableMenu);
            }
            setClassSys();
        });
    </script>
</asp:Content>


