﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="outputfiles.aspx.cs" Inherits="menu_outputfiles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <JP:PageHeader ID="PageFiles" runat="server" Text="Lang.OutputFiles" />

    <JP:Pill ID="pillTaxFields" runat="server" Href="list.aspx?table=ReportFields&view=taxfields&menu=salarysa" Access="salary,taxfields" Text="Lang.TaxFields" />
    <JP:Pill ID="pillInsuranceFields" runat="server" Href="list.aspx?table=ReportFields&view=insurancefields&menu=salarysa" Access="salary,insurancefields" Text="Lang.InsuranceFields" />
    <JP:Pill ID="pillTax" runat="server" Href="Tax.aspx" Access="salary,tax" Text="Lang.TaxAffairs" />
    <JP:Pill ID="pillInsurande" runat="server" Href="Insurance.aspx" Access="salary,insurance" Text="Lang.Insurance" />
    <JP:Pill ID="pillBankFile" runat="server" Href="BankFile.aspx" Access="salary,bankfile" Text="Lang.BankFile" />
</asp:Content>

