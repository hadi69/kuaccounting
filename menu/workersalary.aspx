﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="workersalary.aspx.cs" Inherits="menu_workersalary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" Runat="Server">
    <div class="row">
        <JP:PageHeader ID="pageHeader" runat="server" Text="Lang.MWorkerSalary" />
    </div>
    
    <div id="pillsContainer" runat="server" class="row sub-system system-salary">
        
        <JP:Pill ID="pillFish" runat="server" Href="list.aspx?table=Rate&menu=salarysa" Access="salary,rate" Text="Lang.RatesTable" />
        <JP:Pill ID="pill1" runat="server" Href="list.aspx?table=SalaryFish&menu=salarysa" Access="salary,salaryfish" Text="Lang.Fish" />
    </div>
    <script type="text/javascript">
        $(function () {
            checkAccess(['mainmenu,salary'], '../');
            $(".system-salary a").each(enableMenu);
            //setClassSalary();
        });
    </script>
</asp:Content>

