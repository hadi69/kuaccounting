﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true"
    CodeFile="store.aspx.cs" Inherits="menu_store" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div class="row">
        <JP:PageHeader ID="pageHeader" runat="server" Text="Lang.Store" />
    </div>

    <div id="pillsContainer" runat="server" class="row sub-system system-store">
        <JP:Pill ID="pillMeasurementUnits" runat="server" Access="stores,units" IconClass="sprites-units-32" Text="Lang.MeasurementUnits" Href="list.aspx?table=Units&menu=storea" />
        <JP:Pill ID="pillServiceType" runat="server" Access="stores,servicetype" IconClass="sprites-producttype-32" Text="Lang.ServiceType" Href="list.aspx?table=ServiceType&menu=storea" />
        <JP:Pill ID="pillCommodity" runat="server" Access="stores,service" IconClass="sprites-product-32" Text="Lang.Commodity" Href="list.aspx?table=Service&menu=storea" />
        <!--<JP:Pill ID="pillStoreNature" runat="server" Access="stores,storecategory" IconClass="sprites-storecategory-32" Text="Lang.StoreNature" Href="list.aspx?table=StoreCategory&menu=storea" />-->
        <JP:Pill ID="pillStore" runat="server" Access="stores,store" IconClass="sprites-store-32" Text="Lang.Store" Href="list.aspx?table=Stores&menu=storea" />
        <JP:Pill ID="pillWFTTransfer" runat="server" Level="2" Access="stores,transfer" IconClass="sprites-transfer-32" Text="Lang.WFTTransfer" Href="list.aspx?table=Transfer&menu=storea" />
<%--        <JP:Pill ID="pillWFTPurchase" runat="server" Access="stores,purchase" IconClass="sprites-purchase-32" Text="Lang.WFTPurchase" Href="list.aspx?table=Purchase&menu=storea" />
        <JP:Pill ID="pillWFTStoreDraft" runat="server" Access="stores,storedraft" IconClass="sprites-storedraft-32" Text="Lang.WFTStoreDraft" Href="list.aspx?table=StoreDraft&menu=storea" />
        <JP:Pill ID="pillWFTRefuse" runat="server" Access="stores,refuse" IconClass="sprites-refuse-32" Text="Lang.WFTRefuse" Href="list.aspx?table=Refuse&menu=storea" />
        <JP:Pill ID="pillWFTReturn" runat="server" Access="stores,returns" IconClass="sprites-return-32" Text="Lang.WFTReturn" Href="list.aspx?table=Returns&menu=storea" />--%>
        <asp:PlaceHolder runat="server" ID="mFreeForms"></asp:PlaceHolder>
    </div>

    <script type="text/javascript">
        $(function () {
            checkAccess(['mainmenu,stores'], '../');
            var isadmin = $('.isadmin').val();
            $(".system-store a").each(enableMenu);
            setClassStore();
        });
    </script>
</asp:Content>
