﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="basics.aspx.cs" Inherits="menu_basics" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" Runat="Server">
<div id='basicsMenu' class="basicsMenu menu" runat="server">
    <div class="column">
            <asp:Label ID="Label1" runat="server"><h3>Lang.MBasics</h3></asp:Label>
        <ul>
            <li><a href="../list.aspx?table=Country&menu=basicsa" access="basics,country"><img src="../img/invoice.png" /><asp:Label ID="Label4" runat="server">Lang.Country</asp:Label></a></li>
            <li><a href="../list.aspx?table=Province&menu=basicsa" access="basics,province"><img src="../img/invoice.png" /><asp:Label ID="Label3" runat="server">Lang.ProvinceName</asp:Label></a></li>
            <li><a href="../list.aspx?table=City&menu=basicsa" access="basics,city"><img src="../img/invoice.png" /><asp:Label ID="Label5" runat="server">Lang.City</asp:Label></a></li>
        </ul>
    </div>
    <div class="column">
            <asp:Label ID="Label2" runat="server"><h3>Lang.Facilities</h3></asp:Label>
        <ul>
            <li><a href="../list.aspx?table=Address&menu=destribution" access="destribution,address"><img src="../img/invoice.png" /><asp:Label ID="Label6" runat="server">Lang.Address</asp:Label></a></li>
        </ul>
    </div>
</div>
    <script type="text/javascript">
        $(function () {
            checkAccess(['mainmenu,basics'], '../');
            $(".basicsMenu a").each(enableMenu);
            setClassBasic();
            if (_Context.isAccountingVer)
                $('.noacc').hide();
        });
    </script>
</asp:Content>

