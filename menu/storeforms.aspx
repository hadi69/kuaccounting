﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="storeforms.aspx.cs" Inherits="menu_storeforms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div class="row">
        <JP:PageHeader ID="pageHeader" runat="server" Text="Lang.MStoreForms" />
    </div>
    
    <div id="pillsContainer" runat="server" class="row sub-system system-storeforms">
        <asp:PlaceHolder runat="server" ID="mFreeForms"></asp:PlaceHolder>
    </div>

    <script type="text/javascript">
        $(function () {
            checkAccess(['mainmenu,storeforms'], '../');
            var isadmin = $('.isadmin').val();
            $(".system-storeforms a").each(enableMenu);
            setClassStoreForms();
        });
    </script>
</asp:Content>
