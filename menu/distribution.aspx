﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="distribution.aspx.cs" Inherits="menu_distribution" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div class="row">
        <JP:PageHeader ID="pageHeader" runat="server" Text="Lang.MDistribution" />
    </div>
    
    <div id="pillsContainer" runat="server" class="row sub-system system-distribution">
        <JP:Pill ID="pillVisitorFixedRoutes" runat="server" Access="distribution,visitorfixedroutes" IconClass="sprites-visitor-32" Text="Lang.VisitorFixedRoutes" Href="list.aspx?table=VisitorFixedRoutes" />
        <JP:Pill ID="pillHoliDays" runat="server" Access="distribution,holidays" IconClass="sprites-holiday-32" Text="Lang.Holidays" Href="list.aspx?table=HoliDays&view=holidays" />
        <JP:Pill ID="pillVisitorPlan" runat="server" Access="distribution,visitorplan" IconClass="sprites-visitor-32" Text="Lang.VisitorPlan" Href="list.aspx?table=VisitorPlan" />
        <JP:Pill ID="pillVisitorsPerformanceReport" runat="server" Access="distribution,visitorsperformancereport" IconClass="sprites-visitor-32" Text="Lang.VisitorsPerformanceReport" Href="reports/accounting/VisitorsPerformanceReport.aspx" />
        <JP:Pill ID="pillCooperationType" runat="server" Level="2" Access="distribution,cooperationtype" IconClass="sprites-cooperationtype-32" Text="Lang.CooperationType" Href="list.aspx?table=CooperationType" />
        <JP:Pill ID="pillOffers" runat="server" Access="distribution,offers" IconClass="sprites-invoice-32" Text="Lang.Offers" Href="list.aspx?table=Offers&menu=basicsa" />
        <JP:Pill ID="pillAwards" runat="server" Access="distribution,awards" IconClass="sprites-awards-32" Text="Lang.Awards" Href="list.aspx?table=Awards&view=awards&menu=basicsa" />
        <JP:Pill ID="pillGroupSearch" runat="server" Access="distribution,groupsearch" IconClass="sprites-invoice-32" Text="Lang.GroupSearch" Href="list.aspx?table=Grouping&view=groupsearch&menu=basicsa" />
        <JP:Pill ID="pillDailyPlan" runat="server" Access="distribution,dailyplan" IconClass="sprites-visitor-32" Text="Lang.DailyPlan" Href="list.aspx?table=VisitorCustomersStatus&view=visitorcustomersstatuss" />
        <JP:Pill ID="pillTabletWorkFlow" runat="server" Access="distribution,tabletworkflow" IconClass="sprites-workflow-32" Text="Lang.TabletWorkFlow" Href="list.aspx?table=TabletWorkFlow" />
        <JP:Pill ID="pillLoadDistribute" runat="server" Access="distribution,loaddistribute" IconClass="sprites-performancereport-32" Text="Lang.LoadDistribute" Href="LoadDistribute.aspx" />
        <JP:Pill ID="pillLoadDistributeCheckOut" runat="server" Access="distribution,loaddistributecheckout" IconClass="sprites-accountcheckout-32" Text="Lang.LoadDistributeCheckOut" Href="list.aspx?table=StoreDraft&view=loaddistributecheckout" />
        <JP:Pill ID="pillHotSaleCheckout" runat="server" Access="distribution,hotsalecheckout" IconClass="sprites-accountcheckout-32" Text="Lang.HotSaleCheckout" Href="list.aspx?table=VisitorPlanDayService&view=hotsalecheckout" />
        <JP:Pill ID="pillAssessment" runat="server" Access="distribution,assessment" IconClass="sprites-accountcheckout-32" Text="Lang.Assessment" Href="list.aspx?table=AssessmentField" />
        <JP:Pill ID="pillDoAssessment" runat="server" Access="distribution,doassessment" IconClass="sprites-accountcheckout-32" Text="Lang.DoAssessment" Href="DoAssessment.aspx" />
        <JP:Pill ID="pillCalculatePursant" runat="server" Access="distribution,calculatepursant" IconClass="sprites-accountcheckout-32" Text="Lang.CalculatePursant" Href="CalculatePursant.aspx" />
        <JP:Pill ID="pillVisitorOffers" runat="server" Access="distribution,visitoroffers" IconClass="sprites-visitor-32" Text="Lang.VisitorOffers" Href="help/notfound.htm" />
        <JP:Pill ID="pillPerformanceReport" runat="server" Access="distribution,performancereport" IconClass="sprites-performancereport-32" Text="Lang.PerformanceReport" Href="help/notfound.htm" />
        <JP:Pill ID="pillContactCenter" runat="server" Access="distribution,contactcenter" IconClass="sprites-contactcenter-32" Text="Lang.ContactCenter" Href="help/notfound.htm" />
        <JP:Pill ID="pillMap" runat="server" Access="distribution,map" IconClass="sprites-map" Text="Lang.OnlineMap" Href="map.aspx" />
        <JP:Pill ID="pillVMap" runat="server" Access="distribution,map" IconClass="sprites-map" Text="Lang.ShowPath" Href="vmap.aspx" />
        <JP:Pill ID="pillMsg" runat="server" Access="distribution,tabletmsg" IconClass="sprites-contactcenter-32" Text="Lang.TabletMsg" Href="list.aspx?table=TabletMsg" />
        <JP:Pill ID="pillNoVisitReason" runat="server" Level="3" Access="distribution,novisitreason" IconClass="sprites-monetaryunit-32" Text="Lang.NoVisitReason" Href="list.aspx?table=NoVisitReason" />
        <JP:Pill ID="pillClaim" runat="server" Level="3" Access="distribution,claim" IconClass="sprites-monetaryunit-32" Text="Lang.Claim" Href="list.aspx?table=claim" />
        <asp:PlaceHolder runat="server" ID="mFreeForms"></asp:PlaceHolder>
    </div>

    <script type="text/javascript">
        $(function () {
            checkAccess(['mainmenu,distribution'], '../');
            $(".system-distribution a").each(enableMenu);
            setClassDistribution('../');
            if (_Context.isAccountingVer)
                $('.noacc').hide();
        });
    </script>
</asp:Content>

