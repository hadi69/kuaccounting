﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class menu_basics : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.SetLang();
        if (Request.QueryString["ajax"] == "true")
        {
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            basicsMenu.RenderControl(htw);
            htw.Flush();
            string contents = sw.ToString();
            contents = contents.Replace("../list", "list");
            Response.Clear();
            Response.Write(contents);
            Response.End();
        }
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");
    }
}