﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="admin.aspx.cs" Inherits="menu_admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div class="row">
        <JP:PageHeader ID="pageHeader" runat="server" Text="Lang.Management" />
    </div>


    <div id="pillsContainer" runat="server" class="row sub-system system-admin">
        <JP:Pill ID="pillConfig" runat="server" Access="admin,config" IconClass="sprites-config-32" Text="Lang.Settings" Href="list.aspx?table=config" />
        <JP:Pill ID="pillRoles" runat="server" Access="admin,roles" IconClass="sprites-role-32" Text="Lang.Permission" Href="list.aspx?table=roles" />
        <JP:Pill ID="pillUsers" runat="server" Access="admin,users" IconClass="sprites-users-32" Text="Lang.Users" Href="list.aspx?table=users" />
        <JP:Pill ID="pillDbAdmin" runat="server" Access="admin,database" IconClass="sprites-dbadmin-32" Text="Lang.DataBaseAdmin" Href="dbadmin.aspx" />
        <JP:Pill ID="pillCloseTopics" runat="server" Access="admin,close" IconClass="sprites-closetopic-32" Text="Lang.CloseTopics" Href="CloseTopics.aspx" />
        <JP:Pill ID="pillFinanceYear" runat="server" Access="admin,financeyear" IconClass="sprites-financeyear-32" Text="Lang.FinanceYear" Href="list.aspx?table=FinanceYear" />
        <JP:Pill ID="pillOpenTopics" runat="server" Access="admin,open" IconClass="sprites-opentopic-32" Text="Lang.ActivateYear" Href="OpenTopics.aspx" />
        <JP:Pill ID="pillStart" runat="server" Access="admin,start" IconClass="sprites-getprice-32" Text="Lang.Start" Href="start.aspx" />
        <JP:Pill ID="pillCloseStore" runat="server" Access="admin,storehandling" IconClass="sprites-getprice-32" Text="Lang.CloseStore" Href="list.aspx?table=StoreHandling" />
        <JP:Pill ID="pillMonetaryunit" runat="server" Access="sys,monetaryunit" IconClass="sprites-monetaryunit-32" Text="Lang.MonetaryUnit" Href="list.aspx?table=MonetaryUnit&menu=sysa" />
        <JP:Pill ID="pillConversion" runat="server" Access="sys,conversion" IconClass="sprites-conversion-32" Text="Lang.Conversion" Href="list.aspx?table=Conversion&menu=sysa" />
        <JP:Pill ID="pillLang" runat="server" Access="sys,langs" IconClass="sprites-lang-32" Text="Lang.LangEdit" Href="list.aspx?view=langs" />
      <asp:PlaceHolder runat="server" ID="mFreeForms"></asp:PlaceHolder> 
    </div>

    <script type="text/javascript">
        $(function () {
            checkAccess(['mainmenu,admin'], '../');            
            var isadmin = $('.isadmin').val();
            if (isadmin != '1') {
                $(".system-admin a").each(enableMenu);
            }
            setClassManager();
        });
    </script>
</asp:Content>

