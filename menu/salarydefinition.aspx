﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="salarydefinition.aspx.cs" Inherits="menu_salarydefinition" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" Runat="Server">
    
    <div id="pillsContainer" runat="server" class="row sub-system system-salary">
        
        <JP:PageHeader ID="pageHeadBasic" runat="server" Text="Lang.MBasics" />

        <JP:Pill ID="pillEnums" runat="server" Href="list.aspx?table=Enums&menu=salarysa" Access="salary,enums" Text="Lang.DefineBasicData" />
        <JP:Pill ID="pillSection" runat="server" Href="list.aspx?table=Ward&menu=salarysa&view=sections" Access="salary,sections" Text="Lang.Ward" />
        <JP:Pill ID="pillJob" runat="server" Href="list.aspx?table=Job&menu=salarysa" Access="salary,job" Text="Lang.Job" />
        <JP:Pill ID="pillEmploymentType" runat="server" Href="list.aspx?table=EmploymentType&menu=salarysa" Access="salary,employmenttype" Text="Lang.EmpType" />
        <JP:Pill ID="pillMarital" runat="server" Href="list.aspx?table=Marital&menu=salarysa" Access="salary,marital" Text="Lang.Marital" />
        <JP:Pill ID="pillGender" runat="server" Href="list.aspx?table=Gender&menu=salarysa" Access="salary,gender" Text="Lang.Gender" />
        <JP:Pill ID="pillCountry" runat="server" Href="list.aspx?table=Country&menu=salarysa" Access="salary,country" Text="Lang.Country" />
        <JP:Pill ID="pillProvince" runat="server" Href="list.aspx?table=Province&menu=salarysa" Access="salary,province" Text="Lang.ProvinceName" />
        <JP:Pill ID="pillCity" runat="server" Href="list.aspx?table=City&menu=salarysa" Access="salary,city" Text="Lang.City" />
        <JP:Pill ID="pillCompanyInfo" runat="server" Access="salary,companyinfo" Text="Lang.CompanyInfo" Href="list.aspx?table=Config&view=companyinfos" />
        
        <JP:PageHeader ID="PageFish" runat="server" Text="Lang.FormDeclareItems" />

        <JP:Pill ID="pillFields" runat="server" Href="list.aspx?table=Fields&menu=salarysa" Access="salary,fields" Text="Lang.Fields" />
        <JP:Pill ID="pillDeductionFields" runat="server" Href="list.aspx?table=DeductionFields&menu=salarysa" Access="salary,deductionfields" Text="Lang.DeductionFields" />
        
        <JP:PageHeader ID="pageOrder" runat="server" Text="Lang.MOrderDefs" />

        <JP:Pill ID="pillOrderDefs" runat="server" Href="list.aspx?table=OrderDefs&menu=salarysa" Access="salary,orderdefs" Text="Lang.DefineOrderDefs" />
        <JP:Pill ID="pillFishDef" runat="server" Href="list.aspx?table=FishDefs&menu=salarysa" Access="salary,fishdefs" Text="Lang.FishDef" />

        <JP:PageHeader ID="pageHeadCoefficient" runat="server" Text="Lang.CoEfficient" />

        <JP:Pill ID="pillConstItems" runat="server" Href="list.aspx?table=ConstItems&menu=salarysa" Access="salary,constitems" Text="Lang.SalaryItems" />
        <JP:Pill ID="pillConstDefinition" runat="server" Href="list.aspx?table=ConstDefinitions&menu=salarysa" Access="salary,constdefination" Text="Lang.SalaryCoefficientsGroup" />

        
    </div>
    <script type="text/javascript">
        $(function () {
            checkAccess(['mainmenu,salary'], '../');
            $(".system-salary a").each(enableMenu);
            //setClassSalary();
        });
    </script>
</asp:Content>

