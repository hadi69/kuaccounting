﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class menu_accounting : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = LangProvider.Instance["MAccounting"];
        Page.SetLang();
        mFreeForms.Controls.Add(new LiteralControl(FormEngine.BuildMenu(1, "accounting,freeforms")));
        if (Request.QueryString["ajax"] == "true")
        {
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            pillsContainer.Attributes["class"] += " floating";
            pillsContainer.RenderControl(htw);
            htw.Flush();
            string contents = sw.ToString();
            //contents = contents.Replace("../list", "list");
            contents = contents.Replace("../", "");
            Response.Clear();
            Response.Write(contents);
            Response.End();
        }
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");
    }
    
}