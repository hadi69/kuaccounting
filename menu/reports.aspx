﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="reports.aspx.cs" Inherits="menu_reports" %>
<%@ Reference Control="~/controls/pill.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div class="row">
        <JP:PageHeader ID="pageHeader" runat="server" Text="Lang.Reports" />
    </div>
    <div id="pillsContainer" runat="server" class="row sub-system system-report smallpill ">
                <JP:Pill ID="pillPersonBook" runat="server" Access="reports,personbook" IconClass="sprites-bill-32" Text="Lang.PersonBook" Href="reports/accounting/PersonBook.aspx?baseOn=0" />
                <JP:Pill ID="pillFundReport" runat="server" Access="reports,fundreport" IconClass="sprites-topics-32" Text="Lang.FundReport" Href="reports/accounting/FundReport.aspx" />
                <JP:Pill ID="pillListOfOwe" runat="server" Access="reports,personbook" IconClass="sprites-bill-32" Text="Lang.ListOfOwe" Href="reports/accounting/PersonBook.aspx?baseOn=3" />
                <JP:Pill ID="pillListOfDue" runat="server" Access="reports,personbook" IconClass="sprites-bill-32" Text="Lang.ListOfDue" Href="reports/accounting/PersonBook.aspx?baseOn=4" />
                <JP:Pill ID="pillShopProfit" runat="server" Access="reports,shopprofit" IconClass="sprites-bill-32" Text="Lang.FinanceProfit" Href="reports/accounting/ShopProfit.aspx" />
                <JP:Pill ID="pillFinanceSheet" runat="server" Access="reports,financesheet" IconClass="sprites-topics-32" Text="Lang.TotalProfitReport" Href="reports/accounting/FinanceSheet.aspx" />
                <JP:Pill ID="pillTotalBook" runat="server" Access="reports,totalbook" IconClass="sprites-totalbook-32" Text="Lang.TotalBook" Href="reports/accounting/TotalBook.aspx" />
                <JP:Pill ID="pillPaperBook" runat="server" Access="reports,paperbook" IconClass="sprites-paperbook-32" Text="Lang.PaperBook" Href="reports/accounting/PaperBook.aspx" />
                <JP:Pill ID="pillExpBalance" runat="server" Access="reports,expbalance" IconClass="sprites-functionalbalance-32" Text="Lang.ExpBalance" Href="reports/accounting/ExpBalance.aspx" />
                <JP:Pill ID="pillTopicRemain" runat="server" Access="accounting,topic" IconClass="sprites-topics-32" Text="Lang.TopicAccountReport" Href="list.aspx?table=Trees&view=treeremain" />
                <JP:Pill ID="pillServiceReport" runat="server" Access="reports,servicereport" IconClass="sprites-bill-32" Text="Lang.ServiceReport" Href="reports/accounting/services.aspx" />
                <JP:Pill ID="pillCommoditySaleReport" runat="server" Access="reports,commoditysalereport" IconClass="sprites-bill-32" Text="Lang.CommoditySaleReport" Href="reports/accounting/CommoditySaleReport.aspx" />
                <JP:Pill ID="pillCommodityBuyReport" runat="server" Access="reports,commoditybuyreport" IconClass="sprites-bill-32" Text="Lang.CommodityBuyReport" Href="reports/accounting/CommoditybuyReport.aspx" />
          <asp:PlaceHolder runat="server" ID="mFreeForms"></asp:PlaceHolder>
    </div>
    <script type="text/javascript">
        $(function () {
            checkAccess(['mainmenu,reports'], '../');
            $(".system-report a").each(enableMenu);
            setClassReports();
        });
    </script>
</asp:Content>

