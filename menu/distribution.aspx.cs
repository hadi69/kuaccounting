﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class menu_distribution : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = LangProvider.Instance["MDistribution"];
        Page.SetLang(); 
        mFreeForms.Controls.Add(new LiteralControl(FormEngine.BuildMenu(10, "distribution,freeforms")));
        if (Request.QueryString["ajax"] == "true")
        {
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            pillsContainer.Attributes["class"] += " floating";
            pillsContainer.RenderControl(htw);
            htw.Flush();
            string contents = sw.ToString();
            contents = contents.Replace("../list", "list");
            Response.Clear();
            Response.Write(contents);
            Response.End();
        }
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");
    }
}