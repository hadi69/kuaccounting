﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class menu_sys : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["MManagement"];
            Page.SetLang();
        }
        if (Request.QueryString["ajax"] == "true")
        {
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            pillsContainer.Attributes["class"] += " floating";
            pillsContainer.RenderControl(htw);
            htw.Flush();
            string contents = sw.ToString();
            contents = contents.Replace("../", "");
            Response.Clear();
            
            Response.Write(contents);
            Response.End();
        }
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");
    }
}