﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class menu_reports : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = LangProvider.Instance["Reports"];
        Page.SetLang();
        if (Request.QueryString["ajax"] == "true")
        {
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            pillsContainer.Attributes["class"] += " floating";
            pillsContainer.RenderControl(htw);
            htw.Flush();
            string contents = sw.ToString();
            //contents = contents.Replace("../list", "list");
            contents = contents.Replace("../", "");
            Response.Clear();
            Response.Write(contents);
            Response.End();
        }
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");
        BindAccReports();
    }

    void BindAccReports()
    {
        DataTable reports = DbProxy.Instance.FillData(
            SiteSettings.ReplaceOptions("SELECT ID, ReportTitle FROM AccReport WHERE __PrOgrAmconD__ ORDER BY ReportTitle"), "Rep");
        if (reports != null && reports.Rows.Count > 0)
        {
            for (int i = 0; i < reports.Rows.Count; i++)
            {
                var pill = (controls_pill) LoadControl("~/controls/pill.ascx");
                pill.IconClass = "sprites-report-general-32";
                pill.Text = reports.Rows[i][1].ToString();
                pill.Href = string.Format("reports/accounting/accrep.aspx?id={0}", reports.Rows[i][0]);

               // mReportHolder.Controls.Add(pill);
            }

          //  mReportHolder.Visible = true;
        }
    }
}