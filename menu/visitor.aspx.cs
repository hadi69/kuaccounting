﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class controls_visitor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = LangProvider.Instance["Visitor"];
        Page.SetLang();
        mFreeForms.Controls.Add(new LiteralControl(FormEngine.BuildMenu(12, "visitor,freeforms")));
        if (Request.QueryString["ajax"] == "true")
        {
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            pillsContainer.Attributes["class"] += " floating";
            pillsContainer.RenderControl(htw);
            htw.Flush();
            string contents = sw.ToString();
            contents = contents.Replace("../list", "list");
            Response.Clear();
            Response.Write(contents);
            Response.End();
        }
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");
    }
}