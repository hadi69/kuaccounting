﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="salary.aspx.cs" Inherits="menu_salary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" Runat="Server">
    <div class="row">
        <JP:PageHeader ID="pageHeader" runat="server" Text="Lang.MSalary" />
    </div>
    
    <div id="pillsContainer" runat="server" class="row sub-system system-salary smallpill ">
        
        <JP:Pill ID="pillPersonnel" runat="server" Href="list.aspx?table=Personnel&menu=salarysa&view=staffs" IconClass="sprites-person-32" Access="salary,personnel" Text="Lang.Personnel" />
        <JP:Pill ID="pillFishValues" runat="server" Href="list.aspx?table=Fish&menu=salarysa&view=fishvalues" IconClass="sprites-edit-32" Access="salary,fishvalues" Text="Lang.EditFishValues" />
        <JP:Pill ID="pillSalaryDefinition" runat="server" Href="menu/salarydefinition.aspx" IconClass="sprites-definition-32" Access="salary,salarydefinition" Text="Lang.SalaryDefinition" />
        <%--<JP:Pill ID="pillEmployeeSalary" runat="server" Href="menu/employeesalary.aspx" Access="salary,employeesalary" Text="Lang.MEmployeeSalary" />--%>
        
        <JP:PageHeader ID="pageLoad" runat="server" Text="Lang.Loan" />
        <JP:Pill ID="pillFundType" runat="server"  Href="list.aspx?table=FundType&menu=salarysa" IconClass="sprites-fundtype-32" Access="salary,fundtype" Text="Lang.FundType" />
        <JP:Pill ID="pillFunds" runat="server" Href="list.aspx?table=Funds&menu=salarysa" IconClass="sprites-fund-32" Access="salary,funds" Text="Lang.Funds" />

        <JP:PageHeader ID="PageDocument" runat="server" Text="Lang.WageDocument" />
        
        <JP:Pill ID="pillDocumentField" runat="server"  Href="list.aspx?table=ReportFields&view=documentfields&menu=salarysa" IconClass="sprites-field-32" Access="salary,documentfields" Text="Lang.DocumentFields" />
        <JP:Pill ID="pillDocument" runat="server"  Href="FishDocument.aspx" Access="salary,fishdocument" IconClass="sprites-voucher-32" Text="Lang.FishDocument" />
        
        <JP:PageHeader ID="PageReport" runat="server" Text="Lang.Report" />
        <JP:Pill ID="pillSalaryReports" runat="server"  Href="menu/salaryreports.aspx" IconClass="sprites-report-32" Access="salary,report" Text="Lang.SalaryReports" />
        <JP:Pill ID="pillOutputFiles" runat="server"  Href="menu/outputfiles.aspx" IconClass="sprites-outputfile-32" Access="salary,outputfiles" Text="Lang.OutputFiles" />
        <JP:PageHeader ID="PageHeader1" runat="server" Text="Lang.FreeForms" />
        <asp:PlaceHolder runat="server" ID="mFreeForms"></asp:PlaceHolder>
    </div>
    <script type="text/javascript">
        $(function () {
            checkAccess(['mainmenu,salary'], '../');
            setClassSalary('../');
            $(".system-salary a").each(enableMenu);
            //setClassSalary();
        });
    </script>
</asp:Content>

