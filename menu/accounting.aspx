﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="accounting.aspx.cs" Inherits="menu_accounting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div class="row">
        <JP:PageHeader ID="pageHeader" runat="server" Text="Lang.MAccounting" />
    </div>
    
    <div id="pillsContainer" runat="server" class="row sub-system system-accounting">
        <!--<JP:Pill ID="pillTopicGroup" runat="server" Access="accounting,topicgroup" IconClass="sprites-topicgroup-32" Text="Lang.TopicGroup" Href="list.aspx?table=TopicGroup&menu=accountinga" />-->
        <JP:Pill ID="pillTopicAccount" runat="server" Access="accounting,topic" IconClass="sprites-topics-32" Text="Lang.TopicAccount" Href="topics.aspx" />
        <JP:Pill ID="pillDocumentAccounting" runat="server" Access="accounting,voucher" IconClass="sprites-voucher-32" Text="Lang.DocumentAccounting" Href="list.aspx?table=DocumentAccounting" />
        <JP:Pill ID="pillPersonnelGroup" runat="server" Access="accounting,chart" IconClass="sprites-personnelgroup-32" Text="Lang.PersonnelGroup" Href="list.aspx?table=PersonnelGroup&menu=accountinga" />
        <JP:Pill ID="pillPersons" runat="server" Access="accounting,person" IconClass="sprites-person-32" Text="Lang.Persons" Href="list.aspx?table=Personnel&view=persons" />
        <JP:Pill ID="pillBankAccount" runat="server" Access="bank,bankaccount" IconClass="sprites-bankaccount-32" Text="Lang.BankAccount" Href="list.aspx?table=BankAccount" />
        <JP:Pill ID="pillBankAccountCheck" runat="server" Access="bank,bankaccountcheck" IconClass="sprites-bankaccountcheck-32" Text="Lang.BankAccountCheck" Href="list.aspx?table=BankAccountCheck" />
        <JP:Pill ID="pillAutPayChecksRegister" runat="server" Access="bank,paychecksshops" IconClass="sprites-paycheck-32" Text="Lang.PayChecks" Href="list.aspx?table=PayCheck&view=paycheckshops&qd=checksshop" />
        <JP:Pill ID="pillAutoChecksRegister" runat="server" Access="bank,checksshops" IconClass="sprites-check-32" Text="Lang.Checks" Href="list.aspx?table=Checks&view=checksshops&qd=checksshop" />
        <JP:Pill ID="pillGuaranteeChecks" runat="server" Access="bank,guaranteechecks" IconClass="sprites-check-32" Text="Lang.GuaranteeChecks" Href="list.aspx?table=Checks&view=guaranteecheckss&qd=checksshop" />
        <asp:PlaceHolder runat="server" ID="mFreeForms"></asp:PlaceHolder>
    </div>

    <input type="hidden" runat="server" id="isadmin" class="isadmin" />
    <script type="text/javascript">
        $(function () {
            checkAccess(['mainmenu,accounting'], '../');
            var isadmin = $('.isadmin').val();
            if (isadmin != '1') {
                $(".system-accounting a").each(enableMenu);
            }
            setClassAccounting();
            if (_Context.isAccountingVer)
                $('.noacc').hide();
        });
    </script>
</asp:Content>

