﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="bank.aspx.cs" Inherits="controls_bank" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div class="row">
        <JP:PageHeader ID="pageHeader" runat="server" Text="Lang.BankAndCheck" />
    </div>
    
    <div id="pillsContainer" runat="server" class="row sub-system system-bank">
        <JP:Pill ID="pillBankAccount" runat="server" Access="bank,bankaccount" IconClass="sprites-bankaccount-32" Text="Lang.BankAccount" Href="list.aspx?table=BankAccount" />
        <JP:Pill ID="pillBankAccountCheck" runat="server" Access="bank,bankaccountcheck" IconClass="sprites-bankaccountcheck-32" Text="Lang.BankAccountCheck" Href="list.aspx?table=BankAccountCheck" />
        <JP:Pill ID="pillAutPayChecksRegister" runat="server" Access="bank,paychecksshops" IconClass="sprites-paycheck-32" Text="Lang.AutoPayChecksRegister" Href="list.aspx?table=PayCheck&view=paycheckshops&qd=checksshop" />
        <JP:Pill ID="pillAutoChecksRegister" runat="server" Access="bank,checksshops" IconClass="sprites-check-32" Text="Lang.AutoChecksRegister" Href="list.aspx?table=Checks&view=checksshops&qd=checksshop" />
        <JP:Pill ID="pillGuaranteeChecks" runat="server" Access="bank,guaranteechecks" IconClass="sprites-check-32" Text="Lang.GuaranteeChecks" Href="list.aspx?table=Checks&view=guaranteecheckss&qd=checksshop" />
        <JP:Pill ID="pillLoan" runat="server" Level="2" Access="bank,loan" IconClass="sprites-loans-32" Text="Lang.Loans" Href="list.aspx?table=loan" />
        <asp:PlaceHolder runat="server" ID="mFreeForms"></asp:PlaceHolder>
    </div>
    <input type="hidden" runat="server" id="isadmin" class="isadmin" />
    <script type="text/javascript">
        $(function () {
            checkAccess(['mainmenu,bank'], '../');
            $(".system-bank a").each(enableMenu);
            setClassBank();
        });
    </script>
</asp:Content>

