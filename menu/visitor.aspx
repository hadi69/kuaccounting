﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="visitor.aspx.cs" Inherits="controls_visitor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div class="row">
        <JP:PageHeader ID="pageHeader" runat="server" Text="Lang.Visitor" />
    </div>
    
    <div id="pillsContainer" runat="server" class="row sub-system system-visitor">
        
        <asp:PlaceHolder runat="server" ID="mFreeForms"></asp:PlaceHolder>
    </div>
    <input type="hidden" runat="server" id="isadmin" class="isadmin" />
    <script type="text/javascript">
        $(function () {
            checkAccess(['distribution,visitor'], '../');
            $(".system-visitor a").each(enableMenu);
            setClassDistribution();
        });
    </script>
</asp:Content>
