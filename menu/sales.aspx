﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="sales.aspx.cs" Inherits="menu_sales" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div class="row">
        <JP:PageHeader ID="pageHeader" runat="server" Text="Lang.SellAndBuy" />
    </div>
    
    <div id="pillsContainer" runat="server" class="row sub-system system-sales">
        <JP:Pill ID="pillShopSalesPurchase" runat="server" Access="sales,shoppurchaseinvoice" IconClass="sprites-purchase-32" Text="Lang.SalesPurchase" Href="list.aspx?table=Purchase&menu=accountinga&view=shoppurchases" />
        <JP:Pill ID="pillShopSalesInvoice" runat="server" Access="sales,shopsalesinvoice" IconClass="sprites-storedraft-32" Text="Lang.SalesInvoice" Href="list.aspx?table=StoreDraft&menu=accountinga&view=shopstoredrafts" />
        <JP:Pill ID="pillShopReturnInvoice" runat="server" Access="sales,shopreturninvoice" IconClass="sprites-return-32" Text="Lang.ReturnInvoice" Href="list.aspx?table=Returns&menu=accountinga&view=shopreturns" />
        <JP:Pill ID="pillShopRefuseInvoice" runat="server" Access="sales,shoprefuseinvoice" IconClass="sprites-refuse-32" Text="Lang.RefuseInvoice" Href="list.aspx?table=Refuse&menu=accountinga&view=shoprefuses" />
        <JP:Pill ID="pillMeasurementUnits" runat="server" Access="stores,units" IconClass="sprites-units-32" Text="Lang.MeasurementUnits" Href="list.aspx?table=Units&menu=storea" />
        <JP:Pill ID="pillServiceType" runat="server" Access="stores,servicetype" IconClass="sprites-producttype-32" Text="Lang.ServiceType" Href="list.aspx?table=ServiceType&menu=storea" />
        <JP:Pill ID="pillCommodity" runat="server" Access="stores,service" IconClass="sprites-product-32" Text="Lang.Commodity" Href="list.aspx?table=Service&menu=storea" />
        <JP:Pill ID="pillStore" runat="server" Access="stores,store" IconClass="sprites-store-32" Text="Lang.Store" Href="list.aspx?table=Stores&menu=storea" />
        <JP:Pill ID="pillWFTTransfer" runat="server" Level="2" Access="stores,transfer" IconClass="sprites-transfer-32" Text="Lang.WFTTransfer" Href="list.aspx?table=Transfer&menu=storea" />
        <asp:PlaceHolder runat="server" ID="mFreeForms"></asp:PlaceHolder>
    </div>
    <script type="text/javascript">
        $(function () {
            checkAccess(['mainmenu,sales'], '../');
            $(".system-sales a").each(enableMenu);
            setClassSales();
        });
    </script>
</asp:Content>

