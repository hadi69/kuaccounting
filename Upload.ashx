﻿<%@ WebHandler Language="C#" Class="Upload" %>

using System;
using System.Web;
using System.IO;
using Newtonsoft.Json;

public class Upload : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "application/json";
        context.Response.Expires = -1;
        try
        {
            int serviceId = Helper.GetInt(context.Request.Params["serviceId"], Null.NullInteger);
            if (serviceId > 0)
            {
                // it is for Service
                String filename = HttpContext.Current.Request.Headers["X-File-Name"].Slugify();
                Stream inputStream = HttpContext.Current.Request.InputStream;
                string tempPath = "files/services/" + serviceId.ToString(); // System.Configuration.ConfigurationManager.AppSettings["FolderPath"];
                string savePath = context.Server.MapPath("~/" + tempPath);
                if (!Directory.Exists(savePath))
                {
                    Directory.CreateDirectory(savePath);
                }
                string actualFileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + "_" + filename;
                string actualFile = savePath + '\\' + actualFileName;
                string thumbnailFileName = "thumb_" + actualFileName;
                string thumbnail = savePath + '\\' + thumbnailFileName;
                FileStream fileStream = new FileStream(actualFile, FileMode.OpenOrCreate);
                inputStream.CopyTo(fileStream);
                fileStream.Close();

                ImageUtilities.ScaleImage(actualFile, thumbnail, 250, 200);

                var serviceImage = new ServiceImage();
                serviceImage.MainImage = tempPath + "/" + actualFileName;
                serviceImage.ThumbImage = tempPath + "/" + thumbnailFileName;
                serviceImage.Title = "";

                string query = "INSERT INTO ServiceImage (ServiceID, Title, MainImage, ThumbImage) Values({0}, '{1}', '{2}', '{3}'); Select SCOPE_IDENTITY();";
                query = String.Format(query, serviceId, "", serviceImage.MainImage, serviceImage.ThumbImage);
                object id = DbProxy.Instance.FillScalar(query);

                serviceImage.id = int.Parse(id.ToString());

                context.Response.Write("{success:true, model: " + JsonConvert.SerializeObject(serviceImage) + "}");
            }
            else
            {
                HttpPostedFile postedFile = context.Request.Files["Filedata"];

                string tempPath = "~/files";// System.Configuration.ConfigurationManager.AppSettings["FolderPath"];
                string savepath = context.Server.MapPath(tempPath);
                if (!Directory.Exists(savepath))
                    Directory.CreateDirectory(savepath);
                string folder = context.Request["folder"];
                if (!string.IsNullOrEmpty(folder))
                {
                    if (folder.StartsWith("/"))
                        folder = folder.Substring(folder.LastIndexOf('/') + 1);
                    savepath = Path.Combine(savepath, folder);
                    if (!Directory.Exists(savepath))
                        Directory.CreateDirectory(savepath);
                }
                string namePrefix = Helper.GetString( context.Request["namePrefix"], "");

                string filename = namePrefix + postedFile.FileName;
                postedFile.SaveAs(savepath + @"\" + filename);
                context.Response.Write(tempPath + "/" + filename);
                context.Response.StatusCode = 200;
            }

        }
        catch (Exception ex)
        {
            context.Response.Write("Error: " + ex.Message);
        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}

class ServiceImage
{
    public string MainImage { get; set; }
    public string ThumbImage { get; set; }
    public string Title { get; set; }
    public int id { get; set; }
}