﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

public partial class Master : System.Web.UI.MasterPage
{
    protected string Version
    {
        // Current version of the app! DO NOT MODIFY!!

        get { return "16.04.17.p1"; }

    }

    protected int ShowDropDownMenu
    {
        get
        {
            try
            {
                int result = Int32.Parse(DbProxy.Instance.GetConfig("ShowDropDownMenu", SiteSettings.ProgramID));
                if (result == 0 || result == 1)
                {
                    return result;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
    public bool UseMinifiedAssets
    {
        get
        {
            string env = ConfigurationManager.AppSettings.Get("ENV");
            if (env != null && env == "prod")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.SetLang();
        TodyDate.Value = Helper.FormatDate(DateTime.Now, null);
        string logoImage = DbProxy.Instance.GetConfig("StoreDraftLogo", "");
        if(!string.IsNullOrEmpty(logoImage))
        //CompanyLogo.ImageUrl = Helper.GetDomain() + "/files/config/" + logoImage;
        if (Request.QueryString["hidemenu"] == "true")
            menu1.Visible = false;
        // user
        StringBuilder script = new StringBuilder();
        script.AppendLine("<script>");
        script.AppendLine("$(function(){");

        // User props
        if (SiteSettings.User != null)
        {
            string pf = "[]";
            List<Tuple<DateTime, DateTime, decimal>> priceFactors = SiteSettings.MonetaryUnitFactors;
            if (priceFactors != null && priceFactors.Count > 0)
            {
                pf = "[";
                for (int i = 0; i < priceFactors.Count; i++)
                    pf += string.Format("['{0}', '{1}', {2}], "
                        , Helper.FormatDate(priceFactors[i].Item1, null)
                        , Helper.FormatDate(priceFactors[i].Item2, null)
                        , priceFactors[i].Item3);
                pf = pf.TrimEnd(' ', ',') + "]";
            }
            string[] words = SiteSettings.User.Access.ToLower().Split(';');
            StringBuilder access = new StringBuilder(",");
            for (int i = 0; i < words.Length; i++)
                if (words[i] != "")
                {
                    int hash = Helper.GetStableHash(words[i], 10000000);
                    access.Append(hash);
                    access.Append(",");
                }
            string user = string.Format(@"
    _Context.isAdmin = {0};
    _Context.userID = {1};
    _Context.userName = '{2}';
    _Context.userFullName = '{3}';
    _Context.access = '{4}';
    _Context.personnelID = {5};
    _Context.personnelName = '{6}';
    _Context.fyStart = '{7}';
    _Context.fyEnd = '{8}';
    _Context.priceFactor = {9};
    _Context.programStatus = {10};
    _Context.displayMenu = {11};
    _Context.level = {12};
    _Context.salary = {13};
    _Context.distribution = {14};
    _Context.workflowInheritance = {15};
    _Context.decimalPoints = {16};
    _Context.isStoreUser = {17};
    _Context.POSUrl = '{18}';
    _Context.POSPort = '{19}';
    _Context.monetaryUnitAbbreviation = '{20}';
    _Context.SecondFactorPrice = '{21}';
    _Context.monetaryUnitAbbreviationSecond = '{22}';
    _Context.useAdditionalValue = '{23}';
    _Context.todayDate = '{24}';
    setQuickLaunchItems();
//alert('{0} --> {1} --> {2} --> {3}');
", SiteSettings.User.IsAdmin ? "true" : "false"
   , SiteSettings.User.ID, SiteSettings.User.Username, SiteSettings.User.Fullname
   , access.ToString(), SiteSettings.User.PersonnelID, SiteSettings.User.PersonnelName
   , SiteSettings.FinanceYearStartDate == null ? "" : Helper.FormatDate(SiteSettings.FinanceYearStartDate.Value, null)
   , SiteSettings.FinanceYearEndDate == null ? "" : Helper.FormatDate(SiteSettings.FinanceYearEndDate.Value, null)
   , pf
   , SiteSettings.ProgramID == 0 ? 0 : SiteSettings.ProgramClosed ? 2 : 1 // programStatus= 0: all, 1: ok, 2: closed
   , DbProxy.Instance.GetConfig("DisplayMenu", "0") == "1" ? "true" : "false"
   , SiteSettings.DongleLevel
   , SiteSettings.DongleSalary ? 1 : 0
   , SiteSettings.DongleVisitor ? 1 : 0
   , SiteSettings.WorkflowInheritance ? "true" : "false"
   , SiteSettings.DecimalPlaces
   , SiteSettings.User.IsStoreUser ? "true" : "false"
   , SiteSettings.POSUrl
   , SiteSettings.POSPort
   ,SiteSettings.MonetaryUnitAbbreviation
   ,SiteSettings.SecondMonetaryUnitFactors
   ,SiteSettings.SecondMonetaryUnitAbbreviation
   , SiteSettings.UseAdditionalValue
   , SiteSettings.GetTodayDate
   );
            script.AppendLine(user);
        }

        // options : settings
        string options = string.Format(@"
    _options.enableAdmissionDate = {0};
    _options.dateType = '{1}';
    _options.lang = '{2}';
//alert('{0}');
", DbProxy.Instance.GetConfig("EnableAdmissionDate", "1") == "1" ? "true" : "false"
 , SiteSettings.DateType
 , SiteSettings.Lang);
        script.AppendLine(options);

        script.AppendLine("});");
        script.AppendLine("</script>");
        Page.RegisterClientScriptBlock("setContext", script.ToString());

        if (!IsPostBack)
        {
            mUsername.Text = SiteSettings.User == null ?LangProvider.Instance["NoUser"] : SiteSettings.User.Fullname;
            BindDBs();
        }
        BindInitData();
    }
    void BindInitData()
    {
        using (DbProxy proxy = new DbProxy())
        {
            // Monetary Units
            //System.Data.DataTable monetaryUnits = proxy.FillData("SELECT ID, Title, Abbreviation FROM MonetaryUnit", "f");
            //mMonetaryUnit.Items.Clear();
            //if (monetaryUnits != null)
            //{
            //    for (int i = 0; i < monetaryUnits.Rows.Count; i++)
            //    {
            //        if (monetaryUnits.Rows[i][1] != null && monetaryUnits.Rows[i][0] != null)
            //        {
            //            ListItem item = new ListItem(monetaryUnits.Rows[i][1].ToString(),monetaryUnits.Rows[i][0].ToString());
            //            string abbreviation = "";
            //            if (monetaryUnits.Rows[i][2] != null)
            //            {
            //                abbreviation = monetaryUnits.Rows[i][2].ToString();
            //            }
            //            item.Attributes.Add("Abbreviation", abbreviation);
            //            mMonetaryUnit.Items.Add(item);
            //        }
            //    }
            //}
            mMonetaryUnit.DataSource = proxy.FillData("SELECT ID, Title FROM MonetaryUnit", "f");
            mMonetaryUnit.DataTextField = "Title";
            mMonetaryUnit.DataValueField = "ID";
            mMonetaryUnit.DataBind();
            try
            {
                mMonetaryUnit.SelectedValue = SiteSettings.MonetaryUnit.ToString();
            }
            catch { }

            //// Programs
            //System.Data.DataTable programs = proxy.FillData("SELECT ID, Title FROM Program", "fuck turkey");
            //mProgram.Items.Clear();
            //mProgram.Items.Add(new ListItem(LangProvider.Instance["Default"], "-1"));
            //mProgram.Items.Add(new ListItem(LangProvider.Instance["All"], "0"));
            //if (programs != null)
            //    for (int i = 0; i < programs.Rows.Count; i++)
            //        mProgram.Items.Add(new ListItem(programs.Rows[i][1].ToString(), programs.Rows[i][0].ToString()));
            //try
            //{
            //    mProgram.SelectedValue = SiteSettings.ProgramID.ToString();
            //}
            //catch { }
        }
    }
    void BindDBs()
    {
        mDBs.Items.Clear();
        mDBs.Items.Add(LangProvider.Instance["ActiveYear"]);
        try
        {
            string path = Server.MapPath("~/files/fdb");
            if (System.IO.Directory.Exists(path))
            {
                string[] files = System.IO.Directory.GetFiles(path, "DB_*.mdf");
                for (int i = 0; i < files.Length; i++)
                {
                    string fileName = System.IO.Path.GetFileNameWithoutExtension(files[i]);
                    if (!fileName.ToLower().EndsWith("_log"))
                        mDBs.Items.Add(fileName);
                }
            }
            if (!string.IsNullOrEmpty(SiteSettings.OtherCatalogName))
                mDBs.SelectedValue = SiteSettings.OtherCatalogName;
            else
                mDBs.SelectedIndex = 0;
        }
        catch { }
    }
    protected string GetLang()
    {
        return SiteSettings.Lang;
    }
    protected string GetUserStyle()
    {
        DataTable dt = DbProxy.Instance.FillData("SELECT [Name], Value FROM Style WHERE UserID=" + SiteSettings.UserID + " ORDER BY SortIndex", "style");
        string style = "";
        if (dt != null && dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string name = Helper.GetString(dt.Rows[i][0], null), value = Helper.GetString(dt.Rows[i][1], null);
                if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(value))
                    style += name + "{" + value + "}\r\n";
            }
        }
        if (dt != null)
            dt.Dispose();
        return style;// ".flexigrid{font-family: ;color:#f00}";
    }
}
