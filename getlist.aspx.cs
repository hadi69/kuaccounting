﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml;

public partial class getlist : System.Web.UI.Page
{
    TableInfo mInfo = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        string table = Request.QueryString["table"];
        string q = Request.QueryString["qd"];
        if (null != table)
            mInfo = TableController.Instance[table];
        if (mInfo == null)
            Response.End();
        int page = Helper.GetInt(Request["page"], 1);
        int rp = Helper.GetInt(Request["rp"], 1);
        string sortname = Helper.GetString(Request["sortname"], "ID");

        string whereCondition = "";
        if (Request["qtype"] != null && Request["query"] != null && Request["query"].ToString() != string.Empty)
            whereCondition = BuildWhereCondition(Request["qtype"].ToString(), Request["query"].ToString());
        string sortorder = Helper.GetString(Request["sortorder"], "asc");
        string sortExp = sortname + " " + sortorder;
        int start = ((page - 1) * rp);

        string select = "*", from = table;
        if (string.IsNullOrEmpty(q))
        {
            QueryInfo qInfo = QueryController.Instance[q];
            if (qInfo != null)
            {
                if (!string.IsNullOrEmpty(qInfo.From))
                    from = qInfo.From;
                if (!string.IsNullOrEmpty(qInfo.Select))
                    select = qInfo.Select;
            }
        }
        string totalQry = string.Format("SELECT COUNT(*) FROM {0} {1}", from, whereCondition);
        int total = Helper.GetInt(DbProxy.Instance.FillScalar(totalQry), 0);

        string qry = string.Format(@"
WITH TableRows AS 
(
	SELECT ROW_NUMBER() OVER (ORDER BY {0}) AS ZeilNummer,
		{1} FROM {2}
	{3}
)
SELECT * FROM TableRows WHERE ZeilNummer between {4} And {5} ORDER BY  ID
SELECT COUNT(*) FROM 
	{2} {3}"
            , sortExp, select, from, whereCondition, start, start + rp);
        DataTable dt = DbProxy.Instance.FillData(qry, "fck");

        /*
         <rows>
          <page>10</page>
          <total>100</total>
          <row id="0">
            <cell>0</cell>
            <cell>n0</cell>
          </row>
          <row id="1">
            <cell>1</cell>
            <cell>n1</cell>
          </row>
        </rows>
        */
        System.Text.StringBuilder builder = new System.Text.StringBuilder();

        builder.Append("<?xml version='1.0' encoding='utf-8' ?>");
        builder.Append("<rows>");
        builder.Append("<page>" + page + " </page>");
        builder.Append("<total>" + total + " </total>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            builder.Append("<row>");
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                builder.Append(string.Format("<cell>{0}</cell>", dt.Rows[i][j]));
            }
            builder.Append("</row>");
        }
        builder.Append("</rows>");
        XmlDocument newDoc = new XmlDocument();
        newDoc.LoadXml(builder.ToString());
        Response.Clear();
        Response.ContentType = "text/xml";
        //Response.Write(xml);
        Response.Write(newDoc.OuterXml);
    }
    private string BuildWhereCondition(string columnName, string columnValue)
    {
        string whereCondition = string.Empty;
        FieldInfo field = mInfo.GetField(columnName);
        if (field == null)
            return whereCondition;
        if (field.Type == "int") // todo : other numerics
            whereCondition = columnName + "=" + columnValue;
        else if (field.Type.StartsWith("nvarchar")) // todo: other strings
            whereCondition = columnName + " LIKE '%" + columnValue + "%'";
        else if (field.Type == "datetime") // todo
            whereCondition = columnName + "='" + columnValue + "'";
        return whereCondition;
    }
}