﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Net;
using Microsoft.SqlServer.Management.Smo.Agent;
using System.Globalization;

public partial class Tax : System.Web.UI.Page
{
    int NumOfPersonnel = 0;
    private string folderName = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindYear();
            BindBanks();
        }
    }
    void BindYear()
    {
        PersianCalendar calendar = new PersianCalendar();
        int year = calendar.GetYear(DateTime.Now);
        ddlYear.Items.Add((year - 1).ToString());
        ddlYear.Items.Add(year.ToString());
        ddlYear.Items.Add((year + 1).ToString());
        ddlYear.SelectedIndex = 1;
    }
    void BindBanks()
    {
        DataTable banks = DbProxy.Instance.FillData("SELECT EnumValues.ID, EnumValues.Title FROM EnumValues INNER JOIN Enums ON EnumValues.EnumID=Enums.ID WHERE Name=N'Banks'", "banks");

        Bank.DataTextField = "Title";
        Bank.DataValueField = "ID";
        Bank.DataSource = banks;
        Bank.DataBind();
        Bank.Items.Insert(0, "");
        Bank.SelectedIndex = 0;
    }
    string GetFolder()
    {
        string folder = Server.MapPath("~/temp/Tax");
        folderName = Helper.FormatDate(DateTime.Now, "yyyy-MM-dd-fff"); ;
        folder = Path.Combine(folder, folderName);
        if (!Directory.Exists(folder))
            Directory.CreateDirectory(folder);
        return folder;
    }
    protected void CreateFile_Click(object sender, EventArgs e)
    {
        string folder = GetFolder();
        CreateTaxFile(folder);
        CreateShortTaxFile(folder);
        CreatePersonnelFile(folder);
        mFile.Visible = true;

    }

    void CreatePersonnelFile(string folder)
    {
        string fileName = "WP.txt";
        string file = Path.Combine(folder, fileName);
        if (!file.ToLower().EndsWith(".txt"))
            file += ".txt";
        StreamWriter sw = new StreamWriter(file, true, Encoding.UTF8);
        try
        {
            DataTable fishs = DbProxy.Instance.FillData("SELECT ID, PersonnelID FROM Fish WHERE FishYear=" + ddlYear.Text + " AND FishMonth=" + ddlMonth.Text, "fishs");
            for (int i = 0; i < fishs.Rows.Count; i++)
            {
                DataTable personnelInfo =
                    DbProxy.Instance.FillData(
                        "SELECT * FROM Personnel WHERE ID=" + Helper.GetString(fishs.Rows[i], "PersonnelID", ""),
                        "personnelInfo");
                string UnTaxPersonCode =
                    Helper.GetEnumValuesCode(Helper.GetString(personnelInfo.Rows[0], "UnTaxPersonID", ""));
                if (!ShowUnTax.Checked && UnTaxPersonCode != "0")
                    NumOfPersonnel--;
                else
                {
                    string fid = Helper.GetString(fishs.Rows[i], "ID", "");
                    DataTable fishValues = DbProxy.Instance.FillData(
                        "SELECT * FROM FishValuesView WHERE FishID=" + fid, "fishValue");
                    string EmploymentTypeCode =
                        Helper.GetString(
                            DbProxy.Instance.FillScalar("SELECT Code FROM EmploymentType WHERE ID=" +
                                                        Helper.GetString(personnelInfo.Rows[0], "EmploymentTypeID", "")),
                            "");
                    string PostName =
                        Helper.GetString(
                            Helper.GetEnumValuesTitle(Helper.GetString(personnelInfo.Rows[0], "PostID", "")), "");
                    string JobName =
                        Helper.GetString(
                            DbProxy.Instance.FillScalar("SELECT Title FROM Job WHERE ID=" +
                                                        Helper.GetString(personnelInfo.Rows[0], "JobID", "")), "");
                    string EducationCode =
                        Helper.GetEnumValuesCode(Helper.GetString(personnelInfo.Rows[0], "EducationID", ""));
                    string CategoryCode =
                        Helper.GetEnumValuesCode(Helper.GetString(personnelInfo.Rows[0], "CategoryID", ""));

                    string NationalityCode =
                        Helper.GetEnumValuesCode(Helper.GetString(personnelInfo.Rows[0], "NationalityID", ""));
                    string InsuranceTypeCode =
                        Helper.GetEnumValuesCode(Helper.GetString(personnelInfo.Rows[0], "InsuranceTypeID", ""));
                    string InformationTypeCode =
                        Helper.GetEnumValuesCode(Helper.GetString(personnelInfo.Rows[0], "InformationTypeID", ""));
                    string WorkPlaceStatusCode =
                        Helper.GetEnumValuesCode(Helper.GetString(personnelInfo.Rows[0], "WorkPlaceStatusID", ""));
                    string ContractTypeCode =
                        Helper.GetEnumValuesCode(Helper.GetString(personnelInfo.Rows[0], "ContractTypeID", ""));
                    string PersonnelStatusCode =
                        Helper.GetEnumValuesCode(Helper.GetString(personnelInfo.Rows[0], "PersonnelStatusID", ""));
                    string PersonnelItems = NationalityCode + ","//نوع تابعیت
                                            + InformationTypeCode + "," //نوع اطلاعات
                                            + Helper.GetString(personnelInfo.Rows[0], "NatinoalCode", "") + "," //کد ملی
                                            + Helper.GetString(personnelInfo.Rows[0], "Name", "") + "," //نام
                                            + Helper.GetString(personnelInfo.Rows[0], "Surname", "") + ","//نام خانوادگی
                                            + "103," //کشور
                                            + Helper.GetString(personnelInfo.Rows[0], "PersonneliNum", "")+","//شناسه کارمند
                                            + EducationCode + "," //مدرک تحصیلی
                                            + PostName + "," //سمت
                                            + InsuranceTypeCode + ",," // نوع بیمه و نام بیمه
                                            + Helper.GetString(personnelInfo.Rows[0], "InsuranceNo", "") + ","// شماره بیمه
                                            + Helper.GetString(personnelInfo.Rows[0], "PostCode", "") + "," //کد پستی
                                            + "," +
                                            Helper.FormatDate(Convert.ToDateTime(Helper.GetString(personnelInfo.Rows[0], "StartDate", null)), "yyyyMMdd") + "," //آدرس و تاریخ استخدام
                                            + EmploymentTypeCode + "," //نوع استخدام
                                            + Helper.GetString(personnelInfo.Rows[0], "WorkPlace", "") + "," //محل خدمت
                                            + WorkPlaceStatusCode + "," //وضعیت محل خدمت
                                            + ContractTypeCode + "," //نوع قرارداد
                                            + "," //تاریخ پایان کار
                                            + PersonnelStatusCode + "," //وضعیت کارمند
                                            + ","; //شماره تلفن همراه و پست الکترونیکی
                    sw.WriteLine(PersonnelItems);
                }
            }
            mFile.Text += LangProvider.Instance.ParseLang("Lang.PersonnelFileCreated") + ": <a href='temp/Tax/" +
                          folderName + "/" + fileName + "' download='" + fileName + "'>" +
                          LangProvider.Instance.ParseLang("Lang.Download") + "</a><br />";
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
        }
        finally
        {
            if (sw != null)
            {
                sw.Flush();
                sw.Close();
                sw.Dispose();
            }
        }
    }
    void CreateTaxFile(string folder)
    {
        string fileName = "WH" + ddlYear.Text + ddlMonth.Text + ".txt";
        string file = Path.Combine(folder, fileName);
        if (!file.ToLower().EndsWith(".txt"))
            file += ".txt";
        StreamWriter sw = new StreamWriter(file, true, Encoding.UTF8);
        try
        {
            //sw = File.CreateText(file);
            string PersonTypeCode = Helper.GetString(DbProxy.Instance.FillScalar("SELECT Code FROM EnumValues WHERE ID=" + DbProxy.Instance.GetConfig("PayerType")), "");
            string LegalPersonTypeCode = Helper.GetString(DbProxy.Instance.FillScalar("SELECT Code FROM EnumValues WHERE ID=" + DbProxy.Instance.GetConfig("LegalPayerType")), "");
            DataTable FirstSignature = DbProxy.Instance.FillData(@"SELECT NatinoalCode, Name, Surname, PostName FROM PersonnelView WHERE ID=" + Helper.GetInt(DbProxy.Instance.GetConfig("Signature1"), -1), "FirstSignature");
            DataTable SecondSignature = DbProxy.Instance.FillData(@"SELECT NatinoalCode, Name, Surname, PostName FROM PersonnelView WHERE ID=" + Helper.GetInt(DbProxy.Instance.GetConfig("Signature2"), -1), "SecondSignature");
            string CompanyInfo = DbProxy.Instance.GetConfig("TaxNo") + ","
                                 + DbProxy.Instance.GetConfig("BranchCode") + ","
                                 + ddlYear.Text + ","
                                 + ddlMonth.Text + ","
                                 + PersonTypeCode + ","
                                 + LegalPersonTypeCode + ","
                                 + DbProxy.Instance.GetConfig("PayerName") + ","
                                 + DbProxy.Instance.GetConfig("CompanyName") + ","
                                 + DbProxy.Instance.GetConfig("CompanyBranch") + ","
                                 + DbProxy.Instance.GetConfig("EconomicNo") + ","
                                 + DbProxy.Instance.GetConfig("PostCode") + ","
                                 + DbProxy.Instance.GetConfig("Phone") + ","
                                 + DbProxy.Instance.GetConfig("Address") + ",";
            if (FirstSignature.Rows.Count > 0)
            {
                CompanyInfo += Helper.GetString(FirstSignature.Rows[0], "NatinoalCode", "") + ","
                               + Helper.GetString(FirstSignature.Rows[0], "Name", "") + ","
                               + Helper.GetString(FirstSignature.Rows[0], "Surname", "") + ","
                               + Helper.GetString(FirstSignature.Rows[0], "PostName", "") + ",";
            }
            else
            {
                CompanyInfo += ",,,,";
            }
            if (SecondSignature.Rows.Count > 0)
            {
                CompanyInfo += Helper.GetString(SecondSignature.Rows[0], "NatinoalCode", "") + ","
                               + Helper.GetString(SecondSignature.Rows[0], "Name", "") + ","
                               + Helper.GetString(SecondSignature.Rows[0], "Surname", "") + ","
                               + Helper.GetString(SecondSignature.Rows[0], "PostName", "");
            }
            else
            {
                CompanyInfo += ",,,";
            }


            //sw.WriteLine(CompanyInfo);
            DataTable fishs = DbProxy.Instance.FillData("SELECT ID, PersonnelID FROM Fish WHERE FishYear=" + ddlYear.Text + " AND FishMonth=" + ddlMonth.Text, "fishs");
            NumOfPersonnel = fishs.Rows.Count;
            for (int i = 0; i < fishs.Rows.Count; i++)
            {
                DataTable personnelInfo = DbProxy.Instance.FillData("SELECT * FROM Personnel WHERE ID=" + Helper.GetString(fishs.Rows[i], "PersonnelID", ""), "personnelInfo");
                string UnTaxPersonCode = Helper.GetEnumValuesCode(Helper.GetString(personnelInfo.Rows[0], "UnTaxPersonID", ""));
                if (!ShowUnTax.Checked && UnTaxPersonCode != "0")
                    NumOfPersonnel--;
                else
                {
                    string fid = Helper.GetString(fishs.Rows[i], "ID", "");
                    DataTable fishValues = DbProxy.Instance.FillData("SELECT * FROM FishValuesView WHERE FishID=" + fid, "fishValue");
                    string EmploymentTypeCode = Helper.GetString(DbProxy.Instance.FillScalar("SELECT Code FROM EmploymentType WHERE ID=" + Helper.GetString(personnelInfo.Rows[0], "EmploymentTypeID", "")), "");
                    string PostName = Helper.GetString(Helper.GetEnumValuesTitle(Helper.GetString(personnelInfo.Rows[0], "PostID", "")), "");
                    string JobName = Helper.GetString(DbProxy.Instance.FillScalar("SELECT Title FROM Job WHERE ID=" + Helper.GetString(personnelInfo.Rows[0], "JobID", "")), "");
                    string EducationCode = Helper.GetEnumValuesCode(Helper.GetString(personnelInfo.Rows[0], "EducationID", ""));
                    string CategoryCode = Helper.GetEnumValuesCode(Helper.GetString(personnelInfo.Rows[0], "CategoryID", ""));

                    string NationalityCode = Helper.GetEnumValuesCode(Helper.GetString(personnelInfo.Rows[0], "NationalityID", ""));
                    string InsuranceTypeCode = Helper.GetEnumValuesCode(Helper.GetString(personnelInfo.Rows[0], "InsuranceTypeID", ""));
                    string WorkPlaceStatusCode =
                        Helper.GetEnumValuesCode(Helper.GetString(personnelInfo.Rows[0], "WorkPlaceStatusID", ""));
                    string PersonnelStatusCode =
                        Helper.GetEnumValuesCode(Helper.GetString(personnelInfo.Rows[0], "PersonnelStatusID", ""));
                    string TaxItems = Helper.GetString(personnelInfo.Rows[0], "NatinoalCode", "") + "," //کد ملی
                                      + "1," //نوع پرداخت
                                      + Helper.GetInt(ddlMonth.SelectedValue,1) + "," //تعداد ماه کارکرد
                                      + "0," //آیا آخرین ماه کاری پرسنل است
                                      + "85," //نوع ارز
                                      + "1," //نرخ تسعیر ارز
                                      + Helper.FormatDate(Convert.ToDateTime(Helper.GetString(personnelInfo.Rows[0], "StartDate", null)), "yyyyMMdd") +"," //تاریخ شروع به کار
                                      + "," //تاریخ پایان کار
                                      + PersonnelStatusCode+ "," //وضعیت کارمند
                                      + WorkPlaceStatusCode + "," //وضعیت محل خدمت
                                      +
                                      Helper.GetFishValue(fishValues,
                                          Helper.GetField("TotalWagePrice", ReportFieldType.Tax), 0) + ","
                        //ناخالص حقوق و دستمزد مستمر ماه جاری
                                      + "," //پرداخت های مستمر معوق که مالیاتی برای آنها محاسبه نشده است
                                      + "," //مسکن
                                      + "," //مبلغ کسر شده از حقوق کارمند بابت مسکن ماه جاری
                                      + "," //وسیله نقلیه
                                      + "," //مبلغ کسر شده از حقوق کارمند بابت وسیله نقلیه ماه جاری
                                      + "," //پرداخت مزایای مستمر غیر نقدی ماه جاری
                                      + "," //هزینه های درمانی موضوع ماده 0۳7 ق.م.م.
                                      + "," //حق بیمه پرداختی موضوع ماده 0۳7 ق.م.م.
                                      + ","
                        //تسهیلات اعتباری مسکن از بانک ها )موضوع بند الف ماده 0۳۱ قانون برنامه سوم(      
                                      + "," //سایر معافیت ها
                                      + "," //ناخالص اضافه کاری ماه جاری
                                      + "," //سایر پرداخت های غیر مستمر نقدی ماه جاری
                                      + "," //پاداش های موردی ماه جاری
                                      + "," //پرداخت های غیر مستمر نقدی معوقه ماه جاری
                                      + "," //کسر می شود: معافیت های غیر مستمر نقدی
                                      + "," //پرداخت مزایای غیر مستمر غیر نقدی ماه جاری
                                      + "," //عیدی و مزایای پایان سال
                                      + "," //بازخرید مرخصی و بازخرید سنوات
                                      + "," //کسر می شود: معافیت
                                      + "," //معافیت مربوط به مناطق آزاد تجاری
                                      + "," //معافیت موضوع قانون اجتناب از اخذ مالیات مضاعف
                                      +Helper.GetFishValue(fishValues, Helper.GetField("Tax", ReportFieldType.Tax), 0)+"," //مالیات متعلّقه حقوق و دستمزد مستمر نقدی
                                      + Helper.GetFishValue(fishValues, Helper.GetField("TotalTax", ReportFieldType.Tax), 0);//جمع خالص مالیات متعلّقه ماه جاری
                    sw.WriteLine(TaxItems);
                }
            }
            mFile.Text += LangProvider.Instance.ParseLang("Lang.TaxFileCreated") + ": <a href='temp/Tax/" + folderName + "/" + fileName + "' download='" + fileName + "'>" + LangProvider.Instance.ParseLang("Lang.Download") + "</a><br />";
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
        }
        finally
        {
            if (sw != null)
            {
                sw.Flush();
                sw.Close();
                sw.Dispose();
            }
        }
    }
    void CreateShortTaxFile(string folder)
    {
        string fileName = "WK" + ddlYear.Text + ddlMonth.Text + ".txt";
        string file = Path.Combine(folder, fileName);
        if (!file.ToLower().EndsWith(".txt"))
            file += ".txt";
        StreamWriter sw = new StreamWriter(file, true, Encoding.UTF8);
        decimal TotalPrice = 0, Taxable = 0, Tax = 0, TotalIncomeExtraPaid = 0, TotalTaxExtraPaid = 0;
        try
        {
            //sw = File.CreateText(file);

            DataTable fishs =
                DbProxy.Instance.FillData(
                    "SELECT ID, PersonnelID FROM Fish WHERE FishYear=" + ddlYear.Text + " AND FishMonth=" +
                    ddlMonth.SelectedValue, "fishs");
            NumOfPersonnel = fishs.Rows.Count;
            for (int i = 0; i < fishs.Rows.Count; i++)
            {
                DataTable personnelInfo =
                    DbProxy.Instance.FillData(
                        "SELECT * FROM Personnel WHERE ID=" + Helper.GetString(fishs.Rows[i], "PersonnelID", ""),
                        "personnelInfo");
                string UnTaxPersonCode =
                    Helper.GetEnumValuesCode(Helper.GetString(personnelInfo.Rows[0], "UnTaxPersonID", ""));
                if (!ShowUnTax.Checked && UnTaxPersonCode != "0")
                    NumOfPersonnel--;
                else
                {
                    string fid = Helper.GetString(fishs.Rows[i], "ID", "");
                    DataTable fishValues = DbProxy.Instance.FillData(
                        "SELECT * FROM FishValuesView WHERE FishID=" + fid, "fishValue");

                    TotalPrice += Helper.GetFishValue(fishValues, Helper.GetField("TotalWagePrice", ReportFieldType.Tax),
                        0);
                    Taxable += Helper.GetFishValue(fishValues, Helper.GetField("Taxable", ReportFieldType.Tax), 0);
                    Tax += Helper.GetFishValue(fishValues, Helper.GetField("Tax", ReportFieldType.Tax), 0);
                    TotalIncomeExtraPaid += Helper.GetFishValue(fishValues,
                        Helper.GetField("TotalIncomeExtraPaid", ReportFieldType.Tax), 0);
                    TotalTaxExtraPaid += Helper.GetFishValue(fishValues,
                        Helper.GetField("TotalTaxExtraPaid", ReportFieldType.Tax), 0);
                }
            }
            string BankCode = Helper.GetEnumValuesCode(Bank.SelectedValue);
            string CompanyInfo = ddlYear.Text + "," //سال
                               + ddlMonth.Text + "," //ماه
                               + Helper.GetDecimal(CurFine3.Text, 0) + "," //بدهی مالیات ماه جاری
                               + PastFine3.Text + ","//بدهی مالیات ماه گذشته
                               + Date.Text.Replace("/","") + ","//تاریخ
                               + PayType.SelectedValue + "," //نوع پرداخت
                               + SerialNo.Text + "," //سریال چک
                               + CheckDate.Text.Replace("/", "") + ","//تاریخ چک
                               + BankCode + "," //بانک
                               + BranchName.Text + "," //شعبه
                               + AccountNo.Text + "," //شماره حساب
                               + CheckPrice.Text + ","//مبلغ چک
                               +",";//تاریخ و مبلغ واریز خزانه
            sw.WriteLine(CompanyInfo);
            TaxHistory(Helper.GetInt(ddlYear.SelectedValue, -1), Helper.GetInt(ddlMonth.SelectedValue, -1), TotalPrice, Taxable, Tax);
            lblTotalPrice.Text = SiteSettings.ToMonetary(TotalPrice, DateTime.Now).ToString();
            lblTaxable.Text = SiteSettings.ToMonetary(Taxable, DateTime.Now).ToString();
            lblTax.Text = SiteSettings.ToMonetary(Tax, DateTime.Now).ToString();
            mFile.Text += LangProvider.Instance.ParseLang("Lang.ShortTaxFileCreated") + ": <a href='temp/Tax/" +
                          folderName + "/" + fileName + "' download='" + fileName + "'>" +
                          LangProvider.Instance.ParseLang("Lang.Download") + "</a><br />";

        }
        catch (Exception ex)
        {
            Logger.Log(ex);
        }
        finally
        {
            if (sw != null)
            {
                sw.Flush();
                sw.Close();
                sw.Dispose();
            }
        }
    }

    string[] GetTaxPrice(string Month, string Year)
    {
        int cnt = 0;
        decimal TotalPrice = 0, Taxable = 0, TotalTax = 0;
        string[] shortTaxPrice = new string[3];
        DataTable PastFishs = DbProxy.Instance.FillData(string.Format("SELECT ID, PersonnelID, Taxable FROM Fish WHERE FishYear={0} AND FishMonth<={1}", Year, Month), "fishs");
        if (PastFishs.Rows.Count > 0)
            for (int i = 0; i < PastFishs.Rows.Count; i++)
            {
                string fid = Helper.GetString(PastFishs.Rows[i], "ID", "");
                DataTable fishValues = DbProxy.Instance.FillData("SELECT * FROM FishValuesView WHERE FishID=" + fid, "fishValue");
                DataTable personnelInfo = DbProxy.Instance.FillData("SELECT * FROM Personnel WHERE ID=" + Helper.GetString(PastFishs.Rows[i], "PersonnelID", ""), "personnelInfo");

                string UnTaxPersonCode = Helper.GetEnumValuesCode(Helper.GetString(personnelInfo.Rows[0], "UnTaxPersonID", ""));
                if (fishValues != null)
                    if (!ShowUnTax.Checked && UnTaxPersonCode != "0")
                        cnt++;
                    else
                    {
                        TotalPrice = TotalPrice + Helper.GetFishValue(fishValues, Helper.GetField("TotalWagePrice", ReportFieldType.Tax), 0);
                        Taxable = Taxable + Helper.GetFishValue(fishValues, Helper.GetField("TotalTaxable", ReportFieldType.Tax), 0);
                        TotalTax = TotalTax + Helper.GetFishValue(fishValues, Helper.GetField("TotalTax", ReportFieldType.Tax), 0);
                    }
            }
        shortTaxPrice[0] = TotalPrice.ToString();
        shortTaxPrice[1] = Taxable.ToString();
        shortTaxPrice[2] = TotalTax.ToString();
        return shortTaxPrice;
    }
    protected void CalcPastWage_Click1(object sender, EventArgs e)
    {
        DataTable pastTaxHistory = GetPastTaxHistory(Helper.GetInt(ddlYear.SelectedValue, -1), Helper.GetInt(ddlMonth.SelectedValue, -1));
        if (pastTaxHistory.Rows.Count > 0)
        {
            PastTotalPrice.Text = Helper.GetString(pastTaxHistory.Rows[0], "PastTotalPrice", "");
            PastTaxable.Text = Helper.GetString(pastTaxHistory.Rows[0], "PastTotalTaxable", "");
            PastTotalTax.Text = Helper.GetString(pastTaxHistory.Rows[0], "PastTotalTax", "");
            PastFine1.Text = Helper.GetString(pastTaxHistory.Rows[0], "PastTotalFine1", "");
            PastFine2.Text = Helper.GetString(pastTaxHistory.Rows[0], "PastTotalFine2", "");
            PastFine3.Text = Helper.GetString(pastTaxHistory.Rows[0], "PastTotalFine3", "");
        }
    }

    void TaxHistory(int year, int month, decimal totalPrice, decimal taxable, decimal tax)
    {
        string query = string.Format(@"SELECT ID FROM TaxHistory WHERE Year={0} AND Month={1}", year, month);
        int id = Helper.GetInt(DbProxy.Instance.FillScalar(query), -1);
        if (id > 0)
        {
            try
            {
                query = string.Format(@"UPDATE TaxHistory SET TotalPrice={0}, TotalTaxable={1}, TotalTax={2}, TotalFine1={3}, TotalFine2={4}, TotalFine3={5} WHERE ID={6}", totalPrice, taxable, tax, Helper.GetDecimal(CurFine1.Text, 0), Helper.GetDecimal(CurFine2.Text, 0), Helper.GetDecimal(CurFine3.Text, 0), id);
                DbProxy.Instance.ExecuteCommand(query);
            }
            catch (Exception)
            {

                throw;
            }
        }
        else
        {
            try
            {
                query = string.Format(@"INSERT INTO TaxHistory (Year, Month, TotalPrice, TotalTaxable, TotalTax, TotalFine1, TotalFine2, TotalFine3) VALUES ({0},{1},{2},{3},{4}, {5}, {6}, {7})", year, month, totalPrice, taxable, tax, Helper.GetDecimal(CurFine1.Text, 0), Helper.GetDecimal(CurFine2.Text, 0), Helper.GetDecimal(CurFine3.Text, 0));
                DbProxy.Instance.ExecuteCommand(query);
            }
            catch (Exception)
            {

                throw;
            }
        }

    }

    DataTable GetPastTaxHistory(int year, int month)
    {
        try
        {
            string query = string.Format(@"SELECT SUM(TotalPrice) AS PastTotalPrice, SUM(TotalTaxable) AS PastTotalTaxable, SUM(TotalTax) AS PastTotalTax, SUM(TotalFine1) AS PastTotalFine1, SUM(TotalFine2) AS PastTotalFine2, SUM(TotalFine3) AS PastTotalFine3 FROM TaxHistory WHERE Year={0} AND Month<{1} ", year, month);
            return DbProxy.Instance.FillData(query, "past");
        }
        catch (Exception)
        {
            throw;
        }
    }
}