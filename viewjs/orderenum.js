﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
92-10-16: file created
*/
function view_orderenum() {
    // ------------------------------
    // startup code
    // ------------------------------
    setChecked("#Enabled");
    if (toInt($('#ID').val(), -1) < 0)
        $('#Enabled').attr("checked", 'checked');
    $('.popupTitle').html(_Context.title);
    insertValidation();
}