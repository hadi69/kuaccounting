﻿
//author:ZAHIR   1393/5/27

function view_mloan() {
    
    $("#tableItems").smartTable();
    var cnt = 0, mID = -1;

    function addLoanItem(node) {
        var row = '<tr>';

        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">' + (cnt + 1) + '</td>';

        row += '<td data-th="' + Lang.PayDate + '">'
            + '<input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="hidden"  name="LoansID" value="' + getNodeVal(node, "LoansID", -1) + '" />'
            + '<input type="text" name="PayDate' + cnt + '" id="pd_' + cnt + '" value="' + getNodeVal(node, "PayDate")
            + '" class="dateSelect PayDate form-control input-sm align-center" />'
            + '</td>';
        row += '<td data-th="' + Lang.PayAmount + '">'
            + '<input type="text" id="pa_' + cnt + '" name="PayAmount" value="' + getNodeVal(node, "PayAmount") + '" class="PayAmount validate[custom[integer]] form-control input-sm"/>'
            + '</td>';
        row += '<td data-th="' + Lang.PayStatus + '">'
            + '<select id="ps_' + cnt + '"  class="PayStatus" name="PayStatus" v="' + getNodeVal(node, "PayStatus") + '" >'
            + '<option value="0">' + Lang.NotPaid + '</option>'
            + '<option value="1">' + Lang.Paid + '</option>'
            + '</select>'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a class="btn btn-danger btn-xs btn-block btn-remove removeLoanItem" id="r_' + cnt + '">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';

        cnt++;
        return row;
    }

    function removeLoanItemRow($who) {
        var id = $who.attr('id'); // r_1
        id = id.substring(2); // r_1  -->  1
        //  alert(id);
        $('#pa_' + id).val(""); // 
        $who.parent().parent().hide();
        Growl.Notice(Lang.DeletedSuccessfully);
    }

    mID = toInt($("#MasterID").val(), -1);
    fillTable("get.aspx?op=GetXml&table=LoanItems&qd=loanitem&LoansID=" + mID, '#tableItems', addLoanItem);
    dateSelect('.dateSelect');
    autoComplete('Funds', 'fundsName', '#FundName', '#FundID');
    autoComplete('Personnel', 'staffauto', '#PersonnelName', '#PersonnelID');
    setValue("#Status");
    setValue('.PayStatus');
    if ($("#TotalPaied").val() == '')
        $("#TotalPaied").val(0);

    
    
    doMore();

    insertValidation();
    makeChosen();
    $('#InstallmentStart').click(function () {
        var table = $('#tableItems');
        var date = $('#StartDate').val().split('/');
        var num = $('#InstallmentNum').val();
        var amount = toFloat($('#Amount').val(), 0);
        var percent = $('#BenefitPercent').val();
        var monthly = toFloat($('#MonthlyPay').val(), 0);
        var payamount = 0;
        if (percent > 0) {
            amount = amount + amount * percent / 100;
        }
        if (monthly > 0) {
            payamount = monthly;
        }
        else {
            payamount = amount / num;
        }
        if (date[2] == 30 || date[2] == 31) {
            alert(Lang.LoanJsEM1);
            return;
        }
        if (date[1] == null || !(num && amount && percent)) {
            alert(Lang.LoanJsEM2);
            return;
        }
        for (var i = 1; i <= num; i++) {
            if (amount % num > 0 && i == num) {
                if (monthly > 0) {
                    payamount = amount - (payamount * (i - 1))
                }
                else {
                    payamount = amount % num;
                }
            }
            $('tr:last', table).after(addLoanItem(null));
            var lastRow = $('tr:last', table);
            $('tr:last .PayAmount', table).val(payamount);
            $('tr:last .PayStatus', table).val('0');
            if (i == 1) {
                $('tr:last .PayDate', table).val(date.join('/'));
            }
            else {
                if (date[1] < 12) {
                    date[1]++;
                }
                else {
                    date[0]++;
                    date[1] = '01';
                }
                $('tr:last .PayDate', table).val(date.join('/'));
            }
        }
    });
    function doMore() {
        dateSelect("#tableItems .dateSelect");
        $('.addLoanItem').click(function () {
            var table = $("#tableItems");
            $('tr:last', table).after(addLoanItem(null));
            Growl.Notice(Lang.RowAddedSuccessfully);
            dateSelect('#tableItems tr:last .dateSelect');
            var lastRow = $('tr:last', table);
            $('.removeLoanItem', lastRow).click(function () {
                removeLoanItemRow($(this));
            });
        });

        $('.removeLoanItem').click(function () {
            removeLoanItemRow($(this));
        });
    }
    commaInputs("#subDetailForm");
    monetaryKeys();
}