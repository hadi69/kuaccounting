﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
93-01-12: file created
*/
function view_memberships() {

    function doCommand(com, grid) {
        if (com == Lang.EditPay) {
            showEditPay();
            return;
        }
        generalCommand(com, grid, show);
    }
    function run() {
        var fID = $.urlParam('id');
        var more = "FundID=" + fID;
        var terminology = getTerminology(_Context.table);
        if (terminology)
            $('#term').html("<a href='help/terminology.aspx' target=_blank title='" + Lang.HasTerminology + "'>" + terminology + "</a>");
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + '&more=' + more + appendTime();
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.FundsID, name: 'FundName', sortable: true, css_class: 'width-120' },
                    { display: Lang.PersonnelName, name: 'PersonnelName', sortable: true },
                    { display: Lang.MonthlyPay, name: 'MonthlyPay', sortable: true, css_class: 'width-100 align-center', process: showMonetary },
                    { display: Lang.TotalPaied, name: 'TotalPaied', sortable: true, css_class: 'width-100 align-center', process: showMonetary },
                    { display: Lang.Status, name: 'Status', sortable: false, css_class: 'width-80 align-center', process: displayStatus },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, css_class: 'action-cell', process: function (celDiv, id) { editLink(celDiv, id, show); } },
                    { display: Lang.ColDel, name: 'Del', sortable: false, css_class: 'action-cell', process: delLink }
                ],
                buttons:
                [
                    { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                    { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                    { separator: true },
                    { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                    { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand },
                    { separator: true },
                    { name: Lang.EditPay, bclass: 'editpay', onpress: doCommand }
                ],
                sortname: "PersonnelName",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                }
            }
            );
    }

    function displayStatus(celDiv, id) {
        var status = parseInt($(celDiv).html(), 6);
        displayMembershipStatusEnum(celDiv, status);
    }

    function showcallback(v, m, f) {
        return newShowcallback(v, m, f, 'membershipsupdate', 'membershipsinsert', _Context.table, null, show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        var fID = $.urlParam('id');
        var m = "&ID=" + fID;
        var fName = getServerValue("selectfund", "Funds", m);
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + '&FundID=' + fID + '&FundName=' + fName + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }

    function editshowcallback(v, m, f) {
        if (v == "OK") {
            $('#tableItems tr').each(function () {
                var id = $(this).find('td:nth-child(2)').find('input:first').val();
                var val = toFloat($(this).find('td:nth-child(4)').find('input').val(), -1);
                if (id > 0 && val >= 0) {
                    runQuery('membershippayupdate', 'Memberships', "&MonthlyPay=" + val + "&ID=" + id);
                }
            });
            debugger;
            Growl.Notice(Lang.UpdatedSuccessfully);
            $("#flexi").flexReload();
        }
    }
    function showEditPay() {
        var fID = $.urlParam('id');
        var m = "&ID=" + fID;
        var fName = getServerValue("selectfund", "Funds", m);
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&view=editmembership&FundID=' + fID + '&FundName=' + fName + appendTime();
        showPopup(_url, editshowcallback);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    $("#bar").prepend(breadcrumb({
        title: _Context.title,
        grandParent: {
            title: Lang.MSalary,
            url: 'menu/salary.aspx'
        },
        parent: {
            title: Lang.Funds,
            url: 'list.aspx?table=Funds&menu=salarysa'
        }
    }));
    setClassSalary();
    run();
}