﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-10-19: file created
*/
function view_paycheckshops() {
    function doCommand(com, grid) {
        if (com == Lang.ShowReceived) //
        {
            if ($('.showReceived').hasClass('glyphicon-ok')) {
                $('.showReceived').removeClass('glyphicon glyphicon-ok');
            } else {
                $('.showReceived').toggleClass('glyphicon glyphicon-ok');
            }
            var more = '__PrOgrAmconD__ AND Status NOT IN(1,5,2) AND IsShop = 1';
            if ($('.showReceived').hasClass('glyphicon-ok')) {
                more = '__PrOgrAmconD__  AND IsShop = 1';
            }
            var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=paycheck' + '&more=' + more + appendTime();
            $("#flexi").flexOptions({ url: _url });
            $('#fRefresh').click();
            return;
        }
        if (com == Lang.GBMultiAction) {
            var ids = '', id = '', multiStatus = -1, oldStatus = -1, counter = 0, oldCStatus = 'False', customerStatus = 'False';
            $('.trSelected', grid).each(function () {
                multiStatus = getStatus(getColClass(this, 9), 2);
                customerStatus = $('._cstatus', this).html();
                if (oldStatus != multiStatus || customerStatus != oldCStatus) {
                    if (counter == 0) {
                        oldStatus = multiStatus;
                        oldCStatus = customerStatus;
                        counter = 1;
                    }
                    else {
                        Growl.Error(Lang.DifferentStatus, 3000);
                        return false;
                    }
                }
                id = $(this).attr('id');
                id = id.substring(id.lastIndexOf('row') + 3);
                ids += ',' + id;
            });
            ids = ids.replace(',', '');
            if (ids == '') {
                Growl.Error(Lang.NoCheckSelected, 3000);
                return false;
            }
            if (oldStatus != multiStatus || customerStatus != oldCStatus) {
                return false;
            }
            if (ids.split(',').length > 15) {
                Growl.Error(Lang.MultiChecksShopMax, 3000);
                return false;
            }
            var statusName = Lang.ColToCustomer;
            if (oldCStatus == 'True') {
                statusName = Lang.CheckStatusFund;
            }
            showMenu({
                parent: this,
                width: '110px',
                cmds: [
                      [statusName, multiStatus == '0' ? 'ToCustomer' : '']
                     ,[Lang.ColReceive, multiStatus == '0' ? 'Receive' : ''] // وصول
                    , [Lang.ColRefuse, multiStatus == '0' ? 'Refuse' : ''] // برگشت
                ],

                callback: function (cmd) {
                    if (cmd == 'Receive') {
                        doConfirm(function yes() { showPayCheckReceive(ids, 'PayCheck', 'multiupdatepaycheckshop', true); }, Lang.MultiChangeCheckStatus1 + '"' + Lang.CheckStatusReceived + '"' + Lang.ChangeCheckStatus2, function no() { return; });
                    } else if (cmd == 'Refuse') {
                        doConfirm(function yes() { showPayCheckRefuse(ids, 'PayCheck', 'multiupdatepaycheckshop', true); }, Lang.MultiChangeCheckStatus1 + '"' + Lang.CheckStatusRefused + '"' + Lang.ChangeCheckStatus2, function no() { return; });
                    } else if (cmd == 'ToCustomer') {
                        if (oldCStatus == 'True') {
                            runQuery('multipaychecktocustomer', 'paycheck', 'CustomerStatus=0&MultiIDs=' + ids);
                            $('#fRefresh').click();
                        } else {
                            runQuery('multipaychecktocustomer', 'paycheck', 'CustomerStatus=1&MultiIDs=' + ids);
                            $('#fRefresh').click();
                        }
                    }
                }
            });
            return false;
        }
        generalCommand(com, grid, show, Lang.AutoChecks, "ID;CheckType", "Status=0^CheckStatusNew^1^CheckStatusReceived^2^CheckStatusRefused^3^CheckStatusSleep^4^CheckStatusExpend^5^CheckStatusSleepReceive");
    }
    function showSumAmount() {
        var val = getServerValueFiltered("paychecksshopsumamount", "PayCheck");
        if (!val)
            val = 0;
        $('#SumAmount').html(numberWithCommas(toMonetary(val, _Context.today)));
    }
    function run() {
        var more = '__PrOgrAmconD__ AND Status NOT IN(1,5,2) AND IsShop = 1';
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=paycheck' + '&more=' + more + appendTime();
        //alert(_url);
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'center', hide: true },
                    { display: Lang.Serial, name: 'CustomerStatus', sortable: true, hide: true, process: displayCustomerStatus },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.BehindNum, name: 'No', sortable: true, css_class: 'width-80 align-center' },
                    { display: Lang.Bank, name: 'BankName', sortable: true, css_class: 'width-200 align-center' },
                    { display: Lang.ReceiverName, name: 'ReceiverName', sortable: true, css_class: 'width-220 align-center' },
                    { display: Lang.DueDate, name: 'DueDate', sortable: true, process: showDate, css_class: 'date-cell' },
                    { display: Lang.Amount, name: 'Amount', sortable: true, process: showMonetary, css_class: 'width-100 align-center', footer: 'sum' },
                    { display: Lang.Status, name: 'Status', sortable: true, process: displayStatus, css_class: 'width-100 align-center' },
                    { display: Lang.BankAccountCheckName, name: 'BankAccountCheckName', hide:true },
                    { display: Lang.Serial, name: 'Serial', sortable: true },
                    { display: Lang.ColOperation, name: 'Edit', sortable: false, hide: hideColumn("bank", "paychecksshops", "edit"), process: myEditLink, css_class: 'action-cell' },
                    { display: Lang.ColDel, name: 'Del', sortable: false, hide: hideColumn("bank", "paychecksshops", "delete"), process: myDelLink, css_class: 'action-cell' }
                ],
                buttons:
                [
                   { name: Lang.GBNew, bclass: 'new', onpress: doCommand, onaccess: hasAccess("bank", "paychecksshops", "new") },
                   { name: Lang.GBEdit, bclass: 'edit', onpress: doCommand, onaccess: hasAccess("bank", "paychecksshops", "edit") },
                   { name: Lang.GBPrint, bclass: 'print', onpress: doCommand, onaccess: hasAccess("bank", "paychecksshops", "print") },
                   { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand, onaccess: hasAccess("bank", "paychecksshops", "excel") },
                   { name: Lang.MultiAction, bclass: 'multiAction', onpress: doCommand },
                   { name: Lang.ShowReceived, bclass: 'showReceived', onpress: doCommand }
                ],
                searchitems: null,
                sortname: "No",
                sortorder: "desc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,

                height: gridHeight,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                },
                onSuccess: function (grid) {
                    restoreSelectedRows('#flexi');
                    if (urlId) {
                        $('#row' + urlId).addClass('trSelected');
                    }
                }
            }
            );
        showSumAmount();
        _Context.filterPostBack = showSumAmount;

    }
    function displayCustomerStatus(celDiv, id) {
        $(celDiv).addClass('_cstatus');
    }
    function myEditLink(celDiv, id) {
        $(celDiv).html('<img src="img/edit.gif" />');
        $(celDiv).click(function (event) {
            var tr = $(celDiv).closest('tr');
            window.tr = tr;
            $('#flexi tr').removeClass('trSelected');
            tr.addClass('trSelected');
            var status = getStatus(getColClass(tr, 9), 2);
            var customerStatus = $('._cstatus', tr).html();
            var statusName = Lang.ColToCustomer;
            if (customerStatus == 'True') {
                statusName = Lang.CheckStatusFund;
            }
                showMenu({
                    parent: this,
                    width: '100px',
                    cmds: [
                        [Lang.ColEdit, 'Edit']
                         , [statusName, status == '0' ? 'ToCustomer' : '']
                        , [Lang.ColReceive, status == '0' ? 'Receive' : ''] // وصول 
                        , [Lang.ColRefuse, status == '0' ? 'Refuse' : ''] //برگشت
                    ],

                    callback: function (cmd) {
                        if (cmd == 'Edit') {
                            show(id, celDiv);
                        } else if (cmd == 'Receive')
                            doConfirm(function yes() { showPayCheckReceive(id, 'PayCheck', 'updatepaycheckshop'); }, Lang.ChangeCheckStatus1 + '"' + Lang.CheckStatusReceived + '"' + Lang.ChangeCheckStatus2, function no() { return; });
                        else if (cmd == 'Refuse')
                            doConfirm(function yes() { showPayCheckRefuse(id, 'PayCheck', 'updatepaycheckshop'); }, Lang.ChangeCheckStatus1 + '"' + Lang.CheckStatusRefused + '"' + Lang.ChangeCheckStatus2, function no() { return; });
                        else if (cmd == 'ToCustomer') {
                            if (customerStatus == 'True') {
                                runQuery('paychecktocustomer', 'paycheck', 'CustomerStatus=0&PaycheckID=' + id);
                                $('#fRefresh').click();
                            } else {
                                runQuery('paychecktocustomer', 'paycheck', 'CustomerStatus=1&PaycheckID=' + id);
                                $('#fRefresh').click();
                            }
                        }
                    }
                });
        });
    }
    function myDelLink(celDiv, id) {
        $(celDiv).html('<img src="img/delete.gif" />');
        $(celDiv).click(function (event) {
            if (clicksDisabled)
                return;
            disableClicks();
            event.preventDefault();
            doConfirm(function () {
                _url = 'set.aspx?op=Delete&table=PayCheck&id=' + id + appendTime();
                //alert(_url);
                $.ajax({
                    url: _url,
                    type: 'GET',
                    success: function (html) {
                        message(html);
                        if (html.indexOf("Error:") >= 0 || html.indexOf('خطا:') >= 0) {
                        }
                        else
                            $("#flexi").flexReload();
                    },
                    error: ajaxError
                });
            });
        });
    }
    function displayStatus(celDiv, id) {
        var _mode = parseInt($(celDiv).html(), 12);
        var customerStatus = $('._cstatus').eq(-1).html();
        displayPayCheckStatus(celDiv, _mode, customerStatus);
    }

    function showcallback(v, m, f) {
        restoreSelectedRows('#flexi');
        if (v == 'OK' && !checkProgramStatus())
            return false;
        _Context.priceDate = f.IssueDate;
            if (v == "Print") {
                var template = getServerValue("bankprinttemplate", "BankPrint", "&ID=" + f.ID);
                if (template && template != '')
                    return generalPrintTemplate('#subDetailForm', template);
                return generalPrint(v, m, f, '#subDetailForm');
            }
            // alert(TypeOfSelect);
            return newShowcallback(v, m, f, 'paycheckupdate', 'paycheckinsert', 'PayCheck', '#subDetailForm', show);
        }
    function show(id, celDiv, successCallback) {
        saveTrSelected(id);
        window.celDiv = celDiv;
        var table = _Context.table;
        var qd = _Context.query;
        _Context.table = "PayCheck";
        _Context.query = "paycheckShop";
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&view=paycheckshop&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, showcallback, getDefaultButtons(true, 'bank', "paychecksshops"), null, null, true);
        _Context.table = table;
        _Context.query = qd;
    }
    function showPayCheckReceive(id, table, qd, multi) {
        if (!multi) {
            saveTrSelected(id);
            updateCheckStatus(id, CheckStatusEnum.Received, table, qd);
        } else {
            Growl.Notice(Lang.MultiChecksShopWating, 4000);
            updateCheckStatus('', CheckStatusEnum.Received, table, qd, id);
        }
    }
    function showPayCheckRefuse(id, table, qd, multi) {
        if (!multi) {
            saveTrSelected(id);
            updateCheckStatus(id, CheckStatusEnum.Refused, table, qd);
        } else {
            Growl.Notice(Lang.MultiChecksShopWating, 4000);
            updateCheckStatus('', CheckStatusEnum.Refused, table, qd, id);
        }
    }
    function updateCheckStatus(checkId, newStatus, table, qd, mIds) {

        // alert(table);
        // alert(qd);
        var _url = 'set.aspx?op=RunScalar&table=' + table + '&qd=' + qd + appendTime();
        var form_data = {
            id: checkId,
            status: newStatus,
            multiids: mIds
        };

        $.ajax({
            url: _url,
            data: form_data,
            success: function () {
                Growl.Notice(Lang.Success, 3000);
                $("#flexi").flexReload();
            },
            error: function (xhr, textStatus) {
            if (textStatus === 'timeout') {
                Growl.Error(Lang.MultiChecksShopTimeOut, 5000);
            }
        },
        });
    }
    function saveTrSelected(id) {
        $('.trSelected').removeClass('trSelected');
        $('#row' + id).addClass('trSelected');
         saveSelectedRows('#flexi');
        $('.trSelected').removeClass('trSelected');
    }

    // ------------------------------
    // startup code
    // ------------------------------
    var urlId = $.urlParam('id');
    dateSelect('.dateSelect');
    checkAccess(['bank,paychecksshops']);
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.PayChecks, parentTitle: Lang.MAccounting, parentUrl: 'menu/accounting.aspx' } }));
    setClassBank();
    run();
}
