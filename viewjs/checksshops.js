﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-10-19: file created
*/
function view_checksshops() {
    function doCommand(com, grid) {
        if (com == Lang.ShowReceived) //
        {
            if ($('.showReceived').hasClass('glyphicon-ok')) {
                $('.showReceived').removeClass('glyphicon glyphicon-ok');
            } else {
                $('.showReceived').toggleClass('glyphicon glyphicon-ok');
            }
            var more = '__PrOgrAmconD__ AND Status NOT IN(1,5,4,2) AND IsShop = 1';
            if ($('.showReceived').hasClass('glyphicon-ok')) {
                more = '__PrOgrAmconD__  AND IsShop = 1';
            }
            var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=check' + '&more=' + more + appendTime();
            $("#flexi").flexOptions({ url: _url });
            $('#fRefresh').click();
            return;
        }
        if (com == Lang.GBMultiAction) {
            var ids = '' , id = '', multiStatus = -1, oldStatus = -1, counter = 0 ;
            $('.trSelected', grid).each(function () {
                multiStatus = getStatus(getColClass(this, 9), 2);
                if (oldStatus != multiStatus) {
                    if (counter == 0) {
                        oldStatus = multiStatus;
                        counter = 1;
                    }
                    else {
                        Growl.Error(Lang.DifferentStatus, 3000);
                        return false;
                    }
                }
                id = $(this).attr('id');
                id = id.substring(id.lastIndexOf('row') + 3);
                ids += ',' + id;
            });
            ids = ids.replace(',', '');
            if (ids == '') {
                Growl.Error(Lang.NoCheckSelected, 3000);
                return false;
            }
            if (oldStatus != multiStatus) {
                return false;
            }
            if (ids.split(',').length > 15) {
                Growl.Error(Lang.MultiChecksShopMax, 3000);
                return false;
            }
            showMenu({
                parent: this,
                width: '130px',
                cmds: [
                    //[Lang.ColEdit, 'Edit']
                      [Lang.ColReceive, multiStatus == '0' || multiStatus == '6' ? 'Receive' : ''] // وصول
                    , [Lang.ColRefuse, multiStatus == '0' || multiStatus == '6' ? 'Refuse' : ''] // برگشت
                    , [Lang.ColRefuseVisitor, multiStatus == '6' ? 'ColRefuseVisitor' : '']
                    , [Lang.ColSleep, multiStatus == '0' ? 'Sleep' : ''] // خواباندن 
                    , [Lang.ColExpend, multiStatus == '0' ? 'Expend' : ''] // خرج
                    , [Lang.ColVisitor, multiStatus == '0' ? 'Visitor' : '']
                    , [Lang.ColRefuseSleep, multiStatus == '3' ? 'RefuseSleep' : '']
                    , [Lang.ColSleepReceive, multiStatus == '3' ? 'SleepReceive' : '']
                    , [Lang.ColRefuseExpend, multiStatus == '4' ? 'RefuseExpend' : '']
                ],

                callback: function (cmd) {
                    if (cmd == 'Edit') {
                        show(id, celDiv);
                    } else if (cmd == 'Receive') {
                        showMultiReceive(ids, 'Checks', 'multiupdatecheckshop');
                    } else if (cmd == 'Refuse') {
                        doConfirm(function yes() { showRefuse(ids, 'Checks', 'multiupdatecheckshop', true); }, Lang.MultiChangeCheckStatus1 + '"' + Lang.CheckStatusRefused + '"' + Lang.ChangeCheckStatus2, function no() { return; });
                    } else if (cmd == 'Sleep') {
                        showMultiSleep(ids, 'Checks', 'multiupdatecheckshop');
                    } else if (cmd == 'Expend') {
                        showMultiExpand(ids, 'Checks', 'multiupdatecheckshop');
                    } else if (cmd == 'Visitor') {
                        showMultiVisitor(ids, 'Checks', 'multiupdatecheckshop');
                    } else if (cmd == 'ColRefuseVisitor') {
                        doConfirm(function yes() { showRefuseVisitor(ids, 'Checks', 'multiupdatecheckshop', true); }, Lang.MultiChangeCheckStatus1 + '"' + Lang.CheckStatusNew + '"' + Lang.ChangeCheckStatus2, function no() { return; });
                    } else if (cmd == 'RefuseSleep') {
                        doConfirm(function yes() { showRefuseSleep(ids, 'Checks', 'multiupdatecheckshop', true); }, Lang.MultiChangeCheckStatus1 + '"' + Lang.CheckStatusNew + '"' + Lang.ChangeCheckStatus2, function no() { return; });
                    } else if (cmd == 'SleepReceive') {
                        doConfirm(function yes() { showSleepReceive(ids, 'Checks', 'multiupdatecheckshop', true); }, Lang.MultiChangeCheckStatus1 + '"' + Lang.CheckStatusSleepReceive + '"' + Lang.ChangeCheckStatus2, function no() { return; });
                    } else if (cmd == 'RefuseExpend') {
                        doConfirm(function yes() { showRefuseExpend(ids, 'Checks', 'multiupdatecheckshop', true); }, Lang.MultiChangeCheckStatus1 + '"' + Lang.CheckStatusNew + '"' + Lang.ChangeCheckStatus2, function no() { return; });
                    }
                }
            });
            return false;
        }
        generalCommand(com, grid, show, Lang.AutoChecks, "ID;CheckType", "Status=0^CheckStatusNew^1^CheckStatusReceived^2^CheckStatusRefused^3^CheckStatusSleep^4^CheckStatusExpend^5^CheckStatusSleepReceive");
    }
    function showSumAmount() {
        var val = getServerValueFiltered("checkshopsumamount", "Checks");
        if (!val)
            val = 0;
        $('#SumAmount').html(numberWithCommas(toMonetary(val, _Context.today)));
    }
    function run() {
        var more = '__PrOgrAmconD__ AND Status NOT IN(1,5,4,2) AND IsShop = 1';
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=check' + '&more=' + more + appendTime();
        //alert(_url);
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'center', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.BehindNum, name: 'No', sortable: true, css_class: 'width-60 align-center' },
                    { display: Lang.OwnerCheck, name: 'PersonnelName', sortable: true, css_class: 'align-center' },
                    { display: Lang.Bank, name: 'BankAccountName', sortable: true, css_class: 'width-110 align-center' },
                    { display: Lang.ReceiverBankName, name: 'ReceiverBankName', sortable: true, css_class: 'width-120 align-center' },
                    { display: Lang.DueDate, name: 'DueDate', sortable: true, process: showDate, css_class: 'date-cell' },
                    { display: Lang.Amount, name: 'Amount', sortable: true, process: showMonetary, css_class: 'width-70 align-center', footer: 'sum' },
                    { display: Lang.Status, name: 'Status', sortable: true, process: displayStatus, css_class: 'width-50 align-center'},
                    { display: Lang.Visitor, name: 'VisitorName', hide: true, sortable: true, css_class: 'width-80 align-center' },
                    { display: Lang.FollowUpTime, name: 'FollowUpTime', hide: true, sortable: true, process: showDate, css_class: 'date-cell' },
                    { display: Lang.Serial, name: 'Serial', sortable: true, css_class: 'width-80 align-center' },
                    //{ display: Lang.ColEdit, name: 'Edit', width: 40, sortable: false, align: 'left', process: function (celDiv, id) { editLink(celDiv, id, show) } },
                    { display: Lang.ColOperation, name: 'Edit', sortable: false, hide: hideColumn("bank", "checksshops", "edit"), process: myEditLink, css_class: 'action-cell' },
                    { display: Lang.ColDel, name: 'Del', sortable: false, hide: hideColumn("bank", "checksshops", "delete"), process: myDelLink, css_class: 'action-cell' }
                ],
                buttons:
                [
                   { name: Lang.GBNew, bclass: 'new', onpress: doCommand, onaccess: hasAccess("bank", "checksshops", "new") },
                   { name: Lang.GBEdit, bclass: 'edit', onpress: doCommand, onaccess: hasAccess("bank", "checksshops", "edit") },
                   { name: Lang.GBPrint, bclass: 'print', onpress: doCommand, onaccess: hasAccess("bank", "checksshops", "print") },
                   { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand, onaccess: hasAccess("bank", "checksshops", "excel") },
                   { name: Lang.MultiAction, bclass: 'multiAction', onpress: doCommand},
                   { name: Lang.ShowReceived, bclass: 'showReceived', onpress: doCommand, onaccess: hasAccess("bank", "checksshops", "edit") }
                ],
                searchitems: null,
                sortname: "No",
                sortorder: "desc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,

                height: gridHeight,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                },
                onSuccess: function(grid) {
                    restoreSelectedRows('#flexi');
                    if (urlId) {
                        $('#row' + urlId).addClass('trSelected');
                    }
                }
            }
            );
        showSumAmount();
        _Context.filterPostBack = showSumAmount;

    }
    function myEditLink(celDiv, id) {
        $(celDiv).html('<img src="img/edit.gif" />');
        $(celDiv).click(function(event) {
            var tr = $(celDiv).closest('tr');
            window.tr = tr;
            $('#flexi tr').removeClass('trSelected');
            tr.addClass('trSelected');
            var status = getStatus(getColClass(tr, 9), 2);
                showMenu({
                    parent: this,
                    width: '120px',
                    cmds: [
                        [Lang.ColEdit, 'Edit'], [Lang.ColReceive, status == '0' || status == '6' ? 'Receive' : ''] // وصول
                        , [Lang.ColRefuse, status == '0' || status == '6' ? 'Refuse' : ''] // برگشت
                        , [Lang.ColRefuseVisitor, status == '6' ? 'ColRefuseVisitor' : '']
                        , [Lang.ColSleep, status == '0' ? 'Sleep' : ''] // خواباندن 
                        , [Lang.ColExpend, status == '0' ? 'Expend' : ''] // خرج
                        , [Lang.ColRefuseSleep, status == '3' ? 'RefuseSleep' : '']
                        , [Lang.ColSleepReceive, status == '3' ? 'SleepReceive' : '']
                        , [Lang.ColRefuseExpend, status == '4' ? 'RefuseExpend' : '']
                    ],

                    callback: function(cmd) {
                        if (cmd == 'Edit') {
                            show(id, celDiv);
                        } else if (cmd == 'Receive') {
                            showReceive(id, 'Checks', 'updatecheckshop');
                        } else if (cmd == 'Refuse') {
                            doConfirm(function yes() { showRefuse(id, 'Checks', 'updatecheckshop'); }, Lang.ChangeCheckStatus1 + '"' + Lang.CheckStatusRefused + '"' + Lang.ChangeCheckStatus2, function no() { return; });
                        } else if (cmd == 'Sleep') {
                            showSleep(id);
                        } else if (cmd == 'Expend') {
                            showExpend(id);
                        } else if (cmd == 'ColRefuseVisitor') {
                            doConfirm(function yes() { showRefuseVisitor(id, 'Checks', 'updatecheckshop'); }, Lang.ChangeCheckStatus1 + '"' + Lang.CheckStatusNew + '"' + Lang.ChangeCheckStatus2, function no() { return; });
                        } else if (cmd == 'RefuseSleep') {
                            doConfirm(function yes() { showRefuseSleep(id, 'Checks', 'updatecheckshop'); }, Lang.ChangeCheckStatus1 + '"' + Lang.CheckStatusNew + '"' + Lang.ChangeCheckStatus2, function no() { return; });
                        } else if (cmd == 'SleepReceive') {
                            doConfirm(function yes() { showSleepReceive(id, 'Checks', 'updatecheckshop'); }, Lang.ChangeCheckStatus1 + '"' + Lang.CheckStatusSleepReceive + '"' + Lang.ChangeCheckStatus2, function no() { return; });
                        } else if (cmd == 'RefuseExpend') {
                            doConfirm(function yes() { showRefuseExpend(id, 'Checks', 'updatecheckshop'); }, Lang.ChangeCheckStatus1 + '"' + Lang.CheckStatusNew + '"' + Lang.ChangeCheckStatus2, function no() { return; });
                        }
                    }
                });
        });
    }
    function myDelLink(celDiv, id) {
        $(celDiv).html('<img src="img/delete.gif" />');
        $(celDiv).click(function (event) {
            if (clicksDisabled)
                return;
            disableClicks();
            event.preventDefault();

            doConfirm(function () {
                var _url = 'set.aspx?op=Delete&table=Checks&id=' + id + appendTime();
                $.ajax({
                    url: _url,
                    type: 'GET',
                    success: function (html) {
                        message(html);
                        if (html.indexOf("Error:") >= 0 || html.indexOf('خطا:') >= 0) {
                        }
                        else
                            $("#flexi").flexReload();
                    },
                    error: ajaxError
                });
            });
        });
    }
    function displayStatus(celDiv, id) {
        var _mode = parseInt($(celDiv).html(), 12);
        displayCheckStatus(celDiv, _mode);
    }
    function showReceiveLink(celDiv, id) {
        $(celDiv).html('<img src="img/edit.gif" />');
        $(celDiv).click(function(event) {
            event.preventDefault();
            showReceive(id);
        });
    }


    function showcallback(v, m, f) {
        restoreSelectedRows('#flexi');
        if (v == 'OK' && !checkProgramStatus()) 
            return false;
           _Context.priceDate = f.IssueDate;
            return newShowcallback(v, m, f, 'checkupdate', 'checkinsert', 'Checks', '#subDetailForm', show);
    }
    function show(id, celDiv, successCallback) {
        saveTrSelected(id);
        window.celDiv = celDiv;
        var table = _Context.table;
        var qd = _Context.query;
        _Context.table = "Checks";
        _Context.query = "checkshop";
        _Context.id = id;
            var _url = 'get.aspx?op=GetHtml&view=checksshop&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
            showPopup(_url, showcallback, getDefaultButtons(true, 'bank', "checksshops"), null, null, true);
            _Context.table = table;
            _Context.query = qd;
    }
    function showReceive(id, table, qd, multi) {
        saveTrSelected(id);
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&view=checksshop&table=Checks&qd=checkshop&id=' + id + '&setstatus=' + CheckStatusEnum.Received + appendTime();
        showPopup(_url, showcallback, getButtons(), null, null, true);
        //if (!multi) {
        //    saveTrSelected(id);
        //    updateCheckStatus(id, CheckStatusEnum.Received, table, qd);
        //} else {
        //    Growl.Notice(Lang.MultiChecksShopWating, 4000);
        //    updateCheckStatus('', CheckStatusEnum.New, table, qd, id);
        //}
    }

    function showRefuse(id, table, qd, multi) {
        if (!multi) {
            saveTrSelected(id);
            updateCheckStatus(id, CheckStatusEnum.Refused, table, qd);
        } else {
            Growl.Notice(Lang.MultiChecksShopWating, 4000);
            updateCheckStatus('', CheckStatusEnum.Refused, table, qd, id);
        }
    }
    function showSleep(id) {
        saveTrSelected(id);
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&view=checksshop&table=Checks&qd=checkshop&id=' + id + '&setstatus=' + CheckStatusEnum.Sleep + appendTime();
        showPopup(_url, showcallback, getButtons(), null, null, true);
    }
    function showExpend(id) {
        saveTrSelected(id);
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&view=checksshop&table=Checks&qd=checkshop&id=' + id + '&setstatus=' + CheckStatusEnum.Expend + appendTime();
        showPopup(_url, showcallback, getButtons(), null, null, true);
    }
    function showVisitor(id) {
        saveTrSelected(id);
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&view=checksshop&table=Checks&qd=checkshop&id=' + id + '&setstatus=' + CheckStatusEnum.Visitor + appendTime();
        showPopup(_url, showcallback, getButtons(), null, null, true);
    }
    function showRefuseSleep(id, table, qd, multi) {
        if (!multi) {
            saveTrSelected(id);
            updateCheckStatus(id, CheckStatusEnum.New, table, qd);
        } else {
            Growl.Notice(Lang.MultiChecksShopWating, 4000);
            updateCheckStatus('', CheckStatusEnum.New, table, qd, id);
        }
    }
    function showRefuseVisitor(id, table, qd, multi) {
        if (!multi) {
            saveTrSelected(id);
            updateCheckStatus(id, CheckStatusEnum.New, table, qd);
        } else {
            Growl.Notice(Lang.MultiChecksShopWating, 4000);
            updateCheckStatus('', CheckStatusEnum.New, table, qd, id);
        }
    }
    function showSleepReceive(id, table, qd, multi) {
        if (!multi) {
            saveTrSelected(id);
            updateCheckStatus(id, CheckStatusEnum.SleepReceive, table, qd);
        } else {
            Growl.Notice(Lang.MultiChecksShopWating, 4000);
            updateCheckStatus('', CheckStatusEnum.SleepReceive, table, qd, id);
        }
    }
    function showRefuseExpend(id, table, qd, multi) {
        if (!multi) {
            saveTrSelected(id);
            updateCheckStatus(id, CheckStatusEnum.New, table, qd);
        } else {
            Growl.Notice(Lang.MultiChecksShopWating, 4000);
            updateCheckStatus('', CheckStatusEnum.New, table, qd, id);
        }

    }

    function showMultiSleep(ids, table, qd) {
        var _url = 'get.aspx?op=GetHtml&table=Checks&view=multichecksshop&ShowDiv=1' + appendTime();
        var rBankTreePath;
        var btns = {};
            btns[Lang.OK] = "OK";
            btns[Lang.Cancel] = "_CANCEL_";

            showPopup(_url, function (v, m, f) {
                if (v == "OK") {
                    var valid = validateInputs('#subDetailForm', f);
                    if (!valid)
                        return 0;
                    rBankTreePath = f.ReceiverBankTreePath;
                    Growl.Notice(Lang.MultiChecksShopWating, 4000);
                        updateCheckStatus('', CheckStatusEnum.Sleep, table, qd, ids, rBankTreePath);
                }
            }, btns);
    }

    function showMultiReceive(ids, table, qd) {
        var _url = 'get.aspx?op=GetHtml&table=Checks&view=multichecksshop&ShowDiv=4' + appendTime();
        var oweTreePath;
        var btns = {};
        btns[Lang.OK] = "OK";
        btns[Lang.Cancel] = "_CANCEL_";

        showPopup(_url, function (v, m, f) {
            if (v == "OK") {
                var valid = validateInputs('#subDetailForm', f);
                if (!valid)
                    return 0;
                oweTreePath = f.OweTreePath;
                Growl.Notice(Lang.MultiChecksShopWating, 4000);
                updateCheckStatus('', CheckStatusEnum.Received, table, qd, ids, '', null, null, oweTreePath);
            }
        }, btns);
    }

    function showMultiExpand(ids, table, qd) {
        var _url = 'get.aspx?op=GetHtml&table=Checks&view=multichecksshop&ShowDiv=2' + appendTime();
        var rPersonId;
        var btns = {};
        btns[Lang.OK] = "OK";
        btns[Lang.Cancel] = "_CANCEL_";

        showPopup(_url, function (v, m, f) {
            if (v == "OK") {
                var valid = validateInputs('#subDetailForm', f);
                if (!valid)
                    return 0;
                rPersonId = f.ReceiverPersonID;
                Growl.Notice(Lang.MultiChecksShopWating, 4000);
                updateCheckStatus('', CheckStatusEnum.Expend, table, qd, ids, '', rPersonId);
            }
        }, btns);
    }

    function showMultiVisitor(ids, table, qd) {
        var _url = 'get.aspx?op=GetHtml&table=Checks&view=multichecksshop&ShowDiv=3' + appendTime();
        var visitorId;
        var btns = {};
        btns[Lang.OK] = "OK";
        btns[Lang.Cancel] = "_CANCEL_";

        showPopup(_url, function (v, m, f) {
            if (v == "OK") {
                var valid = validateInputs('#subDetailForm', f);
                if (!valid)
                    return 0;
                visitorId = f.VisitorID;
                Growl.Notice(Lang.MultiChecksShopWating, 4000);
                updateCheckStatus('', CheckStatusEnum.Visitor, table, qd, ids, '', '', visitorId);
            }
        }, btns);
    }

    function updateCheckStatus(checkId, newStatus, table, qd, mIds, rBTreePath, rPId, vId, oweTreePath) {
        var _url = 'set.aspx?op=RunScalar&table=' + table + '&qd=' + qd + appendTime();
        var form_data = {
            id: checkId,
            status: newStatus,
            multiids: mIds,
            receiverbanktreepath: rBTreePath,
            receiverpersonid: rPId,
            visitorid: vId,
            oweTreePath: oweTreePath
        };

        $.ajax({
            url: _url,
            data: form_data,
            success: function () {
                Growl.Notice(Lang.Success, 3000);
                $("#flexi").flexReload();
            },
            error: function (xhr, textStatus) {
                if (textStatus === 'timeout') {
                    Growl.Error(Lang.MultiChecksShopTimeOut, 5000);
                }
            },
        });
    }
    function saveTrSelected(id) {
        $('.trSelected').removeClass('trSelected');
        $('#row' + id).addClass('trSelected');
        saveSelectedRows('#flexi');
        $('.trSelected').removeClass('trSelected');
    }

    // ------------------------------
    // startup code
    // ------------------------------
    var urlId = $.urlParam('id');
    dateSelect('.dateSelect');
    checkAccess(['bank,checksshops']);
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.Checks, parentTitle: Lang.MAccounting, parentUrl: 'menu/accounting.aspx' } }));
    setClassBank();
    run();
  }
        