﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
94-05-15: file created
*/
function view_field() {
    // ------------------------------
    // startup code
    // ------------------------------
    setValue("#FieldType");
    setValue("#Used");
    setValue("#FieldShowType");
    setChecked("#Printable");
    insertValidation();
    if ($("#DecimalPlaces").val() == "") {
        $("#DecimalPlaces").val(0);
    }

    $("#Formula").dblclick(function () {
        show();
    });

    function showcallback(v, m, f) {
        if (v == 'OK') {
            $("#Formula").val(f.FormulaText);
        }
        return true;
    }

    function show() {
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&view=formula&FormulaText=' + $("#Formula").val().split('+').join('|') + appendTime();
        var btns = getButtons();
        showPopup(_url, showcallback, btns);
    }

    makeChosen();
}