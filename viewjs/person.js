﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
92-10-16: file created
*/
function view_person() {
    var cnt = 0, cnt1 = 0;
    var mID = -1;
    $("#tableGroup").smartTable();
    function addGroup(node) {
        var row = '<tr>';
        row += '<td class="width-40 align-center">' + (cnt + 1) + '</td><td><input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" /><input type="text" id="g_' + cnt + '" name="GroupingName" value="' + getNodeVal(node, "GroupingName") + '" class="groupingname form-control input-sm full-width" /><input type="hidden" name="GroupingID" value="' + getNodeVal(node, "GroupingID") + '" /></td>';
        row += '<td class="width-20 align-center"><img src="img/delete.gif" id="r_' + cnt + '" class="removeGroup" /></td>';
        row += '</tr>';
        cnt++;
        return row;
    }
    function addAssessmentField(node) {
        var type = toInt(getNodeVal(node, "Type", -1), -1);
        var row = '<tr>';
        row += '<td class="width-20 align-center">' + (cnt1 + 1) + '</td><td>' +
            '<input type="hidden" name="ID2" value="' + getNodeVal(node, "ID", -1) + '" />' +
            '<input type="text" readonly="readonly" id="afn_' + cnt1 + '" name="AssessmentFieldName" value="' + getNodeVal(node, "AssessmentFieldName") + '" class="assessmentfieldname form-control input-sm full-width" />' +
            '<input type="hidden" name="AssessmentFieldID" value="' + getNodeVal(node, "AssessmentFieldID") + '" /></td>';

        row += '<td class="width-60 align-center"><input type="text" readonly="readonly" id="wv_' + cnt1 + '" name="WeightedValue" value="' + getNodeVal(node, "WeightedValue") + '" class="weightedvalue form-control input-sm full-width" /></td>';

        if (type == 1) {
            row += '<td class="width-100 align-center"><input type="text" id="v_' + cnt1 + '" name="Value" value="' + numberWithCommas(getNodeVal(node, "Value")) + '" class="value form-control input-sm full-width" /></td>';
        } else if (type == 2) {
            var uptions;
            var chosenvalue = getNodeVal(node, "ChosenValue");
            if (chosenvalue) {
                uptions = chosenvalue.split('،');
                row += '<td>' +
                    '<select id="v_' + cnt1 + '"  class="value" name="Value" v="' + toInt(getNodeVal(node, "Value"), 0) + '" >'
                   +'<option value="0"></option>';
                for (var i = 1; i <= uptions.length; i++) {
                    row += '<option value="' + i + '">' + uptions[i - 1] + '</option>';
                }
                row += '</select>';
                row += '</td>';
            } else {
                row += '<td class="width-100 align-center"><input type="text" id="v_' + cnt1 + '" name="Value" value="' + numberWithCommas(getNodeVal(node, "Value")) + '" class="value form-control input-sm full-width" /></td>';
            }
        } else {
            row += '<td class="width-100 align-center"><input type="text" readonly="readonly" id="v_' +
                cnt1 +
                '" name="Value" value="' +
                numberWithCommas(getNodeVal(node, "Value")) +
                '" class="value form-control input-sm full-width" /></td>';
        }

        //row += '<td class="width-20 align-center"><img src="img/delete.gif" id="ra_' + cnt1 + '" class="removeAssessment" /></td>';
        row += '</tr>';
        cnt1++;
        return row;
    }
    function RemoveGroup($who) {
        var id = $who.attr('id'); // r_1
        id = id.substring(2); // r_1  -->  1
          //alert(id);
        $('#g_' + id).val(""); // 
        $('#g_' + id).next().val(-1); // 
        $who.parent().parent().hide();
        Growl.Notice(Lang.DeletedSuccessfully);
    }
    function autoPostPack(event, data, formatted) {
        $(event.target).next().val(data[0]); // ID
    }
    function bindAdds() {
        $('.addGroup').click(function () {
            var table = $(this).closest('table');
            var tableID = table.attr("id");
            $('tr:last', table).after(addGroup(null));
            var lastRow = $('tr:last', table);
            lastRow.find(".removeGroup").click(function() {
                RemoveGroup($(this));
            });
            Growl.Notice(Lang.RowAddedSuccessfully);
            autoComplete('Grouping', 'grouppersonauto', "#tableGroup tr:last .groupingname", "#justfurfun"
               , autoPostPack
               , function (data, i, n, value) {
                   return data[1];
               });
        });
    }
    function bindRemoves() {
        //-----------
        $('.removeGroup').click(function () {
            RemoveGroup($(this));
        });
    }

    function checkAditionalInformation()
    {
        if ($('#AdditionalInformation').attr('checked')) {
            $('#AI').show();
        }
        else {
            $('#AI').hide();
        }
        $('#AdditionalInformation').click(function () {
            if ($(this).attr('checked')) {
                $('#AI').show();
            }
            else {
                $('#AI').hide();
            }
        });
    }

    function handleHuman() {
        if ($('#IsCompany').attr('checked')) {
            $('.surname').hide();
            $('#Surname').val('');
            $('.namelbl').html(Lang.Company);
        }
        else {
            $('.surname').show();
            $('.namelbl').html(Lang.Name);
        }
    }
    // ------------------------------
    // startup code
    // ------------------------------
    mID = toInt($('#MasterID').val(), -1);
    if (mID == -1)
        $('#Enabled').attr("checked", 'checked');
    else
        setChecked("#Enabled");
    setChecked("#AdditionalInformation");
    checkAditionalInformation();
    setValue('#Nature');
    setValue('.Value');
    // binds form submission and fields to the validation engine
    //$('.popupTitle').html(Lang.Person);

    if (hasValuePositive(mID)) {
        fillTable("get.aspx?op=GetXml&qd=personnelgrouping&PersonnelID=" + mID, '#tableGroup', addGroup);
        fillTable("get.aspx?op=GetXml&qd=assessmentfieldvalue&PersonnelID=" + mID, '#tableAssessment', addAssessmentField);
        setValue('.value');
    }
    else
        setDefaultByTitle('PersonnelGroup', 'personnelgroupauto', '#PersonnelGroupName', '#PersonnelGroupID', null);

    function autoPostPack(event, data, formatted) {
        $(event.target).next().val(data[0]); // ID
    }
    autoComplete('Grouping', 'grouppersonauto', "#tableGroup .groupingname", "#GroupingID"
               , autoPostPack
               , function (data, i, n, value) {
                   return data[1];
               });
    autoCompleteNew({
        table: 'Personnel', qd: 'personnameauto', textInput: '.personName'
        , postback: function (event, data) {
            $("#Surname").unautocomplete();
            autoCompleteNew({
                table: 'Personnel', qd: 'personsurnameauto', textInput: "#Surname"
                , allowNew: true
                , autourl: 'get.aspx?op=Auto&table=Personnel&qd=personsurnameauto&Name=' + encodeURIComponent(data[1])
            });
        }
        , allowNew: true
    });
    //autoCompleteFree('Personnel', 'personnameauto', '.personName', null
    //    , function (event, data) {
    //        var _autourl1 = 'get.aspx?op=Auto&table=Personnel&qd=personsurnameauto&Name=' + encodeURIComponent(data[1]);
    //        $("#Surname").unautocomplete();
    //        autoComplete('Personnel', 'personsurnameauto', "#Surname", "#IyXomeUNaydemBeTo", null, null, null, null, _autourl1);
    //    });
    autoComplete('PersonnelGroup', 'personnelgroupauto', '#PersonnelGroupName', '#PersonnelGroupID');
    autoComplete('CooperationType', 'enumauto1', '#CooperationTypeName', '#CooperationTypeID');

    bindAdds();
    bindRemoves();
    tabify();
    insertValidation("#subDetailForm");
    _Context.priceDate = _Context.today;
    monetaryInputsSet(null, null);
    commaInputs('#subDetailForm');
    if (_Context.isAccountingVer)
        $('.noacc').hide();

    $("#Surname").focus();
    $("#Name").focus();
    $('#IsCompany,#IsHuman').click(handleHuman);
    if (mID > 0 && ($('#Surname').val() == '' || $('#Surname').val() == '.')) {
        $('#IsCompany').attr("checked", 'checked');
    }
    handleHuman();
}