﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-11-05: file created
*/
function view_stores() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var terminology = getTerminology(_Context.table);
        if (terminology)
            $('#term').html("<a href='help/terminology.aspx' target=_blank title='" + Lang.HasTerminology + "'>" + terminology + "</a>");
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell'},
                    { display: Lang.headline, name: 'Title', sortable: true},
                    { display: Lang.Code, name: 'Code', sortable: true, css_class: 'width-80 align-center'},
                    //{ display: Lang.StoreNature, name: 'StoreCategoryName', sortable: true},
                    //{ display: Lang.OriginStore, name: 'MainStoreName', sortable: true, css_class: 'width-100 align-center'},
                    { display: Lang.StartTime, name: 'StartTimeActivityDaily', hide: true, sortable: true, css_class: 'width-80 align-center' },
                    { display: Lang.EndTime, name: 'EndTimeActivityDaily', hide: true, sortable: true, css_class: 'width-80 align-center' },
                    { display: Lang.NumServices, name: 'NumServices', sortable: true, css_class: 'width-80 align-center' },
                    { display: Lang.Default, name: 'IsDefault', sortable: true, process: showBool, css_class: 'width-60 align-center' },
                    { display: Lang.Enabled, name: 'Enabled', sortable: true, process: showBool, css_class: 'width-40 align-center' },
                    { display: Lang.Edit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell'},
                    { display: Lang.Commodity, name: 'StoreService', sortable: false, process: StoreServiceDefination, css_class: 'width-50 align-center' },
                    { display: Lang.Order, name: 'StoreLayout', sortable: false, process: StoreLayoutDefination, css_class: 'width-50 align-center', hide: true }
                ],
                buttons:
                [
                    { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                    { name: Lang.GBDel, bclass: 'delete',onpress: doCommand },
                    { separator: true },
                    { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                searchitems: null,
                sortname: "Title",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight, resizable: gridResizable,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                }
            }
            );
    }
    function StoreServiceDefination(celDiv, id) {
        $(celDiv).html('<a href="list.aspx?table=StoreServiceDef&id=' + id + '"><img src="img/task_icon.jpg" /></a>');
    }
    function StoreLayoutDefination(celDiv, id) {
        $(celDiv).html('<a href="list.aspx?table=StoreLayout&view=storelayouts&id=' + id + '"><img src="img/task_icon.jpg" /></a>');
    }
    function showcallback(v, m, f) {
        $('.Starttime').attr('name', 'Starttime');
        $('.EndTime').attr('name', 'EndTime');
        $('.FirstRoundDate').attr('name', 'FirstRoundDate');
        return newShowcallback(v, m, f, 'storesupdate', 'storesinsert', 'Stores', null, show);
        // return generalShowcallback(v, m, f, 'storesupdate', 'storesinsert', _Context.table);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['stores,store']);
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.Store, parentTitle: Lang.SellAndBuy, parentUrl: 'menu/sales.aspx' } }));
    setClassStore();
    run();
}