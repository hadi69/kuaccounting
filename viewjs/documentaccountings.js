﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
92-11-28
    - colorGrid
92-10-16: 
    - file created
    - editCallback: Cancel --> return true for id=-1
    - doEdit: getDefaultButtons --> getButtons
*/
function view_documentaccountings() {
    var startupID = -1;
    var curSimpleVoucherID = -1;
    var _voucherWorkflow = -1; // -2 = not read from server, 0 = disabled, 1 = true
    function isVoucherWorkflow()
    {
        if (_voucherWorkflow < 0) {
            _voucherWorkflow = getSettings("VoucherWorkflow");
            if (_voucherWorkflow != 1)
                _voucherWorkflow = 0;
        }
        return _voucherWorkflow;
    }
    function doCommand(com, grid) {
        var isNew = com == Lang.GBNew;
        if (!isNew) {
            var btn = $(".fbutton span:contains(" + com + ")");
            if (btn.hasClass('new'))
                isNew = true;
        }
        if (isNew && isVoucherWorkflow() == 1) {
            var more = '&Type=' + WorkflowTypeEnum.Voucher;
            var count = getServerValue('checkworkflow', 'Workflow', more);
            if (count == 0 || count == null) {
                displayError(Lang.WorkflowM1);
                return;
            }
        }
        
        if (isNew || com == Lang.GBDel || com == Lang.GBReorder) {
            if (_Context.fyStart == '') {
                alert(Lang.NoCurYear);
                return;
            }
        }
        if (com == Lang.GBNewAuto) {
            _Context.id = -1;
            var btns = getDefaultButtons();
            btns[Lang.ReceiptPrint] = 'Print';
            btns[Lang.Docs] = 'Documents';
            curSimpleVoucherID = Math.floor((Math.random() * 1000000000) + 1);
            var op = "GetHtml";
            var _url = 'get.aspx?op=' + op + '&table=' + _Context.table + '&qd=' + _Context.query + '&view=documentaccountingsimple' + appendTime();
            saveSelectedRows("#flexi");
            showPopup(_url, autoEditCallback, btns, '1%', null, true);
            return;
        }

        if (isNew && com != Lang.GBNew) {
            var desiredEntryTaskID = $(".fbutton span:contains(" + com + ")").attr('class').substring('new '.length);
            show(-1, null, null, desiredEntryTaskID);
        } else {
            generalCommand(com, grid, show, _Context.title, 'ValidVoucher,OtherID'
            , 'Mode=0^' + Lang.DAModeNormal + '^3^' + Lang.DAModeCheck + '^14^' + Lang.DAModeCheckOwe + '^4^' + Lang.DAModePurchase + '^5^' + Lang.DAModeRefuse + '^6^' + Lang.DAModeReturn + '^7^' + Lang.DAModeStoreDraft + '^8^' + Lang.DAModePayCheck + '^15^' + Lang.DAModePayCheckOwe + '^9^' + Lang.DAModePurchaseInvoice + '^10^' + Lang.DAModeRefuseInvoice + '^11^' + Lang.DAModeClosing + '^12^' + Lang.DAModeFinal + '^13^' + Lang.DAModeOpening + '^19^' + Lang.DAModeFish
            + '&Status=0^' + Lang.DAStatusClosed + '^1^' + Lang.DAStatusLocked + '^2^' + Lang.DAStatusDetermined);
        }
        
    }

    // similar to getTaskGridButtons in site.js
    function getGridButtons(type, doCommand, subsystem, form) {
        var btns = [];
        var numTasks = 0, first = true;
        if (isVoucherWorkflow() == 1) {
            var items = getItems("getuserstarttasks", "Type=" + type + '&UserID=' + _Context.userID);
            if (items) {
                $(items).find("R").each(function () {
                    if (first) // bypass first
                    {
                        //first = false;
                        //return;
                    }
                    var node = $(this);
                    var id = node.attr("ID");
                    var title = node.attr("Title");
                    var btnTitle = node.attr("BtnTitle");
                    if (!btnTitle)
                        btnTitle = title;
                    var showIt = true; //getServerValue('hasaccesstoworkflowstarterbutton', 'Task', '&TaskID=' + id + '&UserID=' + _Context.userID + '&Type=' + type);
                    if (showIt) {
                        btns[numTasks] = { name: btnTitle, bclass: 'new ' + id, onpress: doCommand, onaccess: hasAccess(subsystem, form, "new") && hasGeneralAccess("new") };
                        numTasks++;
                    }
                });
            }
        }

        btns[btns.length] = { name: Lang.GBNewAuto, bclass: 'newauto', onpress: doCommand, onaccess: hasAccess("accounting", "voucher", "new") };
        btns[btns.length] = { name: Lang.GBPrint, bclass: 'print', onpress: doCommand };
        btns[btns.length] = { name: Lang.GBDel, bclass: 'delete', onpress: doCommand, onaccess: hasAccess("accounting", "voucher", "delete") };
        btns[btns.length] = { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand, onaccess: hasAccess("accounting", "voucher", "excel") && hasGeneralAccess("excel") };
        return btns;
    }
    function run() {
        var query = isVoucherWorkflow() ? "documentaccountingworkflow" : "documentaccounting";
        var _url = 'service.asmx/GetList?table=DocumentAccounting&qd=' + query + "&more=__PrOgrAmconD__" + appendTime();
        //alert(_url);
        $("#flexi").flexigrid
                (
                {
                    url: _url,
                    dataType: 'xml',
                    colModel:
                    [
                        { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'center', hide: true },
                        { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                        { display: Lang.No, name: 'No', sortable: true, sortname: "No_cast2int", css_class: 'width-70 align-center' },
                        { display: Lang.Comments, name: 'DoComments', sortable: true},
                        { display: Lang.Date, name: 'Date', sortable: true, process: capturePriceDate, css_class: 'date-cell' },
                        { display: Lang.TotalOwe, name: 'TotalOwe', sortable: true, process: showMonetary, css_class: 'width-80 align-center', footer: 'sum' },
                        { display: Lang.TotalDue, name: 'TotalDue', sortable: true, process: showMonetary, css_class: 'width-80 align-center', footer: 'sum' },
                        { display: Lang.Status, name: 'Status', sortable: true, process: displayStatus, css_class: 'width-80 align-center', hide: true },
                        { display: Lang.OtherID, name: 'OtherID', sortable: true, hide: true },
                        { display: Lang.Type, name: 'Mode', sortable: true, process: displayMode, css_class: 'width-80 align-center' },
                        { display: Lang.User, name: 'UserName', sortable: true, hide: true },
                        { display: Lang.Fixer, name: 'FixerName', sortable: true, css_class: 'width-100 align-center', hide: true },
                        { display: Lang.Locker, name: 'LockerName', sortable: true, css_class: 'width-100 align-center', hide: true },
                        { display: Lang.Locker, name: 'ValidVoucher', sortable: false, process: displayValid, hide: true },
                        { display: Lang.Factor, name: 'PurchaseID', sortable: false, css_class: 'puid', hide: true },
                        { display: Lang.Factors, name: 'PurchaseNo', sortable: false, css_class: 'puno width-80' },
                        { display: '', name: 'StoreDraftID', sortable: false, css_class: 'stid', hide: true },
                        { display: '', name: 'StoreDraftNo', sortable: false, css_class: 'stno', hide: true },
                        { display: '', name: 'ReturnID', sortable: false, css_class: 'rtid', hide: true },
                        { display: '', name: 'ReturnNo', sortable: false, css_class: 'rtno', hide: true },
                        { display: '', name: 'RefuseID', sortable: false, css_class: 'rfid', hide: true },
                        { display: Lang.PreStatus, name: 'PreTaskName', sortable: true, css_class: 'task-cell', hide: isVoucherWorkflow() == 0 },
                        { display: Lang.Status, name: 'TaskName', sortable: true, css_class: 'task-cell', hide: isVoucherWorkflow() == 0 },
                        { display: '', name: 'RefuseNo', sortable: false, css_class: 'rfno', hide: true },
                        { display: '', name: 'WorkflowColor', css_class: 'WorkflowColor', hide: true },
                        { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, hide: hideColumn("accounting", "voucher", "edit"), css_class: 'action-cell' },
                        { display: Lang.ColView, name: 'Show', width: 40, sortable: false, align: 'center', process: showButton, hide: true },
                        { display: Lang.ColView, name: 'View', width: 40, sortable: false, align: 'center', process: showViewLink, hide: true }

                    ],
                    buttons: getGridButtons(WorkflowTypeEnum.Voucher, doCommand, "accounting", "voucher"),
                    searchitems: null,
                    sortname: "No_cast2int",
                    sortorder: "desc",
                    usepager: true,
                    rpOptions: [20, 50, 1000, 1500, 3000], //allowed per-page values
                    rp: 1500,
                    useRp: true,
                    rp: gridRp,
                    showTableToggleBtn: gridToggle,
                    width: gridWidth,
                    height: gridHeight, resizable: gridResizable,
                    singleSelect: true,
                    onDoubleClick: function (row, grid, options) {
                        var id = row.id.substring(3);
                        show(id);
                    },
                    onSuccess: function (grid) {
                        restoreSelectedRows('#flexi');
                        postProcessGrid();
                        if (startupID != -1) {
                            show(startupID);
                            startupID = -1;
                        }                        
                    }
                }
                );

        if (_Context.level == 1)
            $('.gridMode').hide();
    }
    
    function displayMode(celDiv, id, text) {
        var _mode = parseInt(text, 10);
        displayDAMode(celDiv, _mode);
    }
    function displayStatus(celDiv, id, text) {
        var _mode = parseInt(text, 10);
        displayDAStatus(celDiv, _mode);
    }
    function displayValid(celDiv, id) {
        //if (celDiv.innerText == 'True')
        $(celDiv).addClass('_valid');
    }
    function colorGrid() {
        $('#flexi tr').each(function () {
            var _valid = $('._valid', this).html();
            if (_valid == 'False') {
                $('td', this).addClass('invalid');
            }
            //unbalance
            var owe = toFloat(getColContent(this, 6));
            var due = toFloat(getColContent(this, 7));
            if (owe != due) {
                $('td:nth-child(6)', this).addClass('unbalance');
                $('td:nth-child(7)', this).addClass('unbalance');
            }
        });
        //ValidVoucher
    }
    function showFactorLinks() {
        $('#flexi tr').each(function () {
            var _id = $('.puid div', this).html();
            if (_id && _id.trim().length > 0) {
                var _no = $('.puno div', this).html();
                $('.puno div', this).html('<a href="list.aspx?table=Purchase&view=shoppurchases&id=' + _id + '">' + _no + '</a>');
                return;
            }
            _id = $('.stid div', this).html();
            if (_id && _id.trim().length > 0) {
                var _no = $('.stno div', this).html();
                $('.puno div', this).html('<a href="list.aspx?table=StoreDraft&view=shopstoredrafts&id=' + _id + '">' + _no + '</a>');
                return;
            }
            _id = $('.rtid div', this).html();
            if (_id && _id.trim().length > 0) {
                var _no = $('.rtno div', this).html();
                $('.puno div', this).html('<a href="list.aspx?table=Returns&view=shopreturns&id=' + _id + '">' + _no + '</a>');
                return;
            }
            _id = $('.rfid div', this).html();
            if (_id && _id.trim().length > 0) {
                var _no = $('.rfno div', this).html();
                $('.puno div', this).html('<a href="list.aspx?table=Refuse&view=shoprefuses&id=' + _id + '">' + _no + '</a>');
                return;
            }
        });
    }
    function postProcessGrid() {
        setTimeout(function () {
            showFactorLinks();
            colorGrid();
            colorTasks();
        }, 500);
    }
    function showEditLink(celDiv, id) {
        $(celDiv).html('<a href="Vouchers.aspx?id=' + id + '"><img src="img/edit.gif" /></a>'); //reports/accounting/Voucher.aspx
    }
    function showViewLink(celDiv, id) {
        $(celDiv).html('<a href="reports/accounting/Voucher.aspx?id=' + id + '"><img src="img/edit.gif" /></a>'); //
    }
    function showButton(celDiv, id) {
        $(celDiv).html('<img src="img/edit.gif" />');
        $(celDiv).click(function (event) {
            event.preventDefault();
            show();
        })
    }
    function isGridMode() {
        return true;
    }
    function autoEditCallback(v, m, f) {
        if (v == 'OK') {

            if (!checkDateIntoFinanceYear(f))
                return false;

            if (_Context.fyStart == '') {
                alert(Lang.NoCurYear);
                return false;
            }
            if (!checkProgramStatus())
                return false;
            var valid = validateInputs(null, f);
            if (!valid)
                return false;
            if (f.No.indexOf(',') >= 0) {
                alert(findTitleById("No", '#detailForm') + ' : ' + Lang.MustBeAnInteger );
                return false;
            }
            if (toFloat(f.TotalBalance, 0) != 0)
                alert(Lang.Unbalanced);
            $('#GeneralID', '#detailForm').val(curSimpleVoucherID);
            _Context.priceDate = f.Date;
            var id = generalOKScalar(v, m, f, 'documentaccountingautoinsert', 'documentaccountingautoinsert', 'DocumentAccounting,DocumentAccountingItems');
            id = parseInt(id, 10);
            if (id <= 0)
                return false;
            if (_Context.id == -1) {
                _Context.id = id;
                $('#SID', '#detailForm').val(_Context.id);
                $('#No', '#detailForm').val(getFieldValue('DocumentAccounting', 'No', 'ID=' + _Context.id));
            }
            var title = $('.popupTitle').html();
            //$('.popupTitle').html(Lang.Saved);
            $('.popupTitle').html(Lang.Saved).css('background-color', '#5cb85c');
            //$('.popupTitle').animate({ 'background-color': '#f00' }, 1000, 'linear', function () { $('.popupTitle').animate({ 'background-color': '#00496F' }, 1000, 'linear'); $('.popupTitle').html(title); });
            replacePrintBtn();
            return false;
        }
        if (v == 'New') {
            var type = $('#TypeSel').val();
            var sType = $('#SType').val();
            var btns = getDefaultButtons();
            btns[Lang.ReceiptPrint] = 'Print';
            btns[Lang.Docs] = 'Documents';
            _Context.id = -1;
            curSimpleVoucherID = Math.floor((Math.random() * 1000000000) + 1);
            var op = "GetHtml";
            var _url = 'get.aspx?op=' + op + '&table=' + _Context.table + '&qd=' + _Context.query + '&view=documentaccountingsimple&Type=' + type + '&SType=' + sType + appendTime();
            saveSelectedRows("#flexi");
            showPopup(_url, autoEditCallback, btns, '1%', null, true);
        }
        else if (v == 'Documents') {
            if (_Context.id > 0) {
                var _url = 'list.aspx?table=DocumentAccountingDocument&daid=' + _Context.id;
                popup(_url);
            }
            return false;
        }
        else if (v == 'Print') {
            Growl.Notice(Lang.ReceiptPrintMsg, 2000);
            return false;
        }
        return true;
    }

    function replacePrintBtn() {
    var typeSelected = $('#TypeSel').val();
    if (typeSelected == 4 || typeSelected == 5 || typeSelected == 6) {
        var docNo = $('#No').val();
        var amount = toFloat($('#Price').val(), 0);
        var discount = toFloat($('#Discount').val(), 0);
        var totalAmount = toFloat($('#TotalDue').val(), 0);
        var person = null;
        var personId = -1;
        var checkName = null;
        var topicName = null;

        if (typeSelected == 5) { // Fund2Person
            person = $('#ToTopic').val().toString().trim();
            personId = $('#ToTopicID').val();
            if ($('#PayCheckName').val())
                checkName = $('#PayCheckName').val().toString().trim();
        }
        else if (typeSelected == 4) { // Person2Fund
            person = $('#FromTopic').val().toString().trim();
            personId = $('#FromTopicID').val();
            if ($('#CheckName').val())
            checkName = $('#CheckName').val().toString().trim();
        }
        else if (typeSelected == 6) { //Fund2Topic
            topicName = $('#ToTopic').val().toString().trim();
            if ($('#PersonnelID').val()) {
                personId = $('#PersonnelID').val();
                person = $('#PersonnelName').val().toString().trim();
            } else {
                person = "-";
            }
        }
        var checkAmount = totalAmount - amount + discount;
        replacePrintButton({
            primary: {
                text: Lang.ReceiptPrint,
                href: Router.Generate("print-docreceipt", { params: {
                    No: docNo, Amount: totalAmount - checkAmount, Discount: discount,
                    CheckAmount: checkAmount, Type: typeSelected, Person: person
                    , CheckSerial: checkName, PersonId: personId, TopicName: topicName
                }, query: { print: "true" } })
            }
        });
    }
}
    function editCallback(v, m, f) {
        if (v == 'TaskImpls') {
            showTaskImpls(_Context.id, WorkflowTypeEnum.Voucher);
            return false;
        }
       if (v == 'ChangeLog') {
            showLogChange('DocumentAccounting', _Context.id, WorkflowTypeEnum.Voucher);
            return false;
        }
        if (v == 'ToVoucherDB') {
            var programID = $(this).val();
            var _url = 'set.aspx?op=tovoucherdb&id=' + _Context.id + appendTime();
            //alert(_url);
            $.ajax({
                url: _url,
                type: 'GET',
                success: function (html) {
                    if (html.indexOf('Error:') != 0 && html.indexOf('خطا:') != 0) {
                        message(html);
                    }
                    else {
                        displayError(html);
                    }
                },
                error: ajaxError
            });
            return true;
        }
        if (v == 'Another') {
            doEdit(_Context.id);
            return true;
        }
        if (v == 'ShowBtns' || v == undefined) {
            return false;
        }
        else if (v == 'Cancel') {
            // unlock
            if (_Context.id > 0) {
                if (runQuery("documentaccountingunlock", "DocumentAccounting", "&ID=" + _Context.id)) {
                    $("#flexi").flexReload();
                    return true;
                }
                return false;
            }
            return true;
        }
        else if (v == 'Determine') {
            // unlock
            if (_Context.fyStart == '') {
                alert(Lang.NoCurYear);
                return false;
            }
            if (_Context.id > 0)
                if (runQuery("documentaccountingdetermine", "DocumentAccounting", "&ID=" + _Context.id + "&UserID=" + _Context.userID)) {
                    $("#flexi").flexReload();
                    return true;
                }
            return false;
        }
        else if (v == 'Documents') {
            if (_Context.id > 0) {
                var _url = 'list.aspx?table=DocumentAccountingDocument&daid=' + _Context.id;
                popup(_url);
            }
            return false;
        }
        //else if (v == 'Print') {
        //    //var no = $('#No').val(),
        //    //    url = Router.Generate("print-document-accounting", { params: { from: no, to: no }, query: { print: "true" } });
        //    //window.open(url);

        //   // return false;
        //}
        else if (v == 'Reorder') {
            if (_Context.id > 0) {
                if (runQuery("documentaccountingorder", "DocumentAccounting", "&ID=" + _Context.id)) {
                    doEdit(_Context.id);
                    return true;
                }
            }
            return false;
        }
        else if (v == 'ReorderID') {
            if (_Context.id > 0) {
                if (runQuery("documentaccountingorderid", "DocumentAccounting", "&ID=" + _Context.id)) {
                    doEdit(_Context.id);
                    return true;
                }
            }
            return false;
        }
        else if (v == 'Copy') {
            if (_Context.id > 0) {
                var msg = '<div class="container"><div class="row margin-20-top"><div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container"><div class="form-group"><label for="RComments" class="left-aligned-label control-label col-xs-12 col-sm-4">' + Lang.RComments + '</label><div class="col-xs-12 col-sm-8"><input type="text" id="RComments" name="RComments" class="form-control input-sm" /></div></div></div></div></div>';
                var btns = { کپی: 'ok' };
                btns[Lang.Cancel] = 'Cancel';
                $.prompt(msg, {
                    submit: function (v, m, f) {
                        if (v == 'ok') {
                            var rComments;
                            if ($('#RComments').val() == '') {
                                rComments = 'NULL';
                            } else {
                                rComments = "N'" + $('#RComments').val() + "'";
                            }
                            var copyID = getServerValue("duplicatevoucher", "DocumentAccounting", "&ID=" + _Context.id + "&RComments=" + rComments);
                            if (toInt(copyID, 0) > 0) {
                                doEdit(copyID);
                                $("#flexi").flexReload();
                            }
                        }
                        else
                            doEdit(_Context.id);
                    },
                    buttons: btns,
                    focus: 0
                });
            }
            return true;
        }
        else if (v == 'CopyFrom') {
            if (_Context.id > 0) {
                var msg = '<div class="container">'
                    + '<div class="row margin-20-top">'
                    + '<div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">'
                    + '<div class="form-group">'
                    + '<label for="CopyFromNo" class="left-aligned-label control-label col-xs-12 col-sm-4">'
                    + Lang.CopyFromComments
                    + '</label>'
                    + '<div class="col-xs-12 col-sm-8">'
                    + '<input type="text" id="CopyFromNo" name="CopyFromNo" class="form-control input-sm" />'
                    + '</div>'
                    + '</div>'
                    + '</div>'
                    + '</div>'
                    + '</div>';
                var btns = { کپی: 'ok' };
                btns[Lang.Cancel] = 'Cancel';
                $.prompt(msg, {
                    submit: function (v, m, f) {
                        if (v == 'ok') {
                            if (f.CopyFromNo == '')
                                return false;
                            var done = getServerValue("copyfromvoucher", "DocumentAccounting", "&ID=" + _Context.id + "&CopyFromNo=" + f.CopyFromNo);
                            if (toInt(done, 0) > 0)
                                message(Lang.Success);
                            else
                                message(Lang.Failed);
                        }
                        doEdit(_Context.id);
                    },
                    buttons: btns,
                    focus: 0
                });
            }
            return true;
        } else if (v == 'ExchangeNo') {
            if (_Context.id > 0) {
                var msg = '<br/><label for="CopyFromNo">' + Lang.ExchangeNoComments + '</label><input type="text" id="CopyFromNo" name="CopyFromNo" value="" />';
                var btns = {};
                btns[Lang.Confirm] = "Confrim";
                btns[Lang.Cancel] = 'Cancel';

                $.prompt(msg, {
                    submit: function (v, m, f) {
                        if (v == 'Confrim') {
                            if (f.CopyFromNo == '')
                                return false;
                            var done = getServerValue("exchangevoucherno", "DocumentAccounting", "&ID=" + _Context.id + "&CopyFromNo=" + f.CopyFromNo);
                            if (toInt(done, 0) > 0) {
                                message(Lang.Success);
                                $("#flexi").flexReload();
                            }
                            else
                                message(Lang.Failed);
                        }
                        doEdit(_Context.id);
                    },
                    buttons: btns,
                    focus: 0
                });
            }
            return true;
        }
        else if (v == 'EditComments') {
            if (_Context.id > 0) {
                var msg = '<br/><label for="ReplaceAll">' + Lang.ReplaceAll + '</label><input type="radio" id="ReplaceAll" name="ReplaceWhat" value="all" />';
                msg += '<br/><label for="ReplaceWord">' + Lang.ReplaceWord + '</label><input type="radio" id="ReplaceWord" name="ReplaceWhat" value="word" checked="checked" />';
                msg += '<br/><label for="ReplaceFrom">' + Lang.ReplaceFrom + '</label><input type="text" id="ReplaceFrom" name="ReplaceFrom" />';
                msg += '<br/><label for="ReplaceTo">' + Lang.ReplaceTo + '</label><input type="text" id="ReplaceTo" name="ReplaceTo" />';

                var msg = '<div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">'
                        + '<div class="form-group">'
                        + '        <div class="col-xs-12 col-sm-4">&nbsp;</div>'
                        + '        <div class="col-xs-12 col-sm-8 form-col-without-padding align-right">'
                        + '             <input type="radio" id="ReplaceAll" name="ReplaceWhat" value="all" />'
                        + '             <label for="ReplaceAll">' + Lang.ReplaceAll + '</label>'
                        + '             <span class="margin-15-right">&nbsp;</span>'
                        + '             <input type="radio" id="ReplaceWord" name="ReplaceWhat" value="word" checked="checked" />'
                        + '             <label for="ReplaceWord">' + Lang.ReplaceWord + '</label>'
                        + '        </div>'
                        + '    </div>'
                        + '</div>'
                        + '<div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">'
                        + '    <div class="form-group">'
                        + '        <label for="ReplaceFrom" class="left-aligned-label control-label col-xs-12 col-sm-4">' + Lang.ReplaceFrom + '</label>'
                        + '        <div class="col-xs-12 col-sm-8 form-col-without-padding">'
                        + '            <input type="text" id="ReplaceFrom" name="ReplaceFrom" class="form-control input-sm" />'
                        + '        </div>'
                        + '    </div>'
                        + '</div>'
                        + '<div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">'
                        + '    <div class="form-group">'
                        + '        <label for="ReplaceTo" class="left-aligned-label control-label col-xs-12 col-sm-4">' + Lang.ReplaceTo + '</label>'
                        + '        <div class="col-xs-12 col-sm-8 form-col-without-padding">'
                        + '            <input type="text" id="ReplaceTo" name="ReplaceTo" class="form-control input-sm" />'
                        + '        </div>'
                        + '    </div>'
                        + '</div>';

                var btns = {};
                btns[Lang.Confirm] = "Confrim";
                btns[Lang.Cancel] = 'Cancel';

                $.prompt(msg, {
                    submit: function (v, m, f) {
                        if (v == 'Confrim') {
                            if (f.ReplaceWhat == "word" && f.ReplaceFrom == '')
                                return false;
                            var done = getServerValue("replacevouchercomment", "DocumentAccounting", "&ID=" + _Context.id
                                + "&ReplaceFrom=" + (f.ReplaceWhat == "all" ? "" : f.ReplaceFrom) + "&ReplaceTo=" + f.ReplaceTo);
                            if (toInt(done, 0) > 0)
                                message(Lang.Success);
                            else
                                message(Lang.Failed);
                        }
                        doEdit(_Context.id);
                    },
                    buttons: btns,
                    focus: 0
                });
            }
            return true;
        }
        else if (v == 'New') {
            // unlock
            doEdit(-1);
            return true;
        }

        //if (v == 'OK')
        {
            if (!checkDateIntoFinanceYear(f))
                return false;
            if (!checkProgramStatus())
                return false;
            _Context.priceDate = f.Date;
            if (_Context.id > 0 && isGridMode()) {
                if ($('.docno.invalid').length > 0) {
                    alert(Lang.BankNeedDocNo);
                    $('.docno.invalid').focus();
                    return false;
                }
                $('.DocDate').attr('name', 'DocDate');
                var qry = (_Context.id == -1) ? 'documentaccountinginsert' : 'documentaccountinggridupdate';
                return newMastercallback(v, m, f, 'DocumentAccounting', qry, 'DocumentAccountingItems'
                    , 'documentaccountingitemupdate', 'documentaccountingitemgridinsert', 'documentaccountingitemdelete', 'TreePath', null, show);
            }

            var valid = validateInputs(null, f);
            if (!valid)
                return false;
            if (f.No.indexOf(',') >= 0) {
                alert(findTitleById("No", '#detailForm') + ' : ' + Lang.MustBeAnInteger);
                return false;
            }
            if (toFloat(f.TotalBalance, 0) != 0)
                alert(Lang.Unbalanced);
            var isOk = getServerValue("documentaccountinginsertcheck", "DocumentAccounting", "&No=" + f.No + "&Date=" + f.Date);
            if (isOk != 0 && _Context.id == -1) {
                alert(Lang.UnorderedVoucher + " : " + isOk);
                //return false;
            }
            if (_Context.id == -1) {
                var id = generalOKScalar(v, m, f, 'documentaccountingupdate', 'documentaccountinginsert', 'DocumentAccounting');
                id = parseInt(id, 10);
                if (id <= 0)
                    return false;
                doEdit(id);
                return true;
            }
            else {
                var taskMsg = f.ConfirmTaskMsg;
                var updateRes = generalOK(v, m, f, 'documentaccountingupdate', 'documentaccountinginsert', 'DocumentAccounting');
                if (v != 'OK') {
                    if (!_Context.success)
                        return false;
                    var more = '&RelatedID=' + f.ID + '&NextTaskID=' + v + '&UserID=' + _Context.userID + '&Type=' + WorkflowTypeEnum.Voucher
                        + '&Msg=' + encodeURIComponent(taskMsg) + '&FileName=' + encodeURIComponent(f.FileName);
                    if (runQuery('dotransition', 'TaskImpl', more)) {
                        setTimeout(function () { $("#flexi").flexReload(); }, 200);
                        return true;
                    }
                }
                return updateRes;
            }
        }
        return true;
    }
    function doEdit(id, desiredEntryTaskID) {
        var btns = getButtons(false, "accounting", "voucher");
        if (id == -1)
            btns = getButtons(false);
        else {
            if (isVoucherWorkflow() == 1)
                btns = getTaskButtons(WorkflowTypeEnum.Voucher, id, false, "accounting", "voucher");
        }
        _Context.id = id;
        var op = "GetHtml";
        if (id > 0) {
            if (hasAccess("accounting", "voucher", "print"))
                btns[Lang.Print] = 'Print';
            op = "EditAcc";
            var numDocs = toInt(getServerValue("getnumdads", "DocumentAccountingDocument", "ID=" + id), 0);
            if (numDocs > 0)
                btns[Lang.Docs + ' ' + numDocs] = 'Documents';
            else
                btns[Lang.Docs] = 'Documents';
        }
        var _url = 'get.aspx?op=' + op + '&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();

        if (id > 0 && isGridMode())
            _url += '&view=documentaccountinggrid';

        if (desiredEntryTaskID)
            _url += '&DesiredEntryTaskID=' + desiredEntryTaskID;
        saveSelectedRows("#flexi");
        showPopup(_url, editCallback, btns, '1%', null, true);
        //$.ajax({
        //    url: _url,
        //    type: 'GET',
        //    success: function (html) {
        //        $.prompt(html, {
        //            submit: editCallback,
        //            buttons: btns,
        //            async: false,
        //            focus: 0,
        //            top: '1%'
        //        });

        //        $('#Name').focus();
        //        jscolor.init();
        //    },
        //    error: function (xhr, ajaxOptions, thrownError) {
        //        showWaiting = false;
        //        alert('Status=' + xhr.status + ' StatusText=' + xhr.statusText + ' thrownError=' + thrownError);
        //    }
        //});
    }
    function show(id, celDiv, successCallback, desiredEntryTaskID) {
        if (id) {
            // edit
            if (!hasAccess("accounting", "voucher", "new")) {
                displayError(Lang.AccessToThisPageIsProhibited);
                return;
            }
            var cls = celDiv ? getColClass($(celDiv).closest('tr'), 8) : getGridRowClass("#flexi", 8);
            var daStatus = getStatus(cls, 2);
            if (daStatus == DAStatusEnum.Determined) {
                alert(Lang.DAStatusIsDetermined);
                return;
            }
            doEdit(id, desiredEntryTaskID);
            return;
        }
        var mode = getGridRowContents("#flexi", 10); // nazanm eme desturekeye ya na
        var otherid = getGridRowContents("#flexi", 9);
        //alert(mode + " === " + otherid);
        var page = "", table = "", query = "";
        if (mode == Lang.DAModeFile) {
            page = "bedridden.htm";
            table = "Files";
            query = "bedridden";
        }
        else if (mode == Lang.DAModePurchase) {
            page = "purcahse.htm";
            table = "Purchase";
            query = "purchase";
        }
        else if (mode == Lang.DAModeCheck) {
            page = "check.htm";
            table = "Checks";
            query = "check";
        }
        else if (mode == Lang.DAModeRefuse) {
            page = "refuse.htm";
            table = "Refuse";
            query = "refuse";
        }
        else if (mode == Lang.DAModeReturn) {
            page = "return.htm";
            table = "Returns";
            query = "return";
        }
        else if (mode == Lang.DAModeStoreDraft) {
            page = "storedraft.htm";
            table = "StoreDraft";
            query = "storedraft";
        }
        else {
            alert(Lang.PageNotFound);
            return false;
        }
        var _url = 'get.aspx?op=GetHtml&table=' + table + '&qd=' + query + '&view=' + page + '&id=' + otherid + appendTime();
        showPopup(_url);
    }

    function checkDateIntoFinanceYear(f) {
        if (_Context.fyStart == '') {
            alert(Lang.NoCurYear);
            return false;
        }
        if (f.Date == '' || validateDateIntoFinanceYear(f.Date))
            return true;
        alert(Lang.PurchasesJsM2 + _Context.fyStart + " - " + _Context.fyEnd + Lang.PurchasesJsM3);
        return false;
    }

    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['accounting,voucher']);
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.DocumentAccounting, parentTitle: Lang.MAccounting, parentUrl: 'menu/accounting.aspx' } }));
    setClassAccounting();
    dateSelect('.dateSelect');
    startupID = $.urlParam('id');
    if (!startupID)
        startupID = -1;


    function handleArrows(e) {
        // up: 38, down: 40
        var selected = $('#items .selected');
        if (selected.length == 0)
            return;
        var index = $('#items tr').index(selected);
        var len = $('#items tr').length;
        //alert($('#items tr').length + " - " + index);
        if (e.keyCode == 38) {
            if (index == 1)
                return;
            e.preventDefault();
            selected.removeClass('selected');
            $('#items tr:eq(' + (index - 1) + ')').addClass('selected');
            return false;
        }
        if (e.keyCode == 40) {
            if (index == len - 1)
                return;
            e.preventDefault();
            selected.removeClass('selected');
            $('#items tr:eq(' + (index + 1) + ')').addClass('selected');
            return false;
        }
    }
    $(document).unbind('keydown', handleArrows);
    $(document).bind('keydown', handleArrows);


    run();

    // ------------------------    
    var lastChecked = toInt(getCookie("gridModeVoucher"), 0);
    if (lastChecked)
        $('.gridMode').toggleClass('glyphicon glyphicon-ok');
}