﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
92-11-12: 
    - file created
*/
function view_orderdefs() {
    var startupID = -1;
    function doCommand(com, grid) {
        var isNew = com == Lang.GBNew;
        if (!isNew) {
            var btn = $(".fbutton span:contains(" + com + ")");
            if (btn.hasClass('new'))
                isNew = true;
        }
        if (isNew && com != Lang.GBNew) {
            var desiredEntryTaskID = $(".fbutton span:contains(" + com + ")").attr('class').substring('new '.length);
            //alert(desiredEntryTaskID);
            show(-1, null, null, desiredEntryTaskID);
        }
        else
            generalCommand(com, grid, show);
    }
    function run() {
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.Title, name: 'Title', sortable: true, css_class: 'width-250' },
                    { display: Lang.FormName, name: 'FormName', sortable: false, css_class: 'width-100 align-center' },
                    { display: Lang.ReportName, name: 'ReportName', sortable: true, css_class: 'width-200 align-center' },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, hide: hideColumn("sales", "return", "edit"), css_class: 'action-cell' }
                ],
                buttons:
                   [
                       { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                       { name: Lang.GBEdit, bclass: 'edit', onpress: doCommand },
                       { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                       { separator: true },
                      // { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                       { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                   ],
                searchitems: null,
                sortname: "Title",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight, resizable: gridResizable,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                },
                onSuccess: function (grid) {
                    if (startupID != -1) {
                        show(startupID);
                        startupID = -1;
                    }
                }
            }
            );
    }

    function showcallback(v, m, f) {
        var qry = (_Context.id == -1) ? 'orderdefinsert' : 'orderdefupdate';
        return newMastercallback(v, m, f, 'OrderDefs', qry, 'OrderDefFields'
                , 'orderdeffieldupdate', 'orderdeffieldinsert', 'orderdeffielddelete', 'FieldID', null, show);
    }
    function show(id, celDiv, successCallback, desiredEntryTaskID) {
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, successCallback, true);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    
    $("#bar").prepend(breadcrumb({ title: _Context.title, grandParent: { title: Lang.MSalary, url: 'menu/salary.aspx' }, parent: { title: Lang.SalaryDefinition, url: 'menu/salarydefinition.aspx' } }));
    run();
}