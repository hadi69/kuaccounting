﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    94-06-04: file created
*/
function view_buildorder() {
    makeChosen();
    $("#tableItems").smartTable();
    var cnt = 0, mID = -1;
    function setAutoCompletes() {
        $("#tableItems tr:last .storeservicename").unautocomplete();
        $('#tableItems .storeservicename').each(function () {
            var id = $(this).attr('id');
            id = id.substring(id.indexOf('_') + 1);
            var unitName = $('#u_' + id);
            var storeServiceId = $('#sid_' + id).val();
            if (storeServiceId > 0) {
                var _autourl1 = 'get.aspx?op=Auto&table=Units&qd=storedraftunitsauto&StoreServiceID=' + storeServiceId;
                autoComplete('Units', 'unitsauto', unitName, "#justfurfun",
                    function (event, data, formatted) {
                        var id1 = $(event.target).attr('id');
                        id1 = id1.substring(id1.indexOf('_') + 1);
                        $(event.target).next().val(data[0]); // ID
                        if (data[2] && data[2] != 0) {
                            //$('#a_' + id1).next().text('x')
                            $('#ucc_' + id1).val(data[2]); // CurrencyConverter}
                        }
                        else {
                            //$('#a_' + id1).next().text(' ')
                            $('#ucc_' + id1).val('1');
                        }
                    }
                , null, null, null, _autourl1);
            }
        });
        if ($("#tableItems tr:last .storeservicename").val() != '') {
            return;
        }
        var more = encodeURIComponent("StoreID=" + $("#StoreID").val());
        var _autourl = 'get.aspx?op=Auto&table=BuildOrder&qd=storeservicesauto&BuyerID=NULL&StoreID=' + $("#StoreID").val() + appendTime();
        autoCompleteFree('StoreServices', 'storeservicesauto', "#tableItems tr:last .storeservicename", "#justfurfun"
                , function (event, data, formatted, textInput, hiddenInput, $input) {
                    $(event.target).next().val(data[0]); // ID
                    var id = $(event.target).attr('id');
                    id = id.substring(id.indexOf('_') + 1);
                    $('#u_' + id).next().val(data[6]); // unitid
                    $('#u_' + id).val(data[7]); // unitname
                    var _autourl1 = 'get.aspx?op=Auto&table=Units&qd=storedraftunitsauto&StoreServiceID=' + data[0];
                    autoComplete('Units', 'unitsauto', "#tableItems tr:last .unitname", "#justfurfun",
                        function (event, data, formatted) {
                            var id1 = $(event.target).attr('id');
                            id1 = id1.substring(id1.indexOf('_') + 1);
                            $(event.target).next().val(data[0]); // ID
                            if (data[2] && data[2] != 0) {
                                //$('#a_' + id1).next().text('x')
                                $('#ucc_' + id1).val(data[2]); // CurrencyConverter}
                            }
                            else {
                                //$('#a_' + id1).next().text(' ')
                                $('#ucc_' + id1).val('1');
                            }
                        }
                    , null, null, null, _autourl1);
                    $hiddenInput = $input.next();
                    $('#tableItems tr .storeserviceid').each(function () {
                        if ($(this).attr('id') != $hiddenInput.attr('id')) {
                            if ($(this).val() == data[0]) {
                                $hiddenInput.val('');
                                $input.val('');
                                    alert(Lang.StoreDraftJsM3);
                                }
                            }
                        });
                }
                , function (data, i, n, value) {
                    return formatService(data);
                }, null, null, _autourl);
    }
    function addRow(node) {
        var row = '<tr>';
        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">'
            + (cnt + 1)
            + '</td>';

        row += '<td data-th="' + Lang.CommodityName + '">'
            + '<input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="text" id="se_' + cnt + '" name="StoreServiceName" value="' + getNodeVal(node, "StoreServiceName") + '" class="storeservicename form-control input-sm"/>'
            + '<input type="hidden" id="sid_' + cnt + '" name="StoreServiceID" value="' + getNodeVal(node, "StoreServiceID") + '" class="storeserviceid" />'
            + '</td>';

        row += '<td data-th="' + Lang.Units + '">'
            + '<input type="text" id="u_' + cnt + '" name="UnitName" value="' + getNodeVal(node, "UnitName") + '" class="unitname align-center form-control input-sm" />'
            + '<input type="hidden" name="UnitID" value="' + getNodeVal(node, "UnitID") + '" />'
            + '</td>';

        /*row += '<td data-th="' + Lang.NumberOfApplicants + '">'
            + '<input type="text" id="rq_' + cnt + '" name="NumRequest" value="' + getNodeVal(node, "NumRequest") + '"'
            + 'class="validate[custom[number]] numrequest form-control input-sm align-center" />'
            + '<input type="hidden" name="SNumRequest" value="' + getNodeVal(node, "NumRequest") + '" />'
            + '</td>';*/

        row += '<td data-th="' + Lang.NumberOfApplicants + '">'
          + '<div class="input-group input-group-sm">'
          + '<input type="text" id="rq_' + cnt + '" name="NumRequest" value="' + getNodeVal(node, "NumRequest", 0) + '" class="validate[custom[number]] numrequest form-control width-100-percent min-width-50 align-center" />'
          + '<span class="input-group-addon no-left-border no-right-border">x</span>'
          + '<input readonly="readonly" size="1" id="ucc_' + cnt + '" name="UnitCurrencyConverter" value="' + getNodeVal(node, "UnitCurrencyConverter", 1) + '" class="form-control width-50 align-center" />'
          + '<input type="hidden" name="SNumRequest" value="' + getNodeVal(node, "NumRequest") + '" />'
          + '</div>'
          + '</td>';

        row += '<td data-th="' + Lang.ProductedNum + '">'
            + '<input type="text" id="pr_' + cnt + '" readonly="readonly" name="Producted" value="' + getNodeVal(node, "CalcProducted") + '"'
            + 'class="validate[custom[number]] form-control input-sm producted align-center" />'
            + '</td>';

        row += '<td data-th="' + Lang.PeriodProduct + '">'
            + '<input type="text" id="nc_' + cnt + '" name="NumConfirmed" placeholder="'
            + (toFloat(getNodeVal(node, "NumRequest"), 0) - toFloat(getNodeVal(node, "CalcProducted"), 0)).toString() + '" value=""'
            + 'class="validate[custom[number]] form-control input-sm numconfirmed align-center" />'
            + '</td>';

        row += '<td data-th="' + Lang.Comments + '">'
            + '<input type="text" id="co_' + cnt + '" name="Comments" value="' + getNodeVal(node, "Comments") + '"'
            + 'class="form-control input-sm" />'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a class="btn btn-danger btn-xs btn-block btn-remove remove" id="r_' + cnt + '">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }

    function calcSumConfirmed() {
        var sumConfirmed = 0;
        $('.numconfirmed').each(function (index, value) {
            sumConfirmed += toFloat($(value).val(), 0);
        });

        $('#SumConfirmed').val(sumConfirmed);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    setTimeout(function() {
        $('#WardName').focus();
    },3);
    mID = toInt($('#MasterID').val(), -1);
    processPrintButton(0, 1, mID);

    dateSelect('.dateSelect');
    if ($("#OrderDate").val() == '')
        $("#OrderDate").val(_Context.today);
    setValue("#WorkflowStatus");

    if (mID == -1) {
        $("#RequesterID").val(_Context.personnelID);
        $("#RequesterName").val(_Context.personnelName);
    }

    if (_Context.isAccountingVer)
        $('.noacc').hide();
    autoComplete('Ward', 'wardauto', '#WardName', '#WardID');
    autoComplete('Personnel', 'personauto', '#RequesterName', '#RequesterID', null
            , function (data, i, n, value) {
                $('#RequesterID').val('');
                return formatPersonnelItem(data);
            });
    autoComplete('Stores', 'storeauto', '#StoreName', '#StoreID', setAutoCompletes);
    if (mID == -1)
    {
        setDefault('Stores', 'firststore', "#StoreName", "#StoreID");
    }
    else
        $('#workflowtasks').html(showWorkflowTasks(mID, WorkflowTypeEnum.BuildOrder));

    autoComplete('Priority', 'enumauto', '#PriorityName', '#PriorityID');
    // DetailForm
    fillTable("get.aspx?op=GetXml&qd=buildorderitem&BuildOrderID=" + mID, '#tableItems', addRow);
   
    setAutoCompletes();
    doMore();

    function doMore() {

        function removeRow($who) {
            var id = $who.attr('id'); // r_1
            id = id.substring(2); // r_1  -->  1
            if (toFloat($('#pr_' + id).val(), 0) > 0) {
                Growl.Notice(Lang.BuildOrderItemDeleteMsg, 3000);
                return false;
            }
            $('#se_' + id).val(""); // CommodityName
            $('#se_' + id).next().val(-1); // CommodityID
            $who.parent().parent().hide();
            Growl.Notice(Lang.DeletedSuccessfully);
        }
        $('.remove').click(function () {
            removeRow($(this));
        });

        $('.numrequest').blur(function () {
            var rowid = $(this).attr('id');
            rowid = rowid.substring(rowid.indexOf('_', 0) + 1); // d_1  -->  1
            var Producted = toFloat($('#pr_' + rowid).val(), 0);
            var requested = toFloat($(this).val(), 0);
            if (requested < Producted) {
                Growl.Notice(Lang.RequestedMsg, 3000);
                $(this).val($(this).next().val());
            };
        });

        $('.numconfirmed').blur(function() {
            var rowid = $(this).attr('id');
            rowid = rowid.substring(rowid.indexOf('_', 0) + 1); // d_1  -->  1
            var remaind = toFloat($('#rq_' + rowid).val(), 0) - toFloat($('#pr_' + rowid).val(), 0);
            var periodProduct = toFloat($(this).val(), 0);
            if (periodProduct > remaind) {
                Growl.Notice(Lang.PeriodProductMsg, 3000);
                $(this).val('');
            };
            calcSumConfirmed();
        });
        disableKeydownOnLastInputByName('Comments');
        $('.add').click(function () {
            var table = $("#tableItems");
            $('tr:last', table).after(addRow(null));
            Growl.Notice(Lang.RowAddedSuccessfully);
            var lastRow = $('tr:last', table);
            disableKeydownOnLastInputByName('Comments');
            setAutoCompletes();
            $('.remove', lastRow).click(function () {
                removeRow($(this));
            });
            $('.numconfirmed', lastRow).blur(function () {
                var rowid = $(this).attr('id');
                rowid = rowid.substring(rowid.indexOf('_', 0) + 1); // d_1  -->  1
                var remaind = toFloat($('#rq_' + rowid).val(), 0) - toFloat($('#pr_' + rowid).val(), 0);
                var periodProduct = toFloat($(this).val(), 0);
                if (periodProduct > remaind) {
                    Growl.Notice(Lang.PeriodProductMsg, 3000);
                    $(this).val('');
                };
                calcSumConfirmed();
            });
            $('input[name=StoreServiceName]', lastRow).focus();
        });
        insertValidation();
    }

    if (mID > 0) {
        uploadify('#ChooseFile', '#FileName', '#StartUpload', 'workflow/buildorder/' + mID
                   , function (fileName) {
                       $('#FileNameDisplay').html(fileName);
                   });
    }
    else
        $('.transDiv').hide();
}