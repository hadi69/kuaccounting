﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
93-05-15: file created
*/
function view_constitem() {
    // ------------------------------
    // startup code
    // ------------------------------
    setChecked("#Enabled");
    autoComplete('Category', 'enumauto', '#CategoryName', '#CategoryID');
    insertValidation();
}