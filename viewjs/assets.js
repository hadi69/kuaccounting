﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
    95-12-27: file created
*/
function view_assets() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        $("#flexi").flexigrid
                (
                {
                    url: _url,
                    dataType: 'xml',
                    colModel:
                    [
                        { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'center', hide: true },
                        { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell'},
                        { display: Lang.Code, name: 'Code', sortable: true, css_class: 'width-90 align-center' },
                        { display: Lang.Title, name: 'Title', sortable: true },
                        { display: Lang.TitleEn, name: 'EnTitle', sortable: true, css_class: 'width-120 align-center' },
                        { display: Lang.Units, name: 'UnitName', sortable: true, css_class: 'width-120 align-center' },
                        { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' }
                    ],
                    buttons:
                    [
                        { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                        { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                        { separator: true },
                        { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                    ],
                    searchitems: null,
                    sortname: "UnitName",
                    sortorder: "desc",
                    usepager: true,
                    useRp: true,
                    rp: gridRp,
                    showTableToggleBtn: gridToggle,
                    width: gridWidth,
                    height: gridHeight, resizable: gridResizable,
                    singleSelect: false,
                    onDoubleClick: function (row, grid, options) {
                        var id = row.id.substring(3);
                        show(id);
                    }
                });
    }
    function displayCalcMethod(celDiv, id) {
        var _mode = parseInt($(celDiv).html(), 10);
        ShowCalcMethod(celDiv, _mode);
    }
    function showcallback(v, m, f) {
        return newShowcallback(v, m, f, 'assetupdate', 'assetinsert', _Context.table, null, show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['asset,asset']);
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.Asset, parentTitle: Lang.MAsset, parentUrl: 'menu/asset.aspx' } }));
    setClassAsset();
    run();
}