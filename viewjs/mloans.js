﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
93-01-12: file created
*/
function view_mloans() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var fID = $.urlParam('id');
        var more = "FundID=" + fID;
        var terminology = getTerminology(_Context.table);
        if (terminology)
            $('#term').html("<a href='help/terminology.aspx' target=_blank title='" + Lang.HasTerminology + "'>" + terminology + "</a>");
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query +'&more='+ more + appendTime();
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.FundName, name: 'FundName', sortable: true, css_class: 'width-120 align-center' },
                    { display: Lang.PersonnelName, name: 'PersonnelName', sortable: false},
                    { display: Lang.Amount, name: 'Amount', sortable: false, process:showMonetary, css_class: 'width-100 align-center' },
                    { display: Lang.MonthlyPay, name: 'MonthlyPay', sortable: false, process:showMonetary, css_class: 'width-100 align-center' },
                    { display: Lang.TotalPaied, name: 'TotalPaied', sortable: false, process:showMonetary, css_class: 'width-100 align-center', },
                    { display: Lang.status, name: 'Status', sortable: false, process: displayStatus, css_class: 'width-80 align-center' },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, css_class: 'action-cell', process: function (celDiv, id) { editLink(celDiv, id, show); } },
                    { display: Lang.ColDel, name: 'Del', sortable: false, css_class: 'action-cell', process: delLink }

                ],
                buttons:
                [
                    { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                    { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                    { separator: true },
                    { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                    { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                sortname: "FundName",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                }
            }
            );
    }

    function displayStatus(celDiv, id) {
        var status = parseInt($(celDiv).html(), 7);
        displayLoanStatusEnum(celDiv, status);
    }

    function showcallback(v, m, f) {
        $('.PayDate').attr('name', 'PayDate');
        var qry = _Context.id == -1 ? 'loansinsert' : 'loansupdate';
        return newMastercallback(v, m, f, _Context.table, qry, 'LoanItems', 'loanitemupdate', 'loaniteminsert', 'loanitemdelete', 'PayAmount', '#subDetailForm', show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        var fID = $.urlParam('id');
        var m = "&ID=" + fID;
        var fName = getServerValue("selectfund", "Funds", m);
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + '&FundID=' + fID + '&FundName=' + fName + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    $("#bar").prepend(breadcrumb({
        title: _Context.title,
        grandParent: {
            title: Lang.MSalary,
            url: 'menu/salary.aspx'
        },
        parent: {
            title: Lang.Funds,
            url: 'list.aspx?table=Funds&menu=salarysa'
        }
    }));
    setClassSalary();
    run();
}