﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
92-11-10:
    - combined with purchaseinvoice
92-10-29: 
    - file created
*/
function view_basket() {
    var cnt = 0, mID = -1;
    makeChosen();
    $("#tableItems").smartTable();

    function updateTotal() {
        var sumPrice = 0, sumAmount = 0;
        $('.total').each(function (index, value) {
            sumPrice += toFloat($(value).val(), 0);
        });
        $('.num').each(function (index, value) {
            sumAmount += toFloat($(value).val(), 0);
        });

        $('#TotalPrice').val(sumPrice.toFixed(_Context.decimalPoints));
        $('#TotalNum').val(sumAmount);
        var finalPrice = sumPrice - toFloat($('#CouponPrice').val(), 0) + toFloat($('#ShipmentPrice').val(), 0);
        $('#FinalPrice').val(numberWithCommas(finalPrice)); 
        
        commaInputsSet();
    }
    function updatePrice(was) {
        var id = $(was).attr('id');
        id = id.substring(id.indexOf('_') + 1); // d_1  -->  1
        var price = $('#pr_' + id).val();
        var amount = $('#n_' + id).val();
        $('#t_' + id).val((toFloat(price, 0) * toFloat(amount, 0)).toFixed(_Context.decimalPoints));
        updateTotal();
    }
  
    function setServiceData(serviceNameInput, data) {
        serviceNameInput.next().val(data[0]); // ID
        var id = serviceNameInput.attr('id');
        id = id.substring(id.indexOf('_') + 1);
        var percent = 1 + toFloat(data[11], 0) / 100.0;
        $('#n_' + id).val(1);
        var price = 0;
        // has ProfitPercent from service declaration
        // Price priority:
        // 1- GroupingPrice: col10 | col14 = BuyPrice, col16 = SalePrice
        // 2- ProfitPercent: col10 = BasePrice, col2 = SalePrice
        // 3- Last Prices: col12 = LastSalePrice (TheLPSP), col14 = LastBuyPrice (TheLPBP)
        //price = data[2];

        if (data[11]) {
            // ProfitPercent
            if (data[2])
                price = data[2];
        }
        else {
            // Last Prices
            if (data[12])
                price = data[12];
        }
        if (data[16]) {
            // GroupingPrice
            price = data[16];
            // BuyPrice is from Old method
        }
        $('#p_' + id).val(numberWithCommas((toFloat(toMonetary(price), 0.0)).toFixed(_Context.decimalPoints))); // price
    }
    function setServiceAutoComplete() {
        var table = $("#tableItems");
        $("#tableItems tr:last .storeservicename").unautocomplete();
        if ($("#tableItems tr:last .storeservicename").val() != '') {
            return;
        }
        var _autourl = 'get.aspx?op=Auto&table=StoreDraft&qd=storeservicesauto&StoreID=-1&BuyerID=-1' + appendTime();
        autoCompleteFree('StoreServices', 'storeservicesauto', "#tableItems tr:last .storeservicename", "#justfurfun"
                , function (event, data, formatted) {
                    $('tr .storeserviceid', table).each(function () {
                        if ($(this).attr('id') != $('tr:last .storeserviceid', table).attr('id')) {
                            if ($(this).val() == data[0]) {
                                alert(Lang.StoreDraftJsM3);
                            }
                        }
                    });
                    setServiceData($(event.target), data);
                }
                , function (data, i, n, value) {
                    return formatService(data);
                }, null, null, _autourl);
    }
    function addRow(node) {
        // id="x" --> the validationEngine requires ID
        var readonly = node != null ? ' readonly="readonly" ' : '';
        var row = '<tr>';
        row += '<td data-th="' + Lang.ColRowNr + '" class="row-number align-center  width-40">' + (cnt + 1) + '</td>';

        row += '<td class="width-300" data-th="' + Lang.StoreService + '">'
            + '<input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="text" id="se_' + cnt + '" name="ServicePriceName" value="' + getNodeVal(node, "ServicePriceName") + '" class="servicepricename form-control"' + readonly + ' />'
            + '<input type="hidden" id="sid_' + cnt + '" name="ServicePriceID" value="' + getNodeVal(node, "ServicePriceID", 0) + '" class="servicepriceid" />'
            + '</td>';

        row += '<td data-th="' + Lang.Num + '" class="align-center width-130">'
            + '<input type="text" id="n_' + cnt + '" name="Num" value="' + getNodeVal(node, "Num")
            + '" class="validate[custom[number]] amount form-control width-100-percent min-width-50 align-center" />'
            + '</td>';

        row += '<td data-th="' + Lang.Price + '" class="align-center acc width-150">'
            + '<input type="text" id="pr_' + cnt + '" name="Price" value="'
            + getNodeVal(node, "Price") + '" class="price monetary validate[custom[integer]] form-control input-sm align-center" />'
            + '</td>';

        row += '<td data-th="' + Lang.TotalPrice + '" class="align-center acc width-100">'
            + '<input type="text" id="t_' + cnt + '" name="Total" value="'
            + (getNodeVal(node, "Price", 0) * getNodeVal(node, "Num", 0)).toFixed(_Context.decimalPoints)
            + '" class="acc total monetary validate[custom[integer]] form-control input-sm align-center" readonly="readonly" />'
            + tdEnd();


        row += '<td class="align-center action-remove width-40">'
        + '<a id="r_' + cnt + '" class="btn btn-danger btn-xs btn-block btn-remove remove">'
        + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
        + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
        + '</a>'
        + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }

    function autoPostPack(event, data, formatted) {
        $(event.target).next().val(data[0]); // ID
    }
  
    // ------------------------------
    // startup code
    // ------------------------------
    mID = toInt($('#MasterID').val(), -1);

    // MasterForm
    dateSelect('.dateSelect');
    setValue("#Status");
    setValue("#PayStatus");
    setValue("#PayMethod");
    $('#HiddenStatus').val($('#Status').val());
    if ($("#BasketDate").val() == '')
        $("#BasketDate").val(_Context.today);


    autoComplete('Users', 'userauto', '#UserName', '#UserID');

    // DetailForm
    if (hasValuePositive(mID)) {
        fillTable("get.aspx?op=GetXml&qd=basketitem&BasketID=" + mID, '#tableItems', addRow);
        disableKeydownOnLastInputByName('Series');
    }
    disableKeydownOnLastInputByName('Price');

    $('.price, .num').blur(function () { updatePrice(this); });

    function removeRow($who) {
        var id = $who.attr('id'); // r_1
        id = id.substring(id.indexOf('_') + 1); // r_1  -->  1
        $('#se_' + id).val(""); // StoreServiceName
        $('#se_' + id).next().val(-1); // StoreServiceID
        $('#n_' + id).val(''); // amount
        $('#pr_' + id).val(''); // price
        $who.parent().parent().hide();
        $who.parent().parent().find(':input').removeClass();
        updateTotal();
        Growl.Notice(Lang.DeletedSuccessfully);
    }
    $('.remove').click(function () {
        var element = $(this);
        doConfirm(function () {
            removeRow(element);
        }, Lang.ConfirmDelete);
    });
    $('.add').click(function () {
        var table = $('#tableItems');
        $('tr:last', table).after(addRow(null));
        Growl.Notice(Lang.RowAddedSuccessfully);
        var lastRow = $('tr:last', table);
        disableKeydownOnLastInputByName('Price');//ExpireDate--->bug

        setServiceAutoComplete();

    
        $('.remove', lastRow).click(function () {
            var element = $(this);
            doConfirm(function () {
                removeRow(element);
            }, Lang.ConfirmDelete);
        });
     
        $('.price, .num', lastRow).blur(function () { updatePrice(this); });
        commaInputs();
        monetaryKeys();
        // Focus the input used for ServicePriceName
        $('input[name=ServicePriceName]', lastRow).focus();
    });


    $('#ShipmentPrice,#TotalPrice,#CouponPrice').blur(updateTotal);
    insertValidation();
    monetaryInputsSet(null, '#BasketDate');
    commaInputs();
    updateTotal();
}