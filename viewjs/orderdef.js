﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
92-11-12: 
    - file created
*/
function view_orderdef() {
    $('#tableItems').smartTable();

    var cnt = 0, mID = -1;
    function autoPostPack(event, data, formatted) {
        $(event.target).next().val(data[0]); // FieldID
    }

    function addRow(node) {
        // id="x" --> the validationEngine requires ID
        var readonly = node != null ? " readonly='readonly' " : "";
        var row = '<tr>';
        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">'
            + (cnt + 1)
            + '</td>';

        row += '<td data-th="' + Lang.Field + '">'
            + '<input type="hidden" id="oid_' + cnt + '" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="text" id="f_' + cnt + '" name="FieldName" value="' + getNodeVal(node, "FieldName") + '" class="fieldname form-control input-sm" ' + readonly + ' />'
            + '<input type="hidden" name="FieldID" value="' + getNodeVal(node, "FieldID") + '" />'
            + '</td>';

        row += '<td data-th="' + Lang.DefaultValue + '">'
            + '<input type="text" id="de_' + cnt + '" name="DefaultValue" value="' + getNodeVal(node, "DefaultValue") + '" class="defaultvalue form-control input-sm align-center" />'
            + '</td>';

        row += '<td data-th="' + Lang.Formula + '">'
            + '<input type="text" id="fo_' + cnt + '" name="Formula" value="' + getNodeVal(node, "Formula") + '" class="formula form-control input-sm" />'
            + '</td>';

        row += '<td data-th="' + Lang.Minimum + '">'
            + '<input type="text" id="mi_' + cnt + '" name="Minimum" value="' + getNodeVal(node, "Minimum") + '" class="minimum form-control input-sm align-center" />'
            + '</td>';

        row += '<td data-th="' + Lang.Maximum + '">'
            + '<input type="text" id="ma_' + cnt + '" name="Maximum" value="' + getNodeVal(node, "Maximum") + '" class="maximum form-control input-sm align-center" />'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a class="btn btn-danger btn-xs btn-block btn-remove remove" id="r_' + cnt + '">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }

    // ------------------------------
    // startup code
    // -----------------------------
    mID = toInt($('#MasterID').val(), -1);
    // DetailForm
    if (hasValuePositive(mID)) {
        
        fillTable("get.aspx?op=GetXml&qd=orderdeffield&OrderDefID=" + mID, '#tableItems', addRow);
        var more = "(Used=0 OR Used=2)";
        autoCompleteFree('Fields', 'enumauto1', "#tableItems .fieldname", "#justfurfun"
                , autoPostPack, null, null, more);
    }
    function removeRow($who) {
        var id = $who.attr('id'); // r_1
        id = id.substring(id.indexOf('_') + 1); // r_1  -->  1
        
        doConfirm(function () {
            if (runQuery("orderdeffielddelete", "OrderDefFields", "&ID=" + $('#oid_'+id).val())) {
                $('#f_' + id).val(""); // StoreServiceName
                $('#f_' + id).next().val(-1); //StoreServiceID
                $('#mi_' + id).val(''); // price
                $who.parent().parent().remove();
                $who.parent().parent().find(':input').removeClass();
                Growl.Notice(Lang.DeletedSuccessfully);
            }
        }, Lang.ConfirmDelete);
        // This causes an erro, since this function is not found.
        // I commented it out, to prevent more errors
        //updateTotal();
    }
    $('.remove').click(function () {
        removeRow($(this));
    });

    function doMore() {
        $('.add').click(function () {
            var table = $('#tableItems');
            $('tr:last', table).after(addRow(null));
            Growl.Notice(Lang.RowAddedSuccessfully);
            var lastRow = $('tr:last', table);
            $('.remove', lastRow).click(function () {
                removeRow($(this));
            });
            var more = "(Used=0 OR Used=2)";
            autoCompleteFree('Fields', 'enumauto1', "#tableItems tr:last .fieldname", "#justfurfun"
                , autoPostPack, null, null, more);
        });
        insertValidation();
    }
    doMore();
    disableKeydownOnLastInputByName('FieldName');
}