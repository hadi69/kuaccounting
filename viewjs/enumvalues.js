﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
92-10-19: showPopup used
92-10-16: file created
*/
function view_enumvalues() {
    function doCommand(com, grid) {
        if (_Context.table.toLowerCase() == 'program')
            if (com == Lang.GBNew || com == Lang.GBDel) {
                if (_Context.fyStart == '') {
                    alert(Lang.NoCurYear);
                    return;
                }
            }
        generalCommand(com, grid, show);
    }
    function run() {
        var id = $.urlParam('id');
        var more = '&ID=' + id;
        var parent = getServerValue('getparentenum', 'Enums', more);
        var terminology = getTerminology(_Context.table);
        if (terminology)
            $('#term').html("<a href='help/terminology.aspx' target=_blank title='" + Lang.HasTerminology + "'>" + terminology + "</a>");
        more = 'EnumID=' + id;
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + '&more=' + more + appendTime();
        $("#flexi").flexigrid
                (
                {
                    url: _url,
                    dataType: 'xml',
                    colModel:
                    [
                        { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                        { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                        { display: Lang.Title, name: 'Title', sortable: true, css_class: 'width-150' },
                        { display: Lang.Code, name: 'Code', sortable: true, css_class: 'width-150' },
                        { display: Lang.Parent, name: 'ParentName', sortable: true, css_class: 'width-200'},
                        { display: Lang.Value, name: 'Value', sortable: true, css_class: 'width-200'},
                        { display: Lang.Comments, name: 'Comments', sortable: true },
                        { display: Lang.ColEdit, name: 'Edit', sortable: false, hide: hideColumn("manager", "enums", "edit"), process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' },
                        { display: Lang.ColDel, name: 'Del', sortable: false, hide: hideColumn("manager", "enums", "delete"), process: delLink, css_class: 'action-cell' }
                    ],
                    buttons:
                    [
                        { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                        { name: Lang.GBEdit, bclass: 'edit', onpress: doCommand },
                        { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                        { separator: true },
                        { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                        { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                    ],
                    searchitems: null,
                    sortname: "Title",
                    sortorder: "asc",
                    usepager: true,
                    //title: _Context.title,
                    useRp: true,
                    rp: gridRp,
                    showTableToggleBtn: _options.gridToggle,
                    width: _options.gridWidth,

                    height: _options.gridHeight, resizable: _options.gridResizable,
                    singleSelect: true,
                    onDoubleClick: function (row, grid, options) {
                        var id = row.id.substring(3);
                        show(id);
                    },
                    onSuccess: function (grid) {
                        restoreSelectedRows('#flexi');
                    }
                }
                );
    }
    function showcallback(v, m, f) {
        return newShowcallback(v, m, f, 'enumvalueupdate', 'enumvalueinsert', _Context.table, null, show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        saveSelectedRows('#flexi');
        var EnumID = $.urlParam('id');
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + '&EnumID=' + EnumID + appendTime();
        showPopup(_url, showcallback, null, null, function () {
            jscolor.init();
            if (successCallback)
                successCallback();
        });
    }
    // ------------------------------
    // startup code
    // ------------------------------
    $("#bar").prepend(breadcrumb({
        title: _Context.title,
        grandParent: {
            title: Lang.MSalary,
            url: 'menu/salary.aspx'
        },
        parent: {
            title: Lang.DefineBasicData,
            url: 'list.aspx?table=Enums&menu=salarysa'
        }
    }));

    run();
}