﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
    92-10-19: file created
*/
function view_monetaryunit() {
    // ------------------------------
    // startup code
    // ------------------------------
    var mID = toInt($('#ID').val(), -1);
    if (mID == -1)
        $('#Enabled').attr("checked", 'checked');
    else
        setChecked("#Enabled");
    insertValidation();
}