﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
92-10-16: file created
*/
function view_workflow() {
    // ------------------------------
    // startup code
    // ------------------------------
    var mID = toInt($('#ID').val(), -1);
    if (mID == -1)
        $('#Active').attr("checked", 'checked');
    else
        setChecked("#Active");
    setValue("#Type");
    insertValidation();
    makeChosen();
    
    var redraw, g, renderer;


    var width = 900;
    var height = 400;

    g = new Graph();
    
    var _url = "get.aspx?op=GetXml&qd=workflowtasks&WorkflowID=" + mID;
    iterateRows(_url, function (node) {
        //g.addNode(getNodeVal(node, 'Title', ''));
        g.addNode(getNodeVal(node, 'ID', ''), { label: getNodeVal(node, 'Title', '') });
    });

    var _url = "get.aspx?op=GetXml&qd=workflowtransition&WorkflowID=" + mID;
    var edges = ";";
    iterateRows(_url, function (node) {
        var lbl = getNodeVal(node, 'Name', '');
        var already = false;
        var edge = getNodeVal(node, 'TaskID', '') + "_" + getNodeVal(node, 'NexttaskID', '');
        var edgeReverse = getNodeVal(node, 'NexttaskID', '') + "_" + getNodeVal(node, 'TaskID', '');
        if (edges.indexOf(edgeReverse) > 0)
            already = true;
        //    lbl = ".\n.\n" + lbl;
        edges += edge + ";";
        var st = {
            directed: true,
            hasReturn: already,
            label: lbl,
            "label-style": {
                "font-size": 20
            }
        };
        g.addEdge(getNodeVal(node, 'TaskID', ''), getNodeVal(node, 'NexttaskID', ''), st);
    });


    /* add a node with a customized shape 
       (the Raphael graph drawing implementation can draw this shape, please 
       consult the RaphaelJS reference for details http://raphaeljs.com/) */
    //    var render = function(r, n) {
    //        var label = r.text(0, 30, n.label).attr({opacity:0});
    /* the Raphael set is obligatory, containing all you want to display */
    //        var set = r.set().push(
    //            r.rect(-30, -13, 62, 86).attr({"fill": "#fa8", "stroke-width": 2, r : "9px"}))
    //            .push(label);
    /* make the label show only on hover */
    //        set.hover(function(){ label.animate({opacity:1,"fill-opacity":1}, 500); }, function(){ label.animate({opacity:0},300); });

    //        tooltip = r.set()
    //            .push(
    //                r.rect(0, 0, 90, 30).attr({"fill": "#fec", "stroke-width": 1, r : "9px"})
    //            ).push(
    //                r.text(25, 15, "overlay").attr({"fill": "#000000"})
    //            );
    //        for(i in set.items) {
    //            set.items[i].tooltip(tooltip);
    //        };
    //	//            set.tooltip(r.set().push(r.rect(0, 0, 30, 30).attr({"fill": "#fec", "stroke-width": 1, r : "9px"})).hide());
    //        return set;
    //    };

  

    /* layout the graph using the Spring layout implementation */
    var layouter = new Graph.Layout.Spring(g);

    /* draw the graph using the RaphaelJS draw implementation */
    renderer = new Graph.Renderer.Raphael('canvas', g, width, height);

    redraw = function () {
        layouter.layout();
        renderer.draw();
    };
    hide = function (id) {
        g.nodes[id].hide();
    };
    show = function (id) {
        g.nodes[id].show();
    };
    //    console.log(g.nodes["kiwi"]);

}