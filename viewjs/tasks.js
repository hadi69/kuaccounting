﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-11-01: file created
*/
function view_tasks() {
    var WID = -1, WorkflowType = -1;
    var status = -1;
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var id = $.urlParam('id');
        var more = decodeURIComponent("WorkflowID=" + id);
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + "&more=" + more + appendTime();
        // alert(_url);
        $("#flexi").flexigrid
            ({
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell'},
                    { display: Lang.Workflow, name: 'WorkflowName', sortable: true},
                    { display: Lang.Title, name: 'Title', sortable: true, css_class: 'width-120 align-center' },
                    { display: Lang.Color, name: 'Color', sortable: true, css_class: 'color-cell', process: showColor },
                    { display: Lang.TaskState, name: 'State', sortable: true, css_class: 'width-120 align-center', process: _displayTaskStateEnum },
                    { display: Lang.NoStarting, name: 'NoStarting', sortable: true, css_class: 'width-100 align-center' },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, css_class: 'action-cell', process: function (celDiv, id) { editLink(celDiv, id, show); } }
                ],
                buttons:
                [
                    { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                    { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                    { separator: true },
                    { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                searchitems: null,
                sortname: "WorkflowName",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,

                height: gridHeight, resizable: gridResizable,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                }
            });
    }
    function _displayTaskStateEnum(celDiv, id) {
        var type = toInt($(celDiv).html(), 0);
        displayTaskStateEnum(celDiv, type);
    }
    function refresh() {
        var _url = 'service.asmx/GetList?table=Task&qd=task' + appendTime();
        var workflowTypeID = $('#WorkflowTypeID').val();
        if (workflowTypeID > 0) {
            var more = encodeURIComponent("WorkflowID=" + workflowTypeID);
            _url += '&more=' + more;
        }
        $("#flexi").flexOptions({ url: _url });
        $("#flexi").flexReload();

    }
    function showcallback(v, m, f) {
        var qry = (_Context.id == -1) ? 'taskinsert' : 'taskupdate';
        return newMastercallback(v, m, f, 'Task', qry, 'TaskViewer'
                , 'taskviewerupdate'
                , 'taskviewerinsert'
                , 'taskviewerdelete'
                , 'RoleID', null, show);
    }
    var showWaiting = false;
    function show(id, celDiv, successCallback) {
        var btns = getDefaultButtons();
        var workflowID = $('#WorkflowTypeID').val();
        var workflowName = $('#WorkflowTypeName').val();
        var more = '&WorkflowName=' + encodeURIComponent(workflowName) + '&WorkflowID=' + workflowID;
        var WfID = $.urlParam('id');
        var m = "&ID=" + WfID;
        var WfName = getServerValue("selectworlflow", "Workflow", m);
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + "&WorkflowID=" + WfID + "&WorkflowName=" + WfName + appendTime();
        if (workflowID > 0)
            _url += '&more=' + more;
        showPopup(_url, showcallback, btns, null, function () {
            $('#Name').focus();
            jscolor.init();
            if (successCallback)
                successCallback();
        });
    }
    // -------------------------
    // Startup code
    // -------------------------
    $("#bar").prepend(breadcrumb({
        pageInfo: {
            pageTitle: Lang.Task,
            parentTitle: Lang.Workflow,
            parentUrl: 'list.aspx?table=Workflow',
            grandParent: {
                title: Lang.MSys,
                url: 'menu/sys.aspx'
            }
}
    }));
    setClassSys();
    run();
}