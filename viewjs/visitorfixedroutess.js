﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

function view_visitorfixedroutess() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }

    function run(parameters) {
        var _url = 'service.asmx/GetList?table=VisitorFixedRoutes&qd=visitorfixedroutes&more=' + appendTime();
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.Visitor, name: 'VisitorName', sortable: true, css_class: "width-220 align-center" },
                    { display: Lang.NumOfCustomer, name: 'NumOfCustomers', sortable: true, css_class: "width-100 align-center" },
                    { display: Lang.Comments, name: 'Comments', sortable: true },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' }
                ],
                buttons:
                [
                   { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                   { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                   { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                sortname: "VisitorName",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight, resizable: gridResizable,
                singleSelect: false
            });
    }
    function showcallback(v, m, f) {
        _Context.table = 'VisitorFixedRoutes';
        var qry = (_Context.id == -1) ? 'visitorfixedroutesinsert' : 'visitorfixedroutesupdate';
        return newMastercallback(v, m, f, 'VisitorFixedRoutes', qry, 'VisitorFixedRoutesItem', 'visitorfixedroutesitemupdate', 'visitorfixedroutesiteminsert', 'visitorfixedroutesItemdelete', 'FGroupingID', '#subDetailForm', show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&ID=' + id + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }


    // ------------------------------
    // startup code
    // ------------------------------
    setClassDistribution();
    $("#bar").prepend(breadcrumb({
        title: _Context.title,
        grandParent: {
            title: Lang.MDistribution,
            url: 'menu/distribution.aspx'
        },
    }));

    run();
}