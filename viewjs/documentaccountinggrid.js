﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
92-11-10:
    - combined with purchaseinvoice
92-10-29: 
    - file created
*/
function view_documentaccountinggrid() {
    $("#tableItems").smartTable();
    fixFormButtonContainer();
    var documentNo = $('#No').val();
    replacePrintButton({
        primary: {
            text: Lang.Print,
            href: Router.Generate("print-documentaccounting", { params: { from: documentNo, to: documentNo }, query: { report: 'secondsimple', print: "true" } })

        },
        other: [
            {
                text: Lang.SimpleFormat,
                href: Router.Generate("print-documentaccounting", { params: { from: documentNo, to: documentNo }, query: { print: "true" } })
            }
            //{
            //    text: Lang.SimpleFormat,
            //    href: Router.Generate("print-documentaccounting", { params: { from: documentNo, to: documentNo }, query: { report: 'simple', print: "true" } })
            //}

        ]
    });
    var cnt = 0, mID = -1;
    function updateTotal() {
        var sumOwe = 0, sumDue = 0, sumDiscount = 0;
        $('.owe').each(function (index, value) {
            sumOwe += toFloat($(value).val(), 0);
        });
        $('.due').each(function (index, value) {
            sumDue += toFloat($(value).val(), 0);
        });
        $('#TotalBalance').val(numberWithCommas((sumOwe.toFixed(_Context.decimalPoints) - sumDue.toFixed(_Context.decimalPoints)).toFixed(_Context.decimalPoints)));
        $('#TotalOwe').val(numberWithCommas(sumOwe.toFixed(_Context.decimalPoints)));
        $('#TotalDue').val(numberWithCommas(sumDue.toFixed(_Context.decimalPoints)));
        updateSumOweDue();
    }
    function updateSumOweDue() {
        //var owe = 0, due = 0;
        //$(".sel").each(function () {
        //    if ($(this).attr('checked')) {
        //        var o = toFloat($(this).closest('tr').find('.owe').html(), 0);
        //        var d = toFloat($(this).closest('tr').find('.due').html(), 0);
        //        owe += o;
        //        due += d;
        //    }
        //});
        //$('#SumOwe').val(owe);
        //$('#SumDue').val(due);
    }
   
    function addRow(node) {
        // id="x" --> the validationEngine requires ID
        var readonly = node != null ? " readonly='readonly' " : "";
        var settingTax = "", settingVat = "";
        if (node == null) {
            settingTax = getSettingTax();
            settingVat = getSettingVat();
        }
        var row = '<tr>';

        // ColRowNr
        row += '<td data-th="' + Lang.ColRowNr + '" class="align-center row-number">' + (cnt + 1) + '</td>';

        // Person
        row += '<td data-th="' + Lang.Person + '">'
            + '<input type="hidden" id=sdi_' + cnt + ' name="ID" value="' + getNodeVal(node, "ID", -1) + '" class="id" />'
            + '<input type="text" id="pr_' + cnt + '" name="PersonnelName" value="' + getNodeVal(node, "PersonnelName") + '" class="personnelname form-control input-sm" />'
            + '<input type="hidden" name="PersonnelID" value="' + getNodeVal(node, "PersonnelID") + '" />'
            + '</td>';

        // Topic
        row += '<td data-th="' + Lang.Topic + '">'
            + '<input type="text" id="tr_' + cnt + '" name="TreePathName" value="' + getNodeVal(node, "TreePathName") + '" class="treepathname form-control input-sm" />'
            + '<input type="hidden" name="TreePath" value="' + getNodeVal(node, "TreePath") + '" />'
            + '</td>';

        // Debtor
        row += '<td data-th="' + Lang.Debtor + '">'
            + '<input type="text" id="ow_' + cnt + '" name="Owe" value="' + getNodeVal(node, "Owe") + '" class="owe form-control input-sm monetary validate[custom[integer]]" />'
            + '</td>';

        // Creditor
        row += '<td data-th="' + Lang.Creditor + '">'
            + '<input type="text" id="du_' + cnt + '" name="Due" value="' + getNodeVal(node, "Due") + '" class="due form-control input-sm monetary validate[custom[integer]]" />'
            + '</td>';

        // Comments
        row += '<td data-th="' + Lang.Comments + '">'
            + '<input type="text" id="cm_' + cnt + '" name="Comments" value="' + getNodeVal(node, "Comments") + '" class="form-control input-sm" />'
            + '</td>';

        // DocNo
        row += '<td data-th="' + Lang.DocNo + '">'
            + '<input type="text" id="dn_' + cnt + '" name="DocNo" value="' + getNodeVal(node, "DocNo") + '" class="form-control input-sm docno" />'
            + '</td>';

        // DocDate
        row += '<td data-th="' + Lang.DocDate + '">'
            + '<input type="text" id="dd_' + cnt + '" name="DocDate' + cnt + '" value="' + getNodeVal(node, "DocDate") + '" class="dateSelect DocDate form-control input-sm" />'
            + '</td>';

        // Delete row
        row += '<td class="align-center action-remove">'
            + '<a class="btn btn-danger btn-xs btn-block btn-remove remove" id="r_' + cnt + '">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left">' + Lang.ColDel + '</span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
                
        cnt++;
        return row;
    }


    // ------------------------------
    // startup code
    // ------------------------------
    mID = toInt($('#MasterID').val(), -1);
    dateSelect('.dateSelect');
 
    if ($("#Date").val() == '')
        $("#Date").val(_Context.today);

    if (_Context.level == 1)
        $('.project').hide();

    function handleEnter() {
        var lastRow = $('tr:last', '#tableItems');
        $('.DocDate').unbind('keydown');
        $('.DocDate', lastRow).keydown(function (e) {
            if (e.keyCode === 13) {
                addClick();
                //;return false;
            }
        });
    }

    // DetailForm
    if (hasValuePositive(mID)) {
        fillTable("get.aspx?op=GetXml&qd=documentaccountingitem&DocumentAccountingID=" + mID, '#tableItems', addRow);
        for (var i = cnt; i < 1; i++)
            $('#tableItems').append(addRow(null));
        if (_Context.level == 1) {
            $('.project').hide();
        }
        dateSelect('#tableItems .dateSelect');
        handleEnter();
        autoCompleteFree('Personnel', 'personnelselectauto', "#tableItems .personnelname", "#justfurfun"
            , tableAutoPostPack
            , function (data, i, n, value) {
                $('#PersonnelID').val('');
                return formatPersonnelItem(data);
            });
        autoComplete('Ward', 'wardauto', '#tableItems .projectname', '#LongLiveKurdistan', tableAutoPostPack);
        autoComplete('Trees', 'treesauto', '#tableItems .treepathname', '#Nazdar', function (event, data) {
            // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due
            tableAutoPostPack(event, data);
            var hasChild = data[5] && data[5] != 'False';
            if (hasChild) {
                $(event.target).next().val('');
                $(event.target).val('');
                alert(Lang.HasChild);
            }
            else {
                //$('#PeNeed').val(data[2]);
                //$('#PrNeed').val(data[3]);
                //$('#TopicCode').val(data[4]);
                //$('#TopicTitle').val(data[8]);
            }
        }, formatTreePath);
        bindCheckDocNo();
    }
    $('.owe, .due').blur(function () { updateTotal(); });
    function bindCheckDocNo() {
        $('.docno').unbind('blur').blur(
            function () {
                if (!checkDocNo($(this)))
                    $(this).addClass('invalid');
                else
                    $(this).removeClass('invalid');
            });
    }
    function checkDocNo(me) {
        var meID = me.attr('id');
        var tr = me.closest('tr');
        var docNo = me.val();
        var treePath = tr.find('.treepathname').next().val();
        var id = tr.find('.id').val();
        var owe = toFloat(tr.find('.owe').val(), 0);
        var due = toFloat(tr.find('.due').val(), 0);
        // alert(docNo + " - " + treePath + "  - " + id);
        // if (docNo == '')
        //   return;
        // OK = 1, NOK = -1
        // OKOwe = 1, NOKOwe = -1, OKDue = 2, NOKDue = -2, OKBoth = 3
        var oOrD = "";
        if (owe > 0) {
            oOrD = "&Owe=1";
        }
        else if (due > 0) {
            oOrD = "&Due=1";
        }
        var count = 0;
        if (docNo) {
            // the list is not saved yet. check for duplicate docNo here too
            var found = false;
            $('.docno', "#tableItems").each(function () {
                if (found)
                    return;
                if ($(this).val() == docNo && $(this).attr('id') != meID) {
                    var _owe = toFloat($(this).closest('tr').find('.owe').val(), 0);
                    var _due = toFloat($(this).closest('tr').find('.due').val(), 0);
                    if ((_owe > 0 && owe > 0) || (_due > 0 && due > 0))
                        found = true;
                }
            });
            if (found)
                count = 1;
            else {
                if (id)
                    count = toInt(getServerValue("getNumDocNoEx", "DocumentAccountingItems", oOrD + "&DocNo=" + docNo + "&ID=" + id), 0);
                else
                    count = toInt(getServerValue("getNumDocNo", "DocumentAccountingItems", oOrD + "&DocNo=" + docNo), 0);
            }
        }
        if (count > 0)
            alert(Lang.DuplicateDocNo);
        // banktreepath need to have DocNo
        var ok = toInt(getServerValue("checkbanktreepath", "Trees", "&TreePath=" + treePath), 0);
        //  var ok = toInt(getServerValue("checkbanktreepath2", "Trees", "&TreePath=" + treePath + "&DocNo=" + docNo), 0);
        // -- 0: No Bank, 1: Bank and DocNo is not OK, 2: Bank and DocNo OK
        //alert(ok);
        if (docNo == '' && ok == 1) {
            alert(Lang.BankNeedDocNo);
            me.focus();
            return false;
        }
        return true;
        // alert(Lang.BankNeedDocNoValid);
        //return false;
    }
    function removeRow($who) {
        var id = $who.attr('id'); // r_1
        id = id.substring(id.indexOf('_') + 1); // r_1  -->  1
        $('#tr_' + id).val(""); // TreePathName
        $('#tr_' + id).next().val(-1); // TreePath
        $who.parent().parent().hide();
        //$who.parent().parent().find(':input').removeClass();
        Growl.Notice(Lang.DeletedSuccessfully);
        updateTotal();
    }
    $('.remove').click(function () {
        removeRow($(this));
    });
    function addClick() {
        var table = $("#tableItems");
        $('tr:last', table).after(addRow(null));
        if (_Context.level == 1) {
            $('.project').hide();
        }
        Growl.Notice(Lang.RowAddedSuccessfully);
        var lastRow = $('tr:last', table);
        dateSelect('#tableItems tr:last .dateSelect');
        handleEnter();

        autoCompleteFree('Personnel', 'personnelselectauto', "#tableItems tr:last .personnelname", "#justfurfun"
           , tableAutoPostPack
           , function (data, i, n, value) {
               $('#PersonnelID').val('');
               return formatPersonnelItem(data);
           });
        autoComplete('Ward', 'wardauto', '#tableItems tr:last .projectname', '#LongLiveKurdistan', tableAutoPostPack);
        autoComplete('Trees', 'treesauto', '#tableItems tr:last .treepathname', '#Nazdar', function (event, data) {
            // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due
            tableAutoPostPack(event, data);
            var hasChild = data[5] && data[5] != 'False';
            var canHaveSubTree = data[12] && data[12] != 'False';
            if (hasChild || canHaveSubTree) {
                $(event.target).next().val('');
                $(event.target).val('');
                alert(Lang.HasChild);
            }
            else {
                //$('#PeNeed').val(data[2]);
                //$('#PrNeed').val(data[3]);
                //$('#TopicCode').val(data[4]);
                //$('#TopicTitle').val(data[8]);
            }
        }, formatTreePath);
        $('.remove', lastRow).click(function () {
            removeRow($(this));
        });
        $('.owe, .due', lastRow).blur(function () { updateTotal(); });
        commaInputs();
        monetaryKeys();
        bindCheckDocNo();
    }
    $('.add').click(addClick);

    insertValidation();
    monetaryInputsSet(null, '#Date');
    commaInputs();
    updateTotal();
}