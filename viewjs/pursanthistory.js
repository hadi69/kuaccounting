﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
92-11-08: file created
*/
function view_pursanthistory() {
    // ------------------------------
    // startup code
    // ------------------------------
    var personId = $("#PersonID").val();
    if (!personId) {
        personId = 'NULL';
    }
    function bindPursantHistory() {
        var treePath = getSettings('SalePursantTreePath');
        var _url = 'service.asmx/GetList?table=DocumentAccountingItems&qd=getpursanthistory&PersonID=' + personId + '&TreePath=' + treePath + appendTime();

        $("#scondFlexi").flexigrid
        (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.Date, name: 'DocumentAccountingDate', css_class: 'date-cell', sortable: true, process: capturePriceDate },
                    { display: Lang.Comments, name: 'Comments' , sortable: true },
                    { display: Lang.AmountPrice, name: 'Price', css_class: 'width-100 align-center', sortable: true, process: showMonetary, footer: 'sum' },
                    { display: Lang.DocumentAccountingNum, name: 'DocumentAccountingID', hide: true, process: displayVoucherID },
                    { display: Lang.DocumentAccountingNum, name: 'DocumentAccountingNum', css_class: 'width-70 align-center', sortable: true, process: displayVoucherNo }
                ],
                searchitems: null,
                sortname: "ID",
                sortorder: "desc",
                usepager: false,
                rpOptions: [20, 50, 1000, 1500, 3000], //allowed per-page values
                rp: 1500,
                useRp: false,
                rp: gridRp,
                showTableToggleBtn: false,
                width: gridWidth,
                height: 400,
                resizable: gridResizable,
                singleSelect: true,
                onSuccess: function (grid) {
                    showVoucherLinks();
                }
            }
        );
    }

    function displayVoucherID(celDiv, id) {
        $(celDiv).addClass('_vid');
    }
    function displayVoucherNo(celDiv, id) {
        $(celDiv).addClass('_vno');
    }

    function showVoucherLinks() {
        setTimeout(function () {
            $('#scondFlexi tr').each(function () {
                var _id = $('._vid', this).html();
                debugger;
                //alert(_id);
                if (_id && _id != '')
                    $('._vno', this).html("<a href='list.aspx?table=DocumentAccounting&id=" + _id + "'>" + $('._vno', this).html() + "</a>");
            });
        }, 500);
    }
    bindPursantHistory();
}