﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-11-11: file created
*/
function view_purchaserequests() {
    var startupID = -1;
    function doCommand(com, grid) {
        var isNew = com == Lang.GBNew;
        if (!isNew) {
            var btn = $(".fbutton span:contains(" + com + ")");
            if (btn.hasClass('new'))
                isNew = true;
        }
        if (isNew) {
            var more = '&Type=' + WorkflowTypeEnum.PurchaseRequest;
            var count = getServerValue('checkworkflow', 'Workflow', more);
            if (count == 0 || count == null) {
                displayError(Lang.WorkflowM1);
                return;
            }
        }
        if (isNew && com != Lang.GBNew) {
            var desiredEntryTaskID = $(".fbutton span:contains(" + com + ")").attr('class').substring('new '.length);
            purchaserequestshow(-1, null, null, desiredEntryTaskID);
        }
        else
            generalCommand(com, grid, purchaserequestshow);
    }
    function run() {
        autoComplete('TasK', 'purchaserequestphase', "#fTaskName", "#fTaskID");

        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell'},
                    { display: Lang.RequestNumber, name: 'NOSerial', sortable: true, css_class: 'width-100 align-center'},
                    { display: Lang.RequestDate, name: 'RequestDate', sortable: true, css_class: 'date-cell'},
                    { display: Lang.ActorName, name: 'PersonnelName', sortable: true},
                    { display: Lang.Confirm, name: 'Accepted', sortable: true, process: showBool, css_class: 'width-40 align-center', hide:true },
                    { display: Lang.PreStatus, name: 'PreTaskName', sortable: true, css_class: 'width-80 align-center' },
                    { display: Lang.Status, name: 'TaskName', sortable: true, css_class: 'width-100 align-center' },
                    { display: Lang.Purchase, name: 'PurchaseTitle', css_class: 'width-80 align-center _ptitle' },
                    { display: '', name: 'PurchaseID', css_class: '_pid', hide: true },
                    { display: '', name: 'WorkflowColor', css_class: 'WorkflowColor', hide: true },
                    { display: Lang.NumPrints, name: 'NumPrints', sortable: true, css_class: 'width-100 align-center' },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, purchaserequestshow); }, css_class: 'action-cell' }

                ],
                buttons: getTaskGridButtons(WorkflowTypeEnum.PurchaseRequest, doCommand, "storeforms", "purchaserequest"),
                searchitems: null,
                sortname: "RequestDate",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,

                height: gridHeight, resizable: gridResizable,
                singleSelect: true,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    purchaserequestshow(id);
                },
                onSuccess: function (grid) {
                    postProcessGrid();
                    if (startupID != -1) {
                        purchaserequestshow(startupID);
                        startupID = -1;
                    }
                }
            });
    }
    function postProcessGrid() {
        setTimeout(function () {
            colorTasks();
            showLinks();
        }, 500);
    }
    

    function showLinks() {
        $('#flexi tr').each(function () {
            var _id = $('._pid div', this).html();
           // alert(_id);
            if (_id && _id != '')
                $('._ptitle div', this).html("<a href='list.aspx?table=Purchase&id=" + _id + "'>" + $('._ptitle', this).find('div').html() + "</a>");
        });
    }

    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['stores,purchaserequest']);
    checkLevel(2);
    $("#bar").prepend(breadcrumb({pageInfo: {pageTitle: Lang.PurchaseRequest, parentTitle: Lang.MStore, parentUrl: 'menu/store.aspx'} }));
    setClassStore();
    dateSelect('.dateSelect');
    startupID = $.urlParam('id');
    if (!startupID)
        startupID = -1;
    
    run();
}