﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />
function view_customerslimit() {
    $("#tableServices").smartTable();
    var cnt = 0, mID = -1;
    function addService(node) {
        var row = '<tr>';

        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">'
            + (cnt + 1)
            + '</td>';

        row += '<td data-th="' + Lang.ServicesThatDoNotsell + '">'
            + '<input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="text" id="s_' + cnt + '" name="ServiceName" value="'
            + getNodeVal(node, "ServiceName") + '" class="servicename form-control input-sm" />'
            + '<input type="hidden" name="ServicePriceID" value="' + getNodeVal(node, "ServicePriceID") + '" />'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a id="r_' + cnt + '" class="btn btn-danger btn-xs btn-block btn-remove removeService" href="#">'
            + '<span class="visible-xs-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }
    function autoPostPack(event, data, formatted) {
        $(event.target).next().val(data[0]); // ID
    }
    function removeService($who) {
        var id = $who.attr('id'); // r_1
        id = id.substring(2); // r_1  -->  1
        //alert(id);
        $('#s_' + id).val(""); // 
        $('#s_' + id).next().val(-1); // 
        $who.parent().parent().hide();
        Growl.Notice(Lang.DeletedSuccessfully);

    }

    function bindAdd() {
        $('.addService').click(function () {
            var table = $('#tableServices');
            var tableID = table.attr("id");
            $('tr:last', table).after(addService(null));
            Growl.Notice(Lang.RowAddedSuccessfully);
            var lastRow = $('tr:last', table);
            autoComplete('ServicePrice', 'servicepriceauto', "#tableServices tr:last .servicename", "#justfurfun"
               , autoPostPack
               , function (data, i, n, value) {
                   return data[1];
               });
            $('.removeService').click(function () {
                removeService($(this));
            });
        });
    }

    function bindRemove() {
        $('.removeService').click(function () {
            removeService($(this));
        });
    }
    //----------------
    // startup code
    //------------------
    mID = toInt($('#MasterID').val(), -1);
    fillTable("get.aspx?op=GetXml&qd=serviceslimit&CustomersLimitID=" + mID, '#tableServices', addService);
    function doMore() {
        var visitorplandaysid = $('#VisitorPlanDaysID').val();
        autoComplete('Personnel', 'visitorplanpersonauto', "#CustomerName", "#CustomerID", null, null, null, null
            , 'get.aspx?op=Auto&table=Personnel&qd=visitorplanpersonauto&VisitorPlanDaysID=' + visitorplandaysid + appendTime());
        autoComplete('ServicePrice', 'servicepriceauto', "#tableService tr:last .servicename", "#justfurfun"
           , autoPostPack
           , function (data, i, n, value) {
               return data[1];
           });
        bindAdd();
        bindRemove();
        commaInputs('#subDetailForm');
        //monetaryInputsSet('#subDetailForm');
        insertValidation('#subDetailForm');
    }
    doMore();
    $('#Credit').focus();
    $('#CustomerName').focus();
}