﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
93-04-08: file created
*/
function view_accountcheckout() {
    checkAccess(['sales,accountcheckout']);
    checkLevel(2);

    function doCommand(com, grid) {
        generalCommand(com, grid, function() {});
    }
    var sum = 0;
    var firstFlexi = true;
    function gridLoad() {
        var _url = 'service.asmx/GetQueryList?table=StoreDraft&qd=getstoredraftitemsforaccountcheckout&LoadOwnerID=' + $('#LoadOwnerID').val() + '&FromDate=' + $('#FromDate').val() + '&ToDate=' + $('#ToDate').val() + appendTime();
        if (firstFlexi) {
            firstFlexi = false;
            $("#flexiSales").flexigrid
            (
                {
                    url: _url,
                    dataType: 'xml',
                    colModel:
                    [
                        { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                        { display: '', name: 'ID', sortable: true, hide: true },
                        { display: Lang.No, name: 'NoSerial', sortable: true, css_class: 'width-50' },
                        { display: Lang.FactorDate, name: 'FactorDate', sortable: true, css_class: 'date-cell' },
                        { display: '', name: 'StoreServiceID', sortable: true, hide: true },
                        { display: Lang.Commodity, name: 'StoreServiceName', sortable: true, css_class: 'width-200' },
                        { display: '', name: 'UnitID', sortable: true, hide: true },
                        { display: Lang.Units, name: 'UnitName', sortable: true, css_class: 'width-100' },
                        { display: Lang.Value, name: 'DeliveredAmount', sortable: true, css_class: 'width-150' },
                        { display: Lang.UnitCurrencyConverter, name: 'UnitCurrencyConverter', sortable: true, css_class: 'width-100' },
                        { display: Lang.Price, name: 'Price', sortable: true, process: showMonetary, css_class: 'width-100' },
                        { display: Lang.TotalPrice, name: 'TotalPrice', process: sumPrice, sortable: true }
                    ],
                    searchitems: null,
                    sortname: "FactorDate",
                    sortorder: "asc",
                    usepager: true,
                    //title: _Context.title,
                    useRp: true,
                    rp: gridRp,
                    showTableToggleBtn: _options.gridToggle,
                    width: _options.gridWidth,
                    height: 300,
                    singleSelect: false,
                    onSuccess: function (grid) {
                        $('#Total').val(numberWithCommas(toMonetary(sum)));
                    }
                }
            );
        } else {
            $('#flexiSales').flexOptions({ url: _url });
            $('#flexiSales').flexReload();
           // $('#TotalPrice').val(sum);
        }
    }

    function sumPrice(celDiv, id, text) {
        $(celDiv).html(numberWithCommas(toMonetary(text)));
        sum += toFloat(text, 0);
    }

// ------------------------------
    // startup code
    // ------------------------------
    gridLoad();
    $('#Search').click(function () {
        sum = 0;
        gridLoad();
    });

    autoCompleteFree('Personnel', 'personnelselectauto', '#PersonName', '#PersonID', null
        , function (data, i, n, value) {
            //$('#BuyerID').val('');
            return formatPersonnelItem(data);
        });
    autoCompleteFree('Personnel', 'personnelselectauto', '#ShipmentPersonName', '#ShipmentPersonID', null
    , function (data, i, n, value) {
        //$('#BuyerID').val('');
        return formatPersonnelItem(data);
    });
    autoCompleteFree('Personnel', 'personnelselectauto', '#LoadOwnerName', '#LoadOwnerID', null
    , function (data, i, n, value) {
        //$('#BuyerID').val('');
        return formatPersonnelItem(data);
    });

    autoCompleteFree('Personnel', 'personnelselectauto', '#UnloadPersonName', '#UnloadPersonID', null
    , function (data, i, n, value) {
        //$('#BuyerID').val('');
        return formatPersonnelItem(data);
    });

    setDefaultByTitle('Trees', 'settingtreesauto', '#ShipmentTreePathName', '#ShipmentTreePath', 'AccountCheckoutShipmentTreePath');
    setDefaultByTitle('Trees', 'settingtreesauto', '#UnloadTreePathName', '#UnloadTreePath', 'AccountCheckoutUnloadTreePath');


    autoCompleteTreePath('#ShipmentTreePathName', '#ShipmentTreePath');
    autoCompleteTreePath('#UnloadTreePathName', '#UnloadTreePath');

    $('#PercentCommission').blur(function() {
        var percent = toFloat($(this).val(), 0);
        var totalPrice = toFloat($('#Total').val(), 0);
        var commission = (percent * totalPrice) / 100;
        $('#Commission').val(numberWithCommas(toMonetary(commission)));
    });

    $('#DoAccountCheckout').click(function () {
        if (toFloat($('#Total').val())) {
            var more = '&LoadOwnerID=' + $('#LoadOwnerID').val() + '&FromDate=' + $('#FromDate').val() + '&ToDate=' + $('#ToDate').val() + '&Total=' + toFloat($('#Total').val(), 0)
                + '&Commission=' + toFloat($('#Commission').val(), 0) + '&ShipmentPrice=' + toFloat($('#ShipmentPrice').val(), 0) + '&UnloadPrice=' + toFloat($('#UnloadPrice').val(), 0)
                + '&ShipmentPersonID=' + $('#ShipmentPersonID').val() + '&ShipmentTreePath=' + $('#ShipmentTreePath').val() + '&UnloadPersonID=' + $('#UnloadPersonID').val() + '&UnloadTreePath=' + $('#UnloadTreePath').val();
            //alert(more);
            var ok = getServerValue('accountcheckoutdocument', 'DocumentAccounting', more);
            if (ok != -1 && ok != null) {
                Growl.Notice(Lang.Success);
            } else {
                Growl.Notice(Lang.Failed);
            }
        } else {
            alert(Lang.AccountCheckoutMsg);
        }
    });

    dateSelect('.dateSelect');
    $('#ShipmentPrice, #UnloadPrice, #Commission').keyup(function () {
        $(this).val(numberWithCommas(stripCommas($(this).val())));
    });
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.AccountCheckout, parentTitle: Lang.SellAndBuy, parentUrl: 'menu/sales.aspx' } }));
    setClassSales();
}