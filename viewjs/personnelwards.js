﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-11-01: file created
*/
function view_personnelwards() {
    var FID = -1;
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var id = $.urlParam('fid');
        var more = decodeURIComponent("FishID=" + id);
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + "&more=" + more + appendTime();
        // alert(_url);
        $("#flexi").flexigrid
            ({
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width:20, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: 'FishID', name: 'FishID', sortable: false, hide: true },
                    { display: Lang.Person, name: 'PersonnelID', sortable: true, hide:true },
                    { display: Lang.Ward, name: 'WardName', sortable: true, css_class: 'width-300 align-center' },
                    { display: Lang.WorkDays, name: 'WorkDays', sortable: true, css_class: 'width-500 align-center' },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show) } }
                ],
                buttons:
                [
                    { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                    { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                    { separator: true },
                    { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                searchitems: null,
                sortname: "ID",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,

                height: gridHeight, resizable: gridResizable,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                }
            });
    }
    function showcallback(v, m, f) {
        return newShowcallback(v, m, f, 'personnelwardupdate', 'personnelwardinsert', _Context.table, null, show);
    }
    var showWaiting = false;
    function show(id, celDiv, successCallback) {
        var btns = getDefaultButtons();
        var pid = $.urlParam('pid');
        FID = $.urlParam('fid');
        var m = "&ID=" + pid;
        var PName = getServerValue("selectpersonname", "Personnel", m);
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + "&FishID=" + FID + '&PersonnelID=' + pid + '&PersonnelName=' + PName + appendTime();
        showPopup(_url, showcallback, btns, null, function () {
            $('#WardName').focus();
            jscolor.init();
            if (successCallback)
                successCallback();
        });
    }
    // -------------------------
    // Startup code
    // -------------------------
    $('#ptitle').html("<a href='menu/salary.aspx'>" + Lang.MSalary + "</a> :: <a href='list.aspx?table=Fish'>" + Lang.Fish + "</a> ::" + _Context.title);
    setClassManager();
    run();
}