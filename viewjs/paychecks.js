﻿
/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
92-10-19: file created
*/
function view_paychecks() {
    function doCommand(com, grid) {
        if (com == Lang.ShowReceived) //
        {
            if ($('.showReceived').hasClass('glyphicon-ok')) {
                $('.showReceived').removeClass('glyphicon glyphicon-ok');
            } else {
                $('.showReceived').toggleClass('glyphicon glyphicon-ok');
            }
            var more = '__PrOgrAmconD__ AND Status NOT IN(1) AND (IsShop=0 OR IsShop IS NULL)';
            if ($('.showReceived').hasClass('glyphicon-ok')) {
                more = '__PrOgrAmconD__  AND (IsShop=0 OR IsShop IS NULL)';
            }
            var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + '&more=' + more + appendTime();
            $("#flexi").flexOptions({ url: _url });
            $('#fRefresh').click();
            return;
        }
        generalCommand(com, grid, show, _Context.title, "ID", "Status=0^CheckStatusNew^1^CheckStatusReceived^2^CheckStatusRefused^3^CheckStatusSleep^4^CheckStatusExpend^5^CheckStatusSleepReceive");
    }

    function showSumAmount() {
        var val = getServerValueFiltered("paychecksumamount", "PayCheck");
        if (!val)
            val = 0;
        $('#SumAmount').html(numberWithCommas(toMonetary(val, _Context.today)));
    }
    function run() {
        var more = '__PrOgrAmconD__ AND Status NOT IN(1) AND (IsShop=0 OR IsShop IS NULL)';
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + '&more=' + more + appendTime();
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'center', hide: true },
                    { display: Lang.Serial, name: 'CustomerStatus', sortable: true, hide: true, process: displayCustomerStatus },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.BehindNum, name: 'No', sortable: true, css_class: 'width-100' },
                    { display: Lang.BankName, name: 'BankName', sortable: true, css_class: 'width-200' },
                    { display: Lang.Reciever, name: 'ReceiverName', sortable: true, css_class: 'width-200' },
                    { display: Lang.DueDate, name: 'DueDate', sortable: true, css_class: 'date-cell' },
                    { display: Lang.Amount, name: 'Amount', sortable: true, process: showMonetary, css_class: 'width-70 align-center', footer: 'sum' },
                    { display: Lang.Status, name: 'Status', sortable: true, process: displayStatus, css_class: 'width-60 align-center' },
                    { display: Lang.BankAccountCheckName, name: 'BankAccountCheckName', hide: true },
                    { display: Lang.Serial, name: 'Serial', sortable: true },
                  //{ display: Lang.ColEdit, name: 'Edit', width: 40, sortable: false, align: 'left', process: function (celDiv, id) { editLink(celDiv, id, show) } },
                    { display: Lang.ColOperation, name: 'Edit', sortable: false, hide: hideColumn("bank", "paycheck", "edit"), process: myEditLink, css_class: 'action-cell' },
                    { display: Lang.ColDel, name: 'Del', sortable: false, process: delLink, hide: hideColumn("bank", "paycheck", "delete"), css_class: 'action-cell' }
                ],
                buttons:
                [
                    { name: Lang.GBNew, bclass: 'new', onpress: doCommand, onaccess: hasAccess("bank", "paycheck", "new") },
                   { name: Lang.GBDel, bclass: 'delete', onpress: doCommand, onaccess: hasAccess("bank", "paycheck", "delete") },
                   { name: Lang.GBPrint, bclass: 'print', onpress: doCommand, onaccess: hasAccess("bank", "paycheck", "print") },
                   { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand, onaccess: hasAccess("bank", "paycheck", "excel") },
                   { name: Lang.ShowReceived, bclass: 'showReceived', onpress: doCommand }
                ],
                searchitems: null,
                sortname: "DueDate",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,

                height: gridHeight, resizable: gridResizable,
                singleSelect: true,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                },
                onSuccess: function(grid) {
                    restoreSelectedRows('#flexi');
                    if (urlId) {
                        $('#row' + urlId).addClass('trSelected');
                    }
                }
            });
        showSumAmount();
        _Context.filterPostBack = showSumAmount;
    }
    function myEditLink(celDiv, id) {
        $(celDiv).html('<img src="img/edit.gif" />');
        $(celDiv).click(function (event) {
            var tr = $(celDiv).closest('tr');
            var status = getStatus(getColClass(tr, 9), 2);
            var customerStatus = $('._cstatus', tr).html();
            var statusName = Lang.ColToCustomer;
            if (customerStatus == 'True') {
                statusName = Lang.CheckStatusFund;
            }
            $('#flexi tr').removeClass('trSelected');
            tr.addClass('trSelected');
            showMenu({
                parent: this, width: '100px',
                cmds: [
                          [Lang.ColEdit, 'Edit']
                        , [statusName, status == '0' ? 'ToCustomer' : '']
                        , [Lang.ColReceive, status == '0' ? 'Receive' : '']
                        , [Lang.ColRefuse, status == '0' ? 'Refuse' : '']
                      ],
                callback: function (cmd) {
                    if (cmd == 'Edit')
                        show(id);
                    else if (cmd == 'Receive')
                        showReceive(id);
                    else if (cmd == 'Refuse')
                        showRefuse(id);
                    else if (cmd == 'ToCustomer') {
                        if (customerStatus == 'True') {
                            runQuery('paychecktocustomer', 'paycheck', 'CustomerStatus=0&PaycheckID=' + id);
                            $('#fRefresh').click();
                        } else {
                            runQuery('paychecktocustomer', 'paycheck', 'CustomerStatus=1&PaycheckID=' + id);
                            $('#fRefresh').click();
                        }
                    }
                }
            });
        });
    }
    function displayStatus(celDiv, id) {
        var _mode = parseInt($(celDiv).html(), 13);
        var customerStatus = $('._cstatus').eq(-1).html();
        displayPayCheckStatus(celDiv, _mode, customerStatus);
    }
    function displayCustomerStatus(celDiv, id) {
        $(celDiv).addClass('_cstatus');
    }
    function showcallback(v, m, f) {
        restoreSelectedRows('#flexi');
        if (v == 'OK' && !checkProgramStatus())
            return false;
        if (v == "Print") {
            var template = getServerValue("bankprinttemplate", "BankPrint", "&ID=" + f.ID);
            if (template && template != '')
                return generalPrintTemplate('#subDetailForm', template);
            return generalPrint(v, m, f, '#subDetailForm');
        }
        _Context.priceDate = f.PayDate;
        var qry = (_Context.id == -1) ? 'paycheckinsert' : 'paycheckupdate';
        var extraQ = 'paycheckinsertdocument';
        return newMastercallback(v, m, f, 'PayCheck', qry, 'PayCheckItems', 'paycheckitemsupdate', 'paycheckitemsinsert', 'paycheckitemsdelete', 'TreePaths', '#subDetailForm', show, null, extraQ);
    }
    function show(id, celDiv, successCallback) {
        saveTrSelected(id);
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        if (id == -1)
            _url += "&oweD=1";
        showPopup(_url, showcallback, getDefaultButtons(true, 'bank', "paycheck"), null, successCallback, true);
    }
    function showReceive(id) {
        saveTrSelected(id);
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + '&setstatus=' + CheckStatusEnum.Received + '&dueD=1' + appendTime();
        showPopup(_url, showcallback, getButtons(), null, null, true);
    }
    function showRefuse(id) {
        saveTrSelected(id);
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + '&setstatus=' + CheckStatusEnum.Refused + '&dueD=1' + appendTime();
        showPopup(_url, showcallback, getButtons(), null, null, true);
    }
    function saveTrSelected(id) {
        $('.trSelected').removeClass('trSelected');
        $('#row' + id).addClass('trSelected');
        saveSelectedRows('#flexi');
        $('.trSelected').removeClass('trSelected');
    }
    // ------------------------------
    // startup code
    // ------------------------------
    var urlId = $.urlParam('id');
    dateSelect('.dateSelect');
    checkAccess(['bank,paycheck']);
    setClassBank();
    $("#bar").prepend(breadcrumb({pageInfo: {pageTitle: Lang.PayCheck, parentTitle: Lang.BankAndCheck, parentUrl: 'menu/bank.aspx'} }));
    run();
}