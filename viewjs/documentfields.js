﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-11-23: file created
*/
function view_documentfields() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + "&more=" + encodeURIComponent("(__PrOgrAmconD__) AND Type=3") + appendTime();
        //alert(_url);
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: true, align: 'center', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeileNummer', sortable: true, css_class: 'row-cell' },
                    { display: Lang.Title, name: 'Name', sortable: true, css_class: 'width-200' },
                    { display: Lang.headline, name: 'Display', sortable: true, css_class: 'width-200' },
                    { display: Lang.Value, name: 'Value', sortable: true, hide: true },
                    { display: Lang.Value, name: 'DisplayValue', sortable: true },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' }
                    //,                    { display: Lang.ColDel, name: 'Del', width: 40, sortable: false, align: 'left', process: delLink }
                ],
                buttons:
                [
                    //{ name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                    { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                    { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand },
                    { separator: true }
                ],
                sortname: "Display",
                sortorder: "asc",
                usepager: true,
                //title: _Context.title,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,

                height: gridHeight, resizable: gridResizable,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                }
            }
            );
    }
    function showcallback(v, m, f) {
        return generalShowcallback(v, m, f, 'reportfieldupdate', 'reportfieldinsertupdate', "ReportFields", null, show);
    }

    function show(id) {
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&view=documentfield&id=' + id + '&Type=3' + appendTime();
        showPopup(_url, showcallback, getButtons());
    }

    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['salary,documentfields']);
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.DocumentFields, parentUrl: 'menu/salary.aspx', parentTitle: Lang.MSalary } }));
    setClassSalary();
    run();
}