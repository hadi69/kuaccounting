﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
93-01-12: file created
*/
function view_unitconverters() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', width: 20, sortable: false, align: 'left' },
                    { display: Lang.SrcUnitsName, name: 'SrcUnitsName', width: 180, sortable: true, align: 'left' },
                    { display: Lang.DestUnitsName, name: 'DestUnitsName', width: 80, sortable: true, align: 'left' },
                    { display: Lang.Scale, name: 'Scale', width: 220, sortable: true, align: 'left' },
                    { display: Lang.Comments, name: 'Comments', width: 220, sortable: true, align: 'left' },
                    { display: Lang.Enabled, name: 'Enabled', width: 40, sortable: true, align: 'left', process: showBool },
                    { display: Lang.ColEdit, name: 'Edit', width: 40, sortable: false, align: 'left', process: function (celDiv, id) { editLink(celDiv, id, show) } },
                ],
                buttons:
                [
                    { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                    { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                    { separator: true },
                    { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                    { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                sortname: "ID",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight,
                singleSelect: false
            }
            );
    }
    function showcallback(v, m, f) {
        return newShowcallback(v, m, f, 'unitconverterupdate', 'unitconverterinsert', _Context.table, null, show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    $('#ptitle').html("<a href='menu/basics.aspx'>" + Lang.MBasics + "</a> :: " + _Context.title);
    setClassBasic();
    run();
}