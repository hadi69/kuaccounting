﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-12-04: file created
*/
function view_service() {
    makeChosen();
    $("#tableServicePrice").smartTable();
    $("#tableUnits").smartTable();
    $("#tableGroup").smartTable();

    var cnt = 0, cnt2 = 0, cnt3 = 0, cnt4 = 0, cnt5 = 0, cnt6 = 0, cnt7 = 0, cnt8 = 0, cnt9 = 0, cntSV = 0;
    var mID = -1;

    function updateVolume(was) {
        var id = $(was).attr('id');
        id = id.substring(id.indexOf('_', 0) + 1); // len1_90  -->  90
        var len = toFloat($('#len1_' + id).val(), 0);
        var width = toFloat($('#wd1_' + id).val(), 0);
        var height = toFloat($('#hgt1_' + id).val(), 0);
        var vol = len * width * height;
        $('#vl1_' + id).val((vol).toFixed(_Context.decimalPoints)); // total
    }
    function updateUVolume(was) {
        var id = $(was).attr('id');
        id = id.substring(id.indexOf('_', 0) + 1); // len1_90  -->  90
        var len = toFloat($('#ulen1_' + id).val(), 0);
        var width = toFloat($('#uwd1_' + id).val(), 0);
        var height = toFloat($('#uhgt1_' + id).val(), 0);
        var vol = len * width * height;
        $('#uvl1_' + id).val((vol).toFixed(_Context.decimalPoints)); // total
    }

    // ---------------------
    // Add Rows
    // ---------------------
    function addServicePrice(node) {
        //alert(node);
        var row = '<tr>';
        row += '<td class="row-cell" data-th=" ' + Lang.ColRowNr + '">' + (cnt6 + 1)
        + '<input type="hidden" name="ID5" class="servicepriceid" value="' + getNodeVal(node, "ID", -1) + '" />'
        + '</td>';

        if (!_Context.isAccountingVer) {
            row += '<td>'
            + '<input type="text" id="smn_' + cnt6 + '" name="StartDatesp' + cnt6 + '" value="' + getNodeVal(node, "StartDatesp")
            + '" class="form-control input-sm dateSelect StartDatesp" />'
            + '</td>';
        }

        row += '<td class="pricing" data-th="' + Lang.BuyPrice + '">'
            + '<input type="text" id="fb1_' + cnt6 + '" name="FreeBase" value="' + numberWithCommas(getNodeVal(node, "FreeBase"))
            + '" class="monetary validate[custom[integer]] FreeBase form-control input-sm align-center" />'
            + '</td>';


        row += '<td class="pricing" data-th="' + Lang.ProfitPercent + '">'
            + '<div class="input-group input-group-sm">'
            + '<input type="text" id="prf_' + cnt6 + '" name="ProfitPercent" value="' + numberWithCommas(getNodeVal(node, "ProfitPercent"))
            + '" class="validate[custom[number]] form-control align-center ProfitPercent" />'
            + '<span class="input-group-addon">%</span>'
            + '</div>'
            + '</td>';
        row += '<td class="nopricing" data-th="' + Lang.TheLPBP + '" class="align-center acc">'
            + '<input type="text" id="bp_' + cnt6 + '" name="TheLPBP" value="'
            + getNodeVal(node, "TheLPBP") + '" class="acc TheLPBP monetary validate[custom[integer]] form-control input-sm align-center" />'
            + '</td>';

        row += '<td class="nopricing" data-th="' + Lang.TheLPSP + '" class="align-center acc">'
           + '<input type="text" id="sp_' + cnt6 + '" name="TheLPSP" value="'
           + getNodeVal(node, "TheLPSP") + '" class="acc TheLPSP monetary validate[custom[integer]] form-control input-sm align-center" />'
           + '</td>';

        row += '<td data-th="' + Lang.DefaultDiscount + '">'
            + '<input type="text" id="DD_' + cnt6 + '" name="DefaultDiscount" value="'
            + numberWithCommas(getNodeVal(node, "DefaultDiscount")) + '" class="DefaultDiscount percent monetary validate[custom[integer]] form-control input-sm align-center" />'
            + '</td>';

        row += '<td  class="hidden" data-th="' + Lang.Model + '">'
            + '<input type="text" id="m1_' + cnt6 + '" name="ModelName" value="' + getNodeVal(node, "ModelName")
            + '" class="modelname form-control input-sm align-center" />'
            + '<input type="hidden" name="Model" value="' + getNodeVal(node, "Model") + '" />'
            + '</td>';

        row += '<td class="align-center action-remove hidden">'
        + '<a id="r6_' + cnt6 + '" class="btn btn-danger btn-xs btn-block btn-remove removeServicePrice" href="#">'
        + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
        + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
        + '</a>'
        + '</td>';

        row += '</tr>';
        cnt6++;
        return row;
    }
    //--------------------------------------
    function addUnits(node) {
        var row = '<tr>';

        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">' + (cnt8 + 1)
            + '<input type="hidden" name="ID7" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '</td>';

        row += '<td data-th="' + Lang.UnitsName + '">'
            + '<input type="text" id="un_' + cnt8 + '" name="UnitsName" value="' + getNodeVal(node, "UnitsName") + '" class="unitsname form-control input-sm" />'
            + '<input type="hidden"  name="UnitsID" value="' + getNodeVal(node, "UnitsID") + '" />'
            + '</td>';

        row += '<td data-th="' + Lang.CurrencyConverter + '">'
            + '<input type="text" name="CurrencyConverter"  value="' + getNodeVal(node, "CurrencyConverter") + '" class="form-control input-sm align-center" />'
            + '</td>';

        row += '<td data-th="' + Lang.BarCode + '">'
            + '<div class="input-group input-group-sm">'
            + '<input type="text" id="ubar_' + cnt8 + '" name="UnitBarCode" value="' + getNodeVal(node, "UnitBarCode")
            + '" class="form-control align-center UnitBarCode" />'
            + '</div>'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a id="ru_' + cnt8 + '" class="btn btn-danger btn-xs btn-block btn-remove removeunits" href="#">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt8++;
        return row;
    }
    //--------------------------------------------
    function addGroup(node) {
        var row = '<tr>';

        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">' + (cnt9 + 1) + '</td>';

        row += '<td data-th="' + Lang.Groups + '">'
            + '<input type="hidden" name="ID8" value="' + getNodeVal(node, "GroupingID", -1) + '" />'
            + '<input type="text" id="g_' + cnt9 + '" name="GroupingName" value="'
            + getNodeVal(node, "GroupingName") + '" class="groupingname form-control input-sm" />'
            + '<input type="hidden" name="GroupingID" value="' + getNodeVal(node, "GroupingID") + '" />'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a id="r_' + cnt9 + '" class="btn btn-danger btn-xs btn-block btn-remove removeGroup" href="#">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt9++;
        return row;
    }
    //-------------------------------------------

    function addNormal(node) {
        var row = '<tr>';
        if (node) {
            row += '<td>' + (cnt2 + 1) + '</td><td><input type="hidden" name="ID" value="' + node.attr("ID") + '" /><input type="text" id="req2_' + cnt2 + '" name="KitName" value="' + node.attr("KitName") + '" class="kitname" /><input type="hidden" name="KitID" value="' + node.attr("KitID") + '" /></td>';
            row += '<td><input type="text" id="g1_' + cnt2 + '" name="GenderName" value="' + node.attr("GenderName") + '" class="gendername narrow1" /><input type="hidden" name="GenderID" value="' + node.attr("GenderID") + '" /></td>';
            row += '<td><input type="text" id="n1_' + cnt2 + '" name="StartAge" value="' + node.attr("StartAge") + '" class="validate[custom[number]] narrow startage" /></td>';
            row += '<td><select id="ft1_' + cnt2 + '" name="Kind" style="width:80px;min-width:80px">';
            row += '    <option value="0" ' + (node.attr("Kind") == '0' ? 'selected=selected' : '') + '>' + Lang.KindDay + '</option>';
            row += '    <option value="1" ' + (node.attr("Kind") == '1' ? 'selected=selected' : '') + '>' + Lang.KindMonth + '</option>';
            row += '    <option value="2" ' + (node.attr("Kind") == '2' ? 'selected=selected' : '') + '>' + Lang.KindYear + '</option>';
            row += '</select></td>';
            row += '<td><input type="text" id="mi1_' + cnt2 + '" name="Minimum" value="' + node.attr("Minimum") + '" class="narrow15 Minimum" /></td>';
            row += '<td><input type="text" id="ma2_' + cnt2 + '" name="Maximum" value="' + node.attr("Maximum") + '" class="narrow15 Maximum" /></td>';
            row += '<td><input type="text" id="c1_' + cnt2 + '" name="NComments" value="' + node.attr("NComments") + '" class="narrow15 NComments" /></td>';
            row += '<td><input type="checkbox" id="ifr1_' + cnt2 + '" name="Enabled1' + cnt2 + '" ' + (node.attr("Enabled1") == '1' || node.attr("Enabled1") == 'True' ? 'checked=checked' : '') + ' /></td>';
        }
        else {
            row += '<td>' + (cnt2 + 1) + '</td><td><input type="hidden" name="ID" value="-1" /><input type="text" id="req2_' + cnt2 + '" name="KitName" value="" class="kitname" /><input type="hidden" name="KitID" value="" /></td>';
            row += '<td><input type="text" id="g1_' + cnt2 + '" name="GenderName" value="" class="gendername narrow1" /><input type="hidden" name="GenderID" value="" /></td>';
            row += '<td><input type="text" id="n1_' + cnt2 + '" name="StartAge" value="" class="validate[custom[number]] narrow startage" /></td>';
            row += '<td><select id="ft1_' + cnt2 + '" name="Kind" style="width:80px;min-width:80px">';
            row += '    <option value="0">' + Lang.KindDay + '</option>';
            row += '    <option value="1">' + Lang.KindMonth + '</option>';
            row += '    <option value="2">' + Lang.KindYear + '</option>';
            row += '</select></td>';
            row += '<td><input type="text" id="mi1_' + cnt2 + '" name="Minimum" value="" class="narrow15 Minimum" /></td>';
            row += '<td><input type="text" id="ma2_' + cnt2 + '" name="Maximum" value="" class="narrow15 Maximum" /></td>';
            row += '<td><input type="text" id="c1_' + cnt2 + '" name="NComments" value="" class="narrow15 NComments" /></td>';
            row += '<td><input type="checkbox" id="ifr1_' + cnt2 + '" name="Enabled1' + cnt2 + '" /></td>';
        }
        row += '<td><img src="img/delete.gif" id="r2_' + cnt2 + '" class="removeNormal" /></td>';
        row += '</tr>';
        cnt2++;
        return row;
    }
    function addServiceRelation(node) {
        var row = '<tr>';
        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">' + (cnt3 + 1) + '</td>';
        row += '<td data-th="' + Lang.RelatedName + '">'
            + '<input type="hidden" name="ID2" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="text" id="rn_' + cnt3 + '" name="RelatedName" value="'
            + getNodeVal(node, "RelatedName") + '" class="relatedname form-control input-sm" />'
            + '<input type="hidden" id="rid_' + cnt3 + '" name="RelatedID" value="' + getNodeVal(node, "RelatedID") + '" />'
            + '</td>';

        row += '<td data-th="' + Lang.Type + '">'
            + '<select id="t_' + cnt3 + '"  class="Type" name="Type" v="' + getNodeVal(node, "Type") + '" >'
            + '<option value="2">' + Lang.Related + '</option>'
            + '<option value="1">' + Lang.ServiceVariate + '</option>'
            + '</select>'
            + '</td>';

        row += '<td data-th="' + Lang.CurrencyConverter + '">'
            + '<input type="text" id="n2_' + cnt3 + '" name="Num"  value="' + getNodeVal(node, "Num") + '" class="validate[custom[number]] num form-control input-sm align-center" />'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a id="rsr_' + cnt3 + '" class="btn btn-danger btn-xs btn-block btn-remove removeServiceRelation" href="#">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';
        row += '</tr>';
        cnt3++;
        setValue('.Type');
        return row;
    }
    function addPanelServices(node) {
        var row = '<tr>';
        if (node) {
            row += '<td>' + (cnt4 + 1) + '</td><td><input type="hidden" name="ID3" value="' + node.attr("ID") + '" /><input type="text" id="prn_' + cnt4 + '" name="PropName" value="' + node.attr("PropName") + '" class=" narrow title" style="width:300px;min-width:80px" /></td>';
            row += '<td><input type="text" id="req4_' + cnt4 + '" name="SubServiceName" value="' + node.attr("SubServiceName") + '" class="subservicename" style="width:300px;min-width:80px"/><input type="hidden" name="SubServiceID" value="' + node.attr("SubServiceID") + '" /></td>';
        }
        else {
            row += '<td>' + (cnt4 + 1) + '</td><td><input type="hidden" name="ID3" value="-1" /><input type="text" id="prn_' + cnt4 + '" name="PropName" value="" class=" narrow title" style="width:300px;min-width:80px" value=""/></td>';
            row += '<td><input type="text" id="req4_' + cnt4 + '" name="SubServiceName" value="" class="subservicename" style="width:300px;min-width:80px"/><input type="hidden" name="SubServiceID"  /></td>';
        }
        row += '<td><img src="img/delete.gif" id="r4_' + cnt4 + '" class="removePanelServices" /></td>';
        row += '</tr>';
        cnt4++;
        return row;
    }
    function addServiceItems(node) {
        // id="x" --> the validationEngine requires ID
        var row = '<tr>';
        if (node) {
            row += '<td>' + (cnt5 + 1) + '</td><td><input type="hidden" name="ID4" value="' + node.attr("ID") + '" /><input type="text" id="req5_' + cnt5 + '" name="ItemName" value="' + node.attr("ItemName") + '" class=" narrow itemname" style="width:300px;min-width:80px" /></td>';
            row += '<td><select id="ty5_' + cnt5 + '" name="ItemType" style="width:80px;min-width:80px">';
            row += '    <option value="0" ' + (node.attr("ItemType") == '0' ? 'selected=selected' : '') + '>Quantity</option>';
            row += '    <option value="1" ' + (node.attr("ItemType") == '1' ? 'selected=selected' : '') + '>Enum</option>';
            row += '    <option value="2" ' + (node.attr("ItemType") == '2' ? 'selected=selected' : '') + '>Ordinal</option>';
            row += '    <option value="3" ' + (node.attr("ItemType") == '3' ? 'selected=selected' : '') + '>Boolean</option>';
            row += '    <option value="4" ' + (node.attr("ItemType") == '4' ? 'selected=selected' : '') + '>Count</option>';
            row += '    <option value="5" ' + (node.attr("ItemType") == '5' ? 'selected=selected' : '') + '>Proportion</option>';
            row += '    <option value="6" ' + (node.attr("ItemType") == '6' ? 'selected=selected' : '') + '>Interalint</option>';
            row += '    <option value="7" ' + (node.attr("ItemType") == '7' ? 'selected=selected' : '') + '>string</option>';
            row += '</select></td>';
            row += '<td><input type="text" id="iet_' + cnt5 + '" name="ItemEnumTable" value="' + node.attr("ItemEnumTable") + '" class="narrow15 ItemEnumTable" /></td>';
        }
        else {
            row += '<td>' + (cnt5 + 1) + '</td><td><input type="hidden" name="ID4" value="-1" /><input type="text" id="req5_' + cnt5 + '" name="ItemName" value="" class=" narrow itemname" style="width:300px;min-width:80px" value=""/></td>';
            row += '<td><select id="ty5_' + cnt5 + '" name="ItemType" style="width:80px;min-width:80px">';
            row += '    <option value="0">Quantity</option>';
            row += '    <option value="1">Enum</option>';
            row += '    <option value="2">Ordinal</option>';
            row += '    <option value="3">Boolean</option>';
            row += '    <option value="4">Count</option>';
            row += '    <option value="5">Proportion</option>';
            row += '    <option value="6">Intervalint</option>';
            row += '    <option value="7">string</option>';
            row += '</select></td>';
            row += '<td><input type="text" id="iet_' + cnt5 + '" name="ItemEnumTable" class="narrow15 ItemEnumTable" /></td>';
        }
        row += '<td><img src="img/delete.gif" id="r5_' + cnt5 + '" class="removeServiceItems" /></td>';
        row += '</tr>';
        cnt5++;
        return row;
    }
    function addServiceSpeciality(node) {
        var row = '<tr>';
        if (node) {
            row += '<td>' + (cnt7 + 1) + '</td><td><input type="hidden" name="ID6" value="' + node.attr("ID") + '" /><input type="text" id="spe6_' + cnt7 + '" name="SpecialtyName" value="' + node.attr("SpecialtyName") + '" class="specialtyname" /><input type="hidden" name="SpecialtyID" value="' + node.attr("SpecialtyID") + '" /></td>';
            row += '<td><input type="text" id="max_' + cnt7 + '" name="MaxAllowed" value="' + node.attr("MaxAllowed") + '" class="validate[custom[number]] narrow MaxAllowed" style="width:90px;min-width:80px" /></td>';
            row += '<td><input type="checkbox" id="ssn_' + cnt7 + '" name="SSEnabled' + cnt7 + '" ' + (node.attr("SSEnabled") == '0' || node.attr("SSEnabled") == 'False' ? '' : 'checked=checked') + ' class="SSEnabled" /></td>';
        }
        else {
            row += '<td>' + (cnt7 + 1) + '</td><td><input type="hidden" name="ID6" value="-1" /><input type="text" id="spe6_' + cnt7 + '" name="SpecialtyName" value="" class="specialtyname" /><input type="hidden" name="SpecialtyID" value="" /></td>';
            row += '<td><input type="text" id="max_' + cnt7 + '" name="MaxAllowed" value="" class="validate[custom[number]] narrow MaxAllowed" style="width:90px;min-width:80px"/></td>';
            row += '<td><input type="checkbox" id="ssn_' + cnt7 + '" name="SSEnabled' + cnt7 + '" class="SSEnabled" /></td>';
        }
        row += '<td><img src="img/delete.gif" id="r7_' + cnt7 + '" class="removeServiceSpeciality" /></td>';
        row += '</tr>';
        cnt7++;
        return row;
    }
    function addServiceVariate(node) {
        //alert(node);
        var row = '<tr>';
        row += '<td class="row-cell" data-th=" ' + Lang.ColRowNr + '">' + (cntSV + 1)
        + '<input type="hidden" id="i_' + cntSV + '" name="ID9" value="' + getNodeVal(node, "ID", -1) + '" />'
        + '</td>';

        row += '<td data-th="' + Lang.Title + '">'
            + '<input type="text" id="svart_' + cntSV + '" name="VarTitle" value="' + getNodeVal(node, "VarTitle")
            + '" class="Title form-control input-sm align-center" />'
            + '</td>';

        row += '<td data-th="' + Lang.BarCode + '">'
            + '<div class="input-group input-group-sm">'
            + '<input type="text" id="svarbar_' + cntSV + '" name="VarBarCode" value="' + getNodeVal(node, "VarBarCode")
            + '" class="form-control align-center VarBarCode" />'
            + '</div>'
            + '</td>';

        row += '<td class="align-center action-remove">'
        + '<a id="r10_' + cntSV + '" class="btn btn-danger btn-xs btn-block btn-remove removeServiceVariate" href="#">'
        + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
        + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
        + '</a>'
        + '</td>';

        row += '</tr>';
        cntSV++;
        return row;
    }
    function autoPostPack(event, data, formatted) {
        $(event.target).next().val(data[0]); // ID
    }
    function groupAutoPostPack(event, data, formatted) {
        $(event.target).prev("input").val(data[0]);
        $(event.target).next().val(data[0]); // ID
    }

    // ---------------------
    // Remove Rows
    // ---------------------
    function removeServiceInsurancePrice($who) {
        var id = $who.attr('id'); // r_1
        id = id.substring(2); // r_1  -->  1
        $('#req_' + id).val(""); // InsuranceTypeName
        $('#req_' + id).next().val(-1); // InsuranceTypeID
        $who.parent().parent().hide();

    }
    //------------
    function removeServicePrice($who) {
        var id = $who.attr('id'); // r6_1
        id = id.substring(3); // r6_1  -->  1
        $('#smn_' + id).val(""); // 
        $('#m1_' + id).val("");
        $('#m1_' + id).next().val(-1); //
        $('#des_' + id).val("");
        $who.parent().parent().hide();
        Growl.Notice(Lang.DeletedSuccessfully);
    }
    //---------------------
    function removeUnits($who) {
        var id = $who.attr('id'); // pn_1
        id = id.substring(3); // pn_1  -->  1
        $('#un_' + id).val("");
        $who.parent().parent().hide();
        Growl.Notice(Lang.DeletedSuccessfully);
    }
    //---------------------------
    function RemoveGroup($who) {
        var id = $who.attr('id'); // r_1
        id = id.substring(2); // r_1  -->  1
        //alert(id);
        $('#g_' + id).val(""); // 
        $('#g_' + id).next().val(-1); // 
        $who.parent().parent().hide();
        Growl.Notice(Lang.DeletedSuccessfully);
    }
    //----------------------------
    function removeNormal($who) {
        var id = $who.attr('id'); // r2_1
        id = id.substring(3); // r2_1  -->  1
        $('#req2_' + id).val(""); // InsuranceTypeName
        $('#req2_' + id).next().val(-1); // InsuranceTypeID
        $who.parent().parent().hide();

    }
    function removeServiceRelation($who) {
        var id = $who.attr('id'); // rsr_1-->  1
        id = id.substring(4); // rsr_1  -->  1
        $('#rn_' + id).val(""); // relatedname
        $('#rn_' + id).next().val(-1); // relatedid
        $who.parent().parent().hide();

    }
    function removePanelServices($who) {
        var id = $who.attr('id'); // r4_1
        id = id.substring(3); // r4_1  -->  1
        $('#req4_' + id).val(""); // 
        $('#req4_' + id).next().val(-1); // 
        $who.parent().parent().hide();
    }
    function removeServiceItems($who) {
        var id = $who.attr('id'); // r5_1
        id = id.substring(3); // r5_1  -->  1
        $('#req5_' + id).val(""); // 
        $('#req5_' + id).next().val(-1); // 
        $who.parent().parent().hide();

    }
    function removeServiceSpeciality($who) {
        var id = $who.attr('id'); // r5_1
        id = id.substring(3); // r5_1  -->  1
        $('#spe6_' + id).val(""); // 
        $('#spe6_' + id).next().val(-1); // 
        $who.parent().parent().hide();

    }
    function removeServiceVariate($who) {
        var id = $who.attr('id'); // r10_1
        id = id.substring(4); // r10_1  -->  1
        $('#svart_' + id).val("");
        $('#svarbar_' + id).val(""); // 
        $who.parent().parent().hide();
        Growl.Notice(Lang.DeletedSuccessfully);
    }
    function showTypeDiv() {
        $('#label1').hide(); $('#TwoUnitName').hide();

        // ServicePrice is for all types
        $('#tServicePrice, tabServicePrice').show();
        $('#tServiceInsurancePrice, tabServiceInsurancePrice').show();
    }
    function addNewServicePriceRow() {
        var table = $("#tableServicePrice");
        $('tr:last', table).after(addServicePrice(null));
        var lastRow6 = $('tr:last', table);

        $('.removeServicePrice', lastRow6).click(function () {
            removeServicePrice($(this));
        });
        dateSelect('.dateSelect', lastRow6);
        $('.dateSelect', lastRow6).keydown(function (e) {
            if (e.keyCode == 40)
                displayDatePicker2(this);
        });
        autoCompleteFree('Supplier', 'supplierauto', "#tableServicePrice tr:last .modelname", "#justfurfun"
            , autoPostPack
            , function (data, i, n, value) {
                return data[1] + ' (' + data[2] + ')';
            });
        $('.width, .length, .height', lastRow6).blur(function () { updateVolume(this); });
        commaInputs('#subDetailForm');
        monetaryKeys('#subDetailForm');
        Growl.Notice(Lang.RowAddedSuccessfully);
    }
    function addNewServiceVariateRow() {
        var table = $("#tableServiceVariate");
        $('tr:last', table).after(addServiceVariate(null));
        var lastRow6 = $('tr:last', table);

        $('.removeServiceVariate', lastRow6).click(function () {
            removeServiceVariate($(this));
        });
        Growl.Notice(Lang.RowAddedSuccessfully);
    }
    function bindAdds() {
        $('.addServiceInsurancePrice').click(function () {
            var table = $(this).closest('table');
            $('tr:last', table).after(addServiceInsurancePrice(null));
            var lastRow = $('tr:last', table);
            autoComplete('Insurance', 'insuranceauto', "#tableServiceInsurancePrice .insurancename", "#justfurfun"
                , autoPostPack
                , function (data, i, n, value) {
                    return data[1] + ' (' + data[2] + ')';
                });
            autoComplete('PatientType', 'enumauto2', "#tableServiceInsurancePrice .patienttypename", "#justfurfun"
                , autoPostPack
                , function (data, i, n, value) {
                    return data[1] + ' (' + data[2] + ')';
                });


            $('.removeServiceInsurancePrice', lastRow).click(function () {
                removeServiceInsurancePrice($(this));
            });
            dateSelect('.dateSelect', lastRow);
            $('.dateSelect', lastRow).keydown(function (e) {
                if (e.keyCode == 40)
                    displayDatePicker2(this);
            });
            bindIsFranchise();
            checkIsFranchise();
        });
        //-----------------------
        $('.addServicePrice').click(addNewServicePriceRow);
        $('.addServiceVariate').click(addNewServiceVariateRow);

        //-------------------------------
        $('.addUnits').click(function () {
            var table = $("#tableUnits");
            $('tr:last', table).after(addUnits(null));
            var lastRow6 = $('tr:last', table);

            $('.removeunits', lastRow6).click(function () {
                removeUnits($(this));
            });
            autoComplete('Units', 'enumauto', "#tableUnits .unitsname", "#justfurfun"
                , autoPostPack
                , function (data, i, n, value) {
                    return data[1];
                });
            $('.uwidth, .ulength, .uheight', lastRow6).blur(function () { updateUVolume(this); });

            Growl.Notice(Lang.RowAddedSuccessfully);
        });
        //--------------------------
        $('.addGroup').click(function () {
            var table = $("#tableGroup");
            var tableID = table.attr("id");
            $('tr:last', table).after(addGroup(null));
            var lastRow = $('tr:last', table);
            if (_Context.isStoreUser) {
                autoComplete('Grouping', 'storeusergroupserviceauto', "#tableGroup tr:last .groupingname", "#justfurfun"
                   , groupAutoPostPack
                   , function (data, i, n, value) {
                       return data[1];
                   });
            } else {
                autoComplete('Grouping', 'groupserviceauto', "#tableGroup tr:last .groupingname", "#justfurfun"
                   , groupAutoPostPack
                   , function (data, i, n, value) {
                       return data[1];
                   });
            }
            Growl.Notice(Lang.RowAddedSuccessfully);
        });
        //-----------------------------

        $('.addNormal').click(function () {
            var table = $(this).closest('table');
            var tableID = table.attr("id");
            $('tr:last', table).after(addNormal(null));
            var lastRow1 = $('tr:last', table);
            autoComplete('Service', 'enumauto', "#tableNormal .kitname", "#justfurfun"
                , autoPostPack
                , function (data, i, n, value) {
                    return data[1] + ' (' + data[2] + ')';
                });
            autoComplete('Gender', 'enumauto', "#tableNormal .gendername", "#justfurfun"
                , autoPostPack
                , function (data, i, n, value) {
                    return data[1] + ' (' + data[2] + ')';
                });

            $('.removeNormal', lastRow1).click(function () {
                removeNormal($(this));
            });
        });
        //--------------
        $('.addServiceSpeciality').click(function () {
            var table = $(this).closest('table');
            var tableID = table.attr("id");
            $('tr:last', table).after(addServiceSpeciality(null));
            var lastRow8 = $('tr:last', table);
            autoComplete('Specialty', 'enumauto', "#tableServiceSpecialityl .specialtyname", "#justfurfun"
                , autoPostPack
                , function (data, i, n, value) {
                    return data[1] + ' (' + data[2] + ')';
                });


            $('.removeServiceSpeciality', lastRow8).click(function () {
                removeServiceSpeciality($(this));
            });
        });
        //--------------
        $('.addServiceRelation').click(function () {
            var table = $(this).closest('table');
            var tableID = table.attr("id");
            $('tr:last', table).after(addServiceRelation(null));
            var lastRow2 = $('tr:last', table);
            if (_Context.isStoreUser) {
                autoComplete('Service',
                    'storeuserserviceauto',
                    "#tableServiceRelation .relatedname",
                    "#justfurfun",
                    autoPostPack,
                    function (data, i, n, value) {
                        return data[1];
                    });
            } else {
                autoComplete('Service',
                'service2auto',
                "#tableServiceRelation .relatedname",
                "#justfurfun",
                autoPostPack,
                function (data, i, n, value) {
                    return data[1];
                     });
            }

            $('.removeServiceRelation', lastRow2).click(function () {
                removeServiceRelation($(this));
            });
        });

        $('.addPanelServices').click(function () {
            var table = $(this).closest('table');
            var tableID = table.attr("id");
            $('tr:last', table).after(addPanelServices(null));
            var lastRow4 = $('tr:last', table);
            autoComplete('Service', 'testdefinition2auto', "#tablePanelServices tr:last .subservicename", "#justfurfun"
                , autoPostPack
                , function (data, i, n, value) {
                    return data[1] + ' (' + data[2] + ')';
                });
            $('.removePanelServices', lastRow4).click(function () {
                removePanelServices($(this));
            });
        });

        $('.addServiceItems').click(function () {
            var table = $(this).closest('table');
            var tableID = table.attr("id");
            $('tr:last', table).after(addServiceItems(null));
            var lastRow5 = $('tr:last', table);

            $('.removeServiceItems', lastRow5).click(function () {
                removeServiceItems($(this));
            });
        });
    }
    function bindRemoves() {
        //$('.removeNormal').unbind('click');
        $('.removeServiceInsurancePrice').click(function () {
            removeServiceInsurancePrice($(this));
        });
        //-----------
        $('.removeServicePrice').click(function () {
            removeServicePrice($(this));
        });
        $('.removeServiceVariate').click(function () {
            removeServiceVariate($(this));
        });
        //-------------
        $('.removeunits').click(function () {
            removeUnits($(this));
        });
        //---------------------
        $('.removeGroup').click(function () {
            RemoveGroup($(this));
        });
        //-----------------------------
        $('.removeNormal').click(function () {
            removeNormal($(this));
        });
        $('.removeServiceRelation').click(function () {
            removeServiceRelation($(this));
        });
        $('.removePanelServices').click(function () {
            removePanelServices($(this));
        });
        $('.removeServiceItems').click(function () {
            removeServiceItems($(this));
        });
        $('.removeServiceSpeciality').click(function () {
            removeServiceSpeciality($(this));
        });
    }
    function bindIsFranchise() {
        $('#tableServicePrice .IsFranchise').unbind('blur').unbind('click');
        $('#tableServicePrice .IsFranchise').blur(function () {
            checkIsFranchise();
        }).click(function () {
            checkIsFranchise();
        });
    }

    function checkIsFranchise() {
        $('#tableServicePrice .IsFranchise').each(function (index, value) {
            var ch = $(this).attr("checked");
            if (ch) {
                $(this).parent().parent().find('.FreePrice').hide();
                $(this).parent().parent().find('.BookletPrice').hide();
                $('#m').hide();
                $('#m1').hide();
            }
            else {
                $(this).parent().parent().find('.FreePrice').show();
                $(this).parent().parent().find('.BookletPrice').show();
                $('#m').show();
                $('#m1').show();
            }
        });
    }

    function handleSingleModel() {
        if (mID <= 0) {
            var _autourl = 'get.aspx?op=Auto&table=Supplier&qd=supplierdefaultauto' + appendTime();
            var row = getServerValues(_autourl);
            if (row && row.length && row.length > 2) {
                addNewServicePriceRow();
                $("#tableServicePrice .modelname").next().val(row[0]);
                $("#tableServicePrice .modelname").val(row[1]);
            }
        }
    }
    function pricingChange() {
        var pricing = $('#Pricing').val();
        if (toInt(pricing, 1) === 4) {
            $('.nopricing').show();
            $('.pricing').hide();
            $('.ProfitPercent').val('');
        }
        else {
            $('.nopricing').hide();
            $('.pricing').show();
        }
    }
    // ------------------------------
    // startup code
    // ------------------------------
    // Master Form
    mID = toInt($('#MasterID', '#subDetailForm').val(), -1);
    $("#ResultType").val('@ResultType');
    $('#ResultType').change(function () {
        setresulttype();
    });

    $('#DiscountStartTime,#DiscountEndTime').timeEntry(
                    { spinnerImage: 'img/timeentry/spinnerDefault.png', spinnerSize: [20, 20, 0] }); //برای نمایش آیکون مربوط به ساعت
    if (_Context.isStoreUser) {
        $('.discountdatetime').show();
    }
    if (mID == -1)
        $('#Enabled').attr("checked", 'checked');
    else
        setChecked("#Enabled");
    setChecked("#IsYaraneh");
    $("#Global").attr("checked", '@Global');
    $("#Midwife").attr("checked", '@Midwife');
    $("#Expert").attr("checked", '@Expert');
    $("#Drug").attr("checked", '@Drug');
    $("#Hoteling").attr("checked", '@Hoteling');
    $("#Emergency").attr("checked", '@Emergency');
    setChecked("#IsVirtual");
    setValue('#Pricing');
    autoComplete('ServiceCoding', 'servicecodingauto', "#ServiceCodingName", "#ServiceCodingID");

    if (false /*toInt($('#ServiceTypeID').val(), 0) > 0 || mID > 0*/) {
        $("#ServiceTypeName2Container").hide();
    } else {
        autoComplete('ServiceType', 'servicetypeauto', "#ServiceTypeName", "#ServiceTypeID", function (event, data, formatted) {
            $("#ServiceTypeName").val($("#ServiceTypeName").val());
            $("#ServiceTypeID").val($("#ServiceTypeID").val());
            //showTypeDiv();
        }, function (data, i, n, value) {
            return '<div dir=rtl><b>' + data[1] + '</b><br />' + Lang.Code + ': ' + data[2] + '</div>';
        });
    }
    autoComplete('Units', 'enumauto', "#UnitName", "#UnitID");
    autoComplete('Pricing', 'enumauto', "#PricingName", "#PricingID");
    autoComplete('Units', 'enumauto', "#TwoUnitName", "#TwoUnitID");
    autoComplete('DrugType', 'enumauto', "#DrugTypeName", "#DrugTypeID");
    autoComplete('ConsumptionType', 'enumauto', "#ConsumptionTypeName", "#ConsumptionTypeID");
    autoComplete('TestType', 'enumauto', "#TestTypeName", "#TestTypeID");
    autoComplete('RSType', 'enumauto', "#RSTypeName", "#RSTypeID");
    autoComplete('RSNormal', 'enumauto', "#RSNormalName", "#RSNormalID");
    //     autoComplete('RSConsume', 'enumauto', "#RSConsumeName", "#RSConsumeID");
    autoComplete('Storing', 'enumauto', '#StoringName', '#StoringID');
    function setresulttype() {
        var t = $("#ResultType").val();

        if (t == 1) {
            $('#enums').show();
            $('#EnumTable').show();
        }
        else {
            $('#enums').hide();
            $('#EnumTable').hide();

        }
    }

    showTypeDiv();

    tabify();

    fillTable("get.aspx?op=GetXml&qd=serviceprice&ServiceID=" + mID, '#tableServicePrice', addServicePrice);
    $('.width, .length, .height').blur(function () { updateVolume(this); });

    var repeat = $("#Repeat").val();
    if (toInt(repeat, 0) === 1) {
        $("#Code, #BarCode").val("");
        $("#MasterID").val(-1);
        $(".servicepriceid").val(-1);
        _Context.id = -1;
    }

    pricingChange();
    $('#Pricing').change(function () {
        pricingChange();
    });

    fillTable("get.aspx?op=GetXml&qd=servicevariate&ServiceID=" + mID, '#tableServiceVariate', addServiceVariate);

    fillTable("get.aspx?op=GetXml&qd=serviceunits&ServiceID=" + mID, '#tableUnits', addUnits);
    $('.uwidth, .ulength, .uheight').blur(function () { updateUVolume(this); });

   
    setValue('.Type');

    if (_Context.isAccountingVer) {
        $('#tableServiceInsurancePrice, #tServiceInsurancePrice').hide();
        $('#tServicePrice').click();
        $('#tabServicePrice').click();
    }
    else {
        alert(0);
        fillTable("get.aspx?op=GetXml&qd=serviceinsuranceprice&ServiceID=" + mID, '#tableServiceInsurancePrice', addServiceInsurancePrice);
        fillTable("get.aspx?op=GetXml&qd=servicespeciality&ServiceID=" + mID, '#tableServiceSpecialityl', addServiceSpeciality);
        fillTable("get.aspx?op=GetXml&qd=normal&ServiceID=" + mID, '#tableNormal', addNormal);
        //fillTable("get.aspx?op=GetXml&qd=ServiceRelation&ServiceID=" + mID, '#tableServiceRelation', addServiceRelation);
        fillTable("get.aspx?op=GetXml&qd=panelservice&ServiceID=" + mID, '#tablePanelServices', addPanelServices);
        fillTable("get.aspx?op=GetXml&qd=serviceitem&ServiceID=" + mID, '#tableServiceItems', addServiceItems);
    }
    function doMore() {
        dateSelect('.dateSelect');
        $('.dateSelect').keydown(function (e) {
            if (e.keyCode == 40)
                displayDatePicker2(this);
        });

        function autoPostPack(event, data, formatted) {
            $(event.target).next().val(data[0]); // ID
        }
        autoComplete('Insurance', 'insuranceauto', "#tableServiceInsurancePrice .insurancename", "#justfurfun"
            , autoPostPack
            , function (data, i, n, value) {
                return data[1] + ' (' + data[2] + ')';
            });
        autoCompleteFree('Supplier', 'supplierauto', "#tableServicePrice .modelname", "#justfurfun"
            , autoPostPack
            , function (data, i, n, value) {
                return data[1] + ' (' + data[2] + ')';
            });
        autoComplete('Units', 'enumauto', "#tableUnits .unitsname", "#justfurfun"
               , autoPostPack
               , function (data, i, n, value) {
                   return data[1];
               });

        autoComplete('Service', 'enumauto', "#tableNormal .kitname", "#justfurfun"
            , autoPostPack
            , function (data, i, n, value) {
                return data[1] + ' (' + data[2] + ')';
            });
        autoComplete('Gender', 'enumauto', "#tableNormal .gendername", "#justfurfun"
            , autoPostPack
            , function (data, i, n, value) {
                return data[1] + ' (' + data[2] + ')';
            });


        autoComplete('Service', 'testdefinition2auto', "#tablePanelServices .subservicename", "#justfurfun"
            , autoPostPack
            , function (data, i, n, value) {
                return data[1] + ' (' + data[2] + ')';
            });
        autoComplete('Specialty', 'enumauto', "#tableServiceSpecialityl .specialtyname", "#justfurfun"
            , autoPostPack
            , function (data, i, n, value) {
                return data[1] + ' (' + data[2] + ')';
            });
        bindAdds();
        bindRemoves();
        checkIsFranchise();
        bindIsFranchise();
        insertValidation('#subDetailForm');
        monetaryKeys();
        commaInputs();
        // percentInputsSet('#subDetailForm');
    }
    doMore();
    handleSingleModel();
    if (_Context.isAccountingVer) {
        $('.noacc').hide();
        $('#tServicePrice').html(Lang.Model);
    }
    monetaryInputsSet('#subDetailForm', '#Date');
    commaInputs('#subDetailForm');
    monetaryKeys('#subDetailForm');
    insertValidation('#subDetailForm');


    percentInputsSet('#subDetailForm');

    function checkBarCodeRepetition(elm, serviceId, serviceVariatId) {
        var val = $(elm).val();
        var ok = getServerValue('checkbarcoderepetition', 'Service', 'BarCode=' + val + '&ServiceId=' + serviceId + '&ServiceVariatId=' + serviceVariatId);
        if (toInt(ok, 0) === 1) {
            $(elm).val('');
            $(elm).focus();
            Growl.Error(Lang.BarCodeRepetitionMsg, 3000);
        }
    }
    $('#BarCode').blur(function () {
        var id = toInt($('#MasterID').val(), -1);
        checkBarCodeRepetition($(this), toInt(id, -1), -1);
    });
    $('.VarBarCode').blur(function () {
        var id = $(this).attr('id');
        id = id.substring(id.indexOf('_') + 1);
        id = $('#i_' + id).val();
        checkBarCodeRepetition($(this), -1, toInt(id, -1));
    });
}