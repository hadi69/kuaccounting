﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
    92-10-16: file created
*/
function view_assessmentfield() {
    function typeChange() {
        var type = toInt($('#Type').val(), 10);
        if (type == 2) {
            $('.chosenvalue').show();
        }else {
            $('.chosenvalue').hide();
        }
        $('#Type').change(function () {
            type = toInt($(this).val(), 10);
            if (type == 2) {
                $('.chosenvalue').show();
            } else {
                $('.chosenvalue').hide();
            }
        });
    }
    ///
    setValue('#Type');
    typeChange();
    autoComplete('CooperationType', 'enumauto1', '#CooperationTypeName', '#CooperationTypeID');
}
