﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
92-10-19: file created
*/
function view_checksshop() {
    if (_Context.id != -1) {
        replacePrintButton({
            primary: {
                text: Lang.CheckReceipt,
                href: Router.Generate("print-checkreceipt", { params: { id: _Context.id }, query: { print: "true" } })
            }
        });
    } else {
        replacePrintButton({
        });
    }
    makeChosen();
    function displayAmount() {
        $('#AmountStr').html(numToString($('#Amount').val()));
    }

    function lastValues() {
        var id = toInt($('#subDetailForm #PersonnelID').val(), 0);
        if (id <= 0)
            return;
        var u = 'get.aspx?op=Auto&table=Checks&qd=checkslast&q=' + id + appendTime();
        var row = getServerValues(u);
        if (row && row.length && row.length > 2){
            // AccountNo, Bank, Branch
            $('#subDetailForm #AccountNo').val(row[0]);
            $('#subDetailForm #BankTreePath').val(row[1]);
            $('#subDetailForm #BankName').val(row[2]);
            $("#subDetailForm #Branch").val(row[3]);
        }
    }

    function bindVouchers() {
        // Vouchers
        var _autourl = 'get.aspx?op=Auto&table=DocumentAccounting&qd=checkvouchers&q=' + $('#ID').val();
        var res = getRowtable(_autourl);
        if (!res[0]) {
            _autourl = 'get.aspx?op=Auto&table=DocumentAccounting&qd=checkvouchers_old&q=' + $('#ID').val();
            res = getRowtable(_autourl);
        }
        if (res && res.length > 0) {
            $('#Voucher1').attr('href', "list.aspx?table=DocumentAccounting&id=" + res[0][0] + "&itemid=" + res[0][2]);
            $('#Voucher1').html(res[0][1]);
        }
        if (res && res.length > 1) {
            $('#Voucher2').attr('href', "list.aspx?table=DocumentAccounting&id=" + res[1][0] + "&itemid=" + res[1][2]);
            $('#Voucher2').html(res[1][1]);
        }
        if (res && res.length > 2) {
            $('#Voucher3').attr('href', "list.aspx?table=DocumentAccounting&id=" + res[2][0] + "&itemid=" + res[2][2]);
            $('#Voucher3').html(res[2][1]);
        }
    }
    function checkPersonCredit() {
        var personnelID = toInt($('#subDetailForm #PersonnelID').val(), 0);
        if (toInt($('#subDetailForm #AssignorID').val(), 0) > 0) {
            personnelID = toInt($('#subDetailForm #AssignorID').val(), 0);
        }
        $('#creditmsg').html('');
        if (personnelID > 0) {
            var url = 'get.aspx?op=Auto&table=Personnel&qd=personcheckcredit&PersonnelID=' + personnelID + appendTime();
            var row = getServerValues(url);
            var checkCredit = toFloat(row[0], 0);
            var amount = toFloat(row[1], 0);
            if (checkCredit > 0) {
                amount += toFloat($('#Amount').val(), 0);
                if (checkCredit < amount)
                    $('#creditmsg').html(Lang.LowCheckCredit);//checkCredit
            }
        }
    }
    function checkVisitorCredit() {
        var visitorID = toInt($('#subDetailForm #VisitorID').val(), 0);
        $('#visitorcreditmsg').html('');
        if (visitorID > 0) {
            var url = 'get.aspx?op=Auto&table=Personnel&qd=visitorcheckcredit&VisitorID=' + visitorID + appendTime();
            var row = getServerValues(url);
            var checkCredit = toFloat(row[0], 0);
            var amount = toFloat(row[1], 0);
            if (checkCredit > 0) {
                amount += toFloat($('#Amount').val(), 0);
                if (checkCredit < amount)
                    $('#visitorcreditmsg').html(Lang.VisitorLowCheckCredit);//checkCredit
            }
        }
    }

    function checkview() {
            if ($('#setstatus').val() != CheckStatusEnum.Expend)
                $('#personDiv').hide();
            $('#BankDiv').hide();
            $('#Serial').attr('readonly', '');            
    }
    // ------------------------------
    // startup code
    // ------------------------------
    $('#Serial').focus();
   // checkview();
    dateSelect('.dateSelect');
    setValue('#Status');
    if (!hasValuePositive($('#UserID').val()))
        $('#UserID').val(_Context.userID);
    if ($("#IssueDate").val() == '')
        $("#IssueDate").val(_Context.today);

    autoComplete('Trees', 'treesbanksauto', '#ReceiverBankName', '#ReceiverBankTreePath', function (event, data, formatted) {
        // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due
        var hasChild = data[5] != '';
        if (hasChild) {
            $('#ReceiverBankName, #ReceiverBankTreePath').val('');
            alert(Lang.HasChild);
        }
    }, formatTreePath, null, null);

    autoComplete('Trees', 'treesauto', '#OweTreePathName', '#OweTreePath', function (event, data, formatted) {
        // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due
        var hasChild = toBool(data[5]), canHaveSubTree = toBool(data[12]);
        if (hasChild || canHaveSubTree) {
            $('#OweTreePathName, #OweTreePath').val('');
            if (hasChild)
                alert(Lang.HasChild);
            else
                alert(Lang.HasSubTreeBut);
        }
    }, formatTreePath, null, null);


    autoComplete('Personnel', 'personnelselectauto', '#subDetailForm #AssignorName', '#subDetailForm #AssignorID', lastValues
    , function (data, i, n, value) {
        $('#subDetailForm #AssignorID').val('');
        return formatPersonnelItem(data);
    });

    autoCompleteFree('Personnel', 'visitorauto', '#VisitorName', '#VisitorID', null
    , function (data, i, n, value) {
        //$('#VisitorID').val('');
        return formatPersonnelItem(data);
    });

    autoComplete('BankAccountCheck', 'bankaccountautoforchecks', '#BankAccountName', '#BankAccountID');

    autoComplete('Personnel', 'personnelselectauto', '#subDetailForm #PersonnelName', '#subDetailForm #PersonnelID', lastValues
    , function (data, i, n, value) {
        $('#subDetailForm #PersonnelID').val('');
        return formatPersonnelItem(data);
    });

    autoComplete('Personnel', 'personnelselectauto', '#subDetailForm #ReceiverPerson', '#subDetailForm #ReceiverPersonID', lastValues
, function (data, i, n, value) {
    $('#subDetailForm #ReceiverPersonID').val('');
    return formatPersonnelItem(data);
});
    lastValues();
    $("#Amount").blur(checkPersonCredit);
    $("#VisitorName").blur(checkVisitorCredit);
    if (!$('#setstatus').val())
        $('#setstatus').val($('#Status').val());

    if (!hasValuePositive($('#ID').val())) {
        $('#Status, #HiddenStatus').val(CheckStatusEnum.New);
        $('#visitorDiv').hide();
        $('#personDiv').hide();
        $('#BankDiv').hide();
        $('#RecievedDiv').hide();
    }
    if ($('#setstatus').val() == CheckStatusEnum.Received) {
        $('#Status, #HiddenStatus').val(CheckStatusEnum.Received);
        $('#visitorDiv').hide();
        $('#personDiv').hide();
        $('#BankDiv').hide();
        $('#RecievedDiv').show();
    }
    else if ($('#setstatus').val() == CheckStatusEnum.Refused) {
        $('#Status, #HiddenStatus').val(CheckStatusEnum.Refused);
        $('#visitorDiv').hide();
        $('#personDiv').hide();
        $('#BankDiv').hide();
        $('#RecievedDiv').hide();
    }
    else if ($('#setstatus').val() == CheckStatusEnum.New && $('#setstatus').val() != '') {
        $('#Status, #HiddenStatus').val(CheckStatusEnum.New);
        $('#visitorDiv').hide();
        $('#personDiv').hide();
        $('#BankDiv').hide();
        $('#RecievedDiv').hide();
    }
    else if ($('#setstatus').val() == CheckStatusEnum.Sleep) {
        $('#Status, #HiddenStatus').val(CheckStatusEnum.Sleep);
        $('#personDiv').hide();
        $('#BankDiv').show();
        $('#visitorDiv').hide();
        $('#RecievedDiv').hide();
    }
    else if ($('#setstatus').val() == CheckStatusEnum.Expend) {
        $('#Status, #HiddenStatus').val(CheckStatusEnum.Expend);
        $('#personDiv').show();
        $('#BankDiv').hide();
        $('#visitorDiv').hide();
        $('#RecievedDiv').hide();
    }
    else if ($('#setstatus').val() == CheckStatusEnum.Visitor) {
        $('#Status, #HiddenStatus').val(CheckStatusEnum.Visitor);
        $('#visitorDiv').show();
        $('#personDiv').hide();
        $('#BankDiv').hide();
        $('#RecievedDiv').hide();
    }
    else {
        $('#visitorDiv').hide();
        $('#personDiv').hide();
        $('#BankDiv').hide();
        $('#RecievedDiv').hide();
    }
 
    bindVouchers();
  

    insertValidation('#subDetailForm');
    monetaryInputsSet('#subDetailForm');
    commaInputs('#subDetailForm');

    $('#Amount').blur(displayAmount).keyup(displayAmount);
    displayAmount();
    if ($('#Status').val() != CheckStatusEnum.Visitor) {
        $('#visitorDiv').hide();
    }
    var id = $('#ID').val();
    if (id > 0) {
        uploadify('#ChooseFile', '#FileName', '#StartUpload', 'checks/' + id
                   , function (fileName) {
                       $('#FileNameDisplay').html(fileName);
                   });
        $('#FileLink').html("<a href='files/checks/" + id + "/" + $('#FileName').val() + "' target=_blank>" + $('#FileName').val() + "</a>");
    }
    else
        $('.choosefile').hide();
}