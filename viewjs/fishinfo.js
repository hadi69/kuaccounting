﻿function view_fishinfo() {
    // ------------------------------
    // startup code
    // ------------------------------
    setChecked("#WorkingFriday");
    setChecked("#HasInsurance");
    setValue("#Month");
    setValue("#Shift");
    autoComplete('Personnel', 'staffauto', '#Person', '#PersonnelID');
    autoComplete('Marital', 'enumauto', '#MaritalStatusName', '#MaritalStatus');
    $('.popupTitle').html(Lang.IssuanceFish);
   
    insertValidation();
}