﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
93-01-12: file created
*/
function view_wards() {
    var selectedIDs = -1, selectedPatientNames = -1, selectedIndex = -1, selectedPatientFullNames = -1, mCmd = -1;
    function doCommand(com, grid) {
        if (com == Lang.Forms)
            showforms();
        else
            generalCommand(com, grid, show);
    }
    //شروع فرم ساز
    function addCmd(cmd, text, onlyView) {
        $('#cmdsDiv').append("<a href='" + cmd + "' class='cmd'>" + text + "</a>");
    }
    function prepareSelection() {
        //در این قسمت تعداد رکوردهایی که انتخاب شده اند مشخص می شوند و مقدار آنها در آرایه ذخیره می شوند
        selectedIDs = getRowIDs("#flexi");
        if (selectedIDs.length == 0)
            return false;
        selectedPatientNames = getGridRowContents("#flexi", 3);
        //            for (var i = 0; i < selectedStatueses.length; i++)
        //                selectedStatueses[i] = getStatus(selectedStatueses[i]);
        selectedIndex = -1; //شمارنده آرایه هاست
        return true;
    }
    function handleNextForm() {
        selectedIndex++;
        var formDeclareID = mCmd.substring(1);
        _Context.id = -1;
        var _url = 'get.aspx?op=GetForm&id=-1&OtherID=' + selectedIDs[selectedIndex] + "&OtherTitle=" + encodeURIComponent(selectedPatientNames[selectedIndex])
            + '&PersonnelID=' + _Context.personnelID + '&FormDeclareID=' + formDeclareID + appendTime();
        showPopup(_url, showFormDeclareCallback);
        return;
    }
    function bindEvent() {
        $('.cmd').click(function () {
            if (!prepareSelection()) {
                alert(Lang.ChoosePatients);
                return false;
            }
            $('.cmdDiv').hide();
            mCmd = $(this).attr('href');
            handleNextForm();
            return false;
        });
    }

    function prepare() {
        var _url = 'get.aspx?op=GetXml&qd=formdeclaredisplay&table=FormDeclare&FormType=' + FormTypeEnum.Ward;
        $.ajax({
            url: _url,
            type: 'GET',
            dataType: 'xml',
            async: false,
            success: function (xmlDoc) {
                var xNode = (xmlDoc.firstChild.nextSibling) ? xmlDoc.firstChild.nextSibling : xmlDoc.firstChild;
                var items = xNode.childNodes[0];
                // Bind Items
                $(items).find("R").each(function () {
                    var node = $(this);
                    addCmd("F" + node.attr("ID"), node.attr("Title"));
                });
            },
            error: ajaxError
        });
    }
    function prepareSelection() {//این تابع از رکوردی که انتخاب شده است اطلاعات لازم را استخراج می کند
        selectedIDs = getRowIDs("#flexi"); //میقدار ID  رکورد انتخابی را بر می گرداند
        if (selectedIDs.length == 0)
            return false;
        selectedPatientNames = getGridRowContents("#flexi", 3); //میقدار سومین ستون را برمی گرداند
        selectedPatientFullNames = getGridRowContents("#flexi", 4);
        selectedIndex = -1;
        return true;
    }
    function showforms() {
        killEvent();
        $('.cmdDiv').hide();
        $('html').unbind('click');
        var offset = $(".forms").offset();
        $('#cmdsDiv').offset({ top: offset.top + 10, left: offset.left - $('#cmdsDiv').width() }); //$('#usermenu').width()
        $('#cmdsDiv').show();
        $('#cmdsDiv').offset({ top: offset.top + 10, left: offset.left - $('#cmdsDiv').width() }); //
        $('html').click(function () {
            $('#cmdsDiv').hide();
        });
        return false;
    }
    //پایان فرم ساز
    function run() {
        prepare();
        bindEvent();
        var terminology = getTerminology(_Context.table);
        if (terminology)
            $('#term').html("<a href='help/terminology.aspx' target=_blank title='" + Lang.HasTerminology + "'>" + terminology + "</a>");
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        //alert(_url);
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', width: 20, sortable: false, align: 'left' },
                    { display: Lang.Location, name: 'LocationName', width: 100, sortable: true, align: 'center' },
                    { display: Lang.WardType, name: 'WardTypeName', width: 100, sortable: true, align: 'right' },
                    { display: Lang.Title, name: 'Name', width: 100, sortable: true, align: 'right' },
                    { display: Lang.Code, name: 'Code', width: 70, sortable: true, align: 'right' },
                    { display: Lang.Gender, name: 'GenderName', width: 70, sortable: true, align: 'right' },
                    { display: Lang.Outgo, name: 'OutgoName', width: 100, sortable: true, align: 'right' },
                    { display: Lang.Enabled, name: 'Enabled', width: 40, sortable: false, align: 'right', process: showBool },
                    { display: Lang.ColEdit, name: 'Edit', width: 40, sortable: false, align: 'right', process: function (celDiv, id) { editLink(celDiv, id, show) } }
                ],
                buttons:
                [
                    { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                    { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                    { name: Lang.Forms, bclass: 'forms', onpress: doCommand, onaccess: hasAccess("Ward", "ward", "forms") },
                    { separator: true },
                    { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                searchitems: null,
                sortname: "LocationName",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,

                height: gridHeight, resizable: gridResizable,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                }
            });
    }
    function showNames(celDiv, id) {
        //todo: format integr
        $(celDiv).html(celDiv.innerText);
    }
    function showcallback(v, m, f) {
        $('.ClosingTime').attr('name', 'ClosingTime');
        $('.OpeningTime').attr('name', 'OpeningTime');

        var qry = (_Context.id == -1) ? 'wardinsert' : 'wardupdate';
        return newMasterscallback(v, m, f, 'Ward', qry, 'Bed,WardServiceType,WardFund'
                , 'bedupdate,wardservicetypeupdate,wardfundupdate'
                , 'bedinsert,wardservicetypeinsert,wardfundinsert'
                , 'beddelete,wardservicetypedelete,wardfunddelete'
                , 'RoomNo,ServiceTypeID,FundID', null, null, show);

    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;

        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    $('#ptitle').html("<a href='menu/basics.aspx'>" + Lang.MBasics + "</a> :: " + _Context.title);
    setClassBasic();
    run();
}