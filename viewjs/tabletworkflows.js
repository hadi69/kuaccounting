﻿
/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
92-10-19: file created
*/
function view_tabletworkflows() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'center', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: true, css_class: 'row-cell' },
                    { display: Lang.Type, name: 'Type', sortable: true, css_class: 'width-100 align-center', process: displayType },
                    { display: Lang.WorkflowType, name: 'WorkflowType', sortable: true, css_class: 'width-200 align-center', process: _displayWorkflowType },
                    { display: Lang.OutputFromServerTaskName, name: 'OutputFromServerTaskName', sortable: true, css_class: 'width-150 align-center' },
                    { display: Lang.CooperationType, name: 'CooperationTypeName', sortable: true, css_class: 'width-100 align-center' },
                    { display: Lang.Comments, name: 'Comments', sortable: true },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' }
                ],
                buttons:
                [
                   { name: Lang.GBNew, bclass: 'new', onpress: doCommand},
                   { name: Lang.GBDel, bclass: 'delete', onpress: doCommand},
                   { name: Lang.GBPrint, bclass: 'print', onpress: doCommand},
                   { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand}
                ],
                searchitems: null,
                sortname: "Type",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,

                height: gridHeight, resizable: gridResizable,
                singleSelect: true,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                },
            });
    }
    function _displayWorkflowType(celDiv, id) {
        var type = toInt($(celDiv).html(), 0);
        displayWorkflowType(celDiv, type);
    }
    function saveTrSelected(id) {
        $('.trSelected').removeClass('trSelected');
        $('#row' + id).addClass('trSelected');
        saveSelectedRows('#flexi');
        $('.trSelected').removeClass('trSelected');
    }
    function displayType(celDiv, id) {
        var type = parseInt($(celDiv).html(), 0);
        displayTabletWorkFlowType(celDiv, type);
    }
    function showcallback(v, m, f) {
        restoreSelectedRows('#flexi');
        var qry = (_Context.id == -1) ? 'tabletworkflowinsert' : 'tabletworkflowupdate';
        return newMastercallback(v, m, f, 'TabletWorkFlow', qry, 'TabletWorkFlowItems', 'tabletworkflowitemsupdate', 'tabletworkflowitemsinsert', 'tabletworkflowitemsdelete', 'GroupingID', '#subDetailForm', show);
    }
    function show(id, celDiv, successCallback) {
        saveTrSelected(id);
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }
    // ------------------------------
    // startup code
    // ------------------------------
    dateSelect('.dateSelect');
    checkAccess(['distribution,tabletworkflow']);
    setClassDistribution();
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.TabletWorkFlow, parentTitle: Lang.MDistribution, parentUrl: 'menu/distribution.aspx' } }));
    run();
}