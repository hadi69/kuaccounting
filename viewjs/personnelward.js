﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-11-01: file created
*/
function view_personnelward() {


    function autoPostPack(event, data, formatted) {
        $(event.target).next().val(data[0]); // ID
    }


    // -------------------------
    // Startup code
    // -------------------------
    autoComplete('Ward', 'wardauto', '#WardName', '#WardID');

    insertValidation();
}