﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-11-23: file created
*/
function view_documentfield() {
    // ------------------------------
    // startup code
    // ------------------------------
    if (_Context.id > 0) {
        $("#Name").attr("readonly", "readonly");
        $("#Display").attr("readonly", "readonly");
    }
    var more = "Used=1";
    autoComplete('Fields', 'enumauto1', "#DisplayValue", "#Value"
            , null, null, null, more);
    insertValidation();
}