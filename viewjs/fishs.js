﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
93-05-14: file created
*/
function view_fishs() {
    var formName = "";
    var personid=$.urlParam("pid");
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + "&more=" + encodeURIComponent("PersonnelID=" + personid) + appendTime();
        $("#flexi").flexigrid
            ({
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 20, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell'},
                    { display: Lang.PersonnelName, name: 'PersonnelName', sortable: true, css_class: 'width-200'},
                    { display: Lang.OrderDefName, name: 'FishDefName', sortable: true},
                    { display: Lang.Year, name: 'FishYear', sortable: true, css_class: 'width-100 align-center'},
                    { display: Lang.Month, name: 'FishMonth', sortable: true, css_class: 'width-100 align-center'},
                    { display: Lang.Date, name: 'Date', sortable: true, css_class: 'date-cell' },
                    { display: Lang.Status, name: 'Status', sortable: true, process: displayStatus, css_class: 'width-80 align-center' },
                    { display: Lang.PersonnelWard, name: 'PersonnelWard', sortable: false, process: showPersonnelWard, css_class: 'action-cell' },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' },
                    { display: Lang.ColDel, name: 'Del', sortable: false, process: delLink, css_class: 'action-cell' }
                ],
                buttons:
                [
                    { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                    { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                    { separator: true },
                    { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                    { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                sortname: "FishYear,FishMonth",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                }
            }
            );
    }
    function displayStatus(celDiv, id) {
        var status = parseInt($(celDiv).html(), 10);
        displayFishStatusEnum(celDiv, status);
    }

    function showPersonnelWard(celDiv, id) {
        $(celDiv).html('<a href="list.aspx?table=PersonnelWard&fid=' + id + '&pid=' + $.urlParam("pid") + '"><img src="img/task_icon.jpg" /></a>');
    }
    //insert fish
    
    
    function ordershowcallback(v, m, f) {
        // ex: formName + 'update' --> rule86update
        if (v == 'Calc') {
            calcOrder(v, m, f, f.ConstID, "", f.OrderDefID, "", '#detailFormOrder');
            return false;
        }
             
        if (v == 'Another') {
            show(f.ID);
            return true;
        }
        insertOrder(v, m, f, 'orderupdate', 'orderinsert', 'Orders', '#detailFormOrder', show);
        _Context.id = $("#ID").val();
        return true;
    }
    function showcallback(v, m, f) {
        // ex: formName + 'update' --> fishrule86update
        if (v == 'Calc') {
            calcOrder(v, m, f, "", f.OrderID, f.FishDefID, "", '#detailFormFish');
            return false;
        }
            
        if (v == 'Another') {
            _Context.id = f.ID;
            show(f.ID);
            return true;
        }
        if (v == 'Order') {
            showOrder(f.OrderID);
            return false;
        }
        if (v == "Print") {
            popup(Router.Generate('print-fish', { params: { id: _Context.id } }));
            return false;
        }
        if (v == 'Lock') {
            $("#LockStatus").val(1);
            $(".jqibuttons").find(":button[value='Lock']").last().hide();
            $(".jqibuttons").find(":button[value='Unlock']").last().show();
        }
        if (v == 'Unlock') {
            $("#LockStatus").val(0);
            $(".jqibuttons").find(":button[value='Unlock']").last().hide();
            $(".jqibuttons").find(":button[value='Lock']").last().show();
        }
        if (v == 'OK') {
            var lockStatus = getServerValue('getfishlockstatus', 'Fish', "&ID=" + f.ID);
            if (lockStatus == 1) {
                alert(Lang.FishLocked);
                return false;
            }
        }
        var res = insertOrder(v, m, f, 'fishupdate', 'fishinsert', _Context.table, '#detailFormFish', show);
        if (v == 'OK') {
            show(_Context.id);
        }
        if(v=='Cancel')
        return res;
    }
    function show(id, celDiv, successCallback,pid) {
        _Context.id = id;
        formName = getServerValue('fishformshow', 'FishDefs', null); // ex: fishrule86
        var btns = getDefaultButtons(true);
        btns[Lang.Calculate+' F4'] = 'Calc';
        //btns[Lang.Copy] = 'Copy';
        btns['Another'] = 'Another';
        btns[Lang.Lock] = 'Lock';
        btns[Lang.Unlock] = 'Unlock';
        btns[Lang.Orders] = 'Order';
        var m = '&ID=' + personid;
        var fishDefID = getServerValue("getfishdefID","FishDefs",null);
        var personName = getServerValue("selectpersonname", "Personnel", m);
        var _url = 'get.aspx?op=GetHtmlOrder&table=' + _Context.table + '&view=WorkerFish&qd=' + _Context.query + '&id=' + id + '&PersonnelID=' + personid + '&PersonnelName=' + personName + "&FishDefID=" + fishDefID + "&type=Fish" + appendTime();
        showPopup(_url, showcallback, btns, null, successCallback);
    }

    function showOrder(id, celDiv, successCallback) {
        _Context.id = id;
        var btns = getDefaultButtons();
        btns[Lang.Calculate] = 'Calc';
        btns['Another'] = 'Another';
        formName = getServerValue('orderformshow', 'OrderDefs', null); // ex: rule86
        var orderdefID = getServerValue('getorderdefID', 'OrderDefs', null);
        var m = '&ID=' + personid;
        var personName = getServerValue("selectpersonname", "Personnel", m);
        var _url = 'get.aspx?op=GetHtmlOrder&table=Orders&view=WorkerOrder&qd=orders&id=' + id + '&PersonnelID=' + personid + '&PersonnelName=' + personName + '&OrderDefID=' + orderdefID + "&type=Order" + appendTime();
        showPopup(_url, ordershowcallback, btns, null, successCallback);
    }
    // ------------------------------
    // startup code
    // ------------------------------
    $("#bar").prepend(breadcrumb({
        title: _Context.title,
        grandParent: {
            title: Lang.MSalary,
            url: 'menu/salary.aspx'
        },
        parent: {
            title: Lang.Staff,
            url: 'list.aspx?table=Personnel&menu=salarysa&view=staffs'
        }
    }));
    setClassBasic();
    run();
}