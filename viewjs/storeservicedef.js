﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-11-08: bindStoreServices added
    92-11-05: file created
*/
function view_storeservicedef() {
    var mID = -1;
   
    function bindStoreServices() {
        var _url = 'service.asmx/GetQueryList?table=StoreServices&qd=storeservices&StoreServiceDefID=' + mID + "&StoreID=" + $('#StoreID').val() + appendTime();
        $("#flexiItems").flexigrid(getFlexiOptions(_url, [
                { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                { display: Lang.ExpiryDate, name: 'ExpireDate', sortable: true, css_class: 'date-cell' },
                { display: Lang.Income, name: 'Income', sortable: true, css_class: 'width-250' },
                { display: Lang.Outcome, name: 'Outcome', sortable: true },
                { display: Lang.Cardex, name: 'Cardex', sortable: false, process: function (celDiv, id) { cardexLink(celDiv, id); }, css_class: 'action-cell' }
        ]));
    }
    function cardexLink(celDiv, id) {
        $(celDiv).html(Lang.Cardex).click(function () {
            var store = $('#StoresTitle').val();
            var storeservice = $('#ServiceName').val();
            var _url = 'get.aspx?op=GetHtml&table=Cardex&view=cardex&SSDefID=' + $('#ID').val() + '&StoreTitle=' + store + '&StoreServiceName=' + storeservice + '&SSID=' + id + '&PerStoreService=1' + appendTime();
            showPopup(_url, function (v, m, f) {
                if (v == "Print")
                    return generalPrint(v, m, f, '#detailForm');
                return true;
            }, getCloseAndPrintButtons());
        });
    }
    // ------------------------------
    // startup code
    // ------------------------------
    mID = toInt($('#ID').val(), -1);
    if (mID == -1)
        autoCompleteFree('ServicePrice', 'servicepriceauto', '#ServiceName', '#ServicePriceID');
    else
        $('#ServiceName').attr('readonly', 'readonly');
    var more = 'StoreID=' + $("#StoreID").val();
    autoComplete('StoreLayout', 'storlayoutauto', '#StoreLayoutName', '#StoreLayoutID', null, null, null, more, null);

    bindStoreServices();
    insertValidation();

    $('#StoreLayoutName').focus();
    if (mID == -1)
    $('#ServiceName').focus();

    if (mID == -1)
    {
        $('#Income,#Outcome').attr('readonly', null);
    }
    $('#ServiceName').blur(function () {
        if (mID <= 0) {
            var ok = getServerValue('checkservicepriceinstore', 'StoreServiceDef', '&ServicePriceID=' + $('#ServicePriceID').val() + '&StoreID=' + $('#StoreID').val());
            if (ok == 1) {
                Growl.Error(Lang.StoreServiceDefMsg, 3000);
                $('#ServiceName').val('');
                $('#ServicePriceID').val('');
            }
        }
    });
}