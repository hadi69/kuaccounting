﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
    92-10-16: file created
*/
function view_rates() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        //alert(_url);
        $("#flexi").flexigrid
                (
                {
                    url: _url,
                    dataType: 'xml',
                    colModel:
                    [
                        { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                        { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                        { display: Lang.Title, name: 'Title', sortable: true, css_class: 'width-320' },
                        { display: Lang.StartDate, name: 'StartDate', sortable: true, process: showDate, css_class:'date-cell' },
                        { display: Lang.StartDate, name: 'StartDate', sortable: true, process: showDate, css_class: 'date-cell' },
                        { display: Lang.Enabled, name: 'Enabled', sortable: true, process: showBool, css_class: 'width-40 align-center' },
                        { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' },
                        { display: Lang.RateItem, name: 'RateItem', sortable: false, process: rateitemDefinition, css_class: 'action-cell' },
                        { display: Lang.Benefits, name: 'Benefits', sortable: false, process: benefitDefinition, css_class: 'width-100 align-center' }
                    ],
                    buttons:
                    [
                        { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                        { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                        { separator: true },
                        { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                    ],
                    searchitems: null,
                    sortname: "Title",
                    sortorder: "asc",
                    usepager: true,
                    useRp: true,
                    rp: gridRp,
                    showTableToggleBtn: gridToggle,
                    width: gridWidth,
                    height: gridHeight, resizable: gridResizable,
                    singleSelect: false,
                    onDoubleClick: function (row, grid, options) {
                        var id = row.id.substring(3);
                        show(id);
                    }
                });
    }
    function rateitemDefinition(celDiv, id) {
        $(celDiv).html('<a href="list.aspx?table=RateItem&id=' + id + '"><img src="img/task_icon.jpg" /></a>');
    }
    function benefitDefinition(celDiv, id) {
        $(celDiv).html('<a href="list.aspx?table=Benefits&id=' + id + '"><img src="img/transition_icon.jpg" /></a>');
    }
    function showcallback(v, m, f) {
        return newShowcallback(v, m, f, 'rateupdate', 'rateinsert', _Context.table, null, show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        saveSelectedRows('#flexi');
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, function () {
            jscolor.init();
            if (successCallback)
                successCallback();
        });
    }

    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['salary,rate']);
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.Rate, parentTitle: Lang.MSalary, parentUrl: 'menu/salary.aspx' } }));
    setClassSys();
    run();
}