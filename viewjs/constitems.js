﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
93-05-14: file created
*/
function view_constitems() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var terminology = getTerminology(_Context.table);
        if (terminology)
            $('#term').html("<a href='help/terminology.aspx' target=_blank title='" + Lang.HasTerminology + "'>" + terminology + "</a>");
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeileNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.headline, name: 'Title', sortable: true, css_class: 'width-300'},
                    { display: Lang.Name, name: 'Name', sortable: true},
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' },
                    { display: Lang.ColDel, name: 'Del', sortable: false, process: delLink, css_class: 'action-cell' }
                ],
                buttons:
                [
                    { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                    { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                    { separator: true },
                    { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                    { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                sortname: "Title",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight,
                singleSelect: false
            }
            );
    }
    function showcallback(v, m, f) {
        return newShowcallback(v, m, f, 'ConstItemsupdate', 'ConstItemsinsert', _Context.table, null, show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    $("#bar").prepend(breadcrumb({ title: Lang.ConstItems, grandParent: { title: Lang.MSalary, url: 'menu/salary.aspx' }, parent: { title: Lang.SalaryDefinition, url: 'menu/salarydefinition.aspx' } }));
    setClassSalary();
    run();
}