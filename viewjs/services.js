﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-12-04: file created
*/
function view_services() {
    var selectedIDs = -1, selectedPatientNames = -1, selectedIndex = -1, selectedPatientFullNames = -1, mCmd = -1;
    var SID = -1;
    var parentRelatedId = -1;
    function doCommand(com, grid) {
        if (com == Lang.UpdateCodings){
            var isOk = getServerValue('updateservicepricecodings', 'ServicePrice', null);
            if (isOk) {
                Growl.Notice(Lang.Success);
            } else {
                Growl.Notice(Lang.Failed);
            }
            return;
        }else if (com == Lang.GBBarCodesPrint) {
            var ids = '';
            for (var i = 0; i < _checkedRowIDs.length; i++) {
                ids += _checkedRowIDs[i] + '|';
            }
            if (ids.length > 0) {
                _checkedRowIDs = [];
                $('.mastercheckmark').attr('checked', '');
                $("#flexi").flexReload();
                ids = ids.substring(0, ids.length - 1);

                var msg = '<div class="container">' +
                    '<div class="row margin-20-top">' +
                    '<div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">' +
                    '<div class="form-group">' +
                    '<label for="NumOfRow" class="left-aligned-label control-label col-xs-12 col-sm-4">'
                    + Lang.NumOfRow + '</label>' +
                    '<div class="col-xs-12 col-sm-8">' +
                    '<input type="text" id="NumOfRow" name="NumOfRow" class="form-control input-sm" />' +
                    '</div></div></div>' +
                    ' <div class="col-xs-12 col-sm-10 col-md-6 col-md-offset-2 form-group-container">'
                   // +'<div class="form-group">'
                  //  + '<label class="left-aligned-label control-label col-xs-12 col-sm-4">'
                  //  + Lang.HasServiceVariat
                  //  + '</label>'
                  //  + '<div class="col-xs-12 col-sm-1">'
                  //  + '<input type="checkbox" id="HasServiceVariat" name="HasServiceVariat" />'
                  //  + '</div></div>'
                    + '</div></div></div>';

                var btns = {};
                btns[Lang.Print] = 'Print';
                btns[Lang.Cancel] = 'Cancel';
                $.prompt(msg, {
                    submit: function (v, m, f) {
                        if (v == 'Print') {
                            var hasServiceVariat = 0;
                            var numOfRow = f.NumOfRow;
                            if (f.HasServiceVariat)
                                hasServiceVariat = 1;
                            var ok = getServerValue('barcodeprint', 'BarCodePrint', '&Ids=' + ids.replace(/\|/g, ',') + '&NumOfRow=' + numOfRow);
                            if (toInt(ok, 0) === 1) {
                                alert(Lang.BarCodeSucces);
                            } else {
                                Growl.Error(Lang.Error);
                            }
                            //window.open('file:///C:Users/Administrator/Downloads/label.ezpx');
                            // window.open(Router.Generate("print-servicebarcode",
                            //{ params: { ids: ids, numOfRow: numOfRow, hasServiceVariat: hasServiceVariat}, query: { print: "true"} }));
                        } else {
                            $("#flexi").flexReload();
                            return true;
                        }
                    },
                    buttons: btns,
                    focus: 0
                });

            } else {
                Growl.Error(Lang.PleseSelectAService, 3000);
            }
        }else if (com == Lang.Forms) {
            showforms();
        }
        else
            generalCommand(com, grid, show);
    }
    //شروع فرم ساز
    function addCmd(cmd, text, onlyView) {
        $('#cmdsDiv').append("<a href='" + cmd + "' class='cmd'>" + text + "</a>");
    }
    function prepareSelection() {
        //در این قسمت تعداد رکوردهایی که انتخاب شده اند مشخص می شوند و مقدار آنها در آرایه ذخیره می شوند
        selectedIDs = getRowIDs("#flexi");
        if (selectedIDs.length == 0)
            return false;
        selectedPatientNames = getGridRowContents("#flexi", 3);
        //            for (var i = 0; i < selectedStatueses.length; i++)
        //                selectedStatueses[i] = getStatus(selectedStatueses[i]);
        selectedIndex = -1; //شمارنده آرایه هاست
        return true;
    }
    function handleNextForm() {
        selectedIndex++;
        var formDeclareID = mCmd.substring(1);
        _Context.id = -1;
        var _url = 'get.aspx?op=GetForm&id=-1&OtherID=' + selectedIDs[selectedIndex] + "&OtherTitle=" + encodeURIComponent(selectedPatientNames[selectedIndex])
            + '&PersonnelID=' + _Context.personnelID + '&FormDeclareID=' + formDeclareID + appendTime();
        showPopup(_url, showFormDeclareCallback);
        return;
    }
    function bindEvent() {
        $('.cmd').click(function () {
            if (!prepareSelection()) {
                alert(Lang.ChoosePatients);
                return false;
            }
            $('.cmdDiv').hide();
            mCmd = $(this).attr('href');
            handleNextForm();
            return false;
        });
    }

    function prepare() {
        var _url = 'get.aspx?op=GetXml&qd=formdeclaredisplay&table=FormDeclare&FormType=' + FormTypeEnum.Service;
        $.ajax({
            url: _url,
            type: 'GET',
            dataType: 'xml',
            async: false,
            success: function (xmlDoc) {
                var xNode = (xmlDoc.firstChild.nextSibling) ? xmlDoc.firstChild.nextSibling : xmlDoc.firstChild;
                var items = xNode.childNodes[0];
                // Bind Items
                $(items).find("R").each(function () {
                    var node = $(this);
                    addCmd("F" + node.attr("ID"), node.attr("Title"));
                });
            },
            error: ajaxError
        });
    }
    function prepareSelection() {//این تابع از رکوردی که انتخاب شده است اطلاعات لازم را استخراج می کند
        selectedIDs = getRowIDs("#flexi"); //میقدار ID  رکورد انتخابی را بر می گرداند
        if (selectedIDs.length == 0)
            return false;
        selectedPatientNames = getGridRowContents("#flexi", 3); //میقدار سومین ستون را برمی گرداند
        selectedPatientFullNames = getGridRowContents("#flexi", 4);
        selectedIndex = -1;
        return true;
    }
    function showforms() {
        killEvent();
        $('.cmdDiv').hide();
        $('html').unbind('click');
        var offset = $(".forms").offset();
        $('#cmdsDiv').offset({ top: offset.top + 10, left: offset.left - $('#cmdsDiv').width() }); //$('#usermenu').width()
        $('#cmdsDiv').show();
        $('#cmdsDiv').offset({ top: offset.top + 10, left: offset.left - $('#cmdsDiv').width() }); //
        $('html').click(function () {
            $('#cmdsDiv').hide();
        });
        return false;
    }
    //پایان فرم ساز
    function run() {
        prepare();
        bindEvent();
        var _url = 'service.asmx/GetList?table=Service&qd=service' + appendTime();
        if (_Context.isStoreUser) {
            _url = 'service.asmx/GetList?table=Service&qd=storeuserservice' + appendTime();
        }
        var btns =
            [
                { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                { name: Lang.Forms, bclass: 'forms', onpress: doCommand, onaccess: hasAccess("Service", "service", "forms") },
                { separator: true },
                { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand },
                { name: Lang.GBBarCodesPrint, bclass: 'barcodesprint', onpress: doCommand }
            ];
        if (_Context.isAccountingVer)
            btns =
            [
                { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                { separator: true },
                { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand },
                { name: Lang.GBBarCodesPrint, bclass: 'barcodesprint', onpress: doCommand },
                { name: '<input type="checkbox" Title="' + Lang.ChooseAllForBarCodePrint + '" class="mastercheckmark" />', bclass: '' }
            ];
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.Code, name: 'Code', sortable: true, css_class: 'width-100 align-center' },
                    { display: Lang.BarCode, name: 'BarCode', sortable: true, css_class: 'width-120 align-center' },
                    { display: Lang.Title, name: 'Title', sortable: true },
                    { display: Lang.ServiceType, name: 'ServiceTypeName', sortable: true, css_class: 'width-150' },
                    { display: Lang.Enabled, name: 'Enabled', sortable: true, process: showBool, css_class: 'width-40 align-center' },
                    { display: Lang.Pricing, name: 'Pricing', sortable: true, hide: true },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' },
                    { display: Lang.Cardex, name: 'Cardex', sortable: false, process: showCardex, css_class: 'width-70 align-center' },
                    { display: Lang.ColDel, name: 'Del', sortable: false, process: delLink, css_class: 'action-cell' },
                    { display: Lang.CheckMark, name: 'CheckMark', sortable: false, process: function (celDiv, id) { checkMark(celDiv, id, show); }, css_class: 'action-cell' }
                ],
                buttons: btns,
                sortname: "Title",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight, resizable: gridResizable,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                },
                onSuccess: function (grid) {
                    bindCheckBoxClick();
                    restoreCheckedRows(grid);
                    bindMasterCheckBox();
                    restoreSelectedRows('#flexi');
                }
            }
            );
        //            $('#ServiceTypeName').change(function () {
        //                refresh();
        //            });
    }
    function showCardex(celDiv, id) {
        $(celDiv).html('<img src="img/task_icon.jpg" />');
        $(celDiv).click(function (event) {
            event.preventDefault();
            if (clicksDisabled)
                return;
            disableClicks();
            showCardexPopup(id, this);
        });
    }
    function showcallback(v, m, f) {
        if (v == 'Documents') {
            if (_Context.id > 0) {
                var _url = "get.aspx?op=GetHtml&table=ServiceImage&ID=" + _Context.id;
                showPopup(_url, null, getCloseButtons());
            }
            return false;
        }
        if (v == 'NewRelated') {
            show(-1);
        }
        if (v == 'Repeat') {
            show(_Context.id, null, null, 1);
        }
        if (!$('.modelname').val() && v == 'OK') {
            Growl.Notice(Lang.AddARowPlease, 4000);
            return false;
        }
        if (_Context.isStoreUser) {
            if (!$('.groupingname').val() && v == 'OK') {
              Growl.Notice(Lang.AddServiceGroupPlease, 4000);
                return false;
            }
        }
        restoreSelectedRows('#flexi');
        $('.StartDate').attr('name', 'StartDate');
        $('.EndDate').attr('name', 'EndDate');
        $('.StartDatesp').attr('name', 'StartDatesp');
        var qry = (_Context.id == -1) ? 'serviceinsert' : 'serviceupdate';
        return newMasterscallback(v, m, f, 'Service', qry, 'Normal,ServiceRelation,PanelServices,ServiceItems,ServicePrice,ServiceSpeciality,ServiceUnits,GroupingMember,ServiceVariate'
                , 'normalupdate,servicerelationupdate,panelserviceupdate,serviceitemupdate,servicepriceupdate,servicespecialityupdate,serviceunitsupdate,servicegroupingupdate,servicevariateupdate'
                , 'normalinsert,servicerelationinsert,panelserviceinsert,serviceiteminsert,servicepriceinsert,servicespecialityinsert,serviceunitsinsert,servicegroupinginsert,servicevariateinsert'
                , 'normaldelete,servicerelationdelete,panelservicedelete,serviceitemdelete,servicepricedelete,servicespecialitydelete,serviceunitsdelete,servicegroupingdelete,servicevariatedelete'
                , 'KitID,RelatedID,SubServiceID,ItemName,ModelName,SpecialtyID,UnitsName,GroupingID,VarBarCode'
                , null, '#subDetailForm', show);

    }
    function showCardexPopup(id, celDiv, successCallback) {
        _Context.id = id;
        $('.trSelected').removeClass('trSelected');
        var _url = 'get.aspx?op=GetHtml&table=service&view=cardexservice&id=' + id + appendTime();
        var btns = getButtons(true);
        showPopup(_url, function () { }, btns, null, successCallback);
    }
    function show(id, celDiv, successCallback, repeat) {
        if (parentRelatedId == -1 && id == -1){
             parentRelatedId = toInt($('#ParentRelatedID').val(), -1);
            if (parentRelatedId == -1) {
                parentRelatedId = toInt($('#MasterID').val(), -1);
           }
        };
           var serviceTypeID = "";
           var serviceTypeName = "";
        var more = '&ServiceTypeName=' + encodeURIComponent(serviceTypeName) + '&ServiceTypeID=' + serviceTypeID;
        _Context.id = id;

        $('.trSelected').removeClass('trSelected');
        $('#row' + id).addClass('trSelected');
        saveSelectedRows('#flexi');
        $('.trSelected').removeClass('trSelected');
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + '&ParentRelatedID=' + parentRelatedId + '&Repeat=' + repeat + appendTime();
        _url += '&more=' + more;
        var btns = getBtn(id);
        showPopup(_url, showcallback, btns, null, successCallback);
    }

    function getBtn(id) {
        var btns = {};
        btns[Lang.OK] = 'OK';
        btns[Lang.GBNew] = 'New';
        if (id > 0) {
            btns[Lang.NewRelated] = 'NewRelated';
            btns[Lang.Documents] = 'Documents';
            btns[Lang.Repeat] = 'Repeat';
        }
        btns[Lang.Close] = 'Cancel';
        return btns;
    }
    function checkMark(celDiv, id, funcShow) {
        funcShow = funcShow || show;
        $(celDiv).html('<input type="checkbox" class="checkmark" />');
    }
    function bindCheckBoxClick() {
        $('.checkmark').click(function () {
            var elem = $(this);
            var id = elem.parent().parent().parent().attr('id');
            id = id.substring(id.lastIndexOf('row') + 3);
            if (elem.attr('checked')) {
                saveCheckedRows(id);
            } else {
                saveDisCheckedRows(id);
            }
        });
    }
    function bindMasterCheckBox() {
        $(".mastercheckmark").click(function () {
            var masterElem = $(this);
            if (masterElem.attr('checked')) {
                $('.checkmark').each(function () {
                    var elem = $(this);
                    var id = elem.parent().parent().parent().attr('id');
                    id = id.substring(id.lastIndexOf('row') + 3);
                    saveDisCheckedRows(id);
                    saveCheckedRows(id);
                    $("#flexi").flexReload();
                });
            } else {
                $('.checkmark').each(function () {
                    var elem = $(this);
                    var id = elem.parent().parent().parent().attr('id');
                    id = id.substring(id.lastIndexOf('row') + 3);
                    saveDisCheckedRows(id);
                    $("#flexi").flexReload();
                });
            }
        });
    }
    // ------------------------------
    // startup code
    // ------------------------------
    if (_Context.isStoreUser) {
        $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.Service } }));
    } else {
        checkAccess(['stores,service']);
        if ($.urlParam('menu') == 'storea') {
            $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.Service, parentTitle: Lang.SellAndBuy, parentUrl: 'menu/sales.aspx' } }));
            setClassStore();
        }
        else {
            $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.Service, parentTitle: Lang.SellAndBuy, parentUrl: 'menu/sales.aspx' } }));
            setClassService();
        }
    }
    autoCompleteFree('Supplier', 'supplierauto', "#fModelName", "#justfurfun"
    , null
    , function (data, i, n, value) {
        return data[1] + ' (' + data[2] + ')';
    });
    run();
}