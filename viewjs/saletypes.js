﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
93-08-24: file created
*/
function view_saletypes() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        $("#flexi").flexigrid
                (
                {
                    url: _url,
                    dataType: 'xml',
                    colModel:
                    [
                        { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'center', hide: true },
                        { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                        { display: Lang.Title, name: 'Title', sortable: true},
                        { display: Lang.Default, name: 'IsDefault', sortable: true, process: showBool, css_class: 'width-60 align-center' },
                        { display: Lang.ColEdit, name: 'Edit', sortable: false, hide: hideColumn("manager", "enums", "edit"), process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' },
                        { display: Lang.ColDel, name: 'Del', sortable: false, hide: hideColumn("manager", "enums", "delete"), process: delLink, css_class: 'action-cell' }
                    ],
                    buttons:
                    [
                        { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                        { name: Lang.GBEdit, bclass: 'edit', onpress: doCommand },
                        { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                        { separator: true },
                        { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                        { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                    ],
                    searchitems: null,
                    sortname: "Title",
                    sortorder: "asc",
                    usepager: true,
                    //title: _Context.title,
                    useRp: true,
                    rp: gridRp,
                    showTableToggleBtn: _options.gridToggle,
                    width: _options.gridWidth,

                    height: _options.gridHeight, resizable: _options.gridResizable,
                    singleSelect: true,
                    onDoubleClick: function (row, grid, options) {
                        var id = row.id.substring(3);
                        show(id);
                    },
                    onSuccess: function (grid) {
                        restoreSelectedRows('#flexi');
                    }
                }
                );
    }
    function showcallback(v, m, f) {
        var qry = (_Context.id == -1) ? 'saletypeinsert' : 'saletypeupdate';
        return newMastercallback(v, m, f, 'SaleType', qry, 'SaleTypeDiscount', 'saletypediscountupdate', 'saletypediscountinsert', 'saletypediscountdelete', 'ServiceGroupName', '#subDetailForm', show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        saveSelectedRows('#flexi');
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }

    // ------------------------------
    // startup code
    // ------------------------------
        checkAccess(['sales,saletype']);
        checkLevel(3);
        setClassSales();
    checkLevel(2);
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.SaleType, parentTitle: Lang.SellAndBuy, parentUrl: 'menu/sales.aspx' } }));
    
    run();
}