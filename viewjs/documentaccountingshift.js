﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
93-04-10: 
- file created
*/
function view_documentaccountingshift() {

    function formatDocumentAccountingResult(data, value) {
        return data[0];// + ' - ' + (data[1] || '<' + Lang.NoComments + '>');
    }

    autoComplete('DocumentAccounting', 'documentaccountingsearch', '#FromNo', '#SFromNo', null, formatDocumentAccounting, formatDocumentAccountingResult);
    autoComplete('DocumentAccounting', 'documentaccountingsearch', '#ToNo', '#SToNo', null, formatDocumentAccounting, formatDocumentAccountingResult);
    insertValidation();
    $('#ToNo').focus();
    $('#FromNo').focus();
}