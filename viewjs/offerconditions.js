﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
92-10-19: file created
*/
function view_offerconditions() {
    
    // -----------------------------
    // startup code
    // ------------------------------
    makeChosen();
    $('#NewGroup').click(function () {

        var oldID = _Context.id;
        var oldTable = _Context.table;
        var oldQuery = _Context.query;
        function showcallback(v, m, f) {
            if (v == 'OK') {
                masterscallbackScalar(v, m, f, 'Grouping', 'groupinginsert', 'GroupingMember', 'groupingmemberupdate', 'groupingmemberinsert', 'groupingmemberdelete', 'OtherName', null, '#subDetailForm');
                if (!_Context.success)
                    return false;
                 }
            _Context.id = oldID;
            _Context.table = oldTable;
            _Context.query = oldQuery;
        }
        _Context.id = -1;
        _Context.table = 'Grouping';
        _Context.query = 'grouping';
        var _url = 'get.aspx?op=GetHtml&table=Grouping&qd=grouping&id=-1' + appendTime();
        //alert(_url);
        showPopup(_url, showcallback, getButtons());
    });
    setValue('#ConditionType');
    setValue('#MergeType');
    autoComplete('Grouping', 'groupauto', "#GroupingName", "#GroupingID");
    insertValidation('#detailForm');
}
   