﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
92-10-29: 
    - file created
*/
function view_purchases() {
    var storeMode = true, totalPriceColmunHideshow = false;;
    var startupID = -1;
    function doCommand(com, grid) {
        if (com == Lang.GBRefuse) {
            createRefuse(grid);
        }
        else {
            var isNew = com == Lang.GBNew;
            if (!isNew) {
                var btn = $(".fbutton span:contains(" + com + ")");
                if (btn.hasClass('new'))
                    isNew = true;
            }
            if (isNew) {
                var more = '&Type=' + WorkflowTypeEnum.Purchase;
                var count = getServerValue('checkworkflow', 'Workflow', more);
                if (count == 0 || count == null) {
                    displayError(Lang.WorkflowM1);
                    return;
                }
            }
            if (isNew && com != Lang.GBNew) {
                var desiredEntryTaskID = $(".fbutton span:contains(" + com + ")").attr('class').substring('new '.length);
                //alert(desiredEntryTaskID);
                purchaseshow(-1, null, null, desiredEntryTaskID);
            }
            else
                generalCommand(com, grid, purchaseshow);
        }
    }

    function run() {
        autoComplete('TasK', 'purchasephase', "#fTaskName", "#fTaskID");
        var btns = getTaskGridButtons(WorkflowTypeEnum.Purchase, doCommand, "sales", "purchase");
        btns[btns.length] = { name: Lang.GBRefuse, bclass: 'refuse', onpress: doCommand, onaccess: hasAccess('sales', 'refuse', "new") && hasGeneralAccess("new") };

        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + (_Context.workflowInheritance ? 'purchase' : 'purchasenoinheritance') + "&more=__PrOgrAmconD__" + appendTime();
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell'},
                    { display: Lang.FactorNo, name: 'NOSerial', sortable: true, css_class: 'width-70 align-center' },
                    { display: Lang.SellerNOSerial, name: 'SellerNOSerial', hide: true },
                    { display: Lang.FactorDate, name: 'ReceiptDate', sortable: true , css_class: 'date-cell' },
                    { display: Lang.Date, name: 'Date', sortable: true, hide: true, css_class: 'date-cell'},
                    { display: Lang.Store, name: 'StoreName', sortable: true, css_class: 'width-70 align-center' },
                    { display: Lang.Comments, name: 'Comments', sortable: true, hide: true },
                    { display: Lang.Inventory, name: 'StoreKeeperName', sortable: true, hide: true},
                    { display: Lang.Accountant, name: 'AccountantName', sortable: true, hide: true},
                    { display: Lang.Seller, name: 'SellerName', sortable: true, css_class: 'width-200 align-center' },
                    { display: Lang.TotalPrice, name: 'TotalPrice', hide:totalPriceColmunHideshow, sortable: true, process: showMonetary, css_class: 'width-120 align-center', footer: 'sum' },
                    { display: Lang.TotalAmount, name: 'TotalAmount', sortable: true, css_class: 'width-80 align-center', footer: 'sum' },
                    { display: Lang.PreStatus, name: 'PreTaskName', sortable: true, css_class: 'task-cell' },
                    { display: Lang.Status, name: 'TaskName', sortable: true, css_class: 'task-cell' },
                    { display: '', name: 'WorkflowColor', css_class: 'WorkflowColor', hide: true },
                    { display: Lang.Status, name: 'VoucherID', hide: true, process: displayVoucherID },
                     { display: Lang.Status, name: 'DocItemID', hide: true, process: displayDocItemID },
                    { display: Lang.DocumentNumber, name: 'VoucherNo', sortable: false, process: displayVoucherNo, hide: _Context.isMobile, css_class: 'width-80 align-center' },
                    { display: Lang.NumPrints, name: 'NumPrints', sortable: true, hide: true },
                    { display: Lang.Status, name: 'PurchaseRequestID', hide: true, process: displayPurchaseRequestID },
                    { display: Lang.PurchaseRequestNo, name: 'PurchaseRequestNo', sortable: false, process: displayPurchaseRequestNo, hide: _Context.isMobile },
                    { display: '', name: 'PayDate', hide: true },
                    { display: '', name: 'PayStatus', hide: true },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, purchaseshow); }, hide: hideColumn("sales", "purchase", "edit"), css_class: 'action-cell' }
                ],
                buttons: btns,
                searchitems: null,
                sortname: "NOSerial",
                sortorder: "desc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight, resizable: gridResizable,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    purchaseshow(id);
                },
                onSuccess: function (grid) {
                    postProcessGrid();
                    if (startupID != -1) {
                        purchaseshow(startupID);
                        startupID = -1;
                    }
                }
            });
    }
    function displayVoucherID(celDiv, id) {
        $(celDiv).addClass('_vid');
    }
    function displayDocItemID(celDiv, id) {
        $(celDiv).addClass('_diid');
    }
    function displayPurchaseRequestID(celDiv, id) {
        $(celDiv).addClass('_prid');
    }
    function displayVoucherNo(celDiv, id) {
        $(celDiv).addClass('_vno');
    }
    function displayPurchaseRequestNo(celDiv, id) {
        $(celDiv).addClass('_prno');
    }
    function postProcessGrid()
    {
        setTimeout(function () {
            showVoucherLinks();
            colorTasks();
        }, 500);
    }
    function showVoucherLinks() {
        $('#flexi tr').each(function () {
            var _vid = $('._vid', this).html();
            var _prid = $('._prid', this).html();
            var _docitemid = $('._diid', this).html();
            //alert(_id);
            if (_vid && _vid != '') {
                $('._vno', this).html("<a href='list.aspx?table=DocumentAccounting&id=" + _vid + "&itemid=" + _docitemid + "'>" + $('._vno', this).html() + "</a>");
            }
            if (_prid && _prid != '') {
                $('._prno', this).html("<a href='list.aspx?table=PurchaseRequest&id=" + _prid + "'>" + $('._prno', this).html() + "</a>");
            }
        });
    }
    function _displayWorkflowStatus(celDiv, id) {
        var type = toInt($(celDiv).html(), 0);
        displayWorkflowStatus(celDiv, type);
    }
    
    
    function showrefusecallback(v, m, f) {
        if (v != 'OK' && v != 'Cancel') {
            var ID = '&RelatedID=' + f.MasterID;
            var taskID = getServerValue('gettaskid', 'TaskImpl', ID);
            var user = _Context.userID;
            var more = '&ID=' + taskID + '&NextTaskID=' + v + '&UserID=' + user;
            runQuery('updatetaskimpl', 'TaskImpl', more);
            v = 'OK';
        }
        var qry = (_Context.id == -1) ? 'refuseinsert' : 'refuseupdate';
        return generalMasterscallback(v, m, f, 'Refuse', qry, 'RefuseItem'
                , 'refuseitemupdate'
                , 'refuseiteminsert'
                , 'refuseitemdelete'
                , 'StoreServiceID');
    }
    
    function createRefuse(grid) {
        var ids = '';
        $('.trSelected', grid).each(function () {
            id = $(this).attr('id');
            id = id.substring(id.lastIndexOf('row') + 3);
            ids += id + ',';
        });
        if (ids == '')
            return;
        ids = ids.substr(0, ids.length - 1);
        var refuseID = getServerValue("purchase2refuse", 'Refuse', 'ids=' + ids);
        if (!refuseID)
            displayError(Lang.Purchase2RefuseError);
        else
            displayError(Lang.Purchase2RefuseSuccess + "<br /><a href='list.aspx?table=Refuse&id=" + refuseID + "'>" + Lang.Purchase2RefuseShow + "</a>", null, Lang.Success);
    }
    function createPurchase(grid) {
        //alert(getGridRowContent(grid, 1));
        var _url = 'get.aspx?op=GetHtml&table=Refuse&qd=createrefuse&view=purchasetorefuse&ID=' + getGridRowContent(grid, 1) + appendTime();
        showPopup(_url, showrefusecallback);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['sales,purchaseinvoice', 'stores,purchase']);
    checkLevel(2);
    storeMode = !hasAccess("links", "mainmenu", "accounting") && !hasAccess("links", "mainmenu", "sales");
    storeMode = storeMode || $.urlParam('menu') == 'storea';
    if (storeMode) {
        $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.Purchase, parentTitle: Lang.MStoreForms, parentUrl: 'menu/store.aspx' } }));
        setClassStore();
    }
    else {
        $("#bar").prepend(breadcrumb({pageInfo: {pageTitle: Lang.PurchaseInvoice, parentTitle: Lang.SellAndBuy, parentUrl: 'menu/sales.aspx'} }));
        setClassSales();
    }
    if (storeMode) {
        totalPriceColmunHideshow = true;
    }
    dateSelect('.dateSelect');
    startupID = $.urlParam('id');
    if (!startupID)
        startupID = -1;
    $('#chprint').click(function () { choosePrintFields(_Context.table); });
    run();
}