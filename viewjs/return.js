﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
92-11-12: 
    - file created
*/
function view_return() {
    makeChosen();
    $("#tableItems").smartTable();
    var cnt = 0, mID = -1;
    var storeMode = !hasAccess("links", "mainmenu", "accounting") && !hasAccess("links", "mainmenu", "sales");
    storeMode = storeMode || $.urlParam('menu') == 'storea';
    var pagetitle = "";
    if (storeMode) {
        pagetitle = 'Return';
        replacePrintButton({
            primary: {
                text: Lang.Print,
                href: Router.Generate("print-return", { params: { ids: _Context.id, title: pagetitle }, query: { report: 'returnstoremode', print: "true" } })
                , type: 4, mode: 1, id: _Context.id
            }
        });
    } else {
        pagetitle = 'ReturnInvoice';
        replacePrintButton({
            primary: {
                text: Lang.Print,
                href: Router.Generate("print-return", { params: { ids: _Context.id, title: pagetitle }, query: { print: "true" } })
                , type: 4, mode: 0, id: _Context.id
            }
        });
    }

    function updateTotal() {
        var sumPrice = 0, sumAmount = 0, sumDiscount = 0, sumTax = 0, sumVat = 0, sumPursant = 0;
        $('.total').each(function (index, value) {
            sumPrice += toFloat($(value).val(), 0);
        });
        $('.amount').each(function (index, value) {
            sumAmount += toFloat($(value).val(), 0);
        });
        $('.discount').each(function (index, value) {
            if ($(value).val().indexOf('%') >= 0) {
                var id = $(value).attr('id');
                id = id.substring(id.indexOf('_', 0) + 1); // d_1  -->  1
                var total = toFloat($('#t_' + id).val(), 0); // total
                sumDiscount += toFloat($(value).val().replace('%', ''), 0) * total / 100;
            }
            else
                sumDiscount += toFloat($(value).val(), 0);
        });
        var discount = 0;
        $('.tax').each(function (index, value) {
            if ($(value).val().indexOf('%') >= 0) {
                var id = $(value).attr('id');
                id = id.substring(id.indexOf('_', 0) + 1); // d_1  -->  1
                var total = toFloat($('#t_' + id).val(), 0); // total
                if ($('#d_' + id).val().indexOf('%') >= 0) {
                    discount = toFloat($('#d_' + id).val().replace('%', ''), 0) * total / 100; // discount
                } else {
                    discount = toFloat($('#d_' + id).val(), 0);
                }
                sumTax += toFloat($(value).val().replace('%', ''), 0) * (total - discount) / 100;
            }
            else
                sumTax += toFloat($(value).val(), 0);
        });
        $('.svat').each(function (index, value) {
            if ($(value).val().indexOf('%') >= 0) {
                var id = $(value).attr('id');
                id = id.substring(id.indexOf('_', 0) + 1); // d_1  -->  1
                var total = toFloat($('#t_' + id).val(), 0); // total
                if ($('#d_' + id).val().indexOf('%') >= 0) {
                    discount = toFloat($('#d_' + id).val().replace('%', ''), 0) * total / 100; // discount
                } else {
                    discount = toFloat($('#d_' + id).val(), 0);
                }
                sumVat += toFloat($(value).val().replace('%', ''), 0) * (total - discount) / 100;
            }
            else
                sumVat += toFloat($(value).val(), 0);
        });
        $('.salesmancommission').each(function (index, value) {
            var id = $(value).attr('id');
            id = id.substring(id.indexOf('_', 0) + 1); // d_1  -->  1
            var total = toFloat($('#t_' + id).val(), 0); // total
            sumPursant += toFloat($(value).val(), 0) * total / 100;
        });

        var factorTotalDiscount = $('#FactorTotalDiscount').val();
        if (factorTotalDiscount.indexOf('%') >= 0) {
            factorTotalDiscount = Math.round(toFloat(factorTotalDiscount.replace('%', ''), 0) * toFloat($('#TotalPrice').val(), 0) / 100); // factortotaldiscount
        }
        factorTotalDiscount = toFloat(factorTotalDiscount, 0);

        $('#TotalPrice').val(sumPrice.toFixed(_Context.decimalPoints));
        $('#TotalAmount').val(sumAmount);
        $('#TotalDiscount').val((sumDiscount + factorTotalDiscount).toFixed(_Context.decimalPoints));
        $('#TotalTax').val(sumTax.toFixed(_Context.decimalPoints));
        $('#TotalAwarez').val(sumVat.toFixed(_Context.decimalPoints));
        $('#Pursant').val(sumPursant.toFixed(_Context.decimalPoints));
        var vat = toFloat($('#TotalAwarez').val(), 0);

        var total = sumPrice - toFloat($('#TotalDiscount').val(), 0)
            + vat + toFloat($('#ShipmentPrice').val(), 0) + toFloat($('#TotalTax').val(), 0);
        $('#FinalPrice').val(total.toFixed(_Context.decimalPoints));
        commaInputsSet();
    }

    function updatePrice(was) {
        var id = $(was).attr('id');
        id = id.substring(id.indexOf('_') + 1); // d_1  -->  1
        var price = $('#pr_' + id).val();
        var amount = $('#a_' + id).val();
        var unitCarrency = toFloat($('#ucc_' + id).val(), 1);
        $('#t_' + id).val((toFloat(price, 0) * toFloat(amount, 0) * toFloat(unitCarrency, 0)).toFixed(_Context.decimalPoints));
        updateTotal();
    }
    function autoPostPackStoreService(serviceNameInput, data) {
        serviceNameInput.next().val(data[0]); // StoreServicesID
        serviceNameInput.attr('title', data[19]); // StoreName
        serviceNameInput.next().next().val(data[2]); // ServicePriceID
        var id = serviceNameInput.attr('id');
        id = id.substring(id.indexOf('_') + 1);
        if (toInt(data[22], -1) > 0) // OtherUnitID
        {
            $('#u_' + id).next().val(data[22]); // OtherUnitID
            $('#u_' + id).val(data[23]); // OtherUnitName
            $('#ucc_' + id).val(data[24]); // CurrencyConverter
        } else {
            $('#u_' + id).next().val(data[6]); // unitid
            $('#u_' + id).val(data[7]); // unitname
        }
        var _autourl1 = 'get.aspx?op=Auto&table=Units&qd=storedraftunitsauto&StoreServiceID=' + data[0];
        autoComplete('Units', 'unitsauto', "#tableItems tr:last .unitname", "#justfurfun",
            function (event, data, formatted) {
                var id1 = $(event.target).attr('id');
                id1 = id1.substring(id1.indexOf('_') + 1);
                $(event.target).next().val(data[0]); // ID
                //alert(data[2]);
                if (data[2] && data[2] != 0) {
                    //$('#a_' + id1).next().text('x')
                    $('#ucc_' + id1).val(data[2]); // CurrencyConverter}
                }
                else {
                    //$('#a_' + id1).next().text(' ')
                    $('#ucc_' + id1).val('1');
                }
            }
        , null, null, null, _autourl1);
        // has ProfitPercent from service declaration
        // Price priority:
        // 1- GroupingPrice: col10 | col14 = BuyPrice, col16 = SalePrice
        // 2- ProfitPercent: col10 = BasePrice, col2 = SalePrice
        // 3- Last Prices: col12 = LastSalePrice (TheLPSP), col14 = LastBuyPrice (TheLPBP)
        var price = 0;
        if (data[11]) {
            // ProfitPercent
            if (data[2])
                price = data[2];
            if (data[10])
                $('#bpr_' + id).val(numberWithCommas((toFloat(toMonetary(data[10]), 0.0)).toFixed(_Context.decimalPoints)));
        }
        else {
            // Last Prices
            if (data[12])
                price = data[12];
            if (data[14])
                $('#bpr_' + id).val(numberWithCommas((toFloat(toMonetary(data[14]), 0.0)).toFixed(_Context.decimalPoints)));
        }
        if (data[16]) {
            // GroupingPrice
            price = data[16];
            // BuyPrice is from Old method
        }
        $('#pr_' + id).val(numberWithCommas((toFloat(toMonetary(price), 0.0)).toFixed(_Context.decimalPoints))); // price
        $('#smc_' + id).val(numberWithCommas(data[9])); // salesmancommission

        // ServiceVariate
        if (data.length > 18 && data[17]) {
            $('#varid_' + id).val(data[17]);
        }
        updateTotal();
    }

    function addRow(node) {
        // id="x" --> the validationEngine requires ID
        var readonly = node != null ? " readonly='readonly' " : "";
        var settingTax = "", settingVat = "";
        if (node == null) {
            settingTax = getSettingTax();
            settingVat = getSettingVat();
        }
        var row = '<tr>';
        row += '<td data-th="' + Lang.ColRowNr + '" class="row-cell">' + (cnt + 1) + '</td>';

        row += '<td data-th="' + Lang.StoreService + '">'
            + '<input type="hidden" id=sdi_' + cnt + ' name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="text" id="se_' + cnt + '" title="' + getNodeVal(node, "StoreName") + '" name="StoreServiceName" value="'
            + getNodeVal(node, "StoreServiceName") + '" class="storeservicename form-control input-sm" ' + readonly + ' />'
            + '<input type="hidden" class="storeserviceid" id="sid_' + cnt + '" name="StoreServiceID" value="' + getNodeVal(node, "StoreServiceID") + '" />'
            + '<input type="hidden" name="ServicePriceID" value="' + getNodeVal(node, "ServicePriceID") + '" />'
            + '<input type="hidden" name="PricingID" value="' + getNodeVal(node, "PricingID") + '" />'
            + '<input type="hidden" id="varid_' + cnt + '" name="ServiceVariateID" value="' + getNodeVal(node, "ServiceVariateID", 0) + '" class="ServiceVariateID" />'
            + '</td>';

        row += '<td data-th="' + Lang.Units + '">'
            + '<input type="text" id="u_' + cnt + '" name="UnitName" value="' + getNodeVal(node, "UnitName") + '" class="unitname form-control input-sm" />'
            + '<input type="hidden" name="UnitID" value="' + getNodeVal(node, "UnitID") + '" />'
            + '</td>';

        /*row += '<td data-th="' + Lang.Num + '">'
            + '<input type="text" id="a_' + cnt + '" name="Num" value="' + getNodeVal(node, "Num", 1) + '" class="amount form-control input-sm align-center" />'
            + '</td>';*/

        row += '<td data-th="' + Lang.Num + '">'
       + '<div class="input-group input-group-sm">'
       + '<input type="text" id="a_' + cnt + '" name="Num" value="' + getNodeVal(node, "Num", 0) + '" class="amount form-control width-100-percent min-width-50 align-center" />'
       + '<span class="input-group-addon no-left-border no-right-border">x</span>'
       + '<input readonly="readonly" size="1" id="ucc_' + cnt + '" name="UnitCurrencyConverter" value="' + getNodeVal(node, "UnitCurrencyConverter", 1) + '" class="form-control width-50 align-center" />'
       + '</div>'
       + '</td>';

        if ($('#ReturnFromPastYear').attr('checked')) {
            row += '<td class="acc buyprice" data-th="' + Lang.BuyPrice + '">'
                + '<input type="text" id="bpr_' + cnt + '" name="BuyPrice" value="'
                + getNodeVal(node, "BuyPrice") + '" class="buyprice monetary form-control input-sm align-center validate[custom[integer]]" />'
                + '</td>';
        }

        row += '<td class="acc" data-th="' + Lang.Price + '">'
            + '<input type="text" id="pr_' + cnt + '" name="Price" value="'
            + getNodeVal(node, "Price") + '" class="price monetary form-control input-sm align-center validate[custom[integer]]" />'
            + '</td>';

        row += '<td class="acc" data-th="' + Lang.SalesManCommission + '">' // SalesManComission
            + '<div class="input-group input-group-sm">'
            + '<input type="text" id="smc_' + cnt + '" name="SalesManCommission" value="' + getNodeVal(node, "SalesManCommission", 0) + '" class=" salesmancommission form-control align-center" readonly="readonly" />'
            + '<span class="input-group-addon">%</span>'
            + '</span>'
            + '</td>';


        row += '<td class="acc" data-th="' + Lang.TotalPrice + '">'
            + '<input type="text" id="t_' + cnt + '" name="Total" value="'
            + (toFloat(getNodeVal(node, "Price", 0), 0) * toFloat(getNodeVal(node, "Num", 0), 0) * toFloat(getNodeVal(node, "UnitCurrencyConverter", 1), 1)).toFixed()
            + '" class="total monetary form-control input-sm align-center validate[custom[integer]]" readonly="readonly" />'
            + '</td>';

        row += '<td class="acc" data-th="' + Lang.Discount + '">'
            + '<input type="text" id="d_' + cnt + '" name="Discount" value="'
            + getNodeVal(node, "Discount") + '" class="discount percent monetary form-control input-sm align-center validate[custom[integer]]" />'
            + '</td>';

        row += '<td class="acc" data-th="' + Lang.Tax + '">'
            + '<input type="text" id="ta_' + cnt + '" name="Tax" value="'
            + getNodeVal(node, "Tax", settingTax) + '" class="tax percent autopercentsign monetary form-control input-sm align-center validate[custom[integer]]" />'
            + '</td>';

        row += '<td class="acc" data-th="' + Lang.VAT + '">'
            + '<input type="text" id="sv_' + cnt + '" name="SingleVAT" value="'
            + getNodeVal(node, "SingleVAT", settingVat) + '" class="svat form-control input-sm align-center monetary percent autopercentsign validate[custom[integer]]" />'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a id="r_' + cnt + '" class="btn btn-danger btn-xs btn-block btn-remove remove">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }

    function returnFromPastYearChecked() {
        if ($('#ReturnFromPastYear').attr('checked')) {
            $('#ChooseStoreDraft').hide();
            $('.buyprice').show();
        } else {
            $('#ChooseStoreDraft').show();
            $('.buyprice').hide();
        }
        $('#ReturnFromPastYear').change(function () {
            if ($(this).attr('checked')) {
                $('#ChooseStoreDraft').hide();
                $('.buyprice').show();
            } else {
                $('#ChooseStoreDraft').show();
                $('.buyprice').hide();
            }
        });
    }

    // ------------------------------
    // startup code
    // ------------------------------
    $('.ChooseStoreDraft').hide();
    mID = toInt($('#MasterID').val(), -1);
    if (mID == -1) {
        $("#UserID").val(_Context.userID);
    }
    var manualNoSerial = toInt(getSettings('ManualNoSerial'), 0);
    if (manualNoSerial <= 0) {
        $('#NOSerial').attr('readonly', 'readonly');
    }
    processPrintButton(0, 4, mID);

    // MasterForm
    dateSelect('.dateSelect');
    setChecked("#IsTrust");
    setChecked("#ReturnFromPastYear");
    setChecked("#IsCashReturn");
    returnFromPastYearChecked();
    setValue("#WorkflowStatus");
    setValue('#ReturnsType');
    if ($('#WorkflowStatus').val() == 1)
        $('.DesiredDocNo').hide();

    if ($("#ReturnDate").val() == '')
        $("#ReturnDate").val(_Context.today);

    $('#FactorTotalDiscount').blur(function () { updateTotal(); });

    autoCompleteTreePath('#OweTreePathName', '#OweTreePath', '#OweSel');
    autoCompleteTreePath('#DueTreePathName', '#DueTreePath', '#DueSel');
    autoComplete('Ward', 'wardauto', '#WardName', '#WardID');
    autoComplete('Personnel', 'personnelauto', '#WardPersonlName', '#WardPersonlID');
    //autoComplete('Stores', 'storeauto', '#StoreName', '#StoreID', setServiceAutoComplete);
    autoCompleteNew({
        table: 'Stores', qd: 'storeauto', textInput: "#StoreName", hiddenInput: "#StoreID"
            , postback: setServiceAutoComplete, allowNull: true
    });
    if (mID == -1) {
        if (!hasValuePositive($("#StoreID").val()))
            setDefault('Stores', 'firststore', "#StoreName", "#StoreID");
    } else {
        if (!hasValuePositive($("#StoreID").val())) {
            $("#StoreID").val(0);
            $("#StoreName").val(Lang.Null);
        }
    }
    if (!hasGeneralAccess("selectstore", generalSettings))
        $('#StoreName').attr('readonly', 'readonly');
    autoComplete('StoreDraft', 'storedraftautonotemp', '#StoreDraftName', '#StoreDraftID');
    autoComplete('Personnel', 'personnelselectauto', '#BuyerName', '#BuyerID'
            , function (event, data) {
                // Nature: 0=Owe;1=Due;2=Both
                if (mID == -1) {
                    if (data[6] == 1)
                        setDefaultByTitle('Trees', 'settingtreesauto', '#DueTreePathName', '#DueTreePath', 'DueTreePath');
                    else
                        setDefaultByTitle('Trees', 'settingtreesauto', '#DueTreePathName', '#DueTreePath', 'OweTreePath');
                }
            }
            , function (data, i, n, value) {
                $('#BuyerID').val('');
                return formatPersonnelItem(data);
            });
    autoComplete('Personnel', 'visitorauto', '#VisitorName', '#VisitorID', null
       , function (data, i, n, value) {
           $('#SellerID').val('');
           return formatPersonnelItem(data);
       });

    if (mID == -1) {
        setDefaultByTitle('Trees', 'settingtreesauto', '#OweTreePathName', '#OweTreePath', 'ReturnTreePath');
        setDefaultByTitle('Trees', 'settingtreesauto', '#DueTreePathName', '#DueTreePath', 'OweTreePath');
    }

    if (!storeMode) {
        showPersonBook('#BuyerID');
    }

    // DetailForm
    // DetailForm
    if (hasValuePositive(mID)) {
        fillTable("get.aspx?op=GetXml&qd=returnitem&ReturnID=" + mID, '#tableItems', addRow);
        disableKeydownOnLastInputByName('SingleVAT');
        $('#tableItems .storeservicename').each(function () {
            var id = $(this).attr('id');
            id = id.substring(id.indexOf('_') + 1);
            var unitName = $('#u_' + id);
            var storeServiceId = $('#sid_' + id).val();
            if (storeServiceId > 0) {
                var _autourl1 = 'get.aspx?op=Auto&table=Units&qd=storedraftunitsauto&StoreServiceID=' + storeServiceId;
                autoComplete('Units', 'unitsauto', unitName, "#justfurfun",
                    function (event, data, formatted) {
                        var id1 = $(event.target).attr('id');
                        id1 = id1.substring(id1.indexOf('_') + 1);
                        $(event.target).next().val(data[0]); // ID
                        //alert(data[2]);
                        if (data[2] && data[2] != 0) {
                            //$('#a_' + id1).next().text('x')
                            $('#ucc_' + id1).val(data[2]); // CurrencyConverter}
                        }
                        else {
                            //$('#a_' + id1).next().text(' ')
                            $('#ucc_' + id1).val('1');
                        }
                    }
                , null, null, null, _autourl1);
            }
        });
        if (storeMode) {
            disableKeydownOnLastInputByName('Num');
        }
    }
    autoPercentSign();

    $('#ShipmentPrice,#TotalAwarez').blur(function () { updateTotal(); });
    $('.price, .amount, .discount, .tax, .svat, .unitname').blur(function () { updatePrice(this); });

    function removeRow($who) {
        var id = $who.attr('id'); // r_1
        id = id.substring(id.indexOf('_') + 1); // r_1  -->  1
        $('#se_' + id).val(""); // StoreServiceName
        $('#a_' + id).val(""); // num
        $('#se_' + id).next().val(-1); //StoreServiceID
        $('#pr_' + id).val(''); // price
        $who.parent().parent().hide();
        $who.parent().parent().find(':input').removeClass();
        updateTotal();
        Growl.Notice(Lang.DeletedSuccessfully);
    }
    $('.remove').click(function () {
        removeRow($(this));
    });

    function setServiceAutoComplete() {
        $("#tableItems tr:last .storeservicename").unautocomplete();
        if ($("#tableItems tr:last .storeservicename").val() != '')
            return;
        var table = $("#tableItems");
        var storeID = toInt($("#StoreID").val(), -1);
        var buyerID = toInt($("#BuyerID").val(), -1);

        var _autourl = 'get.aspx?op=Auto&table=StoreDraft&qd=storeservicesauto&StoreID=' + storeID + '&BuyerID=' + buyerID + appendTime();
        autoCompleteFree('StoreServices', 'storeservicesauto', "#tableItems tr:last .storeservicename", "#justfurfun"
                ,function (event, data, formatted) {
                    autoPostPackStoreService($(event.target), data);
                }
                , function (data, i, n, value) {
                    return formatService(data);
                }, null, null, _autourl);
    }

    function doMore() {
        $('.add').click(function () {
            var storeID = toInt($("#StoreID").val(), -1);
            //if (!hasValuePositive(storeID)) {
            //    alert(Lang.ChooseStore);
            //    return;
            //}
            var table = $("#tableItems");
            $('tr:last', table).after(addRow(null));
            disableKeydownOnLastInputByName('SingleVAT');
            Growl.Notice(Lang.RowAddedSuccessfully);
            var lastRow = $('tr:last', table);
            if (storeMode) {
                $('.acc').hide();
                disableKeydownOnLastInputByName('Num');
            }
            $('.remove', lastRow).click(function () {
                removeRow($(this));
            });

            dateSelect('#tableItems tr:last .dateSelect');
            //autoCompleteFree('Units', 'enumauto', "#tableItems tr:last .unitname", "#justfurfun"
            //    , tableAutoPostPack
            //    , formatItemNameCode);
            // for new items : read from ServicePrices
            //var more = encodeURIComponent("&StoreID=" + storeID);
            setServiceAutoComplete();

            function checkServiceBarCode(me, meID, meSID, barcode) {
                var buyerID = toInt($("#BuyerID").val(), -1);
                var StoreID = toInt($("#StoreID").val(), -1);
                var _autourl = 'get.aspx?op=Auto&table=StoreServices&qd=storeservicesbarcodeauto' + '&BuyerID=' + buyerID + '&StoreID=' + StoreID + appendTime();
                var row = getServerValues(_autourl + "&q=" + barcode);
                if (!row) {
                    return false;
                }
                if (row && row.length && row.length > 10) {
                    var varID = -1;
                    // ServiceVariate
                    if (row[17] > 0)
                        varID = row[17];
                    // -----------------------------------
                    // check if the service is already used, just increase the number
                    var already = false;
                    $('tr .storeserviceid', table).each(function () {
                        if ($(this).attr('id') != meSID) {
                            if ($(this).val() == row[0]) {
                              var v = toInt($(this).parent().find('.ServiceVariateID').val(), 0);
                           if (v == 0)
                            {
                               already = true;
                                me.val('');
                                me.focus();
                                var aID = $(this).attr('id');
                                var _cnt = aID.substring(aID.indexOf('_') + 1);
                                $('#a_' + _cnt).val(toInt($('#a_' + _cnt).val(), 0) + 1);
                                updatePrice($('#a_' + _cnt));
                                setTimeout(function () {
                                    $(me).focus();
                                }, 1);
                             }
                            }
                        }
                    });
                    if (!already) {
                        autoPostPackStoreService(me, row);
                        me.val(row[1]);
                        $('.add').first().click();
                    }
                }
                return true;
            }
            function checkServiceVariateBarCode(me, meID, meSID, barcode) {
                var buyerID = toInt($("#BuyerID").val(), -1);
                var StoreID = toInt($("#StoreID").val(), -1);
                var _autourl = 'get.aspx?op=Auto&table=StoreServices&qd=storeservicesvariatebarcodeauto' + '&BuyerID=' + buyerID + '&StoreID=' + StoreID + appendTime();
                var row = getServerValues(_autourl + "&q=" + barcode);
                if (!row) {
                    return false;
                }
                if (row && row.length && row.length > 10) {
                    var varID = -1;
                    // ServiceVariate
                    if (row[17] > 0)
                        varID = row[17];
                    // -----------------------------------
                    // check if the service is already used, just increase the number
                    var already = false;
                    $('tr .storeserviceid', table).each(function () {
                        if ($(this).attr('id') != meSID) {
                            if ($(this).val() == row[0]) {
                                // check variate
                                if (varID != -1 && toInt(varID) > 0) {
                                    // parent = td
                                    var v = $(this).parent().find('.ServiceVariateID').val();
                                    if (v != varID)
                                        return;
                                }
                                already = true;
                                me.val('');
                                me.focus();
                                var aID = $(this).attr('id');
                                var _cnt = aID.substring(aID.indexOf('_') + 1);
                                $('#a_' + _cnt).val(toInt($('#a_' + _cnt).val(), 0) + 1);
                                updatePrice($('#a_' + _cnt));
                                setTimeout(function () {
                                    $(me).focus();
                                }, 1);
                            }
                        }
                    });
                    if (!already) {
                        autoPostPackStoreService(me, row);
                        me.val(row[1] + ' - ' + row[18]);  // row[18] = ServiceVariate.VarTitle 


                        $('.add').first().click();
                    }
                }
                return true;
            }
             // Handle barcode reader
            $("#tableItems tr:last .storeservicename").blur(function () {
                // se_id, sid_id
                var me = $(this);
                var meID = me.attr('id'); // se_1
                var meSID = '#sid_' + meID.substring(meID.indexOf('_') + 1);

                var barcode = $(this).val();
                var stsID = $(meSID).val(); // StoreServiceID
                if (barcode.length > 0 && stsID == '') {
                    // it is a barcode
                    if (!checkServiceBarCode(me, meID, meSID, barcode))
                        if (!checkServiceVariateBarCode(me, meID, meSID, barcode)) {
                            $(me).val('');
                            setTimeout(function () { $(me).focus(); }, 10);
                        }
                }
            });

            $('.price, .amount, .discount, .tax, .svat, .unitname', lastRow).blur(function () { updatePrice(this); });
            commaInputs();
            monetaryKeys();
            autoPercentSign();
            setTimeout(function () {
                $("#tableItems tr:last .storeservicename").focus();
            }, 10);
        });
        insertValidation();
        monetaryInputsSet(null, '#ReturnDate');
        commaInputs();
        percentInputsSet();
        updateTotal();
    }
    doMore();

    $('#selectItems').click(function () {
        var _url = 'get.aspx?op=GetHtml&table=StoreDraft&view=storedraftselect' + appendTime();
        var btns = {};
        btns[Lang.Cancel] = "_CANCEL_";
        btns[Lang.OK] = "OK";
        showPopup(_url, function (v, m, f) {
            if (v == "OK") {
                $('#StoreDraftID').val(f.SStoreDraftID);
                $('#StoreDraftName').val(f.SStoreDraftName);
                $('#BuyerID').val(f.SBuyerID);
                $('#BuyerName').val(f.SBuyerName);
                $('#VisitorID').val(f.SVisitorID);
                $('#VisitorName').val(f.SVisitorName);
                $('#Pursant').val(f.SPursant);
                for (var i = 0; i < _Context.selectResult.length; i++) {
                    var row = addRow($(_Context.selectResult[i]));
                    $('#tableItems').append(row);
                    var lastRow = $('tr:last', '#tableItems');
                    $('.price, .amount, .discount, .tax, .svat', lastRow).blur(function () { updatePrice(this); });
                    $('.remove', lastRow).click(function () {
                        removeRow($(this));
                    });
                    if (storeMode) {
                        $('.acc').hide();
                        disableKeydownOnLastInputByName('Num');
                    }
                }
                updateTotal();
            }
        }, btns);
    });

    if (storeMode) {
        $('.acc').hide();
        $('.store').hide();
        $('.popupTitle').html(_Context.title);
        if ($("#StoreKeeperName").val() == '') {
            $("#StoreKeeperID").val(_Context.personnelID);
            $("#StoreKeeperName").val(_Context.personnelName);
        }
    }
    else {
        $('.store').hide();
        $('.popupTitle').html(Lang.ReturnInvoice);
        if ($("#AccountantName").val() == '') {
            $("#AccountantID").val(_Context.personnelID);
            $("#AccountantName").val(_Context.personnelName);
        }
    }

    if (mID > 0) {
        uploadify('#ChooseFile', '#FileName', '#StartUpload', 'workflow/return/' + mID
                   , function (fileName) {
                       $('#FileNameDisplay').html(fileName);
                   });
        $('#workflowtasks').html(showWorkflowTasks(mID, WorkflowTypeEnum.Return));
    }
    else
        $('.transDiv').hide();
}