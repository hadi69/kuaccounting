﻿
/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
92-11-10:
    - combined with purchaseinvoice
92-10-29: 
    - file created
*/
function view_shoppurchase() {
    var cnt = 0, mID = -1, mTreePathWasEmpty = true;
    var storeMode = true;
    makeChosen();
    $("#tableItems").smartTable();
    replacePrintButton({
        primary: {
            text: Lang.Print,
            href: Router.Generate("print-purchase", { params: { id: _Context.id }, query: { print: "true" } })
                , type: 2, mode: 0, id: _Context.id
        }
    });

    function updateTotal() {
        var sumPrice = 0, sumAmount = 0, sumDiscount = 0, sumTax = 0, sumVat = 0;
        $('.total').each(function (index, value) {
            sumPrice += toFloat($(value).val(), 0);
        });
        $('.amount').each(function (index, value) {
            sumAmount += toFloat($(value).val(), 0);
        });
        $('.discount').each(function (index, value) {
            if ($(value).val().indexOf('%') >= 0) {
                var id = $(value).attr('id');
                id = id.substring(id.indexOf('_', 0) + 1); // d_1  -->  1
                var total = toFloat($('#t_' + id).val(), 0); // total
                sumDiscount += Math.round(toFloat($(value).val().replace('%', ''), 0) * total / 100);
            }
            else
                sumDiscount += toFloat($(value).val(), 0);
        });
        var discount = 0;
        $('.tax').each(function (index, value) {
            if ($(value).val().indexOf('%') >= 0) {
                var id = $(value).attr('id');
                id = id.substring(id.indexOf('_', 0) + 1); // d_1  -->  1
                var total = toFloat($('#t_' + id).val(), 0); // total
                if ($('#d_' + id).val().indexOf('%') >= 0) {
                    discount = toFloat($('#d_' + id).val().replace('%', ''), 0) * total / 100; // discount
                } else {
                    discount = toFloat($('#d_' + id).val(), 0);
                }
                sumTax += toFloat($(value).val().replace('%', ''), 0) * (total - discount) / 100;
            }
            else
                sumTax += toFloat($(value).val(), 0);
        });
        $('.svat').each(function (index, value) {
            if ($(value).val().indexOf('%') >= 0) {
                var id = $(value).attr('id');
                id = id.substring(id.indexOf('_', 0) + 1); // d_1  -->  1
                var total = toFloat($('#t_' + id).val(), 0); // total
                if ($('#d_' + id).val().indexOf('%') >= 0) {
                    discount = toFloat($('#d_' + id).val().replace('%', ''), 0) * total / 100; // discount
                } else {
                    discount = toFloat($('#d_' + id).val(), 0);
                }
                sumVat += toFloat($(value).val().replace('%', ''), 0) * (total - discount) / 100;
            }
            else
                sumVat += toFloat($(value).val(), 0);
        });

        $('#TotalPrice').val(sumPrice.toFixed(_Context.decimalPoints));
        $('#TotalAmount').val(sumAmount);
        
        var factorTotalDiscount = $('#FactorTotalDiscount').val();
        if (factorTotalDiscount.indexOf('%') >= 0) {
            factorTotalDiscount = Math.round(toFloat(factorTotalDiscount.replace('%', ''), 0) * toFloat($('#TotalPrice').val(), 0) / 100); // factortotaldiscount
        }
        factorTotalDiscount = toFloat(factorTotalDiscount, 0);

        $('#TotalDiscount').val((sumDiscount + factorTotalDiscount).toFixed(_Context.decimalPoints));
        $('#TotalTax').val(sumTax.toFixed(_Context.decimalPoints));
        $('#VAT').val(sumVat.toFixed(_Context.decimalPoints));

        updateFormTotal();
    }
    function updateFormTotal() {

        var total = toFloat($('#TotalPrice').val(), 0) - toFloat($('#TotalDiscount').val(), 0) + toFloat($('#ShipmentPrice').val(), 0) + toFloat($('#TotalTax').val(), 0) + toFloat($('#VAT').val(), 0);
        $('#FinalPrice').val(total.toFixed(_Context.decimalPoints));
         autoSumOnComplexPayMethod();
        commaInputsSet();
    }
    function updatePrice(was) {
        var id = $(was).attr('id');
        id = id.substring(id.indexOf('_') + 1); // d_1  -->  1
        var price = $('#pr_' + id).val();
        var amount = $('#a_' + id).val();
        var unitCarrency = toFloat($('#ucc_' + id).val(), 1);
        $('#t_' + id).val((toFloat(price, 0) * toFloat(amount, 0) * unitCarrency).toFixed(_Context.decimalPoints));
        updateTotal();
    }
    function setServiceData(serviceNameInput, data) {
        var _autourl1 = 'get.aspx?op=Auto&table=Units&qd=purchaseunitsauto&ServicePriceID=' + data[0];
        autoComplete('Units', 'purchaseunitsauto', "#tableItems tr:last .unitname", "#justfurfun",
            function (event, data, formatted) {
                var id1 = $(event.target).attr('id');
                id1 = id1.substring(id1.indexOf('_') + 1);
                $(event.target).next().val(data[0]); // ID
                if (data[2] && data[2] != 0) {
                    $('#ucc_' + id1).val(data[2]); // CurrencyConverter}
                }
                else {
                    $('#ucc_' + id1).val('1');
                }
            }
        , null, null, null, _autourl1);
        serviceNameInput.next().val(0); // StoreServicesID
        serviceNameInput.next().next().val(data[0]); // ServicePriceID
        var id = serviceNameInput.attr('id');
        id = id.substring(id.indexOf('_') + 1);
        if (data[14]) {
            if (data[11]) {
                $('#pr_' + id).val(numberWithCommas((toFloat(toMonetary(data[11]), 0.0)).toFixed(_Context.decimalPoints)));// Price
            }
            if (data[8]) {
                $('#sw_' + id).val(numberWithCommas((toFloat(toMonetary(data[8]), 0.0)).toFixed(_Context.decimalPoints)));// SaleWorth
            }
        } else {
            if (data[4]) {
                $('#pr_' + id).val(numberWithCommas((toFloat(toMonetary(data[4]), 0.0)).toFixed(_Context.decimalPoints)));// Price
            }
            if (data[13]) {
                $('#sw_' + id).val(numberWithCommas((toFloat(toMonetary(data[13]), 0.0)).toFixed(_Context.decimalPoints)));// SaleWorth
            }
        }
        $('#a_' + id).val(1);
        if (toInt(data[15], -1) > 0) // OtherUnitID
        {
            $('#u_' + id).next().val(data[15]); // OtherUnitID
            $('#u_' + id).val(data[16]); // OtherUnitName
            $('#ucc_' + id).val(data[17]); // CurrencyConverter
        } else {
            $('#u_' + id).next().val(data[5]); // unitid
            $('#u_' + id).val(data[6]); // unitname
        }
        updateTotal();
    }
    function setServicePrices(serviceNameInput, data) {
        //storeservicename
        var servicePriceID = data[0];
        //alert(storeServiceID + " = " + serviceNameInput.next().val());
        var prices = '';
        iterateRows("get.aspx?op=GetXml&qd=servicepricepurchases&ID=" + servicePriceID, function (node) {
            prices += numberWithCommas(toMonetary(getNodeVal(node, 'Price', ''))) + '\r\n';
        });
        serviceNameInput.attr('title', prices);
    }
    function autoPostPackServicePrice(event, data, formatted) {
        $('#tableItems tr .servicepriceid').each(function () {
            if ($(this).attr('id') != $('#tableItems tr:last .servicepriceid').attr('id')) {
                if ($(this).val() == data[0]) {
                    alert(Lang.StoreDraftJsM3);
                }
            }
        });
        setServiceData($(event.target), data);
        setServicePrices($(event.target), data);
    }

    function addRow(node) {
        var settingTax = "", settingVat = "";
        if (node == null) {
            settingTax = getSettingTax();
            settingVat = getSettingVat();
        }
        // id="x" --> the validationEngine requires ID
        var readonly = node != null ? ' readonly="readonly" ' : '';
        var row = '<tr>';
        row += '<td data-th="' + Lang.ColRowNr + '" class="row-number align-center">' + (cnt + 1) + '</td>';

        row += '<td data-th="' + Lang.StoreService + '">'
            + '<input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
           + '<div class="input-group input-group-sm">'
            + '<input type="text" id="se_' + cnt + '" name="StoreServiceName" value="'
            + getNodeVal(node, "StoreServiceName") + '" class="storeservicename form-control"' + readonly + ' />'
            + '<input type="hidden" id="sid_' + cnt + '" name="StoreServiceID" value="' + getNodeVal(node, "StoreServiceID") + '" class="storeserviceid" />'
            + '<input type="hidden" name="ServicePriceID" value="' + getNodeVal(node, "ServicePriceID") + '" id="seid_' + cnt + '" class="servicepriceid" />'
            + '<span class="input-group-btn">'
            + '<input type="button" id="neuS_' + cnt + '" value="..."' + (node !== null ? 'disabled="disabled"' : '') + ' class="neuS btn btn-dark-grey" />'
            + '</span>'
            + '</div>'
            + '</td>';

        row += '<td data-th="' + Lang.Units + '" class="align-center">'
            + '<input type="text" id="u_' + cnt + '" name="UnitName" value="' + getNodeVal(node, "UnitName") + '" class="unitname form-control input-sm" />'
            + '<input type="hidden" name="UnitID" value="' + getNodeVal(node, "UnitID") + '" />'
            + '</td>';

        row += '<td data-th="' + Lang.Num + '" class="align-center">'
            + '<div class="input-group input-group-sm">'
            + '<input type="text" id="a_' + cnt + '" name="Amount" value="' + getNodeVal(node, "Amount")
            + '" class="validate[custom[number]] amount form-control width-100-percent min-width-50 align-center" />'
            + '<span class="input-group-addon no-left-border no-right-border">X</span>'
            + '<input readonly="readonly" size="1" id="ucc_' + cnt + '" name="UnitCurrencyConverter" value="'
            + getNodeVal(node, "UnitCurrencyConverter", 1) + '" class="form-control width-100-percent min-width-40 align-center" />'
            + '</div>'
            + '</td>';

        row += '<td data-th="' + Lang.Price + '" class="align-center acc">'
            + '<input type="text" id="pr_' + cnt + '" name="Price" value="'
            + getNodeVal(node, "Price") + '" class="acc price monetary validate[custom[integer]] form-control input-sm align-center" />'
            + '</td>';

        row += '<td data-th="' + Lang.SaleWorth + '" class="align-center acc">'
           + '<input type="text" id="sw_' + cnt + '" name="SaleWorth" value="'
           + getNodeVal(node, "SaleWorth") + '" class="acc saleworth monetary validate[custom[integer]] form-control input-sm align-center" />'
           + '</td>';

        row += '<td data-th="' + Lang.ExpireDate + '" class="align-center">'
            + '<input type="text" id="e_' + cnt + '" name="ExpireDate' + cnt + '" value="'
            + getNodeVal(node, "ExpireDate") + '" class=" dateSelect ExpireDate form-control input-sm" ' + readonly + ' />'
            + '</td>';

        row += '<td data-th="' + Lang.TotalPrice + '" class="align-center acc">'
            + '<input type="text" id="t_' + cnt + '" name="Total" value="'
            + (getNodeVal(node, "Price", 0) * getNodeVal(node, "Amount", 0) * getNodeVal(node, "UnitCurrencyConverter", 1)).toFixed(_Context.decimalPoints)
            + '" class="acc total monetary validate[custom[integer]] form-control input-sm align-center" readonly="readonly" />'
            + '</td>';

        if (_Context.useAdditionalValue > 0) {
            row += '<td class="acc width-70" data-th="' + Lang.Tax + '">' // Tax
            + '<input type="text" id="tax_' + cnt + '" name="Tax" value="' + getNodeVal(node, "Tax", settingTax) + '" class="tax percent autopercentsign monetary validate[custom[integer]] form-control input-sm align-center" />'
            + '</td>';

            row += '<td class="acc width-70" data-th="' + Lang.VAT + '">' // VAT
            + '<input type="text" id="sv_' + cnt + '" name="SingleVAT" value="' + getNodeVal(node, "SingleVAT", settingVat) + '" class="svat monetary percent autopercentsign validate[custom[integer]] form-control input-sm align-center" />'
            + '<input type="hidden" class="source" id="s_' + cnt + '" name="Source" value="' + getNodeVal(node, "Source", 0) + '" />'
            + '</td>';
        }

        row += '<td data-th="' + Lang.Discount + '" class="align-center acc">'
            + '<input type="text" id="d_' + cnt + '" name="Discount" value="'
            + getNodeVal(node, "Discount") + '" class="acc discount monetary percent validate[custom[integer]] form-control input-sm align-center" />'
            + '</td>';

        row += '<td class="align-center action-remove">'
        + '<a id="r_' + cnt + '" class="btn btn-danger btn-xs btn-block btn-remove remove">'
        + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left">' + Lang.ColDel + '</span>'
        + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
        + '</a>'
        + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }
    function PayMethodChange(foreChangeDefault) {
        $('#PayCheckName').removeClass('validate[required]');
        $('.topics').hide();
        var method = toInt($('#PayMethod').val(), SalesInvoicePayMethodEnum.Cash);
        if (method != SalesInvoicePayMethodEnum.Check && method != SalesInvoicePayMethodEnum.Complex) {
            //$('#PayCheckName,label[for="PayCheckName"]').hide();
            $('.newCheck').hide();
        }
        else {
            //$('#PayCheckName,label[for="PayCheckName"]').show();
            $('.newCheck').show();
        }
        if (method == SalesInvoicePayMethodEnum.Check) {
            //$('.newCheck').insertAfter('#PayMethod');
        }
        if (method == SalesInvoicePayMethodEnum.Complex) {
            $('#CheckPrice').blur(function () {
                if (toFloat($(this).val()) > 0) {
                    $('#PayCheckName').addClass('validate[required]');
                } else {
                    $('#PayCheckName').removeClass('validate[required]');
                }
            });
            //$('.newCheck').insertAfter('#CheckSel');
            $('.nocomplex').hide();
            $('.complex').show();
            $('.topics').show();
            $('#DueTopicTitle').removeClass('validate[required]');
            $("label[for = DueTopicCode]").text(Lang.Bank);
            $('.owetopic').hide();
            $('#CashPrice, #CheckPrice, #DuedPrice, #BankPrice').blur(function () {
                autoSumOnComplexPayMethod($(this));
            });
            autoSumOnComplexPayMethod();

        }
        else {
            $('.nocomplex').show();
            $('.complex').hide();
            $('#OwedPrice').val(0);
            $('#CashPrice').val(0);
            $('#BankPrice').val(0);
            $('#CheckPrice').val(0);
        }
        if (method == SalesInvoicePayMethodEnum.Bank) {
            $('.topics').show();
            $("label[for = DueTopicCode]").text(Lang.Bank);
            $('.owetopic').hide();
        }
        // select owetreepath for shop version
        setDefaultByTitleTree('TreePath', 'settingtreesauto', '#OweTopicCode', '#OweTopicTitle', '#OweTreePath', 'BuyTreePath');
        if (!storeMode && (foreChangeDefault || mID == -1 || mTreePathWasEmpty)) {
            // var method = $('#PayMethod').val();
            if (method == SalesInvoicePayMethodEnum.Cash)
                setDefaultByTitleTree('TreePath', 'settingtreesauto', '#DueTopicCode', '#DueTopicTitle', '#DueTreePath', 'FundTreePath');
            else if (method == SalesInvoicePayMethodEnum.Check) {
                $('#PayCheckName').removeClass('validate[required]');
                $('#PayCheckName').addClass('validate[required]');
                setDefaultByTitleTree('TreePath', 'settingtreesauto', '#DueTopicCode', '#DueTopicTitle', '#DueTreePath', 'PayCheckTreePath');
            }
            else if (method == SalesInvoicePayMethodEnum.Other)
                setDefaultByTitleTree('TreePath', 'settingtreesauto', '#DueTopicCode', '#DueTopicTitle', '#DueTreePath', 'DueTreePath');
            else if (method == SalesInvoicePayMethodEnum.Bank || method == SalesInvoicePayMethodEnum.Complex) {
                $('#DueTopicCode,#DueTopicTitle').val('');
                $('#DueTopicTitle').unautocomplete();
                $('#DueTopicCode').unautocomplete();
                autoComplete('Trees', 'treestypedauto', '#DueTopicTitle', '#DueTreePath', function (event, data, formatted) {
                    // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due, Nature, TopicTitle, ProgramCode, EquipmentNeed, CanHaveSubTree
                    var hasChild = toBool(data[5]), canHaveSubTree = toBool(data[12]);
                    if (hasChild || canHaveSubTree) {
                        $('#ToTopic, #ToTopicID').val('');
                        if (hasChild)
                            alert(Lang.HasChild);
                        else
                            alert(Lang.HasSubTreeBut);
                    }
                }, formatTreePath, null, null, 'get.aspx?op=Auto&table=Trees&qd=treestypedauto&Type=0' + appendTime());
            }
        }
        if ((foreChangeDefault || mID == -1) && method == SalesInvoicePayMethodEnum.Complex) {
            setDefaultByTitleTree('TreePath', 'settingtreesauto', '#CashTopicCode', '#CashTopicTitle', '#CashTreePath', 'FundTreePath');
            setDefaultByTitleTree('TreePath', 'settingtreesauto', '#CheckTopicCode', '#CheckTopicTitle', '#CheckTreePath', 'PayCheckTreePath');
            setDefaultByTitleTree('TreePath', 'settingtreesauto', '#DuedTopicCode', '#DuedTopicTitle', '#DuedTreePath', 'DueTreePath');
        }
    }

    function autoSumOnComplexPayMethod(target) {
        if (toInt($('#PayMethod').val(), -1) == 3) {
            var fPrice = toFloat($('#FinalPrice').val(), 0);
            var cPrice = toFloat($('#CashPrice').val(), 0);
            var bPrice = toFloat($('#BankPrice').val(), 0);
            var chPrice = toFloat($('#CheckPrice').val(), 0);
            var oPrice = fPrice - (cPrice + chPrice + bPrice);
            if (oPrice >= 0) {
                $('#DuedPrice').val(numberWithCommas(oPrice));
            } else {
                if (target) {
                    Growl.Notice(Lang.autoSumOnComplexPayMethodMsg, 4000);
                    target.val(0);
                }
            }
        }
    }

    function checkAuto() {
        var more = '';
        //var buyerID = $('#BuyerID').val();
        //if (hasValuePositive(buyerID))
        //    more = "PersonnelID=" + buyerID;
       // $('#CheckName').unautocomplete();
       // autoCompleteFree('PayCheck', 'paycheckauto', '#PayCheckName', '#PayCheckID', null, null, null, more);
    }

    function setRequireds() {
        if (toFloat($('#CashPrice').val(), 0) > 0)
            addRequired('#CashTreePathName');
        else
            removeRequired('#CashTreePathName');
        if (toFloat($('#CheckPrice').val(), 0) > 0)
            addRequired('#CheckTreePathName');
        else
            removeRequired('#CheckTreePathName');
        if (toFloat($('#OwedPrice').val(), 0) > 0)
            addRequired('#OwedTreePathName');
        else
            removeRequired('#OwedTreePathName');
    }

    $('#NewPerson').click(function () {
        var oldID = _Context.id;
        function showcallback(v, m, f) {
            if (v == 'OK') {
                generalOKScalar(v, m, f, 'personupdate', 'personinsert', 'Personnel', '#subDetailForm');
                if (!_Context.success)
                    return false;
                $('#SellerName').val(f.Name + ' ' + f.Surname);
                $('#SellerID').val(_Context.resultID);
            }
            _Context.id = oldID;
        }
        _Context.id = -1;
        var _url = 'get.aspx?op=GetHtml&table=Personnel&qd=paycheck&id=-1&view=person' + appendTime();
        showPopup(_url, showcallback, getButtons());
    });

    function purchaseTypeChange(notChangeDefault) {
        var purchaseType = toInt($('#PurchaseType').val(), 0);
        if (purchaseType == 0 || purchaseType == 3) {
            $('.notguildtrustfrom').show();
            if (notChangeDefault) {
                PayMethodChange(false);
            }
            else {
                PayMethodChange(true);
            }
        }
        else if (purchaseType == 2 || purchaseType == 1) {
            $('.notguildtrustfrom').hide();
        }
        if (storeMode) {
            $('.acc').hide();
        }
    }

    // ------------------------------
    // startup code
    // ------------------------------
    mID = toInt($('#MasterID').val(), -1);
    var manualNoSerial = toInt(getSettings('ManualNoSerial'), 0);
    if (manualNoSerial <= 0) {
        if (mID === -1) {
            $('#NOSerial').val('');
        }
        $('#NOSerial').attr('readonly', 'readonly');
    }
    //storeMode = $.urlParam('menu') != 'accountinga';
    storeMode = !hasAccess("links", "mainmenu", "accounting") && !hasAccess("links", "mainmenu", "sales");

    var sellerBuyerNameValidtion = toInt(getSettings('SellerBuyerNameValidation'), 0);
    if (sellerBuyerNameValidtion == 1) {
        $("#SellerName").addClass('validate[required]');
    }

    if (storeMode)
        $('.acc').hide();
    if (_Context.isAccountingVer)
        $('.noacc').hide();
    if ($('#PurchaseDiscount').val() == '')
        $('#PurchaseDiscount').val(0);

    if ($('#DueTopicTitle').val()) {
        mTreePathWasEmpty = false;
    }
    dateSelect('.dateSelect');
    setValue('#PayMethod');
    setValue('#PurchaseType');

    setValue("#WorkflowStatus");
    if ($('#WorkflowStatus').val() == 1)
        $('.DesiredDocNo').hide();

    // MasterForm
   // $('.popupTitle').html(Lang.SalesPurchase);
    if ($("#ReceiptDate").val() == '')
        $("#ReceiptDate").val(_Context.today);

    autoCompleteFree('Personnel', 'personnelselectauto', '#SellerName', '#SellerID', null
            , function (data, i, n, value) {
                //$('#SellerID').val('');
                return formatPersonnelItem(data);
            });
    if (!storeMode) {
        showPersonBook('#SellerID', true);
    }
    // autoComplete('Supplier', 'supplierauto', '#SupplierName', '#SupplierID');
    autoComplete('PayMethod', 'enumauto2', '#PayMethodName', '#PayMethodID');
    if (mID == -1) {
        autoComplete('Stores', 'storeauto', '#StoreName', '#StoreID');
    }
    if (!hasValuePositive($("#StoreID").val())) {
        setDefault('Stores', 'firststore', "#StoreName", "#StoreID");
    } else {
        if (mID > 0) {
            $('#StoreName').attr('readonly', 'readonly');
        }
    }
    if (!hasGeneralAccess("selectstore", generalSettings))
        $('#StoreName').attr('readonly', 'readonly');

    // autoCompleteTreePath('#OweTreePathName', '#OweTreePath', '#OweSel');
    // autoCompleteTreePath('#DueTreePathName', '#DueTreePath', '#DueSel');
    // autoCompleteTreePath('#CashTreePathName', '#CashTreePath', '#CashSel');
    // autoCompleteTreePath('#CheckTreePathName', '#CheckTreePath', '#CheckSel');
    // autoCompleteTreePath('#DuedTreePathName', '#DuedTreePath', '#DuedSel');
    // autoCompleteTreePath('#ShipmentTreePathName', '#ShipmentTreePath', '#ShipmentSel');

    autoCompleteTreePath2('#OweTopicCode', '#OweTopicTitle', '#OweTreePath', '#OweSel');
    autoCompleteTreePath2('#DueTopicCode', '#DueTopicTitle', '#DueTreePath', '#DueSel');
    autoCompleteTreePath2('#CashTopicCode', '#CashTopicTitle', '#CashTreePath', '#CashSel');
    autoCompleteTreePath2('#CheckTopicCode', '#CheckTopicTitle', '#CheckTreePath', '#CheckSel');
    autoCompleteTreePath2('#DuedTopicCode', '#DuedTopicTitle', '#DuedTreePath', '#DuedSel');

    if (mID == -1)
        setDefault('Purchase', 'lastpurchaseowe', "#OweTreePathName", "#OweTreePath");

    $('#PurchaseDiscount').blur(function () { updateTotal(); });
    $('#FactorTotalDiscount').blur(function () { updateTotal(); });
    $('.tax, .svat').blur(function () { updateTotal(); });
    checkAuto();


    // DetailForm
    if (hasValuePositive(mID)) {
        fillTable("get.aspx?op=GetXml&qd=purchaseitem&PurchaseID=" + mID, '#tableItems', addRow);
        disableKeydownOnLastInputByName('Discount');
    }
    autoPercentSign();
    $('#PurchaseType').change(purchaseTypeChange);
    purchaseTypeChange(true);

    $('#PayMethod').change(function () { PayMethodChange(true); });
    PayMethodChange(false);

    if (storeMode)
        $('.acc').hide();
    $('.price, .amount, .discount, .unitname, .storeservicename').blur(function () { updatePrice(this); });

    $('#tableItems .storeservicename').each(function () {
        var id = $(this).attr('id');
        id = id.substring(id.indexOf('_') + 1);
        var unitName = $('#u_' + id);
        var storeServiceId = $('#sid_' + id).val();
        if (storeServiceId > 0) {
            $(unitName).unautocomplete();
            var _autourl1 = 'get.aspx?op=Auto&table=Units&qd=storedraftunitsauto&StoreServiceID=' + storeServiceId;
            autoComplete('Units', 'unitsauto', unitName, "#justfurfun",
                function (event, data, formatted) {
                    var id1 = $(event.target).attr('id');
                    id1 = id1.substring(id1.indexOf('_') + 1);
                    $(event.target).next().val(data[0]); // ID
                    if (data[2] && data[2] != 0) {
                        //$('#a_' + id1).next().text('x')
                        $('#ucc_' + id1).val(data[2]); // CurrencyConverter}
                    }
                    else {
                        //$('#a_' + id1).next().text(' ')
                        $('#ucc_' + id1).val('1');
                    }
                }
            , null, null, null, _autourl1);
        }
    });

    // for current items : the user should not change the StoreServices
    //autoComplete('StoreServices', 'storeserviceauto', "#tableItems .storeservicename", "#justfurfun"
    //   , autoPostPackStoreService
    //  , function (data, i, n, value) {
    //      return data[1] + ' (' + data[2] + ')';
    //  });
    function removeRow($who) {
        var id = $who.attr('id'); // r_1
        id = id.substring(id.indexOf('_') + 1); // r_1  -->  1
        $('#a_' + id).val(''); // amount
        $('#se_' + id).val(""); // StoreServiceName
        $('#se_' + id).next().val(-1); // StoreServiceID
        $('#pr_' + id).val(''); // price
        $('#sw_' + id).val(''); // saleworth
        $who.parent().parent().hide();
        $('.ProductionDate', $who.parent().parent()).attr('name', 'ProductionDate');
        $('.ExpireDate', $who.parent().parent()).attr('name', 'ExpireDate');
        $who.parent().parent().find(':input').removeClass();
        updateTotal();
        Growl.Notice(Lang.DeletedSuccessfully);
    }
    $('.remove').click(function () {
        var element = $(this);
        doConfirm(function () {
            removeRow(element);
        }, Lang.ConfirmDelete);
    });
    $('.add').click(function () {
        addClick();
    });
    function addClick () {
        /*if (!hasValuePositive($('#SupplierID').val())) {
            alert(Lang.ChooseSupplier);
            return;
        }*/
        var table = $('#tableItems');
        $('tr:last', table).after(addRow(null));
        $('.monetary').keyup(function () {
            convertToScondMonetary(this);
        });
        disableKeydownOnLastInputByName('Discount');
        Growl.Notice(Lang.RowAddedSuccessfully);
        var lastRow = $('tr:last', table);
        dateSelect('#tableItems tr:last .dateSelect');
        if (storeMode)
            $('.acc').hide();
        /* autoCompleteFree('Units', 'enumauto', "#tableItems tr:last .unitname", "#justfurfun"
             , tableAutoPostPack
             , formatItemNameCode);*/
        // for new items : read from ServicePrices
        //var supplier = $("#SupplierID").val();
        var more = null; //'Model=' + supplier;
        autoCompleteFree('ServicePrice', 'servicepriceauto', "#tableItems tr:last .storeservicename", "#justfurfun"
            , autoPostPackServicePrice
            , function (data, i, n, value) {
                return formatServicePrice(data);
            }, null, more, null);
        // Handle barcode reader
        $("#tableItems tr:last .storeservicename").blur(function () {
            // se_id, sid_id
            var me = $(this);
            var meID = me.attr('id'); // se_1
            var meSID = '#seid_' + meID.substring(meID.indexOf('_') + 1);

            var barcode = $(this).val();
            var stsID = me.next().val(); // StoreServiceID
            if (barcode.length > 0 && stsID == '') {
                // it is a barcode
                var _autourl = 'get.aspx?op=Auto&table=ServicePrice&qd=servicepricebarcodeauto' + appendTime();
                var row = getServerValues(_autourl + "&q=" + barcode);
                if (row && row.length && row.length > 6) {
                    // insert new row when we have variate unit
                    var unitId = row[5];
                    if (toInt(row[15], -1) > 0) {
                        unitId = row[15];
                    }
                    // -----------------------------------
                    // check if the service is already used, just increase the number
                    var already = false;
                    $('tr .servicepriceid', table).each(function () {
                        var thisId = $(this).attr('id');
                        thisId = thisId.substring(thisId.indexOf('_') + 1);
                        if ($(this).attr('id') != meSID) {
                            if ($(this).val() === row[0] && $('#u_' + thisId).next().val() === unitId) {
                                already = true;
                                me.val('');
                                var aID = $(this).attr('id');
                                var _cnt = aID.substring(aID.indexOf('_') + 1);
                                $('#a_' + _cnt).val(toInt($('#a_' + _cnt).val(), 0) + 1);
                                updatePrice($('#a_' + _cnt));
                                setTimeout(function () {
                                    $(me).focus();
                                }, 1);
                            }
                        }
                    });
                    if (!already) {
                        setServiceData(me, row);
                        setServicePrices(me, row);
                        setTimeout(function () {
                            me.val(row[1]);
                        }, 5);
                        updatePrice(me);
                        addClick();
                    }
                }
            }
        });
        $('.remove', lastRow).click(function () {
            var element = $(this);
            doConfirm(function () {
                removeRow(element);
            }, Lang.ConfirmDelete);
        });
        $('.tax, .svat', lastRow).blur(function () { updateTotal(); });
        $('.neuS', lastRow).click(function () {
            var oldID = _Context.id;
            var id = $(this).attr('id');
            id = id.substring(id.indexOf('_', 0) + 1); // neuS_1  -->  1
            function showcallback(v, m, f) {
                if (v == 'OK') {
                    $('.StartDate').attr('name', 'StartDate');
                    $('.EndDate').attr('name', 'EndDate');
                    $('.StartDatesp').attr('name', 'StartDatesp');
                    var qry = (_Context.id == -1) ? 'serviceinsert' : 'serviceupdate';
                    masterscallbackScalar(v, m, f, 'Service', qry, 'Normal,ServiceRelation,PanelServices,ServiceItems,ServicePrice,ServiceSpeciality'
                            , 'normalupdate,servicerelationupdate,panelserviceupdate,serviceitemupdate,servicepriceupdate,servicespecialityupdate'
                            , 'normalinsert,servicerelationinsert,panelserviceinsert,serviceiteminsert,servicepriceinsert,servicespecialityinsert'
                            ,'normaldelete,servicerelationdelete,panelservicedelete,serviceitemdelete,servicepricedelete,servicespecialitydelete'
                            ,'KitID,RelatedID,SubServiceID,ItemName,ModelName,SpecialtyID'
                            , null, '#subDetailForm');

                    if (!_Context.success)
                        return false;
                    $('#se_' + id).next().val(0);
                    var _autourl = 'get.aspx?op=Auto&table=Service&qd=servicefirstmodelandbaseunit&ServiceID=' + _Context.resultID + appendTime();
                    var data = getServerValues(_autourl);
                    $('#se_' + id).val(data[1]);//servicename
                    $('#seid_' + id).val(data[0]);//servicepriceid
                    $('#u_' + id).val(data[3]);//unitname
                    $('#u_' + id).next().val(data[2]);//unitid
                    //setDefaultByTitle('Service', 'servicefirstmodel', '#se_' + id, '#seid_' + id, _Context.resultID)
                    //$('#PayCheckID').val(_Context.resultID);
                }
                _Context.id = oldID;
            }
            _Context.id = -1;
            var _url = 'get.aspx?op=GetHtml&table=service&qd=servicedetail&id=-1&ParentRelatedID=-1' + appendTime();
            showPopup(_url, showcallback, getButtons());
        });
        $('.price, .amount, .discount, .unitname, .storeservicename', lastRow).blur(function () { updatePrice(this); });
        commaInputs();
        monetaryKeys();
        // Focus the input used for StoreServiceName
        //$('input[name=StoreServiceName]', lastRow).focus();
        autoPercentSign();
        setTimeout(function () {
            $("#tableItems tr:last .storeservicename").focus();
        }, 10);
    };

    $('#CashPrice,#CheckPrice,#OwedPrice').blur(setRequireds);
    setRequireds();

    insertValidation();
    monetaryInputsSet(null, '#ReceiptDate');
    commaInputs();
    percentInputsSet();
    updateTotal();

    $('#newCheck').click(function () {
        var oldID = _Context.id;
        function showcallback(v, m, f) {
            if (v == 'OK') {
                generalOKScalar(v, m, f, 'paycheckupdate', 'paycheckinsert', 'PayCheck', '#subDetailForm');
                if (!_Context.success)
                    return false;
                $('#PayCheckName').val(f.Serial);
                $('#PayCheckID').val(_Context.resultID);
            }
            _Context.id = oldID;
        }
        _Context.id = -1;
        var _url = 'get.aspx?op=GetHtml&table=PayCheck&qd=paycheckshop&view=paycheckshop&id=-1&CheckType=paycheck&ReceiverName=' + encodeURIComponent($('#SellerName').val())
            + "&ReceiverID=" + $('#SellerID').val() + "&FromPurchase=1&Amount=" + fromMonetary($('#FinalPrice').val(), null) + appendTime();
        showPopup(_url, showcallback, getButtons());
    });

    $('#UpdateCurrentServicesPrice').click(function () {
        if (_Context.id != -1) {
            var isOk = getServerValue('updateservicepriceincurrentpurchase', 'PurchaseItems', '&PurchaseID=' + _Context.id);
            if (isOk) {
                Growl.Notice(Lang.Success);
            } else {
                Growl.Notice(Lang.Failed);
            }
        } else {
            alert(Lang.purchaseM6);
        }
    });
    $('#ShipmentPrice').blur(function () { updateFormTotal(); });
    $('#DistributionShipmentPrice').click(function () {
        var table = $("#tableItems");
        var shipmentPrice = toFloat($('#ShipmentPrice').val(), 0);
        var totalAmount = toFloat($('#TotalAmount').val(), 0);
        var distShipmentPriceOnSaleWorth = toInt(getSettings('DistShipmentPriceOnSaleWorth'), 0);
        var amount = 0;
        var count = 0;
        while (shipmentPrice % totalAmount !== 0) {
            shipmentPrice = shipmentPrice - 1;
            count++;
        }
        amount = shipmentPrice / totalAmount;
        $('tr .storeserviceid', table).each(function () {
            var id = $(this).attr('id');
            id = id.substring(4);
            // var num = toInt($('#a_' + id).val(), 0) * toInt($('#ucc_' + id).val(), 1);
            var price = toFloat($('#pr_' + id).val(), 0);
            var saleWorth = toFloat($('#sw_' + id).val(), 0);
            //if (count > 0) {
            //    $('#pr_' + id).val(price + amount + count);
            //    if (saleWorth && distShipmentPriceOnSaleWorth > 0)
            //        $('#sw_' + id).val(saleWorth + amount + count);
            //    count = 0;
            //} else {
            $('#pr_' + id).val(price + amount);
            if (saleWorth && distShipmentPriceOnSaleWorth > 0)
                $('#sw_' + id).val(saleWorth + amount);
            // }
            updatePrice('#a_' + id);
        });
        if (count > 0) {
            $('#ShipmentPrice').val(count);
            count = 0;
        } else {
            $('#ShipmentPrice').val(0);
        }
        updateTotal();
    });

    if (mID > 0) {
        uploadify('#ChooseFile', '#FileName', '#StartUpload', 'workflow/purchase/' + mID
                   , function (fileName) {
                       $('#FileNameDisplay').html(fileName);
                   });
        $('#workflowtasks').html(showWorkflowTasks(mID, WorkflowTypeEnum.Purchase));
    }
    else
        $('.transDiv').hide();

    // Check access for finalized factors:
    //if ($('#WorkflowStatus').val() == 1) {
    //    if (hasAccess("sales", "purchase", "price")) {
    //        // it can do everything: add / delete / edit
    //    }
    //    else if (hasAccess("sales", "purchase", "count")) {
    //        // it can not do add or delete
    //        // it can not change price
    //        // it can change amount
    //        $('.add').remove();
    //        $('.remove').remove();
    //        $('.price, .saleworth').attr('readonly', true);
    //    }
    //    else {
    //        // it can do nothing
    //        $('.add').remove();
    //        $('.remove').remove();
    //        $('.price, .saleworth').attr('readonly', true);
    //        $('.amount').attr('readonly', true);
    //        $('.unitname').attr('readonly', true);
    //    }
    //}
    $('#SellerName').focus();
    $('.monetary').keyup(function() {
        convertToScondMonetary(this);
    });
    $('#ConvertedPriceAbbreviation').text(_Context.monetaryUnitAbbreviationSecond);
    $("#hidenTotal")
        .click(function () {
            if ($("#hideTotal").css("display") == "none") {
                $("#hideTotal").slideDown();
                $("#hideTotal").css("display", "block");
                $(".fa-angle-double-down").addClass("rotated");

            } else {
                $("#hideTotal").slideUp();
                $("#hideTotal").css("display", "none");
                $(".fa-angle-double-down").removeClass("rotated");
            }
        });
    if (_Context.useAdditionalValue > 0) {
        $('.additionalvalue').show();
    } else {
        $('.additionalvalue').hide();
    }
}