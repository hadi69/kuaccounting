﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
95-12-27: file created
*/
function view_asset() {
    makeChosen();
    // ------------------------------
    // startup code
    // -----------------------------
    dateSelect('.dateSelect')
    autoComplete('Units', 'enumauto1', "#UnitName", "#UnitID");
    insertValidation();
}