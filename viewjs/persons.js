﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
    92-10-16: file created
*/
function view_persons() {
    function doCommand(com, grid) {
        if (com == Lang.UpdateCodings) {
            if (runQuery('updatepersoncodings', 'Personnel', null)) {
                message(Lang.UpdateCodingsSuccessfully);
                $('.updatecoding').removeClass('important');
            }
        }
        else
            generalCommand(com, grid, show);
    }
    function run() {
        var more = 'Type=101';
        var _url = 'service.asmx/GetList?table=Personnel&qd=person&more=' + more + appendTime();
        //alert(_url);
        $("#flexi").flexigrid
                (
                {
                    url: _url,
                    dataType: 'xml',
                    colModel:
                    [
                        { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                        { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                        { display: Lang.Name, name: 'Name', sortable: true, css_class: 'width-200' },
                        { display: Lang.SurName, name: 'SurName', sortable: true, css_class: 'width-200' },
                        { display: Lang.HomeTelNum, name: 'HomeTelNum', sortable: true, css_class: 'width-150' },
                        { display: Lang.Code, name: 'Code', sortable: true, css_class: 'width-100 align-center' },
                        { display: Lang.Coding, name: 'Coding', sortable: true, css_class: 'width-150 align-center', hide: true },
                        { display: Lang.Comments, name: 'Comments', sortable: true},
                        { display: Lang.Enabled, name: 'Enabled', sortable: true, process: showBool, css_class: 'width-40 align-center' },
                        { display: Lang.CellNumber1, name: 'CellNumber1', hide: true },
                        { display: Lang.Email, name: 'Email', hide: true },
                        { display: Lang.HomeAddress, name: 'HomeAddress', hide: true },
                        { display: Lang.GPS, name: 'Latittude', sortable: true, process: showLoc, css_class: 'width-40 align-center', hide: true },
                        { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' },
                    ],
                    buttons:
                    [
                        { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                        { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                        { separator: true },
                        { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                        { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand },
                    ],
                    searchitems: null,
                    sortname: "Name",
                    sortorder: "asc",
                    usepager: true,
                    //title: _Context.title,
                    useRp: true,
                    rp: gridRp,
                    showTableToggleBtn: _options.gridToggle,
                    width: _options.gridWidth,

                    height: _options.gridHeight, resizable: _options.gridResizable,
                    singleSelect: false,
                    onDoubleClick: function (row, grid, options) {
                        var id = row.id.substring(3);
                        show(id);
                    },
                    onSuccess: function (grid) {
                        restoreSelectedRows('#flexi');
                    }
                }
                );

        if (_Context.level == 1)
            $('.updatecoding').hide();
    }
    function showLoc(celDiv, id) {
        if (celDiv.innerText && celDiv.innerText.trim() != "")
            $(celDiv).html('<span style="color: red;"><i class="fa fa-map-marker"></i></span>');
        else
            $(celDiv).html('');
    }
    function showcallback(v, m, f) {
        restoreSelectedRows('#flexi');

        var isInsert = (_Context.id == -1);
        var qry = (_Context.id == -1) ? 'personinsert' : 'personupdate';
        var res = newMasterscallback(v, m, f, 'Personnel', qry, 'GroupingMember,AssessmentFieldValue', 'personnelgroupingupdate,assessmentfieldvalueupdate', 'personnelgroupinginsert,assessmentfieldvalueinsert', 'personnelgroupingdelete,assessmentfieldvaluedelete', 'GroupingID,AssessmentFieldID', null
            , '#subDetailForm', show);
        if (res && isInsert && (v == 'OK' || v == 'OKPrint'))
            $('.updatecoding').removeClass('important').addClass('important');
        return res;
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        
        $('.trSelected').removeClass('trSelected');
        $('#row' + id).addClass('trSelected');
        saveSelectedRows('#flexi');
        $('.trSelected').removeClass('trSelected');

        var _url = 'get.aspx?op=GetHtml&table=Personnel&view=person&qd=person&id=' + id + appendTime();
        showPopup(_url, showcallback, getDefaultButtons(), null, successCallback);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['accounting,person']);
    setClassAccounting();
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.Person, parentTitle: Lang.MAccounting, parentUrl: 'menu/accounting.aspx' } }));
    run();
}