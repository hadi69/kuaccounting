﻿function view_deductionfield() {

    function autoPostPack(event, data, formatted) {
        $(event.target).next().val(data[0]); // FieldID
    }
    var more = "Used=1";
    autoCompleteFree('Fields', 'enumauto1', "#FieldName", "#FieldID"
                , autoPostPack, null, null, more);
    autoComplete('Trees', 'treesauto', "#TopicName", "#TreePath"
       , autoPostPack
       , function (data, i, n, value) {
           return data[1];
       });
    autoComplete('Trees', 'treesauto', "#DueTopicName", "#DueTreePath"
       , autoPostPack
       , function (data, i, n, value) {
           return data[1];
       });
    insertValidation();
}