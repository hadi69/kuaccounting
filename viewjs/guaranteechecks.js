﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
92-10-19: file created
*/
function view_guaranteechecks() {
    makeChosen();
    $("#CheckType").trigger('chosen:activate');
    function displayAmount() {
        $('#AmountStr').html(numToString($('#Amount').val()));
    }
    function changeReciveDest() {
        if ($("#FinalDate").val() != '')
            $("#ReceiveDest").attr('disabled', false);
        else {
            $("#ReceiveDest").val('');
            $("#ReceiveDest").attr('disabled', true);
        }
    }

    function lastValues() {
        var id = toInt($('#subDetailForm #PersonnelID').val(), 0);
        if (id <= 0)
            return;
        var u = 'get.aspx?op=Auto&table=Checks&qd=checkslast&q=' + id + appendTime();
        var row = getServerValues(u);
        if (row && row.length && row.length > 2) {
            // AccountNo, Bank, Branch
            $('#subDetailForm #AccountNo').val(row[0]);
            $('#subDetailForm #BankTreePath').val(row[1]);
            $('#subDetailForm #BankName').val(row[2]);
            $("#subDetailForm #Branch").val(row[3]);
        }
    }

    /* function showDivs() {
         if ($('#dueD').val() == 1 || $('#subDetailForm #DueTreePath').val() != '')
             $('#dueDiv').show();
         else
             $('#dueDiv').hide();
         if ($('#oweD').val() == 1 || $('#subDetailForm #OweTreePath').val() != '')
             $('#oweDiv').show();
         else
             $('#oweDiv').hide();
     }*/

    function bindVouchers() {
        // Vouchers\
        if ($('#CheckType').val() == "checks") {
            var _autourl = 'get.aspx?op=Auto&table=DocumentAccounting&qd=checkvouchers&q=' + $('#ID').val();
            var res = getRowtable(_autourl);
            if (res && res.length > 0) {
                $('#Voucher1').attr('href', "list.aspx?table=DocumentAccounting&id=" + res[0][0]);
                $('#Voucher1').html(res[0][1]);
            }
            if (res && res.length > 1) {
                $('#Voucher2').attr('href', "list.aspx?table=DocumentAccounting&id=" + res[1][0]);
                $('#Voucher2').html(res[1][1]);
            }
        } else {
            var _autourl1 = 'get.aspx?op=Auto&table=DocumentAccounting&qd=paycheckvouchers&q=' + $('#MasterID').val();
            var res1 = getRowtable(_autourl1);
            if (res1 && res1.length > 0) {
                $('#PayCheckVoucher1').attr('href', "list.aspx?table=DocumentAccounting&id=" + res1[0][0]);
                $('#PayCheckVoucher1').html(res1[0][1]);
            }
            if (res1 && res1.length > 1) {
                $('#PayCheckVoucher2').attr('href', "list.aspx?table=DocumentAccounting&id=" + res1[1][0]);
                $('#PayCheckVoucher2').html(res1[1][1]);
            }
        }
    }
    function checkPersonCredit() {
        var personnelID = toInt($('#subDetailForm #PersonnelID').val(), 0);
        clearError();
        if (personnelID > 0) {
            var checkCredit = toFloat(getServerValue("personcheckcredit", "Personnel", "&PersonnelID=" + personnelID), 0);
            if (checkCredit > 0) {
                var amount = toFloat($('#Amount').val());
                if (checkCredit < amount) {
                    showError(Lang.LowCredit);
                }
            }
        }
    }

    function showError(msg) {
        $("#creditmsg").html(msg);
        $("#notification-message").show();
    }

    function clearError() {
        $("#creditmsg").html("");
        $("#notification-message").hide();
    }

    function checkview() {
        var x = $('#CheckType').val();
        if (x == "paycheck") {
            $('.check').hide();
            $('.paycheck').show();
            $('#Serial').attr('readonly', 'readonly');
        }
        if (x == "checks") {
            $('.check').show();
            $('.paycheck').hide();
            if ($('#setstatus').val() != CheckStatusEnum.Expend)
                $('#personDiv').hide();
            $('#BankDiv').hide();
            $('#Serial').attr('readonly', '');
        }

        $('#CheckType').change(function () {
            if ($('#CheckType').val() == "paycheck") {

                $('.check').hide();
                $('.paycheck').show();
                $('#Serial').attr('readonly', 'readonly');
            }
            if ($('#CheckType').val() == "checks") {
                $('.check').show();
                $('.paycheck').hide();
                if ($('#setstatus').val() != CheckStatusEnum.Expend)
                    $('#personDiv').hide();
                $('#BankDiv').hide();
                $('#Serial').attr('readonly', '');
                $('#Serial').unautocomplete();
            }
        });

    }
    // ------------------------------
    // startup code
    // ------------------------------
    setValue('#CheckType');
    checkview();
    setValue('#ReceiveDest');
    dateSelect('.dateSelect');
    setValue('#Status');
    if (!hasValuePositive($('#UserID').val()))
        $('#UserID').val(_Context.userID);
    if ($("#IssueDate").val() == '')
        $("#IssueDate").val(_Context.today);
    if ($("#PayDate").val() == '')
        $("#PayDate").val(_Context.today);
    if ($("#FinalDate").val() == '')
        $("#ReceiveDest").attr('disabled', true);

    autoComplete('Trees', 'treesbanksauto', '#BankName', '#BankTreePath', function (event, data, formatted) {
        // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due
        var hasChild = data[5] != '';
        if (hasChild) {
            $('#BankName, #BankTreePath').val('');
            alert(Lang.HasChild);
        }
    }, formatTreePath, null, null);

    autoComplete('BankAccountCheck', 'bankaccountautoforchecks', '#BankAccountName', '#BankAccountID'
    , function (event, data, formatted) {
        $('#BankName').val(data[3]);
        $('#BankTreePath').val(data[2]);

    });

    autoComplete('Trees', 'treesbanksauto', '#ReceiverBankName', '#ReceiverBankTreePath', function (event, data, formatted) {
        // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due
        var hasChild = data[5] != '';
        if (hasChild) {
            $('#ReceiverBankName, #ReceiverBankTreePath').val('');
            alert(Lang.HasChild);
        }
    }, formatTreePath, null, null);


    autoComplete('Personnel', 'personnelselectauto', '#subDetailForm #AssignorName', '#subDetailForm #AssignorID', lastValues
    , function (data, i, n, value) {
        $('#subDetailForm #AssignorID').val('');
        return formatPersonnelItem(data);
    });

    autoCompleteFree('Personnel', 'visitorauto', '#VisitorName', '#VisitorID', null
    , function (data, i, n, value) {
        //$('#VisitorID').val('');
        return formatPersonnelItem(data);
    });


    autoComplete('Personnel', 'personnelselectauto', '#subDetailForm #PersonnelName', '#subDetailForm #PersonnelID', lastValues
    , function (data, i, n, value) {
        $('#subDetailForm #PersonnelID').val('');
        return formatPersonnelItem(data);
    });

    autoComplete('Personnel', 'personnelselectauto', '#subDetailForm #ReceiverPerson', '#subDetailForm #ReceiverPersonID', lastValues
, function (data, i, n, value) {
    $('#subDetailForm #ReceiverPersonID').val('');
    return formatPersonnelItem(data);
});

    autoComplete('Personnel', 'personnelselectauto', '#subDetailForm #ReceiverName', '#subDetailForm #ReceiverID', lastValues
, function (data, i, n, value) {
    $('#subDetailForm #ReceiverID').val('');
    return formatPersonnelItem(data);
});
    autoComplete('BankAccountCheck', 'bankaccountcheckauto', '#BankAccountCheckName', '#BankAccountCheckID'
        , function (event, data, formatted) {
            $('#BankName').val(data[3]);
            $('#BankTreePath').val(data[2]);

            // serial : get unused one
            var serial = toInt(getServerValue("reternpaycheckminserial", "PayCheck", "&BankAccountCheckID=" + data[0]), 0);
            if (!serial) {
                alert(Lang.PayCheckJsM1);
                runQuery('bankaccountcheckupdateformpaycheck', 'PayCheck', '&ID=' + data[0]);
            }
            else {
                $('#Serial').val(serial);
            }

            $('#Serial').attr('readonly', false);
            // #Res.Serial, #Res.Serial Serial2, Amount, DueDate, ReceiverName, No
            autoComplete('BankAccountCheck', 'bankaccountcheckserialauto', '#Serial', '#Serial'
                , function (event, data, formatted) {
                    if (!data[5]) // has no
                        $('#Serial').val(data[0]);
                    else
                        $('#Serial').val('');
                }, function (data, i, n, value) {
                    // $('#Serial').val('');
                    if (data[5]) // used
                        return Lang.Serial + " : " + data[0] + "<br />" + Lang.PayCheck + " - " + Lang.No + " : " + data[5]
                            + ", " + Lang.Reciever + " : " + data[4]
                           + "<br />" + Lang.DueDate + " : " + data[3]
                           + ", " + Lang.Amount + " : " + data[2];
                    return Lang.Serial + " : " + data[0] + "<br />" + Lang.Unused;
                }, null, null, null, 'BankAccountCheckID=' + data[0]);
        });

    autoCompleteTreePath('#subDetailForm #TreePathName', '#subDetailForm #TreePath', '#subDetailForm #selTree');
    autoCompleteTreePath('#subDetailForm #DueTreePathName', '#subDetailForm #DueTreePath', '#subDetailForm #selDueTree');
    autoCompleteTreePath('#subDetailForm #OweTreePathName', '#subDetailForm #OweTreePath', '#subDetailForm #selOweTree');
    autoCompleteTreePath('#subDetailForm #TreePathName', '#subDetailForm #TreePath', '#subDetailForm #selTree');
    autoCompleteTreePath('#subDetailForm #DueTreePathName', '#subDetailForm #DueTreePath', '#subDetailForm #selDueTree');
    autoCompleteTreePath('#subDetailForm #OweTreePathName', '#subDetailForm #OweTreePath', '#subDetailForm #selOweTree');
    autoComplete('', 'FindBankTreePathQuery', '#BankChoose', '#BankChooseID');



    lastValues();

    // showDivs();

    $("#FinalDate").blur(function () { changeReciveDest(); });
    $("#Amount").blur(checkPersonCredit);
    if (!hasValuePositive($('#ID').val()))
        $('#Status').val(CheckStatusEnum.New);
    // alert($('#setstatus').val());
    if ($('#setstatus').val() == CheckStatusEnum.Received) {
        $('#Status').val(CheckStatusEnum.Received);
    }
    else if ($('#setstatus').val() == CheckStatusEnum.Refused) {
        $('#Status').val(CheckStatusEnum.Refused);
    }
    else if ($('#setstatus').val() == CheckStatusEnum.New && $('#setstatus').val() != '') {
        $('#Status').val(CheckStatusEnum.New);
    }
    else if ($('#setstatus').val() == CheckStatusEnum.Sleep) {
        $('#Status').val(CheckStatusEnum.Sleep);
        $('#personDiv').hide();
        $('#BankDiv').show();
        $('#visitorDiv').hide();
    }
    else if ($('#setstatus').val() == CheckStatusEnum.Expend) {
        $('#Status').val(CheckStatusEnum.Expend);
        $('#personDiv').show();
        $('#BankDiv').hide();
        $('#visitorDiv').hide();
    }
    else if ($('#setstatus').val() == CheckStatusEnum.Visitor) {
        $('#Status').val(CheckStatusEnum.Visitor);
        $('#visitorDiv').show();
    }
    else {

        if (hasValuePositive($('#ReceiverBankTreePath').val()))
            $('#BankDiv').show();
        else
            $('#BankDiv').hide();
        if (hasValuePositive($('#ReceiveTreeID').val()))
            $('#Receive').show();
        else
            $('#Receive').hide();
    }
    changeReciveDest();

    bindVouchers();


    insertValidation('#subDetailForm');
    monetaryInputsSet('#subDetailForm');
    commaInputs('#subDetailForm');

    $('#Amount').blur(displayAmount).keyup(displayAmount);
    displayAmount();
    if ($('#Status').val() != CheckStatusEnum.Visitor) {
        $('#visitorDiv').hide();
    }
}