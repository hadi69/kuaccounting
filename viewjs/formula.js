﻿function view_formula() {
    var cnt = 0;
    var fieldname = "";
    function addRow(node) {
        // id="x" --> the validationEngine requires ID
        var readonly = node != null ? " readonly='readonly' " : "";
        var row = '<tr>';

        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">'
            + (cnt + 1)
            + '</td>';

        row += '<td data-th="' + Lang.PTitle + '">'
            + '<input type="hidden" id=id_' + cnt + ' name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="text" id="t_' + cnt + '" name="Title" value="' + getNodeVal(node, "Title") +'"'
            + 'class="title form-control input-sm" ' + readonly + ' />'
            + '</td>';

        row += '<td data-th="' + Lang.ETitle + '">'
            + '<input type="text" id="n_' + cnt + '" name="Name" value="' + getNodeVal(node, "Name") + '" class="name form-control input-sm"' + readonly + ' />'
            + '</td>';

        row += '<td data-th="' + Lang.FieldType + '">'
            + '<select id="ft_' + cnt + '" name="Used" v="' + getNodeVal(node, "Used") + '" class="Used" disabled="true">'
            + '<option value="0">' + Lang.Orders + '</option>'
            + '<option value="1">' + Lang.Fish + '</option>'
            + '<option value="2">' + Lang.Both + '</option>'
            + '</select>'
            + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }

    function fillInfo() {
        var url = "get.aspx?op=GetXml&table=Fields&qd=allfield";
        fillTable(url, '#tableItems', addRow);
    }

    fillInfo();
    setValue('.Used');
    $("#FormulaText").val($("#FormulaText").val().split('|').join('+'));
    $("#tableItems tr").each(function() {
        $(this).click(function() {
            var formula = $("#FormulaText").val();
            fieldname = $(this).find('.name').val();
            formula += fieldname;
            $("#FormulaText").val(formula);
            $("#FormulaText").focus();
        });
    });

    $('.operator').click(function () {
        var formula = $("#FormulaText").val();
        formula += $(this).val();
        $("#FormulaText").val(formula);
        $("#FormulaText").focus();
    });
    $("#Clean").click(function () {
        var formula = $("#FormulaText").val();
        if (formula.substr(formula.length - 1, 1).indexOf('*') >= 0 || formula.substr(formula.length - 1, 1).indexOf('+') >= 0 || formula.substr(formula.length - 1, 1).indexOf('-') >= 0 || formula.substr(formula.length - 1, 1).indexOf('/') >= 0 || formula.substr(formula.length - 1, 1).indexOf('(') >= 0 || formula.substr(formula.length - 1, 1).indexOf(')') >= 0)
            $("#FormulaText").val(formula.substr(0, formula.length - 1));
        else {
            $("#FormulaText").val(formula.substr(0, formula.length - fieldname.length));
        }
        $("#FormulaText").focus();
    });
    var value = $("#FormulaText").val();
    $("#FormulaText").val('');
    $("#FormulaText").val(value);
    $("#FormulaText").focus();
    
}