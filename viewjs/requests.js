﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-11-11: file created
*/
function view_requests() {
    var startupID = -1;
    function doCommand(com, grid) {
         if (com == Lang.CreatDraft) {
            var ids = '';
            $('.trSelected', grid).each(function () {
                var id = $(this).attr('id');
                id = id.substring(id.lastIndexOf('row') + 3);
                ids += id + ',';
            });
            if (ids.length > 0) {
                ids = ids.substring(0, ids.length - 1);
                // alert(ids.substring(0,ids[1]));
                // alert(ids.split(','));
                var more = '&ids=' + ids;
                if (runQuery('createstoredraft', 'Request', more))
                    displayError(Lang.Success, null, Lang.Success);
            }
         }
         else if (com == Lang.CreatePurchaseRequest) {
             var ids = '';
             $('.trSelected', grid).each(function () {
                 var id = $(this).attr('id');
                 id = id.substring(id.lastIndexOf('row') + 3);
                 ids += id + ',';
             });
             if (ids.length > 0) {
                 ids = ids.substring(0, ids.length - 1);
                 // alert(ids.substring(0,ids[1]));
                 // alert(ids.split(','));
                 var more = '&ids=' + ids;
                 if (runQuery('createpurchaserequest', 'Request', more, true))
                     displayError(Lang.Success, null, Lang.Success);
             }
         }
        else {
            var isNew = com == Lang.GBNew;
            if (!isNew) {
                var btn = $(".fbutton span:contains(" + com + ")");
                if (btn.hasClass('new'))
                    isNew = true;
            }
            if (isNew) {
                var more = '&Type=' + WorkflowTypeEnum.Request;
                var count = getServerValue('checkworkflow', 'Workflow', more);
                if (count == 0 || count == null) {
                    displayError(Lang.WorkflowM1);
                    return;
                }
            }
            if (isNew && com != Lang.GBNew) {
                var desiredEntryTaskID = $(".fbutton span:contains(" + com + ")").attr('class').substring('new '.length);
                //alert(desiredEntryTaskID);
                requestshow(-1, null, null, desiredEntryTaskID);
            }
            else
                generalCommand(com, grid, requestshow);
        }
    }
    function run() {
        autoComplete('TasK', 'requestphase', "#fTaskName", "#fTaskID");
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        var btns = getTaskGridButtons(WorkflowTypeEnum.Request, doCommand, "storeforms", "request");
       // btns[btns.length] = { name: Lang.CreatDraft, bclass: 'create', onpress: doCommand };
        //btns[btns.length] = { name: Lang.CreatePurchaseRequest, bclass: 'create', onpress: doCommand };
        // btns[btns.length] = { name: Lang.CreatDraft, bclass: 'create', onpress: doCommand };
        $("#flexi").flexigrid
                    ({
                        url: _url,
                        dataType: 'xml',
                        colModel:
                        [
                            { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                            { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                            { display: Lang.RequestNo, name: 'NOSerial', sortable: true, css_class: 'width-100 align-center' },
                            { display: Lang.Store, name: 'StoreName', sortable: false, css_class: 'width-70' },
                            { display: Lang.RequestDate, name: 'RequestDate', sortable: true, process: showDate, css_class: 'date-cell' },
                            { display: Lang.Applicant, name: 'RequesterName', sortable: true, css_class: 'width-200 align-center' },
                            { display: Lang.Priority, name: 'PriorityName', sortable: true, css_class: 'width-50 align-center' },
                            { display: Lang.PreStatus, name: 'PreTaskName', sortable: true, css_class: 'width-60 align-center' },
                            { display: Lang.Status, name: 'TaskName', sortable: true, css_class: 'width-60 align-center' },
                            { display: '', name: 'WorkflowColor', css_class: 'WorkflowColor', hide: true },
                            { display: '', name: 'PurchaseRequestID', css_class: '_prid', hide: true },
                            { display: Lang.PurchaseRequest, name: 'PurchaseRequestTitle', css_class: 'width-80 align-center _prtitle' },
                            { display: '', name: 'StoreDraftID', css_class: '_sdid', hide: true },
                            { display: Lang.StoreDraft, name: 'StoreDraftTitle', css_class: 'width-80 align-center _sdtitle' },
                            { display: Lang.NumPrints, name: 'NumPrints', sortable: true},
                            { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, requestshow); }, css_class: 'action-cell' }
                        ],
                        buttons: btns,
                        searchitems: null,
                        sortname: "ID",
                        sortorder: "asc",
                        usepager: true,
                        useRp: true,
                        rp: gridRp,
                        showTableToggleBtn: gridToggle,
                        width: gridWidth,

                        height: gridHeight, resizable: gridResizable,
                        singleSelect: false,
                        onDoubleClick: function (row, grid, options) {
                            var id = row.id.substring(3);
                            requestshow(id);
                        },
                        onSuccess: function (grid) {
                            postProcessGrid();
                            if (startupID != -1) {
                                requestshow(startupID);
                                startupID = -1;
                            }
                        }
                    });
    }
    function postProcessGrid() {
        setTimeout(function () {
            colorTasks();
            showLinks();
        }, 500);
    }
    function showLinks() {
        $('#flexi tr').each(function () {
            var _id = $('._prid div', this).html();
            //alert(_id);
            if (_id && _id != '')
                $('._prtitle div', this).html("<a href='list.aspx?table=PurchaseRequest&id=" + _id + "'>" + $('._prtitle', this).find('div').html() + "</a>")

            _id = $('._sdid div', this).html();
            if (_id && _id != '')
                $('._sdtitle div', this).html("<a href='list.aspx?table=StoreDraft&id=" + _id + "'>" + $('._sdtitle', this).find('div').html() + "</a>")
        });
    }

    

    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['stores,request']);
    checkLevel(2);
    $("#bar").prepend(breadcrumb({pageInfo: {pageTitle: Lang.Request, parentTitle: Lang.MStore, parentUrl: 'menu/store.aspx'} }));
    setClassStore();
    dateSelect('.dateSelect');
    startupID = $.urlParam('id');
    if (!startupID)
        startupID = -1;
    run();
}