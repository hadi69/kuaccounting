﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
92-10-19: file created
*/
function view_paycheckshop() {
    if (_Context.id != -1) {
        replacePrintButton({
            primary: {
                text: Lang.Print,
                href: Router.Generate("print-bank-print", { params: { id: _Context.id } })
            },
            other: [
                {
                    text: Lang.PayCheckReceipt,
                    href: Router.Generate("print-paycheckreceipt", { params: { id: _Context.id }, query: { print: "true" } })
                }
            ]
        });
    } else {
        replacePrintButton({
        });
    }
    makeChosen();
    function displayAmount() {
        $('#AmountStr').html(numToString($('#Amount').val()));
    }

    function bindVouchers() {
            var _autourl1 = 'get.aspx?op=Auto&table=DocumentAccounting&qd=paycheckvouchers&q=' + $('#MasterID').val();
            var res1 = getRowtable(_autourl1);
            if (res1 && res1.length > 0) {
                $('#PayCheckVoucher1').attr('href', "list.aspx?table=DocumentAccounting&id=" + res1[0][0] + "&itemid=" + res1[0][2]);
                $('#PayCheckVoucher1').html(res1[0][1]);
            }
            if (res1 && res1.length > 1) {
                $('#PayCheckVoucher2').attr('href', "list.aspx?table=DocumentAccounting&id=" + res1[1][0] + "&itemid=" + res1[1][2]);
                $('#PayCheckVoucher2').html(res1[1][1]);
            }
    }
    function checkPersonCredit() {
        var personnelID = toInt($('#subDetailForm #PersonnelID').val(), 0);
        clearError();
        if (personnelID > 0) {
            var checkCredit = toFloat(getServerValue("personcheckcredit", "Personnel", "&PersonnelID=" + personnelID), 0);
            if (checkCredit > 0) {
                var amount = toFloat($('#Amount').val());
                if (checkCredit < amount) {
                    showError(Lang.LowCredit);
                }
            }
        }
    }

    function showError(msg) {
        $("#creditmsg").html(msg);
        $("#notification-message").show();
    }

    function clearError() {
        $("#creditmsg").html("");
        $("#notification-message").hide();
    }

    // ------------------------------
    // startup code
    // ------------------------------
    setTimeout(function () {
        $('#BankAccountCheckName').focus();
    }, 1);
    setTimeout(function () {
    if (toInt($('#ID').val(), 0) > 0)
        $("#Amount").focus();
    }, 2);
    setValue('#CheckType');
    dateSelect('.dateSelect');
    setValue('#Status');
    setChecked('#AddToReceiverName');
    if (!hasValuePositive($('#UserID').val()))
        $('#UserID').val(_Context.userID);
    if ($("#PayDate").val() == '')
        $("#PayDate").val(_Context.today);

    autoComplete('Trees', 'treesbanksauto', '#BankName', '#BankTreePath', function (event, data, formatted) {
        // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due
        var hasChild = data[5] != '';
        if (hasChild) {
            $('#BankName, #BankTreePath').val('');
            alert(Lang.HasChild);
        }
    }, formatTreePath, null, null);

    autoComplete('Personnel', 'personnelselectauto', '#subDetailForm #ReceiverName', '#subDetailForm #ReceiverID', null
   , function (data, i, n, value) {
    $('#subDetailForm #ReceiverID').val('');
    return formatPersonnelItem(data);
   });
    autoComplete('BankAccountCheck', 'bankaccountcheckauto', '#BankAccountCheckName', '#BankAccountCheckID'
        , function (event, data, formatted) {
            $('#BankName').val(data[3]);
            $('#BankTreePath').val(data[2]);

            // serial : get unused one
            var serial = toInt(getServerValue("reternpaycheckminserial", "PayCheck", "&BankAccountCheckID=" + data[0]), 0);
            if (!serial) {
                alert(Lang.PayCheckJsM1);
                runQuery('bankaccountcheckupdateformpaycheck', 'PayCheck', '&ID=' + data[0]);
            }
            else {
                $('#Serial').val(serial);
            }

            $('#Serial').attr('readonly', false);
            // #Res.Serial, #Res.Serial Serial2, Amount, DueDate, ReceiverName, No, documentNo, Status
            autoComplete('BankAccountCheck', 'bankaccountcheckserialauto', '#Serial', '#Serial'
                , function (event, data, formatted) {
                    if (data[5] || data[6] || data[7] == 11) // has no or DocumentNo or disproof
                        $('#Serial').val('');
                    else
                        $('#Serial').val(data[0]);
                }, function (data, i, n, value) {
                    // $('#Serial').val('');
                    if (data[5] && data[7] != 11) {
                        // used// data[7] != 11//disproof checks
                        return Lang.Serial + " : " + data[0] + "<br />" + Lang.PayCheck + " - " + Lang.No + " : " + data[5]
                            + ", " + Lang.Reciever + " : " + data[4]
                           + "<br />" + Lang.DueDate + " : " + data[3]
                           + ", " + Lang.Amount + " : " + data[2];
                    }
                    if (data[6]) {
                        return Lang.Serial + " : " + data[0] + "<br />" + Lang.UsedInDocAccounting + " : " + data[6];
                    }
                    if (data[7] == 11) {
                        return Lang.Serial + " : " + data[0] + "<br />" + Lang.DisProof;
                    }
                    return Lang.Serial + " : " + data[0] + "<br />" + Lang.Unused;
                }, null, null, null, 'TreePath=NULL&BankAccountCheckID=' + data[0]);
        });

    $("#Amount").blur(checkPersonCredit);
    if (!hasValuePositive($('#ID').val()))
        $('#Status').val(CheckStatusEnum.New);

    if (!$('#setstatus').val())
        $('#setstatus').val($('#Status').val());

    if ($('#setstatus').val() == CheckStatusEnum.Received) {
        $('#Status, #HiddenStatus').val(CheckStatusEnum.Received);
    }
    else if ($('#setstatus').val() == CheckStatusEnum.Refused) {
        $('#Status, #HiddenStatus').val(CheckStatusEnum.Refused);
    }
    else if ($('#setstatus').val() == CheckStatusEnum.New && $('#setstatus').val() != '') {
        $('#Status, #HiddenStatus').val(CheckStatusEnum.New);
    }
    bindVouchers();
    insertValidation('#subDetailForm');
    monetaryInputsSet('#subDetailForm');
    commaInputs('#subDetailForm');

    $('#Amount').blur(displayAmount).keyup(displayAmount);
    displayAmount();
}