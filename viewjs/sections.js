﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
    92-10-16: file created
*/
function view_sections() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var more = '(SectionType=9 OR SectionType=8)';
        var _url = 'service.asmx/GetList?table=Ward&qd=section&more=' + more + appendTime();
        //alert(_url);
        $("#flexi").flexigrid
                (
                {
                    url: _url,
                    dataType: 'xml',
                    colModel:
                    [
                        { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'right', hide: true },
                        { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                        { display: Lang.Name, name: 'Name', sortable: true, align: 'right', css_class: 'width-300' },
                        { display: Lang.Code, name: 'Code', sortable: true, align: 'right', css_class: 'width-120' },
                        { display: Lang.TreePathName, name: 'TreePathName', width: 80, sortable: true, align: 'right' },
                        { display: Lang.Comments, name: 'Comments', width: 220, sortable: true, align: 'right' },
                        { display: Lang.Enabled, name: 'Enabled', sortable: true, process: showBool, css_class: 'align-center width-40' },
                        { display: Lang.ColEdit, name: 'Edit', sortable: false, hide: hideColumn("manager", "enums", "edit"), process: function (celDiv, id) { editLink(celDiv, id, show) }, css_class: 'action-cell' },
                        { display: Lang.ColDel, name: 'Del', sortable: false, hide: hideColumn("manager", "enums", "delete"), process: delLink, css_class: 'action-cell' }
                    ],
                    buttons:
                    [
                        { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                        { name: Lang.GBEdit, bclass: 'edit', onpress: doCommand },
                        { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                        { separator: true },
                        { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                        { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                    ],
                    searchitems: null,
                    sortname: "Name",
                    sortorder: "asc",
                    usepager: true,
                    useRp: true,
                    rp: gridRp,
                    showTableToggleBtn: _options.gridToggle,
                    width: gridWidth,

                    height: _options.gridHeight, resizable: _options.gridResizable,
                    singleSelect: false,
                    onDoubleClick: function (row, grid, options) {
                        var id = row.id.substring(3);
                        show(id);
                    },
                    onSuccess: function (grid) {
                        restoreSelectedRows('#flexi');
                    }
                }
                );
    }
    function showcallback(v, m, f) {
        var qry = (_Context.id == -1) ? 'sectioninsert' : 'sectionupdate';
        return newMastercallback(v, m, f,_Context.table,qry, 'SectionTreePath', 'sectiontreepathupdate', 'sectiontreepathinsert', 'sectiontreepathdelete','FieldID', '#subDetailForm', show);
    }

    function show(id, celDiv, successCallback) {
        _Context.id = id;
        saveSelectedRows('#flexi');
        var _url = 'get.aspx?op=GetHtml&table=Ward&view=section&qd=section&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
        //jscolor.init();
    }

    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['salary,sections']);
    setClassSys();
    $("#bar").prepend(breadcrumb({ title: Lang.Ward, grandParent: { title: Lang.MSalary, url: 'menu/salary.aspx' }, parent: { title: Lang.SalaryDefinition, url: 'menu/salarydefinition.aspx' } }));
    run();
}