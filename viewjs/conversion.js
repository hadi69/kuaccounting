﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-10-19: file created
*/
function view_conversion() {
    // ------------------------------
    // startup code
    // ------------------------------
    var mID = toInt($('#ID').val(), -1);
    if (mID == -1)
        $('#Enabled').attr("checked", 'checked');
    else
        setChecked("#Enabled");
    dateSelect('.dateSelect');
    //$('#Factor').val(numberWithCommas($('#Factor').val()));
    autoComplete('MonetaryUnit', 'enumauto', '#FromName', '#FromID');
    autoComplete('MonetaryUnit', 'enumauto', '#ToName', '#ToID');
    if ($("#StartDate").val() == '')
        $("#StartDate").val(_Context.today);
    insertValidation();
    commaInputsSet();
    $('#Factor').keyup(function () {
        $(this).val(numberWithCommas($(this).val()));
    });

    $('#StartDate, #EndDate').blur(function () {
        var date = $(this).val();
        var fromId = $('#FromID').val();
        var toId = $('#ToID').val();
        if (date) {
            var ok = getServerValue('checkconversiondaterangeconflict', 'Conversion', '&Date=' + date + '&ConversionID=' + $('#ID').val() + '&FromId=' + fromId + '&ToId=' + toId);
            if (ok == 1) {
                Growl.Error(Lang.ConvertionDateRangeConflict);
                $(this).val('');
                $(this).focus();
            }
        }
    });
}