﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
92-10-29: 
    - file created
*/
function view_shoppurchases() {
    var storeMode = true;
    var startupID = -1;
    function doCommand(com, grid) {
        if (com == Lang.GBRefuse) {
            createPurchase(grid);
        }
        else {
            var isNew = com == Lang.GBNew;
            if (!isNew) {
                var btn = $(".fbutton span:contains(" + com + ")");
                if (btn.hasClass('new'))
                    isNew = true;
            }
            if (isNew) {
                var more = '&Type=' + WorkflowTypeEnum.Purchase;
                var count = getServerValue('checkworkflow', 'Workflow', more);
                if (count == 0 || count == null) {
                    displayError(Lang.WorkflowM1);
                    return;
                }
            }
            if (isNew && com != Lang.GBNew) {
                var desiredEntryTaskID = $(".fbutton span:contains(" + com + ")").attr('class').substring('new '.length);
                //alert(desiredEntryTaskID);
                show(-1, null, null, desiredEntryTaskID);
            }
            else
                generalCommand(com, grid, show);
        }
    }
    function run() {
        autoComplete('TasK', 'purchasephase', "#fTaskName", "#fTaskID");

        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + (_Context.workflowInheritance ? 'purchase' : 'purchasenoinheritance') + "&more=__PrOgrAmconD__" + appendTime();
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell'},
                    { display: Lang.FactorNo, name: 'NOSerial', sortable: true, css_class: 'width-80 align-center' },
                    { display: Lang.SellerNOSerial, name: 'SellerNOSerial', hide: true },
                    { display: Lang.FactorDate, name: 'ReceiptDate', sortable: true, css_class: 'date-cell' },
                    { display: Lang.FactorDate, name: 'Date', hide: true, css_class: 'date-cell' },
                    { display: Lang.Store, name: 'StoreName', sortable: true, css_class: 'width-80 align-center' },
                    { display: Lang.Comments, name: 'Comments', sortable: true, hide: true },
                    { display: 'Inventory', name: 'StoreKeeperName', width: 10, hide: true },
                    { display: 'Accountant', name: 'AccountantName', width: 10, hide: true },
                    { display: Lang.Seller, name: 'SellerName', sortable: true},
                    { display: Lang.TotalPrice, name: 'TotalPrice', sortable: true, process: showMonetary, css_class: 'width-100 align-center', footer: 'sum' },
                    { display: Lang.TotalAmount, name: 'TotalAmount', sortable: true, css_class: 'width-100 align-center', footer: 'sum' },
                    { display: Lang.PreStatus, name: 'PreTaskName', sortable: true, css_class: 'task-cell' },
                    { display: Lang.Status, name: 'TaskName', sortable: true, css_class: 'task-cell' },
                    { display: '', name: 'WorkflowColor', css_class: 'WorkflowColor', hide: true },
                    { display: Lang.Status, name: 'VoucherID', hide: true, process: displayVoucherID },
                    { display: Lang.Status, name: 'DocItemID', hide: true, process: displayDocItemID },
                    { display: Lang.DocumentNumber, name: 'VoucherNo', sortable: false, process: displayVoucherNo, hide: _Context.isMobile, css_class: 'width-80 align-center' },
                    { display: Lang.NumPrints, name: 'NumPrints', css_class: 'width-50 align-center' },
                    { display: Lang.Status, name: 'PurchaseRequestID', hide: true },
                    { display: Lang.PurchaseRequestNo, name: 'PurchaseRequestNo', sortable: false,hide: true },
                    { display: '', name: 'PayDate', hide: true },
                    { display: '', name: 'PayStatus', hide: true },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, hide: hideColumn("sales", "purchase", "edit"), css_class: 'action-cell' }
                ],
                buttons: getTaskGridButtons(WorkflowTypeEnum.Purchase, doCommand, "sales", "purchase"),
                searchitems: null,
                sortname: "NOSerial",
                sortorder: "desc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight, resizable: gridResizable,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                },
                onSuccess: function (grid) {
                    postProcessGrid();
                    if (startupID != -1) {
                        show(startupID);
                        startupID = -1;
                    }
                }
            });
    }
    function displayVoucherID(celDiv, id) {
        $(celDiv).addClass('_vid');
    }
    function displayDocItemID(celDiv, id) {
        $(celDiv).addClass('_diid');
    }
    function displayVoucherNo(celDiv, id) {
        $(celDiv).addClass('_vno');
    }
    function postProcessGrid() {
        setTimeout(function () {
            showVoucherLinks();
            colorTasks();
        }, 500);
    }
    function showVoucherLinks() {
        setTimeout(function () {
            $('#flexi tr').each(function () {
                var _id = $('._vid', this).html();
                var _docitemid = $('._diid', this).html();
                //alert(_id);
                if (_id && _id != '')
                    $('._vno', this).html("<a href='list.aspx?table=DocumentAccounting&id=" + _id + "&itemid=" + _docitemid + "'>" + $('._vno', this).html() + "</a>")
            });
        }, 500);
        //ValidVoucher
    }
    function _displayWorkflowStatus(celDiv, id) {
        var type = toInt($(celDiv).html(), 0);
        displayWorkflowStatus(celDiv, type);
    }
    function checkDate(f) {
        return true;
    }
    function checkDateIntoFinanceYear(f) {
        if (_Context.fyStart == '') {
            alert(Lang.NoCurYear);
            return false;
        }
        if ((f.ReceiptDate == '' || validateDateIntoFinanceYear(f.ReceiptDate)))
            return true;
        if (!_Context.isAccountingVer)
            if (validateDateIntoFinanceYear(f.PayDate))
                return true;
        alert(Lang.PurchasesJsM2 + _Context.fyStart + " - " + _Context.fyEnd + Lang.PurchasesJsM3 );
        return false;
    }
    function checkExpireDate(f) {
        var res = true;
        $('.ExpireDate').each(function (index, value) {
            var id = $(this).attr('id'); // a_0
            id = '#s_' + id.substring(2); // a_0 --> r_0
            var expiredate = $(this).val();
            if (expiredate == '') {
                if (_options.dateType != 'fa') {
                    $(this).val('21/03/2111');
                } else {
                    $(this).val('1490/01/01');
                }
            }
            else
                res = res;
        });
        if (!res)
            alert(Lang.PurchasesJsM4);
        //$('.ExpireDate').each(function (index, value) {
        //    var id = $(this).attr('id'); // a_0
        //    id = '#s_' + id.substring(2); // a_0 --> r_0
        //    var expiredate = $(this).val();
        //    var productdate = $(id).val();
        //    res = res && (compareDate(productdate, expiredate) < 0);
        //});
        //if (!res)
        //    alert("تاریخ انقضا باید بزرگتر از تاریخ تولید باشد");
        return res;
    }
    function checkProductDate(f) {
        var res = true;
        $('.ProductionDate').each(function (index, value) {
            var id = $(this).attr('id'); // a_0
            var productdate = $(this).val();
            res = res && compareDate(productdate, f.Date) <= 0
                && compareDate(productdate, f.ReceiptDate) <= 0
                && compareDate(productdate, f.PayDate) <= 0;
        });
        if (!res)
            alert(Lang.PurchasesJsM5);
        return res;
    }
    function showcallback(v, m, f) {
        if (v == 'TaskImpls') {
            showTaskImpls(_Context.id, WorkflowTypeEnum.Purchase);
            return false;
        }
        if (v == 'ChangeLog') {
            showLogChange(_Context.table, _Context.id, WorkflowTypeEnum.Purchase);
            return false;
        }
        if (v == 'Print') {
            applyPrintFields(_Context.table);
            printSubform();
            return false;
        }
        if (v == 'Cancel')
            return true;
        if (v == 'Documents') {
            if (_Context.id > 0) {
                var _url = 'list.aspx?table=PurchaseDocument&pid=' + _Context.id;
                popup(_url);
            }
            return false;
        }
        if (!checkProgramStatus())
            return false;
        if (v != 'New') {
            if (!checkDate(f))
                return false;
            if (!checkDateIntoFinanceYear(f))
                return false;
            if (!checkExpireDate(f))
                return false;
            //if (!checkProductDate(f))
            //    return false;
        }
        _Context.priceDate = f.ReceiptDate;
        
        if (v != 'OK' && v != 'New') {
            var valid = validateInputs('#detailForm', f);
            if (!valid)
                return false;
            var taskMsg = f.ConfirmTaskMsg;
            //var taskMsg = prompt(Lang.ConfirmTaskMsg, '');
            //if (taskMsg == null)
            //    return false;
            // we need to save it before sending to next task, because the user makes changes and dosen't hit save
            $('.ProductionDate').attr('name', 'ProductionDate');
            $('.ExpireDate').attr('name', 'ExpireDate');
            // here is a bug: all changes must be done in a single request|transaction. fix it later
            generalMastercallback('OK', m, f, 'Purchase', 'purchaseupdate', 'PurchaseItems'
                , 'purchaseitemupdate', 'purchaseiteminsert', 'purchaseitemdelete', 'StoreServiceID', null, show, true);
            if (!_Context.success)
                return false;
            var more = '&RelatedID=' + f.MasterID + '&NextTaskID=' + v + '&UserID=' + _Context.userID + '&Type=' + WorkflowTypeEnum.Purchase
                + '&Msg=' + encodeURIComponent(taskMsg) + '&FileName=' + encodeURIComponent(f.FileName);
            if (runQuery('dotransition', 'TaskImpl', more)) {
                setTimeout(function () { $("#flexi").flexReload(); }, 200);
                return true;
            }
            return false;
        }

        if (!checkWorkflowStatus(f))
            return false;
        var qry = (_Context.id == -1) ? 'purchaseinsert' : 'purchaseupdate';
        var extraQ = (_Context.id == -1) ? 'purchasenewextra' : 'purchaseextra';
        $('.ProductionDate').attr('name', 'ProductionDate');
        $('.ExpireDate').attr('name', 'ExpireDate');
        return newMastercallback(v, m, f, 'Purchase', qry, 'PurchaseItems'
            , 'purchaseitemupdate', 'purchaseiteminsert', 'purchaseitemdelete', 'StoreServiceID', null, show, null, extraQ);
    }
    function showrefusecallback(v, m, f) {
        if (v != 'OK' && v != 'Cancel') {
            var ID = '&RelatedID=' + f.MasterID;
            var taskID = getServerValue('gettaskid', 'TaskImpl', ID);
            var user = _Context.userID;
            var more = '&ID=' + taskID + '&NextTaskID=' + v + '&UserID=' + user;
            runQuery('updatetaskimpl', 'TaskImpl', more);
            v = 'OK';
        }
        var qry = (_Context.id == -1) ? 'refuseinsert' : 'refuseupdate';
        return generalMasterscallback(v, m, f, 'Refuse', qry, 'RefuseItem'
                , 'refuseitemupdate'
                , 'refuseiteminsert'
                , 'refuseitemdelete'
                , 'StoreServiceID');
    }
    function show(id, celDiv, successCallback, desiredEntryTaskID) {
        var btns;
        if (id == -1) {
            btns = getDefaultButtons(false);
        }
        else {
            //if (storeMode)
            btns = getTaskButtons(WorkflowTypeEnum.Purchase, id, false, "sales", "purchase");
            var numDocs = toInt(getServerValue("getnumpds", "PurchaseDocument", "ID=" + id), 0);
            if (numDocs > 0)
                btns[Lang.Docs + ' ' + numDocs] = 'Documents';
            else
                btns[Lang.Docs] = 'Documents';
            //else
            //{
            //    var cls = celDiv ? getColClass($(celDiv).closest('tr'), 11) : getGridRowClass("#flexi", 8);
            //    var wfStatus = getStatus(cls, 3);
            //    btns = wfStatus == 1 ? getCloseButtons() : getDefaultButtons(id != -1);
            //}
        }
        var fID = getServerValue('getfinanceyear', 'FinanceYear', null);
        if (fID == -1) {
            alert(Lang.NoActiveFinancialYear);
            return false;
        }
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&view=shoppurchase&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + '&FinanceYearID=' + fID;
        if (desiredEntryTaskID)
            _url += '&DesiredEntryTaskID=' + desiredEntryTaskID;
        _url += appendTime();
        showPopup(_url, showcallback, btns, null, successCallback, true);
    }
    function createPurchase(grid) {
        //alert(getGridRowContent(grid, 1));
        var _url = 'get.aspx?op=GetHtml&table=Refuse&qd=createrefuse&view=purchasetorefuse&ID=' + getGridRowContent(grid, 1) + appendTime();
        showPopup(_url, showrefusecallback);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['sales,shoppurchaseinvoice', 'storeforms,purchase']);
    storeMode = $.urlParam('menu') != 'accountinga';
    if (storeMode) {
        $("#bar").prepend(breadcrumb({pageInfo: {pageTitle: Lang.Purchase, parentTitle: Lang.MStoreForms, parentUrl: 'menu/storeforms.aspx'} }));
        setClassStoreForms();
    }
    else {
        $("#bar").prepend(breadcrumb({pageInfo: {pageTitle: Lang.PurchaseInvoice, parentTitle: Lang.SellAndBuy, parentUrl: 'menu/sales.aspx'} }));
        setClassSales();
    }
    dateSelect('.dateSelect');
    startupID = $.urlParam('id');
    if (!startupID)
        startupID = -1;
    $('#chprint').click(function () { choosePrintFields(_Context.table); });
    run();
}