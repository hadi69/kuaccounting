﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-11-05: file created
*/
function view_store() {
    var cnt = 0, cnt1 = 0, mID = -1;
    //---------------add row
    function addStoreServices(node) {
        var row = '<tr>';
        row += '<td>' + (cnt + 1) + '</td><td><input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" /><input type="text" id="se_' + cnt + '" name="ServiceName" value="' + getNodeVal(node, "ServiceName") + '" class="servicename snd" /><input type="hidden" name="ServicePriceID" value="' + getNodeVal(node, "ServicePriceID") + '" /></td>';
        row += '<td><input type="text" id="ma_' + cnt + '" name="MaxStock" value="' + getNodeVal(node, "MaxStock") + '" class="validate[custom[number]] narrow MaxStock" /></td>';
        row += '<td><input type="text" id="mi_' + cnt + '" name="MinStock" value="' + getNodeVal(node, "MinStock") + '" class="validate[custom[number]] narrow MinStock" /></td>';
        row += '<td><input type="text" id="or_' + cnt + '" name="OrderPoint" value="' + getNodeVal(node, "OrderPoint") + '" class="validate[custom[integer]] narrow OrderPoint" /></td>';
        row += '<td><input type="text" id="av_' + cnt + '" name="AveragePrice" value="' + getNodeVal(node, "AveragePrice") + '" class="validate[custom[integer]] narrow AveragePrice" /></td>';
        row += '<td><input type="text" id="cu_' + cnt + '" name="CurrentPrice" value="' + getNodeVal(node, "CurrentPrice") + '" class="validate[custom[integer]] narrow CurrentPrice" /></td>';
        row += '<td><input type="text" id="in_' + cnt + '" name="Income" value="' + getNodeVal(node, "Income") + '" class="validate[custom[integer]] narrow Income" readonly="readonly" /></td>';
        row += '<td><input type="text" id="ou_' + cnt + '" name="Outcome" value="' + getNodeVal(node, "Outcome") + '" class="validate[custom[integer]] narrow Outcome" readonly="readonly" /></td>';
        row += '<td><img src="img/delete.gif" id="r_' + cnt + '" class="removeStoreServices" /></td>';
        row += '</tr>';
        cnt++;
        return row;
    }
    //----------end addrow
    function autoPostPack(event, data, formatted) {
        $(event.target).next().val(data[0]); // ID
    }
    //------------
    function removeStoreServices($who) {
        var id = $who.attr('id'); // r_1
        id = id.substring(2); // r_1  -->  1
        $('#se_' + id).val(""); // 
        $('#se_' + id).next().val(-1); // 
        $who.parent().parent().hide();

    }

    function handleRowEvents() {
        $('.removeStoreServices').unbind('click');
        $("#tableStoreServices .servicename").unautocomplete();
        autoComplete('ServicePrice', 'servicepriceauto', "#tableStoreServices .servicename", "#justfurfun"
           , autoPostPack
           , function (data, i, n, value) {
               return "<div class='auto'>" + Lang.Name + data[1] + '<br />'+Lang.Code + data[2] + '<br />' + Lang.Model + data[3] + '<br />'+Lang.Price + data[4] + '</div>';
           });
        //---------
        $('.removeStoreServices').click(function () {
            removeStoreServices($(this));
        });
    }

    // ------------------------------
    // startup code
    // ------------------------------
    mID = toInt($('#MasterID').val(), -1);
    if (mID == -1) {
        $('#CreateInDoc').attr("checked", 'checked');
        $('#CreateOutDoc').attr("checked", 'checked');
        $('#Enabled').attr("checked", 'checked');
    } else {
        setChecked("#Enabled");
        setChecked("#CreateInDoc");
        setChecked("#CreateOutDoc");
    }
    setChecked("#IsWaste");
    setChecked("#IsDefault");
    autoComplete('StoreCategory', 'enumauto', '#StoreCategoryName', '#StoreCategoryID');
    autoComplete('Stores', 'enumauto', '#MainStoreName', '#MainStoreID');
    dateSelect('.dateSelect');

    fillTable("get.aspx?op=GetXml&qd=storeservices&StoreID=" + mID, '#tableStoreServices', addStoreServices);

    $('#StartTimeActivityDaily,#EndTimeActivityDaily').timeEntry(
                    { spinnerImage: 'img/timeentry/spinnerDefault.png', spinnerSize: [20, 20, 0] }); //برای نمایش آیکون مربوط به ساعت

    $('.addStoreServices').click(function () {
        var table = $(this).closest('table');
        $('tr:last', table).after(addStoreServices(null));
        // handleRowEvents();
    });
    // handleRowEvents();
    insertValidation();
}