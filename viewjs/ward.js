﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
93-01-12: file created
*/
function view_ward() {
    var mID = -1;
    function hideMaxPerDay() {
        $('#MaxPerDayLable').hide();
        $('#MaxPerDay').hide();
        if ($('#SectionType').val() == 3) {
            $('#MaxPerDayLable').show();
            $('#MaxPerDay').show();
        }
        else {
            $('#MaxPerDayLable').hide();
            $('#MaxPerDay').hide();

        }
    }
    var cnt = 0, cnt1 = 0, cnt2 = 0;
    function addRow(node) {
        // id="x" --> the validationEngine requires ID

        var row = '<tr>';
        row += '<td>' + (cnt + 1) + '</td><td><input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" /><input type="text" id="it_' + cnt + '" name="RoomNo" value="' + getNodeVal(node, "RoomNo") + '" class="roomno narrow1" /></td>';
        row += '<td><input type="text" id="s_' + cnt + '" name="BedNo" value="' + getNodeVal(node, "BedNo", -1) + '" class="validate[custom[integer]] narrow1 BedNo" /></td>';
        row += '<td><input type="text" id="e_' + cnt + '" name="CompanionPrice" value="' + getNodeVal(node, "CompanionPrice") + '" class="validate[custom[integer]] narrow15 CompanionPrice" /></td>';
        row += '<td><input type="text" id="bt_' + cnt + '" name="BedTypeName" value="' + getNodeVal(node, "BedTypeName") + '" class="bedtypename narrow2" /><input type="hidden" name="BedTypeID" value="' + getNodeVal(node, "BedTypeID") + '" /></td>';
        row += '<td><select id="t1_' + cnt + '" name="Status" style="width:100px;min-width:80px">';
        row += '    <option value="1" ' + (getNodeVal(node, "Status") == '1' ? 'selected=selected' : '') + '>' + Lang.BedOccupied + '</option>';
        row += '    <option value="2" ' + (getNodeVal(node, "Status") == '2' ? 'selected=selected' : '') + '>' + Lang.BedAvailable + '</option>';
        row += '    <option value="3" ' + (getNodeVal(node, "Status") == '3' ? 'selected=selected' : '') + '>' + Lang.BedBad + '</option>';
        row += '    <option value="4" ' + (getNodeVal(node, "Status") == '4' ? 'selected=selected' : '') + '>' + Lang.BedReserved + '</option>';
        row += '    <option value="5" ' + (getNodeVal(node, "Status") == '5' ? 'selected=selected' : '') + '>' + Lang.BedMoving + '</option>';
        row += '    <option value="6" ' + (getNodeVal(node, "Status") == '6' ? 'selected=selected' : '') + '>' + Lang.BedReleasing + '</option>';
        row += '</select></td>';
        row += '<td><input type="text" id="gn_' + cnt + '" name="GendersName" value="' + getNodeVal(node, "GendersName") + '" class="gendersname narrow2" /><input type="hidden" name="GendersID" value="' + getNodeVal(node, "GendersID") + '" /></td>';

        row += '<td><input type="text" id="mi_' + cnt + '" name="MinAge" value="' + getNodeVal(node, "MinAge") + '" class="narrow15 MinAge" /></td>';
        row += '<td><input type="text" id="ma_' + cnt + '" name="MaxAge" value="' + getNodeVal(node, "MaxAge") + '" class="narrow15 MaxAge" /></td>';

        row += '<td><input type="text" id="c_' + cnt + '" name="Comments" value="' + getNodeVal(node, "Comments") + '" class="narrow15 Comments" /></td>';
        row += '<td><input type="checkbox" id="ena_' + cnt + '" name="Enabled' + cnt + '" ' + (getNodeVal(node, "Enabled") == '1' || getNodeVal(node, "Enabled") == 'True' ? 'checked=checked' : '') + ' /></td>';
        //row += '<td><input type="hidden" id="we_' + cnt + '" name="Weight" value="' + getNodeVal(node, "Weight") + '" class="validate[custom[integer]] narrow15 Weight" /></td>';

        row += '<td><img src="img/delete.gif" id="r_' + cnt + '" class="remove" /></td>';
        row += '</tr>';
        cnt++;
        return row;
    }
    function addWardServiceType(node) {
        var row = '<tr>';
        row += '<td>' + (cnt1 + 1) + '</td><td><input type="hidden" name="ID2" value="' + getNodeVal(node, "ID", -1) + '" /><input type="text" id="sen_' + cnt1 + '" name="ServiceTypeName" value="' + getNodeVal(node, "ServiceTypeName") + '" class="servicetypename" style="width:300px;min-width:80px"/><input type="hidden" name="ServiceTypeID" value="' + getNodeVal(node, "ServiceTypeID") + '" /></td>';
        row += '<td><img src="img/delete.gif" id="r1_' + cnt1 + '" class="removewardservicetype" /></td>';
        row += '</tr>';
        cnt1++;
        return row;
    }

    function addWardFund(node) {
        var row = '<tr>';
        row += '<td>' + (cnt2 + 1) + '</td><td><input type="hidden" name="ID3" value="' + getNodeVal(node, "ID", -1) + '" /><input type="text" id="fun_' + cnt2 + '" name="FundName" value="' + getNodeVal(node, "FundName") + '" class="fundname" style="width:150px;min-width:80px"/><input type="hidden" name="FundID" value="' + getNodeVal(node, "FundID") + '" /></td>';
        row += '<td><img src="img/delete.gif" id="r4_' + cnt2 + '" class="removewardfund" /></td>';
        row += '</tr>';
        cnt2++;
        return row;
    }


    // ------------------------------
    // startup code
    // ------------------------------
    // Master Form
    mID = toInt($('#MasterID').val(), -1);
    setChecked("#Enabled");
    setValue("#SectionType");
    setValue("#Type");
    $("#SectionType").change(function () { hideMaxPerDay(); }); $("#Type").val('@Type');
    autoComplete('Location', 'idnameauto', "#LocationName", "#LocationID");
    autoComplete('WardType', 'enumauto', "#WardTypeName", "#WardTypeID");
    autoComplete('Gender', 'enumauto', "#GenderName", "#GenderID");
    autoComplete('Outgo', 'idnameauto', "#OutgoName", "#OutgoID");
    autoComplete('Stores', 'enumauto', "#StoreName", "#StoreID");
    autoComplete('Ward', 'wardlabauto', "#LabName", "#LabID");
    autoComplete('Ward', 'wardpharmacyauto', "#PharmacyName", "#PharmacyID");
    autoComplete('Ward', 'wardradioauto', "#RadiologyName", "#RadiologyID");
    tabify();
    // Detail Form
    // OtherType: 0:Service;1:Drug;2:Test;3:Panel;4:RSService
    fillTable("get.aspx?op=GetXml&qd=bed&WardID=" + mID, '#tableItems', addRow);

    //
    fillTable("get.aspx?op=GetXml&qd=wardservicetype&WardID=" + mID, '#tableWardServiceType', addWardServiceType);

    //--wardfund
    fillTable("get.aspx?op=GetXml&qd=wardfund&WardID=" + mID, '#tableWardFund', addWardFund);

    function doMore() {
        autoComplete('Fund', 'idnameauto', "#tableWardFund .fundname", "#justfurfun", tableAutoPostPack, formatItemNameCode);
        autoComplete('Gender', 'enumauto', "#tableItems .gendersname", "#justfurfun", tableAutoPostPack, formatItemNameCode);
        autoComplete('Personnel', 'personnelauto', "#tableQueue .doctorname", "#justfurfun", tableAutoPostPack);
        autoComplete('ServiceType', 'enumauto', "#tableWardServiceType .servicetypename", "#justfurfun", tableAutoPostPack);
        autoComplete('BedType', 'enumauto', "#tableItems .bedtypename", "#justfurfun", tableAutoPostPack, formatItemNameCode);

        function removeRow($who) {
            var id = $who.attr('id'); // r_1
            id = id.substring(2); // r_1  -->  1
            $('#it_' + id).val(""); // StoreName
            $('#it_' + id).next().val(-1); // StoreID
            $who.parent().parent().hide();
        }
        $('.remove').click(function () {
            removeRow($(this));
        });
        $('.add').click(function () {
            var table = $(this).closest('table');
            $('tr:last', table).after(addRow(null));
            var lastRow = $('tr:last', table);
            autoComplete('Gender', 'enumauto', "#tableItems .gendersname", "#justfurfun", tableAutoPostPack);
            $('.remove', lastRow).click(function () {
                removeRow($(this));
            });
            autoComplete('BedType', 'enumauto', "#tableItems .bedtypename", "#justfurfun", tableAutoPostPack);
            $('.remove', lastRow).click(function () {
                removeRow($(this));
            });
        });
        //--
        function removeQueue($who) {
            var id = $who.attr('id'); // r_1
            id = id.substring(2); // r_1  -->  1
            $('#ti_' + id).val(""); // StoreName
            $('#ti_' + id).next().val(-1); // StoreID
            $who.parent().parent().hide();
        }
        $('.removeQueue').click(function () {
            removeQueue($(this));
        });
        $('.addQueue').click(function () {
            var table = $(this).closest('table');
            $('tr:last', table).after(addQueue(null));
            var lastRowQueue = $('tr:last', table);
            $('.removeQueue', lastRowQueue).click(function () {
                removeQueue($(this));
            });
            $('.OpeningTime, .ClosingTime', lastRowQueue).timeEntry(
          { spinnerImage: 'img/timeentry/spinnerDefault.png', spinnerSize: [20, 20, 0] });
            autoComplete('Personnel', 'personnelauto', "#tableQueue .doctorname", "#justfurfun", tableAutoPostPack);
            $('.removeQueue', lastRowQueue).click(function () {
                removeQueue($(this));
            });
        });
        //--wardfund
        function removeRowwardfund($who) {
            var id = $who.attr('id'); // r_1
            id = id.substring(3); // r_1  -->  1
            $('#fun_' + id).val(""); // StoreName
            $('#fun_' + id).next().val(-1); // StoreID
            $who.parent().parent().hide();
        }
        $('.removewardfund').click(function () {
            removeRowwardfund($(this));
        });
        $('.addWardFund').click(function () {
            var table = $(this).closest('table');
            $('tr:last', table).after(addWardFund(null));
            var lastRow2 = $('tr:last', table);
            autoComplete('Fund', 'idnameauto', "#tableWardFund .fundname", "#justfurfun", tableAutoPostPack);
            $('.removewardfund', lastRow2).click(function () {
                removeRowwardfund($(this));
            });
            $('.removewardfund', lastRow2).click(function () {
                removeRowwardfund($(this));
            });
        });
        //
        function removeRowWardServiceType($who) {
            var id = $who.attr('id'); // r_1
            id = id.substring(2); // r_1  -->  1
            $('#sen_' + id).val(""); // StoreName
            $('sen_' + id).next().val(-1); // StoreID
            $who.parent().parent().hide();
        }
        $('.removewardservicetype').click(function () {
            removeRowWardServiceType($(this));
        });
        $('.addWardServiceType').click(function () {
            var table = $(this).closest('table');
            $('tr:last', table).after(addWardServiceType(null));
            var lastRow1 = $('tr:last', table);
            autoComplete('ServiceType', 'enumauto', "#tableWardServiceType .servicetypename", "#justfurfun", tableAutoPostPack);
            $('.removewardservicetype', lastRow1).click(function () {
                removeRowWardServiceType($(this));
            });

        });

        insertValidation();
    }

    // binds form submission and fields to the validation engine
    hideMaxPerDay();
    doMore();
    insertValidation();
}