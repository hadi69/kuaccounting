﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-10-19: file created
*/
function view_offerstepss() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var id = $.urlParam('id');
        var more = decodeURIComponent("OfferID=" + id);
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + '&more=' + more + appendTime();
        if (_Context.query == "offersfrombrowse") {
            var storeserviceid = $.urlParam('StoreServiceID');
            _url = 'service.asmx/GetQueryList?table=' + _Context.table + '&qd=' + _Context.query + '&StoreServiceID=' + storeserviceid + appendTime();
        }
        else if (_Context.query == "factoroffersfrombrowse") {
            _url = 'service.asmx/GetQueryList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        }
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'center', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.SaleType, name: 'SaleType', sortable: true, process: displaySaleType, css_class: 'width-100 align-center' },
                    { display: Lang.MinValue, name: 'MinValue', sortable: false, process: displayMinValue, css_class: 'width-100 align-center' },
                    { display: Lang.MaxValue, name: 'MaxValue', sortable: false, process: displayMaxValue, css_class: 'width-100 align-center' },
                    { display: Lang.RewardType, name: 'RewardType', sortable: true, process: displayRewardType, css_class: 'width-100 align-center' },
                    { display: Lang.Value, name: 'Amount', sortable: false, process: displayAmount },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, css_class: 'action-cell', process: function (celDiv, id) { editLink(celDiv, id, show); } }
                ],
                buttons:
                [
                   { name: Lang.GBNew, bclass: 'new', onpress: doCommand, },
                   { name: Lang.GBDel, bclass: 'delete', onpress: doCommand, },
                   { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                searchitems: null,
                sortname: "MinValue",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,

                height: gridHeight,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                },
                onSuccess: function (grid) {
                    displayChanges();
                }
            }
            );
    }

    function showcallback(v, m, f) {

        var qry = (_Context.id == -1) ? 'offerstepsinsert' : 'offerstepsupdate';
        return newMastercallback(v, m, f, 'OfferSteps', qry, 'OfferRewards', 'offerrewardsupdate', 'offerrewardsinsert', 'offerrewardsdelete', 'ServiceName', '#subDetailForm', show);
    }
    function displaySaleType(celDiv, id) {
        $(celDiv).addClass('_st');
        var _mode = parseInt($(celDiv).html(), 12);
        displayOfferStepSaleType(celDiv, _mode);
    }
    function displayRewardType(celDiv, id) {
        $(celDiv).addClass('_rt');
        var _mode = parseInt($(celDiv).html(), 12);
        displayOfferStepRewardType(celDiv, _mode);
    }
    function displayMinValue(celDiv, id) {
        $(celDiv).addClass('_minv');
    }
    function displayMaxValue(celDiv, id) {
        $(celDiv).addClass('_maxv');
    }
    function displayAmount(celDiv, id) {
        $(celDiv).addClass('_a');
    }
    function displayChanges() {
        setTimeout(function () {
            $('#flexi tr').each(function () {
                var saleType = $('._st', this).html();// 0:Numerical, 1:Amount1
                var rewardType = $('._rt', this).html();// 0:Commodity, 1:Discount, 2:Deadline, 3:Pursant
                if (saleType == Lang.Amount1) {
                    $('._minv', this).html(numberWithCommas(toMonetary($('._minv', this).html())));
                    $('._maxv', this).html(numberWithCommas(toMonetary($('._maxv', this).html())));
                }
                if (rewardType == Lang.Discount || rewardType == Lang.Pursant) {
                    if (toFloat($('._a', this).html()) < 0) {
                        $('._a', this).html(numberWithCommas(toMonetary(-1 * $('._a', this).html())) + ' %');
                    } else {
                        $('._a', this).html(numberWithCommas(toMonetary($('._a', this).html())));
                    }
                }
            });
        }, 50);
        //ValidVoucher
    }
    function show(id, celDiv, successCallback) {
        var OfferID = $.urlParam('id');
        var OfferNo = getServerValue('offersno', 'Offers', '&ID=' + OfferID);
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=offersteps&id=' + id + '&OfferID=' + OfferID + '&OfferNo=' + OfferNo + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    $("#bar").prepend(breadcrumb({
        title: Lang.OfferSteps,
        grandParent: {
            title: Lang.MDistribution,
            url: 'menu/distribution.aspx'
        },
        parent: {
            title: Lang.Offers,
            url: 'list.aspx?table=Offers&menu=basicsa'
        }
    }));
    run();
}