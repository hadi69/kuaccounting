﻿function view_rate() {
    // ------------------------------
    // startup code
    // ------------------------------
    $('.popupTitle').html(_Context.title);
    dateSelect('.dateSelect');
    $('.dateSelect').keydown(function (e) {
        if (e.keyCode == 40)
            displayDatePicker2(this);
    });
    setChecked("#Enabled");
    insertValidation();
}