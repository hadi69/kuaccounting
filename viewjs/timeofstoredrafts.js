﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-10-30: file created
*/
function view_timeofstoredrafts() {
    var storeMode;
    var startupID = -1;
    function doCommand(com, grid) {
        var isNew = com == Lang.GBNew;
        if (!isNew) {
            var btn = $(".fbutton span:contains(" + com + ")");
            if (btn.hasClass('new'))
                isNew = true;
        }
        if (isNew) {
            var more = '&Type=' + WorkflowTypeEnum.StoreDraft;
            var count = getServerValue('checkworkflow', 'Workflow', more);
            if (count == 0 || count == null) {
                displayError(Lang.WorkflowM1);
                return;
            }
        }
        if (isNew && com != Lang.GBNew) {
            var desiredEntryTaskID = $(".fbutton span:contains(" + com + ")").attr('class').substring('new '.length);
            //alert(desiredEntryTaskID);
            show(-1, null, null, desiredEntryTaskID);
        }
        else
            generalCommand(com, grid, show);
    }
    function purchaseRun() {
        var _url = "service.asmx/GetList?table=Purchase&qd=purchase&more=__PrOgrAmconD__ AND (PayDate BETWEEN " + encodeURIComponent("GETDATE() - [dbo].[ToInt]([dbo].[GetSetting]('WarningTimeChecks'),3) AND GETDATE()+[dbo].[ToInt]([dbo].[GetSetting]('WarningTimeChecks'),3)) AND PayStatus = 0 AND PayMethod IN (2,3)") + appendTime();
        //autoComplete('TasK', 'storedraftphase', "#fTaskName", "#fTaskID");
        //alert(_url);
        $("#secondFlexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'center', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.FactorNo, name: 'NOSerial', sortable: true, css_class: 'width-80 align-center' },
                    { display: '', name: 'SellerNOSerial', sortable: true, hide: true },
                    { display: Lang.FactorDate, name: 'ReceiptDate', sortable: true, process: capturePriceDate, css_class: 'date-cell' },
                    { display: '', name: 'Date', sortable: true, hide: true },
                    { display: Lang.Store, name: 'StoreName', sortable: true, hide: true },
                    { display: Lang.Comments, name: 'Comments', sortable: true, hide: true },
                    { display: Lang.Inventory, name: 'StoreKeeperName', sortable: true, hide: true },
                    { display: Lang.Accountant, name: 'AccountantName', sortable: true, hide: true },
                    { display: Lang.BuyerName, name: 'SellerName', sortable: true, css_class: 'width-200 align-center' },
                    { display: Lang.TotalS, name: 'TotalPrice', sortable: true, process: showMonetary, css_class: 'width-80 align-center', footer: 'sum' },
                    { display: Lang.TotalAmount, name: 'TotalAmount', sortable: true, css_class: 'width-100 align-center', footer: 'sum' },
                    { display: Lang.PreStatus, name: 'PreTaskName', sortable: true, hide: true },
                    { display: Lang.Status, name: 'TaskName', sortable: true, css_class: 'width-100 align-center' },
                    { display: '', name: 'WorkflowColor', css_class: 'WorkflowColor', hide: true },
                    { display: Lang.Status, name: 'VoucherID', hide: true },// process: displayVoucherID},
                    { display: Lang.DocumentNumber, name: 'VoucherNo', sortable: false },// process: displayVoucherNo, hide: true },
                    { display: Lang.NumPrints, name: 'NumPrints', sortable: true, hide: true },
                    { display: Lang.Status, name: 'PurchaseRequestID', hide: true },
                    { display: Lang.PurchaseRequestNo, name: 'PurchaseRequestNo', sortable: false, hide: true },
                    { display: Lang.PayDate, name: 'PayDate', sortable: true, process: showDate, css_class: 'date-cell' },
                    { display: Lang.PayStatus, name: 'PayStatus', sortable: false, process: displayPayStatus},
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: myEditLinkPurchase, hide: hideColumn("sales", "purchase", "edit"), css_class: 'action-cell' }
                ],
                // buttons: getTaskGridButtons(WorkflowTypeEnum.StoreDraft, doCommand, "sales", "storedraft"),
                buttons:
                   [
                       { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                       { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                   ],
                searchitems: null,
                sortname: "NOSerial",
                sortorder: "desc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,

                height: gridHeight, resizable: gridResizable,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    purchaseshow(id);
                },
                //onSuccess: function (grid) {
                //    showVoucherLinks();
                //}
            });
    }
    function storedraftRun() {
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + "&more=__PrOgrAmconD__ AND (PayDate BETWEEN " + encodeURIComponent("GETDATE() - [dbo].[ToInt]([dbo].[GetSetting]('WarningTimeChecks'),3) AND GETDATE()+[dbo].[ToInt]([dbo].[GetSetting]('WarningTimeChecks'),3)) AND PayStatus = 0 AND PayMethod IN (2,3)") + appendTime();
        //autoComplete('TasK', 'storedraftphase', "#fTaskName", "#fTaskID");
        //alert(_url);
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'center', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.FactorNo, name: 'NOSerial', sortable: true, css_class: 'width-80 align-center' },
                    { display: Lang.FactorDate, name: 'FactorDate', sortable: true, process: capturePriceDate, css_class: 'date-cell' },
                    { display: Lang.Store, name: 'StoreName', sortable: true, hide: true },
                    { display: Lang.Comments, name: 'Comments', sortable: true, hide: true },
                    { display: Lang.Inventory, name: 'StoreKeeperName', sortable: true, hide: true },
                    { display: Lang.Accountant, name: 'AccountantName', sortable: true, hide: true },
                    { display: Lang.BuyerName, name: 'BuyerNameComment', sortable: true, css_class: 'width-200 align-center' },
                    { display: Lang.Visitor, name: 'VisitorName', sortable: true, css_class: 'width-100 align-center' },
                    { display: Lang.TotalS, name: 'TotalPrice', sortable: true, process: showMonetary, css_class: 'width-80 align-center', footer: 'sum' },
                    { display: Lang.TotalAmount, name: 'TotalAmount', sortable: true, css_class: 'width-100 align-center', footer: 'sum' },
                    { display: Lang.SumProfit, name: 'SumProfit', hide: true },
                    { display: Lang.PreStatus, name: 'PreTaskName', sortable: true, hide: true },
                    { display: Lang.Status, name: 'TaskName', sortable: true, css_class: 'width-100 align-center' },
                    { display: Lang.PayMethod, name: 'PayMethod', sortable: true, hide: true },
                    { display: '', name: 'WorkflowColor', css_class: 'WorkflowColor', hide: true },
                { display: Lang.Status, name: 'VoucherID', hide: true },// process: displayVoucherID },
                { display: Lang.DocumentNumber, name: 'VoucherNo', sortable: false },// process: displayVoucherNo, hide: true },
                    { display: Lang.PayDate, name: 'PayDate', sortable: true, process: showDate, css_class: 'date-cell' },
                    { display: Lang.PayStatus, name: 'PayStatus', sortable: false, process: displayPayStatus },
                    { display: Lang.NumPrints, name: 'NumPrints', sortable: true, hide: true },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: myEditLinkStoreDraft, hide: hideColumn("sales", "storedraft", "edit"), css_class: 'action-cell' }
                ],
                // buttons: getTaskGridButtons(WorkflowTypeEnum.StoreDraft, doCommand, "sales", "storedraft"),
                buttons:
                   [
                       { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                       { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                   ],
                searchitems: null,
                sortname: "NOSerial",
                sortorder: "desc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,

                height: gridHeight, resizable: gridResizable,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    purchaseshow(id);
                },
                //onSuccess: function (grid) {
                //    showVoucherLinks(); 
                //}
            });
    }
    function myEditLinkStoreDraft(celDiv, id) {
        $(celDiv).html('<img src="img/edit.gif" />');
        $(celDiv).click(function (event) {
            showMenu({
                parent: this, width: '100px',
                cmds: [[Lang.ColEdit, 'Edit']
                    , [Lang.Paid,'Pay']],
                callback: function (cmd) {
                    if (cmd == 'Edit')
                        storedraftshow(id);
                    else if (cmd == 'Pay')
                        updateStoreDraftPayStatus(id);
                     }
            });
        });
    }

    function myEditLinkPurchase(celDiv, id) {
        $(celDiv).html('<img src="img/edit.gif" />');
        $(celDiv).click(function (event) {
            showMenu({
                parent: this, width: '100px',
                cmds: [[Lang.ColEdit, 'Edit']
                    , [Lang.Paid, 'Pay']],
                callback: function (cmd) {
                    if (cmd == 'Edit')
                        purchaseshow(id);
                    else if (cmd == 'Pay')
                        updatePurchasePayStatus(id);
                }
            });
        });
    }

    function updateStoreDraftPayStatus(id) {
        runQuery('changestoredraftpaystatus', 'StoreDraft', '&PayStatus=1&ID=' + id);
        $("#flexi").flexReload();
    }
    function updatePurchasePayStatus(id) {
        runQuery('changepurchasepaystatus', 'Purchase', '&PayStatus=1&ID=' + id);
        $("#secondFlexi").flexReload();
    }
    function displayPayStatus(celDiv, id) {
        var _mode = parseInt($(celDiv).html(), 16);
        displayStoredraftPayStatus(celDiv, _mode);
    }
    //function displayVoucherID(celDiv, id) {
    //    $(celDiv).addClass('_vid');
    //}
    //function displayVoucherNo(celDiv, id) {
    //    $(celDiv).addClass('_vno');
    //}
    //function showVoucherLinks() {
    //    setTimeout(function () {
    //        $('#flexi tr').each(function () {
    //            var _id = $('._vid', this).html();
    //            //alert(_id);
    //            if (_id && _id != '')
    //                $('._vno', this).html("<a href='list.aspx?table=DocumentAccounting&id=" + _id + "'>" + $('._vno', this).html() + "</a>");
    //        });
    //    }, 500);
    //    //ValidVoucher
    //}


    // ------------------------------
    // startup code
    // ------------------------------
    tabify();
    if (!checkAccess(['sales,salesinvoice', 'stores,storedraft'])) {
        checkAccess(['sales,purchaseinvoice', 'stores,purchase']); 
    };
    dateSelect('.dateSelect');
    storedraftRun();
    purchaseRun();
}