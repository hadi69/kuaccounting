﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-10-28: file created
*/
function view_accreports() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=accreport&more=__PrOgrAmconD__' + appendTime();
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell'},
                    { display: Lang.ReportTitle, name: 'ReportTitle', sortable: true, css_class: 'width-200 align-center'},
                    { display: Lang.Comments, name: 'Comments', sortable: true},
                    { display: Lang.Enabled, name: 'Enabled', sortable: true, process: showBool, css_class: 'width-40 align-center' },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, hide: hideColumn("manager", "enums", "edit"), process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' }
                ],
                buttons:
                [
                    { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                    { name: Lang.GBEdit, bclass: 'edit', onpress: doCommand },
                    { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                    { separator: true },
                    { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                    { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                searchitems: null,
                sortname: "ID",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: _options.gridToggle,
                width: _options.gridWidth,
                height: _options.gridHeight, resizable: _options.gridResizable,
                singleSelect: true,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                },
                onSuccess: function (grid) {
                    restoreSelectedRows('#flexi');
                }
            }
            );
    }
    function showcallback(v, m, f) {
        var qry = (_Context.id == -1) ? 'accreportinsert' : 'accreportupdate';
        return generalMastercallback(v, m, f, 'AccReport', qry, 'AccReportCol', 'accreportcolupdate', 'accreportcolinsert', 'accreportcoldelete'
            , 'Title', null, show);
    }
    function show(id) {
        _Context.id = id;
        saveSelectedRows('#flexi');
        var _url = 'get.aspx?op=GetHtml&table=AccReport&view=accreport&qd=accreport&id=' + id + appendTime();
        showPopup(_url, showcallback);
    }
 
    // ------------------------------
    // startup code
    // ------------------------------
    $("#bar").prepend(breadcrumb({pageInfo: {pageTitle: Lang.CreatAccountingReport, parentTitle: Lang.Reports, parentUrl: 'menu/reports.aspx'} }));
    setClassReports();
    run();
}