﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-10-19: file created
*/
function view_loans() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + "&more=__PrOgrAmconD__" + appendTime();
        //alert(_url);
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.LoanType, name: 'LoanType', sortable: false, process: disType, css_class: 'width-100 align-center' },
                    { display: Lang.headline, name: 'Title', sortable: true, css_class: 'width-300 align-center' },
                    { display: Lang.Amounts, name: 'Amounts', sortable: false, process: showMonetary, css_class: 'width-200 align-center' },
                    { display: Lang.InstallmentAmount, name: 'InstallmentAmount', sortable: false, process: showMonetary, css_class: 'width-120 align-center' },
                    { display: Lang.PaidAmount, name: 'PaidAmount', sortable: false, process: showMonetary },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, hide: hideColumn("bank", "loans", "edit"), css_class: 'action-cell' }
                ],
                buttons:
                [
                   { name: Lang.GBNew, bclass: 'new', onpress: doCommand, onaccess: hasAccess("bank", "loans", "new") },
                   { name: Lang.GBDel, bclass: 'delete', onpress: doCommand, onaccess: hasAccess("bank", "loans", "delete") },
                   { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand, onaccess: hasAccess("bank", "loans", "excel") }
                ],
                searchitems: null,
                sortname: "Title",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,

                height: gridHeight,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                }
            }
            );
    }

    function showcallback(v, m, f) {
        $('.PayDate').attr('name', 'PayDate');
        $('.DueDate').attr('name', 'DueDate');
        //var installmentAmount;
        //$('.Amount').each(function () {
        //    installmentAmount = toFloat(installmentAmount,0) + toFloat($(this).val(), 0);
        //});
        //$('#InstallmentAmount').val(toFloat(installmentAmount,0));
        var qry = (_Context.id == -1) ? 'loaninsert' : 'loanupdate';
        return newMastercallback(v, m, f, 'Loan', qry, 'Installment', 'installmentupdate', 'installmentinsert', 'installmentdelete', 'Amount', null, show);
    }
    function show(id, celDiv, successCallback) {

        var fID = getServerValue('getfinanceyear', 'FinanceYear', null);
        if (fID == -1) {
            alert(Lang.NoActiveFinancialYear);
            return false;
        }
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + '&FinanceYearID=' + fID + appendTime();
        showPopup(_url, showcallback, getDefaultButtons(true, 'bank', "loans"), null, successCallback);
    }

    function disType(celDiv, id) {
        var _mode = parseInt($(celDiv).html(), 2);
        displayLoanType(celDiv, _mode);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['bank,loan']);
    checkLevel(2);
    $("#bar").prepend(breadcrumb({pageInfo: {pageTitle: Lang.Loans, parentTitle: Lang.BankAndCheck, parentUrl: 'menu/bank.aspx'} }));
    setClassBank();
    run();
}