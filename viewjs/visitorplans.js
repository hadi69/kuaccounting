﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

function view_visitorplans() {
    function doCommand(com, grid) {
        //we have to because we need VisitorPlanID when we are in form
        if (com == Lang.GBNew) {
            var id = getServerValue('visitorplaninsert', 'VisitorPlan');
            show(id);
            return;
        }
        if (com == Lang.RepeatPlan) {
            var id = getRowID(grid);
            if (id != -1) {
                var btns = getDefaultButtons();
                var op = "GetHtml";
                var _url = 'get.aspx?op=' + op + '&table=' + _Context.table + '&qd=' + _Context.query + '&view=visitorplanrepeat&VisitorPlanID='+ id + appendTime();
               showPopup(_url, repeatPlan, btns, '1%', null, true);
            }
            return;
        }
        generalCommand(com, grid, show);
        }

    function run(parameters) {
        var _url = 'service.asmx/GetList?table=VisitorPlan&qd=visitorplan' + appendTime();
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.Visitor, name: 'VisitorName', sortable: true, css_class: 'width-200 align-center' },
                    { display: Lang.StartDate, name: 'StartDate', sortable: true, css_class: 'date-cell' },
                    { display: Lang.EndDate, name: 'EndDate', sortable: true, css_class: 'date-cell' },
                    { display: Lang.Comments, name: 'Description', sortable: true },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' },
                    { display: Lang.Days, name: 'VisitorPlanDays', sortable: false, process: visitorPlanDaysDefination, css_class: 'width-50 align-center' }
                ],
                buttons:
                [
                   { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                   { name: Lang.RepeatPlan, bclass: 'RepeatPlan', onpress: doCommand },
                   { name: Lang.GBDel, bclass: 'delete', onpress: doCommand},
                   { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand}
                ],
                sortname: "VisitorName",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight, resizable: gridResizable,
                singleSelect: false
            });
    }
function showcallback(v, m, f) {
    return newShowcallback(v, m, f, 'visitorplanupdate', 'visitorplaninsert', _Context.table, '#subDetailForm', show);
}
function show(id, celDiv, successCallback) {
    _Context.id = id;
    var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
    showPopup(_url, showcallback, null, null, successCallback);
}
function visitorPlanDaysDefination(celDiv, id) {
    $(celDiv).html('<a href="list.aspx?table=VisitorPlanDays&id=' + id + '"><img src="img/task_icon.jpg" /></a>');
}

function repeatPlan(v, m, f) {
    if (v == 'OK') {
        var more = '&VisitorPlanID=' + $('#VisitorPlanID').val() + '&RepeatStartDate=' + $('#RepeatStartDate').val() + '&Weekdays='
            + $('#Weekdays').val() + '&NumOfRepeat=' + $('#NumOfRepeat').val() + '&RepeatType=' + $('#RepeatType').val() + '&HolidayCopy=' + $('#HolidayCopy').attr('checked');
        runQuery('repeatplan', 'VisitorPlan', more);
        var title = $('.popupTitle').html();
        $('.popupTitle').html(Lang.RepeatSuccess);
        $('.popupTitle').animate({ 'background-color': '#f00' }, 1000, 'linear', function () { $('.popupTitle').animate({ 'background-color': '#00496F' }, 1000, 'linear'); $('.popupTitle').html(title); });
        return true;
    }
    return true;

}


// ------------------------------
    // startup code
    // ------------------------------
checkAccess(['distribution,visitorplan']);
    $("#bar").prepend(breadcrumb({
        title: Lang.VisitorPlan,
        parent: {
            title: Lang.MDistribution,
            url: 'menu/Distribution.aspx'
        }
    }));

    setClassDistribution();
    
    run();
}