﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

function view_membership() {

    $("#tableItems").smartTable();
    var cnt = 0, mID = -1;

    function addLoanItem(node) {
        var row = '<tr>';

        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">' + (cnt + 1) + '</td>';

        row += '<td data-th="' + Lang.PayYear + '">'
            + '<input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="hidden"  name="MembershipID" value="' + getNodeVal(node, "MembershipID", -1) + '" />'
            + '<input type="text" name="PayYear' + cnt + '" id="py_' + cnt + '" value="' + getNodeVal(node, "PayYear")
            + '" class="PayYear form-control input-sm align-center" />'
            + '</td>';
        row += '<td data-th="' + Lang.PayMonth + '">'
            + '<input type="text" id="pm_' + cnt + '" name="PayMonth" value="' + getNodeVal(node, "PayMonth") + '" class="PayMonth form-control input-sm"/>'
            + '</td>';
        row += '<td data-th="' + Lang.PayAmount + '">'
            + '<input type="text" id="pa_' + cnt + '" name="PayAmount" value="' + getNodeVal(node, "PayAmount") + '" class="PayAmount validate[custom[integer]] form-control input-sm"/>'
            + '</td>';

        row += '</tr>';

        cnt++;
        return row;
    }

    function doMore() {
        dateSelect("#tableItems .dateSelect");
        $('.addLoanItem').click(function () {
            var table = $("#tableItems");
            $('tr:last', table).after(addLoanItem(null));
            Growl.Notice(Lang.RowAddedSuccessfully);
            dateSelect('#tableItems tr:last .dateSelect');
        });
    }


    doMore();

    mID = toInt($("#MasterID").val(), -1);
    fillTable("get.aspx?op=GetXml&table=MembershipItems&qd=membershipitem&MembershipID=" + mID, '#tableItems', addLoanItem);
    autoComplete('Funds', 'fundsName', '#FundName', '#FundID');
    autoComplete('Personnel', 'staffauto', '#PersonnelName', '#PersonnelID');
    setValue('#Status');
    insertValidation();
    commaInputs();
    makeChosen();
    monetaryKeys();
}