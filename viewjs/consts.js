﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
93-05-18: file created
*/
function view_consts() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var id = $.urlParam('id');
        var more = decodeURIComponent("ConstDefinitionID=" + id);
        var terminology = getTerminology(_Context.table);
        if (terminology)
            $('#term').html("<a href='help/terminology.aspx' target=_blank title='" + Lang.HasTerminology + "'>" + terminology + "</a>");
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + "&more=" + more + appendTime();
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.ConstDefinition, name: 'ConstDefinitionName', sortable: true, css_class: 'width-150 align-center' },
                    { display: Lang.Title, name: 'Title', sortable: true},
                    { display: Lang.CreationDate, name: 'CreationDate', sortable: true, css_class: 'date-cell' },
                    { display: Lang.StartDate, name: 'StartDate', sortable: true, css_class: 'date-cell' },
                    { display: Lang.EndDate, name: 'EndDate', sortable: true, css_class: 'date-cell' },
                    { display: Lang.ConstType, name: 'ConstType', sortable: true, css_class: 'width-80 align-center hidden', process:displayConstType},
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, css_class: 'action-cell', process: function (celDiv, id) { editLink(celDiv, id, show); } },
                    { display: Lang.ColDel, name: 'Del', sortable: false, css_class: 'action-cell', process: delLink }
                ],
                buttons:
                [
                    { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                    { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                    { separator: true },
                    { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                    { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                sortname: "CreationDate",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight,
                singleSelect: false
            }
            );
    }
    function displayConstType(celDiv, id){
        var type = parseInt($(celDiv).html(), 0);
        if (type == 0)
        {
            $(celDiv).html(Lang.Order);
            $(celDiv).addClass("cfs0");
        }
        else if (type == 1) {
            $(celDiv).html(Lang.Fish);
            $(celDiv).addClass("cfs1");
        }
    }
    function showcallback(v, m, f) {
        if (v == 'OK')
        {
            var qry = (_Context.id == -1) ? 'constsinsert' : 'constsupdate';
            return newMastercallback(v, m, f, _Context.table, qry, 'ConstValues'
                , 'constvaluesupdate'
                , 'constvaluesinsert'
                , 'constvaluesdelete'
                , '', null, show);
        }
        if (v == 'New')
            show(-1);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        var ConstDefinitionID = $.urlParam('id');
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + '&ConstDefinitionID=' + ConstDefinitionID + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    $("#bar").prepend(breadcrumb({
        title: Lang.Consts,
        grandParent: {
            title: Lang.MSalary,
            url: 'menu/salary.aspx'
        },
        parent: {
            title: Lang.ConstDefinition,
            url: 'list.aspx?table=ConstDefinitions&menu=salarysa'
        }
    }));
    setClassSalary();
    run();
}