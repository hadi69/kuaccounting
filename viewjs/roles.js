﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    93-01-18: file created
*/
function view_roles() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        //alert(_url);
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell'},
                    { display: Lang.Title, name: 'Title', sortable: true},
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' },
                    { display: Lang.RoleUsers, name: 'Edit', sortable: false, process: function (celDiv, id) { editLinkrole(celDiv, id, 'Users', 'roleusers', '&view=roleusers&roleid=' + id, function () { }); }, css_class: 'action-cell' }
                ],
                buttons:
                [
                   { name: Lang.GBNew, bclass: 'new', onpress: doCommand},
                   { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                   { separator: true },
                    { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                    { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                sortname: "Title",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight, resizable: gridResizable,
                singleSelect: true,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                },
                onSuccess: function (grid) {
                    restoreSelectedRows('#flexi');
                }
            }
            );
    }
    function showcallback(v, m, f) {
        return newShowcallback(v, m, f, 'roleupdate', 'roleinsert', _Context.table, null, show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&id=' + id + appendTime();
        saveSelectedRows("#flexi", id);
        showPopup(_url, showcallback, null, null, successCallback);
    }

    function editLinkrole(celDiv, id, table, qd, more, callback, op) {
        op = op || 'GetHtml';
        $(celDiv).html('<span class="glyphicon glyphicon-user"></span>');
        $(celDiv).click(function (event) {
            event.preventDefault();
            if (clicksDisabled)
                return;
            disableClicks();
            var _url = 'get.aspx?op=' + op + '&table=' + table + '&qd=' + qd + '&id=' + id + more + appendTime();
            $.ajax({
                url: _url,
                type: 'GET',
                success: function (html) {
                    $.prompt(html, {
                        submit: callback,
                        buttons: getButtons(),
                        focus: 1
                    });
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert('Status=' + xhr.status + ' StatusText=' + xhr.statusText + ' thrownError=' + thrownError);
                }
            });
        });
    }

    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['admin,roles']);
    $("#bar").prepend(breadcrumb({
        title: Lang.Permission,
        parent: {
            title: Lang.Management,
            url: 'menu/admin.aspx'
        }
    }));
    setClassManager();
    run();
}