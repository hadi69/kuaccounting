﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
92-11-08: file created
*/
function view_visitortimeorder() {
    // ------------------------------
    // startup code
    // ------------------------------
    var id = $("#VisitorPlanDaysID").val();
    if (!id)
        id = 0;

    var _url = 'service.asmx/GetQueryList?table=VisitorTime&qd=visitortimeorder&ID=' + id + appendTime();

    $("#scondFlexi").flexigrid
               (
               {
                   url: _url,
                   dataType: 'xml',
                   colModel:
                   [
                        { display: 'ID', name: 'ID', hide: true },
                        { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                        { display: Lang.FactorNo, name: 'FactorNo', css_class: 'width-100 align-center' },
                        { display: Lang.BuyerName, name: 'BuyerName', css_class: 'width-200 align-center' },
                        { display: Lang.Time, name: 'Time', css_class: 'width-60 align-center' },
                        { display: Lang.TimeDifference, name: 'TimeDifference' }
                   ],
                   searchitems: null,
                   sortname: "ID",
                   sortorder: "asc",
                   usepager: false,
                   rpOptions: [20, 50, 1000, 1500, 3000], //allowed per-page values
                   rp: 1500,
                   useRp: false,
                   rp: gridRp,
                   showTableToggleBtn: false,
                   width: gridWidth,
                   height: 400, resizable: gridResizable,
                   singleSelect: true,
               }
               );


    function showId(celDiv) {
        $(celDiv).addClass('_id');
    }

    function showNo(celDiv) {
        $(celDiv).addClass('_no');
    }

    function createLink() {
        setTimeout(function () {
            $('#scondFlexi tr').each(function () {
                var _id = $('._id', this).html();
                if (_id && _id != '' && _id.trim() != '') {
                    if ($('._pType', this).html())
                        $('._no', this).html("<a href='list.aspx?table=Purchase&id=" + _id + "'>" + $('._no', this).html() + "</a>");
                    else if ($('._sType', this).html())
                        $('._no', this).html("<a href='list.aspx?table=StoreDraft&id=" + _id + "'>" + $('._no', this).html() + "</a>");
                    else if ($('._tType', this).html())
                        $('._no', this).html("<a href='list.aspx?table=Transfer&id=" + _id + "'>" + $('._no', this).html() + "</a>");
                    else if ($('._rnType', this).html())
                        $('._no', this).html("<a href='list.aspx?table=Returns&id=" + _id + "'>" + $('._no', this).html() + "</a>");
                    else if ($('._reType', this).html())
                        $('._no', this).html("<a href='list.aspx?table=Refuse&id=" + _id + "'>" + $('._no', this).html() + "</a>");
                }
            });
        }, 500);
    }
    // createLink();
}