﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-10-19: file created
*/
function view_offerconditionss() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var id = $.urlParam('id');
        var more = decodeURIComponent("OfferID=" + id);
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + "&more=" + more + appendTime();
        //alert(_url);
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'center', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.GroupingName, name: 'GroupingName', width: 200, sortable: false, css_class: 'align-right' },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, css_class: 'action-cell', process: function (celDiv, id) { editLink(celDiv, id, show); } }
                ],
                buttons:
                [
                   { name: Lang.GBNew, bclass: 'new', onpress: doCommand, },
                   { name: Lang.GBDel, bclass: 'delete', onpress: doCommand, },
                   { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                searchitems: null,
                sortname: "GroupingName",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,

                height: gridHeight,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                }
            }
            );
    }

    function showcallback(v, m, f) {

        var qry = (_Context.id == -1) ? 'offerstepsinsert' : 'offerstepsupdate';
        return newShowcallback(v, m, f, 'offerconditionsupdate', 'offerconditionsinsert', 'OfferConditions', '#detailForm', show);
    }
    function show(id, celDiv, successCallback) {
        var OfferID = $.urlParam('id');
        var OfferNo = getServerValue('offersno', 'Offers', '&ID=' + OfferID);
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + '&OfferID=' + OfferID + '&OfferNo=' + OfferNo + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    $("#bar").prepend(breadcrumb({
        title: Lang.OfferConditions,
        grandParent: {
            title: Lang.MDistribution,
            url: 'menu/distribution.aspx'
        },
        parent: {
            title: Lang.Offers,
            url: 'list.aspx?table=Offers&menu=basicsa'
        }
    }));
    run();
}