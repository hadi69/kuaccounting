﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
92-10-19: file created
*/
function view_grouping() {
    makeChosen();

    $("#tablePerson").smartTable();
    $("#tableVisitor").smartTable();
    $("#tableService").smartTable();
    $("#tableSupplier").smartTable();
    $("#tableAdvertisement").smartTable();

    var cnt = 0, cnt2 = 0, cnt3 = 0, cnt4 = 0, cntPrice = 0, cnt5 = 0;
    var mID = -1;
    // ---------------------
    // Add Rows
    // ---------------------
    function addPerson(node) {
        var row = '<tr>';

        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">' + (cnt + 1) + '</td>';

        row += '<td data-th="' + Lang.Name + '">'
            + '<input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="hidden" name="GroupingID" value="' + getNodeVal(node, "GroupingID") + '" />'
            + '<input type="text" name="OtherName" id="pn_' + cnt + '" value="' + getNodeVal(node, "OtherName") + '" class=" personname form-control input-sm " />'
            + '<input type="hidden" name="OtherID" id="oid_' + cnt + '"  value="' + getNodeVal(node, "OtherID") + '" class="otherid" />'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a class="btn btn-danger btn-xs btn-block btn-remove removeperson" id="rp_' + cnt + '">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }
    function addPrice(node) {
        var row = '<tr>';

        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">' + (cntPrice + 1) + '</td>';

        row += '<td data-th="' + Lang.CommodityName + '">'
            + '<input type="hidden" name="ID2" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="hidden" name="GroupingID" value="' + getNodeVal(node, "GroupingID") + '" />'
            + '<input type="text" name="ServiceFullName" id="sf_' + cntPrice + '" value="' + getNodeVal(node, "ServiceFullName") + '" class=" servicefullname form-control input-sm " />'
            + '<input type="hidden" name="ServicePriceID" id="spd_' + cntPrice + '"  value="' + getNodeVal(node, "ServicePriceID") + '" class="servicepriceid" />'
            + '</td>';

        row += '<td data-th="' + Lang.SalePrice + '">'
            + '<input type="text" name="SalePrice" id="sp_' + cntPrice + '" value="' + getNodeVal(node, "SalePrice") + '" class="monetary validate[custom[integer]] form-control input-sm align-center" />'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a class="btn btn-danger btn-xs btn-block btn-remove removeprice" id="rpr_' + cntPrice + '">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cntPrice++;
        return row;
    }
    

    function addVisitor(node) {
        var row = '<tr>';
        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">' + (cnt4 + 1) + '</td>';

        row += '<td data-th="' + Lang.Name + '">'
            + '<input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="hidden" name="GroupingID" value="' + getNodeVal(node, "GroupingID") + '" />'
            + '<input type="text" name="OtherName" id="vn_' + cnt4 + '" value="' + getNodeVal(node, "OtherName") + '" class=" visitorname form-control input-sm " />'
            + '<input type="hidden" name="OtherID" id="oid_' + cnt4 + '"  value="' + getNodeVal(node, "OtherID") + '" class="otherid" />'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a class="btn btn-danger btn-xs btn-block btn-remove removevisitor" id="rv_' + cnt4 + '">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt4++;
        return row;
    }

    function addService(node) {
        var row = '<tr>';
        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">' + (cnt2 + 1) + '</td>';

        row += '<td data-th="' + Lang.Name + '">'
            + '<input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="text" name="OtherName" id="sn_' + cnt2 + '" value="' + getNodeVal(node, "OtherName") + '" class=" servicename form-control input-sm" />'
            + '<input type="hidden" name="OtherID" id="oid_' + cnt2 + '" value="' + getNodeVal(node, "OtherID") + '" class="otherid" />'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a class="btn btn-danger btn-xs btn-block btn-remove removeservice" id="rs_' + cnt2 + '">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt2++;
        return row;
    }

    function addAdvertisement(node) {
        var row = '<tr>';
        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">' + (cnt5 + 1) + '</td>';

        row += '<td data-th="' + Lang.Name + '">'
            + '<input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="hidden" name="GroupingID" value="' + getNodeVal(node, "GroupingID") + '" />'
            + '<input type="text" name="OtherName" id="an_' + cnt5 + '" value="' + getNodeVal(node, "OtherName") + '" class=" advertisementname form-control input-sm" />'
            + '<input type="hidden" name="OtherID" id="oid_' + cnt5 + '" value="' + getNodeVal(node, "OtherID") + '" class="otherid" />'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a class="btn btn-danger btn-xs btn-block btn-remove removeadvertisement" id="ra_' + cnt5 + '">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt5++;
        return row;
    }

    function addSupplier(node) {
        var row = '<tr>';
        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">' + (cnt3 + 1) + '</td>';

        row += '<td data-th="' + Lang.Name + '">'
            + '<input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="hidden" name="GroupingID" value="' + getNodeVal(node, "GroupingID") + '" />'
            + '<input type="text" name="OtherName" id="sun_' + cnt3 + '" value="' + getNodeVal(node, "OtherName") + '" class=" suppliername form-control input-sm " />'
            + '<input type="hidden" name="OtherID" id="oid_' + cnt3 + '"  value="' + getNodeVal(node, "OtherID") + '" class="otherid"/>'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a class="btn btn-danger btn-xs btn-block btn-remove removesupplier" id="rsu_' + cnt3 + '">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt3++;
        return row;
    }
    //-------------------
    // remove rows
    //----------------
    function removePerson($who) {
        var id = $who.attr('id'); // pn_1
        id = id.substring(3); // pn_1  -->  1
        $('#pn_' + id).val("");
        $who.parent().parent().hide();
        Growl.Notice(Lang.DeletedSuccessfully);
    }
    function removePrice($who) {
        var id = $who.attr('id'); // rpr_1
        id = id.substring(4); // rpr_1  -->  1
        $('#sF_' + id).val("");
        $('#spd_' + id).val("");
        $who.parent().parent().hide();
        Growl.Notice(Lang.DeletedSuccessfully);
    }
    function removeVisitor($who) {
        var id = $who.attr('id'); // pn_1
        id = id.substring(3); // pn_1  -->  1
        $('#vn_' + id).val("");
        $('#vn_' + id).next().val("");
        $who.parent().parent().hide();
        Growl.Notice(Lang.DeletedSuccessfully);
    }
    function removeService($who) {
        var id = $who.attr('id');
        id = id.substring(3);
        $('#sn_' + id).val("");
        $('#sn_' + id).next().val("");
        $who.parent().parent().hide();
        Growl.Notice(Lang.DeletedSuccessfully);
    }
    function removeAdvertisement($who) {
        var id = $who.attr('id');
        id = id.substring(3);
        $('#an_' + id).val("");
        $('#an_' + id).next().val("");
        $who.parent().parent().hide();
        Growl.Notice(Lang.DeletedSuccessfully);
    }
    function removeSupplier($who) {
        var id = $who.attr('id');
        id = id.substring(4);
        $('#sun_' + id).val("");
        $('#sun_' + id).next().val("");
        $who.parent().parent().hide();
        Growl.Notice(Lang.DeletedSuccessfully);
    }

    //-------------------

    function bindAdds() {
        $('#addPerson').click(function () {
            var table = $('#tablePerson');
            $('tr:last', table).after(addPerson(null));
            var lastRow6 = $('tr:last', table);
            disableKeydownOnLastInputByName('OtherName');
            Growl.Notice(Lang.RowAddedSuccessfully);

            $('.removeperson', lastRow6).click(function () {
                removePerson($(this));
            });
            autoComplete('Personnel', 'personnelaccauto', "#tablePerson tr:last .personname", "#justfurfun"
                , function(event, data) {
                    $('tr .otherid', table).each(function () {
                        if ($(this).attr('id') != $('tr:last .otherid', table).attr('id')) {
                            if ($(this).val() == data[0]) {
                                alert(Lang.RepeatedMember);
                            }
                        }
                    });
                    $(event.target).next().val(data[0]);
                }
                , function (data, i, n, value) {
                    if (getSelectedRow("#tablePerson .personname").indexOf(data[0]) >= 0) {
                        return '<span class="red1">' + data[1] + '</span>';
                    }
                    else {
                        return data[1];
                    }
                });
            setTimeout(function () {
                $("#tablePerson tr:last .personname").focus();
            }, 2);
        });
        $('#addPrice').click(function () {
            var table = $('#tablePrice');
            $('tr:last', table).after(addPrice(null));
            var lastRow = $('tr:last', table);
            disableKeydownOnLastInputByName('SalePrice');
            Growl.Notice(Lang.RowAddedSuccessfully);

            $('.removeprice', lastRow).click(function () {
                removePrice($(this));
            });
            autoCompleteFree('Service', 'servicepriceauto', "#tablePrice .servicefullname", "#justfurfun"
               , function (event, data, formatted) {
                   $(event.target).next().val(data[0]); // ID
                   var id = $(event.target).attr('id');
                   id = id.substring(id.indexOf('_') + 1);
                   //$('#u_' + id).next().val(data[5]); // unitid
                   //$('#u_' + id).val(data[6]); // unitname
               }
               , function (data, i, n, value) {
                   return data[1];
               });
            commaInputs("#subDetailForm");
            setTimeout(function () {
                $("#tablePrice tr:last .servicefullname").focus();
            }, 2);
        });

        $('#addVisitor').click(function () {
            var table = $('#tableVisitor');
            $('tr:last', table).after(addVisitor(null));
            var lastRow6 = $('tr:last', table);
            disableKeydownOnLastInputByName('OtherName');
            Growl.Notice(Lang.RowAddedSuccessfully);

            $('.removevisitor', lastRow6).click(function () {
                removeVisitor($(this));
            });
            autoComplete('Personnel', 'personnelaccauto', "#tableVisitor tr:last .visitorname", "#justfurfun"
                , function (event, data) {
                    $('tr .otherid', table).each(function () {
                        if ($(this).attr('id') != $('tr:last .otherid', table).attr('id')) {
                            if ($(this).val() == data[0]) {
                                alert(Lang.RepeatedMember);
                            }
                        }
                    });
                    $(event.target).next().val(data[0]);
                }
                , function (data, i, n, value) {
                    if (getSelectedRow("#tableVisitor .visitorname").indexOf(data[0]) >= 0) {
                        return '<span class="red1">' + data[1] + '</span>';
                    }
                    else {
                        return data[1];
                    }
                });
            setTimeout(function () {
                $("#tableVisitor tr:last .visitorname").focus();
            }, 2);
        });

        $('#addService').click(function () {
            var table = $('#tableService');
            $('tr:last', table).after(addService(null));
            var lastRow6 = $('tr:last', table);
            disableKeydownOnLastInputByName('OtherName');
            Growl.Notice(Lang.RowAddedSuccessfully);

            $('.removeservice', lastRow6).click(function () {
                removeService($(this));
            });
            autoComplete('ServicePrice', 'groupingservicepriceauto', "#tableService tr:last .servicename", "#justfurfun"
                , function (event, data) {
                    $('tr .otherid', table).each(function () {
                        if ($(this).attr('id') != $('tr:last .otherid', table).attr('id')) {
                            if ($(this).val() == data[0]) {
                                alert(Lang.RepeatedMember);
                            }
                        }
                    });
                    $(event.target).next().val(data[0]);
                }
                , function (data, i, n, value) {
                    if (getSelectedRow("#tableService .servicename").indexOf(data[0]) >= 0) {
                        return '<span class="red1">' + data[1] + '</span>';
                    }
                    else {
                        return data[1];
                    }
                });
            setTimeout(function () {
                $("#tableService tr:last .servicename").focus();
            }, 2);
        });

        $('#addAdvertisement').click(function () {
            var table = $('#tableAdvertisement');
            $('tr:last', table).after(addAdvertisement(null));
            var lastRow6 = $('tr:last', table);
            disableKeydownOnLastInputByName('OtherName');
            Growl.Notice(Lang.RowAddedSuccessfully);

            $('.removeadvertisement', lastRow6).click(function () {
                removeAdvertisement($(this));
            });
            autoComplete('Advertisement', 'enumauto1', "#tableAdvertisement tr:last .advertisementname", "#justfurfun"
                , function (event, data) {
                    $('tr .otherid', table).each(function () {
                        if ($(this).attr('id') != $('tr:last .otherid', table).attr('id')) {
                            if ($(this).val() == data[0]) {
                                alert(Lang.RepeatedMember);
                            }
                        }
                    });
                    $(event.target).next().val(data[0]);
                }
                , function (data, i, n, value) {                    
                    if (getSelectedRow("#tableAdvertisement .advertisementname").indexOf(data[0]) >= 0) {
                        return '<span class="red1">' + data[1] + '</span>';
                    }
                    else {
                        return data[1];
                    }
                });
            setTimeout(function () {
                $("#tableAdvertisement tr:last .advertisementname").focus();
            }, 2);
        });

        $('#addSupplier').click(function () {
            var table = $('#tableSupplier');
            $('tr:last', table).after(addSupplier(null));
            var lastRow6 = $('tr:last', table);
            disableKeydownOnLastInputByName('OtherName');
            Growl.Notice(Lang.RowAddedSuccessfully);

            $('.removesupplier', lastRow6).click(function () {
                removeSupplier($(this));
            });
            autoCompleteFree('Supplier', 'supplierauto', "#tableSupplier tr:last .suppliername", "#justfurfun"
                , function (event, data) {
                    $('tr .otherid', table).each(function () {
                        if ($(this).attr('id') != $('tr:last .otherid', table).attr('id')) {
                            if ($(this).val() == data[0]) {
                                alert(Lang.RepeatedMember);
                            }
                        }
                    });
                    $(event.target).next().val(data[0]);
                }
                , function (data, i, n, value) {
                    if (getSelectedRow("#tableSupplier .suppliername").indexOf(data[0]) >= 0) {
                        return '<span class="red1">' + data[1] + '</span>';
                    }
                    else {
                        return data[1];
                    }
                });
            setTimeout(function () {
                $("#tableSupplier tr:last .suppliername").focus();
            }, 2);
        });
    }

    //------------------

    function getSelectedRow(elements) {
        var selectedItems = "";
        $(elements).each(function () {
            if (toInt($(this).next().val(), 0) > 0)
                selectedItems += $(this).next().val() + ',';
        });
        selectedItems = selectedItems.substring(0, selectedItems.length - 1);
        return selectedItems;
    }
    function bindRemoves() {
        $('.removeperson').click(function () {
            removePerson($(this));
        });

        $('.removevisitor').click(function () {
            removeVisitor($(this));
        });

        $('.removeservice').click(function () {
            removeService($(this));
        });

        $('.removeadvertisement').click(function () {
            removeAdvertisement($(this));
        });

        $('.removesupplier').click(function () {
            removeSupplier($(this));
        });
    }
   function groupingType()
    {
       if ($('#GroupType').val() == 0) {
           $('#tVisitor, #tabVisitor').hide();
           $('#tService, #tabService').hide();
           $('#tSupplier, #tabSupplier').hide();
           $('#tPerson, #tabPerson').show();
           $('#tAdvertisement, #tabAdvertisement').hide();
           $('.buyandsaletopic').hide(); 
           $('.groupingsubtype').hide();
       }
       else if ($('#GroupType').val() == 1) {
           $('#tVisitor, #tabVisitor').hide();
           $('#tPerson, #tabPerson').hide();
           $('#tSupplier, #tabSupplier').hide();
           $('#tService, #tabService').show();
           $('#tAdvertisement, #tabAdvertisement').hide();
           $('.buyandsaletopic').show();
           $('.groupingsubtype').hide();
       }
       else if ($('#GroupType').val() == 2) {
           $('#tService, #tabService').hide();
           $('#tPerson, #tabPerson').hide();
           $('#tSupplier, #tabSupplier').hide();
           $('#tVisitor, #tabVisitor').show();
           $('#tAdvertisement, #tabAdvertisement').hide();
           $('.buyandsaletopic').hide();
           $('.groupingsubtype').hide();
       }
       else if ($('#GroupType').val() == 3) {
           $('#tService, #tabService').hide();
           $('#tPerson, #tabPerson').hide();
           $('#tVisitor, #tabVisitor').hide();
           $('#tAdvertisement, #tabAdvertisement').hide();
           $('#tSupplier, #tabSupplier').show();
           $('.buyandsaletopic').hide();
           $('.groupingsubtype').hide();
       }
       else if ($('#GroupType').val() == 4) {
           $('#tService, #tabService').hide();
           $('#tPerson, #tabPerson').hide();
           $('#tVisitor, #tabVisitor').hide();
           $('#tSupplier, #tabSupplier').hide();
           $('#tAdvertisement, #tabAdvertisement').show();
           $('.buyandsaletopic').hide();
           $('.groupingsubtype').show();
       }
       $('#GroupType').change(function () {
           if ($(this).val() == 0)
            {
               $('#tVisitor, #tabVisitor').hide();
               $('#tService, #tabService').hide();
               $('#tSupplier, #tabSupplier').hide();
               $('#tPerson, #tabPerson').show();
               $('#tAdvertisement, #tabAdvertisement').hide();
               $('.buyandsaletopic').hide();
               $('.groupingsubtype').hide();
            }
            else if ($(this).val() == 1) {
                $('#tVisitor, #tabVisitor').hide();
                $('#tPerson, #tabPerson').hide();
                $('#tSupplier, #tabSupplier').hide();
                $('#tAdvertisement, #tabAdvertisement').hide();
                $('#tService, #tabService').show();
                $('.buyandsaletopic').show();
                $('.groupingsubtype').hide();
            }
            else if ($(this).val() == 2) {
                $('#tService, #tabService').hide();
                $('#tPerson, #tabPerson').hide();
                $('#tSupplier, #tabSupplier').hide();
                $('#tAdvertisement, #tabAdvertisement').hide();
                $('#tVisitor, #tabVisitor').show();
                $('.buyandsaletopic').hide();
                $('.groupingsubtype').hide();
                   }
            else if ($(this).val() == 3) {
                $('#tService, #tabService').hide();
                $('#tPerson, #tabPerson').hide();
                $('#tVisitor, #tabVisitor').hide();
                $('#tAdvertisement, #tabAdvertisement').hide();
                $('#tSupplier, #tabSupplier').show();
                $('.buyandsaletopic').hide();
                $('.groupingsubtype').hide();
            }
            else if ($(this).val() == 4) {
                $('#tService, #tabService').hide();
                $('#tPerson, #tabPerson').hide();
                $('#tVisitor, #tabVisitor').hide();
                $('#tAdvertisement, #tabAdvertisement').show();
                $('#tSupplier, #tabSupplier').hide();
                $('.buyandsaletopic').hide();
                $('.groupingsubtype').show();
            }
            });
   }

   function checkBuyAndSaleTopics() {
       if ($('#BuyAndSaleTopics').attr('checked')) {
           $('.BAST').show();
       }
       else {
           $('.BAST').hide();
       }
       $('#BuyAndSaleTopics').click(function () {
           if ($(this).attr('checked')) {
               $('.BAST').show();
           }
           else {
               $('.BAST').hide();
           }
       });
   }
   function codePlaceHolder() {
       var newCode = getServerValue('getgroupingnewcode', 'Grouping');
       $('#Code').attr('placeholder', newCode);
   }
    // -----------------------------
    // startup code
    // ------------------------------

    mID = toInt($('#MasterID').val(), -1);
    setValue('#GroupType');
    setValue('#GroupingSubType');
    groupingType();

    setChecked("#BuyAndSaleTopics");
    checkBuyAndSaleTopics();

    var code = $('#Code').val();
    if (code == '' || code == null || code == undefined) {
        codePlaceHolder();
    }



    autoCompleteTreePath('#SaleTreePathName', '#SaleTreePath', '#SaleSel');
    autoCompleteTreePath('#OweSaleTreePathName', '#OweSaleTreePath', '#OweSaleSel');
    autoCompleteTreePath('#SaleDiscountTreePathName', '#SaleDiscountTreePath', '#SaleDiscountSel');
    autoCompleteTreePath('#SaleAwarezTreePathName', '#SaleAwarezTreePath', '#SaleAwarezSel');
    autoCompleteTreePath('#SaleTaxTreePathName', '#SaleTaxTreePath', '#SaleTaxSel');
    autoCompleteTreePath('#SalePursantTreePathName', '#SalePursantTreePath', '#SalePursantSel');
    autoCompleteTreePath('#SaleShipmentTreePathOweName', '#SaleShipmentTreePathOwe', '#SaleShipmentOweSel');
    autoCompleteTreePath('#SaleShipmentTreePathDueName', '#SaleShipmentTreePathDue', '#SaleShipmentDueSel');
    autoCompleteTreePath('#BuyTreePathName', '#BuyTreePath', '#BuySel');
    autoCompleteTreePath('#BuyDiscountTreePathName', '#BuyDiscountTreePath', '#BuyDiscountSel');
    autoCompleteTreePath('#BuyAwarezTreePathName', '#BuyAwarezTreePath', '#BuyAwarezSel');
    autoCompleteTreePath('#BuyTaxTreePathName', '#BuyTaxTreePath', '#BuyTaxSel');
    autoCompleteTreePath('#BuyShipmentTreePathOweName', '#BuyShipmentTreePathOwe', '#BuyShipmentSel');
    autoCompleteTreePath('#ReturnTreePathName', '#ReturnTreePath', '#ReturnSel');
    autoCompleteTreePath('#ReturnDiscountTreePathName', '#ReturnDiscountTreePath', '#ReturnDiscountSel');
    autoCompleteTreePath('#RefuseTreePathName', '#RefuseTreePath', '#RefuseSel');
    autoCompleteTreePath('#RefuseDiscountTreePathName', '#RefuseDiscountTreePath', '#RefuseDiscountSel');

    if (_Context.isStoreUser) {
        $('#Parent').addClass('validate[required]');
        autoComplete('Grouping', 'storeusergroupauto', '#Parent', '#ParentID', function (e, data) {
            $('#ParentID').val(data[0]);
            $('#GroupType').attr('v', data[2]);
            setValue('#GroupType');
        });
    } else {
        autoComplete('Grouping', 'groupauto', '#Parent', '#ParentID',
            function (e, data) {
                $('#ParentID').val(data[0]);
                $('#GroupType').attr('v', data[2]);
                setValue('#GroupType');
            });
    }
    $('#selGroup').click(function () {
        var _url = 'get.aspx?op=GetHtml&table=Grouping&view=groupselect' + appendTime();
        var btns = {};
        btns[Lang.Cancel] = "_CANCEL_";
        showPopup(_url, function (v, m, f) {
            $('#ParentID').val(f.Id);
            $('#Parent').val(f.STreePathName);
        }, btns);
    });

    $('#Parent').blur(function() {
        var val = toInt($('#ParentID').val(), 0);
        if (val > 0) {
            $('#Code').attr('readonly', 'readonly');
            $('#Code').val('');
            $('#Code').attr('placeholder', '');
        } else {
            $('#Code').attr('readonly', '');
            codePlaceHolder();
        }
    });

    fillTable("get.aspx?op=GetXml&qd=groupingmember&GroupingID=" + mID + "&GroupingType=0", '#tablePerson', addPerson);
    fillTable("get.aspx?op=GetXml&qd=groupingmember&GroupingID=" + mID + "&GroupingType=2", '#tableVisitor', addVisitor);
    fillTable("get.aspx?op=GetXml&qd=groupingmember&GroupingID=" + mID + "&GroupingType=1", '#tableService', addService);
    fillTable("get.aspx?op=GetXml&qd=groupingmember&GroupingID=" + mID + "&GroupingType=3", '#tableSupplier', addSupplier);
    fillTable("get.aspx?op=GetXml&qd=groupingmember&GroupingID=" + mID + "&GroupingType=4", '#tableAdvertisement', addAdvertisement);
    fillTable("get.aspx?op=GetXml&qd=groupingprice&GroupingID=" + mID, '#tablePrice', addPrice);
    tabify();

  function doMore() {
        function autoPostPack(event, data, formatted) {
            $(event.target).next().val(data[0]);
        }

        autoComplete('Personnel', 'personnelaccauto', "#tablePerson .personname", "#justfurfun"
           , autoPostPack
           , function (data, i, n, value) {
               if (getSelectedRow("#tablePerson .personname").indexOf(data[0]) >= 0) {
                   return '<span class="red1">' + data[1] + '</span>';
               }
               else {
                   return data[1];
               }
           });

        autoComplete('Personnel', 'personnelaccauto', "#tableVisitor .visitorname", "#justfurfun"
            , autoPostPack
            , function (data, i, n, value) {
                if (getSelectedRow("#tableVisitor .visitorname").indexOf(data[0]) >= 0) {
                    return '<span class="red1">' + data[1] + '</span>';
                }
                else {
                    return data[1];
                }
            });

        autoComplete('Service', 'enumauto1', "#tableService .servicename", "#justfurfun"
                 , autoPostPack
                 , function (data, i, n, value) {
                     if (getSelectedRow("#tableService .servicename").indexOf(data[0]) >= 0) {
                         return '<span class="red1">' + data[1] + '</span>';
                     }
                     else {
                         return data[1];
                     }
                 });

        autoComplete('Advertisement', 'enumauto1', "#tableAdvertisement .advertisementname", "#justfurfun"
           , autoPostPack
           , function (data, i, n, value) {
               if (getSelectedRow("#tableAdvertisement .advertisementname").indexOf(data[0]) >= 0) {
                   return '<span class="red1">' + data[1] + '</span>';
               }
               else {
                   return data[1];
               }
           });

        autoCompleteFree('Supplier', 'supplierauto', "#tableSupplier .suppliername", "#justfurfun"
                , autoPostPack
                , function (data, i, n, value) {
                    if (getSelectedRow("#tableSupplier .suppliername").indexOf(data[0]) >= 0) {
                        return '<span class="red1">' + data[1] + '</span>';
                    }
                    else {
                        return data[1];
                    }
                });

        bindAdds();
        bindRemoves();
        insertValidation('#subDetailForm');
        setValue('#GroupType');
        setValue('#GroupingSubType');
        groupingType();
  };
   doMore();
   monetaryInputsSet('#subDetailForm', null);

   insertValidation('#subDetailForm');
   commaInputs("#subDetailForm");

   uploadify('#ChooseFile', '#Icon', '#StartUpload', 'grouping'
                   , function (fileName) {
                       var link = fileName.toLowerCase();
                       if (link.indexOf('.png') > 0 || link.indexOf('.gif') > 0 || link.indexOf('.jpg') > 0 || link.indexOf('.jpeg') > 0)
                           $('#preview').attr('src', "files/grouping/" + link);
                   }, '', null, mID > 0 ? mID : '');
   var link = $('#IconLink').text().toLowerCase();
   if (link.indexOf('.png') > 0 || link.indexOf('.gif') > 0 || link.indexOf('.jpg') > 0 || link.indexOf('.jpeg') > 0)
       $('#preview').attr('src', $('#IconLink').attr('href'));
    
}