﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
93-05-18: file created
*/
function view_const() {
    makeChosen();
    $('#tableItems').smartTable();
    // ------------------------------
    // startup code
    // ------------------------------
    var cnt = 0, mID = -1;
    function addConstValues(node) {
        var row = '<tr>';
        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">'
            + (cnt + 1)
            + '</td>';

        row += '<td data-th="' + Lang.ConstDefinitionItem + '">'
            + '<input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="text" id="se_' + cnt + '" name="ConstDefinitionItemName" value="' + getNodeVal(node, "ConstDefinitionItemName") + '" '
            + 'class="constdefinitionitemname form-control input-sm" disabled="disabled"/>'
            + '<input type="hidden" name="ConstDefinitionItemID" value="' + getNodeVal(node, "ConstDefinitionItemID") + '" />'
            + '</td>';

        row += '<td data-th="' + Lang.Value + '">'
            + '<input type="text" id="v_' + cnt + '" name="Value" value="' + getNodeVal(node, "Value") + '" class="value validate[custom[integer]] form-control input-sm monetary"/>'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a class="btn btn-danger btn-xs btn-block btn-remove removeRole" id="r_' + cnt + '">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }

    function RemoveRoleRow($who) {
        var id = $who.attr('id'); // r_1
        id = id.substring(2); // r_1  -->  1
        //  alert(id);
        $('#se_' + id).val(""); // 
        $('#se_' + id).next().val(-1); // 
        $who.parent().parent().remove();

    }

    function autoPostPack(event, data, formatted) {
        $(event.target).next().val(data[0]); // ID
    }
    function bindAdds() {
        $('.addConstValue').click(function () {
            var table = $('#tableItems');
            var tableID = table.attr("id");
            $('tr:last', table).after(addConstValues(null));
            var lastRow = $('tr:last', table);
            autoComplete('ConstDefinitionItems', 'constdefinitionitemauto', "#tableItems tr:last .constdefinitionitemname", "#justfurfun"
           , autoPostPack);
        });

    }

    function bindRemoves() {
        //-----------
        $('.removeRole').click(function () {
            RemoveRoleRow($(this));
        });
    }
    setValue("#ConstType");
    dateSelect('.dateSelect');
    //if ($("#CreationDate").val() == '')
    //    $("#CreationDate").val('_TODAY_');
    mID = toInt($("#MasterID").val(), -1);
    fillTable("get.aspx?op=GetXml&qd=constvalues&ConstID=" + mID, '#tableItems', addConstValues);
    doMore();

    function doMore() {
        function autoPostPack(event, data, formatted) {
            $(event.target).next().val(data[0]); // ID
        }
        autoComplete('ConstDefinitionItems', 'constdefinitionitemauto', "#tableItems .constdefinitionitemname", "#justfurfun"
       , autoPostPack);

        $('.addConstValue').click(function () {
            var table = $('#tableItems');
            $('tr:last', table).after(addConstValues(null));
            var lastRow = $('tr:last', table);
            autoComplete('ConstDefinitionItems', 'constdefinitionitemauto', "#tableItems tr:last .constdefinitionitemname", "#justfurfun"
       , autoPostPack);
            $('.removeRole', lastRow).click(function () {
                RemoveRoleRow($(this));
            });
        });

        $('.removeRole').click(function () {
            RemoveRoleRow($(this));
        });
    }
    //----------end addrow
    setChecked("#Enabled");
    commaInputs();
    insertValidation();
}