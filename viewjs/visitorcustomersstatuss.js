﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
    92-10-16: file created
*/
function view_visitorcustomersstatuss() {
    setClassDistribution();
    function doCommand(com, grid) {
        if (com == Lang.ToOrder) {
            var id = getRowID(grid);
            if (id != -1) {
                var customerID = getColContent($('.trSelected', grid), 2);
                var customerName = getColContent($('.trSelected', grid), 5);
                runQuery('visitorcustomersstatusupdate','VisitorCustomersStatus', '&Status=1&Comments=&ID=' + id);
                var _url = 'list.aspx?table=Grouping&view=browse' + '&BrowseBuyerID=' + customerID + '&BrowseBuyerName=' + customerName + appendTime();
                document.location = _url;
            }
            return;
        }
        if (com == Lang.FailerToVisit) {
            var id = getRowID(grid);
            if (id != -1) {
                var btns = getDefaultButtons();
                var op = "GetHtml";
                var _url = 'get.aspx?op=' + op + '&table=VisitorCustomersStatus&qd=visitorcustomersstatusupdate&view=visitorcustomersstatus&VisitorCustomersStatusID=' + id + appendTime();
                showPopup(_url, writComments, btns, '1%', null, true);
            }
            return;
        }
        generalCommand(com, grid, show);
    }
    function run() {
        var _url = 'service.asmx/GetQueryList?table=VisitorCustomersStatus&qd=visitorcustomersstatus&VisitorID=' + _Context.personnelID +appendTime();
        $("#flexi").flexigrid
                (
                    {
                    url: _url,
                    dataType: 'xml',
                    colModel:
                    [
                        { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                        { display: 'CustomersID', name: 'CustomersID', width: 10, sortable: false, align: 'left', hide: true },
                        { display: 'Status', name: 'Status', width: 10, sortable: false, align: 'left', hide: true, process: rowColor },
                        { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                        { display: Lang.CustomerName, name: 'CustomerName', sortable: true, css_class: 'width-150' },
                        { display: Lang.FirstGroup, name: 'FGroupName', sortable: true, css_class: 'width-200' },
                        { display: Lang.SecondGroup, name: 'SGroupName', sortable: true, css_class: 'width-200' },
                        { display: Lang.Address, name: 'Address', sortable: true}
                    ],
                    buttons:
                    [
                        { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                        { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand },
                        { name: Lang.ToOrder, bclass: 'ToOrder', onpress: doCommand },
                        { name: Lang.FailerToVisit, bclass: 'FailerToVisit', onpress: doCommand }
                    ],
                    searchitems: null,
                    sortname: "CustomerName",
                    sortorder: "asc",
                    usepager: true,
                    //title: _Context.title,
                    useRp: true,
                    rp: gridRp,
                    showTableToggleBtn: _options.gridToggle,
                    width: _options.gridWidth,

                    height: _options.gridHeight, resizable: _options.gridResizable,
                    singleSelect: false,
                    onDoubleClick: function (row, grid, options) {
                        var id = row.id.substring(3);
                        show(id);
                    },
                    onSuccess: function (grid) {
                        colorGrid();
                        restoreSelectedRows('#flexi');
                    }
                }
                );
    }

    function writComments(v, f, m) {
        if (v == 'OK') {
            var more = '&ID=' + $('#ID').val() + '&Status=' + $('#Status').val() + '&Comments='+ $('#Comments').val();
            runQuery('visitorcustomersstatusupdate', 'VisitorCustomersStatus', more);
            var title = $('.popupTitle').html();
            $('.popupTitle').html(Lang.Saved);
            $('.popupTitle').animate({ 'background-color': '#f00' }, 1000, 'linear', function () { $('.popupTitle').animate({ 'background-color': '#00496F' }, 1000, 'linear'); $('.popupTitle').html(title); });
            $("#flexi").flexReload();
            return true;
        }
        $("#flexi").flexReload();
        return true;
        
    }
    function rowColor(celDiv, id) {
        $(celDiv).addClass('status');
    };

    function colorGrid() {
        setTimeout(function () {
            $('#flexi tr').each(function () {
                var status = $('.status', this).html();
                if (status == 2) {
                    $('td', this).addClass('invalid');
                }
                if (status == 1) {
                    $('td', this).addClass('visited');
                }
            });
        }, 1000);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    if (_Context.isAdmin) {
        $('#choosevisitor').removeClass('hidden');
    }
    checkAccess(['distribution,dailyplan']);
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.DailyPlan, parentTitle: Lang.MDistribution, parentUrl: 'menu/distribution.aspx' } }));
    run();
    autoComplete('Personnel', 'visitorauto', "#VisitorName", "#VisitorID", function (e, data) {
        refresh(data[0]);
    });

    function refresh(visitorId) {
        var _url = 'service.asmx/GetQueryList?table=VisitorCustomersStatus&qd=visitorcustomersstatus&VisitorID=' + visitorId + appendTime();
        $("#flexi").flexOptions({ url: _url });
        $("#flexi").flexReload();
    }
}