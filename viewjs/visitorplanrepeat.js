﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
92-10-16: 
- file created
- Date: dateSelect
*/
function view_visitorplanrepeat() {

    // startup code
    //--------------------------
    setChecked("#HolidayCopy");
    setValue('#Weekdays');
    setValue('#RepeatType');
    dateSelect('.dateSelect');
    insertValidation();
}