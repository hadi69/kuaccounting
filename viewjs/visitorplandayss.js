﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

function view_visitorplandayss() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }

    function run(parameters) {
        var more = decodeURIComponent("VisitorPlanID=" + visitorplanid);

        var _url = 'service.asmx/GetList?table=VisitorPlanDays&qd=visitorplandays&more=' + more + appendTime();
        $("#flexi").flexigrid
            (
        {
            url: _url,
            dataType: 'xml',
            colModel:
            [
                { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                { display: Lang.VisitDate, name: 'VisitDate', sortable: true, process: _displayDate, css_class: 'date-cell' },
                { display: Lang.Weekday, name: 'Weekdays', sortable: true, process: _displayWeekdayEnum, css_class: 'width-100 align-center' },
                { display: Lang.NumOfCustomer, name: 'NumOfCustomer', sortable: true, css_class: 'width-100 align-center' },
                { display: Lang.Comments, name: 'Comments', sortable: true },
                { display: Lang.VisitorTimeOrder, name: 'VisitorTimeOrder', sortable: false, process: visitorTimeOrder, css_class: 'width-120 align-center' },
                { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function(celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' },
                { display: Lang.Limits, name: 'CustomersLimit', sortable: false, process: customersLimitDefination, css_class: 'width-80 align-center' }
            ],
            buttons:
            [
                { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
            ],
            sortname: "VisitDate",
            sortorder: "asc",
            usepager: true,
            useRp: true,
            rp: gridRp,
            showTableToggleBtn: gridToggle,
            width: gridWidth,
            height: gridHeight,
            resizable: gridResizable,
            singleSelect: false
        });
    }
    function showcallback(v, m, f) {
        _Context.table = 'VisitorPlanDays';
        var qry = (_Context.id == -1) ? 'visitorplandaysinsert' : 'visitorplandaysupdate';
        return newMasterscallback(v, m, f, 'VisitorPlanDays', qry
            , 'DailyCustomers,VisitorPlanDayServcie'
            , 'dailycustomersupdate,visitorplandayserviceupdate'
            , 'dailycustomersinsert,visitorplandayserviceinsert'
            , 'dailycustomersdelete,visitorplandayservicedelete'
            , 'FGroupingID,StoreServiceID', null, '#subDetailForm', show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + '&VisitorPlanID=' + visitorplanid + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }
    function customersLimitDefination(celDiv, id) {
        $(celDiv).html('<a href="list.aspx?table=CustomersLimit&id=' + id + '"><img src="img/task_icon.jpg" /></a>');
    }

    function visitorTimeOrder(celDiv, id) {
        $(celDiv).html('<img src="img/task_icon.jpg" />');
        $(celDiv).click(function (event) {
            event.preventDefault();
            if (clicksDisabled)
                return;
            disableClicks();
            showVisitorTimeOrderPopup(id, this);
        });
    }

    function showVisitorTimeOrderPopup(id, celDiv, successCallback) {
        var title = Lang.VisitorTimeOrder + ' ' + visitorname
         + ' ' + $('._weekday', $(celDiv).parent().parent()).html()
         + ' ' + $('._date', $(celDiv).parent().parent()).html();

        _Context.id = id;
        $('.trSelected').removeClass('trSelected');
        var _url = 'get.aspx?op=GetHtml&table=VisitorPlanDays&view=visitortimeorder&ID=' + id + '&Title=' + title + appendTime();
        var btns = getCloseButtons();
        showPopup(_url, function () { }, btns, null, successCallback);
    }

    function _displayWeekdayEnum(celDiv, id) {
        $(celDiv).addClass('_weekday');
        var _num = parseInt($(celDiv).html(), 10);
        displayWeekdayNameEnum(celDiv, _num);
    }

    function _displayDate(celDiv, id) {
        $(celDiv).addClass('_date');
    }
    // ------------------------------
    // startup code
    // ------------------------------
    var visitorplanid = $.urlParam('id');
    var visitorname = ' - ' + getServerValue('getpersonnelname', 'Personnel', '&VisitorPlanID=' + visitorplanid);
    $("#bar").prepend(breadcrumb({
        title: _Context.title + visitorname,
        grandParent: {
            title: Lang.MDistribution,
            url: 'menu/distribution.aspx'
        },
        parent: {
            title: Lang.VisitorPlan,
            url: 'list.aspx?table=VisitorPlan&menu=admin'
        }
    }));

    run();
}