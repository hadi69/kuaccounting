﻿function view_salaryfishs() {
    function doCommand(com, grid) {
        if (com == Lang.GBEdit)
            showCmds();
        else
            generalCommand(com, grid, show);
    }

    function run() {
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query+ appendTime();

        //alert(_url);
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.Personnel, name: 'Person', sortable: true, css_class: 'width-200' },
                    { display: Lang.FishNo, name: 'No', sortable: true, css_class: 'width-80 align-center' },
                    { display: Lang.WorkGroup, name: 'Month', sortable: true },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, hide: false && hideColumn("personnel", "personnel", "edit"), css_class: 'action-cell' },
                    { display: Lang.ColDel, name: 'Del', sortable: false, process: delLink, hide: hideColumn("personnel", "personnel", "delete"), css_class: 'action-cell' }
                ],
                buttons:
                [
                   { name: Lang.GBNew, bclass: 'new', onpress: doCommand, onaccess: hasAccess("personnel", "personnel", "new") },
                    { name: Lang.GBEdit, bclass: 'edit', onpress: doCommand, onaccess: hasAccess("personnel", "personnel", "edit") },
                    { name: Lang.GBDel, bclass: 'delete', onpress: doCommand, onaccess: hasAccess("personnel", "personnel", "delete") },
                    { separator: true },
                    { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                sortname: "No",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight, resizable: gridResizable,
                singleSelect: true,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                }
            });
    }
    function showcallback(v, m, f) {
        if (v == 'Calc') {
            var id = f.ID;
            if (id == -1)
                id = newShowcallbackInternal(v, m, f, 'salaryfishupdate', 'salaryfishinsert', _Context.table, null, show);
            var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=calcsalaryfish&view=salaryfish&id=' + id + appendTime();
            showPopup(_url, showcallback, null, null, null);
        }
        return newShowcallback(v, m, f, 'salaryfishupdate', 'salaryfishinsert', _Context.table, null, show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&view=fishinfo&id=' + id + appendTime();
        //alert(2);
        var btns = getDefaultButtons();
        btns[Lang.Calculate] = 'Calc';
        showPopup(_url, showcallback, btns, null, successCallback);
    }
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: _Context.table, parentTitle: Lang.MSalary, parentUrl: 'menu/salary.aspx' } }));
    run();
}