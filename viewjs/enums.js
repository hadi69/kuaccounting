﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
92-10-19: showPopup used
92-10-16: file created
*/
function view_enums() {
    function doCommand(com, grid) {
        if (_Context.table.toLowerCase() == 'program')
            if (com == Lang.GBNew || com == Lang.GBDel) {
                if (_Context.fyStart == '') {
                    alert(Lang.NoCurYear);
                    return;
                }
            }
        generalCommand(com, grid, show);
    }
    function run() {
        var terminology = getTerminology(_Context.table);
        if (terminology)
            $('#term').html("<a href='help/terminology.aspx' target=_blank title='" + Lang.HasTerminology + "'>" + terminology + "</a>");
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        if (isProgramDependent())
            _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + "&more=__PrOgrAmconD__" + appendTime();
        //alert(_url);
        $("#flexi").flexigrid
                (
                {
                    url: _url,
                    dataType: 'xml',
                    colModel:
                    [
                        { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                        { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell'},
                        { display: Lang.Title, name: 'Title', sortable: true, css_class: 'width-150'},
                        { display: Lang.Code, name: 'Code', sortable: true, css_class: 'color-cell'},
                        { display: Lang.Color, name: 'Color', sortable: true, process: showColor, css_class: 'color-cell', hide: true },
                        { display: Lang.Comments, name: 'Comments', sortable: true},
                        { display: Lang.Enabled, name: 'Enabled', sortable: true, process: showBool, css_class: 'width-40 align-center' },
                        { display: Lang.ColEdit, name: 'Edit', sortable: false, hide: hideColumn("manager", "enums", "edit"), process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' },
                        { display: Lang.ColDel, name: 'Del', sortable: false, hide: hideColumn("manager", "enums", "delete"), process: delLink, css_class:'action-cell' }
                    ],
                    buttons:
                    [
                        { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                        { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                        { separator: true },
                        { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                        { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                    ],
                    searchitems: null,
                    sortname: "Title",
                    sortorder: "asc",
                    usepager: true,
                    //title: _Context.title,
                    useRp: true,
                    rp: gridRp,
                    showTableToggleBtn: _options.gridToggle,
                    width: _options.gridWidth,

                    height: _options.gridHeight, resizable: _options.gridResizable,
                    singleSelect: true,
                    onDoubleClick: function (row, grid, options) {
                        var id = row.id.substring(3);
                        show(id);
                    },
                    onSuccess: function (grid) {
                        restoreSelectedRows('#flexi');
                    }
                }
                );
    }
    function isProgramDependent() {
        return (_Context.table.toLowerCase() == 'TopicGroup'.toLowerCase());
    }
    function showcallback(v, m, f) {
        if (isProgramDependent())
            return newShowcallback(v, m, f, 'penumupdate', 'penuminsert', _Context.table, null, show);
        if (_Context.table.toLowerCase() == 'program')
            return newShowcallback(v, m, f, 'programupdate', 'programinsert', _Context.table, null, show);
        return newShowcallback(v, m, f, 'enumupdate', 'enuminsert', _Context.table, null, show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        saveSelectedRows('#flexi');
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, function () {
            jscolor.init();
            if (successCallback)
                successCallback();
        });
    }
    function setQuickMenu() {
        var menu = $.urlParam('menu');
        if (menu == 'accountinga')
            setClassAccounting();
        if (menu == 'basicsa')
            setClassBasic();
        if (menu == 'managera')
            setClassManager();
        if (menu == 'sysa')
            setClassSys();
        if (menu == 'blooda')
            setClassBlood();
        if (menu == 'clinica')
            setClassClinic();
        if (menu == 'fooda')
            setClassFood();
        if (menu == 'insa')
            setClassIns();
        if (menu == 'paraclinica')
            setClassParaclinic();
        if (menu == 'personnela')
            setClassPersonnel();
        if (menu == 'druga')
            setClassDrug();
        if (menu == 'servicea')
            setClassService();
        if (menu == 'storea')
            setClassStore();
        if (menu == 'programa')
            setClassProgram();
        
    }
    function setPTitle() {
        var menu = $.urlParam('menu');
        if (menu == 'accountinga')
            $("#bar").prepend(breadcrumb({pageInfo: {pageTitle: _Context.title, parentTitle: Lang.MAccounting, parentUrl: 'menu/accounting.aspx'} }));
        if (menu == 'basicsa')
            $('#ptitle').html("<a href='menu/basics.aspx'>" + Lang.MBasics + "</a> :: " + _Context.title);
        if (menu == 'managera')
            $('#ptitle').html("<a href='menu/admin.aspx'>" + Lang.MPersonnel + "</a> :: " + _Context.title);
        if (menu == 'sysa')
            $("#bar").prepend(breadcrumb({pageInfo: {pageTitle: _Context.title, parentTitle: Lang.MManagement, parentUrl: 'menu/sys.aspx'} }));
        if (menu == 'blooda')
            $('#ptitle').html("<a href='menu/blood.aspx'>" + Lang.MBlood + "</a> :: " + _Context.title);
        if (menu == 'clinica')
            $('#ptitle').html("<a href='menu/clinic.aspx'>" + Lang.MClinic + "</a> :: " + _Context.title);
        if (menu == 'fooda')
            $('#ptitle').html("<a href='menu/food.aspx'>" + Lang.MFood + "</a> :: " + _Context.title);
        if (menu == 'insa')
            $('#ptitle').html("<a href='menu/insurance.aspx'>" + Lang.MInsurance + "</a> :: " + _Context.title);
        if (menu == 'paraclinica')
            $('#ptitle').html("<a href='menu/paraclinic.aspx'>" + Lang.MParaclinic + "</a> :: " + _Context.title);
        if (menu == 'personnela')
            $('#ptitle').html("<a href='menu/personnel.aspx'>" + Lang.MPersonnel + "</a> :: " + _Context.title);
        if (menu == 'druga')
            $('#ptitle').html("<a href='menu/pharmacy.aspx'>" + Lang.MPharmacy + "</a> :: " + _Context.title);
        if (menu == 'servicea')
            $('#ptitle').html("<a href='menu/service.aspx'>" + Lang.MService + "</a> :: " + _Context.title);
        if (menu == 'storea')
            $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: _Context.title, parentTitle: Lang.SellAndBuy, parentUrl: 'menu/sales.aspx' } }));
        if (menu == 'salarysa')
            $("#bar").prepend(breadcrumb({ title: _Context.title, grandParent: { title: Lang.MSalary, url: 'menu/salary.aspx' }, parent: { title: Lang.SalaryDefinition, url: 'menu/salarydefinition.aspx' } }));
    }

    // ------------------------------
    // startup code
    // ------------------------------
    if (_Context.table.toLowerCase() == 'program') {
        checkAccess(['sys,program']);
        checkLevel(3);
    }
    else if (_Context.table.toLowerCase() == 'units')
        checkAccess(['stores,units']);
    else if (_Context.table.toLowerCase() == 'storecategory')
        checkAccess(['stores,storecategory']);
    else if (_Context.table.toLowerCase() == 'topicgroup')
        checkAccess(['accounting,topicgroup']);

    $('.body').addClass('basics');
    setQuickMenu();
    setPTitle();
    run();
}