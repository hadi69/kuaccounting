﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-10-30: file created
*/
function view_shoprefuses() {
    var storeMode = true;
    var startupID = -1;
    function doCommand(com, grid) {
        var isNew = com == Lang.GBNew;
        if (!isNew) {
            var btn = $(".fbutton span:contains(" + com + ")");
            if (btn.hasClass('new'))
                isNew = true;
        }
        if (isNew) {
            var more = '&Type=' + WorkflowTypeEnum.Refuse;
            var count = getServerValue('checkworkflow', 'Workflow', more);
            if (count == 0 || count == null) {
                displayError(Lang.WorkflowM1);
                return;
            }
        }
        if (isNew && com != Lang.GBNew) {
            var desiredEntryTaskID = $(".fbutton span:contains(" + com + ")").attr('class').substring('new '.length);
            show(-1, null, null, desiredEntryTaskID);
        }
        else
            generalCommand(com, grid, show);
    }
    function run() {
        autoComplete('TasK', 'purchasephase', "#fTaskName", "#fTaskID");

        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + "&more=__PrOgrAmconD__" + appendTime();
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.RefuseNo, name: 'NOSerial', sortable: true, css_class: 'width-100 align-center' },
                    { display: Lang.RefuseDate, name: 'RefuseDate', sortable: true, css_class: 'date-cell' },
                    { display: Lang.Store, name: 'StoreName', sortable: false, css_class: 'width-200 align-center' },
                    { display: Lang.Inventory, name: 'StoreKeeperName', sortable: true, css_class: 'width-120 align-center', hide: true },
                    { display: Lang.Seller, name: 'SellerName', sortable: true},
                    { display: Lang.PreStatus, name: 'PreTaskName', hide: true, sortable: true, css_class: 'width-80 align-center' },
                    { display: Lang.Status, name: 'TaskName', hide: true, sortable: false, css_class: 'width-100 align-center' },
                    { display: '', name: 'WorkflowColor', css_class: 'WorkflowColor', hide: true },
                    { display: Lang.Status, name: 'VoucherID', hide: true, process: displayVoucherID },
                    { display: Lang.Status, name: 'DocItemID', hide: true, process: displayDocItemID },
                    { display: Lang.DocumentNumber, name: 'VoucherNo', sortable: false, process: displayVoucherNo, css_class: 'width-100 align-center' },
                    { display: Lang.NumPrints, name: 'NumPrints', sortable: true, css_class: 'width-100 align-center' },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, hide: hideColumn("sales", "refuse", "edit"), css_class: 'action-cell' }
                ],
                buttons:
                   [
                       { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                       { name: Lang.GBEdit, bclass: 'edit', onpress: doCommand },
                       { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                       { separator: true },
                      // { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                       { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                   ],
                searchitems: null,
                sortname: "NOSerial",
                sortorder: "desc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight, resizable: gridResizable,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                },
                onSuccess: function (grid) {
                    postProcessGrid();
                    if (startupID != -1) {
                        show(startupID);
                        startupID = -1;
                    }
                }
            }
            );
    }
    function displayVoucherID(celDiv, id) {
        $(celDiv).addClass('_vid');
    }
    function displayDocItemID(celDiv, id) {
        $(celDiv).addClass('_diid');
    }
    function displayVoucherNo(celDiv, id) {
        $(celDiv).addClass('_vno');
    }
    function postProcessGrid() {
        setTimeout(function () {
            showVoucherLinks();
            colorTasks();
        }, 500);
    }
    function showVoucherLinks() {
        setTimeout(function () {
            $('#flexi tr').each(function () {
                var _id = $('._vid', this).html();
                var _docitemid = $('._diid', this).html();
                //alert(_id);
                if (_id && _id != '')
                    $('._vno', this).html("<a href='list.aspx?table=DocumentAccounting&id=" + _id + "&itemid=" + _docitemid + "'>" + $('._vno', this).html() + "</a>")
            });
        }, 500);
        //ValidVoucher
    }
    function checkDateIntoFinanceYear(f) {
        if (_Context.fyStart == '') {
            alert(Lang.NoCurYear);
            return false;
        }
        if (validateDateIntoFinanceYear(f.RefuseDate))
            return true;
        alert(Lang.PurchasesJsM2 + _Context.fyStart + " - " + _Context.fyEnd + Lang.PurchasesJsM3);
        return false;
    }
    function showcallback(v, m, f) {
        if (v == 'TaskImpls') {
            showTaskImpls(_Context.id, WorkflowTypeEnum.Refuse);
            return false;
        }
        if (v == 'ChangeLog') {
            showLogChange(_Context.table, _Context.id, WorkflowTypeEnum.Refuse);
            return false;
        }
        if (v == 'Print') {
            applyPrintFields(_Context.table);
            printSubform();
            return false;
        }
        if (v == 'Cancel')
            return true;

        if (!checkDateIntoFinanceYear(f))
            return false;

        if (!checkProgramStatus())
            return false;

        _Context.priceDate = f.RefuseDate;
        var qry = (_Context.id == -1) ? 'refuseinsert' : 'refuseupdate';
        if (v != 'OK' && v != 'New') {
            var valid = validateInputs('#detailForm', f);
            if (!valid)
                return false;
            var taskMsg = f.ConfirmTaskMsg;
            //var taskMsg = prompt(Lang.ConfirmTaskMsg, '');
            //if (taskMsg == null)
            //    return false;
            _Context.success = false;
            generalMastercallback('OK', m, f, 'Refuse', qry, 'RefuseItem'
                , 'refuseitemupdate'
                , 'refuseiteminsert'
                , 'refuseitemdelete'
                , 'StoreServiceID', null, show, true);
            if (!_Context.success)
                return false;
            var more = '&RelatedID=' + f.MasterID + '&NextTaskID=' + v + '&UserID=' + _Context.userID + '&Type=' + WorkflowTypeEnum.Refuse
                + '&Msg=' + encodeURIComponent(taskMsg) + '&FileName=' + encodeURIComponent(f.FileName);
            if (runQuery('dotransition', 'TaskImpl', more)) {
                setTimeout(function () { $("#flexi").flexReload(); }, 200);
                return true;
            }
            return false;
        }

        return newMastercallback(v, m, f, 'Refuse', qry, 'RefuseItem'
                , 'refuseitemupdate'
                , 'refuseiteminsert'
                , 'refuseitemdelete'
                , 'StoreServiceID', null, show, null, null);
    }
    function show(id, celDiv, successCallback, desiredEntryTaskID) {
        var btns;
        if (id == -1)
            btns = getDefaultButtons();
        else
            btns = getTaskButtons(WorkflowTypeEnum.Refuse, id, false, "sales", "refuse");
        _Context.id = id;
        var fID = getServerValue('getfinanceyear', 'FinanceYear', null);
        if (fID == -1) {
            alert(Lang.NoCurYear);
            return false;
        }
        var _url = 'get.aspx?op=GetHtml&view=shoprefuse&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + '&FinanceYearID=' + fID;
        if (desiredEntryTaskID)
            _url += '&DesiredEntryTaskID=' + desiredEntryTaskID;
        _url += appendTime();
        showPopup(_url, showcallback, btns, null, successCallback, true);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    dateSelect('.dateSelect');
    checkAccess(['sales,shoprefuseinvoice', 'storeforms,refuse']);
    storeMode = $.urlParam('menu') != 'accountinga';
    if (storeMode) {
        $("#bar").prepend(breadcrumb({pageInfo: {pageTitle: Lang.Refuse, parentTitle: Lang.MStoreForms, parentUrl: 'menu/storeforms.aspx'} }));
        setClassStoreForms();
    }
    else {
        $("#bar").prepend(breadcrumb({pageInfo: {pageTitle: Lang.RefuseInvoice, parentTitle: Lang.SellAndBuy, parentUrl: 'menu/sales.aspx'} }));
        setClassSales();
    }
    startupID = $.urlParam('id');
    if (!startupID)
        startupID = -1;
    $('#chprint').click(function () { choosePrintFields(_Context.table); });
    run();
}