﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
95-12-27: file created
*/
function view_assetdecay() {
    makeChosen();
    // ------------------------------
    // startup code
    // -----------------------------
    dateSelect('.dateSelect')
    autoComplete('Asset', 'enumauto1', "#AssetName", "#AssetID");
    autoComplete('AssetGroup', 'enumauto1', "#AssetGroupName", "#AssetGroupID");
    autoComplete('OrgUnit', 'enumauto1', "#OrgUnitName", "#OrgUnitID");
    autoComplete('Seating', 'enumauto1', "#SeatingName", "#SeatingID");
    autoCompleteFree('Personnel', 'personnelselectauto', '#ResponsibleName', '#ResponsibleID', null
            , function (data, i, n, value) {
                return formatPersonnelItem(data);
            });
    autoCompleteFree('Personnel', 'personnelselectauto', '#JamedarName', '#JamedarID', null
            , function (data, i, n, value) {
                return formatPersonnelItem(data);
            });
    insertValidation();
}