﻿function view_fishvalue() {
    var cnt = 0;

    function addRow(node) {
        // id="x" --> the validationEngine requires ID
        var readonly = node != null ? " readonly='readonly' " : "";
        var row = '<tr>';

        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">'
            + (cnt + 1)
            + '</td>';

        row += '<td data-th="' + Lang.Field + '">'
            + '<input type="hidden" id=f_' + cnt + ' name="ID" value="' + getNodeVal(node, "FishID", -1) + '" />'
            + '<input type="hidden" id=fv_' + cnt + ' name="FishValueID" value="' + getNodeVal(node, "FishValueID", -1) + '" />'
            + '<input type="hidden" id=ffid_' + cnt + ' name="FishDefFieldID" value="'+$("#FishDefFieldID").val()+'" />'
            + '<input type="text" id="p_' + cnt + '" name="PersonnelName" value="' + getNodeVal(node, "Name") + " "+ getNodeVal(node, "Surname")+'"'
            + 'class="personnelname form-control input-sm" ' + readonly + ' />'
            + '<input type="hidden" name="PersonnelID" value="' + getNodeVal(node, "ID") + '" class="PersonnelID" />'
            + '</td>';

        row += '<td data-th="' + Lang.PersonneliNum + '">'
            + '<input type="text" id="pn_' + cnt + '" name="PersonneliNum" value="' + getNodeVal(node, "PersonneliNum") + '" class="personnelinum form-control input-sm"' + readonly + ' />'
            + '</td>';

        row += '<td data-th="' + Lang.Value + '">'
            + '<input type="text" id="fo_' + cnt + '" name="Value" value="' + getNodeVal(node, "Value") + '" class="value form-control input-sm validate[custom[integer]] monetary" />'
            + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }

    function fillInfo() {
        var url = "get.aspx?op=GetXml&table=Personnel&qd=getpersonnelvalue&FishYear=" + year + "&FishMonth=" + month + "&FishDefFieldID=" + fishdeffield;
        fillTable(url, '#tableItems', addRow);
    }

    

    setValue("#FishMonth");
    var year = $("#FishYear").val();
    var month = $("#FishMonth").val();
    var fishdeffield = $("#FishDefFieldID").val();
    fillInfo();
    var lastvalue = 0, curvalue = 0;
    $("#tableItems").find('.value').each(function () {
        $(this).focus(function () {
            lastvalue = $(this).val();
        });
        $(this).focusout(function () {
            curvalue = $(this).val();
            if (lastvalue != curvalue) {
                $(this).addClass('changed');
            }
        });
        
    });
    $("#tableItems tr:first").next().find('.value').focus();
    if ($("#DeductID").val() > 0) {
        debugger;
        $('.value').addClass('changed');
    }
    
    commaInputs();
    monetaryKeys();
}