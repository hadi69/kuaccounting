﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
92-11-08: file created
*/
function view_servicepriceinformation() {
    // ------------------------------
    // startup code
    // ------------------------------

    var storeServiceID = $("#StoreServiceID").val();
    var servicePriceID = $("#ServicePriceID").val();
    var saleTypeID = $("#SaleTypeID").val();
    if (!storeServiceID) {
        storeServiceID = 'NULL';
    }
    if (!servicePriceID) {
        servicePriceID = 'NULL';
    }
    if (!saleTypeID) {
        saleTypeID = 'NULL';
    }
    function bindServicePricing() {
        var _url = 'service.asmx/GetQueryList?table=SaleTypeItem&qd=saletypeitem&StoreServiceID=' + storeServiceID + '&SaleTypeID=' + saleTypeID + '&ServicePriceID=' + servicePriceID + appendTime();

        $("#scondFlexi").flexigrid
        (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', hide: true },
                    { display: Lang.Date, name: 'Date', css_class: 'date-cell', sortable: false, process: capturePriceDate },
                    { display: Lang.Price, name: 'Price', css_class: 'width-100 align-center', sortable: true, process: showMonetary }
                ],
                searchitems: null,
                sortname: "ID",
                sortorder: "asc",
                usepager: false,
                rpOptions: [20, 50, 1000, 1500, 3000], //allowed per-page values
                rp: 1500,
                useRp: false,
                rp: gridRp,
                showTableToggleBtn: false,
                width: gridWidth,
                height: 400,
                resizable: gridResizable,
                singleSelect: true,
            }
        );
    }

    function bindChangeLog() {
        var tableID = toInt($('#TableID').val(), -1);
        var storeDraftItemId = toInt($('#StoreDraftItemID').val(), -1);
        var _url = 'service.asmx/GetList?table=LogChange&qd=storeservicelogchange&TableID=' + tableID + '&TableName=StoreDraft&StoreDraftItemID=' + storeDraftItemId + appendTime();
        $("#changelogflexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 30, sortable: true, hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.Username, name: 'UserName', sortable: true, css_class: 'width-100 align-center' },
                    { display: Lang.Date, name: 'LogDate', sortable: true, css_class: 'date-cell' },
                    { display: Lang.Time, name: 'LogTime', sortable: true, css_class: 'date-cell' },
                    { display: Lang.Comments, name: 'ShortData', sortable: true },
                    { display: Lang.Type, name: 'Type', sortable: true, css_class: 'width-100 align-center', process: displayType },
                    { display: Lang.ColEdit, name: 'View', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' }
                ],
                sortname: "LogDate",
                sortorder: "desc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight, resizable: gridResizable,
                singleSelect: false
            }
            );
    }
    function show(id) {
        //_Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=LogChange&qd=logchange&id=' + id + appendTime();
        //var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, function (v, m, f) { return true; }, getCloseButtons());
    }
    function displayType(celDiv, id) {
        var _type = parseInt($(celDiv).html(), 10);
        // 0:Login;1:Logout;2:Query;3:Insert;4:Update;5:Delete;6:MultiDelete
        if (_type == 0)
            $(celDiv).html(Lang.Login);
        else if (_type == 1)
            $(celDiv).html(Lang.Exit);
        else if (_type == 2)
            $(celDiv).html(Lang.Query);
        else if (_type == 3)
            $(celDiv).html(Lang.GBNew);
        else if (_type == 4)
            $(celDiv).html(Lang.Update);
        else if (_type == 5)
            $(celDiv).html(Lang.GBDel);
        else if (_type == 6)
            $(celDiv).html(Lang.RemoveGroup);
    }
    bindServicePricing();
    bindChangeLog();
    tabify('#subDetailForm');
}