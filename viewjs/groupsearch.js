﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
93-04-08: file created
*/
function view_groupsearch() {
    checkAccess(['distribution,groupsearch']);

    var mGroupID = -1, Type = 1;
   // var mFactType = 1; // 0 : purchase, 1: StoreDraft
    function setGroup(gID) {
        if (mGroupID != gID) {
            mGroupID = gID;
            showBreadcrumb();
            showGroups();
            showMembers();
        }
    }
    function showBreadcrumb() {
        $('a', '#breadcrumb').unbind('click');
        var str = "";
        if (mGroupID != -1) {
            var items = fillTable("get.aspx?op=GetXml&qd=breadcrumb&ID=" + mGroupID, null, null);
            $(items).find("R").each(function () {
                var node = $(this);
                str = '<li><a href="' + getNodeVal(node, "ID") + '">' + getNodeVal(node, "Title") + '</a></li>' + str;
            });
        }
        str = "<li><a href='-1'>" + Lang.AllGroup + "</a></li>" + str;
        $('#breadcrumb').html(str);
        $('a', '#breadcrumb').click(function () {
            var gid = toInt($(this).attr('href'), -1);
            setGroup(gid);
            return false;
        });
    }
    function showGroups() {
        $('a', '#subgroups').unbind('click');
        var str = "";
        var items = fillTable("get.aspx?op=GetXml&qd=subgrouping&GroupingType=" + Type + "&ParentID=" + mGroupID, null, null);

        $(items).find("R").each(function () {
            //var node = $(this);
            //str += '<a href="' + getNodeVal(node, "ID") + '">' + getNodeVal(node, "Title") + '(' + getNodeVal(node, "NumMembers") + ')</a>';
            var tag = NodeToJson(this);
            var tagTemplate = CompileHB('tag-template', tag);
            str += tagTemplate;
        });
        $('#subgroups').html(str);

        if ($(items).find('R').length == 0) {
            $('#subgroups').html(Lang.EmptyData);
        }

        $('a', '#subgroups').click(function () {
            var gid = toInt($(this).attr('href'), -1);
            setGroup(gid);
            return false;
        });
    }
    function showMembers() {
        
        if (Type == GroupingTypeEnum.Person) {
            $("#flexiPersons").show();
            $('#members').html("");
            showPersons();
        }
        else {
            $("#flexiPersons").hide();
            var str = "";
            var items = fillTable("get.aspx?op=GetXml&qd=groupmembers1&GroupingType=" + Type + "&GroupingID=" + mGroupID, null, null);
            var moneyUnit = $(".MonetaryUnit option:selected").text();
            $(items).find("R").each(function () {
                var node = $(this);
                str += '<div>' + getNodeVal(node, "OtherName") + '</div>';
            });
            $('#members').html(str);

            if ($(items).find('R').length == 0) {
                $('#members').html('<p class="padding-5">' + Lang.EmptyData + '</p>');
            }
        }

    }
    function doCommand(com, grid) {
        generalCommand(com, grid, function () { });
    }
    var firstFlexi = true;
    function showPersons() {
        var _url = 'service.asmx/GetList?table=GroupingMember&qd=groupmembersperson&GroupingID=' + mGroupID + appendTime();
        //alert(_url);
        if (firstFlexi) {
            firstFlexi = false;
            $("#flexi").flexigrid
                    (
                    {
                        url: _url,
                        dataType: 'xml',
                        colModel:
                        [
                            { display: 'ID', name: 'ID', sortable: false, align: 'center', hide: true },
                            { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: true, css_class: 'row-cell' },
                            { display: 'OtherID', name: 'OtherID', sortable: false, align: 'center', hide: true },
                            { display: Lang.PersonFullName, name: 'OtherName', sortable: true, css_class: 'width-200 align-center' },
                            { display: Lang.FirstGroup, name: 'FIdentify', sortable: true, css_class: 'width-100 align-center' },
                            { display: Lang.SecondGroup, name: 'SIdentify', sortable: true, css_class: 'width-100 align-center' },
                            { display: Lang.HomeAddress, name: 'HomeAddress', sortable: true },
                            { display: Lang.HomeTelNum, name: 'HomeTelNum', sortable: true, css_class: 'width-80 align-center' },
                            { display: Lang.CellNumber1, name: 'CellNumber1', sortable: true, css_class: 'width-80 align-center'},
                            { display: Lang.Email, name: 'Email', sortable: true, css_class: 'width-80 align-center'}
                        ],
                        buttons:
                        [
                            { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                            { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand, onaccess: hasAccess("accounting", "voucher", "excel") && hasGeneralAccess("excel") }
                        ],
                        searchitems: null,
                        sortname: "OtherName",
                        sortorder: "desc",
                        usepager: true,
                        rpOptions: [20, 50, 1000, 1500, 3000], //allowed per-page values
                        rp: 1500,
                        useRp: true,
                        rp: gridRp,
                        showTableToggleBtn: gridToggle,
                        width: gridWidth,
                        height: gridHeight, resizable: gridResizable,
                        singleSelect: true
                    }
                    );
        }
        else {
            $("#flexi").flexOptions({ url: _url });
            $("#flexi").flexReload();
        }
    }
    // ------------------------------
    // startup code
    // ------------------------------
    showBreadcrumb();
    showGroups();
    showMembers();
    $('#SearchType').change(function () {
        Type = $(this).val();
        showBreadcrumb();
        showGroups();
        showMembers();
    });
    $("#bar").prepend(breadcrumb({pageInfo: {pageTitle: Lang.Search, parentTitle: Lang.MDistribution, parentUrl: 'menu/distribution.aspx'} }));
    setClassDistribution();
}