﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
93-05-15: file created
*/
function view_bank() {
    // ------------------------------
    // startup code
    // ------------------------------
    function autoPostPack(event, data, formatted) {
        $(event.target).next().val(data[0]); // ID
    }
    setChecked("#Enabled");
    autoComplete('BankType', 'enumauto', '#BankTypeName', '#BankTypeID', autoPostPack
     , function (data, i, n, value) {
         return data[1];
     });
    insertValidation();
}