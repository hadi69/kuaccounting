﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    94-06-04: file created
*/
function view_buildorders() {
    var startupID = -1;
    function doCommand(com, grid) {
         if (com == Lang.CreatDraft) {
            var ids = '';
            $('.trSelected', grid).each(function () {
                var id = $(this).attr('id');
                id = id.substring(id.lastIndexOf('row') + 3);
                ids += id + ',';
            });
            if (ids.length > 0) {
                ids = ids.substring(0, ids.length - 1);
                // alert(ids.substring(0,ids[1]));
                // alert(ids.split(','));
                var more = '&ids=' + ids;
                if (runQuery('createstoredraftforbuildorder', 'BuildOrder', more))
                    displayError(Lang.Success, null, Lang.Success);
            }
         }
        else {
            var isNew = com == Lang.GBNew;
            if (!isNew) {
                var btn = $(".fbutton span:contains(" + com + ")");
                if (btn.hasClass('new'))
                    isNew = true;
            }
            if (isNew) {
                var more = '&Type=' + WorkflowTypeEnum.BuildOrder;
                var count = getServerValue('checkworkflow', 'Workflow', more);
                if (count == 0 || count == null) {
                    displayError(Lang.WorkflowM1);
                    return;
                }
            }
            if (isNew && com != Lang.GBNew) {
                var desiredEntryTaskID = $(".fbutton span:contains(" + com + ")").attr('class').substring('new '.length);
                //alert(desiredEntryTaskID);
                buildordershow(-1, null, null, desiredEntryTaskID);
            }
            else
                generalCommand(com, grid, buildordershow);
        }
    }
    function run() {
        autoComplete('TasK', 'buildorderphase', "#fTaskName", "#fTaskID");
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        var btns = getTaskGridButtons(WorkflowTypeEnum.BuildOrder, doCommand, "storeforms", "buildorder");
        $("#flexi").flexigrid
                    ({
                        url: _url,
                        dataType: 'xml',
                        colModel:
                        [
                            { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                            { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                            { display: Lang.BuildOrderNo, name: 'NOSerial', sortable: true, css_class: 'width-80 align-center' },
                            { display: Lang.Store, name: 'StoreName', sortable: false, css_class: 'width-80' },
                            { display: Lang.BuildOrderDate, name: 'OrderDate', sortable: true, process: showDate, css_class: 'date-cell' },
                            { display: Lang.Applicant, name: 'RequesterName', sortable: true },
                          //  { display: Lang.Priority, name: 'PriorityName', sortable: true, css_class: 'width-100 align-center' },
                            { display: Lang.PreStatus, name: 'PreTaskName', sortable: true, css_class: 'width-80 align-center' },
                            { display: Lang.Status, name: 'TaskName', sortable: true, css_class: 'width-80 align-center' },
                            { display: '', name: 'WorkflowColor', css_class: 'WorkflowColor', hide: true },
                           // { display: '', name: 'PurchaseRequestID', css_class: '_prid', hide: true },
                            //{ display: Lang.PurchaseRequest, name: 'PurchaseRequestTitle', css_class: 'width-80 align-center _prtitle' },
                           // { display: '', name: 'PurchaseID', css_class: '_sdid', hide: true },
                           // { display: Lang.Purchase, name: 'PurchaseTitle', css_class: 'width-80 align-center _sdtitle' },
                            { display: Lang.Purchases, name: 'Purchases', process: function (celDiv, id) { purchasesLink(celDiv, id); }, sortable: true, css_class: 'width-100 align-center' },
                            { display: Lang.NumPrints, name: 'NumPrints', sortable: true, css_class: 'width-100 align-center' },
                            { display: Lang.ProductedNum, name: 'Producted', sortable: true, hide: true, css_class: 'producted' },
                            { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, buildordershow); }, css_class: 'action-cell' }
                        ],
                        buttons: btns,
                        searchitems: null,
                        sortname: "ID",
                        sortorder: "asc",
                        usepager: true,
                        useRp: true,
                        rp: gridRp,
                        showTableToggleBtn: gridToggle,
                        width: gridWidth,

                        height: gridHeight, resizable: gridResizable,
                        singleSelect: false,
                        onDoubleClick: function (row, grid, options) {
                            var id = row.id.substring(3);
                            buildordershow(id);
                        },
                        onSuccess: function (grid) {
                            postProcessGrid();
                            if (startupID != -1) {
                                buildordershow(startupID);
                                startupID = -1;
                            }
                        }
                    });
    }
    function postProcessGrid() {
        setTimeout(function () {
            colorTasks();
            showLinks();
            colorComments();
        }, 500);
    }
    function showLinks() {
        $('#flexi tr').each(function () {
            var _id = $('._prid div', this).html();
            //alert(_id);
            if (_id && _id != '')
                $('._prtitle div', this).html("<a href='list.aspx?table=PurchaseRequest&id=" + _id + "'>" + $('._prtitle', this).find('div').html() + "</a>")

            _id = $('._sdid div', this).html();
            if (_id && _id != '')
                $('._sdtitle div', this).html("<a href='list.aspx?table=Purchase&id=" + _id + "'>" + $('._sdtitle', this).find('div').html() + "</a>")
        });
    }

    function colorComments() {
        $('#flexi tr .producted').each(function () {
            var producted = toInt($('div', this).html(), 1);
            if (producted == 0)
                $(this).closest('tr').find('td').css('background-color', '#F0AD4E');
        });
    }

    function purchasesLink(celDiv, id) {
        $(celDiv).html('<a>' + Lang.Purchases + '</a>');
        $(celDiv).click(function (event) {
            event.preventDefault();
            if (clicksDisabled)
                return;
            disableClicks();
            var _url = 'get.aspx?op=GetHtml&view=buildorderpurchases&table=Purchase&qd=purchase&id=' + id + appendTime();
            showPopup(_url, null, getCloseButtons(), null, function () { return true; });
        });
    }

    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['stores,buildorder']);
    checkLevel(2);
    $("#bar").prepend(breadcrumb({pageInfo: {pageTitle: Lang.BuildOrder, parentTitle: Lang.MStore, parentUrl: 'menu/store.aspx'} }));
    setClassStore();

    startupID = $.urlParam('id');
    if (!startupID)
        startupID = -1;
    run();
}