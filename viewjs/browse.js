﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
93-04-08: file created
*/
function view_browse() {
    checkAccess(['sales,browse']);
    checkLevel(2);

    // ToggleShoppingCart button
    $("#toggle-shopping-cart").click(function () {
        $("#items").toggle();
    });
    //$("#bar").prepend(breadcrumb({
    //    title: Lang.Browse,
    //    //parent: {
    //    //    title: Lang.SellAndBuy,
    //    //    url: 'menu/sales.aspx'
    //    //}
    //}));
    // GroupingType:    0:person;1:servic;2:visitor;3:supplier
    var mGroupID = -1, GroupingType = 1;
    var mFactType = WorkflowTypeEnum.StoreDraft;
    function setGroup(gID) {
        if (mGroupID != gID) {
            mGroupID = gID;
            showBreadcrumb();
            showGroups();
            showMembers();
            handleServiceAuto();
        }
    }
    function showBreadcrumb() {
        $('a', '#breadcrumb').unbind('click');
        $('.samelevel', '#breadcrumb').unbind('click');
        var str = "";
        if (mGroupID != -1) {
            var items = fillTable("get.aspx?op=GetXml&qd=breadcrumb&ID=" + mGroupID, null, null);
            $(items).find("R").each(function () {
                var node = $(this);
                var list = "";
                list += '<li>'
                + '<a href="' + getNodeVal(node, "ID") + '">'
                + getNodeVal(node, "Title")
                + '</a>'
                + '<img src="img/ddn.png" gid="' + getNodeVal(node, "ParentID") + '" class="samelevel" />'
                + '</li>';

                str = list + str;
            });
        }
        str = "<li><a href='-1'>" + Lang.AllGroup + "</a></li>" + str;
        $('#breadcrumb').html(str);
        $('a', '#breadcrumb').click(function () {
            var gid = toInt($(this).attr('href'), -1);
            setGroup(gid);
            return false;
        });
        $('.samelevel', '#breadcrumb').click(function () {
            var gid = toInt($(this).attr('gid'), -1);

            var _cmds = [];
            var items = fillTable("get.aspx?op=GetXml&qd=subgrouping&GroupingType=" + GroupingType + "&ParentID=" + gid, null, null);
            $(items).find("R").each(function () {
                var node = $(this);
                _cmds[_cmds.length] = [getNodeVal(node, "Title") + '(' + getNodeVal(node, "NumMembers") + ')', getNodeVal(node, "ID")];
            });
            // options:parent, cmds, callback
            // cmds: [['do it', 0], ['see it', 1], ['title'], ['why', 2]]
            // callback(cmd) //cmd = 0, 1, ....
            showMenu({
                parent: $(this), cmds: _cmds, callback: function (groupID) { setGroup(groupID); }
            })
        });
    }
    function showGroups() {
        $('a', '#subgroups').unbind('click');
        var str = "";
        var items = fillTable("get.aspx?op=GetXml&qd=subgrouping&GroupingType=" + GroupingType + "&ParentID=" + mGroupID, null, null);

        if ($(items).find('R').length == 0) {
            $("#subgroups-container").hide();
        } else {
            $("#subgroups-container").show();
        }

        $(items).find("R").each(function () {
            var tag = NodeToJson(this);
            var tagTemplate = CompileHB('tag-template', tag);
            str += tagTemplate;
        });
        $('#subgroups').html(str);
        $('a', '#subgroups').click(function () {
            var gid = toInt($(this).attr('href'), -1);
            setGroup(gid);
            return false;
        });
    }
    function showMembers() {
        $('a', '#members').unbind('click');
        var qry;
        if ($('#BrowseBuyerID').val()) {
            qry = 'buyerstoregroupmembers';
        } else {
            qry = 'storegroupmembers';
        }
        var str = "";
        var qd = mFactType == WorkflowTypeEnum.Purchase ? "groupmembers" : qry;
        var items = fillTable("get.aspx?op=GetXml&qd=" + qd + "&GroupingID=" + mGroupID + '&FactType=' + mFactType + '&CurID=' + getCurID() + '&BrowseBuyerID=' + $('#BrowseBuyerID').val(), null, null);
        var moneyUnit = $(".MonetaryUnit option:selected").text();
        $("#members").html("");
        var allServices = new app.Collections.Services();
        allServices.parseXml(items);
        var allServicesView = new app.Views.Services({collection: allServices});
        $("#members").html(allServicesView.el);

        // Check if there are no items to show, and if so, show a message!
        if ($(items).find('R').length == 0) {
            $("#members").append(CompileHB('empty-data', {}));
        }
        window.bindMemberEvents = bindMemberEvents;
        bindMemberEvents();
        //alert($("#members .next").length);
    }
    function bindMemberEvents() {
        $('a.add-button', '#members').unbind('click').click(function () {
            var curID = getCurID();
            if (!hasValuePositive(curID)) {
                Growl.Error(Lang.NoFactor);
                return false;
            }
            var id = toInt($(this).attr('href'), -1);
            var price = $(this).attr("data-price");
            if (price === void 0) {
                price = 0;
            }
            if (mFactType == WorkflowTypeEnum.StoreDraft) {
                if (runQuery('addtocurstoredraft', 'StoreDraftItems', '&StoreServiceID=' + id + '&StoreDraftID=' + curID
                    + '&DeliveredAmount=' + toInt($(this).closest('div').find('input').val(), 0) + '&Price=' + price)) {
                    Growl.Notice(Lang.SuccessfullyAddedToFactor);
                    showFactor();
                }
            }
            else if (mFactType == WorkflowTypeEnum.Purchase) {
                var spid = toInt($(this).attr('spid'), -1);
                if (runQuery('addtocurpurchase', 'PurchaseItems', '&StoreServiceID=' + id + '&PurchaseID=' + curID
                    + '&Amount=' + toInt($(this).closest('div').find('input').val(), 0) + '&Price=' + price
                    + '&ServicePriceID=' + spid + '')) {
                    Growl.Notice(Lang.SuccessfullyAddedToFactor);
                    showFactor();
                }
            }
            else if (mFactType == WorkflowTypeEnum.Return) {
                if (runQuery('addtocurreturn', 'ReturnItem', '&StoreServiceID=' + id + '&ReturnID=' + curID
                    + '&Num=' + toInt($(this).closest('div').find('input').val(), 0) + '&Price=' + price)) {
                    Growl.Notice(Lang.SuccessfullyAddedToFactor);
                    showFactor();
                }
            }
            else if (mFactType == WorkflowTypeEnum.Refuse) {
                if (runQuery('addtocurrefuse', 'RefuseItem', '&StoreServiceID=' + id + '&RefuseID=' + curID
                    + '&Num=' + toInt($(this).closest('div').find('input').val(), 0) + '&Price=' + price)) {
                    Growl.Notice(Lang.SuccessfullyAddedToFactor);
                    showFactor();
                }
            }
            return false;
        });
    }

    function showFactors() {
        var _url = 'get.aspx?op=auto';
        if (mFactType == WorkflowTypeEnum.StoreDraft) {
            if ($('#BrowseBuyerID').val()) {
                _url += '&table=StoreDraft&qd=getcursBuyertoredrafts&StoreKeeperID=' + _Context.personnelID + '&BuyerID=' + $('#BrowseBuyerID').val();
            } else {
                _url += '&table=StoreDraft&qd=getcurstoredrafts&StoreKeeperID=' + _Context.personnelID;
            }
        }
        else if (mFactType == WorkflowTypeEnum.Purchase)
            _url += '&table=StoreDraft&qd=getcurpurchases&StoreKeeperID=' + _Context.personnelID;
        else if (mFactType == WorkflowTypeEnum.Return)
            _url += '&table=StoreDraft&qd=getcurreturns&StoreKeeperID=' + _Context.personnelID;
        else if (mFactType == WorkflowTypeEnum.Refuse)
            _url += '&table=StoreDraft&qd=getcurrefuses&StoreKeeperID=' + _Context.personnelID;
        _url += appendTime();
        fillCombo($('#curFacts'), _url);
        triggerChosenUpdate("#curFacts");
    }
    function getCurID() {
        return $('#curFacts').val();
    }
    function showFactor() {
        $('#items').html('');
        handleServiceAuto();
        var curID = getCurID();
        if (!hasValuePositive(curID))
            return;
        $('.num', '#items').unbind('click');
        $('.remove', '#items').unbind('click');
        var str = '<table class="table-with-border table-with-padding">'
            + '<tr class="th">'
            + '<td>' + Lang.GoodName + '</td>'
            + '<td class="width-80 align-center">' + Lang.Price + '</td>'
            + '<td class="width-60 align-center">' + Lang.Quantity + '</td>'
            + '<td class="width-60 align-center">' + Lang.ColDel + '</td>'
            + '</tr>';
        var items;
        if (mFactType == WorkflowTypeEnum.StoreDraft)
            items = fillTable("get.aspx?op=GetXml&qd=curstoredraftitem&StoreDraftID=" + curID, null, null);
        else if (mFactType == WorkflowTypeEnum.Purchase)
            items = fillTable("get.aspx?op=GetXml&qd=curpurchaseitem&PurchaseID=" + curID, null, null);
        else if (mFactType == WorkflowTypeEnum.Return)
            items = fillTable("get.aspx?op=GetXml&qd=curreturnitem&ReturnID=" + curID, null, null);
        else if (mFactType == WorkflowTypeEnum.Refuse)
            items = fillTable("get.aspx?op=GetXml&qd=currefuseitem&RefuseID=" + curID, null, null);
        var moneyUnit = $(".MonetaryUnit option:selected").text();
        var totalItems = 0;
        if ($(items).find("R").length == 0) {
            str = CompileHB('empty-data', { message: Lang.EmptyFactor });
        }
        $(items).find("R").each(function () {
            var node = $(this);

            var model = NodeToJson(this);
            totalItems += +model.DeliveredAmount;

            str += '<tr>';
            str += '<td>' + getNodeVal(node, "StoreServiceName") + '</td>';
            str += '<td class="width-80 align-center">' + numberWithCommas(toMonetary(getNodeVal(node, "Price"))) + ' ' + moneyUnit + '</td>';
            str += '<td class="width-60 align-center"><a href="' + getNodeVal(node, "ID") + '" class="num">' + getNodeVal(node, "DeliveredAmount") + '</a></td>';
            str += '<td class="width-60 align-center"><img src="img/delete.gif" class=remove itemid="' + getNodeVal(node, "ID") + '" ></td>';
            str += '</tr>';
        });

        // Update the counter in the toggle button
        $("#shopping-cart-item-count").html(totalItems);
        str += "</table>";
        $('#items').html(str);
        $('.num', '#items').click(function () {
            var id = toInt($(this).attr('href'), -1);
            var num = toInt($(this).html(), -1);
            var neu = toInt(prompt(Lang.Num, num), num);
            if (num != neu) {
                if (mFactType == WorkflowTypeEnum.StoreDraft) {
                    if (runQuery('setstoredraftitemnum', 'StoreDraft', '&ID=' + id + '&Num=' + neu))
                        showFactor();
                }
                else if (mFactType == WorkflowTypeEnum.Purchase) {
                    if (runQuery('setpurchaseitemnum', 'Purchase', '&ID=' + id + '&Num=' + neu))
                        showFactor();
                }
                else if (mFactType == WorkflowTypeEnum.Return) {
                    if (runQuery('setreturnitemnum', 'Returns', '&ID=' + id + '&Num=' + neu))
                        showFactor();
                }
                else if (mFactType == WorkflowTypeEnum.Refuse) {
                    if (runQuery('setrefuseitemnum', 'Refuse', '&ID=' + id + '&Num=' + neu))
                        showFactor();
                }
            }
            return false;
        });
        $('.remove', '#items').click(function () {
            var id = toInt($(this).attr('itemid'), -1);
            if (mFactType == WorkflowTypeEnum.StoreDraft) {
                if (runQuery('storedraftitemdelete', 'StoreDraft', '&ID=' + id)) {
                    Growl.Notice(Lang.SuccessfullyRemovedFromFactor);
                    showFactor();
                }
            }
            else if (mFactType == WorkflowTypeEnum.Purchase) {
                if (runQuery('purchaseitemdelete', 'Purchase', '&ID=' + id)) {
                    Growl.Notice(Lang.SuccessfullyRemovedFromFactor);
                    showFactor();
                }
            }
            else if (mFactType == WorkflowTypeEnum.Return) {
                if (runQuery('returnitemdelete', 'ReturnItem', '&ID=' + id)) {
                    Growl.Notice(Lang.SuccessfullyRemovedFromFactor);
                    showFactor();
                }
            }
            else if (mFactType == WorkflowTypeEnum.Refuse) {
                if (runQuery('refuseitemdelete', 'RefuseItem', '&ID=' + id)) {
                    Growl.Notice(Lang.SuccessfullyRemovedFromFactor);
                    showFactor();
                }
            }
            return false;
        });
    }
    function getDesiredEntryTaskID() {
        var items = getItems("getstarttasks", "Type=" + mFactType);
        var first = true;
        var id = -1;
        if (items) {
            $(items).find("R").each(function () {
                if ($(this).attr("State") == 0) // TaskStateEnum: Start=0
                {
                    // we may have more than one entry task. in this case, we select the second one.
                    // because we suppose the second one is Pish Factor
                    if (id == -1)
                        id = $(this).attr("ID");
                    else if (first) {
                        first = false;
                        id = $(this).attr("ID");
                    }
                }
            });
        }
        return id;
    }

    function handleServiceAuto() {
      //  $('#TheName').unautocomplete();
        var _autourl = 'get.aspx?op=Auto&table=Service&qd=groupmembersauto&GroupingID=' + mGroupID + '&FactType=' + mFactType + '&CurID=' + getCurID() + appendTime();
        autoCompleteFree('StoreServices', 'groupmembersauto', '#TheName', '#TheID'
                , function (event, data, formatted) {
                    $('#TheID').val(data[0]);
                    $('#TheName').val(data[1]);
                    $('#ThePrice').val(data[3]);
                }
                , function (data, i, n, value) {
                    // StoreServices.ID, Title, ServiceID, Price, ExpireDate, Income, Outcome, UnitID, UnitName
                return '<div dir=rtl><b>' + data[1] + '</b><br />'
                    + Lang.Price + " : " + numberWithCommas(toFloat(toMonetary(data[3]), 0.0).toFixed(_Context.decimalPoints)) + '<br />'
                    + Lang.ExpireDate + " : " + data[4] + '<br />'
                    + Lang.Supply + " : " + (data[5] - data[6]).toFixed(_Context.decimalPoints)
                    + '</div>';
            }, null, null, _autourl);
        //$('#TheName').focus(function () {
        //    $('#TheID').val('');
        //    $('#TheName').val('');
        ////    $('#ThePrice').val('');
        //    return false;
        //});
    }

    $('#checkout').click(function () {
        // check finance year
        var fID = getServerValue('getfinanceyear', 'FinanceYear', null);
        if (fID == -1) {
            alert(Lang.NoCurYear);
            return false;
        }

        //  check workflow
        var more = '&Type=' + mFactType;
        var count = getServerValue('checkworkflow', 'Workflow', more);
        if (count == 0 || count == null) {
            displayError(Lang.WorkflowM1);
            return false;
        }

        if (mFactType == WorkflowTypeEnum.StoreDraft)
            showStoreDraft(-1, clickSaveButton, fID);
        return false;

    });
    $('#failertoorder').click(function () {
        document.location.href = 'list.aspx?table=VisitorCustomersStatus&view=visitorcustomersstatuss';
        return false;
    });
    // ------------------------------
    // startup code
    // ------------------------------
    showBreadcrumb();
    showGroups();
    showFactors();
    showFactor();
    showMembers();
    if (getCurID() > 0) {
        $('#shopingcartdiv').show();
        $('.del').show();
    }
    if ($('#BrowseBuyerID').val()) {
        $('#Buyerdiv').show();
        showPersonBook('#BrowseBuyerID');
    } else {
        $('#factortypediv').show();
    }

    $('#factType').chosen().change(function () {
        var t = $(this).val();
        if (t != mFactType) {
            mFactType = t;
            showFactors();
            showFactor();
            showMembers();
        }
    });
    $('#curFacts').chosen().change(function () {
        if (mFactType == WorkflowTypeEnum.StoreDraft)
            showMembers();
        showFactor();
    });
    $('.new').click(function () {
        // check finance year
        var fID = getServerValue('getfinanceyear', 'FinanceYear', null);
        if (fID == -1) {
            alert(Lang.NoCurYear);
            return false;
        }

        //  check workflow
        var more = '&Type=' + mFactType;
        var count = getServerValue('checkworkflow', 'Workflow', more);
        if (count == 0 || count == null) {
            displayError(Lang.WorkflowM1);
            return false;
        }

        if (mFactType == WorkflowTypeEnum.StoreDraft)
            showStoreDraft(-1, null, fID);
        else if (mFactType == WorkflowTypeEnum.Purchase)
            showPurchase(-1, null, fID);
        else if (mFactType == WorkflowTypeEnum.Refuse)
            showRefuse(-1, null, fID);
        else if (mFactType == WorkflowTypeEnum.Return)
            showReturn(-1, null, fID);
        return false;
    });

    $('.del').click(function () {
        var ok = runQuery('storedraftdelete', 'StoreDraft', '&ID=' + getCurID());
        if (ok) {
            Growl.Notice(Lang.RecordDeletedSuccess, 3000);
            $('#shopingcartdiv').hide();
            $('#curFacts').val('');
            showBreadcrumb();
            showGroups();
            showFactors();
            showFactor();
            showMembers();
            $('.del').hide();
        } else {
            Growl.Error(Lang.Failed, 3000);
        }
    });
    //$('.edit').click(function () {
    //    var curID = getCurID();
    //    if (!hasValuePositive(curID))
    //        return;
    //    if (mFactType == WorkflowTypeEnum.StoreDraft) {
    //        showStoreDraft(curID, null, null);
    //    }
    //    else if (mFactType == WorkflowTypeEnum.Purchase) {
    //        //var id = toInt(getServerValue('getcurpurchase', 'Purchase', "StoreKeeperID=" + _Context.personnelID), -1);
    //        showPurchase(curID, null, null);
    //    }
    //    else if (mFactType == WorkflowTypeEnum.Refuse) {
    //        //var id = toInt(getServerValue('getcurrefuse', 'Refuse', "StoreKeeperID=" + _Context.personnelID), -1);
    //        showRefuse(curID, null, null);
    //    }
    //    else if (mFactType == WorkflowTypeEnum.Return) {
    //        //var id = toInt(getServerValue('getcurreturn', 'Returns', "StoreKeeperID=" + _Context.personnelID), -1);
    //        showReturn(curID, null, null);
    //    }
    //    return false;
    //});

    function showStoreDraft(id, successCallback, fID) {
        var btns;
        if (id == -1)
            btns = getDefaultButtons(false);
        else
            btns = getTaskButtons(WorkflowTypeEnum.StoreDraft, id);
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=StoreDraft&qd=storedraft&id=' + id + '&FinanceYearID=' + fID;
        if (id == -1) {
            var entryID = getDesiredEntryTaskID();
            if (entryID == -1) {
                displayError(Lang.WorkflowM2);
                return false;
            }
            _url += '&view=browsenew&ft=' + mFactType + '&DesiredEntryTaskID=' + entryID + '&BuyerID=' + $('#BrowseBuyerID').val() + '&BuyerName=' + $('#BrowseBuyerName').val();
        }
        _url += appendTime();
        showPopup(_url, showStoreDraftCallback, btns, null, successCallback, true);
    }
    function showPurchase(id, successCallback, fID) {
        var btns;
        if (id == -1)
            btns = getDefaultButtons(false);
        else
            btns = getTaskButtons(WorkflowTypeEnum.Purchase, id);
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=Purchase&qd=purchase&id=' + id + '&FinanceYearID=' + fID;
        if (id == -1) {
            var entryID = getDesiredEntryTaskID();
            if (entryID == -1) {
                displayError(Lang.WorkflowM2);
                return false;
            }
            _url += '&view=browsenew&ft=' + mFactType + '&DesiredEntryTaskID=' + entryID;
        }
        _url += appendTime();
        showPopup(_url, showPurchaseCallback, btns, null, successCallback, true);
    }
    function showRefuse(id, successCallback, fID) {
        var btns;
        if (id == -1)
            btns = getDefaultButtons(false);
        else
            btns = getTaskButtons(WorkflowTypeEnum.Purchase, id);
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=Refuse&qd=refuse&id=' + id + '&FinanceYearID=' + fID;
        if (id == -1) {
            var entryID = getDesiredEntryTaskID();
            if (entryID == -1) {
                displayError(Lang.WorkflowM2);
                return false;
            }
            _url += '&view=browsenew&ft=' + mFactType + '&DesiredEntryTaskID=' + entryID;
        }
        _url += appendTime();
        showPopup(_url, showRefuseCallback, btns, null, successCallback, true);
    }
    function showReturn(id, successCallback, fID) {
        var btns;
        if (id == -1)
            btns = getDefaultButtons(false);
        else
            btns = getTaskButtons(WorkflowTypeEnum.Purchase, id);
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=Returns&qd=return&id=' + id + '&FinanceYearID=' + fID;
        if (id == -1) {
            var entryID = getDesiredEntryTaskID();
            if (entryID == -1) {
                displayError(Lang.WorkflowM2);
                return false;
            }
            _url += '&view=browsenew&ft=' + mFactType + '&DesiredEntryTaskID=' + entryID;
        }
        _url += appendTime();
        showPopup(_url, showReturnCallback, btns, null, successCallback, true);
    }
    function showStoreDraftCallback(v, m, f) {
        if (v == 'Print')
            return false;
        if (v == 'Cancel') {
            if (mFactType == WorkflowTypeEnum.StoreDraft) {
                // maybe the Store is changed, so we need to show groups and members again
                showGroups();
                showMembers();
            }
            showFactor();
            return true;
        }
        if (!checkProgramStatus())
            return false;

        _Context.priceDate = f.FactorDate;
        if (v != 'OK' && v != 'New') {
            var valid = validateInputs('#detailForm', f);
            if (!valid)
                return false;
            generalMastercallback('OK', m, f, 'StoreDraft', 'storedraftupdate', 'StoreDraftItems'
                , 'storedraftitemupdate', 'storedraftiteminsert', 'storedraftitemdelete', 'StoreServiceID', null, showStoreDraft, true);
            if (!_Context.success)
                return;
            var more = '&RelatedID=' + f.MasterID + '&NextTaskID=' + v + '&UserID=' + _Context.userID + '&Type=' + WorkflowTypeEnum.StoreDraft;
            if (runQuery('dotransition', 'TaskImpl', more)) {
                showFactors();
                setCurrentFact(_Context.resultID);
                showFactor();
                return true;
            }
            return false;
        }

        if (!checkWorkflowStatus(f))
            return false;

        //var isNew = _Context.id == -1;
        var qry = (_Context.id == -1) ? 'storedraftinsert' : 'storedraftupdate';
        var res = newMastercallback(v, m, f, 'StoreDraft', qry, 'StoreDraftItems'
            , 'storedraftitemupdate', 'storedraftiteminsert', 'storedraftitemdelete', 'StoreServiceID', null, showStoreDraft, true);

        showFactors();
        setCurrentFact(_Context.resultID);
        showFactor();
        if (v == 'OK') {
            $('.del').show();
            $('#shopingcartdiv').show();
        }
        return res;
    }
    function showPurchaseCallback(v, m, f) {
        if (v == 'Print')
            return false;
        if (v == 'Cancel') {
            showFactor();
            return true;
        }
        if (!checkProgramStatus())
            return false;

        _Context.priceDate = f.FactorDate;
        if (v != 'OK' && v != 'New') {
            var valid = validateInputs('#detailForm', f);
            if (!valid)
                return false;
            generalMastercallback('OK', m, f, 'Purchase', 'purchaseupdate', 'PurchaseItems'
                , 'purchaseitemupdate', 'purchaseiteminsert', 'purchaseitemdelete', 'StoreServiceID', null, showPurchase, true);
            if (!_Context.success)
                return;
            var more = '&RelatedID=' + f.MasterID + '&NextTaskID=' + v + '&UserID=' + _Context.userID + '&Type=' + WorkflowTypeEnum.Purchase;
            if (runQuery('dotransition', 'TaskImpl', more)) {
                showFactors();
                setCurrentFact(_Context.resultID);
                showFactor();
                return true;
            }
            return false;
        }

        if (!checkWorkflowStatus(f))
            return false;

        var qry = (_Context.id == -1) ? 'purchaseinsert' : 'purchaseupdate';
        var res = newMastercallback(v, m, f, 'Purchase', qry, 'PurchaseItems'
            , 'purchaseitemupdate', 'purchaseiteminsert', 'purchaseitemdelete', 'StoreServiceID', null, showPurchase, true);
        showFactors();
        setCurrentFact(_Context.resultID);
        showFactor();
        return res;
    }
    function showReturnCallback(v, m, f) {
        if (v == 'Print')
            return false;
        if (v == 'Cancel') {
            showFactor();
            return true;
        }
        if (!checkProgramStatus())
            return false;

        _Context.priceDate = f.ReturnDate;
        if (v != 'OK' && v != 'New') {
            var valid = validateInputs('#detailForm', f);
            if (!valid)
                return false;
            generalMastercallback('OK', m, f, 'Returns', 'returnupdate', 'ReturnItem'
                , 'returnitemupdate', 'returniteminsert', 'returnitemdelete', 'StoreServiceID', null, showReturn, true);
            if (!_Context.success)
                return;
            var more = '&RelatedID=' + f.MasterID + '&NextTaskID=' + v + '&UserID=' + _Context.userID + '&Type=' + WorkflowTypeEnum.Return;
            if (runQuery('dotransition', 'TaskImpl', more)) {
                showFactors();
                setCurrentFact(_Context.resultID);
                showFactor();
                return true;
            }
            return false;
        }

        if (!checkWorkflowStatus(f))
            return false;

        var qry = (_Context.id == -1) ? 'returninsert' : 'returnupdate';
        var res = newMastercallback(v, m, f, 'Returns', qry, 'ReturnItem'
            , 'returnitemupdate', 'returniteminsert', 'returnitemdelete', 'StoreServiceID', null, showReturn, true);
        showFactors();
        setCurrentFact(_Context.resultID);
        showFactor();
        return res;
    }
    function showRefuseCallback(v, m, f) {
        if (v == 'Print')
            return false;
        if (v == 'Cancel') {
            showFactor();
            return true;
        }
        if (!checkProgramStatus())
            return false;

        _Context.priceDate = f.RefuseDate;
        if (v != 'OK' && v != 'New') {
            var valid = validateInputs('#detailForm', f);
            if (!valid)
                return false;
            generalMastercallback('OK', m, f, 'Refuse', 'refuseupdate', 'RefuseItem'
                , 'refuseitemupdate', 'refuseiteminsert', 'refuseitemdelete', 'StoreServiceID', null, showRefuse, true);
            if (!_Context.success)
                return;
            var more = '&RelatedID=' + f.MasterID + '&NextTaskID=' + v + '&UserID=' + _Context.userID + '&Type=' + WorkflowTypeEnum.Refuse;
            if (runQuery('dotransition', 'TaskImpl', more)) {
                showFactors();
                setCurrentFact(_Context.resultID);
                showFactor();
                return true;
            }
            return false;
        }

        if (!checkWorkflowStatus(f))
            return false;

        var qry = (_Context.id == -1) ? 'refuseinsert' : 'refuseupdate';
        var res = newMastercallback(v, m, f, 'Refuse', qry, 'RefuseItem'
            , 'refuseitemupdate', 'refuseiteminsert', 'refuseitemdelete', 'StoreServiceID', null, showRefuse, true);
        showFactors();
        setCurrentFact(_Context.resultID);
        showFactor();
        return res;
    }

    $('#Clear').click(function () {
        $('#TheID').val('');
        $('#ThePrice').val('');
        $('#TheName').val('');
    });
    $('#Add').click(function () {
        var curID = getCurID();
        if (!hasValuePositive(curID))
            return;
        var id = toInt($('#TheID').val(), -1);
        var price = toFloat($('#ThePrice').val(), 0);
        var amount = $('#amount').val();
        if (mFactType == WorkflowTypeEnum.StoreDraft) {
            if (runQuery('addtocurstoredraft', 'StoreDraftItems', '&StoreServiceID=' + id + '&StoreDraftID=' + curID
                + '&DeliveredAmount=' + amount + '&Price=' + price))
                showFactor();
        }
        else if (mFactType == WorkflowTypeEnum.Purchase) {
            if (runQuery('addtocurpurchase', 'PurchaseItems', '&StoreServiceID=' + id + '&PurchaseID=' + curID
                + '&Amount=' + amount + '&Price=' + price))
                showFactor();
        }
        else if (mFactType == WorkflowTypeEnum.Return) {
            if (runQuery('addtocurreturn', 'ReturnItem', '&StoreServiceID=' + id + '&ReturnID=' + curID
                + '&Num=' + amount + '&Price=' + price))
                showFactor();
        }
        else if (mFactType == WorkflowTypeEnum.Refuse) {
            if (runQuery('addtocurrefuse', 'RefuseItem', '&StoreServiceID=' + id + '&RefuseID=' + curID
                + '&Num=' + amount + '&Price=' + price))
                showFactor();
        }
        return false;
    });

    //$('#TheName').blur(function () {
    //    var code = $('#TheName').val();
    //    if (code.length > 0 && $('#TheID').val() == '') {
    //        // it is a barcode
    //        var _autourl = 'get.aspx?op=Auto&table=Service&qd=groupmembersauto&GroupingID=' + mGroupID + '&FactType=' + mFactType + '&CurID=' + getCurID() + appendTime();
    //        var row = getServerValues(_autourl + "&q=" + name);
    //        if (row && row.length && row.length > 2) {
    //            $('#TheID').val(row[0]);
    //            $('#TheName').val(row[2]);
    //            $('#ThePrice').val(row[3]);
    //        }

    //    }
    //});

    function setCurrentFact(val) {
        $('#curFacts').val(val);
        triggerChosenUpdate("#curFacts");
    }

    function clickSaveButton() {
        $("button[value=OK]").click();
        $('#shopingcartdiv').show();
    }

    setClassSales();
}