﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    93-01-18: file created
*/
function view_role() {
    var firstID = -1;
    function createStructure() {
        var divs = "", lis = "";
        //if (_Context.isAccountingVer) {
        //    debugger;
        //    divs = "<div id='tabAccess' class='tab_content hiddentab hidden'><div style='height:400px;overflow:scroll'><div class='StoreAccess' style='width:200px;float:right;'></div></div></div>";
        //    //divs = "<div id='tab' class='tab_content'><div class='yscroll padding-10 compact-panel' style='height: 400px;'></div></div>";
        //    lis += "<li class='hidden'><a href='#tabAccess'>" + Lang.Accesses + "</a></li>";
        //}
        //else {
        //    divs = "<div id='tab' class='tab_content hiddentab hidden'><div style='height:500px;overflow:scroll'><div class='WardAccess' style='width:200px;float:right;'></div><div class='StoreAccess' style='width:200px;float:right;'></div><div class='BloodBankAccess' style='width:200px;float:right;'></div></div></div>";
        //    lis += "<li class='hidden'><a href='#tab'>" + Lang.Links + "</a></li>";
        //}

        var subsystems = null, buttons = null, jsonNodes;
        $.ajax({
            url: "get.aspx?op=GetXml&qd=rolestructure",
            type: 'GET',
            dataType: 'xml',
            async: false,
            success: function (xmlDoc) {
                var xNode = (xmlDoc.firstChild.nextSibling) ? xmlDoc.firstChild.nextSibling : xmlDoc.firstChild;
                window.subsystems = subsystems = xNode.childNodes[0];
                window.buttons = buttons = xNode.childNodes[1];
                jsonNodes = ConvertXMLToJsonTree(xNode);
            },
            error: ajaxError
        });
        // Create Tabs
        if (subsystems)
            $(subsystems).find("R").each(function () {
                var node = $(this);
                if (node.attr("TitleEn") == 'Links' || node.attr("TitleEn") == 'general') {
                    divs += "<div id='tab" + node.attr("ID") + "' class='tab_content shoptab'><div class='yscroll padding-10 compact-panel' style='height: 420px;'></div></div>";
                    lis += "<li><a href='#tab" + node.attr("ID") + "'>" + node.attr("TitleFa") + "</a></li>";
                    if (firstID == -1)
                        firstID = node.attr("ID");
                } else {
                    divs += "<div id='tab" + node.attr("ID") + "' class='tab_content hiddentab hidden'><div class='yscroll padding-10 compact-panel' style='height: 420px;'></div></div>";
                    lis += "<li class='hidden'><a href='#tab" + node.attr("ID") + "'>" + node.attr("TitleFa") + "</a></li>";
                    if (firstID == -1)
                        firstID = node.attr("ID");
                }
            });
        $('div.tab_container').html(divs);
        $('ul.tabs').html(lis);
        
        if (buttons) {
            $(jsonNodes).each(function() {
                var parentItem = this,
                    $parentItem = $(this);
                $(parentItem.items).each(function() {
                    var tab = '#tab' + this.SubsystemID + " div.yscroll";
                    $(tab).append(CompileHB("access-list-form", this));
                });
            });
        }


        /*
        var wardtable = ""; var storetable = ""; var bloodbanktable = "";
        var _autourl = 'get.aspx?op=Auto&table=Ward&qd=wards';
        var res = getRowtable(_autourl);
        var i = 0;
        wardtable += "<input type='checkbox' id='ward' name='ward'/><lable><b>" + Lang.Wards + "</b></lable><br/>";
        while (i < res.length - 1) {
            wardtable += "<input type='checkbox' id='ward_" + res[i][0] + "' name='ward_" + res[i][0] + "'/><lable>" + res[i][1] + "</lable><br/>";
            i++;
        }
        $('div.WardAccess').html(wardtable);*/

        // -------------------------------
        // Stores
        var storetable = ""; 
        var _autourl1 = 'get.aspx?op=Auto&table=Stores&qd=storeaccess';
        var res1 = getRowtable(_autourl1);
        var j = 0;
        storetable += "<input type='checkbox' id='store' name='store'/><lable><b>" + Lang.Stores + "</b></lable><br/>";
        while (j < res1.length - 1) {
            storetable += "<input type='checkbox' id='store_" + res1[j][0] + "' name='store_" + res1[j][0] + "'/><lable>" + res1[j][1] + "</lable><br/>";
            j++;
        }

        // -------------------------------
        // Bankaccounts
        var bankaccounttable = "";
        var _autourl1 = 'get.aspx?op=Auto&table=BankAccount&qd=bankaccountaccess';
        var res1 = getRowtable(_autourl1);
        var j = 0;
        bankaccounttable += "<input type='checkbox' id='bankaccount' name='bankaccount'/><lable><b>" + Lang.Banks + "</b></lable><br/>";
        while (j < res1.length - 1) {
            bankaccounttable += "<input type='checkbox' id='bankaccount_" + res1[j][0] + "' name='bankaccount_" + res1[j][0] + "'/><lable>" + res1[j][1] + "</lable><br/>";
            j++;
        }

        // -------------------------------
        // Groups
        var grouptable = "";
        var _autourl1 = 'get.aspx?op=Auto&table=Grouping&qd=persongroupaccess';
        var res1 = getRowtable(_autourl1);
        var j = 0;
        grouptable += "<input type='checkbox' id='grouping' name='grouping'/><lable><b>" + Lang.Grouping + "</b></lable><br/>";
        while (j < res1.length - 1) {
            grouptable += "<input type='checkbox' id='pg_" + res1[j][0] + "' name='pg_" + res1[j][0] + "'/><lable>" + res1[j][1] + "</lable><br/>";
            j++;
        }
        $('div.StoreAccess').html(storetable + bankaccounttable + grouptable);
    }

    function checkedAllHidden() {
        $('.allContainer .hiddentab').find(':checkbox').attr("checked", 'checked');
    }

    // ------------------------------
    // startup code
    // ------------------------------
    setChecked("#OnlyCurDay");

    //fillTabs();
    createStructure();
    tabify();

    $('a[href="#tab' + firstID + '"]').click();

    //alert('@SectionNames');
    var wards = $("#WardNames").val();
    if (wards)
        if (wards.length > 0)
            $("#WardNames").val(wards + ', ');
    var _autourl = 'get.aspx?op=Auto&table=Ward&qd=idnameauto' + appendTime();
    $("#WardNames").autocomplete(_autourl, {
        width: 300,
        minChars: 0,
        multiple: true,
        mustMatch: true,
        autoFill: true,
        matchContains: true,
        formatItem: function (data, i, n, value) {
            return data[1];
        },
        formatResult: function (data, value) {
            return data[1];
        }
    });

    var access = ';' + $('#Access').val() + ';';
    $(':checkbox').each(function (index, value) {
        var id = ';' + $(this).attr("id") + ';';
        if (id.length > 4)
            if (access.indexOf(id) >= 0)
                $(this).attr("checked", 'checked');
            else
                $(this).attr("checked", null);
        //alert(index + ': ' + $(this).text());
    });
    //
    $('.all').click(function () {
        var checked = this.checked;
        if (checked)
            $('.allContainer .shoptab').find(':checkbox').attr("checked", 'checked');
        else
            $('.allContainer .shoptab').find(':checkbox').attr("checked", null);
        this.checked = checked;
    });
    $('.sub').click(function () {
        var checked = this.checked;
        if (checked)
            $('.allContainer .shoptab').find(':checkbox[id^="' + $(this).attr('id') + '"]').attr("checked", 'checked');
        else
            $('.allContainer .shoptab').find(':checkbox[id^="' + $(this).attr('id') + '"]').attr("checked", null);
        this.checked = checked;
    });
    $('.sub').each(function () {
        var allChecked = true;
        var id = $(this).attr('id');
        $(this).parentsUntil('.shoptab').find(':checkbox[id^="' + id + '"]').each(function () {
            if ($(this).attr('id') != id) {
                if (!$(this).attr("checked")) allChecked = false;
            }
        });
        if (allChecked)
            $(this).attr("checked", 'checked');
        else
            $(this).attr("checked", null);
    });
    function updateAll(){
        var _allChecked = true;
        $('.allContainer').find(':checkbox').each(function () {
            if (!$(this).attr("checked")) _allChecked = false;
        });
        if (_allChecked)
            $('.all').attr("checked", 'checked');
        else
            $('.all').attr("checked", null);
    }
    updateAll();
    $('.allContainer').find(':checkbox').click(updateAll);
    $(':checkbox').click(function () {
        var acc = ';';
        $(':checkbox').each(function (index, value) {
            if ($(this).hasClass('all') || $(this).hasClass('sub'))
                return;
            var id = $(this).attr("id");
            if (id && id.length > 2)
                if (id.indexOf('__tab') < 0) {
                    var checked = this.checked;
                    if (checked)
                        acc += id + ';';
                }
        });
        $('#Access').val(acc);
        //alert(acc);
    });

    function FilterByAttr(nodes, attr, value) {
        return $(nodes).filter(function(index, element) {
            return ($(element).attr(attr) != null && $(element).attr(attr) == value);
        });
    }

    function Pluck(nodes, attr) {
        var items = [];

        $(nodes).each(function() {
            if (items.indexOf($(this).attr(attr)) == -1) {
                items.push($(this).attr(attr));
            }
        });

        return items;
    }
    if (_Context.id == -1) {
        checkedAllHidden();
    }

    function ConvertXMLToJsonTree(xml) {
        var parentNodes = $(xml.childNodes[0]).find("R"),
            childNodes = $(xml.childNodes[1]).find("R"),
            json = [],
            nodeItems,
            formNodes,
            formIds;

        $(parentNodes).each(function() {
            var node = NodeToJson(this);
            node.items = [];

            nodeItems = FilterByAttr(childNodes, "SubsystemID", node.ID);
            formIds = Pluck(nodeItems, "Ename");
            $(formIds).each(function () {
                var myItems = {};
                myItems.items = $.map(FilterByAttr(nodeItems, "Ename", this), function(obj, index) {
                    return NodeToJson(obj);
                });
                if (myItems.items && myItems.items.length > 0) {
                    myItems.id = myItems.items[0].Ename;
                    myItems.title = myItems.items[0].Pname;
                    myItems.SubsystemID = myItems.items[0].SubsystemID;
                } else {
                    myItems.id = "";
                    myItems.title = "";
                    myItems.SubsystemID = "";
                }
                node.items.push(myItems);
            });

            json.push(node);
        });

        return json;
    }
}