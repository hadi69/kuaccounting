﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-10-19: file created
*/
function view_guaranteecheckss() {
    function doCommand(com, grid) {
        //if (com == Lang.ShowReceived) //
        //{
        //    if ($('.showReceived').hasClass('glyphicon-ok')) {
        //        $('.showReceived').removeClass('glyphicon glyphicon-ok');
        //    } else {
        //        $('.showReceived').toggleClass('glyphicon glyphicon-ok');
        //    }
        //    var more = '__PrOgrAmconD__ AND Status NOT IN(1,5)';
        //    if ($('.showReceived').hasClass('glyphicon-ok')) {
        //        more = '__PrOgrAmconD__';
        //    }
        //    var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=checksshop' + '&more=' + more + appendTime();
        //    $("#flexi").flexOptions({ url: _url });
        //    $('#fRefresh').click();
        //    return;
        //}
        generalCommand(com, grid, show, Lang.AutoChecks, "ID;CheckType", "Status=0^CheckStatusNew^1^CheckStatusReceived^2^CheckStatusRefused^3^CheckStatusSleep^4^CheckStatusExpend^5^CheckStatusSleepReceive");
    }
    function showSumAmount() {
        var val = getServerValueFiltered("guarantichecksumamount", "PayCheck");
        if (!val)
            val = 0;
        $('#SumAmount').html(numberWithCommas(toMonetary(val, _Context.today)));
    }
    function run() {
        var more = '__PrOgrAmconD__';
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=guaranteechecks' + '&more=' + more + appendTime();
        //alert(_url);
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'center', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: true, css_class: 'row-cell' },
                    { display: '', name: 'CheckType', sortable: true, hide: true },
                    { display: Lang.CheckStatus, name: 'Kind', sortable: true, css_class: 'width-80 align-center' },
                    { display: Lang.BehindNum, name: 'No', sortable: true, css_class: 'width-80 align-center' },
                    { display: Lang.Bank, name: 'BankName', sortable: true, css_class: 'width-100 align-center', hide: true },
                    { display: Lang.ReceiverBankName, name: 'ReceiverBankName', sortable: true, css_class: 'width-100 align-center', hide: true },
                    { display: Lang.OwnerCheck, name: 'PersonnelName', sortable: true, css_class: 'width-120 align-center' },
                    { display: Lang.Reciever, name: 'ReceiverName', sortable: true, css_class: 'width-80 align-center' },
                    { display: Lang.DueDate, name: 'DueDate', sortable: true, process: showDate, css_class: 'date-cell', hide: true },
                    { display: Lang.Amount, name: 'Amount', sortable: true, process: showMonetary, css_class: 'width-70 align-center', footer: 'sum' },
                    { display: Lang.Status, name: 'Status', sortable: true, process: displayStatus, css_class: 'width-60 align-center' },
                    { display: Lang.Serial, name: 'Serial', sortable: true, css_class: 'width-80 align-center' },
                    { display: Lang.Visitor, name: 'VisitorName', sortable: true, css_class: 'width-100 align-center', hide: true },
                    { display: Lang.BankAccountCheckName, name: 'BankAccountCheckName', hide: true },
                    { display: Lang.Branch, name: 'Branch', hide: true },
                    { display: Lang.AccountNo, name: 'AccountNo', hide: true },
                    { display: Lang.IssueDate, name: 'IssueDate', sortable: true, process: showDate, css_class: 'date-cell', hide: true },
                    //{ display: Lang.ColEdit, name: 'Edit', width: 40, sortable: false, align: 'left', process: function (celDiv, id) { editLink(celDiv, id, show) } },
                    { display: Lang.ColOperation, name: 'Edit', sortable: false, hide: hideColumn("bank", "checksshops", "edit"), process: myEditLink, css_class: 'action-cell' },
                    { display: Lang.ColDel, name: 'Del', sortable: false, hide: hideColumn("bank", "checksshops", "delete"), process: myDelLink, css_class: 'action-cell' }
                ],
                buttons:
                [
                   { name: Lang.GBNew, bclass: 'new', onpress: doCommand, onaccess: hasAccess("bank", "guaranteechecks", "new") },
                   { name: Lang.GBEdit, bclass: 'edit', onpress: doCommand, onaccess: hasAccess("bank", "guaranteechecks", "edit") },
                   { name: Lang.GBPrint, bclass: 'print', onpress: doCommand, onaccess: hasAccess("bank", "guaranteechecks", "print") },
                   { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand, onaccess: hasAccess("bank", "guaranteechecks", "excel") },
                ],
                searchitems: null,
                sortname: "DueDate",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,

                height: gridHeight,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id, row);
                },
                onSuccess: function (grid) {
                    restoreSelectedRows('#flexi');
                }
            }
            );
        showSumAmount();
        _Context.filterPostBack = showSumAmount;

    }
    function myEditLink(celDiv, id) {

        $(celDiv).html('<img src="img/edit.gif" />');
        $(celDiv).click(function (event) {
            var tr = $(celDiv).closest('tr');
            window.tr = tr;
            $('#flexi tr').removeClass('trSelected');
            tr.addClass('trSelected');
            var status = getStatus(getColClass(tr, 12), 2);
            var TypeOfCheck = getColContent($(celDiv).closest('tr'), 3);
            if (TypeOfCheck == 'checks') {
                showMenu({
                    parent: this, width: '100px',
                    cmds: [
                              [Lang.ColEdit, 'Edit']
                            , [Lang.ColGuarantee, status == '7' ? 'giveBackCheck' : ''] // پس دادن گارانتی
                          ],

                    callback: function (cmd) {
                        if (cmd == 'Edit') {
                            show(id, celDiv);
                        }
                        else if (cmd == 'giveBackCheck') {
                            doConfirm(function yes() { showGiveBack(id, 'Checks', 'updatecheckshop'); }, Lang.ChangeCheckStatus1 + '"' + Lang.CheckStatusGiveBack + '"' + Lang.ChangeCheckStatus2, function no() { return; });
                        }
                     }
                });
            }
            if (TypeOfCheck == 'paycheck') {

                showMenu({
                    parent: this, width: '100px',
                    cmds: [
                              [Lang.ColEdit, 'Edit']
                            , [Lang.ColGuarantee, status == '7' ? 'takeBackCheck' : ''] // پس گرفتن چک ضمانتی
                          ],
                    callback: function (cmd) {
                        if (cmd == 'Edit') {
                            show(id, celDiv);
                        }
                        else if (cmd == 'takeBackCheck')
                            doConfirm(function yes() { showPayCheckTakeBack(id, 'PayCheck', 'updatepaycheckshop'); }, Lang.ChangeCheckStatus1 + '"' + Lang.CheckStatusTakeBack + '"' + Lang.ChangeCheckStatus2, function no() { return; });
                    }
                });
            }
        });

    }
    function myDelLink(celDiv, id) {
        $(celDiv).html('<img src="img/delete.gif" />');
        $(celDiv).click(function (event) {
            if (clicksDisabled)
                return;
            disableClicks();
            event.preventDefault();

            doConfirm(function () {
                var TypeOfCheck = getColContent($(celDiv).closest('tr'), 3);
                var _url = 'set.aspx?op=Delete&table=Checks&id=' + id + appendTime();
                if (TypeOfCheck == 'paycheck')
                    _url = 'set.aspx?op=Delete&table=PayCheck&id=' + id + appendTime();
                //alert(_url);
                $.ajax({
                    url: _url,
                    type: 'GET',
                    success: function (html) {
                        message(html);
                        if (html.indexOf("Error:") >= 0 || html.indexOf('خطا:') >= 0) {
                        }
                        else
                            $("#flexi").flexReload();
                    },
                    error: ajaxError
                });
            });
        });
    }
    function displayStatus(celDiv, id) {
        var _mode = parseInt($(celDiv).html(), 12);
        displayCheckStatus(celDiv, _mode);
    }

    var payCheckType = false;
    function showcallback(v, m, f) {
        restoreSelectedRows('#flexi');
        if (v == 'OK' && !checkProgramStatus())
            return false;
        var TypeOfSelect = $('#CheckType').val();
        _Context.priceDate = f.IssueDate;

        if (TypeOfSelect == "checks") {
            // alert(TypeOfSelect);
            return newShowcallback(v, m, f, 'checkupdate', 'checkinsert', 'Checks', '#subDetailForm', show);
        }
        if (TypeOfSelect == "paycheck") {
            if (v == "Print") {
                var template = getServerValue("bankprinttemplate", "BankPrint", "&ID=" + f.ID);
                if (template && template != '')
                    return generalPrintTemplate('#subDetailForm', template);
                return generalPrint(v, m, f, '#subDetailForm');
            }
            payCheckType = true;
            // alert(TypeOfSelect);
            return newShowcallback(v, m, f, 'paycheckupdate', 'paycheckinsert', 'PayCheck', '#subDetailForm', show);
        }
    }
    function show(id, celDiv, successCallback) {
        saveTrSelected(id);
        window.celDiv = celDiv;
        var TypeOfCheck = getColContent($(celDiv).closest('tr'), 3);
        var table = _Context.table;
        var qd = _Context.query;
        _Context.table = "Checks";
        _Context.query = "checkshop";

        if (TypeOfCheck == "paycheck" || payCheckType) {
            _Context.table = "PayCheck";
            _Context.query = "paycheckShop";
            payCheckType = false;
        }
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&view=guaranteechecks&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, showcallback, getDefaultButtons(true, 'bank', "guaranteechecks"), null, null, true);
        _Context.table = table;
        _Context.query = qd;
    }
    function showGiveBack(id, table, qd) {
        saveTrSelected(id);
        updateCheckStatus(id, CheckStatusEnum.GiveBack, table, qd);
    }
    function showPayCheckTakeBack(id, table, qd) {
        saveTrSelected(id);
        updateCheckStatus(id, CheckStatusEnum.TakeBack, table, qd);
    }

    function updateCheckStatus(checkId, newStatus, table, qd) {

        // alert(table);
        // alert(qd);
        var _url = 'set.aspx?op=RunScalar&table=' + table + '&qd=' + qd + appendTime();
        var form_data = {
            id: checkId,
            status: newStatus
        };

        $.ajax({
            url: _url,
            data: form_data,
            success: function () {
                $("#flexi").flexReload();
            }
        });
    }
    function saveTrSelected(id) {
        $('.trSelected').removeClass('trSelected');
        $('#row' + id).addClass('trSelected');
        saveSelectedRows('#flexi');
        $('.trSelected').removeClass('trSelected');
    }

    // ------------------------------
    // startup code
    // ------------------------------
    dateSelect('.dateSelect');
    checkAccess(['bank,guaranteechecks']);
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.GuaranteeChecks, parentTitle: Lang.MAccounting, parentUrl: 'menu/accounting.aspx' } }));
    setClassBank();
    run();
}