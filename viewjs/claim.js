﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
95-08-06: file created
*/
function view_claim() {
    // ------------------------------
    // startup code
    // -----------------------------
    dateSelect('.dateSelect')
    autoCompleteFree('Personnel', 'personnelselectauto', '#PersonnelName', '#PersonnelID', null
            , function (data, i, n, value) {
                return formatPersonnelItem(data);
            });
    autoCompleteFree('Personnel', 'visitorauto', '#VisitorName', '#VisitorID', null
            , function (data, i, n, value) {
                return formatPersonnelItem(data);
            });
    autoCompleteFree('ServicePrice', 'servicepriceauto', '#ServicePriceName', '#ServicePriceID', null
            , null);
    /*function (data, i, n, value) {
        return formatService(data);
    }*/
    $('#TheClaim').focus();
    $('#PersonnelName').focus();
    setValue('#Type');
    setValue('#Status');
    insertValidation();
}