﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-11-01: file created
*/
function view_constdefinition() {
    $('#tableItems').smartTable();
    var cnt = 0, cnt1 = 0, mID = -1;
    function addSystemPosition(node) {
        var row = '<tr>';
        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">'
            + (cnt + 1)
            + '</td>';

        row += '<td data-th="' + Lang.headline + '">'
            + '<input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="text" id="se_' + cnt + '" name="ConstItemTitle" value="' + getNodeVal(node, "ConstItemTitle") + '" class="ConstItemTitle form-control input-sm" />'
            + '<input type="hidden" name="ConstItemID" value="' + getNodeVal(node, "ConstItemID") + '" />'
            + '</td>';

        row += '<td data-th="' + Lang.Name + '">'
            + '<input type="text" id="n_' + cnt + '" name="ConstItemName" value="' + getNodeVal(node, "ConstItemName") + '" class="ConstItemName form-control input-sm" />'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a class="btn btn-danger btn-xs btn-block btn-remove removeRole" id="r_' + cnt + '">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }
    //----------end addrow

    function RemoveRoleRow($who) {
        var id = $who.attr('id'); // r_1
        id = id.substring(2); // r_1  -->  1
        //  alert(id);
        $('#se_' + id).val(""); // 
        $('#se_' + id).next().val(-1); // 
        $who.parent().parent().remove();

    }

    function autoPostPack(event, data, formatted) {
        $(event.target).next().val(data[0]); // ID
        var id = $(event.target).attr('id');
        id = id.substring(3);
        $('#n_' + id).val(data[2]);
    }
    function bindAdds() {
        $('.addRole').click(function () {
            var table = $('#tableItems');
            var tableID = table.attr("id");
            $('tr:last', table).after(addSystemPosition(null));
            var lastRow = $('tr:last', table);
            autoComplete('ConstItems', 'constitemauto', "#tableItems tr:last .ConstItemTitle", "#justfurfun"
           , autoPostPack
           , function (data, i, n, value) {
               return data[1];
           });
        });
        
    }

    function bindRemoves() {
        //-----------
        $('.removeRole').click(function () {
            RemoveRoleRow($(this));
        });
    }

    // -------------------------
    // Startup code
    // -------------------------
    mID = toInt($("#MasterID").val(), -1);
    setValue("#State");
    fillTable("get.aspx?op=GetXml&qd=constdefinitionitems&ConstDefinitionID=" + mID, '#tableItems', addSystemPosition);
    doMore();

    function doMore() {
        function autoPostPack(event, data, formatted) {
            $(event.target).next().val(data[0]); // ID
            var id = $(event.target).attr('id');
            id = id.substring(3);
            $('#n_' + id).val(data[2]);
        }
        autoComplete('ConstItems', 'constitemauto', "#tableItems .ConstItemTitle", "#ConstItemID"
       , autoPostPack
       , function (data, i, n, value) {
           return data[1];
       });

        $('.addRole').click(function () {
            var table = $('#tableItems');
            $('tr:last', table).after(addSystemPosition(null));
            var lastRow = $('tr:last', table);
            autoComplete('ConstItems', 'constitemauto', "#tableItems tr:last .ConstItemTitle", "#ConstItemID"
       , autoPostPack
       , function (data, i, n, value) {
           return data[1];
       });
            $('.removeRole', lastRow).click(function () {
                RemoveRoleRow($(this));
            });
        });

        $('.removeRole').click(function () {
            RemoveRoleRow($(this));
        });
    }

    insertValidation();
}