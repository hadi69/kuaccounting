﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
92-10-19: showPopup used
92-10-16: file created
*/
function view_benefits() {
    function doCommand(com, grid) {
        if (_Context.table.toLowerCase() == 'program')
            if (com == Lang.GBNew || com == Lang.GBDel) {
                if (_Context.fyStart == '') {
                    alert(Lang.NoCurYear);
                    return;
                }
            }
        generalCommand(com, grid, show);
    }
    function run() {
        var terminology = getTerminology(_Context.table);
        if (terminology)
            $('#term').html("<a href='help/terminology.aspx' target=_blank title='" + Lang.HasTerminology + "'>" + terminology + "</a>");
        var more = 'RateID=' + $.urlParam('id');
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + '&more=' + more + appendTime();
        //alert(_url);
        $("#flexi").flexigrid
                (
                {
                    url: _url,
                    dataType: 'xml',
                    colModel:
                    [
                        { name: 'ID', sortable: false, hide: true },
                        { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                        { name: 'RateID', sortable: false, hide: true },
                        { display: Lang.HMaskan, name: 'HMaskan', sortable: true, css_class: 'width-200' },
                        { display: Lang.HOwlad, name: 'HOwlad', sortable: true, css_class: 'width-100 align-center' },
                        { display: Lang.Bon, name: 'Bon', sortable: true, css_class: 'width-80 align-center' },
                        { display: Lang.MaritalStatus, name: 'MaritalStatusName', sortable: true, css_class: 'width-80 align-center' },
                        { display: Lang.ColEdit, name: 'Edit', sortable: false, hide: hideColumn("manager", "enums", "edit"), process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' },
                        { display: Lang.ColDel, name: 'Del', sortable: false, hide: hideColumn("manager", "enums", "delete"), process: delLink, css_class: 'action-cell' }
                    ],
                    buttons:
                    [
                        { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                        { name: Lang.GBEdit, bclass: 'edit', onpress: doCommand },
                        { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                        { separator: true },
                        { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                        { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                    ],
                    searchitems: null,
                    sortname: "ID",
                    sortorder: "asc",
                    usepager: true,
                    //title: _Context.title,
                    useRp: true,
                    rp: gridRp,
                    showTableToggleBtn: _options.gridToggle,
                    width: _options.gridWidth,

                    height: _options.gridHeight, resizable: _options.gridResizable,
                    singleSelect: true,
                    onDoubleClick: function (row, grid, options) {
                        var id = row.id.substring(3);
                        show(id);
                    },
                    onSuccess: function (grid) {
                        restoreSelectedRows('#flexi');
                    }
                }
                );
    }
    function isProgramDependent() {
        return (_Context.table.toLowerCase() == 'TopicGroup'.toLowerCase());
    }
    function showcallback(v, m, f) {
        return newShowcallback(v, m, f, 'benefitupdate', 'benefitinsert', _Context.table, null, show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        saveSelectedRows('#flexi');
        var RID = $.urlParam('id');
        var m = "&RateID=" + RID;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + '&more=' + m + appendTime();
        showPopup(_url, showcallback, null, null, function () {
            jscolor.init();
            if (successCallback)
                successCallback();
        });
    }
    function setQuickMenu() {
        setClassSalary();
    }

    // ------------------------------
    // startup code
    // ------------------------------
    if (_Context.table.toLowerCase() == 'program')
        checkAccess(['mainmenu,program']);
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.Benefits, parentTitle: Lang.MSalary, parentUrl: 'menu/salary.aspx' } }));

    run();
}