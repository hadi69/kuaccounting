﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
93-02-13: file created
*/
function view_contacts() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        $("#flexi").flexigrid
                (
                {
                    url: _url,
                    dataType: 'xml',
                    colModel:
                    [
                        { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                        { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                        { display: Lang.Name, name: 'Name', sortable: true, css_class: 'width-120' },
                        { display: Lang.Surname, name: 'Surname', sortable: true, css_class: 'width-120' },
                        { display: Lang.Mobile, name: 'Mobile', sortable: true, css_class: 'width-120 align-center', process: showColor },
                        { display: Lang.Tel, name: 'Tel', sortable: true, css_class: 'width-120 align-center', process: showColor },
                        { display: Lang.Address, name: 'Address', sortable: true, css_class: '' },
                        { display: Lang.ColEdit, name: 'Edit', sortable: false, css_class: 'action-cell', hide: hideColumn("manager", "enums", "edit"), process: function (celDiv, id) { editLink(celDiv, id, show); } },
                        { display: Lang.ColDel, name: 'Del', sortable: false, css_class: 'action-cell', hide: hideColumn("manager", "enums", "delete"), process: delLink }
                    ],
                    buttons:
                    [
                        { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                        { name: Lang.GBEdit, bclass: 'edit', onpress: doCommand },
                        { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                        { separator: true },
                        { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                        { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                    ],
                    searchitems: null,
                    sortname: "Name",
                    sortorder: "asc",
                    usepager: true,
                    useRp: true,
                    rp: gridRp,
                    showTableToggleBtn: _options.gridToggle,
                    width: _options.gridWidth,
                    height: _options.gridHeight, resizable: _options.gridResizable,
                    singleSelect: true,
                    onDoubleClick: function (row, grid, options) {
                        var id = row.id.substring(3);
                        show(id);
                    },
                    onSuccess: function (grid) {
                        restoreSelectedRows('#flexi');
                    }
                }
                );
    }
    function showcallback(v, m, f) {
        return newShowcallback(v, m, f, 'contactupdate', 'contactinsert', _Context.table, null, show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        saveSelectedRows('#flexi');
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    //checkAccess(['mainmenu,program']);
    //setClassAccounting();
    //$("form > div").not(".main").hide();
    $('.navbar').hide();
    $('.sidebar').hide();
    $('.footer').hide();
    $('body').css('background-image', 'none');
    run();
}