﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-11-23: file created
*/
function view_companyinfos() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + "&more=" + encodeURIComponent("Type=6") + appendTime();
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: true, align: 'center', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeileNummer', sortable: true, css_class: 'row-cell' },
                    { display: Lang.Title, name: 'Name', sortable: true, css_class: 'width-200' },
                    { display: Lang.headline, name: 'Display', sortable: true, css_class: 'width-200' },
                    { display: Lang.Value, name: 'Value', sortable: true, hide: true },
                    { display: Lang.Value, name: 'DisplayValue', sortable: true },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' }
                    //,                    { display: Lang.ColDel, name: 'Del', width: 40, sortable: false, align: 'left', process: delLink }
                ],
                buttons:
                [
                    //{ name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                    //{ name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                    { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand },
                    { separator: true }
                ],
                sortname: "Display",
                sortorder: "asc",
                usepager: true,
                //title: _Context.title,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,

                height: gridHeight, resizable: gridResizable,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                }
            }
            );
    }
    function showcallback(v, m, f) {
        return generalShowcallback(v, m, f, 'configupdate', 'configinsert', "Config", null, show);
    }

    function show(id) {
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id +'&view=companyinfo'+ appendTime();
        showPopup(_url, showcallback, getButtons());
    }

    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['salary,companyinfo']);
    $("#bar").prepend(breadcrumb({ title: _Context.title, grandParent: { title: Lang.MSalary, url: 'menu/salary.aspx' }, parent: { title: Lang.SalaryDefinition, url: 'menu/salarydefinition.aspx' } }));
    setClassSalary();
    run();
}