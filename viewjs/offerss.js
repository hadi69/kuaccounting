﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
    92-10-16: file created
*/
function view_offerss() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        $("#flexi").flexigrid
                (
                {
                    url: _url,
                    dataType: 'xml',
                    colModel:
                    [
                        { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'center', hide: true },
                        { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell'},
                        { display: Lang.No, name: 'No', sortable: true, css_class: 'width-70 align-center' },
                        { display: Lang.OfferType, name: 'OfferType', sortable: true, process: displayType, css_class: 'width-80 align-center' },
                        { display: Lang.CommodityName, name: 'ServiceName', sortable: true},
                        { display: Lang.GroupingName, name: 'GroupingName', sortable: true, css_class: 'width-120 align-center'},
                        { display: Lang.StartDate, name: 'StartDate', sortable: true, css_class: 'date-cell' },
                        { display: Lang.EndDate, name: 'EndDate', sortable: true, css_class: 'date-cell'},
                        { display: Lang.Attainer, name: 'Attainer', sortable: true, css_class: 'width-120 align-center'},
                        { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' },
                        { display: Lang.OfferSteps, name: 'OfferSteps', sortable: false, process: offerstepsDefinition, css_class: 'width-100 align-center'},
                        { display: Lang.OfferConditions, name: 'OfferConditions', sortable: false, process: offerconditionsDefinition, css_class: 'width-90 align-center' }
                    ],
                    buttons:
                    [
                        { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                        { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                        { separator: true },
                        { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                    ],
                    searchitems: null,
                    sortname: "No",
                    sortorder: "asc",
                    usepager: true,
                    useRp: true,
                    rp: gridRp,
                    showTableToggleBtn: gridToggle,
                    width: gridWidth,
                    height: gridHeight, resizable: gridResizable,
                    singleSelect: false,
                    onDoubleClick: function (row, grid, options) {
                        var id = row.id.substring(3);
                        show(id);
                    }
                });
    }
    function _displayWorkflowType(celDiv, id) {
        var type = toInt($(celDiv).html(), 0);
        displayWorkflowType(celDiv, type);
    }
    function displayType(celDiv, id) {
        $(celDiv).addClass('_rt');
        var _mode = parseInt($(celDiv).html(), 4);
        displayOfferType(celDiv, _mode);
    }
    function showcallback(v, m, f) {
        if ($('#GroupingID').val() == '' && $('#ServicePriceID').val() == '' && v == 'OK' && $('#OfferType').val() != 2) {
       //     $('#ServiceID').addClass('validate[required]');
            insertValidation('#subDetailForm');
            alert(Lang.OfferssJsM1);
            return 0;
        }
        return newShowcallback(v, m, f, 'offersupdate', 'offersinsert', _Context.table, null, show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['distribution,offers']);
    $("#bar").prepend(breadcrumb({pageInfo: {pageTitle: Lang.Offers, parentTitle: Lang.MDistribution, parentUrl: 'menu/distribution.aspx'} }));
    setClassDistribution();
    run();
}