﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-12-11: combined with salesinvoice
    92-10-30: file created
*/
function view_shopstoredraft() {
    makeChosen();
    $("#tableItems").smartTable();
    $("#tablePartners").smartTable();
    var pagetitle = 'SalesInvoice';
    if (!$('#WorkflowStatus').attr('v')) {
        pagetitle = 'PerSalesInvoice';
    };
    replacePrintButton({
        primary: {
            text: Lang.Print,
            href: Router.Generate("print-storedraft", { params: { ids: _Context.id, title: pagetitle }, query: { print: "true", report: 'catering' } })
                , type: 6, mode: 0, id: _Context.id
        },
        other: [
            {
                text: Lang.A4FormatWithTax,
                href: Router.Generate("print-storedraft", { params: { ids: _Context.id, title: pagetitle }, query: { report: 'simple2WithTax', print: "true" } })
                , type: 6, mode: 0, id: _Context.id
            },
            {
                text: Lang.A4Format,
                href: Router.Generate("print-storedraft", { params: { ids: _Context.id, title: pagetitle }, query: { report: 'simple', print: "true" } })
                , type: 6, mode: 0, id: _Context.id
            }
        ]
    });

    var cnt = 0, cnt1 = 0, mID = -1, mTreePathWasEmpty = true, DiscountPercent = offerDiscount = payAllowDays = '', fromTempBtn = false;
    function updateTotal() {
        var sumPrice = 0, sumAmount = 0, sumDiscount = 0, sumTax = 0, sumVat = 0;
        $('.total').each(function (index, value) {
            sumPrice += toFloat($(value).val(), 0);
        });
        $('.amount').each(function (index, value) {
            sumAmount += toFloat($(value).val(), 0);
        });
        $('.discount').each(function (index, value) {
            if ($(value).val().indexOf('%') >= 0) {
                var id = $(value).attr('id');
                id = id.substring(id.indexOf('_', 0) + 1); // d_1  -->  1
                var total = toFloat($('#t_' + id).val(), 0); // total
                sumDiscount += Math.round(toFloat($(value).val().replace('%', ''), 0) * total / 100);
            }
            else
                sumDiscount += toFloat($(value).val(), 0);
        });
        var discount = 0;
        $('.tax').each(function (index, value) {
            if ($(value).val().indexOf('%') >= 0) {
                var id = $(value).attr('id');
                id = id.substring(id.indexOf('_', 0) + 1); // d_1  -->  1
                var total = toFloat($('#t_' + id).val(), 0); // total
                if ($('#d_' + id).val().indexOf('%') >= 0) {
                    discount = toFloat($('#d_' + id).val().replace('%', ''), 0) * total / 100; // discount
                } else {
                    discount = toFloat($('#d_' + id).val(), 0);
                }
                sumTax += toFloat($(value).val().replace('%', ''), 0) * (total - discount) / 100;
            }
            else
                sumTax += toFloat($(value).val(), 0);
        });
        $('.svat').each(function (index, value) {
            if ($(value).val().indexOf('%') >= 0) {
                var id = $(value).attr('id');
                id = id.substring(id.indexOf('_', 0) + 1); // d_1  -->  1
                var total = toFloat($('#t_' + id).val(), 0); // total
                if ($('#d_' + id).val().indexOf('%') >= 0) {
                    discount = toFloat($('#d_' + id).val().replace('%', ''), 0) * total / 100; // discount
                } else {
                    discount = toFloat($('#d_' + id).val(), 0);
                }
                sumVat += toFloat($(value).val().replace('%', ''), 0) * (total - discount) / 100;
            }
            else
                sumVat += toFloat($(value).val(), 0);
        });
        //$('.salesmancommission').each(function (index, value) {
        //    var id = $(value).attr('id');
        //    id = id.substring(id.indexOf('_', 0) + 1); // d_1  -->  1
        //    var total = toFloat($('#t_' + id).val(), 0); // total
        //});
        $('#TotalPrice').val(sumPrice.toFixed(_Context.decimalPoints));
        $('#TotalAmount').val(sumAmount);
        var factorTotalDiscount = $('#FactorTotalDiscount').val();
        if (factorTotalDiscount.indexOf('%') >= 0) {
            factorTotalDiscount = Math.round(toFloat(factorTotalDiscount.replace('%', ''), 0) * toFloat($('#TotalPrice').val(), 0) / 100); // factortotaldiscount
        }
        factorTotalDiscount = toFloat(factorTotalDiscount, 0);
        $('#TotalTax').val(sumTax.toFixed(_Context.decimalPoints));
        $('#VAT').val(sumVat.toFixed(_Context.decimalPoints));
        $('#TotalDiscount').val((sumDiscount + factorTotalDiscount).toFixed(_Context.decimalPoints));
        updateFormTotal();
        updateSumProfit();
    }
    function updateFormTotal() {
        var total = toFloat($('#TotalPrice').val(), 0) - toFloat($('#TotalDiscount').val(), 0) + toFloat($('#TotalTax').val(), 0) + toFloat($('#VAT').val(), 0);
        $('#Total').val(total.toFixed(_Context.decimalPoints));
        autoSumOnComplexPayMethod();
        commaInputsSet();
        checkCredit();
    }
    function updateSumProfit() {
        var table = $("#tableItems");
        var profit = 0;
        var discount = toFloat($('#TotalDiscount').val(), 0);
        // var pursant = toFloat($('#Pursant').val(), 0);
        $('tr .storeserviceid', table).each(function () {
            var id = $(this).attr('id');
            id = id.substring(4);
            var num = toInt($('#a_' + id).val(), 0) * toInt($('#ucc_' + id).val(), 1);
            profit += ((num * toFloat($('#p_' + id).val(), 0)) - (num * toFloat($('#bp_' + id).val(), 0)));
        });
        profit = profit - discount;//+ pursant);
        $('#SumProfit').val(profit);
        commaInputsSet();
    }
    function checkCredit() {
        $('#creditmsg').html();
        var neuremain = 0;
        if (toInt($('#PayMethod').val(), -1) == 2)
            neuremain = toFloat(fromMonetary($('#Total').val(), $('#FactorDate').val()), 0);
        else if (toInt($('#PayMethod').val(), -1) == 3)
            neuremain = toFloat(fromMonetary($('#OwedPrice').val(), $('#FactorDate').val()), 0);
        var status = 0;
        if (toFloat(neuremain, 0) > 0) {
            status = getPersonCredit($('#BuyerID').val(), neuremain);
        }
        if (status == -1) {
            $('#creditmsg').html(Lang.LowCredit);//checkCredit
            return false;
        }
        return true;
    }
    function updatePrice(was) {
        var id = $(was).attr('id');
        id = id.substring(id.indexOf('_', 0) + 1); // d_1  -->  1
        var price = toFloat($('#p_' + id).val(), 0);
        var amount = toFloat($('#a_' + id).val(), 0);
        var unitCarrency = toFloat($('#ucc_' + id).val(), 1);
        $('#t_' + id).val((price * amount * unitCarrency).toFixed(_Context.decimalPoints)); // total
        updateTotal();
    }
    function checkAvail(was) {
        var id = $(was).attr('id');
        if (id && id != undefined && id != null) {
            id = id.substring(id.indexOf('_', 0) + 1); // d_1  -->  1
            if ($('#isv_' + id).val() == 0 || $('#isv_' + id).val() == 'False') {
                // don't check availablity for IsVirtual service
                var amount = toFloat($('#a_' + id).val(), 0);
                var unitCarrency = toFloat($('#ucc_' + id).val(), 1);
                if ($('#avial_' + id).val() != '') {
                    var avail = toFloat($('#avial_' + id).val(), 1);
                    if ((avail < (amount * unitCarrency)) && serviceRemainMsg == 1) {
                        if (avail < 0) {
                            avail = 0;
                        }
                        if (insertStoreDraftWithNegitiveServiceRemain != 1) {
                            //$('#a_' + id).val(0);
                            $('#se_' + id).val('');
                            $('#sid_' + id).val('');
                            $.ionSound.play("bell_ring");
                            //$('#a_' + id).blur();
                            updatePrice(was);
                            setTimeout(function () {
                                $('#se_' + id).focus();
                            },
                                20);
                            $('#a_' + id).attr('style', 'background-color:red');
                           // Growl.Error(Lang.NoEnoughSupply + '     ' + Lang.ServiceAllowedAmount + avail, 2000);
                            //alert(Lang.NoEnoughSupply + '     ' + Lang.ServiceAllowedAmount + avail);
                            return -1;
                        }
                        $('#a_' + id).attr('style', 'background-color:red');
                        //Growl.Error(Lang.NoEnoughSupply + '     ' + Lang.ServiceAllowedAmount + avail, 2000);
                        return 1;
                    }
                    $('#a_' + id).attr('style', 'background-color:white');
                }
            }
        }
        return 1;
        // colorRows();
    }

    function checkgraterbuyprice(was) {
        var id = $(was).attr('id');
        if (id && id != undefined && id != null) {
            id = id.substring(id.indexOf('_', 0) + 1); // d_1  -->  1
            if (toFloat($('#bp_' + id).val(), 0) > toFloat($('#p_' + id).val(), 0)) {
                if (servicePriceMsg == 1) {
                    $('#p_' + id).attr('style', 'background-color:red');
                    //Growl.Error(Lang.buyPriceIsGreaterThanSalePrice, 2000);
                    //alert(Lang.buyPriceIsGreaterThanSalePrice);
                } else if (servicePriceMsg == 2) {
                    // $('#se_' + id).focus();
                    $('#p_' + id).attr('style', 'background-color:red');
                    //Growl.Error(Lang.buyPriceIsGreaterThanSalePrice, 2000);
                    //alert(Lang.buyPriceIsGreaterThanSalePrice);
                    // $('#p_' + id).val(0);
                    $('#a_' + id).val(0);
                    $('#se_' + id).val('');
                    $('#sid_' + id).val('');
                    $.ionSound.play("bell_ring");
                    //$('#a_' + id).blur();
                    updatePrice(was);
                    setTimeout(function () {
                        $('#se_' + id).focus();
                    },
                        20);
                    return -1;
                }
                //$('#p_' + id).attr('style', 'background-color:white');
                return 1;
            }
            $('#p_' + id).attr('style', 'background-color:white');
        }
        return 1;
    };
    function autoPostPack1(event, data, formatted) {
        $(event.target).next().val(data[0]);
    }

    function storeSelectPosback() {
        setServiceAutoComplete();
        // Handle barcode reader
        $("#tableItems tr:last .storeservicename").blur(function () {
            // se_id, sid_id
            var me = $(this);
            var meID = me.attr('id'); // se_1
            var meSID = '#sid_' + meID.substring(meID.indexOf('_') + 1);

            var barcode = $(this).val();
            var stsID = $(meSID).val(); // StoreServiceID
            if (barcode.length > 0 && stsID == '') {
                // it is a barcode
               // barcode = fa2En(barcode);
                if (!checkServiceBarCode(me, meID, meSID, barcode))
                    if (!checkServiceVariateBarCode(me, meID, meSID, barcode)) {
                        $(me).val('');
                        setTimeout(function () { $(me).focus(); }, 10);
                    }
            }
        });
    }

    function accessAutoCompletes() {
        if (mID == -1) {
            autoCompleteNew({
                table: 'Stores',
                qd: 'storeauto',
                textInput: "#StoreName",
                hiddenInput: "#StoreID",
                postback: storeSelectPosback,
                allowNull: true
            });
            if (!hasValuePositive($("#StoreID").val())) {
                setDefault('Stores', 'firststore', "#StoreName", "#StoreID");
            }
           
        } else {
            if (!hasValuePositive($("#StoreID").val())) {
                autoCompleteNew({
                    table: 'Stores',
                    qd: 'storeauto',
                    textInput: "#StoreName",
                    hiddenInput: "#StoreID",
                    postback: storeSelectPosback,
                    allowNull: true
                });
                $("#StoreID").val(0);
                $("#StoreName").val(Lang.Null);
            } else {
                if ((mID > 0))
                $('#StoreName').attr('readonly', 'readonly');
            }
        }
        if (!hasGeneralAccess("selectstore", generalSettings)) {
            $('#StoreName').attr('readonly', 'readonly');
        }
    }

    function addRow(node) {
        var settingTax = "", settingVat = "";
        if (node == null) {
            settingTax = getSettingTax();
            settingVat = getSettingVat();
        }
        var row = '<tr>';
        //if (node != null && getNodeVal(node, "StoreServiceID") == '') {
        //    offerDiscount += toFloat(getNodeVal(node, "OfferDiscount", 0), 0);
        //    payAllowDays += toInt(getNodeVal(node, "PayAllowedDays", 0));
        //    return
        //}
        //else if (node != null && getNodeVal(node, "StoreServiceID") != '' && getNodeVal(node, "Source") == 1) {
        //    row = '<tr  style="background-color:orange">';
        //    //alert(getNodeVal(node, "Source"));
        //    // settingTax = 0;
        //    //settingVat = 0;
        //    //DiscountPercent = 0;
        //}
        //else if (node != null && getNodeVal(node, "Amount") < 0 && getNodeVal(node, "Source") != 1) {
            //row = '<tr  style="background-color:red">';
        //}

        row += '<td class="align-center row-number" data-th="' + Lang.ColRowNr + '">' + (cnt + 1) + '</td>'; // Row

        row += '<td data-th="' + Lang.CommodityName + '">' // CommodityName
        + '<input type="hidden"  class="storedraftitemid" id=sdi_' + cnt + ' name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
        + '<input type="text" id="se_' + cnt + '" title="' + getNodeVal(node, "StoreName") + '" name="StoreServiceName" value="' + getNodeVal(node, "StoreServiceName") + '" class="storeservicename full-width form-control input-sm" />'
        + '<input type="hidden" id="sid_' + cnt + '"  name="StoreServiceID" value="' + getNodeVal(node, "StoreServiceID") + '" class="storeserviceid" />'
        + '<input type="hidden" id="varid_' + cnt + '" name="ServiceVariateID" value="' + getNodeVal(node, "ServiceVariateID", 0) + '" class="ServiceVariateID" />'
        + '<input type="hidden" id="avial_' + cnt + '" name="Avail" value="' + getNodeVal(node, "Avail", 0) + '" class="Avail" /></td>';

        row += '<td class="align-center" data-th="' + Lang.Units + '">' // Units
        + '<input type="text" id="u_' + cnt + '" name="UnitName" value="' + getNodeVal(node, "UnitName") + '" class="unitname form-control input-sm" />'
        + '<input type="hidden" name="UnitID" value="' + getNodeVal(node, "UnitID") + '" />'
        + '</td>';

        row += '<td data-th="' + Lang.NumOrValue + '" class="align-center">'// NumOrValue
        + '<div class="input-group input-group-sm">'
        + '<input type="text" id="a_' + cnt + '" name="DeliveredAmount" value="' + getNodeVal(node, "DeliveredAmount")
        + '" class="validate[custom[number]] amount form-control width-100-percent min-width-60 align-center" />'
        + '<span class="input-group-addon no-left-border no-right-border">X</span>'
        + '<input readonly="readonly" size="1" id="ucc_' + cnt + '" name="UnitCurrencyConverter" value="'
        + getNodeVal(node, "UnitCurrencyConverter", 1) + '" class="form-control width-100-percent min-width-60 align-center" />'
        + '<input type="hidden" id="isv_' + cnt + '" name="IsVirtual" value="' + getNodeVal(node, "IsVirtual", 0) + '" class="isvirtual" />'
        + '</div>'
        + '</td>';

        row += '<td class="acc" data-th="' + Lang.Price + '">' // Price
        + '<input type="text" id="p_' + cnt + '" name="Price" value="' + getNodeVal(node, "Price", 0) + '" class="price monetary validate[custom[integer]] form-control align-center input-sm" />'
        + '<input type="hidden" id="ar_' + cnt + '" name="Items" value="" class="narrow items" />'
        + '<input type="hidden" name="BasePrice" id="bp_' + cnt + '" value="' + getNodeVal(node, "BasePrice") + '" class="baseprice monetary validate[custom[integer]]" />'
        + '<input type="hidden" name="Profit" id="prf_' + cnt + '" value="' + getNodeVal(node, "Profit") + '" />'
        + '</td>';

        //row += '<td class="acc" data-th="' + Lang.SalesManCommission + '">' // SalesManComission
        //+ '<div class="input-group input-group-sm">'
        //+ '<input type="text" id="smc_' + cnt + '" name="SalesManCommission" value="' + getNodeVal(node, "SalesManCommission", 0) + '" class=" salesmancommission form-control align-center" readonly="readonly" />'
        //+ '<span class="input-group-addon">%</span>'
        //+ '</span>'
        //+ '</td>';

        //row += '<td class="acc" data-th="' + Lang.BuyPrice + '">' // BuyPrice
        //+ '<input type="text" id="byp_' + cnt + '" name="BuyPrice" value="' + (getNodeVal(node, "BuyPrice", 0)) + '" class="buyprice monetary validate[custom[integer]] form-control input-sm align-center"  />'
        //+ '</td>';

        row += '<td class="acc" data-th="' + Lang.TotalPrice + '">' // TotalPrice
        + '<input type="text" id="t_' + cnt + '" name="Total" value="' + (getNodeVal(node, "Price", 0) * getNodeVal(node, "DeliveredAmount", 0)).toFixed(_Context.decimalPoints) * getNodeVal(node, "UnitCurrencyConverter", 1) + '" class="total monetary validate[custom[integer]] form-control input-sm align-center" readonly="readonly" />'
        + '</td>';

        if (_Context.useAdditionalValue > 0) {
        row += '<td class="acc width-70" data-th="' + Lang.Tax + '">' // Tax
        + '<input type="text" id="tax_' + cnt + '" name="Tax" value="' + getNodeVal(node, "Tax", settingTax) + '" class="tax percent autopercentsign monetary validate[custom[integer]] form-control input-sm align-center" />'
        + '</td>';

        row += '<td class="acc width-70" data-th="' + Lang.VAT + '">' // VAT
        + '<input type="text" id="sv_' + cnt + '" name="SingleVAT" value="' + getNodeVal(node, "SingleVAT", settingVat) + '" class="svat monetary percent autopercentsign validate[custom[integer]] form-control input-sm align-center" />'
        + '<input type="hidden" class="source" id="s_' + cnt + '" name="Source" value="' + getNodeVal(node, "Source", 0) + '" />'
        + '</td>';
        }

        row += '<td class="acc" data-th="' + Lang.Discount + '">' // Discount
        + '<input type="text" id="d_' + cnt + '" name="Discount" value="' + getNodeVal(node, "Discount", DiscountPercent) + '" class="discount percent monetary validate[custom[integer]] form-control input-sm align-center" />'
        + '<input type="hidden" class="source" id="s_' + cnt + '" name="Source" value="' + getNodeVal(node, "Source", 0) + '" />'
        + '</td>';

        row += '<td class="align-center action-remove">'
        + '<a class="btn btn-danger btn-xs btn-block btn-remove remove" id="r_' + cnt + '">'
        + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
        + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
        + '</a>'
        + '</td>';
        row += '</tr>';
        cnt++;
        return row;
    }
    function addPartner(node) {
        var row = '<tr>';
        row += '<td class="align-center row-number" data-th="' + Lang.ColRowNr + '">' + (cnt1 + 1) + '</td>';

        row += '<td data-th="' + Lang.PartnerName + '">'
        + '<input type="hidden" id=cti_' + cnt1 + ' name="ID2" value="' + getNodeVal(node, "ID", -1) + '" />'
        + '<input type="text" id="pn_' + cnt1 + '" name="PartnerName" value="' + getNodeVal(node, "PartnerName") + '" class="partnername form-control input-sm" />'
        + '<input type="hidden"  name="PartnerID" value="' + getNodeVal(node, "PartnerID") + '" />'
        + '</td>';

        row += '<td class="align-center action-remove">'
        + '<a class="btn btn-danger btn-xs btn-block btn-remove removepartner">'
        + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
        + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
        + '</a>'
        + '</td>';

        row += '</tr>';
        cnt1++;
        return row;
    }

    function PayMethodChange(foreChangeDefault) {
        $('#CheckName').removeClass('validate[required]');
        $('.topics').hide();
        var method = toInt($('#PayMethod').val(), SalesInvoicePayMethodEnum.Cash);
        if (method != SalesInvoicePayMethodEnum.Check && method != SalesInvoicePayMethodEnum.Complex) {
            $('.newCheck').hide();
        }
        else {
            $('.newCheck').show();
        }

        $('.sendToCardDiv').hide();
        if (method == SalesInvoicePayMethodEnum.Bank || method == SalesInvoicePayMethodEnum.Complex) {
            $('.sendToCardDiv').show();
        }

        if (method == SalesInvoicePayMethodEnum.Complex) {
            $('#CheckPrice')
                .blur(function() {
                    if (toFloat($(this).val()) > 0) {
                        $('#CheckName').addClass('validate[required]');
                    } else {
                        $('#CheckName').removeClass('validate[required]');
                    }
                });

            //$('.newCheck').insertAfter('#CheckSel');
            $('.nocomplex').hide();
            $('.complex').show();
            $('.topics').show();
            $('#OweTopicTitle').removeClass('validate[required]');
            $("label[for = OweTopicTitle]").text(Lang.Bank);
            $('.complextemp').hide();
            $('.duetopic').hide();
            if (fromTempBtn) {
                $('#IsTemp').val('');
                $('.complextemp').show();
            }
        $('#CashPrice, #BankPrice, #CheckPrice, #OwedPrice').blur(function () {
                autoSumOnComplexPayMethod($(this));
            });
            autoSumOnComplexPayMethod();
        }
        else {
            $('.nocomplex').show();
            $('.complex').hide();
            $('.complextemp').hide();
            $('#OwedPrice').val(0);
            $('#CashPrice').val(0);
            $('#BankPrice').val(0);
            $('#CheckPrice').val(0);
        }
        if (method == SalesInvoicePayMethodEnum.Bank) {
            $('.topics').show();
            $("label[for = OweTopicTitle]").text(Lang.Bank);
            $('.duetopic').hide();
            if (fromTempBtn) {
               if (mID > 0)
                   $('#IsTemp').val('checked');
            }
        }
        if (method == SalesInvoicePayMethodEnum.Cash && fromTempBtn) {
            $('.topics').show();
            if(mID > 0)
             $('#IsTemp').val('checked');
            $("label[for = OweTopicTitle]").text(Lang.Fund);
            $('.duetopic').hide();
        }
        if ((method == SalesInvoicePayMethodEnum.Other || method == SalesInvoicePayMethodEnum.Check) && fromTempBtn) {
            $('#IsTemp').val('');
        }
        // auto selected for shop mode
        setDefaultByTitleTree('TreePath', 'settingtreesauto', '#DueTopicCode', '#DueTopicTitle', '#DueTreePath', 'SaleTreePath');
        if (!storeMode && (foreChangeDefault || mID == -1 || mTreePathWasEmpty)) {
            // var method = $('#PayMethod').val();
            if (method == SalesInvoicePayMethodEnum.Cash) {
               // if (fromTempBtn) {
                    $('#OweTopicCode,#OweTopicTitle').val('');
                    $('#OweTopicTitle').unautocomplete();
                    $('#OweTopicCode').unautocomplete();
                    setDefaultByTitleTree('TreePath', 'laststoredraftduebyuser', '#OweTopicCode', '#OweTopicTitle', '#OweTreePath'
                        ,null , '&UserID=' + $('#UserID').val() + '&PayMethod=' + SalesInvoicePayMethodEnum.Cash);
                    autoComplete('Trees', 'treestypedauto', '#OweTopicTitle', '#OweTreePath', function (event, data, formatted) {
                        // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due, Nature, TopicTitle, ProgramCode, EquipmentNeed, CanHaveSubTree
                        var hasChild = toBool(data[5]), canHaveSubTree = toBool(data[12]);
                        if (hasChild || canHaveSubTree) {
                            if (hasChild)
                                alert(Lang.HasChild);
                            else
                                alert(Lang.HasSubTreeBut);
                        }
                    }, formatTreePath, null, null, 'get.aspx?op=Auto&table=Trees&qd=treestypedauto&Type=1' + appendTime());
                    if (!$('#OweTreePath').val()) {
                        setDefaultByTitleTree('TreePath', 'settingtreesauto', '#OweTopicCode', '#OweTopicTitle', '#OweTreePath', 'FundTreePath');
                    }
               // }
            }
            else if (method == SalesInvoicePayMethodEnum.Check) {
                $('#CheckName').removeClass('validate[required]');
                $('#CheckName').addClass('validate[required]');
                setDefaultByTitleTree('TreePath', 'settingtreesauto', '#OweTopicCode', '#OweTopicTitle', '#OweTreePath', 'CheckTreePath');
            }
            else if (method == SalesInvoicePayMethodEnum.Other)
                setDefaultByTitleTree('TreePath', 'settingtreesauto', '#OweTopicCode', '#OweTopicTitle', '#OweTreePath', 'OweTreePath');
            else if (method == SalesInvoicePayMethodEnum.Bank || method == SalesInvoicePayMethodEnum.Complex) {
                $('#OweTopicCode,#OweTopicTitle').val('');
                //if (fromTempBtn) {
                    $('#OweTopicTitle').unautocomplete();
                    $('#OweTopicCode').unautocomplete();
                    setDefaultByTitleTree('TreePath', 'laststoredraftduebyuser', '#OweTopicCode', '#OweTopicTitle', '#OweTreePath'
                     , null, '&UserID=' + $('#UserID').val() + '&PayMethod=' + SalesInvoicePayMethodEnum.Bank);
                    autoComplete('Trees', 'accesstreesbanksauto', '#OweTopicTitle', '#OweTreePath', function (event, data, formatted) {
                        // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due, Nature, TopicTitle, ProgramCode, EquipmentNeed, CanHaveSubTree
                        var hasChild = toBool(data[5]), canHaveSubTree = toBool(data[12]);
                        if (hasChild || canHaveSubTree) {
                            if (hasChild)
                                alert(Lang.HasChild);
                            else
                                alert(Lang.HasSubTreeBut);
                        }
                    }, formatTreePath, null, null, 'get.aspx?op=Auto&table=Trees&qd=accesstreesbanksauto' + appendTime());

                    $('#CashTopicCode,#CashTopicTitle').val('');
                    $('#CashTopicTitle').unautocomplete();
                    $('#CashTopicCode').unautocomplete();
                    setDefaultByTitleTree('TreePath', 'laststoredraftduebyuser', '#CashTopicCode', '#CashTopicTitle', '#CashTreePath'
                        , null, '&UserID=' + $('#UserID').val() + '&PayMethod=' + SalesInvoicePayMethodEnum.Cash);
                    autoComplete('Trees', 'treestypedauto', '#CashTopicTitle', '#CashTreePath', function (event, data, formatted) {
                        // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due, Nature, TopicTitle, ProgramCode, EquipmentNeed, CanHaveSubTree
                        var hasChild = toBool(data[5]), canHaveSubTree = toBool(data[12]);
                        if (hasChild || canHaveSubTree) {
                            if (hasChild)
                                alert(Lang.HasChild);
                            else
                                alert(Lang.HasSubTreeBut);
                        }
                    }, formatTreePath, null, null, 'get.aspx?op=Auto&table=Trees&qd=treestypedauto&Type=1' + appendTime());

                    if (!$('#OweTreePath').val()) {
                        setDefaultByTitleTree('TreePath', 'defaulttreesbanksauto', '#OweTopicCode', '#OweTopicTitle', '#OweTreePath', 'OweTreePath');
                    }
              //  }
            }
        }

        if ((foreChangeDefault || mID == -1) && method == SalesInvoicePayMethodEnum.Complex) {
            if (!fromTempBtn) {
                setDefaultByTitleTree('TreePath', 'settingtreesauto', '#CashTopicCode', '#CashTopicTitle', '#CashTreePath', 'FundTreePath');
            }
            setDefaultByTitleTree('TreePath', 'settingtreesauto', '#CheckTopicCode', '#CheckTopicTitle', '#CheckTreePath', 'CheckTreePath');
            setDefaultByTitleTree('TreePath', 'settingtreesauto', '#OwedTopicCode', '#OwedTopicTitle', '#OwedTreePath', 'OweTreePath');
        }
    }
    function autoSumOnComplexPayMethod(target) {
        if (toInt($('#PayMethod').val(), -1) == 3) {
            var fPrice = toFloat($('#Total').val(), 0);
            var cPrice = toFloat($('#CashPrice').val(), 0);
            var bPrice = toFloat($('#BankPrice').val(), 0);
            var chPrice = toFloat($('#CheckPrice').val(), 0);
            var oPrice = fPrice - (cPrice + bPrice + chPrice);
            if (oPrice >= 0) {
                    $('#OwedPrice').val(numberWithCommas(oPrice));
            } else {
                if (target) {
                    Growl.Notice(Lang.autoSumOnComplexPayMethodMsg, 4000);
                    target.val(0);
                }
            }
        }
    }
    function checkAuto() {
        var more = '';
        var buyerID = $('#BuyerID').val();
        if (hasValuePositive(buyerID))
            more = "PersonnelID=" + buyerID;
        $('#CheckName').unautocomplete();
        autoCompleteFree('Checks', 'checksauto', '#CheckName', '#CheckID', null, null, null, more);
    }

    function setRequireds() {
        if (toFloat($('#CashPrice').val(), 0) > 0)
            addRequired('#CashTreePathName');
        else
            removeRequired('#CashTreePathName');
        if (toFloat($('#CheckPrice').val(), 0) > 0)
            addRequired('#CheckTreePathName');
        else
            removeRequired('#CheckTreePathName');
        if (toFloat($('#OwedPrice').val(), 0) > 0)
            addRequired('#OwedTreePathName');
        else
            removeRequired('#OwedTreePathName');
    }

    function checkServiceBarCode(me, meID, meSID, barcode) {
        var table = $("#tableItems");
        var buyerID = toInt($("#BuyerID").val(), -1);
        var StoreID = toInt($("#StoreID").val(), -1);
        var _autourl = 'get.aspx?op=Auto&table=StoreServices&qd=storeservicesbarcodeauto' + '&BuyerID=' + buyerID + '&StoreID=' + StoreID + appendTime();
        var row = getServerValues(_autourl + "&q=" + barcode);
        if (!row) {
            return false;
        }
        if (row && row.length && row.length > 10) {
            // insert new row when we have variate unit
            var unitId = row[6];
            if (toInt(row[22], -1) > 0) {
                unitId = row[22];
            }
            var varID = -1;
            // ServiceVariate
            if (row[17] > 0)
                varID = row[17];
            // -----------------------------------
            // check if the service is already used, just increase the number
            var already = false;
            $('tr .storeserviceid', table).each(function () {
                var thisId = $(this).attr('id');
                thisId = thisId.substring(thisId.indexOf('_') + 1);
                if ($(this).attr('id') != meSID) {
                    if ($(this).val() == row[0] && $('#u_' + thisId).next().val() === unitId) {
                        var v = toInt($(this).parent().find('.ServiceVariateID').val(), 0);
                        if (v == 0) {
                            already = true;
                            me.val('');
                            me.focus();
                            var aID = $(this).attr('id');
                            var _cnt = aID.substring(aID.indexOf('_') + 1);
                            $('#a_' + _cnt).val(toInt($('#a_' + _cnt).val(), 0) + 1);
                            $('#a_' + _cnt).blur();
                            updatePrice($('#a_' + _cnt));
                            setTimeout(function () {
                                $(me).focus();
                            }, 1);
                        }
                    }
                }
            });
            if (!already) {
                setServiceData(me, row);
                me.val(row[1]);
                $('.add').first().click();
            }
        }
        return true;
    }
    function checkServiceVariateBarCode(me, meID, meSID, barcode) {
        var table = $("#tableItems");
        var buyerID = toInt($("#BuyerID").val(), -1);
        var StoreID = toInt($("#StoreID").val(), -1);
        var _autourl = 'get.aspx?op=Auto&table=StoreServices&qd=storeservicesvariatebarcodeauto' + '&BuyerID=' + buyerID + '&StoreID=' + StoreID + appendTime();
        var row = getServerValues(_autourl + "&q=" + barcode);
        if (!row) {
            $.ionSound.play("bell_ring");
            return false;
        }
        if (row && row.length && row.length > 10) {
            var varID = -1;
            // ServiceVariate
            if (row[17] > 0)
                varID = row[17];
            // -----------------------------------
            // check if the service is already used, just increase the number
            var already = false;
            $('tr .storeserviceid', table).each(function () {
                if ($(this).attr('id') != meSID) {
                    if ($(this).val() == row[0]) {
                        // check variate
                        if (varID != -1 && toInt(varID) > 0) {
                            // parent = td
                            var v = $(this).parent().find('.ServiceVariateID').val();
                            if (v != varID)
                                return;
                        }
                        already = true;
                        me.val('');
                        me.focus();
                        var aID = $(this).attr('id');
                        var _cnt = aID.substring(aID.indexOf('_') + 1);
                        $('#a_' + _cnt).val(toInt($('#a_' + _cnt).val(), 0) + 1);
                        $('#a_' + _cnt).blur();
                        updatePrice($('#a_' + _cnt));
                        setTimeout(function () {
                            $(me).focus();
                        }, 1);
                    }
                }
            });
            if (!already) {
                setServiceData(me, row);
                me.val(row[1] + ' - ' + row[18]);  // row[18] = ServiceVariate.VarTitle 


                $('.add').first().click();
            }
        }
        return true;
    }

    function addClick() {
        var storeID = toInt($("#StoreID").val(), -1);
        //if (!hasValuePositive(storeID)) {
        //    alert(Lang.ChooseStore);
        //    return;
        //}

        var table = $("#tableItems");
        var lastRow = $('tr:last', table);
        if ($("#tableItems .amount").length > 0 && toInt($('.storedraftitemid', lastRow).val(), -1) === -1) {
            if (checkAvail($('.amount', lastRow)) < 0 || checkgraterbuyprice($('.amount', lastRow)) < 0) {
                return false;
            };
        }
        if (insertStoreDraftWithBuyerOutBalance != 1) {
            if (!checkCredit()) {
                Growl.Error(Lang.LowCredit, 3000);
                $('.amount', lastRow).val(0);
                $('.amount', lastRow).blur();
                return false;
            }
        }
        $('tr:last', table).after(addRow(null));
        $('.monetary').keyup(function () {
            convertToScondMonetary(this);
        });
        disableKeydownOnLastInputByName('Discount');
        Growl.Notice(Lang.RowAddedSuccessfully);
        lastRow = $('tr:last', table);
        if (storeMode)
            $('.acc').hide();

        $('.remove', lastRow).click(function () {
            var element = $(this);
            doConfirm(function () {
                removeRow(element);
            }, Lang.ConfirmDelete);
        });

        setServiceAutoComplete();
        // Handle barcode reader
        $("#tableItems tr:last .storeservicename").blur(function () {
            // se_id, sid_id
            var me = $(this);
            var meID = me.attr('id'); // se_1
            var meSID = '#sid_' + meID.substring(meID.indexOf('_') + 1);

            var barcode = $(this).val();
            var stsID = $(meSID).val(); // StoreServiceID
            if (barcode.length > 0 && stsID == '') {
                // it is a barcode
                if (!checkServiceBarCode(me, meID, meSID, barcode))
                    if (!checkServiceVariateBarCode(me, meID, meSID, barcode)) {
                        $(me).val('');
                        setTimeout(function () { $(me).focus(); }, 10);
                    }
            }
        });
        //autoComplete('Units', 'unitsauto', "#tableItems tr:last .unitname", "#justfurfun", tableAutoPostPack, formatItemNameCode, null, null, 'get.aspx?op=Auto&table=Units&qd=unitsauto&UnitID=' + 7 + '&StoreServiceID=' + 131 + appendTime());
        $('.tax, .svat', lastRow).blur(function () { updateTotal(); });
        $('.price', lastRow).blur(function () {
            checkgraterbuyprice($(this));
        });
        $('.price, .amount, .discount, .storeservicename, .unitname', lastRow).blur(function () { updatePrice(this); });
        $('.amount', lastRow).blur(function () { checkAvail(this); });
        commaInputs();
        monetaryKeys();
        autoPercentSign();
        //  alert($("#tableItems tr:last .storeservicename").attr('id'));
        setTimeout(function () {
            $("#tableItems tr:last .storeservicename").focus();
        }, 10);
    }


    function removeRow($who) {
        var id = $who.attr('id'); // r_1
        id = id.substring(id.indexOf('_') + 1);
        ///id = id.substring(2); // r_1  -->  1
        $('#se_' + id).val(""); // StoreServiceName
        $('#sid_' + id).val(-1); // StoreServiceID
        $('#p_' + id).val("");
        $('#a_' + id).val("");//DeliverAmount
        $who.parent().parent().hide();
        $who.parent().parent().find(':input').removeClass();
        updateTotal();
        Growl.Notice(Lang.DeletedSuccessfully);
    }

    $('#NewPerson').click(function () {
        var oldID = _Context.id;
        function showcallback(v, m, f) {
            if (v == 'OK') {
                generalOKScalar(v, m, f, 'personupdate', 'personinsert', 'Personnel', '#subDetailForm');
                if (!_Context.success)
                    return false;
                $('#BuyerName').val(f.Name + ' ' + f.Surname);
                $('#BuyerID').val(_Context.resultID);
            }
            _Context.id = oldID;
        }
        _Context.id = -1;
        var _url = 'get.aspx?op=GetHtml&table=Personnel&qd=paycheck&id=-1&view=person' + appendTime();
        showPopup(_url, showcallback, getButtons());
    });

    $('#FactorProfit').click(function () {
        $('#SumProfit').show();
    });

    function storedraftTypeChange(notChangeDefault) {
        var storeDraftType = toInt($('#StoreDraftType').val(), 0);
        if (storeDraftType == 0) {
            $('.notguildtrust').show();
            if (notChangeDefault) {
                PayMethodChange(false);
            }
            else {
                PayMethodChange(true);
            }

        }
        else if (storeDraftType == 2) {
            $('.notguildtrust').hide();
        }
        if (storeMode) {
            $('.acc').hide();
        }
    }
    function showProfitTooltip() {
        var html = '<table class="pricetooltiptable"><td>' + Lang.CommodityName + '</td><td>' + Lang.BuyPrice + '</td><td>' + Lang.SaleWorth + '</td><td>' + Lang.Num + '</td><td>' + Lang.Profit + '</td></tr>';
        var serviceName, buyPrice, salePrice, profit, num;
        $('.storeservicename').each(function (node) {
            var elem = $(this);
            var rowid = $(elem).attr('id');
            rowid = rowid.substring(rowid.indexOf('_', 0) + 1);
            serviceName = $('#se_' + rowid).val();
            buyPrice = numberWithCommas(toMonetary(toFloat($('#bp_' + rowid).val(), 0)));
            salePrice = $('#p_' + rowid).val();
            num = toFloat($('#a_' + rowid).val(), 0) * toFloat($('#ucc_' + rowid).val(), 1);
            profit = numberWithCommas((toFloat((num * toFloat($('#p_' + rowid).val(), 0)) - (num * toMonetary(toFloat($('#bp_' + rowid).val(), 0))), 0)));
            if (serviceName !== "") {
                html += '<tr>';
                html += '<td>' + serviceName + '</td>';
                html += '<td>' + buyPrice + '</td>';
                html += '<td>' + salePrice + '</td>';
                html += '<td>' + num + '</td>';
                html += '<td>' + profit + '</td>';
                html += '</tr>';
            }
        });
        html += '</table>';
        $('#TolTipDiv').find('.alert').remove();
        $('<div class="alert alert-success alert-dismissable pricetooltiptablediv" style="position:absolute;z-index:1000">' + html +
        '<button type="button" class="close" ' +
                'data-dismiss="alert" aria-hidden="true">' +
            '&times;' +
        '</button>' +
     '</div>').appendTo($('#TolTipDiv'));
        //alert(html);
    };
    //function colorRows() {
    //    // Avail
    //    $('#tableItems tr').each(function () {
    //        if ($('.Avail', this).length > 0) {
    //            var avail = toInt($('.Avail', this).val(), 0);
    //            if (avail <= 0)
    //                $('td', this).removeClass('invalid').addClass('invalid');
    //        }
    //    });
    //}
    // ------------------------------
    // startup code
    // ------------------------------

    mID = toInt($('#MasterID').val(), -1);
    if ($('#FromTempBtn').val() == 'checked') {
        fromTempBtn = true;
    }
    var manualNoSerial = toInt(getSettings('ManualNoSerial'), 0);
    if (manualNoSerial <= 0) {
        if (mID === -1) {
            $('#NOSerial').val('');
        }
        $('#NOSerial').attr('readonly', 'readonly');
    }
    tabify();
    if ($('#FromTempBtn').val() == 'checked' || $('#FromTempBtn').val() == 'checked') {
        $('#FactorTotalDiscount').attr('readonly', 'readonly');
    }
    if (fromTempBtn) {
        $('#IsTemp').val('checked');
        $('.personforprint').removeClass('hidden');
    }
    var serviceRemainMsg = getSettings('ServiceRemainMsg');
    var servicePriceMsg = getSettings('ServicePriceMsg');
    var sellerBuyerNameValidtion = toInt(getSettings('SellerBuyerNameValidation'), 0);
    var insertStoreDraftWithBuyerOutBalance = toInt(getSettings('InsertStoreDraftWithBuyerOutBalance'), 0);
    var insertStoreDraftWithNegitiveServiceRemain = toInt(getSettings('InsertStoreDraftWithNegitiveServiceRemain'), 0);
    if (sellerBuyerNameValidtion == 1) {
        $("#BuyerName").addClass('validate[required]');
    }
    // show factoreprofit just for admin
    if (!hasGeneralAccess("storedraftprofit", generalSettings)) {
        $('#FactorProfitDiv').hide();
    }
    setValue('#PayMethod');
    setValue('#StoreDraftType');

    if (toString($('#OweTopicTitle').val())) {
        mTreePathWasEmpty = false;
    }

    $('#SumProfit').focus(showProfitTooltip);
    $('#SumProfit').blur(function () {
        updateSumProfit();
        $('.alert-dismissable').remove();
    });


    // var storeMode = $.urlParam('menu') != 'accountinga';
    var storeMode = !hasAccess("links", "mainmenu", "accounting") && !hasAccess("links", "mainmenu", "sales");
    if (mID == -1) {
        $("#UserID").val(_Context.userID);
        setDefault('Personnel', 'defaultstoredraftperson', "#BuyerName", "#BuyerID");
    }

    $("#BuyerName").blur(function () {
        _url = 'get.aspx?op=Auto&table=Personnel&qd=personneldetailauto&q=' + $('#BuyerID').val();
        detail = getServerValues(_url);
        if (detail && detail[3] > 0) {
            alert(Lang.StoreDraftJsM1 + ' ' + detail[3] + ' ' + Lang.StoreDraftJsM2);
            DiscountPercent = detail[3] + '%';
        }
        else
            DiscountPercent = 0;
        if (DiscountPercent !== 0) {
            $('.discount').val(DiscountPercent);
        }

    });

    shortcut.remove("Ctrl+Enter");
    $('#Comments').focus(function () {
        shortcut.add("Ctrl+Enter", function () {
            $("#tableItems tr:last .storeservicename").focus();
        });
    });


    setChecked('#CommentByBuyerName');
    setChecked('#NoPrintFactorDate');
    setValue("#WorkflowStatus");
    //if ($('#WorkflowStatus').val() == 1)
    //    $('.DesiredDocNo').hide();


    if ($("#TotalAmount").val() == '')
        $("#TotalAmount").val(0);
    if ($("#TotalDiscount").val() == '')
        $("#TotalDiscount").val(0);
    if ($("#TotalPrice").val() == '')
        $("#TotalPrice").val(0);
    if ($("#FactorDate").val() == '')
        $("#FactorDate").val(_Context.today);


    dateSelect('.dateSelect');

    autoCompleteTreePath2('#OweTopicCode', '#OweTopicTitle', '#OweTreePath', '#OweSel');
    autoCompleteTreePath2('#DueTopicCode', '#DueTopicTitle', '#DueTreePath', '#DueSel');
    //autoCompleteTreePath('#DueTreePathName', '#DueTreePath', '#DueSel');
    autoCompleteTreePath2('#CashTopicCode', '#CashTopicTitle', '#CashTreePath', '#CashSel');
    autoCompleteTreePath2('#CheckTopicCode', '#CheckTopicTitle', '#CheckTreePath', '#CheckSel');
    autoCompleteTreePath2('#OwedTopicCode', '#OwedTopicTitle', '#OwedTreePath', '#OwedSel');


    accessAutoCompletes();

    setDefault('StoreDraft', 'laststoredraftdue', "#DueTreePathName", "#DueTreePath");

    autoCompleteFree('Personnel', 'personnelselectauto', '#BuyerName', '#BuyerID', null
            , function (data, i, n, value) {
                //$('#BuyerID').val('');
                return formatPersonnelItem(data);
            });
    if (!storeMode) {
        showPersonBook('#BuyerID', true, true);
    }
    autoComplete('Personnel', 'visitorauto', '#VisitorName', '#VisitorID', null
            , function (data, i, n, value) {
                //$('#SellerID').val('');
                return formatPersonnelItem(data);
            });
    // -------------- +++++


    // checkAuto();
    // DetailForm
    $('.print').hide();
    fillTable("get.aspx?op=GetXml&qd=storedraftitem&StoreDraftID=" + mID, '#tableItems', addRow);
    $('#StoreDraftType').change(storedraftTypeChange);
    storedraftTypeChange(true);
    $('#PayMethod').chosen().change(function () { PayMethodChange(true); });
    PayMethodChange(false);
    autoPercentSign();
    // colorRows();
    fillTable("get.aspx?op=GetXml&qd=storedraftperson&StoreDraftID=" + mID, '#tablePartners', addPartner);
    // autoComplete('Units', 'enumauto', "#tableItems .unitname", "#justfurfun", tableAutoPostPack, formatItemNameCode);

    $('#tableItems .storeservicename').each(function () {
        var id = $(this).attr('id');
        id = id.substring(id.indexOf('_') + 1);
        var unitName = $('#u_' + id);
        var storeServiceId = $('#sid_' + id).val();
        if (storeServiceId > 0) {
            var _autourl1 = 'get.aspx?op=Auto&table=Units&qd=storedraftunitsauto&StoreServiceID=' + storeServiceId;
            $(unitName).unautocomplete();
            autoComplete('Units', 'unitsauto', unitName, "#justfurfun",
                function (event, data, formatted) {
                    var id1 = $(event.target).attr('id');
                    id1 = id1.substring(id1.indexOf('_') + 1);
                    $(event.target).next().val(data[0]); // ID
                    if (data[2] && data[2] != 0) {
                        //$('#a_' + id1).next().text('x')
                        $('#ucc_' + id1).val(data[2]); // CurrencyConverter}
                    }
                    else {
                        //$('#a_' + id1).next().text(' ')
                        $('#ucc_' + id1).val('1');
                    }
                }
            , null, null, null, _autourl1);
        }
    });

    doMore();
    addClick();

    $('#TotalPrice').blur(function () { updateFormTotal(); });
    $('.price, .amount, .discount, .storeservicename, .unitname').blur(function () { updatePrice(this); });
    $('.price').blur(function () {
        checkgraterbuyprice($(this));
    });
    $('.amount').blur(function () { checkAvail(this); });
    autoComplete('Purchase', 'purchaseauto', '#PurchaseName', '#PurchaseID');

    $('#CashPrice,#CheckPrice,#OwedPrice').blur(setRequireds);
    $('.tax, .svat').blur(function () { updateTotal(); });
    $('#FactorTotalDiscount').blur(function () { updateTotal(); });
    setRequireds();

    updateFormTotal();

    function setServiceData(serviceNameInput, data) {
        serviceNameInput.next().val(data[0]); // ID
        serviceNameInput.attr('title', data[19]); // StoreName
        var id = serviceNameInput.attr('id');
        id = id.substring(id.indexOf('_') + 1);
        var percent = 1 + toFloat(data[11], 0) / 100.0;
        var price = 0;
        // has ProfitPercent from service declaration
        // Price priority:
        // 1- GroupingPrice: col10 | col2 = BuyPrice, col17 = SalePrice
        // 2- ProfitPercent: col10 = BasePrice, col2 = SalePrice
        // 3- Last Prices: col12 = LastSalePrice (TheLPSP), col14 = LastBuyPrice (TheLPBP)
        //price = data[2];
        if (data[11]) {
            // ProfitPercent
            if (data[2])
                price = data[2];
            if (data[10])
                $('#bp_' + id).val((toFloat(toMonetary(data[10]), 0.0)));
        }
        else {
            // Last Prices
            if (data[12])
                price = data[12];
            if (data[14])
                $('#bp_' + id).val((toFloat(toMonetary(data[14]), 0.0)));
        }
        if (data[16]) {
            price = data[16];
            // BuyPrice is from Old method
        }
        // $('#byp_' + id).val(numberWithCommas((toFloat(toMonetary(data[14]), 0.0)).toFixed(_Context.decimalPoints))); // buy price
        $('#p_' + id).val(numberWithCommas((toFloat(toMonetary(price), 0.0)).toFixed(_Context.decimalPoints))); // price
        $('#isv_' + id).val(data[21]);//IsVirtaul
        $('#prf_' + id).val(toFloat(data[11], 0));
        //$('#smc_' + id).val(numberWithCommas(data[9])); // salesmancommission
        if (toInt(data[22], -1) > 0) // OtherUnitID
        {
            $('#u_' + id).next().val(data[22]); // OtherUnitID
            $('#u_' + id).val(data[23]); // OtherUnitName
            $('#ucc_' + id).val(data[24]); // CurrencyConverter
        } else {
            $('#u_' + id).next().val(data[6]); // unitid
            $('#u_' + id).val(data[7]); // unitname
        }
        $('#avial_' + id).val(data[4] - data[5]); // available: income - outcome
        var _autourl1 = 'get.aspx?op=Auto&table=Units&qd=storedraftunitsauto&StoreServiceID=' + data[0];
        $("#tableItems tr:last .unitname").unautocomplete();
        autoComplete('Units', 'unitsauto', "#tableItems tr:last .unitname", "#justfurfun",
            function (event, data, formatted) {
                var id1 = $(event.target).attr('id');
                id1 = id1.substring(id1.indexOf('_') + 1);
                $(event.target).next().val(data[0]); // ID
                if (data[2] && data[2] != 0) {
                    //$('#a_' + id1).next().text('x')
                    $('#ucc_' + id1).val(data[2]); // CurrencyConverter}
                }
                else {
                    //$('#a_' + id1).next().text(' ')
                    $('#ucc_' + id1).val('1');
                }
            }
        , null, null, null, _autourl1);
        //bindSaleType();
        // ServiceVariate
        if (data[17] > 0) {
            $('#varid_' + id).val(data[17]);
        }
        $('#a_' + id).val(1);
        $('#a_' + id).blur();
        $('#p_' + id).blur();
        if (data[20]) {
            var val = toFloat(data[20], 0);
            if (val < 0)
                $('#d_' + id).val(numberWithCommas((val * -1.0).toFixed(_Context.decimalPoints)) + '%');
            else
                $('#d_' + id).val(numberWithCommas(toMonetary((val * 1.0)).toFixed(_Context.decimalPoints)));
        }
    }
    function setServiceAutoComplete() {
        //var more = encodeURIComponent("&StoreID=" + storeID);
        var storeID = toInt($("#StoreID").val(), -1);
        var buyerID = toInt($("#BuyerID").val(), -1);
        var table = $("#tableItems");
        $("#tableItems tr:last .storeservicename").unautocomplete();
        if ($("#tableItems tr:last .storeservicename").val() != '') {
            return;
        }
        var _autourl = 'get.aspx?op=Auto&table=StoreDraft&qd=storeservicesauto&StoreID=' + storeID + '&BuyerID=' + buyerID + appendTime();
        autoCompleteFree('StoreServices', 'storeservicesauto', "#tableItems tr:last .storeservicename", "#justfurfun"
                , function (event, data, formatted) {
                    $('tr .storeserviceid', table).each(function () {
                        if ($(this).attr('id') != $('tr:last .storeserviceid', table).attr('id')) {
                            if ($(this).val() == data[0]) {
                                alert(Lang.StoreDraftJsM3);
                            }
                        }
                    });
                    setServiceData($(event.target), data);
                }
                , function (data, i, n, value) {
                    return formatService(data);
                }, null, null, _autourl);
    }

    function doMore() {

        function removePartner($who) {
            var id = $who.attr('id'); // rc_1
            id = id.substring(3); // rc_1  -->  1
            $('#pn_' + id).val("");
            $('#pn_' + id).next().val(-1);
            $who.parent().parent().hide();
        }
        $('.remove').click(function () {
            var element = $(this);
            doConfirm(function () {
                removeRow(element);
            }, Lang.ConfirmDelete);
        });
        $('.removepartner').click(function () {
            removePartner($(this));
        });
        $('.add').click(function () {
            addClick();
        });
        $('.addPartner').click(function () {
            var table = $('#tablePartners');
            $('tr:last', table).after(addPartner(null));
            var lastRow = $('tr:last', table);
            $('.removepartner', lastRow).click(function () {
                removePartner($(this));
            });
            autoComplete('Personnel', 'personnelaccauto', "#tablePartners tr:last .partnername", "#justfurfun"
    , autoPostPack1
    , function (data, i, n, value) {
        return data[1];
    });
        });
        insertValidation();
        monetaryInputsSet(null, '#FactorDate');
        commaInputs();
        percentInputsSet();
        $('#newCheck').click(function () {

            var oldID = _Context.id;
            function showcallback(v, m, f) {
                if (v == 'OK') {
                    generalOKScalar(v, m, f, 'checkupdate', 'checkinsert', 'Checks', '#subDetailForm');
                    if (!_Context.success)
                        return false;
                    $('#CheckName').val(f.Serial);
                    $('#CheckID').val(_Context.resultID);
                }
                _Context.id = oldID;
            }
            _Context.id = -1;
            var _url = 'get.aspx?op=GetHtml&table=Checks&view=checksshop&qd=checkshop&id=-1&PersonnelName=' + encodeURIComponent($('#BuyerName').val()) + "&PersonnelID=" + $('#BuyerID').val()
                + "&FromStoreDraft=1&Amount=" + fromMonetary($('#Total').val(), null) + appendTime();
            //alert(_url);
            showPopup(_url, showcallback, getButtons());
        });
        $('#CalculateRewards').click(function () {
            $('.source').each(function (index, value) {
                if ($(value).val() == 1) {
                    var element = $(this);
                    doConfirm(function () {
                        removeRow(element);
                    }, Lang.ConfirmDelete);
                }
            });
            // offerDiscount = 0;
            // payAllowDays = 0;

            fillTable("get.aspx?op=GetXml&qd=calculaterewards&StoreDraftID=" + mID, '#tableItems', addRow);
            disableKeydownOnLastInputByName('Discount');

            $('.remove').click(function () {
                var element = $(this);
                doConfirm(function () {
                    removeRow(element);
                }, Lang.ConfirmDelete);
            });

            $('#OfferDiscount').val(offerDiscount);
            $('#PayAllowedDays').val(payAllowDays);
            commaInputs();
        });

        updateTotal();
        if (storeMode)
            $('.acc').hide();

    }

    //if (mID > 0) {
    //    uploadify('#ChooseFile', '#FileName', '#StartUpload', 'workflow/storedraft/' + mID
    //               , function (fileName) {
    //                   $('#FileNameDisplay').html(fileName);
    //               });
    //    $('#workflowtasks').html(showWorkflowTasks(mID, WorkflowTypeEnum.StoreDraft));
    //}
    //else
    //    $('.transDiv').hide();

    $('#connectToCard').click(function () {
        PosCloseCOM();
        PosOpenCOM();
        return false;
    });
    $('#sendToCard').click(function () {
        var val = 0;
        var method = toInt($('#PayMethod').val(), SalesInvoicePayMethodEnum.Cash);
        if (method == SalesInvoicePayMethodEnum.Bank) {
            val = stripCommas($('#Total').val());
        }
        else if (method == SalesInvoicePayMethodEnum.Complex) {
            val = stripCommas($('#BankPrice').val());
        }
        if (val <= 0)
            return false;
        PosSendToCOM(val);
        return false;
    });
    $('#disconnectCard').click(function () {
        PosCloseCOM();
        return false;
    });
    $('.monetary').keyup(function () {
        convertToScondMonetary(this);
    });
    $('#ConvertedPriceAbbreviation').text(_Context.monetaryUnitAbbreviationSecond);

    //// Check access for finalized factors:
    //if ($('#WorkflowStatus').val() == 1) {
    //    if (hasAccess("sales", "storedraft", "price")) {
    //        // it can do everything: add / delete / edit
    //    }
    //    else if (hasAccess("sales", "storedraft", "count")) {
    //        // it can not do add or delete
    //        // it can not change price
    //        // it can change amount
    //        $('.add').remove();
    //        $('.remove').remove();
    //        $('.price').attr('readonly', true);
    //    }
    //    else {
    //        // it can do nothing
    //        $('.add').remove();
    //        $('.remove').remove();
    //        $('.price').attr('readonly', true);
    //        $('.amount').attr('readonly', true);
    //        $('.unitname').attr('readonly', true);
    //    }
    //}
    $("#hidenTotal")
        .click(function () {
            if ($("#hideTotal").css("display") == "none") {
                $("#hideTotal").slideDown();
                $("#hideTotal").css("display", "block");
                $(".fa-angle-double-down").addClass("rotated");

            } else {
                $("#hideTotal").slideUp();
                $("#hideTotal").css("display", "none");
                $(".fa-angle-double-down").removeClass("rotated");
            }
        });

    if (_Context.useAdditionalValue > 0) {
        $('.additionalvalue').show();
    } else {
        $('.additionalvalue').hide();
    }
}