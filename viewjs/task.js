﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-11-01: file created
*/
function view_task() {
    $("#tableItems").smartTable();
    var cnt = 0, cnt1 = 0, mID = -1;
    function addSystemPosition(node) {
        var row = '<tr>';

        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">' + (cnt + 1) + '</td>';

        row += '<td data-th="' + Lang.Roles + '">'
            + '<input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="text" id="se_' + cnt + '" name="RoleName" value="' + getNodeVal(node, "RoleName") + '" class="rolename form-control input-sm"/>'
            + '<input type="hidden" name="RoleID" value="' + getNodeVal(node, "RoleID") + '" />'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a class="btn btn-danger btn-xs btn-block btn-remove removeRole" id="r_' + cnt + '">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';

        cnt++;
        return row;
    }
    //----------end addrow

    function RemoveRoleRow($who) {
        var id = $who.attr('id'); // r_1
        id = id.substring(2); // r_1  -->  1
        //  alert(id);
        $('#se_' + id).val(""); // 
        $('#se_' + id).next().val(-1); // 
        $who.parent().parent().hide();
        Growl.Notice(Lang.DeletedSuccessfully);
    }

    function autoPostPack(event, data, formatted) {
        $(event.target).next().val(data[0]); // ID
    }
    function bindAdds() {
        $('.addRole').click(function () {
            var table = $("#tableItems");
            var tableID = table.attr("id");
            $('tr:last', table).after(addSystemPosition(null));
            var lastRow = $('tr:last', table);
            Growl.Notice(Lang.RowAddedSuccessfully);
            autoComplete('Roles', 'enumauto1', "#tableItems tr:last .rolename", "#justfurfun",
                autoPostPack,
                function (data, i, n, value) {
                    return data[1];
                }
           );
        });
    }

    function bindRemoves() {
        //-----------
        $('.removeRole').click(function () {
            RemoveRoleRow($(this));
        });
    }

    function showMoreTasks() {
        $('.AccFinalStateDiv').hide();
        $('.FinalStateDiv').hide();
        $('.SecondFinalStateDiv').hide();
        $('#FactorRole').find('option').remove();
        $('#SecondFactorRole').find('option').remove();
        $('#FactorRole').append('<option value="0">' + Lang.FactorRoleNone + '</option>');
        $('#SecondFactorRole').append('<option value="0">' + Lang.FactorRoleNone + '</option>');
        if ($('#State').val() == TaskStateEnum.Final || $('#State').val() == TaskStateEnum.Single) {
            if ( $('#WorkflowType').val() == WorkflowTypeEnum.BuildOrder
                || $('#WorkflowType').val() == WorkflowTypeEnum.PurchaseRequest) {
                $('.FinalStateDiv').show();
            }
            if ($('#WorkflowType').val() == WorkflowTypeEnum.Request) {
                $('.FinalStateDiv').show();
                $('.SecondFinalStateDiv').show();
            }

            if ($('#WorkflowType').val() == WorkflowTypeEnum.Request) {
                $('#FactorRole').append('<option value="1">' + Lang.FactorRoleStoreDraft + '</option>');
                $('#SecondFactorRole').append('<option value="3">' + Lang.FactorRolePurchaseRequest + '</option>');
            }
            if ($('#WorkflowType').val() == WorkflowTypeEnum.BuildOrder) {
                $('#FactorRole').append('<option value="2">' + Lang.FactorRolePurchase + '</option>');
            }
            if ($('#WorkflowType').val() == WorkflowTypeEnum.PurchaseRequest) {
                $('#FactorRole').append('<option value="2">' + Lang.FactorRolePurchase + '</option>');
            }

            // 0:Transfer, 1:Request, 2:Purchase, 3:PurchaseRequest, 4:Return, 5:Refuse, 6:StoreDraft, 7:BuildOrder, 8:Voucher
            if ($('#WorkflowType').val() == WorkflowTypeEnum.Purchase || $('#WorkflowType').val() == WorkflowTypeEnum.StoreDraft
                || $('#WorkflowType').val() == WorkflowTypeEnum.Return || $('#WorkflowType').val() == WorkflowTypeEnum.Refuse)
                $('.AccFinalStateDiv').show();
        }
        makeChosen();
    }
    function setMoreTaskAutos(clear) {
        $("#StartTaskName").unautocomplete();
        $("#NextTaskName").unautocomplete();
        if (clear)
            $('#StartTaskName,#StartTaskID,#NextTaskName,#NextTaskID').val('');
        $('.starttask,.nexttask').hide();
        var type = $('#FactorRole').val(), wftype = 0;
        if (type == FactorRoleEnum.StoreDraft)
            wftype = WorkflowTypeEnum.StoreDraft;
        else if (type == FactorRoleEnum.Purchase)
            wftype = WorkflowTypeEnum.Purchase;
        else if (type == FactorRoleEnum.PurchaseRequest)
            wftype = WorkflowTypeEnum.PurchaseRequest;
        else
            return;
        $('.starttask,.nexttask').show();
        autoCompleteNew({
            table: 'Task', qd: 'starttaskauto', textInput: "#StartTaskName", hiddenInput: "#StartTaskID"
        , moreUrl: 'Type=' + wftype
        });
        autoCompleteNew({
            table: 'Task', qd: 'anytaskauto', textInput: "#NextTaskName", hiddenInput: "#NextTaskID"
            , moreUrl: 'Type=' + wftype
        });
    }

    function setSecondMoreTaskAutos(clear) {
        $("#SecondStartTaskName").unautocomplete();
        $("#SecondNextTaskName").unautocomplete();
        if (clear)
            $('#SecondStartTaskName,#SecondStartTaskID,#SecondNextTaskName,#SecondNextTaskID').val('');
        $('.secondstarttask,.secondnexttask').hide();
        var secondtype = $('#SecondFactorRole').val(), secondwftype = 0;
        if (secondtype == FactorRoleEnum.StoreDraft)
            secondwftype = WorkflowTypeEnum.StoreDraft;
        else if (secondtype == FactorRoleEnum.Purchase)
            secondwftype = WorkflowTypeEnum.Purchase;
        else if (secondtype == FactorRoleEnum.PurchaseRequest)
            secondwftype = WorkflowTypeEnum.PurchaseRequest;
        else
            return;
        $('.secondstarttask,.secondnexttask').show();
        autoCompleteNew({
            table: 'Task', qd: 'starttaskauto', textInput: "#SecondStartTaskName", hiddenInput: "#SecondStartTaskID"
        , moreUrl: 'Type=' + secondwftype
        });
        autoCompleteNew({
            table: 'Task', qd: 'anytaskauto', textInput: "#SecondNextTaskName", hiddenInput: "#SecondNextTaskID"
            , moreUrl: 'Type=' + secondwftype
        });
    }
    function setAccMoreTaskAutos() {
        $("#AccStartTaskName").unautocomplete();
        $("#AccNextTaskName").unautocomplete();
        autoCompleteNew({
            table: 'Task', qd: 'starttaskauto', textInput: "#AccStartTaskName", hiddenInput: "#AccStartTaskID"
            , moreUrl: 'Type=' + WorkflowTypeEnum.Voucher
        });
        autoCompleteNew({
            table: 'Task', qd: 'anytaskauto', textInput: "#AccNextTaskName", hiddenInput: "#AccNextTaskID"
            , moreUrl: 'Type=' + WorkflowTypeEnum.Voucher
        });
    }
    // -------------------------
    // Startup code
    // -------------------------
    mID = toInt($("#MasterID").val(), -1);
    setValue("#State");
    showMoreTasks();
    setChecked('#InheritRoles');
    setChecked('#AllowPrint');
    setValue("#FactorRole");
    setValue("#SecondFactorRole");
    fillTable("get.aspx?op=GetXml&qd=taskviewer&TaskID=" + mID, '#tableItems', addSystemPosition);

    if (mID == -1)
        $('#WorkflowType').val(getServerValue("workflowtype", "Workflow", "ID=" + $('#WorkflowID').val()));

    doMore();
    $("#State").change(showMoreTasks);
    $("#FactorRole").change(function () { setMoreTaskAutos(true); });
    setMoreTaskAutos(false);
    $("#SecondFactorRole").change(function () { setSecondMoreTaskAutos(true); });
    setSecondMoreTaskAutos(false);
    setAccMoreTaskAutos(false);

    function doMore() {
        function autoPostPack(event, data, formatted) {
            $(event.target).next().val(data[0]); // ID
        }
        autoComplete('Roles', 'enumauto1', "#tableItems .rolename", "#RoleID"
       , autoPostPack
       , function (data, i, n, value) {
           return data[1];
       });

        $('.addRole').click(function () {
            var table = $("#tableItems");
            $('tr:last', table).after(addSystemPosition(null));
            Growl.Notice(Lang.RowAddedSuccessfully);
            var lastRow = $('tr:last', table);
            autoComplete('Roles', 'enumauto1', "#tableItems tr:last .rolename", "#RoleID",
                autoPostPack,
                function (data, i, n, value) {
                    return data[1];
                });
            $('.removeRole', lastRow).click(function () {
                RemoveRoleRow($(this));
            });
        });

        $('.removeRole').click(function () {
            RemoveRoleRow($(this));
        });
    }

    insertValidation();
}