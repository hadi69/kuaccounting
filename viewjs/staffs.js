﻿function view_staffs() {
    function doCommand(com, grid) {
        if (com == Lang.GBEdit)
            showCmds();
        else if (com == Lang.GBCopyFish) {
            showcopy();

        }
        else if (com == Lang.GBCopyOrder) {
            showcopyorder();

        }
        else if (com == Lang.GBLockAllFishs)
            doConfirm(function() {
                runQuery('lockfishs', 'Fish');
                Growl.Notice(Lang.LockSuccessfully);
            }, Lang.DoYouWantLockAllFishs);
        else
            generalCommand(com, grid, show);
    }
   
    function run() {
        var more = 'PersonnelGroupID='+ getServerValue('getconfigvalue','Config','&Name=PersonnelGroup');
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=staff&more=' + more + appendTime();

        //alert(_url);
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.Personnel, name: 'Person', sortable: true, css_class: 'width-200' },
                    { display: Lang.PersonneliNum, name: 'PersonneliNum', sortable: true, css_class: 'width-80 align-center' },
                    { display: Lang.Ward, name: 'WardName', sortable: true },
                    { display: Lang.MaritalStatus, name: 'MaritalName', sortable: true, css_class: 'width-80 align-center' },
                    { display: Lang.NumChildren, name: 'NumChildren', sortable: true, css_class: 'width-80 align-center' },
                    { display: Lang.Orders, name: 'Orders', sortable: true, css_class: 'width-80 align-center', process: showOrders },
                    { display: Lang.Fish, name: 'Fish', sortable: true, css_class: 'width-80 align-center', process: showFishs },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, hide: hideColumn("personnel", "personnel", "edit"), css_class: 'action-cell' },
                    { display: Lang.ColDel, name: 'Del', sortable: false, process: delLink, hide: hideColumn("personnel", "personnel", "delete"), css_class: 'action-cell' }
                ],
                buttons:
                [
                    { name: Lang.GBNew, bclass: 'new', onpress: doCommand,  },
                    { name: Lang.GBEdit, bclass: 'edit', onpress: doCommand,  },
                    { name: Lang.GBDel, bclass: 'delete', onpress: doCommand,  },
                    { separator: true },
                    { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand },
                    { name: Lang.GBCopyFish, bclass: 'copy', onpress: doCommand },
                    { name: Lang.GBCopyOrder, bclass: 'copyorder', onpress: doCommand },
                    { name: Lang.GBLockAllFishs, bclass: 'lockallfish', onpress: doCommand }
                ],
                sortname: "PersonneliNum",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight, resizable: gridResizable,
                singleSelect: true,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                }
            });
    }
    function showOrders(celDiv, id)
    {
        $(celDiv).html('<a href="list.aspx?table=Orders&pid=' + id + '"><img src="img/task_icon.jpg" /></a>');
    }
    function showFishs(celDiv, id) {
        $(celDiv).html('<a href="list.aspx?table=Fish&pid=' + id + '"><img src="img/task_icon.jpg" /></a>');
    }
    function showcallback(v, m, f) {
        return newShowcallback(v, m, f, 'staffupdate', 'staffinsert', _Context.table, "#subDetailForm", show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&view=staff&qd=staff&id=' + id + appendTime();
        //alert(2);
        var btns = getDefaultButtons();
        showPopup(_url, showcallback, null, null, successCallback);
    }
    function showcopycallback(v, m, f) {
        if (v=='Confirm') {
            doConfirm(function () {
                if (f.FromYear == '') {
                    f.FromYear='NULL';
                }
                if (f.ToYear == '') {
                    f.ToYear='NULL';
                }
                var more = "&FromYear=" + f.FromYear + "&ToYear=" + f.ToYear + "&FromMonth=" + f.FromMonth + "&ToMonth=" + f.ToMonth;
                runQuery('copyfishs', 'Fish',more);
                Growl.Notice(Lang.CopySuccessfully);
            }, Lang.DoYouWantCopyLastFishs);
        }
    }
    function showcopy(id, celDiv, successCallback) {
        var _url = 'get.aspx?op=GetHtml&view=copyfish'+ appendTime();
        var btns = {};
        btns[Lang.Confirm] = 'Confirm';
        btns[Lang.Cancel] = 'Cancel';
        showPopup(_url, showcopycallback, btns, null, successCallback);
    }
    function showcopyordercallback(v, m, f) {
        if (v == 'Confirm') {
            var valid = validateInputs(null, f);
            if (!valid)
                return false;
            if (_Context.isDemo) {
                alert(Lang.DemoVersionNoSave);
                return false;
            }
            doConfirm(function () {
                
                if (f.Title == '') {
                    f.Title = 'NULL';
                }
                if (f.PersonnelID == '') {
                    f.PersonnelID = 'NULL';
                }
                if (f.ToPersonnelID == '') {
                    f.ToPersonnelID = 'NULL';
                }
                var more = "&Title=" + f.Title + "&PersonnelID=" + f.PersonnelID +"&ToPersonnelID="+f.ToPersonnelID;
                runQuery('copyorders', 'Orders', more);
                Growl.Notice(Lang.CopySuccessfully);
            }, Lang.DoYouWantCopyLastOrders);
        }
    }
    function showcopyorder(id, celDiv, successCallback) {
        var _url = 'get.aspx?op=GetHtml&view=copyorder' + appendTime();
        var btns = {};
        btns[Lang.Confirm] = 'Confirm';
        btns[Lang.Cancel] = 'Cancel';
        showPopup(_url, showcopyordercallback, btns, null, successCallback);
    }
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.Staff, parentTitle: Lang.MSalary, parentUrl: 'menu/salary.aspx' } }));
    run();
}