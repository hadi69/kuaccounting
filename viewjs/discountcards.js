﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
92-10-29: 
    - file created
*/
function view_discountcards() {
    function doCommand(com, grid) {
            generalCommand(com, grid, show);
    }

    function run() {
        var _url = 'service.asmx/GetList?table=DiscountCard&qd=discountcard' + appendTime();
        $("#flexi").flexigrid
        (
        {
            url: _url,
            dataType: 'xml',
            colModel:
            [
                { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                { display: Lang.User, name: 'UserName', sortable: true, css_class: 'width-70 align-center' },
                { display: Lang.Vendor, name: 'VendorName', sortable: true, css_class: 'width-70 align-center' },
                { display: Lang.TotalAmount, name: 'Num', sortable: true, css_class: 'width-80 align-center', footer: 'sum' },
                { display: Lang.TotalPrice, name: 'TotalPrice', sortable: true, process: showMonetary, css_class: 'width-120 align-center', footer: 'sum' }
            ],
            //buttons:
            //[
            //{ name: Lang.GBNew, bclass: 'new', onpress: doCommand }, // onaccess: hasAccess("sales", "basket", "new") },
            //{ name: Lang.GBEdit, bclass: 'edit', onpress: doCommand }, // onaccess: hasAccess("sales", "basket", "edit") },
            //{ name: Lang.GBDel, bclass: 'delete', onpress: doCommand }, // onaccess: hasAccess("sales", "basket", "delete") },
            //{ separator: true },
            //{ name: Lang.GBExcel, bclass: 'excel', onpress: doCommand },
            //],
            searchitems: null,
            sortname: "UserName",
            sortorder: "desc",
            usepager: true,
            useRp: true,
            rp: gridRp,
            showTableToggleBtn: gridToggle,
            width: gridWidth,
            height: gridHeight, resizable: gridResizable,
            singleSelect: false
        });
    }

    function show(id, celDiv, successCallback) {
        var btns = {};
            btns = getCloseButtons();

        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=DiscountCard&qd=discountcard&id=' + id;
        _url += appendTime();
        showPopup(_url, showcallback, btns, null, successCallback, true);
    }
    function showcallback(v, m, f) {
        if (v == 'Print') {
            applyPrintFields('DiscountCard');
            printSubform();
            return false;
        }
        if (v == 'Cancel')
            return true;

        return newShowcallback(v, m, f, 'discountcardupdate', 'discountcardinsert', 'DiscountCard', null, show);
    }
    // ------------------------------
    // startup code
    // ------------------------------
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.DiscountCard } }));
    setClassSales();
    dateSelect('.dateSelect');
    run();
}