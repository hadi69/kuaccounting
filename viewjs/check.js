﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
92-10-19: file created
*/
function view_check() {
    if (_Context.id != -1) {
        replacePrintButton({
            primary: {
                text: Lang.CheckReceipt,
                href: Router.Generate("print-checkreceipt", { params: { id: _Context.id }, query: { print: "true" } })
            }
        });
    } else {
        replacePrintButton({
        });
    }
    makeChosen();
    $("#Serial").focus();
    function changeReciveDest() {
        if ($("#FinalDate").val() != '')
            $("#ReceiveDest").attr('disabled', false);
        else {
            $("#ReceiveDest").val('');
            $("#ReceiveDest").attr('disabled', true);
        }
    }
    function displayAmount() {
        $('#AmountStr').html(numToString($('#Amount').val()));
    }
    function lastValues() {
        var id = toInt($('#subDetailForm #PersonnelID').val(), 0);
        if (id <= 0)
            return;
        var u = 'get.aspx?op=Auto&table=Checks&qd=checkslast&q=' + id + appendTime();
        var row = getServerValues(u);
        if (row && row.length && row.length > 2) {
            // AccountNo, Bank, Branch
            $('#subDetailForm #AccountNo').val(row[0]);
            $('#subDetailForm #BankAccountID').val(row[1]);
            $('#subDetailForm #BankAccountName').val(row[2]);
            $("#subDetailForm #Branch").val(row[3]);
        }
    }

    function showDivs() {
        if ($('#dueD').val() == 1) {
            $("#subDetailForm #DueTreePathName").val('');
            $('#dueDiv').show();
            $('#personDiv').hide();
            $('#oweDiv').hide();
            $('#visitorDiv').hide();
        }
        else if ($('#oweD').val() == 1) {
            $("#subDetailForm #OweTreePathName").val('');
            $('#personDiv').hide();
            $('#oweDiv').show();
            $('#dueDiv').hide();
            $('#visitorDiv').hide();
        }
       else if ($('#personD').val() == 1) {
           $("#subDetailForm #[OweTreePathName").val('');
            $('#oweDiv').show();
            $('#personDiv').show();
            $('#dueDiv').hide();
            $('#visitorDiv').hide();
        }
        else if ($('#duepersonD').val() == 1) {
            $("#subDetailForm #DueTreePathName").val('');
            $('#dueDiv').show();
            $('#personDiv').show();
            $('#oweDiv').hide();
            $('#visitorDiv').hide();
        }
        else if ($('#owedueD').val() == 1) {
            $("#subDetailForm #DueTreePathName").val('');
            $("#subDetailForm #OweTreePathName").val('');
            $('#dueDiv').show();
            $('#personDiv').hide();
            $('#oweDiv').show();
            $('#visitorDiv').hide();
        }
        else if ($('#visitorD').val() == 1) {
            //$("#subDetailForm #[OweTreePathName").val('');
            $('#oweDiv').hide();
            $('#personDiv').hide();
            $('#dueDiv').hide();
            $('#visitorDiv').show();
        }
        else {
            $('#oweDiv').hide();
            $('#personDiv').hide();
            $('#dueDiv').hide();
            $('#visitorDiv').hide();
        }
    }

    function bindVouchers() {
        // Vouchers
        var _autourl = 'get.aspx?op=Auto&table=DocumentAccounting&qd=checkvouchers&q=' + $('#ID').val();
        var res = getRowtable(_autourl);
        if (!res[0]) {
             _autourl = 'get.aspx?op=Auto&table=DocumentAccounting&qd=checkvouchers_old&q=' + $('#ID').val();
             res = getRowtable(_autourl);
        }
        if (res && res.length > 0) {
            $('#Voucher1').attr('href', "list.aspx?table=DocumentAccounting&id=" + res[0][0] + "&itemid=" + res[0][2]);
            $('#Voucher1').html(res[0][1]);
        }
        if (res && res.length > 1) {
            $('#Voucher2').attr('href', "list.aspx?table=DocumentAccounting&id=" + res[1][0] + "&itemid=" + res[1][2]);
            $('#Voucher2').html(res[1][1]);
        }
        if (res && res.length > 2) {
            $('#Voucher3').attr('href', "list.aspx?table=DocumentAccounting&id=" + res[2][0] + "&itemid=" + res[2][2]);
            $('#Voucher3').html(res[2][1]);
        }
    }
    function checkPersonCredit() {
        var personnelID = toInt($('#subDetailForm #PersonnelID').val(), 0);
        if (toInt($('#subDetailForm #AssignorID').val(), 0) > 0) {
            personnelID = toInt($('#subDetailForm #AssignorID').val(), 0);
        }
        $('#creditmsg').html('');
        if (personnelID > 0)
        {
            var url = 'get.aspx?op=Auto&table=Personnel&qd=personcheckcredit&PersonnelID=' + personnelID + appendTime();
            var row = getServerValues(url);
            var checkCredit = toFloat(row[0], 0);
            var amount = toFloat(row[1], 0);
            if (checkCredit > 0) {
                 amount += toFloat($('#Amount').val(), 0);
                if (checkCredit < amount)
                    $('#creditmsg').html(Lang.LowCheckCredit);//checkCredit
            }
        }
    }
    function checkVisitorCredit() {
        var visitorID = toInt($('#subDetailForm #VisitorID').val(), 0);
        $('#visitorcreditmsg').html('');
        if (visitorID > 0) {
            var url = 'get.aspx?op=Auto&table=Personnel&qd=visitorcheckcredit&VisitorID=' + visitorID + appendTime();
            var row = getServerValues(url);
            var checkCredit = toFloat(row[0], 0);
            var amount = toFloat(row[1], 0);
            if (checkCredit > 0) {
                amount += toFloat($('#Amount').val(), 0);
                if (checkCredit < amount)
                    $('#visitorcreditmsg').html(Lang.VisitorLowCheckCredit);//visitorcheckCredit
            }
        }
    }

    // ------------------------------
    // startup code
    // ------------------------------
    setValue('#Status');
    $('#bankDiv').hide();
    setValue('#ReceiveDest');
    dateSelect('.dateSelect');
    if (!hasValuePositive($('#UserID').val()))
        $('#UserID').val(_Context.userID);
    if ($("#IssueDate").val() == '')
        $("#IssueDate").val(_Context.today);
    if ($("#FinalDate").val() == '')
        $("#ReceiveDest").attr('disabled', true);

    autoComplete('Personnel', 'personnelselectauto', '#subDetailForm #PersonnelName', '#subDetailForm #PersonnelID', lastValues
        , function (data, i, n, value) {
            $('#subDetailForm #PersonnelID').val('');
            return formatPersonnelItem(data);
        });
    autoComplete('Personnel', 'personnelselectauto', '#subDetailForm #ReceiverPerson', '#subDetailForm #ReceiverPersonID', lastValues
        , function (data, i, n, value) {
            $('#subDetailForm #ReceiverPersonID').val('');
            return formatPersonnelItem(data);
        });

        autoComplete('Personnel', 'personnelselectauto', '#subDetailForm #AssignorName', '#subDetailForm #AssignorID', lastValues
        , function (data, i, n, value) {
            $('#subDetailForm #AssignorID').val('');
            return formatPersonnelItem(data);
        });


    //autoComplete('Trees', 'treestypedauto', '#BankName', '#BankTreePath'
    //          , function (event, data, formatted) {
    //              // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due
    //              var hasChild = data[5] != '';
    //              if (hasChild) {
    //                  $('#BankName, #BankTreePath').val('');
    //                  alert(Lang.HasChild);
    //              }
    //          }, formatTreePath, null, null, 'get.aspx?op=Auto&table=Trees&qd=treestypedauto&Type=6' + appendTime());

     autoComplete('BankAccountCheck', 'bankaccountautoforchecks', '#BankAccountName', '#BankAccountID'
       , function (event, data, formatted) {
        $('#BankName').val(data[3]);
        $('#BankTreePath').val(data[2]);

       });

    autoComplete('Trees', 'treesbanksauto', '#ReceiverBankName', '#ReceiverBankTreePath'
          , function (event, data, formatted) {
              // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due
              var hasChild = data[5] != '';
              if (hasChild) {
                  $('#ReceiverBankName, #ReceiverBankTreePath').val('');
                  alert(Lang.HasChild);
              }
          }, formatTreePath, null, null);

    autoCompleteFree('Personnel', 'visitorauto', '#VisitorName', '#VisitorID', null
        , function (data, i, n, value) {
            //$('#VisitorID').val('');
            return formatPersonnelItem(data);
        });

    autoCompleteTreePath('#subDetailForm #TreePathName', '#subDetailForm #TreePath', '#subDetailForm #selTree');
    autoCompleteTreePath('#subDetailForm #DueTreePathName', '#subDetailForm #DueTreePath', '#subDetailForm #selDueTree');
    autoCompleteTreePath('#subDetailForm #OweTreePathName', '#subDetailForm #OweTreePath', '#subDetailForm #selOweTree');
    lastValues();

    showDivs();

    $("#FinalDate").blur(function () { changeReciveDest();});
    $("#Amount").blur(checkPersonCredit);
    $("#VisitorName").blur(checkVisitorCredit);
    if (!hasValuePositive($('#ID').val()))
        $('#Status, #HiddenStatus').val(CheckStatusEnum.New);
    if ($('#setstatus').val() == CheckStatusEnum.Received) {
        $('#Status, #HiddenStatus').val(CheckStatusEnum.Received);
    }
    else if ($('#setstatus').val() == CheckStatusEnum.Refused) {
        $('#Status, #HiddenStatus').val(CheckStatusEnum.Refused);
    }
    else if ($('#setstatus').val() == CheckStatusEnum.Sleep) {
        $('#Status, #HiddenStatus').val(CheckStatusEnum.Sleep);
        $('#bankDiv').show();
    }
    else if ($('#setstatus').val() == CheckStatusEnum.Expend) {
        $('#Status, #HiddenStatus').val(CheckStatusEnum.Expend);
    }
    else if ($('#setstatus').val() == CheckStatusEnum.New && $('#setstatus').val() != '') {
        $('#Status, #HiddenStatus').val(CheckStatusEnum.New);
    }
    else if ($('#setstatus').val() == CheckStatusEnum.SleepReceive) {
        $('#Status, #HiddenStatus').val(CheckStatusEnum.SleepReceive);
        $('#bankDiv').show();
    } else if ($('#setstatus').val() == CheckStatusEnum.Visitor) {
        $('#Status, #HiddenStatus').val(CheckStatusEnum.Visitor);
    } else {
        if (hasValuePositive($('#ReceiveTreeID').val()))
            $('#Receive').show();
        else
            $('#Receive').hide();
    }
    changeReciveDest();

    bindVouchers();

    insertValidation('#subDetailForm');
    monetaryInputsSet('#subDetailForm');
    commaInputs('#subDetailForm');
    dateSelect('.dateSelect');

    $('#Amount').blur(displayAmount).keyup(displayAmount);
    displayAmount();
    if ($('#Status').val() == CheckStatusEnum.Visitor) {
        $('#visitorDiv').show();
    }
    var id = $('#ID').val();
    if (id > 0) {
        uploadify('#ChooseFile', '#FileName', '#StartUpload', 'checks/' + id
                   , function (fileName) {
                       $('#FileNameDisplay').html(fileName);
                   });
        $('#FileLink').html("<a href='files/checks/" + id + "/" + $('#FileName').val() + "' target=_blank>" + $('#FileName').val() + "</a>");
    }
    else
        $('.choosefile').hide();
}