﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
93-05-14: file created
*/
function view_fields() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var terminology = getTerminology(_Context.table);
        if (terminology)
            $('#term').html("<a href='help/terminology.aspx' target=_blank title='" + Lang.HasTerminology + "'>" + terminology + "</a>");
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.ETitle, name: 'Name', sortable: true, css_class: 'width-130' },
                    { display: Lang.PTitle, name: 'Title', sortable: true, css_class: 'width-190'},
                    { display: Lang.FieldType, name: 'FieldType', sortable: true, css_class:'width-50', process:displayFieldType},
                    { display: Lang.Formula, name: 'Formula', sortable: true, css_class: 'width-400 ltr formula' },
                    { display: Lang.DecimalPlaces, name: 'DecimalPlaces', sortable: true, css_class: 'width-110' },
                    { display: Lang.Used, name: 'Used', sortable: true, css_class: 'width-80', process:displayUsed },
                    { display: Lang.Comments, name: 'Comments', sortable: true},
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' },
                    { display: Lang.ColDel, name: 'Del', sortable: false, process: delLink, css_class: 'action-cell' }
                ],
                buttons:
                [
                    { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                    { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                    { separator: true },
                    { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                    { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                sortname: "Title",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    debugger;
                    var id = row.id.substring(3);
                    show(id);
                }
            }
            );
    }
    function displayFieldType(celDiv, id) {
        var fieldtype = parseInt($(celDiv).html(), 0);
        if (fieldtype == 1) {
            $(celDiv).html(Lang.Numeric);
        }
        else if (fieldtype == 2) {
            $(celDiv).html(Lang.Boolean);
        }
        else if (fieldtype == 3) {
            $(celDiv).html(Lang.Enum);
        }
        else if (fieldtype == 4) {
            $(celDiv).html(Lang.RealEnum);
        }
        else if (fieldtype == 5) {
            $(celDiv).html(Lang.Formula);
        }
        else if (fieldtype == 6) {
            $(celDiv).html(Lang.Query);
        }
        else {
            $(celDiv).html(Lang.UnknownError);
            $(celDiv).addClass("unknown");
        }
    }
    function displayUsed(celDiv, id) {
        var used = parseInt($(celDiv).html(), 0);
        if (used == 0) {
            $(celDiv).html(Lang.Orders);
        }
        else if (used == 1) {
            $(celDiv).html(Lang.Fish);
        }
        else if (used == 2) {
            $(celDiv).html(Lang.Both);
        }
        else {
            $(celDiv).html(Lang.UnknownError);
            $(celDiv).addClass("unknown");
        }
    }
    function showcallback(v, m, f) {
        return newShowcallback(v, m, f, 'fieldupdate', 'fieldinsert', _Context.table, null, show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    $("#bar").prepend(breadcrumb({ title: _Context.title, grandParent: { title: Lang.MSalary, url: 'menu/salary.aspx' }, parent: { title: Lang.SalaryDefinition, url: 'menu/salarydefinition.aspx' } }));
    setClassSalary();
    run();
}