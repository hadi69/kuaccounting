﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-10-19: file created
*/
function view_monetaryunits() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        //alert(_url);
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false,  hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.Title, name: 'Title', sortable: true},
                    { display: Lang.Abbreviation, name: 'Abbreviation', sortable: true, css_class: 'width-300'},
                    { display: Lang.ColorChoice, name: 'Color', sortable: true, process: showColor, css_class: 'color-cell', hide: true },
                    { display: Lang.Enabled, name: 'Enabled', sortable: true,  process: showBool, css_class: 'align-center width-40' },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false,  process: function (celDiv, id) { editLink(celDiv, id, show) }, css_class: 'action-cell' },
                    { display: Lang.ColDel, name: 'Del', sortable: false,  process: delLink, css_class: 'action-cell' }
                ],
                buttons:
                [
                    { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                    { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                    { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                sortname: "Title ",
                sortorder: "asc",
                usepager: true,
                //title: _Context.title,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,

                height: gridHeight, resizable: gridResizable,
                singleSelect: false
            }
            );
    }
    function showcallback(v, m, f) {
        return newShowcallback(v, m, f, 'monetaryunitupdate', 'monetaryunitinsert', _Context.table, null, show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, function () {
            jscolor.init();
            if (successCallback)
                successCallback();
        });
    }

    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['sys,monetaryunit']);
   // checkLevel(3);
    $('#ptitle').html("<a href='menu/admin.aspx'>" + Lang.Management + "</a> :: " + _Context.title);
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.MonetaryUnit, parentUrl: "menu/admin.aspx", parentTitle: Lang.Management } }));
    setClassSys();
    run();
}