﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
92-11-08: file created
*/
function view_cardex() {
    // ------------------------------
    // startup code
    // ------------------------------
    var id = $("#SSDefID").val();
    var perStoreService = toInt($('#PerStoreService').val(), -1);
    var ssID = $("#SSID").val();
    if (!ssID)
        ssID = 0;
    var cardexOrderByMainOtherDate = toInt(getSettings('CardexOrderByMainOtherDate'), 0);
    var order = "ID";
    var qry = "cardex";
    if (perStoreService === 1) {
        qry = "cardexstoreservice";
    }
    if (cardexOrderByMainOtherDate === 1) {
        order = "MainOtherDate";
        qry = "cardexperdate";
        if (perStoreService === 1) {
            qry = "cardexstoreserviceperdate";
        }
    }
    var _url = $('#RELPATH').val() + 'service.asmx/GetList?table=Cardex&qd=' + qry + '&StoreServiceDefID=' + id + '&StoreServiceID=' + ssID + appendTime();
    $("#scondFlexi").flexigrid
               (
               {
                   url: _url,
                   dataType: 'xml',
                   colModel:
                   [
                       { display: 'ID', name: 'ID', hide: true },
                        { display: 'StoreTitle', name: 'StoreTitle', hide: true, },
                        { display: 'StoreServiceID', name: 'StoreServiceID', hide: true },
                        { display: Lang.Commodity, name: 'StoreServiceName', hide: true },
                        { display: 'OtherID', name: 'OtherID', hide: true },
                        { display: Lang.Date, name: 'Date', css_class: 'date-cell', sortable: true, process: capturePriceDate },
                        { display: Lang.Input, name: 'Income', css_class: 'width-80 align-center', sortable: true },
                        { display: Lang.Output, name: 'Outcome', css_class: 'width-80 align-center', sortable: true },
                        { display: Lang.Remained, name: 'TotalRemaind', css_class: 'width-80 align-center', sortable: true },
                        { display: Lang.Price, name: 'Price', css_class: 'width-100 align-center', sortable: true, process: showMonetary },
                        { display: Lang.ExpirationDate, name: 'ExpireDate', css_class: 'date-cell', sortable: true},
                        { display: Lang.Comments, name: 'Comments', sortable: false, css_class: 'width-100 align-center' },
                        { display: Lang.Type, name: 'Type', css_class: 'width-100 align-center', sortable: true, process: showCardexType },
                        { display: Lang.No, name: 'NOSerial', css_class: 'width-80 align-center', sortable: true, process: showNo },
                        { display: Lang.SavedNO, name: 'SavedNO', hide: true, process: showSavedNO },
                        { display: Lang.FactorDate, name: 'OtherDate', css_class: 'width-80 align-center', sortable: true },
                        { display: 'MainOtherDate', name: 'MainOtherDate', hide: true },
                        { display: 'OtherMasterID', name: 'OtherMasterID', hide: true, process: showId },
                        { display: Lang.SellerOrBuyerName, name: 'SellerOrBuyerName', sortable: true },
                        { display: Lang.ColDel, name: 'Delete', sortable: false, hide: _Context.userName != 'jp', process: function (celDiv, id) { delLink(celDiv, id, "Cardex", "#scondFlexi"); }, css_class: 'action-cell' },
                        { display: Lang.ColEdit, name: 'Edit', sortable: false, hide: _Context.userName != 'jp', process: function (celDiv, id) { editLink(celDiv, id, showCardexDetail); }, css_class: 'action-cell' }
                    ],
                   searchitems: null,
                   sortname: order,
                   sortorder: "desc",
                   usepager: true,
                   useRp: true,
                   rp: gridRp,
                   showTableToggleBtn: gridToggle,
                   showToggleBtn: true,
                   width: gridWidth,

                   height: gridHeight, resizable: gridResizable,
                   singleSelect: false,
                   onSuccess: function (grid) {
                       createLink();
                   }
               }
               );
    function showCardexCallback(v, m, f) {
        var res = newShowcallback(v, m, f, 'cardexupdate', 'cardexinsert', 'Cardex', '#subDetailForm', showCardexDetail, null, null);
        setTimeout(function () { $("#scondFlexi").flexReload(); }, 100);
        return res;
    }
    function showCardexDetail(id, celDiv, successCallback) {
        var storeid = $.urlParam('id');
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=Cardex&view=cardexdetail&qd=cardexdetail&id=' + id + appendTime();
        var btns = {};
        btns[Lang.OK] = 'OK';
        btns[Lang.Close] = 'Cancel';
        showPopup(_url, showCardexCallback, btns, null, successCallback);
    }
    function showId(celDiv) {
        $(celDiv).addClass('_id');
    }

    function showNo(celDiv) {
        $(celDiv).addClass('_no');
    }
    function showSavedNO(celDiv) {
        $(celDiv).addClass('_SavedNO');
    }

    function createLink() {
        setTimeout(function () {
        $('#scondFlexi tr').each(function () {
            var _id = $('._id', this).html();
            var _no = $('._no', this).html();
            if (!_no || _no.trim() == '') {
                $('._no', this).text($('._SavedNO', this).html());
            }
            else
                if (_id && _id != '' && _id.trim() != '') {
                    if ($('._pType', this).html())
                        $('._no', this).html("<a href='list.aspx?table=Purchase&id=" + _id + "'>" + $('._no', this).html() + "</a>");
                    else if ($('._sType', this).html())
                        $('._no', this).html("<a href='list.aspx?table=StoreDraft&id=" + _id + "'>" + $('._no', this).html() + "</a>");
                    else if ($('._tType', this).html())
                        $('._no', this).html("<a href='list.aspx?table=Transfer&id=" + _id + "'>" + $('._no', this).html() + "</a>");
                    else if ($('._rnType', this).html())
                        $('._no', this).html("<a href='list.aspx?table=Returns&id=" + _id + "'>" + $('._no', this).html() + "</a>");
                    else if ($('._reType', this).html())
                        $('._no', this).html("<a href='list.aspx?table=Refuse&id=" + _id + "'>" + $('._no', this).html() + "</a>");
                }

            if (!$('._oType', this).html())
                $('td:last', this).html("");
            });
        }, 500);
    }
    // createLink();
}