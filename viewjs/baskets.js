﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
92-10-29: 
    - file created
*/
function view_baskets() {
    var startupID = -1;
    function doCommand(com, grid) {
        if (com == Lang.GBStoreDraft) {
            createStoreDraft(grid);
        }
        else if (com == Lang.VendorMsgToUsers) {
            var ids = '';
            var id = '';
            $('.trSelected', grid).each(function () {
                id = $(this).attr('id');
                id = id.substring(id.lastIndexOf('row') + 3);
                ids += id + ',';
            });
            if (ids == '')
                return;
            ids = ids.substr(0, ids.length - 1);
                var btns = getButtons();
                var op = "GetHtml";
                var _url = 'get.aspx?op=' + op + '&table=VendorsMessages&qd=vendorsmessagesinsert&view=vendorsmessages&BasketsIDs=' + ids + appendTime();
                showPopup(_url, writMsg, btns, '1%', null, true);
        }
        else 
            generalCommand(com, grid, show);
    }

    function run() {
        var _url = 'service.asmx/GetList?table=basket&qd=basket' + appendTime();
        $("#flexi").flexigrid
        (
        {
            url: _url,
            dataType: 'xml',
            colModel:
            [
                { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                { display: Lang.User, name: 'UserName', sortable: true, css_class: 'width-70 align-center' },
                { display: Lang.Code, name: 'Code', css_class: 'width-70 align-center' },
                { display: Lang.DeliveryTime, name: 'DeliveryTime', sortable: true },
                { display: Lang.SendOption, name: 'SendOption', sortable: true, css_class: 'width-100' },
                { display: Lang.BasketDate, name: 'BasketDate', sortable: true, hide: true, css_class: 'date-cell' },
                { display: Lang.TotalAmount, name: 'TotalNum', sortable: true, css_class: 'width-80 align-center', footer: 'sum' },
                { display: Lang.TotalPrice, name: 'TotalPrice', sortable: true, process: showMonetary, css_class: 'width-120 align-center', footer: 'sum' },
                { display: Lang.Status, name: 'Status', sortable: true, process: _displayBasketStatus, css_class: 'width-70 align-center' },
                { display: Lang.PayStatus, name: 'PayStatus', sortable: true, process: _displayBasketPayStatus, css_class: 'width-70 align-center' },
                { display: Lang.PayMethod, name: 'PayMethod', sortable: true, process: _displayBasketPayMethod, css_class: 'width-70 align-center' },
                { display: Lang.StoreDraftID, name: 'StoreDraftID', hide: true, process: displayStoreDraftID },
                { display: Lang.StoreDraft, name: 'StoreDraftTitle', sortable: false, process: displayStoreDraftTitle, hide: _Context.isMobile, css_class: 'width-80 align-center' },
                { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function(celDiv, id) { editLink(celDiv, id, show); }, hide: hideColumn("sales", "basket", "edit"), css_class: 'action-cell' }
            ],
            buttons:
            [
            { name: Lang.GBNew, bclass: 'new', onpress: doCommand }, // onaccess: hasAccess("sales", "basket", "new") },
            { name: Lang.GBEdit, bclass: 'edit', onpress: doCommand }, // onaccess: hasAccess("sales", "basket", "edit") },
            { name: Lang.GBDel, bclass: 'delete', onpress: doCommand, onaccess: _Context.isAdmin }, // onaccess: hasAccess("sales", "basket", "delete") },
            { separator: true },
            { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand },
            { separator: true },
            { name: Lang.GBStoreDraft, bclass: 'storedraft', onpress: doCommand, onaccess: hasAccess("sales", "storedraft", "new") && hasGeneralAccess("new") },
                   { name: Lang.VendorMsgToUsers, bclass: 'VendorMsgToUsers', onpress: doCommand }
                   ],
                searchitems: null,
                sortname: "BasketDate",
                sortorder: "desc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight, resizable: gridResizable,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                },
                onSuccess: function (grid) {
                    postProcessGrid();
                    if (startupID != -1) {
                        show(startupID);
                        startupID = -1;
                    }
                }
            });
    }
    function displayStoreDraftID(celDiv, id) {
        $(celDiv).addClass('_sid');
    }
 
    function displayStoreDraftTitle(celDiv, id) {
        $(celDiv).addClass('_sno');
    }
 
    function postProcessGrid()
    {
        setTimeout(function () {
            showStoreDraftLinks();
        }, 500);
    }
    function showStoreDraftLinks() {
        $('#flexi tr').each(function () {
            var _sid = $('._sid', this).html();
            //alert(_id);
            if (_sid && _sid != '') {
                $('._sno', this).html("<a href='list.aspx?table=StoreDraft&id=" + _sid + "'>" + $('._sno', this).html() + "</a>");
            }
        });
    }
    function _displayBasketStatus(celDiv, id) {
        var type = toInt($(celDiv).html(), 0);
        displayBasketStatus(celDiv, type);
    }
    function _displayBasketPayStatus(celDiv, id) {
        var type = toInt($(celDiv).html(), 0);
        displayBasketPayStatus(celDiv, type);
    }
    function _displayBasketPayMethod(celDiv, id) {
        var type = toInt($(celDiv).html(), 0);
        displayBasketPayMethod(celDiv, type);
    }
    function show(id, celDiv, successCallback, desiredEntryTaskID) {
        var btns = {};
        if (id <= 0) {
            btns = getCloseButtons();
        } else {
            btns[Lang.OK] = 'OK';
            btns[Lang.Confrim] = 'Confrim';
            btns[Lang.BasketStatusCanceled] = 'BasketStatusCanceled';
            btns[Lang.Close] = 'Cancel';
        }

        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=Basket&qd=basket&id=' + id;
        _url += appendTime();
        showPopup(_url, showcallback, btns, null, successCallback, true);
    }
    function showcallback(v, m, f) {
        if (v == 'Print') {
            applyPrintFields('Basket');
            printSubform();
            return false;
        }
        var ok = false;
        var status = toInt($('#HiddenStatus').val(), -1);
        if (v == 'Confrim') {
            if (status == 1) {
                ok = runQuery('basketconfirm', 'Basket', '&BasketID=' + $('#MasterID').val() + '&VendorMsg=' + $('#VendorMsg').val());
                if (ok) {
                    Growl.Notice(Lang.Success, 3000);
                } else {
                    Growl.Notice(Lang.Failed, 3000);
                }
            } else {
                Growl.Notice(Lang.BasketStatusMsg, 3000);
                return false;
            }
            $("#flexi").flexReload();
            return true;
        }
        if (v == 'BasketStatusCanceled') {
            if (status == 1) {
                ok = runQuery('basketcanceled', 'Basket', '&BasketID=' + $('#MasterID').val() + '&VendorMsg=' + $('#VendorMsg').val());
                if (ok) {
                    Growl.Notice(Lang.Success, 3000);
                } else {
                    Growl.Notice(Lang.Failed, 3000);
                }
            } else {
                Growl.Notice(Lang.BasketStatusMsg, 3000);
                return false;
            }
            $("#flexi").flexReload();
            return true;
        }
        if (v == 'Cancel')
            return true;

        var qry = (_Context.id == -1) ? 'basketinsert' : 'basketupdate';
        return newMastercallback(v, m, f, 'Basket', qry, 'BasketItem'
            , 'basketitemupdate', 'basketiteminsert', 'basketitemdelete', 'ServicePriceID', null, show, null, null);
    }
    
    function createStoreDraft(grid) {
        var ids = '';
        $('.trSelected', grid).each(function () {
            id = $(this).attr('id');
            id = id.substring(id.lastIndexOf('row') + 3);
            ids += id + ',';
        });
        if (ids == '')
            return;
        ids = ids.substr(0, ids.length - 1);
        var storeDraftID = getServerValue("basket2storedraft", 'StoreDraft', 'ids=' + ids);
        if (!storeDraftID)
            displayError(Lang.Basket2StoreDraftError);
        else
            displayError(Lang.Basket2StoreDraftSuccess + "<br /><a href='list.aspx?table=StoreDraft&id=" + storeDraftID + "'>" + Lang.Basket2StoreDraftShow + "</a>", null, Lang.Success);
    }

    function writMsg(v, f, m) {
        if (v == 'OK') {
            if (!$('#VendorMessage').val()) {
                Growl.Notice(Lang.EmptyMsg, 3000);
                return false;
            }
            var more = '&BasketsIDs=' + $('#BasketsIDs').val() + '&VendorMessage=' + $('#VendorMessage').val();
            var vendorsMassagesId = getServerValue("vendorsmessagesinsert", 'VendorsMessages', more);
            if (!vendorsMassagesId) {
                Growl.Notice(Lang.Failed, 3000);
                $("#flexi").flexReload();
                return true;
            }
            Growl.Notice(Lang.Success, 3000);
            $("#flexi").flexReload();
            return true;
        }
        $("#flexi").flexReload();
        return true;
    }

    // ------------------------------
    // startup code
    // ------------------------------
   // checkAccess(['sales,basket']);
    //checkLevel(2);
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.Basket} }));
    setClassSales();
    dateSelect('.dateSelect');
    startupID = $.urlParam('id');
    if (!startupID)
        startupID = -1;
    $('#chprint').click(function () { choosePrintFields(_Context.table); });
    run();
}