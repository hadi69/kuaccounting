﻿/*
93-06-1: file created by zahir
*/
function view_Rule86() {
    autoComplete('Personnel', 'personnelselectauto', '#PersonnelName', '#PersonnelID');
    autoComplete('OrderType', 'enumauto', '#OrderTypeName', '#OrderTypeID');
    //autoComplete('OrderDefs', 'enumauto', '#OrderDef', '#OrderDefID');
    autoComplete('Category', 'enumauto', '#CategoryName', '#CategoryID');
    autoComplete('Field', 'enumauto', '#FieldName', '#FieldID');
    autoComplete('Post', 'enumauto', '#PostName', '#PostID');
    autoComplete('EmpType', 'enumauto', '#EmpTypeName', '#EmpTypeID');
    autoComplete('HozeMKhdmat', 'enumauto', '#HozeMKhdmatName', '#HozeMKhdmatID');
    autoComplete('WorkRank', 'enumauto', '#WorkRankName', '#WorkRankID');
    autoComplete('ManageLevel', 'enumauto', '#ManageLevelName', '#ManageLevelID');
    autoComplete('ManageType', 'enumauto', '#ManageTypeName', '#ManageTypeID');
    autoComplete('Province', 'enumauto', '#ProvinceMkhdmatName', '#ProvinceMKhdmatID');
    autoComplete('City', 'enumauto', '#MKhdmatName', '#MKhdmatID');
    autoComplete('OrganizationUnit', 'enumauto', '#OrganizationUnitName', '#OrganizationUnitID');
    autoComplete('Post', 'enumauto', '#ResponsiblePostName', '#ResponsiblePostID');
    dateSelect('.dateSelect');
    setValue('#marital');
    $('.formula').attr('readonly', true);
    insertValidation();
}