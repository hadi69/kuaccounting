﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-10-19: file created
*/
function view_checks() {
    var startupID = -1;
    function doCommand(com, grid) {
        if (com == Lang.ShowReceived) //
        {
            if ($('.showReceived').hasClass('glyphicon-ok')) {
                $('.showReceived').removeClass('glyphicon glyphicon-ok');
            } else {
                $('.showReceived').toggleClass('glyphicon glyphicon-ok');
            }
            var more = '__PrOgrAmconD__ AND Status NOT IN(1,5,4) AND (IsShop=0 OR IsShop IS NULL)';
            if ($('.showReceived').hasClass('glyphicon-ok')) {
                more = '__PrOgrAmconD__  AND (IsShop=0 OR IsShop IS NULL)';
            }
            var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' +_Context.query + '&more=' + more + appendTime();
            $("#flexi").flexOptions({ url: _url });
            $('#fRefresh').click();
            return;
        }
        generalCommand(com, grid, show, _Context.title, "ID", "Status=0^CheckStatusNew^1^CheckStatusReceived^2^CheckStatusRefused^3^CheckStatusSleep^4^CheckStatusExpend^5^CheckStatusSleepReceive");
    }
    function showSumAmount() {
        var val = getServerValueFiltered("checkssumamount", "Checks");
        if (!val)
            val = 0;
        $('#SumAmount').html(numberWithCommas(toMonetary(val, _Context.today)));
    }
    function run() {
        var more = '__PrOgrAmconD__ AND Status NOT IN(1,5,4) AND (IsShop=0 OR IsShop IS NULL)';
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + "&more=" + more + appendTime();
        //alert(_url);
        
        $("#flexi").flexigrid
        (
        {
            url: _url,
            dataType: 'xml',
            colModel:
            [
                { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                { display: Lang.No, name: 'No', sortable: true, css_class: 'width-90 align-center' },
                { display: Lang.Person, name: 'PersonnelName', sortable: true, css_class: 'align-center' },
                { display: Lang.BankName, name: 'BankAccountName', sortable: true, css_class: 'width-90 align-center' },
                { display: Lang.ReceiverBankName, name: 'ReceiverBankName', sortable: true, css_class: 'width-90 align-center' },
                { display: Lang.DueDate, name: 'DueDate', sortable: true, process: showDate, css_class: 'date-cell' },
                { display: Lang.Amount, name: 'Amount', sortable: true, process: showMonetary, css_class: 'width-100 align-center', footer: 'sum' },
                { display: Lang.Status, name: 'Status', sortable: true, process: displayStatus, css_class: 'width-80 align-center' },
                { display: Lang.Visitor, name: 'VisitorName', sortable: true, css_class: 'width-80 align-center' },
                { display: Lang.FollowUpTime, name: 'FollowUpTime', sortable: true, process: showDate, css_class: 'date-cell' },
                { display: Lang.Serial, name: 'Serial', sortable: true, css_class: 'width-90 align-center' },
                //{ display: Lang.ColEdit, name: 'Edit', width: 40, sortable: false, align: 'left', process: function (celDiv, id) { editLink(celDiv, id, show) } },
                { display: Lang.ColOperation, name: 'Edit', sortable: false, hide: hideColumn("bank", "checks", "edit"), process: myEditLink, css_class: 'action-cell' },
                { display: Lang.ColDel, name: 'Del', sortable: false, hide: hideColumn("bank", "checks", "delete"), process: delLink, css_class: 'action-cell' }
            ],
                buttons:
                [
                   { name: Lang.GBNew, bclass: 'new', onpress: doCommand, onaccess: hasAccess("bank", "checks", "new") },
                   { name: Lang.GBDel, bclass: 'delete', onpress: doCommand, onaccess: hasAccess("bank", "checks", "delete") },
                   { name: Lang.GBPrint, bclass: 'print', onpress: doCommand, onaccess: hasAccess("bank", "checks", "print") },
                   { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand, onaccess: hasAccess("bank", "checks", "excel") },
                   { name: Lang.ShowReceived, bclass: 'showReceived', onpress: doCommand }
                ],
                searchitems: null,
                sortname: "DueDate",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,

                height: gridHeight,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                },
                onSuccess: function(grid) {
                    restoreSelectedRows('#flexi');
                    if (startupID) {
                        $('#row' + startupID).addClass('trSelected');
                    }
                }
               // onSuccess: function (grid) {
                 //   $('#flexi').rows(1).click()=true 
                    //showVoucherLinks();
               // if (startupID != -1) {
               // show(startupID);
             //   startupID = -1;
           // }
        //}
            }
            );
        showSumAmount();
        _Context.filterPostBack = showSumAmount;
    }
    function myEditLink(celDiv, id) {
        $(celDiv).html('<img src="img/edit.gif" />');
        $(celDiv).click(function (event) {
            var tr = $(celDiv).closest('tr');
            var status = getStatus(getColClass(tr, 9), 2);
            $('#flexi tr').removeClass('trSelected');
            tr.addClass('trSelected');
           // alert(status);
            showMenu({
                parent: this, width: '100px',
                cmds: [[Lang.ColEdit, 'Edit']
                    , [Lang.ColReceive, status == '0' || status == '6' ? 'Receive' : '']
                    , [Lang.ColRefuse, status == '0' || status == '6' ? 'Refuse' : '']
                    , [Lang.ColRefuseVisitor, status == '6' ? 'ColRefuseVisitor' : '']
                     ,[Lang.ColSleep, status == '0' ? 'Sleep' : '']
                    , [Lang.ColExpend, status == '0' ? 'Expend' : '']
                    , [Lang.ColVisitor, status == '0' ? 'Visitor' : '']
                    , [Lang.ColRefuseSleep, status == '3' ? 'RefuseSleep' : '']
                    , [Lang.ColSleepReceive, status == '3' ? 'SleepReceive' : '']
                    , [Lang.ColRefuseExpend, status == '4' ? 'RefuseExpend' : '']],
                callback: function (cmd) {
                    if (cmd == 'Edit')
                        show(id);
                    else if (cmd == 'Receive')
                        showReceive(id);
                    else if (cmd == 'Refuse')
                        showRefuse(id);
                    else if (cmd == 'ColRefuseVisitor')
                        showRefuseVisitor(id);
                    else if (cmd == 'Sleep')
                        showSleep(id);
                    else if (cmd == 'Expend')
                        showExpend(id);
                    else if (cmd == 'RefuseSleep')
                        showRefuseSleep(id);
                    else if (cmd == 'SleepReceive')
                        showSleepReceive(id);
                    else if (cmd == 'RefuseExpend')
                        showRefuseExpend(id);
                    else if (cmd == 'Visitor')
                        showVisitor(id);
                }
            });
        });
    }
    function displayStatus(celDiv, id) {
        var _mode = parseInt($(celDiv).html(), 12);
        displayCheckStatus(celDiv, _mode);
    }
    function showReceiveLink(celDiv, id) {
        $(celDiv).html('<img src="img/edit.gif" />');
        $(celDiv).click(function (event) {
            event.preventDefault();
            showReceive(id);
        })
    }

    function showcallback(v, m, f) {
        restoreSelectedRows('#flexi');
        if (v == 'OK' && !checkProgramStatus())
            return false;
        _Context.priceDate = f.IssueDate;
        return newShowcallback(v, m, f, 'checkupdate', 'checkinsert', _Context.table, '#subDetailForm', show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        saveTrSelected(id);
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        if (id == -1)
            _url += "&dueD=1";
        showPopup(_url, showcallback, getDefaultButtons(true, 'bank', "checks"), null, successCallback, true);
    }
    function showReceive(id) {
        _Context.id = id;
        saveTrSelected(id);
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + '&setstatus=' + CheckStatusEnum.Received + '&oweD=1' + appendTime();
        showPopup(_url, showcallback, getButtons(), null, null, true);
    }
    function showRefuse(id) {
        _Context.id = id;
        saveTrSelected(id);
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + '&setstatus=' + CheckStatusEnum.Refused + '&oweD=1' + appendTime();
        showPopup(_url, showcallback, getButtons(), null, null, true);
    }
    function showSleep(id) {
        _Context.id = id;
        saveTrSelected(id);
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + '&setstatus=' + CheckStatusEnum.Sleep + '&oweD=1' + appendTime();
        showPopup(_url, showcallback, getButtons(), null, null, true);
    }
    function showExpend(id) {
        _Context.id = id;
        saveTrSelected(id);
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + '&setstatus=' + CheckStatusEnum.Expend + '&personD=1' + appendTime();
        showPopup(_url, showcallback, getButtons(), null, null, true);
    }
    function showRefuseSleep(id) {
        _Context.id = id;
        saveTrSelected(id);
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + '&setstatus=' + CheckStatusEnum.New + '&dueD=1' + appendTime();
        showPopup(_url, showcallback, getButtons(), null, null, true);
    }
    function showRefuseVisitor(id) {
        _Context.id = id;
        saveTrSelected(id);
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + '&setstatus=' + CheckStatusEnum.New + '&visitorD=1' + appendTime();
        showPopup(_url, showcallback, getButtons(), null, null, true);
    }
    function showSleepReceive(id) {
        _Context.id = id;
        saveTrSelected(id);
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + '&setstatus=' + CheckStatusEnum.SleepReceive + '&owedueD=1' + appendTime();
        showPopup(_url, showcallback, getButtons(), null, null, true);
    }

    function showRefuseExpend(id) {
        _Context.id = id;
        saveTrSelected(id);
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + '&setstatus=' + CheckStatusEnum.New + '&duepersonD=1' + appendTime();
        showPopup(_url, showcallback, getButtons(), null, null, true);
    }

    function showVisitor(id) {
        _Context.id = id;
        saveTrSelected(id);
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + '&setstatus=' + CheckStatusEnum.Visitor + '&visitorD=1' + appendTime();
        showPopup(_url, showcallback, getButtons(), null, null, true);
    }
    
    
    function saveTrSelected(id)
    {
        $('.trSelected').removeClass('trSelected');
        $('#row' + id).addClass('trSelected');
        saveSelectedRows('#flexi');
        $('.trSelected').removeClass('trSelected');
    }

    // ------------------------------
    // startup code
    // ------------------------------
    startupID = $.urlParam('id');
    if (!startupID) {
        startupID = -1;
    }
    checkAccess(['bank,checks']);
    $("#bar").prepend(breadcrumb({pageInfo: {pageTitle: Lang.Check, parentTitle: Lang.BankAndCheck, parentUrl: 'menu/bank.aspx'} }));
    setClassBank();
    run();
}