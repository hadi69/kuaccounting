﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
92-10-16: file created
*/
function view_offers() {
    makeChosen();
    // ------------------------------
    // startup code
    // -----------------------------
    autoComplete('ServicePriceView', 'offerserviceauto', '#ServiceName', '#ServicePriceID');
    autoComplete('Grouping', 'servicegroupauto', '#GroupingName', '#GroupingID');
    autoComplete('CooperationType', 'enumauto1', '#Attainer', '#AttainerID');
    dateSelect('.dateSelect')
    setValue('#NumOfFactors');
    setValue('#OfferType');
    function offerTypeChange(){
        if ($('#OfferType').val() == 0) {
            $('#SN').show();
            $('#GN').hide();
        }
        else if ($('#OfferType').val() == 1) {
            $('#GN').show();
            $('#SN').hide();
        }
        else {
            $('#SN').hide();
            $('#GN').hide();
        }
    $('#OfferType').change(function () {
        if ($(this).val() == 0) {
            $('#SN').show();
            $('#GN').hide();
            $('#GroupingName').val('');
            $('#GroupingID').val('');
        }
        else if ($(this).val() == 1) {
            $('#GN').show();
            $('#SN').hide();
            $('#ServiceName').val('');
            $('#ServicePriceID').val('');
        }
        else {
            $('#SN').hide();
            $('#GN').hide();
            $('#ServiceName').val('');
            $('#GroupingID').val('');
            $('#GroupingName').val('');
            $('#ServicePriceID').val('');
        }
    });
    }
    function numOfFactorsChange() {
        if ($('#NumOfFactors').val() == 1) {
            $('#NumOfCalcFactorsDays').show();
           
        }
        else {
            $('#NumOfCalcFactorsDays').hide();
        }
        $('#NumOfFactors').change(function () {
            if ($(this).val() == 1) {
                $('#NumOfCalcFactorsDays').show();
            }
            else {
                $('#NumOfCalcFactorsDays').hide();
            }
        });
    }
    numOfFactorsChange();
    offerTypeChange();
    insertValidation();
}