﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-10-19: file created
*/
function view_timeofchecks() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show, _Context.title);
    }
    function run() {
        var d = new Date();
        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        var date = month + '/' + day + '/' + year
        // alert(date);
        var _url = 'service.asmx/GetQueryList?table=' + _Context.table + '&qd=listofChecks&Date=' + date;
        //alert(_url);
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', sortable: false, hide: true },
                    { display: Lang.Staus, name: 'Type', sortable: false, css_class: 'width-100 align-center'},
                    { display: Lang.Pesonnel, name: 'Pesonnel', sortable: false},
                    { display: Lang.Amount, name: 'Amount', sortable: false, css_class: 'width-130 align-center', process: showMonetary, footer: 'sum' },
                    { display: Lang.DueDate, name: 'DueDate', sortable: false, css_class: 'date-cell'},
                    { display: Lang.Status, name: 'Status', sortable: true, css_class: 'width-100 align-center', process: displayStatus },
                    { display: '', name: 'IsShop', width: 90, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, css_class: 'action-cell', process: function (celDiv, id) { editLink(celDiv, id, show); }}

                ],
                buttons:
                [
                   { name: Lang.GBDel, bclass: 'delete', onpress: doCommand, onaccess: hasAccess("shift", "allocationshift", "delete") },
                   { name: Lang.GBPrint, bclass: 'print', onpress: doCommand},
                  // { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand}
                ],
                searchitems: null,
                sortname: "DueDate",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,

                height: gridHeight,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                }
            }
            );
    }
    function displayStatus(celDiv, id) {
        var _mode = parseInt($(celDiv).html(), 10);
        displayCheckStatus(celDiv, _mode);
    }
    function show(id, celDiv, successCallback) {
       
        if (!celDiv) {
            return;
        }
        var relatedID = getColContent($(celDiv).closest('tr'), 1);
        var wfType = getColContent($(celDiv).closest('tr'), 2); //wt1
        if ((wfType == Lang.DAModePayCheck) && getColContent($(celDiv).closest('tr'), 7) == 0)
        {
            location.href = "list.aspx?table=PayCheck&id=" + relatedID;
            
        }
        if ((wfType == Lang.ReceiveCheck) && getColContent($(celDiv).closest('tr'), 7) == 0) {
   
            location.href = "list.aspx?table=Checks&id=" + relatedID;
        }
        if ((wfType == Lang.ReceiveCheck) && getColContent($(celDiv).closest('tr'), 7) == 1) {

            location.href = "list.aspx?table=Checks&view=checksshops&id=" + relatedID;
        }
        if ((wfType == Lang.DAModePayCheck) && getColContent($(celDiv).closest('tr'), 7) == 1) {
            location.href = "list.aspx?table=PayCheck&view=paycheckshops&id=" + relatedID;

        }
      }
    $("#bar").prepend(breadcrumb({pageInfo: {pageTitle: Lang.ListOfChecks, parentTitle: Lang.MAccounting, parentUrl: 'menu/accounting.aspx'}}));
    setClassAccounting();
    run();
}
////////////////////////