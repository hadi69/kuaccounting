﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    93-08-01: file created
*/
function view_codings() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query  + appendTime();
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'center', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell'},
                    { display: Lang.Type, name: 'Type', sortable: true, process:_displayGroupingEnum},
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' }
                ],
                buttons:
                [
                   { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                searchitems: null,
                sortname: "Type",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                }
            }
            );
    }

    function _displayGroupingEnum(celDiv, id) {
        var _mode = parseInt($(celDiv).html(), 10);
        displayGroupingTypeEnum(celDiv, _mode);
    }
    function showcallback(v, m, f) {

        var qry = (_Context.id == -1) ? 'codinginsert' : 'codingupdate';
        return newMastercallback(v, m, f, 'Coding', qry, 'CodingItem', 'codingitemupdate', 'codingiteminsert', 'codingitemdelete', 'GroupingID', '#subDetailForm', show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }
    
    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['sales,coding']);
    checkLevel(2);
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.Coding, parentTitle: Lang.SellAndBuy, parentUrl: 'menu/sales.aspx' } }));
    setClassSales();

    run();
}