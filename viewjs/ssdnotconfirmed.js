﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-11-08: bindStoreServices added
    92-11-05: file created
*/
function view_ssdnotconfirmed() {
    var mID = -1;


   
    function bindStoreServices() {
        var _url = 'service.asmx/GetQueryList?table=StoreServiceDef&qd=ssdnotconfirmed&StoreServiceDefID=' + mID + appendTime();
        $("#scondFlexi").flexigrid
                (
                {
                    url: _url,
                    dataType: 'xml',
                    sortname: "ID",
                    sortorder: "asc",
                    usepager: false,
                    useRp: false,
                    rp: gridRp,
                    showTableToggleBtn: false,
                    width: gridWidth,
                    height: 200,
                    singleSelect: true,
                    colModel:
                    [
                        { display: 'ID', name: 'ID', width: 10, sortable: false, css_class: 'id', hide: true },
                        { display: Lang.No, name: 'NOSerial', sortable: true, css_class: ' width-100 no align-center' },
                        { display: Lang.Type, name: 'Type', sortable: true, css_class: 'type width-200', process: _displayWorkflowType },
                        { display: Lang.User, name: 'Fullname', sortable: true, css_class: 'width-200' },
                        { display: Lang.Num, name: 'Amount', sortable: true, css_class: 'width-200 align-center' },
                        { display: Lang.Task, name: 'TaskName', sortable: true, css_class: 'width-200' },
                        { display: Lang.Duration, name: 'Duration', sortable: true, process: showTaskDuration }
                    ],
                    buttons:
                    [],
                    onSuccess: function (grid) {
                        postProcessGrid();
                    }
                }
                );
     //   { display: Lang.Cardex, name: 'Duration', sortable: false, process: function (celDiv, id) { cardexLink(celDiv, id); }, css_class: 'action-cell' }
    }
    function _displayWorkflowType(celDiv, id) {
        var type = toInt($(celDiv).html(), 0);
        if (type == 20) {
            $(celDiv).html(Lang.VisitorPlanDays);
            $(celDiv).addClass("wt20");
        } else {
            displayWorkflowType(celDiv, type);
        }
    }
    function postProcessGrid() {
        setTimeout(function () {
            showFactorLinks();
        }, 500);
    }

    function showFactorLinks() {
        $('#scondFlexi tr').each(function () {
            var _id = $('.id div', this).html();
            var _no = $('.no div', this).html();
            if ($('.type div', this).hasClass('wt1')) {
                $('.no div', this).html('<a href="list.aspx?table=Request&id=' + _id + '" target=_blank>' + _no + '</a>');
                return;
            }
            if ($('.type div', this).hasClass('wt6')) {
                $('.no div', this).html('<a href="list.aspx?table=StoreDraft&id=' + _id + '" target=_blank>' + _no + '</a>');
                return;
            }
            if ($('.type div', this).hasClass('wt5')) {
                $('.no div', this).html('<a href="list.aspx?table=Refuse&id=' + _id + '" target=_blank>' + _no + '</a>');
                return;
            }
            if ($('.type div', this).hasClass('wt20')) {
                $('.no div', this).html('<a href="list.aspx?table=VisitorPlanDays&id=' + _id + '" target=_blank>' + Lang.Link + '</a>');
                return;
            }
        });
    }
    // ------------------------------
    // startup code
    // ------------------------------
    mID = toInt($('#ID').val(), -1);

    bindStoreServices();
}