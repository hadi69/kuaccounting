﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
92-10-28: file created
*/
function view_financeyears() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        //alert(_url);
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell'},
                    { display: Lang.Title, name: 'Title', sortable: false, css_class: 'width-200'},
                    { display: Lang.Code, name: 'Code', sortable: false, css_class: 'width-100'},
                    { display: Lang.StartDate, name: 'StartDate', sortable: true, process: showDate, css_class: 'width-100 align-center' },
                    { display: Lang.EndDate, name: 'EndDate', sortable: true, process: showDate, css_class: 'date-cell' },
                    { display: Lang.Status, name: 'Status', sortable: true, process: showFinanceYearStatus, css_class:'width-100 align-center' },
                    { display: Lang.Comments, name: 'Comments', sortable: true},
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' }
                ],
                buttons:
                [
                    { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                    { name: Lang.GBEdit, bclass: 'edit', onpress: doCommand },
                    { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                    { separator: true },
                    { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                    { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                searchitems: null,
                sortname: "StartDate",
                sortorder: "asc",
                usepager: true,
                //title: _Context.title,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: _options.gridToggle,
                width: _options.gridWidth,

                height: _options.gridHeight, resizable: _options.gridResizable,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                },
                onSuccess: function (grid) {
                    restoreSelectedRows('#flexi');
                }
            }
            );
    }
    function showFinanceYearStatus(celDiv, id) {
        //0:New,1:Paid
        var status = toInt($(celDiv).html(), 0);
        displayFinanceYearStatus(celDiv, status);
    }
   
    function showcallback(v, m, f) {
        return newShowcallback(v, m, f, 'financeyearupdate', 'financeyearinsert', _Context.table, null, show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        saveSelectedRows('#flexi');
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['admin,financeyear']);
    $("#bar").prepend(breadcrumb({pageInfo: {pageTitle: Lang.FinanceYear, parentTitle: Lang.Management, parentUrl: 'menu/admin.aspx'} }));
    setClassManager();
    run();
}