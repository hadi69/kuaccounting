﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-11-12: review
    92-10-16: file created
*/
function view_transfer() {
    makeChosen();
    $("#tableItems").smartTable();
    var cnt = 0, mID = -1;

    replacePrintButton({
        primary: {
            text: Lang.Print,
            href: Router.Generate("print-transfer", { params: { id: _Context.id }, query: { print: "true" } })
            , type: 0, mode: 1, id: _Context.id
        }
    });

    function accessAutoCompletes() {
        //var more = getAccessID("store", "ID");
        //if (more != "") {
        //    var _autourl = 'get.aspx?op=Auto&table=Stores&qd=enumauto&more=' + more + appendTime();
        //    autoComplete('Stores', 'enumauto', "#SourceStoreName", "#SourceStoreID"
        //                    , autoPostPack
        //                    , function (data, i, n, value) {
        //                        return '<div dir=rtl><b>' + data[1] + '</b>';
        //                    }, null, null, _autourl);
        //}
        //else
            autoComplete('Stores', 'storeauto', "#SourceStoreName", "#SourceStoreID");
    }
    function addRow(node) {
        var row = '<tr>';
        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">'
            + (cnt + 1)
            + '</td>';

        row += '<td data-th="' + Lang.Commodity + '">'
            + '<input type="hidden" class="transferitemid" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="text" id="se_' + cnt + '" name="StoreServiceName" value="' + getNodeVal(node, "StoreServiceName") + '" class="storeservicename form-control input-sm"/>'
            + '<input type="hidden" id="ss_' + cnt + '" name="StoreServiceID" value="' + getNodeVal(node, "StoreServiceID") + '" class="storeserviceid" />'
            + '<input type="hidden" id="varid_' + cnt + '" name="ServiceVariateID" value="' + getNodeVal(node, "ServiceVariateID", 0) + '" class="ServiceVariateID" />'
             + '<input type="hidden" id="avial_' + cnt + '" name="Avail" value="' + getNodeVal(node, "Avail", 0) + '" class="Avail" /></td>'
            + '</td>';

        row += '<td data-th="' + Lang.Units + '">'
            + '<input type="text" id="u_' + cnt + '" name="UnitName" value="' + getNodeVal(node, "UnitName") + '" class="unitname form-control input-sm align-center" />'
            + '<input type="hidden" name="UnitID" value="' + getNodeVal(node, "UnitID") + '" />'
            + '</td>';

        /*row += '<td data-th="' + Lang.Num + '">'
            + '<input type="text" id="n_' + cnt + '" name="Num" value="' + getNodeVal(node, "Num") + '" class="validate[custom[number]] form-control input-sm num align-center"/>'
            + '</td>';*/

        row += '<td data-th="' + Lang.Num + '">'
          + '<div class="input-group input-group-sm">'
          + '<input type="text" id="n_' + cnt + '" name="Num" value="' + getNodeVal(node, "Num", 0) + '" class="validate[custom[number]] form-control width-100-percent min-width-50 align-center num" />'
          + '<span class="input-group-addon no-left-border no-right-border">x</span>'
          + '<input readonly="readonly" size="1" id="ucc_' + cnt + '" name="UnitCurrencyConverter" value="' + getNodeVal(node, "UnitCurrencyConverter", 1) + '" class="form-control width-50 align-center" />'
          + '</div>'
          + '</td>';

        row += '<td data-th="' + Lang.Price + '">'
            + '<input type="text" id="p_' + cnt + '" name="Price" value="' + getNodeVal(node, "Price") + '" class="price monetary validate[custom[integer]] form-control input-sm align-center" />'
            + '</td>';

        row += '<td data-th="' + Lang.Comments + '">'
            + '<input type="text" name="Comments" id="c_' + cnt + '" value="' + getNodeVal(node, "Comments") + '"  class="Comment form-control input-sm"/>'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a class="btn btn-danger btn-xs btn-block btn-remove remove" id="r_' + cnt + '">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }

    // ------------------------------
    // startup code
    // ------------------------------
    mID = toInt($('#MasterID').val(), -1);
    processPrintButton(0, 0, mID);

    // Master Form
    accessAutoCompletes();
    autoComplete('Stores', 'storeauto', "#DestStoreName", "#DestStoreID");
    
    dateSelect('.dateSelect');

    if ($("#Date").val() == '')
        $("#Date").val(_Context.today);
    $('#Date_Time').attr('readonly', 'readonly');
    if (mID == -1) {
        $('#StoreKeeperID').val(_Context.personnelID);
        $("#StoreKeeperName").val(_Context.personnelName);
    }
    else
        $('#workflowtasks').html(showWorkflowTasks(mID, WorkflowTypeEnum.Transfer));


    // Detail Form
    fillTable("get.aspx?op=GetXml&qd=transferitem&TransferID=" + mID, '#tableItems', addRow);
    $('#tableItems .storeservicename').each(function () {
        var id = $(this).attr('id');
        id = id.substring(id.indexOf('_') + 1);
        var unitName = $('#u_' + id);
        var storeServiceId = $('#ss_' + id).val();
        if (storeServiceId > 0) {
            var _autourl1 = 'get.aspx?op=Auto&table=Units&qd=storedraftunitsauto&StoreServiceID=' + storeServiceId;
            autoComplete('Units', 'unitsauto', unitName, "#justfurfun",
                function (event, data, formatted) {
                    var id1 = $(event.target).attr('id');
                    id1 = id1.substring(id1.indexOf('_') + 1);
                    $(event.target).next().val(data[0]); // ID
                    if (data[2] && data[2] != 0) {
                        //$('#a_' + id1).next().text('x')
                        $('#ucc_' + id1).val(data[2]); // CurrencyConverter}
                    }
                    else {
                        //$('#a_' + id1).next().text(' ')
                        $('#ucc_' + id1).val('1');
                    }
                }
            , null, null, null, _autourl1);
        }
    });
    doMore();

    function doMore() {
        function removeRow($who) {
            var id = $who.attr('id');
            id = id.substring(2);
            $('#se_' + id).val(""); // StoreServiceName
            $('#se_' + id).next().val(-1); // StoreServiceID
            $who.parent().parent().hide();
            Growl.Notice(Lang.DeletedSuccessfully);
        }
        $('.remove').click(function () {
            removeRow($(this));
        });
        $('.add').click(function () {
            var table = $("#tableItems");
            var lastRow = $('tr:last', table);
            var storeID = toInt($("#SourceStoreID").val(), -1);
            if (!hasValuePositive(storeID)) {
                alert(Lang.ChooseStore);
                return;
            }
            if ($("#tableItems .num").length > 0 && toInt($('.transferitemid', lastRow).val(), -1) === -1) {
                if (checkAvail($('.num', lastRow)) < 0) {
                    return false;
                };
            }
            $('tr:last', table).after(addRow(null));
            Growl.Notice(Lang.RowAddedSuccessfully);
             lastRow = $('tr:last', table);
            $('.remove', lastRow).click(function () {
                removeRow($(this));
            });
            disableKeydownOnLastInputByName('Comments');

            //var more = encodeURIComponent("&StoreID=" + storeID);
            var _autourl = 'get.aspx?op=Auto&table=StoreDraft&qd=storeservicesauto&StoreID=' + storeID + appendTime();
            autoCompleteFree('StoreServices', 'storeservicesauto', "#tableItems tr:last .storeservicename", "#justfurfun"
                    , function (event, data, formatted) {
                    setServiceData($(event.target), data);
                }
                    , function (data, i, n, value) {
                        var price = 0;
                        if (data[11]) {
                            // ProfitPercent
                            if (data[10])
                                price = data[10];
                        }
                        else {
                            // Last Prices
                            if (data[14])
                                price = data[14];
                        }
                        return '<div dir=rtl><b>' + data[1] + '</b><br />'
                            + Lang.Price + " : " + numberWithCommas(toFloat(toMonetary(price), 0.0).toFixed(_Context.decimalPoints)) + '<br />'
                            + Lang.ExpirationDate + " : " + data[3] + '<br />'
                            + Lang.Supply + " : " + (data[4] - data[5]).toFixed(_Context.decimalPoints)
                            + '</div>';
                    }, null, null, _autourl);
           // autoComplete('Units', 'enumauto', "#tableItems tr:last .unitname", "#justfurfun", tableAutoPostPack, formatItemNameCode);
            //
            function setServiceData(serviceNameInput, data) {
                $(serviceNameInput).next().val(data[0]); // ID
                var id = $(serviceNameInput).attr('id');
                id = id.substring(id.indexOf('_') + 1);
                var price = 0;
                if (data[11]) {
                    // ProfitPercent
                    if (data[10])
                        price = data[10];
                }
                else {
                    // Last Prices
                    if (data[14])
                        price = data[14];
                }
                $('#p_' + id).val(numberWithCommas(toFloat(toMonetary(price), 0.0).toFixed(_Context.decimalPoints))); // price
                if (toInt(data[22], -1) > 0) // OtherUnitID
                {
                    $('#u_' + id).next().val(data[22]); // OtherUnitID
                    $('#u_' + id).val(data[23]); // OtherUnitName
                    $('#ucc_' + id).val(data[24]); // CurrencyConverter
                } else {
                    $('#u_' + id).next().val(data[6]); // unitid
                    $('#u_' + id).val(data[7]); // unitname
                    $('#avial_' + id).val(data[4] - data[5]); // available: income - outcome
                }
                var _autourl1 = 'get.aspx?op=Auto&table=Units&qd=storedraftunitsauto&StoreServiceID=' + data[0];
                autoComplete('Units', 'unitsauto', "#tableItems tr:last .unitname", "#justfurfun",
                    function (event, data, formatted) {
                        var id1 = $(event.target).attr('id');
                        id1 = id1.substring(id1.indexOf('_') + 1);
                        $(event.target).next().val(data[0]); // ID
                        if (data[2] && data[2] != 0) {
                            //$('#a_' + id1).next().text('x')
                            $('#ucc_' + id1).val(data[2]); // CurrencyConverter}
                        }
                        else {
                            //$('#a_' + id1).next().text(' ')
                            $('#ucc_' + id1).val('1');
                        }
                    }
                , null, null, null, _autourl1);

                // ServiceVariate
                if (data[17] > 0) {
                    $('#varid_' + id).val(data[17]);
                }
                setTimeout(function() {
                        $('#n_' + id).val(1);
                        $('#n_' + id).blur();
                    },
                    5);
            }

            function checkServiceBarCode(me, meID, meSID, barcode) {
                var buyerID = -1 //toInt($("#BuyerID").val(), -1);
                var StoreID = toInt($("#SourceStoreID").val(), -1);
                var _autourl = 'get.aspx?op=Auto&table=StoreServices&qd=storeservicesbarcodeauto' + '&BuyerID=' + buyerID + '&StoreID=' + StoreID + appendTime();
                var row = getServerValues(_autourl + "&q=" + barcode);
                if (!row) {
                    return false;
                }
                if (row && row.length && row.length > 10) {
                    // insert new row when we have variate unit
                    var unitId = row[6];
                    if (toInt(row[22], -1) > 0) {
                        unitId = row[22];
                    }
                    var varID = -1;
                    // ServiceVariate
                    if (row[17] > 0)
                        varID = row[17];
                    // -----------------------------------
                    // check if the service is already used, just increase the number
                    var already = false;
                    $('tr .storeserviceid', table).each(function () {
                        var thisId = $(this).attr('id');
                        thisId = thisId.substring(thisId.indexOf('_') + 1);
                        if ($(this).attr('id') != meSID) {
                            if ($(this).val() == row[0] && $('#u_' + thisId).next().val() === unitId) {
                                var v = toInt($(this).parent().find('.ServiceVariateID').val(), 0);
                                if (v == 0) {
                                    already = true;
                                    me.val('');
                                    me.focus();
                                    var aID = $(this).attr('id');
                                    var _cnt = aID.substring(aID.indexOf('_') + 1);
                                    $('#n_' + _cnt).val(toInt($('#n_' + _cnt).val(), 0) + 1);
                                    setTimeout(function () {
                                        $(me).focus();
                                    }, 1);
                                }
                            }
                        }
                    });
                    if (!already) {
                        setServiceData(me, row);
                        me.val(row[1]);
                        $('.add').first().click();
                    }
                }
                return true;
            }
            function checkServiceVariateBarCode(me, meID, meSID, barcode) {
                var buyerID = -1 //toInt($("#BuyerID").val(), -1);
                var StoreID = toInt($("#SourceStoreID").val(), -1);
                var _autourl = 'get.aspx?op=Auto&table=StoreServices&qd=storeservicesvariatebarcodeauto' + '&BuyerID=' + buyerID + '&StoreID=' + StoreID + appendTime();
                var row = getServerValues(_autourl + "&q=" + barcode);
                if (!row) {
                    $.ionSound.play("bell_ring");
                    return false;
                }
                if (row && row.length && row.length > 10) {
                    var varID = -1;
                    // ServiceVariate
                    if (row[17] > 0)
                        varID = row[17];
                    // -----------------------------------
                    // check if the service is already used, just increase the number
                    var already = false;
                    $('tr .storeserviceid', table).each(function () {
                        if ($(this).attr('id') != meSID) {
                            if ($(this).val() == row[0]) {
                                // check variate
                                if (varID != -1 && toInt(varID) > 0) {
                                    // parent = td
                                    var v = $(this).parent().find('.ServiceVariateID').val();
                                    if (v != varID)
                                        return;
                                }
                                already = true;
                                me.val('');
                                me.focus();
                                var aID = $(this).attr('id');
                                var _cnt = aID.substring(aID.indexOf('_') + 1);
                                $('#n_' + _cnt).val(toInt($('#n_' + _cnt).val(), 0) + 1);
                                setTimeout(function () {
                                    $(me).focus();
                                }, 1);
                            }
                        }
                    });
                    if (!already) {
                        setServiceData(me, row);
                        me.val(row[1] + ' - ' + row[18]);  // row[18] = ServiceVariate.VarTitle 


                        $('.add').first().click();
                    }
                }
                return true;
            }

            // Handle barcode reader
            $("#tableItems tr:last .storeservicename").blur(function () {
                // se_id, sid_id
                var me = $(this);
                var meID = me.attr('id'); // se_1
                var meSID = '#ss_' + meID.substring(meID.indexOf('_') + 1);

                var barcode = $(this).val();
                var stsID = $(meSID).val(); // StoreServiceID
                if (barcode.length > 0 && stsID == '') {
                    // it is a barcode
                    if (!checkServiceBarCode(me, meID, meSID, barcode))
                        if (!checkServiceVariateBarCode(me, meID, meSID, barcode)) {
                            $(me).val('');
                            setTimeout(function () { $(me).focus(); }, 10);
                        }
                }
            });
            $('.num', lastRow).blur(function () { checkAvail(this); });
            commaInputs();
            monetaryKeys();
            setTimeout(function () {
                $("#tableItems tr:last .storeservicename").focus();
            }, 10);
        });

       insertValidation();
        checkInputAccess();
        setClassStore();


        monetaryInputsSet(null, '#Date');
        commaInputs();
    }

    $('.num').blur(function () { checkAvail(this); });
    function checkAvail(was) {
        var id = $(was).attr('id');
        if (id && id != undefined && id != null) {
            id = id.substring(id.indexOf('_', 0) + 1); // d_1  -->  1
            // don't check availablity for IsVirtual service
            var amount = toFloat($('#n_' + id).val(), 0);
            var unitCarrency = toFloat($('#ucc_' + id).val(), 1);
            if ($('#avial_' + id).val() != '') {
                var avail = toFloat($('#avial_' + id).val(), 1);
                if ((avail < (amount * unitCarrency))) {
                    //$('#a_' + id).val(0);
                    $('#se_' + id).val('');
                    $('#ss_' + id).val('');
                    $.ionSound.play("bell_ring");
                    setTimeout(function () {
                        $('#se_' + id).focus();
                    },
                        20);
                    $('#a_' + id).attr('style', 'background-color:red');
                    //Growl.Error(Lang.NoEnoughSupply + '     ' + Lang.ServiceAllowedAmount + avail, 2000);
                    //alert(Lang.NoEnoughSupply + '     ' + Lang.ServiceAllowedAmount + avail);
                    return -1;
                }
            }
        }
        return 1;
    };
}