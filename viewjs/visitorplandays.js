﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />
function view_visitorplandays() {
    $("#tableGroup").smartTable();
    var cnt = 0, cnt1 = 0, mID = -1;
    function addGroup(node) {
        var row = '<tr>';

        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">'
            + (cnt + 1)
            + '</td>';

        row += '<td data-th="' + Lang.FirstGroup + '">'
            + '<input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="text" id="fg_' + cnt + '" name="FGroupingName" value="'
            + getNodeVal(node, "FGroupingName") + '" class="fgroupingname form-control input-sm" />'
            + '<input type="hidden" id="fgid_' + cnt + '" name="FGroupingID" value="' + getNodeVal(node, "FGroupingID") + '" class="fgroupingid" />'
            + '</td>';

        row += '<td data-th="' + Lang.SecondGroup + '">'
           + '<input type="text" id="sg_' + cnt + '" name="SGroupingName" placeholder="' + Lang.SGroupingNamePlaceHolder + '" value="'
            + getNodeVal(node, "SGroupingName") + '" class="sgroupingname form-control input-sm" />'
            + '<input type="hidden" id="sgid_' + cnt + '" name="SGroupingID" value="' + getNodeVal(node, "SGroupingID") + '" class="sgroupingid" />'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a id="r_' + cnt + '" class="btn btn-danger btn-xs btn-block btn-remove removeGroup" href="#">'
            + '<span class="visible-xs-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }
    function addService(node) {
        var row = '<tr>';

        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">'
            + (cnt1 + 1)
            + '</td>';

        row += '<td data-th="' + Lang.CommodityName + '">'
            + '<input type="hidden" name="ID2" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="text" id="cn_' + cnt1 + '" name="CommodityName" value="'
            + getNodeVal(node, "CommodityName") + '" class="commodityname form-control input-sm" />'
            + '<input type="hidden" name="StoreServiceID" id="sid_' + cnt1 + '" class="storeserviceid" value="' + getNodeVal(node, "StoreServiceID") + '" />'
            + '</td>';

        row += '<td data-th="' + Lang.Num + '">'
           + '<input type="text" id="n_' + cnt1 + '" name="Num" value="'
            + getNodeVal(node, "Num") + '" class="num form-control input-sm" />'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a id="r_' + cnt1 + '" class="btn btn-danger btn-xs btn-block btn-remove removeService" href="#">'
            + '<span class="visible-xs-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt1++;
        return row;
    }
    function autoPostPack(event, data, formatted) {
        $(event.target).next().val(data[0]); // ID
    }
    function secondAutoPostPack(event, data, formatted) {
        var table = $('#tableGroup');
        var lastFirstGId = $('tr:last .fgroupingid', table).val();
        $('tr .sgroupingid', table).each(function () {
            var rowId = $(this).attr('id');
            rowId = rowId.substring(rowId.indexOf('_') + 1);
            var currentFirstGId = $('#fgid_' + rowId).val();
            if ($(this).attr('id') != $('tr:last .sgroupingid', table).attr('id')) {
                if (($(this).val() == data[0]) && lastFirstGId == currentFirstGId) {
                    alert(Lang.VisitorPlanDayMsg);
                }
            }
        });
        var autoUrl = 'get.aspx?op=Auto&table=DailyCustomers&qd=visitorplandaysrepeatedselect' + '&FirstGroupID=' + lastFirstGId + '&SecondGroupID=' + data[0] + '&VisitDate=' + $('#visitDate').val() + appendTime();
        var inf = getServerValues(autoUrl);
        if (inf) {
            if (inf[0]) {
                alert(Lang.VisitorPlanDayMsg1 + ' ' + inf[1] + ' ' + Lang.VisitorPlanDayMsg2);
            }
        }
        $(event.target).next().val(data[0]); // ID
    }
    function removeGroup($who) {
        var id = $who.attr('id'); // r_1
        id = id.substring(2); // r_1  -->  1
        //alert(id);
        $('#fg_' + id).val(""); // 
        $('#fg_' + id).next().val(-1); // 
        $('#sg_' + id).val(""); // 
        $('#sg_' + id).next().val(-1);
        $who.parent().parent().hide();
        Growl.Notice(Lang.DeletedSuccessfully);
    }
    function removeService($who) {
        var id = $who.attr('id'); // r_1
        id = id.substring(2); // r_1  -->  1
        //alert(id);
        $('#cn_' + id).val(""); // commodity name
        $('#cn_' + id).next().val(-1); // commodityid
        $who.parent().parent().hide();
        Growl.Notice(Lang.DeletedSuccessfully);
    }

    function bindAdd() {
        $('.addGroup').click(function () {
            addGroupClick();
        });

        $('.addService').click(function() {
            addServiceClick();
        });
    }

    function bindRemove() {
        $('.removeGroup').click(function () {
            var element = $(this);
            doConfirm(function () {
                removeGroup(element);
            }, Lang.ConfirmDelete);
        });
        $('.removeService').click(function () {
            var element = $(this);
                doConfirm(function () {
                    removeService(element);
                }, Lang.ConfirmDelete);
        });
    }

    function doCommand(com, grid) {
        if (com == Lang.GBDel) {
            alert(Lang.DeleteDaylyCustomerMsg);
            _Context.table = 'VisitorCustomersStatus';
            generalCommand(com, grid, show1);
            setTimeout(function() {
                    $("#flexiServices").flexReload();
                },
                3500);
        } else if (com == Lang.GBPrint || Lang.GBExcel) {
            generalCommand(com, grid, show1, null, null, null, null, true);
        } else {
            generalCommand(com, grid, show1);
        }
    }

    function show1() {
        return 1;
    }

    var _url = 'service.asmx/GetQueryList?table=VisitorCustomersStatus&qd=visitorcustomersstatusforvisitorplandays&VisitorPlanDaysID=' + $('#MasterID').val() + appendTime();
    var sumMaxCredit = 0;
    var sumCheckCredit = 0;
    $("#scondFlexi").flexigrid
    (
    {
        url: _url,
        dataType: 'xml',
        colModel:
        [
            { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
            { display: 'CustomersID', name: 'CustomersID', width: 10, sortable: false, align: 'left', hide: true },
            { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
            { display: Lang.Code, name: 'Code', width: 10, sortable: false, css_class: 'width-100' },
            { display: Lang.CustomerName, name: 'CustomerName', sortable: true, css_class: 'width-150' },
            { display: Lang.FirstGroup, name: 'FGroupName', sortable: true, css_class: 'width-200' },
            { display: Lang.SecondGroup, name: 'SGroupName', sortable: true, css_class: 'width-100' },
            { display: Lang.MaxCredit, name: 'MaxCredit', sortable: true, css_class: 'width-100', process: showAndSumMonetaryMc },
            { display: Lang.CheckCredit1, name: 'CheckCredit', sortable: true, process: showAndSumMonetaryCc, css_class: 'width-100' },
            { display: Lang.Address, name: 'Address', sortable: true}

        ],
        buttons:
        [
        { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
        { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
        { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
        ],
        searchitems: null,
        sortname: "CustomerName",
        sortorder: "asc",
        usepager: true,
        //title: _Context.title,
        useRp: true,
        rp: gridRp,
        showTableToggleBtn: _options.gridToggle,
        width: _options.gridWidth,
        height: 270,
        singleSelect: false,
        onSuccess: function (grid) {
            $('#SumMaxCredit').html(numberWithCommas(toMonetary(sumMaxCredit)));
            $('#SumCheckCredit').html(numberWithCommas(toMonetary(sumCheckCredit)));
            sumMaxCredit = 0;
            sumCheckCredit = 0;
        }
    }
    );

    function showAndSumMonetaryMc(celDive, id, text) {
        sumMaxCredit += toFloat(text);
        showMonetary(celDive, id, text);
    }

    function showAndSumMonetaryCc(celDive, id, text) {
        sumCheckCredit += toFloat(text);
        showMonetary(celDive, id, text);
    }

    function addGroupClick() {
        var table = $('#tableGroup');
        var tableID = table.attr("id");
        $('tr:last', table).after(addGroup(null));
        Growl.Notice(Lang.RowAddedSuccessfully);
        var lastRow = $('tr:last', table);
        autoComplete('Grouping', 'groupauto', "#tableGroup tr:last .fgroupingname", "#justfurfun"
           , autoPostPack
           , function (data, i, n, value) {
               return data[1];
           });
        autoComplete('Grouping', 'groupauto', "#tableGroup tr:last .sgroupingname", "#justfurfun"
            , secondAutoPostPack
            , function (data, i, n, value) {
                return data[1];
            });
        $('.removeGroup', lastRow).click(function () {
            var element = $(this);
            doConfirm(function () {
                removeGroup(element);
            }, Lang.ConfirmDelete);
        });
        $('.sgroupingname').unbind('keyup');
        $('#tableGroup tr:last .sgroupingname').unbind('keyup');
        $('#tableGroup tr:last .sgroupingname').keyup(function (e) {
            if (e.keyCode === 37) {
                selectMultiGroups();
            }
        });
    }

    function addServiceClick() {
        var table = $('#tableService');
        var tableID = table.attr("id");
        $('tr:last', table).after(addService(null));
        disableKeydownOnLastInputByName('Num');
        Growl.Notice(Lang.RowAddedSuccessfully);
        var lastRow = $('tr:last', table);
        var _autourl = 'get.aspx?op=Auto&table=StoreDraft&qd=storeservicesauto&StoreID=0&BuyerID=NULL'+ appendTime();
        autoCompleteFree('StoreServices', 'storeservicesauto', "#tableService tr:last .commodityname", "#justfurfun"
                , function (event, data, formatted) {
                    $('tr .storeserviceid', table).each(function () {
                        if ($(this).attr('id') != $('tr:last .storeserviceid', table).attr('id')) {
                            if ($(this).val() == data[0]) {
                                alert(Lang.StoreDraftJsM3);
                                $(event.target).val('');
                                $(this).val('');
                            }
                        }
                    });
                $(event.target).next().val(data[0]); // ID
                }
                , function (data, i, n, value) {
                    return formatService(data);
                }, null, null, _autourl);

        $('.removeService', lastRow).click(function () {
            var element = $(this);
            doConfirm(function () {
                removeService(element);
            }, Lang.ConfirmDelete);
        });
        $('#tableService tr:last .commodityname').focus();

    }

    function selectMultiGroups() {
            var _url = 'get.aspx?op=GetHtml&table=Grouping&view=multigroupselect' + appendTime();
            var btns = {};
            btns[Lang.OK] = "OK";
            btns[Lang.Cancel] = "_CANCEL_";
            showPopup(_url, function (v, m, f) {
                if (v == "OK") {
                    var GIDs = f.SGroupIDs.split(',');
                    var Gnames = f.SGroupNames.split(',');
                    var fgName = $('#tableGroup tr:last .fgroupingname').val();
                    var fgID = $('#tableGroup tr:last .fgroupingname').next().val();
                    $('#tableGroup tr:last .sgroupingname').val(Gnames[0]);
                    $('#tableGroup tr:last .sgroupingname').next().val(GIDs[0]);
                    for (var i = 1; i < GIDs.length; i++) {
                        addGroupClick();
                        $('#tableGroup tr:last .fgroupingname').val(fgName);
                        $('#tableGroup tr:last .fgroupingname').next().val(fgID);
                        $('#tableGroup tr:last .sgroupingname').val(Gnames[i]);
                        $('#tableGroup tr:last .sgroupingname').next().val(GIDs[i]);
                    }
                }
            }, btns);
    }

    $('#visitDate').blur(function() {
        var date = $(this).val();
        if(date){
            var ok = getServerValue('checkvisitorplandaterange', 'VisitorPlan', '&VisitorPlanID=' + $('#VisitorPlanID').val() + '&VisitDate=' + date);
            if (ok == 0) {
                alert(Lang.VisitDateMsg);
                $(this).val('');
            } else if (ok == 2) {
                alert(Lang.RepeatVisitDateMsg);
                $(this).val('');
            }
        }
    });
    //----------------
    // startup code
    //------------------
    var visitorname = ' - ' + getServerValue('getpersonnelname', 'Personnel', '&VisitorPlanID=' + $('#VisitorPlanID').val());
    $('#Title').text(Lang.VisitorPlanDays + visitorname);
    setValue('#Weekdays');
    setChecked('#ISCheckedOut');
    mID = toInt($('#MasterID').val(), -1);
    fillTable("get.aspx?op=GetXml&qd=dailycustomers&VisitorPlanDaysID=" + mID, '#tableGroup', addGroup);
    fillTable("get.aspx?op=GetXml&qd=visitorplandayservice&VisitorPlanDaysID=" + mID, '#tableService', addService);
    function doMore() {
        tabify();
        autoComplete('Grouping', 'groupauto', "#tableGroup .fgroupingname", "#FGroupingID"
               , autoPostPack
               , function (data, i, n, value) {
                   return data[1];
               });
        autoComplete('Grouping', 'groupauto', "#tableGroup .sgroupingname", "#SGroupingID"
       , autoPostPack
       , function (data, i, n, value) {
           return data[1];
       });
        bindAdd();
        bindRemove();
        dateSelect('.dateSelect');
        insertValidation('#subDetailForm');
        autoComplete('VisitorPlanDays', 'visitorplandyasauto', '#RepeatPaternDate', '#RepeatPaternDateID', null, null, null, 'VisitorPlanID=' + $('#VisitorPlanID').val());
        $('#RepeatPaternDate').blur(function() {
            var vPDID = $('#RepeatPaternDateID').val();
            if (vPDID) {
                fillTable("get.aspx?op=GetXml&qd=dailycustomersforrepeatdate&VisitorPlanDaysID=" + vPDID, '#tableGroup', addGroup);
            }
        });
    }
    doMore();
    $(".th td", "#tableGroup").each(function (index) {
        var width = $(this).outerWidth();
        $(this).find('div').css("width", width + "px");
    });
}