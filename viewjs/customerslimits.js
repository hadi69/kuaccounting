﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

function view_customerslimits() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }

    function run(parameters) {
        var more = decodeURIComponent("VisitorPlanDaysID=" + visitorplandaysid);
        var _url = 'service.asmx/GetList?table=CustomersLimit&qd=customerslimit&more='+ more + appendTime();
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.CustomerName, name: 'CustomerName', sortable: true, css_class: 'width-200 align-center' },
                    { display: Lang.MaxCredit, name: 'Credit', sortable: true, process: showMonetary},
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' },
                ],
                buttons:
                [
                   { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                   { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                   { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                sortname: "CustomerName",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight, resizable: gridResizable,
                singleSelect: false
            });
    }
    function showcallback(v, m, f) {
        var qry = (_Context.id == -1) ? 'customerslimitinsert' : 'customerslimitupdate';
        return newMastercallback(v, m, f, 'CustomersLimit', qry, 'ServicesLimit', 'serviceslimitupdate', 'serviceslimitinsert', 'serviceslimitdelete', 'ServicePriceID', '#subDetailForm', show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + '&VisitorPlanDaysID=' + visitorplandaysid + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }
    // ------------------------------
    // startup code
    // ------------------------------
    var visitorplandaysid = $.urlParam('id');
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.CustomerLimit, parentTitle: Lang.VisitorPlanDays, parentUrl: 'list.aspx?table=VisitorPlanDays&menu=admin' } }));

    run();
}