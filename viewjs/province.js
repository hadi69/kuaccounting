﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
93-01-12: file created
*/
function view_province() {

    // ------------------------------
    // startup code
    // ------------------------------
    if ($('#ID') == -1)
        $('#Enabled').attr("checked", 'checked');
    else
        setChecked("#Enabled");
    autoComplete('Country', 'countryauto', '#CountryName', '#CountryID');
    insertValidation();
}