﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
93-03-02: 
    - file created
*/
function view_cartable() {
    function doCommand(com, grid) {
        if (com == 'بازگشت از انبار') {
            createPurchase(grid);
        }
        else {
            var isNew = com == Lang.GBNew;
            if (!isNew) {
                var btn = $(".fbutton span:contains(" + com + ")");
                if (btn.hasClass('new'))
                    isNew = true;
            }
            if (isNew) {
                var more = '&Type=' + WorkflowTypeEnum.Purchase;
                var count = getServerValue('checkworkflow', 'Workflow', more);
                if (count == 0 || count == null) {
                    displayError(Lang.CreateAWorkflowForThisSection);
                    return;
                }
            }
            if (isNew && com != Lang.GBNew) {
                var desiredEntryTaskID = $(".fbutton span:contains(" + com + ")").attr('class').substring('new '.length);
                //alert(desiredEntryTaskID);
                show(-1, null, null, desiredEntryTaskID);
            }
            else
                generalCommand(com, grid, show);
        }
    }
    function run() {
        var _url = 'service.asmx/GetQueryList?table=' + _Context.table + '&qd=cartablelist&UserID=' + _Context.userID + appendTime();
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    //{ display: Lang.ColRowNr, name: 'ZeilNummer', width: 20, sortable: false, align: 'left' },
                    { display: 'RelatedID', name: 'RelatedID', sortable: false, css_class: 'width-60 align-center', hide:true},
                    { display: Lang.Type, name: 'Type', sortable: true, css_class: 'width-100', process: _displayWorkflowType },
                    { display: Lang.No, name: 'NOSerial', sortable: true, css_class: 'width-80 align-center' },
                    { display: Lang.FactorDate, name: 'Date', sortable: true, css_class: 'date-cell' },
                    { display: Lang.EntryDate, name: 'EntryDate', sortable: true, css_class: 'date-cell ' },
                    { display: Lang.PreTask, name: 'PreTitle', sortable: true, css_class: 'width-100 align-center' },
                    { display: Lang.Task, name: 'Title', sortable: true, css_class: 'width-100 align-center' },
                    { display: Lang.State, name: 'State', sortable: true, hide: true, css_class: 'width-80 align-center' },
                    { display: Lang.Duration, name: 'Duration', sortable: true, css_class: 'width-60', process: showTaskDuration },
                    { display: Lang.PreUser, name: 'PreExitUserName', sortable: true, css_class: 'width-100' },
                    { display: Lang.StartUser, name: 'StartUserName', sortable: true, css_class: 'width-100' },
                    { display: Lang.About, name: 'About', hide: true, css_class: 'width-150' },
                    { display: Lang.Detail, name: 'Detail', sortable: true },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' }
                ],
                buttons: [
                    { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand, onaccess: true || hasAccess("store", "cartable", "excel") }
                ],
                searchitems: null,
                sortname: "EntryDate",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight, resizable: gridResizable,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                }
            });
    }
 
    function _displayWorkflowType(celDiv, id) {
        var type = toInt($(celDiv).html(), 0);
        displayWorkflowType(celDiv, type);
    }
    function show(id, celDiv, successCallback) {
        if (!celDiv)
            return;
        var relatedID = getColContent($(celDiv).closest('tr'), 2);
        var wfType = getStatus(getColClass($(celDiv).closest('tr'), 3), 2); //wt1
        if (wfType == WorkflowTypeEnum.StoreDraft)
            storedraftshow(relatedID);// location.href = "list.aspx?table=StoreDraft&menu=storea&id=" + relatedID;
        else if (wfType == WorkflowTypeEnum.Purchase)
            purchaseshow(relatedID);// location.href = "list.aspx?table=Purchase&menu=storea&id=" + relatedID;
        else if (wfType == WorkflowTypeEnum.PurchaseRequest)
            purchaserequestshow(relatedID); // location.href = "list.aspx?table=PurchaseRequest&menu=storea&id=" + relatedID;
        else if (wfType == WorkflowTypeEnum.Request)
            requestshow(relatedID);// location.href = "list.aspx?table=Request&menu=storea&id=" + relatedID;
        else if (wfType == WorkflowTypeEnum.Refuse)
            refuseshow(relatedID);// location.href = "list.aspx?table=Refuse&menu=storea&id=" + relatedID;
        else if (wfType == WorkflowTypeEnum.Return)
            returnshow(relatedID);// location.href = "list.aspx?table=Returns&menu=storea&id=" + relatedID;
        else if (wfType == WorkflowTypeEnum.Transfer)
            transfershow(relatedID);// location.href = "list.aspx?table=Transfer&menu=storea&id=" + relatedID;
        else if (wfType == WorkflowTypeEnum.BuildOrder)
            buildordershow(relatedID);// location.href = "list.aspx?table=BuildOrder&menu=storea&id=" + relatedID;
        else if (wfType == WorkflowTypeEnum.Voucher)
            location.href = "list.aspx?table=DocumentAccounting&id=" + relatedID;
        else
            alert('wfType = ' + wfType + '   id=' + relatedID);
    }

    function changeType() {
        var type = toInt($('#fType').val());
        $("#fPreTitle").unautocomplete();
        $("#fTitle").unautocomplete();
        switch (type) {
            case 0:
                autoComplete('TasK', 'transferphase', "#fPreTitle", "#fTaskID");
                autoComplete('TasK', 'transferphase', "#fTitle", "#fTaskID");
                    break;
            case 1:
                autoComplete('TasK', 'requestphase', "#fPreTitle", "#fTaskID");
                autoComplete('TasK', 'requestphase', "#fTitle", "#fTaskID");
                    break;
            case 2:
                autoComplete('TasK', 'purchasephase', "#fPreTitle", "#fTaskID");
                autoComplete('TasK', 'purchasephase', "#fTitle", "#fTaskID");
                    break;
            case 3:
                autoComplete('TasK', 'purchaserequestphase', "#fPreTitle", "#fTaskID");
                autoComplete('TasK', 'purchaserequestphase', "#fTitle", "#fTaskID");
                    break;
            case 4:
                autoComplete('TasK', 'returnphase', "#fPreTitle", "#fTaskID");
                autoComplete('TasK', 'returnphase', "#fTitle", "#fTaskID");
                    break;
            case 5:
                autoComplete('TasK', 'refusephase', "#fPreTitle", "#fTaskID");
                autoComplete('TasK', 'refusephase', "#fTitle", "#fTaskID");
                    break;
            case 6:
                autoComplete('TasK', 'storedraftphase', "#fPreTitle", "#fTaskID");
                autoComplete('TasK', 'storedraftphase', "#fTitle", "#fTaskID");
                    break;
            case 7:
                    autoComplete('TasK', 'buildorderphase', "#fPreTask", "#fTaskID");
                    autoComplete('TasK', 'buildorderphase', "#fTask", "#fTaskID");
                    break;
        }
        $('#fType').change(function() {
            type = toInt($(this).val());
            $("#fPreTitle").unautocomplete();
            $("#fTitle").unautocomplete();
            switch (type) {
                case 0:
                    autoComplete('TasK', 'transferphase', "#fPreTitle", "#fTaskID");
                    autoComplete('TasK', 'transferphase', "#fTitle", "#fTaskID");
                    break;
                case 1:
                    autoComplete('TasK', 'requestphase', "#fPreTitle", "#fTaskID");
                    autoComplete('TasK', 'requestphase', "#fTitle", "#fTaskID");
                    break;
                case 2:
                    autoComplete('TasK', 'purchasephase', "#fPreTitle", "#fTaskID");
                    autoComplete('TasK', 'purchasephase', "#fTitle", "#fTaskID");
                    break;
                case 3:
                    autoComplete('TasK', 'purchaserequestphase', "#fPreTitle", "#fTaskID");
                    autoComplete('TasK', 'purchaserequestphase', "#fTitle", "#fTaskID");
                    break;
                case 4:
                    autoComplete('TasK', 'returnphase', "#fPreTitle", "#fTaskID");
                    autoComplete('TasK', 'returnphase', "#fTitle", "#fTaskID");
                    break;
                case 5:
                    autoComplete('TasK', 'refusephase', "#fPreTitle", "#fTaskID");
                    autoComplete('TasK', 'refusephase', "#fTitle", "#fTaskID");
                    break;
                case 6:
                    autoComplete('TasK', 'storedraftphase', "#fPreTitle", "#fTaskID");
                    autoComplete('TasK', 'storedraftphase', "#fTitle", "#fTaskID");
                    break;
                case 7:
                    autoComplete('TasK', 'buildorderphase', "#fPreTask", "#fTaskID");
                    autoComplete('TasK', 'buildorderphase', "#fTask", "#fTaskID");
                    break;
            }
        });
    }

// ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['sales,salesinvoice', 'stores,storedraft', 'sales,purchaseinvoice', 'stores,purchase'
        , 'sales,returninvoice', 'stores,returns', 'sales,refuseinvoice', 'stores,refuse', 'stores,transfer', 'stores,request']);
    storeMode = $.urlParam('menu') != 'accountinga';
    if (storeMode) {
        $("#bar").prepend(breadcrumb({pageInfo: {pageTitle: Lang.Cartable, parentTitle: Lang.Store, parentUrl: 'menu/store.aspx'} }));
        setClassStore();
    }
    else {
        $("#bar").prepend(breadcrumb({pageInfo: {pageTitle: Lang.SalesPurchase, parentTitle: Lang.MAccounting, parentUrl: 'menu/accounting.aspx'} }));
        setClassAccounting();
    }
    dateSelect('.dateSelect');
    changeType();
    run();
}