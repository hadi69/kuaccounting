﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
    92-10-16: file created
*/
function view_assessmentfields() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var _url = 'service.asmx/GetList?table=AssessmentField&qd=assessmentfield' + appendTime();
        //alert(_url);
        $("#flexi").flexigrid
                (
                {
                    url: _url,
                    dataType: 'xml',
                    colModel:
                    [
                        { display: 'ID', name: 'ID', sortable: false, align: 'right', hide: true },
                        { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell-big' },
                        { display: Lang.CooperationType, name: 'CooperationTypeName', css_class: 'width-100' },
                        { display: Lang.Title, name: 'Title', sortable: true , css_class: 'width-200' },
                        { display: Lang.Type, name: 'Type', css_class: 'width-300', process: displayAssessmentType },
                        { display: Lang.WeightedValue, name: 'WeightedValue', css_class: 'width-100' },
                        { display: Lang.ChosenValue, name: 'ChosenValue', sortable: true },
                        { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show) }, css_class: 'action-cell' }
                    ],
                    buttons:
                    [
                        { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                        { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                        { separator: true },
                        { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                        { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                    ],
                    searchitems: null,
                    sortname: "Title",
                    sortorder: "asc",
                    usepager: true,
                    useRp: true,
                    rp: gridRp,
                    showTableToggleBtn: _options.gridToggle,
                    width: gridWidth,

                    height: _options.gridHeight, resizable: _options.gridResizable,
                    singleSelect: false,
                    onDoubleClick: function (row, grid, options) {
                        var id = row.id.substring(3);
                        show(id);
                    },
                    onSuccess: function (grid) {
                        restoreSelectedRows('#flexi');
                    }
                }
                );
    }
    function showcallback(v, m, f) {
        return newShowcallback(v, m, f, 'assessmentfieldupdate', 'assessmentfieldinsert', 'AssessmentField', '#subDetailForm', show);
    }

    function show(id, celDiv, successCallback) {
        _Context.id = id;
        saveSelectedRows('#flexi');
        var _url = 'get.aspx?op=GetHtml&table=AssessmentField&qd=assessmentfield&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
        //jscolor.init();
    }
    function displayAssessmentType(celDiv, id) {
        var type = toInt($(celDiv).html(), 0);
        ShowAssessmentType(celDiv, type);
    }
    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['distribution,assessment']);
    setClassAccounting();
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.Assessment, parentUrl: 'menu/distribution.aspx', parentTitle: Lang.MDistribution } }));
    run();
}