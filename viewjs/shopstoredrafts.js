﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-10-30: file created
*/
function view_shopstoredrafts() {
    var storeMode;
    var startupID = -1;
    function doCommand(com, grid) {
        if (com == Lang.CreateDailyStoreDraft) {

            var btns = {};
            btns[Lang.CreateDailyStoreDraft] = 'Create';
            btns[Lang.Close] = 'Cancel';
            _Context.id = -1;
            var op = "GetHtml";
            var _url = 'get.aspx?op=' + op + '&table=' + _Context.table + '&qd=' + _Context.query + '&view=createdailystoredraft' + appendTime();
            saveSelectedRows("#flexi");
            showPopup(_url, createStoreDraftCallback, btns, '1%', null, true);
            return;
        }
        var isNew = com == Lang.GBNew;
        if (!isNew) {
            var btn = $(".fbutton span:contains(" + com + ")");
            if (btn.hasClass('new'))
                isNew = true;
        }
        if (isNew) {
            var more = '&Type=' + WorkflowTypeEnum.StoreDraft;
            var count = getServerValue('checkworkflow', 'Workflow', more);
            if (count == 0 || count == null) {
                displayError(Lang.WorkflowM1);
                return;
            }
        }
        if (isNew && com != Lang.GBNew) {
            var desiredEntryTaskID = $(".fbutton span:contains(" + com + ")").attr('class').substring('new '.length);
            //alert(desiredEntryTaskID);
            show(-1, null, null, desiredEntryTaskID);
        } else if (com == Lang.TemporaryStoreDaraft) {
            //var ok = getServerValue('checktempstoredraft', 'StoreDraft');
            //if (ok >= 1) {
            //    Growl.Error(Lang.TemporaryStoreDaraftMsg, 3000);
            //    return;
            //} else {
                show(-1, null, null, null, true);
            //}
        }
        else
            generalCommand(com, grid, show);
    }
    function run() {
        autoComplete('TasK', 'storedraftphase', "#fTaskName", "#fTaskID");

        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + (_Context.workflowInheritance ? 'storedraft' : 'storedraftnoinheritance') + "&more=__PrOgrAmconD__" + appendTime();
        //alert(_url);
        $("#flexi").flexigrid
        (
        {
            url: _url,
            dataType: 'xml',
            colModel:
            [
                { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'center', hide: true },
                { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                { display: Lang.FactorNo, name: 'NOSerial', sortable: true, css_class: 'width-70 align-center' },
                { display: Lang.FactorDate, name: 'FactorDate', sortable: true, process: capturePriceDate, css_class: 'date-cell' },
                { display: Lang.Store, name: 'StoreName', sortable: true, css_class: 'width-70 align-center' },
                { display: Lang.Comments, name: 'Comments', sortable: true, hide: true, css_class: 'cmnts' },
                { display: Lang.Inventory, name: 'StoreKeeperName', sortable: true, hide: true },
                { display: Lang.Accountant, name: 'AccountantName', sortable: true, hide: true },
                { display: Lang.BuyerName, name: 'BuyerNameComment', sortable: true, css_class: 'width-220 align-center' },
                { display: Lang.Visitor, name: 'VisitorName', sortable: true, css_class: 'align-center', hide: true },
                { display: Lang.User, name: 'UserName', sortable: true, css_class: 'align-center'},
                { display: Lang.TotalS, name: 'TotalPrice', hide: true, sortable: true, process: showMonetary, css_class: 'width-80 align-center', footer: 'sum' },
                { display: Lang.TotalS, name: 'FinalPrice', sortable: true, process: showMonetary, css_class: 'width-80 align-center', footer: 'sum' },
                { display: Lang.TotalAmount, name: 'TotalAmount', sortable: true, css_class: 'width-100 align-center', footer: 'sum' },
                { display: Lang.SumProfit, name: 'SumProfit', hide: true },
                { display: Lang.PreStatus, name: 'PreTaskName', sortable: true, hide: true },
                { display: Lang.Status, name: 'TaskName', sortable: true, hide: true },
                { display: Lang.PayMethod, name: 'PayMethod', sortable: true, css_class: 'width-80 align-center', process: displayPayMethod },
                { display: '', name: 'WorkflowColor', css_class: 'WorkflowColor', hide: true },
                { display: Lang.Status, name: 'VoucherID', hide: true, process: displayVoucherID },
                { display: Lang.Status, name: 'DocItemID', hide: true, process: displayDocItemID },
                { display: Lang.DocumentNumber, name: 'VoucherNo', sortable: false, process: displayVoucherNo, css_class: 'width-70 align-center' },
                { display: '', name: 'PayDate', hide: true },
                { display: '', name: 'PayStatus', hide: true },
                //{ display: Lang.NumPrints, name: 'NumPrints', sortable: true, css_class: 'row-cell' },
                { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function(celDiv, id) { editLink(celDiv, id, show); }, hide: hideColumn("sales", "storedraft", "edit"), css_class: 'action-cell' }
            ],
                buttons: getBtns(),
                searchitems: null,
                sortname: "ID",
                sortorder: "desc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,

                height: gridHeight, resizable: gridResizable,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                },
                onSuccess: function (grid) {
                    postProcessGrid();
                    if (startupID != -1) {
                        show(startupID);
                        startupID = -1;
                    }
                }
            });
    }

    function createStoreDraftCallback(v, m, f) {

        if (v == 'Create') {
            var ok = getServerValue('createdailystoredraft', 'StoreDraft', '&FromDate=' + f.FromDate + '&ToDate=' + f.ToDate + '&MainFactorDate=' + f.MainFactorDate);
            if (ok == 1) {
                runQuery('createdailystoredraftdocument', 'StoreDraft', '&MainFactorDate=' + f.MainFactorDate);
                Growl.Notice(Lang.Success, 3000);
                $("#flexi").flexReload();
            } else if (ok == -1) {
                Growl.Error(Lang.DailyStoreDraftMsg, 3000);
            } else {
                Growl.Error(Lang.Failed, 3000);
            }
            return;
            }
        return true;
    }

    function getBtns() {
        var btns = [];
        btns = getTaskGridButtons(WorkflowTypeEnum.StoreDraft, doCommand, "sales", "storedraft");
        return btns;
    }
    function displayPayMethod(celDiv, id) {
        var _mode = parseInt($(celDiv).html(), 16);
        displayStoreDraftPayMethod(celDiv, _mode);
    }
    function displayVoucherID(celDiv, id) {
        $(celDiv).addClass('_vid');
    }
    function displayDocItemID(celDiv, id) {
        $(celDiv).addClass('_diid');
    }
    function displayVoucherNo(celDiv, id) {
        $(celDiv).addClass('_vno');
    }
    function postProcessGrid() {
        setTimeout(function () {
            showVoucherLinks();
            colorTasks();
            colorComments();
        }, 500);
    }
    function showVoucherLinks() {
        setTimeout(function () {
            $('#flexi tr').each(function () {
                var _id = $('._vid', this).html();
                var _docitemid = $('._diid', this).html();
                //alert(_id);
                if (_id && _id != '')
                    $('._vno', this).html("<a href='list.aspx?table=DocumentAccounting&id=" + _id + "&itemid=" + _docitemid + "'>" + $('._vno', this).html() + "</a>");
            });
        }, 500);
        //ValidVoucher
    }
    function colorComments() {
        $('#flexi tr .cmnts').each(function () {
            var cmnts = $('div', this).html();
            if (cmnts && cmnts.trim() != '')
                $(this).closest('tr').find('td').css('background-color', '#F0AD4E');
        });
    }
    function checkDateIntoFinanceYear(f) {
        if (_Context.fyStart == '') {
            alert(Lang.NoCurYear);
            return false;
        }
        if (f.FactorDate == '' || validateDateIntoFinanceYear(f.FactorDate))
            return true;
        alert(Lang.PurchasesJsM2 + _Context.fyStart + " - " + _Context.fyEnd + Lang.PurchasesJsM3);
        return false;
    }
    function checkNums(f) {
        var res = true;
        $('.delivered').each(function (index, value) {
            if (res) {
                var id = $(this).attr('id');
                id = id.substring(id.indexOf('_') + 1);
                var delivered = toInt($(this).val(), 0);
                var requested = toInt($("#r_" + id).val(), 0);
                if (delivered > requested)
                    res = false;
            }
        });
        if (!res)
            alert(Lang.StoreDraftsJsM1);
        return res;
    }
    function showcallback(v, m, f) {
        function checkCredit(f) {
            var neuremain = 0;
            if (toInt(f.PayMethod, -1) == 2)
                neuremain = toFloat(fromMonetary(f.Total[f.Total.length - 1], f.FactorDate), 0);
            else if (toInt(f.PayMethod, -1) == 3)
                var status = 0;
            if (toFloat(neuremain, 0) > 0) {
                status = getPersonCredit(f.BuyerID, neuremain);
            }
            if (status == -1) {
                Growl.Error(Lang.LowCredit, 3000);
                return false;
            }
            return true;
        }
        if (v == 'TaskImpls') {
            showTaskImpls(_Context.id, WorkflowTypeEnum.StoreDraft);
            return false;
        }
        if (v == 'ChangeLog') {
            showLogChange(_Context.table, _Context.id, WorkflowTypeEnum.StoreDraft);
            return false;
        }
        var _url;
        if (v == 'Print') {
            _url = 'print/storedraft/' + _Context.id;
            popup(_url);
            return false;
        }
        if (v == 'Cancel') {
            $('.ac_results').hide();
            return true;
        }
        if (v == 'Documents') {
            if (_Context.id > 0) {
                _url = 'list.aspx?table=StoreDraftDocument&sid=' + _Context.id;
                popup(_url);
            }
            return false;
        }
        if (v != 'New') {
            if (!checkNums(f))
                return false;
            if (!checkDateIntoFinanceYear(f))
                return false;
            var insertStoreDraftWithBuyerOutBalance = toInt(getSettings('InsertStoreDraftWithBuyerOutBalance'), 0);
            if (insertStoreDraftWithBuyerOutBalance != 1)
                if (!checkCredit(f))
                    return false;
        }
        if (v == 'New' && f.IsTemp) {
            show(-1, null, null, null, true);
            return -1;
        }
        if (!checkProgramStatus())
            return false;

        _Context.priceDate = f.FactorDate;
        if (v != 'OK' && v != 'New') {
            var valid = validateInputs('#detailForm', f);
            if (!valid)
                return false;
            var taskMsg = f.ConfirmTaskMsg;
            //var taskMsg = prompt(Lang.ConfirmTaskMsg, '');
            //if (taskMsg == null)
            //    return false;
            generalMasterscallback('OK', m, f, 'StoreDraft', 'storedraftupdate', 'StoreDraftItems'
                , 'storedraftitemupdate ', 'storedraftiteminsert', 'storedraftitemdelete', 'StoreServiceID', null, null, show);
            if (!_Context.success)
                return false;
            var more = '&RelatedID=' + f.MasterID + '&NextTaskID=' + v + '&UserID=' + _Context.userID + '&Type=' + WorkflowTypeEnum.StoreDraft
                + '&Msg=' + encodeURIComponent(taskMsg) + '&FileName=' + encodeURIComponent(f.FileName);
            if (runQuery('dotransition', 'TaskImpl', more)) {
                setTimeout(function () { $("#flexi").flexReload(); }, 200);
                return true;
            }
            return false;
        }

        if (!checkWorkflowStatus(f))
            return false;
        var qry = (_Context.id == -1) ? 'storedraftinsert' : 'storedraftupdate';
        return newMasterscallback(v, m, f, 'StoreDraft', qry, 'StoreDraftItems'
                , 'storedraftitemupdate', 'storedraftiteminsert', 'storedraftitemdelete', 'StoreServiceID', null, null, show);
    }
    function show(id, celDiv, successCallback, desiredEntryTaskID, isTemp) {
        var btns;
        if (id == -1)
            btns = getDefaultButtons(false);
        else {
            btns = getTaskButtons(WorkflowTypeEnum.StoreDraft, id, false, "sales", "storedraft");
            var numDocs = toInt(getServerValue("getnumsds", "StoreDraftDocument", "ID=" + id), 0);
            if (numDocs > 0)
                btns[Lang.Docs + ' ' + numDocs] = 'Documents';
            else
                btns[Lang.Docs] = 'Documents';
        }
        _Context.id = id;
        var fID = getServerValue('getfinanceyear', 'FinanceYear', null);
        if (fID == -1) {
            alert(Lang.NoCurYear);
            return false;
        }
        var _url = 'get.aspx?op=GetHtml&view=shopstoredraft&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + '&FinanceYearID=' + fID;
        if (desiredEntryTaskID)
            _url += '&DesiredEntryTaskID=' + desiredEntryTaskID;
        if (isTemp) {
            _url += '&IsTemp=' + 1 + '&FromTempBtn=' + 1;
        }
        _url += appendTime();
        showPopup(_url, showcallback, btns, null, successCallback, true);
    }

    $('#BuyPriceUpdate').click(function () {
        var access = hasAccess("general", "generalsettings", "buypriceupdate");
        if (!access) {
            displayError(Lang.AccessToThisPageIsProhibited);
            return false;
        }
        var _url = 'get.aspx?op=GetHtml&table=StoreDraft&view=storedraftupdatebuyprice' + appendTime();
        var btns = {};
        btns[Lang.OK] = "OK";
        btns[Lang.Cancel] = "_CANCEL_";

        showPopup(_url, function (v, m, f) {
            if (v == "OK") {
                var more = '&FromDate=' + f.FromDate;
                more += '&ToDate=' + f.ToDate;
                more += '&CommodityID=' + f.CommodityID;
                more += '&BuyPrice=' + f.BuyPrice.replace(/\,/g, '');
                if (f.FromNum) {
                    more += '&FromNum=' + f.FromNum;
                } else {
                    more += '&FromNum=NULL';
                }
                if (f.ToNum) {
                    more += '&ToNum=' + f.ToNum;
                } else {
                    more += '&ToNum=NULL';
                }
                var ok = getServerValue('storedraftbuypriceupdate', 'StoreDraft', more);
                Growl.Notice(ok + Lang.NumOfAffectedRows, 4000);
            }
        }, btns);
    });

    // ------------------------------
    // startup code
    // ------------------------------
    dateSelect('.dateSelect');
    checkAccess(['sales,shopsalesinvoice', 'storeforms,storedraft']);
    storeMode = $.urlParam('menu') != 'accountinga';
    if (storeMode) {
        $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.StoreDraft, parentTitle: Lang.MStoreForms, parentUrl: 'menu/storeforms.aspx' } }));
        setClassStoreForms();
    }
    else {
        $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.SalesInvoice, parentTitle: Lang.SellAndBuy, parentUrl: 'menu/sales.aspx' } }));
        setClassSales();
    }
    startupID = $.urlParam('id');
    if (!startupID)
        startupID = -1;
    run();
}