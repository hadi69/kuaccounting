﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
    95-01-24: file created
*/
function view_costcenter() {
    var cnt = 0, mID = -1;
    // ------------------------------
    // startup code
    // ------------------------------
    //for autocomplete bugg
    $('#Name').focus();
    setTimeout(function () {
        $('#ParentName').focus();
    }, 1);
    mID = toInt($('#MasterID').val(), -1);
    if (mID == -1)
        $('#Enabled').attr("checked", 'checked');
    else
        setChecked("#Enabled");
    $('.popupTitle').html(_Context.title);

    autoComplete('CostCenter', 'costcenterauto', '#ParentName', '#ParentID', autoPostPack);

    function autoPostPack(event, data, formatted) {
        $(event.target).next().val(data[0]); // ID
    }
    insertValidation();
}