﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
92-10-21: ReceiverID autocomplete
92-10-19: file created
*/
function view_paycheck() {
    makeChosen();
    $("#BankAccountCheckName").focus();
    if (_Context.id != -1) {
        replacePrintButton({
            primary: {
                text: Lang.Print,
                href: Router.Generate("print-bank-print", { params: { id: _Context.id } })
            },
            other: [
                {
                    text: Lang.PayCheckReceipt,
                    href: Router.Generate("print-paycheckreceipt", { params: { id: _Context.id }, query: { print: "true" } })
                }
            ]
        });
    } else {
        replacePrintButton({
        });
    }
    function displayDueDate() {
        $('#DueDateStr').html(dateToString($('#DueDate').val()));
    }
    function displayAmount() {
        $('#AmountStr').html(numToString($('#Amount').val()));
    }

    function lastValues() {
        //var id = toInt($('#subDetailForm #PersonnelID').val(), 0);
        //if (id <= 0)
        //    return;
        //var u = 'get.aspx?op=Auto&table=Checks&qd=checkslast&q=' + id + appendTime();
        //var row = getServerValues(u);
        //if (row && row.length && row.length > 2) {
        //    // AccountNo, Bank, Branch
        //    $('#subDetailForm #AccountNo').val(row[0]);
        //    $('#subDetailForm #Bank').val(row[1]);
        //    $("#subDetailForm #Branch").val(row[2]);
        //}
    }

    var cnt = 0;

    function addRow(node) {

        var row = '<tr>';
        row += '<td data-th="' + Lang.ColRowNr + '" class="row-number align-center  width-40">' + (cnt + 1) + '</td>';

        row += '<td class="width-300" data-th="' + Lang.TopicName + '">'
            + '<input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="text" id="tpn_' + cnt + '" name="TreePathsName" value="'
            + getNodeVal(node, "TreePathsName") + '" class="treepathsname form-control input-sm align-center"/>'
            + '<input type="hidden" id="tp_' + cnt + '" name="TreePaths" value="' + getNodeVal(node, "TreePaths", 0) + '" class="treepaths" />'
            + '</td>';

        row += '<td data-th="' + Lang.Amount + '" class="align-center acc width-200">'
            + '<input type="text" id="a_' + cnt + '" name="Amounts" value="'
            + getNodeVal(node, "Amounts") + '" class="amounts monetary validate[custom[integer]] form-control input-sm align-center" />'
            + '</td>';

        row += '<td data-th="' + Lang.Person + '" class="width-200 align-center">'
            + '<input type="text" id="p_' + cnt + '" name="PersonName" value="' + getNodeVal(node, "PersonName") + '" class="personname form-control input-sm" />'
            + '<input type="hidden" name="PersonID" value="' + getNodeVal(node, "PersonID") + '" />'
            + '</td>';

        row += '<td data-th="' + Lang.Project + '" class="width-200 align-center">'
            + '<input type="text" id="w_' + cnt + '" name="WardName" value="' + getNodeVal(node, "WardName") + '" class="wardname form-control input-sm" />'
            + '<input type="hidden" name="WardIDs" value="' + getNodeVal(node, "WardIDs") + '" />'
            + '</td>';

        row += '<td data-th="' + Lang.Equipment + '" class="width-200 align-center">'
            + '<input type="text" id="e_' + cnt + '" name="EquipmentName" value="' + getNodeVal(node, "EquipmentName") + '" class="equipmentname form-control input-sm" />'
            + '<input type="hidden" name="EquipmentIDs" value="' + getNodeVal(node, "EquipmentIDs") + '" />'
            + '</td>';
        row += '<td data-th="' + Lang.CostCenter + '" class="width-200 align-center">'
            + '<input type="text" id="c_' + cnt + '" name="CostCenterName" value="' + getNodeVal(node, "CostCenterName") + '" class="costcentername form-control input-sm" />'
            + '<input type="hidden" name="CostCenterIDs" value="' + getNodeVal(node, "CostCenterIDs") + '" />'
            + '</td>';

        row += '<td class="align-center action-remove width-40">'
        + '<a id="r_' + cnt + '" class="btn btn-danger btn-xs btn-block btn-remove remove">'
        + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
        + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
        + '</a>'
        + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }

    function autoPostPack(event, data, formatted) {
        $(event.target).next().val(data[0]); // ID
    }

    $('.add').click(addClick);
       function addClick() {
        var table = $('#tableItems');
        $('tr:last', table).after(addRow(null));
        disableKeydownOnLastInputByName('CostCenterName');
        Growl.Notice(Lang.RowAddedSuccessfully);
        var lastRow = $('tr:last', table);
         var more = null; //'Model=' + supplier;

        var amount = toFloat($('#Amount').val(), 0);
        var rowAmount = 0;

        $('.amounts').each(function (index, value) {
            rowAmount += toFloat($(value).val(), 0);
        });

        $('.amounts', lastRow).val(amount - rowAmount);

        autoCompleteFree('Equipment', 'equipmentchildsauto', "#tableItems tr:last .equipmentname", "#justfurfun"
            , autoPostPack
            , function (data) {
                return data[1];
            });

        autoCompleteFree('CostCenter', 'costcenterchildsauto', "#tableItems tr:last .costcentername", "#justfurfun"
            , autoPostPack
            , function (data) {
                return data[1];
            });

        autoCompleteFree('Ward', 'wardchildsauto', "#tableItems tr:last .wardname", "#justfurfun"
            , autoPostPack
            , function (data) {
                return data[1];
            });


        autoComplete('Personnel', 'personnelaccauto', "#tableItems tr:last .personname", "#justfurfun"
            , autoPostPack
            , function (data, i, n, value) {
                return data[1];
            });

        autoComplete('Trees', 'treesauto', "#tableItems .treepathsname", "#justfurfun"
            , function (event, data, formatted) {
                // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due, TopicTitle, ProgramCode, Nature, EquipmentNeed, CanHaveSubTree
                var hasChild = toBool(data[5]), canHaveSubTree = toBool(data[12]);
                if (hasChild || canHaveSubTree) {
                    $('#FromTopic, #FromTopicID').val('');
                    if (hasChild)
                        alert(Lang.HasChild);
                    else
                        alert(Lang.HasSubTreeBut);
                }
                $(event.target).next().val(data[0]);
            }, formatTreePath);

        $('.remove', lastRow).click(function () {
            removeRow($(this));
        });
        monetaryInputsSet('#subDetailForm');
        monetaryKeys('#SubDetailForm');
        commaInputs('#subDetailForm');
        insertValidation('#subDetailForm');
        $('input[name=TreePathsName]', lastRow).focus();
    };

    function removeRow($who) {
        var id = $who.attr('id'); // r_1
        id = id.substring(id.indexOf('_') + 1); // r_1  -->  1
        $('#tpn_' + id).val(""); // TreePathName
        $('#tp_' + id).val(''); // TreePath
        $who.parent().parent().hide();
        Growl.Notice(Lang.DeletedSuccessfully);
    }

    function showDivs() {
        if (($('#dueD').val() == 1 || $('#subDetailForm #DueTreePath').val() != '') || $('#DailyCheck').attr('checked')) {
            $('#dueDiv').show();
        }
        else {
            $('#dueDiv').hide();
        }
        if (($('#oweD').val() == 1 || $('#subDetailForm #OweTreePath').val() != '') && !$('#MultiPerson').attr('checked')) {
            $('#oweDiv').show();
        }
        else {
            $('#oweDiv').hide();
        }
    }

    function bindVouchers() {
        // Vouchers
        var _autourl = 'get.aspx?op=Auto&table=DocumentAccounting&qd=paycheckvouchers&q=' + $('#MasterID').val();
        var res = getRowtable(_autourl);
        if (res && res.length > 0) {
            $('#Voucher1').attr('href', "list.aspx?table=DocumentAccounting&id=" + res[0][0] + "&itemid=" + res[0][2]);
            $('#Voucher1').html(res[0][1]);
        }
        if (res && res.length > 1) {
            $('#Voucher2').attr('href', "list.aspx?table=DocumentAccounting&id=" + res[1][0] + "&itemid=" + res[1][2]);
            $('#Voucher2').html(res[1][1]);
        }
    }

    function showLabel() {
        var checked = $('#MultiPerson').attr('checked');
        if (checked) {
            $('.labels').hide();
            $('.persons').show();
        } else {
            $('.labels').show();
            $('.persons').hide();
        }
    }

    function commentsList() {
        $('.ac_results').hide();
        var _url = 'get.aspx?op=GetHtml&table=Paycheck&view=paycheckcommentselect' + appendTime();
        _url += appendTime();
        var btns = {};
        btns[Lang.Cancel] = "_CANCEL_";
        showPopup(_url, function (v, m, f) {
            if (f.SComments != '') {
                $('#Comments').val(f.SComments);

            }
            $('#Comments').focus();
        }, btns);
    }
    // ------------------------------
    // startup code
    // ------------------------------
    if ($("#subDetailForm #PayDate").val() == '')
        $("#subDetailForm #PayDate").val(_Context.today);
    dateSelect('.dateSelect');
    setValue('#Status');
    $('#HiddenStatus').val($('#Status').val());
    setChecked('#DailyCheck');
    setChecked('#MultiPerson');
    setChecked('#AddToReceiverName');

    //$('#Comments').unbind('keyup');
    $('#Comments').keyup(function (e) {
        if (e.keyCode === 40) {
            commentsList();
        }
    });

    var mID = toInt($('#MasterID').val(), -1);

    if (mID > 0)
        $("#Amount").focus();

    triggerChosenUpdate("#Status");

    $('#DailyCheck').change(showDivs);
    $('#MultiPerson').change(function() {
        showLabel();
        if ($(this).attr('checked')) {
            addClick();
        }
    });

    if (!hasValuePositive($('#UserID').val()))
        $('#UserID').val(_Context.userID);
    if (!hasValuePositive($('#MasterID').val()))
        $('#Status').val(CheckStatusEnum.New);

    if (!$('#setstatus').val())
        $('#setstatus').val($('#Status').val());

    if ($('#setstatus').val() == CheckStatusEnum.Received) {
        $('#Status, #HiddenStatus').val(CheckStatusEnum.Received);
    }
    if ($('#setstatus').val() == CheckStatusEnum.New) {
        $('#Status, #HiddenStatus').val(CheckStatusEnum.New);
    }
    else if ($('#setstatus').val() == CheckStatusEnum.Refused) {
        $('#Status, #HiddenStatus').val(CheckStatusEnum.Refused);
        triggerChosenUpdate("#Status, #HiddenStatus");
    }
    else if ($('#setstatus').val() == CheckStatusEnum.TakeBack) {
        $('#DueTreePathName').val('');
        $('#OweTreePathName').val('');
        $('#Status, #HiddenStatus').val(CheckStatusEnum.TakeBack);
    }

    autoComplete('BankAccountCheck', 'bankaccountcheckauto', '#BankAccountCheckName', '#BankAccountCheckID'
    , function (event, data, formatted) {
        $('#BankName').val(data[3]);
        $('#BankTreePath').val(data[2]);

        // serial : get unused one
        var serial = toInt(getServerValue("reternpaycheckminserial", "PayCheck", "&BankAccountCheckID=" + data[0]), 0);
        if (!serial) {
            alert(Lang.PayCheckJsM1);
            runQuery('bankaccountcheckupdateformpaycheck', 'PayCheck', '&ID=' + data[0]);
        }
        else {
            $('#Serial').val(serial);
        }

        $('#Serial').attr('readonly', false);
        $('#Serial').unautocomplete();
        // #Res.Serial, #Res.Serial Serial2, Amount, DueDate, ReceiverName, No, documentNo, Status
         autoComplete('BankAccountCheck', 'bankaccountcheckserialauto', '#Serial', '#justforfun'
             , function (event, data, formatted) {
                 if (data[5] || data[6] || data[7] == 11) // has no or DocumentNo or disproof
                     $('#Serial').val('');
                 else
                     $('#Serial').val(data[0]);
             }, function (data, i, n, value) {
                    // $('#Serial').val('');
                     if (data[5] && data[7] != 11) {
                     // used// data[7] != 11//disproof checks
                                 return Lang.Serial + " : " + data[0] + "<br />" + Lang.PayCheck + " - " + Lang.No + " : " + data[5]
                                     + ", " + Lang.Reciever + " : " + data[4]
                                    + "<br />" + Lang.DueDate + " : " + data[3]
                                    + ", " + Lang.Amount + " : " + data[2];
                     }
                     if (data[6]) {
                         return Lang.Serial + " : " + data[0] + "<br />" + Lang.UsedInDocAccounting + " : " + data[6];
                     }
                     if (data[7] == 11) {
                         return Lang.Serial + " : " + data[0] + "<br />" + Lang.DisProof;
                     }
                     return Lang.Serial + " : " + data[0] + "<br />" + Lang.Unused;
                     }, null, null, null, 'TreePath=NULL&BankAccountCheckID=' + data[0]);
    });

    autoComplete('Trees', 'treesbanksauto', '#BankName', '#BankTreePath'
          , function (event, data, formatted) {
              // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due
              var hasChild = data[5] != '';
              if (hasChild) {
                  $('#BankName, #BankTreePath').val('');
                  alert(Lang.HasChild);
              }
          }, formatTreePath, null, null);
    autoComplete('Personnel', 'personnelselectauto', '#subDetailForm #ReceiverName', '#subDetailForm #ReceiverID', lastValues
        , function (data, i, n, value) {
            $('#subDetailForm #ReceiverID').val('');
            return formatPersonnelItem(data);
        });
    autoCompleteTreePath('#subDetailForm #TreePathName', '#subDetailForm #TreePath', '#subDetailForm #selTree');
    autoCompleteTreePath('#subDetailForm #DueTreePathName', '#subDetailForm #DueTreePath', '#subDetailForm #selDueTree');
    autoCompleteTreePath('#subDetailForm #OweTreePathName', '#subDetailForm #OweTreePath', '#subDetailForm #selOweTree');
    autoComplete('Ward', 'wardchildsauto', '#WardName', '#WardID', null, null
    , function (data, value) {
        return data[1]; //3
    }, null, null, null, false);
    autoComplete('Equipment', 'equipmentchildsauto', '#EquipmentName', '#EquipmentID', null, null
        , function (data, value) {
            return data[1]; // 3
        }, null, null, null, false);
    autoComplete('CostCenter', 'costcenterchildsauto', '#CostCenterName', '#CostCenterID', null, null
    , function (data, value) {
        return data[1]; // 3
    }, null, null, null, false);
    lastValues();

    showDivs();
    showLabel();
    bindVouchers();

    fillTable("get.aspx?op=GetXml&qd=paycheckitems&PayCheckID=" + mID, '#tableItems', addRow);

    $('.remove').click(function () {
        removeRow($(this));
    });
    
    $('#DueDate').blur(displayDueDate);
    $('#Amount').blur(displayAmount).keyup(displayAmount);
    
    insertValidation('#subDetailForm');
    monetaryInputsSet('#subDetailForm', '#PayDate');
    commaInputs('#subDetailForm');

    displayDueDate();
    displayAmount();
}