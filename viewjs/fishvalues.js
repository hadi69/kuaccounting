﻿function view_fishvalues() {
    autoComplete('FishDefs', 'enumauto1', '#FishDefName', '#FishDefID', function (event, data, formatted) {
        autoCompleteFree('FishDefFields', 'fishdeffieldauto', "#FieldName", "#FieldID",
            function (e, d, f) {
                
                $("#FieldID").val(d[0]);
                $("#FieldName").val(d[1]);
                if (d[2] > 0) {
                    $("#FieldID").attr('data-deduct', d[2]);
                }
            }, null, null, "FishDefID=" + data[0]);
    });


    $("#btnEdit").click(function () {
        var year = $("#Year").val();
        var month = $("#Month").val();
        var ffid = $("#FieldID").val();
        var fieldName = $("#FieldName").val();
        var fishDefId = $("#FishDefID").val();
        if (year == "" || month == "" || ffid == "" || fishDefId == "") {
            alert(Lang.AllFieldsNeccessury);
            return false;
        }
        show(year, month, ffid, fieldName, fishDefId);
    });

    function showcallback(v, m, f) {
        if (v == 'Calc') {
            var fishdefId = $("#FishDefID").val();
            $('#tableItems tr').each(function () {
                if ($(this).find('td:nth-child(4)').find('input').hasClass('changed')) {
                    debugger;
                    var fishId = $(this).find('td:nth-child(2)').find('input:first').val();
                    var personnelId = $(this).find('td:nth-child(2)').find('.PersonnelID').val();
                    var fishYear = $("#FishYear").val();
                    var fishMonth = $("#FishMonth").val();
                    var val = toFloat($(this).find('td:nth-child(4)').find('input').val(), -1);
                    if (fishId > 0 && val >= 0) {
                        var orderId = getServerValue("getorderid", "Fish", "&ID=" + fishId);
                        if (orderId==null) {
                            orderId = -1;
                        }
                        calcAllFishs(v, m, f, fishId, orderId, fishdefId, personnelId,fishYear,fishMonth);
                    }
                }
            });
            Growl.Notice("محاسبه با موفقیت انجام شد");
            return false;
        }
        if (v == 'OK') {
            generalDetailscallback(v, m, f, 'FishValues', 'fishvaluesupdate', 'fishvaluesinsert', 'fishvaluesdelete', 'FishDefFieldID', null, null, show);
            return false;
        }
        return true;
    }

    function show(fishYear, fishMonth, fishdeffieldid, fieldname, fishDefId) {
        var btns = getDefaultButtons();
        btns[Lang.Calculate] = "Calc";
        var url = "get.aspx?op=GetHtml&table=FishValues&view=fishvalue&FishYear=" + fishYear + "&FishMonth=" + fishMonth + "&FishDefFieldID=" + fishdeffieldid + "&FieldName=" + fieldname + "&FishDefID=" + fishDefId + "&DeductID=" + $("#FieldID").attr('data-deduct') + appendTime();
        showPopup(url, showcallback, btns);
    }

    function calcAllFishs(v, m, f, fishId, orderId, fishdefId, personnelId, fishYear, fishMonth) {
        var _url = "get.aspx?op=GetXml&table=FishValue&qd=formulafishvalue&FishID=" + fishId;
        $.ajax({
            url: _url,
            type: 'GET',
            dataType: 'xml',
            async: false,
            success: function (xmlDoc) {
                var fields = "PersonnelID-" + personnelId + "|FishYear-" + fishYear + "|FishMonth-" + fishMonth+"|";
                var xNode = (xmlDoc.firstChild.nextSibling) ? xmlDoc.firstChild.nextSibling : xmlDoc.firstChild;
                var items = xNode.childNodes[0];
                $(items).find("R").each(function () {
                    fields += getNodeVal($(this), "FieldName") + "-" + getNodeVal($(this), "Value") + "|";
                });
                if (fields.length > 0) {
                    fields = fields.substr(0, fields.length - 1);
                    fields = calcOrder(v, m, f, "", orderId, fishdefId, fields, "#detailForm", fishId);
                    insertFishValue(v, m, f, fishId, fields);
                }

            },
            error: ajaxError
        });
    }
    function insertFishValue(v, m, f, fishId, fields) {
        var _url = "set.aspx?op=InsertFishValues&fishId=" + fishId + "&fields=" + fields + appendTime();
        $.ajax({
            type: "POST",
            url: _url,
            async: false,
            success: function (html) {
                if (html.indexOf('Error:') != 0 && html.indexOf('خطا:') != 0) {
                    message(html);
                    _Context.resultID = parseInt(html, 10);
                    _Context.success = true;
                }
                else {
                    displayError(html);
                }
            },
            error: function () {
                _Context.success = false;
            }
        });
    }
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.EditFishValues, parentTitle: Lang.MSalary, parentUrl: 'menu/salary.aspx' } }));
}