﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-12-11: combined with salesinvoice
    92-10-30: file created
*/
function view_createdailystoredraft() {
    //$("#tableItems").smartTable();
    var cnt = 0;
    function addRow(node) {
        var row = '<tr class="datarow">';

        row += '<td class="align-center row-number" data-th="' + Lang.ColRowNr + '">' + (cnt + 1) + '</td>'; // Row

        row += '<td class="align-center" data-th="' + Lang.User + '">'
        + '<input type="text" id="un_' + cnt + '" name="UserName" value="' + getNodeVal(node, "UserName") + '" class="username form-control input-sm" />'
        + '</td>';


        row += '<td class="acc" data-th="' + Lang.CashSale + '">'
            + '<input type="text" id="cs_' + cnt + '" name="CashSale" value="' + getNodeVal(node, "CashSale", 0) + '" class="cashsale monetary validate[custom[integer]] form-control input-sm align-center" readonly="readonly" />'
            + '</td>';

        row += '<td class="acc" data-th="' + Lang.BankSale + '">'
            + '<input type="text" id="bs_' + cnt + '" name="BankSale" value="' + getNodeVal(node, "BankSale", 0) + '" class="banksale monetary validate[custom[integer]] form-control input-sm align-center" readonly="readonly" />'
            + '</td>';

        row += '<td class="acc" data-th="' + Lang.OweSale + '">' // TotalPrice
            + '<input type="text" id="os_' + cnt + '" name="OweSale" value="' + getNodeVal(node, "OweSale", 0) + '" class="owesale monetary validate[custom[integer]] form-control input-sm align-center" readonly="readonly" />'
            + '</td>';

        row += '<td class="acc" data-th="' + Lang.CashReturn + '">' // TotalPrice
            + '<input type="text" id="cr_' + cnt + '" name="CashReturn" value="' + getNodeVal(node, "CashReturn", 0) + '" class="cashreturn monetary validate[custom[integer]] form-control input-sm align-center" readonly="readonly" />'
            + '</td>';

        row += '<td class="acc" data-th="' + Lang.OweReturn + '">' // TotalPrice
            + '<input type="text" id="or_' + cnt + '" name="OweReturn" value="' + getNodeVal(node, "OweReturn", 0) + '" class="owereturn monetary validate[custom[integer]] form-control input-sm align-center" readonly="readonly" />'
            + '</td>';

        row += '<td class="acc" data-th="' + Lang.CashOutcome + '">' // TotalPrice
            + '<input type="text" id="co_' + cnt + '" name="CashOutcome" value="' + getNodeVal(node, "CashOutcome", 0) + '" class="cashoutcome monetary validate[custom[integer]] form-control input-sm align-center" readonly="readonly" />'
            + '</td>';

        row += '<td class="acc" data-th="' + Lang.CashIncome + '">' // TotalPrice
            + '<input type="text" id="ci_' + cnt + '" name="CashIncome" value="' + getNodeVal(node, "CashIncome", 0) + '" class="cashincome monetary validate[custom[integer]] form-control input-sm align-center" readonly="readonly" />'
            + '</td>';

        row += '<td class="acc" data-th="' + Lang.CashRemain + '">' // TotalPrice
            + '<input type="text" id="crm_' + cnt + '" name="CashRemain" value="' + getNodeVal(node, "CashRemain", 0) + '" class="cashremain monetary validate[custom[integer]] form-control input-sm align-center" readonly="readonly" />'
            + '</td>';

        row += '<td class="acc" data-th="' + Lang.BankRemain + '">' // TotalPrice
            + '<input type="text" id="brm_' + cnt + '" name="BankRemain" value="' + getNodeVal(node, "BankRemain", 0) + '" class="bankremain monetary validate[custom[integer]] form-control input-sm align-center" readonly="readonly" />'
            + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }

    // ------------------------------
    // startup code
    // ------------------------------

    if ($("#FromDate").val() == '')
        $("#FromDate").val(_Context.today);
    if ($("#ToDate").val() == '')
        $("#ToDate").val(_Context.today);
    if ($("#MainFactorDate").val() == '')
        $("#MainFactorDate").val(_Context.today);
    dateSelect('.dateSelect');

    function showReport(parameters) {
        $('.datarow').remove();
        cnt = 0;
        fillTable("get.aspx?op=GetXml&qd=dailystoredraftreport&FromDate=" + $('#FromDate').val() + "&ToDate=" + $('#ToDate').val(), '#tableItems', addRow);
        commaInputs();
    }
    showReport();
    $('#ShowReport').click(showReport);
}