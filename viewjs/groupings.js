﻿ /// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-10-19: file created
*/
function view_groupings() {
    function doCommand(com, grid) {
        if (com == Lang.GBExcel) {
            var gt = GroupingTypeEnum.Person + "^" + Lang.Persons
                + "^" + GroupingTypeEnum.Service + "^" + Lang.Product
                + "^" + GroupingTypeEnum.Visitor + "^" + Lang.Visitor
                + "^" + GroupingTypeEnum.Supplier + "^" + Lang.Suppliers;
            excelCommand(com, grid, show, "hide=id;parentid&GroupingType=" + encodeURIComponent(gt));
        }
        else
            generalCommand(com, grid, show);
    }
    function run() {
        var orderby = "GroupingType, Code";
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + "&orderby=" + orderby + appendTime();
        if (_Context.isStoreUser) {
            _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=storeusergrouping&orderby=' + orderby + appendTime();
        } 
        //alert(_url);
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell-big' },
                    { display: 'ID', name: 'ID', css_class: '_id', hide: true },
                    { display: Lang.Code, name: 'Code', sortable: false, css_class: 'width-200' },
                    { display: Lang.Type, name: 'GroupingType', sortable: false, process: _displayGroupingEnum, css_class: 'row-cell' },
                    { display: Lang.headline, name: 'Name', sortable: false },
                    { display: 'ParentID', name: 'ParentID', css_class: '_parentID', hide: true },
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' }
                ],
                buttons:
                [
                   { name: Lang.GBNew, bclass: 'new', onpress: doCommand, },
                   { name: Lang.GBDel, bclass: 'delete', onpress: doCommand, },
                   { name: Lang.GBPrint, bclass: 'print', onpress: doCommand, },
                   { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                searchitems: null,
                sortname: "GroupingType, Code",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rpOptions: [1000, 10000],
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,

                height: gridHeight,
                singleSelect: true,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                },
                onSuccess: function (grid) {
                    colorGrid();
                }
            }
            );
    }
    function colorGrid() {
        if (_Context.isStoreUser) {
            var relatedGroupingId = getServerValue('getstoreuserreleatedgroup', 'Users');
        }
        setTimeout(function () {
            $('#flexi tr').each(function () {
                var id = toInt($('._id div', this).html(), 0);
                var parentID = toInt($('._parentID div', this).html(), 0);
                var type = toInt($('td:nth-child(4)', this).find('div').attr('class').substr(1), 0);
                $(this).addClass("treegrid-" + (type * 1000 + id));
                if (_Context.isStoreUser) {
                    if (parentID && (parentID != relatedGroupingId))
                        $(this).addClass("treegrid-parent-" + (type * 1000 + parentID));
                } else {
                    if (parentID)
                        $(this).addClass("treegrid-parent-" + (type * 1000 + parentID));
                }

            });
            $jQuery1_9('#flexi').treegrid({ initialState: 'collapsed' });
        }, 1000);
        //ValidVoucher
    }
    function _displayGroupingEnum(celDiv, id) {
        var _mode = parseInt($(celDiv).html(), 10);
        displayGroupingTypeEnum(celDiv, _mode);
    }
    function showcallback(v, m, f) {
        if (v == 'OK') {
            var parentId = toInt(f.ParentID, -1);
            if (parentId > 0) {
                var parentType = toInt(getServerValue('getgroupingtype', 'Grouping', '&ID=' + parentId), -1);
                var type = toInt(f.GroupingType, -1);
                if (parentType != type) {
                    Growl.Error(Lang.GroupingTypeError, 3000);
                    return false;
                }
            }
        }
        var qry = (_Context.id == -1) ? 'groupinginsert' : 'groupingupdate';

        return newMasterscallback(v, m, f, 'Grouping', qry, 'GroupingMember,GroupingPrice'
                , 'groupingmemberupdate,groupingpriceupdate'
                , 'groupingmemberinsert,groupingpriceinsert'
                , 'groupingmemberdelete,groupingpricedelete'
                , 'OtherName,ServicePriceID'
                , 'groupingextra', '#subDetailForm', show);
        //return newMasterscallback(v, m, f, 'Grouping', qry, 'GroupingMember', 'groupingmemberupdate', 'groupingmemberinsert', 'groupingmemberdelete', 'OtherName', '#subDetailForm', show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }
    // ------------------------------
    // startup code
    // ------------------------------
    if (_Context.isStoreUser) {
        $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.Grouping} }));
    } else {
        checkAccess(['sales,grouping']);
        checkLevel(2);
        $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.Grouping, parentTitle: Lang.SellAndBuy, parentUrl: 'menu/sales.aspx' } }));
        setClassSales();
    }

    run();
}