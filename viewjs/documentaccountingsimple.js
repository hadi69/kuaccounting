﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
92-10-16: 
- file created
- Date: dateSelect
*/
function view_documentaccountingsimple() {
    makeChosen();

    var DSimpleTypeEnum = {
        None: -1,
        Bank2Bank: 0,
        Fund2Bank: 1,
        Person2Bank: 2,
        Bank2Person: 3,
        Person2Fund: 4,
        Fund2Person: 5,
        Fund2Topic: 6,
        Topic2Fund: 7,
        Topic2Bank: 71,
        Bank2Person2: 8,
        Bank2Topic: 9,
        Topic2Topic: 10
    }
    var curType = DSimpleTypeEnum.Bank2Bank;

    function updateTotal() {
        var price = toFloat($('#Price').val(), 0);
        var discount = toFloat($('#Discount').val(), 0);
        var checkAmount = toFloat($('#CheckAmount').val(), 0);
        var payCheckAmount = toFloat($('#PayCheckAmount').val(), 0);
        var sum = 0.0;
        sum += price;
        sum += checkAmount;
        sum += payCheckAmount;
        sum -= discount;
        sum = sum.toFixed(_Context.decimalPoints);
        $('#TotalDue').val(numberWithCommas(sum));
    }

    function updateDoComments() {
        var from = $('#FromTopic').val();
        var to = $('#ToTopic').val();
        var titleFrom = Lang.From;//$('label[for="FromTopic"]').text();
        var titleTo = Lang.To2;// $('label[for="ToTopic"]').text();
        var cmnts = titleFrom + " `" + from + "` " + titleTo + " `" + to + "`";
        if (!hasValuePositive($('#PersonnelID').val()) && $('#PersonnelName').val())
            cmnts += ' - ' + $('#PersonnelName').val();
        $('#DoComments').val(cmnts);
    }
    function typeChange() {
        var type = toInt($('#TypeSel').val(), DSimpleTypeEnum.None);
        setTypeChange(type);
        manageUiBasedOnType(type);
    }
    function setTypeChange(type, changeFOrT) {
        if ($('#SsType').val()) {
            curType = toInt($('#SsType').val());
            $('#SsType').val('');
            topicChange();
        }
        else {
            curType = type;
        }
        $('#SType').val(type);
        $('#TD').hide();
        if (changeFOrT == 'f') {
            $('#FromTopic').unautocomplete();
            $('#FromTopic, #FromTopicID').val('');
        } else if (changeFOrT == 't') {
            $('#ToTopic').unautocomplete();
            $('#ToTopic, #ToTopicID').val('');
        } else {
            $('#FromTopic, #ToTopic').unautocomplete();
            $('#FromTopic, #FromTopicID').val('');
            $('#ToTopic, #ToTopicID').val('');
        }
        $('#changeFromTopic, #changeToTopic').css('visibility', 'hidden');
        $('.check, .paycheck').hide();
        $('#DiscountContainer').hide();
        $('#TransferPriceContainer').hide();
        $('#Discount').val('0');
        $('.personnelDiv,.projectDiv').hide();
        $('#PersonnelName, #PersonnelID').val('');
        $('#ProjectName, #ProjectID').val('');

        var fromType = 0, toType = 0; // 0:Bank, 1:Fund, 2:Person, 3:TopicOutcome, 4:TopicIncome, 5:AllTopics
        switch (type) {
            case DSimpleTypeEnum.Bank2Bank:
                $('label[for="FromTopic"]').text(Lang.From + " " + Lang.Bank);
                $('label[for="ToTopic"]').text(Lang.To2 + " " + Lang.Bank);
                $('#changeToTopic').css('visibility', 'visible');
                $('#TransferPriceContainer').show();
                $('#changeToTopicLbl').html(Lang.Person);
                fromType = 0;
                toType = 0;
                break;
            case DSimpleTypeEnum.Fund2Bank:
                $('label[for="FromTopic"]').text(Lang.From + " " + Lang.Fund);
                $('label[for="ToTopic"]').text(Lang.To2 + " " + Lang.Bank);
                fromType = 1;
                toType = 0;
                break;
            case DSimpleTypeEnum.Person2Bank:
                $('label[for="FromTopic"]').text(Lang.From + " " + Lang.Person);
                $('label[for="ToTopic"]').text(Lang.To2 + " " + Lang.Bank);
                $('#changeToTopic').css('visibility', 'visible');
                $('#changeToTopicLbl').html(Lang.Fund);
                fromType = 2;
                toType = 0;
                break;
            case DSimpleTypeEnum.Bank2Person:
                $('label[for="FromTopic"]').text(Lang.From + " " + Lang.Bank);
                $('label[for="ToTopic"]').text(Lang.To2 + " " + Lang.Person);
                $('#changeToTopic').css('visibility', 'visible');
                $('#changeToTopicLbl').html(Lang.Bank);
                fromType = 0;
                toType = 2;
                break;
            case DSimpleTypeEnum.Person2Fund:
                $('#TD').show();
                $('label[for="FromTopic"]').text(Lang.From + " " + Lang.Person);
                $('label[for="ToTopic"]').text(Lang.To2 + " " + Lang.Fund);
                $('#DiscountContainer').show();
                $('#Discount').val('');

                $('#changeToTopic').css('visibility', 'visible');
                $('#changeToTopicLbl').html(Lang.Bank);
                fromType = 2;
                toType = 1;
                $('.check').show();
                break;
            case DSimpleTypeEnum.Fund2Person:
                $('#TD').show();
                $('label[for="FromTopic"]').text(Lang.From + " " + Lang.Fund);
                $('label[for="ToTopic"]').text(Lang.To2 + " " + Lang.Person);
                $('#DiscountContainer').show();
                $('#Discount').val('');

                $('#changeFromTopic').css('visibility', 'visible');
                $('#changeFromTopicLbl').html(Lang.Bank);
                fromType = 1;
                toType = 2;
                $('.paycheck').show();
                break;
            case DSimpleTypeEnum.Fund2Topic:
                $('label[for="FromTopic"]').text(Lang.From + " " + Lang.Fund);
                $('label[for="ToTopic"]').text(Lang.To2 + " " + Lang.Topic);
                $('#changeFromTopic').css('visibility', 'visible');
                $('#changeFromTopicLbl').html(Lang.Bank);
                $('.personnelDiv').show();
                fromType = 1;
                toType = 3;
                break;
            case DSimpleTypeEnum.Topic2Fund:
                $('label[for="FromTopic"]').text(Lang.From + " " + Lang.Topic);
                $('label[for="ToTopic"]').text(Lang.To2 + " " + Lang.Fund);
                $('#changeToTopic').css('visibility', 'visible');
                $('#changeToTopicLbl').html(Lang.Bank);
                fromType = 4;
                toType = 1;
                break;
            case DSimpleTypeEnum.Topic2Bank:
                $('label[for="FromTopic"]').text(Lang.From + " " + Lang.Topic);
                $('label[for="ToTopic"]').text(Lang.To2 + " " + Lang.Bank);
                $('#changeToTopic').css('visibility', 'visible');
                $('#changeToTopicLbl').html(Lang.Fund);
                fromType = 4;
                toType = 0;
                break;
            case DSimpleTypeEnum.Bank2Person2:
                $('label[for="FromTopic"]').text(Lang.From + " " + Lang.Bank);
                $('label[for="ToTopic"]').text(Lang.To2 + " " + Lang.Person);
                $('#TransferPriceContainer').show();
                $('#changeFromTopic').css('visibility', 'visible');
                $('#changeFromTopicLbl').html(Lang.Fund);
                fromType = 0;
                toType = 2;
                break;
            case DSimpleTypeEnum.Bank2Topic:
                $('label[for="FromTopic"]').text(Lang.From + " " + Lang.Bank);
                $('label[for="ToTopic"]').text(Lang.To2 + " " + Lang.Topic);
                $('#TransferPriceContainer').show();
                $('#changeFromTopic').css('visibility', 'visible');
                $('#changeFromTopicLbl').html(Lang.Fund);
                $('.personnelDiv').show();
                fromType = 0;
                toType = 3;
                break;
            case DSimpleTypeEnum.Topic2Topic:
                $('label[for="FromTopic"]').text(Lang.From + " " + Lang.Topic);
                $('label[for="ToTopic"]').text(Lang.To2 + " " + Lang.Topic);
                $('#TransferPriceContainer').show();
                $('.personnelDiv,.projectDiv').show();
                fromType = 5;
                toType = 5;
                break;
        }

        if (fromType == 5) // all topics
            autoComplete('Trees', 'treesauto', '#FromTopic', '#FromTopicID'
               , function (event, data, formatted) {
                   // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due, TopicTitle, ProgramCode, Nature, EquipmentNeed, CanHaveSubTree
                   var hasChild = toBool(data[5]), canHaveSubTree = toBool(data[12]);
                   if (hasChild || canHaveSubTree) {
                       $('#FromTopic, #FromTopicID').val('');
                       if (hasChild)
                           alert(Lang.HasChild);
                       else
                           alert(Lang.HasSubTreeBut);
                   }
                   updateDoComments();
               }, formatTreePath);
        else if (fromType != 2) // all but person
            autoComplete('Trees', 'treestypedauto', '#FromTopic', '#FromTopicID'
               , function (event, data, formatted) {
                   // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due, Nature, TopicTitle, ProgramCode, EquipmentNeed, CanHaveSubTree
                   var hasChild = toBool(data[5]), canHaveSubTree = toBool(data[12]);
                   if (hasChild || canHaveSubTree) {
                       $('#FromTopic, #FromTopicID').val('');
                       if (hasChild)
                           alert(Lang.HasChild);
                       else
                           alert(Lang.HasSubTreeBut);
                   }
                   updateDoComments();
               }, formatTreePath, null, null, 'get.aspx?op=Auto&table=Trees&qd=treestypedauto&Type=' + fromType + appendTime());
        else
            autoComplete('Personnel', 'personnelselectauto', '#FromTopic', '#FromTopicID', updateDoComments
                , function (data, i, n, value) {
                    $('#subDetailForm #AssignorID').val('');
                    return formatPersonnelItem(data);
                });
        // autoComplete('Personnel', 'personnelselectauto', '#FromTopic', '#FromTopicID');
        if (toType == 5) // all topics
            autoComplete('Trees', 'treesauto', '#ToTopic', '#ToTopicID'
               , function (event, data, formatted) {
                   // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due, TopicTitle, ProgramCode, Nature, EquipmentNeed, CanHaveSubTree
                   var hasChild = toBool(data[5]), canHaveSubTree = toBool(data[12]);
                   if (hasChild || canHaveSubTree) {
                       $('#ToTopic, #ToTopicID').val('');
                       if (hasChild)
                           alert(Lang.HasChild);
                       else
                           alert(Lang.HasSubTreeBut);
                   }
                   updateDoComments();
               }, formatTreePath);
        else if (toType != 2) // all but person
            autoComplete('Trees', 'treestypedauto', '#ToTopic', '#ToTopicID'
               , function (event, data, formatted) {
                   // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due, Nature, TopicTitle, ProgramCode, EquipmentNeed, CanHaveSubTree
                   var hasChild = toBool(data[5]), canHaveSubTree = toBool(data[12]);
                   if (hasChild || canHaveSubTree) {
                       $('#ToTopic, #ToTopicID').val('');
                       if (hasChild)
                           alert(Lang.HasChild);
                       else
                           alert(Lang.HasSubTreeBut);
                   }
                   updateDoComments();
               }, formatTreePath, null, null, 'get.aspx?op=Auto&table=Trees&qd=treestypedauto&Type=' + toType + appendTime())
        else
            autoComplete('Personnel', 'personnelselectauto', '#ToTopic', '#ToTopicID', updateDoComments
            , function (data, i, n, value) {
                $('#subDetailForm #AssignorID').val('');
                return formatPersonnelItem(data);
            });
        //  autoComplete('Personnel', 'personnelselectauto', '#ToTopic', '#ToTopicID', updateDoComments);

        updateDoComments();
    }

    function topicChange() {
        switch (curType) {
            case DSimpleTypeEnum.Fund2Topic:
                setTypeChange(DSimpleTypeEnum.Bank2Topic);
                break;
            case DSimpleTypeEnum.Bank2Topic:
                setTypeChange(DSimpleTypeEnum.Fund2Topic);
                break;
            case DSimpleTypeEnum.Fund2Person:
                setTypeChange(DSimpleTypeEnum.Bank2Person2);
                break;
            case DSimpleTypeEnum.Bank2Person2:
                setTypeChange(DSimpleTypeEnum.Fund2Person);
                break;
            case DSimpleTypeEnum.Topic2Fund:
                setTypeChange(DSimpleTypeEnum.Topic2Bank);
                break;
            case DSimpleTypeEnum.Topic2Bank:
                setTypeChange(DSimpleTypeEnum.Topic2Fund);
                break;
            case DSimpleTypeEnum.Person2Bank:
                setTypeChange(DSimpleTypeEnum.Person2Fund);
                break;
            case DSimpleTypeEnum.Person2Fund:
                setTypeChange(DSimpleTypeEnum.Person2Bank);
                break;
            case DSimpleTypeEnum.Bank2Bank:
                setTypeChange(DSimpleTypeEnum.Bank2Person);
                break;
            case DSimpleTypeEnum.Bank2Person:
                setTypeChange(DSimpleTypeEnum.Bank2Bank);
                break;
        }
    }
    $('#changeFromTopic').click(function () {
        switch (curType) {
            case DSimpleTypeEnum.Fund2Topic:
                setTypeChange(DSimpleTypeEnum.Bank2Topic, 'f');
                break;
            case DSimpleTypeEnum.Bank2Topic:
                setTypeChange(DSimpleTypeEnum.Fund2Topic, 'f');
                break;
            case DSimpleTypeEnum.Fund2Person:
                setTypeChange(DSimpleTypeEnum.Bank2Person2, 'f');
                break;
            case DSimpleTypeEnum.Bank2Person2:
                setTypeChange(DSimpleTypeEnum.Fund2Person, 'f');
                break;
        }
    });
    $('#changeToTopic').click(function () {
        switch (curType) {
            case DSimpleTypeEnum.Topic2Fund:
                setTypeChange(DSimpleTypeEnum.Topic2Bank, 't');
                break;
            case DSimpleTypeEnum.Topic2Bank:
                setTypeChange(DSimpleTypeEnum.Topic2Fund, 't');
                break;
            case DSimpleTypeEnum.Person2Bank:
                setTypeChange(DSimpleTypeEnum.Person2Fund, 't');
                break;
            case DSimpleTypeEnum.Person2Fund:
                setTypeChange(DSimpleTypeEnum.Person2Bank, 't');
                break;
            case DSimpleTypeEnum.Bank2Bank:
                setTypeChange(DSimpleTypeEnum.Bank2Person, 't');
                break;
            case DSimpleTypeEnum.Bank2Person:
                setTypeChange(DSimpleTypeEnum.Bank2Bank, 't');
                break;
        }
    });

    // ------------------------------
    // startup code
    // ------------------------------
    $('#TD').hide();
    setValue('#TypeSel');
    ID = parseInt($('#SID').val(), 10);
    _Context.priceDate = $('#Date').val();
    $('#TypeSel').change(typeChange);
    typeChange();
    topicChange();
    $('#Price,#Discount').blur(updateTotal).keyup(updateTotal);

    // Topic2Topic
    autoComplete('Personnel', 'personnelselectauto', '#PersonnelName', '#PersonnelID', updateDoComments
                , function (data, i, n, value) {
                    $('#PersonnelID').val('');
                    return formatPersonnelItem(data);
                });
    $('#PersonnelName').blur(updateDoComments);
    autoComplete('Ward', 'wardchildsauto', '#ProjectName', '#ProjectID');
    // --------------------------
    // document No
    // --------------------------
    $('#LastDocNo').click(function() {
        var LastDocNo = getServerValue('thelastdocno', 'DocumentAccounting');
        $('#No').val(LastDocNo);
    });
    $('#ShowDoc').click(function () {
        if (!$('#No').val()) {
            alert(Lang.EmptyDocNo);
            return;
        }
        var idd = getServerValue('thelastdocID', 'DocumentAccounting', '&No=' + $('#No').val());
        window.open('list.aspx?table=DocumentAccounting&id=' + idd, '_blank');
    });
    // --------------------------
    // PayCheck
    // --------------------------
    autoCompleteFree('PayCheck', 'paychecknewauto', '#PayCheckName', '#PayCheckID'
        , function (e, data) {
            $('#PayCheckAmount').val(toMonetary(noDecimalPlace(data[2])));
            updateTotal();
        }
        , formatCheck);
    $('#newPayCheck').click(function () {
        var oldID = _Context.id;
        function showcallback(v, m, f) {
            if (v == 'OK') {
                generalOKScalar(v, m, f, 'paycheckupdate', 'paycheckinsert', 'PayCheck', '#subDetailForm');
                if (!_Context.success)
                    return false;
                $('#PayCheckName').val(f.Serial);
                $('#PayCheckAmount').val(f.Amount);
                $('#PayCheckID').val(_Context.resultID);
                updateTotal();
            }
            _Context.id = oldID;
        }
        _Context.id = -1;
        var _url = 'get.aspx?op=GetHtml&table=PayCheck&qd=paycheck&view=checksshop&FromStoreDraft=1&FromPurchase=1&id=-1&ReceiverID=' + $('#ToTopicID').val() + "&ReceiverName=" + encodeURIComponent($('#ToTopic').val()) + appendTime();
        showPopup(_url, showcallback, getButtons());
    });
    $('#editPayCheck').click(function () {
        if (hasValuePositive($('#PayCheckID').val())) {
            var oldID = _Context.id;
            function showcallback(v, m, f) {
                if (v == 'OK') {
                    generalOKScalar(v, m, f, 'paycheckupdate', 'paycheckinsert', 'PayCheck', '#subDetailForm');
                    if (!_Context.success)
                        return false;
                    $('#PayCheckName').val(f.Serial);
                    $('#PayCheckAmount').val(f.Amount);
                    updateTotal();
                }
                _Context.id = oldID;
            }
            _Context.id = $('#PayCheckID').val();
            var _url = 'get.aspx?op=GetHtml&table=PayCheck&qd=paycheck&id=' + $('#PayCheckID').val() + appendTime();
            showPopup(_url, showcallback, getButtons());
        }
    });
    // --------------------------
    // Check
    // --------------------------
    autoCompleteFree('Checks', 'checksnewauto', '#CheckName', '#CheckID'
           , function (e, data) {
               $('#CheckAmount').val(toMonetary(noDecimalPlace(data[2])));
               updateTotal();
           }
           , function (data, i, n, value) {
               // Checks.ID, No AS Title, Amount, DueDate
               return '<div dir=rtl>'
                   + Lang.No + " : " + data[1] + '<br />'
                   + Lang.Amount + " : " + numberWithCommas(toMonetary(noDecimalPlace(data[2]))) + '<br />'
                   + Lang.DueDate + " : " + data[3] + '<br />'
                   + '</div>';
           });
    $('#newCheck').click(function () {
        var oldID = _Context.id;
        function showcallback(v, m, f) {
            if (v == 'OK') {
                generalOKScalar(v, m, f, 'checkupdate', 'checkinsert', 'Checks', '#subDetailForm');
                if (!_Context.success)
                    return false;
                $('#CheckName').val(f.Serial);
                $('#CheckAmount').val(f.Amount);
                $('#CheckID').val(_Context.resultID);
                updateTotal();
            }
            _Context.id = oldID;
        }
        _Context.id = -1;
        var _url = 'get.aspx?op=GetHtml&table=Checks&qd=check&view=checksshop&id=-1&FromStoreDraft=1&FromPurchase=1&PersonnelID=' + $('#FromTopicID').val() + "&PersonnelName=" + encodeURIComponent($('#FromTopic').val()) + appendTime();
        showPopup(_url, showcallback, getButtons());
    });
    $('#editCheck').click(function () {
        if (hasValuePositive($('#CheckID').val())) {
            var oldID = _Context.id;
            function showcallback(v, m, f) {
                if (v == 'OK') {
                    generalOKScalar(v, m, f, 'checkupdate', 'checkinsert', 'Checks', '#subDetailForm');
                    if (!_Context.success)
                        return false;
                    $('#CheckName').val(f.No);
                    $('#CheckAmount').val(f.Amount);
                    updateTotal();
                }
                _Context.id = oldID;
            }
            _Context.id = $('#CheckID').val();
            var _url = 'get.aspx?op=GetHtml&table=Checks&qd=check&id=' + $('#CheckID').val() + appendTime();
            showPopup(_url, showcallback, getButtons());
        }
    });
    $('#Date').val(_Context.today);

    $('#DeclareTopic').click(function () {
        function showcallback(v, m, f) {
            if (v == 'OK') {
                if (f.ParentTreePath != undefined &&
                    f.ParentTreePath != '' &&
                    f.TopicName != undefined &&
                    f.TopicName != '')
                    var result = getServerValue('declaretopic',
                        'Topic',
                        '&TreePath=' + f.ParentTreePath + '&TopicName=' + f.TopicName.trim());
                else if (f.ParentTreePath == undefined || f.ParentTreePath == '') {
                    Growl.Error(Lang.DeclareTopicMsg1, 3000);
                    return false;
                }
                else if (f.TopicName == undefined || f.TopicName == '') {
                    Growl.Error(Lang.DeclareTopicMsg2, 3000);
                    return false;
                }
                if (result > 0) {
                    Growl.Notice(Lang.Success, 3000);
                } else if (result == -1) {
                    Growl.Error(Lang.DeclareTopicMsg, 3000);
                    return false;
                } else {
                    Growl.Error(Lang.Error, 3000);
                    return false;
                }
            }
            return true;
        }
        //_Context.id = -1;
        var _url = 'get.aspx?op=GetHtml&table=Topic&qd=topic&view=declaretopic&id=-1' + appendTime();
        showPopup(_url, showcallback, getButtons());
    });

    dateSelect('.dateSelect');
    insertValidation();
    monetaryKeys();
    monetaryInputsSet(null, '#Date');
    commaInputs();

    function manageUiBasedOnType(type) {
        var $fromTopicContainer = $('#from-topic-container'),
            $fromTopicAddon = $('#from-topic-addon'),
            $toTopicContainer = $('#to-topic-container'),
            $toTopicAddon = $('#to-topic-addon'),
            $typeContainer = $('#type-container'),
            $typeAddon = $('#type-addon');

        $fromTopicContainer.removeClass('input-group input-group-sm');
        $fromTopicAddon.addClass('hidden');

        $toTopicContainer.removeClass('input-group input-group-sm');
        $toTopicAddon.addClass('hidden');

        $typeContainer.removeClass('input-group input-group-sm');
        $typeAddon.addClass('hidden');

        if (type != DSimpleTypeEnum.Fund2Bank && type != DSimpleTypeEnum.Topic2Bank && type != DSimpleTypeEnum.Topic2Fund) {
            $typeContainer.addClass('input-group input-group-sm');
            $typeAddon.removeClass('hidden');
        }

        if (type === DSimpleTypeEnum.Fund2Topic || type === DSimpleTypeEnum.Bank2Topic
            || type === DSimpleTypeEnum.Fund2Person || type === DSimpleTypeEnum.Bank2Person2) {
            $fromTopicContainer.addClass('input-group input-group-sm');
            $fromTopicAddon.removeClass('hidden');
        } else if (type === DSimpleTypeEnum.Topic2Fund || type === DSimpleTypeEnum.Topic2Bank
            || type === DSimpleTypeEnum.Person2Bank || type === DSimpleTypeEnum.Person2Fund
            || type === DSimpleTypeEnum.Bank2Bank || type === DSimpleTypeEnum.Bank2Person) {
            $toTopicContainer.addClass('input-group input-group-sm');
            $toTopicAddon.removeClass('hidden');
        }
    }

    $('#NewPerson').click(function () {
        var oldID = _Context.id;
        function showcallback(v, m, f) {
            if (v == 'OK') {
                generalOKScalar(v, m, f, 'personupdate', 'personinsert', 'Personnel', '#subDetailForm');
                if (!_Context.success)
                    return false;
            }
            _Context.id = oldID;
        }
        _Context.id = -1;
        var _url = 'get.aspx?op=GetHtml&table=Personnel&qd=person&id=-1&view=person' + appendTime();
        showPopup(_url, showcallback, getButtons());
    });
}