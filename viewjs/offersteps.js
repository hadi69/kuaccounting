﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
92-10-19: file created
*/
function view_offersteps() {
    makeChosen();
    $("#tableOfferRewards").smartTable();
    var cnt = 0;
    var mID = -1;
    // ---------------------
    // Add Rows
    // ---------------------
    function addOfferRewards(node) {
        var row = '<tr>';
        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">'
            + (cnt + 1)
            + '</td>';

        row += '<td data-th="' + Lang.CommodityName + '">'
            + '<input type="hidden" id="id_' + cnt + '" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="hidden"  name="OfferStepID" value="' + getNodeVal(node, "OfferStepID", -1) + '" />'
            + '<input type="text" name="ServiceName" id="sn_' + cnt + '" value="' + getNodeVal(node, "ServiceName") + '" class="servicename form-control input-sm" />'
            + '<input type="hidden" id="sid_' + cnt + '"  name="ServicePriceID"  value="' + getNodeVal(node, "ServicePriceID") + '" class=""/>'
            + '</td>';

        row += '<td data-th="' + Lang.Num + '">'
            + '<input type="text" name="Num" id="nu_' + cnt + '" value="' + getNodeVal(node, "Num") + '" class="align-center form-control input-sm" />'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a class="btn btn-danger btn-xs btn-block btn-remove removeofferrewards" id="r_' + cnt + '">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }
    //-------------------
    // remove rows
    //----------------
    function removeOfferRewards($who) {

        var id = $who.attr('id'); // pn_1
        id = id.substring(2); // pn_1  -->  1
        if ($('#id_' + id).val() != -1) {
            runQuery('offerrewardsdelete', 'OfferRewards', '&ID=' + $('#id_' + id).val(), null);            
        }
       // $('#sid_' + id).val('');
       // $('#nu_' + id).val('');
        $who.parent().parent().hide();
        Growl.Notice(Lang.DeletedSuccessfully);
    }
    //--------------
    function autoPostPack(event, data, formatted) {
        $(event.target).next().val(data[0]);
    }
    //-------------------

      function bindAdds() {
        $('.addofferrewards').click(function () {
            var table = $("#tableOfferRewards");
            $('tr:last', table).after(addOfferRewards(null));
            Growl.Notice(Lang.RowAddedSuccessfully);
            var lastRow = $('tr:last', table);
           // dateSelect('#tableOfferRewards tr:last .dateSelect');
            $('.removeofferrewards', lastRow).click(function () {
                removeOfferRewards($(this));
            });
            autoComplete('ServicePriceView', 'offerserviceauto', "#tableOfferRewards .servicename", "#justfurfun"
               , autoPostPack
               , function (data, i, n, value) {
                   return data[1];
               });
        });
      }

    //------------------

    function bindRemoves() {
        $('.removeofferrewards').click(function () {
            removeOfferRewards($(this));
        });
    }
    //---------------------------
    function SaleTypeChange(onChanging)
    {
        var saleType = $('#SaleType').val();

        if (onChanging) {
            $('#MinValue').attr('value', 0);
            $('#MaxValue').attr('value', 0);
        }

        if (saleType == 1) {
            $('#MinMaxDiv').show();
            $('#MinVL').html(Lang.TheMinAmount);
            $('#MaxVL').html(Lang.TheMaxAmount);
            $('#MinValue').addClass('monetary validate[custom[integer]]');
            $('#MaxValue').addClass('monetary validate[custom[integer]]');
            commaInputs('#subDetailForm');
        }
        else if (saleType == 0) {
            $('#MinMaxDiv').show();
            $('#MinVL').html(Lang.TheMinNum);
            $('#MaxVL').html(Lang.TheMaxNum);
            $('#MinValue').removeClass('monetary validate[custom[integer]]');
            $('#MaxValue').removeClass('monetary validate[custom[integer]]');
        }
        else if (saleType == 2 || saleType == 3 || saleType == 4 || saleType == 5) {
            $('#MinMaxDiv').hide();
            $('#RewardType').attr('v', 1);
            setValue('#RewardType');
            $('#RewardType').change();
        }
    }
    $('#RewardType').change(function () {

        if ($(this).val() == 0) {
            $('#AtL').html(Lang.NumberOfGoods);
            $('#Amount').removeClass('monetary validate[custom[integer]]');
            $('#Amount').attr('value',0);
            $('.OfferRewardsDiv').show();
        }
        else if ($(this).val() == 1 || $(this).val() == 3) {
            $('#AtL').html(Lang.Discount);
            $('#Amount').addClass('percent monetary validate[custom[integer]]');
            $('#Amount').attr('value',0);
            $('.OfferRewardsDiv').hide();
            commaInputs('#subDetailForm');
        }
        else {
            $('#AtL').html(Lang.NumOfDelayDay);
            $('#Amount').attr('value',0);
            $('#Amount').removeClass('monetary validate[custom[integer]]');
            $('.OfferRewardsDiv').hide();
        }

    });
    // -----------------------------
    // startup code
    // ------------------------------

    mID = toInt($('#MasterID').val(), -1);
    setValue('#SaleType');
    setValue('#RewardType');
    setValue('#ServiceRewardType');
    //$('#Head').html);
    fillTable("get.aspx?op=GetXml&qd=offerrewards&OfferStepID=" + mID, '#tableOfferRewards', addOfferRewards);


    function doMore() {
        function autoPostPack(event, data, formatted) {
            $(event.target).next().val(data[0]);
        }
        $('.removeofferrewards').click(function () {
            removeOfferRewards($(this));
        });
        autoComplete('ServicePriceView', 'offerserviceauto', "#tableOfferRewards .servicename", "#justfurfun"
                      , autoPostPack
                      , function (data, i, n, value) {
                          return data[1];
                      });
        SaleTypeChange(false);
        $('#SaleType').change(function() {
            SaleTypeChange(true);
        });
        if ($('#RewardType').val() == 0) {
            $('#AtL').html(Lang.NumberOfGoods);
            $('#Amount').removeClass('monetary validate[custom[integer]]');
            $('.OfferRewardsDiv').show();
        }
        else if ($('#RewardType').val() == 1 || $('#RewardType').val() == 3) {
            $('#AtL').html(Lang.Discount);
            $('#Amount').addClass('percent monetary validate[custom[integer]]');
            $('.OfferRewardsDiv').hide();
            commaInputs('#subDetailForm');
        }
        else {
            $('#AtL').html(Lang.NumOfDelayDay);
            $('#Amount').removeClass('monetary validate[custom[integer]]');
            $('.OfferRewardsDiv').hide();
        }

        bindAdds();
        bindRemoves();
        insertValidation('#subDetailForm');
        commaInputs('#subDetailForm');
        dateSelect('.dateSelect');
        commaInputs('#subDetailForm');
        monetaryKeys();
        percentInputsSet('#subDetailForm');

    }
    doMore();
}