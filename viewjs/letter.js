﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    94-06-27: file created
*/
function view_letter() {
    var mID = toInt($('#ID').val(), -1);
    function randomFolder() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 10; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }
    if (mID == -1) {
        $('#SenderID').val(_Context.userID);
        $('#SenderName').val(_Context.userFullName);
        $('#Folder').val(randomFolder());

        var replyID = $('#ReplyToID').val();
        if (hasValuePositive(replyID)) {
            // it is a barcode
            var _autourl = 'get.aspx?op=Auto&table=Letter&qd=lettersender' + appendTime();
            var row = getServerValues(_autourl + "&q=" + replyID);
            if (row && row.length && row.length > 1) {
                $('#ReceiverNames').val(row[1]); 
                $('#Subject').val(Lang.ReplyTo + " : " + row[2]);
            }
        }
    }
    else {
        var forwardID = $('#ForwardID').val();
        if (hasValuePositive(forwardID)) {
            $('#ID').val('');
            $('#ReceiverNames').val('');
            $('#Subject').val(Lang.Forwarded + " : " + $('#Subject').val());
            $('#Message').val(Lang.Forwarded + ':\r\n' + $('#SenderName').val() + ':\r\n' + $('#Message').val());
            $('#SenderID').val(_Context.userID);
            $('#SenderName').val(_Context.userFullName);
        }
        else
            runQuery('letterread', 'Letter', 'LetterID=' + mID, false);
    }


    autoCompleteNew({ table: 'Users', qd: 'userauto', textInput: '#ReceiverNames', hiddenInput: '#dummy', multiple: true });
    $('#Subject').focus();
    $('#ReceiverNames').focus();
    if (mID == -1) {
        var folder = 'letter/' + $('#Folder').val();
        uploadifyMulti('#ChooseFile', '#FileNames', '#StartUpload', folder
                       , function (fileName) {
                           $('.attachments').text(fileName + ', ' + $('.attachments').text());
                           //var link = fileName.toLowerCase();
                           //if (link.indexOf('.png') > 0 || link.indexOf('.gif') > 0 || link.indexOf('.jpg') > 0 || link.indexOf('.jpeg') > 0)
                           //    $('#preview').attr('src', "files/dadocuments/" + link);
                       });
    }
    else {
        $('.upload').hide();
        // show files
        var fileNames = $('#FileNames').val().split(',');
        for (var i = 0; i < fileNames.length; i++) {
            if (fileNames[i] == '')
                continue;
            var link = fileNames[i].toLowerCase();
            var img = '<div class="col-xs-3 form-group-container">';
            img += '<a href="files/letter/' + $('#Folder').val() + '/' + fileNames[i] + '" target="_blank">' + fileNames[i] + '</a><br />';
            if (link.indexOf('.png') > 0 || link.indexOf('.gif') > 0 || link.indexOf('.jpg') > 0 || link.indexOf('.jpeg') > 0) {
                img += '<img style="max-width:100%" id="preview" src="files/letter/' + $('#Folder').val() + '/' + fileNames[i] + '" />';
            }
            else if (link.indexOf('.doc') > 0 || link.indexOf('.docx') > 0)
                img += '<i class="fa fa-file-word-o fa-5x"></i>';
            else if (link.indexOf('.pdf') > 0)
                img += '<i class="fa fa-file-pdf-o fa-5x"></i>';
            else if (link.indexOf('.txt') > 0)
                img += '<i class="fa fa-file-text-o fa-5x"></i>';
            else if (link.indexOf('.zip') > 0 || link.indexOf('.rar') > 0)
                img += '<i class="fa fa-file-zip-o fa-5x"></i>';
            else
                img += '<i class="fa fa-file-o fa-5x"></i>';
            img += '</div>';
            $('.preview').append(img);
            
        }
    }
    setChecked("#Archive");
    dateSelect('.dateSelect');
    insertValidation();
    makeChosen();
}