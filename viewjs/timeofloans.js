﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-10-19: file created
*/
function view_timeofloans() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var d = new Date();
        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        var date = month + '/' + day + '/' + year;
       // alert(date);
        var _url = 'service.asmx/GetQueryList?table=' + _Context.table + '&qd=listofloans&Date='+ date;
        //alert(_url);
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                   // { display: Lang.ColRowNr, name: '', width: 20, sortable: false, align: 'left' },
                    { display: Lang.headline, name: 'Title', width: 100, sortable: true, align: 'left' },
                    { display: Lang.Amounts, name: 'Amounts', width: 90, sortable: false, align: 'left', process: showMonetary },
                    { display: Lang.InstallmentAmount, name: 'InstallmentAmount', width: 90, sortable: false, align: 'left', process: showMonetary },
                    { display: Lang.PaidAmount, name: 'PaidAmount', width: 90, sortable: false, align: 'left', process: showMonetary },
                    { display: Lang.ColEdit, name: 'Edit', width: 40, sortable: false, align: 'left', process: function (celDiv, id) { editLink(celDiv, id, show) } },
                ],
                buttons:
                [
                   { name: Lang.GBNew, bclass: 'new', onpress: doCommand, },
                   { name: Lang.GBDel, bclass: 'delete', onpress: doCommand, },
                   { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                searchitems: null,
                sortname: "Title",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,

                height: gridHeight,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                }
            }
            );
    }

    function showcallback(v, m, f) {
        $('.PayDate').attr('name', 'PayDate');
        $('.DueDate').attr('name', 'DueDate');
        var qry = (_Context.id == -1) ? 'loaninsert' : 'loanupdate';

        return newMastercallback(v, m, f, 'Loan', qry, 'Installment', 'installmentupdate', 'installmentinsert', 'installmentdelete', 'Amount', '#subDetailForm', show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    $('#ptitle').html("<a href='menu/accounting.aspx'>" + Lang.MAccounting + "</a> :: " + _Context.title);
    run();
}