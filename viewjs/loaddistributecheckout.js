﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
93-04-08: file created
*/
function view_loaddistributecheckout() {

    function doCommand(com, grid) {
        generalCommand(com, grid, function () { });
    }
    var sum = 0;
    var firstFlexi = true;
    function gridLoad() {
        var _url = 'service.asmx/GetQueryList?table=VisitorPlanDayService&qd=getservicenumforhotsalecheckout&VisitorID=' + $('#VisitorID').val() + '&FromDate=' + $('#FromDate').val() + '&ToDate=' + $('#ToDate').val() + appendTime();
        if (firstFlexi) {
            firstFlexi = false;
            $("#flexiSales").flexigrid
            (
                {
                    url: _url,
                    dataType: 'xml',
                    colModel:
                    [
                        { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                        { display: '', name: 'ID', sortable: true, hide: true },
                        { display: Lang.CommodityName, name: 'CommodityName', sortable: true, css_class: 'width-300' },
                        { display: Lang.Num, name: 'Num', sortable: true }
                    ],
                    searchitems: null,
                    sortname: "CommodityName",
                    sortorder: "asc",
                    usepager: true,
                    //title: _Context.title,
                    useRp: true,
                    rp: gridRp,
                    showTableToggleBtn: _options.gridToggle,
                    width: _options.gridWidth,
                    height: 450,
                    singleSelect: false,
                }
            );
        } else {
            $('#flexiSales').flexOptions({ url: _url });
            $('#flexiSales').flexReload();
            // $('#TotalPrice').val(sum);
        }
    }

    // ------------------------------
    // startup code
    // ------------------------------
    gridLoad();
    $('#Search').click(function () {
        sum = 0;
        gridLoad();
    });

    autoCompleteFree('Personnel', 'visitorauto', '#VisitorName', '#VisitorID', null
        , function (data, i, n, value) {
            //$('#BuyerID').val('');
            return formatPersonnelItem(data);
        });

    $('#DoLoadConfirm').click(function () {
        var more = '&VisitorID=' + $('#VisitorID').val() + '&FromDate=' + $('#FromDate').val() + '&ToDate=' + $('#ToDate').val();
        var ok = getServerValue('visitorplandayscheckedout', 'VisitorPlanDays', more);
        if (ok != -1 && ok != null) {
            Growl.Notice(Lang.Success);
        } else {
            Growl.Notice(Lang.Failed);
        }
    });

    dateSelect('.dateSelect');
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.LoadDistributeCheckOut, parentTitle: Lang.MDistribution, parentUrl: 'menu/distribution.aspx' } }));
    checkAccess(['distribution,loaddistributecheckout']);
    setClassDistribution();
}