﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
92-10-16: 
- file created
- Date: dateSelect
*/
function view_documentaccounting() {
    fixFormButtonContainer();
    var documentNo = $('#No').val();
    replacePrintButton({
        primary: {
            text: Lang.Print,
            href:Router.Generate("print-documentaccounting", { params: { from: documentNo, to: documentNo }, query: { report: 'secondsimple', print: "true" } }) 
                
        },
        other: [
            {
                text: Lang.SimpleFormat,
                href: Router.Generate("print-documentaccounting", { params: { from: documentNo, to: documentNo }, query: { print: "true" } })
            }
            //{
            //    text: Lang.SimpleFormat,
            //    href: Router.Generate("print-documentaccounting", { params: { from: documentNo, to: documentNo }, query: { report: 'simple', print: "true" } })
            //}

        ]
    });

    shortcut.remove('alt+d'); // If we don't remove the shortcuts, and we close the form and re-open it, it we will have two shortcuts!
    shortcut.add('alt+d', function() {
        var rows = $('#items').find('tr.selected');
        if (rows.length > 0) {
            rows.find('a.delitem').click();
        }
    });
    var ID;
    var numItems = 0;
    var cmntsLen = 70;
    $("#items").smartTable();
    var insertRow, editRow, curRow, curID;
    var insertArr = ['RowID','PersonnelName', 'PersonnelID', 'WardName', 'WardID', 'Owe', 'Due', 'TTitle', 'TCode', 'TreePath', 'DocNo', 'DocDate', 'Comments'
        , 'PeNeed', 'PrNeed', 'EqNeed', 'PayCheckName', 'PayCheckID', 'CheckName', 'CheckID', 'PersonnelTreePathName', 'SecPersonnelID', 'SecTreePath', 'EquipmentName', 'EquipmentID', 'CostCenterName', 'CostCenterID', 'CcNeed', 'MainParentName'];
    var rowArr = ['tr', 'pen', 'peid', 'prn', 'prid', 'ow', 'du', 'trn','trc', 'trid', 'dn', 'dd', 'co'
        , 'peneed', 'prneed', 'eqneed', 'pcheckn', 'pcheckid', 'checkn', 'checkid', 'secpen', 'secpeid', 'sectrid', 'eqn', 'eqid', 'ccn', 'ccid', 'ccneed', 'mpname'];
    var hasOweDue = true;
    function text2Val(id, dest, isRepeat) {
        for (var i = 0; i < dest.length; i++) {
            //if (dest[i] == 'EOwe' || dest[i] == 'EDue' || dest[i] == 'Owe' || dest[i] == 'Due')
            //    $('#' + dest[i]).val($('#' + rowArr[i] + id).text().replace(/,/g, ""));
                //else
            if (dest[i] == 'RowID' && !isRepeat) {
                $('#RowID').val('tr' + id);
            }
            else if (dest[i] == 'RowID') {
                //nothing
            }
            else if (dest[i] == 'TCode') {
                setTimeout(function() {
                    $('#TCode').val(($('#trc' + id).text() + '(' + $('#mpname' + id).text() + ')'));
                }, 200);
            }
            else if (dest[i] == 'TTitle') {
                $('#TTitle').val($('#trl' + id).text());
                $('#ProgramCode').val($('#trp' + id).text());
            } else {
                $('#' + dest[i]).val($('#' + rowArr[i] + id).text());
            }
            //alert($('#' + rowArr[i] + id).text() + " --> " + dest[i]);
        }
    }
    function val2Text(src, id) {
        for (var i = 0; i < src.length; i++) {
            if (src[i] == 'EOwe' || src[i] == 'EDue' || src[i] == 'Owe' || src[i] == 'Due')
                $('#' + rowArr[i] + id).text(numberWithCommas($('#' + src[i]).val()));
            else if (src[i] == 'RowID') {
                //nothing
            } else
                $('#' + rowArr[i] + id).text($('#' + src[i]).val());

            if (src[i] == 'EComments' || src[i] == 'Comments') {
                var cmnts = $('#' + src[i]).val();
                if (cmnts.length > cmntsLen)
                    $('#' + rowArr[i] + id).next().html(cmnts.substr(0, cmntsLen) + '...').attr('title', cmnts.replace('"', "'"));
                else
                    $('#' + rowArr[i] + id).next().html(cmnts).attr('title', cmnts.replace('"', "'"));
            }
            else if (src[i] == 'TCode') {
                var code = $('#TCode').val().toString();
                $('#trc' + id).text(code.substr(0, code.indexOf('(')));
                $('#mpname' + id).text(code.substr(code.indexOf('(') + 1, (code.length - (code.indexOf('(') + 2))));
            }
            else if (src[i] == 'TTitle') {
                $('#trl' + id).text($('#TTitle').val());
                $('#trl' + id).attr('title', $('#TTitle').attr('title'));
                $('#trp' + id).text($('#ProgramCode').val());                
            }
        }
    }
    function makeClear(src) {
        for (var i = 0; i < src.length; i++)
            $('#' + src[i]).val('');
    }
    function dummyNode(id) {
        this.attr = function (key) {
            if (key == 'ID')
                return id;
            return '';
        };
    }
    function addItem(node) {
        // node must not be null
        var id = node.attr("ID");
        var row = '<tr id="tr' + id + '" class="text-center">';
        // ID
        row += '<td class="align-center row-number" data-th="' + Lang.ColRowNr + '"><span class="num2">' + (numItems + 1)
            + '</span>';
        row += '<span class="hidden" id="peneed' + id + '">' + node.attr("PersonNeed") + '</span>' 
            + '<span class="hidden" id="prneed' + id + '">' + node.attr("ProjectNeed") + '</span>'
            + '<span class="hidden" id="eqneed' + id + '">' + node.attr("EquipmentNeed") + '</span>'
            + '<span class="hidden" id="ccneed' + id + '">' + node.attr("CostCenterNeed") + '</span>';
        row += '<span class="hidden" id="secpen' + id + '">' + node.attr("PersonnelTreePathName") + '</span><span class="hidden" id="secpeid'
            + id + '">' + node.attr("SecPersonnelID") + '</span>';
        row += '<span class="hidden" id="sectrid' + id + '">' + node.attr("SecTreePath") + '</span>';
        row += '</td>';
        // TopicCode
        row += '<td data-th="' + Lang.Code + '" class="compact"><span id="trc' + id + '">' + node.attr("TopicCode") + '</span>' +
            '<span class="hidden" id="mpname' + id + '">' + node.attr("MainParentName") + '</span></td>';

        // TopicTitle
        row += '<td data-th="' + Lang.Topic + '" class="compact"><a id="trl' + id + '" title="' + node.attr("TreePathName") + '">' + node.attr("TopicTitle")
            + '</a><span class="hidden" id="trn' + id + '">' + node.attr("TreePathName") + '</span><span class="hidden" id="trid'
            + id + '">' + node.attr("TreePath") + '</span><span class="hidden" id="trp' + id + '">' + node.attr("ProgramCode") + '</span></td>';

        // Comments
        var cmnts = node.attr("Comments") + "";
        if (cmnts.length > cmntsLen)
            row += '<td data-th="' + Lang.Comments + '" class="compact"><span id="co' + id + '" style="display:none">' + cmnts + '</span><a class="cmnts" title="'
                + cmnts.replace('"', "'") + '">' + cmnts.substr(0, cmntsLen) + '...</a></td>';
        else
            row += '<td data-th="' + Lang.Comments + '" class="compact"><span id="co' + id + '" style="display:none">' + cmnts + '</span><a class="cmnts">' + cmnts + '</a></td>';

        // Owe
        row += '<td data-th="' + Lang.Debtor + '" class="compact"><span id="ow' + id + '" class="owe">' + numberWithCommas(toMonetary(node.attr("Owe"))) + '</span></td>';

        // Due
        row += '<td data-th="' + Lang.Creditor + '" class="compact"><span id="du' + id + '" class="due">' + numberWithCommas(toMonetary(node.attr("Due"))) + '</span></td>';

        // DocNo
        row += '<td data-th="' + Lang.DocNo + '" class="compact"><span id="dn' + id + '">' + node.attr("DocNo") + '</span></td>';

        // DocDate
        row += '<td data-th="' + Lang.DocDate + '" class="compact"><span id="dd' + id + '">' + node.attr("DocDate") + '</span></td>';

        // Person
        row += '<td data-th="' + Lang.Person + '" class="compact"><span id="pen' + id + '">' + node.attr("PersonnelName") + '</span><span class="hidden" id="peid'
            + id + '">' + node.attr("PersonnelID") + '</span></td>';


        // Project
        row += '<td data-th="' + Lang.Project + '" class="compact"><span id="prn' + id + '">' + node.attr("ProjectFullName") + '</span><span class="hidden" id="prid'
            + id + '">' + node.attr("ProjectID") + '</span></td>';

        // CostCenter
        row += '<td data-th="' + Lang.CostCenter + '" class="compact"><span id="ccn' + id + '">' + node.attr("CostCenterFullName") + '</span><span class="hidden" id="ccid'
            + id + '">' + node.attr("CostCenterID") + '</span></td>';

        // Equipment
        row += '<td data-th="' + Lang.Equipment + '" class="compact"><span id="eqn' + id + '">' + node.attr("EquipmentFullName") + '</span><span class="hidden" id="eqid'
            + id + '">' + node.attr("EquipmentID") + '</span></td>';

        // Buttons
        row += '<td class="sel1 align-center compact" data-th="' + Lang.ToBeSelected + '"><input type="checkbox" id="sel' + id + '" class="sel" /></td>';

        row += '<td class="align-center action-remove del1">'
            + '<a class="btn btn-danger btn-xs btn-block btn-remove delitem" id="dl' + id + '">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        numItems++;
        return row;
    }
    function addSumItem(sumOwe, sumDue, topicCode, topicTitle) {
        var row = '<tr class="text-center sumRow">';
        row += '<td class="align-center row-number" data-th="' + Lang.ColRowNr + '"><span class="num2">#</span></td>';
        // TopicCode
        row += '<td data-th="' + Lang.Code + '" class="compact"><span>' + topicCode + '</span></td>';

        // TopicTitle
        row += '<td data-th="' + Lang.Topic + '" class="compact"><span>' + topicTitle + '</span></td>';

        // Comments
        row += '<td data-th="' + Lang.Comments + '" class="compact"><span>&nbsp;</span></td>';

        // Owe
        row += '<td data-th="' + Lang.Debtor + '" class="compact"><span class="sowe">' + numberWithCommas(toMonetary(sumOwe)) + '</span></td>';

        // Due
        row += '<td data-th="' + Lang.Creditor + '" class="compact"><span class="sdue">' + numberWithCommas(toMonetary(sumDue)) + '</span></td>';

        // DocNo
        row += '<td data-th="' + Lang.DocNo + '" class="compact"><span>&nbsp;</span></td>';

        // DocDate
        row += '<td data-th="' + Lang.DocDate + '" class="compact"><span>&nbsp;</span></td>';

        // Person
        row += '<td data-th="' + Lang.Person + '" class="compact"><span>&nbsp;</span></td>';

        // Project
        row += '<td data-th="' + Lang.Project + '" class="compact"><span>&nbsp;</span></td>';


        // Buttons
        row += '<td class="align-center compact" data-th="' + Lang.ToBeSelected + '">&nbsp;</td>';
        row += '<td class="align-center action-remove">&nbsp;</td>';

        row += '</tr>';
        return row;
    }
    function bindItems() {
        // remove all rows except first
        $("#items").find("tr:gt(0)").remove();
        var items = getItems('documentaccountingitem', "DocumentAccountingID=" + ID);
        if (items == null)
            return;
        $("#items").addClass("colored");
        $("#items").addClass("table-with-border");
        var all = "", cnt = 0;
        $(items).find("R").each(function () {
            var row = addItem($(this));
            all += row;
            cnt ++;
        });
        $('#items tr:last').after(all);
        if(cnt < 200)
        $("#items tbody").sortable({
            placeholder: "voucher-item-highlight",
            update: function (event, ui) {
                var counter = 0;
                var ids = '';
                // for update row number when displace row
                $("#items tr").each(function () {
                    if ($(this).find('td:first').find('span:first').text() !== '#') {
                        $(this).find('td:first').find('span:first').text(counter);
                        counter++;
                    };
                    var id = $(this).attr('id');
                    if (id && id.length > 2)
                        ids += $(this).attr('id').substring(2) + "_";
                });
                if (ids.length > 0) {
                    ids = ids.substring(0, ids.length - 1);
                    runQuery("sortindices", "DocumentAccountingItems", "&ids=" + ids);
                    //alert(ids);
                }
                //alert($(ui.item).html());
                // we need update sum rows ofter displace rows
                postProcessRows();
            }
           
        });
        postProcessRows();

        var startupID = toInt($.urlParam('itemid'), -1);
        if (startupID > 0 && $('#tr' + startupID).length > 0)
            $("#itemscontainer").animate({ scrollTop: $('#tr' + startupID).offset().top - $("#itemscontainer").offset().top - 10 }, 1000);
        else
            $("#itemscontainer").animate({ scrollTop: $('#itemscontainer')[0].scrollHeight }, 1000);

    }
    function postProcessRows() {
        // 1- colorRows:
        //  make person red, if it supposed to have person, but is empty
        //  make project red, if it supposed to have project, but is empty
        // 2- align Dues left-aligned
        var sumOwe = 0, sumDue = 0, topicCode = '', topicTitle = '', lasttrid = null, startingTr = null;
        $('.sumRow').remove();
        $('#items tr').each(function () {
            var id = $(this).attr('id');
            if (id && id != '') {
                id = id.substring(2);
                $(this).removeClass('editing');
                // Coloring person
                //tr232   peneed232    peid232
                $('#peid' + id).closest('td').css('background-color', 'transparent');
                var peneed = $('#peneed' + id).text();
                if (peneed == 'True' || peneed == '1')
                    if (toInt($('#peid' + id).text(), 0) <= 0)
                        $('#peid' + id).closest('td').css('background-color', '#ff7D64');

                // Coloring project
                //tr232   prneed232    prid232
                $('#prid' + id).closest('td').css('background-color', 'transparent');
                var prneed = $('#prneed' + id).text();
                if (prneed == 'True' || prneed == '1')
                    if (toInt($('#prid' + id).text(), 0) <= 0)
                        $('#prid' + id).closest('td').css('background-color', '#ff7D64');

                // Coloring equipment
                //tr232   eqneed232    eqid232
                $('#eqid' + id).closest('td').css('background-color', 'transparent');
                var eqneed = $('#eqneed' + id).text();
                if (eqneed == 'True' || eqneed == '1')
                    if (toInt($('#eqid' + id).text(), 0) <= 0)
                        $('#eqid' + id).closest('td').css('background-color', '#ff7D64');

                // Coloring costcenter
                //tr232   csneed232   ccid232
                $('#ccid' + id).closest('td').css('background-color', 'transparent');
                var ccneed = $('#ccneed' + id).text();
                if (ccneed == 'True' || ccneed == '1')
                    if (toInt($('#ccid' + id).text(), 0) <= 0)
                        $('#ccid' + id).closest('td').css('background-color', '#ff7D64');

                $('#trn' + id).closest('td').removeClass('tl');
                $('#trc' + id).closest('td').removeClass('tl');
                if (toInt($('#du' + id).text(), 0) > 0) {
                    $('#trn' + id).closest('td').addClass('tl');
                    $('#trc' + id).closest('td').addClass('tl');
                }
                var trid = $('#trid' + id).text();                
                if (trid != '' && trid.indexOf('-') > 0)
                {
                    trid = trid.substr(0, trid.indexOf('-'));
                    var owe = toInt(fromMonetary(stripCommas($('#ow' + id).text())), 0);
                    var due = toInt(fromMonetary(stripCommas($('#du' + id).text())), 0);
                    if (lasttrid == trid) {
                        sumOwe += owe;
                        sumDue += due;
                    }
                    else {
                        if (lasttrid != null)
                            $(startingTr).before(addSumItem(sumOwe, sumDue, topicCode, topicTitle));
                        lasttrid = trid;
                        startingTr = $(this);
                        topicTitle = $('#trn' + id).text();
                        if (topicTitle == '' || topicTitle.indexOf('-') < 0)
                            topicTitle = $('#trl' + id).attr('title');
                        topicTitle = topicTitle.substr(0, topicTitle.indexOf('-'));
                        topicCode = $('#trp' + id).text();
                        sumOwe = owe;
                        sumDue = due;
                    }
                }
            }
        });
        if (startingTr)
        {
            $(startingTr).before(addSumItem(sumOwe, sumDue, topicCode, topicTitle));
            lasttrid = null;
            sumOwe = 0;
            sumDue = 0;
        }
    }
    function edit(id) {
        if (!checkProgramStatus())
            return false;
        if (curRow)
            $('#doCancel').click();
        curID = id;
        text2Val(id, insertArr);
    }

    function getChecked(selector) {
        // .is(':visible')
        if (!$(selector).is(':visible'))
            return 2;
        return $(selector).attr("checked") ? 1 : 0;
    }

    function insert() {
        if (!checkProgramStatus())
            return false;
        // validate
        if ($('#TreePath').val() == '') {
            $('#TTitle').focus();
            return false;
        }
        // both must not be 0 or empty
        hasOweDue = true;
        if (($('#Owe').val() == '' || $('#Owe').val() == '0') && ($('#Due').val() == '' || $('#Due').val() == '0')) {
            alert(Lang.EmptyInsert);
            hasOweDue = false;
            //$('#Owe').focus();
            //return false;
        }
        // both must not have value
        if ($('#Owe').val() != '' && $('#Owe').val() != '0' && $('#Due').val() != '' && $('#Due').val() != '0') {
            $('#Owe').focus();
            return false;
        }
        var _url = "set.aspx?op=RunScalar&qd=documentaccountingiteminsert&table=DocumentAccountingItems" + appendTime();
        if ($('#RowID').val()) {
            _url = "set.aspx?op=RunScalar&qd=documentaccountingitemupdate&table=DocumentAccountingItems&ID=" + curID + appendTime();
        }
        var personnelIDs = [$('#PersonnelID').val()];
        var multi = $('#PersonnelIDs').val() != '';
        if (multi)
            personnelIDs = $('#PersonnelIDs').val().split(',');
        var id = 0;
        for (var i = 0; i < personnelIDs.length; i++) {
            var dataString = {
                DocumentAccountingID: ID, TreePath: $('#TreePath').val(), PersonnelID: personnelIDs[i], ProjectID: $('#WardID').val()
                        , Owe: fromMonetary(stripCommas($('#Owe').val())), Due: fromMonetary(stripCommas($('#Due').val())), DocNo: $('#DocNo').val(), DocDate: $('#DocDate').val()
                        , PayCheckID: $('#PayCheckID').val(), CheckID: $('#CheckID').val(), Comments: $('#Comments').val()
                        , PayCheckRefuse: getChecked('#PayCheckRefuse'), CheckRefuse: getChecked('#CheckRefuse')
                        , SecTreePath: $('#SecTreePath').val(), SecPersonnelID: $('#SecPersonnelID').val()
                        , EquipmentID: $('#EquipmentID').val(), CostCenterID: $('#CostCenterID').val()
            };
            //alert(_url + " --> " + dataString);
            $.ajax({
                type: "POST",
                url: _url,
                data: dataString,
                async: false,
                success: function (html) {
                    //alert(html);
                    if (html.indexOf('Error:') < 0 && html.indexOf(Lang.Error) < 0) {
                        message(html + '<br />personnel is not allowed');
                        id = parseInt(html, 10);
                        //alert(html);
                    }
                    else {
                        displayError(html);
                    }
                },
                error: function () {
                    alert('error');
                }
            });
        }
        if ($('#RowID').val()) {
            id = curID;
        }
        return id;
    }
    function updateSumOweDue() {
        var owe = 0, due = 0;
        $(".sel").each(function () {
            if ($(this).attr('checked')) {
                var o = toFloat($(this).closest('tr').find('.owe').html(), 0);
                var d = toFloat($(this).closest('tr').find('.due').html(), 0);
                owe += o;
                due += d;
            }
        });
        $('#SumOwe').val(numberWithCommasAllowDP(owe));
        $('#SumDue').val(numberWithCommasAllowDP(due));
    }
    function bindEvents() {
        $(".sel").closest("tr").unbind("click");
        $(".sel").closest("tr").click(function (evt) {
            var row = $(this);
            var cell = $(evt.target).closest('td');
            if (cell.hasClass("del1") || cell.hasClass("sel1"))
                return;
            // curRowIndex = $('#items tr').index($(this).closest('tr'));
            var selected = row.hasClass('selected');
            $('#items tr').removeClass('selected');
                if (selected) {
                    // add blue color for editing row
                    $('#items tr').removeClass('editing');
                    row.addClass('editing');
                    //show clear button for cancel editing
                    //$('#doClear').show();

                    var id = $('.sel', row).attr('id').substring(3); // sel_1 --> 1
                    edit(id);
                }
                else {
                    row.addClass('selected');
                   // $('input:focus').blur();
                }
        });

        $(".delitem").unbind("click");
        $(".delitem").click(function () {
            var id = $(this).attr('id').substring(2); // dl_1 --> 1
            var row = $('#tr' + id);
            doConfirm(function () {
                if (runQuery("documentaccountingitemdelete", "DocumentAccountingItem", "&ID=" + id)) {
                    row.detach();
                    updateTotal();
                    postProcessRows();
                }
            }, Lang.ConfirmDelete);
        });

        $(".sel").unbind("click");
        $(".sel").click(updateSumOweDue);

       // $("#doCancel").unbind("click");
        //$('#doCancel').click(function () {
        //    curRow.show();
        //    editRow.detach();
        //    $('#doInsert').attr('disabled', null); //insertRow.show();
        //    setInsertAutos();
        //    bindEvents();
        //    curRow = null;
        //});

        //$("#doSave").unbind("click");
        //$('#doSave').click(function () {
        //    if (!checkDocNo($('#EDocNo').val(), $('#ETreePath').val())) {
        //       return;
        //    }
        //    if (save()) {
        //        checkOweDue($('#ETreePath').val());
        //        checkPersonCredit($('#EPersonnelID').val());
        //         curRow.show();
        //        val2Text(editArr, curID);
        //        editRow.detach();
        //        $('#doInsert').attr('disabled', null); //insertRow.show();
        //        setInsertAutos();
        //        bindEvents();
        //        updateTotal();
        //        postProcessRows();
        //    }
        //});
        //$("#doEInsertBalance").unbind("click");
        //$('#doEInsertBalance').click(function () {
        //    var balance = toInt($('#TotalBalance').val(), 0) - toInt($('#EOwe').val(), 0) + toInt($('#EDue').val(), 0);
        //    $('#EOwe').val('');
        //    $('#EDue').val('');
        //    if (balance < 0)
        //        $('#EOwe').val(-1 * balance);
        //    else if (balance > 0)
        //        $('#EDue').val(balance);
        //});


        $("#doInsert").unbind("click");
        $('#doInsert').click(function () {
            var multi = $('#PersonnelIDs').val() != '';
            if (!checkDocNo($('#DocNo').val(), $('#TreePath').val())) {
                return;
            };
            var id = insert();
            if (id > 0) {
                $('#PersonnelIDs').val('');
                $('#MultiPerson').removeClass('selected');
                if (multi) {
                    bindItems();
                    bindEvents();
                    updateTotal();
                    makeClear(insertArr);
                }
                else {
                    checkOweDue($('#TreePath').val());
                    checkPersonCredit($('#PersonnelID').val());
                     
                    // when we are editing we do not need add new row
                    if (!$('#RowID').val()) {
                        // curRowIndex = $('#items tr').index($(this).closest('tr'));
                        var node = new dummyNode(id);
                        var row = addItem(node);
                        $('#items tr:last').after(row);
                        $("#itemscontainer").animate({ scrollTop: $('#itemscontainer')[0].scrollHeight }, 1000);
                    }

                    //insertRow.before(row);
                    val2Text(insertArr, id);
                    makeClear(insertArr);
                    showInsertRowAtEnd();
                    bindEvents();
                    updateTotal();
                    postProcessRows();
                    // hide clear button after edit
                    //$('#doClear').hide();
                    autoDocNo();
                }
            }
            $('#TCode').focus();
        });

        $('#doClear').click(function() {
            makeClear(insertArr);
            postProcessRows();
            $('#TCode').focus();
            // postProcessRows();
            //showInsertRowAtEnd();
            //bindEvents();
            //updateTotal();
        });

        $("#doInsertBalance").unbind("click");
        $('#doInsertBalance').click(function () {
            var balance = toInt($('#TotalBalance').val(), 0);
            var owe = toInt($('#Owe').val(), 0);
            var due = toInt($('#Due').val(), 0);
            //when we are editing
            if ($('#RowID').val()) {
                if (balance < 0) {
                    if (owe) {
                        $('#Owe').val(numberWithCommas(owe + (-1 * balance)));
                    }
                    else if (due) {
                        $('#Due').val(numberWithCommas(due - (-1 * balance)));
                    }
                } else {
                    if (owe) {
                        $('#Owe').val(numberWithCommas(owe - balance));
                    }
                    else if (due) {
                        $('#Due').val(numberWithCommas(due + balance));
                    }
                }
            } else {
            $('#Owe').val('');
            $('#Due').val('');
            if (balance < 0)
                $('#Owe').val(numberWithCommas(-1 * balance));
            else if (balance > 0)
                $('#Due').val(numberWithCommas(balance));
          }
        });

        $("#doInsertLast").unbind("click");
        $('#doInsertLast').click(function () {
            var last = $('.delitem:last');
            if (last.attr('id')) {
                var id = last.attr('id').substring(2); // dl1 --> 1
                text2Val(id, insertArr, true);

                //// make Project empty if it is not required by topic
                //var prneed = $('#prneed' + id).text();
                //if (prneed != 'True' && prneed != '1') {
                //    $('#WardName,#WardID,#PrNeed').val('');
                //}

                //// make Equipment empty if it is not required by topic
                //var eqneed = $('#eqneed' + id).text();
                //if (eqneed != 'True' && eqneed != '1') {
                //    $('#EquipmentName,#EquipmentID,#EqNeed').val('');
                //}

                //// make Person empty if it is not required by topic
                //var peneed = $('#peneed' + id).text();
                //if (peneed != 'True' && peneed != '1') {
                //    $('#PersonnelName,#PersonnelID,#PeNeed').val('');
                //}

                //$('#TreePathName,#TreePath,#Owe,#Due').val('');
                //var balance = toInt($('#TotalBalance').val(), 0);
                //if (balance < 0)
                //    $('#Owe').val(-1 * balance);
                //else if (balance > 0)
                //    $('#Due').val(balance);
            }
        });


        $('#NewPerson').click(function () {
            var personnelID = toInt($('#PersonnelID').val(), -1);
            var oldID = _Context.id;
            var title = $('.popupTitle').html();
            function showcallback(v, m, f) {
                if (v == 'OK') {
                    var qry = (_Context.id == -1) ? 'personinsert' : 'personupdate';
                    newMastercallback(v, m, f, 'Personnel', qry, 'GroupingMember', 'personnelgroupingupdate', 'personnelgroupinginsert', 'personnelgroupingdelete', 'GroupingID', '#subDetailForm', function() { return false; });
                    if (!_Context.success)
                        return false;
                    $('.popupTitle').html(title).css('background-color','#000080');
                    $('#PersonnelName').val(f.Name + ' ' + f.Surname);
                    $('#PersonnelID').val(_Context.resultID);
                }
                $('#PersonnelName').focus();
                _Context.id = oldID;
            }
            _Context.id = personnelID;
            var _url = 'get.aspx?op=GetHtml&table=Personnel&qd=person&id=' + personnelID + '&view=person' + appendTime();
            showPopup(_url, showcallback, getButtons());
        });
        //$('#ENewPerson').click(function () {
        //    var oldID = _Context.id;
        //    function showcallback(v, m, f) {
        //        if (v == 'OK') {
        //            generalOKScalar(v, m, f, 'personupdate', 'personinsert', 'Personnel', '#subDetailForm');
        //            if (!_Context.success)
        //                return false;
        //            $('#EPersonnelName').val(f.Name + ' ' + f.Surname);
        //            $('#EPersonnelID').val(_Context.resultID);
        //        }
        //        _Context.id = oldID;
        //    }
        //    _Context.id = -1;
        //    var _url = 'get.aspx?op=GetHtml&table=Personnel&qd=paycheck&id=-1&view=person' + appendTime();
        //    showPopup(_url, showcallback, getButtons());
        //});
        //$('.cmnts').unbind('click');
        //$('.cmnts').click(function()
        //{
        //    $.prompt($(this).prev().html());
        //})
        dateSelect('.dateSelect');
    }
    var checkWarnNature = -10;
    function checkOweDue(treePath) {
        if (!hasOweDue)
            return;
        if (checkWarnNature == -10) {
            checkWarnNature = getSettings('WarnWrongNature');
            //alert(checkWarnNature);
        }
        if (checkWarnNature != 1)
            return;
        // OK = 1, NOK = -1
        // OKOwe = 1, NOKOwe = -1, OKDue = 2, NOKDue = -2, OKBoth = 3
        var status = getServerValue("treecheckowedue", "Trees", "&TreePath=" + treePath);
        if (status == -1)
            alert(Lang.InvalidNatureOwe);
        else if (status == -2)
            alert(Lang.InvalidNatureDue);
    }
    function checkDocNo(docNo, treePath) {
       // if (docNo == '')
         //   return;
        // OK = 1, NOK = -1
        // OKOwe = 1, NOKOwe = -1, OKDue = 2, NOKDue = -2, OKBoth = 3
        var oOrD = "";
        if ($('#Owe').val()) {
            oOrD = "&Owe=1";
        }
        else if ($('#Due').val()) {
            oOrD = "&Due=1";
        }
        var count = 0;
        if (toFloat(getSettings("CheckVoucherDupDocNo"), 0) == 1) {
            if ($('#RowID').val()) {
                count = toInt(getServerValue("getNumDocNoEx", "DocumentAccountingItems", oOrD + "&DocNo=" + docNo + "&ID=" + curID), 0);
            }
            else {
                count = toInt(getServerValue("getNumDocNo", "DocumentAccountingItems", oOrD + "&DocNo=" + docNo), 0);
            }
        }
        if (count > 0)
            alert(Lang.DuplicateDocNo);
        // banktreepath need to have DocNo
        var ok = toInt(getServerValue("checkbanktreepath", "Trees", "&TreePath=" + treePath), 0);
      //  var ok = toInt(getServerValue("checkbanktreepath2", "Trees", "&TreePath=" + treePath + "&DocNo=" + docNo), 0);
        // -- 0: No Bank, 1: Bank and DocNo is not OK, 2: Bank and DocNo OK
        //alert(ok);
       if(docNo == '' && ok == 1) {
            alert(Lang.BankNeedDocNo);
            return false;
       }
       return true;
       // alert(Lang.BankNeedDocNoValid);
        //return false;
    }
    //function bindPerssonel(event, data) {
    //    if (data) {
    //        // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due
    //        var hasChild = data[5] != '';
    //        if (hasChild) {
    //            $('#TreePathName, #TreePath').val('');
    //            alert(Lang.HasChild);
    //        }
    //        else {
    //            $('#PeNeed').val(data[2]);
    //            $('#PrNeed').val(data[3]);
    //        }
    //    }
    //    $('#PersonnelName').unautocomplete();
    //    autoComplete('Personnel', 'personnelselectauto', '#PersonnelName', '#PersonnelID', null
    //        , function (data, i, n, value) {
    //            $('#PersonnelID').val('');
    //            return formatPersonnelItem(data);
    //        });
    //}
    function updateTotal() {
        var sumOwe = 0, sumDue = 0, sumDiscount = 0;
        $('.owe').each(function (index, value) {
            sumOwe += toFloat($(value).text(), 0);
        });
        $('.due').each(function (index, value) {
            sumDue += toFloat($(value).text(), 0);
        });
        $('#TotalBalance').val(numberWithCommas((sumOwe.toFixed(_Context.decimalPoints) - sumDue.toFixed(_Context.decimalPoints)).toFixed(_Context.decimalPoints)));
        $('#TotalOwe').val(numberWithCommas(sumOwe.toFixed(_Context.decimalPoints)));
        $('#TotalDue').val(numberWithCommas(sumDue.toFixed(_Context.decimalPoints)));
        updateSumOweDue();
    }

    function showInsertRowAtEnd() {
        insertRow = $('.insertRow').detach();
        $('#commands').append(insertRow);
        //$('#commands tr:last').after(insertRow);
        $('#CheckRefuse,#PayCheckRefuse').attr('checked', false);
        $('#CheckRefuse,#PayCheckRefuse').hide();
        $('label[for="CheckRefuse"],label[for="PayCheckRefuse"]').hide();
        setInsertAutos();
       // $('#Comments').unbind('focus');
        //$('#Comments').focus(function () {
          //  $('#Comments').unautocomplete();
           // autoCompleteFree('DocumentAccountingItems', 'commentsauto', '#Comments', 'letsFuckTurkey', null, null, null, null, null, null
               // , 'TreePath=' + $('#TreePath').val());
     //   });
        $('#MultiPerson').click(function () {
            var _url = 'get.aspx?op=GetHtml&table=Personnel&view=personsselect' + appendTime();
            var btns = {};
            btns[Lang.Cancel] = "_CANCEL_";
            btns[Lang.OK] = "OK";
            showPopup(_url, function (v, m, f) {
                if (v == "OK") {
                    if (f.SPersonnelIDs != '') {
                        $('#PersonnelIDs').val(f.SPersonnelIDs);
                        if (!$('#MultiPerson').hasClass('selected'))
                            $('#MultiPerson').addClass('selected');
                    }
                    else
                        $('#MultiPerson').removeClass('selected');
                }
            }, btns);
        });
    }
    function bindPopupSelectors()
    {
        $('#EquipmentName').unbind('keyup');
        $('#EquipmentName').keyup(function (e) {
            if (e.keyCode === 37) {
                equipmentList();
            }
        });

        $('#WardName').unbind('keyup');
        $('#WardName').keyup(function (e) {
            if (e.keyCode === 37) {
                projectList();
            }
        });

        $('#CostCenterName').unbind('keyup');
        $('#CostCenterName').keyup(function (e) {
            if (e.keyCode === 37) {
                costCenterList();
            }
        });

        $('#PersonnelName').unbind('keyup');
        $('#PersonnelName').keyup(function (e) {
            if (e.keyCode === 37) {
                personList();
            }
        });

        $('#Comments').unbind('keyup');
        $('#Comments').keyup(function (e) {
            if (e.keyCode === 40) {
                commentsList();
            }
        });
    }
    function setInsertAutos() {
        $('#PersonnelName').unautocomplete();
        $('#TTitle').unautocomplete();
        $('#PersonnelTreePathName').unautocomplete();
        $('#WardName').unautocomplete();
        $('#EquipmentName').unautocomplete();
        $('#CostCenterName').unautocomplete();
        $('#CheckName').unautocomplete();
        $('#PayCheckName').unautocomplete();
        //bindComments();

        autoCompleteTreePath2('#TCode', '#TTitle', '#TreePath', '#selTree', '#ProgramCode', true);
        //autoComplete('Personnel', 'personnelselectauto', '#PersonnelName', '#PersonnelID', null
        //    , function (data, i, n, value) {
        //        $('#PersonnelID').val('');
        //        return formatPersonnelItem(data);
        //    }, null, null, null, null, true);
        autoCompleteNew({
            table: 'Personnel', qd: 'personnelselectauto', textInput: "#PersonnelName", hiddenInput: "#PersonnelID"
           , formatItem: function (data, i, n, value) {
               $('#PersonnelID').val('');
               return formatPersonnelItem(data);
           }, allowNull: false, above: true
        });
        autoCompleteFree('Personnel', 'persontreesauto', '#PersonnelTreePathName', '#SecTreePath'
            , function (event, data) {
                $('#SecPersonnelID').val(data[2]);
            }, null, null, null, null, null, null, true);

        autoComplete('Ward', 'wardchildsauto', '#WardName', '#WardID', null, null
            , function (data, value) {
                return data[1]; //3
            }, null, null, null, true);
        autoComplete('Equipment', 'equipmentchildsauto', '#EquipmentName', '#EquipmentID', null, null
            , function (data, value) {
                return data[1]; // 3
            }, null, null, null, true);
        autoComplete('CostCenter', 'costcenterchildsauto', '#CostCenterName', '#CostCenterID', null, null
            , function (data, value) {
                return data[1]; // 3
            }, null, null, null, true);
               

        autoCompleteFree('Checks', 'checksnewauto', '#CheckName', '#CheckID'
            , function () {
                $('#CheckRefuse').show();
                $('label[for="CheckRefuse"]').show();
            }
            , function (data, i, n, value) {
                // Checks.ID, No AS Title, Amount, DueDate
                return '<div dir=rtl>'
                    + Lang.No + " : " + data[1] + '<br />'
                    + Lang.Amount + " : " + numberWithCommas(toMonetary(noDecimalPlace(data[2]))) + '<br />'
                    + Lang.DueDate + " : " + data[3] + '<br />'
                    + '</div>';
            }, null, null, null, null, null, true);
        autoCompleteFree('PayCheck', 'paychecknewauto', '#PayCheckName', '#PayCheckID'
            , function () {
                $('#PayCheckRefuse').show();
                $('label[for="PayCheckRefuse"]').show();
            }
            , function (data, i, n, value) {
                // PayCheck.ID, PayCheck.Serial AS Title, Amount, DueDate
                return '<div dir=rtl>'
                    + Lang.No + " : " + data[1] + '<br />'
                    + Lang.Amount + " : " + numberWithCommas(toMonetary(noDecimalPlace(data[2]))) + '<br />'
                    + Lang.DueDate + " : " + data[3] + '<br />'
                    + '</div>';
            }, null, null, null, null, null, true);

        bindPopupSelectors();

        $('#TreePathName').focus();
    }

    //$('#GoAnotherDoc').click(function () {
    //    var serverid = getServerValue("getdocumentid", "DocumentAccounting", "&No=" + $('#No').val());
    //    if (serverid != _Context.id) {
    //        _Context.id = serverid;
    //        $(".jqibuttons").find(":button[value='Another']").last().click();
    //        //var url = 'get.aspx?op=GetHtml&table=DocumentAccounting&qd=documentaccounting&id=' + serverid + appendTime();
    //        // $(".jqibuttons").find(":button[value='Cancel']").last().click();
    //        //_Context.id = serverid;
    //        //showPopup(url);
    //    }
    //});

    // If direction is prev, we will show previous document (when the "Up" key is pressed), otherwise we show next document
    function showSiblingDocument(direction) {
        var serverid = _Context.id;
        if (direction == "prev") { // up
            serverid = getServerValue("getpredocumentid", "DocumentAccounting", "&No=" + $('#No').val());
        } else { // down
            serverid = getServerValue("getnextdocumentid", "DocumentAccounting", "&No=" + $('#No').val());
        }
        if (serverid > 0 && serverid != _Context.id) {
            _Context.id = serverid;
            $(".jqibuttons").find(":button[value='Another']").last().click();
        }
    }
    function bindUpDownArrows() {
        shortcut.remove('Up');
        shortcut.remove('Down');
        shortcut.remove('Left');
        shortcut.remove('Right');

        var $no = document.getElementById('No');
        shortcut.add('Up', function () {
            showSiblingDocument('prev');
        }, { target: $no });
        shortcut.add('Down', function () {
            showSiblingDocument('next');
        }, { target: $no });
    }
    bindUpDownArrows();

    function removeRows() {
        var ids = new Array();
        $(".sel").each(function() {
            if ($(this).attr('checked')) {
                 ids.push($(this).attr('id').substring(3));
            }
        });
        doConfirm(function() {
            for (var i = 0; i < ids.length; i++) {
                if (runQuery("documentaccountingitemdelete", "DocumentAccountingItem", "&ID=" + ids[i])) {
                    var row = $('#tr' + ids[i]);
                    row.detach();
                    updateTotal();
                    postProcessRows();
                };
            }
        }, Lang.ConfirmDeletes);
    }

    function autoDocNo(treePath, due, onChange) {
        var ok = 0;
        if(!onChange){
            ok = toInt(getServerValue("checkbanktreepath", "Trees", "&TreePath=" + treePath), 0);
        }
        if ((ok == 1 || onChange) && due) {
            $("#DocNoContainer").removeClass("col-sm-8").addClass("col-sm-7");
            $('#FromBankContainer').show();
            if (!$('#FromBank').attr('checked')) {
                // #Res.Serial, #Res.Serial Serial2, Amount, DueDate, ReceiverName, No, documentNo, Status
                autoComplete('BankAccountCheck', 'bankaccountcheckserialauto', '#DocNo', '#DocNo'
                    , function (event, data, formatted) {
                        if (data[5] || data[6] || data[7] == 11) // has no or DocumentNo or disproof
                            $('#DocNo').val('');
                        else
                            $('#DocNo').val(data[0]);
                    }, function (data, i, n, value) {
                        // $('#Serial').val('');
                        if (data[5] && data[7] != 11) {
                            // used// data[7] != 11//disproof checks
                            return Lang.Serial + " : " + data[0] + "<br />" + Lang.PayCheck + " - " + Lang.No + " : " + data[5]
                                + ", " + Lang.Reciever + " : " + data[4]
                               + "<br />" + Lang.DueDate + " : " + data[3]
                               + ", " + Lang.Amount + " : " + data[2];
                        }
                        if (data[6]) {
                            return Lang.Serial + " : " + data[0] + "<br />" + Lang.UsedInDocAccounting + " : " + data[6];
                        }
                        if (data[7] == 11) {
                            return Lang.Serial + " : " + data[0] + "<br />" + Lang.DisProof;
                        }
                        return Lang.Serial + " : " + data[0] + "<br />" + Lang.Unused;
                    }, null, null, null, 'BankAccountCheckID=NULL&TreePath=' + treePath, true);
            }
            else {
                $('#DocNo').unautocomplete();
            }
            $('#FromBank').unbind('click');
            $('#FromBank').click(function () {
                $('#DocNo').val('');
                $('#DocNo').unautocomplete();
                if ($('#FromBank').attr('checked')) {
                } else {
                    autoDocNo(treePath, due, true);
                }
            });
        } else {
            $("#DocNoContainer").removeClass("col-sm-7").addClass("col-sm-8");
            $('#FromBank').attr('checked', false);
            $('#FromBankContainer').hide();
            $('#DocNo').unautocomplete();
        }
    }

    //function bindComments() {
    //    //function TCodeBlur() {
    //    //    //alert($('#TreePath').val());
    //    //    $('#Comments').unautocomplete();
    //    //    autoCompleteFree('DocumentAccountingItems', 'commentsauto', '#Comments', 'letsFuckTurkey', null, null, null, null, null, null
    //    //        , 'TreePath=' + $('#TreePath').val(), true);
    //    //}
    //    //$('#TTitle, #TCode').unbind('blur', TCodeBlur);
    //    //$('#TTitle, #TCode').blur(TCodeBlur);
    //}

    function equipmentList() {
        $('.ac_results').hide();
        var _url = 'get.aspx?op=GetHtml&table=Equipment&view=equipmentselect' + appendTime();
        var baseID = toInt($('#EquipmentID').val(), -1);
        if (baseID != -1)
            _url += '&bid=' + baseID;
        _url += appendTime();
        var btns = {};
        btns[Lang.Cancel] = "_CANCEL_";
        showPopup(_url, function (v, m, f) {
            if (f.SEquipmentName != '') {
                $('#EquipmentName').val(f.SEquipmentName);
                $('#EquipmentID').val(f.SEquipmentID);

            }
            $('#EquipmentName').focus();
        }, btns); 
    }
    function costCenterList() {
        $('.ac_results').hide();
        var _url = 'get.aspx?op=GetHtml&table=CostCenter&view=costcenterselect' + appendTime();
        var baseID = toInt($('#CostCenterID').val(), -1);
        if (baseID != -1)
            _url += '&bid=' + baseID;
        _url += appendTime();
        var btns = {};
        btns[Lang.Cancel] = "_CANCEL_";
        showPopup(_url, function (v, m, f) {
            if (f.SCostCenterName != '') {
                $('#CostCenterName').val(f.SCostCenterName);
                $('#CostCenterID').val(f.SCostCenterID);

            }
            $('#CostCenterName').focus();
        }, btns);
    }

    function projectList() {
        $('.ac_results').hide();
        var _url = 'get.aspx?op=GetHtml&table=Ward&view=wardselect' + appendTime();
        var baseID = toInt($('#WardID').val(), -1);
        if (baseID != -1)
            _url += '&bid=' + baseID;
        _url += appendTime();
        var btns = {};
        btns[Lang.Cancel] = "_CANCEL_";
        showPopup(_url, function (v, m, f) {
            if (f.SWardName != '') {
                $('#WardName').val(f.SWardName);
                $('#WardID').val(f.SWardID);

            }
            $('#WardName').focus();
        }, btns);
    }

    function personList() {
        $('.ac_results').hide();
        var _url = 'get.aspx?op=GetHtml&table=Ward&view=personselect' + appendTime();
        var btns = {};
        btns[Lang.Cancel] = "_CANCEL_";
        showPopup(_url, function (v, m, f) {
            if (f.SPersonName != '') {
                $('#PersonnelName').val(f.SPersonName);
                $('#PersonnelID').val(f.SPersonID);

            }
            $('#PersonnelName').focus();
        }, btns);
    }

    function commentsList() {
        $('.ac_results').hide();
        var _url = 'get.aspx?op=GetHtml&table=DocumentAccountingItems&view=commentselect' + appendTime();
        var treePath = $('#TreePath').val();
        if (treePath)
            _url += '&TreePath=' + treePath;
        _url += appendTime();
        var btns = {};
        btns[Lang.Cancel] = "_CANCEL_";
        showPopup(_url, function (v, m, f) {
            if (f.SComments != '') {
                $('#Comments').val(f.SComments);

            }
            $('#Comments').focus();
        }, btns);
    }


    // ------------------------------
    // startup code
    // ------------------------------
    ID = parseInt($('#ID').val(), 10);
    _Context.priceDate = $('#Date').val();

    //$('#doClear').hide();


    showPersonBook('#PersonnelID', true);
    //load the last doc No
    $('#LastDocNo').click(function () {
        var LastDocNo = getServerValue('thelastdocno', 'DocumentAccounting');
        $('#No').val(LastDocNo);
    });
    $('#ShowDoc').click(function () {
        if (!$('#No').val()) {
            alert(Lang.EmptyDocNo);
            return;
        }
        var idd = getServerValue('thelastdocID', 'DocumentAccounting', '&No=' + $('#No').val());
        window.open('list.aspx?table=DocumentAccounting&id=' + idd, '_blank');
    });
   // showPersonBook('#EPersonnelID');
    if (ID > 0) {
        editRow = $('.editRow').detach();
        bindItems();
        showItems();
        showInsertRowAtEnd();

        //setInsertAutos();

        // load latest comments for selected treepath and 
        //bindComments();

        $('#Owe, #Due').blur(function () {
            var treePath = $('#TreePath').val();
           // var owe = toInt($('#Owe').val(), 0);
            var due = toInt($('#Due').val(), 0);
            autoDocNo(treePath, due);
        });

        $('#ToDue').click(function () {
            if ($('#Owe').val()) {
                $('#Due').val($('#Owe').val());
                $('#Owe').val('');
            }
        });

        $('#ToOwe').click(function () {
            if ($('#Due').val()){
            $('#Owe').val($('#Due').val());
            $('#Due').val('');
        }
        });

        $('#PersonsList').click(function() {
            personList();
        });

        $('#ProjectList').click(function () {
            projectList();
        });

        $('#NewProject').click(function () {
            var oldID = _Context.id;
            function showcallback(v, m, f) {
                if (v == 'OK') {
                    generalOKScalar(v, m, f, 'projectupdate', 'projectinsert', 'Ward', '#subDetailForm');
                    if (!_Context.success)
                        return false;
                    $('#WardName').val(f.ParentName + '-' + f.Name);
                    $('#WardID').val(_Context.resultID);
                }
                _Context.id = oldID;
            }
            _Context.id = -1;
            var _url = 'get.aspx?op=GetHtml&table=Ward&qd=project&id=-1&view=project' + appendTime();
            showPopup(_url, showcallback, getButtons());
        });

        $('#EquipmentList').click(function () {
            equipmentList();
        });

        $('#NewEquipment').click(function () {
            var oldID = _Context.id;
            function showcallback(v, m, f) {
                if (v == 'OK') {
                    generalOKScalar(v, m, f, 'equipmentupdate', 'equipmentinsert', 'Equipment', '#subDetailForm');
                    if (!_Context.success)
                        return false;
                    $('#EquipmentName').val(f.ParentName + '-' + f.Title);
                    $('#EquipmentID').val(_Context.resultID);
                }
                _Context.id = oldID;
            }
            _Context.id = -1;
            var _url = 'get.aspx?op=GetHtml&table=Equipment&qd=equipment&id=-1&view=equipment' + appendTime();
            showPopup(_url, showcallback, getButtons());
        });

        $('#CostCenterList').click(function () {
            costCenterList();
        });

        $('#NewCostCenter').click(function () {
            var oldID = _Context.id;
            function showcallback(v, m, f) {
                if (v == 'OK') {
                    generalOKScalar(v, m, f, 'costcenterupdate', 'costcenterinsert', 'CostCenter', '#subDetailForm');
                    if (!_Context.success)
                        return false;
                    $('#CostCenterName').val(f.ParentName + '-' + f.Name);
                    $('#CostCenterID').val(_Context.resultID);
                }
                _Context.id = oldID;
            }
            _Context.id = -1;
            var _url = 'get.aspx?op=GetHtml&table=CostCenter&qd=costcenter&id=-1&view=costcenter' + appendTime();
            showPopup(_url, showcallback, getButtons());
        });


        $('#newPayCheck').click(function () {
            var oldID = _Context.id;
            function showcallback(v, m, f) {
                if (v == 'OK') {
                    generalOKScalar(v, m, f, 'paycheckupdate', 'paycheckinsert', 'PayCheck', '#subDetailForm');
                    if (!_Context.success)
                        return false;
                    $('#PayCheckName').val(f.Serial);
                    $('#PayCheckID').val(_Context.resultID);
                }
                _Context.id = oldID;
            }
            _Context.id = -1;
            var _url = 'get.aspx?op=GetHtml&table=PayCheck&qd=paycheck&id=-1' + appendTime();
            showPopup(_url, showcallback, getButtons());
        });
        $('#newCheck').click(function () {
            var oldID = _Context.id;
            function showcallback(v, m, f) {
                if (v == 'OK') {
                    generalOKScalar(v, m, f, 'checkupdate', 'checkinsert', 'Checks', '#subDetailForm');
                    if (!_Context.success)
                        return false;
                    $('#CheckName').val(f.No);
                    $('#CheckID').val(_Context.resultID);
                }
                _Context.id = oldID;
            }
            _Context.id = -1;
            var _url = 'get.aspx?op=GetHtml&table=Checks&qd=check&id=-1' + appendTime();
            showPopup(_url, showcallback, getButtons());
        });
        $('#selTree').click(function () {
            var _url = 'get.aspx?op=GetHtml&table=Trees&view=treeselect' + appendTime();
            var btns = {};
            btns[Lang.Cancel] = "_CANCEL_";
            showPopup(_url, function (v, m, f) {
                if (f.STreePathName != '') {
                    $('#TopicCode').val(f.STopicCode);
                    $('#TopicTitle').val(f.STopicTitle);
                    $('#TreePathName').val(f.STreePathName);
                    $('#TreePath').val(f.STreePath);
                    $('#PeNeed').val(f.SPeNeed);
                    $('#PrNeed').val(f.SPrNeed);
                    $('#EqNeed').val(f.SEqNeed);
                    $('#CcNeed').val(f.SCcNeed);
                }
                // In the shown popup, we unbind the up/down keys, so we need to re-bind them here!
                bindUpDownArrows();
            }, btns);
        });

        bindEvents();
        updateTotal();
    }
    else {
        $('#commands').hide();
        hideItems();
        $('#Date').val(_Context.today);
    }
    dateSelect('.dateSelect');
    insertValidation();
    monetaryKeys();
    $('#Owe, #Due').keyup(function () {
        $(this).val(numberWithCommas($(this).val()));
    });

    shortcut.remove("F3");
    shortcut.add("F3", function () {
        if (!document.activeElement) {
            if ($("#TreePathName").is(':visible'))
                $("#TreePathName").focus();
            else if ($("#ETreePathName").is(':visible'))
                $("#ETreePathName").focus();
        }
        else if (document.activeElement.id == "TreePathName")
            $("#Owe").focus();
        else if (document.activeElement.id == "Owe")
            $("#Due").focus();
        else if (document.activeElement.id == "Due")
            $("#TreePathName").focus();
        else if (document.activeElement.id == "ETreePathName")
            $("#EOwe").focus();
        else if (document.activeElement.id == "EOwe")
            $("#EDue").focus();
        else if (document.activeElement.id == "EDue")
            $("#ETreePathName").focus();
        else {
            if ($("#TreePathName").is(':visible'))
                $("#TreePathName").focus();
            else if ($("#ETreePathName").is(':visible'))
                $("#ETreePathName").focus();
        }
    });

    shortcut.remove("F4");
    shortcut.add("F4", function () {
        var focused = $(':focus');
        focused.blur();
        if ($("#doSave").is(":visible"))
            $("#doSave").click();
        else if ($("#doInsert").is(":visible"))
            $("#doInsert").click();
    });
    shortcut.remove("alt+R");
    shortcut.add("alt+R", function () {
        $("#doInsertLast").click();
    });
    shortcut.remove("alt+C");
    shortcut.add("alt+C", function () {
        $("#doClear").click();
    });
    shortcut.remove("=");
    shortcut.add("=", function () {
        $("#doInsertBalance").click();
    });

    shortcut.remove("ESC");
    shortcut.add("ESC", function () {
        if ($("#doCancel").is(":visible"))
            $("#doCancel").click();
            // second _CANCEL_
        else if ($('.dpTodayButton').last().is(':visible'))// close Datepicker
            $('.dpTodayButton').last().click();
        else if ($('#items .selected').length > 0)
            $('#items .selected').removeClass('selected');
            // second _CANCEL_
        else if ($(".jqibuttons").find(":button[value='_CANCEL_']").length == 1) {
            $(".jqibuttons").find(":button[value='_CANCEL_']").click();
        }
            // support second form: use .last() to click the top one
        else if ($(".jqibuttons").find(":button[value='Cancel']").length > 0) {
            $(".jqibuttons").find(":button[value='Cancel']").last().click();
        }
        $('#filterDiv').hide();
    });

    shortcut.remove("=");
    shortcut.remove("»");
    shortcut.add("»", function () {
        $("#doInsertBalance").click();
    });
    shortcut.add("=", function () {
        $("#doInsertBalance").click();
    });
    shortcut.remove("Delete");
    shortcut.add("Delete", function () {
        var selected = false;
        $('.sel').each(function() {
            if ($(this).attr('checked')) {
                selected = true;
                return;
            }
        });
        if (selected){
            removeRows();
        }
    });
    var startupID = toInt($.urlParam('itemid'), -1);
    if (startupID > 0) {
        $('#tr' + startupID).addClass('selected');
       // $('#TCode').focus();
    }

    $('#Search').keyup(function () {
        var what = $('#Search').val();
        if (what == '')
            $('#items tr').show();
        else
            $('#items tr').each(function () {
                if ($(this).hasClass('th'))
                    return;
                if ($(this).html().indexOf(what) >= 0)
                    $(this).show();
                else
                    $(this).hide();
            });
    });
    $("#No").focus();
    $('#TCode').focus();
    $(".jqibuttons").find(":button[value='Another']").last().hide();
    function showItems() {
        $("#itemscontainer").show();
        $("#items-empty").hide();
    }

    function hideItems() {
        $("#itemscontainer").hide();
        $("#items-empty").show();
    }

    if (ID > 0) {
        uploadify('#ChooseFile', '#FileName', '#StartUpload', 'workflow/documentaccounting/' + ID
                   , function (fileName) {
                       $('#FileNameDisplay').html(fileName);
                   });
        $('#workflowtasks').html(showWorkflowTasks(ID, WorkflowTypeEnum.Voucher));
    }
    else
        $('.transDiv').hide();
    $('.determine, .documents, .reorder, .reorderid, .copy, .copyfrom, .exchangeno, .editcomments, .another, .tovoucherdb').addClass('hidden');
    $('.showbtns').click(function () {
        if ($(this).text() == '>>') {
            $(this).text('<<');
        } else {
            $(this).text('>>');
        }
        $('.determine, .documents, .reorder, .reorderid, .copy, .copyfrom, .exchangeno, .editcomments, .another, .tovoucherdb').toggleClass('hidden');
    });

    var h = $(window).height();
    h -= $('.row1').height() + $('.row3').height() + $('.popupTitle').height() + $('.jqibuttons').height() + 80;
    $('#itemscontainer').css("max-height", h).height(h);
    $jQuery1_9('[data-toggle="tooltip"]').tooltip();
}