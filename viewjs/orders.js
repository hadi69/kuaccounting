﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
93-05-14: file created
*/
function view_orders() {
    var pid = $.urlParam('pid');
    var formName = "";
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var terminology = getTerminology(_Context.table);
        if (terminology)
            $('#term').html("<a href='help/terminology.aspx' target=_blank title='" + Lang.HasTerminology + "'>" + terminology + "</a>");
        var personid = $.urlParam("pid");
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + "&more=" + encodeURIComponent("PersonnelID=" + personid) + appendTime();
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 20, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.Title, name: 'Title', sortable: true, css_class: 'width-200' },
                    { display: Lang.PersonnelName, name: 'PersonnelName', sortable: true, css_class: 'width-200'},
                    { display: Lang.OrderDefName, name: 'OrderDefName', sortable: true},
                    { display: Lang.OrderDefID, name: 'OrderDefID', sortable: true,hide: true},
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' },
                    { display: Lang.ColDel, name: 'Del', sortable: false, process: delLink, css_class: 'action-cell' }
                ],
                buttons:
                [
                    { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                    { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                    { separator: true },
                    { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                    { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                sortname: "PersonnelName",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                }
            });
    }
    function showcallback(v, m, f) {
        // ex: formName + 'update' --> rule86update
        if (v == 'Calc') {
            calcOrder(v, m, f, f.ConstID, "", f.OrderDefID, "", '#detailFormOrder');
            return false;
        }
        if (v == 'Another') {
            _Context.id = f.ID;
            show(f.ID);
            return true;
        }
        return insertOrder(v, m, f, 'orderupdate','orderinsert',_Context.table, '#detailFormOrder', show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        var btns = getDefaultButtons();
        btns[Lang.Calculate] = 'Calc';
        btns['Another'] = 'Another';
        formName = getServerValue('orderformshow', 'OrderDefs', null); // ex: rule86
        var orderdefID = getServerValue('getorderdefID', 'OrderDefs', null);
        var m = '&ID=' + pid;
        var personName = getServerValue("selectpersonname", "Personnel", m);
        var _url = 'get.aspx?op=GetHtmlOrder&table=' + _Context.table + '&view=WorkerOrder&qd=' + _Context.query + '&id=' + id + '&PersonnelID=' + pid + '&PersonnelName=' + personName + '&OrderDefID=' + orderdefID + "&type=Order" + appendTime();
        showPopup(_url, showcallback, btns, null, successCallback);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    $("#bar").prepend(breadcrumb({
        title: _Context.title,
        grandParent: {
            title: Lang.MSalary,
            url: 'menu/salary.aspx'
        },
        parent: {
            title: Lang.Staff,
            url: 'list.aspx?table=Personnel&menu=salarysa&view=staffs'
        }
    }));
    setClassBasic();
    run();
}