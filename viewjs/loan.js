﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
92-10-19: file created
*/
function view_loan() {
    makeChosen();
    $("#tableInstallment").smartTable();
   var cnt = 0;
    var mID = -1;
    // ---------------------
    // Add Rows
    // ---------------------
    function addInstallment(node) {
        var row = '<tr>';
        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '" >' + (cnt + 1) + '</td>';

        row += '<td data-th="' + Lang.DueDate + '">'
            + '<input type="hidden" id="i_' + cnt + '" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="hidden"  name="LoanID" value="' + getNodeVal(node, "LoanID", -1) + '" />'
            + '<input type="text" name="DueDate' + cnt + '" id="dd_' + cnt + '" value="' + getNodeVal(node, "DueDate")
            + '" class="dateSelect DueDate form-control input-sm align-center" />'
            + '</td>';

        row += '<td data-th="' + Lang.Amount + '">'
            + '<input type="text" name="Amount" id="a_' + cnt + '" value="' + getNodeVal(node, "Amount")
            + '" class=" monetary validate[custom[integer]] Amount form-control input-sm align-center" />'
            + '</td>';

        row += '<td data-th="' + Lang.ValueAdded + '">'
            + '<input type="text" name="ValueAdded" id="va_' + cnt + '" value="' + getNodeVal(node, "ValueAdded")
            + '" class=" monetary validate[custom[integer]] ValueAdded form-control input-sm align-center" />'
            + '</td>';

        row += '<td data-th="' + Lang.PayStatus + '">'
            + '<select id="ps_' + cnt + '"  class="PayStatus" name="PayStatus" v="' + getNodeVal(node, "PayStatus") + '" >'
            + '<option value="0">' + Lang.NotPaid + '</option>'
            + '<option value="1">' + Lang.Paid + '</option>'
            + '</select>'
            + '</td>';

        row += '<td data-th="' + Lang.DueTopicTreePathName + '">'
            + '<input type="text" id="DueTopicTreePathName" name="DueTopicTreePathName" value="' + getNodeVal(node, "DueTopicTreePathName")
            + '" class="duetopictreepathname form-control input-sm"/>'
            + '<input type="hidden" id="DueTopicTreePath" name="DueTopicTreePath" value="' + getNodeVal(node, "DueTopicTreePath") + '" class="duetopictreepath" />'
            + '</td>';

        row += '<td data-th="' + Lang.PayDate + '">'
            + '<input type="text" name="PayDate' + cnt + '" id="pd_' + cnt + '" value="' + getNodeVal(node, "PayDate")
            + '" class="dateSelect PayDate form-control input-sm align-center" />'
            + '</td>';

        row += '<td data-th="' + Lang.DesiredDocNo + '">'
            + '<input type="text" name="InstallmentDesiredDocNo" id="idn_' + cnt + '" value="' + getNodeVal(node, "InstallmentDesiredDocNo")
            + '" class=" InstallmentDesiredDocNo form-control input-sm align-center" />'
            + '</td>';

        row += '<td data-th="' + Lang.Comments + '">'
            + '<input type="text" name="Comment" id="c_' + cnt + '" value="' + getNodeVal(node, "Comment") + '"  class="Comment form-control input-sm"/>'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a class="btn btn-danger btn-xs btn-block btn-remove removeinstallment" id="ri_' + cnt + '">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt++;
        setValue('.PayStatus');
        return row;
    }
    //-------------------
    // remove rows
    //----------------
    function removeInstallment($who) {

        var id = $who.attr('id'); // pn_1
        id = id.substring(3); // pn_1  -->  1
        $('#a_' + id).val("");
        $('#va_' + id).val("");
        $who.parent().parent().hide();
        updateInstallmentsAmount();
        Growl.Notice(Lang.DeletedSuccessfully);
    }
    //--------------
    function autoPostPack(event, data, formatted) {
        $(event.target).next().val(data[0]);
    }
    //-------------------

    function bindAdds() {
        $('.addinstallment').click(function () {
            var table = $('#tableInstallment');
            $('tr:last', table).after(addInstallment(null));
            Growl.Notice(Lang.RowAddedSuccessfully);
            var lastRow = $('tr:last', table);
            dateSelect('#tableInstallment tr:last .dateSelect');
            autoComplete('Trees', 'treesauto', "#tableInstallment tr:last .duetopictreepathname", "#justfurfun"
                    , function (event, data, formatted) {
                        // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due, TopicTitle, ProgramCode, Nature, EquipmentNeed, CanHaveSubTree
                        var hasChild = toBool(data[5]), canHaveSubTree = toBool(data[12]);
                        if (hasChild || canHaveSubTree) {
                            $('#FromTopic, #FromTopicID').val('');
                            if (hasChild)
                                alert(Lang.HasChild);
                            else
                                alert(Lang.HasSubTreeBut);
                        }
                        $(event.target).next().val(data[0]);
                    }, formatTreePath);
            $('.removeinstallment', lastRow).click(function () {
                removeInstallment($(this));
            });
            $('.Amount, .ValueAdded', lastRow).blur(function () {
                updateInstallmentsAmount();
            });
            commaInputs();
        });
        $('#InstallmentStart').click(function () {
            var table = $('#tableInstallment');
            var date = $('#DateOfFirstInstallment').val().split('/')
            var num = $('#InstallmentNum').val();
            var amount = $('#AmountOfEach').val();
            var valueAdded = $('#ValueAddedOfEach').val();
            if ($('#ValueAddedOfEach').val().indexOf('%') >= 0) {
                valueAdded = toFloat($('#ValueAddedOfEach').val().replace('%', ''), 0) * toFloat($('#AmountOfEach').val()) / 100;
            }
            
            if (date[2] == 30 || date[2] == 31)
            {
                alert(Lang.LoanJsEM1);
                return;
            }
            if (date[1] == null || !(num && amount))
            {
                alert(Lang.LoanJsEM2);
                return;
            }
            for (var i = 1; i <= num; i++) {
                $('tr:last', table).after(addInstallment(null));
                var lastRow = $('tr:last', table);
                $('tr:last .Amount', table).val(amount);
                $('tr:last .ValueAdded', table).val(valueAdded);
                $('tr:last .PayStatus', table).val('0');

                autoComplete('Trees', 'treesauto', "#tableInstallment tr:last .duetopictreepathname", "#justfurfun"
                    , function (event, data, formatted) {
                       // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due, TopicTitle, ProgramCode, Nature, EquipmentNeed, CanHaveSubTree
                       var hasChild = toBool(data[5]), canHaveSubTree = toBool(data[12]);
                       if (hasChild || canHaveSubTree) {
                           $('#FromTopic, #FromTopicID').val('');
                           if (hasChild)
                               alert(Lang.HasChild);
                           else
                               alert(Lang.HasSubTreeBut);
                       }
                       $(event.target).next().val(data[0]);
                  }, formatTreePath);

                dateSelect('#tableInstallment tr:last .dateSelect');
                if (i == 1)
                {
                    $('tr:last .DueDate', table).val(date.join('/'));
                }
                else {
                    if (date[1] < 12) {
                        date[1]++;
                    }
                    else {
                        date[0]++;
                        date[1] = '01';
                    }
                    $('tr:last .DueDate', table).val(date.join('/'));
                }
            }
           commaInputs();
            updateInstallmentsAmount();
        });
    }

    function loanTypeChange() {
        var type = $('#LoanType').val();
        if (type == 0) {
            $('#BorrowerDiv').hide();
            $('#LoanerDiv').show();
            $('#OweOrDueTreePath').html(Lang.DueTopicTreePathName);
            $('#OweTreePathName').unautocomplete();
            $('#DueTreePathName').unautocomplete();
            autoCompleteTreePath('#OweTreePathName', '#OweTreePath', '#selOweTree');
            autoComplete('Trees', 'treestypedauto', '#DueTreePathName', '#DueTreePath'
            , function (event, data, formatted) {
            // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due, Nature, TopicTitle, ProgramCode, EquipmentNeed, CanHaveSubTree
            var hasChild = toBool(data[5]), canHaveSubTree = toBool(data[12]);
            if (hasChild || canHaveSubTree) {
                $('#FromTopic, #FromTopicID').val('');
                if (hasChild)
                    alert(Lang.HasChild);
                else
                    alert(Lang.HasSubTreeBut);
            }
          }, formatTreePath, null, null, 'get.aspx?op=Auto&table=Trees&qd=treestypedauto&Type=5' + appendTime());
        }
        if (type == 1) {
            $('#BorrowerDiv').show();
            $('#LoanerDiv').hide();
            $('#OweOrDueTreePath').html(Lang.OweTopicTreePathName);
            $('#OweTreePathName').unautocomplete();
            $('#DueTreePathName').unautocomplete();
            autoComplete('Trees', 'treestypedauto', '#OweTreePathName', '#OweTreePath'
            , function (event, data, formatted) {
                // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due, Nature, TopicTitle, ProgramCode, EquipmentNeed, CanHaveSubTree
                var hasChild = toBool(data[5]), canHaveSubTree = toBool(data[12]);
                if (hasChild || canHaveSubTree) {
                    $('#FromTopic, #FromTopicID').val('');
                    if (hasChild)
                        alert(Lang.HasChild);
                    else
                        alert(Lang.HasSubTreeBut);
                }
            }, formatTreePath, null, null, 'get.aspx?op=Auto&table=Trees&qd=treestypedauto&Type=10' + appendTime());
            autoCompleteTreePath('#DueTreePathName', '#DueTreePath', '#selDueTree');
        }
    }

    function updateInstallmentsAmount() {
        var sumAmount = 0, addedValue = 0;
        $('.Amount').each(function(index, value) {
            var id = $(value).attr('id');
            id = id.substring(id.indexOf('_', 0) + 1); // d_1  -->  1
            addedValue = toFloat($('#va_' + id).val(), 0); // total
            sumAmount += toFloat($(value).val(), 0) + addedValue;

        });

        $('#InstallmentAmount').val(numberWithCommas(stripCommas(sumAmount)));
    }

    //------------------

    function bindRemoves() {
        $('.removeinstallment').click(function () {
            removeInstallment($(this));
        });
    }
    //-------------------------
    function doInstallment() {
        if ($('#DoInstallment').attr('checked'))
        {
            $('#DateOfFirstInstallment').addClass('validate[required]');
            $('#AmountOfEach').addClass('validate[required]');
            $('#InstallmentNum').addClass('validate[required]');
            insertValidation();
            $('#DoIt').show();
        }
        else
            $('#DoIt').hide();
    }
    // -----------------------------
    // startup code
    // ------------------------------

    mID = toInt($('#MasterID').val(), -1);
    $('#DoIt').hide();
    $('#DoInstallment').click(doInstallment);
    fillTable("get.aspx?op=GetXml&qd=installment1&LoanID=" + mID, '#tableInstallment', addInstallment);
    $('#Remain').val($('#InstallmentAmount').val() - $('#PaidAmount').val());
    setValue('.PayStatus');
    setValue('#LoanType');
    setValue('#Status');  
    bindAdds();
    bindRemoves();
    dateSelect('.dateSelect');
    function doMore()
    {
        var lastRow = $('tr:last', '#tableInstallment');
        dateSelect('#tableInstallment .dateSelect');
        autoComplete('Trees', 'treesauto', "#tableInstallment .duetopictreepathname", "#justfurfun"
            , function (event, data, formatted) {
                // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due, TopicTitle, ProgramCode, Nature, EquipmentNeed, CanHaveSubTree
                var hasChild = toBool(data[5]), canHaveSubTree = toBool(data[12]);
                if (hasChild || canHaveSubTree) {
                    $('#FromTopic, #FromTopicID').val('');
                    if (hasChild)
                        alert(Lang.HasChild);
                    else
                        alert(Lang.HasSubTreeBut);
                }
                $(event.target).next().val(data[0]);
            }, formatTreePath);
        $('.Amount, .ValueAdded', lastRow).blur(function () {
            updateInstallmentsAmount();
        });
        updateInstallmentsAmount();
        commaInputs();
        insertValidation();
    }
    autoCompleteFree('Personnel', 'personnelselectauto', '#BorrowerName', '#BorrowerID', null
        , function (data, i, n, value) {
            //$('#BuyerID').val('');
            return formatPersonnelItem(data);
        });
    loanTypeChange();
    $('#LoanType').change(loanTypeChange);
    //setValue('#LoanDate');
    monetaryInputsSet(null, '#LoanDate');
    doMore();
}