﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-10-19: file created
*/
function view_conversions() {
    var status = -1;
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        //alert(_url);
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell'},
                    { display: Lang.ExchangeofOrigin, name: 'FromName', sortable: true, css_class: 'width-200'},
                    { display: Lang.ExchangeDestination, name: 'ToName', sortable: true},
                    { display: Lang.ExchangeRate, name: 'Factor', sortable: true, process: showInt, css_class: 'width-200'},
                    { display: Lang.On, name: 'StartDate', sortable: true, css_class: 'date-cell'},
                    { display: Lang.UpToDate, name: 'EndDate', sortable: true, css_class: 'date-cell'},
                    { display: Lang.Enabled, name: 'Enabled',sortable: true, process: showBool, css_class: 'align-center width-40' },
                    { display: Lang.ColView, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' },
                    { display: Lang.ColDel, name: 'Del', sortable: false, process: delLink, css_class: 'action-cell' }
                ],
                buttons:
                [
                    { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                    { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                    { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                sortname: "FromName",
                sortorder: "asc",
                usepager: true,
                //title: _Context.title,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,

                height: gridHeight, resizable: gridResizable,
                singleSelect: false
            });
        $('#Sections').change(function () {
            refresh();
        });
        status = -1;
    }
    function showcallback(v, m, f) {
        return newShowcallback(v, m, f, 'conversionupdate', 'conversioninsert', _Context.table, null, show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['sys,conversion']);
   // checkLevel(3);
    setClassSys();
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.Conversion, parentTitle: Lang.Management, parentUrl: "menu/admin.aspx" } }));
    run();
}