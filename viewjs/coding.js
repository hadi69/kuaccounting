﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
92-10-19: file created
*/
function view_coding() {
    makeChosen();
    $('#tablecodingItem').smartTable();
    var cnt = 0, cnt2 = 0, cnt3 = 0, cnt4 = 0;
    var mID = -1;
    // ---------------------
    // Add Rows
    // ---------------------
    function addRow(node) {
        var row = '<tr>';
        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">'
            + (cnt + 1)
            + '<input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '</td>';

        row += '<td data-th=" ' + Lang.Grouping + '">'
            + '<input type="text" name="GroupingName" id="gn_' + cnt + '" value="' + getNodeVal(node, "GroupingName") + '" class="groupingname form-control input-sm"/>'
            + '<input type="hidden" name="GroupingID"  value="' + getNodeVal(node, "GroupingID") + '" />'
            + '</td>';

        row += '<td data-th="' + Lang.Level + '">'
            + '<input type="text" name="Level" id="l_' + cnt + '" value="' + getNodeVal(node, "Level") + '" class="level form-control input-sm" />'
            + '</td>';

        row += '<td data-th="' + Lang.NumDigits + '">'
            + '<input type="text" name="NumDigits" id="l_' + cnt + '" value="' + getNodeVal(node, "NumDigits") + '" class="numdigits form-control input-sm align-center" />'
            + '</td>';

        if ($('#Type').val() == 0 || $('#Type').val() == 1) {
            row += '<td data-th="' + Lang.ToIdentify + '" class="align-center">'
                + '<input type="checkbox" id="tip_' + cnt + '" name="ToIdentify' + cnt + '" '
                + (getNodeVal(node, "ToIdentify", '') == '1' || getNodeVal(node, "ToIdentify", '') == 'True' ? 'checked=checked' : '') + ' />'
                + '</td>';
        }

        row += '<td class="align-center action-remove">'
            + '<a class="btn btn-danger btn-xs btn-block btn-remove remove" id="rp_' + cnt + '">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }
    //-------------------
    // remove rows
    //----------------
    function removeRow($who) {
        var id = $who.attr('id'); // rp_1
        id = id.substring(3); // rp_1  -->  1
        $('#gn_' + id).val("");
        $('#gn_' + id).next().val("");
        $who.parent().parent().hide();
        Growl.Notice(Lang.DeletedSuccessfully);
    }
  

    // -----------------------------
    // startup code
    // ------------------------------

    mID = toInt($('#MasterID').val(), -1);
    setValue('#Type');
    setChecked('#EndCounter');

    if ($('#Type').val() == 0 || $('#Type').val() == 1) {
        $('.tip').show();
    } else {
        $('.tip').hide();
    }

    fillTable("get.aspx?op=GetXml&qd=codingitem&CodingID=" + mID, '#tablecodingItem', addRow);
    function autoPostPack(event, data, formatted) {
        $(event.target).next().val(data[0]);
    }
    autoComplete('Grouping', 'grouprootsauto', "#tablecodingItem .groupingname", "#justfurfun"
       , autoPostPack
       , function (data, i, n, value) {
           return data[1];
       }, null, null,
           'get.aspx?op=Auto&table=Grouping&qd=grouprootstypeauto&GroupingType=' + $('#Type').val() + appendTime());

    $('.addRow').click(function () {
        $('tr:last', '#tablecodingItem').after(addRow(null));
        Growl.Notice(Lang.RowAddedSuccessfully);
        var lastRow = $('tr:last', '#tablecodingItem');
        $('.remove', lastRow).click(function () {
            removeRow($(this));
        });
        // 0:person;1:servic;2:visitor;3:supplier
        autoComplete('Grouping', 'grouprootsauto', "#tablecodingItem .groupingname", "#justfurfun"
           , autoPostPack
           , function (data, i, n, value) {
               return data[1];
           }, null, null, 
           'get.aspx?op=Auto&table=Grouping&qd=grouprootstypeauto&GroupingType=' + $('#Type').val() + appendTime());
    });
    $('.remove').click(function () {
        removeRow($(this));
    });
    insertValidation('#subDetailForm');

}