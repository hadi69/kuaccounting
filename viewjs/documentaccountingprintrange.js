﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
92-10-16: 
- file created
- Date: dateSelect
*/
function view_documentaccountingprintrange() {

    function formatDocumentAccountingResult(data, value) {
        return data[0];// + ' - ' + (data[1] || '<' + Lang.NoComments + '>');
    }

    autoComplete('DocumentAccounting', 'documentaccountingsearch', '#FromNo', '#SFromNo', null, formatDocumentAccounting
        , formatDocumentAccountingResult);
    autoComplete('DocumentAccounting', 'documentaccountingsearch', '#ToNo', '#SToNo', null, formatDocumentAccounting
        , formatDocumentAccountingResult);
    insertValidation();
    $('#ToNo').focus();
    $('#FromNo').focus();

    if ($('#seltype').val() == 'del')
        $('#SelRangePopupTitle').text(Lang.DeleteDocumentRange);
}