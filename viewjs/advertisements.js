﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
92-10-29: 
    - file created
*/
function view_advertisements() {
    var startupID = -1;
    function doCommand(com, grid) {
            generalCommand(com, grid, show);
    }

    function run() {
        var _url = 'service.asmx/GetList?table=Advertisement&qd=advertisement' + appendTime();
        $("#flexi").flexigrid
        (
        {
            url: _url,
            dataType: 'xml',
            colModel:
            [
                { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                { display: Lang.RegisterDate, name: 'RegisterDate', sortable: true, css_class: 'date-cell' },
                { display: Lang.ProvinceName, name: 'ProvinceName', sortable: true, css_class: 'width-80 align-center' },
                { display: Lang.City, name: 'CityName', sortable: true, css_class: 'width-80 align-center' },
                { display: Lang.headline, name: 'Title', sortable: true },
                { display: Lang.Type, name: 'Type', sortable: true, process: displayAdvertisementType, css_class: 'width-80 align-center' },
                { display: Lang.GeneralType, name: 'GeneralType', sortable: true, process: displayAdvertisementGeneralType, css_class: 'width-80 align-center' },
                { display: Lang.ReleaseDate, name: 'ReleaseDate', sortable: true, css_class: 'date-cell'},
                { display: Lang.Status, name: 'Status', sortable: true, process: displayAdvertisementStatus, css_class: 'width-80 align-center' },
                { display: Lang.Price, name: 'Price', hide: true, process: showMonetary, css_class: 'width-100 align-center' },
                { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, hide: hideColumn("sales", "basket", "edit"), css_class: 'action-cell' }
            ],
            buttons:
            [
            { name: Lang.GBNew, bclass: 'new', onpress: doCommand }, // onaccess: hasAccess("sales", "basket", "new") },
            { name: Lang.GBEdit, bclass: 'edit', onpress: doCommand }, // onaccess: hasAccess("sales", "basket", "edit") },
            { name: Lang.GBDel, bclass: 'delete', onpress: doCommand }, // onaccess: hasAccess("sales", "basket", "delete") },
            { separator: true },
            { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
            ],
            searchitems: null,
            sortname: "RegisterDate",
            sortorder: "desc",
            usepager: true,
            useRp: true,
            rp: gridRp,
            showTableToggleBtn: gridToggle,
            width: gridWidth,
            height: gridHeight, resizable: gridResizable,
            singleSelect: false,
            onDoubleClick: function (row, grid, options) {
                var id = row.id.substring(3);
                show(id);
            },
            onSuccess: function (grid) {
                if (startupID != -1) {
                    show(startupID);
                    startupID = -1;
                }
            }
        });
    }
    function displayAdvertisementType(celDiv, id) {
        var type = toInt($(celDiv).html(), 0);
        ShowAdvertisementType(celDiv, type);
    }
    function displayAdvertisementGeneralType(celDiv, id) {
        var type = toInt($(celDiv).html(), 0);
        ShowAdvertisementGeneralType(celDiv, type);
    }
    function displayAdvertisementStatus(celDiv, id) {
        var type = toInt($(celDiv).html(), 0);
        ShowAdvertisementStatus(celDiv, type);
    }

    function show(id, celDiv, successCallback) {
        var btns = {};
        if (id <= 0) {
            btns = getDefaultButtons();
        } else {
            btns[Lang.OK] = 'OK';
           // btns[Lang.soldBtn] = 'Sold';
            //btns[Lang.Void] = 'Voided';
            btns[Lang.AdvertisementImage] = 'Documents';
            btns[Lang.Close] = 'Cancel';
        }
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=Advertisement&qd=advertisement&id=' + id;
        _url += appendTime();
        showPopup(_url, showcallback, btns, null, successCallback, true);
    }
    function showcallback(v, m, f) {
        if (v == 'Print') {
            applyPrintFields('Advertisement');
            printSubform();
            return false;
        }
        if (v == 'Documents') {
            if (_Context.id > 0) {
                var _url = "get.aspx?op=GetHtml&table=AdvertisementImage&ID=" + _Context.id;
                showPopup(_url, null, getCloseButtons());
            }
            return false;
        }
        if (v == 'Sold') {
            ok = runQuery('advertisementstatusupdate', 'Advertisement', '&Status=3&ID=' + $('#MasterID').val());
                if (ok) {
                    Growl.Notice(Lang.Success, 3000);
                } else {
                    Growl.Notice(Lang.Failed, 3000);
                }
            $("#flexi").flexReload();
            return true;
        }
        if (v == 'Voided') {
            ok = runQuery('advertisementstatusupdate', 'Advertisement', '&Status=2&ID=' + $('#MasterID').val());
                if (ok) {
                    Growl.Notice(Lang.Success, 3000);
                } else {
                    Growl.Notice(Lang.Failed, 3000);
                }
            $("#flexi").flexReload();
            return true;
        }
        if (!$('.groupingname').val() && v == 'OK') {
            Growl.Notice(Lang.AddAdvertisementGroupPlease, 4000);
            return false;
        }
        if (v == 'Cancel')
            return true;

        var qry = (_Context.id == -1) ? 'advertisementinsert' : 'advertisementupdate';
        return newMastercallback(v, m, f, 'Advertisement', qry, 'GroupingMember'
            , 'advertisementgroupingupdate', 'advertisementgroupinginsert', 'advertisementgroupingdelete', 'GroupingID', null, show, null, null);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    // checkAccess(['sales,basket']);
    //checkLevel(2);
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.Advertisement, parentTitle: Lang.Management, parentUrl: 'menu/admin.aspx' } }));
    setClassSales();
    dateSelect('.dateSelect');
    startupID = $.urlParam('id');
    if (!startupID)
        startupID = -1;
    $('#chprint').click(function () { choosePrintFields(_Context.table); });
    run();
}