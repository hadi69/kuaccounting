﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-12-11: combined with salesinvoice
    92-10-30: file created
*/
function view_storedraft() {
    makeChosen();
    var storeMode = !hasAccess("links", "mainmenu", "accounting") && !hasAccess("links", "mainmenu", "sales");
    storeMode = storeMode || $.urlParam('menu') == 'storea';
    $("#tableItems").smartTable();
    $("#tablePartners").smartTable();
    if (storeMode) {
        replacePrintButton({
            primary: {
                text: Lang.SimpleFormat,
                href: Router.Generate("print-wftstoredraft", { params: { id: _Context.id }, query: { print: "true" } })
                , type: 6, mode: 3, id: _Context.id
            },
            other: [
                {
                    text: Lang.SimpleFormat2,
                    href: Router.Generate("print-wftstoredraft", { params: { id: _Context.id}, query: { print: "true", report: 'simple2' } })
                   , type: 6, mode: 3, id: _Context.id
                }
            ]
        });
    } else {
        var pagetitle = 'SalesInvoice';
        if (!$('#WorkflowStatus').attr('v')) {
            pagetitle = 'PerSalesInvoice';
        };

        replacePrintButton({
            primary: {
                text: Lang.ForDistribute,
                href: Router.Generate("print-storedraft", { params: { ids: _Context.id, title: pagetitle }, query: { report: 'distribute', print: "true" } })
                , type: 6, mode: 3, id: _Context.id
            },
            other: [
                {
                    text: Lang.ShopFormat,
                    href: Router.Generate("print-storedraft", { params: { ids: _Context.id, title: pagetitle }, query: { print: "true", report: 'catering' } })
                , type: 6, mode: 2, id: _Context.id
                },
                {
                    text: Lang.OfficialFormat,
                    href: Router.Generate("print-storedraft", { params: { ids: _Context.id, title: pagetitle }, query: { print: "true" } })
                , type: 6, mode: 1, id: _Context.id
                },
                {
                    text: Lang.SimpleFormat,
                    href: Router.Generate("print-storedraft", { params: { ids: _Context.id, title: pagetitle }, query: { report: 'simple', print: "true" } })
                    , type: 6, mode: 0, id: _Context.id
                },
                {
                   text: Lang.SimpleFormat2,
                   href: Router.Generate("print-storedraft", { params: { ids: _Context.id, title: pagetitle }, query: { report: 'simple2', print: "true" } })
                   , type: 6, mode: 0, id: _Context.id
                },
                {
                    text: Lang.SimpleFormat3,
                    href: Router.Generate("print-storedraft", { params: { ids: _Context.id, title: pagetitle }, query: { report: 'simple3', print: "true" } })
                    , type: 6, mode: 0, id: _Context.id
                }
            ]
        });
    }

    var cnt = 0, cnt1 = 0, mID = -1, mTreePathWasEmpty = true, DiscountPercent = 0, buyerDiscountPercent = 0, offerDiscount = 0, payAllowDays = 0, offerCash = 0, offercheck1 = 0, offercheck2 = 0, offerbank = 0, offerOther = 0;
    function updateTotal() {
        var sumPrice = 0, sumAmount = 0, sumDiscount = 0, sumTax = 0, sumVat = 0, sumPursant = 0, sumbasePrice = 0;
        $('.total').each(function (index, value) {
            sumPrice += toFloat($(value).val(), 0);
        });
        $('.basepricetotal').each(function (index, value) {
            sumbasePrice += toFloat($(value).val(), 0);
        });
        $('.amount').each(function (index, value) {
            sumAmount += toFloat($(value).val(), 0);
        });
        $('.discount').each(function (index, value) {
            if ($(value).val().indexOf('%') >= 0) {
                var id = $(value).attr('id');
                id = id.substring(id.indexOf('_', 0) + 1); // d_1  -->  1
                var total = toFloat($('#t_' + id).val(), 0); // total
                sumDiscount += toFloat($(value).val().replace('%', ''), 0) * total / 100;
            }
            else
                sumDiscount += toFloat($(value).val(), 0);
        });
        var discount = 0;
        $('.tax').each(function (index, value) {
            if ($(value).val().indexOf('%') >= 0) {
                var id = $(value).attr('id');
                id = id.substring(id.indexOf('_', 0) + 1); // d_1  -->  1
                var total = toFloat($('#t_' + id).val(), 0); // total
                if ($('#d_' + id).val().indexOf('%') >= 0) {
                    discount = toFloat($('#d_' + id).val().replace('%', ''), 0) * total / 100; // discount
                } else {
                    discount = toFloat($('#d_' + id).val(), 0);
                }
                sumTax += toFloat($(value).val().replace('%', ''), 0) * (total - discount) / 100;
            }
            else
                sumTax += toFloat($(value).val(), 0);
        });
        $('.svat').each(function (index, value) {
            if ($(value).val().indexOf('%') >= 0) {
                var id = $(value).attr('id');
                id = id.substring(id.indexOf('_', 0) + 1); // d_1  -->  1
                var total = toFloat($('#t_' + id).val(), 0); // total
                if ($('#d_' + id).val().indexOf('%') >= 0) {
                    discount = toFloat($('#d_' + id).val().replace('%', ''), 0) * total / 100; // discount
                } else {
                    discount = toFloat($('#d_' + id).val(), 0);
                }
                sumVat += toFloat($(value).val().replace('%', ''), 0) * (total - discount) / 100;
            }
            else
                sumVat += toFloat($(value).val(), 0);
        });
        $('.salesmancommission').each(function (index, value) {
            var id = $(value).attr('id');
            id = id.substring(id.indexOf('_', 0) + 1); // d_1  -->  1
            var total = toFloat($('#t_' + id).val(), 0); // total
            sumPursant += toFloat($(value).val(), 0) * total / 100;
        });
        $('#TotalBasePrice').val(sumbasePrice.toFixed(_Context.decimalPoints));
        $('#TotalPrice').val(sumPrice.toFixed(_Context.decimalPoints));
        $('#TotalAmount').val(sumAmount);
        var factorTotalDiscount = $('#FactorTotalDiscount').val();
        if (factorTotalDiscount.indexOf('%') >= 0) {
            factorTotalDiscount = toFloat(factorTotalDiscount.replace('%', ''), 0) * toFloat($('#TotalPrice').val(), 0) / 100; // factortotaldiscount
        }
        factorTotalDiscount = toFloat(factorTotalDiscount, 0);

        var factorTotalPursant = $('#FactorTotalPursant').val();
        if (factorTotalPursant.indexOf('%') >= 0) {
            factorTotalPursant = toFloat(factorTotalPursant.replace('%', ''), 0) * toFloat($('#TotalPrice').val(), 0) / 100; // factortotaldiscount
        }
        factorTotalPursant = toFloat(factorTotalPursant, 0);

        $('#TotalDiscount').val((sumDiscount + toFloat($('#OfferDiscount').val(), 0) + factorTotalDiscount).toFixed(_Context.decimalPoints));
        $('#TotalTax').val(sumTax.toFixed(_Context.decimalPoints));
        $('#VAT').val(sumVat.toFixed(_Context.decimalPoints));
        $('#Pursant').val((sumPursant + factorTotalPursant).toFixed(_Context.decimalPoints));
        updateFormTotal();
        updateSumProfit();
    }
    function updateFormTotal() {
        var vat = toFloat($('#VAT').val(), 0);
        var shipment = toFloat($('#ShipmentPrice').val(), 0);
        if ($('#SellerShipment').attr('checked')) {
            shipment = 0;
            $('.seller-shipment-controls').show();
        }
        else {
            $('.seller-shipment-controls').hide();
        }
        var total = toFloat($('#TotalPrice').val(), 0) - toFloat($('#TotalDiscount').val(), 0) + toFloat($('#TotalTax').val(), 0) + shipment + vat;
        $('#Total').val(total.toFixed(_Context.decimalPoints));
        autoSumOnComplexPayMethod();
        calcofferPrice();
        updateSumProfit();
        commaInputsSet();
        checkCredit();
        checkVisitorCredit();
    }
    function updateSumProfit() {
        var table = $("#tableItems");
        var profit = 0;
        var discount = toFloat($('#TotalDiscount').val(), 0);
        var pursant = toFloat($('#Pursant').val(), 0);
        var shipmentPrice = toFloat($('#ShipmentPrice').val(), 0);
        $('tr .storeserviceid', table).each(function () {
            var id = $(this).attr('id');
            id = id.substring(4);
            var num = toInt($('#a_' + id).val(), 0) * toInt($('#ucc_' + id).val(), 1);
            profit += ((num * toFloat($('#p_' + id).val(), 0)) - (num * toFloat($('#bp_' + id).val(), 0)));
        });
        profit = profit - (discount + pursant);
        if ($('#SellerShipment').attr('checked')) {
            profit = profit - shipmentPrice;
        }
        $('#SumProfit').val(profit);
        commaInputsSet();
    }
    function checkCredit() {
        $('#creditmsg').html();
        var neuremain = 0;
        if (toInt($('#PayMethod').val(), -1) == 2)
            neuremain = toFloat(fromMonetary($('#Total').val(), $('#FactorDate').val()), 0);
        else if (toInt($('#PayMethod').val(), -1) == 3)
            neuremain = toFloat(fromMonetary($('#OwedPrice').val(), $('#FactorDate').val()), 0);
        var status = getPersonCredit($('#BuyerID').val(), neuremain);
        if (status == -1) {
            $('#creditmsg').html(Lang.LowCredit);//checkCredit
            return false;
        }
        return true;
    }
    function checkVisitorCredit() {
        var neuremain = 0;
        if (toInt($('#PayMethod').val(), -1) == 2)
            neuremain = toFloat(fromMonetary($('#Total').val(), $('#FactorDate').val()), 0);
        else if (toInt($('#PayMethod').val(), -1) == 3)
            neuremain = toFloat(fromMonetary($('#OwedPrice').val(), $('#FactorDate').val()), 0);
        var status = getVisitorCredit($('#MasterID').val(), neuremain);
        if (status == -1)
            $('#visitorcreditmsg').html(Lang.VisitorLowCredit);//checkVisitorCredit
    }
    function updatePrice(was) {
        var id = $(was).attr('id');
        id = id.substring(id.indexOf('_', 0) + 1); // d_1  -->  1
        var price = toFloat($('#p_' + id).val(), 0);
        var amount = toFloat($('#a_' + id).val(), 0);
        var unitCarrency = toFloat($('#ucc_' + id).val(), 1);
        var basePrice = toFloat($('#bp_' + id).val(), 0);
        $('#bpt_' + id).val((basePrice * amount * unitCarrency).toFixed(_Context.decimalPoints)); // buypricetotal
        $('#t_' + id).val((price * amount * unitCarrency).toFixed(_Context.decimalPoints)); // total
        updateTotal();
    }
    function checkAvail(was) {
        var id = $(was).attr('id');
        id = id.substring(id.indexOf('_', 0) + 1); // d_1  -->  1
        if ($('#isv_' + id).val() == 0 || $('#isv_' + id).val() == 'False') { // don't check availablity for IsVirtual service
            var amount = toFloat($('#a_' + id).val(), 0);
            var unitCarrency = toFloat($('#ucc_' + id).val(), 1);
            if ($('#avial_' + id).val() != '') {
                var avail = toFloat($('#avial_' + id).val(), 1);
                if ((avail < (amount * unitCarrency)) && serviceRemainMsg == 1) {
                    if (avail < 0) {
                        avail = 0;
                    }
                    alert(Lang.NoEnoughSupply + '     ' + Lang.ServiceAllowedAmount + avail);
                    if (insertStoreDraftWithNegitiveServiceRemain != 1) {
                        $('#a_' + id).val(0);
                        $('#se_' + id).val('');
                        $('#sid_' + id).val('');
                        $.ionSound.play("bell_ring");
                        //$('#a_' + id).blur();
                        updatePrice(was);
                        setTimeout(function () {
                            $('#se_' + id).focus();
                        }, 20);
                        return -1;
                    }
                }
            }
        }
        return 1;
        // colorRows();
    }
    function autoPostPack1(event, data, formatted) {
        $(event.target).next().val(data[0]);
    }

    function accessAutoCompletes() {
        //autoComplete('Stores', 'storeauto', "#StoreName", "#StoreID", setServiceAutoComplete);
        autoCompleteNew({
            table: 'Stores', qd: 'storeauto', textInput: "#StoreName", hiddenInput: "#StoreID"
            , postback: setServiceAutoComplete, allowNull: true
        });
        if (mID == -1) {
            if (!hasValuePositive($("#StoreID").val()))
                setDefault('Stores', 'firststore', "#StoreName", "#StoreID");
        } else {
            if (!hasValuePositive($("#StoreID").val())) {
                $("#StoreID").val(0);
                $("#StoreName").val(Lang.Null);
            }
        }
        if (!hasGeneralAccess("selectstore", generalSettings)) {
            $('#StoreName').attr('readonly', 'readonly');
        }
    }

    function addRow(node) {
        var settingTax = "", settingVat = "", row = '<tr>';
        if (node == null) {
            settingTax = getSettingTax();
            settingVat = getSettingVat();
        }
        if (node != null && getNodeVal(node, "StoreServiceID") == '') {
            offerDiscount += toFloat(getNodeVal(node, "OfferDiscount", 0), 0);
            payAllowDays += toInt(getNodeVal(node, "PayAllowedDays", 0), 0);
            offerCash += toFloat(getNodeVal(node, "OfferCashPrice", 0), 0);
            offercheck1 += toFloat(getNodeVal(node, "OfferCheck1Price", 0), 0);
            offercheck2 += toFloat(getNodeVal(node, "OfferCheck2Price", 0), 0);
            offerbank += toFloat(getNodeVal(node, "OfferBankPrice", 0), 0);
            return
        }
        else if (node != null && getNodeVal(node, "StoreServiceID") != '' && getNodeVal(node, "Source") == 1) {
            row = '<tr  style="background-color:orange">';
            //alert(getNodeVal(node, "Source"));
            // settingTax = 0;
            //settingVat = 0;
            //DiscountPercent = 0;
        }
        else if (node != null && getNodeVal(node, "Amount") < 0 && getNodeVal(node, "Source") != 1) {
            row = '<tr  style="background-color:red">';
        }

        row += '<td class="align-center width-40 row-number" data-th="' + Lang.ColRowNr + '"><a id="l_' + cnt + '" class="link" href="#" title="' + Lang.Portfolios + '">' + (cnt + 1) + '</a></td>'; // Row

        row += '<td class="align-center width-350 " data-th="' + Lang.CommodityName + '">' // CommodityName
        + '<input type="hidden" id=sdi_' + cnt + ' name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
        + '<input type="text" id="se_' + cnt + '" title="' + getNodeVal(node, "StoreName") + '" name="StoreServiceName" value="' + getNodeVal(node, "StoreServiceName") + '" class="storeservicename full-width form-control input-sm" />'
        + '<input type="hidden" id="sid_' + cnt + '"  name="StoreServiceID" value="' + getNodeVal(node, "StoreServiceID") + '" class="storeserviceid" />'
        + '<input type="hidden" id="varid_' + cnt + '" name="ServiceVariateID" value="' + getNodeVal(node, "ServiceVariateID", 0) + '" class="ServiceVariateID" />'
        + '<input type="hidden" id="avial_' + cnt + '" name="Avail" value="' + getNodeVal(node, "Avail", 0) + '" class="Avail" /></td>';

        row += '<td class="align-center width-80" data-th="' + Lang.Units + '">' // Units
        + '<input type="text" id="u_' + cnt + '" name="UnitName" value="' + getNodeVal(node, "UnitName") + '" class="unitname form-control input-sm" />'
        + '<input type="hidden" name="UnitID" value="' + getNodeVal(node, "UnitID") + '" />'
        + '</td>';

        row += '<td class="align-center width-80 saletypetd" data-th="' + Lang.SaleType + '">' // SaleType
        + '<input type="text" id="stn_' + cnt + '" name="SaleTypeName" value="' + getNodeVal(node, "SaleTypeName") + '" class="saletype form-control input-sm ac_input" />'
        + '<input type="hidden" id="stid_' + cnt + '" name="SaleTypeID" value="' + getNodeVal(node, "SaleTypeID") + '" class="saletypeid" />'
        + '<input type="hidden" id="stt_' + cnt + '" name="SaleTypeType" value="" class="saletypetype" />'
        + '<input type="hidden" id="isD_' + cnt + '" name="IsDefault" value="" class="isdefault" />'
        + '</td>';

        row += '<td class="align-center width-170" data-th="' + Lang.NumOrValue + '">' // NumOrValue
        + '<div class="input-group input-group-sm">'
        + '<input type="text" id="a_' + cnt + '" name="DeliveredAmount" value="' + getNodeVal(node, "DeliveredAmount", 0) + '" class="amount form-control width-100-percent min-width-50 align-center" />'
        + '<span class="input-group-addon no-left-border no-right-border">x</span>'
        + '<input readonly="readonly" size="1" id="ucc_' + cnt + '" name="UnitCurrencyConverter" value="' + getNodeVal(node, "UnitCurrencyConverter", 1) + '" class="form-control width-50 align-center" />'
        + '<input type="hidden" id="isv_' + cnt + '" name="IsVirtual" value="' + getNodeVal(node, "IsVirtual", 0) + '" class="isvirtual" />'
        + '</div>'
        + '</td>';

        row += '<td class="acc width-200" data-th="' + Lang.Price + '">' // Price
        + '<input type="text" id="p_' + cnt + '" name="Price" value="' + getNodeVal(node, "Price", 0) + '" class="price monetary validate[custom[integer]] form-control align-center input-sm" />'
        + '<input type="hidden" id="ar_' + cnt + '" name="Items" value="" class="narrow items" />'
        + '<input type="hidden" name="BasePrice" id="bp_' + cnt + '" value="' + getNodeVal(node, "BasePrice") + '" class="baseprice" />'
        + '<input type="hidden" name="Profit" id="prf_' + cnt + '" value="' + getNodeVal(node, "Profit") + '" />'
        + '</td>';

        row += '<td class="acc width-80" data-th="' + Lang.SalesManCommission + '">' // SalesManComission
        + '<div class="input-group input-group-sm">'
        + '<input type="text" id="smc_' + cnt + '" name="SalesManCommission" value="' + getNodeVal(node, "SalesManCommission", 0) + '" class=" salesmancommission form-control align-center" readonly="readonly" />'
        + '<span class="input-group-addon">%</span>'
        + '</span>'
        + '</td>';

        row += '<td class="acc width-120" data-th="' + Lang.TotalPrice + '">' // TotalPrice
        + '<input type="text" id="t_' + cnt + '" name="Total" value="' + (getNodeVal(node, "Price", 0) * getNodeVal(node, "DeliveredAmount", 0)).toFixed(_Context.decimalPoints) * getNodeVal(node, "UnitCurrencyConverter", 1) + '" class="total monetary validate[custom[integer]] form-control input-sm align-center" readonly="readonly" />'
        + '<input type="text" id="bpt_' + cnt + '" name="BasePriceTotal" value="' + (getNodeVal(node, "BasePrice", 0) * getNodeVal(node, "DeliveredAmount", 0)).toFixed(_Context.decimalPoints) * getNodeVal(node, "UnitCurrencyConverter", 1) + '" class="basepricetotal monetary validate[custom[integer]] form-control hidden input-sm align-center"/>'
        + '</td>';

        row += '<td class="acc width-70" data-th="' + Lang.Discount + '">' // Discount
        + '<input type="text" id="d_' + cnt + '" name="Discount" value="' + getNodeVal(node, "Discount", DiscountPercent) + '" class="discount percent monetary validate[custom[integer]] form-control input-sm align-center" />'
        + '</td>';

        row += '<td class="acc width-70" data-th="' + Lang.Tax + '">' // Tax
        + '<input type="text" id="tax_' + cnt + '" name="Tax" value="' + getNodeVal(node, "Tax", settingTax) + '" class="tax percent autopercentsign monetary validate[custom[integer]] form-control input-sm align-center" />'
        + '</td>';

        row += '<td class="acc width-70" data-th="' + Lang.VAT + '">' // VAT
        + '<input type="text" id="sv_' + cnt + '" name="SingleVAT" value="' + getNodeVal(node, "SingleVAT", settingVat) + '" class="svat monetary percent autopercentsign validate[custom[integer]] form-control input-sm align-center" />'
        + '<input type="hidden" class="source" id="s_' + cnt + '" name="Source" value="' + getNodeVal(node, "Source", 0) + '" />'
        + '</td>';

        row += '<td data-th="' + Lang.label + '" class="acc labeltd width-100">'
        + '<input type="text" id="label_' + cnt + '" name="Label" value="'
        + getNodeVal(node, "Label") + '" class=" acc labell form-control input-sm" />'
        + '<input type="hidden" name="LabelID" value="' + getNodeVal(node, "LabelID") + '" class="labelid" />';

        row += '<td class="align-center width-40 action-remove">'
        + '<a class="btn btn-danger btn-xs btn-block btn-remove remove" id="r_' + cnt + '">'
        + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
        + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
        + '</a>'
        + '</td>';
        row += '</tr>';
        cnt++;
        return row;
    }
    function addPartner(node) {
        var row = '<tr>';
        row += '<td class="align-center row-number" data-th="' + Lang.ColRowNr + '">' + (cnt1 + 1) + '</td>';

        row += '<td data-th="' + Lang.PartnerName + '">'
        + '<input type="hidden" id=cti_' + cnt1 + ' name="ID2" value="' + getNodeVal(node, "ID", -1) + '" />'
        + '<input type="text" id="pn_' + cnt1 + '" name="PartnerName" value="' + getNodeVal(node, "PartnerName") + '" class="partnername form-control input-sm" />'
        + '<input type="hidden"  name="PartnerID" value="' + getNodeVal(node, "PartnerID") + '" />'
        + '</td>';

        row += '<td class="align-center action-remove">'
        + '<a class="btn btn-danger btn-xs btn-block btn-remove removepartner" id="rp_' + cnt1 + '">'
        + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
        + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
        + '</a>'
        + '</td>';

        row += '</tr>';
        cnt1++;
        return row;
    }

    function PayMethodChange(foreChangeDefault) {
        $('#CheckName').removeClass('validate[required]');
        var method = toInt($('#PayMethod').val(), SalesInvoicePayMethodEnum.Cash);
        if (method != SalesInvoicePayMethodEnum.Check && method != SalesInvoicePayMethodEnum.Check2 && method != SalesInvoicePayMethodEnum.Complex) {
            $('.newCheck').hide();
        }
        else {
            $('.newCheck').show();
        }
        if (method != SalesInvoicePayMethodEnum.Other && method != SalesInvoicePayMethodEnum.Complex) {
            $('.paydate').hide();
        }
        else {
            $('.paydate').show();
        }

        $('.sendToCardDiv').hide();
        if (method == SalesInvoicePayMethodEnum.Bank || method == SalesInvoicePayMethodEnum.Complex) {
            $('.sendToCardDiv').show();
        }//
        if (method == SalesInvoicePayMethodEnum.Complex) {
            $('#CheckPrice').blur(function () {
                if (toFloat($(this).val()) > 0) {
                    $('#CheckName').addClass('validate[required]');
                } else {
                    $('#CheckName').removeClass('validate[required]');
                }
            });
            //$('.newCheck').insertAfter('#CheckSel');
            $('.nocomplex').hide();
            $('.complex').show();
        }
        else {
            $('.nocomplex').show();
            $('.complex').hide();
        }

        if (!$('#OweTreePath').val() || foreChangeDefault) {
            // var method = $('#PayMethod').val();
            if (method == SalesInvoicePayMethodEnum.Cash)
                setDefaultByTitleTree('TreePath', 'settingtreesauto', '#OweTopicCode', '#OweTopicTitle', '#OweTreePath', 'FundTreePath');
            else if (method == SalesInvoicePayMethodEnum.Check || method == SalesInvoicePayMethodEnum.Check2) {
                $('#CheckName').removeClass('validate[required]');
                $('#CheckName').addClass('validate[required]');
                setDefaultByTitleTree('TreePath', 'settingtreesauto', '#OweTopicCode', '#OweTopicTitle', '#OweTreePath', 'CheckTreePath');
            }
            else if (method == SalesInvoicePayMethodEnum.Other)
                setDefaultByTitleTree('TreePath', 'settingtreesauto', '#OweTopicCode', '#OweTopicTitle', '#OweTreePath', 'OweTreePath');
            else if (method == SalesInvoicePayMethodEnum.Bank) {
                $('#OweTopicCode, #OweTopicTitle, #OweTreePath').val('');
                setDefaultByTitleTree('TreePath', 'defaulttreesbanksauto', '#OweTopicCode', '#OweTopicTitle', '#OweTreePath', 'OweTreePath');
            }
        }

        if (method == SalesInvoicePayMethodEnum.Complex) {
            if (!$('#CashTreePath').val() || foreChangeDefault)
                setDefaultByTitleTree('TreePath', 'settingtreesauto', '#CashTopicCode', '#CashTopicTitle', '#CashTreePath', 'FundTreePath');
            if (!$('#CheckTreePath').val() || foreChangeDefault)
                setDefaultByTitleTree('TreePath', 'settingtreesauto', '#CheckTopicCode', '#CheckTopicTitle', '#CheckTreePath', 'CheckTreePath');
            if (!$('#OwedTreePath').val() || foreChangeDefault)
                setDefaultByTitleTree('TreePath', 'settingtreesauto', '#OwedTopicCode', '#OwedTopicTitle', '#OwedTreePath', 'OweTreePath');
            if (!$('#BankTreePath').val() || foreChangeDefault)
                setDefaultByTitleTree('TreePath', 'defaulttreesbanksauto', '#BankTopicCode', '#BankTopicTitle', '#BankTreePath', 'BankTreePath');
            $('#CashPrice, #BankPrice, #CheckPrice, #OwedPrice').blur(function () {
                autoSumOnComplexPayMethod($(this));
            });
            autoSumOnComplexPayMethod();
        }
        if (!$('#DueTreePath').val() || foreChangeDefault) {
            setDefaultByTitleTree('TreePath', 'settingtreesauto', '#DueTopicCode', '#DueTopicTitle', '#DueTreePath', 'SaleTreePath');
        }
    }

    function autoSumOnComplexPayMethod(target) {
        if (toInt($('#PayMethod').val(), -1) == 3) {
            var fPrice = toFloat($('#Total').val(), 0);
            var cPrice = toFloat($('#CashPrice').val(), 0);
            var bPrice = toFloat($('#BankPrice').val(), 0);
            var chPrice = toFloat($('#CheckPrice').val(), 0);
            var oPrice = fPrice - (cPrice + bPrice + chPrice);
            if (oPrice >= 0) {
                $('#OwedPrice').val(numberWithCommas(oPrice));
            } else {
                if (target) {
                    Growl.Notice(Lang.autoSumOnComplexPayMethodMsg, 4000);
                    target.val(0);
                }
            }
        }
    }

    function checkAuto() {
        var more = '';
        var buyerID = $('#BuyerID').val();
        if (hasValuePositive(buyerID))
            more = "PersonnelID=" + buyerID;
        $('#CheckName').unautocomplete();
        autoCompleteFree('Checks', 'checksauto', '#CheckName', '#CheckID', null, null, null, more);
    }

    function setRequireds() {
        if (toFloat($('#CashPrice').val(), 0) > 0)
            addRequired('#CashTreePathName');
        else
            removeRequired('#CashTreePathName');
        if (toFloat($('#CheckPrice').val(), 0) > 0)
            addRequired('#CheckTreePathName');
        else
            removeRequired('#CheckTreePathName');
        if (toFloat($('#OwedPrice').val(), 0) > 0)
            addRequired('#OwedTreePathName');
        else
            removeRequired('#OwedTreePathName');
    }

    function bindSaleType() {
        $('.saletype').unautocomplete();
        var autourl = '';
        if (saleType == 2) {
            autourl = 'get.aspx?op=Auto&table=SaleType&qd=saletypebasebuyerauto&BuyerID=' + $('#BuyerID').val() + appendTime();
        }
        autoComplete('SaleType', 'saletypeauto', "#tableItems .saletype", "#justfurfun"
            , function (event, data, formatted) {
                var id = $(event.target).attr('id');
                id = id.substring(id.indexOf('_', 0) + 1); // stn_1 ==> 1
                $(event.target).next().val(data[0]); // ID
                $(event.target).next().next().val(data[3]); // saletypetype
                $(event.target).next().next().next().val(data[2]); // saletypetype
                var selectedPrice = getServerValue('getselectedprice', 'SaleTypeItem', '&SaleTypeID=' + data[0] + '&StoreServiceID=' + $('#sid_' + id).val() + '&FactorDate=' + $('#FactorDate').val());
                if ((toInt(data[2], 0) == 1) || (toInt(selectedPrice, 0) == 0 || !selectedPrice)) {
                    selectedPrice = getServerValue('getdefaultprice', 'ServicePrice', '&StoreServiceID=' + $('#sid_' + id).val());
                }
                if (saleType == 1 || saleType == 2) {
                    if (selectedPrice != '0.00' && selectedPrice) {
                        $('#p_' + id).val(numberWithCommas((toFloat(selectedPrice, 0.0)).toFixed(_Context.decimalPoints))); // price
                    }
                }
                // if (data[4] && data[4] > 0) {
                calcRowDiscountPercent(id);
                //}
                updatePrice('#p_' + id);
                //checkAvail('#p_' + id);
            }
            , null, null, null, autourl);
    }

    $('#NewPerson').click(function () {
        var oldID = _Context.id;
        function showcallback(v, m, f) {
            if (v == 'OK') {
                generalOKScalar(v, m, f, 'personupdate', 'personinsert', 'Personnel', '#subDetailForm');
                if (!_Context.success)
                    return false;
                $('#BuyerName').val(f.Name + ' ' + f.Surname);
                $('#BuyerID').val(_Context.resultID);
            }
            _Context.id = oldID;
        }
        _Context.id = -1;
        var _url = 'get.aspx?op=GetHtml&table=Personnel&qd=paycheck&id=-1&view=person' + appendTime();
        showPopup(_url, showcallback, getButtons());
    });

    $('#FactorProfit').click(function () {
        $('#SumProfit').show();
    });

    function storedraftTypeChange(notChangeDefault) {
        var storeDraftType = toInt($('#StoreDraftType').val(), 0);
        if (storeDraftType == 0 || storeDraftType == 3 || storeDraftType == 4) {
            $('.notforinternal').show();
            $('.notguildtrust').show();
            $('.IsInternal').hide();
            if (notChangeDefault) {
                PayMethodChange(false); 
            }
            else {
                PayMethodChange(true);
            }
        }
        else if (storeDraftType == 1) {
            $('.IsInternal').show();
            $('.notguildtrust').show();
            $('.notforinternal').hide();
            $('#BuyerBalanceDiv').hide();
            if (notChangeDefault) {
                PayMethodChange(false);
            }
            else {
                PayMethodChange(true);
            }
        }
        else if (storeDraftType == 2) {
            $('.IsInternal').hide();
            $('.notforinternal').show();
            $('.notguildtrust').hide();
        }
        if (storeMode) {
            $('.acc').hide();
        }
    }

    function labelCheckBox() {
        $('#LabelCheckBox').click(function () {
            if ($(this).attr('checked')) {
                $('.labeltd').show();
            } else {
                $('.labeltd').hide();
            }
        });

        if ($('#LabelCheckBox').attr('checked')) {
            $('.labeltd').show();
        } else {
            $('.labeltd').hide();
        }
    }

    function saleTypeShow() {
        if (saleType == 0) {
            $('.saletypetd').hide();
        } else {
            $('.saletypetd').show();
        }
    }

    function calcRowDiscountPercent(rowId) {
        var percent = 0;
        $('#d_' + rowId).val(0);
        // calculate saletype discountpercent
        var saleType = toInt($('#stid_' + rowId).val(), -1);
        var storeServiceID = toInt($('#sid_' + rowId).val(), -1);
        var buyerID = toInt($('#BuyerID').val(), -1);
        if (saleType > 0) {
            var saletypeDiscountPercent = getServerValue('getsaletypeDiscountPercent', 'SaleType', 'SaleTypeID=' + saleType + '&StoreServiceID=' + storeServiceID + '&BuyerID=' + buyerID);
            saletypeDiscountPercent = toFloat(saletypeDiscountPercent, 0);
            if (saletypeDiscountPercent > 0) {
                percent += saletypeDiscountPercent;
            }
        }


        // calculate buyer discountpercent
        if (buyerDiscountPercent > 0) {
            percent += buyerDiscountPercent;
        }

        // calculate service discountpercent
        var storeserviceId = $('#sid_' + rowId).val();
        if (storeserviceId > 0) {
            var serviceDiscountPercent = getServerValue('getserviceDiscountPercent', 'SaleType', 'StoreServiceID=' + storeserviceId);
            serviceDiscountPercent = toFloat(serviceDiscountPercent, 0);
            if (serviceDiscountPercent < 0) {
                percent += (-1 * serviceDiscountPercent);
            } else {
                if (percent <= 0) {
                    $('#d_' + rowId).val(serviceDiscountPercent.toFixed(2).replace('.00', ''));
                }
            }
        }
        if (percent > 0)
            $('#d_' + rowId).val(percent.toFixed(2).replace('.00', '') + '%');
    }

    function calcofferPrice() {
        if (saleType == 1 || saleType == 2) {
            var oldSaleTypeType = -1;
            var diferent = false;
            offerCash = 0, offerbank = 0, offercheck1 = 0, offercheck2 = 0, offerOther = 0;
            $('.saletypetype').each(function (index, value) {
                var rowid = $(value).attr('id');
                rowid = rowid.substring(rowid.indexOf('_', 0) + 1); // d_1  -->  1
                var discount = 0;
                var tax = 0;
                var vat = 0;

                var total = toFloat($('#t_' + rowid).val(), 0); // total

                if ($('#d_' + rowid).val().indexOf('%') >= 0) {
                    discount = toFloat($('#d_' + rowid).val().replace('%', ''), 0) * total / 100;
                } else {
                    discount = toFloat($('#d_' + rowid).val(), 0);
                }

                if ($('#tax_' + rowid).val().indexOf('%') >= 0) {
                    tax = toFloat($('#tax_' + rowid).val().replace('%', ''), 0) * (total - discount) / 100;
                }
                else {
                    tax = toFloat($('#tax_' + rowid).val(), 0);
                }

                if ($('#sv_' + rowid).val().indexOf('%') >= 0) {
                    vat = toFloat($('#sv_' + rowid).val().replace('%', ''), 0) * (total - discount) / 100;
                }
                else {
                    vat = toFloat($('#sv_' + rowid).val(), 0);
                }

                var saleTypeType = toInt($(this).val(), -1);
                var def = toInt($(this).next().val(), 0);
                if (index == 0) {
                    oldSaleTypeType = saleTypeType;
                }
                if (saleTypeType == 0 && def == 0) { // cash
                    offerCash += (total + tax + vat) - discount;
                } else if (saleTypeType == 4 && def == 0) { // bank
                    offerbank += (total + tax + vat) - discount;
                } else if (saleTypeType == 1 && def == 0) { // check1
                    offercheck1 += (total + tax + vat) - discount;
                } else if (saleTypeType == 5 && def == 0) { // check2
                    offercheck2 += (total + tax + vat) - discount;
                } else {
                    offerOther += (total + tax + vat) - discount;
                }
                if (oldSaleTypeType != saleTypeType) {
                    diferent = true;
                }
                oldSaleTypeType = saleTypeType;
            });
            if (diferent) {
                $('#PayMethod').val('3');
                setValue('#PayMethod');
                PayMethodChange(true);
                if (!$('#SellerShipment').attr('checked')) {
                    offerOther += toFloat($('#ShipmentPrice').val(), 0);
                }
                $('#CashPrice').val(numberWithCommas((toFloat(offerCash, 0.0)).toFixed(_Context.decimalPoints)));
                $('#CheckPrice').val(numberWithCommas((toFloat(offercheck1 + offercheck2, 0.0)).toFixed(_Context.decimalPoints)));
                $('#OwedPrice').val(numberWithCommas((toFloat(offerOther, 0.0)).toFixed(_Context.decimalPoints)));
                $('#BankPrice').val(numberWithCommas((toFloat(offerbank, 0.0)).toFixed(_Context.decimalPoints)));
            }
        }
    }

    function calcofferPriceOnPaymethodChange(defaultUlr) {
        if (saleType == 1 || saleType == 2) {
            var pMethod = toInt($('#PayMethod').val(), -1);
            if (pMethod == 3) {
                return false; // we don't have saletype for PayMethodComplex
            }
            var _autourl = 'get.aspx?op=Auto&table=SaleType&qd=selectedsaletype&Type=' + pMethod + appendTime();
            if (saleType == 2) {
                _autourl = 'get.aspx?op=Auto&table=SaleType&qd=selectedsaletypebaseonbuyer&Type=' + pMethod + '&BuyerID=' + $('#BuyerID').val() + appendTime();
            }
            if (defaultUlr) {
                _autourl = defaultUlr;
            }
            var saleTypeData = getServerValues(_autourl);
            if (saleTypeData) {
                $('.storeserviceid').each(function (index, value) {
                    var rowid = $(value).attr('id');
                    rowid = rowid.substring(rowid.indexOf('_', 0) + 1); // d_1  -->  1
                    var selectedPrice = getServerValue('getselectedprice', 'SaleTypeItem', '&SaleTypeID=' + saleTypeData[0] + '&StoreServiceID=' + $(value).val() + '&FactorDate=' + $('#FactorDate').val());
                    if ((toInt(saleTypeData[2], 0) == 1) || (toInt(selectedPrice, 0) == 0 || !selectedPrice)) {
                        selectedPrice = getServerValue('getdefaultprice', 'ServicePrice', '&StoreServiceID=' + $(value).val());
                    }
                    if (selectedPrice != '0.00' && selectedPrice)
                        $('#p_' + rowid).val(numberWithCommas((toFloat(selectedPrice, 0.0)).toFixed(_Context.decimalPoints)));

                    $('#stn_' + rowid).val(saleTypeData[1]);
                    $('#stn_' + rowid).next().val(saleTypeData[0]);
                    $('#stt_' + rowid).val(saleTypeData[3]);
                    calcRowDiscountPercent(rowid);
                });
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function commentsList() {
        $('.ac_results').hide();
        var _url = 'get.aspx?op=GetHtml&table=StoreDraft&view=storedraftcommentselect' + appendTime();
        _url += appendTime();
        var btns = {};
        btns[Lang.Cancel] = "_CANCEL_";
        showPopup(_url, function (v, m, f) {
            if (f.SComments != '') {
                $('#Comments').val(f.SComments);

            }
            $('#Comments').focus();
        }, btns);
    }

    function linkClick() {
        $('.link').click(function () {
            if (saleType == 1 || saleType == 2) {
                var id = $(this).attr('id').substring(2, $(this).attr('id').length);
                var storeServiceID = $('#sid_' + id).val();
                var saleTypeID = $('#stid_' + id).val();
                var saleTypeName = $('#stn_' + id).val();
                var serviceName = $('#se_' + id).val();
                var storeDraftItemId = $('#sdi_' + id).val();
                var _url = 'get.aspx?op=GetHtml&table=SaleTypeItem&view=servicepriceinformation&StoreServiceID=' + storeServiceID + '&STypeID=' + saleTypeID + '&ServiceName=' + serviceName + '&SaleTypeName=' + saleTypeName + '&TableID=' + mID + '&StoreDraftItemID=' + storeDraftItemId + appendTime();
                showPopup(_url, function (v, m, f) {
                    return true;
                }, getCloseButtons());
            }
        });
    }
    // ------------------------------
    // startup code
    // ------------------------------

    mID = toInt($('#MasterID').val(), -1);
    var manualNoSerial = toInt(getSettings('ManualNoSerial'), 0);
    if (manualNoSerial <= 0) {
        $('#NOSerial').attr('readonly', 'readonly');
    }
    tabify("#detailForm");
    var serviceRemainMsg = getSettings('ServiceRemainMsg');
    var servicePriceMsg = getSettings('ServicePriceMsg');
    var saleType = toInt(getSettings('SaleTypeShow'), 0);
    var insertStoreDraftWithBuyerOutBalance = toInt(getSettings('InsertStoreDraftWithBuyerOutBalance'), 0);
    var insertStoreDraftWithNegitiveServiceRemain = toInt(getSettings('InsertStoreDraftWithNegitiveServiceRemain'), 0);
    if (saleType == 1 || saleType == 1) {
        $('#LoadDefaultPricing').show();
    }
    var sellerBuyerNameValidtion = toInt(getSettings('SellerBuyerNameValidation'), 0);
    if (sellerBuyerNameValidtion == 1) {
        $("#BuyerName").addClass('validate[required]');
    }

    $('#LoadDefaultPricing').click(function () {
        calcofferPriceOnPaymethodChange('get.aspx?op=Auto&table=SaleType&qd=getdefaultsaletype' + appendTime());
    });

    if (!hasGeneralAccess("storedraftprofit", generalSettings)) {
        $('#FactorProfitDiv').hide();
    }

    if (mID == -1) {
        $("#UserID").val(_Context.userID);
        setDefault('Personnel', 'defaultstoredraftperson', "#BuyerName", "#BuyerID");
    }
    if ($('#OweTopicTitle').val()) {
        mTreePathWasEmpty = false;
    }

    $('#Comments').keyup(function (e) {
        if (e.keyCode === 40) {
            commentsList();
        }
    });

    $("#BuyerName").blur(function () {
        if (storeMode) {
            return;
        }
        var buyerBalance,
            LD = '';
        _url = 'get.aspx?op=Auto&table=Personnel&qd=personneldetailauto&q=' + $('#BuyerID').val();
        detail = getServerValues(_url);
        if (detail && detail[3] > 0) {
            alert(Lang.StoreDraftJsM1 + ' ' + detail[3] + ' ' + Lang.StoreDraftJsM2);
            DiscountPercent = toFloat(detail[3], 0).toFixed(_Context.decimalPoints) + '%';
            buyerDiscountPercent = toFloat(detail[3], 0);
        }
        else
            DiscountPercent = 0;

        $('.storeserviceid').each(function (index, value) {
            var rowid = $(value).attr('id');
            calcRowDiscountPercent(rowid);
        });

        buyerBalance = getServerValue('personaccountbalance', null, '&PersonnelID=' + $('#BuyerID').val());
        buyerBalance = buyerBalance.split('|');
        if (toInt(buyerBalance[0], 0) < 0) {
            buyerBalance[0] = toInt(buyerBalance[0], 0) * -1;
            LD = Lang.Cr;
        } else if (toInt(buyerBalance[0], 0) > 0) {
            LD = Lang.Li;
        }
        $('#BuyerBalance').val(Lang.Remained + ' : ' + numberWithCommas(buyerBalance[0]) + ' ' + LD + ' // ' + Lang.ChecksNotPassed + ' : ' + numberWithCommas(buyerBalance[1]) + ' // ' + Lang.TheLastBuy + ' : ' + buyerBalance[2] + ' // ' + Lang.RefusedCheck + ' : ' + buyerBalance[3]);
        $('#BuyerBalanceDiv').show();
    });

    $("#VisitorName").blur(function () {
        _url = 'get.aspx?op=Auto&table=Checks&qd=visitornotfollowedchecks&VisitorID=' + $('#VisitorID').val();
        var notFollowedChecks = getServerRows(_url);
        var msg = Lang.StoreDraftJsM4;
        for (var i = 0; i < notFollowedChecks.length; i++) {
            if (i == notFollowedChecks.length - 1)
                msg += notFollowedChecks[i];
            else
                msg += notFollowedChecks[i] + ' :: ';
        }
        if ($('#VisitorID').val() && msg != Lang.StoreDraftJsM4)
            alert(msg);
    });
    setValue('#PayMethod');
    setChecked('#SellerShipment');
    setChecked('#CommentByBuyerName');
    setChecked('#AddToCommentList');
    setValue("#WorkflowStatus");
    setValue("#PayStatus");
    setValue('#StoreDraftType');
    setChecked('#LabelCheckBox');
    setChecked('#NoPrintFactorDate');


    if ($('#WorkflowStatus').val() == 1)
        $('.DesiredDocNo').hide();

    $('#SellerShipment').click(updateFormTotal);

    if ($("#TotalAmount").val() == '')
        $("#TotalAmount").val(0);
    if ($("#TotalDiscount").val() == '')
        $("#TotalDiscount").val(0);
    if ($("#TotalPrice").val() == '')
        $("#TotalPrice").val(0);
    if ($("#VAT").val() == '')
        $("#VAT").val(0);
    if ($("#ShipmentPrice").val() == '')
        $("#ShipmentPrice").val(0);
    if ($("#TotalTax").val() == '')
        $("#TotalTax").val(0);
    if ($("#SaleDate").val() == '')
        $("#SaleDate").val(_Context.today);


    dateSelect('.dateSelect');

    accessAutoCompletes();

    autoCompleteTreePath2('#OweTopicCode', '#OweTopicTitle', '#OweTreePath', '#OweSel');
    autoCompleteTreePath2('#DueTopicCode', '#DueTopicTitle', '#DueTreePath', '#DueSel');
    //autoCompleteTreePath('#DueTreePathName', '#DueTreePath', '#DueSel');
    autoCompleteTreePath2('#CashTopicCode', '#CashTopicTitle', '#CashTreePath', '#CashSel');
    autoCompleteTreePath2('#CheckTopicCode', '#CheckTopicTitle', '#CheckTreePath', '#CheckSel');
    autoCompleteTreePath2('#OwedTopicCode', '#OwedTopicTitle', '#OwedTreePath', '#OwedSel');


    autoComplete('Trees', 'treestypedauto', '#BankTopicTitle', null, function (event, data, formatted) {
        // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due, Nature, TopicTitle, ProgramCode, EquipmentNeed, CanHaveSubTree
        var hasChild = toBool(data[5]), canHaveSubTree = toBool(data[12]);
        if (hasChild || canHaveSubTree) {
            $('#BankTopicCode').val('');
            $('#BankTopicTitle').val('');
            $('#BankTreePath').val('');
            if (hasChild)
                alert(Lang.HasChild);
            else
                alert(Lang.HasSubTreeBut);
        }
        else {
            $('#BankTopicCode').val(data[4]);
            $('#BankTopicTitle').val(data[9]);
            $('#BankTopicTitle').attr('title', data[1]);
            $('#BankTreePath').val(data[0]);
        }
    }, formatTreePath, function (data) { return data[9]; }, null, 'get.aspx?op=Auto&table=Trees&qd=treestypedauto&Type=0' + appendTime());


    autoComplete('Trees', 'treestypedauto', '#BankTopicCode', null, function (event, data, formatted) {
        // TreePath, Titles, PersonNeed, ProjectNeed, TopicCode, Child, Owe, Due, Nature, TopicTitle, ProgramCode, EquipmentNeed, CanHaveSubTree
        var hasChild = toBool(data[5]), canHaveSubTree = toBool(data[12]);
        if (hasChild || canHaveSubTree) {
            $('#BankTopicCode').val('');
            $('#BankTopicTitle').val('');
            $('#BankTreePath').val('');
            if (hasChild)
                alert(Lang.HasChild);
            else
                alert(Lang.HasSubTreeBut);
        }
        else {
            $('#BankTopicCode').val(data[4]);
            $('#BankTopicTitle').val(data[9]);
            $('#BankTopicTitle').attr('title', data[1]);
            $('#BankTreePath').val(data[0]);
        }
    }, formatTreePath, function (data) { return data[4]; }, null, 'get.aspx?op=Auto&table=Trees&qd=treestypedauto&Type=0' + appendTime());

    //autoCompleteTreePath2('#BankTopicCode', '#BankTopicTitle', '#BankTreePath', '#BankSel');
    autoCompleteTreePath2('#ShipmentTopicCode', '#ShipmentTopicTitle', '#ShipmentTreePath', '#ShipmentSel');

    if (mID == -1)
        setDefaultByTitleTree('TreePath', 'settingtreesnochildauto', '#DueTopicCode', '#DueTopicTitle', '#DueTreePath', 'SaleTreePath');

    //setDefault('StoreDraft', 'laststoredraftdue', "#DueTreePathName", "#DueTreePath");

    autoCompleteFree('Personnel', 'personnelselectauto', '#BuyerName', '#BuyerID', null
            , function (data, i, n, value) {
                //$('#BuyerID').val('');
                return formatPersonnelItem(data);
            });
    if (!storeMode) {
        showPersonBook('#BuyerID', true, true);
    }

    autoCompleteFree('Personnel', 'visitorauto', '#VisitorName', '#VisitorID', null
            , function (data, i, n, value) {
                //$('#VisitorID').val('');
                return formatPersonnelItem(data);
            });
    //checkAuto();

    autoComplete('Personnel', 'personnelauto', '#PersonnelName', '#PersonnelID');
    autoComplete('Ward', 'companyunitchildsauto', '#WardName', '#WardID');
    autoComplete('Personnel', 'personnelauto', '#AccepterName', '#AccepterID');
    autoComplete('Purchase', 'purchaseauto', '#PurchaseName', '#PurchaseID');

    if (storeMode) {
        $('.popupTitle').html(_Context.title);
        $('.acc').hide();
        if ($("#Date").val() == '')
            $("#Date").val(_Context.today);
        if ($("#storekeepername").val() == '') {
            $("#storekeeperid").val(_context.personnelid);
            $("#storekeepername").val(_context.personnelname);
        }
    }
    else {
        $('.popupTitle').html(Lang.SalesInvoice);
        if ($("#FactorDate").val() == '')
            $("#FactorDate").val(_Context.today);
        if ($("#AccountantName").val() == '') {
            $("#AccountantID").val(_Context.personnelID);
            $("#AccountantName").val(_Context.personnelName);
        }
    }


    // DetailForm
    $('.print').hide();
    fillTable("get.aspx?op=GetXml&qd=storedraftitem&StoreDraftID=" + mID, '#tableItems', addRow);
    $('#StoreDraftType').change(storedraftTypeChange);
    storedraftTypeChange(true);
    $('#PayMethod').chosen().change(function () {
        var ok = calcofferPriceOnPaymethodChange(null);
        PayMethodChange(true);
        if (ok) {
            $('.price').blur();
        }

    });
    PayMethodChange(false);
    labelCheckBox();
    saleTypeShow();
    autoPercentSign();
    // colorRows();
    fillTable("get.aspx?op=GetXml&qd=storedraftperson&StoreDraftID=" + mID, '#tablePartners', addPartner);
    $('#tableItems .storeservicename').each(function () {
        var id = $(this).attr('id');
        id = id.substring(id.indexOf('_') + 1);
        var unitName = $('#u_' + id);
        var storeServiceId = $('#sid_' + id).val();
        if (storeServiceId > 0) {
            var _autourl1 = 'get.aspx?op=Auto&table=Units&qd=storedraftunitsauto&StoreServiceID=' + storeServiceId;
            autoComplete('Units', 'unitsauto', unitName, "#justfurfun",
                function (event, data, formatted) {
                    var id1 = $(event.target).attr('id');
                    id1 = id1.substring(id1.indexOf('_') + 1);
                    $(event.target).next().val(data[0]); // ID
                    if (data[2] && data[2] != 0) {
                        //$('#a_' + id1).next().text('x')
                        $('#ucc_' + id1).val(data[2]); // CurrencyConverter}
                    }
                    else {
                        //$('#a_' + id1).next().text(' ')
                        $('#ucc_' + id1).val('1');
                    }
                }
            , null, null, null, _autourl1);
        }
    });

    autoComplete('Personnel', 'personnelaccauto', "#tableItems .labell", "#justfurfun"
    , autoPostPack1
    , function (data, i, n, value) {
        return data[1];
    });

    bindSaleType();

    doMore();
    $('#TotalPrice, #TotalTax, #ShipmentPrice, #VAT').blur(function () { updateFormTotal(); });
    $('.price, .amount, .discount, .storeservicename, .unitname').blur(function () { updatePrice(this); });

    $('.tax, .svat').blur(function () { updateTotal(); });

    $('#FactorTotalDiscount').blur(function () { updateTotal(); });
    $('#FactorTotalPursant').blur(function () { updateTotal(); });


    $('#CashPrice,#CheckPrice,#OwedPrice').blur(setRequireds);
    setRequireds();

    updateFormTotal();

    function setServiceData(serviceNameInput, data) {
        serviceNameInput.next().val(data[0]); // ID
        serviceNameInput.attr('title', data[19]);
        var id = serviceNameInput.attr('id');
        id = id.substring(id.indexOf('_') + 1);
        var percent = 1 + toFloat(data[11], 0) / 100.0;
        $('#isv_' + id).val(data[21]);//IsVirtaul
        $('#prf_' + id).val(toFloat(data[11], 0));
        var price = 0;
        // has ProfitPercent from service declaration
        // Price priority:
        // 1- GroupingPrice: col10 | col14 = BuyPrice, col16 = SalePrice
        // 2- ProfitPercent: col10 = BasePrice, col2 = SalePrice
        // 3- Last Prices: col12 = LastSalePrice (TheLPSP), col14 = LastBuyPrice (TheLPBP)
        //price = data[2];

        if (data[11]) {
            // ProfitPercent
            if (data[2])
                price = data[2];
            if (data[10])
                $('#bp_' + id).val(data[10]);
        }
        else {
            // Last Prices
            if (data[12])
                price = data[12];
            if (data[14])
                $('#bp_' + id).val(data[14]);
        }
        if (data[16]) {
            // GroupingPrice
            price = data[16];
            // BuyPrice is from Old method
        }
        $('#p_' + id).val(numberWithCommas((toFloat(toMonetary(price), 0.0)).toFixed(_Context.decimalPoints))); // price
        $('#smc_' + id).val(numberWithCommas(data[9])); // salesmancommission
        $('#u_' + id).next().val(data[6]); // unitid
        $('#u_' + id).val(data[7]); // unitname
        $('#avial_' + id).val(data[4] - data[5]); // available: income - outcome
        var _autourl1 = 'get.aspx?op=Auto&table=Units&qd=storedraftunitsauto&StoreServiceID=' + data[0];
        autoComplete('Units', 'unitsauto', "#tableItems tr:last .unitname", "#justfurfun",
            function (event, data, formatted) {
                var id1 = $(event.target).attr('id');
                id1 = id1.substring(id1.indexOf('_') + 1);
                $(event.target).next().val(data[0]); // ID
                if (data[2] && data[2] != 0) {
                    //$('#a_' + id1).next().text('x')
                    $('#ucc_' + id1).val(data[2]); // CurrencyConverter}
                }
                else {
                    //$('#a_' + id1).next().text(' ')
                    $('#ucc_' + id1).val('1');
                }
            }
        , null, null, null, _autourl1);
        bindSaleType();

        // ServiceVariate
        if (data[17] > 0) {
            $('#varid_' + id).val(data[17]);
        }
        $('#a_' + id).val(1);
        $('#a_' + id).blur();
        //ServicePrice DefaultDoscount
        //if (data[20]) {
        //    var val = toFloat(data[20], 0);
        //    if (val < 0) {
        // serviceDiscountPercent = -1 * val;
        calcRowDiscountPercent(id);
        // }
        //$('#d_' + id).val(numberWithCommas((val * -1.0).toFixed(_Context.decimalPoints)) + '%');
        //else
        //   $('#d_' + id).val(numberWithCommas((val * 1.0).toFixed(_Context.decimalPoints)));
        //}
    }

    function setServiceAutoComplete() {
        //var more = encodeURIComponent("&StoreID=" + storeID);
        var storeID = toInt($("#StoreID").val(), -1);
        var buyerID = toInt($("#BuyerID").val(), -1);
        var table = $("#tableItems");
        $("#tableItems tr:last .storeservicename").unautocomplete();
        if ($("#tableItems tr:last .storeservicename").val() != '') {
            return;
        }
        var _autourl = 'get.aspx?op=Auto&table=StoreDraft&qd=storeservicesauto&StoreID=' + storeID + '&BuyerID=' + buyerID + appendTime();
        autoCompleteFree('StoreServices', 'storeservicesauto', "#tableItems tr:last .storeservicename", "#justfurfun"
                , function (event, data, formatted) {
                    $('tr .storeserviceid', table).each(function () {
                        if ($(this).attr('id') != $('tr:last .storeserviceid', table).attr('id')) {
                            if ($(this).val() == data[0]) {
                                alert(Lang.StoreDraftJsM3);
                            }
                        }
                    });
                    setServiceData($(event.target), data);
                }
                , function (data, i, n, value) {
                    return formatService(data);
                }, null, null, _autourl);
    }

    function doMore() {

        function removeRow($who) {
            var id = $who.attr('id'); // r_1
            id = id.substring(2); // r_1  -->  1
            $('#a_' + id).val("");//DeliverAmount
            $('#p_' + id).val("");//price
            $('#se_' + id).val(""); // StoreServiceName
            $('#sid_' + id).val(-1); // StoreServiceID
            $who.parent().parent().find(':input').removeClass();
            $who.parent().parent().hide();
            updateTotal();
            Growl.Notice(Lang.DeletedSuccessfully);
        }
        function removePartner($who) {
            var id = $who.attr('id'); // rc_1
            id = id.substring(3); // rc_1  -->  1
            $('#pn_' + id).val("");
            $('#pn_' + id).next().val(-1);
            $who.parent().parent().hide();
            $who.parent().parent().find(':input').removeClass();
        }
        $('.remove').click(function () {
            var element = $(this);
            doConfirm(function () {
                removeRow(element);
            }, Lang.ConfirmDelete);
        });
        $('.removepartner').click(function () {
            removePartner($(this));
        });

        $('.add').click(function () {
            var table = $("#tableItems");
            var lastRow = $('tr:last', table);
            if ($("#tableItems .amount").length > 0) {
                if (checkAvail($('.amount', lastRow)) < 0) {
                    return false;
                };
            }
            if (saleType == 2 && toInt($('#BuyerID').val(), 0) <= 0) {
                Growl.Notice(Lang.SelectBuyerMsg, 3000);
                return false;
            }
            if (insertStoreDraftWithBuyerOutBalance != 1) {
                if (!checkCredit()) {
                    Growl.Error(Lang.LowCredit, 3000);
                    $('.amount', lastRow).val(0);
                    $('.amount', lastRow).blur();
                    return false;
                }
            }
            var storeID = toInt($("#StoreID").val(), -1);
            //if (!hasValuePositive(storeID)) {
            //    alert(Lang.ChooseStore);
            //    return;
            //}

            var labelId = 0;
            var labelName = '';
            if ($('#LabelCheckBox').attr('checked')) {
                labelId = $('tr:last .labelid', table).val();
                labelName = $('tr:last .labell', table).val();
            }
            $('tr:last', table).after(addRow(null));
            Growl.Notice(Lang.RowAddedSuccessfully);
            if ($('#LabelCheckBox').attr('checked')) {
                 $('tr:last .labelid', table).val(labelId);
                 $('tr:last .labell', table).val(labelName);
            }
            labelCheckBox();
            saleTypeShow();
            setDefault('SaleType', 'defaultsaletype', "#tableItems tr:last .saletype", "#tableItems tr:last .saletypeid");
            if ($('#LabelCheckBox').attr('checked')) {
                disableKeydownOnLastInputByName('Label');
            } else {
                disableKeydownOnLastInputByName('SingleVAT');
            }
            lastRow = $('tr:last', table);
            if (storeMode) {
                $('.acc').hide();
                disableKeydownOnLastInputByName('DeliveredAmount');
            }

            $('.remove', lastRow).click(function () {
                var element = $(this);
                doConfirm(function () {
                    removeRow(element);
                }, Lang.ConfirmDelete);
            });


            setServiceAutoComplete();

            autoComplete('Personnel', 'personnelaccauto', "#tableItems tr:last .labell", "#justfurfun"
                , autoPostPack1
                , function (data, i, n, value) {
                    return data[1];
                });

            function checkServiceBarCode(me, meID, meSID, barcode) {
                var buyerID = toInt($("#BuyerID").val(), -1);
                var StoreID = toInt($("#StoreID").val(), -1);
                var _autourl = 'get.aspx?op=Auto&table=StoreServices&qd=storeservicesbarcodeauto' + '&BuyerID=' + buyerID + '&StoreID=' + StoreID + appendTime();
                var row = getServerValues(_autourl + "&q=" + barcode);
                if (!row) {
                    return false;
                }
                if (row && row.length && row.length > 10) {
                    var varID = -1;
                    // ServiceVariate
                    if (row[17] > 0)
                        varID = row[17];
                    // -----------------------------------
                    // check if the service is already used, just increase the number
                    var already = false;
                    $('tr .storeserviceid', table).each(function () {
                        if ($(this).attr('id') != meSID) {
                            if ($(this).val() == row[0]) {
                                var v = toInt($(this).parent().find('.ServiceVariateID').val(), 0);
                                if (v == 0) {
                                    already = true;
                                    me.val('');
                                    me.focus();
                                    var aID = $(this).attr('id');
                                    var _cnt = aID.substring(aID.indexOf('_') + 1);
                                    $('#a_' + _cnt).val(toInt($('#a_' + _cnt).val(), 0) + 1);
                                    $('#a_' + _cnt).blur();
                                    updatePrice($('#a_' + _cnt));
                                    setTimeout(function () {
                                        $(me).focus();
                                    }, 1);
                                }
                            }
                        }
                    });
                    if (!already) {
                        setServiceData(me, row);
                        me.val(row[1]);
                        $('.add').first().click();
                    }
                }
                return true;
            }
            function checkServiceVariateBarCode(me, meID, meSID, barcode) {
                var buyerID = toInt($("#BuyerID").val(), -1);
                var StoreID = toInt($("#StoreID").val(), -1);
                var _autourl = 'get.aspx?op=Auto&table=StoreServices&qd=storeservicesvariatebarcodeauto' + '&BuyerID=' + buyerID + '&StoreID=' + StoreID + appendTime();
                var row = getServerValues(_autourl + "&q=" + barcode);
                if (!row) {
                    $.ionSound.play("bell_ring");
                    return false;
                }
                if (row && row.length && row.length > 10) {
                    var varID = -1;
                    // ServiceVariate
                    if (row[17] > 0)
                        varID = row[17];
                    // -----------------------------------
                    // check if the service is already used, just increase the number
                    var already = false;
                    $('tr .storeserviceid', table).each(function () {
                        if ($(this).attr('id') != meSID) {
                            if ($(this).val() == row[0]) {
                                // check variate
                                if (varID != -1 && toInt(varID) > 0) {
                                    // parent = td
                                    var v = $(this).parent().find('.ServiceVariateID').val();
                                    if (v != varID)
                                        return;
                                }
                                already = true;
                                me.val('');
                                me.focus();
                                var aID = $(this).attr('id');
                                var _cnt = aID.substring(aID.indexOf('_') + 1);
                                $('#a_' + _cnt).val(toInt($('#a_' + _cnt).val(), 0) + 1);
                                $('#a_' + _cnt).blur();
                                updatePrice($('#a_' + _cnt));
                                setTimeout(function () {
                                    $(me).focus();
                                }, 1);
                            }
                        }
                    });
                    if (!already) {
                        setServiceData(me, row);
                        me.val(row[1] + ' - ' + row[18]);  // row[18] = ServiceVariate.VarTitle 


                        $('.add').first().click();
                    }
                }
                return true;
            }
            // Handle barcode reader
            $("#tableItems tr:last .storeservicename").blur(function () {
                // se_id, sid_id
                var me = $(this);
                var meID = me.attr('id'); // se_1
                var meSID = '#sid_' + meID.substring(meID.indexOf('_') + 1);

                var barcode = $(this).val();
                var stsID = $(meSID).val(); // StoreServiceID
                if (barcode.length > 0 && stsID == '') {
                    // it is a barcode
                    if (!checkServiceBarCode(me, meID, meSID, barcode))
                        if (!checkServiceVariateBarCode(me, meID, meSID, barcode)) {
                            $(me).val('');
                            setTimeout(function () { $(me).focus(); }, 10);
                        }
                }
            });
            //autoComplete('Units', 'unitsauto', "#tableItems tr:last .unitname", "#justfurfun", tableAutoPostPack, formatItemNameCode, null, null, 'get.aspx?op=Auto&table=Units&qd=unitsauto&UnitID=' + 7 + '&StoreServiceID=' + 131 + appendTime());
            $('.price', lastRow).blur(function () {
                if (toFloat($('.baseprice', lastRow).val(), 0) > toFloat($('.price', lastRow).val(), 0) && servicePriceMsg == 1) {
                    alert(Lang.buyPriceIsGreaterThanSalePrice);
                }
            });
            $('.price, .amount, .discount, .storeservicename, .unitname', lastRow).blur(function () { updatePrice(this); });
            $('.amount', lastRow).blur(function () { checkAvail(this); });
            $('.tax, .svat', lastRow).blur(function () { updateTotal(); });
            commaInputs();
            monetaryKeys();
            setTimeout(function () {
                $("#tableItems tr:last .storeservicename").focus();
            }, 10);
            linkClick();
            autoPercentSign();
        });
        $('.addPartner').click(function () {
            var table = $('#tablePartners');
            $('tr:last', table).after(addPartner(null));
            var lastRow = $('tr:last', table);
            $('.removepartner', lastRow).click(function () {
                removePartner($(this));
            });
            autoComplete('Personnel', 'personnelaccauto', "#tablePartners tr:last .partnername", "#justfurfun"
                , autoPostPack1
                , function (data, i, n, value) {
                    return data[1];
                });
        });
        insertValidation();
        monetaryInputsSet(null, '#FactorDate');
        commaInputs();
        percentInputsSet();
        $('#newCheck').click(function () {

            var oldID = _Context.id;
            function showcallback(v, m, f) {
                if (v == 'OK') {
                    generalOKScalar(v, m, f, 'checkupdate', 'checkinsert', 'Checks', '#subDetailForm');
                    if (!_Context.success)
                        return false;
                    $('#CheckName').val(f.Serial);
                    $('#CheckID').val(_Context.resultID);
                }
                _Context.id = oldID;
            }
            _Context.id = -1;
            var _url = 'get.aspx?op=GetHtml&table=Checks&qd=check&id=-1&PersonnelName=' + encodeURIComponent($('#BuyerName').val())
                + "&PersonnelID=" + $('#BuyerID').val()
                + "&VisitorName=" + encodeURIComponent($('#VisitorName').val()) + "&VisitorID=" + $('#VisitorID').val()
                + "&FromStoreDraft=1&Amount=" + fromMonetary($('#Total').val(), null) + appendTime();
            //alert(_url);
            showPopup(_url, showcallback, getButtons());
        });

        $('#CalculateRewards').click(function () {
            var title = '', final = 0, totalPrice = 0;
            $('.source').each(function (index, value) {
                if ($(value).val() == 1)
                    removeRow($(this));
            });
            offerDiscount = 0;
            payAllowDays = 0;
            offerCash = 0;
            offercheck1 = 0;
            offercheck2 = 0;
            offerbank = 0;

            fillTable("get.aspx?op=GetXml&qd=calculaterewards&StoreDraftID=" + mID, '#tableItems', addRow);
            disableKeydownOnLastInputByName('SingleVAT');
            labelCheckBox();
            saleTypeShow();

            $('.remove').click(function () {
                var element = $(this);
                doConfirm(function () {
                    removeRow(element);
                }, Lang.ConfirmDelete);
            });

            $('#OfferDiscount').val(numberWithCommas(offerDiscount));

            $('#OfferCashDiscount').val(offerCash);
            $('#OfferCheck1Discount').val(offercheck1);
            $('#OfferCheck2Discount').val(offercheck2);
            $('#OfferBankDiscount').val(offerbank);


            final = toFloat($('#Total').val(), 0);
            totalPrice = toFloat($('#TotalPrice').val(), 0);

            title = Lang.PayMethodCash + ': ' + numberWithCommas(final - toMonetary(offerCash)) + ' ' + Lang.PayMethodCheck1 + ': ' + numberWithCommas(final - toMonetary(offercheck1))
                + ' ' + Lang.PayMethodCheck2 + ': ' + numberWithCommas(final - toMonetary(offercheck2)) + ' ' + Lang.PayMethodBank + ': ' + numberWithCommas(final - toMonetary(offerbank));
            $('#CalculateRewards').attr('title', title);

            if (saleType != 1 && saleType != 2) {
                $('#OfferCashPrice').val(totalPrice - toMonetary(offerCash));
                $('#OfferCheck1Price').val(totalPrice - toMonetary(offercheck1));
                $('#OfferCheck2Price').val(totalPrice - toMonetary(offercheck2));
                $('#OfferBankPrice').val(totalPrice - toMonetary(offerbank));
            }
            commaInputs();
            updateTotal();
            $('#PayAllowedDays').val(payAllowDays);
            if (payAllowDays) {
                var PDate = getServerValue('calcstoredraftpaydate', 'StoreDraft', 'PayAllowDays=' + payAllowDays + '&StoreDraftID=' + mID);
                $('#PayDate').val(PDate);
            }
        });

        updateTotal();
        if (storeMode) {
            $('.acc').hide();
            disableKeydownOnLastInputByName('DeliveredAmount');
        }
        linkClick();
    }

    if (mID > 0) {
        uploadify('#ChooseFile', '#FileName', '#StartUpload', 'workflow/storedraft/' + mID
                   , function (fileName) {
                       $('#FileNameDisplay').html(fileName);
                   });
        $('#workflowtasks').html(showWorkflowTasks(mID, WorkflowTypeEnum.StoreDraft));
    }
    else
        $('.transDiv').hide();

    $(".collapsbtn").click(function () {
        if ($(this).html().search('glyphicon-collapse-down') > -1) {
            $(this).find('span').remove();
            $(this).html('<span class="glyphicon glyphicon-collapse-up"></span> ' + $(this).html());
        }
        else {
            $(this).find('span').remove();
            $(this).html('<span class="glyphicon glyphicon-collapse-down"></span> ' + $(this).html());
        }
    });

    
    $('#connectToCard').click(function () {
        PosCloseCOM();
        PosOpenCOM();
        return false;
    });
    $('#sendToCard').click(function () {
        var val = 0;
        var method = toInt($('#PayMethod').val(), SalesInvoicePayMethodEnum.Cash);
        if (method == SalesInvoicePayMethodEnum.Bank) {
            val = stripCommas($('#Total').val());
        }
        else if (method == SalesInvoicePayMethodEnum.Complex) {
            val = stripCommas($('#BankPrice').val());
        }
        if (val <= 0)
            return false;
        PosSendToCOM(val);
        return false;
    });
    $('#disconnectCard').click(function () {
        PosCloseCOM();
        return false;
    });
    // -----------------------------------


    // Check access for finalized factors:
    if ($('#WorkflowStatus').val() == 1) {
        if (hasAccess("sales", "storedraft", "price")) {
            // it can do everything: add / delete / edit
        }
        else if (hasAccess("sales", "storedraft", "count")) {
            // it can not do add or delete
            // it can not change price
            // it can change amount
            $('.add').remove();
            $('.remove').remove();
            $('.price').attr('readonly', true);
        }
        else {
            // it can do nothing
            $('.add').remove();
            $('.remove').remove();
            $('.price').attr('readonly', true);
            $('.amount').attr('readonly', true);
            $('.unitname').attr('readonly', true);
        }
    }

    $('#VisitorName').focus();
}