﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/site.js" />

/*
92-10-28: file created
*/
function view_accreport() {
    var cnt = 0, mID = -1;
    $('#tableTopic').smartTable();
    function addTopic(node) {
        var row = '<tr>';
        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">' + (cnt + 1) + '</td>';

        row += '<td data-th="' + Lang.Title + '">'
            + '<input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="text" id="ti_' + cnt + '" name="Title" value="' + getNodeVal(node, "Title") + '" class="Title form-control input-sm" />'
            + '</td>';

        row += '<td data-th="' + Lang.Topic + '">'
            + '<input type="text" id="t_' + cnt + '" name="TreePathName" value="' + getNodeVal(node, "TreePathName") + '" class="TreePathName form-control input-sm"/>'
            + '<input type="hidden" name="TreePath" class="TreePath" value="' + getNodeVal(node, "TreePath") + '" />'
            + '</td>';

        row += '<td data-th="' + Lang.Formula + '">'
            + '<input type="text" id="f_' + cnt + '" name="Formula" value="' + getNodeVal(node, "Formula") + '" class="Formula form-control input-sm ltr" />'
            + '</td>';

        row += '<td data-th="' + Lang.IsSecond + '" class="align-center compact">'
            + '<input type="checkbox" id="sec_' + cnt + '" name="IsSecond' + cnt + '" ' + (getNodeVal(node, "IsSecond", '') == '1' || getNodeVal(node, "IsSecond", '') == 'True' ? 'checked=checked' : '') + ' />'
            + '</td>';

        row += '<td data-th="' + Lang.IsVisible + '" class="align-center compact">'
            + '<input type="checkbox" id="v_' + cnt + '" name="IsVisible' + cnt + '" ' + (getNodeVal(node, "IsVisible", '') == '1' || getNodeVal(node, "IsVisible", '') == 'True' ? 'checked=checked' : '') + ' />'
            + '</td>';

        row += '<td data-th="' + Lang.InChart + '" class="align-center compact">'
            + '<input type="checkbox" id="v_' + cnt + '" name="InChart' + cnt + '" ' + (getNodeVal(node, "InChart", '') == '1' || getNodeVal(node, "InChart", '') == 'True' ? 'checked=checked' : '') + ' />'
            + '</td>';

        row += '<td data-th="' + Lang.SortIndex + '">'
            + '<input type="text" id="si_' + cnt + '" name="SortIndex" value="' + getNodeVal(node, "SortIndex") + '" class="SortIndex form-control input-sm" />'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a class="btn btn-danger btn-xs btn-block btn-remove removeTopic" id="r_' + cnt + '">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }
    function RemoveTopicRow($who) {
        var id = $who.attr('id'); // r_1
        id = id.substring(2); // r_1  -->  1
        //  alert(id);
        $('#ti_' + id).val(""); // 
        $('#t_' + id).val(""); // 
        $('#t_' + id).next().val(-1); // 
        $who.parent().parent().hide();
        Growl.Notice(Lang.DeletedSuccessfully);
    }
    function autoPostPack(event, data, formatted) {
        $(event.target).next().val(data[0]); // ID
    }
    // ------------------------------
    // startup code
    // ------------------------------
    mID = toInt($('#MasterID').val(), -1);
    setChecked("#Enabled");

    $('.popupTitle').html(_Context.title);
    fillTable("get.aspx?op=GetXml&qd=accreportcol&AccReportID=" + mID, '#tableTopic', addTopic);
    doMore();

    function doMore() {
        function autoPostPack(event, data, formatted) {
            $(event.target).next().val(data[0]); // ID
        }
        autoComplete('Trees', 'accreporttreesauto', '#tableTopic .TreePathName', 'funny', autoPostPack);

        $('.addTopic').click(function () {
            var table = $('#tableTopic');
            $('tr:last', table).after(addTopic(null));
            Growl.Notice(Lang.RowAddedSuccessfully);
            var lastRow = $('tr:last', table);
            autoComplete('Trees', 'treesauto', '#tableTopic .TreePathName', 'funny', autoPostPack);
            $('.removeTopic', lastRow).click(function () {
                RemoveTopicRow($(this));
            });
        });

        $('.removeTopic').click(function () {
            RemoveTopicRow($(this));
        });
    }
    insertValidation();
}