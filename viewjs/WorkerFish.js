﻿function view_WorkerFish() {
    $(function () {
        
        //$("#PersonnelName").attr('disabled', 'disabled');
        dateSelect('.dateSelect');
        if ($("#Date").val() == '')
            $("#Date").val(_Context.today);
        if ($("#LockStatus").val() == '')
            $("#LockStatus").val(0);
        insertValidation();
        
        autoComplete('Personnel', 'staffauto', '#PersonnelName', '#PersonnelID',function(event, data, formatted) {
            var id = getServerValue('curpersonfish', 'Fish', "&PersonnelID=" + data[0] + "&FishYear=" + $("#FishYear").val() + "&FishMonth=" + $("#FishMonth").val());
            if (id != null) {
                $('#ID').val(id);
                $(".jqibuttons").find(":button[value='Another']").last().click();
            }
        });
        autoComplete('FishDefs', 'enumauto1', '#FishDefName', '#FishDefID');
        var m1 = "";
        var more = "PersonnelID=" + $("#PersonnelID").val();
        autoComplete('Orders', 'enumauto1', '#OrderName', '#OrderID', null, null, null, more);
        setValue("#Status");
        setValue("#FishMonth");
        setChecked("#HasInsurance");
        shortcut.remove("F4");
        shortcut.add("F4", function() {
            if ($(".jqibuttons").find(":button[value='Calc']").length > 0) {
                $(".jqibuttons").find(":button[value='Calc']").last().click();
            }
        });
        if ($("#LockStatus").val() == 0) {
            $(".jqibuttons").find(":button[value='Unlock']").last().hide();
        }
        else {
            $(".jqibuttons").find(":button[value='Lock']").last().hide();
        }
        $(".jqibuttons").find(":button[value='Another']").last().hide();
        $('#FirstPerson').click(function () {
            m1 = "&FishYear=" + $("#FishYear").val() + "&FishMonth=" + $("#FishMonth").val();
            var id = getServerValue('firstpersonfish', 'Fish',m1);
            if (id != null) {
                $("#ID").val(id);
                $(".jqibuttons").find(":button[value='Another']").last().click();
            }
            
        });
        $('#LastPerson').click(function () {
            m1 = "&FishYear=" + $("#FishYear").val() + "&FishMonth=" + $("#FishMonth").val();
            var id = getServerValue('lastpersonfish', 'Fish',m1);
            if (id!=null) {
                $("#ID").val(id);
                $(".jqibuttons").find(":button[value='Another']").last().click();
            }
        });
        $('#PrevPerson').click(function () {
            m1 ="&PersonnelID=" + $("#PersonnelID").val()+ "&FishYear=" + $("#FishYear").val() + "&FishMonth=" + $("#FishMonth").val();
            var id = getServerValue('prevpersonfish', 'Fish', m1);
            if (id!=null) {
                $('#ID').val(id);
                $(".jqibuttons").find(":button[value='Another']").last().click();
            }
        });
        $('#NextPerson').click(function () {
            m1 ="&PersonnelID=" + $("#PersonnelID").val()+ "&FishYear=" + $("#FishYear").val() + "&FishMonth=" + $("#FishMonth").val();
            var id = getServerValue('nextpersonfish', 'Fish', m1);
            if (id!=null) {
                $('#ID').val(id);
                $(".jqibuttons").find(":button[value='Another']").last().click();
            }
            
        });
        $('#PrevFish').click(function () {
            m1 = "&PersonnelID=" + $("#PersonnelID").val() + "&FishID=" + $("#ID").val();
            var id = getServerValue('prevfish', 'Fish', m1);
            if (id!=null) {
                $('#ID').val(id);
                $(".jqibuttons").find(":button[value='Another']").last().click();
            }
        });
        $('#NextFish').click(function () {
            m1 = "&PersonnelID=" + $("#PersonnelID").val() + "&FishID=" + $("#ID").val();
            var id = getServerValue('nextfish', 'Fish', m1);
            if (id != null) {
                $('#ID').val(id);
                $(".jqibuttons").find(":button[value='Another']").last().click();
            }
        });
        $('.efield').click(function () {
            var fieldId = $(this).attr('data-id');
            showfield(fieldId);
        });
        $('#FishMonth').change(function () {
            m1 ="&PersonnelID=" + $("#PersonnelID").val()+ "&FishYear=" + $("#FishYear").val() + "&FishMonth=" + $("#FishMonth").val();
            var id = getServerValue('curpersonfish', 'Fish', m1);
            if (id != null) {
                $('#ID').val(id);
                $(".jqibuttons").find(":button[value='Another']").last().click();
            }
        });
        function showfieldcallback(v, m, f) {
            newShowcallback(v, m, f, 'fieldupdate', 'fieldinsert', 'Fields', "#detailForm", showfield);
            _Context.id = $("#ID").val();
            return true;
        }
        function showfield(id, celDiv, successCallback) {
            _Context.id = id;
            var _url = 'get.aspx?op=GetHtml&table=Fields&qd=field&id=' + id + appendTime();
            showPopup(_url, showfieldcallback, null, null, successCallback);
        }
        makeChosen();
        monetaryKeys();
        commaInputs('#detailFormFish');
    });
}