﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
93-02-08: file created
*/
function view_style() {
    function updateValue()
    {
        var val = '';
        if ($('#FontFamily').val() != '')
            val += 'font-family:' + $('#FontFamily').val() + ';';
        if ($('#FontSize').val() != '')
            val += 'font-size:' + $('#FontSize').val() + 'px;';
        if ($('#LineHeight').val() != '')
            val += 'line-height:' + $('#LineHeight').val() + 'px;';
        if ($('#Color').val() != '')
            val += 'color:#' + $('#Color').val() + ';';
        if ($('#BackColor').val() != '')
            val += 'background-color:#' + $('#BackColor').val() + ';';
        if ($('#BorderWidth').val() != '')
            val += 'border-style:solid;border-width:' + $('#BorderWidth').val() + 'px;';
        if ($('#BorderColor').val() != '')
            val += 'border-color:#' + $('#BorderColor').val() + ';';
        $('#Value').val(val);
        $('#sample').attr('style', val);
    }
    // ------------------------------
    // startup code
    // ------------------------------
    $('.popupTitle').html(_Context.title);
    $('#FontFamily,#FontSize,#Color,#BackColor,#BorderWidth,#BorderColor,#LineHeight').blur(updateValue);
    updateValue();

    $('#clearBackColor').click(function () {
        $('#BackColor').val('');
    });
    $('#clearColor').click(function () {
        $('#Color').val('');
    });
    insertValidation();
}