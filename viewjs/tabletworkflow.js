﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
92-10-21: ReceiverID autocomplete
92-10-19: file created
*/
function view_tabletworkflow() {
    var cnt = 0;

    function addRow(node) {

        var row = '<tr>';
        row += '<td data-th="' + Lang.ColRowNr + '" class="row-number align-center  width-40">' + (cnt + 1) + '</td>';

        row += '<td class="width-300" data-th="' + Lang.GroupingName + '">'
            + '<input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="text" id="gn_' + cnt + '" name="GroupingName" value="'
            + getNodeVal(node, "GroupingName") + '" class="groupingname form-control input-sm align-center"/>'
            + '<input type="hidden" id="gid_' + cnt + '" name="GroupingID" value="' + getNodeVal(node, "GroupingID", 0) + '" class="groupingid" />'
            + '</td>';

        row += '<td data-th="' + Lang.Task + '" class="width-200 align-center">'
            + '<input type="text" id="tn_' + cnt + '" name="TaskName" value="' + getNodeVal(node, "TaskName") + '" class="taskname form-control input-sm" />'
            + '<input type="hidden" name="TaskID" value="' + getNodeVal(node, "TaskID") + '" />'
            + '</td>';

        row += '<td data-th="' + Lang.CooperationType + '" class="width-200 align-center cooperationtypeitem">'
            + '<input type="text" id="ctn_' + cnt + '" name="CooperationTypeNames" value="' + getNodeVal(node, "CooperationTypeNames") + '" class="cooperationtypenames form-control input-sm" />'
            + '<input type="hidden" name="CooperationTypeIDs" value="' + getNodeVal(node, "CooperationTypeIDs") + '" class="cooperationtypeids" />'
            + '</td>';

        row += '<td class="align-center action-remove width-40">'
            + '<a id="r_' + cnt + '" class="btn btn-danger btn-xs btn-block btn-remove remove">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }

    function autoPostPack(event, data, formatted) {
        $(event.target).next().val(data[0]); // ID
    }

    $('.add').click(addClick);

    function addClick() {
        var table = $('#tableItems');
        $('tr:last', table).after(addRow(null));
        disableKeydownOnLastInputByName('CooperationTypeName');
        Growl.Notice(Lang.RowAddedSuccessfully);
        var lastRow = $('tr:last', table);

        autoComplete('TasK', getTaskAutoQry(), "#tableItems tr:last .taskname", "#justfurfun", autoPostPack, function(data, i, n, value) {
            return data[1];
        });
        autoComplete('CooperationType', 'enumauto1', "#tableItems tr:last .cooperationtypenames", "#justfurfun", autoPostPack, function(data, i, n, value) {
            return data[1];
        });

        autoComplete('Grouping', 'groupauto', "#tableItems tr:last .groupingname", "#justfurfun", autoPostPack, function(data, i, n, value) {
            return data[1];
        });

        $('.remove', lastRow).click(function() {
            removeRow($(this));
        });
        insertValidation('#subDetailForm');
        $('input[name=GroupingName]', lastRow).focus();
        coopertionTypeDivSorH();
    };

    function removeRow($who) {
        var id = $who.attr('id'); // r_1
        id = id.substring(id.indexOf('_') + 1); // r_1  -->  1
        $('#gn_' + id).val(""); // GroupingName
        $('#gid_' + id).val(''); // GroupingID
        $who.parent().parent().hide();
        Growl.Notice(Lang.DeletedSuccessfully);
    }

    function getTaskAutoQry() {
        var selectedVal = toInt($('#WorkFlowType').val(), 6);
        var qry = 'storedraftphase';
        switch (selectedVal) {
        case 4:
            qry = 'returnphase';
            break;
        case 2:
            qry = 'purchasephase';
            break;
        case 5:
            qry = 'refusephase';
            break;
        case 0:
            qry = 'transferphase';
            break;
        case 1:
            qry = 'requestphase';
            break;
        case 3:
            qry = 'purchaserequestphase';
            break;
        case 7:
            qry = 'buildorderphase';
            break;
        }
        return qry;
    }

    function coopertionTypeDivSorH() {
        var selectedVal = toInt($('#Type').val(), 6);
        if (selectedVal == 1) {
            $('#CooperationTypeName').val('');
            $('#CooperationTypeID').val('');
            $('.cooperationtypediv').hide();
            $('.cooperationtypeitem').show();
        } else {
            $('.cooperationtypediv').show();
            $('.cooperationtypeids').val('');
            $('.cooperationtypenames').val('');
            $('.cooperationtypeitem').hide();
        }
    }

// ------------------------------
    // startup code
    // ------------------------------
    dateSelect('.dateSelect');
    setValue('#Type');
    setValue('#WorkFlowType');

    var mID = toInt($('#MasterID').val(), -1);


    autoComplete('CooperationType', 'enumauto1', '#CooperationTypeName', '#CooperationTypeID');
    autoComplete('TasK', getTaskAutoQry(), "#OutputFromServerTaskName", "#OutputFromServerTaskID");
    $('#WorkFlowType').change(function() {
        $('#OutputFromServerTaskName').unautocomplete();
        autoComplete('TasK', getTaskAutoQry(), "#OutputFromServerTaskName", "#OutputFromServerTaskID");
    });

    $('#Type').change(function () {
        coopertionTypeDivSorH();
    });

    fillTable("get.aspx?op=GetXml&qd=tabletworkflowitems&TabletWorkFlowID=" + mID, '#tableItems', addRow);

    coopertionTypeDivSorH();

    autoComplete('TasK', getTaskAutoQry(), "#tableItems .taskname", "#justfurfun"
    , autoPostPack, function (data, i, n, value) {
        return data[1];
    });
    autoComplete('CooperationType', 'enumauto1', "#tableItems .cooperationtypenames", "#justfurfun"
         , autoPostPack, function (data, i, n, value) {
             return data[1];
         });

    autoComplete('Grouping', 'groupauto', "#tableItems .groupingname", "#justfurfun"
        , autoPostPack
        , function (data, i, n, value) {
            return data[1];
        });

    $('.remove').click(function () {
        removeRow($(this));
    });
    insertValidation('#subDetailForm');
}