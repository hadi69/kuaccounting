﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
93-01-12: file created
*/
function view_unitconverter() {
    // ------------------------------
    // startup code
    // ------------------------------
    setChecked("#Enabled");

    autoComplete('Units', 'enumauto', "#SrcUnitsName", "#SrcUnitsID");
    autoComplete('Units', 'enumauto', "#DestUnitsName", "#DestUnitsID");
    insertValidation();
}