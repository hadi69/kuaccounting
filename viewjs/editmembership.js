﻿function view_editmembership() {
    var cnt = 0;

    function addRow(node) {
        // id="x" --> the validationEngine requires ID
        var readonly = node != null ? " readonly='readonly' " : "";
        var row = '<tr>';

        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">'
            + (cnt + 1)
            + '</td>';

        row += '<td data-th="' + Lang.Field + '">'
            + '<input type="hidden" id=f_' + cnt + ' name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="text" id="p_' + cnt + '" name="PersonnelName" value="' + getNodeVal(node, "PersonnelName") +'"'
            + 'class="personnelname form-control input-sm" ' + readonly + ' />'
            + '<input type="hidden" name="PersonnelID" value="' + getNodeVal(node, "ID") + '" />'
            + '</td>';

        row += '<td data-th="' + Lang.PersonneliNum + '">'
            + '<input type="text" id="pn_' + cnt + '" name="PersonneliNum" value="' + getNodeVal(node, "PersonneliNum") + '" class="personnelinum form-control input-sm"' + readonly + ' />'
            + '</td>';

        row += '<td data-th="' + Lang.Value + '">'
            + '<input type="text" id="m_' + cnt + '" name="MonthlyPay" value="' + getNodeVal(node, "MonthlyPay") + '" class="value form-control input-sm validate[custom[integer]] monetary" />'
            + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }

    function fillInfo() {
        var url = "get.aspx?op=GetXml&table=Memberships&qd=allmemberships&FundID="+$("#FundID").val();
        fillTable(url, '#tableItems', addRow);
    }

    $(".popupTitle").html($("#FundName").val());
    fillInfo();
    monetaryKeys();
    commaInputs();

}