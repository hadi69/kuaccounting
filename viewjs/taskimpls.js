﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    94-02-28: file created
*/
function view_taskimpls() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var id = $.urlParam('id');
        var more = decodeURIComponent("RelatedID=" + $('#RelatedID').val() + " AND " + "Type=" + $('#Type').val());
        var _url = 'service.asmx/GetList?table=TaskImpl&qd=taskimpl&more=' + more + appendTime();
        // alert(_url);
        $("#taskimplflexi").flexigrid
            ({
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.Title, name: 'TaskName', sortable: true, css_class: 'width-120' },
                    { display: Lang.EntryUserName, name: 'EnteryUserName', css_class: 'width-120', sortable: true },
                    { display: Lang.ExitUserName, name: 'ExitUserName', css_class: 'width-120', sortable: true },
                    { display: Lang.EntryDate, name: 'EntryDate', sortable: true, css_class: 'width-120', process: showDate },
                    { display: Lang.ExitDate, name: 'ExitDate', sortable: true, css_class: 'width-120', process: showDate },
                    { display: Lang.Duration, name: 'Duration', sortable: true, css_class: 'width-60', process: showTaskDuration },
                    { display: Lang.TaskMsg, name: 'Msg', sortable: true },
                    { display: Lang.Done, name: 'Done', sortable: true, css_class: 'width-100 align-center', process: showBool }
                ],
                buttons:
                [
                    { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                searchitems: null,
                sortname: "ID",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight, resizable: gridResizable,
                singleSelect: true
            });
    }

    // -------------------------
    // Startup code
    // -------------------------
    run();
}