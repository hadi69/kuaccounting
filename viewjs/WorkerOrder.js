﻿function view_WorkerOrder() {
    $(function () {
        dateSelect('.dateSelect');
        if ($("#OrderDate").val() == '')
            $("#OrderDate").val(_Context.today);
        insertValidation();
        autoComplete('Personnel', 'staffauto', '#PersonnelName', '#PersonnelID', function (event, data, formatted) {
            var id = getServerValue('curpersonorder', 'Order', "&PersonnelID="+data[0]);
            if (id != null) {
                $('#ID').val(id);
                $(".jqibuttons").find(":button[value='Another']").last().click();
            }
        });
        autoComplete('OrderDefs', 'enumauto1', '#OrderDefName', '#OrderDefID');
        autoComplete('Consts', 'enumauto1', '#ConstName', '#ConstID');

        $(".jqibuttons").find(":button[value='Another']").last().hide();
        var more = "&PersonnelID=" + $("#PersonnelID").val();
        $('#FirstPerson').click(function () {
            var id = getServerValue('firstpersonorder', 'Order');
            if (id != null) {
                $("#ID").val(id);
                $(".jqibuttons").find(":button[value='Another']").last().click();
            }

        });
        $('#LastPerson').click(function () {
            var id = getServerValue('lastpersonorder', 'Order');
            if (id != null) {
                $("#ID").val(id);
                $(".jqibuttons").find(":button[value='Another']").last().click();
            }
        });
        $('#PrevPerson').click(function () {
            var id = getServerValue('prevpersonorder', 'Order', more);
            if (id != null) {
                $('#ID').val(id);
                $(".jqibuttons").find(":button[value='Another']").last().click();
            }
        });
        $('#NextPerson').click(function () {
            var id = getServerValue('nextpersonorder', 'Order', more);
            if (id != null) {
                $('#ID').val(id);
                $(".jqibuttons").find(":button[value='Another']").last().click();
            }

        });
        $('#PrevOrder').click(function () {
            more = "&PersonnelID=" + $("#PersonnelID").val() + "&OrderID=" + $("#ID").val();
            var id = getServerValue('prevorder', 'Order', more);
            if (id != null) {
                $('#ID').val(id);
                $(".jqibuttons").find(":button[value='Another']").last().click();
            }
        });
        $('#NextOrder').click(function () {
            more = "&PersonnelID=" + $("#PersonnelID").val() + "&OrderID=" + $("#ID").val();
            var id = getServerValue('nextorder', 'Order', more);
            if (id != null) {
                $('#ID').val(id);
                $(".jqibuttons").find(":button[value='Another']").last().click();
            }
        });
        $('.efield').click(function() {
            var fieldId = $(this).attr('data-id');
            showfield(fieldId);
        });

        function showfieldcallback(v, m, f) {
            newShowcallback(v, m, f, 'fieldupdate', 'fieldinsert', 'Fields', "#detailForm", showfield);
            _Context.id = $("#ID").val();
            return true;
        }
        function showfield(id, celDiv, successCallback) {
            _Context.id = id;
            var _url = 'get.aspx?op=GetHtml&table=Fields&qd=field&id=' + id + appendTime();
            showPopup(_url, showfieldcallback, null, null, successCallback);
        }
        commaInputs("#detailFormOrder");
        monetaryKeys();
        //tabify();
    });
}