﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
    95-01-24: file created
*/
function view_costcenters() {
    function doCommand(com, grid) {        
        generalCommand(com, grid, show);
    }
    function run() {
        var _url = 'service.asmx/GetList?table=CostCenter&qd=costcenter' + appendTime();
        //alert(_url);
        $("#flexi").flexigrid
                (
                {
                    url: _url,
                    dataType: 'xml',
                    colModel:
                    [
                        { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell-big' },
                        { display: 'ID', name: 'ID', css_class: '_id', width: 10, sortable: false, align: 'right', hide: true },
                        { display: Lang.Name, name: 'Name', sortable: true, align: 'right', css_class: 'width-300' },
                        { display: 'ParentID', name: 'ParentID', css_class: '_parentID', hide: true },
                        { display: Lang.Code, name: 'Code', sortable: true, align: 'right', css_class: 'width-120' },
                        { display: Lang.Comments, name: 'Comments', width: 220, sortable: true, align: 'right'},
                        { display: Lang.Enabled, name: 'Enabled', sortable: true, process: showBool, css_class: 'align-center width-40' },
                        { display: Lang.ColEdit, name: 'Edit', sortable: false, hide: hideColumn("manager", "enums", "edit"), process: function (celDiv, id) { editLink(celDiv, id, show) }, css_class: 'action-cell' },
                        { display: Lang.ColDel, name: 'Del', sortable: false, hide: hideColumn("manager", "enums", "delete"), process: delLink, css_class: 'action-cell'}
                    ],
                    buttons:
                    [
                        { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                        { name: Lang.GBEdit, bclass: 'edit', onpress: doCommand },
                        { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                        { separator: true },
                        { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                        { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                    ],
                    searchitems: null,
                    sortname: "Code",
                    sortorder: "asc",
                    usepager: true,
                    useRp: true,
                    rp: gridRp,
                    showTableToggleBtn: _options.gridToggle,
                    width: gridWidth,

                    height: _options.gridHeight, resizable: _options.gridResizable,
                    singleSelect: false,
                    onDoubleClick: function (row, grid, options) {
                        var id = row.id.substring(3);
                        show(id);
                    },
                    onSuccess: function (grid) {
                        restoreSelectedRows('#flexi');
                        colorGrid();
                    }
                }
                );
    }
    function showcallback(v, m, f) {
        var qry = (_Context.id == -1) ? 'costcenterinsert' : 'costcenterupdate';
        return newShowcallback(v, m, f, 'costcenterupdate', 'costcenterinsert', 'CostCenter', '#subDetailForm', show);
    }

    function show(id, celDiv, successCallback) {
        _Context.id = id;
        saveSelectedRows('#flexi');
        var _url = 'get.aspx?op=GetHtml&table=CostCenter&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, successCallback)
        //jscolor.init();
    }
    function colorGrid() {
        setTimeout(function () {
            $('#flexi tr').each(function () {
                var id = $('._id div', this).html();
                var parentID = $('._parentID div', this).html();
                $(this).addClass("treegrid-" + id);
                if (parentID)
                    $(this).addClass("treegrid-parent-" + parentID);
            });
            $jQuery1_9('#flexi').treegrid({ initialState: 'collapsed' });
        }, 1000);
    }
    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['accounting,costcenter']);
    checkLevel(2);
    setClassAccounting();
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.CostCenter, parentUrl: 'menu/accounting.aspx', parentTitle: Lang.MAccounting } }));
    run();
}