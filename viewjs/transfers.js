﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-10-16: file created
*/
function view_transfers() {
    var startupID = -1;
    function doCommand(com, grid) {
        if (com == Lang.DoubleTransfer) {
            var id = getRowID(grid);
            if (id != -1) {
                var more = '&ID=' + id;
                runQuery('retransfer', 'Transfer', more);
                var _url = 'service.asmx/GetList?table=Transfer&qd=transfer' + appendTime();
                $("#flexi").flexOptions({ url: _url });
                $("#flexi").flexReload();
            }
        }
        else {
            var isNew = com == Lang.GBNew;
            if (!isNew) {
                var btn = $(".fbutton span:contains(" + com + ")");
                if (btn.hasClass('new'))
                    isNew = true;
            }
            if (isNew) {
                var more = '&Type=' + WorkflowTypeEnum.Transfer;
                var count = getServerValue('checkworkflow', 'Workflow', more);
                if (count == 0 || count == null) {
                    displayError(Lang.WorkflowM1);
                    return;
                }
            }
            if (isNew && com != Lang.GBNew) {
                var desiredEntryTaskID = $(".fbutton span:contains(" + com + ")").attr('class').substring('new '.length);
                //alert(desiredEntryTaskID);
                transfershow(-1, null, null, desiredEntryTaskID);
            }
            else
                generalCommand(com, grid, transfershow);
        }
    }
    function run() {
        autoComplete('TasK', 'transferphase', "#fTaskName", "#fTaskID");

        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        var btns = getTaskGridButtons(WorkflowTypeEnum.Transfer, doCommand, "storeforms", "transfer");
        //btns[btns.length] = { name: Lang.DoubleTransfer, bclass: 'retransfer', onpress: doCommand };
        //alert(_url);
        $("#flexi").flexigrid
            ({
                url: _url,
                dataType: 'xml',
                colModel:
                [
                        { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                        { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                        { display: Lang.No, name: 'NoSerial', sortable: true, css_class: 'width-70 align-center' },
                        { display: Lang.OriginStore, name: 'SourceStoreName', sortable: true, css_class: 'width-200'},
                        { display: Lang.DestinationStore, name: 'DestStoreName', sortable: true},
                        { display: Lang.Date, name: 'Date', sortable: true, css_class: 'date-cell'},
                        { display: Lang.PreStatus, name: 'PreTaskName', sortable: true, css_class: 'width-80 align-center' },
                        { display: Lang.Status, name: 'TaskName', sortable: true, css_class: 'width-80 align-center' },
                        { display: '', name: 'WorkflowColor', css_class: 'WorkflowColor', hide: true },
                        { display: Lang.NumPrints, name: 'NumPrints', sortable: true, css_class: 'width-100 align-center' },
                        {
                            display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, transfershow); }, css_class: 'action-cell'
                            
                        }
                ],
                buttons: btns,
                searchitems: null,
                sortname: "NOSerial",
                sortorder: "desc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight, resizable: gridResizable,
                singleSelect: true,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    transfershow(id);
                },
                onSuccess: function (grid) {
                    postProcessGrid();
                    if (startupID != -1) {
                        transfershow(startupID);
                        startupID = -1;
                    }
                }

            });
    }
    function postProcessGrid() {
        setTimeout(function () {
            colorTasks();
        }, 500);
    }
    
    
    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['stores,transfer']);
    checkLevel(2);
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.Transfer, parentTitle: Lang.SellAndBuy, parentUrl: 'menu/sales.aspx' } }));
    setClassStoreForms();
    dateSelect('.dateSelect');
    startupID = $.urlParam('id');
    if (!startupID)
        startupID = -1;

    run();
}