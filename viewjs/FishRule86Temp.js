﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    93-06-23: file created
*/
function view_FishRule86Temp() {
    autoComplete('Personnel', 'personnelselectauto', '#PersonnelName', '#PersonnelID');
    dateSelect('.dateSelect');
    setChecked('#DoLifeInsurance');
    setChecked('#HasStoreFund');
    setChecked('#UseSocialInsurance');
    setValue('#Status');
    $('.formula').attr('readonly', true);
    insertValidation();
}