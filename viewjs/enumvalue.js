﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
92-10-16: file created
*/
function view_enumvalue() {
    // ------------------------------
    // startup code
    // ------------------------------
    var id = $.urlParam('id');
    function getParentTitle() {
        var more = '&ID=' + id;
        var ptitle = getServerValue('getparentenum', 'Enums', more);
        return ptitle;
    }
    function getpTitle() {
        var more = '&ID=' + id;
        var ename = getServerValue('getenumname', 'Enums', more);
        return ename;
    }
    function getParentEnumID() {
        var more = '&ID=' + id;
        var ID = getServerValue('getparentenumid', 'Enums', more);
        return ID;
    }
    $('.popupTitle').html(getpTitle());
    if (getParentTitle() == null) {
        $('.pname').hide();
        $('#ParentName').hide();
    }
    else {
        var more = 'EnumID=' + getParentEnumID();
        $('.pname').html(getParentTitle());
        autoComplete('EnumValues', 'enumvalueauto', '#ParentName', '#ParentID'
            , function (event, data, formatted) {
                $("#ParentID").val(data[0]);
            }
            , null, null, more);
    }
    insertValidation();
}