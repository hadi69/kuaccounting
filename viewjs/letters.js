﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    94-06-27: file created
*/
function view_letters() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    var firstTime = true;
    function run() {
        var _url = 'service.asmx/GetList?table=Letter&qd=letter' + appendTime();
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                   // { display: Lang.Program, name: 'ProgramName', sortable: true, hide: true },
                    { display: Lang.Type, name: 'LetterType', sortable: true, css_class: 'width-30 align-center', process: displayLetterType },
                    { display: Lang.SenderID, name: 'SenderID', hide: true },
                    { display: Lang.Sender, name: 'SenderName', sortable: true, css_class: 'width-100 align-center' },
                    { display: Lang.Date, name: 'Date', sortable: true, css_class: 'width-80 align-center', process: showDate },
                    { display: Lang.Subject, name: 'Subject', sortable: true },
                    { display: Lang.ReceiverNames, name: 'ReceiverNames', sortable: true, css_class: 'width-200' },
                    { display: Lang.ReplyTo, name: 'ReplyToName', sortable: true, css_class: 'width-90' },
                    { display: Lang.Archive, name: 'Archive', hide: true },
                    { display: Lang.Deleted, name: 'Deleted', hide: true },
                    { display: Lang.Read, name: 'Read', hide: true, css_class: 'read' },
                    { display: Lang.ReadDate, name: 'ReadDate', sortable: true, css_class: 'width-80 align-center', process: showDate },
                    { display: Lang.FileName, name: 'FileNames', hide: true },
                    { display: Lang.Message, name: 'Message', hide: true},
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' }
                ],
                buttons:
                [
                   { name: Lang.GBNew, bclass: 'new', onpress: doCommand, onaccess: true },
                   { name: Lang.GBDel, bclass: 'delete', onpress: doCommand, onaccess: true },
                   { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand, onaccess: true }
                ],
                searchitems: null,
                sortname: "Date",
                sortorder: "desc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight, resizable: gridResizable,
                singleSelect: true,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                },
                onSuccess: function () {
                    if (firstTime) {
                        firstTime = false;
                        $('#fRefresh').click();
                    }
                    else
                        postProcessGrid();
                }
            });
    }
    function postProcessGrid() {
        setTimeout(function () {
            colorNotRead();
        }, 500);
    }
    function colorNotRead() {
        $('#flexi tr').each(function () {
            var read = $('.read div', this).html();
            if (read == 0) {
                $('.lettertype', this).addClass('neg');
            }
        });
    }
    function showcallback(v, m, f) {
        if (v == "Reply")
        {
            show(-1, null, null, _Context.id, null);
        }
        else if (v == "Forward") {
            show(-1, null, null, null, _Context.id);
        }
        else if (v == "Archive") {
            if (runQuery('letterarchive', 'Letter', 'LetterID=' + _Context.id + '&Archive=1', false))
                $("#flexi").flexReload();
        }
        return newShowcallback(v, m, f, 'letterupdate', 'letterinsert', _Context.table, null, show);
    }
    function show(id, celDiv, successCallback, replyToID, forwardID) {
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=Letter&qd=' + _Context.query + '&id=' + id + appendTime();
        if (replyToID)
            _url += "&ReplyToID=" + replyToID;
        if (forwardID)
            _url = 'get.aspx?op=GetHtml&table=Letter&qd=' + _Context.query + '&id=' + forwardID + "&ForwardID=" + forwardID + appendTime();
        var btns = {};
        if (id > 0) {
            btns[Lang.GBNew] = 'New';
            btns[Lang.Reply] = 'Reply';
            btns[Lang.Forward] = 'Forward';
            btns[Lang.Print] = 'Print';
            btns[Lang.Archive] = 'Archive';
        }
        else {
            btns[Lang.OK] = 'OK';
        }
        btns[Lang.Close] = 'Cancel';
        showPopup(_url, showcallback, btns, null, successCallback);
    }
    $(function () {
        //checkAccess(['bank,bankaccount']);
        setClassBank();
        $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.Letter } }));
    });
    function displayLetterType(celDiv, id) {
        var _mode = parseInt($(celDiv).html(), 10);
        if (_mode == 0)
            $(celDiv).html('<i class="lettertype fa fa-sign-out"></i>');
        else if (_mode == 1)
            $(celDiv).html('<i class="lettertype fa fa-sign-in"></i>');
        else
            $(celDiv).html(_mode);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    dateSelect('.dateSelect');
    run();
}