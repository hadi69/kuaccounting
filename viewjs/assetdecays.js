﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
    95-12-27: file created
*/
function view_assetdecays() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        $("#flexi").flexigrid
                (
                {
                    url: _url,
                    dataType: 'xml',
                    colModel:
                    [
                        { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'center', hide: true },
                        { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell'},
                        { display: Lang.Asset, name: 'AssetName', sortable: true},
                        { display: Lang.AssetGroup, name: 'AssetGroupName', sortable: true, css_class: 'width-90 align-center' },
                        { display: Lang.OrgUnit, name: 'OrgUnitName', sortable: true, css_class: 'width-90 align-center' },
                        { display: Lang.Seating, name: 'SeatingName', sortable: true, css_class: 'width-80 align-center' },
                        { display: Lang.Num, name: 'Num', sortable: true, css_class: 'width-80 align-center' },
                        { display: Lang.Date, name: 'FillDate', sortable: true, css_class: 'date-cell' },
                        { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' }
                    ],
                    buttons:
                    [
                        { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                        { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                        { separator: true },
                        { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                    ],
                    searchitems: null,
                    sortname: "AssetName",
                    sortorder: "desc",
                    usepager: true,
                    useRp: true,
                    rp: gridRp,
                    showTableToggleBtn: gridToggle,
                    width: gridWidth,
                    height: gridHeight, resizable: gridResizable,
                    singleSelect: false,
                    onDoubleClick: function (row, grid, options) {
                        var id = row.id.substring(3);
                        show(id);
                    }
                });
    }
    function showcallback(v, m, f) {
        return newShowcallback(v, m, f, 'assetdecayupdate', 'assetdecayinsert', _Context.table, null, show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['asset,assetdecay']);
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.AssetDecay, parentTitle: Lang.MAsset, parentUrl: 'menu/asset.aspx' } }));
    setClassAsset();
    run();
}