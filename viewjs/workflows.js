﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
    92-10-16: file created
*/
function view_workflows() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        //alert(_url);
        $("#flexi").flexigrid
                (
                {
                    url: _url,
                    dataType: 'xml',
                    colModel:
                    [
                        { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                        { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell'},
                        { display: Lang.Title, name: 'Name', sortable: true, css_class: 'width-250'},
                        { display: Lang.Type, name: 'Type', sortable: true, process: _displayWorkflowType},
                        { display: Lang.Enabled, name: 'Active', sortable: true, process: showBool, css_class: 'width-40 align-center' },
                        { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' },
                        { display: Lang.Tasks, name: 'Tasks', sortable: false, process: taskDefinition, css_class: 'action-cell'},
                        { display: Lang.Transitions, name: 'Transitions', sortable: false, process: transitionDefinition, css_class: 'width-100 align-center' }
                    ],
                    buttons:
                    [
                        { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                        { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                        { separator: true },
                        { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                    ],
                    searchitems: null,
                    sortname: "Name",
                    sortorder: "asc",
                    usepager: true,
                    useRp: true,
                    rp: gridRp,
                    showTableToggleBtn: gridToggle,
                    width: gridWidth,
                    height: gridHeight, resizable: gridResizable,
                    singleSelect: false,
                    onDoubleClick: function (row, grid, options) {
                        var id = row.id.substring(3);
                        show(id);
                    }
                });
    }
    function _displayWorkflowType(celDiv, id) {
        var type = toInt($(celDiv).html(), 0);
        displayWorkflowType(celDiv, type);
    }
    function showcallback(v, m, f) {
        return newShowcallback(v, m, f, 'workflowupdate', 'workflowinsert', _Context.table, null, show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['sys,workflow']);
    checkLevel(3);
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.Workflow, parentTitle: Lang.MManagement, parentUrl: 'menu/sys.aspx' } }));
    setClassSys();
    run();
}