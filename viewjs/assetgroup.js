﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
94-09-12: file created
*/
function view_assetgroup() {
    makeChosen();
    // ------------------------------
    // startup code
    // -----------------------------
    dateSelect('.dateSelect')
    autoComplete('AssetLevel', 'enumauto1', "#AssetLevelName", "#AssetLevelID");
    autoComplete('DecayRule', 'enumauto1', "#DecayRuleName", "#DecayRuleID");
    autoCompleteTreePath('#AssetTreePathName', '#AssetTreePath', '#AssetSel');
    autoCompleteTreePath('#DecayCostTreePathName', '#DecayCostTreePath', '#DecayCostSel');
    autoCompleteTreePath('#DecaySumTreePathName', '#DecaySumTreePath', '#DecaySumSel');
    autoCompleteTreePath('#CostTreePathName', '#CostTreePath', '#CostSel');
    autoCompleteTreePath('#IncomeTreePathName', '#IncomeTreePath', '#IncomeSel');
    insertValidation();
}