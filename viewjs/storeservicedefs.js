﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-11-05: file created
*/
function view_storeservicedefs() {
    function doCommand(com, grid) {
        if (com == Lang.Cardex) {
            showCardex(grid);
        }
        else if (com == Lang.CreatePurchaseRequest) {
            var ids = '';
            for (var i = 0; i < _checkedRowIDs.length; i++) {
                   ids += _checkedRowIDs[i] + ',';
                }
            if (ids.length > 0) {
                _checkedRowIDs = [];
                $("#flexi").flexReload();
                ids = ids.substring(0, ids.length - 1);
                var more = '&ids=' + ids;
                var newId = getServerValue('createpurchaserequestfromstoreservicedef', 'StoreServiceDef', more);
                if (newId > 0)
                    window.open('list.aspx?table=PurchaseRequest&id=' + newId);
                // displayError(Lang.Success, null, Lang.Success);
            } else {
                Growl.Notice(Lang.PleseSelectAService, 3000);
            }
        }
        else if (com == Lang.GBStroeLabels) {
            storeLabelCommand(com, grid, show, null, "تگ انبارگردانی", false, "");
        }
        else
            generalCommand(com, grid, show);
    }
    // it is similar to site.js excelCommand
    function storeLabelCommand(com, grid, funcShow, extra, title, hide, enums) {
        var _url = $("#flexi").flexUrl();
        var fParams = "";
        $(':input', '#fValues').each(function () {
            // 'q' is for fields, 'w' is more Where (w in queries.xml)
            var fName = '_q_' + $(this).attr('q');
            if ($(this).attr('w'))
                fName = $(this).attr('w');
            var fValue = $(this).val();
            if (fValue && fValue.length > 0 && fValue != -1)
                fParams += "&" + fName + "=" + fValue;
        });
        if (fParams.length > 0)
            fParams += '&comb=' + $('input:checked', '#fComb').val();
        fParams += '&orderby=' + $('#flexi').flexSortname() + '&sortorder=' + $('#flexi').flexSortorder();
        _url = _url.replace('service.asmx/GetList?', 'get.aspx?op=stroelabels&');
        _url += fParams;
        if (extra)
            _url += "&" + extra;
        if (title)
            _url += "&t=" + title;
        if (hide)
            _url += "&hide=" + hide;
        if (enums)
            _url += "&" + enums;
        //$.prompt(_url);
        //return false;
        document.location = _url;
    }

    function run() {
        var id = $.urlParam('id');
        // $('#NumAviServices').html(getServerValue("storeservicedefsum", "StoreServiceDef", "StoreID=" + id));
        var u = 'get.aspx?op=Auto&table=StoreServices&qd=storeservicedefsumprices&q=' + id + appendTime();
        var rows = getServerRows(u);
        var sumPrices = 0;
        if (rows && rows.length && rows.length > 0)
            for (var i = 0; i < rows.length; i++) {
                sumPrices += toFloat(rows[i], 0);
            }
        $('#NumAviServices').html(numberWithCommas(toMonetary(sumPrices, _Context.today)));
        var more = encodeURIComponent('StoreID=' + id);
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + '&more=' + more + appendTime();
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeileNummer', sortable: false, css_class: 'row-cell' },
                    { display: 'StoreID', name: 'StoreID', width: 0, sortable: false, align: 'left', hide: true },
                    { display: Lang.Store, name: 'StoreTitle', width: 0, sortable: false, align: 'left', hide: true },
                    { display: Lang.Commodity, name: 'ServiceName', sortable: false },
                    { display: Lang.Code, name: 'Code', sortable: true, css_class: 'width-70 align-center' },
                    { display: Lang.Order, name: 'StoreLayout', sortable: false, css_class: 'width-50 align-center', hide: true },
                    { display: Lang.MostHdmvjvdy, name: 'MaxStock', sortable: true, css_class: 'width-110 align-center',hide: true  },
                    { display: Lang.LowestHdmvjvdy, name: 'MinStock', sortable: true, css_class: 'width-100 align-center',hide: true  },
                    { display: Lang.TheOrder, name: 'OrderPoint', sortable: true, css_class: 'width-80 align-center',hide: true  },
                    { display: Lang.Input, name: 'Income', sortable: true, css_class: 'width-60 align-center', hide: true },
                    { display: Lang.Output, name: 'Outcome', sortable: true, css_class: 'width-60 align-center', hide: true },
                    { display: Lang.StoreStock, name: 'Available', sortable: true, process: displayAvailable, css_class: 'width-80 align-center' },
                    { display: Lang.NotConfirmOutcomeG, name: 'NotConfirmOutcome', process: function (celDiv, id) { notConfirmedLink(celDiv, id); }, sortable: true, css_class: 'width-100 align-center' },
                    { display: Lang.PurchasePriceOfTheUnit, name: 'AvgPrice', sortable: true, process: displayAvgPrice, css_class: 'width-90 align-center' },
                    { display: Lang.SaleWorth, name: 'SaleWorth', sortable: true, process: displayAvgPrice, css_class: 'width-90 align-center' },
                    { display: Lang.Sum, name: 'TotalPrice', sortable: true, process: displayTotalPrice, css_class: 'width-100 align-center', hide: true },
                    { display: Lang.SalesManCommission, name: 'SalesManCommission', sortable: true, css_class: 'width-90 align-center' },
                    { display: Lang.Status, name: 'Describe', sortable: true, process: displayDescribe, css_class: 'width-150 align-center' },
                    { display: Lang.Edit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' },
                    { display: Lang.CheckMark, name: 'CheckMark', sortable: false, process: function (celDiv, id) { checkMark(celDiv, id, show); }, css_class: 'action-cell' }
                ],
                buttons:
                [
                    { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                    { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                    { name: Lang.Cardex, bclass: 'cardex', onpress: doCommand },
                    { separator: true },
                    { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                    { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand },
                    { name: Lang.GBStroeLabels, bclass: 'stroelabels', onpress: doCommand }
                ],
                searchitems: null,
                sortname: "ServiceName",
                sortorder: "asc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                width: gridWidth,
                height: gridHeight, resizable: gridResizable,
                //onDoubleClick: function (row, grid, options) {
                //    var id = row.id.substring(3);
                //    show(id);
                //},
                onSuccess: function (grid) {
                    bindCheckBoxClick();
                    restoreCheckedRows(grid);
                    calcSum();
                }
            }
            );
    }
    function notConfirmedLink(celDiv, id) {
        var num = toInt($(celDiv).html(), 0);
        if (num <= 0)
            return;
        $(celDiv).html('<a>' + num + '</a>');
        $(celDiv).click(function (event) {
            event.preventDefault();
            if (clicksDisabled)
                return;
            disableClicks();
            var _url = 'get.aspx?op=GetHtml&view=ssdnotconfirmed&table=' + _Context.table + '&qd=storeservicedef&id=' + id + appendTime();
            showPopup(_url, showcallback, getCloseButtons(), null, function () { return true;});
        });
    }

    function displayDescribe(celDiv, id) {
        var describe = toInt($(celDiv).html(), 0);
        if (describe == 1)
            $(celDiv).html(Lang.ExceedsTheBalance);
        else if (describe == 2)
            $(celDiv).html(Lang.LowerThanInventory);
        else if (describe == 3)
            $(celDiv).html(Lang.LessThanTheOrderPoint);
    }
    function displayAvgPrice(celDiv, id) {
        _Context.priceDate = _Context.today;
        $(celDiv).html(numberWithCommas(toMonetary(celDiv.innerText, 0)));
        $(celDiv).addClass('_avg');
    }
    function displayAvailable(celDiv, id) {
        $(celDiv).addClass('_avi');
    }
    function displayTotalPrice(celDiv, id) {
        $(celDiv).addClass('_tot');
    }
    function showcallback(v, m, f) {
        var extraQ = (_Context.id == -1) ? 'storeservicedefnewextra' : '';
        return newShowcallback(v, m, f, 'storeservicedefupdate', 'storeservicedefinsert', _Context.table, null, show, null, extraQ);
    }
    function calcSum() {
        var sum = 0;
        setTimeout(function () {
            $('#flexi tr').each(function () {
                var _avg = toFloat($('._avg', this).html(), 0);
                var _avi = toFloat($('._avi', this).html(), 0);
                var total = _avg * _avi;
                if (total < 0) total = 0;
                sum += total;
                $('._tot', this).html(numberWithCommas((total).toFixed(_Context.decimalPoints)));
            });
            //alert(sum);
        }, 100);
        //ValidVoucher
    }
    function show(id, celDiv, successCallback) {
        var storeid = $.urlParam('id');
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=storeservicedef&id=' + id + '&StoreID=' + storeid + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }
    
    function showCardex(grid) {
        if (clicksDisabled)
            return;
        disableClicks();
        var id = getRowID(grid);
        var store = getGridRowContent(grid, 4);
        var storeservice = getGridRowContent(grid, 5);
        var _url = 'get.aspx?op=GetHtml&table=Cardex&view=cardex&SSDefID=' + id + '&StoreTitle=' + store + '&StoreServiceName=' + storeservice + '&PerStoreService=-1' + appendTime();
        showPopup(_url, function (v, m ,f) {
            if (v == "Print")
                return generalPrint(v, m, f, '#detailForm');
            return true;
        }, getCloseAndPrintButtons());
    }

    function bindCheckBoxClick() {
        $('.checkmark').click(function () {
            var elem = $(this);
            var id = elem.parent().parent().parent().attr('id');
            id = id.substring(id.lastIndexOf('row') + 3);
            if (elem.attr('checked')) {
                saveCheckedRows(id);
            } else {
                saveDisCheckedRows(id);
            }
        });
    }

    // ------------------------------
    // startup code
    // ------------------------------
    var storename = getServerValue('getstorename', 'Stores', '&StoreID=' + $.urlParam('id'));
    $("#bar").prepend(breadcrumb({
        title: _Context.title + ' - ' + storename,
        grandParent: {
            title: Lang.MStore,
            url: 'menu/store.aspx'
        },
        parent: {
            title: Lang.Stores,
            url: 'list.aspx?table=Stores&menu=storea'
        }
    }));
    setClassStore();
    run();

    // display Cardex
    var ssid = $.urlParam('ssid');
    if (ssid)
    {
        var store = '';//getGridRowContent(grid, 4);
        var storeservice = '';//getGridRowContent(grid, 5);
        var _url = 'get.aspx?op=GetHtml&table=Cardex&view=cardex&StoreServiceDefID=' + ssid + '&StoreTitle=' + store + '&StoreServiceName=' + storeservice + appendTime();
        showPopup(_url, function (v, m, f) {
            if (v == "Print")
                return generalPrint(v, m, f, '#detailForm');
            return true;
        }, getCloseAndPrintButtons());
    }
    autoComplete('ServiceType', "enumauto", "#fServiceTypeName", "oh yeah");
}