﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
94-09-15: file created
*/
function view_tabletmsg() {
    // ------------------------------
    // startup code
    // -----------------------------
    dateSelect('.dateSelect')
    autoCompleteFree('Personnel', 'visitorauto', '#VisitorName', '#VisitorID', null
            , function (data, i, n, value) {
                //$('#VisitorID').val('');
                return formatPersonnelItem(data);
            });
    $('#Msg').focus();
    $('#VisitorName').focus();
    if (toInt($('#ID').val(), 0) > 0)
        $('#AllVisitorsDiv').hide();
    insertValidation();
}