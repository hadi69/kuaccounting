﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-10-30: file created
*/
function view_storedrafts() {
    var storeMode, totalPriceColmunHideshow = false;
    var startupID = -1;
    function doCommand(com, grid) {
        //if (com == Lang.CreateDailyStoreDraft) {
        //    var ok = getServerValue('createdailystoredraft', 'StoreDraft');
        //    if (ok == 1) {
        //        Growl.Notice(Lang.Success, 3000);
        //        $("#flexi").flexReload();
        //    }else if (ok == -1) {
        //        Growl.Error(Lang.DailyStoreDraftMsg, 3000);
        //    } else {
        //        Growl.Error(Lang.Failed, 3000);
        //    }
        //    return;
        //}
        var isNew = com == Lang.GBNew;
        if (!isNew)
        {
            var btn = $(".fbutton span:contains(" + com + ")");
            if (btn.hasClass('new'))
                isNew = true;
        }
        if (isNew) {
            var more = '&Type=' + WorkflowTypeEnum.StoreDraft;
            var count = getServerValue('checkworkflow', 'Workflow', more);
            if (count == 0 || count == null) {
                displayError(Lang.WorkflowM1);
                return;
            }
        }
        if (isNew && com != Lang.GBNew) {
            var desiredEntryTaskID = $(".fbutton span:contains(" + com + ")").attr('class').substring('new '.length);
            //alert(desiredEntryTaskID);
            storedraftshow(-1, null, null, desiredEntryTaskID);
        }
        //else if (com == Lang.TemporaryStoreDaraft) {
        //    var ok = getServerValue('checktempstoredraft', 'StoreDraft');
        //        if (ok >= 1) {
        //            Growl.Error(Lang.TemporaryStoreDaraftMsg, 3000);
        //            return;
        //        } else {
        //            storedraftshow(-1, null, null, null, true);
        //        }
        //}
          else {
            var multiPrintRoot =
                "rootPath + Router.Generate('print-storedraft', { params: { ids: totalIds, title: 'SalesInvoice' }, query: { report: 'simple', print: 'true' } })";
            generalCommand(com, grid, storedraftshow, null, null, null, multiPrintRoot);
        }
    }
    function run() {
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + (_Context.workflowInheritance ? 'storedraft' : 'storedraftnoinheritance') + "&more=__PrOgrAmconD__" + appendTime();
        autoComplete('TasK', 'storedraftphase', "#fTaskName", "#fTaskID");
        //alert(_url);
        $("#flexi").flexigrid
        (
        {
            url: _url,
            dataType: 'xml',
            colModel:
            [
                { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'center', hide: true },
                { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                { display: Lang.FactorNo, name: 'NOSerial', sortable: true, css_class: 'width-70 align-center' },
                { display: Lang.FactorDate, name: 'FactorDate', sortable: true, process: capturePriceDate, css_class: 'date-cell' },
                { display: Lang.Store, name: 'StoreName', sortable: true, css_class: 'width-60 align-center' },
                { display: Lang.Comments, name: 'Comments', sortable: true, hide: true, css_class:'cmnts' },
                { display: Lang.Inventory, name: 'StoreKeeperName', sortable: true, hide: true },
                { display: Lang.Accountant, name: 'AccountantName', sortable: true, hide: true },
                { display: Lang.BuyerName, name: 'BuyerNameComment', sortable: true, css_class: 'width-150 align-center' },
                { display: Lang.Visitor, name: 'VisitorName', sortable: true, css_class: 'align-center' },
                { display: Lang.TotalS, name: 'TotalPrice', hide: totalPriceColmunHideshow, sortable: true, process: showMonetary, css_class: 'width-80 align-center', footer: 'sum' },
                { display: Lang.TotalAmount, name: 'TotalAmount', sortable: true, css_class: 'width-60 align-center', footer: 'sum' },
                { display: Lang.SumProfit, name: 'SumProfit', hide: true },
                { display: Lang.PreStatus, name: 'PreTaskName', sortable: true, css_class: 'task-cell' },
                { display: Lang.Status, name: 'TaskName', sortable: true, css_class: 'task-cell' },
                { display: Lang.PayMethod, name: 'PayMethod', sortable: true, hide: true },
                { display: '', name: 'WorkflowColor', css_class: 'WorkflowColor', hide: true },
                { display: Lang.Status, name: 'VoucherID', hide: true, process: displayVoucherID },
                { display: Lang.Status, name: 'DocItemID', hide: true, process: displayDocItemID },
                { display: Lang.DocumentNumber, name: 'VoucherNo', sortable: false, process: displayVoucherNo, css_class: 'width-70 align-center' },
                { display: '', name: 'PayDate', hide: true },
                { display: '', name: 'PayStatus', hide: true },
                { display: Lang.NumPrints, name: 'NumPrints', sortable: true, css_calss: 'width-50 align-center' },
                { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, storedraftshow); }, hide: hideColumn("sales", "storedraft", "edit"), css_class: 'action-cell' }
             ],
            buttons: getTaskGridButtons(WorkflowTypeEnum.StoreDraft, doCommand, "sales", "storedraft"),
                searchitems: null,
                sortname: "NOSerial",
                sortorder: "desc",
                usepager: true,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: gridToggle,
                showToggleBtn: true,
                width: gridWidth,

                height: gridHeight, resizable: gridResizable,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    storedraftshow(id);
                },
                onSuccess: function (grid) {
                    postProcessGrid();
                    if (startupID != -1) {
                        storedraftshow(startupID);
                        startupID = -1;
                    }
                }
            });
    }

    //function getBtns() {
    //    var btns = [];
    //    btns = getTaskGridButtons(WorkflowTypeEnum.StoreDraft, doCommand, "sales", "storedraft");
    //    btns.push({ separator: true });
    //    btns.push({ name: Lang.TemporaryStoreDaraft, bclass: 'newtemp', onpress: doCommand });
    //    btns.push({ name: Lang.CreateDailyStoreDraft, bclass: 'daily', onpress: doCommand });
    //   return btns;
    //}

    function displayVoucherID(celDiv, id) {
        $(celDiv).addClass('_vid');
    }
    function displayDocItemID(celDiv, id) {
        $(celDiv).addClass('_diid');
    }
    function displayVoucherNo(celDiv, id) {
        $(celDiv).addClass('_vno');
    }
    function postProcessGrid() {
        setTimeout(function () {
            showVoucherLinks();
            colorTasks();
            colorComments();
        }, 500);
    }
    function showVoucherLinks() {
        setTimeout(function () {
            $('#flexi tr').each(function () {
                var _id = $('._vid', this).html();
                var _docitemid = $('._diid', this).html();
                //alert(_id);
                if (_id && _id != '')
                    $('._vno', this).html("<a href='list.aspx?table=DocumentAccounting&id=" + _id + "&itemid=" + _docitemid + "'>" + $('._vno', this).html() + "</a>");
            });
        }, 500);
        //ValidVoucher
    }
    function colorComments() {
        $('#flexi tr .cmnts').each(function () {
            var cmnts = $('div', this).html();
            if (cmnts && cmnts.trim() != '')
                $(this).closest('tr').find('td').css('background-color', '#F0AD4E');
        });
    }


    $('#BuyPriceUpdate').click(function () {
        var access = hasAccess("general", "generalsettings", "buypriceupdate");
        if (!access) {
            displayError(Lang.AccessToThisPageIsProhibited);
            return false;
        }
        var _url = 'get.aspx?op=GetHtml&table=StoreDraft&view=storedraftupdatebuyprice' + appendTime();
        var btns = {};
        btns[Lang.OK] = "OK";
        btns[Lang.Cancel] = "_CANCEL_";

        showPopup(_url, function (v, m, f) {
            if (v == "OK") {
                var more = '&FromDate=' + f.FromDate;
                more += '&ToDate=' + f.ToDate;
                more += '&CommodityID=' + f.CommodityID;
                more += '&BuyPrice=' + f.BuyPrice.replace(/\,/g, '');
                if (f.FromNum) {
                    more += '&FromNum=' + f.FromNum;
                } else {
                    more += '&FromNum=NULL';
               }
               if (f.ToNum) {
                   more += '&ToNum=' + f.ToNum;
               } else {
                   more += '&ToNum=NULL';
               }
                var ok = getServerValue('storedraftbuypriceupdate', 'StoreDraft', more);
                Growl.Notice(ok + Lang.NumOfAffectedRows, 4000);
            }
        }, btns);  
    });

    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['sales,salesinvoice', 'stores,storedraft']);
    checkLevel(2);
    storeMode = !hasAccess("links", "mainmenu", "accounting") && !hasAccess("links", "mainmenu", "sales");
    storeMode = storeMode || $.urlParam('menu') == 'storea';
    if (storeMode) {
        $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.StoreDraft, parentTitle: Lang.MStore, parentUrl: 'menu/stores.aspx' } }));
        setClassStore();
    }
    else {
        $("#bar").prepend(breadcrumb({pageInfo: {pageTitle: Lang.SalesInvoice, parentTitle: Lang.SellAndBuy, parentUrl: 'menu/sales.aspx'} }));
        setClassSales();
    }
    if (storeMode) {
        totalPriceColmunHideshow = true;
    } 
    dateSelect('.dateSelect');
    startupID = $.urlParam('id');
    if (!startupID)
        startupID = -1;
    run();
}