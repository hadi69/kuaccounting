﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-11-11: file created
*/
function view_purchaserequest() {
    makeChosen();
    $("#tableItems").smartTable();
    var cnt = 0, mID = -1;

    pagetitle = 'WFTPurchaseRequest';
    replacePrintButton({
        primary: {
            text: Lang.Print,
            href: Router.Generate("print-purchaserequest", { params: { ids: _Context.id, title: pagetitle }, query: { report: 'report', print: "true" } })
            , type: 3, mode: 1, id: _Context.id
        }
    });

    function updateTotal() {
        var sumPrice = 0, sumAmount = 0;
        $('.total').each(function (index, value) {
            sumPrice += toFloat($(value).val(), 0);
        });
        $('.amount').each(function (index, value) {
            sumAmount += toFloat($(value).val(), 0);
        });
        $('#TotalPrice').val(sumPrice.toFixed(_Context.decimalPoints));
        $('#TotalAmount').val(sumAmount);
        commaInputsSet();
    }
    function updatePrice(was) {
        var id = $(was).attr('id');
        id = id.substring(2); // d_1  -->  1
        var unitCarrency = toFloat($('#ucc_' + id).val(), 1);
        $('#t_' + id).val((toFloat($('#p_' + id).val(), 0) * toFloat($('#a_' + id).val(), 0) * unitCarrency).toFixed(_Context.decimalPoints)); // total
        updateTotal();
    }
    function autoPostPack(event, data, formatted) {
        var id = $(event.target).attr('id');
        $(event.target).next().val(data[0]); // CommodityID
        id = id.substring(id.indexOf('_') + 1); // d_1  -->  1
        $('#p_' + id).val(0); // price
        $('#a_' + id).val(1); // amount
        $('#t_' + id).val(0); // total
        updateTotal();
    }
    function setAutoCompletes() {
        $('.servicename').unautocomplete();
        $('.unitname').unautocomplete();
        $('#tableItems .servicename').each(function () {
            var id = $(this).attr('id');
            id = id.substring(id.indexOf('_') + 1);
            var unitName = $('#u_' + id);
            var servicePriceId = $('#spi_' + id).val();
            if (servicePriceId > 0) {
                var _autourl1 = 'get.aspx?op=Auto&table=Units&qd=purchaserequestunitsauto&ServicePriceID=' + servicePriceId;
                autoComplete('Units', 'unitsauto', unitName, "#justfurfun",
                    function (event, data, formatted) {
                        var id1 = $(event.target).attr('id');
                        id1 = id1.substring(id1.indexOf('_') + 1);
                        $(event.target).next().val(data[0]); // ID
                        if (data[2] && data[2] != 0) {
                            $('#a_' + id1).next().text('x')
                            $('#ucc_' + id1).val(data[2]); // CurrencyConverter}
                        }
                        else {
                            $('#a_' + id1).next().text(' ')
                            $('#ucc_' + id1).val('1');
                        }
                    }
                , null, null, null, _autourl1);
            }
        });
        var _autourl = 'get.aspx?op=Auto&table=PurchaseRequest&qd=purchaserequestservicepriceauto&StoreID=' + $('#StoreID').val() + appendTime();
        autoCompleteFree('Service', 'purchaserequestservicepriceauto', "#tableItems .servicename", "#justfurfun"
               , function (event, data, formatted) {
                   $(event.target).next().val(data[0]); // ID
                   var id = $(event.target).attr('id');
                   id = id.substring(id.indexOf('_') + 1);
                   $('#u_' + id).next().val(data[5]); // unitid
                   $('#u_' + id).val(data[6]); // unitname
                   var _autourl1 = 'get.aspx?op=Auto&table=Units&qd=purchaserequestunitsauto&ServicePriceID=' + data[0];
                   autoComplete('Units', 'unitsauto', "#tableItems tr:last .unitname", "#justfurfun",
                       function (event, data, formatted) {
                           var id1 = $(event.target).attr('id');
                           id1 = id1.substring(id1.indexOf('_') + 1);
                           $(event.target).next().val(data[0]); // ID
                           if (data[2] && data[2] != 0) {
                               $('#ucc_' + id1).val(data[2]); // CurrencyConverter}
                           }
                           else {
                               $('#ucc_' + id1).val('1');
                           }
                       }
                   , null, null, null, _autourl1);
               }
               , function (data, i, n, value) {
                   return data[1];
               }, null, null, _autourl);
    }
    function accessAutoCompletes() {
        //var more = getAccessID("store", "ID");
        //if (more != "") {
        //    var _autourl = 'get.aspx?op=Auto&table=Stores&qd=enumauto&more=' + more + appendTime();
        //    autoComplete('Stores', 'enumauto', "#StoreName", "#StoreID"
        //                    , autoPostPack
        //                    , function (data, i, n, value) {
        //                        return '<div dir=rtl><b>' + data[1] + '</b>';
        //                    }, null, null, _autourl);
        //}
        //else
        //autoComplete('Stores', 'storeauto', "#StoreName", "#StoreID");
        autoCompleteNew({
            table: 'Stores', qd: 'storeauto', textInput: "#StoreName", hiddenInput: "#StoreID"
          , postback: setAutoCompletes, allowNull: true
        });
        if (mID == -1)
            setDefault('Stores', 'firststore', "#StoreName", "#StoreID");
    }
    function addRow(node) {
        var row = '<tr>';
        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">'
            + (cnt + 1)
            + '</td>';

        row += '<td data-th="' + Lang.CommodityName + '">'
            + '<input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="text" id="c_' + cnt + '" name="ServiceName" value="' + getNodeVal(node, "ServiceName") + '" class="servicename form-control input-sm"/>'
            + '<input type="hidden" id="spi_' + cnt + '" name="ServicePriceID" value="' + getNodeVal(node, "ServicePriceID") + '" />'
            + '</td>';

        row += '<td data-th="' + Lang.Units + '">'
            + '<input type="text" id="u_' + cnt + '" name="UnitName" value="' + getNodeVal(node, "UnitName") + '" class="unitname form-control input-sm align-center" />'
            + '<input type="hidden" name="UnitID" value="' + getNodeVal(node, "UnitID") + '" />'
            + '</td>';

        /*row += '<td data-th="' + Lang.Num + '">'
            + '<input type="text" id="a_' + cnt + '" name="Amount" value="' + getNodeVal(node, "Amount") + '" class="amount form-control input-sm align-center" />'
            + '</td>';*/

        row += '<td data-th="' + Lang.Num + '">'
          + '<div class="input-group input-group-sm">'
          + '<input type="text" id="a_' + cnt + '" name="Amount" value="' + getNodeVal(node, "Amount", 0) + '" class="amount form-control width-100-percent min-width-50 align-center" />'
          + '<span class="input-group-addon no-left-border no-right-border">x</span>'
          + '<input readonly="readonly" size="1" id="ucc_' + cnt + '" name="UnitCurrencyConverter" value="' + getNodeVal(node, "UnitCurrencyConverter", 1) + '" class="form-control width-50 align-center" />'
          + '</div>'
          + '</td>';

        row += '<td data-th="' + Lang.Price + '">'
            + '<input type="text" id="p_' + cnt + '" name="Price" value="' + getNodeVal(node, "Price") + '" class="price monetary validate[custom[integer]] form-control input-sm align-center" />'
            + '</td>';

        row += '<td data-th="' + Lang.TotalPrice + '">'
            + '<input type="text" id="t_' + cnt + '" name="Total" value="' + (getNodeVal(node, "Price", 0) * getNodeVal(node, "Amount", 0) * toFloat(getNodeVal(node, "UnitCurrencyConverter", 1), 1)).toFixed(_Context.decimalPoints) + '"'
            + 'class="total monetary validate[custom[integer]] form-control input-sm align-center" readonly="readonly" />'
            + '</td>';

        row += '<td data-th="' + Lang.Comments + '">'
            + '<input type="text" id="co_' + cnt + '" name="Comment" value="' + getNodeVal(node, "Comment") + '"'
            + 'class="form-control input-sm" />'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a class="btn btn-danger btn-xs btn-block btn-remove remove" id="r_' + cnt + '">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }

    // ------------------------------
    // startup code
    // ------------------------------
    mID = toInt($('#MasterID').val(), -1);
   // processPrintButton(0, 3, mID);

    dateSelect('.dateSelect');
    if ($("#RequestDate").val() == '')
        $("#RequestDate").val(_Context.today);
    setValue("#WorkflowStatus");

    if (mID == -1) {
        $("#PersonnelID").val(_Context.personnelID);
        $("#PersonnelName").val(_Context.personnelName);
    }
    accessAutoCompletes();

    // DetailForm
    fillTable("get.aspx?op=GetXml&qd=purchaserequestitem&PurchaseRequestID=" + mID, '#tableItems', addRow);

    $('.price, .amount, .discount, .unitname').blur(function () { updatePrice(this); });
    setAutoCompletes();

    doMore();

    function doMore() {
        function removeRow($who) {
            var id = $who.attr('id'); // r_1
            id = id.substring(2); // r_1  -->  1
            $('#c_' + id).val(""); // CommodityName
            $('#c_' + id).next().val(-1); // TestDefinitionID
            $('#p_' + id).val(''); // price
            $('#a_' + id).val(-1); // price
            $who.parent().parent().hide();
            Growl.Notice(Lang.DeletedSuccessfully);
            updateTotal();
        }
        $('.remove').click(function () {
            removeRow($(this));
        });
        $('.add').click(function () {
            var table = $("#tableItems");
            $('tr:last', table).after(addRow(null));
            Growl.Notice(Lang.RowAddedSuccessfully);
            var lastRow = $('tr:last', table);
            setAutoCompletes();
            $('.remove', lastRow).click(function () {
                removeRow($(this));
            });
            $('.price, .amount, .discount, .unitname', lastRow).blur(function () { updatePrice(this); });
            commaInputs();
        });
        updateTotal();
        insertValidation();
        monetaryInputsSet(null, '#RequestDate');
        commaInputs();
    }

    if (mID > 0) {
        uploadify('#ChooseFile', '#FileName', '#StartUpload', 'workflow/purchaserequest/' + mID
                   , function (fileName) {
                       $('#FileNameDisplay').html(fileName);
                   });
        $('#workflowtasks').html(showWorkflowTasks(mID, WorkflowTypeEnum.PurchaseRequest));
    }
    else
        $('.transDiv').hide();
}