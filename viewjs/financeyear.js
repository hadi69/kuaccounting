﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
92-10-28: file created
*/
function view_financeyear() {
    // ------------------------------
    // startup code
    // ------------------------------
    dateSelect('.dateSelect');
    if ($("#StartDate").val() == '')
        $("#StartDate").val(_Context.today);
    setValue("#Status");
    insertValidation();
    makeChosen();
}