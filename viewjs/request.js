﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-11-11: file created
*/
function view_request() {
    makeChosen();
    $("#tableItems").smartTable();
    var cnt = 0, mID = -1;
    function setAutoCompletes() {
        var storeID = toInt($("#StoreID").val(), -1);
        $("#tableItems tr:last .storeservicename").unautocomplete();
        $('#tableItems .storeservicename').each(function () {
            var id = $(this).attr('id');
            id = id.substring(id.indexOf('_') + 1);
            var unitName = $('#u_' + id);
            var storeServiceId = $('#sid_' + id).val();
            if (storeServiceId > 0) {
                var _autourl1 = 'get.aspx?op=Auto&table=Units&qd=storedraftunitsauto&StoreServiceID=' + storeServiceId;
                autoComplete('Units', 'unitsauto', unitName, "#justfurfun",
                    function (event, data, formatted) {
                        var id1 = $(event.target).attr('id');
                        id1 = id1.substring(id1.indexOf('_') + 1);
                        $(event.target).next().val(data[0]); // ID
                        if (data[2] && data[2] != 0) {
                            //$('#a_' + id1).next().text('x')
                            $('#ucc_' + id1).val(data[2]); // CurrencyConverter}
                        }
                        else {
                            //$('#a_' + id1).next().text(' ')
                            $('#ucc_' + id1).val('1');
                        }
                    }
                , null, null, null, _autourl1);
            }
        });
        if ($("#tableItems tr:last .storeservicename").val() != '') {
            return;
        }
        var more = encodeURIComponent("StoreID=" + $("#StoreID").val());
        var _autourl = 'get.aspx?op=Auto&table=Request&qd=storeservicesauto&BuyerID=NULL&StoreID=' + storeID + appendTime();
        autoCompleteFree('StoreServices', 'storeservicesauto', "#tableItems .storeservicename", "#justfurfun"
                , function (event, data, formatted) {
                    $(event.target).next().val(data[0]); // ID
                    $(event.target).attr('title', data[17]); // StoreName
                    var id = $(event.target).attr('id');
                    id = id.substring(id.indexOf('_') + 1);
                    $('#u_' + id).next().val(data[6]); // unitid
                    $('#u_' + id).val(data[7]); // unitname
                    var _autourl1 = 'get.aspx?op=Auto&table=Units&qd=storedraftunitsauto&StoreServiceID=' + data[0];
                    autoComplete('Units', 'unitsauto', "#tableItems tr:last .unitname", "#justfurfun",
                        function (event, data, formatted) {
                            var id1 = $(event.target).attr('id');
                            id1 = id1.substring(id1.indexOf('_') + 1);
                            $(event.target).next().val(data[0]); // ID
                            if (data[2] && data[2] != 0) {
                                $('#a_' + id1).next().text('x')
                                $('#ucc_' + id1).val(data[2]); // CurrencyConverter}
                            }
                            else {
                                $('#a_' + id1).next().text(' ')
                                $('#ucc_' + id1).val('1');
                            }
                        }
                    , null, null, null, _autourl1);
                }
                , function (data, i, n, value) {
                    return formatService(data);
                }, null, null, _autourl);
        //autoCompleteFree('Units', 'enumauto', "#tableItems .unitname", "#justfurfun"
        //    , tableAutoPostPack
        //    , formatItemNameCode);
    }
    function addRow(node) {
        var row = '<tr>';
        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">'
            + (cnt + 1)
            + '</td>';

        row += '<td data-th="' + Lang.CommodityName + '">'
            + '<input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="text" id="se_' + cnt + '" title="' + getNodeVal(node, "StoreName") + '" name="StoreServiceName" value="' + getNodeVal(node, "StoreServiceName") + '" class="storeservicename form-control input-sm"/>'
            + '<input type="hidden" id="sid_' + cnt + '" name="StoreServiceID" value="' + getNodeVal(node, "StoreServiceID") + '" />'
            + '</td>';

        row += '<td data-th="' + Lang.Units + '">'
            + '<input type="text" id="u_' + cnt + '" name="UnitName" value="' + getNodeVal(node, "UnitName") + '" class="unitname align-center form-control input-sm" />'
            + '<input type="hidden" name="UnitID" value="' + getNodeVal(node, "UnitID") + '" />'
            + '</td>';

        /*row += '<td data-th="' + Lang.NumberOfApplicants + '">'
            + '<input type="text" id="r_' + cnt + '" name="NumRequest" value="' + getNodeVal(node, "NumRequest") + '"'
            + 'class="validate[custom[number]] numrequest form-control input-sm align-center" />'
            + '</td>';*/

        row += '<td data-th="' + Lang.NumberOfApplicants + '">'
          + '<div class="input-group input-group-sm">'
          + '<input type="text" id="r_' + cnt + '" name="NumRequest" value="' + getNodeVal(node, "NumRequest", 0) + '" class="numrequest validate[custom[number]] form-control width-100-percent min-width-50 align-center" />'
          + '<span class="input-group-addon no-left-border no-right-border">x</span>'
          + '<input readonly="readonly" size="1" id="ucc_' + cnt + '" name="UnitCurrencyConverter" value="' + getNodeVal(node, "UnitCurrencyConverter", 1) + '" class="form-control width-50 align-center" />'
          + '</div>'
          + '</td>';

        row += '<td data-th="' + Lang.NumberOfConfirmed + '">'
            + '<input type="text" id="a_' + cnt + '" name="NumConfirmed" value="' + getNodeVal(node, "NumConfirmed") + '"'
            + 'class="validate[custom[number]] form-control input-sm numconfirmed align-center" />'
            + '</td>';


        row += '<td data-th="' + Lang.UsePlace + '">'
            + '<input type="text" id="up_' + cnt + '" name="UsePlace" value="' + getNodeVal(node, "UsePlace") + '"'
            + 'class="form-control input-sm" />'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a class="btn btn-danger btn-xs btn-block btn-remove remove" id="r_' + cnt + '">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }

    // ------------------------------
    // startup code
    // ------------------------------
    setTimeout(function() {
        $('#WardName').focus();
    },3);
    mID = toInt($('#MasterID').val(), -1);
    processPrintButton(0, 1, mID);

    dateSelect('.dateSelect');
    if ($("#RequestDate").val() == '')
        $("#RequestDate").val(_Context.today);
    setValue("#WorkflowStatus");

    if (mID == -1) {
        $("#RequesterID").val(_Context.personnelID);
        $("#RequesterName").val(_Context.personnelName);
    }

    if (_Context.isAccountingVer)
        $('.noacc').hide();
    autoComplete('Ward', 'wardauto', '#WardName', '#WardID');
    autoComplete('Personnel', 'personauto', '#RequesterName', '#RequesterID', null
            , function (data, i, n, value) {
                $('#RequesterID').val('');
                return formatPersonnelItem(data);
            });
    //autoComplete('Stores', 'storeauto', '#StoreName', '#StoreID', setAutoCompletes);
    autoCompleteNew({
        table: 'Stores', qd: 'storeauto', textInput: "#StoreName", hiddenInput: "#StoreID"
            , postback: setAutoCompletes, allowNull: true
    })
    if (mID == -1)
    {
        setDefault('Stores', 'firststore', "#StoreName", "#StoreID");
    }
    autoComplete('Priority', 'enumauto', '#PriorityName', '#PriorityID');
    // DetailForm
    fillTable("get.aspx?op=GetXml&qd=requestitem&RequestID=" + mID, '#tableItems', addRow);
   
    setAutoCompletes();
    doMore();

    function doMore() {

        function removeRow($who) {
            var id = $who.attr('id'); // r_1
            id = id.substring(2); // r_1  -->  1
            $('#se_' + id).val(""); // CommodityName
            $('#se_' + id).next().val(-1); // CommodityID
            $who.parent().parent().hide();
            Growl.Notice(Lang.DeletedSuccessfully);
        }
        $('.remove').click(function () {
            removeRow($(this));
        });
        $('.add').click(function () {
            var table = $("#tableItems");
            $('tr:last', table).after(addRow(null));
            Growl.Notice(Lang.RowAddedSuccessfully);
            var lastRow = $('tr:last', table);
            setAutoCompletes();
            $('.remove', lastRow).click(function () {
                removeRow($(this));
            });
        });
        insertValidation();
    }

    if (mID > 0) {
        uploadify('#ChooseFile', '#FileName', '#StartUpload', 'workflow/request/' + mID
                   , function (fileName) {
                       $('#FileNameDisplay').html(fileName);
                   });
        $('#workflowtasks').html(showWorkflowTasks(mID, WorkflowTypeEnum.Request));
    }
    else
        $('.transDiv').hide();
}