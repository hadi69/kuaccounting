﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
93-05-14: file created
*/
function view_cooperationtypes() {
    function doCommand(com, grid) {
        generalCommand(com, grid,show);
    }
    function run() {
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        //alert(_url);
        $("#flexi").flexigrid
            (
            {
                url: _url,
                dataType: 'xml',
                colModel:
                [
                    { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'center', hide: true },
                    { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                    { display: Lang.Title, name: 'Title', sortable: true, css_class: 'width-250' },
                    { display: Lang.Code, name: 'Code', sortable: true, css_class: 'width-200'},
                    { display: Lang.Comments, name: 'Comments', sortable: true},
                    { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' },
                    { display: Lang.ColDel, name: 'Del', sortable: false, process: delLink, css_class: 'action-cell' }
                ],
                buttons:
                [
                    { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                    //{ name: Lang.GBEdit, bclass: 'edit', onpress: doCommand },
                    { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                    { separator: true },
                   // { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                    { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                ],
                searchitems: null,
                sortname: "Title",
                sortorder: "asc",
                usepager: true,
                //title: _Context.title,
                useRp: true,
                rp: gridRp,
                showTableToggleBtn: _options.gridToggle,
                width: _options.gridWidth,

                height: _options.gridHeight, resizable: _options.gridResizable,
                singleSelect: false,
                onDoubleClick: function (row, grid, options) {
                    var id = row.id.substring(3);
                    show(id);
                },
                onSuccess: function (grid) {
                    restoreSelectedRows('#flexi');
                }
            }
            );
    }
    function showcallback(v, m, f) {
        return newShowcallback(v, m, f, 'cooperationtypeupdate', 'cooperationtypeinsert', _Context.table, null, show);
    }
    function show(id, celDiv, successCallback) {

        _Context.id = id;
        saveSelectedRows('#flexi');
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }
    $(function () {
        checkAccess(['distribution,cooperationtype']);
        $('#ptitle').html("<a href='menu/sys.aspx'>" + Lang.MSys + "</a> :: " + _Context.title);
        $(document).ready(function () {
            $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.CooperationType, parentUrl: "menu/distribution.aspx", parentTitle: Lang.MDistribution } }));
        });
        setClassDistribution();
    });
    run();
}