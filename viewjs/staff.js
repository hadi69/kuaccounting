﻿function view_staff() {
        autoComplete('Gender', 'enumauto', '#GenderName', '#GenderID');
        autoComplete('City', 'enumauto', '#PlaceofbirthName', '#PlaceofbirthID');
        autoComplete('City', 'enumauto', '#IssuePlaceName', '#IssuePlaceID');
        autoComplete('EmploymentType', 'enumauto', '#EmploymentTypeName', '#EmploymentTypeID');
        autoComplete('Marital', 'enumauto', '#MaritalName', '#MaritalID');
        autoComplete('Ward', 'personnelwardauto', '#WardName', '#WardID');
        autoComplete('Post', 'enumsauto', '#PostName', '#PostID', null, null, null, "Enums.Name=N'Post'");
        autoComplete('Education', 'enumsauto', '#EducationName', '#EducationID', null, null, null, "Enums.Name=N'Education'");
        autoComplete('Category', 'enumsauto', '#CategoryName', '#CategoryID', null, null, null, "Enums.Name=N'Category'");
        autoComplete('InsuranceType', 'enumsauto', '#InsuranceTypeName', '#InsuranceTypeID', null, null, null, "Enums.Name=N'InsuranceType'");
        autoComplete('Nationality', 'enumsauto', '#NationalityName', '#NationalityID', null, null, null, "Enums.Name=N'Nationality'");
        autoComplete('UnTaxPerson', 'enumsauto', '#UnTaxPersonName', '#UnTaxPersonID', null, null, null, "Enums.Name=N'UnTaxPerson'");
        autoComplete('InformationType', 'enumsauto', '#InformationTypeName', '#InformationTypeID', null, null, null, "Enums.Name=N'InformationType'");
        autoComplete('WorkPlaceStatus', 'enumsauto', '#WorkPlaceStatusName', '#WorkPlaceStatusID', null, null, null, "Enums.Name=N'WorkPlaceStatus'");
        autoComplete('ContractType', 'enumsauto', '#ContractTypeName', '#ContractTypeID', null, null, null, "Enums.Name=N'ContractType'");
        autoComplete('PersonnelStatus', 'enumsauto', '#PersonnelStatusName', '#PersonnelStatusID', null, null, null, "Enums.Name=N'PersonnelStatus'");
        autoComplete('FishDefs', 'enumauto1', '#FishDefName', '#FishDefID');
        autoComplete('OrderDefs', 'enumauto1', '#OrderDefName', '#OrderDefID');
        autoComplete('Job', 'enumauto2', '#JobName', '#JobID'
            , function (event, data, formatted, textInput, hiddenInput) {
                debugger;
            $(event.target).next().val(data[0]);
            $(event.target).val(data[1] + ' : ' + data[2]);
        },function(data) {
            return data[1] + ' : ' + data[2];
        }, function (data) {
            return data[1] + ' : ' + data[2];
        });
        

        dateSelect('.dateSelect');

        if ($("#Sanawat").val() == "")
            $("#Sanawat").val(0);

        //click for new
        $("#newCity").click(function() {
            createNewItem('cityupdate', 'cityinsert', 'city', 'City', 'IssuePlaceName', 'IssuePlaceID');
        });
        $("#newWard").click(function() {
            createNewItem('sectionupdate', 'sectioninsert', 'section', 'Ward', 'WardName', 'WardID', 'section');
        });
        $("#newNationality").click(function () {
            createNewItem('enumvalueupdate', 'enumvalueinsert', 'enumvalue', 'EnumValues', 'NationalityName', 'NationalityID', null,'Nationality');
        });
        $("#newPost").click(function () {
            createNewItem('enumvalueupdate', 'enumvalueinsert', 'enumvalue', 'EnumValues', 'PostName', 'PostID', null, 'Post');
        });
        $("#newCategory").click(function () {
            createNewItem('enumvalueupdate', 'enumvalueinsert', 'enumvalue', 'EnumValues', 'CategoryName', 'CategoryID', null, 'Category');
        });
        $("#newJob").click(function () {
            createNewItem('enumupdate', 'enuminsert', 'enum', 'Job', 'JobName', 'JobID');
        });
        $("#newEmploymentType").click(function () {
            createNewItem('enumupdate', 'enuminsert', 'enum', 'EmploymentType', 'EmploymentTypeName', 'EmploymentTypeID');
        });
        $("#newEducation").click(function () {
            createNewItem('enumvalueupdate', 'enumvalueinsert', 'enumvalue', 'EnumValues', 'EducationName', 'EducationID', null, 'Education');
        });
        $("#newTaxPerson").click(function () {
            createNewItem('enumvalueupdate', 'enumvalueinsert', 'enumvalue', 'EnumValues', 'UnTaxPersonName', 'UnTaxPersonID', null, 'UnTaxPerson');
        });
        $("#newInsuranceType").click(function () {
            createNewItem('enumvalueupdate', 'enumvalueinsert', 'enumvalue', 'EnumValues', 'InsuranceTypeName', 'InsuranceTypeID', null, 'InsuranceType');
        });
        $("#newInformationType").click(function () {
            createNewItem('enumvalueupdate', 'enumvalueinsert', 'enumvalue', 'EnumValues', 'InformationTypeName', 'InformationTypeID', null, 'InformationType');
        });
        $("#newWorkPlaceStatus").click(function () {
            createNewItem('enumvalueupdate', 'enumvalueinsert', 'enumvalue', 'EnumValues', 'WorkPlaceStatusName', 'WorkPlaceStatusID', null, 'WorkPlaceStatus');
        });
        $("#newContractType").click(function () {
            createNewItem('enumvalueupdate', 'enumvalueinsert', 'enumvalue', 'EnumValues', 'ContractTypeName', 'ContractTypeID', null, 'ContractType');
        });
        $("#newPersonnelStatus").click(function () {
            createNewItem('enumvalueupdate', 'enumvalueinsert', 'enumvalue', 'EnumValues', 'PersonnelStatusName', 'PersonnelStatusID', null, 'PersonnelStatus');
        });
        $("#newPlaceofbirth").click(function () {
            createNewItem('cityupdate', 'cityinsert', 'city', 'City', 'PlaceofbirthName', 'PlaceofbirthID');
        });

        function createNewItem(updatequery, insertquery, qd, table, fieldname, fieldid, view, EnumName) {
            var oldID = _Context.id;
            function showcallback(v, m, f) {
                if (v == 'OK') {
                    generalOKScalar(v, m, f, updatequery, insertquery, table, '#detailForm');
                    if (!_Context.success)
                        return false;
                    $("#" + fieldname).val(f.Title);
                    $('#' + fieldid).val(_Context.resultID);
                }
                _Context.id = oldID;
            }
            _Context.id = -1;
            var _url = 'get.aspx?op=GetHtml&table=' + table + '&qd=' + qd + '&id=-1';
            if (view != null)
                _url += '&view=' + view;
            if (EnumName != null) {
                var EnumID = getServerValue('getenumid', 'Enums', '&Name=' + EnumName);
                _url += '&EnumID=' + EnumID;
            }
            _url += appendTime();
            showPopup(_url, showcallback, getButtons());
        }

        if ($("#JobID").val() >0) {
            var code = getServerValue("getjobcode", "Job", "ID=" + $("#JobID").val());
            $("#JobName").val($("#JobName").val() + ' : ' + code);
        }

        function checkAditionalInformation() {
            if ($('#AdditionalInformation').attr('checked')) {
                $('#AI').show();
            }
            else {
                $('#AI').hide();
            }
            $('#AdditionalInformation').click(function () {
                if ($(this).attr('checked')) {
                    $('#AI').show();
                }
                else {
                    $('#AI').hide();
                }
            });
        }

        checkAditionalInformation();
        insertValidation();

        //tabify();
}