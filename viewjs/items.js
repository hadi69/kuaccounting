﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
    94-09-12: file created
*/
function view_items() {
    function doCommand(com, grid) {
        generalCommand(com, grid, show);
    }
    function run() {
        var _url = 'service.asmx/GetList?table=' + _Context.table + '&qd=' + _Context.query + appendTime();
        $("#flexi").flexigrid
                (
                {
                    url: _url,
                    dataType: 'xml',
                    colModel:
                    [
                        { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'center', hide: true },
                        { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell'},
                        { display: Lang.Person, name: 'Person', sortable: true, css_class: 'width-90 align-center' },
                        { display: Lang.CellNumber, name: 'CellNo', sortable: true, css_class: 'width-90 align-center' },
                        { display: Lang.Message, name: 'Message', sortable: true },
                        { display: Lang.Status, name: 'Status', sortable: true, process: displayStatus, css_class: 'width-80 align-center' },
                        { display: Lang.CreationDate, name: 'AddDate', sortable: true, css_class: 'date-cell' },
                        { display: Lang.SentDate, name: 'SentDate', sortable: true, css_class: 'date-cell' },
                        { display: Lang.ColEdit, name: 'Edit', sortable: false, process: function (celDiv, id) { editLink(celDiv, id, show); }, css_class: 'action-cell' }
                    ],
                    buttons:
                    [
                        { name: Lang.GBNew, bclass: 'new', onpress: doCommand },
                        { name: Lang.GBDel, bclass: 'delete', onpress: doCommand },
                        { separator: true },
                        { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                    ],
                    searchitems: null,
                    sortname: "AddDate",
                    sortorder: "desc",
                    usepager: true,
                    useRp: true,
                    rp: gridRp,
                    showTableToggleBtn: gridToggle,
                    width: gridWidth,
                    height: gridHeight, resizable: gridResizable,
                    singleSelect: false,
                    onDoubleClick: function (row, grid, options) {
                        var id = row.id.substring(3);
                        show(id);
                    }
                });
    }
    function displayStatus(celDiv, id) {
        var _mode = parseInt($(celDiv).html(), 10);
        displaySmsStatus(celDiv, _mode);
    }
    function showcallback(v, m, f) {
        return newShowcallback(v, m, f, 'itemupdate', 'iteminsert', _Context.table, null, show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        var _url = 'get.aspx?op=GetHtml&table=' + _Context.table + '&qd=' + _Context.query + '&id=' + id + appendTime();
        showPopup(_url, showcallback, null, null, successCallback);
    }

    // ------------------------------
    // startup code
    // ------------------------------
    checkAccess(['distribution,offers']);
    $("#bar").prepend(breadcrumb({ pageInfo: { pageTitle: Lang.SmsQueue, parentTitle: Lang.Management, parentUrl: 'menu/admin.aspx' } }));
    setClassDistribution();
    run();
}