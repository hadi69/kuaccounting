﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
92-11-10:
    - combined with purchaseinvoice
92-10-29: 
    - file created
*/
function view_advertisement() {
    var cnt = 0, mID = -1;
    makeChosen();
    $("#tableItems").smartTable();

    function addGroup(node) {
        var row = '<tr>';

        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">' + (cnt + 1) + '</td>';

        row += '<td data-th="' + Lang.Groups + '">'
            + '<input type="hidden" name="ID" value="' + getNodeVal(node, "GroupingID", -1) + '" />'
            + '<input type="text" id="g_' + cnt + '" name="GroupingName" value="'
            + getNodeVal(node, "GroupingName") + '" class="groupingname form-control input-sm" />'
            + '<input type="hidden" name="GroupingID" value="' + getNodeVal(node, "GroupingID") + '" />'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a id="r_' + cnt + '" class="btn btn-danger btn-xs btn-block btn-remove removeGroup" href="#">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }

    function removeGroup($who) {
        var id = $who.attr('id'); // r_1
        id = id.substring(2); // r_1  -->  1
        //alert(id);
        $('#g_' + id).val(""); // 
        $('#g_' + id).next().val(-1); // 
        $who.parent().parent().hide();
        Growl.Notice(Lang.DeletedSuccessfully);
    }

    function groupAutoPostPack(event, data, formatted) {
        $(event.target).prev("input").val(data[0]);
        $(event.target).next().val(data[0]); // ID
    }

    function typeChange() {
        var type = toInt($('#Type').val(), 10);
        if (type >= 20 && type <= 29) {
            $('.car').show();
            $('.estate').hide();
        } else if (type >= 10 && type <= 19) {
            $('.car').hide();
            $('.estate').show();
        } else {
            $('.car').hide();
            $('.estate').hide();
        }
        $('#Type').change(function () {
            type = toInt($(this).val(), 10);
            if (type >= 20 && type <= 29) {
                $('.car').show();
                $('.estate').hide();
            } else if (type >= 10 && type <= 19) {
                $('.car').hide();
                $('.estate').show();
            } else {
                $('.car').hide();
                $('.estate').hide();
            }
        });
    }

    // ------------------------------
    // startup code
    // ------------------------------
    $('#Title').focus();
    setTimeout(function() {
        $('#ProvinceName').focus();
    }, 30);
    mID = toInt($('#MasterID').val(), -1);
    dateSelect('.dateselect');
    setValue('#Status');
    if (!$('#HiddenStatus').val())
        $('#HiddenStatus').val(1);
    setValue('#Type');
    setValue('#GeneralType');
    setValue('#CarFuelType');
    setValue('#CarGearbox');
    setValue('#CarBodyCondition');
    typeChange();
    setChecked('#EstateEvidence');
    setChecked('#EstateCountrySide');

    autoComplete('Province', 'enumauto', '#ProvinceName', '#ProvinceID'
        , function (e, data) {
            $('#ProvinceID').val(data[0]);
            var url = 'get.aspx?op=Auto&table=City&qd=cityauto&ProvinceID=' + data[0] + appendTime();
            $('#CityName').unautocomplete();
            $('#CityName').val('');
            $('#CityID').val('');
            autoComplete('City', 'cityauto', '#CityName', '#CityID', function (e1, data1) {
                $('#CityID').val(data1[0]);
                var url = 'get.aspx?op=Auto&table=TheNeighbourhood&qd=theneighbourhoodauto&CityID=' + data1[0] + appendTime();
                $('#TheNeighbourhoodName').unautocomplete();
                $('#TheNeighbourhoodName').val('');
                $('#TheNeighbourhoodID').val('');
                autoComplete('TheNeighbourhood', 'theneighbourhoodauto', '#TheNeighbourhoodName', '#TheNeighbourhoodID', null, null, null, null, url);
            }, null, null, null, url);
        });

    $('.addGroup').click(function () {
        var table = $("#tableGroup");
        var tableID = table.attr("id");
        $('tr:last', table).after(addGroup(null));
        var lastRow = $('tr:last', table);
        autoComplete('Grouping', 'groupadvertisementauto', "#tableGroup tr:last .groupingname", "#justfurfun"
               , groupAutoPostPack
               , function (data, i, n, value) {
                   return data[1];
               });
        $('.removeGroup').click(function () {
            removeGroup($(this));
        });
        Growl.Notice(Lang.RowAddedSuccessfully);
        $('input[name=GroupingName]', lastRow).focus();
    });

    fillTable("get.aspx?op=GetXml&qd=advertisementgrouping&AdvertisementID=" + mID, '#tableGroup', addGroup);
    $('.removeGroup').click(function () {
        removeGroup($(this));
    });

    autoComplete('Advertisement', 'autoyears', '#CarYear', '#justforfun');

    disableKeydownOnLastInputByName('GroupingName');
    insertValidation();
    commaInputs();
}