﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-12-04: file created
*/
function view_visitorplan() {

    //startup code

    if ($('#StartDate').val() && $('#EndDate').val()) {
        getCalendar();
    }
    autoComplete('Personnel', 'visitorauto', "#VisitorName", "#VisitorID");
    dateSelect('.dateSelect');
    insertValidation('#subDetailForm');


    $('.dateSelect').focus();
    $('#VisitorName').focus();
}