﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-11-23: file created
*/
function view_companyinfo() {
    var TypeEnum =
    {
        Text: 0,
        Bool: 1,
        Numeric: 2,
        TreePath: 3,
        MonetaryUnit: 4,
        PersonnelGroup: 5,
        CompanyInfo: 6
    }
    function getauto(more) {
        autoComplete('EnumValues', 'enumsauto', '#DisplayValue', '#Value', null, null, null, more);
    }
    var mType = TypeEnum.Text;
    var more = "";
    // ------------------------------
    // startup code
    // ------------------------------
    setChecked("#Hidden");
    //if ($("#Hidden").attr("checked") == "checked" || $("#Hidden").attr("checked") == true) {
    //    $('#Value').val('');
    //}
    mType = toInt($('#Type').val(), 0);
    var Name = $('#Name', "#detailForm").val();
    $('#DisplayValue').hide();
    if (Name == "PayerType") {
        $('#DisplayValue').show();
        $('#Value').hide();
        more = "Name=N'PersonType'";
        getauto(more);
    }
    else if (Name == "LegalPayerType") {
        $('#DisplayValue').show();
        $('#Value').hide();
        more = "Name=N'LegalPersonType'";
        getauto(more);
    }
    else if(Name=="Signature1"||Name=="Signature2")
    {
        $('#DisplayValue').show();
        $('#Value').hide();
        autoComplete('Personnel', 'staffauto', '#DisplayValue', '#Value');
    }
    insertValidation();
}