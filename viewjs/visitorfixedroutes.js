﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />
function view_visitorfixedroutes() {
    $("#tableGroup").smartTable();
    var cnt = 0, mID = -1;
    function addGroup(node) {
        var row = '<tr>';

        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">'
            + (cnt + 1)
            + '</td>';

        row += '<td data-th="' + Lang.FirstGroup + '">'
            + '<input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="text" id="fg_' + cnt + '" name="FGroupingName" value="'
            + getNodeVal(node, "FGroupingName") + '" class="fgroupingname form-control input-sm" />'
            + '<input type="hidden" name="FGroupingID" value="' + getNodeVal(node, "FGroupingID") + '" />'
            + '</td>';

        row += '<td data-th="' + Lang.SecondGroup + '">'
           + '<input type="text" id="sg_' + cnt + '" name="SGroupingName" placeholder="' + Lang.SGroupingNamePlaceHolder + '" value="'
            + getNodeVal(node, "SGroupingName") + '" class="sgroupingname form-control input-sm" />'
            + '<input type="hidden" name="SGroupingID" value="' + getNodeVal(node, "SGroupingID") + '" />'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a id="r_' + cnt + '" class="btn btn-danger btn-xs btn-block btn-remove removeGroup" href="#">'
            + '<span class="visible-xs-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }
    function autoPostPack(event, data, formatted) {
        $(event.target).next().val(data[0]); // ID
    }
    function removeGroup($who) {
        var id = $who.attr('id'); // r_1
        id = id.substring(2); // r_1  -->  1
        //alert(id);
        $('#fg_' + id).val(""); // 
        $('#fg_' + id).next().val(-1); // 
        $('#sg_' + id).val(""); // 
        $('#sg_' + id).next().val(-1);
        $who.parent().parent().hide();
        Growl.Notice(Lang.DeletedSuccessfully);

    }

    function bindAdd() {
        $('.addGroup').click(function () {
            addGroupClick();
        });
    }

    function bindRemove() {
        $('.removeGroup').click(function () {
            var element = $(this);
            doConfirm(function () {
                removeGroup(element);
            }, Lang.ConfirmDelete);
        });
    }

    function doCommand(com, grid) {
        alert(Lang.DeleteDaylyCustomerMsg);
        _Context.table = 'visitorfixedcustomers';
        generalCommand(com, grid, show1);
        setTimeout(function () {
            $("#flexiServices").flexReload();
        }, 3500);
    }

    function show1() {
        return 1;
    }

    var _url = 'service.asmx/GetQueryList?table=VisitorFixedRoutes&qd=visitorfixedcustomers&VisitorFixedRoutesID=' + $('#MasterID').val() + appendTime();

    $("#flexiServices").flexigrid
               (
               {
                   url: _url,
                   dataType: 'xml',
                   colModel:
                   [
                       { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                       { display: 'CustomersID', name: 'CustomersID', width: 10, sortable: false, align: 'left', hide: true },
                       { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                       { display: Lang.Code, name: 'Code', width: 10, sortable: false, css_class: 'width-100' },
                       { display: Lang.CustomerName, name: 'CustomerName', sortable: true, css_class: 'width-150' },
                       { display: Lang.FirstGroup, name: 'FGroupName', sortable: true, css_class: 'width-200' },
                       { display: Lang.SecondGroup, name: 'SGroupName', sortable: true, css_class: 'width-200' },
                       { display: Lang.Address, name: 'Address', sortable: true }
                   ],
                   buttons:
                   [
                   { name: Lang.GBDel, bclass: 'delete', onpress: doCommand }
                   ],
                   searchitems: null,
                   sortname: "CustomerName",
                   sortorder: "asc",
                   usepager: true,
                   //title: _Context.title,
                   useRp: true,
                   rp: gridRp,
                   showTableToggleBtn: _options.gridToggle,
                   width: _options.gridWidth,
                   height: 270,
                   singleSelect: false,
               }
               );

    function addGroupClick() {
        var table = $('#tableGroup');
        var tableID = table.attr("id");
        $('tr:last', table).after(addGroup(null));
        Growl.Notice(Lang.RowAddedSuccessfully);
        var lastRow = $('tr:last', table);
        autoComplete('Grouping', 'groupauto', "#tableGroup tr:last .fgroupingname", "#justfurfun"
           , autoPostPack
           , function (data, i, n, value) {
               return data[1];
           });
        autoComplete('Grouping', 'groupauto', "#tableGroup tr:last .sgroupingname", "#justfurfun"
            , autoPostPack
            , function (data, i, n, value) {
                return data[1];
            });
        $('.removeGroup').click(function () {
            var element = $(this);
            doConfirm(function () {
                removeGroup(element);
            }, Lang.ConfirmDelete);
        });
        $('.sgroupingname').unbind('keyup');
        $('#tableGroup tr:last .sgroupingname').unbind('keyup');
        $('#tableGroup tr:last .sgroupingname').keyup(function (e) {
            if (e.keyCode === 37) {
                selectMultiGroups();
            }
        });
    }

    function selectMultiGroups() {
        var _url = 'get.aspx?op=GetHtml&table=Grouping&view=multigroupselect' + appendTime();
        var btns = {};
        btns[Lang.OK] = "OK";
        btns[Lang.Cancel] = "_CANCEL_";
        showPopup(_url, function (v, m, f) {
            if (v == "OK") {
                var GIDs = f.SGroupIDs.split(',');
                var Gnames = f.SGroupNames.split(',');
                var fgName = $('#tableGroup tr:last .fgroupingname').val();
                var fgID = $('#tableGroup tr:last .fgroupingname').next().val();
                $('#tableGroup tr:last .sgroupingname').val(Gnames[0]);
                $('#tableGroup tr:last .sgroupingname').next().val(GIDs[0]);
                for (var i = 1; i < GIDs.length; i++) {
                    addGroupClick();
                    $('#tableGroup tr:last .fgroupingname').val(fgName);
                    $('#tableGroup tr:last .fgroupingname').next().val(fgID);
                    $('#tableGroup tr:last .sgroupingname').val(Gnames[i]);
                    $('#tableGroup tr:last .sgroupingname').next().val(GIDs[i]);
                }
            }
        }, btns);
    }

    //----------------
    // startup code
    //------------------
    setTimeout(function() {
        $('#VisitorName').focus();
    }, 3);
    mID = toInt($('#MasterID').val(), -1);
    fillTable("get.aspx?op=GetXml&qd=visitorfixedroutesItem&VisitorFixedRoutesID=" + mID, '#tableGroup', addGroup);
    function doMore() {
        autoComplete('Grouping', 'groupauto', "#tableGroup .fgroupingname", "#FGroupingID"
               , autoPostPack
               , function (data, i, n, value) {
                   return data[1];
               });
        autoComplete('Grouping', 'groupauto', "#tableGroup .sgroupingname", "#SGroupingID"
       , autoPostPack
       , function (data, i, n, value) {
           return data[1];
       });
        bindAdd();
        bindRemove();
        dateSelect('.dateSelect');
        insertValidation('#subDetailForm');
        autoComplete('Personnel', 'visitorauto', '#VisitorName', '#VisitorID');

    }
    doMore();
}