﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
93-08-24: file created
*/
function view_saletype() {
    var mID = -1;
    // ---------------------
    // Add Rows
    // ---------------------
    var cnt = 0;
    function addSaleTypeDiscount(node) {
        var row = '<tr>';
        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">'
            + (cnt + 1)
            + '</td>';

        row += '<td data-th="' + Lang.ServiceGroup + '">'
            + '<input type="hidden" name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="text" id="sg_' + cnt + '" name="ServiceGroupName" value="'
            + getNodeVal(node, "ServiceGroupName") + '" class="servicegroupname form-control input-sm" />'
            + '<input type="hidden" name="ServiceGroupID" value="' + getNodeVal(node, "ServiceGroupID") + '" />'
            + '</td>';

        row += '<td data-th="' + Lang.PersonnelGroup + '">'
           + '<input type="text" id="pg_' + cnt + '" name="PersonnelGroupName" value="'
            + getNodeVal(node, "PersonnelGroupName") + '" class="personnelgroupname form-control input-sm" />'
            + '<input type="hidden" name="PersonnelGroupID" value="' + getNodeVal(node, "PersonnelGroupID") + '" />'
            + '</td>';

        row += '<td data-th="' + Lang.DiscountPercent + '">'
            + '<div class="input-group input-group-sm">'
            + '<input type="text" id="d_' + cnt + '" name="discount" value="' + numberWithCommas(getNodeVal(node, "Discount"))
            + '" class="validate[custom[number]] form-control align-center Discount" />'
            + '<span class="input-group-addon">%</span>'
            + '</div>'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a class="btn btn-danger btn-xs btn-block btn-remove removesaletypediscount" id="r_' + cnt + '">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }

    function autoPostPack(event, data, formatted) {
        $(event.target).next().val(data[0]); // ID
    }

    function addSaleTypeDiscountClick() {
        var table = $('#tableSaleTypeDiscount');
        var tableID = table.attr("id");
        $('tr:last', table).after(addSaleTypeDiscount(null));
        Growl.Notice(Lang.RowAddedSuccessfully);
        var lastRow = $('tr:last', table);
        autoComplete('Grouping', 'groupserviceauto', "#tableSaleTypeDiscount tr:last .servicegroupname", "#justfurfun"
           , autoPostPack
           , function (data, i, n, value) {
               return data[1];
           });
        autoComplete('Grouping', 'grouppersonauto', "#tableSaleTypeDiscount tr:last .personnelgroupname", "#justfurfun"
            , autoPostPack
            , function (data, i, n, value) {
                return data[1];
            });
        $('.removesaletypediscount', lastRow).click(function () {
            var element = $(this);
            doConfirm(function () {
                removeSaleTypeDiscount(element);
            }, Lang.ConfirmDelete);
        });
    }

    function removeSaleTypeDiscount($who) {
        var id = $who.attr('id'); // r_1
        id = id.substring(2); // r_1  -->  1
        //alert(id);
        $('#sg_' + id).val(""); // 
        $('#sg_' + id).next().val(-1); // 
        $('#pg_' + id).val(""); // 
        $('#pg_' + id).next().val(-1);
        $who.parent().parent().hide();
        Growl.Notice(Lang.DeletedSuccessfully);
    }

    function checkIsDiscount() {
        if ($('#IsDiscount').attr('checked')) {
            $('.saletypediscountdiv').show(); 
            $('.mainPG').hide();
        }
        else {
            $('.saletypediscountdiv').hide();
            $('.mainPG').show();
        }
        $('#IsDiscount').click(function () {
            if ($(this).attr('checked')) {
                $('.saletypediscountdiv').show();
                $('.mainPG').hide();
            }
            else {
                $('.saletypediscountdiv').hide();
                $('.mainPG').show();
            }
        });
    }
    // ------------------------------
    // startup code
    // ------------------------------
    $('.popupTitle').html(_Context.title);
    setChecked("#IsDefault");
    setChecked("#IsDiscount");
    setValue('#Type');
    checkIsDiscount();
    autoComplete('Grouping', 'grouppersonauto', '#GroupingName', '#GroupingID');
    if ($("#IsDefault").attr('checked')) {
        $('#GroupingName').attr('disabled', true);
        //  $('#Type').attr('disabled', true);
    }
    mID = toInt($('#MasterID').val(), -1);
    fillTable("get.aspx?op=GetXml&qd=saletypediscount&SaleTypeID=" + mID, '#tableSaleTypeDiscount', addSaleTypeDiscount);

    function doMore() {
        autoComplete('Grouping', 'groupserviceauto', "#tableSaleTypeDiscount .servicegroupname", "#justfurfun"
        , autoPostPack
        , function (data, i, n, value) {
        return data[1];
        });
        autoComplete('Grouping', 'grouppersonauto', "#tableSaleTypeDiscount .personnelgroupname", "#justfurfun"
            , autoPostPack
            , function (data, i, n, value) {
                return data[1];
            });
        $('.addSaleTypeDiscount').click(function () {
            addSaleTypeDiscountClick();
        });
        $('.removesaletypediscount').click(function () {
            var element = $(this);
            doConfirm(function () {
                removeSaleTypeDiscount(element);
            }, Lang.ConfirmDelete);
        });
    }

    doMore();
    insertValidation();
}