﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />

/*
93-02-08: file created
*/
function view_styles() {
    function doCommand(com, grid) {
    }
    function run() {
        runQuery("ensurestyle", "style", "&UserID=" + _Context.userID, true);
        var _url = 'service.asmx/GetList?table=style&qd=' + _Context.query + appendTime();
        $("#flexi").flexigrid
                (
                {
                    url: _url,
                    dataType: 'xml',
                    colModel:
                    [
                        { display: 'ID', name: 'ID', width: 10, sortable: false, align: 'left', hide: true },
                        { display: Lang.ColRowNr, name: 'ZeilNummer', sortable: false, css_class: 'row-cell' },
                        { display: Lang.Title, name: 'Title', sortable: true},
                        { display: Lang.FontFamily, name: 'FontFamily', sortable: true, css_class: 'width-200 align-center' },
                        { display: Lang.FontSize, name: 'FontSize', sortable: true, css_class: 'width-100 align-center' },
                        { display: Lang.Color, name: 'Color', sortable: true, css_class: 'width-60 align-center', process: showColor },
                        { display: Lang.CssStyle, name: 'Value', sortable: true, css_class: 'width-400 align-center' },
                        { display: Lang.ColEdit, name: 'Edit', sortable: false, css_class: 'action-cell', process: function (celDiv, id) { editLink(celDiv, id, show); } }
                    ],
                    buttons:
                    [
                        { name: Lang.GBEdit, bclass: 'edit', onpress: doCommand },
                        { separator: true },
                        { name: Lang.GBPrint, bclass: 'print', onpress: doCommand },
                        { name: Lang.GBExcel, bclass: 'excel', onpress: doCommand }
                    ],
                    searchitems: null,
                    sortname: "SortIndex",
                    sortorder: "asc",
                    usepager: true,
                    useRp: true,
                    rp: gridRp,
                    showTableToggleBtn: _options.gridToggle,
                    width: _options.gridWidth,
                    height: _options.gridHeight, resizable: _options.gridResizable,
                    singleSelect: true,
                    onDoubleClick: function (row, grid, options) {
                        var id = row.id.substring(3);
                        show(id);
                    },
                    onSuccess: function (grid) {
                        restoreSelectedRows('#flexi');
                    }
                }
                );
    }
    function showcallback(v, m, f) {
        if (v == 'Reset') {
            newShowcallback('OK', m, f, 'stylereset', 'styleinsert', _Context.table, null, show);
            show(_Context.id);
        }
        return newShowcallback(v, m, f, 'styleupdate', 'styleinsert', _Context.table, null, show);
    }
    function show(id, celDiv, successCallback) {
        _Context.id = id;
        saveSelectedRows('#flexi');
        var _url = 'get.aspx?op=GetHtml&table=style&qd=style&id=' + id + appendTime();
        var btns = getDefaultButtons();
        if (id != -1)
            btns[Lang.Reset] = 'Reset';
        showPopup(_url, showcallback, btns, null, function () {
            jscolor.init();
            if (successCallback)
                successCallback();
        });
    }
    function setQuickMenu() {
    }
    function setPTitle() {
        $('#ptitle').html(_Context.title);
    }
    // ------------------------------
    // startup code
    // ------------------------------
    setQuickMenu();
    setPTitle();
    run();
}