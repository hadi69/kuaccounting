﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
92-11-12: 
    - file created
*/
function view_fishdef() {
    $('#tableItems').smartTable();

    var cnt = 0, mID = -1;
    function autoPostPack(event, data, formatted) {
        $(event.target).next().val(data[0]); // FieldID
    }

    function addRow(node) {
        // id="x" --> the validationEngine requires ID
        var readonly = node != null ? " readonly='readonly' " : "";
        var row = '<tr>';

        row += '<td class="row-cell" data-th="' + Lang.ColRowNr + '">'
            + (cnt + 1)
            + '</td>';

        row += '<td data-th="' + Lang.Field + '">'
            + '<input type="hidden" id=fid_' + cnt + ' name="ID" value="' + getNodeVal(node, "ID", -1) + '" />'
            + '<input type="text" id="f_' + cnt + '" name="FieldName" value="' + getNodeVal(node, "FieldName") + '"'
            + 'class="fieldname form-control input-sm" ' + readonly + ' />'
            + '<input type="hidden" name="FieldID" value="' + getNodeVal(node, "FieldID") + '" />'
            + '</td>';

        row += '<td data-th="' + Lang.DefaultValue + '">'
            + '<input type="text" id="de_' + cnt + '" name="DefaultValue" value="' + getNodeVal(node, "DefaultValue") + '" class="defaultvalue form-control input-sm" />'
            + '</td>';

        row += '<td data-th="' + Lang.Formula + '">'
            + '<input type="text" id="fo_' + cnt + '" name="Formula" value="' + getNodeVal(node, "Formula") + '" class="formula form-control input-sm" />'
            + '</td>';

        row += '<td data-th="' + Lang.Minimum + '">'
            + '<input type="text" id="mi_' + cnt + '" name="Minimum" value="' + getNodeVal(node, "Minimum") + '" class="minimum form-control input-sm" />'
            + '</td>';

        row += '<td data-th="' + Lang.Maximum + '">'
            + '<input type="text" id="ma_' + cnt + '" name="Maximum" value="' + getNodeVal(node, "Maximum") + '" class="maximum form-control input-sm" />'
            + '</td>';

        row += '<td data-th="' + Lang.Topic + '">'
            + '<input type="text" id="t_' + cnt + '" name="OWeTopicTitle" value="' + getNodeVal(node, "OweTopicTitle") + '" class="owetopictitle form-control input-sm" />'
            + '<input type="hidden" name="OweTreePath" value="' + getNodeVal(node, "OweTreePath") + '" />'
            + '</td>';

        row += '<td data-th="' + Lang.Topic + '">'
            + '<input type="text" id="t_' + cnt + '" name="DueTopicTitle" value="' + getNodeVal(node, "DueTopicTitle") + '" class="duetopictitle form-control input-sm" />'
            + '<input type="hidden" name="DueTreePath" value="' + getNodeVal(node, "DueTreePath") + '" />'
            + '</td>';

        row += '<td class="align-center" data-th="' + Lang.IsOwe + '">'
            + '<input type="checkbox" id="o_' + cnt + '" name="IsOwe' + cnt + '" '
            + (getNodeVal(node, "IsOwe", '') == '1' || getNodeVal(node, "IsOwe", '') == 'True' ? 'checked=checked' : '') + ' />'
            + '</td>';

        row += '<td class="align-center" data-th="' + Lang.IsOwe + '">'
            + '<input type="checkbox" id="bp_' + cnt + '" name="BasedOnPerson' + cnt + '" '
            + (getNodeVal(node, "BasedOnPerson", '') == '1' || getNodeVal(node, "BasedOnPerson", '') == 'True' ? 'checked=checked' : '') + ' />'
            + '</td>';

        row += '<td class="align-center action-remove">'
            + '<a class="btn btn-danger btn-xs btn-block btn-remove remove" id="r_' + cnt + '">'
            + '<span class="visible-xs-inline-block visible-sm-inline-block margin-5-left"> ' + Lang.ColDel + ' </span>'
            + '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'
            + '</a>'
            + '</td>';

        row += '</tr>';
        cnt++;
        return row;
    }

    // ------------------------------
    // startup code
    // -----------------------------
    mID = toInt($('#MasterID').val(), -1);
    // DetailForm
    if (hasValuePositive(mID)) {
        fillTable("get.aspx?op=GetXml&qd=fishdeffield&FishDefID=" + mID, '#tableItems', addRow);
        var more = "Used=1";
        autoCompleteFree('Fields', 'enumauto1', "#tableItems .fieldname", "#justfurfun"
                , autoPostPack, null, null, more);
        autoComplete('Trees', 'treesauto', "#tableItems .owetopictitle", "#justfurfun"
           , autoPostPack
           , function (data, i, n, value) {
               return data[1];
           });
        autoComplete('Trees', 'treesauto', "#tableItems .duetopictitle", "#justfurfun"
           , autoPostPack
           , function (data, i, n, value) {
               return data[1];
           });
    }
    function removeRow($who) {
        var id = $who.attr('id'); // r_1
        id = id.substring(id.indexOf('_') + 1); // r_1  -->  1

        doConfirm(function() {
            if (runQuery("fishdeffielddelete", "FishDefFields", "&ID=" + $('#fid_' + id).val())) {
                $('#f_' + id).val(""); // FieldName
                $('#f_' + id).next().val(-1); //FieldID
                $('#mi_' + id).val('');
                $who.parent().parent().remove();
                $who.parent().parent().find(':input').removeClass();
                Growl.Error(Lang.DeletedSuccessfully);
            }
        }, Lang.ConfirmDelete);
        
    }
    $('.remove').click(function () {
        removeRow($(this));
    });

    function doMore() {
        $('.add').click(function () {
            Growl.Notice(Lang.RowAddedSuccessfully);
            disableKeydownOnLastInputByName('FieldName');
            var table = $('#tableItems');
            $('tr:last', table).after(addRow(null));
            var lastRow = $('tr:last', table);
            $('.remove', lastRow).click(function () {
                removeRow($(this));
            });
            var more="Used IN (1,2)";
            autoCompleteFree('Fields', 'enumauto1', "#tableItems tr:last .fieldname", "#justfurfun"
                , autoPostPack, null, null, more);
            autoCompleteTreePath2('#TopicCode', "#tableItems tr:last .owetopictitle", '#justfurfun');
            autoCompleteTreePath2('#TopicCode', "#tableItems tr:last .duetopictitle", '#justfurfun');

        });
        insertValidation();
    }
    doMore();
    $('.owetopictitle').focusout(function () {
        if ($(this).val()=="") {
            $(this).next().val('');
        }
    });
    $('.duetopictitle').focusout(function () {
        if ($(this).val() == "") {
            $(this).next().val('');
        }
    });
    disableKeydownOnLastInputByName('FieldName');
}