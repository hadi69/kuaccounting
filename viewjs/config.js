﻿/// <reference path="~/js/jquery-1.4.1-vsdoc.js" />
/// <reference path="~/js/plugins.js" />
/// <reference path="~/js/site.js" />
/// <reference path="~/js/lang/fa.js" />

/*
    92-11-23: file created
*/
function view_config() {
    var TypeEnum =
    {
        Text: 0,
        Bool: 1,
        Numeric : 2,
        TreePath: 3,
        MonetaryUnit: 4,
        PersonnelGroup: 5,
        CompanyInfo: 6,
        Image: 7,
        Personnel: 8,
        Cooperation: 10,
        TopicGroup: 11,
        Grouping: 12,
        Color: 14
    }
    var mType = TypeEnum.Text;

    // ------------------------------
    // startup code
    // ------------------------------
    setChecked("#Hidden");
    //if ($("#Hidden").attr("checked") == "checked" || $("#Hidden").attr("checked") == true) {
    //    $('#Value').val('');
    //}
    mType = toInt($('#Type').val(), 0);
    $('#DisplayValue').hide();
    $('#TypeImage').hide();
    
    if (mType == TypeEnum.TreePath)
    {
        $('#DisplayValue').show();
        $('#Value').hide();
        autoComplete('Trees', 'treesallauto', '#DisplayValue', '#Value', null);
    }
    else if (mType == TypeEnum.MonetaryUnit) {
        $('#DisplayValue').show();
        $('#Value').hide();
        var hasVoucher = getServerValue('numofdocumentaccounting', 'DocumentAccounting');
        if (toInt(hasVoucher, 0) <= 0) {
            autoComplete('MonetaryUnit', 'enumauto', '#DisplayValue', '#Value', null);
        } else {
            $('#DisplayValue').attr('readonly', 'readonly');
            Growl.Notice(Lang.WeHaveSomeVoucher, 3000);
        }
    }
    else if (mType == TypeEnum.PersonnelGroup) {
        $('#DisplayValue').show();
        $('#Value').hide();
        autoComplete('PersonnelGroup', 'enumauto1', '#DisplayValue', '#Value', null);
    }
    else if (mType == TypeEnum.Grouping) {
        $('#DisplayValue').show();
        $('#Value').hide();
        autoComplete('Grouping', 'groupauto', '#DisplayValue', '#Value', null);
    }
    else if (mType == TypeEnum.Image) {
        $('#TypeImage').show();
        $('#Value').hide();
        uploadify('#ChooseFile', '#Value', '#StartUpload', 'config');
    } else if (mType == TypeEnum.Personnel) {
        $('#DisplayValue').show();
        $('#Value').hide();
        autoComplete('Personnel', 'personauto', '#DisplayValue', '#Value', null);
    } else if (mType == TypeEnum.Cooperation) {
        $('#DisplayValue').show();
        $('#Value').hide();
        autoComplete('CooperationType', 'enumauto1', '#DisplayValue', '#Value', null);
    } else if (mType == TypeEnum.TopicGroup) {
        $('#DisplayValue').show();
        $('#Value').hide();
        autoComplete('TopicGroup', 'enumauto1', '#DisplayValue', '#Value', null);
    } else if (mType == TypeEnum.Color) {
        $('#Value').addClass('color');
        $('#Value').attr('style', 'background-color: #' + $('#Value').val() + ';');
        jscolor.init();
        $('#Display').focus();
       // $('#Value').focus();
    }
    insertValidation();
}