﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="storeuserserviceprices.aspx.cs" Inherits="storeuserserviceprices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" Runat="Server">
    <input type="hidden" runat="server" id="mMsg" class="mMsg" />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container">
            <div class="form-group">
                <div class="col-xs-12 col-sm-8 form-col-without-padding">
                    <asp:Button runat="server" ID="doSavePrice1" OnClick="doSavePrice_Click" Text="Lang.SaveQuery (F2)" CssClass="btn btn-primary doSavePrice" />
                </div>
            </div>
        </div>
    </div>
    <asp:PlaceHolder runat="server" ID="mH"></asp:PlaceHolder>
    <script>

        $(function () {
            var msg = $('.mMsg').val();
            if (msg)
                Growl.Notice(msg);
        });
    </script>
</asp:Content>

