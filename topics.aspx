﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="topics.aspx.cs" Inherits="topics" %>

<%@ Register Src="controls/TopicProxy.ascx" TagName="TopicProxy" TagPrefix="uc1" %>
<%@ Register Src="controls/TopicChooser.ascx" TagName="TopicChooser" TagPrefix="uc1" %>
<asp:Content runat="server" ID="headContent" ContentPlaceHolderID="head">
    <style>
        .btn-danger.disabled, .btn-danger[disabled], fieldset[disabled] .btn-danger, .btn-danger.disabled:hover, .btn-danger[disabled]:hover, fieldset[disabled] .btn-danger:hover, .btn-danger.disabled:focus, .btn-danger[disabled]:focus, fieldset[disabled] .btn-danger:focus, .btn-danger.disabled:active, .btn-danger[disabled]:active, fieldset[disabled] .btn-danger:active, .btn-danger.disabled.active, .btn-danger[disabled].active, fieldset[disabled] .btn-danger.active {
            background-color: #CBB7B7!important;
            border-color: #B5B3B3!important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div class="row margin-10-top">
        <div class="col-xs-12 col-md-7">
            <div class="align-right small-page-title">
                <asp:Label ID="mSelectedTitle" runat="server" Text="Lang.TopicsM1" />
            </div>
        </div>
        <div class="col-xs-12 col-md-5">
            <div class="pull-left">
                <input type="button" class="btn btn-sm btn-primary" id="DeclareTopic"/>
                <asp:Label runat="server" ID="mCopyModeLbl" CssClass="red" Text='Lang.CopySubAccountsChooseDestinationAccount' Visible="false" />
                <asp:Button ID="doEdit" runat="server" CssClass="btn btn-xs btn-primary doEdit" Text="Lang.TopicEdit alt+e" OnClick="doEdit_Click" />
                <asp:Button ID="doCopy" runat="server" CssClass="Button doCopy" Text="Lang.CopySubAccounts" OnClick="doCopy_Click" Visible="false" />
                <asp:Button ID="doChange" runat="server" CssClass="Button doChange" Text="Lang.ExchangeAccounts" OnClick="doChange_Click" Visible="false" />
                <asp:Button runat="server" ID="doAdd" Visible="False" Text='Lang.NewTopic alt+n' CssClass="btn btn-xs btn-primary doAdd" OnClick="doAdd_Click" />
                <asp:Button runat="server" ID="doDelete" Text='Lang.DeleteTopic alt+d' CssClass="btn btn-danger btn-xs doDelete" OnClick="doDelete_Click" />
                <asp:Button ID="doUpdateOweDue" runat="server" CssClass="btn btn-xs btn-primary refresh" Visible="False"  OnClick="doUpdateOweDue_Click"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="dashed margin-10-top"></div>
        </div>
    </div>
    <uc1:TopicProxy ID="mTopicProxy" runat="server" Visible="False" />
    <uc1:TopicChooser runat="server" ID="mTopicChooser" Visible="False" />
    <div class="row margin-10-top">
        <div class="col-xs-12 col-md-3 scroll-x">
            <asp:TreeView ID="mTree" runat="server" CssClass="Tree" OnSelectedNodeChanged="mTree_SelectedNodeChanged" ShowLines="True" SkipLinkText="">
                <SelectedNodeStyle CssClass="TreeSelected" />
            </asp:TreeView>
        </div>
        <div class="col-xs-12 col-md-9">
            <asp:GridView ID="mGrid" AutoGenerateColumns="False" GridWidth="95%"
                Width="100%" CssClass="ScrollPanelCartable awesome-table table-with-border zebra" Scrolling="Vertical" runat="server"
                OnRowDataBound="mGrid_RowDataBound" GridLines="Both" EmptyDataText="" ShowFooter="true">
                <Columns>
                    <asp:TemplateField HeaderText="Lang.ColRowNr">
                        <ItemTemplate>
                            <%# Container.DataItemIndex + 1 %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ID" ItemStyle-HorizontalAlign="Center"  Visible="false"/>
                    <asp:HyperLinkField DataTextField="No" DataNavigateUrlFields="ID,ItemID" DataNavigateUrlFormatString="~/list.aspx?table=DocumentAccounting&id={0}&itemid={1}" HeaderText="Lang.DocumentNumber"  Visible="false" />
                    <asp:TemplateField HeaderText="Lang.DateDocument"  Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblDocumentDate" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="TreePathName" HeaderText="Lang.Topic" />
                    <asp:BoundField DataField="PersonnelName" HeaderText="Lang.PersonnelName" ItemStyle-HorizontalAlign="Center"  Visible="false" />
                    <asp:BoundField DataField="Comments" HeaderText="Lang.Comments" Visible="false"/>
                    <asp:TemplateField HeaderText="Lang.TotalOwe" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="owe" runat="server" Text='<%#Eval("Owe") %>'/>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="TotalOwe" runat="server" Text="" />
                            </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Lang.TotalDue" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="due" runat="server" Text='<%#Eval("Due") %>'/>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="TotalDue" runat="server" Text="" />
                            </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Lang.Remained" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="remaining" runat="server" Text='<%#Eval("Due") %>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="remainingTotal" runat="server" Text="" />
                            </FooterTemplate>
                     </asp:TemplateField>
                </Columns>
                <HeaderStyle CssClass="GridHeader" />
                <RowStyle CssClass="GridRow" />
                <SelectedRowStyle CssClass="GridRowSelected" />
            </asp:GridView>
        </div>
    </div>
    <script>
        var isOk = false;
        $(function () {
            $('#DeclareTopic').attr('value', Lang.DeclareTopic);
            $('.doDelete').click(function () {
                if (isOk)
                    return true;
                doConfirm(function () { isOk = true; $('.doDelete').click() }, null, function () { isOk = false; });
                return false;
            });
            setClassAccounting();

            shortcut.remove('alt+n');
            shortcut.remove('alt+e');
            shortcut.remove('alt+d');

<%--            shortcut.add('alt+n', function() {
                var newButton = $('#<%= doAdd.ClientID %>');
                if (!newButton.attr('disabled')) {
                    newButton.click();
                }
            });--%>

            shortcut.add('alt+e', function() {
                var editButton = $('#<%= doEdit.ClientID %>');
                if (!editButton.attr('disabled')) {
                    editButton.click();
                }
            });

            shortcut.add('alt+d', function() {
                var deleteButton = $('#<%= doDelete.ClientID %>');
                if (!deleteButton.attr('disabled')) {
                    deleteButton.click();
                }
            });

            $('#DeclareTopic').click(function () {
                function showcallback(v, m, f) {
                    if (v == 'OK') {
                        if (f.ParentTreePath != undefined &&
                            f.ParentTreePath != '' &&
                            f.TopicName != undefined &&
                            f.TopicName != '')
                            var result = getServerValue('declaretopic',
                                'Topic',
                                '&TreePath=' + f.ParentTreePath + '&TopicName=' + f.TopicName.trim());
                        else if (f.ParentTreePath == undefined || f.ParentTreePath == '') {
                            Growl.Error(Lang.DeclareTopicMsg1, 3000);
                            return false;
                        }
                        else if (f.TopicName == undefined || f.TopicName == '') {
                            Growl.Error(Lang.DeclareTopicMsg2, 3000);
                            return false;
                        }
                        if (result > 0) {
                            Growl.Notice(Lang.Success, 3000);
                            location.reload();
                        } else if (result == -1) {
                            Growl.Error(Lang.DeclareTopicMsg, 3000);
                            return false;
                        } else {
                            Growl.Error(Lang.Error, 3000);
                            return false;
                        }
                    }
                    return true;
                }
                //_Context.id = -1;
                var _url = 'get.aspx?op=GetHtml&table=Topic&qd=topic&view=declaretopic&id=-1' + appendTime();
                showPopup(_url, showcallback, getButtons());
            });
        });
    </script>
</asp:Content>
