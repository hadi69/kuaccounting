﻿using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LoadDistribute : System.Web.UI.Page
{
    System.Globalization.PersianCalendar cal = new System.Globalization.PersianCalendar();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (SiteSettings.User == null)
            Response.Redirect("~/default.aspx");

        mGrid.EmptyDataText = LangProvider.Instance["ListIsEmpty"];
        serviceGrid.EmptyDataText = LangProvider.Instance["ListIsEmpty"];

        if (!IsPostBack)
        {
            Page.Title = LangProvider.Instance["LoadDistribute"];
            Page.SetLang();
            IDataReader financeYearStartDate = DbProxy.Instance.ExecuteReader("SELECT StartDate FROM FinanceYear WHERE Status = 2");//0:Temp;1:Closed;2:Active;3:Closing
            if (financeYearStartDate.Read())
                mFromDate.Text = Helper.FormatDate(financeYearStartDate.GetDateTime(0), null);
            //mFromDate.Text = 
            mToDate.Text = Helper.FormatDate(DateTime.Now, null);
            initial();
            BindDBs();

            mReportSetting.ReportName = "LoadDistribute";
        }
        mReportSetting.SaveClick += new EventHandler(mReportSetting_SaveClick);
        mReportSetting.ReportSettingSelect += new EventHandler(mReportSetting_ReportSettingSelect);
    }
    void BindDBs()
    {
        mDBList.Items.Clear();
        ListItem item = new ListItem(LangProvider.Instance["ActiveYear"] + " ", "");
        if (string.IsNullOrEmpty(SiteSettings.OtherCatalogName))
            item.Selected = true;
        mDBList.Items.Add(item);
        try
        {
            string path = Server.MapPath("~/files/fdb");
            if (System.IO.Directory.Exists(path))
            {
                string[] files = System.IO.Directory.GetFiles(path, "DB_*.mdf");
                for (int i = 0; i < files.Length; i++)
                {
                    string fileName = System.IO.Path.GetFileNameWithoutExtension(files[i]);
                    if (!fileName.ToLower().EndsWith("_log"))
                    {
                        item = new ListItem(fileName + " ", fileName);
                        if (fileName == SiteSettings.OtherCatalogName)
                            item.Selected = true;
                        mDBList.Items.Add(item);
                    }
                }
            }
        }
        catch { }
        panelDBs.Visible = mDBList.Items.Count > 1;
    }
    private List<string> GetDBs()
    {
        List<string> dbs = new List<string>();
        for (int i = 0; i < mDBList.Items.Count; i++)
            if (mDBList.Items[i].Selected)
                dbs.Add(mDBList.Items[i].Value);
        return dbs;
    }
    void mReportSetting_ReportSettingSelect(object sender, EventArgs e)
    {
        ReportSettingInfo info = mReportSetting.SelectedReportSettings;
        if (info == null)
            return;
        string[] lines = info.Settings.Split('\n');
        mPersons.SelectedValue = lines[0].Trim('\r');
        mVisitors.SelectedValue = lines[1].Trim('\r');
        mFromDate.Text = lines[2].Trim('\r');
        mToDate.Text = lines[3].Trim('\r');
        if (lines[7] == "false")
        {
            mShowServices.Checked = false;
        }
        else
        {
            mShowServices.Checked = true;
        }
    }

    void mReportSetting_SaveClick(object sender, EventArgs e)
    {
        StringBuilder b = new StringBuilder();
        b.AppendLine(mPersons.SelectedValue); //0
        b.AppendLine(mVisitors.SelectedValue); //1
        b.AppendLine(mFromDate.Text); //2
        b.AppendLine(mToDate.Text); //3
        b.AppendLine(mShowServices.Checked.ToString()); //7
        mReportSetting.Save(b.ToString());
    }
    void initial()
    {
        {
            IDataReader persons = DbProxy.Instance.ExecuteReader("SELECT ID, [dbo].[DisplayPerson](ID, 1), ISNULL(Code, '') FROM Personnel WHERE Type=101");
            IDataReader distAgents = DbProxy.Instance.ExecuteReader("SELECT Personnel.ID, [dbo].[DisplayPerson](ID, 0) FROM Personnel WHERE Type=101 AND Personnel.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID = CONVERT(int, dbo.GetSetting('VisitorGrouping'), 0))");
            IDataReader visitors = DbProxy.Instance.ExecuteReader("SELECT Personnel.ID, [dbo].[DisplayPerson](ID, 0) FROM Personnel WHERE Type=101 AND Personnel.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID = CONVERT(int, dbo.GetSetting('VisitorGrouping'), 0))");
            IDataReader storedraftTasks = DbProxy.Instance.ExecuteReader("SELECT Task.ID, Task.Title AS TaskName FROM Task INNER JOIN Workflow ON Task.WorkflowID=Workflow.ID WHERE Workflow.Type=6 AND Workflow.Active=1");
            mPersons.Items.Clear();
            mVisitors.Items.Clear();
            mDistAgents.Items.Clear();
            mStoredaraftTasks.Items.Clear();

            mPersons.Items.Add(new ListItem(LangProvider.Instance["AllPersons"], ""));
            mVisitors.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
            mDistAgents.Items.Add(new ListItem(LangProvider.Instance["All"], ""));
            mStoredaraftTasks.Items.Add(new ListItem(LangProvider.Instance["All"], ""));

            if (persons != null)
            {
                while (persons.Read())
                {
                    if (!persons.IsDBNull(0) && !persons.IsDBNull(1) && !persons.IsDBNull(2))
                        mPersons.Items.Add(new ListItem(persons.GetString(1) + " - کد: " + persons.GetString(2), persons.GetInt32(0).ToString()));
                }
                persons.Close();
            }
            mPersons.SelectedIndex = 0;

            if (visitors != null)
            {
                while (visitors.Read())
                {
                    if (!visitors.IsDBNull(0) && !visitors.IsDBNull(1))
                        mVisitors.Items.Add(new ListItem(visitors.GetString(1), visitors.GetInt32(0).ToString()));
                }
                visitors.Close();
            }
            mVisitors.SelectedIndex = 0;

            if (distAgents != null)
            {
                while (distAgents.Read())
                {
                    if (!distAgents.IsDBNull(0) && !distAgents.IsDBNull(1))
                        mDistAgents.Items.Add(new ListItem(distAgents.GetString(1), distAgents.GetInt32(0).ToString()));
                }
                distAgents.Close();
            }
            mDistAgents.SelectedIndex = 0;

            if (storedraftTasks != null)
            {
                while (storedraftTasks.Read())
                {
                    if (!storedraftTasks.IsDBNull(0) && !storedraftTasks.IsDBNull(1))
                        mStoredaraftTasks.Items.Add(new ListItem(storedraftTasks.GetString(1), storedraftTasks.GetInt32(0).ToString()));
                }
                storedraftTasks.Close();
            }
            mStoredaraftTasks.SelectedIndex = 0;
        }


        {
            IDataReader Grouping = DbProxy.Instance.ExecuteReader("SELECT ID, dbo.[CodeToString](Code, GroupingType) Title FROM Grouping  WHERE GroupingType = 0");
            mPersonsDummy.Items.Clear();
            if (Grouping != null)
            {
                while (Grouping.Read())
                {
                    if (!Grouping.IsDBNull(0) && !Grouping.IsDBNull(1))
                        mPersonsDummy.Items.Add(new ListItem(Grouping.GetString(1), Grouping.GetInt32(0).ToString()));
                }
                Grouping.Close();
            }
        }

        {
            IDataReader Grouping = DbProxy.Instance.ExecuteReader("SELECT ID, Title FROM PersonnelGroup");
            mCatsDummy.Items.Clear();
            if (Grouping != null)
            {
                while (Grouping.Read())
                {
                    if (!Grouping.IsDBNull(0) && !Grouping.IsDBNull(1))
                        mCatsDummy.Items.Add(new ListItem(Grouping.GetString(1), Grouping.GetInt32(0).ToString()));
                }
              Grouping.Close();
            }
        }
    }

    protected void doShow_Click(object sender, EventArgs e)
    {
            mGrid.Columns[0].Visible = true;
            mGrid.Columns[2].Visible = true;
            mGrid.Columns[6].Visible = true;
            // mGrid.Columns[4].Visible = true;
            // mGrid.Columns[7].Visible = true;
            mGrid.Columns[11].Visible = true;
            mGrid.Columns[14].Visible = false;
            mGrid.Columns[15].Visible = false;
            mGrid.Columns[16].Visible = false;
            mGrid.Columns[17].Visible = true;
            mGrid.Columns[7].HeaderText = LangProvider.Instance["FinalPrice"];
            // mGrid.Columns[7].HeaderText = LangProvider.Instance["Pursant"];
            mGrid.Columns[9].HeaderText = LangProvider.Instance["PayMethodCash"];
            mGrid.Columns[10].HeaderText = LangProvider.Instance["PayMethodCheck"];
            mGrid.Columns[12].HeaderText = LangProvider.Instance["PriodRemain"];
        doExcel.Enabled = true;
        BindGrid(false);

        serviceGridPanel.Visible = mShowServices.Checked;
        if (mShowServices.Checked)
            BindServices();
    }

    protected void confirmReportAndPrint_Click(object sender, EventArgs e)
    {
        if (mDistAgents.SelectedValue != "" && ReportTitle.Text != "")
        {
            BindGrid(true);
        }
    }

    #region Grid
    decimal sumFactor = 0, sumPursant = 0, sumRemain = 0, sumTotalRemain = 0, sumCashPrice = 0, sumOwedPrice = 0, sumCheckPrice = 0
        , sumNumFactors = 0, sumNumOfPaidFactors = 0, sumNumOfNotPaidFactors = 0, _paid = -2;
    private string rowStyle = "GridAltRow";
    protected void mGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView record = e.Row.DataItem as DataRowView;
            DateTime dt = DateTime.Now;

            e.Row.Cells[4].Text = string.Format("<a href='../../reports/accounting/PersonBook.aspx?pid={0}&queryfromdate={1}&querytodate={2}'>{3}</a>", record["BuyerID"], mFromDate.Text, mToDate.Text, record["BuyerName"]);

            decimal _numFactors = 0, _numOfPaidFactors = 0, _numOfNotPaidFactors = 0;
                dt = Helper.GetDateTime(record["FactorDate"], Helper.NullDate);
                Label lblGridDate = (Label)e.Row.FindControl("lblGridDate");
                if (dt != Helper.NullDate && lblGridDate != null)
                {
                    lblGridDate.Text = Helper.ToFaFormat(Helper.GetDateTime(dt, Helper.NullDate));
                }
            decimal _totalPrice = SiteSettings.ToMonetary(Helper.GetDecimal(record["FinalPrice"], 0), dt);
            decimal _pursant = SiteSettings.ToMonetary(Helper.GetDecimal(record["Pursant"], 0), dt);
            decimal _CashPrice = SiteSettings.ToMonetary(Helper.GetDecimal(record["CashPrice"], 0), dt);
            decimal _CheckPrice = SiteSettings.ToMonetary(Helper.GetDecimal(record["CheckPrice"], 0), dt);
            decimal _OwedPrice = SiteSettings.ToMonetary(Helper.GetDecimal(record["OwedPrice"], 0), dt);
            decimal _remain = SiteSettings.ToMonetary(Helper.GetDecimal(record["Remain"], 0), dt);
            decimal _TotalRemain = SiteSettings.ToMonetary(Helper.GetDecimal(record["TotalRemain"], 0), dt);

            sumFactor += _totalPrice;
            sumPursant += _pursant;
            sumCashPrice += _CashPrice;
            sumCheckPrice += _CheckPrice;
            sumOwedPrice += _OwedPrice;
            sumRemain += _remain;
            sumTotalRemain += _TotalRemain;
            sumNumFactors += _numFactors;
            sumNumOfPaidFactors += _numOfPaidFactors;
            sumNumOfNotPaidFactors += _numOfNotPaidFactors;

            ((Label)e.Row.FindControl("totalPrice")).Text = _totalPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("pursant")).Text = _pursant.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("CashPrice")).Text = _CashPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("CheckPrice")).Text = _CheckPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("OwedPrice")).Text = _OwedPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("remain")).Text = _remain >= 0 ? _remain.ToString("N4", Helper.NumberCulture).Replace(".0000", "") + " " + LangProvider.Instance["AbbrvDue"] : (-1 * _remain).ToString("N4", Helper.NumberCulture).Replace(".0000", "") + " " + LangProvider.Instance["AbbrvOwe"];
            ((Label)e.Row.FindControl("TotalRemain")).Text = _TotalRemain >= 0 ? _TotalRemain.ToString("N4", Helper.NumberCulture).Replace(".0000", "") + " " + LangProvider.Instance["AbbrvDue"] : (-1 * _TotalRemain).ToString("N4", Helper.NumberCulture).Replace(".0000", "") + " " + LangProvider.Instance["AbbrvOwe"];

                   _paid = Helper.GetInt(record["Paid"], Null.NullInteger);
                    if (_paid == 0)
                    {
                        e.Row.Cells[17].Text = LangProvider.Instance["Paid"];
                        rowStyle = "paidRow";
                    }
                    else if (_paid == -1)
                    {
                        e.Row.Cells[17].Text = LangProvider.Instance["NotConfirmed"];
                        rowStyle = "notPaidRow";
                    }
                    else if (_paid == -2)
                    {
                        e.Row.Cells[17].Text = "-";
                        rowStyle = "GridAltRow";
                    }
                    else
                    {
                        e.Row.Cells[17].Text = LangProvider.Instance["Remained"] + " : " + SiteSettings.ToMonetary(_paid, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                        rowStyle = "notPaidRow";
                    }
            e.Row.CssClass = rowStyle;
            e.Row.CssClass = rowStyle;

            if (_remain > 0)
                Helper.SignifyNegative(e.Row.Cells[12]);
            if (_TotalRemain > 0)
                Helper.SignifyNegative(e.Row.Cells[13]);
            CheckBox chkPrintID = e.Row.FindControl("chkPrintID") as CheckBox;
            if (chkPrintID != null)
            {
                chkPrintID.InputAttributes.Add("data-id", record["ID"].ToString());
                chkPrintID.CssClass += "data-print";
            }

        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            (e.Row.FindControl("totalPriceTotal") as Label).Text = sumFactor.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("pursantTotal") as Label).Text = sumPursant.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("CashPriceTotal") as Label).Text = sumCashPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("CheckPriceTotal") as Label).Text = sumCheckPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("OwedPriceTotal") as Label).Text = sumOwedPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            (e.Row.FindControl("remainTotal") as Label).Text = sumRemain >= 0 ? sumRemain.ToString("N4", Helper.NumberCulture).Replace(".0000", "") + " " + LangProvider.Instance["AbbrvDue"] : (-1 * sumRemain).ToString("N4", Helper.NumberCulture).Replace(".0000", "") + " " + LangProvider.Instance["AbbrvOwe"];
            (e.Row.FindControl("totalRemainTotal") as Label).Text = sumTotalRemain >= 0 ? sumTotalRemain.ToString("N4", Helper.NumberCulture).Replace(".0000", "") + " " + LangProvider.Instance["AbbrvDue"] : (-1 * sumTotalRemain).ToString("N4", Helper.NumberCulture).Replace(".0000", "") + " " + LangProvider.Instance["AbbrvOwe"];

            if (sumRemain > 0)
                Helper.SignifyNegative(e.Row.Cells[12]);
            if (sumTotalRemain > 0)
                Helper.SignifyNegative(e.Row.Cells[13]);

        }
    }

    protected void mSGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView record = e.Row.DataItem as DataRowView;
            DateTime dt = DateTime.Now;

            e.Row.Cells[3].Text = string.Format("<a href='../../reports/accounting/PersonBook.aspx?pid={0}&queryfromdate={1}&querytodate={2}'>{3}</a>", record["BuyerID"], mFromDate.Text, mToDate.Text, record["BuyerName"]);

            decimal _numFactors = 0, _numOfPaidFactors = 0, _numOfNotPaidFactors = 0;
                _numFactors = Helper.GetInt(record["NumFactors"], 0);
                _numOfPaidFactors = Helper.GetInt(record["NumOfPaidFactors"], 0);
                _numOfNotPaidFactors = Helper.GetInt(record["NumOfNotPaidFactors"], 0);
                ((Label)e.Row.FindControl("NumFactors")).Text = _numFactors.ToString("N0");
                ((Label)e.Row.FindControl("NumOfPaidFactors")).Text = _numOfPaidFactors.ToString("N0");
                ((Label)e.Row.FindControl("NumOfNotPaidFactors")).Text = _numOfNotPaidFactors.ToString("N0");

            decimal _totalPrice = SiteSettings.ToMonetary(Helper.GetDecimal(record["FinalPrice"], 0), dt);
            decimal _pursant = SiteSettings.ToMonetary(Helper.GetDecimal(record["Pursant"], 0), dt);
            decimal _CashPrice = SiteSettings.ToMonetary(Helper.GetDecimal(record["CashPrice"], 0), dt);
            decimal _CheckPrice = SiteSettings.ToMonetary(Helper.GetDecimal(record["CheckPrice"], 0), dt);
            decimal _OwedPrice = SiteSettings.ToMonetary(Helper.GetDecimal(record["OwedPrice"], 0), dt);
            decimal _remain = SiteSettings.ToMonetary(Helper.GetDecimal(record["Remain"], 0), dt);
            decimal _TotalRemain = SiteSettings.ToMonetary(Helper.GetDecimal(record["TotalRemain"], 0), dt);

            sumFactor += _totalPrice;
            sumPursant += _pursant;
            sumCashPrice += _CashPrice;
            sumCheckPrice += _CheckPrice;
            sumOwedPrice += _OwedPrice;
            sumRemain += _remain;
            sumTotalRemain += _TotalRemain;
            sumNumFactors += _numFactors;
            sumNumOfPaidFactors += _numOfPaidFactors;
            sumNumOfNotPaidFactors += _numOfNotPaidFactors;
                    _paid = Helper.GetInt(record["Paid"], Null.NullInteger);
                    if (_paid == 0)
                    {
                        e.Row.Cells[16].Text = LangProvider.Instance["Paid"];
                        rowStyle = "paidRow";
                    }
                    else if (_paid == -1)
                    {
                        e.Row.Cells[16].Text = LangProvider.Instance["NotConfirmed"];
                        rowStyle = "notPaidRow";
                    }
                    else if (_paid == -2)
                    {
                        e.Row.Cells[16].Text = "-";
                        rowStyle = "GridAltRow";
                    }
                    else
                    {
                        e.Row.Cells[16].Text = LangProvider.Instance["Remained"] + " : " + SiteSettings.ToMonetary(_paid, DateTime.Now).ToString("N4", Helper.NumberCulture).Replace(".0000", "");
                        rowStyle = "notPaidRow";
                    }
            e.Row.CssClass = rowStyle;

            ((Label)e.Row.FindControl("totalPrice")).Text = _totalPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("pursant")).Text = _pursant.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("CashPrice")).Text = _CashPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("CheckPrice")).Text = _CheckPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("OwedPrice")).Text = _OwedPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("remain")).Text = _remain >= 0 ? _remain.ToString("N4", Helper.NumberCulture).Replace(".0000", "") + " " + LangProvider.Instance["AbbrvDue"] : (-1 * _remain).ToString("N4", Helper.NumberCulture).Replace(".0000", "") + " " + LangProvider.Instance["AbbrvOwe"];
            ((Label)e.Row.FindControl("TotalRemain")).Text = _TotalRemain >= 0 ? _TotalRemain.ToString("N4", Helper.NumberCulture).Replace(".0000", "") + " " + LangProvider.Instance["AbbrvDue"] : (-1 * _TotalRemain).ToString("N4", Helper.NumberCulture).Replace(".0000", "") + " " + LangProvider.Instance["AbbrvOwe"];

            if (_remain > 0)
                Helper.SignifyNegative(e.Row.Cells[11]);
            if (_TotalRemain > 0)
                Helper.SignifyNegative(e.Row.Cells[12]);

            CheckBox chkPrintID = e.Row.FindControl("chkPrintID") as CheckBox;
            if (chkPrintID != null)
            {
                chkPrintID.InputAttributes.Add("data-id", record["ID"].ToString());
                chkPrintID.CssClass += "data-print";
            }
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            (e.Row.FindControl("totalPriceTotal") as Label).Text = sumFactor.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("pursantTotal") as Label).Text = sumPursant.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("CashPriceTotal") as Label).Text = sumCashPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("CheckPriceTotal") as Label).Text = sumCheckPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            (e.Row.FindControl("OwedPriceTotal") as Label).Text = sumOwedPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");

            (e.Row.FindControl("remainTotal") as Label).Text = sumRemain >= 0 ? sumRemain.ToString("N4", Helper.NumberCulture).Replace(".0000", "") + " " + LangProvider.Instance["AbbrvDue"] : (-1 * sumRemain).ToString("N4", Helper.NumberCulture).Replace(".0000", "") + " " + LangProvider.Instance["AbbrvOwe"];
            (e.Row.FindControl("totalRemainTotal") as Label).Text = sumTotalRemain >= 0 ? sumTotalRemain.ToString("N4", Helper.NumberCulture).Replace(".0000", "") + " " + LangProvider.Instance["AbbrvDue"] : (-1 * sumTotalRemain).ToString("N4", Helper.NumberCulture).Replace(".0000", "") + " " + LangProvider.Instance["AbbrvOwe"];

            if (sumRemain > 0)
                Helper.SignifyNegative(e.Row.Cells[11]);
            if (sumTotalRemain > 0)
                Helper.SignifyNegative(e.Row.Cells[12]);

        }
    }

    protected void mGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            SortOrder = SortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            SortCol = e.SortExpression;
            SortOrder = "asc";
        }
        BindGrid(false);
    }
    void BindGrid(bool confirmReportAndPrint)
    {
        sumFactor = sumPursant = sumRemain = sumTotalRemain
        = sumCashPrice = sumOwedPrice = sumCheckPrice = sumNumFactors = 0;
        sumFactor = sumPursant = sumRemain = 0;
        DateTime start, end;
        start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);


        gridPanel.Visible = true;

        string pgIDs, mgIDs, pgWhere;
        string query1 = "", query2 = "";
        GetPersonsGroupWhere(out mgIDs, out pgIDs, out pgWhere);
                #region all
          string query = string.Format(@"SELECT ID, BuyerID, NOSerial, ISNULL(BuyerCode, '') + ' ' +  BuyerName AS BuyerName, VisitorName, FactorDate, FinalPrice, Pursant
        , CASE 
         WHEN PayMethod = 0 -- Cash
          THEN FinalPrice
         WHEN PayMethod = 1 -- Check
          THEN 0
         WHEN PayMethod = 2 -- Other, Owed
          THEN 0
         WHEN PayMethod = 3 -- Complex
          THEN CashPrice
        ELSE 0
        END CashPrice
        , CASE 
        WHEN PayMethod = 0 -- Cash
          THEN 0
        WHEN PayMethod = 1 -- Check
          THEN FinalPrice
        WHEN PayMethod = 2 -- Other, Owed
          THEN 0
        WHEN PayMethod = 3 -- Complex
          THEN CheckPrice
        ELSE 0
       END CheckPrice
       , CASE 
       WHEN PayMethod = 0 -- Cash
          THEN 0
       WHEN PayMethod = 1 -- Check
          THEN 0
       WHEN PayMethod = 2 -- Other, Owed
          THEN FinalPrice
       WHEN PayMethod = 3 -- Complex
          THEN OwedPrice
      ELSE 0
      END OwedPrice
	  , ISNULL((SELECT SUM(ISNULL(DA.Due, 0) - ISNULL(DA.Owe, 0)) FROM DocumentAccountingItems DA WHERE DA.PersonnelID = StoreDraftView.BuyerID
            AND DA.TreePath IN (SELECT Trees.TreePath FROM Trees WHERE Trees.PersonNeed=1))
		, 0) TotalRemain
     , ISNULL((SELECT SUM(ISNULL(DA.Due, 0) - ISNULL(DA.Owe, 0)) FROM DocumentAccountingItems DA WHERE DA.PersonnelID = StoreDraftView.BuyerID
            AND DA.TreePath IN (SELECT Trees.TreePath FROM Trees WHERE Trees.PersonNeed=1) 
            AND DA.DocumentAccountingID IN (SELECT ID FROM DocumentAccounting WHERE DocumentAccounting.Date >= {0} AND  DocumentAccounting.Date <= {1}))
		, 0) Remain
        ,[dbo].[PaidOrNotPaidOwe](StoreDraftView.ID, 1) Paid
        ,StoreDraftView.TaskName
     FROM StoreDraftView", Helper.QoutedDateTime(start), Helper.QoutedDateTime(end));
                #endregion


                    #region DistReportPrint
                    query2 = string.Format(@"
                    ALTER TABLE StoreDraft DISABLE TRIGGER StoreDraftInsertUpdate
                    UPDATE StoreDraftView SET DistAgentID = {0}
                     ", Helper.GetInt(mDistAgents.SelectedValue, Null.NullInteger));

                    query1 += string.Format(@" IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DistReportPrint]') AND type in (N'U'))
                        DROP TABLE DistReportPrint
                        Create TABLE DistReportPrint(
                                ID INT Identity(1, 1),
                                    ReportDate DATETIME,
                                    NOSerial NVARCHAR(100),
                                    FactorDate DATETIME,
                                    BuyerName NVARCHAR(500),
                                    FinalPrice NUMERIC(18, 2),
                                    BuyerRemain NUMERIC(18, 2)
                            );
                            INSERT INTO DistReportPrint
                        SELECT GETDATE(), NOSerial, FactorDate, ISNULL(BuyerCode, '') + ' ' +  BuyerName AS BuyerName, FinalPrice
                        , ISNULL((SELECT SUM(ISNULL(DA.Due, 0) - ISNULL(DA.Owe, 0)) FROM DocumentAccountingItems DA WHERE DA.PersonnelID = StoreDraftView.BuyerID
			                                AND DA.TreePath IN (SELECT Trees.TreePath FROM Trees WHERE Trees.PersonNeed=1) 
                            AND DA.DocumentAccountingID IN (SELECT ID FROM DocumentAccounting WHERE DocumentAccounting.Date >= {0} 
	                        AND  DocumentAccounting.Date <= {1}))
                        , 0) Remain
                        FROM StoreDraftView", Helper.QoutedDateTime(start), Helper.QoutedDateTime(end));
                    #endregion
            string where = string.Format(" WHERE FactorDate>={0} AND FactorDate<={1}", Helper.QoutedDateTime(start), Helper.QoutedDateTime(end));
            int buyerID = Helper.GetInt(mPersons.SelectedValue, Null.NullInteger);
            if (buyerID != Null.NullInteger)
                where += " AND BuyerID=" + buyerID;
            int visitorID = Helper.GetInt(mVisitors.SelectedValue, Null.NullInteger);
            if (visitorID != Null.NullInteger)
                where += " AND VisitorID=" + visitorID;
            int taskID = Helper.GetInt(mStoredaraftTasks.SelectedValue, Null.NullInteger);
            if (taskID != Null.NullInteger)
                where += " AND StoreDraftView.TaskID =" + taskID;

            if (!string.IsNullOrEmpty(removeIDs.Value))
            {
                string noIDs = removeIDs.Value.Replace('|', ',').Trim().Trim(',');
                if (noIDs.Length > 0)
                    where += " AND NOT ID IN(" + noIDs + ")";
            }

            if (!(string.IsNullOrEmpty(pgIDs) && string.IsNullOrEmpty(mgIDs)) && !string.IsNullOrEmpty(pgWhere))
            {
                // ------------------------
                // ids: 1,2,5
                // where: '&1%' AND ('&2%' OR '&5%')
                // it could be better, but i dont know regex well :(
                // ------------------------
                where += "\r\nAND (\r\n";
                if (!string.IsNullOrEmpty(mgIDs))
                {
                    // grouping members : '&1%', '!&1%'
                    string[] ids = mgIDs.Split(',');
                    for (int i = 0; i < ids.Length; i++)
                    {
                        StringBuilder allIds = new StringBuilder();
                        int j = 0;
                        IDataReader childIds = DbProxy.Instance.ExecuteReader("SELECT ID FROM [GetAllGroupingChilds](" + ids[i] + ")");
                        while (childIds.Read())
                        {
                            if (j != 0)
                            {
                                allIds.Append(",");
                            }
                            allIds.Append(childIds.GetValue(0));
                            j++;
                        }
                        pgWhere = pgWhere.Replace("'&" + ids[i] + "%'", " BuyerID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + ")) \r\n");
                        pgWhere = pgWhere.Replace("'!&" + ids[i] + "%'", " NOT BuyerID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + ")) \r\n");
                        //where1 += "AND Personnel.ID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + "))";
                    }
                }

                if (!string.IsNullOrEmpty(pgIDs))
                {
                    // personnel groups: '^1%', '!^1%'
                    string[] ids = pgIDs.Split(',');
                    for (int i = 0; i < ids.Length; i++)
                    {
                        pgWhere = pgWhere.Replace("'^" + ids[i] + "%'", " BuyerID IN (SELECT ID FROM Personnel WHERE PersonnelGroupID=" + ids[i] + ") \r\n");
                        pgWhere = pgWhere.Replace("'!^" + ids[i] + "%'", " NOT BuyerID IN (SELECT ID FROM Personnel WHERE PersonnelGroupID=" + ids[i] + ") \r\n");
                    }
                }

                where += pgWhere;
                where += "\r\n)\r\n";
            }

            query += where;

            //if (!mShowServices.Checked)
            //{
            //    query = "SELECT * FROM ( " + query + " ) A WHERE NumOfNotPaidFactors > 0";
            //    // where += string.Format(" AND [dbo].[PaidOrNotPaidfactors](StoreDraftView.BuyerID, StoreDraftView.VisitorID, {0}, {1}, 0) > 0", Helper.QoutedDateTime(start), Helper.QoutedDateTime(end));
            //}
            //if (!mShowServices.Checked)
            //{
            //    query = "SELECT * FROM ( " + query + " ) A WHERE NOT Paid IN (0, -2)";
            //    //where += "AND NOT [dbo].[PaidOrNotPaidOwe](StoreDraftView.ID, 1) IN (0, -2)";
            //}

            query1 += where;
            query2 += where;
            if (!string.IsNullOrEmpty(SortCol))
                query1 += " ORDER BY " + SortCol + " " + SortOrder;
            else
            {
                query1 += " ORDER BY BuyerID, FactorDate, DATEADD(dd, 0, DATEDIFF(dd, 0, FactorDate)), NoSerial, ID";
            }
            if (confirmReportAndPrint)
            {
                query1 = query2 + query1 + " ALTER TABLE StoreDraft ENABLE TRIGGER StoreDraftInsertUpdate;";
            }

        if (!string.IsNullOrEmpty(SortCol))
                query += " ORDER BY " + SortCol + " " + SortOrder;
            else
            {
                query += " ORDER BY BuyerID, FactorDate, DATEADD(dd, 0, DATEDIFF(dd, 0, FactorDate)), NoSerial, ID";
            }
       // }

        DataTable items = DbProxy.Instance.FillData(query, "all", GetDBs(), string.IsNullOrEmpty(SortCol) ? "" : (SortCol + " " + SortOrder));
        DataTable items2 = DbProxy.Instance.FillData(query1, "all", GetDBs(), string.IsNullOrEmpty(SortCol) ? "" : (SortCol + " " + SortOrder));
        try
        {
                    mGrid.DataSource = items;
                    mGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Dispose();
            }
        }
    }
    protected string SortCol
    {
        get
        {
            return Helper.GetString(ViewState["SortCol"], null);
        }
        set
        {
            ViewState["SortCol"] = value;
        }
    }
    protected string SortOrder
    {
        get
        {
            return Helper.GetString(ViewState["SortOrder"], "asc");
        }
        set
        {
            ViewState["SortOrder"] = value;
        }
    }
    #endregion
 
    #region Services Grid
    decimal serviceSumPrice = 0, serviceSumNum = 0, serviceSumWeight = 0, serviceSumVolume = 0, serviceSumPursant = 0;
    protected void serviceGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView record = e.Row.DataItem as DataRowView;

            decimal _SumPrice = SiteSettings.ToMonetary(Helper.GetDecimal(record["SumPrice"], 0), Null.NullDate);
            decimal _SumNum = Helper.GetDecimal(record["SumNum"], 0);
            decimal _SumWeight = Helper.GetDecimal(record["SumWeight"], 0);
            decimal _SumVolume = Helper.GetDecimal(record["SumVolume"], 0);
            decimal _SumPursant = Helper.GetDecimal(record["SumPursant"], 0);

            serviceSumPrice += _SumPrice;
            serviceSumNum += _SumNum;
            serviceSumWeight += _SumWeight;
            serviceSumVolume += _SumVolume;
            serviceSumPursant += _SumPursant;

            ((Label)e.Row.FindControl("SumPrice")).Text = _SumPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("SumNum")).Text = _SumNum.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("SumWeight")).Text = _SumWeight.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("SumVolume")).Text = _SumVolume.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("SumPursant")).Text = _SumPursant.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            ((Label)e.Row.FindControl("SumPriceTotal")).Text = serviceSumPrice.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("SumNumTotal")).Text = serviceSumNum.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("SumWeightTotal")).Text = serviceSumWeight.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("SumVolumeTotal")).Text = serviceSumVolume.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
            ((Label)e.Row.FindControl("SumPursantTotal")).Text = serviceSumPursant.ToString("N4", Helper.NumberCulture).Replace(".0000", "");
        }
    }
    void BindServices()
    {
        serviceSumPrice = serviceSumNum = serviceSumWeight = serviceSumVolume = serviceSumPursant = 0;

        DateTime start = Helper.ParseDate(mFromDate.Text, DateTime.Now.AddYears(-50));
        DateTime end = Helper.ParseDate(mToDate.Text, DateTime.Now.AddYears(50));
        start = new DateTime(start.Year, start.Month, start.Day, 0, 0, 0, 0);
        end = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 0);

        string pgIDs, mgIDs, pgWhere;
        GetPersonsGroupWhere(out mgIDs, out pgIDs, out pgWhere);

        string where2 = string.Format(" StoreDraft.WorkflowStatus IS NOT NULL  AND StoreDraft.WorkflowStatus = 1 AND StoreDraft.FactorDate>={0} "
            + "AND StoreDraft.FactorDate<={1}", Helper.QoutedDateTime(start), Helper.QoutedDateTime(end));
        int buyerID = Helper.GetInt(mPersons.SelectedValue, Null.NullInteger);
        if (buyerID != Null.NullInteger)
            where2 += " AND BuyerID=" + buyerID;
        int visitorID = Helper.GetInt(mVisitors.SelectedValue, Null.NullInteger);
        if (visitorID != Null.NullInteger)
            where2 += " AND VisitorID=" + visitorID;

        if (!(string.IsNullOrEmpty(pgIDs) && string.IsNullOrEmpty(mgIDs)) && !string.IsNullOrEmpty(pgWhere))
        {
            // ------------------------
            // ids: 1,2,5
            // where: '&1%' AND ('&2%' OR '&5%')
            // it could be better, but i dont know regex well :(
            // ------------------------
            where2 += "\r\nAND (\r\n";
            if (!string.IsNullOrEmpty(mgIDs))
            {
                // grouping members : '&1%', '!&1%'
                string[] ids = mgIDs.Split(',');
                for (int i = 0; i < ids.Length; i++)
                {
                    StringBuilder allIds = new StringBuilder();
                    int j = 0;
                    IDataReader childIds = DbProxy.Instance.ExecuteReader("SELECT ID FROM [GetAllGroupingChilds](" + ids[i] + ")");
                    while (childIds.Read())
                    {
                        if (j != 0)
                        {
                            allIds.Append(",");
                        }
                        allIds.Append(childIds.GetValue(0));
                        j++;
                    }
                    pgWhere = pgWhere.Replace("'&" + ids[i] + "%'", " StoreDraft.BuyerID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + ")) \r\n");
                    pgWhere = pgWhere.Replace("'!&" + ids[i] + "%'", " NOT StoreDraft.BuyerID IN (SELECT OtherID FROM GroupingMember WHERE GroupingID IN (" + allIds + ")) \r\n");
                }
            }

            if (!string.IsNullOrEmpty(pgIDs))
            {
                // personnel groups: '^1%', '!^1%'
                string[] ids = pgIDs.Split(',');
                for (int i = 0; i < ids.Length; i++)
                {
                    pgWhere = pgWhere.Replace("'^" + ids[i] + "%'", " StoreDraft.BuyerID IN (SELECT ID FROM Personnel WHERE PersonnelGroupID=" + ids[i] + ") \r\n");
                    pgWhere = pgWhere.Replace("'!^" + ids[i] + "%'", " NOT StoreDraft.BuyerID IN (SELECT ID FROM Personnel WHERE PersonnelGroupID=" + ids[i] + ") \r\n");
                }
            }

            where2 += pgWhere;
            where2 += "\r\n)\r\n";
        }

        string where = where2;
        if (!string.IsNullOrEmpty(removeIDs.Value))
        {
            string noIDs = removeIDs.Value.Replace('|', ',').Trim().Trim(',');
            if (noIDs.Length > 0)
                where += " AND NOT StoreDraft.ID IN(" + noIDs + ")";
        }
        string query = string.Format(@"SELECT Service.ID, Service.Title ServiceTitle, Supplier.CompanyName, Units.Title UnitTitle
	, SUM(DeliveredAmount * Price * ISNULL(UnitCurrencyConverter, 1)) SumPrice
	, SUM(DeliveredAmount * 1) SumNum
	, SUM(DeliveredAmount * ServicePrice.Weight) SumWeight
	, SUM(DeliveredAmount * ServicePrice.Volume) SumVolume
	, SUM(ISNULL(StoreDraftItems.SalesManCommission, 0) / 100 * DeliveredAmount * Price * ISNULL(UnitCurrencyConverter, 1)) SumPursant
    , Stores.ID StoreID, Stores.Title StoreName, StoreServiceDef.ID StoreServiceDefID, Service.Code
	from StoreDraftItems
INNER JOIN StoreDraft ON StoreDraftItems.StoreDraftID = StoreDraft.ID
INNER JOIN StoreServices ON StoreDraftItems.StoreServiceID = StoreServices.ID
INNER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
INNER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID
INNER JOIN Supplier ON ServicePrice.Model = Supplier.ID
INNER JOIN Service ON ServicePrice.ServiceID = Service.ID
INNER JOIN Units ON Service.UnitID = Units.ID
INNER JOIN Stores ON StoreServiceDef.StoreID = Stores.ID
WHERE Service.UnitID = StoreDraftItems.UnitID AND {0}
GROUP BY Service.ID, Service.Title, Service.Code, Units.Title, Supplier.CompanyName, Stores.ID, Stores.Title, StoreServiceDef.ID

UNION

SELECT Service.ID, Service.Title ServiceTitle, Supplier.CompanyName, Units.Title UnitTitle
	, SUM(DeliveredAmount * Price * ISNULL(UnitCurrencyConverter, 1)) SumPrice
	, SUM(DeliveredAmount * 1) SumNum
	, SUM(DeliveredAmount * ServiceUnits.UWeight) SumWeight
	, SUM(DeliveredAmount * ServiceUnits.UVolume) SumVolume
	, SUM(ISNULL(StoreDraftItems.SalesManCommission, 0) / 100 * DeliveredAmount * Price * ISNULL(UnitCurrencyConverter, 1)) SumPursant
    , Stores.ID StoreID, Stores.Title StoreName, StoreServiceDef.ID StoreServiceDefID, Service.Code
	from StoreDraftItems
INNER JOIN StoreDraft ON StoreDraftItems.StoreDraftID = StoreDraft.ID
INNER JOIN StoreServices ON StoreDraftItems.StoreServiceID = StoreServices.ID
INNER JOIN StoreServiceDef ON StoreServices.StoreServiceDefID = StoreServiceDef.ID
INNER JOIN ServicePrice ON StoreServiceDef.ServicePriceID = ServicePrice.ID
INNER JOIN Supplier ON ServicePrice.Model = Supplier.ID
INNER JOIN Service ON ServicePrice.ServiceID = Service.ID
INNER JOIN ServiceUnits ON StoreDraftItems.UnitID = ServiceUnits.ID
INNER JOIN Units ON ServiceUnits.UnitsID = Units.ID
INNER JOIN Stores ON StoreServiceDef.StoreID = Stores.ID
WHERE Service.UnitID <> StoreDraftItems.UnitID AND {0}
GROUP BY Service.ID, Service.Title, Units.Title, Service.Code, Supplier.CompanyName, Stores.ID, Stores.Title, StoreServiceDef.ID", where);

        if (!string.IsNullOrEmpty(ServiceSortCol))
            query += " ORDER BY " + ServiceSortCol + " " + ServiceSortOrder;
        else
            query += " ORDER BY Service.ID";
        DataTable items = DbProxy.Instance.FillData(query, "services", GetDBs(), string.IsNullOrEmpty(ServiceSortCol) ? "" : (ServiceSortCol + " " + ServiceSortOrder));
        try
        {
            serviceGrid.DataSource = items;
            serviceGrid.DataBind();
        }
        finally
        {
            if (null != items)
            {
                items.Dispose();
            }
        }
    }

    protected void serviceGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortCol)
        {
            ServiceSortOrder = ServiceSortOrder == "asc" ? "desc" : "asc";
        }
        else
        {
            ServiceSortCol = e.SortExpression;
            ServiceSortOrder = "asc";
        }
        BindServices();
    }


    protected string ServiceSortCol
    {
        get
        {
            return Helper.GetString(ViewState["ServiceSortCol"], null);
        }
        set
        {
            ViewState["ServiceSortCol"] = value;
        }
    }
    protected string ServiceSortOrder
    {
        get
        {
            return Helper.GetString(ViewState["ServiceSortOrder"], "asc");
        }
        set
        {
            ViewState["ServiceSortOrder"] = value;
        }
    }
    #endregion

    // ------------------------
    // ids: 1,2,5
    // where: '&1%' AND ('&2%' OR '&5%')
    // ------------------------
    private void GetPersonsGroupWhere(out string gmIDs, out string pgIDs, out string where)
    {
        gmIDs = pgIDs = "";
        where = "";

        // grouping members : '&1%', '!&1%'
        // category = 1375 AND ( category != 1076 OR category = 1075 ) 
        // --> '&1375%' AND ('!&1076%' OR '&1075%')
        where = mQueryBuilderSql.Value.Trim();
        where += " ";// we need this space at end to find IDs
        while (where.Contains("category = "))
        {
            int index = where.IndexOf("category = ") + "category = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            gmIDs += id + ",";
            where = where.Replace("category = " + id + " ", "'&" + id + "%' ");
        }
        while (where.Contains("category != "))
        {
            int index = where.IndexOf("category != ") + "category != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            gmIDs += id + ",";
            where = where.Replace("category != " + id + " ", "'!&" + id + "%' ");
        }
        if (gmIDs.EndsWith(","))
            gmIDs = gmIDs.Substring(0, gmIDs.Length - ",".Length);


        // personnel groups: '^1%', '!^1%'
        while (where.Contains("pcat = "))
        {
            int index = where.IndexOf("pcat = ") + "pcat = ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pgIDs += id + ",";
            where = where.Replace("pcat = " + id + " ", "'^" + id + "%' ");
        }
        while (where.Contains("pcat != "))
        {
            int index = where.IndexOf("pcat != ") + "pcat != ".Length;
            int nextIndex = where.IndexOf(' ', index);
            int id = Helper.GetInt(where.Substring(index, nextIndex - index), -1);
            pgIDs += id + ",";
            where = where.Replace("pcat != " + id + " ", "'!^" + id + "%' ");
        }
        if (pgIDs.EndsWith(","))
            pgIDs = pgIDs.Substring(0, pgIDs.Length - ",".Length);
    }

    protected void doExcel_Click(object sender, EventArgs e)
    {
        Exporter.ExportExcel(Response
            , new List<string>(new string[] { mGridTitle.Text})
            , new List<GridView>(new GridView[] { mGrid}));
    }
    protected void doExcelServices_Click(object sender, EventArgs e)
    {
        Exporter.ExportExcel(Response
            , new List<string>(new string[] { mServiceTitle.Text })
            , new List<GridView>(new GridView[] { serviceGrid }));
    }
 
}