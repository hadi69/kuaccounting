﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="messages.aspx.cs" Inherits="market_messages" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width=device-width, height=device-height,  initial-scale=1.0, user-scalable=no;user-scalable=0;" />
    <title></title>
    <link href="<%= ResolveClientUrl("~/") %>styles/style.css" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveClientUrl("~/") %>styles/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveClientUrl("~/") %>styles/bootstrap-rtl.css" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveClientUrl("~/") %>styles/BYekan.css" rel="stylesheet" type="text/css" />
    <link href="market.css" rel="stylesheet" type="text/css" />

    <script src="<%= ResolveClientUrl("~/") %>js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="<%= ResolveClientUrl("~/") %>js/bootstrap.min.js" type="text/javascript"></script>

</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <asp:Repeater runat="server" ID="mItems">
                <ItemTemplate>
                    <div class="row basketitem" style="padding:10px">
                        <div class="row">
                            <div class="col-xs-12 left-aligned-label"><%# Eval("Title") %> <span class="value" style="margin-right:10px;direction:rtl">(<%# Helper.FormatDate((DateTime)Eval("Date"), "yyyy-MM-dd HH:mm") %>)</span></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 left-aligned-label">
                                <span class="value"><%# Eval("Msg") %></span>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </form>
</body>
</html>
