﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class market_showbasket : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int id = Helper.GetInt(Request.QueryString["id"], -1);
        DataRow row = DbProxy.Instance.FillRow("SELECT * FROM BasketView WHERE ID=" + id);
        DataRow userRow = DbProxy.Instance.FillRow("SELECT * FROM Users WHERE ID=" + Helper.GetInt(row, "UserID", 0));
        mCode.Text = Helper.GetString(row, "Code", "");

        mStatus.Text = LangProvider.Instance["BasketStatusNew"];
        switch (Helper.GetInt(row, "Status", 0))
        {
            case 1:
                mStatus.Text = LangProvider.Instance["BasketStatusNew"];
                break;
            case 2:
                mStatus.Text = LangProvider.Instance["BasketStatusConfirmed"];
                break;
            case 3:
                mStatus.Text = LangProvider.Instance["BasketStatusCanceled"];
                break;
        }

        mPayStatus.Text = LangProvider.Instance["BasketPayStatusNotPaid"];
        switch (Helper.GetInt(row, "PayStatus", 0))
        {
            case 1:
                mPayStatus.Text = LangProvider.Instance["BasketPayStatusNotPaid"];
                break;
            case 2:
                mPayStatus.Text = LangProvider.Instance["BasketPayStatusInPayment"];
                break;
            case 3:
                mPayStatus.Text = LangProvider.Instance["BasketPayStatusPaid"];
                break;
        }

        mPayMethod.Text = LangProvider.Instance["BasketPayMethodCash"];
        switch (Helper.GetInt(row, "PayMethod", 0))
        {
            case 1:
                mPayMethod.Text = LangProvider.Instance["BasketPayMethodOnline"];
                break;
            case 2:
                mPayMethod.Text = LangProvider.Instance["BasketPayMethodCoupon"];
                break;
            case 3:
                mPayMethod.Text = LangProvider.Instance["BasketPayMethodCash"];
                break;
            case 4:
                mPayMethod.Text = LangProvider.Instance["BasketPayMethodInPlace"];
                break;
        }
        mDate.Text = Helper.FormatDate(Helper.GetDateTime(row, "BasketDate", Null.NullDate), "yyyy-MM-dd");
        mVendorname.Text = Helper.GetString(row, "VendorName", "");
        mPrice.Text = Helper.GetDecimal(row, "TotalPrice", 0).ToString("N0");


        mUsername.Text = Helper.GetString(userRow, "Fullname", Helper.GetString(row, "UserName", ""));
        mAddress.Text = Helper.GetString(row, "DeliveryAddress","");
        mMobile.Text = Helper.GetString(userRow, "Mobile", "");
        mDeliveryTime.Text = Helper.GetString(row, "DeliveryTime", "");

        mItems.DataSource = DbProxy.Instance.FillData("SELECT *, (Price * Num) AS TotalPrice, (SELECT TOP 1 MainImage FROM ServiceImage WHERE ServiceID IN (SELECT ServiceID FROM ServicePrice WHERE ID = ServicePriceID)) MainImage FROM BasketItemView WHERE BasketID=" + id, "SS");
        mItems.DataBind();
    }
}