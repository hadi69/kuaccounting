﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="about.aspx.cs" Inherits="market_about" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width=device-width, height=device-height,  initial-scale=1.0, user-scalable=no;user-scalable=0;" />
    <title></title>
    <link href="<%= ResolveClientUrl("~/") %>styles/style.css" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveClientUrl("~/") %>styles/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveClientUrl("~/") %>styles/bootstrap-rtl.css" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveClientUrl("~/") %>styles/BYekan.css" rel="stylesheet" type="text/css" />
    <link href="market.css" rel="stylesheet" type="text/css" />

    <script src="<%= ResolveClientUrl("~/") %>js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="<%= ResolveClientUrl("~/") %>js/bootstrap.min.js" type="text/javascript"></script>

</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 sectitle">
                    <h2>ایرامال نوین IRAMALL NOVIN</h2>
                </div>
                <div class="col-lg-12">
                    <h3>نام ثبت شده: شرکت ارمغان توسعه</h3>
                    <h4>تلفن ها : 09197741975 - 09197741976</h4>
                    <strong>موضوع فعالیت : ارائه پلتفرم موبایل مشاهده، انتخاب، سفارش و تحویل کالا و خدمات</strong><br />
                    امروزه همه ما برای سفارش کالا و خدمات مورد نیاز  خود از محل کار و یا منزل مشکل داریم و دراینترنت برای آن جستجو کرده  و  جای مناسب پیدا نمی  کنیم و اگر هم پیدا شد مطمئن نیستیم که کیفیت آن چطور است.
                    <br />
                    <br />
                    برای همین ما تصمیم به راه اندازیاپلیکیشن ایرامال نوین گرفتیم. شاید برای شما سوال باشد که ایرامال دقیقا چه کاری انجام می دهد؟ ما قراراست خدمات یک مجتمع‌ (بازار) را ارائه کنیم. یعنی مجموعه‌ای از بهترین فروشگاهها راکنار هم قرار بدهیم و شما براساس سلیقه و محل زندگیتان از بین این فروشگاهها انتخاب کنید. علاوه بر آن می توانید از قسمت آگهی های رایگان استفاده نمایید و کالایی را که دیگر نیاز ندارید بفروشیدو یا کالا وخدمات مورد نیاز تان را تهیه نمایید.
                </div>
            </div>
        </div>
    </form>
</body>
</html>
