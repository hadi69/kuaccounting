﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class market_messages : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        using (DbProxy proxy = new DbProxy())
        {
            int vendorID = Helper.GetInt(Request.QueryString["vid"], Null.NullInteger);
            int userID = Helper.GetInt(Request.QueryString["uid"], Null.NullInteger);
            bool all = Helper.GetInt(Request.QueryString["all"], Null.NullInteger) == 1;
            string query = @"SELECT VendorsMessages.ID, VendorsMessages.VendorID, VendorsMessages.Msg, VendorsMessages.Date, VendorsMessages.Seen
, [Grouping].Title FROM VendorsMessages 
LEFT JOIN Users ON VendorsMessages.VendorID = Users.ID
LEFT JOIN [Grouping] ON [Grouping].ID = Users.RelatedGroupID
WHERE VendorsMessages.Msg IS NOT NULL";
            if (!all)
                query += " AND (Seen IS NULL OR Seen = 0)";
            if (vendorID > 0)
                query += " AND VendorsMessages.VendorID = " + vendorID;
            //if (userID > 0)
                query += " AND VendorsMessages.UserID = " + userID;
            query += " ORDER BY VendorsMessages.Date DESC";

            IDataReader reader = proxy.ExecuteReader(query);
            mItems.DataSource = reader;
            mItems.DataBind();
            reader.Close();
            //Helper.FormatDate(reader.GetDateTime(3), "yyyy-MM-dd HH:mm")


            //string ids = Helper.GetString(request.QueryString["ids"], null);
            query = "UPDATE VendorsMessages SET Seen=1 WHERE 1 = 1";
            if (vendorID > 0)
                query += " AND VendorsMessages.VendorID = " + vendorID;
            //if (userID > 0)
                query += " AND VendorsMessages.UserID = " + userID;
            proxy.RunQuery(query);
        }
    }
}