﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="showbasket.aspx.cs" Inherits="market_showbasket" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width=device-width, height=device-height,  initial-scale=1.0, user-scalable=no;user-scalable=0;" />
    <title></title>
    <link href="<%= ResolveClientUrl("~/") %>styles/style.css" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveClientUrl("~/") %>styles/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveClientUrl("~/") %>styles/bootstrap-rtl.css" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveClientUrl("~/") %>styles/BYekan.css" rel="stylesheet" type="text/css" />
    <link href="market.css" rel="stylesheet" type="text/css" />

    <script src="<%= ResolveClientUrl("~/") %>js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="<%= ResolveClientUrl("~/") %>js/bootstrap.min.js" type="text/javascript"></script>

</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 sectitle">
                    اطلاعات سفارش
                </div>
            </div>
            <div class="section">
                <div class="row">
                    <div class="col-lg-12 form-group-container">
                        <div class="form-group">
                            <label class="left-aligned-label col-xs-6 col-sm-4">کد سفارش :</label>
                            <div class="col-xs-6 col-sm-8 form-col-without-padding">
                                <asp:Label runat="server" ID="mCode" Text="asd asd asd" CssClass="value" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 form-group-container">
                        <div class="form-group">
                            <label class="left-aligned-label col-xs-6 col-sm-4">وضعیت سفارش :</label>
                            <div class="col-xs-6 col-sm-8 form-col-without-padding">
                                <asp:Label runat="server" ID="mStatus" Text="asd asd asd" CssClass="value" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 form-group-container">
                        <div class="form-group">
                            <label class="left-aligned-label col-xs-6 col-sm-4">مبلغ قابل پرداخت :</label>
                            <div class="col-xs-6 col-sm-8 form-col-without-padding">
                                <asp:Label runat="server" ID="mPrice" Text="asd asd asd" CssClass="value" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 form-group-container">
                        <div class="form-group">
                            <label class="left-aligned-label col-xs-6 col-sm-4">روش پرداخت :</label>
                            <div class="col-xs-6 col-sm-8 form-col-without-padding">
                                <asp:Label runat="server" ID="mPayMethod" Text="asd asd asd" CssClass="value" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 form-group-container">
                        <div class="form-group">
                            <label class="left-aligned-label col-xs-6 col-sm-4">وضعیت پرداخت :</label>
                            <div class="col-xs-6 col-sm-8 form-col-without-padding">
                                <asp:Label runat="server" ID="mPayStatus" Text="asd asd asd" CssClass="value" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 form-group-container">
                        <div class="form-group">
                            <label class="left-aligned-label col-xs-6 col-sm-4">تاریخ سفارش :</label>
                            <div class="col-xs-6 col-sm-8 form-col-without-padding">
                                <asp:Label runat="server" ID="mDate" Text="asd asd asd" CssClass="value" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 form-group-container">
                        <div class="form-group">
                            <label class="left-aligned-label col-xs-6 col-sm-4">نام فروشگاه :</label>
                            <div class="col-xs-6 col-sm-8 form-col-without-padding">
                                <asp:Label runat="server" ID="mVendorname" Text="asd asd asd" CssClass="value" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 sectitle">
                    اطلاعات سفارش گیرنده
                </div>
            </div>
            <div class="section">
                <div class="row">
                    <div class="col-lg-12 form-group-container">
                        <div class="form-group">
                            <label class="left-aligned-label col-xs-6 col-sm-4">نام و نام خانوادگی :</label>
                            <div class="col-xs-6 col-sm-8 form-col-without-padding">
                                <asp:Label runat="server" ID="mUsername" Text="asd asd asd" CssClass="value" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 form-group-container">
                        <div class="form-group">
                            <label class="left-aligned-label col-xs-6 col-sm-4">شماره تماس :</label>
                            <div class="col-xs-6 col-sm-8 form-col-without-padding">
                                <asp:Label runat="server" ID="mMobile" Text="asd asd asd" CssClass="value" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 form-group-container">
                        <div class="form-group">
                            <label class="left-aligned-label col-xs-6 col-sm-4">آدرس :</label>
                            <div class="col-xs-6 col-sm-8 form-col-without-padding">
                                <asp:Label runat="server" ID="mAddress" Text="asd asd asd" CssClass="value" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 form-group-container">
                        <div class="form-group">
                            <label class="left-aligned-label col-xs-6 col-sm-4">زمان تحویل :</label>
                            <div class="col-xs-6 col-sm-8 form-col-without-padding">
                                <asp:Label runat="server" ID="mDeliveryTime" Text="asd asd asd" CssClass="value" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 sectitle">
                    ریز سفارش
                </div>
            </div>
            <asp:Repeater runat="server" ID="mItems">
                <ItemTemplate>
                    <div class="row basketitem">
                        <div class="col-xs-3">
                            <img src="<%= ResolveClientUrl("~/") %><%# Eval("MainImage") %>" class="full" />
                        </div>
                        <div class="col-xs-9">
                            <div class="row">
                                <div class="col-xs-5 left-aligned-label"><%# Eval("ServicePriceName") %></div>
                                <div class="col-xs-7">
                                    <div class="form-group">
                                        <label class="left-aligned-label col-xs-8">قیمت واحد :</label>
                                        <div class="col-xs-4 form-col-without-padding">
                                            <span class="value"><%# Eval("Price", "{0:N0}") %></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-5 left-aligned-label">
                                    <div class="form-group">
                                        <label class="left-aligned-label col-xs-9 form-col-without-padding">تعداد :</label>
                                        <div class="col-xs-3 form-col-without-padding">
                                            <span class="value"><%# Eval("Num") %></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-7">
                                    <div class="form-group">
                                        <label class="left-aligned-label col-xs-7">قیمت کل :</label>
                                        <div class="col-xs-5 form-col-without-padding">
                                            <span class="value"><%# Eval("TotalPrice", "{0:N0}") %></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </form>
</body>
</html>
