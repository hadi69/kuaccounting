﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true"
    CodeFile="welcome.aspx.cs" Inherits="welcome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <JP:PageHeader ID="pageHeader" runat="server" Text="Lang.WelcomeMessage" />
    <div class="row">
        <div class="col-xs-12">
            <div class="r tc noacc">
                <h1>
                    <asp:Label runat="server" ID="mUsername"></asp:Label>
                </h1>
            </div>
        </div>
    </div>
    <div class="row welcomemenu margin-100-top">
<%--        <div class="col-xs-12 col-md-6 col-lg-4 pill-container">
            <a href="menu/sys.aspx" access="mainmenu,sys" level="3">
                <div class="pill">
                    <div class="icon">
                        <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-system-32" />
                    </div>
                    <div class="text">
                        <asp:Label ID="Label11" runat="server" Text="Label">Lang.MManagement</asp:Label>
                    </div>
                </div>
            </a>
        </div>--%>
        <div class="col-xs-12 col-md-6 col-lg-3 pill-container">
            <a href="menu/admin.aspx" access="mainmenu,admin">
                <div class="pill">
                    <div class="icon">
                        <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-admin-32" />
                    </div>
                    <div class="text">
                        <asp:Label ID="Label10" runat="server" Text="Label">Lang.Management</asp:Label>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3 pill-container">
            <a href="menu/accounting.aspx" access="mainmenu,accounting">
                <div class="pill">
                    <div class="icon">
                        <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-abacus-32" />
                    </div>
                    <div class="text">
                        <asp:Label ID="Label3" runat="server" Text="Label">Lang.MAccounting</asp:Label>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3 pill-container">
            <a href="menu/sales.aspx" access="mainmenu,sales">
                <div class="pill">
                    <div class="icon">
                        <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-cart-32" />
                    </div>
                    <div class="text">
                        <asp:Label ID="Label1" runat="server" Text="Label">Lang.SellAndBuy</asp:Label>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3 pill-container">
            <a href="menu/reports.aspx" access="mainmenu,reports">
                <div class="pill">
                    <div class="icon">
                        <img src="<%= UIHelper.DummyImage() %>" class="sprites sprites-reports-32" />
                    </div>
                    <div class="text">
                        <asp:Label ID="Label4" runat="server" Text="Label">Lang.Reports</asp:Label>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3 pill-container">
            <%--<asp:Image CssClass="sprites sprites-logo2 logo2" runat="server"/>--%>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            //if (!_Context.isAdmin)
                $(".welcomemenu a").each(enableMenu);

            if (_Context.isAccountingVer)
                $('.noacc').hide();
        });
    </script>
</asp:Content>
