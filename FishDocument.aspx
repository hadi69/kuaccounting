﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="FishDocument.aspx.cs" Inherits="FishDocument" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">
    <div class="row">
        <div class="col-xs-12">
            <div id="bar">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
            <div class="form-group">
                <asp:Label ID="Label2" runat="server" Text="Lang.Year" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"></asp:Label>
                <div class="col-xs-12 col-sm-8 form-col-without-padding">
                    <asp:DropDownList ID="ddlYear" runat="server" PlaceHolder="YYYY" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
            <div class="form-group">
                <asp:Label ID="Label1" runat="server" Text="Lang.Month" PlaceHolder="MM" CssClass="left-aligned-label control-label col-xs-12 col-sm-4"></asp:Label>
                <div class="col-xs-12 col-sm-8 form-col-without-padding">
                    <asp:DropDownList ID="ddlMonth" runat="server" CssClass="chosen-select chosen-rtl">
                        <asp:ListItem Value="01">Lang.Farvardin</asp:ListItem>
                        <asp:ListItem Value="02">Lang.Ordibehesht</asp:ListItem>
                        <asp:ListItem Value="03">Lang.Khordad</asp:ListItem>
                        <asp:ListItem Value="04">Lang.Tir</asp:ListItem>
                        <asp:ListItem Value="05">Lang.Mordad</asp:ListItem>
                        <asp:ListItem Value="06">Lang.Shahrivar</asp:ListItem>
                        <asp:ListItem Value="07">Lang.Mehr</asp:ListItem>
                        <asp:ListItem Value="08">Lang.Aban</asp:ListItem>
                        <asp:ListItem Value="09">Lang.Azar</asp:ListItem>
                        <asp:ListItem Value="10">Lang.Dey</asp:ListItem>
                        <asp:ListItem Value="11">Lang.Bahman</asp:ListItem>
                        <asp:ListItem Value="12">Lang.Esfand</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
            <asp:Label ID="Label3" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.BankTopic</asp:Label>
            <div class="form-group">
                <div class="col-xs-12 col-sm-8 form-col-without-padding">
                    <asp:DropDownList ID="ddlBank" runat="server" CssClass="chosen-select chosen-rtl" />
                </div>
            </div>
        </div>
    </div>

    <div class="row margin-5-top dashed-bottom padding-10-bottom">
        <div class="col-xs-12 col-md-4 col-lg-3 form-group-container">
            <asp:Button ID="CreateFishDocument" runat="server" Text="Lang.TotalWageDocument" OnClick="CreateFishDocument_Click" CssClass="btn btn-xs btn-primary btn-block" />
        </div>
        <div class="col-xs-12 col-md-4 col-lg-3 form-group-container">
            <asp:Button ID="CreateFishProjectDocument" runat="server" Text="Lang.TotalWageDocumentByProject" CssClass="btn btn-xs btn-primary btn-block" OnClick="CreateFishProjectDocument_Click" />
        </div>
        <div class="col-xs-12 col-md-4 col-lg-3 form-group-container">
            <asp:Button ID="SettlementDocument" runat="server" Text="Lang.WageInsertDocument" OnClick="SettlementDocument_Click" CssClass="btn btn-xs btn-primary btn-block" />
        </div>
        <div class="col-xs-12 col-md-4 col-lg-3 form-group-container">
            <asp:Button ID="InsuranceDocument" runat="server" Text="Lang.InsuranceDocument" CssClass="btn btn-xs btn-primary btn-block" OnClick="InsuranceDocument_Click" />
        </div>
        <div class="col-xs-12 col-md-4 col-lg-3 form-group-container">
            <asp:Button ID="ShareDocument" runat="server" Text="Lang.WageDistributionDocument" OnClick="ShareDocument_Click" CssClass="btn btn-xs btn-primary btn-block" />
        </div>
    </div>
    
    <div class="row margin-10-top">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
            <div class="form-group">
                <asp:Label Text="Lang.Ward" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4" />
                <div class="col-xs-12 col-sm-8 form-col-without-padding">
                    <asp:DropDownList runat="server" ID="ddlSection" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
            <div class="form-group">
                <asp:Label Text="Lang.Personnel" runat="server" CssClass="left-aligned-label control-label col-xs-12 col-sm-4" />
                <div class="col-xs-12 col-sm-8 form-col-without-padding">
                    <asp:DropDownList runat="server" ID="ddlPersonnel" CssClass="chosen-select chosen-rtl"></asp:DropDownList>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <asp:Button ID="CreateTestDocument" Text="Lang.ManuallyCreateDocument" runat="server" OnClick="CreateTestDocument_Click" CssClass="btn btn-xs btn-primary btn-block" />
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <asp:Button ID="CreateTestDocumentByProject" Text="Lang.CreateTestDocumentByProject" runat="server" CssClass="btn btn-xs btn-primary btn-block" OnClick="CreateTestDocumentByProject_Click" />
        </div>
    </div>
    <div class="row margin-10-top">
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    </div>
    <asp:GridView ID="mGrid" AutoGenerateColumns="False" CssClass="ScrollPanelCartable Grid table table-bordered table-condensed zebra"
        scrolling="Vertical" runat="server" BorderStyle="None" OnRowDataBound="mGrid_RowDataBound"
        AllowSorting="True" OnRowDeleting="mGrid_RowDeleting">
        <Columns>
            <asp:TemplateField HeaderText="Lang.ColRowNr">
                <ItemTemplate>
                    <%# Container.DataItemIndex + 1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ID" HeaderText="ID" Visible="False" />
            <asp:BoundField DataField="DoComments" HeaderText="Lang.Comments" ItemStyle-HorizontalAlign="Center">
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="Date" HeaderText="Lang.Date" />
            <asp:BoundField DataField="No" HeaderText="Lang.DocumentNumber" ItemStyle-HorizontalAlign="Center">
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:BoundField>
            <asp:TemplateField ShowHeader="False" Visible="False">
                <ItemTemplate>
                    <asp:LinkButton ID="lbDelete" runat="server" CausesValidation="False" CommandName="Delete" Text="Lang.ColDel" OnClientClick="return confirm(Lang.ConfirmDelete); "></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle CssClass="GridHeader" />
        <RowStyle CssClass="GridRow" />
        <SelectedRowStyle CssClass="GridRowSelected" />
        <FooterStyle CssClass="GridHeader" HorizontalAlign="Center" />
    </asp:GridView>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#bar").prepend(breadcrumb({
                title: Lang.FishDocument,
                parent: {
                    title: Lang.MSalary,
                    url: 'menu/salary.aspx'
                }
            }));
            setClassSalary();
            makeChosen();
        });
    </script>
</asp:Content>

