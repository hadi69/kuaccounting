﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;

public partial class list : System.Web.UI.Page
{
    TableInfo tInfo = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //string url = HttpContext.Current.Request.Url.AbsoluteUri;
            //int indexTable = url.IndexOf("table=");
            // url = url.Substring(indexTable + 6);
            // int index = url.IndexOf("&");
            //if (index == -1)
            //{
            //    index = url.Length;
            //}
            //Page.Title = LangProvider.Instance[url.Substring(0, index)];

               
            TableName = Request.QueryString["table"];
            ViewName = Request.QueryString["view"];
            QueryName = Request.QueryString["qd"];
            tInfo = TableController.Instance[TableName];
            if (string.IsNullOrEmpty(ViewName) && tInfo != null)
                ViewName = tInfo.List;
            if (string.IsNullOrEmpty(QueryName) && tInfo != null)
                QueryName = tInfo.ListQuery;
        }
        else
            tInfo = TableController.Instance[TableName];
        if (SiteSettings.User == null || !SiteSettings.User.HasAccess(TableName, AccessType.View))
            Response.Redirect("~/default.aspx");
        BindView();
    }
    string FindViewFile()
    {
        if (string.IsNullOrEmpty(ViewName))
            return null;
        string path = Server.MapPath("~/views/" + ViewName + ".htm");
        if (File.Exists(path))
            return path;
        //string path = Server.MapPath("~/user0/" + ViewName + ".htm");
        //if (File.Exists(path))
        //    return path;
        //path = Server.MapPath("~/user1/" + ViewName + ".htm");
        //if (File.Exists(path))
        //    return path;
        //path = Server.MapPath("~/user2/" + ViewName + ".htm");
        //if (File.Exists(path))
        //    return path;
        return null;
    }
    string FindViewJSFile()
    {
        if (string.IsNullOrEmpty(ViewName))
            return null;
        string path = Server.MapPath("~/viewjs/" + ViewName + ".js");
        if (File.Exists(path))
            return path;
        return null;
    }
    void BindView()
    {
        mHolder.Controls.Clear();

        if (string.IsNullOrEmpty(ViewName))
            return;
        string path = FindViewFile();
        if (string.IsNullOrEmpty(path))
            return;
        string content = File.ReadAllText(path);
        // purge bounding html
        int start = content.IndexOf("<body>");
        if (start >= 0)
            content = content.Substring(start + "<body>".Length);
        start = content.IndexOf("</body>");
        if (start >= 0)
            content = content.Substring(0, start);
        content = Helper.ParseVars(content, tInfo, null, Request).Replace("_TODAY_", Helper.FormatDate(DateTime.Now, null));
        content = LangProvider.Instance.ParseLang(content);
        mHolder.Controls.Add(new LiteralControl(content));

        string tableInit = "";
        if (tInfo != null)
            tableInit = string.Format(@"
_Context.table = '{0}';
_Context.title = '{1}';
_Context.query = '{2}';
_Context.refreshInterval = '{3}';"
                , tInfo.Name, tInfo.Title, QueryName, Helper.GetInt(DbProxy.Instance.GetConfig("RefreshInterval"), 10) * 1000);

        string viewjs = "run();";
        if (FindViewJSFile() != null)
            viewjs = @"$.getScript('viewjs/{0}.js?time={1}', function(){view_{0}();});".Replace("{0}", ViewName).Replace("{1}", DateTime.Now.ToOADate().ToString());
        string script = @"<script type='text/javascript'>
         function checkReady() {
            if (typeof _Context == 'undefined') {
                setTimeout(checkReady, 1000);
                return;
            }
            tableInit
            _Context.today = '_TODAY_';
            _Context.promptOpen = false;
            _Context.ready = true;
            viewjs
        }
        $(function () {
            checkReady();
            TweakUI();
        });	
</script>".Replace("tableInit", tableInit).Replace("_TODAY_", Helper.FormatDate(DateTime.Now, null)).Replace("viewjs", viewjs);
        Page.RegisterStartupScript("pageinit", script);
        #region not used
        //StringBuilder b = new StringBuilder();
        //string url = "getlist.aspx?table=" + TableName;
        //StringBuilder colModel = new StringBuilder(" ");
        //StringBuilder searchitems = new StringBuilder(" ");
        //string search = null;
        //for (int i = 0; i < mInfo.Fields.Count; i++)
        //{
        //    FieldInfo f = mInfo.Fields[i];
        //    if (f.List)
        //        colModel.AppendFormat("{display: '{0}', name : '{1}', width : {2}, sortable : true, align: 'left'},"
        //            , f.Display, f.Name, f.Width);
        //    if (f.Search)
        //    {
        //        searchitems.AppendFormat("{display: 'Name', name : 'Name'},", f.Display, f.Name);
        //        if (search == null)
        //            search = f.Name;
        //    }
        //}

        //$(document).ready(new function(){
        //    $("#fgrdProduct").flexigrid(
        //    {
        //        url: 'FlexiGridService.asmx/GetProductList',
        //        dataType: 'xml',
        //        colModel : 
        //            [
        //                {display: 'Id', name : 'Id', width : 20, sortable : true, align: 'left'},
        //                {display: 'Name', name : 'Name', width : 180, sortable : true, align: 'left'},
        //                {display: 'Description', name : 'Description', width : 180, sortable : true, align: 'left'},
        //                {display: 'Unit', name : 'Unit', width : 120, sortable : true, align: 'left'},
        //                {display: 'Unit Price', name : 'UnitPrice', width : 130, sortable : true, align: 'left', hide: false},
        //                {display: 'Create Date', name : 'CreateDate', width : 80, sortable : true, align: 'left'}
        //            ],
        //        searchitems : 
        //            [
        //                {display: 'Name', name : 'Name'},
        //                {display: 'Description', name : 'Description'},
        //                {display: 'Unit', name : 'Unit'},
        //                {display: 'Unit Price', name : 'UnitPrice'},
        //                {display: 'Create Date', name : 'CreateDate'},
        //                {display: 'Id', name : 'Id', isdefault: true}
        //            ],
        //        sortname: "Name",
        //        sortorder: "asc",
        //        usepager: true,
        //        title: 'List of Products',
        //        useRp: true,
        //        rp: gridRp,
        //        showTableToggleBtn: gridToggle,
        //        width: gridWidth,
        //        
        //        height: 200
        //    });
        //});
        #endregion
    }
    public string TableName
    {
        get
        {
            return Helper.GetString(ViewState["TableName"], null);
        }
        set
        {
            ViewState["TableName"] = value;
        }
    }
    public string ViewName
    {
        get
        {
            return Helper.GetString(ViewState["ViewName"], null);
        }
        set
        {
            ViewState["ViewName"] = value;
        }
    }
    public string QueryName
    {
        get
        {
            return Helper.GetString(ViewState["QueryName"], "");
        }
        set
        {
            ViewState["QueryName"] = value;
        }
    }
}