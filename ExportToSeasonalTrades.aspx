﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="ExportToSeasonalTrades.aspx.cs" Inherits="ExportToSeasonalTrades" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cnt" runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="row">
        <div class="col-xs-12">
            <JP:PageHeader runat="server" Text="Lang.ExportToSeasonalTrades" />
        </div>
    </div>
    <div class="well callout padding-5">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label22" runat="server" AssociatedControlID="fFromDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.DateRange</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:DropDownList runat="server" ID="fRangeType" OnSelectedIndexChanged="fRangeType_SelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem Text="Lang.FinanceYear" Value="0" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Lang.Spring" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Lang.Summer" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Lang.Autumn" Value="3"></asp:ListItem>
                            <asp:ListItem Text="Lang.Winter" Value="4"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" AssociatedControlID="fFromDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.FromDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="fFromDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mex" runat="server" TargetControlID="fFromDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label3" runat="server" AssociatedControlID="fToDate" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ToDate</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="fToDate" runat="server" CssClass="T2 dateselect form-control input-sm"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="mee1" runat="server" TargetControlID="fToDate" Mask="9999/99/99"
                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                            MaskType="None" ClearMaskOnLostFocus="false" DisplayMoney="None" AcceptNegative="None" ErrorTooltipEnabled="True" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="well padding-5">
        <div class="row">
            <div class="col-xs-12">
                <h5 class="dashed-bottom padding-5-bottom yekan-family">
                    <asp:Label runat="server" Text="Lang.CompanyInfo" />
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label1" runat="server" AssociatedControlID="companyNameField" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.CompanyName</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="companyNameField" runat="server" CssClass="T2 form-control input-sm" placeholder="نام شخص حقوقی(شرکت ...) /نام بنگاه اقتصادی(مغازه...)"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label4" runat="server" AssociatedControlID="oldEconomicNoField" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.OldEconomicNo</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="oldEconomicNoField" runat="server" CssClass="T2 form-control input-sm" MaxLength="16"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label5" runat="server" AssociatedControlID="newEconomicNoField" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.NewEconomicNo</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="newEconomicNoField" runat="server" CssClass="T2 form-control input-sm" MaxLength="20"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label6" runat="server" AssociatedControlID="nationalCodeField" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.NationalCode</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="nationalCodeField" runat="server" CssClass="T2 form-control input-sm" MaxLength="11"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label7" runat="server" AssociatedControlID="taxPayerTypeField" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.TaxPayerType</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:DropDownList ID="taxPayerTypeField" runat="server" CssClass="T2 form-control input-sm">
                            <asp:ListItem Text="حقیقی" Value="1" />
                            <asp:ListItem Text="حقوقی غیر دولتی" Value="2" />
                            <asp:ListItem Text="حقوقی دولتی وزارت خانه ها و سازمان ها" Value="3" />
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label8" runat="server" AssociatedControlID="postalCodeField" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.PostalCode</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="postalCodeField" runat="server" CssClass="T2 form-control input-sm" MaxLength="10"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label9" runat="server" AssociatedControlID="countryField" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.Country</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="countryField" runat="server" CssClass="T2 form-control input-sm" Text="ایران"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label10" runat="server" AssociatedControlID="telCodeField" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.TelCode</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="telCodeField" runat="server" CssClass="T2 form-control input-sm" MaxLength="10"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label11" runat="server" AssociatedControlID="telNoField" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.Tel</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="telNoField" runat="server" CssClass="T2 form-control input-sm" MaxLength="50"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label12" runat="server" AssociatedControlID="faxNoField" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.Fax</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="faxNoField" runat="server" CssClass="T2 form-control input-sm" MaxLength="50"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label13" runat="server" AssociatedControlID="registrationNoField" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.RegistrationNo</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="registrationNoField" runat="server" CssClass="T2 form-control input-sm" MaxLength="13"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label14" runat="server" AssociatedControlID="hozehField" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.Hozeh</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="hozehField" runat="server" CssClass="T2 form-control input-sm" MaxLength="7"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label15" runat="server" AssociatedControlID="Address" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.Address</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="Address" runat="server" CssClass="T2 form-control input-sm" MaxLength="70"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label16" runat="server" AssociatedControlID="ShahrCode" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.ShahrCode</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="ShahrCode" runat="server" CssClass="T2 form-control input-sm validate[required,custom[integer]]" placeholder="کد  7 رقمی شهر از فایل excel"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label17" runat="server" AssociatedControlID="EdareKolCode" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.EdareKolCode</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="EdareKolCode" runat="server" CssClass="T2 form-control input-sm validate[required,custom[integer]]" placeholder="کد  2 رقمی اداره کل از فایل excel"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label18" runat="server" AssociatedControlID="UserName" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.UserName</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="UserName" runat="server" CssClass="T2 form-control input-sm" MaxLength="20"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label19" runat="server" AssociatedControlID="Band" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.Band</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:DropDownList ID="Band" runat="server" CssClass="T2 form-control input-sm">
                            <asp:ListItem Text="شرکت" Value="0" />
                            <asp:ListItem Text="بند الف" Value="1" />
                            <asp:ListItem Text="بند ب" Value="2" />
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label20" runat="server" AssociatedControlID="SabteNam" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.Register</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                       <asp:DropDownList ID="SabteNam" runat="server" CssClass="T2 form-control input-sm">
                            <asp:ListItem Text="نام کاربری دارم" Value="0" />
                            <asp:ListItem Text="کد رهگیری دارم" Value="1" />
                            <asp:ListItem Text="پیش ثبت نام کد اقتصادی نکرده ام" Value="2" />
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 form-group-container">
                <div class="form-group">
                    <asp:Label ID="Label21" runat="server" AssociatedControlID="MoadyName" CssClass="left-aligned-label control-label col-xs-12 col-sm-4">Lang.MoadyName</asp:Label>
                    <div class="col-xs-12 col-sm-8 form-col-without-padding">
                        <asp:TextBox ID="MoadyName" runat="server" CssClass="T2 form-control input-sm" placeholder="نام و نام خانوادگی مدیر عامل(اشخاص حقوقی) /  نام و نام خانوادگی مودی اصلی(اشخاص حقیقی)"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="well callout padding-5">
        <div class="row margin-5-top">
            <div class="col-md-8 col-lg-6 visible-md visible-lg">&nbsp;</div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 form-group-container form-col-without-padding text-right">
                <div class="col-xs-12 col-sm-3 text-right">
                    <asp:Button ID="doTransfer" runat="server" Text="Lang.Export" CssClass="btn btn-xs btn-primary btn-block" OnClick="doExport_Click" />
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12" style="direction: rtl;">
        <asp:Label runat="server" ID="msg" CssClass="label-30 alert alert-success block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
    </div>
    <div class="col-xs-12" style="direction: ltr;">
        <asp:Label runat="server" ID="error" CssClass="label-30 alert alert-warning block padding-0 padding-5-right" EnableViewState="False" Visible="False" />
    </div>
    <script>
        $(function () {
            dateSelect('.dateselect');
            $('.ch').click(function () {
                if ($(this).attr('checked'))
                    $(this).closest('tr').addClass('selected');
                else
                    $(this).closest('tr').removeClass('selected');
            });
            $("#aspnetForm").validationEngine();
        });
    </script>
</asp:Content>

