﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="print.aspx.cs" Inherits="print" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="js/jquery-1.4.1-vsdoc.js" language="javascript" type="text/javascript"></script>
    <script src="js/plugins.js" language="javascript" type="text/javascript"></script>
    <script src="js/JsFarsiCalendar.js" type="text/javascript" language="JavaScript"></script>
    <script src="js/shortcut.js" language="javascript" type="text/javascript"></script>
    <script src="js/site.js" language="javascript" type="text/javascript"></script>
    <link href="styles/style.css" rel="stylesheet" type="text/css" />
    <link href="styles/flexigrid.pack.css" rel="stylesheet" type="text/css" />
    <link href="styles/formly.css" rel="stylesheet" type="text/css" />
    <link href="styles/impromptu.css" rel="stylesheet" type="text/css" />
    <link href="styles/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <link href="styles/calendar.css" rel="stylesheet" type="text/css" />
    <link href="styles/tabs.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="printForm" class='printForm' width="400" runat="server">
    <asp:PlaceHolder runat="server" ID="p"></asp:PlaceHolder>
    </form>
</body>
</html>
