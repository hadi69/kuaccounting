﻿using ExcelLibrary.BinaryFileFormat;
using ExcelLibrary.CompoundDocumentFormat;
using ExcelLibrary.SpreadSheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BankDifference : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Page.SetLang();
            mReportSetting.ReportName = "BankDifference";
        }
        mReportSetting.SaveClick += new EventHandler(mReportSetting_SaveClick);
        mReportSetting.ReportSettingSelect += new EventHandler(mReportSetting_ReportSettingSelect);
    }

    #region Settings
    void mReportSetting_ReportSettingSelect(object sender, EventArgs e)
    {
        try
        {
            ReportSettingInfo info = mReportSetting.SelectedReportSettings;
            if (info == null)
                return;
            string[] lines = info.Settings.Split('\n');
            mHeaderRow1.Checked = Helper.GetBool(lines[0].Trim('\r'), mHeaderRow1.Checked);
            mBankTopicName.Text = lines[1].Trim('\r');
            mBankTopicCode.Value = lines[2].Trim('\r');
            mRemainIndex.Text = lines[3].Trim('\r');
            mDateIndex.Text = lines[4].Trim('\r');
            mReverseRows.Checked = Helper.GetBool(lines[5].Trim('\r'), mReverseRows.Checked);
        }
        catch (Exception ex)
        {
            ShowMessage(LangProvider.Instance["Error"] + " : " + ex.Message);
        }
    }

    void mReportSetting_SaveClick(object sender, EventArgs e)
    {
        try
        {
            StringBuilder b = new StringBuilder();
            b.AppendLine(mHeaderRow1.Checked.ToString()); //0
            b.AppendLine(mBankTopicName.Text); //1
            b.AppendLine(mBankTopicCode.Value); //2
            b.AppendLine(mRemainIndex.Text); //3
            b.AppendLine(mDateIndex.Text); //4
            b.AppendLine(mReverseRows.Checked.ToString()); //5

            mReportSetting.Save(b.ToString());
        }
        catch (Exception ex)
        {
            ShowMessage(LangProvider.Instance["Error"] + " : " + ex.Message);
        }
    }
    #endregion
    string TempDir
    {
        get
        {
            string dest = Server.MapPath("~/temp");
            dest = Path.Combine(dest, SiteSettings.UserID.ToString());
            if (!Directory.Exists(dest))
                Directory.CreateDirectory(dest);
            return dest;
        }
    }
    protected void doUpload_Click(object sender, EventArgs e)
    {
        if (mFileUpload.HasFile)
        {
            mFileName.Text = Path.GetFileName(mFileUpload.FileName);
            string dest = Path.Combine(TempDir, mFileName.Text);
            if (File.Exists(dest))
                try { File.Delete(dest); }
                catch { }
            mFileUpload.SaveAs(dest);
            BindPreview();
            mDetail.Visible = mHeaders.Text != "";
        }
    }
    class ExcelRow
    {
        public decimal Value;
        public DateTime Date;
    }
    List<ExcelRow> GetExcelRows()
    {
        int remainCol = Helper.GetInt(mRemainIndex.Text, -1) - 1;
        int dateCol = Helper.GetInt(mDateIndex.Text, -1) - 1;
        if (remainCol == -1 || dateCol == -1)
            return null;
        string file = Path.Combine(TempDir, mFileName.Text);
        CompoundDocument doc = null;
        try
        {
            doc = CompoundDocument.Open(file);
            byte[] bookdata = doc.GetStreamData("Workbook");
            if (bookdata == null) return null;
            Workbook book = WorkbookDecoder.Decode(new MemoryStream(bookdata));

            Worksheet sheet = book.Worksheets[0];

            int rowIndex = Math.Max(sheet.Cells.FirstRowIndex, 0);
            if (mHeaderRow1.Checked)
                rowIndex += 1;

            // check if there is any row
            if (rowIndex > sheet.Cells.LastRowIndex)
                return null;

            List<ExcelRow> res = new List<ExcelRow>();
            for (; rowIndex <= sheet.Cells.LastRowIndex; rowIndex++)
            {
                Row row = sheet.Cells.GetRow(rowIndex);
                decimal value = Helper.GetDecimal(Helper.GetString(row.GetCell(remainCol).Value, "").Trim(), 0);
                DateTime date = Helper.ParseDate(Helper.GetString(row.GetCell(dateCol).Value, "").Trim(), Null.NullDate);
                res.Add(new ExcelRow() { Value = value, Date = date });
            }
            return res;
        }
        finally
        {
            if (doc != null)
                doc.Close();
        }
    }

    protected void doCompare_Click(object sender, EventArgs e)
    {
        string file = Path.Combine(TempDir, mFileName.Text);
        DateTime startDate = Helper.ParseDate(mStartDate.Text, DateTime.Now.AddYears(-1));
        DateTime endDate = Helper.ParseDate(mEndDate.Text, DateTime.Now.AddYears(1));
        using (DbProxy proxy = new DbProxy())
            try
            {
                List<ExcelRow> rows = GetExcelRows();
                if (rows == null || rows.Count == 0)
                    return;
                if (mReverseRows.Checked)
                    rows.Reverse();

                // Get Voucher Items
                DataTable dt = proxy.FillData(string.Format(@"SELECT Owe, Due, DocDate, DocNo, DocumentAccounting.No, DocumentAccountingID, 0 Processed 
FROM DocumentAccountingItems INNER JOIN DocumentAccounting ON DocumentAccounting.ID = DocumentAccountingID
WHERE TreePath={0} AND DocDate >= {1} AND DocDate <= {2}", Helper.QoutedString(mBankTopicCode.Value)
                                                         , Helper.QoutedDate(startDate), Helper.QoutedDate(endDate)), "values");

                int rowIndex = 0;
                decimal lastValue = decimal.MinValue;
                StringBuilder builder = new StringBuilder();
                builder.Append(string.Format("<table cellspacing=0 border=1><tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td></tr>"
                    , LangProvider.Instance["ColRowNr"], LangProvider.Instance["Value"], LangProvider.Instance["Remained"], LangProvider.Instance["Date"]
                    , LangProvider.Instance["DocumentNumber"], LangProvider.Instance["DocNo"]));
                for (; rowIndex < rows.Count; rowIndex++)
                {
                    decimal value = rows[rowIndex].Value;
                    DateTime date = rows[rowIndex].Date;
                    if (rowIndex == 0)
                    {
                        builder.Append(string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td></tr>"
                            , rowIndex, value, value, Helper.FormatDate(date, null), "", ""));
                        lastValue = value;
                        continue;
                    }
                    decimal remain = value - lastValue;
                    string no = "", docNo = "";
                    for (int i = 0; i < dt.Rows.Count; i++)
                        if ((int)dt.Rows[i]["Processed"] == 0
                            && Helper.GetDateTime(dt.Rows[i], "DocDate", DateTime.Now).ToString("yyyy-MM-dd") == date.ToString("yyyy-MM-dd"))
                        {
                            if ((remain > 0 && Helper.GetDecimal(dt.Rows[i], "Owe", 0) == remain)
                                || (remain < 0 && Helper.GetDecimal(dt.Rows[i], "Due", 0) == -remain))
                            {
                                dt.Rows[i]["Processed"] = 1;
                                no = Helper.GetString(dt.Rows[i], "No", "");
                                docNo = Helper.GetString(dt.Rows[i], "DocNo", "");
                                break;
                            }
                        }
                    builder.Append(string.Format("<tr {0}><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td>{6}</td></tr>"
                        , no == "" ? " class='selected'" : "", rowIndex, remain, value, Helper.FormatDate(date, null), no, docNo));
                    lastValue = value;
                }

                // and display the voucher items that are not processed (= are not in excel)
                for (int i = 0; i < dt.Rows.Count; i++)
                    if ((int)dt.Rows[i]["Processed"] == 0)
                    {
                        decimal val = Helper.GetDecimal(dt.Rows[i], "Owe", 0) - Helper.GetDecimal(dt.Rows[i], "Due", 0);
                        builder.Append(string.Format("<tr {0}><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td>{6}</td></tr>"
                        , "class='selected'", rowIndex + i, val, "", Helper.FormatDate(Helper.GetDateTime(dt.Rows[i], "DocDate", DateTime.Now), null), Helper.GetString(dt.Rows[i], "No", ""), Helper.GetString(dt.Rows[i], "DocNo", "")));
                    }
                builder.Append("</table>");
                mResult.Controls.Add(new LiteralControl(builder.ToString()));
                ShowMessage(LangProvider.Instance["Done"]);
            }
            catch (Exception ex)
            {
                ShowMessage("Error: " + ex.Message);
                if (proxy != null)
                    proxy.RollbackTransaction();
            }
    }

    void BindPreview()
    {
        mHeaders.Text = "";
        mPreview.Text = "";
        string file = Path.Combine(TempDir, mFileName.Text);
        CompoundDocument doc = null;
        try
        {
            doc = CompoundDocument.Open(file);
            byte[] bookdata = doc.GetStreamData("Workbook");
            if (bookdata == null) return;
            Workbook book = WorkbookDecoder.Decode(new MemoryStream(bookdata));

            Worksheet sheet = book.Worksheets[0];
            int rowIndex = Math.Max(sheet.Cells.FirstRowIndex, 0);
            Row row = sheet.Cells.GetRow(rowIndex);
            for (int colIndex = sheet.Cells.FirstColIndex; colIndex <= sheet.Cells.LastColIndex; colIndex++)
            {
                string title = Helper.GetString(row.GetCell(colIndex).Value, "");
                mHeaders.Text += title.Replace(',', '_') + ",";
            }
            if (mHeaders.Text.Length == 0)
                return;
            mHeaders.Text = mHeaders.Text.Substring(0, mHeaders.Text.Length - 1);

            StringBuilder b = new StringBuilder();
            b.Append("<table cellspacing=0 border=1>");
            string[] titles = mHeaders.Text.Split(',');
            b.Append("<tr>");
            for (int i = 0; i < titles.Length; i++)
                b.AppendFormat("<td>{0} {1}</td>", LangProvider.Instance["Column"], i + 1);
            b.Append("</tr>");
            b.Append("<tr>");
            for (int i = 0; i < titles.Length; i++)
                b.AppendFormat("<td>{0}</td>", titles[i]);
            b.Append("</tr>");
            rowIndex += 1;
            if (sheet.Cells.LastRowIndex >= rowIndex)
            {
                row = sheet.Cells.GetRow(rowIndex);
                b.Append("<tr>");
                for (int i = 0; i < titles.Length; i++)
                    b.AppendFormat("<td>{0}</td>", Helper.GetString(row.GetCell(sheet.Cells.FirstColIndex + i).Value, ""));
                b.Append("</tr>");
            }
            b.Append("</table>");
            mPreview.Text = b.ToString();
        }
        catch (Exception ex)
        {
            ShowMessage("Error: " + ex.Message);
        }
        finally
        {
            if (doc != null)
                doc.Close();
        }
    }
    private void ShowMessage(string message, string type="danger")
    {
        if (type != "success" && type != "danger")
        {
            type = "danger";
        }

        mMsg.Text = message;
        mMsg.Visible = true;
        mMsg.CssClass += " alert-" + type;
    }

    private void AppendMessage(string message)
    {
        mMsg.Text += message;
    }
}