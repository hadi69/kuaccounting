﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class tipofday : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int visitorID = Helper.GetInt(Request.QueryString["vid"], Null.NullInteger);
        if (visitorID != Null.NullInteger)
        {
            StringBuilder builder = new StringBuilder();
            DataTable dt = DbProxy.Instance.FillData("SELECT * FROM TabletMsg WHERE [Mode] = 0 AND SentDate IS NULL AND VisitorID = " + visitorID, "f");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                builder.AppendLine(Helper.GetString(dt.Rows[i], "Msg", ""));
            }
            DbProxy.Instance.RunQuery("UPDATE TabletMsg SET SentDate = GETDATE() WHERE [Mode] = 0 AND SentDate IS NULL AND VisitorID = " + visitorID);
            Response.Write(builder.ToString());
        }
        Response.End();
    }
}