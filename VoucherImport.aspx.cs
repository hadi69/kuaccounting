﻿using ExcelLibrary.BinaryFileFormat;
using ExcelLibrary.CompoundDocumentFormat;
using ExcelLibrary.SpreadSheet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class VoucherImport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Page.SetLang();
            mReportSetting.ReportName = "VoucherImport";
        }
        BindColumns();
        mReportSetting.SaveClick += new EventHandler(mReportSetting_SaveClick);
        mReportSetting.ReportSettingSelect += new EventHandler(mReportSetting_ReportSettingSelect);
    }

    #region Settings
    void mReportSetting_ReportSettingSelect(object sender, EventArgs e)
    {
        try
        {
            if (mHeaders.Text.Length == 0)
                return;
            string[] titles = mHeaders.Text.Split(',');
            ReportSettingInfo info = mReportSetting.SelectedReportSettings;
            if (info == null)
                return;
            string[] lines = info.Settings.Split('\n');
            mHeaderRow1.Checked = Helper.GetBool(lines[0].Trim('\r'), mHeaderRow1.Checked);
            mOneVoucher.Checked = Helper.GetBool(lines[1].Trim('\r'), mOneVoucher.Checked);
            mType.SelectedValue = lines[2].Trim('\r');

            for (int i = 0; i < titles.Length && (i + 3) < lines.Length; i++)
            {
                DropDownList dp = mColumns.FindControl("dp" + i) as DropDownList;
                TextBox t = mColumns.FindControl("t" + i) as TextBox;
                HiddenField h = mColumns.FindControl("h" + i) as HiddenField;
                DropDownList fld = mColumns.FindControl("fld" + i) as DropDownList;
                TextBox q = mColumns.FindControl("q" + i) as TextBox;
                // string row = dp.SelectedValue + "_" + t.Text + "_" + h.Value + "_" + fld.SelectedValue + "_" + q.Text;
                string[] words = lines[3 + i].Split('_');
                dp.SelectedValue = words[0];
                t.Text = words[1];
                h.Value = words[2];
                fld.SelectedValue = words[3];
                q.Text = words[4];
            }
        }
        catch (Exception ex)
        {
            ShowMessage(LangProvider.Instance["Error"] + " : " + ex.Message);
        }
    }

    void mReportSetting_SaveClick(object sender, EventArgs e)
    {
        try
        {
            if (mHeaders.Text.Length == 0)
                return;
            string[] titles = mHeaders.Text.Split(',');
            StringBuilder b = new StringBuilder();
            b.AppendLine(mHeaderRow1.Checked.ToString()); //0
            b.AppendLine(mOneVoucher.Checked.ToString()); //1
            b.AppendLine(mType.SelectedValue);  //2

            for (int i = 0; i < titles.Length; i++)
            {
                DropDownList dp = mColumns.FindControl("dp" + i) as DropDownList;
                TextBox t = mColumns.FindControl("t" + i) as TextBox;
                HiddenField h = mColumns.FindControl("h" + i) as HiddenField;
                DropDownList fld = mColumns.FindControl("fld" + i) as DropDownList;
                TextBox q = mColumns.FindControl("q" + i) as TextBox;
                string row = dp.SelectedValue + "_" + t.Text + "_" + h.Value + "_" + fld.SelectedValue + "_" + q.Text;
                b.AppendLine(row);  // 3 + i
            }
            mReportSetting.Save(b.ToString());
        }
        catch (Exception ex)
        {
            ShowMessage(LangProvider.Instance["Error"] + " : " + ex.Message);
        }
    }
    #endregion
    string TempDir
    {
        get
        {
            string dest = Server.MapPath("~/temp");
            dest = Path.Combine(dest, SiteSettings.UserID.ToString());
            if (!Directory.Exists(dest))
                Directory.CreateDirectory(dest);
            return dest;
        }
    }
    protected void doUpload_Click(object sender, EventArgs e)
    {
        if (mFileUpload.HasFile)
        {
            mFileName.Text = Path.GetFileName(mFileUpload.FileName);
            string dest = Path.Combine(TempDir, mFileName.Text);
            if (File.Exists(dest))
                try { File.Delete(dest); }
                catch { }
            mFileUpload.SaveAs(dest);
            BindPreview();
            BindColumns();
            mDetail.Visible = mHeaders.Text != "";
        }
    }

    protected void doImport_Click(object sender, EventArgs e)
    {
        if (mHeaders.Text.Length == 0)
            return;
        string[] titles = mHeaders.Text.Split(',');
        string file = Path.Combine(TempDir, mFileName.Text);
        CompoundDocument doc = null;
        DbProxy proxy = null;
        try
        {
            doc = CompoundDocument.Open(file);
            byte[] bookdata = doc.GetStreamData("Workbook");
            if (bookdata == null) return;
            Workbook book = WorkbookDecoder.Decode(new MemoryStream(bookdata));

            Worksheet sheet = book.Worksheets[0];
            proxy = new DbProxy();
            proxy.BeginTransaction();
            if (mType.SelectedValue == "Detail")
                ImportDetail(sheet, proxy, titles);
            else
                ImportMaster(sheet, proxy, titles);
            try
            {
                proxy.CommitTransaction();
            }
            catch { }
        }
        catch (Exception ex)
        {
            ShowMessage("Error: " + ex.Message);
            if (proxy != null)
                proxy.RollbackTransaction();
        }
        finally
        {
            if (doc != null)
                doc.Close();
        }
    }
    private void ImportDetail(Worksheet sheet, DbProxy proxy, string[] titles)
    {
        int rowIndex = Math.Max(sheet.Cells.FirstRowIndex, 0);
        if (mHeaderRow1.Checked)
            rowIndex += 1;
        // check if there is any row
        if (rowIndex > sheet.Cells.LastRowIndex)
            return;
        int voucherID = Null.NullInteger;
        for (; rowIndex <= sheet.Cells.LastRowIndex; rowIndex++)
        {
            Row row = sheet.Cells.GetRow(rowIndex);
            decimal owe = 0, due = 0;
            string field = "", fieldName = "", treePath = "";
            #region find values
            for (int i = 0; i < titles.Length; i++)
            {
                DropDownList dp = mColumns.FindControl("dp" + i) as DropDownList;
                HiddenField h = mColumns.FindControl("h" + i) as HiddenField;
                DropDownList fld = mColumns.FindControl("fld" + i) as DropDownList;
                TextBox q = mColumns.FindControl("q" + i) as TextBox;
                if (dp.SelectedValue == "empty")
                    continue;
                if (dp.SelectedValue == "owe")
                {
                    owe = Helper.GetDecimal(Helper.GetString(row.GetCell(sheet.Cells.FirstColIndex + i).Value, "").Trim(), 0);
                    treePath = h.Value;
                }
                else if (dp.SelectedValue == "due")
                {
                    due = Helper.GetDecimal(Helper.GetString(row.GetCell(sheet.Cells.FirstColIndex + i).Value, "").Trim(), 0);
                    treePath = h.Value;
                }
                else
                {
                    string field1 = Helper.GetString(row.GetCell(sheet.Cells.FirstColIndex + i).Value, "").Trim();
                    string fieldName1 = fld.SelectedValue;
                    if (fieldName1 == "DocDate")
                        field1 = Helper.ParseDate(field1, DateTime.Now).ToString("yyyy-MM-dd");
                    if (q.Text.Trim().Length > 0)
                        field1 = Helper.GetString(proxy.FillScalar(q.Text.Replace("field", field1)), field1);
                    field += ", " + Helper.QoutedString(Helper.GetString(proxy.FillScalar(q.Text.Replace("field", field1)), field1));
                    fieldName += ", " + fieldName1;
                }
            }
            #endregion


            // insert voucher
            if (voucherID == Null.NullInteger || !mOneVoucher.Checked)
            {
                string query = string.Format(@"DECLARE @FinanceYearID int;
            SELECT @FinanceYearID=ID FROM FinanceYear WHERE Status=2;
            INSERT INTO DocumentAccounting (No, Date, UserID, LockerID, DoComments, Reciever, TotalOwe, TotalDue, Mode, FinanceYearID, Status, ProgramID) 
            VALUES (dbo.[NeuVoucherNo](Null, @FinanceYearID, __PrOgramiD__, NULL), {0}, _UserID_, _UserID_, '', '', 0, 0, 0, @FinanceYearID, 1, __PrOgramiD__);
SELECT SCOPE_IDENTITY();"
                    , Helper.QoutedDate(DateTime.Now));
                query = SiteSettings.ReplaceOptions(query);
                voucherID = Helper.GetInt(proxy.FillScalar(query), Null.NullInteger);
                if (voucherID == Null.NullInteger)
                    throw new Exception("Failed to insert voucher! : " + proxy.LastException.Message);
            }
            string qry = string.Format(@"INSERT INTO DocumentAccountingItems (DocumentAccountingID, TreePath, Owe, Due, CheckID, PayCheckID{0}) 
            VALUES ({1}, {2}, {3}, {4}, NULL, NULL{5});
 -- Update Totals
  DECLARE @TotalOwe numeric(18, 2), @TotalDue numeric(18, 2);
  SELECT @TotalOwe = SUM(Owe), @TotalDue = SUM(Due) FROM DocumentAccountingItems WHERE DocumentAccountingID = {1}
  UPDATE DocumentAccounting SET TotalDue = @TotalDue, TotalOwe = @TotalOwe WHERE ID = {1}"
                , fieldName, voucherID, Helper.QoutedString(treePath), owe, due, field);
            proxy.RunQuery(qry);
        }
        ShowMessage(LangProvider.Instance["Done"], "success");
        if (voucherID != Null.NullInteger)
            AppendMessage(" [<a href='list.aspx?table=DocumentAccounting&id=" + voucherID + "'>" + LangProvider.Instance["ColView"] + "</a>]");
    }

    private void ImportMaster(Worksheet sheet, DbProxy proxy, string[] titles)
    {
        int rowIndex = Math.Max(sheet.Cells.FirstRowIndex, 0);
        if (mHeaderRow1.Checked)
            rowIndex += 1;
        // check if there is any row
        if (rowIndex > sheet.Cells.LastRowIndex)
            return;
        int voucherID = Null.NullInteger;
        for (; rowIndex <= sheet.Cells.LastRowIndex; rowIndex++)
        {
            Row row = sheet.Cells.GetRow(rowIndex);
            string field = "", fieldName = "", treePath = "";
            #region find fields
            for (int i = 0; i < titles.Length; i++)
            {
                DropDownList dp = mColumns.FindControl("dp" + i) as DropDownList;
                HiddenField h = mColumns.FindControl("h" + i) as HiddenField;
                DropDownList fld = mColumns.FindControl("fld" + i) as DropDownList;
                TextBox q = mColumns.FindControl("q" + i) as TextBox;
                if (dp.SelectedValue != "field")
                    continue;
                string field1 = Helper.GetString(row.GetCell(sheet.Cells.FirstColIndex + i).Value, "").Trim();
                string fieldName1 = fld.SelectedValue;
                if (fieldName1 == "DocDate")
                    field1 = Helper.ParseDate(field1, DateTime.Now).ToString("yyyy-MM-dd");
                if (q.Text.Trim().Length > 0)
                    field1 = Helper.GetString(proxy.FillScalar(q.Text.Replace("field", field1)), field1);
                field += ", " + Helper.QoutedString(Helper.GetString(proxy.FillScalar(q.Text.Replace("field", field1)), field1));
                fieldName += ", " + fieldName1;
            }
            #endregion
            // insert voucher
            if (voucherID == Null.NullInteger || !mOneVoucher.Checked)
            {
                string query = string.Format(@"DECLARE @FinanceYearID int;
            SELECT @FinanceYearID=ID FROM FinanceYear WHERE Status=2;
            INSERT INTO DocumentAccounting (No, Date, UserID, LockerID, DoComments, Reciever, TotalOwe, TotalDue, Mode, FinanceYearID, Status, ProgramID) 
            VALUES (dbo.[NeuVoucherNo](Null, @FinanceYearID, __PrOgramiD__, NULL), {0}, _UserID_, _UserID_, '', '', 0, 0, 0, @FinanceYearID, 1, __PrOgramiD__);
SELECT SCOPE_IDENTITY();"
                    , Helper.QoutedDate(DateTime.Now));
                query = SiteSettings.ReplaceOptions(query);
                voucherID = Helper.GetInt(proxy.FillScalar(query), Null.NullInteger);
                if (voucherID == Null.NullInteger)
                    throw new Exception("Failed to insert voucher! : " + proxy.LastException.Message);
            }
            #region find values
            for (int i = 0; i < titles.Length; i++)
            {
                decimal owe = 0, due = 0;
                DropDownList dp = mColumns.FindControl("dp" + i) as DropDownList;
                HiddenField h = mColumns.FindControl("h" + i) as HiddenField;
                DropDownList fld = mColumns.FindControl("fld" + i) as DropDownList;
                TextBox q = mColumns.FindControl("q" + i) as TextBox;
                if (dp.SelectedValue != "owe" && dp.SelectedValue != "due")
                    continue;
                if (dp.SelectedValue == "owe")
                {
                    owe = Helper.GetDecimal(Helper.GetString(row.GetCell(sheet.Cells.FirstColIndex + i).Value, "").Trim(), 0);
                    treePath = h.Value;
                }
                else if (dp.SelectedValue == "due")
                {
                    due = Helper.GetDecimal(Helper.GetString(row.GetCell(sheet.Cells.FirstColIndex + i).Value, "").Trim(), 0);
                    treePath = h.Value;
                }
                string qry = string.Format(@"INSERT INTO DocumentAccountingItems (DocumentAccountingID, TreePath, Owe, Due, CheckID, PayCheckID{0}) 
            VALUES ({1}, {2}, {3}, {4}, NULL, NULL{5});
 -- Update Totals
  DECLARE @TotalOwe numeric(18, 2), @TotalDue numeric(18, 2);
  SELECT @TotalOwe = SUM(Owe), @TotalDue = SUM(Due) FROM DocumentAccountingItems WHERE DocumentAccountingID ={1}
  UPDATE DocumentAccounting SET TotalDue = @TotalDue, TotalOwe = @TotalOwe WHERE ID = {1}"
               , fieldName, voucherID, Helper.QoutedString(treePath), owe, due, field);
                proxy.RunQuery(qry);
            }
            #endregion
           
        }
        ShowMessage(LangProvider.Instance["Done"], "success");
        if (voucherID != Null.NullInteger)
            ShowMessage(" [<a href='list.aspx?table=DocumentAccounting&id=" + voucherID + "'>" + LangProvider.Instance["ColView"] + "</a>]");
    }
    void BindPreview()
    {
        mHeaders.Text = "";
        mPreview.Text = "";
        string file = Path.Combine(TempDir, mFileName.Text);
        CompoundDocument doc = null;
        try
        {
            doc = CompoundDocument.Open(file);
            byte[] bookdata = doc.GetStreamData("Workbook");
            if (bookdata == null) return;
            Workbook book = WorkbookDecoder.Decode(new MemoryStream(bookdata));

            Worksheet sheet = book.Worksheets[0];
            int rowIndex = Math.Max(sheet.Cells.FirstRowIndex, 0);
            Row row = sheet.Cells.GetRow(rowIndex);
            for (int colIndex = sheet.Cells.FirstColIndex; colIndex <= sheet.Cells.LastColIndex; colIndex++)
            {
                string title = Helper.GetString(row.GetCell(colIndex).Value, "");
                mHeaders.Text += title.Replace(',', '_') + ",";
            }
            if (mHeaders.Text.Length == 0)
                return;
            mHeaders.Text = mHeaders.Text.Substring(0, mHeaders.Text.Length - 1);

            StringBuilder b = new StringBuilder();
            b.Append("<table cellspacing=0 border=1>");
            string[] titles = mHeaders.Text.Split(',');
            b.Append("<tr>");
            for (int i = 0; i < titles.Length; i++)
                b.AppendFormat("<td>{0} {1}</td>", LangProvider.Instance["Column"], i + 1);
            b.Append("</tr>");
            b.Append("<tr>");
            for (int i = 0; i < titles.Length; i++)
                b.AppendFormat("<td>{0}</td>", titles[i]);
            b.Append("</tr>");
            rowIndex += 1;
            if (sheet.Cells.LastRowIndex >= rowIndex)
            {
                row = sheet.Cells.GetRow(rowIndex);
                b.Append("<tr>");
                for (int i = 0; i < titles.Length; i++)
                    b.AppendFormat("<td>{0}</td>", Helper.GetString(row.GetCell(sheet.Cells.FirstColIndex + i).Value, ""));
                b.Append("</tr>");
            }
            b.Append("</table>");
            mPreview.Text = b.ToString();
        }
        catch (Exception ex)
        {
            AppendMessage("Error: " + ex.Message);
        }
        finally
        {
            if (doc != null)
                doc.Close();
        }
    }
    void BindColumns()
    {
        mColumns.Controls.Clear();
        if (mHeaders.Text == "")
            return;
        StringBuilder b = new StringBuilder();
        mColumns.Controls.Add(new LiteralControl(string.Format("<table cellspacing=0 border=1><tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>"
            , LangProvider.Instance["Column"], LangProvider.Instance["Type"], LangProvider.Instance["Name"], LangProvider.Instance["Query"])));
        string[] titles = mHeaders.Text.Split(',');
        for (int i = 0; i < titles.Length; i++)
        {
            // column no
            mColumns.Controls.Add(new LiteralControl(string.Format("<tr><td>{0}</td>", i + 1)));

            // type
            mColumns.Controls.Add(new LiteralControl("<td>"));
            DropDownList dp = new DropDownList();
            dp.Items.Add(new ListItem(LangProvider.Instance["DontUse"], "empty"));
            dp.Items.Add(new ListItem(LangProvider.Instance["NatureOwe"], "owe"));
            dp.Items.Add(new ListItem(LangProvider.Instance["NatureDue"], "due"));
            dp.Items.Add(new ListItem(LangProvider.Instance["DataField"], "field"));
            dp.CssClass = "dp";
            dp.ID = "dp" + i;
            mColumns.Controls.Add(dp);
            mColumns.Controls.Add(new LiteralControl("</td>"));

            // topic | field
            mColumns.Controls.Add(new LiteralControl("<td>"));
            TextBox t = new TextBox();
            t.ID = "t" + i;
            t.CssClass = "t";
            mColumns.Controls.Add(t);
            HiddenField h = new HiddenField();
            h.ID = "h" + i;
            mColumns.Controls.Add(h);

            dp = new DropDownList();
            dp.Items.Add(new ListItem("DocNo"));
            dp.Items.Add(new ListItem("DocDate"));
            dp.Items.Add(new ListItem("Comments"));
            dp.Items.Add(new ListItem("PersonnelID"));
            dp.Items.Add(new ListItem("SecPersonnelID"));
            dp.CssClass = "fld";
            dp.ID = "fld" + i;
            mColumns.Controls.Add(dp);
            mColumns.Controls.Add(new LiteralControl("</td>"));

            // topic | field
            mColumns.Controls.Add(new LiteralControl("<td>"));
            TextBox q = new TextBox();
            q.ID = "q" + i;
            q.CssClass = "q";
            mColumns.Controls.Add(q);
            mColumns.Controls.Add(new LiteralControl("</td>"));

            b.Append("</tr>");
        }
        mColumns.Controls.Add(new LiteralControl("</table>"));
    }
    private void ShowMessage(string message, string type="danger")
    {
        if (type != "success" && type != "danger")
        {
            type = "danger";
        }

        mMsg.Text = message;
        mMsg.Visible = true;
        mMsg.CssClass += " alert-" + type;
    }

    private void AppendMessage(string message)
    {
        mMsg.Text += message;
    }
}