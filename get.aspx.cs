﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Xml;
using System.Text;
using OfficeOpenXml;
using System.Drawing.Imaging;

public partial class get : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        System.Threading.Thread.CurrentThread.CurrentCulture = Helper.NumberCulture;

        //System.Threading.Thread.Sleep(2000);
        string op = Request.QueryString["op"];
        if (op == null)
            Response.End();
        op = op.ToLower();
        if (op == "GetHtml".ToLower())
        {
            Response.Clear();
            //System.Threading.Thread.Sleep(2000);
            Response.Write(GetHtml());
        }
        else if (op == "GetHtmlLang".ToLower())
        {
            Response.Clear();
            Response.Write(GetHtmlLang());
        }
        else if (op == "GetHtmlOrder".ToLower())
        {
            Response.Clear();
            string content = new SalaryFormProvider(Request).GetOrderForm();
            Response.Write(GetHtmlOrder(content));
        }
        else if (op == "GetForm".ToLower())
        {
            Response.Clear();
            Response.Write(new FormEngine(Request).GetForm());
        }
        else if (op == "GetMenu".ToLower())
        {
            Response.Clear();
            Response.Write(GetMenu());
        }
        else if (op == "GetReportFilter".ToLower())
        {
            Response.Clear();
            Response.Write(GetReportFilter());
        }
        else if (op == "Auto".ToLower() || op == "Auto2".ToLower())
        {
            Response.Clear();
            Response.Write(GetAutocomplete());
        }
        else if (op == "AutoForm".ToLower())
        {
            Response.Clear();
            Response.Write(GetAutocompleteForm());
        }
        else if (op == "AutoTables".ToLower())
        {
            Response.Clear();
            Response.Write(GetAutocompleteTables());
        }
        else if (op == "GetTableFields".ToLower())
        {
            Response.Clear();
            Response.Write(GetTableFields());
        }
        else if (op == "GetReportFields".ToLower())
        {
            Response.Clear();
            Response.Write(GetReportFields());
        }
        else if (op == "GetFormFields".ToLower())
        {
            Response.Clear();
            Response.Write(GetFormFields());
        }
        else if (op == "GetXml".ToLower())
        {
            Response.Clear();
            Response.ContentType = "text/xml";
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            XmlDocument xmlDoc = GetXml();
            Response.Write(xmlDoc.OuterXml);
        }
        else if (op == "GetExcel".ToLower())
        {
            HandleGetExcel();
        }
        else if (op == "GetEnumTitle".ToLower())
        {
            Response.Clear();
            //System.Threading.Thread.Sleep(2000);
            Response.Write(GetEnumTitle());
        }
        else if (op == "GetValue".ToLower())
        {
            Response.Clear();
            //System.Threading.Thread.Sleep(2000);
            Response.Write(GetValue());
        }
        else if (op == "GetValueFiltered".ToLower())
        {
            Response.Clear();
            //System.Threading.Thread.Sleep(2000);
            Response.Write(GetValueFiltered());
        }
        else if (op == "excel")
        {
            HandleExcel();
        }
        else if (op == "stroelabels")
        {
            HandleStoreLabel();
        }
        else if (op == "app")
        {
            HandleApp();
        }
        // Documents
        else if (op == "EditAcc".ToLower())
        {
            Response.Clear();
            int id = Helper.GetInt(Request.QueryString["id"], Null.NullInteger);
            VoucherInfo info = AccController.GetVoucher(id);
            if (info == null)
                Response.Write("Error:" + LangProvider.Instance["WrongRecord"]);
            else
            {
                // check if it is in edit mode by other user
                if (info.LockerID != Null.NullInteger && info.LockerID != SiteSettings.UserID)
                    Response.Write("Error:" + LangProvider.Instance["AlreadyLocked"]);
                else
                {
                    // set into edit mode
                    if (info.Status != 1)
                    {
                        AccController.LockVoucher(info);
                        if (info.UserID != SiteSettings.UserID)
                            AccController.UpdateVoucherModifier(info.ID, SiteSettings.UserID, LangProvider.Instance["Edit"]);
                    }
                    Response.Write(GetHtml());
                }
            }
        }
        else if (op == "simg") // service image
        {
            HandleServiceImage();
        }
        Response.End();
    }

    void HandleApp()
    {
        string sub = Request.QueryString["sub"];
        using (DbProxy proxy = new DbProxy())
        {
            if (sub == "version")
                Response.Write(AppHandler.HandleVersion(Request));
            else if (sub == "province")
                Response.Write(AppHandler.HandleProvince(Request));
            else if (sub == "city")
                Response.Write(AppHandler.HandleCity(Request));
            else if (sub == "neighbourhood")
                Response.Write(AppHandler.HandleNeighbourhood(Request));
            else if (sub == "grouping")
                Response.Write(AppHandler.HandleGrouping(Request));
            else if (sub == "groupsbyid")
                Response.Write(AppHandler.HandleGroupingByID(Request));
            else if (sub == "vendors")
                Response.Write(AppHandler.HandleVendors(Request));
            else if (sub == "services")
                Response.Write(AppHandler.HandleServics(Request));
            else if (sub == "servicesbyid")
                Response.Write(AppHandler.HandleServicesByID(Request));
            else if (sub == "related")
                Response.Write(AppHandler.HandleRelatedServics(Request));
            else if (sub == "advs")
                Response.Write(AppHandler.HandleAdvertisements(Request));
            else if (sub == "advsbyid")
                Response.Write(AppHandler.HandleAdvertisementsByID(Request));
            else if (sub == "addadv")
                Response.Write(AppHandler.HandleAddAdvertisement(Request));
            else if (sub == "deladv")
                Response.Write(AppHandler.HandleDeleteAdvertisement(Request));
            else if (sub == "register")
                Response.Write(AppHandler.HandleRegister(Request));
            else if (sub == "login")
                Response.Write(AppHandler.HandleLogin(Request));
            else if (sub == "checkpass")
                Response.Write(AppHandler.HandleCheckPass(Request));
            else if (sub == "addbasket")
                Response.Write(AppHandler.HandleAddBasket(Request));
            else if (sub == "sendbasket")
                Response.Write(AppHandler.HandleSendBasketSMS(Request));
            else if (sub == "baskets")
                Response.Write(AppHandler.HandleBaskets(Request));
            else if (sub == "basket")
                Response.Write(AppHandler.HandleBasket(Request));
            else if (sub == "vendorconfig")
                Response.Write(AppHandler.HandleVendorConfig(Request));
            else if (sub == "vendorcomments")
                Response.Write(AppHandler.HandleVendorComments(Request));
            else if (sub == "userconfig")
                Response.Write(AppHandler.HandleUserConfig(Request));
            else if (sub == "messages")
                Response.Write(AppHandler.HandleMessages(Request));
            else if (sub == "markmessages")
                Response.Write(AppHandler.HandleMarkMessages(Request));
            else if (sub == "customerreport")
                Response.Write(AppHandler.HandleCustomerReport(Request));
            else if (sub == "storedraft")
                Response.Write(AppHandler.HandleStoreDraft(Request));
            else if (sub == "check")
                Response.Write(AppHandler.HandleCheck(Request));
            else if (sub == "visitorrep")
                Response.Write(AppHandler.HandleVisitorReport(Request));
        }

    }
    void HandleServiceImage()
    {
        int serviceID = Helper.GetInt(Request.QueryString["sid"], Null.NullInteger);
        if (serviceID == Null.NullInteger)
            return;
        using (DataTable dt = DbProxy.Instance.FillData("SELECT * FROM ServiceImage WHERE ServiceID = " + serviceID, "o"))
        {
            if (dt.Rows.Count == 0)
                return;
            string fileName = Helper.GetString(dt.Rows[0], "MainImage", null);
            if (string.IsNullOrEmpty(fileName))
                return;
            fileName = Server.MapPath("~/" + fileName);
            if (!File.Exists(fileName))
                return;
            using (System.Drawing.Image bitmap = System.Drawing.Image.FromFile(fileName))
            {
                using (var scaledImage = ImageUtilities.ResizeImage(bitmap, 400, 400 * bitmap.Height / bitmap.Width))
                {
                    Response.ContentType = "image/png";
                    scaledImage.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Png);
                    Response.Flush();
                }
            }
        }
    }
 
    string FindViewFile(string view)
    {
        if (string.IsNullOrEmpty(view))
            return null;
        string path = Server.MapPath("~/views/" + view + ".htm");
        if (File.Exists(path))
            return path;
        return null;
    }
    string FindViewJSFile(string view)
    {
        if (string.IsNullOrEmpty(view))
            return null;
        string path = Server.MapPath("~/viewjs/" + view + ".js");
        if (File.Exists(path))
            return path;
        return null;
    }
    public string GetHtml()
    {
        string view = Request.QueryString["view"];
        string id = Request.QueryString["id"];
        string table = Request.QueryString["table"];
        if (SiteSettings.User == null || (view != "duecommitment" && view != "commitment" && view != "druglimit" && view != "insurancecommitment" && !SiteSettings.User.HasAccess(table, AccessType.Edit)))
        {
            if (string.IsNullOrEmpty(id) || id == "-1")
                return LangProvider.Instance["GetAspxM1"];
            return LangProvider.Instance["GetAspxM2"];
        }
        string q = Request.QueryString["qd"];
        //TableInfo tInfo = TableController.Instance[table];
        List<TableInfo> tInfos = GetTableInfos(table);

        #region Check variables
        if (string.IsNullOrEmpty(view))
            if (tInfos != null && tInfos.Count > 0)
                view = tInfos[0].Detail;
        if (string.IsNullOrEmpty(view))
            return "";

        if (string.IsNullOrEmpty(q))
            if (tInfos != null && tInfos.Count > 0)
                q = tInfos[0].DetailQuery;
        #endregion

        string path = FindViewFile(view);
        if (string.IsNullOrEmpty(path))
            return "";
        string content = File.ReadAllText(path);

        #region purge bounding html
        int start = content.IndexOf("<body>");
        if (start >= 0)
            content = content.Substring(start + "<body>".Length);
        start = content.IndexOf("</body>");
        if (start >= 0)
            content = content.Substring(0, start);
        #endregion

        content = content.Replace("_TODAY_", Helper.FormatDate(DateTime.Now, null))
            .Replace("__SETTING_NATIONAL_CODE_FILE__", DbProxy.Instance.GetConfig("NATIONAL_CODE_FILE", "1") == "1" ? "nein" : "required")
            .Replace("__SETTING_NATIONAL_CODE_CLINIC__", DbProxy.Instance.GetConfig("NATIONAL_CODE_CLINIC", "1") == "1" ? "nein" : "required");
        if (!content.Contains("@"))
        {
            try
            {
                content = LangProvider.Instance.ParseLang(content);
            }
            catch { }
            return content;
        }

        string qry = string.IsNullOrEmpty(table) ? null : "SELECT * FROM @Table WHERE ID=@ID";
        if (!string.IsNullOrEmpty(q))
        {
            QueryInfo qInfo = QueryController.Instance[q];
            if (qInfo != null)
                if (!string.IsNullOrEmpty(qInfo.Query))
                    qry = qInfo.Query;
        }
        if (qry != null)
        {
            qry = qry.Replace("@Table", table).Replace("@ID", id);
            qry = Helper.ParseQueryVars(qry, tInfos, Request);
        }
        DataRow row = qry == null ? null : DbProxy.Instance.FillRow(qry);
        try
        {
            content = Helper.ParseVars(content, tInfos, row, Request);
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
            throw;
        }
        try
        {
            content = LangProvider.Instance.ParseLang(content);
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
            throw;
        }

        // add script
        if (Request.QueryString["disablejs"] != "1" && FindViewJSFile(view) != null)
            content += @"<script language='javascript'>$.getScript('viewjs/{0}.js', function(){view_{0}();});TweakUI();</script>".Replace("{0}", view);
        return content.Replace("@ID", id); // If there is @ID, replace it with the actual ID
    }
    public string GetHtmlLang()
    {
        string view = Request.QueryString["view"];
        string lang = Request.QueryString["lang"];
        string key = Request.QueryString["key"];
        string tran = Request.QueryString["trans"];
        if (SiteSettings.User == null || !SiteSettings.User.HasAccess("lang", AccessType.Edit))
        {
            return LangProvider.Instance["GetAspxM1"];
        }
        //TableInfo tInfo = TableController.Instance[table];

        string path = FindViewFile(view);
        if (string.IsNullOrEmpty(path))
            return "";
        string content = File.ReadAllText(path);

        #region purge bounding html
        int start = content.IndexOf("<body>");
        if (start >= 0)
            content = content.Substring(start + "<body>".Length);
        start = content.IndexOf("</body>");
        if (start >= 0)
            content = content.Substring(0, start);
        #endregion

        Dictionary<string, string> dict = LangProvider.LoadDict(lang);
        content = content.Replace("@Key", key)
            .Replace("@Translation", dict.ContainsKey(key) ? dict[key] : LangProvider.Instance[key]);

        try
        {
            content = LangProvider.Instance.ParseLang(content);
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
            throw;
        }

        return content;
    }
    public string GetMenu()
    {
        string url = Request.QueryString["url"];

        string path = Server.MapPath(url);
        if (!File.Exists(path))
            return "";
        string content = File.ReadAllText(path);

        #region purge bounding html
        int start = content.IndexOf("<!-- Menu Starts -->");
        if (start >= 0)
            content = content.Substring(start + "<!-- Menu Starts -->".Length);
        start = content.IndexOf("<!-- Menu End -->");
        if (start >= 0)
            content = content.Substring(0, start);
        #endregion

        return content.Replace("href=\"../", "href=\"").Replace("href='../", "href='");
    }
    public string GetReportFilter()
    {
        string view = Request.QueryString["view"];

        string path = Server.MapPath("~/reports/" + view + ".htm");
        if (!File.Exists(path))
            return "";
        string content = File.ReadAllText(path);

        #region purge bounding html
        int start = content.IndexOf("<body>");
        if (start >= 0)
            content = content.Substring(start + "<body>".Length);
        start = content.IndexOf("</body>");
        if (start >= 0)
            content = content.Substring(0, start);
        #endregion

        content = content.Replace("_TODAY_", Helper.FormatDate(DateTime.Now, null));
        return content;
    }

    public string GetAutocomplete()
    {
        string table = Request.QueryString["table"];
        string q = Request.QueryString["qd"];
        string title = Helper.NormalizeChars(Helper.GetString(Request.QueryString["q"], "").TrimStart());
        title = title.Replace("  ", " ").Replace("  ", " ").Replace(" ", "%");
        bool allowNull = Helper.GetBool(Request.QueryString["null"], false);
        string query = "SELECT TOP 50 ID, Title FROM @Table WHERE Title LIKE N'@Title%'";
        if (!string.IsNullOrEmpty(q))
        {
            QueryInfo qInfo = QueryController.Instance[q];
            if (qInfo != null)
                if (!string.IsNullOrEmpty(qInfo.Query))
                    query = qInfo.Query;
        }
        query = query.Replace("@Table", table).Replace("@Title", title);
        // Limit the result count
        if (query.ToUpper().StartsWith("SELECT "))
        {
            if (!query.ToUpper().Contains(" TOP "))
                query = "SELECT TOP 50 " + query.Substring(7 /*"SELECT ".Length*/);
        }
        string more = Request.QueryString["more"];
        if (!string.IsNullOrEmpty(more))
            query += (query.ToLower().Contains(" where ") ? " AND " : " WHERE ") + more;
        query = SiteSettings.ReplaceOptions(query);
        try
        {
            query = Helper.ParseQueryVars(query, TableController.Instance[table], Request);
        }
        catch (Exception ex)
        {
            ExceptionLog.Instance.Add(ex);
            throw;
        }
        DataTable dt = null;
        try
        {
            dt = DbProxy.Instance.FillData(query, "fck");
            return GetAutocomplete(dt, allowNull);
        }
        finally
        {
            if (dt != null)
                dt.Dispose();
        }
    }
    public string GetAutocompleteForm()
    {
        //id=' + formDeclareItemID
        int id = Helper.GetInt(Request.QueryString["id"], -1);
        FormDeclareItem item = DbProxy.Instance.GetFormDeclareItem(id);
        if (item == null)
            return "";
        string title = Helper.GetString(Request.QueryString["q"], "").TrimStart();
        DataTable dt = null;
        try
        {
            dt = item.GetValues(title);
            return GetAutocomplete(dt, false);
        }
        finally
        {
            if (dt != null)
                dt.Dispose();
        }
    }
    public string GetAutocompleteTables()
    {
        string title = Helper.GetString(Request.QueryString["q"], "").TrimStart().ToLower();
        Dictionary<string, TableInfo>.Enumerator enu = TableController.Instance.Tables.GetEnumerator();
        System.Text.StringBuilder builder = new System.Text.StringBuilder();
        builder.Append(string.Format("{0}|{1}|{2}{3}", "", "", "", Environment.NewLine));
        int count = 0;
        while (enu.MoveNext() && count < 10)
            if (enu.Current.Key.ToLower().Contains(title) || enu.Current.Value.Title.ToLower().Contains(title))
            {
                count++;
                builder.Append(string.Format("{0}|{1}|{2}{3}", enu.Current.Key, enu.Current.Key, enu.Current.Value.Title, Environment.NewLine));
            }
        return builder.ToString();
    }

    string GetAutocomplete(DataTable dt, bool allowNull)
    {
        System.Text.StringBuilder builder = new System.Text.StringBuilder();
        if (allowNull)
            builder.Append(string.Format("{0}|{1}|{2}|{3}|{4}|{5}{6}", 0, LangProvider.Instance["Null"], "", "", "", "", Environment.NewLine));
        if (dt != null)
        {
            List<int> dateIndices = new List<int>();
            for (int j = 1; j < dt.Columns.Count; j++) // col is always ID
                if (dt.Columns[j].DataType == typeof(DateTime))
                    dateIndices.Add(j);

            // remove \r\n from all fields
            for (int i = 0; i < dt.Rows.Count; i++)
                for (int j = 1; j < dt.Columns.Count; j++) // col is always ID
                {
                    if (!(dt.Rows[i][j] is DBNull))
                        if (dt.Rows[i][j] is string)
                            if (((string)dt.Rows[i][j]).Contains("\n"))
                                dt.Rows[i][j] = ((string)dt.Rows[i][j]).Replace("\n", "").Replace("\r", "");
                }
            if (dateIndices.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    for (int j = 0; j < dt.Columns.Count - 1; j++)
                        if (dateIndices.Contains(j))
                            builder.Append(string.Format("{0}|", Helper.FormatDate(Helper.GetDateTime(dt.Rows[i][j], Helper.NullDate), null)));
                        else
                            builder.Append(string.Format("{0}|", dt.Rows[i][j]));
                    if (dateIndices.Contains(dt.Columns.Count - 1))
                        builder.Append(string.Format("{0}{1}", Helper.FormatDate(Helper.GetDateTime(dt.Rows[i][dt.Columns.Count - 1], Helper.NullDate), null), Environment.NewLine));
                    else
                        builder.Append(string.Format("{0}{1}", dt.Rows[i][dt.Columns.Count - 1], Environment.NewLine));
                }
            }
            else if (dt.Columns.Count > 6)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    for (int j = 0; j < dt.Columns.Count - 1; j++)
                        builder.Append(string.Format("{0}|", dt.Rows[i][j]));
                    builder.Append(string.Format("{0}{1}", dt.Rows[i][dt.Columns.Count - 1], Environment.NewLine));
                }
            }
            else if (dt.Columns.Count == 6) // ID, Fullname, NationalNo, No, Status, Repeat
                for (int i = 0; i < dt.Rows.Count; i++)
                    builder.Append(string.Format("{0}|{1}|{2}|{3}|{4}|{5}{6}", dt.Rows[i][0], dt.Rows[i][1], dt.Rows[i][2], dt.Rows[i][3], dt.Rows[i][4], dt.Rows[i][5], Environment.NewLine));
            else if (dt.Columns.Count == 5) // ID, Fullname, NationalNo, No, Status
                for (int i = 0; i < dt.Rows.Count; i++)
                    builder.Append(string.Format("{0}|{1}|{2}|{3}|{4}{5}", dt.Rows[i][0], dt.Rows[i][1], dt.Rows[i][2], dt.Rows[i][3], dt.Rows[i][4], Environment.NewLine));
            else if (dt.Columns.Count == 4) // ID, Title, Code, Price
                for (int i = 0; i < dt.Rows.Count; i++)
                    builder.Append(string.Format("{0}|{1}|{2}|{3}{4}", dt.Rows[i][0], dt.Rows[i][1], dt.Rows[i][2], dt.Rows[i][3], Environment.NewLine));
            else if (dt.Columns.Count == 3) // ID, Title, Code|Price
                for (int i = 0; i < dt.Rows.Count; i++)
                    builder.Append(string.Format("{0}|{1}|{2}{3}", dt.Rows[i][0], dt.Rows[i][1], dt.Rows[i][2], Environment.NewLine));
            else if (dt.Columns.Count == 2) // ID, Title
                for (int i = 0; i < dt.Rows.Count; i++)
                    builder.Append(string.Format("{0}|{1}{2}", dt.Rows[i][0], dt.Rows[i][1], Environment.NewLine));
            else
                for (int i = 0; i < dt.Rows.Count; i++)
                    builder.Append(string.Format("{0}{1}", dt.Rows[i][0], Environment.NewLine));
        }
        return builder.ToString();
    }
    public string GetTableFields()
    {
        string table = Helper.GetString(Request.QueryString["table"], "");
        TableInfo info = TableController.Instance[table];
        if (info == null)
            return "";
        System.Text.StringBuilder builder = new System.Text.StringBuilder();
        builder.AppendFormat("<table class='full-width table-with-border zebra'><tr><td>{0}</td><td>{1}</td></tr>", LangProvider.Instance["FieldName"], LangProvider.Instance["Title"]);
        for (int i = 0; i < info.Fields.Count; i++)
            builder.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", info.Fields[i].Name, info.Fields[i].Title);
        builder.Append("</table>");
        return builder.ToString();
    }
    public string GetReportFields()
    {
        string report = Helper.GetString(Request.QueryString["report"], "");
        ReportFile info = DbProxy.Instance.GetReport(report);
        if (info == null)
            return "";
        System.Text.StringBuilder builder = new System.Text.StringBuilder();
        builder.AppendFormat("<table border=1 cellspacing=0 cellpading=2><tr><td>{0}</td></tr>", LangProvider.Instance["Title"]);
        for (int i = 0; i < info.Fields.Count; i++)
            builder.AppendFormat("<tr><td>[{0}]</td></td></tr>", info.Fields[i].FullTitle);
        for (int i = 0; i < info.AggregateFields.Count; i++)
            builder.AppendFormat("<tr><td>[{0}]</td></tr>", info.AggregateFields[i].Title);
        builder.Append("</table>");
        //builder.AppendFormat("<table border=1 cellspacing=0 cellpading=2><tr><td>{0}</td><td>{1}</td></tr>", LangProvider.Instance["FieldName"], LangProvider.Instance["Title"]);
        //for (int i = 0; i < info.Fields.Count; i++)
        //    builder.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", info.Fields[i].FullName, info.Fields[i].FullTitle);
        //for (int i = 0; i < info.AggregateFields.Count; i++)
        //    builder.AppendFormat("<tr><td>{0}</td><td>{1}</td></tr>", info.AggregateFields[i].FullName, info.AggregateFields[i].Title);
        //builder.Append("</table>");
        return builder.ToString();
    }
    public string GetFormFields()
    {
        string form = Helper.GetString(Request.QueryString["form"], "");
        List<FormDeclareItem> items = DbProxy.Instance.GetFormDeclareItemsByName(form);
        if (items == null)
            return "";
        System.Text.StringBuilder builder = new System.Text.StringBuilder();
        builder.AppendFormat("<table border=1 cellspacing=0 cellpading=2><tr><td>{0}</td></tr>", LangProvider.Instance["FieldName"]);
        for (int i = 0; i < items.Count; i++)
            builder.AppendFormat("<tr><td>{0}</td></tr>", items[i].Title);
        builder.Append("</table>");
        return builder.ToString();
    }
    public void HandleGetExcel()
    {
        string table = Request.QueryString["table"];
        string q = Request.QueryString["qd"];
        string[] topRows = null;
        if (!string.IsNullOrEmpty(Helper.GetString(Request.QueryString["toprows"], null)))
            topRows = Helper.GetString(Request.QueryString["toprows"], "").Split('\n');
        TableInfo tInfo = TableController.Instance[table];
        string query = "SELECT * FROM @Table";
        if (!string.IsNullOrEmpty(q))
        {
            QueryInfo qInfo = QueryController.Instance[q];
            if (qInfo != null)
                if (!string.IsNullOrEmpty(qInfo.Query))
                    query = qInfo.Query;
        }
        query = Helper.ParseQueryVars(query, tInfo, Request);

        IDataReader reader = DbProxy.Instance.ExecuteReader(query);
        Exporter.ExportExcel(Response, reader, Request, topRows);
    }
    public XmlDocument GetXml()
    {
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml("<?xml version='1.0' encoding='UTF-8'?><Tables></Tables>");

        string table = Request.QueryString["table"];
        string q = Request.QueryString["qd"];
        TableInfo tInfo = TableController.Instance[table];
        string query = "SELECT * FROM @Table";
        if (!string.IsNullOrEmpty(q))
        {
            QueryInfo qInfo = QueryController.Instance[q];
            if (qInfo != null)
                if (!string.IsNullOrEmpty(qInfo.Query))
                    query = qInfo.Query;
        }
        query = Helper.ParseQueryVars(query, tInfo, Request);

        DataSet ds = DbProxy.Instance.FillDataSet(query);
        /*
         * <Tables>
         *  <T>         // firts table
         *      <R ID='' Title='' />    // an attribute for each field
         *      <R ID='' Title='' />
         *  </T>
         *  
         *  <T>         // n-th table
         *      <R ID='' Code='' />
         *      <R ID='' Code='' />
         *  </T>
         * </Tables>
         * 
         */
        if (ds != null && ds.Tables.Count > 0)
            for (int t = 0; t < ds.Tables.Count; t++)
            {
                DataTable dt = ds.Tables[t];
                XmlNode tNode = xmlDoc.CreateNode(XmlNodeType.Element, "T", "");
                if (dt != null && dt.Rows.Count > 0 && dt.Columns.Count > 0)
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        XmlNode node = xmlDoc.CreateNode(XmlNodeType.Element, "R", "");
                        for (int j = 0; j < dt.Columns.Count; j++)
                        {
                            if (dt.Columns[j].DataType == typeof(DateTime))
                                node.Attributes.Append(XmlHelper.CreateXmlAttr(xmlDoc, dt.Columns[j].ColumnName, Helper.FormatDate(Helper.GetDateTime(dt.Rows[i][j], Helper.NullDate), null)));
                            else
                                node.Attributes.Append(XmlHelper.CreateXmlAttr(xmlDoc, dt.Columns[j].ColumnName, dt.Rows[i][j].ToString().Replace("\"", "&quot;")));
                        }
                        tNode.AppendChild(node);
                    }
                xmlDoc.DocumentElement.AppendChild(tNode);
            }

        return xmlDoc;
    }
    string GetEnumTitle()
    {
        int id = (int)Helper.GetFloat(Request.QueryString["id"], 0);
        string enumTable = Request.QueryString["enumTable"];
        return DbProxy.Instance.FillScalarString(string.Format("SELECT Title FROM {0} WHERE ID={1}", enumTable, id));
    }
    List<TableInfo> GetTableInfos(string table)
    {
        List<TableInfo> tInfos = null;
        if (!string.IsNullOrEmpty(table) && table.Contains(","))
        {
            tInfos = new List<TableInfo>();
            string[] tables = table.Split(',');
            for (int i = 0; i < tables.Length; i++)
            {
                TableInfo tInfo = TableController.Instance[tables[i]];
                if (tInfo != null)
                    tInfos.Add(tInfo);
            }
        }
        else
        {
            TableInfo tInfo = TableController.Instance[table];
            if (tInfo != null)
            {
                tInfos = new List<TableInfo>();
                tInfos.Add(tInfo);
            }
        }
        return tInfos;
    }
    private string GetValue()
    {
        using (DbProxy mDbProxy = new DbProxy())
        {
            string table = Request.QueryString["table"];
            string q = Request.QueryString["qd"];
            List<TableInfo> tInfos = GetTableInfos(table);

            string qry = null;
            if (!string.IsNullOrEmpty(q))
            {
                QueryInfo qInfo = QueryController.Instance[q];
                if (qInfo != null)
                    if (!string.IsNullOrEmpty(qInfo.Query))
                        qry = qInfo.Query;
            }
            if (string.IsNullOrEmpty(qry))
                return "Error: NO QUERY";

            qry = qry.Replace("@Table", table);
            try
            {
                qry = Helper.ParseQueryVars(qry, tInfos, Request);
            }
            catch (Exception ex)
            {
                return "Error: " + ex.Message;
            }
            try
            {
                mDbProxy.BeginTransaction();
                object obj = mDbProxy.FillScalar(qry);
                if (obj == null || obj is DBNull)
                {
                    mDbProxy.RollbackTransaction();
                    return "Error: " + mDbProxy.LastException.Message;
                }
                else
                {
                    mDbProxy.CommitTransaction();
                    return obj.ToString();
                }
            }
            catch (Exception ex)
            {
                return Helper.GetError(ex.Message, tInfos != null && tInfos.Count > 0 ? tInfos[0] : null);
            }
        }
    }
    private string GetValueFiltered()
    {
        using (DbProxy mDbProxy = new DbProxy())
        {
            string table = Request.QueryString["table"];
            string q = Request.QueryString["qd"];

            List<TableInfo> tInfos = GetTableInfos(table);
            QueryInfo qInfo = null;
            string qry = null;
            if (!string.IsNullOrEmpty(q))
            {
                qInfo = QueryController.Instance[q];
                if (qInfo != null)
                    if (!string.IsNullOrEmpty(qInfo.Query))
                        qry = qInfo.Query;
            }
            if (string.IsNullOrEmpty(qry))
                return "Error: NO QUERY";

            qry = qry.Replace("@Table", table);
            try
            {
                qry = SiteSettings.ReplaceOptions(qry);
                qry = Helper.ParseQueryVars(qry, tInfos, Request);
            }
            catch (Exception ex)
            {
                return "Error: " + ex.Message;
            }
            try
            {
                string whereCondition = "";
                whereCondition = tInfos[0].BuildWhereConditionFromUrl();

                string more = HttpContext.Current.Request.QueryString["more"];
                if (!string.IsNullOrEmpty(more))
                    whereCondition = (string.IsNullOrEmpty(whereCondition) ? " WHERE " : (whereCondition + " AND ")) + more;

                string w = "";
                if (!string.IsNullOrEmpty(qInfo.Where))
                {
                    w = qInfo.Where.Replace("_PersonnelID_", SiteSettings.User.PersonnelID.ToString())
                        .Replace("_UserID_", SiteSettings.UserID.ToString())
                        .Replace("_IsAdmin_", SiteSettings.User.IsAdmin ? "1" : "0");
                    w = Helper.ParseVars(w, tInfos[0], null, Request);
                }
                if (!string.IsNullOrEmpty(w))
                    whereCondition = (string.IsNullOrEmpty(whereCondition) ? " WHERE " : (whereCondition + " AND ")) + w;

                qry += whereCondition;
                qry = SiteSettings.ReplaceOptions(qry);

                mDbProxy.BeginTransaction();
                object obj = mDbProxy.FillScalar(qry);
                if (obj == null || obj is DBNull)
                {
                    mDbProxy.RollbackTransaction();
                    return "Error: " + mDbProxy.LastException.Message;
                }
                else
                {
                    mDbProxy.CommitTransaction();
                    return obj.ToString();
                }
            }
            catch (Exception ex)
            {
                return Helper.GetError(ex.Message, tInfos != null && tInfos.Count > 0 ? tInfos[0] : null);
            }
        }
    }
    private void HandleExcel()
    {
        // version 0
        string table = HttpContext.Current.Request.QueryString["table"];
        string q = HttpContext.Current.Request.QueryString["qd"];
        TableInfo tInfo = null;
        if (null != table)
            tInfo = TableController.Instance[table];

        // =====================================
        if (tInfo == null)
            return;

        string select = "*", from = table, w = "";
        if (!string.IsNullOrEmpty(q))
        {
            QueryInfo qInfo = QueryController.Instance[q];

            if (qInfo != null)
            {
                if (!string.IsNullOrEmpty(qInfo.From))
                    from = qInfo.From.Replace("@Table", table);
                if (!string.IsNullOrEmpty(qInfo.Select))
                    select = qInfo.Select;
                if (!string.IsNullOrEmpty(qInfo.Where))
                {
                    w = qInfo.Where.Replace("_PersonnelID_", SiteSettings.User.PersonnelID.ToString())
                        .Replace("_UserID_", SiteSettings.UserID.ToString())
                        .Replace("_IsAdmin_", SiteSettings.User.IsAdmin ? "1" : "0");
                    w = Helper.ParseVars(w, tInfo, null, Request);
                }
            }
        }
        string qry;
        if (select.ToLower().Contains("zeilenummer"))
            select = select.Replace("ZeileNummer,", "");

        string whereCondition = "";
        whereCondition = tInfo.BuildWhereConditionFromUrl();

        string more = HttpContext.Current.Request.QueryString["more"];
        if (!string.IsNullOrEmpty(more))
            whereCondition = (string.IsNullOrEmpty(whereCondition) ? " WHERE " : (whereCondition + " AND ")) + more;
        if (!string.IsNullOrEmpty(w))
            whereCondition = (string.IsNullOrEmpty(whereCondition) ? " WHERE " : (whereCondition + " AND ")) + w;
        //key1 , for do order by on query which returns data for grids print & excel
        string sortname = Helper.GetString(HttpContext.Current.Request.QueryString["orderby"], "ID");
        string sortorder = Helper.GetString(HttpContext.Current.Request.Form["sortorder"], "asc");
        string sortExp = (sortname + " " + sortorder).Trim();
        if (sortname != null && sortname.EndsWith("_cast2int"))
        {
            sortExp = ("CAST(" + sortname.Substring(0, sortname.Length - "_cast2int".Length) + " AS INT) " + sortorder).Trim();
        }

        qry = string.Format(@"SELECT ROW_NUMBER() OVER (ORDER BY {0}) AS Row, {1} FROM {2}  {3}"
            , sortExp, select, from, whereCondition, string.IsNullOrEmpty(sortExp) ? "ID" : sortExp);

        //key1 for support op "service.asmx/GetQueryList" from grids
        QueryInfo qInfo1 = QueryController.Instance[q];
        if (qInfo1 != null && select == "*" && !string.IsNullOrEmpty(qInfo1.Query) && !string.IsNullOrWhiteSpace(qInfo1.Query))
        {
            qry = qInfo1.Query;
            qry = Helper.ParseQueryVars(qry, TableController.Instance[table], HttpContext.Current.Request);
        }
        qry = SiteSettings.ReplaceOptions(qry);
        // ======================================
        IDataReader reader = DbProxy.Instance.ExecuteReader(qry);
        Exporter.ExportExcel(Response, reader, Request);
    }
    private void HandleStoreLabel()
    {
        // version 0
        string table = HttpContext.Current.Request.QueryString["table"];
        string q = HttpContext.Current.Request.QueryString["qd"];
        TableInfo tInfo = null;
        if (null != table)
            tInfo = TableController.Instance[table];

        // =====================================
        if (tInfo == null)
            return;

        string select = "*", from = table, w = "";
        if (!string.IsNullOrEmpty(q))
        {
            QueryInfo qInfo = QueryController.Instance[q];

            if (qInfo != null)
            {
                if (!string.IsNullOrEmpty(qInfo.From))
                    from = qInfo.From.Replace("@Table", table);
                if (!string.IsNullOrEmpty(qInfo.Select))
                    select = qInfo.Select;
                if (!string.IsNullOrEmpty(qInfo.Where))
                {
                    w = qInfo.Where.Replace("_PersonnelID_", SiteSettings.User.PersonnelID.ToString())
                        .Replace("_UserID_", SiteSettings.UserID.ToString())
                        .Replace("_IsAdmin_", SiteSettings.User.IsAdmin ? "1" : "0");
                    w = Helper.ParseVars(w, tInfo, null, Request);
                }
            }
        }
        string qry;
        if (select.ToLower().Contains("zeilenummer"))
            select = select.Replace("ZeileNummer,", "");

        string whereCondition = "";
        whereCondition = tInfo.BuildWhereConditionFromUrl();

        string more = HttpContext.Current.Request.QueryString["more"];
        if (!string.IsNullOrEmpty(more))
            whereCondition = (string.IsNullOrEmpty(whereCondition) ? " WHERE " : (whereCondition + " AND ")) + more;
        if (!string.IsNullOrEmpty(w))
            whereCondition = (string.IsNullOrEmpty(whereCondition) ? " WHERE " : (whereCondition + " AND ")) + w;
        //key1 , for do order by on query which returns data for grids print & excel
        string sortname = Helper.GetString(HttpContext.Current.Request.QueryString["orderby"], "ID");
        string sortorder = Helper.GetString(HttpContext.Current.Request.Form["sortorder"], "asc");
        string sortExp = (sortname + " " + sortorder).Trim();

        qry = string.Format(@"SELECT ROW_NUMBER() OVER (ORDER BY {0}) AS Row, {1} FROM {2}  {3}"
            , sortExp, select, from, whereCondition, string.IsNullOrEmpty(sortExp) ? "ID" : sortExp);

        //key1 for support op "service.asmx/GetQueryList" from grids
        QueryInfo qInfo1 = QueryController.Instance[q];
        if (qInfo1 != null && select == "*" && !string.IsNullOrEmpty(qInfo1.Query) && !string.IsNullOrWhiteSpace(qInfo1.Query))
        {
            qry = qInfo1.Query;
            qry = Helper.ParseQueryVars(qry, TableController.Instance[table], HttpContext.Current.Request);
        }
        qry = SiteSettings.ReplaceOptions(qry);


        // ======================================
        IDataReader reader = DbProxy.Instance.ExecuteReader(qry);
        int indexStoreTitle = reader.GetOrdinal("StoreTitle");
        int indexCode = reader.GetOrdinal("Code");
        int indexBarCode = reader.GetOrdinal("BarCode");
        int indexServiceName = reader.GetOrdinal("ServiceName");
        int indexUnitTitle = reader.GetOrdinal("UnitTitle");

        StringBuilder builder = new StringBuilder();
        builder.Append("<html>");
        builder.Append("<head><style type=\"text/css\">body {font-family:peyvand,arial;font-size:14.5;}span{display:inline-block;width:100px;}</style></head>");
        builder.Append("<body dir=rtl>");
        builder.AppendLine("<table border='0' cellpadding='3'><tr>");
        while (reader.Read())
        {
            builder.AppendLine("<tr>");
            builder.AppendLine("<td style='width:33%' valign=top><table border='0' cellspacing='0' cellpadding='3' style='width:97%;border:1px solid #000'>");
            builder.AppendLine(string.Format("<tr><td style='border:solid 1px #000;'>اول</td><td>{0}</td></tr>", reader.GetString(indexStoreTitle)));
            builder.AppendLine(string.Format("<tr><td colspan='2'>{0}</td></tr>", reader.GetString(indexCode)));
            builder.AppendLine(string.Format("<tr><td colspan='2'>{0}</td></tr>", reader.IsDBNull(indexBarCode) ? "" : reader.GetString(indexBarCode)));
            builder.AppendLine(string.Format("<tr><td colspan='2'>{0}</td></tr>", reader.GetString(indexServiceName)));
            builder.AppendLine(string.Format("<tr><td colspan='2'>شمارش&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : {0}</td></tr>", reader.IsDBNull(indexUnitTitle) ? "" : reader.GetString(indexUnitTitle)));
            builder.AppendLine("</table></td>");

            builder.AppendLine("<td style='width:33%' valign=top><table border='0' cellspacing='0' cellpadding='3' style='width:97%;border:1px solid #000'>");
            builder.AppendLine(string.Format("<tr><td style='border:solid 1px #000;'>دوم</td><td>{0}</td></tr>", reader.GetString(indexStoreTitle)));
            builder.AppendLine(string.Format("<tr><td colspan='2'>{0}</td></tr>", reader.GetString(indexCode)));
            builder.AppendLine(string.Format("<tr><td colspan='2'>{0}</td></tr>", reader.IsDBNull(indexBarCode) ? "" : reader.GetString(indexBarCode)));
            builder.AppendLine(string.Format("<tr><td colspan='2'>{0}</td></tr>", reader.GetString(indexServiceName)));
            builder.AppendLine(string.Format("<tr><td colspan='2'>شمارش&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : {0}</td></tr>", reader.IsDBNull(indexUnitTitle) ? "" : reader.GetString(indexUnitTitle)));
            builder.AppendLine("</table></td>");

            builder.AppendLine("<td style='width:33%' valign=top><table border='0' cellspacing='0' cellpadding='3' style='width:97%;border:1px solid #000'>");
            builder.AppendLine(string.Format("<tr><td style='border:solid 1px #000;'>سوم</td><td>{0}</td></tr>", reader.GetString(indexStoreTitle)));
            builder.AppendLine(string.Format("<tr><td colspan='2'>{0}</td></tr>", reader.GetString(indexCode)));
            builder.AppendLine(string.Format("<tr><td colspan='2'>{0}</td></tr>", reader.IsDBNull(indexBarCode) ? "" : reader.GetString(indexBarCode)));
            builder.AppendLine(string.Format("<tr><td colspan='2'>{0}</td></tr>", reader.GetString(indexServiceName)));
            builder.AppendLine(string.Format("<tr><td colspan='2'>شمارش&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : {0}</td></tr>", reader.IsDBNull(indexUnitTitle) ? "" : reader.GetString(indexUnitTitle)));
            builder.AppendLine("</table></td>");
            builder.AppendLine("</tr>");

        }
        builder.AppendLine("</table>");
        builder.Append("</body></html>");

        Response.Clear();
        Response.ClearHeaders();
        Response.ContentType = "application/vnd.ms-word";
        Response.AddHeader("Content-Disposition", "attachment; filename=\"storelabels.doc\"");
        Response.Write(builder.ToString());
        //response.OutputStream.Write(bytes, 0, bytes.Length);
        Response.Flush();
        Response.End();
    }
    public string GetHtmlOrder(string content)
    {
        string view = Request.QueryString["view"];
        string id = Request.QueryString["id"];
        string table = Request.QueryString["table"];
        string type = Request.QueryString["type"];
        if (SiteSettings.User == null || (view != "duecommitment" && view != "commitment" && view != "druglimit" && view != "insurancecommitment" && !SiteSettings.User.HasAccess(table, AccessType.Edit)))
        {
            if (string.IsNullOrEmpty(id) || id == "-1")
                return LangProvider.Instance["GetAspxM1"];
            return LangProvider.Instance["GetAspxM2"];
        }
        string q = Request.QueryString["qd"];
        string fq = Request.QueryString["fqd"];
        List<TableInfo> tInfos = GetTableInfos(table);

        #region Check variables
        if (string.IsNullOrEmpty(view))
            if (tInfos != null && tInfos.Count > 0)
                view = tInfos[0].Detail;
        if (string.IsNullOrEmpty(view))
            return "";

        if (string.IsNullOrEmpty(q))
            if (tInfos != null && tInfos.Count > 0)
                q = tInfos[0].DetailQuery;
        #endregion

        string path = FindViewFile(view);
        if (string.IsNullOrEmpty(path))
            return "";
        //string content = File.ReadAllText(path);

        #region purge bounding html
        int start = content.IndexOf("<body>");
        if (start >= 0)
            content = content.Substring(start + "<body>".Length);
        start = content.IndexOf("</body>");
        if (start >= 0)
            content = content.Substring(0, start);
        #endregion

        content = content.Replace("_TODAY_", Helper.FormatDate(DateTime.Now, null))
            .Replace("__SETTING_NATIONAL_CODE_FILE__", DbProxy.Instance.GetConfig("NATIONAL_CODE_FILE", "1") == "1" ? "nein" : "required")
            .Replace("__SETTING_NATIONAL_CODE_CLINIC__", DbProxy.Instance.GetConfig("NATIONAL_CODE_CLINIC", "1") == "1" ? "nein" : "required")
            .Replace("_ProgramID_", SiteSettings.ProgramID.ToString());
        if (!content.Contains("@"))
        {
            try
            {
                content = LangProvider.Instance.ParseLang(content);
            }
            catch { }
            return content;
        }

        string qry = string.IsNullOrEmpty(table) ? null : "SELECT * FROM @Table WHERE ID=@ID";
        if (!string.IsNullOrEmpty(q))
        {
            QueryInfo qInfo = QueryController.Instance[q];
            if (qInfo != null)
                if (!string.IsNullOrEmpty(qInfo.Query))
                    qry = qInfo.Query;
        }
        if (!string.IsNullOrEmpty(q))
        {
            QueryInfo qInfo = QueryController.Instance[q];
            if (qInfo != null)
                if (!string.IsNullOrEmpty(qInfo.Query))
                    qry = qInfo.Query;
        }
        if (qry != null)
        {
            qry = qry.Replace("@Table", table).Replace("@ID", id);
            qry = Helper.ParseQueryVars(qry, tInfos, Request);
        }
        string fqry = null;
        if (type == "Order")
        {
            fqry = "SELECT * FROM OrderValuesView WHERE OrderID=@ID";
            fqry = Helper.ParseQueryVars(fqry, TableController.Instance["OrderValues"], Request);
        }
        if (type == "Fish")
        {
            fqry = "SELECT * FROM FishValuesView WHERE FishID=@ID";
            fqry = Helper.ParseQueryVars(fqry, TableController.Instance["FishValues"], Request);
        }
        DataRow row = qry == null ? null : DbProxy.Instance.FillRow(qry);
        DataTable ftable = fqry == null ? null : DbProxy.Instance.FillData(fqry, "ftable");
        try
        {
            content = Helper.ParseFieldVars(content, tInfos, row, ftable, Request);
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
            throw;
        }
        try
        {
            content = LangProvider.Instance.ParseLang(content);
        }
        catch (Exception ex)
        {
            Logger.Log(ex);
            throw;
        }

        // add script
        if (Request.QueryString["disablejs"] != "1" && FindViewJSFile(view) != null)
            content += @"<script language='javascript'>$.getScript('viewjs/{0}.js', function(){view_{0}();});</script>".Replace("{0}", view);
        return content.Replace("@ID", id); // If there is @ID, replace it with the actual ID
    }
}