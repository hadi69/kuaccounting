﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class servicepricing : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.SetLang();
        mMsg.Value = "";
        Date.Value = Helper.FormatDate(DateTime.Now, null);
    }
    protected void doSavePrice_Click(object sender, EventArgs e)
    {
        string[] ServicePricingIDs = Helper.GetString(Request["ServicePriceID"], "").Split(',');
        string[] SaleTypeIDs = Helper.GetString(Request["SaleTypeID"], "").Split(',');
        string[] Prices = Helper.GetString(Request["Price"], "").Split(',');
        DbProxy proxy = new DbProxy();
        //proxy.RunQuery("UPDATE SaleTypeItem SET Deleted = 1");

        int num = 0;
        for (int i = 0; i < ServicePricingIDs.Length; i++)
        {
            if (string.IsNullOrEmpty(ServicePricingIDs[i]))
                continue;
            int servicePriceid = Helper.GetInt(ServicePricingIDs[i], -1);
            int saletypeid = Helper.GetInt(SaleTypeIDs[i], -1);
            decimal price = Helper.GetDecimal(Prices[Math.Min(i, Prices.Length - 1)], 0);
            if(price == 0)
                continue;
            {
                string script = @"
                IF EXISTS (SELECT ID FROM SaleTypeItem WHERE ServicePriceID = {0} AND SaleTypeID = {1} AND CONVERT(DATE,PriceDate)=CONVERT(DATE, GETDATE()))
                BEGIN
                    Update SaleTypeItem SET Price = {2} WHERE ServicePriceID = {0} AND SaleTypeID = {1} AND CONVERT(DATE,PriceDate)=CONVERT(DATE, GETDATE()) 
                    SELECT {0} ;
                END ELSE BEGIN
                    IF (SELECT Top 1 Price FROM SaleTypeItem WHERE ServicePriceID = {0} AND SaleTypeID = {1} ORDER BY PriceDate DESC ) <> {2}
                    BEGIN
                        INSERT INTO SaleTypeItem (ServicePriceID, SaleTypeID, Price, Deleted, PriceDate)
                            VALUES ({0}, {1}, {2}, 0, GETDATE());
                        SELECT SCOPE_IDENTITY();
                    END ELSE BEGIN
                        IF NOT EXISTS (SELECT ID FROM SaleTypeItem WHERE ServicePriceID = {0} AND SaleTypeID = {1})
                        BEGIN
                            INSERT INTO SaleTypeItem (ServicePriceID, SaleTypeID, Price, Deleted, PriceDate)
                                VALUES ({0}, {1}, {2}, 0, GETDATE());
                            SELECT SCOPE_IDENTITY();
                        END
                    END
                END";
                script = string.Format(script, servicePriceid, saletypeid, price);
                int id = Helper.GetInt(proxy.FillScalar(script), -1);
                if (id > 0)
                    num++;
            }
        }
        //proxy.RunQuery("DELETE FROM SaleTypeItem WHERE Deleted = 1");
        proxy.Dispose();
        mMsg.Value = string.Format(LangProvider.Instance["Saved"]);
    }
//    protected void doUpdate_Click(object sender, EventArgs e)
//    {
//        string[] ServicePricingIDs = Helper.GetString(Request["ServicePriceID"], "").Split(',');
//        string[] SaleTypeIDs = Helper.GetString(Request["SaleTypeID"], "").Split(',');
//        string[] Prices = Helper.GetString(Request["Price"], "").Split(',');
//        DbProxy proxy = new DbProxy();
//        //proxy.RunQuery("UPDATE SaleTypeItem SET Deleted = 1");

//        int num = 0;
//        for (int i = 0; i < ServicePricingIDs.Length; i++)
//        {
//            if (string.IsNullOrEmpty(ServicePricingIDs[i]))
//                continue;
//            int servicePriceid = Helper.GetInt(ServicePricingIDs[i], -1);
//            int saletypeid = Helper.GetInt(SaleTypeIDs[i], -1);
//            decimal price = Helper.GetDecimal(Prices[Math.Min(i, Prices.Length - 1)], 0);
//            {
//                string script = @"
//                IF EXISTS (SELECT ID FROM SaleTypeItem WHERE ServicePriceID = {0} AND SaleTypeID = {1} AND CONVERT(DATE,PriceDate)=CONVERT(DATE, GETDAATE()))
//                BEGIN
//                Update SaleTypeItem SET Price = {2}, PriceDate = GETDATE() WHERE ServicePriceID = {0} AND SaleTypeID = {1}
//                SELECT {0} ;
//                END
//                ELSE
//                BEGIN
//                INSERT INTO SaleTypeItem (ServicePriceID, SaleTypeID, Price, Deleted, PriceDate)
//                VALUES ({0}, {1}, {2}, 0, GETDATE());
//                SELECT SCOPE_IDENTITY();
//                END";
//                script = string.Format(script, servicePriceid, saletypeid, price);
//                int id = Helper.GetInt(proxy.FillScalar(script), -1);
//                if (id > 0)
//                    num++;
//            }
//        }
//        //proxy.RunQuery("DELETE FROM SaleTypeItem WHERE Deleted = 1");
//        proxy.Dispose();
//        mMsg.Value = string.Format(LangProvider.Instance["Update"]);
//    }
}